//
//  PlayerViewController.m
//  demo-macos
//
//  Created by Single on 2017/3/15.
//  Copyright © 2017年 single. All rights reserved.
//

#import "PlayerViewController.h"

#import "GPUImage/GPUImage.h"
#import "SLKStreamer.h"

@interface PlayerViewController () <SLKStreamerDelegate>

@property (weak) IBOutlet NSTextField *totalTimeLabel;
@property (weak) IBOutlet NSTextField *currentTimeLabel;
@property (weak) IBOutlet NSSlider *progressSilder;
@property (weak) IBOutlet NSButton *playButton;
@property (weak) IBOutlet NSButton *pauseButton;
@property (weak) IBOutlet NSTextField *stateLabel;
@property (weak) IBOutlet NSView *controlView;

@property (nonatomic, strong) SLKStreamer           *slkStreamer;
@property (nonatomic, strong) SLKStreamerOptions    *slkStreamerOptions;
@property (nonatomic, strong) GPUImageAVCamera   *videoCamera;
@property (nonatomic, strong) GPUImageView          *filteredVideoView;
@property (nonatomic, strong) GPUImageBrightnessFilter *brightnessFilter;
@property (nonatomic, strong) GPUImageContrastFilter   *contrastFilter;
@property (nonatomic, strong) GPUImageSaturationFilter *saturationFilter;
@property (nonatomic, strong) GPUImageSharpenFilter    *sharpenFilter;
@property (nonatomic, strong) GPUImageFilterGroup   *filterGroup;

//Configuration
@property (nonatomic, assign) CGSize  sourceVideoSize;
@property (nonatomic, assign) CGSize  targetVideoSize;
@property (nonatomic, assign) NSUInteger expectedSourceVideoFrameRate;
@property (nonatomic, assign) NSUInteger averageBitRate;
@property (nonatomic, assign) NSUInteger maxKeyFrameIntervalMs;

@end

@implementation PlayerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.wantsLayer = YES;
    self.view.layer.backgroundColor = [NSColor blackColor].CGColor;
    self.controlView.wantsLayer = YES;
    self.controlView.layer.backgroundColor = [NSColor colorWithWhite:0 alpha:0.5].CGColor;
    self.progressSilder.trackFillColor = [NSColor yellowColor];
}

- (void)setup:(NSString*)url
{
    //Init Configuration
    self.sourceVideoSize = CGSizeMake(1280, 720);
    self.targetVideoSize = CGSizeMake(1280, 720);
    self.expectedSourceVideoFrameRate = 20;
    self.averageBitRate = 1200;
    self.maxKeyFrameIntervalMs = 3000;
    
    [self setupSLKStreamer:url];
    [self setupGPUImage];
}

- (void)viewDidLayout
{
//    NSLog(@"viewDidLayout");
    
    runSynchronouslyOnVideoProcessingQueue(^{
        self.filteredVideoView.frame = self.view.frame;
    });
}

- (IBAction)play:(id)sender
{
    if (self.slkStreamer) {
        [self.slkStreamer start];
    }
}

- (IBAction)pause:(id)sender
{
    if (self.slkStreamer) {
        [self.slkStreamer stop];
    }
}

- (void)dealloc
{
    [self.filterGroup removeAllTargets];

    [self.videoCamera stopCameraCapture];
    [self.videoCamera removeInputsAndOutputs];
    [self.videoCamera removeAllTargets];

    [self.saturationFilter removeAllTargets];
    [self.sharpenFilter removeAllTargets];

    [self.filteredVideoView removeFromSuperview];
    
    [self.slkStreamer stop];
    [self.slkStreamer terminate];
    
    NSLog(@"PlayerViewController dealloc");
}

- (void)setupGPUImage {
    self.videoCamera = [[GPUImageAVCamera alloc] initWithSessionPreset:AVCaptureSessionPreset1280x720 cameraDevice:nil];
    self.videoCamera.frameRate = (int32_t)[self expectedSourceVideoFrameRate];
    
    self.filterGroup = [[GPUImageFilterGroup alloc] init];
    [self.videoCamera addTarget:self.filterGroup];

    self.saturationFilter = [[GPUImageSaturationFilter alloc] init];
    self.saturationFilter.saturation = 1.5f;
    self.sharpenFilter = [[GPUImageSharpenFilter alloc] init];
    self.sharpenFilter.sharpness = 0.5f;
    [self addGPUImageFilter:self.saturationFilter];
    [self addGPUImageFilter:self.sharpenFilter];

    self.filteredVideoView = [[GPUImageView alloc] initWithFrame:self.view.frame];
    [self.filteredVideoView setBackgroundColorRed:0.0f green:0.0f blue:0.0f alpha:1.0f];
    
    [self.view addSubview:self.filteredVideoView positioned:NSWindowBelow relativeTo:nil];

    [self.filterGroup addTarget:self.filteredVideoView];

    GPUImageRawDataOutput *rawDataOutput = [[GPUImageRawDataOutput alloc] initWithImageSize:[self sourceVideoSize] resultsInBGRAFormat:YES];
    [self.filterGroup addTarget:rawDataOutput];
    __weak GPUImageRawDataOutput *weakOutput = rawDataOutput;
    __weak typeof(self) wself = self;
    [rawDataOutput setNewFrameAvailableBlock:^{
        __strong GPUImageRawDataOutput *strongOutput = weakOutput;
        __strong typeof(wself) strongSelf = wself;
        [strongOutput lockFramebufferForReading];
        GLubyte *outputBytes = [strongOutput rawBytesForImage];
        NSInteger bytesPerRow = [strongOutput bytesPerRowInOutput];
        CVPixelBufferRef pixelBuffer = NULL;
        CVPixelBufferCreateWithBytes(kCFAllocatorDefault, [strongSelf sourceVideoSize].width, [strongSelf sourceVideoSize].height, kCVPixelFormatType_32BGRA, outputBytes, bytesPerRow, nil, nil, nil, &pixelBuffer);
        [strongOutput unlockFramebufferAfterReading];
        if(pixelBuffer == NULL) {
            return ;
        }
        [strongSelf pushPixelBuffer:pixelBuffer Rotation:0];
        CVPixelBufferRelease(pixelBuffer);
    }];
    
    [self.videoCamera startCameraCapture];
}

- (void)addGPUImageFilter:(GPUImageOutput<GPUImageInput> *)filter
{
    [_filterGroup addFilter:filter];
    
    GPUImageOutput<GPUImageInput> *newTerminalFilter = filter;
    
    NSInteger count = _filterGroup.filterCount;
    
    if (count == 1)
    {
        _filterGroup.initialFilters = @[newTerminalFilter];
        _filterGroup.terminalFilter = newTerminalFilter;
        
    } else
    {
        GPUImageOutput<GPUImageInput> *terminalFilter    = _filterGroup.terminalFilter;
        NSArray *initialFilters                          = _filterGroup.initialFilters;
        
        [terminalFilter addTarget:newTerminalFilter];
        
        _filterGroup.initialFilters = @[initialFilters[0]];
        _filterGroup.terminalFilter = newTerminalFilter;
    }
}

- (void)setupSLKStreamer:(NSString*)url {
    self.slkStreamer = [[SLKStreamer alloc] init];
    self.slkStreamer.delegate = self;
    
    self.slkStreamerOptions = [[SLKStreamerOptions alloc] init];
    self.slkStreamerOptions.hasVideo = true;
    self.slkStreamerOptions.videoEncodeType = VIDEO_SOFTWARE_ENCODE;
    self.slkStreamerOptions.videoSize = [self targetVideoSize];
    self.slkStreamerOptions.fps = [self expectedSourceVideoFrameRate];
    self.slkStreamerOptions.videoBitrate = [self averageBitRate] - 32;
    self.slkStreamerOptions.maxKeyFrameIntervalMs = [self maxKeyFrameIntervalMs];
    
    self.slkStreamerOptions.hasAudio = true;
    self.slkStreamerOptions.audioBitrate = 64;
    
    self.slkStreamerOptions.publishUrl = url;
    
    [self.slkStreamer initializeWithOptions:self.slkStreamerOptions];
//    [self.slkStreamer start];
}

- (void)pushPixelBuffer:(CVPixelBufferRef)pixelBuffer
               Rotation:(int)rotation
{
    [self.slkStreamer pushPixelBuffer:pixelBuffer Rotation:0];
}

- (void)gotConnecting
{
    NSLog(@"GPUImageViewController gotConnecting");
}

- (void)didConnected
{
    NSLog(@"GPUImageViewController didConnected");
    
}

- (void)gotStreaming
{
    NSLog(@"GPUImageViewController gotStreaming");
    
}

- (void)didPaused
{
    NSLog(@"GPUImageViewController didPaused");
    
}

- (void)gotStreamerErrorWithErrorType:(int)errorType
{
    NSLog(@"GPUImageViewController gotErrorWithErrorType");
    
    if (errorType == SLK_MEDIA_STREAMER_ERROR_POOR_NETWORK) {
        NSLog(@"Network is too poor");
    }
}

- (void)gotStreamerInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    NSLog(@"GPUImageViewController gotInfoWithInfoType");
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_REAL_BITRATE) {
        NSLog(@"Real Bitrate:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS) {
        NSLog(@"Real Fps:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_DELAY_TIME) {
        NSLog(@"buffer cache duration:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_DOWN_BITRATE) {
        NSLog(@"down target bitrate to:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_UP_BITRATE) {
        NSLog(@"up target bitrate to:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_TIME) {
        NSLog(@"Record Time:%f S",(float)(infoValue)/10.0f);
    }
}

- (void)didEndStreaming
{
    NSLog(@"GPUImageViewController didEndStreaming");
}

@end
