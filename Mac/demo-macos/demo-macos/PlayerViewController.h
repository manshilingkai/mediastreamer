//
//  PlayerViewController.h
//  demo-macos
//
//  Created by Single on 2017/3/15.
//  Copyright © 2017年 single. All rights reserved.
//

#import <Cocoa/Cocoa.h>

typedef NS_ENUM(NSUInteger, MediaStreamerType) {
    MediaStreamerType_SLKMediaStreamer = 0,
    MediaStreamerType_OBS,
    MediaStreamerType_FFmpeg,
    MediaStreamerType_Others,
};

@interface PlayerViewController : NSViewController

@property (nonatomic, assign) MediaStreamerType streamerType;

- (void)setup:(NSString*)url;

@end
