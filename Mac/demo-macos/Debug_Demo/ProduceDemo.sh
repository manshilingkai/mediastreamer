#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

cp ./libMediaStreamer.dylib MediaStreamerDemo.app/Contents/MacOS/
cd MediaStreamerDemo.app/Contents/MacOS
install_name_tool -change /usr/local/lib/libMediaStreamer.dylib @executable_path/libMediaStreamer.dylib MediaStreamerDemo
cd ../../../
