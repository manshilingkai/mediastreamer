//
//  GPUImageRGBFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/2/15.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageRGBFilter_h
#define GPUImageRGBFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageRGBFilter : public GPUImageFilter {
public:
    GPUImageRGBFilter();
    ~GPUImageRGBFilter();
protected:
    void onInit();
private:
    static const char RGB_FRAGMENT_SHADER[];
    
    int mRedLocation;
    int mGreenLocation;
    int mBlueLocation;
};

#endif /* GPUImageRGBFilter_h */
