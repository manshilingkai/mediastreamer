//
//  GPUImageBrooklynFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/8/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageBrooklynFilter_h
#define GPUImageBrooklynFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageBrooklynFilter : public GPUImageFilter{
public:
    GPUImageBrooklynFilter(char* filter_dir);
    ~GPUImageBrooklynFilter();
    
protected:
    void onInit();
    void onInitialized();
    void onDestroy();
    
    void onDrawArraysPre();
    void onDrawArraysAfter();
    
private:
    static const char BROOKLYN_FRAGMENT_SHADER[];
    
private:
    GLuint inputTextureHandles[3];
    int inputTextureUniformLocations[3];
    
private:
    char* mFilterDir;
};

#endif /* GPUImageBrooklynFilter_h */
