//
//  GPUImageTwoInputFilter.h
//  MediaStreamer
//
//  Created by Think on 2017/3/10.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageTwoInputFilter_h
#define GPUImageTwoInputFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageTwoInputFilter : public GPUImageFilter {
public:
    GPUImageTwoInputFilter(const char* fragmentShader);
    virtual ~GPUImageTwoInputFilter();
    
    int onDrawFrame(const int firstInputTextureId, int secondInputTextureId, int x, int y, int secondInputTexture_Width, int secondInputTexture_Height);
protected:
    void onInit();
    void onDrawArraysPre();
    void onDrawArraysAfter();
private:
    static const char TWOINPUT_VERTEX_SHADER[];
    
    float* mTexture2CoordinatesBuffer;
    
    int mFilterSecondTextureCoordinateAttribute;
    int mFilterInputTextureUniform2;
    
    int leftTopUniform;
    int rightBottomUniform;
    
    GLuint mFilterSourceTexture2;
    
    int SecondInput_X;
    int SecondInput_Y;
    int SecondInput_Width;
    int SecondInput_height;
};

#endif /* GPUImageTwoInputFilter_h */
