//
//  GPUImageBrannanFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2017/8/10.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageBrannanFilter.h"
#include "ImageProcesserUtils.h"
#include "StringUtils.h"
#include "OpenGLUtils.h"

const char GPUImageBrannanFilter::BRANNAN_FRAGMENT_SHADER[] = {
    "precision mediump float;\n"
    "varying mediump vec2 textureCoordinate;\n"
    "uniform sampler2D inputImageTexture;\n"
    "uniform sampler2D inputImageTexture2;\n"
    "uniform sampler2D inputImageTexture3;\n"
    "uniform sampler2D inputImageTexture4;\n"
    "uniform sampler2D inputImageTexture5;\n"
    "uniform sampler2D inputImageTexture6;\n"
    "mat3 saturateMatrix = mat3(1.105150,-0.044850,-0.046000,-0.088050,1.061950,-0.089200,-0.017100,-0.017100,1.132900);\n"
    "vec3 luma = vec3(.3, .59, .11);\n"
    "uniform float strength;\n"
    "void main()\n"
    "{\n"
    "vec4 originColor = texture2D(inputImageTexture, textureCoordinate);\n"
    "vec3 texel = texture2D(inputImageTexture, textureCoordinate).rgb;\n"
    "vec2 lookup;\n"
    "lookup.y = 0.5;\n"
    "lookup.x = texel.r;\n"
    "texel.r = texture2D(inputImageTexture2, lookup).r;\n"
    "lookup.x = texel.g;\n"
    "texel.g = texture2D(inputImageTexture2, lookup).g;\n"
    "lookup.x = texel.b;\n"
    "texel.b = texture2D(inputImageTexture2, lookup).b;\n"
    "texel = saturateMatrix * texel;\n"
    "vec2 tc = (2.0 * textureCoordinate) - 1.0;\n"
    "float d = dot(tc, tc);\n"
    "vec3 sampled;\n"
    "lookup.y = 0.5;\n"
    "lookup.x = texel.r;\n"
    "sampled.r = texture2D(inputImageTexture3, lookup).r;\n"
    "lookup.x = texel.g;\n"
    "sampled.g = texture2D(inputImageTexture3, lookup).g;\n"
    "lookup.x = texel.b;\n"
    "sampled.b = texture2D(inputImageTexture3, lookup).b;\n"
    "float value = smoothstep(0.0, 1.0, d);\n"
    "texel = mix(sampled, texel, value);\n"
    "lookup.x = texel.r;\n"
    "texel.r = texture2D(inputImageTexture4, lookup).r;\n"
    "lookup.x = texel.g;\n"
    "texel.g = texture2D(inputImageTexture4, lookup).g;\n"
    "lookup.x = texel.b;\n"
    "texel.b = texture2D(inputImageTexture4, lookup).b;\n"
    "lookup.x = dot(texel, luma);\n"
    "texel = mix(texture2D(inputImageTexture5, lookup).rgb, texel, .5);\n"
    "lookup.x = texel.r;\n"
    "texel.r = texture2D(inputImageTexture6, lookup).r;\n"
    "lookup.x = texel.g;\n"
    "texel.g = texture2D(inputImageTexture6, lookup).g;\n"
    "lookup.x = texel.b;\n"
    "texel.b = texture2D(inputImageTexture6, lookup).b;\n"
    "texel = mix(originColor.rgb, texel.rgb, strength);\n"
    "gl_FragColor = vec4(texel, 1.0);\n"
    "}\n"
};

GPUImageBrannanFilter::GPUImageBrannanFilter(char* filter_dir)
: GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, BRANNAN_FRAGMENT_SHADER)
{
    for (int i = 0; i<5; i++) {
        inputTextureHandles[i] = -1;
        inputTextureUniformLocations[i] = -1;
    }
    
    mFilterDir = NULL;
    
    if (filter_dir) {
        mFilterDir = strdup(filter_dir);
    }
}

GPUImageBrannanFilter::~GPUImageBrannanFilter()
{
    if (mFilterDir) {
        free(mFilterDir);
        mFilterDir = NULL;
    }
}

void GPUImageBrannanFilter::onInit()
{
    GPUImageFilter::onInit();
    
    inputTextureUniformLocations[0] = glGetUniformLocation(getProgram(), "inputImageTexture2");
    inputTextureUniformLocations[1] = glGetUniformLocation(getProgram(), "inputImageTexture3");
    inputTextureUniformLocations[2] = glGetUniformLocation(getProgram(), "inputImageTexture4");
    inputTextureUniformLocations[3] = glGetUniformLocation(getProgram(), "inputImageTexture5");
    inputTextureUniformLocations[4] = glGetUniformLocation(getProgram(), "inputImageTexture6");
}

void GPUImageBrannanFilter::onInitialized()
{
    GPUImageFilter::onInitialized();
    
    char* brannan_process_str = StringUtils::cat(mFilterDir, "/brannan_process.png");
    VideoFrame *brannan_process_frame = PNGImageFileToRGBAVideoFrame(brannan_process_str);
    free(brannan_process_str);
    
    if (brannan_process_frame) {
        inputTextureHandles[0] = OpenGLUtils::loadTexture(brannan_process_frame->data, brannan_process_frame->width, brannan_process_frame->height, inputTextureHandles[0]);
        if (brannan_process_frame->data) {
            free(brannan_process_frame->data);
            brannan_process_frame->data = NULL;
        }
        delete brannan_process_frame;
        brannan_process_frame = NULL;
    }
    
    char* brannan_blowout_str = StringUtils::cat(mFilterDir, "/brannan_blowout.png");
    VideoFrame *brannan_blowout_frame = PNGImageFileToRGBAVideoFrame(brannan_blowout_str);
    free(brannan_blowout_str);
    
    if (brannan_blowout_frame) {
        inputTextureHandles[1] = OpenGLUtils::loadTexture(brannan_blowout_frame->data, brannan_blowout_frame->width, brannan_blowout_frame->height, inputTextureHandles[1]);
        if (brannan_blowout_frame->data) {
            free(brannan_blowout_frame->data);
            brannan_blowout_frame->data = NULL;
        }
        delete brannan_blowout_frame;
        brannan_blowout_frame = NULL;
    }
    
    char* brannan_contrast_str = StringUtils::cat(mFilterDir, "/brannan_contrast.png");
    VideoFrame *brannan_contrast_frame = PNGImageFileToRGBAVideoFrame(brannan_contrast_str);
    free(brannan_contrast_str);
    
    if (brannan_contrast_frame) {
        inputTextureHandles[2] = OpenGLUtils::loadTexture(brannan_contrast_frame->data, brannan_contrast_frame->width, brannan_contrast_frame->height, inputTextureHandles[2]);
        if (brannan_contrast_frame->data) {
            free(brannan_contrast_frame->data);
            brannan_contrast_frame->data = NULL;
        }
        delete brannan_contrast_frame;
        brannan_contrast_frame = NULL;
    }
    
    char* brannan_luma_str = StringUtils::cat(mFilterDir, "/brannan_luma.png");
    VideoFrame *brannan_luma_frame = PNGImageFileToRGBAVideoFrame(brannan_luma_str);
    free(brannan_luma_str);
    
    if (brannan_luma_frame) {
        inputTextureHandles[3] = OpenGLUtils::loadTexture(brannan_luma_frame->data, brannan_luma_frame->width, brannan_luma_frame->height, inputTextureHandles[3]);
        if (brannan_luma_frame->data) {
            free(brannan_luma_frame->data);
            brannan_luma_frame->data = NULL;
        }
        delete brannan_luma_frame;
        brannan_luma_frame = NULL;
    }
    
    char* brannan_screen_str = StringUtils::cat(mFilterDir, "/brannan_screen.png");
    VideoFrame *brannan_screen_frame = PNGImageFileToRGBAVideoFrame(brannan_screen_str);
    free(brannan_screen_str);
    
    if (brannan_screen_frame) {
        inputTextureHandles[4] = OpenGLUtils::loadTexture(brannan_screen_frame->data, brannan_screen_frame->width, brannan_screen_frame->height, inputTextureHandles[4]);
        if (brannan_screen_frame->data) {
            free(brannan_screen_frame->data);
            brannan_screen_frame->data = NULL;
        }
        delete brannan_screen_frame;
        brannan_screen_frame = NULL;
    }
}

void GPUImageBrannanFilter::onDestroy()
{
    GPUImageFilter::onDestroy();
    
    glDeleteTextures(5, inputTextureHandles);
    for(int i = 0; i < 5; i++)
        inputTextureHandles[i] = OpenGLUtils::NO_TEXTURE;
}

void GPUImageBrannanFilter::onDrawArraysPre()
{
    for(int i = 0; i < 5
        && inputTextureHandles[i] != OpenGLUtils::NO_TEXTURE; i++){
        glActiveTexture(GL_TEXTURE0 + (i+1) );
        glBindTexture(GL_TEXTURE_2D, inputTextureHandles[i]);
        glUniform1i(inputTextureUniformLocations[i], (i+1));
    }
}

void GPUImageBrannanFilter::onDrawArraysAfter()
{
    for(int i = 0; i < 5
        && inputTextureHandles[i] != OpenGLUtils::NO_TEXTURE; i++){
        glActiveTexture(GL_TEXTURE0 + (i+1));
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
