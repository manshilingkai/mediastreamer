//
//  GPUImageRawPixelOutputFilter.h
//  MediaStreamer
//
//  Created by Think on 2017/2/28.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageRawPixelOutputFilter_h
#define GPUImageRawPixelOutputFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

#include "MediaDataType.h"

class GPUImageRawPixelOutputFilter : public GPUImageFilter{
public:
    GPUImageRawPixelOutputFilter();
    ~GPUImageRawPixelOutputFilter();

    //Create FBO
    void createFBO(int frameBufferWidth, int frameBufferHeight);

    //Delete FBO
    void deleteFBO();

    void bind();
    void unBind();

    VideoFrame *outputPixelBuffer();
    void outputPixelBuffer(VideoFrame *outputVideoFrame);

private:
    int mOutputFrameBufferWidth;
    int mOutputFrameBufferHeight;
    
    GLuint frameBuffer_;
    GLuint frameBufferTexture_;
    
    bool isCreateFBO;
    
    VideoFrame *mOutputVideoFrame;
};

#endif /* GPUImageRawPixelOutputFilter_h */
