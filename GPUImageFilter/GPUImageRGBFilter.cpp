//
//  GPUImageRGBFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/15.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageRGBFilter.h"

const char GPUImageRGBFilter::RGB_FRAGMENT_SHADER[] = {
    "precision highp float;\n"
    "varying highp vec2 textureCoordinate;\n"
    "uniform sampler2D inputImageTexture;\n"
    "uniform highp float redAdjustment;\n"
    "uniform highp float greenAdjustment;\n"
    "uniform highp float blueAdjustment;\n"
    "void main()\n"
    "{\n"
    "highp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);\n"
    "gl_FragColor = vec4(textureColor.r * redAdjustment, textureColor.g * greenAdjustment, textureColor.b * blueAdjustment, textureColor.a);\n"
    "}\n"
};

GPUImageRGBFilter::GPUImageRGBFilter()
    : GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, RGB_FRAGMENT_SHADER)
{
}

GPUImageRGBFilter::~GPUImageRGBFilter()
{
}

void GPUImageRGBFilter::onInit()
{
    GPUImageFilter::onInit();
    
    mRedLocation = glGetUniformLocation(getProgram(), "redAdjustment");
    mGreenLocation = glGetUniformLocation(getProgram(), "greenAdjustment");
    mBlueLocation = glGetUniformLocation(getProgram(), "blueAdjustment");
    
    setFloat(mRedLocation, 1.0f);
    setFloat(mGreenLocation, 1.0f);
    setFloat(mBlueLocation, 1.0f);
}
