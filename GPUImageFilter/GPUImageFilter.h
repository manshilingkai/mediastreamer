//
//  GPUImageFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/2/13.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageFilter_h
#define GPUImageFilter_h

#include <stdio.h>
#include "LinkedList.h"

#include "OpenGLUtils.h"

class GPUImageFilter {
public:
    GPUImageFilter();
    GPUImageFilter(const char* vertexShader, const char* fragmentShader);

    virtual ~GPUImageFilter();
    
    void init();
    void destroy();
    
    virtual int onDrawFrame(const int textureId, const float* cubeBuffer, const float* textureBuffer);
    virtual int onDrawFrame(const int textureId);
    
    int getProgram() { return mGLProgId; }
    
    virtual void onOutputSizeChanged(int width, int height);
protected:
    virtual void onInit();
    virtual void onInitialized();
    
    virtual void onDestroy() {}
    
    virtual void onDrawArraysPre() {}
    virtual void onDrawArraysAfter() {}
    
    void runPendingOnDrawTasks();
    
    void setInteger(int location, int intValue);
    void setFloat(int location, float floatValue);
    void setFloatVec2(int location, float* arrayValue, int valueCount);
    void setFloatVec3(int location, float* arrayValue, int valueCount);
    void setFloatVec4(int location, float* arrayValue, int valueCount);
    void setFloatArray(int location, float* arrayValue, int valueCount);
    void setPoint(int location, float* arrayValue, int valueCount);
    void setUniformMatrix3f(int location, float* matrix, int valueCount);
    void setUniformMatrix4f(int location, float* matrix, int valueCount);
    
    void runOnDraw(Runnable* runnable);
public:
    static const char NO_FILTER_VERTEX_SHADER[];
    static const char NO_FILTER_FRAGMENT_SHADER[];
private:
    LinkedList mRunOnDraw;
    const char* mVertexShader;
    const char* mFragmentShader;
protected:
    int mGLProgId;
    int mGLAttribPosition;
    int mGLUniformTexture;
    int mGLAttribTextureCoordinate;
    int mGLStrengthLocation;
    bool mIsInitialized;
    float* mGLCubeBuffer;
    float* mGLTextureBuffer;
    
    int mOutputWidth;
    int mOutputHeight;
};

#endif /* GPUImageFilter_h */

