//
//  GPUImageOffScreenRender.h
//  MediaStreamer
//
//  Created by Think on 2017/3/2.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageOffScreenRender_h
#define GPUImageOffScreenRender_h

#include <stdio.h>

#include "MediaDataType.h"

extern "C" {
#include "libavformat/avformat.h"
}

#include "MediaEffect.h"
//#include <wchar.h>

enum OFF_SCREEN_RENDER_TYPE{
    GPU_IMAGE_FILTER = 0,
};

enum OFF_SCREEN_RENDER_INPUT_TYPE{
    I420 = 0,
    NV12 = 1,
};

enum FILTER_TYPE {
    RGB_FILTER = 0,
    SKETCH_FILTER = 1,
    AMARO_FILTER = 2,
    ANTIQUE_FILTER = 3,
    BLACKCAT_FILTER = 4,
    BEAUTY_FILTER = 5,
    BRANNAN_FILTER = 6,
    N1977_FILTER = 7,
    BROOKLYN_FILTER = 8,
    COOL_FILTER = 9,
    CRAYON_FILTER = 10,
    
    BRIGHTNESS_FILTER = 11,
    CONTRAST_FILTER = 12,
    EXPOSURE_FILTER = 13,
    HUE_FILTER = 14,
    SATURATION_FILTER = 15,
    SHARPEN_FILTER = 16,
};

struct WordInfo {
    VideoFrame* wordBitmap;
    int x;
    int y;
    float alpha;
    bool isInRenderRect;
    
    WordInfo()
    {
        wordBitmap = NULL;
        x = 0;
        y = 0;
        alpha = 1.0f;
        isInRenderRect = false;
    }
};

struct TextInfo {
    vector<WordInfo*> words;
    int wordCount;
    
    TextInfo()
    {
        wordCount = 0;
    }
        
    inline void Clear()
    {
        for(vector<WordInfo*>::iterator it = words.begin(); it != words.end(); ++it)
        {
            WordInfo* wordInfo = *it;
            
            if(wordInfo!=NULL)
            {
                delete wordInfo;
                wordInfo = NULL;
            }
        }
        
        words.clear();
        
        wordCount = 0;
    }
    
    inline void Free()
    {
        for(vector<WordInfo*>::iterator it = words.begin(); it != words.end(); ++it)
        {
            WordInfo* wordInfo = *it;
            
            if(wordInfo!=NULL)
            {
                if(wordInfo->wordBitmap)
                {
                    if(wordInfo->wordBitmap->data)
                    {
                        free(wordInfo->wordBitmap->data);
                    }
                    
                    delete wordInfo->wordBitmap;
                }
                
                delete wordInfo;
                wordInfo = NULL;
            }
        }
        
        words.clear();
        
        wordCount = 0;
    }
};

class GPUImageOffScreenRender {
public:
    virtual ~GPUImageOffScreenRender() {}
    
    static GPUImageOffScreenRender* CreateGPUImageOffScreenRender(OFF_SCREEN_RENDER_TYPE type, OFF_SCREEN_RENDER_INPUT_TYPE inputType);
    static void DeleteGPUImageOffScreenRender(GPUImageOffScreenRender* offScreenRender, OFF_SCREEN_RENDER_TYPE type);
    
    virtual bool initialize(int displayWidth, int displayHeight) = 0;
    virtual void terminate() = 0;
    virtual bool isInitialized() = 0;
    
    virtual VideoFrame* render(AVFrame *inputVideoFrame) = 0;
    virtual void render(AVFrame *inputVideoFrame, VideoFrame* outputVideoFrame) = 0;

    virtual void setFilter(FILTER_TYPE filter_type, char* filter_dir, float strength) = 0;
    virtual void removeFilter(FILTER_TYPE filter_type) = 0;
    
    virtual int filterInputToFrameBufferTexture(AVFrame *inputVideoFrame, FILTER_TYPE filterType, char* filterDir, bool isAspectFit) = 0;
    virtual int normalBlendToFrameBufferTexture(VideoFrame* overlay, int rotation, float scale, bool flipHorizontal, bool flipVertical, int x, int y) = 0;
    virtual int normalBlendToFrameBufferTexture(VideoFrame* textBackGroundLay, int rotation, float scale, bool flipHorizontal, bool flipVertical, int x, int y, TextInfo* textInfo) = 0;
    virtual void pumpFrameBuffer(VideoFrame* outputVideoFrame) = 0;
    virtual VideoFrame* pumpFrameBuffer() = 0;
};

#endif /* GPUImageOffScreenRender_h */
