//
//  OpenGLUtils.h
//  MediaPlayer
//
//  Created by Think on 2017/2/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef OpenGLUtils_h
#define OpenGLUtils_h

#ifdef ANDROID
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#endif

#ifdef IOS
#include <OpenGLES/ES2/glext.h>
#endif

#include <stdio.h>
#include <stdlib.h>

class OpenGLUtils {
    
public:
    static const int NO_TEXTURE;
    static const int NOT_INIT;
    static const int ON_DRAWN;
    
    static GLuint loadProgram(const char* vertex_source, const char* fragment_source);
    
    static GLuint loadTexture(void *pixels, int width, int height, int usedTexId);
    
    static GLuint createTexture(void *pixels, int width, int height);
    static void deleteTexture(GLuint usedTexId);
private:
    
    static GLuint loadShader(GLenum shader_type, const char* shader_source);
};

#endif /* OpenGLUtils_h */
