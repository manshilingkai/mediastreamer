//
//  GPUImageFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/13.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageFilter.h"
#include "TextureRotationUtil.h"

const char GPUImageFilter::NO_FILTER_VERTEX_SHADER[] = {
    "attribute vec4 position;\n"
    "attribute vec4 inputTextureCoordinate;\n"
    " \n"
    "varying vec2 textureCoordinate;\n"
    " \n"
    "void main()\n"
    "{\n"
    "    gl_Position = position;\n"
    "    textureCoordinate = inputTextureCoordinate.xy;\n"
    "}\n"};

const char GPUImageFilter::NO_FILTER_FRAGMENT_SHADER[] = {
    "varying highp vec2 textureCoordinate;\n"
    " \n"
    "uniform sampler2D inputImageTexture;\n"
    " \n"
    "void main()\n"
    "{\n"
    "     gl_FragColor = texture2D(inputImageTexture, textureCoordinate);\n"
    "}\n"};

GPUImageFilter::GPUImageFilter()
{
    mVertexShader = NO_FILTER_VERTEX_SHADER;
    mFragmentShader = NO_FILTER_FRAGMENT_SHADER;
    
    mGLCubeBuffer = new float[8];
    for (int i = 0; i<8; i++) {
        mGLCubeBuffer[i] = TextureRotationUtil::CUBE[i];
    }
    mGLTextureBuffer = new float[8];
    TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, mGLTextureBuffer);
}

GPUImageFilter::GPUImageFilter(const char* vertexShader, const char* fragmentShader)
{
    mVertexShader = vertexShader;
    mFragmentShader = fragmentShader;
    
    mGLCubeBuffer = new float[8];
    for (int i = 0; i<8; i++) {
        mGLCubeBuffer[i] = TextureRotationUtil::CUBE[i];
    }
    mGLTextureBuffer = new float[8];
    TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, mGLTextureBuffer);
}

GPUImageFilter::~GPUImageFilter()
{
    delete [] mGLCubeBuffer;
    delete [] mGLTextureBuffer;
}

void GPUImageFilter::init()
{
    onInit();
    mIsInitialized = true;
    onInitialized();
}

void GPUImageFilter::onInit()
{
    mGLProgId = OpenGLUtils::loadProgram(mVertexShader, mFragmentShader);
    mGLAttribPosition = glGetAttribLocation(mGLProgId, "position");
    mGLUniformTexture = glGetUniformLocation(mGLProgId, "inputImageTexture");
    mGLAttribTextureCoordinate = glGetAttribLocation(mGLProgId, "inputTextureCoordinate");
    mGLStrengthLocation = glGetUniformLocation(mGLProgId, "strength");
}

void GPUImageFilter::onInitialized()
{
    setFloat(mGLStrengthLocation, 1.0f);
}

void GPUImageFilter::destroy()
{
    mIsInitialized = false;
    glDeleteProgram(mGLProgId);
    onDestroy();
}

void GPUImageFilter::runPendingOnDrawTasks()
{
    while (true) {
        Runnable *runnable = mRunOnDraw.pop();
        if (runnable) {
            runnable->run();
            delete runnable;
            runnable = NULL;
        }else break;
    }
}

int GPUImageFilter::onDrawFrame(const int textureId, const float* cubeBuffer, const float* textureBuffer)
{
    glUseProgram(mGLProgId);
    runPendingOnDrawTasks();
    if (!mIsInitialized) {
        return OpenGLUtils::NOT_INIT;
    }
    glVertexAttribPointer(mGLAttribPosition, 2, GL_FLOAT, false, 0, cubeBuffer);
    glEnableVertexAttribArray(mGLAttribPosition);
    glVertexAttribPointer(mGLAttribTextureCoordinate, 2, GL_FLOAT, false, 0, textureBuffer);
    glEnableVertexAttribArray(mGLAttribTextureCoordinate);
    
    if (textureId != OpenGLUtils::NO_TEXTURE) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureId);
        glUniform1i(mGLUniformTexture, 0);
    }
    onDrawArraysPre();
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glDisableVertexAttribArray(mGLAttribPosition);
    glDisableVertexAttribArray(mGLAttribTextureCoordinate);
    onDrawArraysAfter();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    return OpenGLUtils::ON_DRAWN;
}

int GPUImageFilter::onDrawFrame(const int textureId)
{
    glUseProgram(mGLProgId);
    runPendingOnDrawTasks();
    if (!mIsInitialized)
        return OpenGLUtils::NOT_INIT;
    glVertexAttribPointer(mGLAttribPosition, 2, GL_FLOAT, false, 0, mGLCubeBuffer);
    glEnableVertexAttribArray(mGLAttribPosition);
    
    glVertexAttribPointer(mGLAttribTextureCoordinate, 2, GL_FLOAT, false, 0, mGLTextureBuffer);
    glEnableVertexAttribArray(mGLAttribTextureCoordinate);
    
    if (textureId != OpenGLUtils::NO_TEXTURE) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureId);
        glUniform1i(mGLUniformTexture, 0);
    }
    
    onDrawArraysPre();
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glDisableVertexAttribArray(mGLAttribPosition);
    glDisableVertexAttribArray(mGLAttribTextureCoordinate);
    onDrawArraysAfter();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    return OpenGLUtils::ON_DRAWN;
}

void GPUImageFilter::setInteger(int location, int intValue)
{
    mRunOnDraw.push(new Runnable(SETINTEGER, location, intValue));
}

void GPUImageFilter::setFloat(int location, float floatValue)
{
    mRunOnDraw.push(new Runnable(SETFLOAT, location, floatValue));
}

void GPUImageFilter::setFloatVec2(int location, float* arrayValue, int valueCount)
{
    mRunOnDraw.push(new Runnable(SETFLOATVEC2, location, arrayValue, valueCount));
}

void GPUImageFilter::setFloatVec3(int location, float* arrayValue, int valueCount)
{
    mRunOnDraw.push(new Runnable(SETFLOATVEC3, location, arrayValue, valueCount));
}

void GPUImageFilter::setFloatVec4(int location, float* arrayValue, int valueCount)
{
    mRunOnDraw.push(new Runnable(SETFLOATVEC4, location, arrayValue, valueCount));
}

void GPUImageFilter::setFloatArray(int location, float* arrayValue, int valueCount)
{
    mRunOnDraw.push(new Runnable(SETFLOATARRAY, location, arrayValue, valueCount));
}

void GPUImageFilter::setPoint(int location, float* arrayValue, int valueCount)
{
    mRunOnDraw.push(new Runnable(SETPOINT, location, arrayValue, valueCount));
}

void GPUImageFilter::setUniformMatrix3f(int location, float* matrix, int valueCount)
{
    mRunOnDraw.push(new Runnable(SETUNIFORMMATRIX3F, location, matrix, valueCount));
}

void GPUImageFilter::setUniformMatrix4f(int location, float* matrix, int valueCount)
{
    mRunOnDraw.push(new Runnable(SETUNIFORMMATRIX4F, location, matrix, valueCount));
}

void GPUImageFilter::runOnDraw(Runnable* runnable)
{
    mRunOnDraw.push(runnable);
}

void GPUImageFilter::onOutputSizeChanged(int width, int height)
{
    mOutputWidth = width;
    mOutputHeight = height;
}
