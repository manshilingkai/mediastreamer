//
//  GPUImageTransformFilter.h
//  MediaStreamer
//
//  Created by Think on 2017/3/23.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageTransformFilter_h
#define GPUImageTransformFilter_h

#include "GPUImageFilter.h"
#include <stdio.h>

class GPUImageTransformFilter : public GPUImageFilter{
public:
    GPUImageTransformFilter();
    ~GPUImageTransformFilter();
    
    void onOutputSizeChanged(int width, int height);
    int onDrawFrame(const int textureId, const float* cubeBuffer, const float* textureBuffer);

    void setTransform3D(float* transform);
protected:
    void onInit();
private:
    static const char TRANSFORM_VERTEX_SHADER[];
private:
    int transformMatrixUniform;
    int orthographicMatrixUniform;
    float* orthographicMatrix;
    float* transform3D;
};

#endif /* GPUImageTransformFilter_h */
