//
//  GPUImageAmaroFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2017/3/21.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageAmaroFilter.h"
#include "ImageProcesserUtils.h"
#include "StringUtils.h"
#include "OpenGLUtils.h"

const char GPUImageAmaroFilter::AMARO_FRAGMENT_SHADER[] = {
    "precision mediump float;\n"
    "varying mediump vec2 textureCoordinate;\n"
    "uniform sampler2D inputImageTexture;\n"
    "uniform sampler2D inputImageTexture2;\n"
    "uniform sampler2D inputImageTexture3;\n"
    "uniform sampler2D inputImageTexture4;\n"
    "uniform float strength;\n"
    "void main()\n"
    "{\n"
    "vec4 originColor = texture2D(inputImageTexture, textureCoordinate);\n"
    "vec4 texel = texture2D(inputImageTexture, textureCoordinate);\n"
    "vec3 bbTexel = texture2D(inputImageTexture2, textureCoordinate).rgb;\n"
    "texel.r = texture2D(inputImageTexture3, vec2(bbTexel.r, texel.r)).r;\n"
    "texel.g = texture2D(inputImageTexture3, vec2(bbTexel.g, texel.g)).g;\n"
    "texel.b = texture2D(inputImageTexture3, vec2(bbTexel.b, texel.b)).b;\n"
    "vec4 mapped;\n"
    "mapped.r = texture2D(inputImageTexture4, vec2(texel.r, .16666)).r;\n"
    "mapped.g = texture2D(inputImageTexture4, vec2(texel.g, .5)).g;\n"
    "mapped.b = texture2D(inputImageTexture4, vec2(texel.b, .83333)).b;\n"
    "mapped.a = 1.0;\n"
    "mapped.rgb = mix(originColor.rgb, mapped.rgb, strength);\n"
    "gl_FragColor = mapped;\n"
    "}\n"
};

GPUImageAmaroFilter::GPUImageAmaroFilter(char* filter_dir)
    : GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, AMARO_FRAGMENT_SHADER)
{
    inputTextureHandle0 = -1;
    inputTextureHandle1 = -1;
    inputTextureHandle2 = -1;
    
    inputTextureUniformLocation0 = -1;
    inputTextureUniformLocation1 = -1;
    inputTextureUniformLocation2 = -1;
    
    mFilterDir = NULL;
    
    if (filter_dir) {
        mFilterDir = strdup(filter_dir);
    }
}

GPUImageAmaroFilter::~GPUImageAmaroFilter()
{
    if (mFilterDir) {
        free(mFilterDir);
        mFilterDir = NULL;
    }
}

void GPUImageAmaroFilter::onInit()
{
    GPUImageFilter::onInit();
    
    inputTextureUniformLocation0 = glGetUniformLocation(mGLProgId, "inputImageTexture2");
    inputTextureUniformLocation1 = glGetUniformLocation(mGLProgId, "inputImageTexture3");
    inputTextureUniformLocation2 = glGetUniformLocation(mGLProgId, "inputImageTexture4");
}

void GPUImageAmaroFilter::onInitialized()
{
    GPUImageFilter::onInitialized();
    
    char* brannan_blowout_str = StringUtils::cat(mFilterDir, "/brannan_blowout.png");
    VideoFrame *brannan_blowout_frame = PNGImageFileToRGBAVideoFrame(brannan_blowout_str);
    free(brannan_blowout_str);
    
    if (brannan_blowout_frame) {
        inputTextureHandle0 = OpenGLUtils::loadTexture(brannan_blowout_frame->data, brannan_blowout_frame->width, brannan_blowout_frame->height, inputTextureHandle0);
        if (brannan_blowout_frame->data) {
            free(brannan_blowout_frame->data);
            brannan_blowout_frame->data = NULL;
        }
        delete brannan_blowout_frame;
        brannan_blowout_frame = NULL;
    }
    
    char* overlaymap_str = StringUtils::cat(mFilterDir, "/overlaymap.png");
    VideoFrame *overlaymap_frame = PNGImageFileToRGBAVideoFrame(overlaymap_str);
    free(overlaymap_str);
    
    if (overlaymap_frame) {
        inputTextureHandle1 = OpenGLUtils::loadTexture(overlaymap_frame->data, overlaymap_frame->width, overlaymap_frame->height, inputTextureHandle1);
        if (overlaymap_frame->data) {
            free(overlaymap_frame->data);
            overlaymap_frame->data = NULL;
        }
        delete overlaymap_frame;
        overlaymap_frame = NULL;
    }

    char* amaromap_str = StringUtils::cat(mFilterDir, "/amaromap.png");
    VideoFrame *amaromap_frame = PNGImageFileToRGBAVideoFrame(amaromap_str);
    free(amaromap_str);

    if (amaromap_frame) {
        inputTextureHandle2 = OpenGLUtils::loadTexture(amaromap_frame->data, amaromap_frame->width, amaromap_frame->height, inputTextureHandle2);
        if (amaromap_frame->data) {
            free(amaromap_frame->data);
            amaromap_frame->data = NULL;
        }
        delete amaromap_frame;
        amaromap_frame = NULL;
    }
}

void GPUImageAmaroFilter::onDestroy()
{
    GPUImageFilter::onDestroy();
    
    glDeleteTextures(1, &inputTextureHandle0);
    inputTextureHandle0 = OpenGLUtils::NO_TEXTURE;
    
    glDeleteTextures(1, &inputTextureHandle1);
    inputTextureHandle1 = OpenGLUtils::NO_TEXTURE;
    
    glDeleteTextures(1, &inputTextureHandle2);
    inputTextureHandle2 = OpenGLUtils::NO_TEXTURE;
}

void GPUImageAmaroFilter::onDrawArraysPre()
{
    if (inputTextureHandle0!=OpenGLUtils::NO_TEXTURE) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, inputTextureHandle0);
        glUniform1i(inputTextureUniformLocation0, 1);
    }
    
    if (inputTextureHandle1!=OpenGLUtils::NO_TEXTURE) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, inputTextureHandle1);
        glUniform1i(inputTextureUniformLocation1, 2);
    }
    
    if (inputTextureHandle2!=OpenGLUtils::NO_TEXTURE) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, inputTextureHandle2);
        glUniform1i(inputTextureUniformLocation2, 3);
    }
}

void GPUImageAmaroFilter::onDrawArraysAfter()
{
    if (inputTextureHandle0!=OpenGLUtils::NO_TEXTURE) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    
    if (inputTextureHandle1!=OpenGLUtils::NO_TEXTURE) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    
    if (inputTextureHandle2!=OpenGLUtils::NO_TEXTURE) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
