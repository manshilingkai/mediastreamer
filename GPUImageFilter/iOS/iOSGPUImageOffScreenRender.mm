//
//  iOSGPUImageOffScreenRender.cpp
//  MediaStreamer
//
//  Created by Think on 2017/3/1.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "iOSGPUImageOffScreenRender.h"
#include "MediaLog.h"
#include "Matrix.h"
#include "MediaMath.h"

iOSGPUImageOffScreenRender::iOSGPUImageOffScreenRender(OFF_SCREEN_RENDER_INPUT_TYPE inputType)
{
    mGLCubeBuffer = new float[8];
    for (int i = 0; i<8; i++) {
        mGLCubeBuffer[i] = TextureRotationUtil::CUBE[i];
    }
    
    initialized_ = false;

    _context = NULL;
    
    offScreenDisPlayWidth = -1;
    offScreenDisplayHeight = -1;
    
    i420InputFilter = NULL;
    nv12InputFilter = NULL;
    workFilter = NULL;
    normalBlendFilter = NULL;
    outputFilter = NULL;
    
    mOffScreenRenderInputType = inputType;
    if (mOffScreenRenderInputType==NV12) {
        nv12InputFilter = new GPUImageNV12InputFilter();
    }else {
        i420InputFilter = new GPUImageI420InputFilter();
    }
    
    workFilter = new GPUImageRGBFilter();
    mCurrentFilterType = RGB_FILTER;
    normalBlendFilter = new GPUImageNormalBlendFilter();
    outputFilter = new GPUImageRawPixelOutputFilter();
    
    outputWidth = -1;
    outputHeight = -1;
    isOutputSizeUpdated = false;
    
    rotationModeForWorkFilter = kGPUImageNoRotation;
    
    textureCoordinates = new float[8];
    
    //overlay
    overlayTransformFilter = new GPUImageTransformFilter();
    overlayTransform = new float[16];
    overlayTextureCoordinates = new float[8];
    
    overlayFrameBuffer = -1;
    overlayFrameBufferTexture = OpenGLUtils::NO_TEXTURE;
    overlayFrameBufferWidth  = -1;
    overlayFrameBufferHeight = -1;
    isCreatedOverlayFBO = false;
    
    //text
    textBackGroundLayFrameBuffer[0] = -1;
    textBackGroundLayFrameBuffer[1] = -1;
    textBackGroundLayFrameBufferTexture[0] = -1;
    textBackGroundLayFrameBufferTexture[1] = -1;
    textBackGroundLayFBOIndex = -1;
    isCreatedTextBackGroundLayFrameBufferObject = false;
    textBackGroundLayFrameBufferWidth = -1;
    textBackGroundLayFrameBufferHeight = -1;
    
    inputTextBackGroundLayTextureCoordinates = new float[8];
    inputTextBackGroundLayFilter = new GPUImageRGBFilter;
    
    normalBlendWordFilter = new GPUImageNormalBlendFilter;
    
    textBackGroundLayTransform = new float[16];
    transformTextBackGroundLayFilter = new GPUImageTransformFilter;
    
    transformedTextBackGroundLayFrameBuffer = -1;
    transformedTextBackGroundLayFrameBufferTexture = -1;
    isCreatedTransformedTextBackGroundLayFBO = false;
    transformedTextBackGroundLayFrameBufferWidth = -1;
    transformedTextBackGroundLayFrameBufferHeight = -1;
    transformedTextBackGroundLayTextureCoordinates = new float[8];
    
    //filter group
    for (int i = 0; i<2; i++) {
        mFGFrameBuffers[i] = -1;
        mFGFrameBufferTextures[i] = -1;
    }
    mFGFrameBufferWidth = -1;
    mFGFrameBufferHeight = -1;
    isFGFrameBuffersCreated = false;
    mFGFrameBufferGLVertexPositionCoordinates = new float[8];
    for (int i = 0; i<8; i++) {
        mFGFrameBufferGLVertexPositionCoordinates[i] = TextureRotationUtil::CUBE[i];
    }
    mFGFrameBufferGLTextureCoordinates = new float[8];
    TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, mFGFrameBufferGLTextureCoordinates);
}

iOSGPUImageOffScreenRender::~iOSGPUImageOffScreenRender()
{
    terminate();
    
    if (mOffScreenRenderInputType==NV12) {
        if (nv12InputFilter) {
            delete nv12InputFilter;
            nv12InputFilter = NULL;
        }
    }else{
        if (i420InputFilter) {
            delete i420InputFilter;
            i420InputFilter = NULL;
        }
    }

    
    if (workFilter) {
        delete workFilter;
        workFilter = NULL;
    }
    
    if (normalBlendFilter) {
        delete normalBlendFilter;
        normalBlendFilter = NULL;
    }
    
    if (outputFilter) {
        delete outputFilter;
        outputFilter = NULL;
    }
    
    delete [] textureCoordinates;
    
    //overlay
    if (overlayTransformFilter) {
        delete overlayTransformFilter;
        overlayTransformFilter = NULL;
    }
    delete [] overlayTransform;
    delete [] overlayTextureCoordinates;
    
    //text
    delete [] inputTextBackGroundLayTextureCoordinates;
    if (inputTextBackGroundLayFilter) {
        delete inputTextBackGroundLayFilter;
        inputTextBackGroundLayFilter = NULL;
    }
    
    if (normalBlendWordFilter) {
        delete normalBlendWordFilter;
        normalBlendWordFilter = NULL;
    }
    
    delete [] textBackGroundLayTransform;
    if (transformTextBackGroundLayFilter) {
        delete transformTextBackGroundLayFilter;
        transformTextBackGroundLayFilter = NULL;
    }
    delete [] transformedTextBackGroundLayTextureCoordinates;
    
    delete [] mGLCubeBuffer;
    
    //filter group
    delete [] mFGFrameBufferGLTextureCoordinates;
    delete [] mFGFrameBufferGLVertexPositionCoordinates;
}

void iOSGPUImageOffScreenRender::createFrameBufferObject(int frameWidth, int frameHeight, int fbo_index)
{
    glGenFramebuffers(1, &frameBuffer[fbo_index]);
    
    glGenTextures(1, &frameBufferTexture[fbo_index]);
    glBindTexture(GL_TEXTURE_2D, frameBufferTexture[fbo_index]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, frameWidth, frameHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer[fbo_index]);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, frameBufferTexture[fbo_index], 0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void iOSGPUImageOffScreenRender::deleteFrameBufferObject(int fbo_index)
{
    glDeleteTextures(1, &frameBufferTexture[fbo_index]);
    glDeleteFramebuffers(1, &frameBuffer[fbo_index]);
}

void iOSGPUImageOffScreenRender::bind(int frameWidth, int frameHeight, int fbo_index)
{
    glViewport(0, 0, frameWidth, frameHeight);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer[fbo_index]);
}

void iOSGPUImageOffScreenRender::unBind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

bool iOSGPUImageOffScreenRender::initialize(int displayWidth, int displayHeight)
{
    if (initialized_) {
        LOGW("Already initialized");
        return true;
    }
    
    EAGLContext* current = [EAGLContext currentContext];
    
    _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!_context) {
        return false;
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        return false;
    }
    
    offScreenDisPlayWidth = displayWidth;
    offScreenDisplayHeight = displayHeight;
    
    for (int i = 0; i < 2; i++) {
        createFrameBufferObject(offScreenDisPlayWidth, offScreenDisplayHeight, i);
    }
    fboIndex = -1;
    
    if (mOffScreenRenderInputType==NV12) {
        nv12InputFilter->init();
    }else {
        i420InputFilter->init();
    }
    
    workFilter->init();
    isOutputSizeUpdated = true;
    normalBlendFilter->init();
    outputFilter->init();
    outputFilter->createFBO(offScreenDisPlayWidth, offScreenDisplayHeight);
    
    //overlay
    overlayTransformFilter->init();
    
    //text
    inputTextBackGroundLayFilter->init();
    normalBlendWordFilter->init();
    transformTextBackGroundLayFilter->init();
    
    [EAGLContext setCurrentContext:current];
    
    initialized_ = true;
    
    return true;
}

void iOSGPUImageOffScreenRender::terminate()
{
    if(!initialized_) {
        LOGW("Haven't initialized");
        return;
    }
    
    EAGLContext* current = [EAGLContext currentContext];
    [EAGLContext setCurrentContext:_context];
    
    for (int i = 0; i < 2; i++) {
        deleteFrameBufferObject(i);
    }
    
    fboIndex = -1;
    
    if (mOffScreenRenderInputType==NV12) {
        nv12InputFilter->destroy();
    }else {
        i420InputFilter->destroy();
    }
    
    // filter group
    destroyFGFramebuffers();
    for(std::map<FILTER_TYPE, GPUImageFilter*>::iterator it = mFilters.begin(); it != mFilters.end(); ++it) {
        GPUImageFilter* filter = it->second;
        if (filter) {
            filter->destroy();
            delete filter;
        }
    }
    mFilters.clear();
    
    workFilter->destroy();
    isOutputSizeUpdated = false;
    normalBlendFilter->destroy();
    outputFilter->deleteFBO();
    outputFilter->destroy();
    
    //overlay
    deleteOverlayFBO();
    overlayTransformFilter->destroy();
    overlayFrameBufferWidth  = -1;
    overlayFrameBufferHeight = -1;
    isCreatedOverlayFBO = false;
    
    //text
    for (int i = 0; i<2; i++) {
        deleteTextBackGroundLayFrameBufferObject(i);
    }
    textBackGroundLayFBOIndex = -1;
    isCreatedTextBackGroundLayFrameBufferObject = false;
    textBackGroundLayFrameBufferWidth = -1;
    textBackGroundLayFrameBufferHeight = -1;
    
    inputTextBackGroundLayFilter->destroy();
    
    normalBlendWordFilter->destroy();
    
    transformTextBackGroundLayFilter->destroy();
    
    deleteTransformedTextBackGroundLayFBO();
    isCreatedTransformedTextBackGroundLayFBO = false;
    transformedTextBackGroundLayFrameBufferWidth = -1;
    transformedTextBackGroundLayFrameBufferHeight = -1;
    
    [EAGLContext setCurrentContext:current];
    
    if (_context!=nil) {
        [_context release];
        _context = nil;
    }
    
    initialized_ = false;
}

bool iOSGPUImageOffScreenRender::isInitialized()
{
    return initialized_;
}

VideoFrame* iOSGPUImageOffScreenRender::render(AVFrame *inputVideoFrame)
{
    int rotate = 0;
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(inputVideoFrame->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            rotate = atoi(m->value);
        }
    }
    
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }

    int inputFrameBufferTexture = -1;
    if (mOffScreenRenderInputType==NV12) {
        NV12GPUImage nv12GPUImage;
        nv12GPUImage.width = inputVideoFrame->width;
        nv12GPUImage.height = inputVideoFrame->height;
        nv12GPUImage.rotation = rotate;
        nv12GPUImage.opaque = inputVideoFrame->opaque;
        
        inputFrameBufferTexture = nv12InputFilter->onDrawToTexture(_context, nv12GPUImage);

    }else{
        I420GPUImage i420GPUImage;
        i420GPUImage.width = inputVideoFrame->width;
        i420GPUImage.height = inputVideoFrame->height;
        i420GPUImage.y_stride = inputVideoFrame->linesize[0];
        i420GPUImage.u_stride = inputVideoFrame->linesize[1];
        i420GPUImage.v_stride = inputVideoFrame->linesize[2];
        i420GPUImage.y_plane = inputVideoFrame->data[0];
        i420GPUImage.u_plane = inputVideoFrame->data[1];
        i420GPUImage.v_plane = inputVideoFrame->data[2];
        i420GPUImage.rotation = rotate;
        
        inputFrameBufferTexture = i420InputFilter->onDrawToTexture(i420GPUImage);
    }
    
    int videoWidth = -1;
    int videoHeight = -1;
    
    if (mOffScreenRenderInputType==NV12) {
        videoWidth = nv12InputFilter->getOutputFrameBufferWidth();
        videoHeight = nv12InputFilter->getOutputFrameBufferHeight();
    }else{
        videoWidth = i420InputFilter->getOutputFrameBufferWidth();
        videoHeight = i420InputFilter->getOutputFrameBufferHeight();
    }
    
    ScaleAspectFill(rotationModeForWorkFilter, offScreenDisPlayWidth, offScreenDisplayHeight, videoWidth, videoHeight);
    
    outputFilter->bind();
    workFilter->onDrawFrame(inputFrameBufferTexture, TextureRotationUtil::CUBE, textureCoordinates);
    VideoFrame* fboVideoFrame = outputFilter->outputPixelBuffer();
    outputFilter->unBind();

    [EAGLContext setCurrentContext:current];
    
    return fboVideoFrame;
}

void iOSGPUImageOffScreenRender::render(AVFrame *inputVideoFrame, VideoFrame* outputVideoFrame)
{
    int rotate = 0;
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(inputVideoFrame->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            rotate = atoi(m->value);
        }
    }
    
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    int inputFrameBufferTexture = -1;
    if (mOffScreenRenderInputType==NV12) {
        NV12GPUImage nv12GPUImage;
        nv12GPUImage.width = inputVideoFrame->width;
        nv12GPUImage.height = inputVideoFrame->height;
        nv12GPUImage.rotation = rotate;
        nv12GPUImage.opaque = inputVideoFrame->opaque;
        
        inputFrameBufferTexture = nv12InputFilter->onDrawToTexture(_context, nv12GPUImage);
        
    }else{
        I420GPUImage i420GPUImage;
        i420GPUImage.width = inputVideoFrame->width;
        i420GPUImage.height = inputVideoFrame->height;
        i420GPUImage.y_stride = inputVideoFrame->linesize[0];
        i420GPUImage.u_stride = inputVideoFrame->linesize[1];
        i420GPUImage.v_stride = inputVideoFrame->linesize[2];
        i420GPUImage.y_plane = inputVideoFrame->data[0];
        i420GPUImage.u_plane = inputVideoFrame->data[1];
        i420GPUImage.v_plane = inputVideoFrame->data[2];
        i420GPUImage.rotation = rotate;
        
        inputFrameBufferTexture = i420InputFilter->onDrawToTexture(i420GPUImage);
    }
    
    int videoWidth = -1;
    int videoHeight = -1;
    
    if (mOffScreenRenderInputType==NV12) {
        videoWidth = nv12InputFilter->getOutputFrameBufferWidth();
        videoHeight = nv12InputFilter->getOutputFrameBufferHeight();
    }else{
        videoWidth = i420InputFilter->getOutputFrameBufferWidth();
        videoHeight = i420InputFilter->getOutputFrameBufferHeight();
    }
    
    ScaleAspectFill(rotationModeForWorkFilter, offScreenDisPlayWidth, offScreenDisplayHeight, videoWidth, videoHeight);
    
    outputFilter->bind();
    workFilter->onDrawFrame(inputFrameBufferTexture, TextureRotationUtil::CUBE, textureCoordinates);
    outputFilter->outputPixelBuffer(outputVideoFrame);
    outputFilter->unBind();
    
    [EAGLContext setCurrentContext:current];
}


void iOSGPUImageOffScreenRender::ScaleAspectFill(GPUImageRotationMode rotationMode, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    int dst_width = displayWidth;
    int dst_height = displayHeight;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    if(src_width*dst_height>dst_width*src_height)
    {
        crop_width = dst_width*src_height/dst_height;
        crop_height = src_height;
        
        crop_x = (src_width - crop_width)/2;
        crop_y = 0;
        
    }else if(src_width*dst_height<dst_width*src_height)
    {
        crop_width = src_width;
        crop_height = dst_height*src_width/dst_width;
        
        crop_x = 0;
        crop_y = (src_height - crop_height)/2;
    }else {
        crop_width = src_width;
        crop_height = src_height;
        crop_x = 0;
        crop_y = 0;
    }
    
    float minX = (float)crop_x/(float)src_width;
    float minY = (float)crop_y/(float)src_height;
    float maxX = 1.0f - minX;
    float maxY = 1.0f - minY;
    
    if (outputWidth!=crop_width || outputHeight!=crop_height) {
        outputWidth = crop_width;
        outputHeight = crop_height;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, textureCoordinates);
    
    for (int i = 0; i<8; i++) {
        mGLCubeBuffer[i] = TextureRotationUtil::CUBE[i];
    }
}

void iOSGPUImageOffScreenRender::ScaleAspectFit(GPUImageRotationMode rotationMode, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
{
    int src_width;
    int src_height;
    
    if (GPUImageRotationSwapsWidthAndHeight(rotationMode))
    {
        src_width = videoHeight;
        src_height = videoWidth;
    }else{
        src_width = videoWidth;
        src_height = videoHeight;
    }
    
    videoWidth = src_width;
    videoHeight = src_height;
    
    float startWidth = 0.0f;
    float stopWidth = 1.0f;
    float startHeight = 0.0f;
    float stopHeight = 1.0f;
    
    if(displayWidth*videoHeight>videoWidth*displayHeight)
    {
        float winWidth = (float)displayWidth;
        float winHeight = (float)displayHeight;
        
        float textureWidth = (float)videoWidth;
        float textureHeight = (float)videoHeight;
        
        startWidth = (winWidth-winHeight*textureWidth/textureHeight)/2.0f/winWidth;
        stopWidth = 1.0f-(winWidth-winHeight*textureWidth/textureHeight)/2.0f/winWidth;
    }else if(displayWidth*videoHeight<videoWidth*displayHeight)
    {
        float winWidth = (float)displayWidth;
        float winHeight = (float)displayHeight;
        
        float textureWidth = (float)videoWidth;
        float textureHeight = (float)videoHeight;
        
        startHeight = (winHeight-winWidth*textureHeight/textureWidth)/2.0f/winHeight;
        stopHeight = 1.0f-(winHeight-winWidth*textureHeight/textureWidth)/2.0f/winHeight;
    }
    
    // convert from 0.0 <= size <= 1.0 to
    // open gl world -1.0 < size < 1.0
    GLfloat xStart = 2.0f * startWidth - 1.0f;
    GLfloat xStop = 2.0f * stopWidth - 1.0f;
    GLfloat yStart = 1.0f - 2.0f * stopHeight;
    GLfloat yStop = 1.0f - 2.0f * startHeight;
    
    //-1.0f, -1.0f,
    //1.0f, -1.0f,
    //-1.0f, 1.0f,
    //1.0f, 1.0f,
    mGLCubeBuffer[0] = xStart;
    mGLCubeBuffer[1] = yStart;
    mGLCubeBuffer[2] = xStop;
    mGLCubeBuffer[3] = yStart;
    mGLCubeBuffer[4] = xStart;
    mGLCubeBuffer[5] = yStop;
    mGLCubeBuffer[6] = xStop;
    mGLCubeBuffer[7] = yStop;

    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
        outputWidth = videoWidth;
        outputHeight = videoHeight;
        
        isOutputSizeUpdated = true;
    }
    
    if (isOutputSizeUpdated) {
        isOutputSizeUpdated = false;
        workFilter->onOutputSizeChanged(outputWidth, outputHeight);
    }
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, textureCoordinates);
}

int iOSGPUImageOffScreenRender::filterInputToFrameBufferTexture(AVFrame *inputVideoFrame, FILTER_TYPE filterType, char* filterDir, bool isAspectFit)
{
    int rotate = 0;
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(inputVideoFrame->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
//        printf("%s : %s\n", m->key,m->value);
        
        if(strcmp(m->key, "rotate")) continue;
        else{
            rotate = atoi(m->value);
        }
    }
    
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    int inputFrameBufferTexture = -1;
    if (mOffScreenRenderInputType==NV12) {
        NV12GPUImage nv12GPUImage;
        nv12GPUImage.width = inputVideoFrame->width;
        nv12GPUImage.height = inputVideoFrame->height;
        nv12GPUImage.rotation = rotate;
        nv12GPUImage.opaque = inputVideoFrame->opaque;
        
        inputFrameBufferTexture = nv12InputFilter->onDrawToTexture(_context, nv12GPUImage);
    }else{
        I420GPUImage i420GPUImage;
        i420GPUImage.width = inputVideoFrame->width;
        i420GPUImage.height = inputVideoFrame->height;
        i420GPUImage.y_stride = inputVideoFrame->linesize[0];
        i420GPUImage.u_stride = inputVideoFrame->linesize[1];
        i420GPUImage.v_stride = inputVideoFrame->linesize[2];
        i420GPUImage.y_plane = inputVideoFrame->data[0];
        i420GPUImage.u_plane = inputVideoFrame->data[1];
        i420GPUImage.v_plane = inputVideoFrame->data[2];
        i420GPUImage.rotation = rotate;
        
        inputFrameBufferTexture = i420InputFilter->onDrawToTexture(i420GPUImage);
    }
    
    int videoWidth = -1;
    int videoHeight = -1;
    
    if (mOffScreenRenderInputType==NV12) {
        videoWidth = nv12InputFilter->getOutputFrameBufferWidth();
        videoHeight = nv12InputFilter->getOutputFrameBufferHeight();
    }else{
        videoWidth = i420InputFilter->getOutputFrameBufferWidth();
        videoHeight = i420InputFilter->getOutputFrameBufferHeight();
    }
    
    inputFrameBufferTexture = drawFilter(inputFrameBufferTexture, videoWidth, videoHeight);
    
    if (mCurrentFilterType!=filterType) {
        
        if (workFilter!=NULL) {
            delete workFilter;
            workFilter = NULL;
        }
        
        mCurrentFilterType = filterType;
        
        switch (mCurrentFilterType) {
            case SKETCH_FILTER:
                workFilter = new GPUImageSketchFilter;
                workFilter->init();
                break;
            case AMARO_FILTER:
                workFilter = new GPUImageAmaroFilter(filterDir);
                workFilter->init();
                break;
            case ANTIQUE_FILTER:
                workFilter = new GPUImageAntiqueFilter();
                workFilter->init();
                break;
            case BLACKCAT_FILTER:
                workFilter = new GPUImageBlackCatFilter();
                workFilter->init();
                break;
            case BEAUTY_FILTER:
                workFilter = new GPUImageBeautyFilter();
                workFilter->init();
                break;
            case BRANNAN_FILTER:
                workFilter = new GPUImageBrannanFilter(filterDir);
                workFilter->init();
                break;
            case N1977_FILTER:
                workFilter = new GPUImageN1977Filter(filterDir);
                workFilter->init();
                break;
            case BROOKLYN_FILTER:
                workFilter = new GPUImageBrooklynFilter(filterDir);
                workFilter->init();
                break;
            case COOL_FILTER:
                workFilter = new GPUImageCoolFilter(filterDir);
                workFilter->init();
                break;
            case CRAYON_FILTER:
                workFilter = new GPUImageCrayonFilter(filterDir);
                workFilter->init();
                break;
            default:
                mCurrentFilterType = RGB_FILTER;
                workFilter = new GPUImageRGBFilter;
                workFilter->init();
                break;
        }
        
        isOutputSizeUpdated = true;
    }
    
    if (isAspectFit) {
        ScaleAspectFit(rotationModeForWorkFilter, offScreenDisPlayWidth, offScreenDisplayHeight, videoWidth, videoHeight);
    }else{
        ScaleAspectFill(rotationModeForWorkFilter, offScreenDisPlayWidth, offScreenDisplayHeight, videoWidth, videoHeight);
    }
    
    fboIndex = (fboIndex+1)%2;
    
    bind(offScreenDisPlayWidth, offScreenDisplayHeight, fboIndex);
    workFilter->onDrawFrame(inputFrameBufferTexture, mGLCubeBuffer, textureCoordinates);
    unBind();
    [EAGLContext setCurrentContext:current];
    
    return frameBufferTexture[fboIndex];
}

bool iOSGPUImageOffScreenRender::updateFGFramebuffers(int width, int height)
{
    if (mFGFrameBufferWidth!=width || mFGFrameBufferHeight!=height) {
        destroyFGFramebuffers();
        
        for (int i = 0; i < 2; i++) {
            GLuint framebuffer;
            glGenFramebuffers(1, &framebuffer);
            
            GLuint framebuffertexture;
            glGenTextures(1, &framebuffertexture);
            
            glBindTexture(GL_TEXTURE_2D, framebuffertexture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            
            glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, framebuffertexture, 0);
            
            glBindTexture(GL_TEXTURE_2D, 0);
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            
            mFGFrameBuffers[i] = framebuffer;
            mFGFrameBufferTextures[i] = framebuffertexture;
        }
        
        mFGFrameBufferWidth = width;
        mFGFrameBufferHeight = height;
        
        isFGFrameBuffersCreated = true;
        return true;
    }else return false;
}

void iOSGPUImageOffScreenRender::destroyFGFramebuffers()
{
    if (isFGFrameBuffersCreated) {
        glDeleteFramebuffers(2, mFGFrameBuffers);
        glDeleteTextures(2, mFGFrameBufferTextures);
        
        isFGFrameBuffersCreated = false;
    }
}

void iOSGPUImageOffScreenRender::setFilter(FILTER_TYPE filter_type, char* filter_dir, float strength)
{
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    if (mFilters.find(filter_type)==mFilters.end()) {
        GPUImageFilter *filter = NULL;
        if (filter_type==SKETCH_FILTER) {
            filter = new GPUImageSketchFilter;
        }else if (filter_type==AMARO_FILTER) {
            filter = new GPUImageAmaroFilter(filter_dir);
        }else if (filter_type==ANTIQUE_FILTER) {
            filter = new GPUImageAntiqueFilter();
        }else if (filter_type==BLACKCAT_FILTER) {
            filter = new GPUImageBlackCatFilter();
        }else if (filter_type==BEAUTY_FILTER) {
            filter = new GPUImageBeautyFilter();
        }else if (filter_type==BRANNAN_FILTER) {
            filter = new GPUImageBrannanFilter(filter_dir);
        }else if (filter_type==N1977_FILTER) {
            filter = new GPUImageN1977Filter(filter_dir);
        }else if (filter_type==BROOKLYN_FILTER) {
            filter = new GPUImageBrooklynFilter(filter_dir);
        }else if (filter_type==COOL_FILTER) {
            filter = new GPUImageCoolFilter(filter_dir);
        }else if (filter_type==CRAYON_FILTER) {
            filter = new GPUImageCrayonFilter(filter_dir);
        }else if (filter_type==BRIGHTNESS_FILTER) {
            filter = new GPUImageBrightnessFilter();
        }else if (filter_type==CONTRAST_FILTER) {
            filter = new GPUImageContrastFilter();
        }else if (filter_type==EXPOSURE_FILTER) {
            filter = new GPUImageExposureFilter();
        }else if (filter_type==HUE_FILTER) {
            filter = new GPUImageHueFilter();
        }else if (filter_type==SATURATION_FILTER) {
            filter = new GPUImageSaturationFilter();
        }else if (filter_type==SHARPEN_FILTER) {
            filter = new GPUImageSharpenFilter();
        }
        
        if (filter) {
            filter->init();
            filter->onOutputSizeChanged(mFGFrameBufferWidth, mFGFrameBufferHeight);
            mFilters[filter_type] = filter;
        }
    }
    
    if (mFilters.find(filter_type)!=mFilters.end()) {
        GPUImageFilter *filter = mFilters[filter_type];
        if (filter) {
            if (filter_type==BEAUTY_FILTER) {
                GPUImageBeautyFilter* beautyFilter = (GPUImageBeautyFilter*)filter;
                if (beautyFilter) {
                    if (strength<0.0f) {
                        strength = 0.0f;
                    }
                    if (strength>1.0f) {
                        strength = 1.0f;
                    }
                    beautyFilter->setBeautyLevel(strength);
                }
            }else if (filter_type==BRIGHTNESS_FILTER) {
                GPUImageBrightnessFilter* brightnessFilter = (GPUImageBrightnessFilter*)filter;
                if (brightnessFilter) {
                    if (strength<-1.0f) {
                        strength = -1.0f;
                    }
                    if (strength>1.0f) {
                        strength = 1.0f;
                    }
                    brightnessFilter->setBrightness(strength);
                }
            }else if (filter_type==CONTRAST_FILTER) {
                GPUImageContrastFilter* contrastFilter = (GPUImageContrastFilter*)filter;
                if (contrastFilter) {
                    if (strength<0.0f) {
                        strength = 0.0f;
                    }
                    if (strength>4.0f) {
                        strength = 4.0f;
                    }
                    contrastFilter->setContrast(strength);
                }
            }else if (filter_type==EXPOSURE_FILTER) {
                GPUImageExposureFilter* exposureFilter = (GPUImageExposureFilter*)filter;
                if (exposureFilter) {
                    if (strength<-10.0f) {
                        strength = -10.0f;
                    }
                    if (strength>10.0f) {
                        strength = 10.0f;
                    }
                    exposureFilter->setExposure(strength);
                }
            }else if (filter_type==HUE_FILTER) {
                GPUImageHueFilter* hueFilter = (GPUImageHueFilter*)filter;
                if (hueFilter) {
                    if (strength<0.0f) {
                        strength = 0.0f;
                    }
                    if (strength>360.0f) {
                        strength = 360.0f;
                    }
                    hueFilter->setHue(strength);
                }
            }else if (filter_type==SATURATION_FILTER) {
                GPUImageSaturationFilter* saturationFilter = (GPUImageSaturationFilter*)filter;
                if (saturationFilter) {
                    if (strength<0.0f) {
                        strength = 0.0f;
                    }
                    if (strength>2.0f) {
                        strength = 2.0f;
                    }
                    saturationFilter->setSaturation(strength);
                }
            }else if (filter_type==SHARPEN_FILTER) {
                GPUImageSharpenFilter* sharpenFilter = (GPUImageSharpenFilter*)filter;
                if (sharpenFilter) {
                    if (strength<-4.0f) {
                        strength = -4.0f;
                    }
                    if (strength>4.0f) {
                        strength = 4.0f;
                    }
                    sharpenFilter->setSharpness(strength);
                }
            }
        }
    }
    
    [EAGLContext setCurrentContext:current];
}

void iOSGPUImageOffScreenRender::removeFilter(FILTER_TYPE filter_type)
{
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    if (mFilters.find(filter_type)!=mFilters.end()) {
        GPUImageFilter *filter = mFilters[filter_type];
        if (filter) {
            filter->destroy();
            delete filter;
        }
        mFilters.erase(filter_type);
    }
    
    [EAGLContext setCurrentContext:current];
}

int iOSGPUImageOffScreenRender::drawFilter(int inputFrameBufferTexture, int inputFrameBufferWidth, int inputFrameBufferHeight)
{
    if (updateFGFramebuffers(inputFrameBufferWidth, inputFrameBufferHeight)) {
        for(std::map<FILTER_TYPE, GPUImageFilter*>::iterator it = mFilters.begin(); it != mFilters.end(); ++it) {
            GPUImageFilter* filter = it->second;
            if (filter) {
                filter->onOutputSizeChanged(inputFrameBufferWidth, inputFrameBufferHeight);
            }
        }
    }
    
    int workFrameBufferIndex = 0;
    GLuint workFrameBufferTexture = inputFrameBufferTexture;
    for(std::map<FILTER_TYPE, GPUImageFilter*>::iterator it = mFilters.begin(); it != mFilters.end(); ++it) {
        GPUImageFilter* filter = it->second;
        glViewport(0, 0, inputFrameBufferWidth, inputFrameBufferHeight);
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBindFramebuffer(GL_FRAMEBUFFER, mFGFrameBuffers[workFrameBufferIndex]);
        filter->onDrawFrame(workFrameBufferTexture, mFGFrameBufferGLVertexPositionCoordinates, mFGFrameBufferGLTextureCoordinates);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        workFrameBufferTexture = mFGFrameBufferTextures[workFrameBufferIndex];
        
        if (workFrameBufferIndex==0) {
            workFrameBufferIndex = 1;
        }else if(workFrameBufferIndex==1) {
            workFrameBufferIndex = 0;
        }
    }
    
    return workFrameBufferTexture;
}

//overlay
int iOSGPUImageOffScreenRender::renderOverlayToFBO(VideoFrame* overlay, int rotation, float scale, bool flipHorizontal, bool flipVertical)
{
    float scaled_overlay_Width = scale * (float)overlay->width;
    float scaled_overlay_Height = scale * (float)overlay->height;
    
    int overlayOffDisplayWidth = MediaMath::Sqrt((double)scaled_overlay_Width * (double)scaled_overlay_Width + (double)scaled_overlay_Height * (double)scaled_overlay_Height) + 0.5f;
    int overlayOffDisplayHeight = MediaMath::Sqrt((double)scaled_overlay_Width * (double)scaled_overlay_Width + (double)scaled_overlay_Height * (double)scaled_overlay_Height) + 0.5f;
    
//    double alpha = MediaMath::PI*(double)rotation/(double)180;
//    overlayOffDisplayWidth = scaled_overlay_Width*MediaMath::Cos(alpha) + scaled_overlay_Height*MediaMath::Sin(alpha) + 0.5f;
//    overlayOffDisplayHeight = scaled_overlay_Width*MediaMath::Sin(alpha) + scaled_overlay_Height*MediaMath::Cos(alpha) + 0.5f;
    
    overlayTransformFilter->onOutputSizeChanged(overlayOffDisplayWidth, overlayOffDisplayHeight);
    
    GLuint overlayTexture = OpenGLUtils::createTexture(overlay->data, overlay->width, overlay->height);
    
    Matrix::setIdentityM(overlayTransform, 0);
    Matrix::setRotateM(overlayTransform, 0, (float)rotation, 0.0f, 0.0f, 1.0f);
    Matrix::scaleM(overlayTransform, 0, scale, scale, 1.0f);
    overlayTransformFilter->setTransform3D(overlayTransform);
    
    if (flipHorizontal && !flipVertical) {
        TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageFlipHorizonal, 0.0f, 0.0f, 1.0f, 1.0f, overlayTextureCoordinates);
    }else if(!flipHorizontal && flipVertical) {
        TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageFlipVertical, 0.0f, 0.0f, 1.0f, 1.0f, overlayTextureCoordinates);
    }else if(flipHorizontal && flipVertical) {
        TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageRotate180, 0.0f, 0.0f, 1.0f, 1.0f, overlayTextureCoordinates);
    }else {
        TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, overlayTextureCoordinates);
    }
    
    if (!isCreatedOverlayFBO) {
        createOverlayFBO(overlayOffDisplayWidth, overlayOffDisplayHeight);
        overlayFrameBufferWidth = overlayOffDisplayWidth;
        overlayFrameBufferHeight = overlayOffDisplayHeight;
        isCreatedOverlayFBO = true;
    }
    
    if (isCreatedOverlayFBO) {
        if (overlayFrameBufferWidth!=overlayOffDisplayWidth || overlayFrameBufferHeight != overlayOffDisplayHeight) {
            deleteOverlayFBO();
            createOverlayFBO(overlayOffDisplayWidth, overlayOffDisplayHeight);
            overlayFrameBufferWidth = overlayOffDisplayWidth;
            overlayFrameBufferHeight = overlayOffDisplayHeight;
        }
    }
    
    glViewport(0, 0, overlayFrameBufferWidth, overlayFrameBufferHeight);
    glBindFramebuffer(GL_FRAMEBUFFER, overlayFrameBuffer);
    
    float overlayPosition[8];
    overlayPosition[0] = -1.0f*(float)overlay->width/(float)overlayFrameBufferWidth;
    overlayPosition[1] = -1.0f*(float)overlay->height/(float)overlayFrameBufferHeight;
    overlayPosition[2] = (float)overlay->width/(float)overlayFrameBufferWidth;
    overlayPosition[3] = -1.0f*(float)overlay->height/(float)overlayFrameBufferHeight;
    overlayPosition[4] = -1.0f*(float)overlay->width/(float)overlayFrameBufferWidth;
    overlayPosition[5] = (float)overlay->height/(float)overlayFrameBufferHeight;
    overlayPosition[6] = (float)overlay->width/(float)overlayFrameBufferWidth;
    overlayPosition[7] = (float)overlay->height/(float)overlayFrameBufferHeight;
    
    overlayTransformFilter->onDrawFrame(overlayTexture, overlayPosition, overlayTextureCoordinates);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    OpenGLUtils::deleteTexture(overlayTexture);
    
    return overlayFrameBufferTexture;
}

int iOSGPUImageOffScreenRender::normalBlendToFrameBufferTexture(VideoFrame* overlay, int rotation, float scale, bool flipHorizontal, bool flipVertical, int x, int y)
{
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    normalBlendFilter->onOutputSizeChanged(offScreenDisPlayWidth, offScreenDisplayHeight);
    
    int overlayFBOTextureId = renderOverlayToFBO(overlay, rotation, scale, flipHorizontal, flipVertical);
    int inputFrameBufferTexture = frameBufferTexture[fboIndex];
    fboIndex = (fboIndex+1)%2;
    bind(offScreenDisPlayWidth, offScreenDisplayHeight, fboIndex);
    normalBlendFilter->onDrawFrame(inputFrameBufferTexture, overlayFBOTextureId, x-overlayFrameBufferWidth/2, y-overlayFrameBufferHeight/2, overlayFrameBufferWidth, overlayFrameBufferHeight);
    unBind();

    [EAGLContext setCurrentContext:current];

    return frameBufferTexture[fboIndex];
}

void iOSGPUImageOffScreenRender::pumpFrameBuffer(VideoFrame* outputVideoFrame)
{
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    int inputFrameBufferTexture = frameBufferTexture[fboIndex];
    
    outputFilter->bind();
    outputFilter->onDrawFrame(inputFrameBufferTexture);
    outputFilter->outputPixelBuffer(outputVideoFrame);
    outputFilter->unBind();
    
    [EAGLContext setCurrentContext:current];
}

VideoFrame* iOSGPUImageOffScreenRender::pumpFrameBuffer()
{
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    int inputFrameBufferTexture = frameBufferTexture[fboIndex];
    
    outputFilter->bind();
    outputFilter->onDrawFrame(inputFrameBufferTexture);
    VideoFrame *outputVideoFrame = outputFilter->outputPixelBuffer();
    outputFilter->unBind();
    
    [EAGLContext setCurrentContext:current];
    
    return outputVideoFrame;
}

//Create Overlay FBO
void iOSGPUImageOffScreenRender::createOverlayFBO(int frameWidth, int frameHeight)
{
    glGenFramebuffers(1, &overlayFrameBuffer);
    
    glGenTextures(1, &overlayFrameBufferTexture);
    glBindTexture(GL_TEXTURE_2D, overlayFrameBufferTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, frameWidth, frameHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, overlayFrameBuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, overlayFrameBufferTexture, 0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

//Delete Overlay FBO
void iOSGPUImageOffScreenRender::deleteOverlayFBO()
{
    glDeleteTextures(1, &overlayFrameBufferTexture);
    glDeleteFramebuffers(1, &overlayFrameBuffer);
}

//for text and background overlay
int iOSGPUImageOffScreenRender::flipTextBackGroundLayToFBO(VideoFrame* textBackGroundLay, bool flipHorizontal, bool flipVertical)
{
    GLuint inputTextBackGroundLayTexture = OpenGLUtils::createTexture(textBackGroundLay->data, textBackGroundLay->width, textBackGroundLay->height);
    
    if (flipHorizontal && !flipVertical) {
        TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageFlipHorizonal, 0.0f, 0.0f, 1.0f, 1.0f, inputTextBackGroundLayTextureCoordinates);
    }else if(!flipHorizontal && flipVertical) {
        TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageFlipVertical, 0.0f, 0.0f, 1.0f, 1.0f, inputTextBackGroundLayTextureCoordinates);
    }else if(flipHorizontal && flipVertical) {
        TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageRotate180, 0.0f, 0.0f, 1.0f, 1.0f, inputTextBackGroundLayTextureCoordinates);
    }else {
        TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, inputTextBackGroundLayTextureCoordinates);
    }
    
    textBackGroundLayFBOIndex = (textBackGroundLayFBOIndex+1)%2;
    
    glViewport(0, 0, textBackGroundLayFrameBufferWidth, textBackGroundLayFrameBufferHeight);
    glBindFramebuffer(GL_FRAMEBUFFER, textBackGroundLayFrameBuffer[textBackGroundLayFBOIndex]);
    
    inputTextBackGroundLayFilter->onDrawFrame(inputTextBackGroundLayTexture, TextureRotationUtil::CUBE, inputTextBackGroundLayTextureCoordinates);
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    OpenGLUtils::deleteTexture(inputTextBackGroundLayTexture);
    
    return textBackGroundLayFrameBufferTexture[textBackGroundLayFBOIndex];
}

int iOSGPUImageOffScreenRender::normalBlendWordToTextBackGroundLayFBO(WordInfo* wordInfo)
{
    normalBlendWordFilter->onOutputSizeChanged(textBackGroundLayFrameBufferWidth, textBackGroundLayFrameBufferHeight);
    
    VideoFrame* wordBitmap = wordInfo->wordBitmap;
    GLuint wordBitmapTexture = OpenGLUtils::createTexture(wordBitmap->data, wordBitmap->width, wordBitmap->height);
    
    int inputFrameBufferTexture = textBackGroundLayFrameBufferTexture[textBackGroundLayFBOIndex];
    textBackGroundLayFBOIndex = (textBackGroundLayFBOIndex+1)%2;
    glViewport(0, 0, textBackGroundLayFrameBufferWidth, textBackGroundLayFrameBufferHeight);
    glBindFramebuffer(GL_FRAMEBUFFER, textBackGroundLayFrameBuffer[textBackGroundLayFBOIndex]);
    normalBlendWordFilter->onDrawFrame(inputFrameBufferTexture, wordBitmapTexture, wordInfo->x-wordBitmap->width/2, wordInfo->y-wordBitmap->height/2, wordBitmap->width, wordBitmap->height);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    OpenGLUtils::deleteTexture(wordBitmapTexture);
    
    return textBackGroundLayFrameBufferTexture[textBackGroundLayFBOIndex];
}

int iOSGPUImageOffScreenRender::transformTextBackGroundLayFBO(int rotation, float scale)
{
    float scaled_TextBackGroundLay_Width = scale * (float)textBackGroundLayFrameBufferWidth;
    float scaled_TextBackGroundLay_Height = scale * (float)textBackGroundLayFrameBufferHeight;
    
    int transformedTextBackGroundLayWidth = MediaMath::Sqrt((double)scaled_TextBackGroundLay_Width * (double)scaled_TextBackGroundLay_Width + (double)scaled_TextBackGroundLay_Height * (double)scaled_TextBackGroundLay_Height) + 0.5f;
    int transformedTextBackGroundLayHeight = MediaMath::Sqrt((double)scaled_TextBackGroundLay_Width * (double)scaled_TextBackGroundLay_Width + (double)scaled_TextBackGroundLay_Height * (double)scaled_TextBackGroundLay_Height) + 0.5f;
    
    transformTextBackGroundLayFilter->onOutputSizeChanged(transformedTextBackGroundLayWidth, transformedTextBackGroundLayHeight);

    Matrix::setIdentityM(textBackGroundLayTransform, 0);
    Matrix::setRotateM(textBackGroundLayTransform, 0, (float)rotation, 0.0f, 0.0f, 1.0f);
    Matrix::scaleM(textBackGroundLayTransform, 0, scale, scale, 1.0f);
    transformTextBackGroundLayFilter->setTransform3D(textBackGroundLayTransform);
    
    if (!isCreatedTransformedTextBackGroundLayFBO) {
        createTransformedTextBackGroundLayFBO(transformedTextBackGroundLayWidth, transformedTextBackGroundLayHeight);
        transformedTextBackGroundLayFrameBufferWidth = transformedTextBackGroundLayWidth;
        transformedTextBackGroundLayFrameBufferHeight = transformedTextBackGroundLayHeight;
        isCreatedTransformedTextBackGroundLayFBO = true;
    }
    
    if (isCreatedTransformedTextBackGroundLayFBO) {
        if (transformedTextBackGroundLayFrameBufferWidth!=transformedTextBackGroundLayWidth || transformedTextBackGroundLayFrameBufferHeight != transformedTextBackGroundLayHeight) {
            deleteTransformedTextBackGroundLayFBO();
            createTransformedTextBackGroundLayFBO(transformedTextBackGroundLayWidth, transformedTextBackGroundLayHeight);
            transformedTextBackGroundLayFrameBufferWidth = transformedTextBackGroundLayWidth;
            transformedTextBackGroundLayFrameBufferHeight = transformedTextBackGroundLayHeight;
        }
    }
    
    glViewport(0, 0, transformedTextBackGroundLayFrameBufferWidth, transformedTextBackGroundLayFrameBufferHeight);
    glBindFramebuffer(GL_FRAMEBUFFER, transformedTextBackGroundLayFrameBuffer);
    
    float transformedTextBackGroundLayPosition[8];
    transformedTextBackGroundLayPosition[0] = -1.0f*(float)textBackGroundLayFrameBufferWidth/(float)transformedTextBackGroundLayFrameBufferWidth;
    transformedTextBackGroundLayPosition[1] = -1.0f*(float)textBackGroundLayFrameBufferHeight/(float)transformedTextBackGroundLayFrameBufferHeight;
    transformedTextBackGroundLayPosition[2] = (float)textBackGroundLayFrameBufferWidth/(float)transformedTextBackGroundLayFrameBufferWidth;
    transformedTextBackGroundLayPosition[3] = -1.0f*(float)textBackGroundLayFrameBufferHeight/(float)transformedTextBackGroundLayFrameBufferHeight;
    transformedTextBackGroundLayPosition[4] = -1.0f*(float)textBackGroundLayFrameBufferWidth/(float)transformedTextBackGroundLayFrameBufferWidth;
    transformedTextBackGroundLayPosition[5] = (float)textBackGroundLayFrameBufferHeight/(float)transformedTextBackGroundLayFrameBufferHeight;
    transformedTextBackGroundLayPosition[6] = (float)textBackGroundLayFrameBufferWidth/(float)transformedTextBackGroundLayFrameBufferWidth;
    transformedTextBackGroundLayPosition[7] = (float)textBackGroundLayFrameBufferHeight/(float)transformedTextBackGroundLayFrameBufferHeight;
    
    int inputFrameBufferTexture = textBackGroundLayFrameBufferTexture[textBackGroundLayFBOIndex];
    TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, transformedTextBackGroundLayTextureCoordinates);
    transformTextBackGroundLayFilter->onDrawFrame(inputFrameBufferTexture, transformedTextBackGroundLayPosition, transformedTextBackGroundLayTextureCoordinates);
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    return transformedTextBackGroundLayFrameBufferTexture;
}

//Create transformedTextBackGroundLay FBO
void iOSGPUImageOffScreenRender::createTransformedTextBackGroundLayFBO(int frameWidth, int frameHeight)
{
    glGenFramebuffers(1, &transformedTextBackGroundLayFrameBuffer);
    
    glGenTextures(1, &transformedTextBackGroundLayFrameBufferTexture);
    glBindTexture(GL_TEXTURE_2D, transformedTextBackGroundLayFrameBufferTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, frameWidth, frameHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, transformedTextBackGroundLayFrameBuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, transformedTextBackGroundLayFrameBufferTexture, 0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
//Delete transformedTextBackGroundLay FBO
void iOSGPUImageOffScreenRender::deleteTransformedTextBackGroundLayFBO()
{
    glDeleteTextures(1, &transformedTextBackGroundLayFrameBufferTexture);
    glDeleteFramebuffers(1, &transformedTextBackGroundLayFrameBuffer);
    
    transformedTextBackGroundLayFrameBufferTexture = -1;
    transformedTextBackGroundLayFrameBuffer = -1;
}

void iOSGPUImageOffScreenRender::createTextBackGroundLayFrameBufferObject(int frameWidth, int frameHeight, int fbo_index)
{
    glGenFramebuffers(1, &textBackGroundLayFrameBuffer[fbo_index]);
    
    glGenTextures(1, &textBackGroundLayFrameBufferTexture[fbo_index]);
    glBindTexture(GL_TEXTURE_2D, textBackGroundLayFrameBufferTexture[fbo_index]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, frameWidth, frameHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, textBackGroundLayFrameBuffer[fbo_index]);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textBackGroundLayFrameBufferTexture[fbo_index], 0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void iOSGPUImageOffScreenRender::deleteTextBackGroundLayFrameBufferObject(int fbo_index)
{
    glDeleteTextures(1, &textBackGroundLayFrameBufferTexture[fbo_index]);
    glDeleteFramebuffers(1, &textBackGroundLayFrameBuffer[fbo_index]);
    
    textBackGroundLayFrameBufferTexture[fbo_index] =-1;
    textBackGroundLayFrameBuffer[fbo_index] = -1;
}



int iOSGPUImageOffScreenRender::normalBlendToFrameBufferTexture(VideoFrame* overlay, int rotation, float scale, bool flipHorizontal, bool flipVertical, int x, int y, TextInfo* textInfo)
{
    EAGLContext* current = [EAGLContext currentContext];
    
    if (![EAGLContext setCurrentContext:_context]) {
        LOGE("Set EAGLContext Fail!");
    }
    
    normalBlendFilter->onOutputSizeChanged(offScreenDisPlayWidth, offScreenDisplayHeight);
    
    // init 2 TextBackGroundLay FBOs
    if (!isCreatedTextBackGroundLayFrameBufferObject) {
        for (int i = 0; i < 2; i++) {
            createTextBackGroundLayFrameBufferObject(overlay->width, overlay->height, i);
        }
        textBackGroundLayFrameBufferWidth = overlay->width;
        textBackGroundLayFrameBufferHeight = overlay->height;
        isCreatedTextBackGroundLayFrameBufferObject = true;
    }
    
    if (isCreatedTextBackGroundLayFrameBufferObject) {
        if (textBackGroundLayFrameBufferWidth!=overlay->width || textBackGroundLayFrameBufferHeight!=overlay->height) {
            for (int i=0; i <2; i++) {
                deleteTextBackGroundLayFrameBufferObject(i);
                createTextBackGroundLayFrameBufferObject(overlay->width, overlay->height, i);
            }
            textBackGroundLayFrameBufferWidth = overlay->width;
            textBackGroundLayFrameBufferHeight = overlay->height;
        }
    }
    
    // input and flip TextBackGroundLay to FBO
    flipTextBackGroundLayToFBO(overlay, flipHorizontal, flipVertical);
    
    // blend all words to TextBackGroundLayFBO
    for (int i = 0; i < textInfo->wordCount; i++) {
        WordInfo* wordInfo = textInfo->words[i];

        if (wordInfo->isInRenderRect) {
            normalBlendWordToTextBackGroundLayFBO(wordInfo);
        }
    }
    
    //transform TextBackGroundLayFBO
    int overlayFBOTextureId = transformTextBackGroundLayFBO(rotation, scale);
    
    // blend transformed TextBackGroundLayFBO to Main FBO
    int inputFrameBufferTexture = frameBufferTexture[fboIndex];
    fboIndex = (fboIndex+1)%2;
    bind(offScreenDisPlayWidth, offScreenDisplayHeight, fboIndex);
    normalBlendFilter->onDrawFrame(inputFrameBufferTexture, overlayFBOTextureId, x-transformedTextBackGroundLayFrameBufferWidth/2, y-transformedTextBackGroundLayFrameBufferHeight/2, transformedTextBackGroundLayFrameBufferWidth, transformedTextBackGroundLayFrameBufferHeight);
    unBind();
    
    [EAGLContext setCurrentContext:current];
    
    return frameBufferTexture[fboIndex];}
