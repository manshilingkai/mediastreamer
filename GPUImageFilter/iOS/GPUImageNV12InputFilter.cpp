//
//  GPUImageNV12InputFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2017/4/11.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageNV12InputFilter.h"
#include "OpenGLUtils.h"
#include "TextureRotationUtil.h"

#include <CoreFoundation/CoreFoundation.h>

#include "MediaLog.h"

const char GPUImageNV12InputFilter::vertext_shader_[] = {
    "attribute vec4 position;\n"
    "attribute vec2 texCoord;\n"
    "uniform float preferredRotation;\n"
    "varying vec2 texCoordVarying;\n"
    "void main()\n"
    "{\n"
    "    mat4 rotationMatrix = mat4(cos(preferredRotation), -sin(preferredRotation), 0.0, 0.0,\n"
    "                               sin(preferredRotation),  cos(preferredRotation), 0.0, 0.0,\n"
    "                               0.0,					    0.0, 1.0, 0.0,\n"
    "                               0.0,					    0.0, 0.0, 1.0);\n"
    "    gl_Position = position * rotationMatrix;\n"
    "    texCoordVarying = texCoord;\n"
    "}\n"
};

const char GPUImageNV12InputFilter::fragment_shader_[] = {
    "varying highp vec2 texCoordVarying;\n"
    "precision mediump float;\n"
    "uniform sampler2D SamplerY;\n"
    "uniform sampler2D SamplerUV;\n"
    "uniform mat3 colorConversionMatrix;\n"
    "void main()\n"
    "{\n"
    "    mediump vec3 yuv;\n"
    "    lowp vec3 rgb;\n"
    //   Subtract constants to map the video range start at 0
    "    yuv.x = (texture2D(SamplerY, texCoordVarying).r - (16.0/255.0));\n"
    "    yuv.yz = (texture2D(SamplerUV, texCoordVarying).rg - vec2(0.5, 0.5));\n"
    "    rgb = colorConversionMatrix * yuv;\n"
    "    gl_FragColor = vec4(rgb, 1);\n"
    "}\n"
};

const GLfloat GPUImageNV12InputFilter::kColorConversion601[] = {
    1.164,  1.164, 1.164,
    0.0, -0.392, 2.017,
    1.596, -0.813,   0.0,
};
const GLfloat GPUImageNV12InputFilter::kColorConversion709[] = {
    1.164,  1.164, 1.164,
    0.0, -0.213, 2.112,
    1.793, -0.533,   0.0,
};

GPUImageNV12InputFilter::GPUImageNV12InputFilter()
{
    output_framebuffer_width_ = -1;
    output_framebuffer_height_ = -1;
    isOutputFramebufferSizeChanged = false;
    
    isCreateFBO = false;
    
    textureCoordinates = new float[8];
}

GPUImageNV12InputFilter::~GPUImageNV12InputFilter()
{
    delete [] textureCoordinates;
}

void GPUImageNV12InputFilter::init()
{
    program = OpenGLUtils::loadProgram(vertext_shader_, fragment_shader_);
    
    attribPositionHandle = glGetAttribLocation(program, "position");
    attribTextureCoordinateHandle = glGetAttribLocation(program, "texCoord");

    uniforms[UNIFORM_ROTATION_ANGLE] = glGetUniformLocation(program, "preferredRotation");

    uniforms[UNIFORM_Y] = glGetUniformLocation(program, "SamplerY");
    uniforms[UNIFORM_UV] = glGetUniformLocation(program, "SamplerUV");
    uniforms[UNIFORM_COLOR_CONVERSION_MATRIX] = glGetUniformLocation(program, "colorConversionMatrix");

    TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, textureCoordinates);
}

void GPUImageNV12InputFilter::destroy()
{
    output_framebuffer_width_ = -1;
    output_framebuffer_height_ = -1;
    isOutputFramebufferSizeChanged = false;
    
    if (isCreateFBO) {
        deleteFBO();
        isCreateFBO = false;
    }
    
    glDeleteProgram(program);
}

int GPUImageNV12InputFilter::onDrawToTexture(EAGLContext* context, const NV12GPUImage& frame)
{
    // calculate texture coordinate
    GPUImageRotationMode rotationMode;
    if (frame.rotation==90) {
        rotationMode = kGPUImageRotateRight;
    }else if(frame.rotation==180) {
        rotationMode = kGPUImageRotate180;
    }else if(frame.rotation==270) {
        rotationMode = kGPUImageRotateLeft;
    }else {
        rotationMode = kGPUImageNoRotation;
    }
    
    TextureRotationUtil::calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, textureCoordinates);
    
    //update FBO
    if (frame.rotation==90 || frame.rotation==270) {
        if (output_framebuffer_width_ != frame.height || output_framebuffer_height_ != frame.width) {
            output_framebuffer_width_ = frame.height;
            output_framebuffer_height_ = frame.width;
            
            isOutputFramebufferSizeChanged = true;
        }
    }else{
        if (output_framebuffer_width_ != frame.width || output_framebuffer_height_ != frame.height) {
            output_framebuffer_width_ = frame.width;
            output_framebuffer_height_ = frame.height;
            
            isOutputFramebufferSizeChanged = true;
        }
    }
    
    if (isOutputFramebufferSizeChanged) {
        if (isCreateFBO) {
            deleteFBO();
            isCreateFBO = false;
        }
        
        createFBO(output_framebuffer_width_, output_framebuffer_height_);
        isCreateFBO = true;
    }
    
    //draw
    glViewport(0, 0, output_framebuffer_width_, output_framebuffer_height_);
    
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer_);
    
    glUseProgram(program);
    
    // Update attribute values.
    glVertexAttribPointer(attribPositionHandle, 2, GL_FLOAT, false, 0, TextureRotationUtil::CUBE);
    glEnableVertexAttribArray(attribPositionHandle);
    
    glVertexAttribPointer(attribTextureCoordinateHandle, 2, GL_FLOAT, false, 0, textureCoordinates);
    glEnableVertexAttribArray(attribTextureCoordinateHandle);
    
    //upload texture
    CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)frame.opaque;
    
    CVPixelBufferLockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
    size_t planeCount = CVPixelBufferGetPlaneCount(pixelBuffer);
    int videoWidth = (int)CVPixelBufferGetWidth(pixelBuffer);
    int videoHeight = (int)CVPixelBufferGetHeight(pixelBuffer);
    
    /*
     Use the color attachment of the pixel buffer to determine the appropriate color conversion matrix.
     */
    CFTypeRef colorAttachments = CVBufferGetAttachment(pixelBuffer, kCVImageBufferYCbCrMatrixKey, NULL);
    
    if (CFStringCompare((CFStringRef)colorAttachments, kCVImageBufferYCbCrMatrix_ITU_R_601_4, 0) == kCFCompareEqualTo) {
        preferredConversion = kColorConversion601;
    }
    else {
        preferredConversion = kColorConversion709;
    }
    
    /*
     CVOpenGLESTextureCacheCreateTextureFromImage will create GLES texture optimally from CVPixelBufferRef.
     */
    
    /*
     Create Y and UV textures from the pixel buffer. These textures will be drawn on the frame buffer Y-plane.
     */
    CVReturn err;
    CVOpenGLESTextureCacheRef videoTextureCache;
    
    // Create CVOpenGLESTextureCacheRef for optimal CVPixelBufferRef to GLES texture conversion.
    err = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, context, NULL, &videoTextureCache);
    if (err != noErr) {
        LOGE("Error at CVOpenGLESTextureCacheCreate %d", err);
    }
    
    glActiveTexture(GL_TEXTURE0);
    err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                       videoTextureCache,
                                                       pixelBuffer,
                                                       NULL,
                                                       GL_TEXTURE_2D,
                                                       GL_RED_EXT,
                                                       videoWidth,
                                                       videoHeight,
                                                       GL_RED_EXT,
                                                       GL_UNSIGNED_BYTE,
                                                       0,
                                                       &_lumaTexture);
    if (err) {
        LOGE("Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
    }
    glBindTexture(CVOpenGLESTextureGetTarget(_lumaTexture), CVOpenGLESTextureGetName(_lumaTexture));
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glUniform1i(uniforms[UNIFORM_Y], 0);
    
    if(planeCount == 2) {
        // UV-plane.
        glActiveTexture(GL_TEXTURE1);
        err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                           videoTextureCache,
                                                           pixelBuffer,
                                                           NULL,
                                                           GL_TEXTURE_2D,
                                                           GL_RG_EXT,
                                                           videoWidth / 2,
                                                           videoHeight / 2,
                                                           GL_RG_EXT,
                                                           GL_UNSIGNED_BYTE,
                                                           1,
                                                           &_chromaTexture);
        if (err) {
            LOGE("Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
        }
        glBindTexture(CVOpenGLESTextureGetTarget(_chromaTexture), CVOpenGLESTextureGetName(_chromaTexture));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glUniform1i(uniforms[UNIFORM_UV], 1);
    }
    
    CVPixelBufferUnlockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
    
    glUniform1f(uniforms[UNIFORM_ROTATION_ANGLE], 0);
    glUniformMatrix3fv(uniforms[UNIFORM_COLOR_CONVERSION_MATRIX], 1, GL_FALSE, preferredConversion);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(attribPositionHandle);
    glDisableVertexAttribArray(attribTextureCoordinateHandle);
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    isOutputFramebufferSizeChanged = false;
    
    if (_lumaTexture) {
        CFRelease(_lumaTexture);
        _lumaTexture = NULL;
    }
    
    if (_chromaTexture) {
        CFRelease(_chromaTexture);
        _chromaTexture = NULL;
    }
    
    CVOpenGLESTextureCacheFlush(videoTextureCache, 0);
    
    if(videoTextureCache) {
        CFRelease(videoTextureCache);
    }
    
    return frameBufferTexture_;
}

int GPUImageNV12InputFilter::getOutputFrameBufferWidth()
{
    return output_framebuffer_width_;
}

int GPUImageNV12InputFilter::getOutputFrameBufferHeight()
{
    return output_framebuffer_height_;
}

//Create FBO
void GPUImageNV12InputFilter::createFBO(int frameWidth, int frameHeight)
{
    glGenFramebuffers(1, &frameBuffer_);
    
    glGenTextures(1, &frameBufferTexture_);
    glBindTexture(GL_TEXTURE_2D, frameBufferTexture_);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, frameWidth, frameHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer_);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, frameBufferTexture_, 0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

//Delete FBO
void GPUImageNV12InputFilter::deleteFBO()
{
    glDeleteTextures(1, &frameBufferTexture_);
    glDeleteFramebuffers(1, &frameBuffer_);
}
