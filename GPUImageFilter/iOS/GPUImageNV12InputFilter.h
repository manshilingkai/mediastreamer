//
//  GPUImageNV12InputFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/4/11.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageNV12InputFilter_h
#define GPUImageNV12InputFilter_h

#include <stdio.h>
#include <stdlib.h>

#include <CoreVideo/CoreVideo.h>

// Uniform index.
enum
{
    UNIFORM_Y,
    UNIFORM_UV,
    UNIFORM_ROTATION_ANGLE,
    UNIFORM_COLOR_CONVERSION_MATRIX,
    NUM_UNIFORMS
};

// Attribute index.
enum
{
    ATTRIB_VERTEX,
    ATTRIB_TEXCOORD,
    NUM_ATTRIBUTES
};

struct NV12GPUImage {
    uint8_t* y_plane;
    uint8_t* uv_plane;
    
    int width;
    int height;
    
    int y_stride;
    int uv_stride;
    
    int rotation;
    
    void* opaque;
    
    NV12GPUImage()
    {
        y_plane = NULL;
        uv_plane = NULL;
        
        width = 0;
        height = 0;
        
        y_stride = 0;
        uv_stride = 0;
        
        rotation = 0;
        
        opaque = NULL;
    }
};

class GPUImageNV12InputFilter {
public:
    GPUImageNV12InputFilter();
    ~GPUImageNV12InputFilter();
    
    void init();
    void destroy();
    
    int onDrawToTexture(EAGLContext* context, const NV12GPUImage& frame);
    
    int getOutputFrameBufferWidth();
    int getOutputFrameBufferHeight();
private:
    static const char vertext_shader_[];
    static const char fragment_shader_[];
    
    static const GLfloat kColorConversion601[];
    static const GLfloat kColorConversion709[];
private:
    //Create FBO
    void createFBO(int frameWidth, int frameHeight);
    
    //Delete FBO
    void deleteFBO();
    
    GLuint frameBuffer_;
    GLuint frameBufferTexture_;
    
    bool isCreateFBO;
    
private:
    GLuint program;
    
    int attribPositionHandle;
    int attribTextureCoordinateHandle;
    
    GLint uniforms[NUM_UNIFORMS];
    
    float* textureCoordinates;

    GLsizei output_framebuffer_width_;
    GLsizei output_framebuffer_height_;
    bool isOutputFramebufferSizeChanged;

    const GLfloat *preferredConversion;

    CVOpenGLESTextureRef _lumaTexture;
    CVOpenGLESTextureRef _chromaTexture;
};

#endif /* GPUImageNV12InputFilter_h */
