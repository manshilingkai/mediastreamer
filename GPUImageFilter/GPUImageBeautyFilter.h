//
//  GPUImageBeautyFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/3/22.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageBeautyFilter_h
#define GPUImageBeautyFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageBeautyFilter : public GPUImageFilter{
public:
    GPUImageBeautyFilter();
    ~GPUImageBeautyFilter();
    
    void onOutputSizeChanged(int width, int height);

    void setBeautyLevel(float level);
protected:
    void onInit();
    
private:
    static const char BEAUTY_FRAGMENT_SHADER[];

private:
    void setTexelSize(float w, float h);
//    void setBeautyLevel(float level);
    void setBrightLevel(float level);
    void setToneLevel(float level);
    void setParams(float beautyLevel, float toneLevel);
private:
    int mSingleStepOffsetLocation;
    int mParamsLocation;
    int mBrightnessLocation;
    
    float mBeautyLevel;
    float mBrightLevel;
    float mToneLevel;
};

#endif /* GPUImageBeautyFilter_h */
