//
//  GPUImageRawPixelOutputFilter.cpp
//  MediaStreamer
//
//  Created by Think on 2017/2/28.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageRawPixelOutputFilter.h"

GPUImageRawPixelOutputFilter::GPUImageRawPixelOutputFilter()
{
    isCreateFBO = false;
    mOutputVideoFrame = NULL;
}

GPUImageRawPixelOutputFilter::~GPUImageRawPixelOutputFilter()
{

}

void GPUImageRawPixelOutputFilter::createFBO(int frameBufferWidth, int frameBufferHeight)
{
    if (isCreateFBO) return;
    
    mOutputFrameBufferWidth = frameBufferWidth;
    mOutputFrameBufferHeight = frameBufferHeight;
    
    glGenFramebuffers(1, &frameBuffer_);
    
    glGenTextures(1, &frameBufferTexture_);
    glBindTexture(GL_TEXTURE_2D, frameBufferTexture_);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, frameBufferWidth, frameBufferHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer_);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, frameBufferTexture_, 0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    mOutputVideoFrame = new VideoFrame;
    mOutputVideoFrame->frameSize = mOutputFrameBufferWidth * mOutputFrameBufferHeight * 4;
    mOutputVideoFrame->data = (uint8_t*)malloc(mOutputVideoFrame->frameSize);
    mOutputVideoFrame->width = mOutputFrameBufferWidth;
    mOutputVideoFrame->height = mOutputFrameBufferHeight;
    
    isCreateFBO = true;
}

void GPUImageRawPixelOutputFilter::deleteFBO()
{
    if (!isCreateFBO) return;
    
    glDeleteTextures(1, &frameBufferTexture_);
    glDeleteFramebuffers(1, &frameBuffer_);
    
    if (mOutputVideoFrame!=NULL) {
        if (mOutputVideoFrame->data!=NULL) {
            free(mOutputVideoFrame->data);
            mOutputVideoFrame->data = NULL;
        }
        
        delete mOutputVideoFrame;
        mOutputVideoFrame = NULL;
    }
    
    isCreateFBO = false;
}

void GPUImageRawPixelOutputFilter::bind()
{
    glViewport(0, 0, mOutputFrameBufferWidth, mOutputFrameBufferHeight);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer_);
}

void GPUImageRawPixelOutputFilter::unBind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

VideoFrame *GPUImageRawPixelOutputFilter::outputPixelBuffer()
{
    glReadPixels(0, 0, mOutputFrameBufferWidth, mOutputFrameBufferHeight, GL_RGBA, GL_UNSIGNED_BYTE, mOutputVideoFrame->data);
    return mOutputVideoFrame;
}

void GPUImageRawPixelOutputFilter::outputPixelBuffer(VideoFrame *outputVideoFrame)
{
    outputVideoFrame->width = mOutputFrameBufferWidth;
    outputVideoFrame->height = mOutputFrameBufferHeight;
    outputVideoFrame->frameSize = mOutputFrameBufferWidth * mOutputFrameBufferHeight * 4;
    
    glReadPixels(0, 0, outputVideoFrame->width, outputVideoFrame->height, GL_RGBA, GL_UNSIGNED_BYTE, outputVideoFrame->data);
}

