//
//  GPUImageTransformFilter.cpp
//  MediaStreamer
//
//  Created by Think on 2017/3/23.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageTransformFilter.h"
#include "Matrix.h"
#include "MediaLog.h"

const char GPUImageTransformFilter::TRANSFORM_VERTEX_SHADER[] = {
    "attribute vec4 position;\n"
    "attribute vec4 inputTextureCoordinate;\n"
    "uniform mat4 transformMatrix;\n"
    "uniform mat4 orthographicMatrix;\n"
    "varying vec2 textureCoordinate;\n"
    "void main()\n"
    "{\n"
    "gl_Position = transformMatrix * vec4(position.xyz, 1.0) * orthographicMatrix;\n"
    "textureCoordinate = inputTextureCoordinate.xy;\n"
    "}\n"
};

GPUImageTransformFilter::GPUImageTransformFilter()
    : GPUImageFilter(TRANSFORM_VERTEX_SHADER, GPUImageFilter::NO_FILTER_FRAGMENT_SHADER)
{
    orthographicMatrix = new float[16];
    Matrix::orthoM(orthographicMatrix, 0, -1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f);
    transform3D = new float[16];
    Matrix::setIdentityM(transform3D, 0);
}

GPUImageTransformFilter::~GPUImageTransformFilter()
{
    delete [] orthographicMatrix;
    delete [] transform3D;
}

void GPUImageTransformFilter::onInit()
{
    GPUImageFilter::onInit();
    
    transformMatrixUniform = glGetUniformLocation(getProgram(), "transformMatrix");
    orthographicMatrixUniform = glGetUniformLocation(getProgram(), "orthographicMatrix");
    
    setUniformMatrix4f(transformMatrixUniform, transform3D, 16);
    setUniformMatrix4f(orthographicMatrixUniform, orthographicMatrix, 16);
}

void GPUImageTransformFilter::onOutputSizeChanged(int width, int height)
{
    GPUImageFilter::onOutputSizeChanged(width, height);
    
    Matrix::orthoM(orthographicMatrix, 0, -1.0f, 1.0f, -1.0f * (float) height / (float) width, 1.0f * (float) height / (float) width, -1.0f, 1.0f);
    setUniformMatrix4f(orthographicMatrixUniform, orthographicMatrix, 16);
}

int GPUImageTransformFilter::onDrawFrame(const int textureId, const float* cubeBuffer, const float* textureBuffer)
{
    float* vertBuffer = new float[8];
    for (int i=0; i<8; i++) {
        vertBuffer[i] = cubeBuffer[i];
    }
    float normalizedHeight = (float) mOutputHeight / (float) mOutputWidth;
    normalizedHeight = 1.0f;
    vertBuffer[1] *= normalizedHeight;
    vertBuffer[3] *= normalizedHeight;
    vertBuffer[5] *= normalizedHeight;
    vertBuffer[7] *= normalizedHeight;
    
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT);
    int ret= GPUImageFilter::onDrawFrame(textureId, vertBuffer, textureBuffer);
    delete [] vertBuffer;
    return ret;
}

void GPUImageTransformFilter::setTransform3D(float* transform)
{
    for (int i = 0; i< 16; i++) {
        transform3D[i] = transform[i];
    }
    
    setUniformMatrix4f(transformMatrixUniform, transform3D, 16);
}
