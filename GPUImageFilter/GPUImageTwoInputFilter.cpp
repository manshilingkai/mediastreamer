//
//  GPUImageTwoInputFilter.cpp
//  MediaStreamer
//
//  Created by Think on 2017/3/10.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageTwoInputFilter.h"
#include "TextureRotationUtil.h"

#include "MediaLog.h"

const char GPUImageTwoInputFilter::TWOINPUT_VERTEX_SHADER[] =
{
    "attribute vec4 position;\n"
    "attribute vec4 inputTextureCoordinate;\n"
    "attribute vec4 inputTextureCoordinate2;\n"
    " \n"
    "varying vec2 textureCoordinate;\n"
    "varying vec2 textureCoordinate2;\n"
    "varying lowp vec2 mainPostion;\n"
    " \n"
    "void main()\n"
    "{\n"
    "    mainPostion = position.xy;\n"
    "    gl_Position = position;\n"
    "    textureCoordinate = inputTextureCoordinate.xy;\n"
    "    textureCoordinate2 = inputTextureCoordinate2.xy;\n"
    "}\n"
};

GPUImageTwoInputFilter::GPUImageTwoInputFilter(const char* fragmentShader)
    : GPUImageFilter(TWOINPUT_VERTEX_SHADER, fragmentShader)
{
    mTexture2CoordinatesBuffer = new float[8];
    TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, mTexture2CoordinatesBuffer);
    
    mFilterSourceTexture2 = OpenGLUtils::NO_TEXTURE;
}

GPUImageTwoInputFilter::~GPUImageTwoInputFilter()
{
    delete [] mTexture2CoordinatesBuffer;
}

void GPUImageTwoInputFilter::onInit()
{
    GPUImageFilter::onInit();

    mFilterSecondTextureCoordinateAttribute = glGetAttribLocation(getProgram(), "inputTextureCoordinate2");
    mFilterInputTextureUniform2 = glGetUniformLocation(getProgram(), "inputImageTexture2");
    
    leftTopUniform = glGetUniformLocation(getProgram(), "leftTop");
    rightBottomUniform = glGetUniformLocation(getProgram(), "rightBottom");
}

/*
void GPUImageTwoInputFilter::uploadSecondInput(void *data, int x, int y, int width, int height)
{
    mFilterSourceTexture2 = OpenGLUtils::loadTexture(data, width, height, mFilterSourceTexture2);
    
    SecondInput_X = x;
    SecondInput_Y = y;
    SecondInput_Width = width;
    SecondInput_height = height;
}
*/

int GPUImageTwoInputFilter::onDrawFrame(const int firstInputTextureId, int secondInputTextureId, int x, int y, int secondInputTexture_Width, int secondInputTexture_Height)
{
    mFilterSourceTexture2 = secondInputTextureId;
    SecondInput_X = x;
    SecondInput_Y = y;
    SecondInput_Width = secondInputTexture_Width;
    SecondInput_height = secondInputTexture_Height;
    
    return GPUImageFilter::onDrawFrame(firstInputTextureId);
}

void GPUImageTwoInputFilter::onDrawArraysPre()
{
    glVertexAttribPointer(mFilterSecondTextureCoordinateAttribute, 2, GL_FLOAT, false, 0, mTexture2CoordinatesBuffer);
    glEnableVertexAttribArray(mFilterSecondTextureCoordinateAttribute);
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, mFilterSourceTexture2);
    glUniform1i(mFilterInputTextureUniform2, 1);
    
    /*
    GLfloat leftBottomUniform_x = ((float)SecondInput_X/(float)mOutputWidth)*2.0f-1.0f;
    GLfloat leftBottomUniform_y = (((float)mOutputHeight-(float)SecondInput_Y-(float)SecondInput_height)/(float)mOutputHeight)*2.0f-1.0f;
    
    GLfloat rightTopUniform_x = (((float)SecondInput_X+(float)SecondInput_Width)/(float)mOutputWidth)*2.0f-1.0f;
    GLfloat rightTopUniform_y = (((float)mOutputHeight-(float)SecondInput_Y)/(float)mOutputHeight)*2.0f-1.0f;
    */
    
    GLfloat leftTop_x = ((float)SecondInput_X/(float)mOutputWidth)*2.0f-1.0f;
    GLfloat leftTop_y = ((float)SecondInput_Y/(float)mOutputHeight)*2.0f-1.0f;
    
    GLfloat rightBottom_x = (((float)SecondInput_X+(float)SecondInput_Width)/(float)mOutputWidth)*2.0f-1.0f;
    GLfloat rightBottom_y = (((float)SecondInput_Y+(float)SecondInput_height)/(float)mOutputHeight)*2.0f-1.0f;
    
//    LOGD("leftTop_x:%f",leftTop_x);
//    LOGD("leftTop_y:%f",leftTop_y);
//    LOGD("rightBottom_x:%f",rightBottom_x);
//    LOGD("rightBottom_y:%f",rightBottom_y);
    
    glUniform2f(leftTopUniform, leftTop_x, leftTop_y);
    glUniform2f(rightBottomUniform, rightBottom_x, rightBottom_y);
}

void GPUImageTwoInputFilter::onDrawArraysAfter()
{
    glDisableVertexAttribArray(mFilterSecondTextureCoordinateAttribute);
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, 0);
}
