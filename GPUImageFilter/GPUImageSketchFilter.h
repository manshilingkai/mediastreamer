//
//  GPUImageSketchFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/2/16.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageSketchFilter_h
#define GPUImageSketchFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageSketchFilter : public GPUImageFilter {
public:
    GPUImageSketchFilter();
    ~GPUImageSketchFilter();
    
    void onOutputSizeChanged(int width, int height);
protected:
    void onInit();
private:
    static const char SKETCH_FRAGMENT_SHADER[];

    void setTexelSize(float w, float h);
    
    int mSingleStepOffsetLocation;
};

#endif /* GPUImageSkechFilter_h */
