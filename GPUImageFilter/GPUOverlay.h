//
//  GPUOverlay.h
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/22.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef GPUOverlay_h
#define GPUOverlay_h

#include <stdio.h>
#include "MediaDataType.h"
#include "GPUImageTransformFilter.h"
#include "GPUImageNormalBlendFilter.h"
#include <mutex>

class GPUOverlay {
public:
    GPUOverlay();
    ~GPUOverlay();
    
    void initialize();
    void terminate();
    
    void setOverlay(char* imageUrl);
    int blendTo(int mainTextureId, int mainWidth, int mainHeight, int x, int y);
    int blendTo(int mainTextureId, int mainWidth, int mainHeight, int x, int y, int overlayRotation, float overlayScale, bool overlayFlipHorizontal, bool overlayFlipVertical);
    bool blendTo(int mainTextureId, int mainWidth, int mainHeight, int x, int y, int overlayRotation, float overlayScale, bool overlayFlipHorizontal, bool overlayFlipVertical, int outputTextureId);
private:
    std::mutex mGPUOverlayMutex;
    bool initialized_;
private:
    //for overlay
    VideoFrame* mOverlayImageFrame;
    bool isOverlayUpdate;
    int mScaledOverlayWidth;
    int mScaledOverlayHeight;
    
    GPUImageTransformFilter *overlayTransformFilter;
    float* overlayTransform;
    float* overlayTextureCoordinates;
    
    GLuint overlayFrameBuffer;
    GLuint overlayFrameBufferTexture;
    int overlayFrameBufferWidth;
    int overlayFrameBufferHeight;
    void createOverlayFBO(int frameWidth, int frameHeight);
    void deleteOverlayFBO();
    bool isCreatedOverlayFBO;
    
    int renderOverlayToFBO(VideoFrame* overlay, int rotation, float scale, bool flipHorizontal, bool flipVertical);
private:
    //for output
    GPUImageNormalBlendFilter *normalBlendFilter;
    
    GLuint frameBuffer;
    GLuint frameBufferTexture;
    int frameBufferWidth;
    int frameBufferHeight;
    void createFrameBufferObject(int frameWidth, int frameHeight, int textureId);
    void deleteFrameBufferObject();
    void bind(int frameWidth, int frameHeight);
    void unBind();
    bool isCreatedFrameBufferObject;
};

#endif /* GPUOverlay_h */
