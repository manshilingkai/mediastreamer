//
//  GPUImageNormalBlendFilter.cpp
//  MediaStreamer
//
//  Created by Think on 2017/3/10.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageNormalBlendFilter.h"

const char GPUImageNormalBlendFilter::NORMAL_BLEND_FRAGMENT_SHADER[] = {
    "varying highp vec2 textureCoordinate;\n"
    " varying highp vec2 textureCoordinate2;\n"
    " \n"
    " varying lowp vec2 mainPostion;\n"
    " uniform lowp vec2 leftTop;\n"
    " uniform lowp vec2 rightBottom;\n"
    " \n"
    " uniform sampler2D inputImageTexture;\n"
    " uniform sampler2D inputImageTexture2;\n"
    " \n"
    " void main()\n"
    " {\n"
    "     lowp vec4 c2 = texture2D(inputImageTexture, textureCoordinate);\n"
    "     \n"
    "     if (mainPostion.x >= leftTop.x && mainPostion.y >= leftTop.y && mainPostion.x <= rightBottom.x && mainPostion.y <= rightBottom.y)\n"
    "     {\n"
    "     lowp vec4 outputColor;\n"
    "     \n"
    "     lowp vec2 overlayCoordinate = vec2((mainPostion.x - leftTop.x) / (rightBottom.x - leftTop.x), (mainPostion.y - leftTop.y) / (rightBottom.y - leftTop.y));\n"
    "     lowp vec4 c1 = texture2D(inputImageTexture2, overlayCoordinate);\n"
    "     lowp float a = c1.a + c2.a * (1.0 - c1.a);\n"
    "     lowp float alphaDivisor = a + step(a, 0.0);\n"
    "     outputColor.r = (c1.r * c1.a + c2.r * c2.a * (1.0 - c1.a))/alphaDivisor;\n"
    "     outputColor.g = (c1.g * c1.a + c2.g * c2.a * (1.0 - c1.a))/alphaDivisor;\n"
    "     outputColor.b = (c1.b * c1.a + c2.b * c2.a * (1.0 - c1.a))/alphaDivisor;\n"
    "     outputColor.a = a;\n"
    "     gl_FragColor = outputColor;\n"
    "     } else {\n"
    "     gl_FragColor = c2;\n"
    "     }\n"
    " }\n"
};


GPUImageNormalBlendFilter::GPUImageNormalBlendFilter()
    :GPUImageTwoInputFilter(NORMAL_BLEND_FRAGMENT_SHADER)
{
    
}

GPUImageNormalBlendFilter::~GPUImageNormalBlendFilter()
{

}
