//
//  OpenGLUtils.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "OpenGLUtils.h"
#include "MediaLog.h"

const int OpenGLUtils::NO_TEXTURE = -1;
const int OpenGLUtils::NOT_INIT = -1;
const int OpenGLUtils::ON_DRAWN = 1;

GLuint OpenGLUtils::loadShader(GLenum shader_type, const char* shader_source)
{
    GLuint shader = glCreateShader(shader_type);
    if (shader) {
        glShaderSource(shader, 1, &shader_source, NULL);
        glCompileShader(shader);
        
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint info_len = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_len);
            if (info_len) {
                char* buf = (char*)malloc(info_len);
                glGetShaderInfoLog(shader, info_len, NULL, buf);
                LOGE("%s: Could not compile shader %d: %s",
                     __FUNCTION__,
                     shader_type,
                     buf);
                free(buf);
            }
            glDeleteShader(shader);
            shader = 0;
        }
    }
    return shader;
}

GLuint OpenGLUtils::loadProgram(const char* vertex_source, const char* fragment_source)
{
    GLuint vertex_shader = loadShader(GL_VERTEX_SHADER, vertex_source);
    if (!vertex_shader) {
        return -1;
    }
    
    GLuint fragment_shader = loadShader(GL_FRAGMENT_SHADER, fragment_source);
    if (!fragment_shader) {
        return -1;
    }
    
    GLuint program = glCreateProgram();
    if (program) {
        glAttachShader(program, vertex_shader);
        glAttachShader(program, fragment_shader);
        glLinkProgram(program);
        GLint link_status = GL_FALSE;
        glGetProgramiv(program, GL_LINK_STATUS, &link_status);
        if (link_status != GL_TRUE) {
            GLint info_len = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_len);
            if (info_len) {
                char* buf = (char*)malloc(info_len);
                glGetProgramInfoLog(program, info_len, NULL, buf);
                LOGE("%s: Could not link program: %s",
                     __FUNCTION__,
                     buf);
                free(buf);
            }
            glDeleteProgram(program);
            program = 0;
        }
    }
    
    if (vertex_shader) {
        glDeleteShader(vertex_shader);
    }
    
    if (fragment_shader) {
        glDeleteShader(fragment_shader);
    }
    
    return program;
}


GLuint OpenGLUtils::loadTexture(void *pixels, int width, int height, int usedTexId)
{
    GLuint texture;
    if (usedTexId == NO_TEXTURE) {
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    } else {
        glBindTexture(GL_TEXTURE_2D, usedTexId);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
        texture = usedTexId;
    }
    return texture;
}


GLuint OpenGLUtils::createTexture(void *pixels, int width, int height)
{
    GLuint texture = NO_TEXTURE;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    return texture;
}

void OpenGLUtils::deleteTexture(GLuint usedTexId)
{
    glDeleteTextures(1, &usedTexId);
    
    usedTexId = NO_TEXTURE;
}
