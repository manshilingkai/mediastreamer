//
//  GPUImageBrannanFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/8/10.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageBrannanFilter_h
#define GPUImageBrannanFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

class GPUImageBrannanFilter : public GPUImageFilter{
public:    
    GPUImageBrannanFilter(char* filter_dir);
    ~GPUImageBrannanFilter();
protected:
    void onInit();
    void onInitialized();
    void onDestroy();
    
    void onDrawArraysPre();
    void onDrawArraysAfter();
private:
    static const char BRANNAN_FRAGMENT_SHADER[];
private:
    GLuint inputTextureHandles[5];
    int inputTextureUniformLocations[5];
private:
    char* mFilterDir;
};

#endif /* GPUImageBrannanFilter_h */
