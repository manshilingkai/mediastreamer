//
//  GPUImageNormalBlendFilter.h
//  MediaStreamer
//
//  Created by Think on 2017/3/10.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageNormalBlendFilter_h
#define GPUImageNormalBlendFilter_h

#include <stdio.h>
#include "GPUImageTwoInputFilter.h"

class GPUImageNormalBlendFilter : public GPUImageTwoInputFilter{
public:
    GPUImageNormalBlendFilter();
    ~GPUImageNormalBlendFilter();
private:
    static const char NORMAL_BLEND_FRAGMENT_SHADER[];
};

#endif /* GPUImageNormalBlendFilter_h */
