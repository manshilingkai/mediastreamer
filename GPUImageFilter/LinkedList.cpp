//
//  LinkedList.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/13.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "LinkedList.h"

LinkedList::LinkedList()
{
    pthread_mutex_init(&mLock, NULL);
}

LinkedList::~LinkedList()
{
    pthread_mutex_destroy(&mLock);
}

void LinkedList::push(Runnable* runnable)
{
    pthread_mutex_lock(&mLock);
    
    mRunnableQueue.push(runnable);
    
    pthread_mutex_unlock(&mLock);
}

Runnable* LinkedList::pop()
{
    Runnable *runnable = NULL;
    
    pthread_mutex_lock(&mLock);
    
    if (!mRunnableQueue.empty()) {
        runnable = mRunnableQueue.front();
        mRunnableQueue.pop();
    }
    
    pthread_mutex_unlock(&mLock);
    
    return runnable;
}

void LinkedList::flush()
{
    pthread_mutex_lock(&mLock);
    Runnable *runnable = NULL;
    while (!mRunnableQueue.empty()) {
        runnable = mRunnableQueue.front();
        if (runnable!=NULL) {
            delete runnable;
            runnable = NULL;
        }
        mRunnableQueue.pop();
    }
    pthread_mutex_unlock(&mLock);
}
