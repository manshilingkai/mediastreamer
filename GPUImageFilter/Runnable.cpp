//
//  Runnable.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/13.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "Runnable.h"

Runnable::Runnable(RUNNABLE_EVENT event, const int location, const int intValue)
{
    mEvent = event;
    mLocation = location;
    
    mIntValue = intValue;
    mValueCount = 1;
    
    mFloatValue = NULL;
}

Runnable::Runnable(RUNNABLE_EVENT event, const int location, const float floatValue)
{
    mEvent = event;
    mLocation = location;
    
    mValueCount = 1;
    mFloatValue = new float[1];
    mFloatValue[0] = floatValue;
}

Runnable::Runnable(RUNNABLE_EVENT event, const int location, const float* floatValue, const int valueCount)
{
    mEvent = event;
    mLocation = location;

    mValueCount = valueCount;
    mFloatValue = new float[valueCount];
    for (int i = 0; i<valueCount; i++) {
        mFloatValue[i] = floatValue[i];
    }
}

Runnable::~Runnable()
{
    if (mFloatValue!=NULL) {
        delete [] mFloatValue;
        mFloatValue = NULL;
    }
}

void Runnable::run()
{
    switch (mEvent) {
        case SETINTEGER:
            glUniform1i(mLocation, mIntValue);
            break;
            
        case SETFLOAT:
            glUniform1f(mLocation, mFloatValue[0]);
            break;
            
        case SETFLOATVEC2:
            glUniform2fv(mLocation, 1, mFloatValue);
            break;
            
        case SETFLOATVEC3:
            glUniform3fv(mLocation, 1, mFloatValue);
            break;
            
        case SETFLOATVEC4:
            glUniform4fv(mLocation, 1, mFloatValue);
            break;
        case SETFLOATARRAY:
            glUniform1fv(mLocation, mValueCount, mFloatValue);
            break;
        case SETPOINT:
            glUniform2fv(mLocation, 1, mFloatValue);
            break;
        case SETUNIFORMMATRIX3F:
            glUniformMatrix3fv(mLocation, 1, false, mFloatValue);
            break;
        case SETUNIFORMMATRIX4F:
            glUniformMatrix4fv(mLocation, 1, false, mFloatValue);
            break;
        default:
            break;
    }
}
