//
//  GPUOverlay.cpp
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/22.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "GPUOverlay.h"
#include "ImageProcesserUtils.h"
#include "MediaMath.h"
#include "Matrix.h"
#include "TextureRotationUtil.h"
//#include "MediaLog.h"

GPUOverlay::GPUOverlay()
{
    initialized_ = false;
    
    //overlay
    mOverlayImageFrame = NULL;
    isOverlayUpdate = false;
    mScaledOverlayWidth = 0;
    mScaledOverlayHeight = 0;
    
    overlayTransformFilter = new GPUImageTransformFilter();
    overlayTransform = new float[16];
    overlayTextureCoordinates = new float[8];
    
    overlayFrameBuffer = -1;
    overlayFrameBufferTexture = OpenGLUtils::NO_TEXTURE;
    overlayFrameBufferWidth  = -1;
    overlayFrameBufferHeight = -1;
    isCreatedOverlayFBO = false;
    
    //output
    normalBlendFilter = new GPUImageNormalBlendFilter();
    
    frameBuffer = -1;
    frameBufferTexture = OpenGLUtils::NO_TEXTURE;
    frameBufferWidth = -1;
    frameBufferHeight = -1;
    isCreatedFrameBufferObject = false;
}

GPUOverlay::~GPUOverlay()
{
    terminate();
    
    //overlay
    if (overlayTransformFilter) {
        delete overlayTransformFilter;
        overlayTransformFilter = NULL;
    }
    delete [] overlayTransform;
    delete [] overlayTextureCoordinates;
    
    //output
    if (normalBlendFilter) {
        delete normalBlendFilter;
        normalBlendFilter = NULL;
    }
}

void GPUOverlay::initialize()
{
    if (initialized_) {
        return;
    }
    
    //overlay
    overlayTransformFilter->init();
    
    //output
    normalBlendFilter->init();
    
    initialized_ = true;
}

void GPUOverlay::terminate()
{
    if (!initialized_) {
        return;
    }
    
    //overlay
    if (mOverlayImageFrame!=NULL) {
        mOverlayImageFrame->Free();
        delete mOverlayImageFrame;
        mOverlayImageFrame = NULL;
    }
    isOverlayUpdate = false;
    mScaledOverlayWidth = 0;
    mScaledOverlayHeight = 0;
    
    deleteOverlayFBO();
    overlayFrameBuffer = -1;
    overlayFrameBufferTexture = OpenGLUtils::NO_TEXTURE;
    overlayFrameBufferWidth  = -1;
    overlayFrameBufferHeight = -1;
    isCreatedOverlayFBO = false;
    
    overlayTransformFilter->destroy();
    
    //output
    deleteFrameBufferObject();
    frameBuffer = -1;
    frameBufferTexture = OpenGLUtils::NO_TEXTURE;
    frameBufferWidth = -1;
    frameBufferHeight = -1;
    isCreatedFrameBufferObject = false;
    
    normalBlendFilter->destroy();
    
    initialized_ = false;
}

//overlay
void GPUOverlay::createOverlayFBO(int frameWidth, int frameHeight)
{
    glGenFramebuffers(1, &overlayFrameBuffer);
    
    glGenTextures(1, &overlayFrameBufferTexture);
    glBindTexture(GL_TEXTURE_2D, overlayFrameBufferTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, frameWidth, frameHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, overlayFrameBuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, overlayFrameBufferTexture, 0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GPUOverlay::deleteOverlayFBO()
{
    glDeleteTextures(1, &overlayFrameBufferTexture);
    glDeleteFramebuffers(1, &overlayFrameBuffer);
}

int GPUOverlay::renderOverlayToFBO(VideoFrame* overlay, int rotation, float scale, bool flipHorizontal, bool flipVertical)
{
    float scaled_overlay_Width = scale * (float)overlay->width;
    float scaled_overlay_Height = scale * (float)overlay->height;
    
    mScaledOverlayWidth = scaled_overlay_Width;
    mScaledOverlayHeight = scaled_overlay_Height;
    
    int overlayOffDisplayWidth = MediaMath::Sqrt((double)scaled_overlay_Width * (double)scaled_overlay_Width + (double)scaled_overlay_Height * (double)scaled_overlay_Height) + 0.5f;
    int overlayOffDisplayHeight = MediaMath::Sqrt((double)scaled_overlay_Width * (double)scaled_overlay_Width + (double)scaled_overlay_Height * (double)scaled_overlay_Height) + 0.5f;
    
//    double alpha = MediaMath::PI*(double)rotation/(double)180;
//    overlayOffDisplayWidth = scaled_overlay_Width*MediaMath::Cos(alpha) + scaled_overlay_Height*MediaMath::Sin(alpha) + 0.5f;
//    overlayOffDisplayHeight = scaled_overlay_Width*MediaMath::Sin(alpha) + scaled_overlay_Height*MediaMath::Cos(alpha) + 0.5f;
    
    overlayTransformFilter->onOutputSizeChanged(overlayOffDisplayWidth, overlayOffDisplayHeight);
    
    GLuint overlayTexture = OpenGLUtils::createTexture(overlay->data, overlay->width, overlay->height);
    
    Matrix::setIdentityM(overlayTransform, 0);
    Matrix::setRotateM(overlayTransform, 0, (float)rotation, 0.0f, 0.0f, 1.0f);
    Matrix::scaleM(overlayTransform, 0, scale, scale, 1.0f);
    overlayTransformFilter->setTransform3D(overlayTransform);
    
    if (flipHorizontal && !flipVertical) {
        TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageFlipHorizonal, 0.0f, 0.0f, 1.0f, 1.0f, overlayTextureCoordinates);
    }else if(!flipHorizontal && flipVertical) {
        TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageFlipVertical, 0.0f, 0.0f, 1.0f, 1.0f, overlayTextureCoordinates);
    }else if(flipHorizontal && flipVertical) {
        TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageRotate180, 0.0f, 0.0f, 1.0f, 1.0f, overlayTextureCoordinates);
    }else {
        TextureRotationUtil::calculateCropTextureCoordinates(kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, overlayTextureCoordinates);
    }
    
    if (!isCreatedOverlayFBO) {
        createOverlayFBO(overlayOffDisplayWidth, overlayOffDisplayHeight);
        overlayFrameBufferWidth = overlayOffDisplayWidth;
        overlayFrameBufferHeight = overlayOffDisplayHeight;
        isCreatedOverlayFBO = true;
    }
    
    if (isCreatedOverlayFBO) {
        if (overlayFrameBufferWidth!=overlayOffDisplayWidth || overlayFrameBufferHeight != overlayOffDisplayHeight) {
            deleteOverlayFBO();
            createOverlayFBO(overlayOffDisplayWidth, overlayOffDisplayHeight);
            overlayFrameBufferWidth = overlayOffDisplayWidth;
            overlayFrameBufferHeight = overlayOffDisplayHeight;
        }
    }
    
    glViewport(0, 0, overlayFrameBufferWidth, overlayFrameBufferHeight);
    glBindFramebuffer(GL_FRAMEBUFFER, overlayFrameBuffer);
    
    float overlayPosition[8];
    overlayPosition[0] = -1.0f*(float)overlay->width/(float)overlayFrameBufferWidth;
    overlayPosition[1] = -1.0f*(float)overlay->height/(float)overlayFrameBufferHeight;
    overlayPosition[2] = (float)overlay->width/(float)overlayFrameBufferWidth;
    overlayPosition[3] = -1.0f*(float)overlay->height/(float)overlayFrameBufferHeight;
    overlayPosition[4] = -1.0f*(float)overlay->width/(float)overlayFrameBufferWidth;
    overlayPosition[5] = (float)overlay->height/(float)overlayFrameBufferHeight;
    overlayPosition[6] = (float)overlay->width/(float)overlayFrameBufferWidth;
    overlayPosition[7] = (float)overlay->height/(float)overlayFrameBufferHeight;
    
    overlayTransformFilter->onDrawFrame(overlayTexture, overlayPosition, overlayTextureCoordinates);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    OpenGLUtils::deleteTexture(overlayTexture);
    
    return overlayFrameBufferTexture;
}

//output
void GPUOverlay::createFrameBufferObject(int frameWidth, int frameHeight, int textureId)
{
    glGenFramebuffers(1, &frameBuffer);
    
    if (textureId==OpenGLUtils::NO_TEXTURE) {
        glGenTextures(1, &frameBufferTexture);
        glBindTexture(GL_TEXTURE_2D, frameBufferTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, frameWidth, frameHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, frameBufferTexture, 0);
    }else{
        glBindTexture(GL_TEXTURE_2D, textureId);
        
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);
    }
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GPUOverlay::deleteFrameBufferObject()
{
    if (frameBufferTexture==OpenGLUtils::NO_TEXTURE) {
        glDeleteFramebuffers(1, &frameBuffer);
    }else{
        glDeleteTextures(1, &frameBufferTexture);
        glDeleteFramebuffers(1, &frameBuffer);
    }
}

void GPUOverlay::bind(int frameWidth, int frameHeight)
{
    glViewport(0, 0, frameWidth, frameHeight);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
}

void GPUOverlay::unBind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GPUOverlay::setOverlay(char* imageUrl)
{
    VideoFrame* ret = PNGImageFileToRGBAVideoFrame(imageUrl);
    if (ret) {
        mGPUOverlayMutex.lock();
        if (mOverlayImageFrame) {
            mOverlayImageFrame->Free();
            delete mOverlayImageFrame;
            mOverlayImageFrame = NULL;
        }
        
        mOverlayImageFrame = ret;
        isOverlayUpdate = true;
        mGPUOverlayMutex.unlock();
    }
}

int GPUOverlay::blendTo(int mainTextureId, int mainWidth, int mainHeight, int x, int y)
{
    return this->blendTo(mainTextureId, mainWidth, mainHeight, x, y, 0, 1.0f, false, false);
}

int GPUOverlay::blendTo(int mainTextureId, int mainWidth, int mainHeight, int x, int y, int overlayRotation, float overlayScale, bool overlayFlipHorizontal, bool overlayFlipVertical)
{
    if (!initialized_) return mainTextureId;
    
    mGPUOverlayMutex.lock();
    if (isOverlayUpdate) {
        isOverlayUpdate = false;
        renderOverlayToFBO(mOverlayImageFrame, overlayRotation, overlayScale, overlayFlipHorizontal, overlayFlipVertical);
    }
    mGPUOverlayMutex.unlock();
    
    if (overlayFrameBufferTexture==OpenGLUtils::NO_TEXTURE) {
        return mainTextureId;
    }
    
    normalBlendFilter->onOutputSizeChanged(mainWidth, mainHeight);
    
    if (!isCreatedFrameBufferObject) {
        createFrameBufferObject(mainWidth, mainHeight, OpenGLUtils::NO_TEXTURE);
        frameBufferWidth = mainWidth;
        frameBufferHeight = mainHeight;
        isCreatedFrameBufferObject = true;
    }
    
    if (isCreatedFrameBufferObject) {
        if (frameBufferWidth!=mainWidth || frameBufferHeight != mainHeight) {
            deleteFrameBufferObject();
            createFrameBufferObject(mainWidth, mainHeight, OpenGLUtils::NO_TEXTURE);
            frameBufferWidth = mainWidth;
            frameBufferHeight = mainHeight;
        }
    }
    
    bind(mainWidth, mainHeight);
    normalBlendFilter->onDrawFrame(mainTextureId, overlayFrameBufferTexture, x+mScaledOverlayWidth/2-overlayFrameBufferWidth/2, y+mScaledOverlayHeight/2-overlayFrameBufferHeight/2, overlayFrameBufferWidth, overlayFrameBufferHeight);
    unBind();
    
    return frameBufferTexture;
}

bool GPUOverlay::blendTo(int mainTextureId, int mainWidth, int mainHeight, int x, int y, int overlayRotation, float overlayScale, bool overlayFlipHorizontal, bool overlayFlipVertical, int outputTextureId)
{
    if (outputTextureId == OpenGLUtils::NO_TEXTURE) return false;
    
    if (!initialized_) return false;
    
    mGPUOverlayMutex.lock();
    if (isOverlayUpdate) {
        isOverlayUpdate = false;
        renderOverlayToFBO(mOverlayImageFrame, overlayRotation, overlayScale, overlayFlipHorizontal, overlayFlipVertical);
    }
    mGPUOverlayMutex.unlock();
    
    if (overlayFrameBufferTexture==OpenGLUtils::NO_TEXTURE) {
        return false;
    }
    
    normalBlendFilter->onOutputSizeChanged(mainWidth, mainHeight);
    
    if (!isCreatedFrameBufferObject) {
        createFrameBufferObject(mainWidth, mainHeight, outputTextureId);
        isCreatedFrameBufferObject = true;
    }
    
    bind(mainWidth, mainHeight);
    normalBlendFilter->onDrawFrame(mainTextureId, overlayFrameBufferTexture, x+mScaledOverlayWidth/2-overlayFrameBufferWidth/2, y+mScaledOverlayHeight/2-overlayFrameBufferHeight/2, overlayFrameBufferWidth, overlayFrameBufferHeight);
    unBind();
    
    return true;
}
