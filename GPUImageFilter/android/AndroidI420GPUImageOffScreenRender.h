//
//  AndroidI420GPUImageOffScreenRender.h
//  AndroidMediaStreamer
//
//  Created by Think on 2017/3/2.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef AndroidI420GPUImageOffScreenRender_h
#define AndroidI420GPUImageOffScreenRender_h

#include <stdio.h>

extern "C" {
#include "libavformat/avformat.h"
}

#include "jni.h"
#include <EGL/egl.h>

#include "MediaDataType.h"

#include "GPUImageI420InputFilter.h"

#include "GPUImageRGBFilter.h"
#include "GPUImageSketchFilter.h"
#include "GPUImageAmaroFilter.h"
#include "GPUImageAntiqueFilter.h"
#include "GPUImageBlackCatFilter.h"
#include "GPUImageBeautyFilter.h"
#include "GPUImageBrannanFilter.h"
#include "GPUImageN1977Filter.h"
#include "GPUImageBrooklynFilter.h"
#include "GPUImageCoolFilter.h"
#include "GPUImageCrayonFilter.h"

#include "GPUImageNormalBlendFilter.h"
#include "GPUImageRawPixelOutputFilter.h"
#include "GPUImageTransformFilter.h"

#include "GPUImageOffScreenRender.h"

#include "TextureRotationUtil.h"

#include <map>

class AndroidI420GPUImageOffScreenRender : public GPUImageOffScreenRender {
public:
    AndroidI420GPUImageOffScreenRender();
    ~AndroidI420GPUImageOffScreenRender();
    
    bool initialize(int displayWidth, int displayHeight);
    void terminate();
    bool isInitialized();
    
    VideoFrame* render(AVFrame *inputVideoFrame);
    void render(AVFrame *inputVideoFrame, VideoFrame* outputVideoFrame);
    
    void setFilter(FILTER_TYPE filter_type, char* filter_dir, float strength);
    void removeFilter(FILTER_TYPE filter_type);
    
    int filterInputToFrameBufferTexture(AVFrame *inputVideoFrame, FILTER_TYPE filterType, char* filterDir, bool isAspectFit);
    int normalBlendToFrameBufferTexture(VideoFrame* overlay, int rotation, float scale, bool flipHorizontal, bool flipVertical, int x, int y);
    int normalBlendToFrameBufferTexture(VideoFrame* textBackGroundLay, int rotation, float scale, bool flipHorizontal, bool flipVertical, int x, int y, TextInfo* textInfo);
    void pumpFrameBuffer(VideoFrame* outputVideoFrame);
    VideoFrame* pumpFrameBuffer();
    
private:
    bool initialized_;
    
    bool Egl_Initialize(int displayWidth, int displayHeight);
    void Egl_Terminate();
    
    EGLDisplay _display;
    EGLSurface _surface;
    EGLContext _context;
    
    int offScreenDisPlayWidth;
    int offScreenDisplayHeight;
    
    GPUImageI420InputFilter *i420InputFilter;
    GPUImageFilter *workFilter;
    GPUImageNormalBlendFilter *normalBlendFilter;
    GPUImageRawPixelOutputFilter *outputFilter;
    
    int outputWidth;
    int outputHeight;
    bool isOutputSizeUpdated;
    
    GPUImageRotationMode rotationModeForWorkFilter;
    float* textureCoordinates;
    
private:
    float* mGLCubeBuffer;

    void ScaleAspectFill(GPUImageRotationMode rotationMode, int displayWidth, int displayHeight, int videoWidth, int videoHeight);
    void ScaleAspectFit(GPUImageRotationMode rotationMode, int displayWidth, int displayHeight, int videoWidth, int videoHeight);

private:
    GLuint frameBuffer[2];
    GLuint frameBufferTexture[2];
    int fboIndex;
    
    void createFrameBufferObject(int frameWidth, int frameHeight, int fbo_index);
    void deleteFrameBufferObject(int fbo_index);
    void bind(int frameWidth, int frameHeight, int fbo_index);
    void unBind();
    
private:
    FILTER_TYPE mCurrentFilterType;
    
    //just for overlay
private:
    int renderOverlayToFBO(VideoFrame* overlay, int rotation, float scale, bool flipHorizontal, bool flipVertical);
    GPUImageTransformFilter *overlayTransformFilter;
    float* overlayTransform;
    float* overlayTextureCoordinates;
    
    GLuint overlayFrameBuffer;
    GLuint overlayFrameBufferTexture;
    
    int overlayFrameBufferWidth;
    int overlayFrameBufferHeight;
    
    //Create Overlay FBO
    void createOverlayFBO(int frameWidth, int frameHeight);
    //Delete Overlay FBO
    void deleteOverlayFBO();
    bool isCreatedOverlayFBO;

    //for text and background overlay
private:
    //input and flip TextBackGroundLay
    int flipTextBackGroundLayToFBO(VideoFrame* textBackGroundLay, bool flipHorizontal, bool flipVertical);
    //blend all words to TextBackGroundLay
    int normalBlendWordToTextBackGroundLayFBO(WordInfo* wordInfo);
    //transform TextBackGroundLay
    int transformTextBackGroundLayFBO(int rotation, float scale);
    
    GLuint textBackGroundLayFrameBuffer[2];
    GLuint textBackGroundLayFrameBufferTexture[2];
    int textBackGroundLayFBOIndex;
    void createTextBackGroundLayFrameBufferObject(int frameWidth, int frameHeight, int fbo_index);
    void deleteTextBackGroundLayFrameBufferObject(int fbo_index);
    bool isCreatedTextBackGroundLayFrameBufferObject;
    int textBackGroundLayFrameBufferWidth;
    int textBackGroundLayFrameBufferHeight;
    
    //input and flip TextBackGroundLay
    //    GLuint inputTextBackGroundLayTexture;
    float* inputTextBackGroundLayTextureCoordinates;
    GPUImageRGBFilter *inputTextBackGroundLayFilter;
    
    //blend all words to TextBackGroundLay
    //    GLuint wordBitmapTexture;
    GPUImageNormalBlendFilter *normalBlendWordFilter;
    
    //transform TextBackGroundLay
    float* textBackGroundLayTransform;
    GPUImageTransformFilter *transformTextBackGroundLayFilter;
    
    GLuint transformedTextBackGroundLayFrameBuffer;
    GLuint transformedTextBackGroundLayFrameBufferTexture;
    void createTransformedTextBackGroundLayFBO(int frameWidth, int frameHeight);
    void deleteTransformedTextBackGroundLayFBO();
    bool isCreatedTransformedTextBackGroundLayFBO;
    int transformedTextBackGroundLayFrameBufferWidth;
    int transformedTextBackGroundLayFrameBufferHeight;
    float* transformedTextBackGroundLayTextureCoordinates;
    
//just for filter group
private:
    GLuint mFGFrameBuffers[2];
    GLuint mFGFrameBufferTextures[2];
    int mFGFrameBufferWidth;
    int mFGFrameBufferHeight;
    bool updateFGFramebuffers(int width, int height);
    void destroyFGFramebuffers();
    bool isFGFrameBuffersCreated;
    float* mFGFrameBufferGLVertexPositionCoordinates;
    float* mFGFrameBufferGLTextureCoordinates;
    std::map<FILTER_TYPE, GPUImageFilter*> mFilters;
    int drawFilter(int inputFrameBufferTexture, int inputFrameBufferWidth, int inputFrameBufferHeight);
};

#endif /* AndroidI420GPUImageOffScreenRender_h */
