//
//  GPUImageI420InputFilter.h
//  MediaPlayer
//
//  Created by Think on 2017/2/15.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef GPUImageI420InputFilter_h
#define GPUImageI420InputFilter_h

#include "OpenGLUtils.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

struct I420GPUImage {
    uint8_t* y_plane;
    uint8_t* u_plane;
    uint8_t* v_plane;
    
    int width;
    int height;
    
    int y_stride;
    int u_stride;
    int v_stride;
    
    int rotation;
    
    I420GPUImage()
    {
        y_plane = NULL;
        u_plane = NULL;
        v_plane = NULL;
        
        width = 0;
        height = 0;
        
        y_stride = 0;
        u_stride = 0;
        v_stride = 0;
        
        rotation = 0;
    }
};

class GPUImageI420InputFilter {
public:
    GPUImageI420InputFilter();
    ~GPUImageI420InputFilter();
    
    void init();
    void destroy();
    
    int onDrawToTexture(const I420GPUImage& frame);
    
    int getOutputFrameBufferWidth();
    int getOutputFrameBufferHeight();
private:
    // Initialize the textures by the frame width and height
    void SetupTextures(const I420GPUImage& frame);
    
    // Update the textures by the YUV data from the frame
    void UpdateTextures(const I420GPUImage& frame);
    
    void UnbindTextures();
    
    //Create FBO
    void createFBO(int frameWidth, int frameHeight);
    
    //Delete FBO
    void deleteFBO();
private:
    static const char vertext_shader_[];
    static const char fragment_shader_[];
    
    GLuint texture_ids_[3];  // Texture id of Y,U and V texture.

    GLuint program_;
    
    int attribPositionHandle_;
    int attribTextureCoordinateHandle_;
    
    int uniformYtexhandle_;
    int uniformUtexhandle_;
    int uniformVtexhandle_;
    
    GLsizei output_framebuffer_width_;
    GLsizei output_framebuffer_height_;
    bool isOutputFramebufferSizeChanged;
    
    GLuint frameBuffer_;
    GLuint frameBufferTexture_;
    
    bool isCreateFBO;
    
    float* textureCoordinates;
};

#endif /* GPUImageI420InputFilter_h */
