//
//  TextureRotationUtil.hpp
//  MediaPlayer
//
//  Created by Think on 2017/2/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef TextureRotationUtil_h
#define TextureRotationUtil_h

#include <stdio.h>

#define GPUImageRotationSwapsWidthAndHeight(rotation) ((rotation) == kGPUImageRotateLeft || (rotation) == kGPUImageRotateRight || (rotation) == kGPUImageRotateRightFlipVertical || (rotation) == kGPUImageRotateRightFlipHorizontal)

enum GPUImageRotationMode {
    kGPUImageNoRotation,
    kGPUImageRotateLeft,
    kGPUImageRotateRight,
    kGPUImageFlipVertical,
    kGPUImageFlipHorizonal,
    kGPUImageRotateRightFlipVertical,
    kGPUImageRotateRightFlipHorizontal,
    kGPUImageRotate180
};

class TextureRotationUtil {
public:
    static const float CUBE[];
    
    static void calculateCropTextureCoordinates(GPUImageRotationMode inputRotation, float minX, float minY, float maxX, float maxY, float* cropTextureCoordinates);
};

#endif /* TextureRotationUtil_h */
