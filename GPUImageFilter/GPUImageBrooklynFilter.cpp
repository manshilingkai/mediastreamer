//
//  GPUImageBrooklynFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2017/8/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageBrooklynFilter.h"
#include "ImageProcesserUtils.h"
#include "StringUtils.h"
#include "OpenGLUtils.h"

const char GPUImageBrooklynFilter::BROOKLYN_FRAGMENT_SHADER[] = {
    "precision mediump float;\n"
    "varying mediump vec2 textureCoordinate;\n"
    "uniform sampler2D inputImageTexture;\n"
    "uniform sampler2D inputImageTexture2;\n"
    "uniform sampler2D inputImageTexture3;\n"
    "uniform sampler2D inputImageTexture4;\n"
    "uniform float strength;\n"
    "float NCGray(vec4 color)\n"
    "{\n"
    "float gray = 0.2125 * color.r + 0.7154 * color.g + 0.0721 * color.b;\n"
    "return gray;\n"
    "}\n"
    "vec4 NCTonemapping(vec4 color)\n"
    "{\n"
    "vec4 mapped;\n"
    "mapped.r = texture2D(inputImageTexture2, vec2(color.r, 0.0)).r;\n"
    "mapped.g = texture2D(inputImageTexture2, vec2(color.g, 0.0)).g;\n"
    "mapped.b = texture2D(inputImageTexture2, vec2(color.b, 0.0)).b;\n"
    "mapped.a = color.a;\n"
    "return mapped;\n"
    "}\n"
    "vec4 NCColorControl(vec4 color, float saturation, float brightness, float contrast)\n"
    "{\n"
    "float gray = NCGray(color);\n"
    "color.rgb = vec3(saturation) * color.rgb + vec3(1.0-saturation) * vec3(gray);\n"
    "color.r = clamp(color.r, 0.0, 1.0);\n"
    "color.g = clamp(color.g, 0.0, 1.0);\n"
    "color.b = clamp(color.b, 0.0, 1.0);\n"
    "color.rgb = vec3(contrast) * (color.rgb - vec3(0.5)) + vec3(0.5);\n"
    "color.r = clamp(color.r, 0.0, 1.0);\n"
    "color.g = clamp(color.g, 0.0, 1.0);\n"
    "color.b = clamp(color.b, 0.0, 1.0);\n"
    "color.rgb = color.rgb + vec3(brightness);\n"
    "color.r = clamp(color.r, 0.0, 1.0);\n"
    "color.g = clamp(color.g, 0.0, 1.0);\n"
    "color.b = clamp(color.b, 0.0, 1.0);\n"
    "return color;\n"
    "}\n"
    "vec4 NCHueAdjust(vec4 color, float hueAdjust)\n"
    "{\n"
    "vec3 kRGBToYPrime = vec3(0.299, 0.587, 0.114);\n"
    "vec3 kRGBToI = vec3(0.595716, -0.274453, -0.321263);\n"
    "vec3 kRGBToQ = vec3(0.211456, -0.522591, 0.31135);\n"
    "vec3 kYIQToR   = vec3(1.0, 0.9563, 0.6210);\n"
    "vec3 kYIQToG   = vec3(1.0, -0.2721, -0.6474);\n"
    "vec3 kYIQToB   = vec3(1.0, -1.1070, 1.7046);\n"
    "float yPrime = dot(color.rgb, kRGBToYPrime);\n"
    "float I = dot(color.rgb, kRGBToI);\n"
    "float Q = dot(color.rgb, kRGBToQ);\n"
    "float hue = atan(Q, I);\n"
    "float chroma  = sqrt (I * I + Q * Q);\n"
    "hue -= hueAdjust;\n"
    "Q = chroma * sin (hue);\n"
    "I = chroma * cos (hue);\n"
    "color.r = dot(vec3(yPrime, I, Q), kYIQToR);\n"
    "color.g = dot(vec3(yPrime, I, Q), kYIQToG);\n"
    "color.b = dot(vec3(yPrime, I, Q), kYIQToB);\n"
    "return color;\n"
    "}\n"
    "vec4 NCColorMatrix(vec4 color, float red, float green, float blue, float alpha, vec4 bias)\n"
    "{\n"
    "color = color * vec4(red, green, blue, alpha) + bias;\n"
    "return color;\n"
    "}\n"
    "vec4 NCMultiplyBlend(vec4 overlay, vec4 base)\n"
    "{\n"
    "vec4 outputColor;\n"
    "float a = overlay.a + base.a * (1.0 - overlay.a);\n"
    "outputColor.rgb = ((1.0-base.a) * overlay.rgb * overlay.a + (1.0-overlay.a) * base.rgb * base.a + overlay.a * base.a * overlay.rgb * base.rgb) / a;\n"
    "outputColor.a = a;\n"
    "return outputColor;\n"
    "}\n"
    "void main()\n"
    "{\n"
    "vec4 originColor = texture2D(inputImageTexture, textureCoordinate);\n"
    "vec4 color = texture2D(inputImageTexture, textureCoordinate);\n"
    "color.a = 1.0;\n"
    "color.r = texture2D(inputImageTexture2, vec2(color.r, 0.0)).r;\n"
    "color.g = texture2D(inputImageTexture2, vec2(color.g, 0.0)).g;\n"
    "color.b = texture2D(inputImageTexture2, vec2(color.b, 0.0)).b;\n"
    "color = NCColorControl(color, 0.88, 0.03, 0.85);\n"
    "color = NCHueAdjust(color, -0.0444);\n"
    "vec4 bg = vec4(0.5647, 0.1961, 0.0157, 0.14);\n"
    "color = NCMultiplyBlend(bg, color);\n"
    "vec4 bg2 = texture2D(inputImageTexture3, textureCoordinate);\n"
    "bg2.a *= 0.9;\n"
    "color = NCMultiplyBlend(bg2, color);\n"
    "color.r = texture2D(inputImageTexture4, vec2(color.r, 0.0)).r;\n"
    "color.g = texture2D(inputImageTexture4, vec2(color.g, 0.0)).g;\n"
    "color.b = texture2D(inputImageTexture4, vec2(color.b, 0.0)).b;\n"
    "color.rgb = mix(originColor.rgb, color.rgb, strength);\n"
    "gl_FragColor = color;\n"
    "}\n"
};

GPUImageBrooklynFilter::GPUImageBrooklynFilter(char* filter_dir)
: GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, BROOKLYN_FRAGMENT_SHADER)

{
    for (int i = 0; i<3; i++) {
        inputTextureHandles[i] = -1;
        inputTextureUniformLocations[i] = -1;
    }
    
    mFilterDir = NULL;
    
    if (filter_dir) {
        mFilterDir = strdup(filter_dir);
    }
}

GPUImageBrooklynFilter::~GPUImageBrooklynFilter()
{
    if (mFilterDir) {
        free(mFilterDir);
        mFilterDir = NULL;
    }
}

void GPUImageBrooklynFilter::onInit()
{
    GPUImageFilter::onInit();
    
    inputTextureUniformLocations[0] = glGetUniformLocation(getProgram(), "inputImageTexture2");
    inputTextureUniformLocations[1] = glGetUniformLocation(getProgram(), "inputImageTexture3");
    inputTextureUniformLocations[2] = glGetUniformLocation(getProgram(), "inputImageTexture4");
}

void GPUImageBrooklynFilter::onInitialized()
{
    GPUImageFilter::onInitialized();
    
    char* brooklynCurves1_str = StringUtils::cat(mFilterDir, "/brooklynCurves1.png");
    VideoFrame *brooklynCurves1_frame = PNGImageFileToRGBAVideoFrame(brooklynCurves1_str);
    free(brooklynCurves1_str);
    
    if (brooklynCurves1_frame) {
        inputTextureHandles[0] = OpenGLUtils::loadTexture(brooklynCurves1_frame->data, brooklynCurves1_frame->width, brooklynCurves1_frame->height, inputTextureHandles[0]);
        if (brooklynCurves1_frame->data) {
            free(brooklynCurves1_frame->data);
            brooklynCurves1_frame->data = NULL;
        }
        delete brooklynCurves1_frame;
        brooklynCurves1_frame = NULL;
    }
    
    char* filter_map_first_str = StringUtils::cat(mFilterDir, "/filter_map_first.png");
    VideoFrame *filter_map_first_frame = PNGImageFileToRGBAVideoFrame(filter_map_first_str);
    free(filter_map_first_str);
    
    if (filter_map_first_frame) {
        inputTextureHandles[1] = OpenGLUtils::loadTexture(filter_map_first_frame->data, filter_map_first_frame->width, filter_map_first_frame->height, inputTextureHandles[1]);
        if (filter_map_first_frame->data) {
            free(filter_map_first_frame->data);
            filter_map_first_frame->data = NULL;
        }
        delete filter_map_first_frame;
        filter_map_first_frame = NULL;
    }
    
    char* brooklynCurves2_str = StringUtils::cat(mFilterDir, "/brooklynCurves2.png");
    VideoFrame *brooklynCurves2_frame = PNGImageFileToRGBAVideoFrame(brooklynCurves2_str);
    free(brooklynCurves2_str);
    
    if (brooklynCurves2_frame) {
        inputTextureHandles[2] = OpenGLUtils::loadTexture(brooklynCurves2_frame->data, brooklynCurves2_frame->width, brooklynCurves2_frame->height, inputTextureHandles[2]);
        if (brooklynCurves2_frame->data) {
            free(brooklynCurves2_frame->data);
            brooklynCurves2_frame->data = NULL;
        }
        delete brooklynCurves2_frame;
        brooklynCurves2_frame = NULL;
    }
}

void GPUImageBrooklynFilter::onDestroy()
{
    GPUImageFilter::onDestroy();
    
    glDeleteTextures(3, inputTextureHandles);
    for(int i = 0; i < 3; i++)
        inputTextureHandles[i] = OpenGLUtils::NO_TEXTURE;
}

void GPUImageBrooklynFilter::onDrawArraysPre()
{
    for(int i = 0; i < 3
        && inputTextureHandles[i] != OpenGLUtils::NO_TEXTURE; i++){
        glActiveTexture(GL_TEXTURE0 + (i+1) );
        glBindTexture(GL_TEXTURE_2D, inputTextureHandles[i]);
        glUniform1i(inputTextureUniformLocations[i], (i+1));
    }
}

void GPUImageBrooklynFilter::onDrawArraysAfter()
{
    for(int i = 0; i < 3
        && inputTextureHandles[i] != OpenGLUtils::NO_TEXTURE; i++){
        glActiveTexture(GL_TEXTURE0 + (i+1));
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
