//
//  GPUImageSharpenFilter.h
//  MediaPlayer
//
//  Created by Think on 2019/12/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef GPUImageSharpenFilter_h
#define GPUImageSharpenFilter_h

#include <stdio.h>
#include "GPUImageFilter.h"

// sharpness: from -4.0 to 4.0, with 0.0 as the normal level

class GPUImageSharpenFilter : public GPUImageFilter {
public:
    GPUImageSharpenFilter();
    GPUImageSharpenFilter(float sharpness);
    ~GPUImageSharpenFilter();
    
    void onOutputSizeChanged(int width, int height);
    
    void setSharpness(float sharpness);
protected:
    void onInit();
private:
    static const char SHARPEN_VERTEX_SHADER[];
    static const char SHARPEN_FRAGMENT_SHADER[];

    int mSharpnessLocation;
    float mSharpness;
    int mImageWidthFactorLocation;
    int mImageHeightFactorLocation;
};

#endif /* GPUImageSharpenFilter_h */
