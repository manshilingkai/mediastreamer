//
//  GPUImageN1977Filter.cpp
//  MediaPlayer
//
//  Created by Think on 2017/8/11.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageN1977Filter.h"
#include "ImageProcesserUtils.h"
#include "StringUtils.h"
#include "OpenGLUtils.h"

const char GPUImageN1977Filter::N1977_FRAGMENT_SHADER[] = {
    "precision mediump float;\n"
    "varying mediump vec2 textureCoordinate;\n"
    "uniform sampler2D inputImageTexture;\n"
    "uniform sampler2D inputImageTexture2;\n"
    "void main()\n"
    "{\n"
    "vec3 texel = texture2D(inputImageTexture, textureCoordinate).rgb;\n"
    "texel = vec3(texture2D(inputImageTexture2, vec2(texel.r, .16666)).r,texture2D(inputImageTexture2, vec2(texel.g, .5)).g,texture2D(inputImageTexture2, vec2(texel.b, .83333)).b);\n"
    "gl_FragColor = vec4(texel, 1.0);\n"
    "}\n"
};

GPUImageN1977Filter::GPUImageN1977Filter(char* filter_dir)
: GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, N1977_FRAGMENT_SHADER)
{
    for (int i = 0; i<1; i++) {
        inputTextureHandles[i] = -1;
        inputTextureUniformLocations[i] = -1;
    }
    
    mFilterDir = NULL;
    
    if (filter_dir) {
        mFilterDir = strdup(filter_dir);
    }
}

GPUImageN1977Filter::~GPUImageN1977Filter()
{
    if (mFilterDir) {
        free(mFilterDir);
        mFilterDir = NULL;
    }
}

void GPUImageN1977Filter::onInit()
{
    GPUImageFilter::onInit();

    inputTextureUniformLocations[0] = glGetUniformLocation(getProgram(), "inputImageTexture2");

}

void GPUImageN1977Filter::onInitialized()
{
    GPUImageFilter::onInitialized();
    
    char* n1977map_str = StringUtils::cat(mFilterDir, "/n1977map.png");
    VideoFrame *n1977map_frame = PNGImageFileToRGBAVideoFrame(n1977map_str);
    free(n1977map_str);
    
    if (n1977map_frame) {
        inputTextureHandles[0] = OpenGLUtils::loadTexture(n1977map_frame->data, n1977map_frame->width, n1977map_frame->height, inputTextureHandles[0]);
        if (n1977map_frame->data) {
            free(n1977map_frame->data);
            n1977map_frame->data = NULL;
        }
        delete n1977map_frame;
        n1977map_frame = NULL;
    }
}

void GPUImageN1977Filter::onDestroy()
{
    GPUImageFilter::onDestroy();
    
    glDeleteTextures(1, inputTextureHandles);
    for(int i = 0; i < 1; i++)
        inputTextureHandles[i] = OpenGLUtils::NO_TEXTURE;
}

void GPUImageN1977Filter::onDrawArraysPre()
{
    for(int i = 0; i < 1
        && inputTextureHandles[i] != OpenGLUtils::NO_TEXTURE; i++){
        glActiveTexture(GL_TEXTURE0 + (i+1) );
        glBindTexture(GL_TEXTURE_2D, inputTextureHandles[i]);
        glUniform1i(inputTextureUniformLocations[i], (i+1));
    }
}

void GPUImageN1977Filter::onDrawArraysAfter()
{
    for(int i = 0; i < 1
        && inputTextureHandles[i] != OpenGLUtils::NO_TEXTURE; i++){
        glActiveTexture(GL_TEXTURE0 + (i+1));
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
