//
//  GPUImageOffScreenRender.cpp
//  MediaStreamer
//
//  Created by Think on 2017/3/2.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "GPUImageOffScreenRender.h"

#ifdef IOS
#include "iOSGPUImageOffScreenRender.h"
#endif

#ifdef ANDROID
#include "AndroidI420GPUImageOffScreenRender.h"
#endif

GPUImageOffScreenRender* GPUImageOffScreenRender::CreateGPUImageOffScreenRender(OFF_SCREEN_RENDER_TYPE type, OFF_SCREEN_RENDER_INPUT_TYPE inputType)
{
    if (type == GPU_IMAGE_FILTER) {
#ifdef IOS
        return new iOSGPUImageOffScreenRender(inputType);
#endif

#ifdef ANDROID
        if (inputType==I420) {
            return new AndroidI420GPUImageOffScreenRender();
        }
#endif
    }
    return NULL;
}

void GPUImageOffScreenRender::DeleteGPUImageOffScreenRender(GPUImageOffScreenRender* offScreenRender, OFF_SCREEN_RENDER_TYPE type)
{
    if (type == GPU_IMAGE_FILTER) {
#ifdef IOS
        iOSGPUImageOffScreenRender *GPUImageOffScreenRender = (iOSGPUImageOffScreenRender *)offScreenRender;
#endif
#ifdef ANDROID
        AndroidI420GPUImageOffScreenRender *GPUImageOffScreenRender = (AndroidI420GPUImageOffScreenRender*)offScreenRender;
#endif
        
        if (GPUImageOffScreenRender!=NULL) {
            delete GPUImageOffScreenRender;
            GPUImageOffScreenRender = NULL;
        }
        
    }
}
