//
//  GPUImageContrastFilter.cpp
//  MediaPlayer
//
//  Created by Think on 2019/12/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "GPUImageContrastFilter.h"

const char GPUImageContrastFilter::CONTRAST_FRAGMENT_SHADER[] = {
    "varying highp vec2 textureCoordinate;\n"
    " \n"
    " uniform sampler2D inputImageTexture;\n"
    " uniform lowp float contrast;\n"
    " \n"
    " void main()\n"
    " {\n"
    "     lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);\n"
    "     \n"
    "     gl_FragColor = vec4(((textureColor.rgb - vec3(0.5)) * contrast + vec3(0.5)), textureColor.w);\n"
    " }"
};

GPUImageContrastFilter::GPUImageContrastFilter()
    : GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, CONTRAST_FRAGMENT_SHADER)
{
    mContrast = 1.0f;
}

GPUImageContrastFilter::GPUImageContrastFilter(float contrast)
    : GPUImageFilter(GPUImageFilter::NO_FILTER_VERTEX_SHADER, CONTRAST_FRAGMENT_SHADER)
{
    mContrast = contrast;
}

GPUImageContrastFilter::~GPUImageContrastFilter()
{
    
}

void GPUImageContrastFilter::onInit()
{
    GPUImageFilter::onInit();
    mContrastLocation = glGetUniformLocation(getProgram(), "contrast");
}

void GPUImageContrastFilter::onInitialized()
{
    GPUImageFilter::onInitialized();
    setContrast(mContrast);
}

void GPUImageContrastFilter::setContrast(float contrast)
{
    mContrast = contrast;
    setFloat(mContrastLocation, mContrast);
}
