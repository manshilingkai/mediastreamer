//
//  MediaRemuxer.h
//  MediaStreamer
//
//  Created by slklovewyy on 2018/11/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef MediaRemuxer_h
#define MediaRemuxer_h

#include <stdio.h>

#include <stdio.h>
#include <pthread.h>

#include "MediaMerger.h"
#include "IMediaListener.h"

extern "C" {
#include "libavformat/avformat.h"
}

#include "AVPacketQueue.h"

class MediaRemuxer : public MediaMerger{
public:
#ifdef ANDROID
    MediaRemuxer(JavaVM *jvm, MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup ,MediaProduct* outputProduct);
#else
    MediaRemuxer(MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup ,MediaProduct* outputProduct);
#endif
    
    ~MediaRemuxer();
    
#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#endif
    
#ifdef IOS
    void setListener(void (*listener)(void*,int,int,int), void* arg);
#endif
    
    void notify(int event, int ext1, int ext2);
    
    void start();
    
    void resume();
    void pause();
    
    void stop();
    
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    pthread_mutex_t mMainLock;
    
    IMediaListener *mMediaListener;
    
    bool isTheadLive;
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    void createRemuxerThread();
    static void* handleRemuxerThread(void* ptr);
    void remuxerThreadMain();
    void deleteRemuxerThread();
    
    bool isRemuxing;
    bool isBreakThread;
    
private:
    bool open_all_piplelines();
    void close_all_piplelines();
    
    char *in_filename;
    char *out_filename;
    
    AVFormatContext *ifmt_ctx;
    int mInAudioStreamIndex;
    int mInVideoStreamIndex;
    
    AVFormatContext *ofmt_ctx;
    int mOutputAudioStreamIndex;
    int mOutputVideoStreamIndex;
    
private:
    AVPacketQueue mAudioPacketQueue;
    AVPacketQueue mVideoPacketQueue;
};

#endif /* MediaRemuxer_h */
