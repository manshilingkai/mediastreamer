//
//  MediaDefaultSpliter.cpp
//  MediaStreamer
//
//  Created by Think on 2016/12/5.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include <string.h>
#include <stdlib.h>

#include "AutoLock.h"
#include "MediaLog.h"
#include "MediaDefaultSpliter.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#endif

#ifdef IOS
#include "iOSMediaListener.h"
#endif

struct MediaDefaultSpliterEvent : public TimedEventQueue::Event {
    MediaDefaultSpliterEvent(
                       MediaDefaultSpliter *spliter,
                       void (MediaDefaultSpliter::*method)())
    : mSpliter(spliter),
    mMethod(method) {
    }
    
protected:
    virtual ~MediaDefaultSpliterEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mSpliter->*mMethod)();
    }
    
private:
    MediaDefaultSpliter *mSpliter;
    void (MediaDefaultSpliter::*mMethod)();
    
    MediaDefaultSpliterEvent(const MediaDefaultSpliterEvent &);
    MediaDefaultSpliterEvent &operator=(const MediaDefaultSpliterEvent &);
};

#ifdef ANDROID
MediaDefaultSpliter::MediaDefaultSpliter(JavaVM *jvm, MediaMaterial *inputMediaMaterial, MediaProductGroup* outputProductGroup)
{
    mJvm = jvm;
    
    mInputMediaMaterial.url = strdup(inputMediaMaterial->url);
    mInputMediaMaterial.startPos = inputMediaMaterial->startPos;
    mInputMediaMaterial.endPos = inputMediaMaterial->endPos;
    mInputMediaMaterial.type = inputMediaMaterial->type;
    mInputMediaMaterial.weight = inputMediaMaterial->weight;
    
    mOutputProductGroup.mediaProductNum = outputProductGroup->mediaProductNum;
    
    for (int i=0; i<mOutputProductGroup.mediaProductNum; i++) {
        mOutputProductGroup.mediaProducts[i]->url = strdup(outputProductGroup->mediaProducts[i]->url);
        mOutputProductGroup.mediaProducts[i]->videoOptions = outputProductGroup->mediaProducts[i]->videoOptions;
        mOutputProductGroup.mediaProducts[i]->audioOptions = outputProductGroup->mediaProducts[i]->audioOptions;
    }
    
    mAsyncPrepareEvent = new MediaDefaultSpliterEvent(this, &MediaDefaultSpliter::onPrepareAsyncEvent);
    mProcessOnePacketEvent = new MediaDefaultSpliterEvent(this, &MediaDefaultSpliter::onProcessOnePacketEvent);
    mStopEvent = new MediaDefaultSpliterEvent(this, &MediaDefaultSpliter::onStopEvent);
    mNotifyEvent = new MediaDefaultSpliterEvent(this, &MediaDefaultSpliter::onNotifyEvent);
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mStopCondition, NULL);
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();
    
    mFlags = 0;
    
    mFFmpegReader = NULL;
    mVideoDecoder = NULL;
    mAudioDecoder = NULL;
    mColorSpaceConvert = NULL;
    mAudioFilter = NULL;
    mVideoEncoder = NULL;
    mAudioEncoder = NULL;
    mMediaMuxer = NULL;
    mAudioPCMDataPool = NULL;
    
    mProductIndex = -1;
    is_open_all_target_pipelines = false;
}
#else
MediaDefaultSpliter::MediaDefaultSpliter(MediaMaterial *inputMediaMaterial, MediaProductGroup* outputProductGroup)
{
    mInputMediaMaterial.url = strdup(inputMediaMaterial->url);
    mInputMediaMaterial.startPos = inputMediaMaterial->startPos;
    mInputMediaMaterial.endPos = inputMediaMaterial->endPos;
    mInputMediaMaterial.type = inputMediaMaterial->type;
    mInputMediaMaterial.weight = inputMediaMaterial->weight;
    
    mOutputProductGroup.mediaProductNum = outputProductGroup->mediaProductNum;
    
    for (int i=0; i<mOutputProductGroup.mediaProductNum; i++) {
        mOutputProductGroup.mediaProducts[i] = new MediaProduct;
        mOutputProductGroup.mediaProducts[i]->url = strdup(outputProductGroup->mediaProducts[i]->url);
        mOutputProductGroup.mediaProducts[i]->videoOptions = outputProductGroup->mediaProducts[i]->videoOptions;
        mOutputProductGroup.mediaProducts[i]->audioOptions = outputProductGroup->mediaProducts[i]->audioOptions;
    }
    
    mAsyncPrepareEvent = new MediaDefaultSpliterEvent(this, &MediaDefaultSpliter::onPrepareAsyncEvent);
    mProcessOnePacketEvent = new MediaDefaultSpliterEvent(this, &MediaDefaultSpliter::onProcessOnePacketEvent);
    mStopEvent = new MediaDefaultSpliterEvent(this, &MediaDefaultSpliter::onStopEvent);
    mNotifyEvent = new MediaDefaultSpliterEvent(this, &MediaDefaultSpliter::onNotifyEvent);
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mStopCondition, NULL);
    
    mQueue.start();
    
    mFlags = 0;
    
    mFFmpegReader = NULL;
    mVideoDecoder = NULL;
    mAudioDecoder = NULL;
    mColorSpaceConvert = NULL;
    mAudioFilter = NULL;
    mVideoEncoder = NULL;
    mAudioEncoder = NULL;
    mMediaMuxer = NULL;
    mAudioPCMDataPool = NULL;

    mProductIndex = -1;
    is_open_all_target_pipelines = false;
}
#endif

MediaDefaultSpliter::~MediaDefaultSpliter()
{
    mQueue.stop(true);
    
    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
    
    if (mProcessOnePacketEvent!=NULL) {
        delete mProcessOnePacketEvent;
        mProcessOnePacketEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }

    if (mInputMediaMaterial.url!=NULL) {
        free(mInputMediaMaterial.url);
        mInputMediaMaterial.url = NULL;
    }
    
    mOutputProductGroup.Free();
    
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mStopCondition);
}

#ifdef ANDROID
void MediaDefaultSpliter::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    modifyFlags(INITIALIZED, SET);
    
}
#endif

#ifdef IOS
void MediaDefaultSpliter::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new iOSMediaListener(listener,arg);
    
    modifyFlags(INITIALIZED, SET);
}
#endif

void MediaDefaultSpliter::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void MediaDefaultSpliter::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
    
    switch (event) {
        case MEDIA_PROCESSER_ERROR:
            notifyListener_l(event, ext1, ext2);
            stop_l();
            break;
        case MEDIA_PROCESSER_INFO:
            notifyListener_l(event, ext1, ext2);
            break;
            
        default:
            notifyListener_l(event, ext1, ext2);
            break;
    }
}

void MediaDefaultSpliter::start()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & CONNECTING) {
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    return prepareAsync_l();
}

void MediaDefaultSpliter::prepareAsync_l()
{
    modifyFlags(END, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(CONNECTING, SET);
    
    mQueue.postEvent(mAsyncPrepareEvent);
}

void MediaDefaultSpliter::onPrepareAsyncEvent()
{
    AutoLock autoLock(&mLock);
    
    bool ret = open_all_source_pipelines_l();
    
    if (ret) {
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(CONNECTED, SET);
        
        play_l();
    }
}

void MediaDefaultSpliter::resume()
{
    AutoLock autoLock(&mLock);
    
    play_l();
}

void MediaDefaultSpliter::play_l()
{
    if (mFlags & STREAMING) {
        return;
    }
    
    if (!(mFlags & CONNECTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    modifyFlags(CONNECTED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
    
    modifyFlags(STREAMING, SET);
}

void MediaDefaultSpliter::pause()
{
    AutoLock autoLock(&mLock);
    
    pause_l();
}

void MediaDefaultSpliter::pause_l()
{
    if (!(mFlags & STREAMING)) {
        return;
    }
    
    modifyFlags(STREAMING, CLEAR);
    
    mQueue.cancelEvent(mProcessOnePacketEvent->eventID());
    
    modifyFlags(PAUSED, SET);
    
    return;
}

void MediaDefaultSpliter::stop()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & END) {
        return;
    }
    
    if (mFlags & ENDING) {
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
}

void MediaDefaultSpliter::stop_l()
{
    modifyFlags(ENDING, ASSIGN);
    
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void MediaDefaultSpliter::onStopEvent()
{
    AutoLock autoLock(&mLock);
    
    close_all_source_pipelines_l();
    
    cancelSpliterEvents();
    
    mNotificationQueue.flush();
    
    modifyFlags(END, ASSIGN);
    
    notifyListener_l(MEDIA_PROCESSER_END);
    
    pthread_cond_broadcast(&mStopCondition);
}

void MediaDefaultSpliter::notifyListener_l(int event, int ext1, int ext2)
{
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void MediaDefaultSpliter::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

void MediaDefaultSpliter::cancelSpliterEvents()
{
    mQueue.cancelEvent(mProcessOnePacketEvent->eventID());
    mQueue.cancelEvent(mNotifyEvent->eventID());
    mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
    mQueue.cancelEvent(mStopEvent->eventID());
}

bool MediaDefaultSpliter::open_all_source_pipelines_l()
{
    //open input file
    mFFmpegReader = new FFmpegReader(mInputMediaMaterial.url);
    bool ret = mFFmpegReader->open();
    if (!ret) {
        notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
        stop_l();
        
        return false;
    }
    
    //open video decoder
    AVStream* videoStreamContext = mFFmpegReader->getVideoStreamContext();
    if (videoStreamContext!=NULL) {
        mVideoDecoderType = VIDEO_DECODER_FFMPEG;
#ifdef ANDROID
        if(mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA)
        {
            mVideoDecoder = VideoDecoder::CreateVideoDecoderWithJniEnv(mVideoDecoderType, mJvm, NULL);
        }else{
            mVideoDecoder = VideoDecoder::CreateVideoDecoder(mVideoDecoderType);
        }
#else
        mVideoDecoder = VideoDecoder::CreateVideoDecoder(mVideoDecoderType);
#endif
        
        if (!mVideoDecoder->open(videoStreamContext)) {
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_VIDEO_DECODER_FAIL, 0);
            stop_l();
            return false;
        }
    }
    
    //open audio decoder
    AVStream* audioStreamContext = mFFmpegReader->getAudioStreamContext();
    if (audioStreamContext!=NULL) {
        mAudioDecoder = AudioDecoder::CreateAudioDecoder(AUDIO_DECODER_FFMPEG);
        if (!mAudioDecoder->open(audioStreamContext)) {
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_AUDIO_DECODER_FAIL, 0);
            stop_l();
            return false;
        }
    }
    
    return true;
}

void MediaDefaultSpliter::close_all_source_pipelines_l()
{
    if (mAudioDecoder!=NULL) {
        mAudioDecoder->dispose();
        AudioDecoder::DeleteAudioDecoder(mAudioDecoder, AUDIO_DECODER_FFMPEG);
        mAudioDecoder = NULL;
    }
    
    if (mVideoDecoder!=NULL) {
        mVideoDecoder->dispose();
        VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
        mVideoDecoder = NULL;
    }
    
    if (mFFmpegReader!=NULL) {
        mFFmpegReader->close();
        delete mFFmpegReader;
        mFFmpegReader = NULL;
    }
}

bool MediaDefaultSpliter::open_all_target_pipelines_l(MediaProduct *outputProduct)
{
    AVStream* videoStreamContext = mFFmpegReader->getVideoStreamContext();
    AVStream* audioStreamContext = mFFmpegReader->getAudioStreamContext();
    
    //open video preprocess
    if (videoStreamContext!=NULL) {
        mColorSpaceConvert = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
    }
    
    //open audio preprocess
    if (audioStreamContext!=NULL) {
        mAudioFilter = AudioFilter::CreateAudioFilter(AUDIO_FILTER_FFMPEG);
        
        bool ret = mAudioFilter->open("atempo=1.0", audioStreamContext->codec->channel_layout, audioStreamContext->codec->channels, audioStreamContext->codec->sample_rate, audioStreamContext->codec->sample_fmt, AV_CH_LAYOUT_MONO, outputProduct->audioOptions.audioNumChannels, outputProduct->audioOptions.audioSampleRate, AV_SAMPLE_FMT_S16);
        
        if (!ret) {
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_AUDIO_PREPROCESS_FAIL, 0);
            stop_l();
            return false;
        }
    }
    
    //open video encoder
    if (videoStreamContext!=NULL) {
        bool bRet = false;
#ifdef IOS
        mVideoEncoderType = VIDEO_TOOL_BOX;
        
        mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, outputProduct->videoOptions.videoFps, outputProduct->videoOptions.videoWidth, outputProduct->videoOptions.videoHeight, BASE_LINE, (VIDEO_ENCODE_MODE)outputProduct->videoOptions.encodeMode, outputProduct->videoOptions.videoBitRate, outputProduct->videoOptions.quality, outputProduct->videoOptions.maxKeyFrameIntervalMs, ULTRAFAST, outputProduct->videoOptions.videoBitRate, outputProduct->videoOptions.bStrictCBR, outputProduct->videoOptions.deblockingFilterFactor);
        
        bRet = mVideoEncoder->Open();
#endif
        
#ifdef ANDROID
        if (outputProduct->videoOptions.videoEncoderType==HARD_ENCODER) {
            mVideoEncoderType = MEDIACODEC;
        }else{
            mVideoEncoderType = X264;
        }
        
        mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, outputProduct->videoOptions.videoFps, outputProduct->videoOptions.videoWidth, outputProduct->videoOptions.videoHeight, BASE_LINE, (VIDEO_ENCODE_MODE)outputProduct->videoOptions.encodeMode, outputProduct->videoOptions.videoBitRate, outputProduct->videoOptions.quality, outputProduct->videoOptions.maxKeyFrameIntervalMs, ULTRAFAST, outputProduct->videoOptions.videoBitRate, outputProduct->videoOptions.bStrictCBR, outputProduct->videoOptions.deblockingFilterFactor);
        
        mVideoEncoder->registerJavaVMEnv(mJvm);
        bRet = mVideoEncoder->Open();
#endif
        
        if (!bRet) {
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_VIDEO_ENCODER_FAIL, 0);
            stop_l();
            return false;
        }
    }
    
    //open audio encoder
    if (audioStreamContext!=NULL) {
        bool bRet = false;
#ifdef ANDROID
        mAudioEncoderType = FAAC;
#endif
        
#ifdef IOS
        mAudioEncoderType = AUDIO_TOOL_BOX;
#endif
        
        mAudioEncoder = AudioEncoder::CreateAudioEncoder(mAudioEncoderType, outputProduct->audioOptions.audioSampleRate, outputProduct->audioOptions.audioNumChannels, outputProduct->audioOptions.audioBitRate);
        
        bRet = mAudioEncoder->Open();
        
        if (!bRet) {
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_AUDIO_ENCODER_FAIL, 0);
            stop_l();
            return false;
        }
    }
    
    // open output file
    mMediaMuxer = MediaMuxer::CreateMediaMuxer(FFMPEG_PROCESSER, MP4, outputProduct->url, &outputProduct->videoOptions, &outputProduct->audioOptions);
    mMediaMuxer->setListener(this);
#ifdef ANDROID
    mMediaMuxer->registerJavaVMEnv(mJvm);
#endif
    bool ret = mMediaMuxer->prepare();
    if (!ret) {
        notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_OUTPUT_FILE_FAIL, 0);
        stop_l();
        return false;
    }
    mMediaMuxer->start();
    
    // set VideoEncoder OutputHandler
    if (outputProduct->videoOptions.hasVideo)
    {
        mVideoEncoder->SetOutputHandler(mMediaMuxer);
    }
    
    // set AudioEncoder OutputHandler
    if (outputProduct->audioOptions.hasAudio) {
        mAudioEncoder->SetOutputHandler(mMediaMuxer);
    }
    
    mAudioPCMDataPool = new AudioPCMDataPool(outputProduct->audioOptions.audioSampleRate, outputProduct->audioOptions.audioNumChannels*2, 1000);
    
    return true;
}

void MediaDefaultSpliter::close_all_target_pipelines_l()
{
    if (mVideoEncoder!=NULL) {
        mVideoEncoder->Close();
        VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
        mVideoEncoder = NULL;
    }
    
    if (mAudioEncoder!=NULL) {
        mAudioEncoder->Close();
        AudioEncoder::DeleteAudioEncoder(mAudioEncoder, mAudioEncoderType);
        mAudioEncoder = NULL;
    }
    
    if (mMediaMuxer!=NULL) {
        mMediaMuxer->stop();
        MediaMuxer::DeleteMediaMuxer(mMediaMuxer, FFMPEG_PROCESSER);
        mMediaMuxer = NULL;
    }
    
    if (mAudioFilter!=NULL) {
        mAudioFilter->dispose();
        AudioFilter::DeleteAudioFilter(AUDIO_FILTER_FFMPEG, mAudioFilter);
        mAudioFilter = NULL;
    }
    
    if (mColorSpaceConvert!=NULL) {
        ColorSpaceConvert::DeleteColorSpaceConvert(mColorSpaceConvert, LIBYUV);
        mColorSpaceConvert = NULL;
    }
    
    if (mAudioPCMDataPool!=NULL) {
        delete mAudioPCMDataPool;
        mAudioPCMDataPool = NULL;
    }
}

void MediaDefaultSpliter::onProcessOnePacketEvent()
{
    AutoLock autoLock(&mLock);
    
    if (!is_open_all_target_pipelines) {
        mProductIndex++;
        
        if (mProductIndex>=mOutputProductGroup.mediaProductNum) {
            LOGD("ENDING...");
            
            stop_l();
            
            return;
        }
        
        bool ret = open_all_target_pipelines_l(mOutputProductGroup.mediaProducts[mProductIndex]);
        
        if (!ret) return;
        else {
            is_open_all_target_pipelines = true;
        }
    }
    
    processVideoOrAudioPacket();
}

void MediaDefaultSpliter::processVideoOrAudioPacket()
{
    if (is_open_all_target_pipelines) {
        
        FFPacket* ffPacket = mFFmpegReader->getMediaPacket();
        
        if (ffPacket->ret==AVERROR_INVALIDDATA || ffPacket->ret == AVERROR(EAGAIN))
        {
            av_packet_unref(ffPacket->avPacket);
            av_freep(ffPacket->avPacket);
            
            mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
            return;
        }else if(ffPacket->ret == AVERROR_EOF)
        {
            av_packet_unref(ffPacket->avPacket);
            av_freep(ffPacket->avPacket);
            
            close_all_target_pipelines_l();
            is_open_all_target_pipelines = false;

            mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
            return;
        }else if (ffPacket->ret < 0)
        {
            av_packet_unref(ffPacket->avPacket);
            av_freep(ffPacket->avPacket);
            
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_READ_INPUT_FILE_FAIL);
            stop_l();
            
            return;
        }else{
            if (ffPacket->mediaType==0) {
                videoFlowing_l(ffPacket->avPacket);
            }else if(ffPacket->mediaType==1) {
                audioFlowing_l(ffPacket->avPacket);
            }
        }
    }
}
