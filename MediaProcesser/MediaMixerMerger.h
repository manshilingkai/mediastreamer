//
//  MediaMixerMerger.h
//  MediaStreamer
//
//  Created by Think on 2016/12/1.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaMixerMerger_h
#define MediaMixerMerger_h

#include <stdio.h>

#include "MediaMerger.h"
#include "MediaListener.h"
#include "TimedEventQueue.h"
#include "NotificationQueue.h"

#include "FFmpegReader.h"
#include "AudioDecoder.h"
#include "AudioFilter.h"
#include "AudioEncoder.h"
#include "MediaMuxer.h"

#include "VideoDecoder.h"
#include "VideoFilter.h"
#include "VideoEncoder.h"

#include "AudioPCMDataPool.h"

#include "ColorSpaceConvert.h"

enum MEDIA_MIX_MODE {
    NONE_MIX = -1,
    ONLY_AUDIO_MIX = 0,
    ONLY_VIDEO_MIX = 1,
    VIDEO_AUDIO_MIX = 2,
};

class MediaMixerMerger : public MediaMerger, MediaListener{
    
public:
#ifdef ANDROID
    MediaMixerMerger(JavaVM *jvm, MediaMaterialGroup *inputMediaMaterialGroup, MediaProduct* outputProduct);
#else
    MediaMixerMerger(MediaMaterialGroup *inputMediaMaterialGroup, MediaProduct* outputProduct);
#endif
    
    ~MediaMixerMerger();
    
#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#endif
    
#ifdef IOS
    void setListener(void (*listener)(void*,int,int,int), void* arg);
#endif
    
    void notify(int event, int ext1, int ext2);
    
    void start();
    
    void resume();
    void pause();
    
    void stop();
    
private:
    friend struct MediaMixerMergerEvent;
    
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    MEDIA_MIX_MODE media_mix_mode;
    
    MediaMaterial mMainMediaMaterial;
    MediaMaterial mBackGroundSoundMaterial;
    MediaMaterialGroup mOverlayMediaMaterialGroup;
    VideoOptions mOutputVideoOptions;
    AudioOptions mOutputAudioOptions;
    char* mOutputUrl;
    
    MediaListener *mMediaListener;
    
    MediaMixerMerger(const MediaMixerMerger &);
    MediaMixerMerger &operator=(const MediaMixerMerger &);
    
    pthread_mutex_t mLock;
    pthread_cond_t mStopCondition;
    
    TimedEventQueue mQueue;
    
    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mProcessOnePacketEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mNotifyEvent;
    void onPrepareAsyncEvent();
    void onProcessOnePacketEvent();
    void onStopEvent();
    void onNotifyEvent();
    
    void prepareAsync_l();
    void play_l();
    void pause_l();
    void stop_l();
    
    NotificationQueue mNotificationQueue;
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
    
    void cancelMergerEvents();
    
private:
    bool open_all_input_pipelines_l_for_MainMediaMaterial();
    void close_all_input_pipelines_l_for_MainMediaMaterial();
    FFmpegReader *mFFmpegReaderForMainMediaMaterial;
    AudioDecoder *mAudioDecoderForMainMediaMaterial;
    AudioFilter *mAudioFilterForMainMediaMaterial;
    AudioPCMDataPool *mAudioPCMDataPoolForMainMediaMaterial;
    VideoDecoderType mVideoDecoderTypeForMainMediaMaterial;
    VideoDecoder *mVideoDecoderForMainMediaMaterial;
    VideoFilterSolution mVideoFilterSolutionForMainMediaMaterial;
    VideoFilter *mVideoFilterForMainMediaMaterial;
    int mRotationForMainMediaMaterial;
    
    bool open_all_input_pipelines_l_for_BackGroundSoundMaterial();
    void close_all_input_pipelines_l_for_BackGroundSoundMaterial();
    FFmpegReader *mFFmpegReaderForBackGroundSoundMaterial;
    AudioDecoder *mAudioDecoderForBackGroundSoundMaterial;
    AudioFilter *mAudioFilterForBackGroundSoundMaterial;
    AudioPCMDataPool *mAudioPCMDataPoolForBackGroundSoundMaterial;
    
    bool open_all_input_pipelines_l_for_OverlayMediaMaterialGroup();
    void close_all_input_pipelines_l_for_OverlayMediaMaterialGroup();
    VideoFrame mOverlayVideoFrame;
    
    bool open_all_ouput_pipelines_l();
    void close_all_output_pipelines_l();
    AudioEncoder *mAudioEncoder;
    AUDIO_ENCODER_TYPE mAudioEncoderType;
    MediaMuxer *mMediaMuxer;
    VIDEO_ENCODER_TYPE mVideoEncoderType;
    VideoEncoder *mVideoEncoder;
    
    bool open_all_pipelines_l();
    void close_all_pipelines_l();
    
    ColorSpaceConvert *mColorSpaceConvert;
private:
    AVStream* mAudioStreamContextForMainMediaMaterial;
    AVStream* mVideoStreamContextForMainMediaMaterial;
    
    AVBitStreamFilterContext *mH264Converter;
    
private:
    enum {
        CONNECTING           = 0x01,
        CONNECTED            = 0x02,
        STREAMING            = 0x04,
        PAUSED               = 0x10,
        ERROR                = 0x20,
        END                  = 0x40,
        INITIALIZED          = 0x80,
        ENDING               = 0x100,
    };
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
    
private:
    void audioFlowing_l(AVPacket* audioPacket);
    void videoFlowing_l(AVPacket* videoPacket);
    void remuxVideoPacket(AVPacket* videoPacket);
    bool mixAudioFrame_l(AudioFrame* targetAudioFrame);
    
    void processVideoOrAudioPacket();
    
    int64_t mCurrentVideoPtsUs;
    int64_t mCurrentAudioPtsUs;
    
    void calculateAudioFramePts(AVFrame *audioFrame);
    
    bool mGotFirstVideoFrame;
    int64_t mFirstVideoFramePtsUs;
    int64_t mCurrentVideoPositionUs;
};


#endif /* MediaMixerMerger_h */
