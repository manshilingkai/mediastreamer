//
//  MediaProduct.h
//  MediaStreamer
//
//  Created by Think on 2016/11/21.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaProduct_h
#define MediaProduct_h

#include "MediaDataType.h"

#define MAX_PRODUCT_NUM 64

enum MEDIA_PRODUCT_TYPE {
    MEDIA_PRODUCT_TYPE_UNKNOWN = -1,
    MEDIA_PRODUCT_TYPE_MP4     = 0,
    MEDIA_PRODUCT_TYPE_GIF     = 1,
};

struct MediaProduct {
    int iD;
    
    MEDIA_PRODUCT_TYPE type;
    
    char *url;

    VideoOptions videoOptions;
    AudioOptions audioOptions;
    
    MediaProduct()
    {
        iD = -1;
        type = MEDIA_PRODUCT_TYPE_UNKNOWN;
        url = NULL;
    }
};

struct MediaProductGroup {
    MediaProduct *mediaProducts[MAX_PRODUCT_NUM];
    int mediaProductNum;
    
    MediaProductGroup()
    {
        for(int i = 0; i < MAX_PRODUCT_NUM; i++)
        {
            mediaProducts[i] = NULL;
        }
        
        mediaProductNum = 0;
    }
    
    inline void Clear()
    {
        for(int i = 0; i < MAX_PRODUCT_NUM; i++)
        {
            if(mediaProducts[i]!=NULL)
            {
                delete mediaProducts[i];
                mediaProducts[i] = NULL;
            }
        }
        
        mediaProductNum = 0;
    }
    
    inline void Free()
    {
        for(int i = 0; i < MAX_PRODUCT_NUM; i++)
        {
            if(mediaProducts[i]!=NULL)
            {
                if(mediaProducts[i]->url!=NULL)
                {
                    free(mediaProducts[i]->url);
                    mediaProducts[i]->url = NULL;
                }
                
                delete mediaProducts[i];
                mediaProducts[i] = NULL;
            }
        }
        
        mediaProductNum = 0;
    }
    
};

#endif /* MediaProduct_h */
