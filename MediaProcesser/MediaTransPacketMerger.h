//
//  MediaTransPacketMerger.h
//  MediaStreamer
//
//  Created by Think on 2018/6/13.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef MediaTransPacketMerger_h
#define MediaTransPacketMerger_h

#include <stdio.h>
#include <pthread.h>

#include "MediaMerger.h"
#include "IMediaListener.h"

extern "C" {
#include "libavformat/avformat.h"
}

struct MediaTransPacketStreamMapping {
    int stream_index;
    enum AVMediaType stream_type;

    int64_t last_pts;
    int64_t last_dts;
    
    MediaTransPacketStreamMapping()
    {
        stream_index = -1;
        stream_type = AVMEDIA_TYPE_UNKNOWN;
        
        last_pts = 0;
        last_dts = 0;
    }
};

class MediaTransPacketMerger : public MediaMerger{
public:

#ifdef ANDROID
    MediaTransPacketMerger(JavaVM *jvm, MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup ,MediaProduct* outputProduct);
#else
    MediaTransPacketMerger(MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup ,MediaProduct* outputProduct);
#endif
    
    ~MediaTransPacketMerger();
    
#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#endif
    
#ifdef IOS
    void setListener(void (*listener)(void*,int,int,int), void* arg);
#endif
    
    void notify(int event, int ext1, int ext2);
    
    void start();
    
    void resume();
    void pause();
    
    void stop();
    
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    pthread_mutex_t mMainLock;

    IMediaListener *mMediaListener;
    
    bool isTheadLive;
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    void createTransPacketThread();
    static void* handleTransPacketThread(void* ptr);
    void transPacketThreadMain();
    void deleteTransPacketThread();
    
    bool isTransPacketing;
    bool isBreakThread;
    
private:
    bool open_all_piplelines();
    void close_all_piplelines();
    
    char *in_filename;
    char *out_filename;
    
    MediaTransPacketStreamMapping* stream_mapping;
    int stream_mapping_size;
    
    AVFormatContext *ifmt_ctx;
    AVFormatContext *ofmt_ctx;
    AVOutputFormat *ofmt;
};

#endif /* MediaTransPacketMerger_h */
