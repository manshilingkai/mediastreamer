//
//  MediaRemuxer.cpp
//  MediaStreamer
//
//  Created by slklovewyy on 2018/11/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "MediaRemuxer.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#endif

#ifdef IOS
#include "iOSMediaListener.h"
#endif

#include "MediaLog.h"

#ifdef ANDROID
MediaRemuxer::MediaRemuxer(JavaVM *jvm, MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup ,MediaProduct* outputProduct)
{
    mJvm = jvm;
    
    pthread_mutex_init(&mMainLock, NULL);
    
    mMediaListener = NULL;
    
    isTheadLive = false;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    isRemuxing = false;
    isBreakThread = false;
    
    if (inputMediaMaterialGroup->mediaMaterialNum>0) {
        in_filename = strdup(inputMediaMaterialGroup->mediaMaterials[0]->url);
    }else{
        in_filename = NULL;
    }
    
    out_filename = strdup(outputProduct->url);
    
    ifmt_ctx = NULL;
    mInAudioStreamIndex = -1;
    mInVideoStreamIndex = -1;
    
    ofmt_ctx = NULL;
    mOutputAudioStreamIndex = -1;
    mOutputVideoStreamIndex = -1;
}
#else
MediaRemuxer::MediaRemuxer(MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup ,MediaProduct* outputProduct)
{
    pthread_mutex_init(&mMainLock, NULL);
    
    mMediaListener = NULL;
    
    isTheadLive = false;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    isRemuxing = false;
    isBreakThread = false;
    
    if (inputMediaMaterialGroup->mediaMaterialNum>0) {
        in_filename = strdup(inputMediaMaterialGroup->mediaMaterials[0]->url);
    }else{
        in_filename = NULL;
    }
    
    out_filename = strdup(outputProduct->url);
    
    ifmt_ctx = NULL;
    mInAudioStreamIndex = -1;
    mInVideoStreamIndex = -1;
    
    ofmt_ctx = NULL;
    mOutputAudioStreamIndex = -1;
    mOutputVideoStreamIndex = -1;
}
#endif

MediaRemuxer::~MediaRemuxer()
{
    if (in_filename) {
        free(in_filename);
        in_filename = NULL;
    }
    
    if (out_filename) {
        free(out_filename);
        out_filename = NULL;
    }
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    pthread_mutex_destroy(&mMainLock);
}

#ifdef ANDROID
void MediaRemuxer::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    pthread_mutex_lock(&mMainLock);
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    pthread_mutex_unlock(&mMainLock);
}
#endif

#ifdef IOS
void MediaRemuxer::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    pthread_mutex_lock(&mMainLock);
    mMediaListener = new iOSMediaListener(listener,arg);
    pthread_mutex_unlock(&mMainLock);
}
#endif


void MediaRemuxer::notify(int event, int ext1, int ext2)
{
}

void MediaRemuxer::start()
{
    pthread_mutex_lock(&mMainLock);
    
    pthread_mutex_lock(&mLock);
    isRemuxing = true;
    pthread_mutex_unlock(&mLock);
    
    if (!isTheadLive) {
        createRemuxerThread();
        isTheadLive = true;
    }
    
    pthread_mutex_unlock(&mMainLock);
}

void MediaRemuxer::resume()
{
    pthread_mutex_lock(&mMainLock);
    
    if (isTheadLive) {
        pthread_mutex_lock(&mLock);
        isRemuxing = true;
        pthread_mutex_unlock(&mLock);
        
        pthread_cond_signal(&mCondition);
    }
    
    pthread_mutex_unlock(&mMainLock);
}

void MediaRemuxer::pause()
{
    pthread_mutex_lock(&mMainLock);
    
    if (isTheadLive) {
        pthread_mutex_lock(&mLock);
        isRemuxing = false;
        pthread_mutex_unlock(&mLock);
        
        pthread_cond_signal(&mCondition);
    }
    
    pthread_mutex_unlock(&mMainLock);
}

void MediaRemuxer::stop()
{
    pthread_mutex_lock(&mMainLock);
    
    if (isTheadLive) {
        this->deleteRemuxerThread();
        isTheadLive = false;
    }
    
    pthread_mutex_lock(&mLock);
    isRemuxing = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_mutex_unlock(&mMainLock);
}

void MediaRemuxer::createRemuxerThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, NULL, handleRemuxerThread, this);
    
    pthread_attr_destroy(&attr);
}

void* MediaRemuxer::handleRemuxerThread(void* ptr)
{
    MediaRemuxer* merger = (MediaRemuxer*)ptr;
    merger->remuxerThreadMain();
    
    return NULL;
}

void MediaRemuxer::remuxerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    bool isIdle = !open_all_piplelines();
    
    int64_t baseLineTimeStampForAudio = 0ll;
    bool isSetBaseLineTimeStampForAudio = false;
    int64_t audioTimeLine = 0ll;
    
    int64_t baseLineTimeStampForVideo = 0ll;
    bool isSetBaseLineTimeStampForVideo = false;
    int64_t videoTimeLine = 0ll;
    
    bool gotVideoEof = false;
    bool gotAudioEof = false;
    
    int64_t lastSendTime = 0ll;
    
    bool isFinishRead = false;
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread)
        {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (isIdle) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        if (!isRemuxing) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        pthread_mutex_unlock(&mLock);
        
        while (!isFinishRead) {

            int64_t cachedVideoDurationUs = 0;
            if (mInVideoStreamIndex>=0) {
                cachedVideoDurationUs = mVideoPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(ifmt_ctx->streams[mInVideoStreamIndex]->time_base);
            }
            int64_t cachedAudioDurationUs = 0;
            if (mInAudioStreamIndex>=0) {
                cachedAudioDurationUs = mAudioPacketQueue.duration(0)*AV_TIME_BASE*av_q2d(ifmt_ctx->streams[mInAudioStreamIndex]->time_base);
            }
            
            int64_t cachedDurationUs;
            if (mInVideoStreamIndex==-1 && mInAudioStreamIndex==-1) {
                cachedDurationUs = 0;
            }else if(mInVideoStreamIndex==-1 && mInAudioStreamIndex>=0) {
                cachedDurationUs = cachedAudioDurationUs;
            }else if(mInVideoStreamIndex>=0 && mInAudioStreamIndex==-1) {
                cachedDurationUs = cachedVideoDurationUs;
            }else {
                cachedDurationUs = cachedVideoDurationUs<cachedAudioDurationUs?cachedVideoDurationUs:cachedAudioDurationUs;
            }
            
            if (cachedDurationUs<0) {
                cachedDurationUs = 0;
            }
            
            if (cachedDurationUs>=5*1000*1000) {
                break;
            }
            
            AVPacket* pPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
            av_init_packet(pPacket);
            pPacket->data = NULL;
            pPacket->size = 0;
            pPacket->flags = 0;
            
            int ret = av_read_frame(ifmt_ctx, pPacket);
            if (ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN)) {
                av_packet_unref(pPacket);
                av_freep(&pPacket);

                continue;
            }else if (ret == AVERROR_EOF) {
                av_packet_unref(pPacket);
                av_freep(&pPacket);
                
                //push the end packet
                AVPacket* videoEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoEndPacket);
                videoEndPacket->duration = 0;
                videoEndPacket->data = NULL;
                videoEndPacket->size = 0;
                videoEndPacket->pts = AV_NOPTS_VALUE;
                videoEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
                mVideoPacketQueue.push(videoEndPacket);
                
                AVPacket* audioEndPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioEndPacket);
                audioEndPacket->duration = 0;
                audioEndPacket->data = NULL;
                audioEndPacket->size = 0;
                audioEndPacket->pts = AV_NOPTS_VALUE;
                audioEndPacket->flags = -3; //if AVPacket's flags = -3, then this is the end packet.
                mAudioPacketQueue.push(audioEndPacket);
                
                isFinishRead = true;
                
                break;
            }else if (ret<0) {
                av_packet_unref(pPacket);
                av_freep(&pPacket);
                
                //push the end packet
                AVPacket* videoErrorPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(videoErrorPacket);
                videoErrorPacket->duration = 0;
                videoErrorPacket->data = NULL;
                videoErrorPacket->size = 0;
                videoErrorPacket->pts = AV_NOPTS_VALUE;
                videoErrorPacket->flags = -1; //if AVPacket's flags = -1, then this is the error packet.
                mVideoPacketQueue.push(videoErrorPacket);
                
                AVPacket* audioErrorPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
                av_init_packet(audioErrorPacket);
                audioErrorPacket->duration = 0;
                audioErrorPacket->data = NULL;
                audioErrorPacket->size = 0;
                audioErrorPacket->pts = AV_NOPTS_VALUE;
                audioErrorPacket->flags = -1; //if AVPacket's flags = -1, then this is the error packet.
                mAudioPacketQueue.push(audioErrorPacket);
                
                isFinishRead = true;
                
                break;
            }else {
                if (pPacket->stream_index == mInVideoStreamIndex) {
                    mVideoPacketQueue.push(pPacket);
                    
                    if (!isSetBaseLineTimeStampForVideo) {
                        isSetBaseLineTimeStampForVideo = true;
                        
                        baseLineTimeStampForVideo = pPacket->pts*AV_TIME_BASE*av_q2d(ifmt_ctx->streams[mInVideoStreamIndex]->time_base);
                        videoTimeLine = 0;
                    }
                }else if (pPacket->stream_index == mInAudioStreamIndex) {
                    mAudioPacketQueue.push(pPacket);
                    
                    if (!isSetBaseLineTimeStampForAudio) {
                        isSetBaseLineTimeStampForAudio = true;
                        
                        baseLineTimeStampForAudio = pPacket->pts*AV_TIME_BASE*av_q2d(ifmt_ctx->streams[mInAudioStreamIndex]->time_base);
                        audioTimeLine = 0;
                    }
                }else{
                    av_packet_unref(pPacket);
                    av_freep(&pPacket);
                }
            }
        }
        
        if (mInAudioStreamIndex>=0 && mInVideoStreamIndex>=0) {
            if (gotAudioEof && gotVideoEof) {
                //Write file trailer
                av_write_trailer(ofmt_ctx);
                
                if (mMediaListener!=NULL) {
                    mMediaListener->notify(MEDIA_PROCESSER_END, 0, 0);
                }
                isIdle = true;
                continue;
            }
        }else if (mInAudioStreamIndex>=0 && mInVideoStreamIndex==-1) {
            if (gotAudioEof) {
                //Write file trailer
                av_write_trailer(ofmt_ctx);
                
                if (mMediaListener!=NULL) {
                    mMediaListener->notify(MEDIA_PROCESSER_END, 0, 0);
                }
                isIdle = true;
                continue;
            }
        }else if (mInAudioStreamIndex==-1 && mInVideoStreamIndex>=0) {
            if (gotVideoEof) {
                //Write file trailer
                av_write_trailer(ofmt_ctx);
                
                if (mMediaListener!=NULL) {
                    mMediaListener->notify(MEDIA_PROCESSER_END, 0, 0);
                }
                isIdle = true;
                continue;
            }
        }
        
        bool justWriteVideoPacket = false;
        if (mInAudioStreamIndex>=0) {
            if (gotAudioEof) {
                if (mInVideoStreamIndex>=0) {
                    justWriteVideoPacket = true;
                }
            }else{
                AVPacket* workAudioPacket = mAudioPacketQueue.pop();
                if (workAudioPacket) {
                    if (workAudioPacket->flags>=0) {
                        AVStream *in_audio_stream = ifmt_ctx->streams[mInAudioStreamIndex];
                        workAudioPacket->stream_index = mOutputAudioStreamIndex;
                        AVStream *out_audio_stream = ofmt_ctx->streams[workAudioPacket->stream_index];
                        workAudioPacket->pts = av_rescale_q_rnd(workAudioPacket->pts, in_audio_stream->time_base, out_audio_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
                        workAudioPacket->dts = av_rescale_q_rnd(workAudioPacket->dts, in_audio_stream->time_base, out_audio_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
                        workAudioPacket->duration = av_rescale_q(workAudioPacket->duration, in_audio_stream->time_base, out_audio_stream->time_base);
                        if (workAudioPacket->duration<0) {
                            workAudioPacket->duration = 0;
                        }
                        workAudioPacket->pos = -1;
                        
                        audioTimeLine = workAudioPacket->pts*AV_TIME_BASE*av_q2d(ifmt_ctx->streams[mInAudioStreamIndex]->time_base) - baseLineTimeStampForAudio;
                        
                        int ret = av_interleaved_write_frame(ofmt_ctx, workAudioPacket);
                        
                        av_packet_unref(workAudioPacket);
                        av_freep(&workAudioPacket);
                        
                        if (ret < 0) {
                            LOGE("Error muxing packet\n");
                            if (mMediaListener!=NULL) {
                                mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
                            }
                            isIdle = true;
                            continue;
                        }
                        
                        if (mInVideoStreamIndex>=0) {
                            while (!gotVideoEof) {
                                AVPacket* topVideoPacket = mVideoPacketQueue.front();
                                if (topVideoPacket) {
                                    if (topVideoPacket->flags>=0) {
                                        if (topVideoPacket->pts*AV_TIME_BASE*av_q2d(ifmt_ctx->streams[mInVideoStreamIndex]->time_base) - baseLineTimeStampForVideo<=audioTimeLine) {
                                            AVPacket* workVideoPacket = mVideoPacketQueue.pop();
                                            
                                            AVStream *in_video_stream = ifmt_ctx->streams[mInVideoStreamIndex];
                                            workVideoPacket->stream_index = mOutputVideoStreamIndex;
                                            AVStream *out_video_stream = ofmt_ctx->streams[workVideoPacket->stream_index];
                                            workVideoPacket->pts = av_rescale_q_rnd(workVideoPacket->pts, in_video_stream->time_base, out_video_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
                                            workVideoPacket->dts = av_rescale_q_rnd(workVideoPacket->dts, in_video_stream->time_base, out_video_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
                                            workVideoPacket->duration = av_rescale_q(workVideoPacket->duration, in_video_stream->time_base, out_video_stream->time_base);
                                            if (workVideoPacket->duration<0) {
                                                workVideoPacket->duration = 0;
                                            }
                                            workVideoPacket->pos = -1;
                                            
                                            videoTimeLine = workVideoPacket->pts*AV_TIME_BASE*av_q2d(ifmt_ctx->streams[mInVideoStreamIndex]->time_base) - baseLineTimeStampForVideo;
                                            
                                            int ret = av_interleaved_write_frame(ofmt_ctx, workVideoPacket);
                                            
                                            av_packet_unref(workVideoPacket);
                                            av_freep(&workVideoPacket);
                                            
                                            if (ret < 0) {
                                                LOGE("Error muxing packet\n");
                                                if (mMediaListener!=NULL) {
                                                    mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
                                                }
                                                isIdle = true;
                                                break;
                                            }
                                        }else {
                                            break;
                                        }
                                    }else{
                                        AVPacket* workVideoPacket = mVideoPacketQueue.pop();
                                        
                                        if (topVideoPacket->flags==-1) {
                                            av_packet_unref(workVideoPacket);
                                            av_freep(&workVideoPacket);
                                            
                                            if (mMediaListener!=NULL) {
                                                mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
                                            }
                                            isIdle = true;
                                            break;
                                        }
                                        if (topVideoPacket->flags==-3) {
                                            av_packet_unref(workVideoPacket);
                                            av_freep(&workVideoPacket);
                                            
                                            gotVideoEof = true;
                                            break;
                                        }
                                    }
                                }else {
                                    break;
                                }
                            }
                        }
                        
                        if (audioTimeLine-lastSendTime>=1000000) {
                            if (mMediaListener!=NULL) {
                                mMediaListener->notify(MEDIA_PROCESSER_INFO, MEDIA_PROCESSER_INFO_WRITE_TIMESTAMP, (int)(audioTimeLine/1000000));
                            }
                            lastSendTime = audioTimeLine;
                        }
                        
                    }else {
                        if (workAudioPacket->flags==-1) {
                            av_packet_unref(workAudioPacket);
                            av_freep(&workAudioPacket);
                            
                            if (mMediaListener!=NULL) {
                                mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
                            }
                            isIdle = true;
                            continue;
                        }
                        
                        if (workAudioPacket->flags==-3) {
                            av_packet_unref(workAudioPacket);
                            av_freep(&workAudioPacket);
                            
                            gotAudioEof = true;
                            continue;
                        }
                    }
                }
            }
        }else{
            if (mInVideoStreamIndex>=0) {
                justWriteVideoPacket = true;
            }
        }
        
        
        if (isIdle==true) {
            continue;
        }
        
        if (justWriteVideoPacket) {
            AVPacket* workVideoPacket = mVideoPacketQueue.pop();
            if (workVideoPacket) {
                if (workVideoPacket->flags>=0) {
                    AVStream *in_video_stream = ifmt_ctx->streams[mInVideoStreamIndex];
                    workVideoPacket->stream_index = mOutputVideoStreamIndex;
                    AVStream *out_video_stream = ofmt_ctx->streams[workVideoPacket->stream_index];
                    workVideoPacket->pts = av_rescale_q_rnd(workVideoPacket->pts, in_video_stream->time_base, out_video_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
                    workVideoPacket->dts = av_rescale_q_rnd(workVideoPacket->dts, in_video_stream->time_base, out_video_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
                    workVideoPacket->duration = av_rescale_q(workVideoPacket->duration, in_video_stream->time_base, out_video_stream->time_base);
                    if (workVideoPacket->duration<0) {
                        workVideoPacket->duration = 0;
                    }
                    workVideoPacket->pos = -1;
                    
                    videoTimeLine = workVideoPacket->pts*AV_TIME_BASE*av_q2d(ifmt_ctx->streams[mInVideoStreamIndex]->time_base) - baseLineTimeStampForVideo;
                    
                    int ret = av_interleaved_write_frame(ofmt_ctx, workVideoPacket);
                    
                    av_packet_unref(workVideoPacket);
                    av_freep(&workVideoPacket);
                    
                    if (ret < 0) {
                        LOGE("Error muxing packet\n");
                        if (mMediaListener!=NULL) {
                            mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
                        }
                        isIdle = true;
                        continue;
                    }
                    
                    if (videoTimeLine-lastSendTime>=1000000) {
                        if (mMediaListener!=NULL) {
                            mMediaListener->notify(MEDIA_PROCESSER_INFO, MEDIA_PROCESSER_INFO_WRITE_TIMESTAMP, (int)(videoTimeLine/1000000));
                        }
                        lastSendTime = videoTimeLine;
                    }
                }else{
                    if (workVideoPacket->flags==-1) {
                        av_packet_unref(workVideoPacket);
                        av_freep(&workVideoPacket);
                        
                        if (mMediaListener!=NULL) {
                            mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
                        }
                        isIdle = true;
                        continue;
                    }
                    
                    if (workVideoPacket->flags==-3) {
                        av_packet_unref(workVideoPacket);
                        av_freep(&workVideoPacket);
                        
                        gotVideoEof = true;
                        continue;
                    }
                }
            }
        }
    }
    
    mAudioPacketQueue.flush();
    mVideoPacketQueue.flush();
    
    close_all_piplelines();
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void MediaRemuxer::deleteRemuxerThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

bool MediaRemuxer::open_all_piplelines()
{
    int ret;
    
    av_register_all();
    
    if ((ret = avformat_open_input(&ifmt_ctx, in_filename, 0, 0)) < 0) {
        LOGE("Could not open input file '%s'", in_filename);
        if (mMediaListener!=NULL) {
            mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
        }
        return false;
    }
    
    if ((ret = avformat_find_stream_info(ifmt_ctx, 0)) < 0) {
        LOGE("Failed to retrieve input stream information");
        if (mMediaListener!=NULL) {
            mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
        }
        return false;
    }
    
    av_dump_format(ifmt_ctx, 0, in_filename, 0);
    
    mInAudioStreamIndex = -1;
    mInVideoStreamIndex = -1;
    
    for(int i= 0; i < ifmt_ctx->nb_streams; i++)
    {
        if (ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first audio stream, and discard others.
            if(mInAudioStreamIndex == -1)
            {
                mInAudioStreamIndex = i;
            }
            else
            {
                ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && (ifmt_ctx->streams[i]->codec->codec_id==AV_CODEC_ID_H264 || ifmt_ctx->streams[i]->codec->codec_id==AV_CODEC_ID_HEVC || ifmt_ctx->streams[i]->codec->codec_id==AV_CODEC_ID_MPEG4))
        {
            //by default, use the first video stream, and discard others.
            if(mInVideoStreamIndex == -1)
            {
                mInVideoStreamIndex = i;
            }
            else
            {
                ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
        {
            ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
        }
    }
    
    if (mInAudioStreamIndex==-1 && mInVideoStreamIndex==-1) {
        LOGE("No Audio Stream and No Video Stream");
        
        if (mMediaListener!=NULL) {
            mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
        }
        return false;
    }
    
    avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, out_filename);
    if (!ofmt_ctx) {
        LOGE("Could not create output context\n");
        if (mMediaListener!=NULL) {
            mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
        }
        return false;
    }
    
    ofmt_ctx->oformat->flags |= AVFMT_TS_NONSTRICT;

    int stream_index = -1;
    if (mInAudioStreamIndex>=0) {
        AVStream *in_audio_stream = ifmt_ctx->streams[mInAudioStreamIndex];
        AVCodecParameters *in_audio_codecpar = in_audio_stream->codecpar;
        AVStream *out_audio_stream = avformat_new_stream(ofmt_ctx, NULL);
        if (!out_audio_stream) {
            LOGE("Failed allocating output stream\n");
            if (mMediaListener!=NULL) {
                mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
            }
            return false;
        }
        
        mOutputAudioStreamIndex = ++stream_index;

        int ret = avcodec_parameters_copy(out_audio_stream->codecpar, in_audio_codecpar);
        if (ret < 0) {
            LOGE("Failed to copy codec parameters\n");
            if (mMediaListener!=NULL) {
                mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
            }
            return false;
        }
        
        out_audio_stream->codecpar->codec_tag = 0;
    }
    
    if (mInVideoStreamIndex>=0) {
        AVStream *in_video_stream = ifmt_ctx->streams[mInVideoStreamIndex];
        AVCodecParameters *in_video_codecpar = in_video_stream->codecpar;
        AVStream *out_video_stream = avformat_new_stream(ofmt_ctx, NULL);
        
        if (!out_video_stream){
            LOGE("Failed allocating output stream\n");
            if (mMediaListener!=NULL) {
                mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
            }
            return false;
        }
        
        mOutputVideoStreamIndex = ++stream_index;

        int ret = avcodec_parameters_copy(out_video_stream->codecpar, in_video_codecpar);
        if (ret < 0) {
            LOGE("Failed to copy codec parameters\n");
            if (mMediaListener!=NULL) {
                mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
            }
            return false;
        }
        
        out_video_stream->codecpar->codec_tag = 0;

        AVDictionaryEntry *m = NULL;
        while((m=av_dict_get(in_video_stream->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
            if(strcmp(m->key, "rotate")) continue;
            else{
                int rotate = atoi(m->value);
                av_dict_set_int(&out_video_stream->metadata, "rotate", rotate, 0);
            }
        }
    }
    
    av_dump_format(ofmt_ctx, 0, out_filename, 1);
    
    if (!(ofmt_ctx->oformat->flags & AVFMT_NOFILE)) {
        ret = avio_open(&ofmt_ctx->pb, out_filename, AVIO_FLAG_WRITE);
        if (ret < 0) {
            LOGE("Could not open output file '%s'", out_filename);
            if (mMediaListener!=NULL) {
                mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
            }
            return false;
        }
    }
    
    ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        LOGE("Error occurred when opening output file\n");
        if (mMediaListener!=NULL) {
            mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
        }
        return false;
    }
    
    return true;
}

void MediaRemuxer::close_all_piplelines()
{
    if (ifmt_ctx) {
        avformat_close_input(&ifmt_ctx);
        ifmt_ctx = NULL;
    }
    /* close output */
    if (ofmt_ctx && ofmt_ctx->oformat && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
    {
        avio_close(ofmt_ctx->pb);
    }
    
    if (ofmt_ctx) {
        avformat_free_context(ofmt_ctx);
        ofmt_ctx = NULL;
    }
}
