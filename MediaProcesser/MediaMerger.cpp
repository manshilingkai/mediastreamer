//
//  MediaMerger.cpp
//  MediaStreamer
//
//  Created by Think on 2016/11/17.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaMerger.h"
#include "MediaTimelineMerger.h"
#include "MediaTransPacketMerger.h"
#include "MediaRemuxer.h"

#ifdef ANDROID
MediaMerger* MediaMerger::CreateMediaMerger(JavaVM *jvm, MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup, MEDIA_MERGE_ALGORITHM algorithm, MediaProduct* outputProduct)
{
    if (algorithm==MEDIA_MERGE_ALGORITHM_TIMELINE) {
        return new MediaTimelineMerger(jvm, inputMediaMaterialGroup, inputMediaEffectGroup ,outputProduct);
    }
    
    if (algorithm==MEDIA_MERGE_ALGORITHM_REMUX) {
        return new MediaTransPacketMerger(jvm, inputMediaMaterialGroup, NULL, outputProduct);
    }
    
    if (algorithm==MEDIA_MERGE_ALGORITHM_DEEP_REMUX) {
        return new MediaRemuxer(jvm, inputMediaMaterialGroup, NULL, outputProduct);
    }
    
    return NULL;
}
#else
MediaMerger* MediaMerger::CreateMediaMerger(MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup, MEDIA_MERGE_ALGORITHM algorithm, MediaProduct* outputProduct)
{
    if (algorithm==MEDIA_MERGE_ALGORITHM_TIMELINE) {
        return new MediaTimelineMerger(inputMediaMaterialGroup, inputMediaEffectGroup, outputProduct);
    }
    
    if (algorithm==MEDIA_MERGE_ALGORITHM_REMUX) {
        return new MediaTransPacketMerger(inputMediaMaterialGroup, NULL, outputProduct);
    }
    
    if (algorithm==MEDIA_MERGE_ALGORITHM_DEEP_REMUX) {
        return new MediaRemuxer(inputMediaMaterialGroup, NULL, outputProduct);
    }
    
    return NULL;
}
#endif

void MediaMerger::DeleteMediaMerger(MEDIA_MERGE_ALGORITHM algorithm, MediaMerger* mediaMerger)
{
    if (algorithm==MEDIA_MERGE_ALGORITHM_TIMELINE) {
        MediaTimelineMerger *mediaTimelineMerger = (MediaTimelineMerger*)mediaMerger;
        if (mediaTimelineMerger!=NULL) {
            delete mediaTimelineMerger;
            mediaTimelineMerger = NULL;
        }
    }
    
    if (algorithm==MEDIA_MERGE_ALGORITHM_REMUX) {
        MediaTransPacketMerger* mediaTransPacketMerger = (MediaTransPacketMerger*)mediaMerger;
        if (mediaTransPacketMerger!=NULL) {
            delete mediaTransPacketMerger;
            mediaTransPacketMerger = NULL;
        }
    }
    
    if (algorithm==MEDIA_MERGE_ALGORITHM_DEEP_REMUX) {
        MediaRemuxer* mediaRemuxer = (MediaRemuxer*)mediaMerger;
        if (mediaRemuxer!=NULL) {
            delete mediaRemuxer;
            mediaRemuxer = NULL;
        }
    }
}
