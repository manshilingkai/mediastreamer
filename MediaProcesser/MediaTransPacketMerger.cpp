//
//  MediaTransPacketMerger.cpp
//  MediaStreamer
//
//  Created by Think on 2018/6/13.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "MediaTransPacketMerger.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#endif

#ifdef IOS
#include "iOSMediaListener.h"
#endif

#include "MediaLog.h"

#ifdef ANDROID
MediaTransPacketMerger::MediaTransPacketMerger(JavaVM *jvm, MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup ,MediaProduct* outputProduct)
{
    mJvm = jvm;
    
    pthread_mutex_init(&mMainLock, NULL);
    
    mMediaListener = NULL;
    
    isTheadLive = false;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    isTransPacketing = false;
    isBreakThread = false;
    
    if (inputMediaMaterialGroup->mediaMaterialNum>0) {
        in_filename = strdup(inputMediaMaterialGroup->mediaMaterials[0]->url);
    }else{
        in_filename = NULL;
    }
    
    out_filename = strdup(outputProduct->url);
    
    stream_mapping = NULL;
    stream_mapping_size = 0;
    
    ifmt_ctx = NULL;
    ofmt_ctx = NULL;
    ofmt = NULL;
}
#else
MediaTransPacketMerger::MediaTransPacketMerger(MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup ,MediaProduct* outputProduct)
{
    pthread_mutex_init(&mMainLock, NULL);
    
    mMediaListener = NULL;
    
    isTheadLive = false;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    isTransPacketing = false;
    isBreakThread = false;
    
    if (inputMediaMaterialGroup->mediaMaterialNum>0) {
        in_filename = strdup(inputMediaMaterialGroup->mediaMaterials[0]->url);
    }else{
        in_filename = NULL;
    }
    
    out_filename = strdup(outputProduct->url);
    
    stream_mapping = NULL;
    stream_mapping_size = 0;
    
    ifmt_ctx = NULL;
    ofmt_ctx = NULL;
    ofmt = NULL;
}
#endif

MediaTransPacketMerger::~MediaTransPacketMerger()
{
    if (in_filename) {
        free(in_filename);
        in_filename = NULL;
    }
    
    if (out_filename) {
        free(out_filename);
        out_filename = NULL;
    }
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    pthread_mutex_destroy(&mMainLock);
}

#ifdef ANDROID
void MediaTransPacketMerger::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    pthread_mutex_lock(&mMainLock);
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    pthread_mutex_unlock(&mMainLock);
}
#endif

#ifdef IOS
void MediaTransPacketMerger::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    pthread_mutex_lock(&mMainLock);
    mMediaListener = new iOSMediaListener(listener,arg);
    pthread_mutex_unlock(&mMainLock);
}
#endif


void MediaTransPacketMerger::notify(int event, int ext1, int ext2)
{
}

void MediaTransPacketMerger::start()
{
    pthread_mutex_lock(&mMainLock);
    
    pthread_mutex_lock(&mLock);
    isTransPacketing = true;
    pthread_mutex_unlock(&mLock);

    if (!isTheadLive) {
        createTransPacketThread();
        isTheadLive = true;
    }
    
    pthread_mutex_unlock(&mMainLock);
}

void MediaTransPacketMerger::resume()
{
    pthread_mutex_lock(&mMainLock);
    
    if (isTheadLive) {
        pthread_mutex_lock(&mLock);
        isTransPacketing = true;
        pthread_mutex_unlock(&mLock);
        
        pthread_cond_signal(&mCondition);
    }
    
    pthread_mutex_unlock(&mMainLock);
}

void MediaTransPacketMerger::pause()
{
    pthread_mutex_lock(&mMainLock);
    
    if (isTheadLive) {
        pthread_mutex_lock(&mLock);
        isTransPacketing = false;
        pthread_mutex_unlock(&mLock);
        
        pthread_cond_signal(&mCondition);
    }

    pthread_mutex_unlock(&mMainLock);
}

void MediaTransPacketMerger::stop()
{
    pthread_mutex_lock(&mMainLock);
    
    if (isTheadLive) {
        this->deleteTransPacketThread();
        isTheadLive = false;
    }
    
    pthread_mutex_lock(&mLock);
    isTransPacketing = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_mutex_unlock(&mMainLock);
}

void MediaTransPacketMerger::createTransPacketThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, NULL, handleTransPacketThread, this);
    
    pthread_attr_destroy(&attr);
}

void* MediaTransPacketMerger::handleTransPacketThread(void* ptr)
{
    MediaTransPacketMerger* merger = (MediaTransPacketMerger*)ptr;
    merger->transPacketThreadMain();
    
    return NULL;
}

void MediaTransPacketMerger::transPacketThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    bool isIdle = !open_all_piplelines();
    
    int ret;
    AVPacket pkt;
    AVStream *in_stream, *out_stream;

    int64_t baseLineTimeStamp = 0ll;
    bool isSetBaseLineTimeStamp = false;
    int64_t lastSendTime = 0ll;
    int timestamp_stream_index = -1;
    
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread)
        {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (isIdle) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        if (!isTransPacketing) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        pthread_mutex_unlock(&mLock);
        
        ret = av_read_frame(ifmt_ctx, &pkt);
        if (ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN)) {
            av_packet_unref(&pkt);
            continue;
        }else if (ret == AVERROR_EOF) {
            av_packet_unref(&pkt);
            
            //Write file trailer
            av_write_trailer(ofmt_ctx);
            
            if (mMediaListener!=NULL) {
                mMediaListener->notify(MEDIA_PROCESSER_END, 0, 0);
            }
            isIdle = true;
            continue;
        }else if(ret<0){
            av_packet_unref(&pkt);

            LOGE("Error demuxing packet");
            if (mMediaListener!=NULL) {
                mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
            }
            isIdle = true;
            continue;
        }
        
        in_stream  = ifmt_ctx->streams[pkt.stream_index];
        if (pkt.stream_index >= stream_mapping_size ||
            stream_mapping[pkt.stream_index].stream_index < 0) {
            av_packet_unref(&pkt);
            continue;
        }
        
        if (timestamp_stream_index==-1) {
            timestamp_stream_index = pkt.stream_index;
        }
        
        int in_stream_index = pkt.stream_index;
        pkt.stream_index = stream_mapping[pkt.stream_index].stream_index;
        out_stream = ofmt_ctx->streams[pkt.stream_index];
        
        /* copy packet */
        pkt.pts = av_rescale_q_rnd(pkt.pts, in_stream->time_base, out_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
        if (stream_mapping[in_stream_index].stream_type == AVMEDIA_TYPE_AUDIO && stream_mapping[in_stream_index].last_pts>=pkt.pts) {
            pkt.pts = stream_mapping[in_stream_index].last_pts + 1;
        }
        pkt.dts = av_rescale_q_rnd(pkt.dts, in_stream->time_base, out_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
        if (stream_mapping[in_stream_index].stream_type == AVMEDIA_TYPE_AUDIO && stream_mapping[in_stream_index].last_dts>=pkt.dts) {
            pkt.dts = stream_mapping[in_stream_index].last_dts + 1;
        }
        pkt.duration = av_rescale_q(pkt.duration, in_stream->time_base, out_stream->time_base);
        if (pkt.duration<0) {
            pkt.duration = 0;
        }
        pkt.pos = -1;
        
        stream_mapping[in_stream_index].last_pts = pkt.pts;
        stream_mapping[in_stream_index].last_dts = pkt.dts;
        
        if (timestamp_stream_index==pkt.stream_index) {
            if (!isSetBaseLineTimeStamp) {
                isSetBaseLineTimeStamp = true;
                baseLineTimeStamp = pkt.pts * AV_TIME_BASE * av_q2d(out_stream->time_base);
                
                if(mMediaListener!=NULL)
                {
                    mMediaListener->notify(MEDIA_PROCESSER_INFO, MEDIA_PROCESSER_INFO_WRITE_TIMESTAMP, 0);
                }
                lastSendTime = 0;
            }
            
            int64_t nowTime = pkt.pts * AV_TIME_BASE * av_q2d(out_stream->time_base) - baseLineTimeStamp;
            if (nowTime-lastSendTime>=1000000) {
                if (mMediaListener!=NULL) {
                    mMediaListener->notify(MEDIA_PROCESSER_INFO, MEDIA_PROCESSER_INFO_WRITE_TIMESTAMP, (int)(nowTime/1000000));
                }
                lastSendTime = nowTime;
            }
        }
        
        ret = av_interleaved_write_frame(ofmt_ctx, &pkt);
        if (ret < 0) {
            LOGE("Error muxing packet\n");

            if (mMediaListener!=NULL) {
                mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
            }
            
            isIdle = true;
            continue;
        }
        
        av_packet_unref(&pkt);
    }
    
    close_all_piplelines();
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void MediaTransPacketMerger::deleteTransPacketThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

bool MediaTransPacketMerger::open_all_piplelines()
{
    int ret, i;
    int stream_index = 0;
    
    av_register_all();
    
    if ((ret = avformat_open_input(&ifmt_ctx, in_filename, 0, 0)) < 0) {
        LOGE("Could not open input file '%s'", in_filename);
        if (mMediaListener!=NULL) {
            mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
        }
        return false;
    }
    
    if ((ret = avformat_find_stream_info(ifmt_ctx, 0)) < 0) {
        LOGE("Failed to retrieve input stream information");
        if (mMediaListener!=NULL) {
            mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
        }
        return false;
    }
    
    av_dump_format(ifmt_ctx, 0, in_filename, 0);

    avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, out_filename);
    if (!ofmt_ctx) {
        LOGE("Could not create output context\n");
        if (mMediaListener!=NULL) {
            mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
        }
        return false;
    }
    
    stream_mapping_size = ifmt_ctx->nb_streams;
//    stream_mapping = (int*)av_mallocz_array(stream_mapping_size, sizeof(*stream_mapping));
    stream_mapping = new MediaTransPacketStreamMapping[stream_mapping_size];
    if (!stream_mapping) {
        LOGE("No Memory Space");
        if (mMediaListener!=NULL) {
            mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
        }
        return false;
    }
    
    ofmt = ofmt_ctx->oformat;
    
    for (i = 0; i < ifmt_ctx->nb_streams; i++) {
        AVStream *out_stream;
        AVStream *in_stream = ifmt_ctx->streams[i];
        AVCodecParameters *in_codecpar = in_stream->codecpar;
        
        if (in_codecpar->codec_type != AVMEDIA_TYPE_AUDIO &&
            in_codecpar->codec_type != AVMEDIA_TYPE_VIDEO &&
            in_codecpar->codec_type != AVMEDIA_TYPE_SUBTITLE) {
            stream_mapping[i].stream_index = -1;
            continue;
        }
        
        stream_mapping[i].stream_index = stream_index++;
        stream_mapping[i].stream_type = in_codecpar->codec_type;
        
        out_stream = avformat_new_stream(ofmt_ctx, NULL);
        if (!out_stream) {
            LOGE("Failed allocating output stream\n");
            if (mMediaListener!=NULL) {
                mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
            }
            return false;
        }
        
        ret = avcodec_parameters_copy(out_stream->codecpar, in_codecpar);
        if (ret < 0) {
            LOGE("Failed to copy codec parameters\n");
            if (mMediaListener!=NULL) {
                mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
            }
            return false;
        }
        out_stream->codecpar->codec_tag = 0;
        
        if (in_codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
            AVDictionaryEntry *m = NULL;
            while((m=av_dict_get(in_stream->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
                if(strcmp(m->key, "rotate")) continue;
                else{
                    int rotate = atoi(m->value);
                    av_dict_set_int(&out_stream->metadata, "rotate", rotate, 0);
                }
            }
        }
    }
    
    av_dump_format(ofmt_ctx, 0, out_filename, 1);
    
    if (!(ofmt->flags & AVFMT_NOFILE)) {
        ret = avio_open(&ofmt_ctx->pb, out_filename, AVIO_FLAG_WRITE);
        if (ret < 0) {
            LOGE("Could not open output file '%s'", out_filename);
            if (mMediaListener!=NULL) {
                mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
            }
            return false;
        }
    }
    
    ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        LOGE("Error occurred when opening output file\n");
        if (mMediaListener!=NULL) {
            mMediaListener->notify(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_REMUX_FAIL, 0);
        }
        return false;
    }
    
    return true;
}

void MediaTransPacketMerger::close_all_piplelines()
{
    if (ifmt_ctx) {
        avformat_close_input(&ifmt_ctx);
        ifmt_ctx = NULL;
    }
    /* close output */
    if (ofmt_ctx && ofmt && !(ofmt->flags & AVFMT_NOFILE))
    {
        avio_close(ofmt_ctx->pb);
    }
    
    if (ofmt_ctx) {
        avformat_free_context(ofmt_ctx);
        ofmt_ctx = NULL;
    }
    
    if (stream_mapping) {
        delete [] stream_mapping;
        stream_mapping = NULL;
    }
}
