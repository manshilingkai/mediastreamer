//
//  MediaTimelineMerger.h
//  MediaStreamer
//
//  Created by Think on 2016/11/18.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaTimelineMerger_h
#define MediaTimelineMerger_h

#include <stdio.h>

extern "C" {
#include "libavutil/avstring.h"
}

#include "MediaMerger.h"
#include "IMediaListener.h"
#include "TimedEventQueue.h"
#include "NotificationQueue.h"

#include "FFmpegReader.h"
#include "VideoDecoder.h"
#include "AudioDecoder.h"
#include "ColorSpaceConvert.h"
#include "AudioFilter.h"
#include "VideoEncoder.h"
#include "AudioEncoder.h"
#include "MediaMuxer.h"

#include "AudioPCMDataPool.h"

#include "GPUImageOffScreenRender.h"
#include "Word2Bitmap.h"

#include "AnimatedWebp.h"
#include "AnimatedGif.h"

#include "AnimatedGifCreater.h"

struct TextContext {
    VideoFrame* backGroundLayFrame;
    TextInfo* textInfo;
    
    TextContext()
    {
        backGroundLayFrame = NULL;
        textInfo = NULL;
    }
};

struct BackGroundSoundContext {
    FFmpegReader *ffmpegReaderForBackGroundSound;
    AudioDecoder *audioDecoderForBackGroundSound;
    AudioFilter *audioFilterForBackGroundSound;
    AudioPCMDataPool *audioPCMDataPoolForBackGroundSound;
    
    BackGroundSoundContext()
    {
        ffmpegReaderForBackGroundSound = NULL;
        audioDecoderForBackGroundSound = NULL;
        audioFilterForBackGroundSound = NULL;
        audioPCMDataPoolForBackGroundSound = NULL;
    }
};

class MediaTimelineMerger : public MediaMerger, IMediaListener {

public:
#ifdef ANDROID
    MediaTimelineMerger(JavaVM *jvm, MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup ,MediaProduct* outputProduct);
#else
    MediaTimelineMerger(MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup ,MediaProduct* outputProduct);
#endif
    
    ~MediaTimelineMerger();
    
#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#endif

#ifdef IOS
    void setListener(void (*listener)(void*,int,int,int), void* arg);
#endif
    
    void notify(int event, int ext1, int ext2);
    
    void start();
    
    void resume();
    void pause();
    
    void stop();
    
private:
    friend struct MediaTimelineMergerEvent;
    
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    MediaMaterialGroup *mInputMediaMaterialGroup;
    MediaEffectGroup *mInputMediaEffectGroup;
    VideoOptions mOutputVideoOptions;
    AudioOptions mOutputAudioOptions;
    char* mOutputUrl;
    MEDIA_PRODUCT_TYPE mOutputProductType;
    
    IMediaListener *mMediaListener;
    
    MediaTimelineMerger(const MediaTimelineMerger &);
    MediaTimelineMerger &operator=(const MediaTimelineMerger &);
    
    pthread_mutex_t mLock;
    pthread_cond_t mStopCondition;

    TimedEventQueue mQueue;
    
    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mProcessOnePacketEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mNotifyEvent;
    void onPrepareAsyncEvent();
    void onProcessOnePacketEvent();
    void onStopEvent();
    void onNotifyEvent();
    
    void prepareAsync_l();
    void play_l();
    void pause_l();
    void stop_l();
    
    NotificationQueue mNotificationQueue;
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
    
    void cancelMergerEvents();
private:
    enum {
        CONNECTING           = 0x01,
        CONNECTED            = 0x02,
        STREAMING            = 0x04,
        PAUSED               = 0x10,
        ERROR                = 0x20,
        END                  = 0x40,
        INITIALIZED          = 0x80,
        ENDING               = 0x100,
    };
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
private:
    bool open_input_material_pipelines_l(MediaMaterial * inputMediaMaterial);
    void close_input_material_pipelines_l(MediaMaterial * inputMediaMaterial);
    
    bool open_input_effect_pipelines_l(MediaEffect *inputMediaEffect);
    void close_input_effect_pipelines_l(MediaEffect *inputMediaEffect);
    
    bool open_ouput_pipelines_l(MEDIA_PRODUCT_TYPE outputProductType);
    void close_output_pipelines_l(MEDIA_PRODUCT_TYPE outputProductType);
private:
    //input AV
    FFmpegReader *mFFmpegReaderForInputMaterial;
    AudioDecoder *mAudioDecoderForInputMaterial;
    AudioFilter *mAudioFilterForInputMaterial;
    VideoDecoderType mVideoDecoderTypeForInputMaterial;
    VideoDecoder *mVideoDecoderForInputMaterial;
    int mVideoRotationForInputMaterial;
    int mVideoFpsForInputMaterial;
    
    FFSeekStatus mSeekStatusForInputMaterial;
    bool mFoundStartPosForInputMaterial;
    int64_t mCurrentVideoDurationUsForInputMaterial;
    int64_t mCurrentAudioDurationUsForInputMaterial;
    
    MediaMaterial* currentInputMediaMaterial;
    bool isInputMediaMaterialOpened; //false
    int mInputMaterialIndex; //-1
    
    bool isSwitchToNewVideoBaseLineForInputMaterial;
    bool isSwitchToNewAudioBaseLineForInputMaterial;
    
    int64_t mLastRealVideoPtsForInputMaterial;
    int64_t mLastRealAudioPtsForInputMaterial;
    
    //input Image
    VideoFrame *mImageFrameForInputImageMaterial;

private:
    // Text effect
    TextInfo *TextEffectParamToBlendTextInfo(MediaEffect *inputTextEffect, int64_t blendPts/*us*/);
private:
    //output product
    GPUImageOffScreenRender *mGPUImageOffScreenRenderForOutputProduct;
    ColorSpaceConvert *mColorSpaceConvertForOutputProduct;
    
    //output product : mp4
    VideoEncoder *mVideoEncoderForOutputProduct;
    VIDEO_ENCODER_TYPE mVideoEncoderTypeForOutputProduct;
    
    AudioFilter *mAudioFilterForOutputProduct;
    AudioPCMDataPool *mAudioPCMDataPoolForOutputProduct;
    AudioEncoder *mAudioEncoderForOutputProduct;
    AUDIO_ENCODER_TYPE mAudioEncoderTypeForOutputProduct;
    MediaMuxer *mMediaMuxerForOutputProduct;
    
    int64_t mCurrentVideoPtsForOutputProduct;
    int64_t mCurrentAudioPtsForOutputProduct;
    
    VideoFrame mTargetVideoFrameForOutputProduct;

    //output product : gif
    AnimatedGifCreater *mAnimatedGifCreater;
    uint64_t mLastSendRecordTimeMSForGif;
    uint64_t mLastRecordTimeMSForGif;
    
private:
    void processAVPacket();
    bool audioFlowing_l(uint8_t* pcmData, int pcmSize, int64_t pts);
    bool videoFlowing_l(AVFrame* videoFrame);
    
    bool mixAudioFrame_l(AudioFrame* targetAudioFrame, BackGroundSoundContext *backGroundSoundContext);
    
    bool isNeedConvertVideoFrame(VideoFrame *sourceFrame, VideoFrame *targetFrame);
    
    void calculateAudioFramePts(AVFrame *audioFrame, AVStream* audioStreamContext);
};

#endif /* MediaTimelineMerger_h */
