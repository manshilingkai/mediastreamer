//
//  MediaMaterial.h
//  MediaStreamer
//
//  Created by Think on 2016/11/17.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaMaterial_h
#define MediaMaterial_h

#include <stdio.h>
#include <stdlib.h>

#define MAX_MATERIAL_NUM 64

enum MEDIA_MATERIAL_TYPE{
    MEDIA_MATERIAL_TYPE_UNKNOWN     = -1,
    MEDIA_MATERIAL_TYPE_VIDEO_AUDIO = 0,
    MEDIA_MATERIAL_TYPE_VIDEO       = 1,
    MEDIA_MATERIAL_TYPE_AUDIO       = 2,
    MEDIA_MATERIAL_TYPE_TEXT        = 3,
    MEDIA_MATERIAL_TYPE_PNG         = 4,
    MEDIA_MATERIAL_TYPE_JPEG        = 5,
};

struct MediaMaterial
{
    int iD;
    
    MEDIA_MATERIAL_TYPE type;
    
    char *url;
    
    int64_t startPos; //ms
    int64_t endPos; //ms
    
    float volume;
    
    float speed;
    
    MediaMaterial()
    {
        iD = -1;
        type = MEDIA_MATERIAL_TYPE_UNKNOWN;
        url = NULL;
        
        startPos = -1;
        endPos = -1;
        
        volume = 1.0f;
        speed = 1.0f;
    }
};

struct MediaMaterialGroup {
    MediaMaterial *mediaMaterials[MAX_MATERIAL_NUM];
    int mediaMaterialNum;
    
    MediaMaterialGroup()
    {
        for(int i = 0; i < MAX_MATERIAL_NUM; i++)
        {
            mediaMaterials[i] = NULL;
        }
        
        mediaMaterialNum = 0;
    }
    
    inline void Clear()
    {
        for(int i = 0; i < MAX_MATERIAL_NUM; i++)
        {
            if(mediaMaterials[i]!=NULL)
            {
                delete mediaMaterials[i];
                mediaMaterials[i] = NULL;
            }
        }
        
        mediaMaterialNum = 0;
    }
    
    inline void Free()
    {
        for(int i = 0; i < MAX_MATERIAL_NUM; i++)
        {
            if(mediaMaterials[i]!=NULL)
            {
                if(mediaMaterials[i]->url!=NULL)
                {
                    free(mediaMaterials[i]->url);
                    mediaMaterials[i]->url = NULL;
                }
                
                delete mediaMaterials[i];
                mediaMaterials[i] = NULL;
            }
        }
        
        mediaMaterialNum = 0;
    }

};

#endif /* MediaMaterial_h */
