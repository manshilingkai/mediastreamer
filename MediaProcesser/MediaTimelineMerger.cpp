//
//  MediaTimelineMerger.cpp
//  MediaStreamer
//
//  Created by Think on 2016/11/18.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaTimelineMerger.h"

#include "AutoLock.h"
#include "MediaLog.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#endif

#ifdef IOS
#include "iOSMediaListener.h"
#include <QuartzCore/QuartzCore.h>
#include <CoreVideo/CoreVideo.h>
#endif

extern "C" {
#include "libavformat/avformat.h"
}

#include "AudioMixer.h"

#include "ImageProcesserUtils.h"

struct MediaTimelineMergerEvent : public TimedEventQueue::Event {
    MediaTimelineMergerEvent(
                       MediaTimelineMerger *merger,
                       void (MediaTimelineMerger::*method)())
    : mMerger(merger),
    mMethod(method) {
    }
    
protected:
    virtual ~MediaTimelineMergerEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mMerger->*mMethod)();
    }
    
private:
    MediaTimelineMerger *mMerger;
    void (MediaTimelineMerger::*mMethod)();
    
    MediaTimelineMergerEvent(const MediaTimelineMergerEvent &);
    MediaTimelineMergerEvent &operator=(const MediaTimelineMergerEvent &);
};

#ifdef ANDROID
MediaTimelineMerger::MediaTimelineMerger(JavaVM *jvm, MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup, MediaProduct* outputProduct)
{    
    mJvm = jvm;
    
    mInputMediaMaterialGroup = new MediaMaterialGroup;
    mInputMediaMaterialGroup->mediaMaterialNum = inputMediaMaterialGroup->mediaMaterialNum;
    for (int i = 0; i < inputMediaMaterialGroup->mediaMaterialNum; i++) {
        mInputMediaMaterialGroup->mediaMaterials[i] = new MediaMaterial;
        mInputMediaMaterialGroup->mediaMaterials[i]->iD = inputMediaMaterialGroup->mediaMaterials[i]->iD;
        mInputMediaMaterialGroup->mediaMaterials[i]->type = inputMediaMaterialGroup->mediaMaterials[i]->type;
        mInputMediaMaterialGroup->mediaMaterials[i]->url = strdup(inputMediaMaterialGroup->mediaMaterials[i]->url);
        mInputMediaMaterialGroup->mediaMaterials[i]->startPos = inputMediaMaterialGroup->mediaMaterials[i]->startPos;
        mInputMediaMaterialGroup->mediaMaterials[i]->endPos = inputMediaMaterialGroup->mediaMaterials[i]->endPos;
        mInputMediaMaterialGroup->mediaMaterials[i]->volume = inputMediaMaterialGroup->mediaMaterials[i]->volume;
        mInputMediaMaterialGroup->mediaMaterials[i]->speed = inputMediaMaterialGroup->mediaMaterials[i]->speed;
    }
    
    mInputMediaEffectGroup = new MediaEffectGroup;
    mInputMediaEffectGroup->mediaEffectNum = inputMediaEffectGroup->mediaEffectNum;
    for (int i = 0; i<inputMediaEffectGroup->mediaEffectNum; i++) {
        mInputMediaEffectGroup->mediaEffects[i] = new MediaEffect;
        mInputMediaEffectGroup->mediaEffects[i]->iD = inputMediaEffectGroup->mediaEffects[i]->iD;
        mInputMediaEffectGroup->mediaEffects[i]->type = inputMediaEffectGroup->mediaEffects[i]->type;
        if (inputMediaEffectGroup->mediaEffects[i]->url) {
            mInputMediaEffectGroup->mediaEffects[i]->url = strdup(inputMediaEffectGroup->mediaEffects[i]->url);
        }
        mInputMediaEffectGroup->mediaEffects[i]->effect_in_pos = inputMediaEffectGroup->mediaEffects[i]->effect_in_pos;
        mInputMediaEffectGroup->mediaEffects[i]->effect_out_pos = inputMediaEffectGroup->mediaEffects[i]->effect_out_pos;
        mInputMediaEffectGroup->mediaEffects[i]->startPos = inputMediaEffectGroup->mediaEffects[i]->startPos;
        mInputMediaEffectGroup->mediaEffects[i]->endPos = inputMediaEffectGroup->mediaEffects[i]->endPos;
        mInputMediaEffectGroup->mediaEffects[i]->volume = inputMediaEffectGroup->mediaEffects[i]->volume;
        mInputMediaEffectGroup->mediaEffects[i]->speed = inputMediaEffectGroup->mediaEffects[i]->speed;
        
        mInputMediaEffectGroup->mediaEffects[i]->x = inputMediaEffectGroup->mediaEffects[i]->x;
        mInputMediaEffectGroup->mediaEffects[i]->y = inputMediaEffectGroup->mediaEffects[i]->y;
        mInputMediaEffectGroup->mediaEffects[i]->rotation = inputMediaEffectGroup->mediaEffects[i]->rotation;
        mInputMediaEffectGroup->mediaEffects[i]->scale = inputMediaEffectGroup->mediaEffects[i]->scale;
        mInputMediaEffectGroup->mediaEffects[i]->flipHorizontal = inputMediaEffectGroup->mediaEffects[i]->flipHorizontal;
        mInputMediaEffectGroup->mediaEffects[i]->flipVertical = inputMediaEffectGroup->mediaEffects[i]->flipVertical;
        
        if (inputMediaEffectGroup->mediaEffects[i]->text) {
            mInputMediaEffectGroup->mediaEffects[i]->text = strdup(inputMediaEffectGroup->mediaEffects[i]->text);
        }
        if (inputMediaEffectGroup->mediaEffects[i]->fontLibPath) {
            mInputMediaEffectGroup->mediaEffects[i]->fontLibPath = strdup(inputMediaEffectGroup->mediaEffects[i]->fontLibPath);
        }
        mInputMediaEffectGroup->mediaEffects[i]->fontSize = inputMediaEffectGroup->mediaEffects[i]->fontSize;
        mInputMediaEffectGroup->mediaEffects[i]->fontColor = inputMediaEffectGroup->mediaEffects[i]->fontColor;
        mInputMediaEffectGroup->mediaEffects[i]->leftMargin = inputMediaEffectGroup->mediaEffects[i]->leftMargin;
        mInputMediaEffectGroup->mediaEffects[i]->rightMargin = inputMediaEffectGroup->mediaEffects[i]->rightMargin;
        mInputMediaEffectGroup->mediaEffects[i]->topMargin = inputMediaEffectGroup->mediaEffects[i]->topMargin;
        mInputMediaEffectGroup->mediaEffects[i]->bottomMargin = inputMediaEffectGroup->mediaEffects[i]->bottomMargin;
        mInputMediaEffectGroup->mediaEffects[i]->lineSpace = inputMediaEffectGroup->mediaEffects[i]->lineSpace;
        mInputMediaEffectGroup->mediaEffects[i]->wordSpace = inputMediaEffectGroup->mediaEffects[i]->wordSpace;
        mInputMediaEffectGroup->mediaEffects[i]->text_animation_type = inputMediaEffectGroup->mediaEffects[i]->text_animation_type;
        
        mInputMediaEffectGroup->mediaEffects[i]->gpu_image_filter_type = inputMediaEffectGroup->mediaEffects[i]->gpu_image_filter_type;
        mInputMediaEffectGroup->mediaEffects[i]->gpu_image_transition_type = inputMediaEffectGroup->mediaEffects[i]->gpu_image_transition_type;
        mInputMediaEffectGroup->mediaEffects[i]->transition_source_id = inputMediaEffectGroup->mediaEffects[i]->transition_source_id;
    }
    
    mOutputVideoOptions = outputProduct->videoOptions;
    mOutputAudioOptions = outputProduct->audioOptions;
    mOutputUrl = strdup(outputProduct->url);
    mOutputProductType = outputProduct->type;
    
    mAsyncPrepareEvent = new MediaTimelineMergerEvent(this, &MediaTimelineMerger::onPrepareAsyncEvent);
    mProcessOnePacketEvent = new MediaTimelineMergerEvent(this, &MediaTimelineMerger::onProcessOnePacketEvent);
    mStopEvent = new MediaTimelineMergerEvent(this, &MediaTimelineMerger::onStopEvent);
    mNotifyEvent = new MediaTimelineMergerEvent(this, &MediaTimelineMerger::onNotifyEvent);
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mStopCondition, NULL);
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();
    
    mFlags = 0;
    
    //input
    mFFmpegReaderForInputMaterial = NULL;
    mAudioDecoderForInputMaterial = NULL;
    mAudioFilterForInputMaterial = NULL;
    mVideoDecoderTypeForInputMaterial = VIDEO_DECODER_FFMPEG;
    mVideoDecoderForInputMaterial = NULL;
    mVideoRotationForInputMaterial = 0;
    mVideoFpsForInputMaterial = 0;
    currentInputMediaMaterial = NULL;
    isInputMediaMaterialOpened = false;
    mInputMaterialIndex = -1;
    isSwitchToNewVideoBaseLineForInputMaterial = false;
    isSwitchToNewAudioBaseLineForInputMaterial = false;
    mLastRealVideoPtsForInputMaterial = 0ll;
    mLastRealAudioPtsForInputMaterial = 0ll;
    
    mImageFrameForInputImageMaterial = NULL;
    
    //output product
    mGPUImageOffScreenRenderForOutputProduct = NULL;
    mColorSpaceConvertForOutputProduct = NULL;
    mVideoEncoderForOutputProduct = NULL;
    mVideoEncoderTypeForOutputProduct = VIDEO_TOOL_BOX;
    
    mAudioFilterForOutputProduct = NULL;
    mAudioPCMDataPoolForOutputProduct = NULL;
    mAudioEncoderForOutputProduct = NULL;
    mAudioEncoderTypeForOutputProduct = AUDIO_TOOL_BOX;
    mMediaMuxerForOutputProduct = NULL;
    
    mCurrentVideoPtsForOutputProduct = 0ll;
    mCurrentAudioPtsForOutputProduct = 0ll;
    
    mTargetVideoFrameForOutputProduct.data = (uint8_t*)malloc(mOutputVideoOptions.videoWidth*mOutputVideoOptions.videoHeight*3/2);
    mTargetVideoFrameForOutputProduct.frameSize = mOutputVideoOptions.videoWidth*mOutputVideoOptions.videoHeight*3/2;
    mTargetVideoFrameForOutputProduct.width = mOutputVideoOptions.videoWidth;
    mTargetVideoFrameForOutputProduct.height = mOutputVideoOptions.videoHeight;
    mTargetVideoFrameForOutputProduct.rotation = 0;
    mTargetVideoFrameForOutputProduct.videoRawType = mOutputVideoOptions.videoRawType;
    
    //output product : gif
    mAnimatedGifCreater = NULL;
}
#else
MediaTimelineMerger::MediaTimelineMerger(MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup ,MediaProduct* outputProduct)
{
    mInputMediaMaterialGroup = new MediaMaterialGroup;
    mInputMediaMaterialGroup->mediaMaterialNum = inputMediaMaterialGroup->mediaMaterialNum;
    for (int i = 0; i < inputMediaMaterialGroup->mediaMaterialNum; i++) {
        mInputMediaMaterialGroup->mediaMaterials[i] = new MediaMaterial;
        mInputMediaMaterialGroup->mediaMaterials[i]->iD = inputMediaMaterialGroup->mediaMaterials[i]->iD;
        mInputMediaMaterialGroup->mediaMaterials[i]->type = inputMediaMaterialGroup->mediaMaterials[i]->type;
        mInputMediaMaterialGroup->mediaMaterials[i]->url = strdup(inputMediaMaterialGroup->mediaMaterials[i]->url);
        mInputMediaMaterialGroup->mediaMaterials[i]->startPos = inputMediaMaterialGroup->mediaMaterials[i]->startPos;
        mInputMediaMaterialGroup->mediaMaterials[i]->endPos = inputMediaMaterialGroup->mediaMaterials[i]->endPos;
        mInputMediaMaterialGroup->mediaMaterials[i]->volume = inputMediaMaterialGroup->mediaMaterials[i]->volume;
        mInputMediaMaterialGroup->mediaMaterials[i]->speed = inputMediaMaterialGroup->mediaMaterials[i]->speed;
    }
    
    mInputMediaEffectGroup = new MediaEffectGroup;
    mInputMediaEffectGroup->mediaEffectNum = inputMediaEffectGroup->mediaEffectNum;
    for (int i = 0; i<inputMediaEffectGroup->mediaEffectNum; i++) {
        mInputMediaEffectGroup->mediaEffects[i] = new MediaEffect;
        mInputMediaEffectGroup->mediaEffects[i]->iD = inputMediaEffectGroup->mediaEffects[i]->iD;
        mInputMediaEffectGroup->mediaEffects[i]->type = inputMediaEffectGroup->mediaEffects[i]->type;
        if (inputMediaEffectGroup->mediaEffects[i]->url) {
            mInputMediaEffectGroup->mediaEffects[i]->url = strdup(inputMediaEffectGroup->mediaEffects[i]->url);
        }
        mInputMediaEffectGroup->mediaEffects[i]->effect_in_pos = inputMediaEffectGroup->mediaEffects[i]->effect_in_pos;
        mInputMediaEffectGroup->mediaEffects[i]->effect_out_pos = inputMediaEffectGroup->mediaEffects[i]->effect_out_pos;
        mInputMediaEffectGroup->mediaEffects[i]->startPos = inputMediaEffectGroup->mediaEffects[i]->startPos;
        mInputMediaEffectGroup->mediaEffects[i]->endPos = inputMediaEffectGroup->mediaEffects[i]->endPos;
        mInputMediaEffectGroup->mediaEffects[i]->volume = inputMediaEffectGroup->mediaEffects[i]->volume;
        mInputMediaEffectGroup->mediaEffects[i]->speed = inputMediaEffectGroup->mediaEffects[i]->speed;
        
        mInputMediaEffectGroup->mediaEffects[i]->x = inputMediaEffectGroup->mediaEffects[i]->x;
        mInputMediaEffectGroup->mediaEffects[i]->y = inputMediaEffectGroup->mediaEffects[i]->y;
        mInputMediaEffectGroup->mediaEffects[i]->rotation = inputMediaEffectGroup->mediaEffects[i]->rotation;
        mInputMediaEffectGroup->mediaEffects[i]->scale = inputMediaEffectGroup->mediaEffects[i]->scale;
        mInputMediaEffectGroup->mediaEffects[i]->flipHorizontal = inputMediaEffectGroup->mediaEffects[i]->flipHorizontal;
        mInputMediaEffectGroup->mediaEffects[i]->flipVertical = inputMediaEffectGroup->mediaEffects[i]->flipVertical;
        
        if (inputMediaEffectGroup->mediaEffects[i]->text) {
            mInputMediaEffectGroup->mediaEffects[i]->text = strdup(inputMediaEffectGroup->mediaEffects[i]->text);
        }
        if (inputMediaEffectGroup->mediaEffects[i]->fontLibPath) {
            mInputMediaEffectGroup->mediaEffects[i]->fontLibPath = strdup(inputMediaEffectGroup->mediaEffects[i]->fontLibPath);
        }
        mInputMediaEffectGroup->mediaEffects[i]->fontSize = inputMediaEffectGroup->mediaEffects[i]->fontSize;
        mInputMediaEffectGroup->mediaEffects[i]->fontColor = inputMediaEffectGroup->mediaEffects[i]->fontColor;
        mInputMediaEffectGroup->mediaEffects[i]->leftMargin = inputMediaEffectGroup->mediaEffects[i]->leftMargin;
        mInputMediaEffectGroup->mediaEffects[i]->rightMargin = inputMediaEffectGroup->mediaEffects[i]->rightMargin;
        mInputMediaEffectGroup->mediaEffects[i]->topMargin = inputMediaEffectGroup->mediaEffects[i]->topMargin;
        mInputMediaEffectGroup->mediaEffects[i]->bottomMargin = inputMediaEffectGroup->mediaEffects[i]->bottomMargin;
        mInputMediaEffectGroup->mediaEffects[i]->lineSpace = inputMediaEffectGroup->mediaEffects[i]->lineSpace;
        mInputMediaEffectGroup->mediaEffects[i]->wordSpace = inputMediaEffectGroup->mediaEffects[i]->wordSpace;
        mInputMediaEffectGroup->mediaEffects[i]->text_animation_type = inputMediaEffectGroup->mediaEffects[i]->text_animation_type;

        mInputMediaEffectGroup->mediaEffects[i]->gpu_image_filter_type = inputMediaEffectGroup->mediaEffects[i]->gpu_image_filter_type;
        mInputMediaEffectGroup->mediaEffects[i]->gpu_image_transition_type = inputMediaEffectGroup->mediaEffects[i]->gpu_image_transition_type;
        mInputMediaEffectGroup->mediaEffects[i]->transition_source_id = inputMediaEffectGroup->mediaEffects[i]->transition_source_id;
    }
    
    mOutputVideoOptions = outputProduct->videoOptions;
    mOutputAudioOptions = outputProduct->audioOptions;
    mOutputUrl = strdup(outputProduct->url);
    mOutputProductType = outputProduct->type;
    
    mAsyncPrepareEvent = new MediaTimelineMergerEvent(this, &MediaTimelineMerger::onPrepareAsyncEvent);
    mProcessOnePacketEvent = new MediaTimelineMergerEvent(this, &MediaTimelineMerger::onProcessOnePacketEvent);
    mStopEvent = new MediaTimelineMergerEvent(this, &MediaTimelineMerger::onStopEvent);
    mNotifyEvent = new MediaTimelineMergerEvent(this, &MediaTimelineMerger::onNotifyEvent);
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mStopCondition, NULL);
    
    mQueue.start();
    
    mFlags = 0;
    
    //input
    mFFmpegReaderForInputMaterial = NULL;
    mAudioDecoderForInputMaterial = NULL;
    mAudioFilterForInputMaterial = NULL;
    mVideoDecoderTypeForInputMaterial = VIDEO_DECODER_FFMPEG;
    mVideoDecoderForInputMaterial = NULL;
    mVideoRotationForInputMaterial = 0;
    mVideoFpsForInputMaterial = 0;
    currentInputMediaMaterial = NULL;
    isInputMediaMaterialOpened = false;
    mInputMaterialIndex = -1;
    isSwitchToNewVideoBaseLineForInputMaterial = false;
    isSwitchToNewAudioBaseLineForInputMaterial = false;
    mLastRealVideoPtsForInputMaterial = 0ll;
    mLastRealAudioPtsForInputMaterial = 0ll;
    
    mImageFrameForInputImageMaterial = NULL;
    
    //output product
    mGPUImageOffScreenRenderForOutputProduct = NULL;
    mColorSpaceConvertForOutputProduct = NULL;
    
    //output product : mp4
    mVideoEncoderForOutputProduct = NULL;
    mVideoEncoderTypeForOutputProduct = VIDEO_TOOL_BOX;
    
    mAudioFilterForOutputProduct = NULL;
    mAudioPCMDataPoolForOutputProduct = NULL;
    mAudioEncoderForOutputProduct = NULL;
    mAudioEncoderTypeForOutputProduct = AUDIO_TOOL_BOX;
    mMediaMuxerForOutputProduct = NULL;
    
    mCurrentVideoPtsForOutputProduct = 0ll;
    mCurrentAudioPtsForOutputProduct = 0ll;
    
    mTargetVideoFrameForOutputProduct.data = (uint8_t*)malloc(mOutputVideoOptions.videoWidth*mOutputVideoOptions.videoHeight*4);
    mTargetVideoFrameForOutputProduct.frameSize = mOutputVideoOptions.videoWidth*mOutputVideoOptions.videoHeight*4;
    mTargetVideoFrameForOutputProduct.width = mOutputVideoOptions.videoWidth;
    mTargetVideoFrameForOutputProduct.height = mOutputVideoOptions.videoHeight;
    mTargetVideoFrameForOutputProduct.rotation = 0;
    mTargetVideoFrameForOutputProduct.videoRawType = mOutputVideoOptions.videoRawType;
    
    //output product : gif
    mAnimatedGifCreater = NULL;
}
#endif

MediaTimelineMerger::~MediaTimelineMerger()
{
    stop();
    
    mQueue.stop(true);
    
    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
    
    if (mProcessOnePacketEvent!=NULL) {
        delete mProcessOnePacketEvent;
        mProcessOnePacketEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    if (mOutputUrl) {
        free(mOutputUrl);
        mOutputUrl = NULL;
    }
    
    if (mInputMediaMaterialGroup!=NULL) {
        mInputMediaMaterialGroup->Free();
        delete mInputMediaMaterialGroup;
        mInputMediaMaterialGroup = NULL;
    }
    
    if (mInputMediaEffectGroup!=NULL) {
        mInputMediaEffectGroup->Free();
        delete mInputMediaEffectGroup;
        mInputMediaEffectGroup = NULL;
    }
    
    if (mTargetVideoFrameForOutputProduct.data!=NULL) {
        free(mTargetVideoFrameForOutputProduct.data);
        mTargetVideoFrameForOutputProduct.data = NULL;
    }
    
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mStopCondition);
}

#ifdef ANDROID
void MediaTimelineMerger::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    modifyFlags(INITIALIZED, SET);
    
}
#endif

#ifdef IOS
void MediaTimelineMerger::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new iOSMediaListener(listener,arg);
    
    modifyFlags(INITIALIZED, SET);
}
#endif

void MediaTimelineMerger::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void MediaTimelineMerger::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
    
    switch (event) {
        case MEDIA_PROCESSER_ERROR:
            notifyListener_l(event, ext1, ext2);
            stop_l();
            break;
        case MEDIA_PROCESSER_INFO:
            notifyListener_l(event, ext1, ext2);
            break;
            
        default:
            notifyListener_l(event, ext1, ext2);
            break;
    }
}

void MediaTimelineMerger::start()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & CONNECTING) {
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    return prepareAsync_l();
}

void MediaTimelineMerger::prepareAsync_l()
{
    modifyFlags(END, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(CONNECTING, SET);
    
    mQueue.postEvent(mAsyncPrepareEvent);
}

void MediaTimelineMerger::onPrepareAsyncEvent()
{
    AutoLock autoLock(&mLock);
    
    bool ret = open_ouput_pipelines_l(mOutputProductType);
    
    if (ret) {
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(CONNECTED, SET);
        
        play_l();
    }else{
        stop_l();
    }
}

bool MediaTimelineMerger::open_input_material_pipelines_l(MediaMaterial * inputMediaMaterial)
{
    if (inputMediaMaterial->type == MEDIA_MATERIAL_TYPE_VIDEO_AUDIO || inputMediaMaterial->type == MEDIA_MATERIAL_TYPE_VIDEO || inputMediaMaterial->type == MEDIA_MATERIAL_TYPE_AUDIO) {
        //open input file
        mFFmpegReaderForInputMaterial = new FFmpegReader(inputMediaMaterial->url);
        bool ret = mFFmpegReaderForInputMaterial->open();
        if (!ret) {
            LOGE("Open Input File Fail : %s",inputMediaMaterial->url);
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
            return false;
        }
        
        mSeekStatusForInputMaterial = mFFmpegReaderForInputMaterial->seek(inputMediaMaterial->startPos);
        if (!mSeekStatusForInputMaterial.seekRet) {
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_SEEK_INPUT_FILE_FAIL);
            return false;
        }
        
        mFoundStartPosForInputMaterial = false;
        mCurrentVideoDurationUsForInputMaterial = 0ll;
        mCurrentAudioDurationUsForInputMaterial = 0ll;
        
        AVStream* videoStreamContext = mFFmpegReaderForInputMaterial->getVideoStreamContext();
        if (videoStreamContext!=NULL) {
            
            mVideoFpsForInputMaterial = mFFmpegReaderForInputMaterial->getFps();
            
            AVDictionaryEntry *m = NULL;
            while((m=av_dict_get(videoStreamContext->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
                printf("%s : %s\n", m->key,m->value);
                
                if(strcmp(m->key, "rotate")) continue;
                else{
                    int rotate = atoi(m->value);
                    mVideoRotationForInputMaterial = rotate;
                }
            }
            
            //open video decoder
#ifdef ANDROID
            mVideoDecoderTypeForInputMaterial = VIDEO_DECODER_FFMPEG;
            if(mVideoDecoderTypeForInputMaterial==VIDEO_DECODER_MEDIACODEC_JAVA)
            {
                mVideoDecoderForInputMaterial = VideoDecoder::CreateVideoDecoderWithJniEnv(mVideoDecoderTypeForInputMaterial, mJvm, NULL);
            }else{
                mVideoDecoderForInputMaterial = VideoDecoder::CreateVideoDecoder(mVideoDecoderTypeForInputMaterial);
            }
#endif
            
#ifdef IOS
            mVideoDecoderTypeForInputMaterial=VIDEO_DECODER_VIDEOTOOLBOX;
            mVideoDecoderForInputMaterial = VideoDecoder::CreateVideoDecoder(mVideoDecoderTypeForInputMaterial);
#endif
            
            if (!mVideoDecoderForInputMaterial->open(videoStreamContext)) {
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_VIDEO_DECODER_FAIL, 0);
                return false;
            }
        }
        
        AVStream* audioStreamContext = mFFmpegReaderForInputMaterial->getAudioStreamContext();
        if (audioStreamContext!=NULL) {
            //open audio decoder
            mAudioDecoderForInputMaterial = AudioDecoder::CreateAudioDecoder(AUDIO_DECODER_FFMPEG);
            if (!mAudioDecoderForInputMaterial->open(audioStreamContext)) {
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_AUDIO_DECODER_FAIL, 0);
                return false;
            }
            
            //open audio preprocess
            mAudioFilterForInputMaterial = AudioFilter::CreateAudioFilter(AUDIO_FILTER_FFMPEG);
            char afiltersDesc[256];
            afiltersDesc[0] = 0;
            if (inputMediaMaterial->speed>=0.5f && inputMediaMaterial->speed<=2.0f) {
                av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,volume=%f", inputMediaMaterial->speed, inputMediaMaterial->volume);
            }else if(inputMediaMaterial->speed<0.5f){
                float atempo2 = 0.0f;
                atempo2 = inputMediaMaterial->speed/0.5f;
                if (atempo2<0.5f) {
                    atempo2 = 0.5f;
                }
                
                av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 0.5f, atempo2, inputMediaMaterial->volume);
            }else if (inputMediaMaterial->speed>2.0f) {
                float atempo2 = 0.0f;
                atempo2 = inputMediaMaterial->speed/2.0f;
                if (atempo2>2.0f) {
                    atempo2 = 2.0f;
                }
                
                av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 2.0f, atempo2, inputMediaMaterial->volume);
            }
            bool ret = mAudioFilterForInputMaterial->open(afiltersDesc, audioStreamContext->codec->channel_layout, audioStreamContext->codec->channels, audioStreamContext->codec->sample_rate, audioStreamContext->codec->sample_fmt, AV_CH_LAYOUT_MONO, mOutputAudioOptions.audioNumChannels, mOutputAudioOptions.audioSampleRate, AV_SAMPLE_FMT_S16);
            
            if (!ret) {
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_AUDIO_PREPROCESS_FAIL, 0);
                return false;
            }
            
//            mAudioPCMDataPoolForInputMaterial = new AudioPCMDataPool(mOutputAudioOptions.audioSampleRate, mOutputAudioOptions.audioNumChannels*2, 1000);
        }
        
        return true;

    }else if(inputMediaMaterial->type == MEDIA_MATERIAL_TYPE_PNG || inputMediaMaterial->type == MEDIA_MATERIAL_TYPE_JPEG)
    {
        //open image input pipelines
        bool bret = false;
        if (inputMediaMaterial->type==MEDIA_MATERIAL_TYPE_PNG) {
            mImageFrameForInputImageMaterial = PNGImageFileToRGBAVideoFrame(inputMediaMaterial->url);
            if (mImageFrameForInputImageMaterial!=NULL) {
                bret = true;
            }
        }
        
        if (!bret) {
            LOGE("Open Input File Fail : %s",inputMediaMaterial->url);
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
            return false;
        }
        
        return true;
    }else{
        LOGE("Unknown MediaMaterial Type : %d",inputMediaMaterial->type);
        LOGE("Open Input File Fail : %s",inputMediaMaterial->url);
        notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
        return false;
    }
}

void MediaTimelineMerger::close_input_material_pipelines_l(MediaMaterial * inputMediaMaterial)
{
    if(inputMediaMaterial==NULL) return;
    
    if (inputMediaMaterial->type == MEDIA_MATERIAL_TYPE_VIDEO_AUDIO || inputMediaMaterial->type == MEDIA_MATERIAL_TYPE_VIDEO || inputMediaMaterial->type == MEDIA_MATERIAL_TYPE_AUDIO)
    {
        if (mAudioFilterForInputMaterial!=NULL) {
            mAudioFilterForInputMaterial->dispose();
            AudioFilter::DeleteAudioFilter(AUDIO_FILTER_FFMPEG, mAudioFilterForInputMaterial);
            mAudioFilterForInputMaterial = NULL;
        }
        
        if (mAudioDecoderForInputMaterial!=NULL) {
            mAudioDecoderForInputMaterial->dispose();
            AudioDecoder::DeleteAudioDecoder(mAudioDecoderForInputMaterial, AUDIO_DECODER_FFMPEG);
            mAudioDecoderForInputMaterial = NULL;
        }
        
        if (mVideoDecoderForInputMaterial!=NULL) {
            mVideoDecoderForInputMaterial->dispose();
            VideoDecoder::DeleteVideoDecoder(mVideoDecoderForInputMaterial, mVideoDecoderTypeForInputMaterial);
            mVideoDecoderForInputMaterial = NULL;
        }
        
        if (mFFmpegReaderForInputMaterial!=NULL) {
            mFFmpegReaderForInputMaterial->close();
            delete mFFmpegReaderForInputMaterial;
            mFFmpegReaderForInputMaterial = NULL;
        }
    }else if(inputMediaMaterial->type == MEDIA_MATERIAL_TYPE_PNG || inputMediaMaterial->type == MEDIA_MATERIAL_TYPE_JPEG)
    {
        if (mImageFrameForInputImageMaterial!=NULL) {
            if (mImageFrameForInputImageMaterial->data!=NULL) {
                free(mImageFrameForInputImageMaterial->data);
                mImageFrameForInputImageMaterial->data = NULL;
            }
            
            delete mImageFrameForInputImageMaterial;
            mImageFrameForInputImageMaterial = NULL;
        }
    }else
    {
    
    }
}

bool MediaTimelineMerger::open_input_effect_pipelines_l(MediaEffect *inputMediaEffect)
{
    if (inputMediaEffect->type == MEDIA_EFFECT_TYPE_AUDIO) {
        
        FFmpegReader *ffmpegReaderForBackGroundSound = new FFmpegReader(inputMediaEffect->url);
        bool ret = ffmpegReaderForBackGroundSound->open();
        if (!ret) {
            delete ffmpegReaderForBackGroundSound;
            LOGE("Open Input File Fail : %s",inputMediaEffect->url);
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
            return false;
        }
        AVStream* audioStreamContext = ffmpegReaderForBackGroundSound->getAudioStreamContext();
        if (audioStreamContext==NULL) {
            delete ffmpegReaderForBackGroundSound;
            LOGE("Open Input File Fail : %s",inputMediaEffect->url);
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
            return false;
        }
        
        FFSeekStatus seekStatus = ffmpegReaderForBackGroundSound->seek(inputMediaEffect->startPos);
        if (!seekStatus.seekRet) {
            ffmpegReaderForBackGroundSound->close();
            delete ffmpegReaderForBackGroundSound;
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_SEEK_INPUT_FILE_FAIL);
            return false;
        }

        AudioDecoder *audioDecoderForBackGroundSound = AudioDecoder::CreateAudioDecoder(AUDIO_DECODER_FFMPEG);
        if (!audioDecoderForBackGroundSound->open(audioStreamContext)) {
            AudioDecoder::DeleteAudioDecoder(audioDecoderForBackGroundSound, AUDIO_DECODER_FFMPEG);
            
            ffmpegReaderForBackGroundSound->close();
            delete ffmpegReaderForBackGroundSound;
            
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_AUDIO_DECODER_FAIL, 0);
            return false;
        }
        
        AudioFilter *audioFilterForBackGroundSound = AudioFilter::CreateAudioFilter(AUDIO_FILTER_FFMPEG);
        char afiltersDesc[256];
        afiltersDesc[0] = 0;
        if (inputMediaEffect->speed>=0.5f && inputMediaEffect->speed<=2.0f) {
            av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,volume=%f", inputMediaEffect->speed, inputMediaEffect->volume);
        }else if(inputMediaEffect->speed<0.5f){
            float atempo2 = 0.0f;
            atempo2 = inputMediaEffect->speed/0.5f;
            if (atempo2<0.5f) {
                atempo2 = 0.5f;
            }
            
            av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 0.5f, atempo2, inputMediaEffect->volume);
        }else if (inputMediaEffect->speed>2.0f) {
            float atempo2 = 0.0f;
            atempo2 = inputMediaEffect->speed/2.0f;
            if (atempo2>2.0f) {
                atempo2 = 2.0f;
            }
            
            av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 2.0f, atempo2, inputMediaEffect->volume);
        }
        
        ret = audioFilterForBackGroundSound->open(afiltersDesc, audioStreamContext->codec->channel_layout, audioStreamContext->codec->channels, audioStreamContext->codec->sample_rate, audioStreamContext->codec->sample_fmt, AV_CH_LAYOUT_MONO, mOutputAudioOptions.audioNumChannels, mOutputAudioOptions.audioSampleRate, AV_SAMPLE_FMT_S16);
        
        if (!ret) {
            audioFilterForBackGroundSound->dispose();
            AudioFilter::DeleteAudioFilter(AUDIO_FILTER_FFMPEG, audioFilterForBackGroundSound);
            
            audioDecoderForBackGroundSound->dispose();
            AudioDecoder::DeleteAudioDecoder(audioDecoderForBackGroundSound, AUDIO_DECODER_FFMPEG);
            
            ffmpegReaderForBackGroundSound->close();
            delete ffmpegReaderForBackGroundSound;
            
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_AUDIO_PREPROCESS_FAIL, 0);
            return false;
        }
        
        AudioPCMDataPool *audioPCMDataPoolForBackGroundSound = new AudioPCMDataPool(mOutputAudioOptions.audioSampleRate, mOutputAudioOptions.audioNumChannels*2, 1000);
        
        BackGroundSoundContext *backGroundSoundContext = new BackGroundSoundContext;
        backGroundSoundContext->ffmpegReaderForBackGroundSound = ffmpegReaderForBackGroundSound;
        backGroundSoundContext->audioDecoderForBackGroundSound = audioDecoderForBackGroundSound;
        backGroundSoundContext->audioFilterForBackGroundSound = audioFilterForBackGroundSound;
        backGroundSoundContext->audioPCMDataPoolForBackGroundSound = audioPCMDataPoolForBackGroundSound;
        
        inputMediaEffect->opaque = backGroundSoundContext;

        return true;
    }else if(inputMediaEffect->type == MEDIA_EFFECT_TYPE_PNG || inputMediaEffect->type == MEDIA_EFFECT_TYPE_JPEG)
    {
        //open image input pipelines
        bool bret = false;
        if (inputMediaEffect->type==MEDIA_EFFECT_TYPE_PNG) {
            inputMediaEffect->opaque = (VideoFrame*)PNGImageFileToRGBAVideoFrame(inputMediaEffect->url);
            if (inputMediaEffect->opaque!=NULL) {
                bret = true;
            }
        }
        
        if (!bret) {
            LOGE("Open Input File Fail : %s",inputMediaEffect->url);
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
            return false;
        }
        
        return true;
    }else if(inputMediaEffect->type == MEDIA_EFFECT_TYPE_TEXT)
    {
        //open Word2Bitmap
        Word2Bitmap *pWord2Bitmap = new Word2Bitmap(inputMediaEffect->fontLibPath, inputMediaEffect->fontSize, inputMediaEffect->fontColor);
        bool ret = pWord2Bitmap->initialize();
        if (!ret) {
            delete pWord2Bitmap;
            LOGE("Open Input File Fail : %s",inputMediaEffect->url);
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
            return false;
        }
        
        TextContext *textContext = new TextContext;
        TextInfo *textInfo = new TextInfo;
        
        size_t size = strlen(inputMediaEffect->text) + 1;
        wchar_t* inputWText = new wchar_t[size];
        mbstowcs(inputWText, inputMediaEffect->text, size);

        if (inputMediaEffect->url==NULL) {
            //no text background
            //fontSize is constant, but transparent background lay Size is variable
            int i = -1;
            int MaxWidth = 0;
            int lineWidth = inputMediaEffect->leftMargin;
            int MaxHeight = inputMediaEffect->topMargin;
            while(inputWText[++i]!=L'\0')
            {
                if(inputWText[i]==L'\n')
                {
                    MaxHeight += inputMediaEffect->fontSize + inputMediaEffect->lineSpace;
                    lineWidth = lineWidth - inputMediaEffect->wordSpace + inputMediaEffect->rightMargin;
                    
                    if (lineWidth > MaxWidth) {
                        MaxWidth = lineWidth;
                    }
                    
                    lineWidth = inputMediaEffect->leftMargin;
                }else if(inputWText[i]==L' ')
                {
                    lineWidth += inputMediaEffect->fontSize/2 + inputMediaEffect->wordSpace;
                    
                    if (lineWidth-inputMediaEffect->wordSpace > MaxWidth) {
                        MaxWidth = lineWidth-inputMediaEffect->wordSpace;
                    }
                }else{
                    VideoFrame* wordBitmap = pWord2Bitmap->word2Bitmap(inputWText[i], 1.0f);
                    
                    WordInfo *wordInfo = new WordInfo;
                    wordInfo->wordBitmap = wordBitmap;
                    textInfo->words.push_back(wordInfo);
                    textInfo->wordCount++;
                    
                    int word_W = wordBitmap->width;
                    lineWidth += word_W + inputMediaEffect->wordSpace;
                    
                    if (lineWidth-inputMediaEffect->wordSpace > MaxWidth) {
                        MaxWidth = lineWidth-inputMediaEffect->wordSpace;
                    }
                }
            }
            
            MaxHeight += inputMediaEffect->fontSize + inputMediaEffect->bottomMargin;
            
            VideoFrame *videoFrame = new VideoFrame;
            videoFrame->width = MaxWidth;
            videoFrame->height = MaxHeight;
            videoFrame->frameSize = videoFrame->width * videoFrame->height * 4;
            videoFrame->data = (uint8_t*)malloc(videoFrame->frameSize);
            memset(videoFrame->data, 0, videoFrame->frameSize);
            
            textContext->backGroundLayFrame = videoFrame;
        }else{
            //open text background image
            VideoFrame *videoFrame = (VideoFrame*)PNGImageFileToRGBAVideoFrame(inputMediaEffect->url);
            if (videoFrame==NULL) {
                
                if (pWord2Bitmap) {
                    pWord2Bitmap->terminate();
                    delete pWord2Bitmap;
                    pWord2Bitmap = NULL;
                }
                
                if (textContext) {
                    delete textContext;
                    textContext = NULL;
                }
                
                if (textInfo) {
                    delete textInfo;
                    textInfo = NULL;
                }
                
                if (inputWText) {
                    free(inputWText);
                    inputWText = NULL;
                }
                LOGE("Open Input File Fail : %s",inputMediaEffect->url);
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
                return false;
            }
        
            textContext->backGroundLayFrame = videoFrame;

            //background lay Size is constant, but fontSize is variable [undo]
            int i = -1;
            while(inputWText[++i]!=L'\0')
            {
                if(inputWText[i]==L'\n')
                {
                    continue;
                }else if(inputWText[i]==L' ')
                {
                    continue;
                }else{
                    VideoFrame* wordBitmap = pWord2Bitmap->word2Bitmap(inputWText[i], 1.0f);
                    
                    WordInfo *wordInfo = new WordInfo;
                    wordInfo->wordBitmap = wordBitmap;
                    textInfo->words.push_back(wordInfo);
                    textInfo->wordCount++;
                }
            }
        }
        
        if (pWord2Bitmap) {
            pWord2Bitmap->terminate();
            delete pWord2Bitmap;
            pWord2Bitmap = NULL;
        }
        
        // calculate all words coordinate
        int i = -1;
        int x = inputMediaEffect->leftMargin;
        int y = inputMediaEffect->topMargin + inputMediaEffect->fontSize/2;
        int wordIndex = -1;
        while(inputWText[++i]!=L'\0')
        {
            if(inputWText[i]==L'\n')
            {
                x = inputMediaEffect->leftMargin;
                y = y + inputMediaEffect->fontSize + inputMediaEffect->lineSpace;
                
                if(y+inputMediaEffect->fontSize/2+inputMediaEffect->bottomMargin>textContext->backGroundLayFrame->height)
                {
                    break;
                }else continue;
            }else if(inputWText[i]==L' ')
            {
                if(x + inputMediaEffect->fontSize/2 + inputMediaEffect->rightMargin>textContext->backGroundLayFrame->width)
                {
                    x = inputMediaEffect->leftMargin + inputMediaEffect->fontSize/2 + inputMediaEffect->wordSpace;
                    y = y + inputMediaEffect->fontSize + inputMediaEffect->lineSpace;
                    
                    if(y+inputMediaEffect->fontSize/2+inputMediaEffect->bottomMargin>textContext->backGroundLayFrame->height)
                    {
                        break;
                    }else continue;
                }else{
                    x = x + inputMediaEffect->fontSize/2 + inputMediaEffect->wordSpace;
                    
                    if(x + inputMediaEffect->rightMargin>=textContext->backGroundLayFrame->width)
                    {
                        x = inputMediaEffect->leftMargin;
                        y = y + inputMediaEffect->fontSize + inputMediaEffect->lineSpace;
                        
                        if(y+inputMediaEffect->fontSize/2+inputMediaEffect->bottomMargin>textContext->backGroundLayFrame->height)
                        {
                            break;
                        }else continue;
                    }else continue;
                }
            }else{
                
                wordIndex++;
                WordInfo *wordInfo = textInfo->words[wordIndex];
                int word_W = wordInfo->wordBitmap->width;
                
                if(x + word_W + inputMediaEffect->rightMargin>textContext->backGroundLayFrame->width)
                {
                    x = inputMediaEffect->leftMargin + word_W/2;
                    y = y + inputMediaEffect->fontSize + inputMediaEffect->lineSpace;
                    
                    if(y + inputMediaEffect->fontSize/2+inputMediaEffect->bottomMargin>textContext->backGroundLayFrame->height)
                    {
                        break;
                    }
                    
                    wordInfo->x = x;
                    wordInfo->y = y;
                    wordInfo->isInRenderRect = true;

                    x = x + word_W/2 + inputMediaEffect->wordSpace;
                    
                    continue;
                    
                }else{
                    x = x + word_W/2;

                    wordInfo->x = x;
                    wordInfo->y = y;
                    wordInfo->isInRenderRect = true;
                    
                    x = x + word_W/2 + inputMediaEffect->wordSpace;
                    
                    if(x + inputMediaEffect->rightMargin>=textContext->backGroundLayFrame->width)
                    {
                        x = inputMediaEffect->leftMargin;
                        y = y + inputMediaEffect->fontSize + inputMediaEffect->lineSpace;
                        
                        if(y+inputMediaEffect->fontSize/2+inputMediaEffect->bottomMargin>textContext->backGroundLayFrame->height)
                        {
                            break;
                        }else continue;
                    }else continue;
                }
            }
        }
        
        if (inputWText) {
            free(inputWText);
            inputWText = NULL;
        }
        
        textContext->textInfo = textInfo;
        inputMediaEffect->opaque = (TextContext*)textContext;
        
        return true;
    }else if(inputMediaEffect->type == MEDIA_EFFECT_TYPE_WEBP){
        
        AnimatedWebp* animatedWebp = new AnimatedWebp(inputMediaEffect->url);
        bool ret = animatedWebp->loadAnimation();
        
        if (!ret) {
            delete animatedWebp;
            LOGE("Open Input File Fail : %s",inputMediaEffect->url);
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
            return false;
        }
        
        inputMediaEffect->opaque = (AnimatedWebp*)animatedWebp;
        
        return true;
    }else if (inputMediaEffect->type == MEDIA_EFFECT_TYPE_GIF)
    {
        AnimatedGif* animatedGif = new AnimatedGif(inputMediaEffect->url);
        bool ret = animatedGif->loadAnimation();
        
        if (!ret) {
            delete animatedGif;
            LOGE("Open Input File Fail : %s",inputMediaEffect->url);
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
            return false;
        }
        
        inputMediaEffect->opaque = (AnimatedGif*)animatedGif;
        
        return true;
    }else{
        LOGE("Open Input File Fail : %s",inputMediaEffect->url);
        notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
        return false;
    }
}

void MediaTimelineMerger::close_input_effect_pipelines_l(MediaEffect *inputMediaEffect)
{
    if (inputMediaEffect->type == MEDIA_EFFECT_TYPE_AUDIO) {
        
        BackGroundSoundContext* backGroundSoundContext = (BackGroundSoundContext*)inputMediaEffect->opaque;
        
        if (backGroundSoundContext!=NULL) {
            if (backGroundSoundContext->audioPCMDataPoolForBackGroundSound!=NULL) {
                delete backGroundSoundContext->audioPCMDataPoolForBackGroundSound;
                backGroundSoundContext->audioPCMDataPoolForBackGroundSound = NULL;
            }
            
            if (backGroundSoundContext->audioFilterForBackGroundSound!=NULL) {
                backGroundSoundContext->audioFilterForBackGroundSound->dispose();
                AudioFilter::DeleteAudioFilter(AUDIO_FILTER_FFMPEG, backGroundSoundContext->audioFilterForBackGroundSound);
                backGroundSoundContext->audioFilterForBackGroundSound = NULL;
            }
            
            if (backGroundSoundContext->audioDecoderForBackGroundSound!=NULL) {
                backGroundSoundContext->audioDecoderForBackGroundSound->dispose();
                AudioDecoder::DeleteAudioDecoder(backGroundSoundContext->audioDecoderForBackGroundSound, AUDIO_DECODER_FFMPEG);
                backGroundSoundContext->audioDecoderForBackGroundSound = NULL;
            }
            
            if (backGroundSoundContext->ffmpegReaderForBackGroundSound!=NULL) {
                backGroundSoundContext->ffmpegReaderForBackGroundSound->close();
                delete backGroundSoundContext->ffmpegReaderForBackGroundSound;
                backGroundSoundContext->ffmpegReaderForBackGroundSound = NULL;
            }
        }

        inputMediaEffect->opaque = NULL;
    }else if(inputMediaEffect->type == MEDIA_EFFECT_TYPE_PNG || inputMediaEffect->type == MEDIA_EFFECT_TYPE_JPEG)
    {
        VideoFrame* imageFrameForStaticImageOverlay = (VideoFrame*)inputMediaEffect->opaque;
        
        if (imageFrameForStaticImageOverlay!=NULL) {
            if (imageFrameForStaticImageOverlay->data!=NULL) {
                free(imageFrameForStaticImageOverlay->data);
                imageFrameForStaticImageOverlay->data = NULL;
            }
            
            delete imageFrameForStaticImageOverlay;
            imageFrameForStaticImageOverlay = NULL;
        }
        
        inputMediaEffect->opaque = NULL;
    }else if (inputMediaEffect->type == MEDIA_EFFECT_TYPE_TEXT)
    {
        TextContext *textContext = (TextContext*)inputMediaEffect->opaque;
        
        if (textContext) {
            if (textContext->backGroundLayFrame) {
                if (textContext->backGroundLayFrame->data) {
                    free(textContext->backGroundLayFrame->data);
                }
                delete textContext->backGroundLayFrame;
            }
            if (textContext->textInfo) {
                textContext->textInfo->Free();
                delete textContext->textInfo;
                textContext->textInfo = NULL;
            }
            
            delete textContext;
        }
        
        inputMediaEffect->opaque = NULL;
    }else if(inputMediaEffect->type == MEDIA_EFFECT_TYPE_WEBP)
    {
        AnimatedWebp* animatedWebp = (AnimatedWebp*)inputMediaEffect->opaque;
        
        if (animatedWebp) {
            animatedWebp->freeAnimation();
            delete animatedWebp;
            animatedWebp = NULL;
        }
        
        inputMediaEffect->opaque = NULL;

    }else if(inputMediaEffect->type == MEDIA_EFFECT_TYPE_GIF)
    {
        AnimatedGif* animatedGif = (AnimatedGif*)inputMediaEffect->opaque;
        if (animatedGif) {
            animatedGif->freeAnimation();
            delete animatedGif;
            animatedGif = NULL;
        }
        
        inputMediaEffect->opaque = NULL;
        
    }else{
    
    }
}

bool MediaTimelineMerger::open_ouput_pipelines_l(MEDIA_PRODUCT_TYPE outputProductType)
{
    if (outputProductType==MEDIA_PRODUCT_TYPE_MP4) {
        //open video preprocess
        if (mOutputVideoOptions.hasVideo) {
            //open GPUImageFilter
#ifdef IOS
            mGPUImageOffScreenRenderForOutputProduct = GPUImageOffScreenRender::CreateGPUImageOffScreenRender(GPU_IMAGE_FILTER, NV12);
#endif
            
#ifdef ANDROID
            mGPUImageOffScreenRenderForOutputProduct = GPUImageOffScreenRender::CreateGPUImageOffScreenRender(GPU_IMAGE_FILTER, I420);
#endif
            
            mGPUImageOffScreenRenderForOutputProduct->initialize(mOutputVideoOptions.videoWidth, mOutputVideoOptions.videoHeight);
            
            mColorSpaceConvertForOutputProduct = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
        }
        
        //open video encoder
        if (mOutputVideoOptions.hasVideo) {
            bool bRet = false;
#ifdef IOS
            mVideoEncoderTypeForOutputProduct = VIDEO_TOOL_BOX;
            
            mVideoEncoderForOutputProduct = VideoEncoder::CreateVideoEncoder(mVideoEncoderTypeForOutputProduct, mOutputVideoOptions.videoFps, mOutputVideoOptions.videoWidth, mOutputVideoOptions.videoHeight, mOutputVideoOptions.videoRawType, VIDEO_ENCODE_PROFILE(mOutputVideoOptions.videoProfile), (VIDEO_ENCODE_MODE)mOutputVideoOptions.encodeMode, mOutputVideoOptions.videoBitRate, mOutputVideoOptions.quality, mOutputVideoOptions.maxKeyFrameIntervalMs, ULTRAFAST, mOutputVideoOptions.videoBitRate, mOutputVideoOptions.bStrictCBR, mOutputVideoOptions.deblockingFilterFactor);
            
            bRet = mVideoEncoderForOutputProduct->Open();
#endif
            
#ifdef ANDROID
            if (mOutputVideoOptions.videoEncodeType==VIDEO_HARD_ENCODE) {
                mVideoEncoderTypeForOutputProduct = MEDIACODEC;
            }else{
                mVideoEncoderTypeForOutputProduct = X264;
            }
            
            mVideoEncoderForOutputProduct = VideoEncoder::CreateVideoEncoder(mVideoEncoderTypeForOutputProduct, mOutputVideoOptions.videoFps, mOutputVideoOptions.videoWidth, mOutputVideoOptions.videoHeight, VIDEO_ENCODE_PROFILE(mOutputVideoOptions.videoProfile), (VIDEO_ENCODE_MODE)mOutputVideoOptions.encodeMode, mOutputVideoOptions.videoBitRate, mOutputVideoOptions.quality, mOutputVideoOptions.maxKeyFrameIntervalMs, ULTRAFAST, mOutputVideoOptions.videoBitRate, mOutputVideoOptions.bStrictCBR, mOutputVideoOptions.deblockingFilterFactor);
            
            mVideoEncoderForOutputProduct->registerJavaVMEnv(mJvm);
            bRet = mVideoEncoderForOutputProduct->Open();
#endif
            
            if (!bRet) {
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_VIDEO_ENCODER_FAIL, 0);
                return false;
            }
        }
        
        // open audio pcm data pool
        if (mOutputAudioOptions.hasAudio) {
            mAudioPCMDataPoolForOutputProduct = new AudioPCMDataPool(mOutputAudioOptions.audioSampleRate, mOutputAudioOptions.audioNumChannels*2, 4000);
        }
        
        //open audio encoder
        if (mOutputAudioOptions.hasAudio) {
            bool bRet = false;
#ifdef ANDROID
            mAudioEncoderTypeForOutputProduct = FAAC;
#endif
            
#ifdef IOS
            mAudioEncoderTypeForOutputProduct = AUDIO_TOOL_BOX;
#endif
            
            mAudioEncoderForOutputProduct = AudioEncoder::CreateAudioEncoder(mAudioEncoderTypeForOutputProduct, mOutputAudioOptions.audioSampleRate, mOutputAudioOptions.audioNumChannels, mOutputAudioOptions.audioBitRate);
            
            bRet = mAudioEncoderForOutputProduct->Open();
            
            if (!bRet) {
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_AUDIO_ENCODER_FAIL, 0);
                return false;
            }
        }
        
        // open output file
        mMediaMuxerForOutputProduct = MediaMuxer::CreateMediaMuxer(FFMPEG_PROCESSER, MP4, mOutputUrl, NULL, &mOutputVideoOptions, &mOutputAudioOptions);
        mMediaMuxerForOutputProduct->setListener(this);
#ifdef ANDROID
        mMediaMuxerForOutputProduct->registerJavaVMEnv(mJvm);
#endif
        int iRet = mMediaMuxerForOutputProduct->prepare();
        if (iRet<0) {
            if (iRet==-2) {
                LOGW("Immediate exit was requested");
            }else{
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_OUTPUT_FILE_FAIL, 0);
            }
            return false;
        }
        mMediaMuxerForOutputProduct->start();
        
        // set VideoEncoder OutputHandler
        if (mOutputVideoOptions.hasVideo)
        {
            mVideoEncoderForOutputProduct->SetOutputHandler(mMediaMuxerForOutputProduct);
        }
        
        // set AudioEncoder OutputHandler
        if (mOutputAudioOptions.hasAudio) {
            mAudioEncoderForOutputProduct->SetOutputHandler(mMediaMuxerForOutputProduct);
        }
    }else if (outputProductType==MEDIA_PRODUCT_TYPE_GIF) {
        //init param
        mLastSendRecordTimeMSForGif = 0ll;
        mLastRecordTimeMSForGif = 0ll;
        
        //open video preprocess
#ifdef IOS
        mGPUImageOffScreenRenderForOutputProduct = GPUImageOffScreenRender::CreateGPUImageOffScreenRender(GPU_IMAGE_FILTER, NV12);
#endif
        
#ifdef ANDROID
        mGPUImageOffScreenRenderForOutputProduct = GPUImageOffScreenRender::CreateGPUImageOffScreenRender(GPU_IMAGE_FILTER, I420);
#endif
        
        mGPUImageOffScreenRenderForOutputProduct->initialize(mOutputVideoOptions.videoWidth, mOutputVideoOptions.videoHeight);
        
        mColorSpaceConvertForOutputProduct = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
        
        //open Animated Gif Creater
        mAnimatedGifCreater = AnimatedGifCreater::CreateAnimatedGifCreater(/*GIF_H*/GIFFLEN, mOutputUrl);
        bool ret = mAnimatedGifCreater->open(mOutputVideoOptions.videoWidth, mOutputVideoOptions.videoHeight, 1000/mOutputVideoOptions.videoFps);
        if (!ret) {
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_OUTPUT_FILE_FAIL, 0);
            return false;
        }
    }else {
        notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_OUTPUT_FILE_FAIL, 0);
        return false;
    }

    return true;
}

void MediaTimelineMerger::close_output_pipelines_l(MEDIA_PRODUCT_TYPE outputProductType)
{
    if (outputProductType==MEDIA_PRODUCT_TYPE_MP4)
    {
        if (mVideoEncoderForOutputProduct!=NULL) {
            mVideoEncoderForOutputProduct->Close();
            VideoEncoder::DeleteVideoEncoder(mVideoEncoderForOutputProduct, mVideoEncoderTypeForOutputProduct);
            mVideoEncoderForOutputProduct = NULL;
        }
        
        if (mAudioEncoderForOutputProduct!=NULL) {
            mAudioEncoderForOutputProduct->Close();
            AudioEncoder::DeleteAudioEncoder(mAudioEncoderForOutputProduct, mAudioEncoderTypeForOutputProduct);
            mAudioEncoderForOutputProduct = NULL;
        }
        
        if (mMediaMuxerForOutputProduct!=NULL) {
            mMediaMuxerForOutputProduct->stop();
            MediaMuxer::DeleteMediaMuxer(mMediaMuxerForOutputProduct, FFMPEG_PROCESSER);
            mMediaMuxerForOutputProduct = NULL;
        }
        
        if (mColorSpaceConvertForOutputProduct!=NULL) {
            ColorSpaceConvert::DeleteColorSpaceConvert(mColorSpaceConvertForOutputProduct, LIBYUV);
            mColorSpaceConvertForOutputProduct = NULL;
        }
        
        if (mGPUImageOffScreenRenderForOutputProduct!=NULL) {
            mGPUImageOffScreenRenderForOutputProduct->terminate();
            GPUImageOffScreenRender::DeleteGPUImageOffScreenRender(mGPUImageOffScreenRenderForOutputProduct, GPU_IMAGE_FILTER);
            mGPUImageOffScreenRenderForOutputProduct = NULL;
        }
        
        if (mAudioPCMDataPoolForOutputProduct!=NULL) {
            delete mAudioPCMDataPoolForOutputProduct;
            mAudioPCMDataPoolForOutputProduct = NULL;
        }
    }else if(outputProductType==MEDIA_PRODUCT_TYPE_GIF) {
        if (mAnimatedGifCreater!=NULL) {
            mAnimatedGifCreater->close();
            AnimatedGifCreater::DeleteAnimatedGifCreater(mAnimatedGifCreater, /*GIF_H*/GIFFLEN);
            mAnimatedGifCreater = NULL;
        }

        if (mColorSpaceConvertForOutputProduct!=NULL) {
            ColorSpaceConvert::DeleteColorSpaceConvert(mColorSpaceConvertForOutputProduct, LIBYUV);
            mColorSpaceConvertForOutputProduct = NULL;
        }

        if (mGPUImageOffScreenRenderForOutputProduct!=NULL) {
            mGPUImageOffScreenRenderForOutputProduct->terminate();
            GPUImageOffScreenRender::DeleteGPUImageOffScreenRender(mGPUImageOffScreenRenderForOutputProduct, GPU_IMAGE_FILTER);
            mGPUImageOffScreenRenderForOutputProduct = NULL;
        }
    }
}

void MediaTimelineMerger::resume()
{
    AutoLock autoLock(&mLock);
    
    play_l();
}

void MediaTimelineMerger::play_l()
{
    if (mFlags & STREAMING) {
        return;
    }
    
    if (!(mFlags & CONNECTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    modifyFlags(CONNECTED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
    
    modifyFlags(STREAMING, SET);
}

void MediaTimelineMerger::pause()
{
    AutoLock autoLock(&mLock);
    
    pause_l();
}

void MediaTimelineMerger::pause_l()
{
    if (!(mFlags & STREAMING)) {
        return;
    }
    
    modifyFlags(STREAMING, CLEAR);
    
    mQueue.cancelEvent(mProcessOnePacketEvent->eventID());
    
    modifyFlags(PAUSED, SET);
    
    return;
}

void MediaTimelineMerger::stop()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & END) {
        return;
    }
    
    if (mFlags & ENDING) {
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
}

void MediaTimelineMerger::stop_l()
{
    modifyFlags(ENDING, ASSIGN);
    
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void MediaTimelineMerger::onStopEvent()
{
    AutoLock autoLock(&mLock);
    
    close_input_material_pipelines_l(currentInputMediaMaterial);
    
    for (int i = 0; i<mInputMediaEffectGroup->mediaEffectNum; i++) {
        if(mInputMediaEffectGroup->mediaEffects[i]->isOpened)
        {
            close_input_effect_pipelines_l(mInputMediaEffectGroup->mediaEffects[i]);
            mInputMediaEffectGroup->mediaEffects[i]->isOpened = false;
        }
    }
    
    close_output_pipelines_l(mOutputProductType);
    
    cancelMergerEvents();
    
    mNotificationQueue.flush();
    
    modifyFlags(END, ASSIGN);
    
    notifyListener_l(MEDIA_PROCESSER_END);
    
    pthread_cond_broadcast(&mStopCondition);
}

void MediaTimelineMerger::notifyListener_l(int event, int ext1, int ext2)
{
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void MediaTimelineMerger::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

void MediaTimelineMerger::cancelMergerEvents()
{
    mQueue.cancelEvent(mProcessOnePacketEvent->eventID());
    mQueue.cancelEvent(mNotifyEvent->eventID());
    mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
    mQueue.cancelEvent(mStopEvent->eventID());
}

void MediaTimelineMerger::onProcessOnePacketEvent()
{
    AutoLock autoLock(&mLock);
    
    if (!isInputMediaMaterialOpened) {
        mInputMaterialIndex++;
        
        if (mInputMaterialIndex>=mInputMediaMaterialGroup->mediaMaterialNum) {
            LOGD("ENDING...");
            
            stop_l();
            
            return;
        }
        
        bool ret = open_input_material_pipelines_l(mInputMediaMaterialGroup->mediaMaterials[mInputMaterialIndex]);
        
        if (!ret) {
            stop_l();
            return;
        }else {
            isInputMediaMaterialOpened = true;
            isSwitchToNewVideoBaseLineForInputMaterial = true;
            isSwitchToNewAudioBaseLineForInputMaterial = true;
            currentInputMediaMaterial = mInputMediaMaterialGroup->mediaMaterials[mInputMaterialIndex];
        }
    }
    
    processAVPacket();
}

void MediaTimelineMerger::processAVPacket()
{
    if (currentInputMediaMaterial->startPos*1000ll + mCurrentVideoDurationUsForInputMaterial>=currentInputMediaMaterial->endPos*1000ll || currentInputMediaMaterial->startPos*1000ll + mCurrentAudioDurationUsForInputMaterial>=currentInputMediaMaterial->endPos*1000ll) {
        
        close_input_material_pipelines_l(currentInputMediaMaterial);
        isInputMediaMaterialOpened = false;
        
        isSwitchToNewVideoBaseLineForInputMaterial = false;
        isSwitchToNewAudioBaseLineForInputMaterial = false;
        
        mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
        return;
    }
    
    FFPacket* ffPacket = mFFmpegReaderForInputMaterial->getMediaPacket();
    if (ffPacket->ret==AVERROR_INVALIDDATA || ffPacket->ret == AVERROR(EAGAIN))
    {
        av_packet_unref(ffPacket->avPacket);
        av_freep(&ffPacket->avPacket);
        
        mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
        return;
    }else if(ffPacket->ret == AVERROR_EOF)
    {
        av_packet_unref(ffPacket->avPacket);
        av_freep(&ffPacket->avPacket);
        
        close_input_material_pipelines_l(currentInputMediaMaterial);
        isInputMediaMaterialOpened = false;
        
        isSwitchToNewVideoBaseLineForInputMaterial = false;
        isSwitchToNewAudioBaseLineForInputMaterial = false;
        
        mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
        return;
    }else if (ffPacket->ret < 0)
    {
        av_packet_unref(ffPacket->avPacket);
        av_freep(&ffPacket->avPacket);
        
        notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_READ_INPUT_FILE_FAIL);
        stop_l();
        
        return;
    }else{
        if (ffPacket->mediaType==0) {
            
            int iret = mVideoDecoderForInputMaterial->decode(ffPacket->avPacket);
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            
            if (iret<0) {
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_VIDEO_DECODE_FAIL);
                stop_l();
                
                return;
            }else if (iret==0) {
                mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                return;
            }else {
                AVFrame* videoFrame = mVideoDecoderForInputMaterial->getFrame();
                if (videoFrame==NULL) {
                    mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                    return;
                }else if(!mFoundStartPosForInputMaterial && videoFrame->pkt_pts<mSeekStatusForInputMaterial.seekTargetPos) {
                    mVideoDecoderForInputMaterial->clearFrame();
                    mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                    return;
                }else{
                    mFoundStartPosForInputMaterial = true;
                    
                    if (isSwitchToNewVideoBaseLineForInputMaterial) {
                        mLastRealVideoPtsForInputMaterial = videoFrame->pts;
                        isSwitchToNewVideoBaseLineForInputMaterial = false;
                    }
                    
                    mCurrentVideoDurationUsForInputMaterial += videoFrame->pts - mLastRealVideoPtsForInputMaterial;
                    
                    mCurrentVideoPtsForOutputProduct = mCurrentVideoPtsForOutputProduct + (videoFrame->pts - mLastRealVideoPtsForInputMaterial)/currentInputMediaMaterial->speed;
                    mLastRealVideoPtsForInputMaterial = videoFrame->pts;
                    videoFrame->pts = mCurrentVideoPtsForOutputProduct;
                    
                    //todo : pkt_duration
                    if (videoFrame->pkt_duration <=0 || videoFrame->pkt_duration > 100000ll) {
                        videoFrame->pkt_duration = 1000000ll/mVideoFpsForInputMaterial;
                    }
                    
                    videoFrame->pkt_duration = videoFrame->pkt_duration / currentInputMediaMaterial->speed;
                    
                    av_dict_set_int(&videoFrame->metadata, "rotate", mVideoRotationForInputMaterial,0);
                    bool ret = videoFlowing_l(videoFrame);
                    av_dict_free(&videoFrame->metadata);
                    
                    mVideoDecoderForInputMaterial->clearFrame();
                    
                    if (!ret) {
                        stop_l();
                        return;
                    }
                    
                    mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                    return;
                }
            }
        }else if(ffPacket->mediaType==1) {
            
            if (mOutputProductType == MEDIA_PRODUCT_TYPE_GIF) {
                av_packet_unref(ffPacket->avPacket);
                av_freep(&ffPacket->avPacket);
                
                mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                return;
            }
            
            if (!mFoundStartPosForInputMaterial) {
                av_packet_unref(ffPacket->avPacket);
                av_freep(&ffPacket->avPacket);
                
                mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                return;
            }
            
            int iret = mAudioDecoderForInputMaterial->decode(ffPacket->avPacket);
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            
            if (iret<0) {
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_AUDIO_DECODE_FAIL);
                stop_l();
                
                return;
            }else if (iret==0) {
                mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                return;
            }else {
                AVFrame* sourceAudioFrame = mAudioDecoderForInputMaterial->getPCMData();
                if (sourceAudioFrame==NULL) {
                    mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                    return;
                }else {
                    calculateAudioFramePts(sourceAudioFrame, mFFmpegReaderForInputMaterial->getAudioStreamContext());
                                        
                    if (isSwitchToNewAudioBaseLineForInputMaterial) {
                        mLastRealAudioPtsForInputMaterial = sourceAudioFrame->pts;
                        isSwitchToNewAudioBaseLineForInputMaterial = false;
                    }
                    
                    mCurrentAudioDurationUsForInputMaterial += sourceAudioFrame->pts - mLastRealAudioPtsForInputMaterial;
                    
                    mCurrentAudioPtsForOutputProduct = mCurrentAudioPtsForOutputProduct + (sourceAudioFrame->pts - mLastRealAudioPtsForInputMaterial) / currentInputMediaMaterial->speed;
                    mLastRealAudioPtsForInputMaterial = sourceAudioFrame->pts;
                    
                    //do filter
                    if(mAudioFilterForInputMaterial->filterIn(sourceAudioFrame))
                    {
                        sourceAudioFrame = mAudioFilterForInputMaterial->filterOut();
                        
                        if (sourceAudioFrame==NULL) {
                            mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                            return;
                        }else{
                            int sourceAudioPcmDataSize = av_samples_get_buffer_size(NULL, av_frame_get_channels(sourceAudioFrame),
                                                                                    sourceAudioFrame->nb_samples,
                                                                                    (enum AVSampleFormat)sourceAudioFrame->format, 1);
                            uint8_t* sourceAudioPcmData = *(sourceAudioFrame->extended_data);
                            
                            if(sourceAudioPcmDataSize>0 && sourceAudioPcmData!=NULL)
                            {
                                bool ret = audioFlowing_l(sourceAudioPcmData, sourceAudioPcmDataSize, mCurrentAudioPtsForOutputProduct);
                                
                                if (!ret) {
                                    stop_l();
                                    return;
                                }
                                
                                mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                                return;
                            }else{
                                mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                                return;
                            }
                        }
                    }else{
                        stop_l();
                        
                        mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                        return;
                    }
                }
            }
        }else{
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            
            mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
            return;
        }
    }
}

bool MediaTimelineMerger::videoFlowing_l(AVFrame* videoFrame)
{
    //input and filter
    GPU_IMAGE_FILTER_TYPE current_gpu_image_filter_type = GPU_IMAGE_FILTER_RGB;
    char* filter_dir = NULL;
    for (int i = 0; i<mInputMediaEffectGroup->mediaEffectNum; i++) {
        if (mInputMediaEffectGroup->mediaEffects[i]->type == MEDIA_EFFECT_TYPE_FILTER && videoFrame->pts>=mInputMediaEffectGroup->mediaEffects[i]->effect_in_pos*1000 && videoFrame->pts<=mInputMediaEffectGroup->mediaEffects[i]->effect_out_pos*1000) {
            current_gpu_image_filter_type = mInputMediaEffectGroup->mediaEffects[i]->gpu_image_filter_type;
            filter_dir = mInputMediaEffectGroup->mediaEffects[i]->url;
            break;
        }
    }
    mGPUImageOffScreenRenderForOutputProduct->filterInputToFrameBufferTexture(videoFrame, (FILTER_TYPE)current_gpu_image_filter_type, filter_dir, mOutputVideoOptions.isAspectFit);
    
    //normal blend
    for (int i = 0; i<mInputMediaEffectGroup->mediaEffectNum; i++) {
        if (mInputMediaEffectGroup->mediaEffects[i]->type == MEDIA_EFFECT_TYPE_PNG || mInputMediaEffectGroup->mediaEffects[i]->type == MEDIA_EFFECT_TYPE_JPEG)
        {
            if (videoFrame->pts>=mInputMediaEffectGroup->mediaEffects[i]->effect_in_pos*1000 && videoFrame->pts<=mInputMediaEffectGroup->mediaEffects[i]->effect_out_pos*1000) {
                if (!mInputMediaEffectGroup->mediaEffects[i]->isOpened) {
                    bool ret = open_input_effect_pipelines_l(mInputMediaEffectGroup->mediaEffects[i]);
                    if (ret) {
                        mInputMediaEffectGroup->mediaEffects[i]->isOpened = true;
                    }else return false;
                }
                
                //do blend
                mGPUImageOffScreenRenderForOutputProduct->normalBlendToFrameBufferTexture((VideoFrame *)mInputMediaEffectGroup->mediaEffects[i]->opaque, mInputMediaEffectGroup->mediaEffects[i]->rotation, mInputMediaEffectGroup->mediaEffects[i]->scale, mInputMediaEffectGroup->mediaEffects[i]->flipHorizontal, mInputMediaEffectGroup->mediaEffects[i]->flipVertical, mInputMediaEffectGroup->mediaEffects[i]->x, mInputMediaEffectGroup->mediaEffects[i]->y);
            }else{
                if (mInputMediaEffectGroup->mediaEffects[i]->isOpened) {
                    close_input_effect_pipelines_l(mInputMediaEffectGroup->mediaEffects[i]);
                    mInputMediaEffectGroup->mediaEffects[i]->isOpened = false;
                }
            }
        }else if(mInputMediaEffectGroup->mediaEffects[i]->type == MEDIA_EFFECT_TYPE_TEXT)
        {
            if (videoFrame->pts>=mInputMediaEffectGroup->mediaEffects[i]->effect_in_pos*1000 && videoFrame->pts<=mInputMediaEffectGroup->mediaEffects[i]->effect_out_pos*1000)
            {
                if (!mInputMediaEffectGroup->mediaEffects[i]->isOpened) {
                    bool ret = open_input_effect_pipelines_l(mInputMediaEffectGroup->mediaEffects[i]);
                    if (ret) {
                        mInputMediaEffectGroup->mediaEffects[i]->isOpened = true;
                    }else return false;
                }
                
                //do text blend
                TextContext *textContext = (TextContext *)mInputMediaEffectGroup->mediaEffects[i]->opaque;
                if (textContext==NULL) {
                    return false;
                }
                TextInfo *textInfo = TextEffectParamToBlendTextInfo(mInputMediaEffectGroup->mediaEffects[i], videoFrame->pts/1000-mInputMediaEffectGroup->mediaEffects[i]->effect_in_pos);
                if (textInfo==NULL) {
                    return false;
                }
                
                mGPUImageOffScreenRenderForOutputProduct->normalBlendToFrameBufferTexture(textContext->backGroundLayFrame, mInputMediaEffectGroup->mediaEffects[i]->rotation, mInputMediaEffectGroup->mediaEffects[i]->scale, mInputMediaEffectGroup->mediaEffects[i]->flipHorizontal, mInputMediaEffectGroup->mediaEffects[i]->flipVertical, mInputMediaEffectGroup->mediaEffects[i]->x, mInputMediaEffectGroup->mediaEffects[i]->y, textInfo);

            }else{
                if (mInputMediaEffectGroup->mediaEffects[i]->isOpened) {
                    close_input_effect_pipelines_l(mInputMediaEffectGroup->mediaEffects[i]);
                    mInputMediaEffectGroup->mediaEffects[i]->isOpened = false;
                }
            }
        }else if (mInputMediaEffectGroup->mediaEffects[i]->type == MEDIA_EFFECT_TYPE_WEBP){
            if (videoFrame->pts>=mInputMediaEffectGroup->mediaEffects[i]->effect_in_pos*1000 && videoFrame->pts<=mInputMediaEffectGroup->mediaEffects[i]->effect_out_pos*1000) {
                if (!mInputMediaEffectGroup->mediaEffects[i]->isOpened) {
                    bool ret = open_input_effect_pipelines_l(mInputMediaEffectGroup->mediaEffects[i]);
                    if (ret) {
                        mInputMediaEffectGroup->mediaEffects[i]->isOpened = true;
                    }else return false;
                }
                
                AnimatedWebp* animatedWebp = (AnimatedWebp*)mInputMediaEffectGroup->mediaEffects[i]->opaque;
                VideoFrame* webpFrame = animatedWebp->getAnimatedFrame(videoFrame->pts/1000-mInputMediaEffectGroup->mediaEffects[i]->effect_in_pos);
                
                if (webpFrame) {
                    //do blend
                    mGPUImageOffScreenRenderForOutputProduct->normalBlendToFrameBufferTexture(webpFrame, mInputMediaEffectGroup->mediaEffects[i]->rotation, mInputMediaEffectGroup->mediaEffects[i]->scale, mInputMediaEffectGroup->mediaEffects[i]->flipHorizontal, mInputMediaEffectGroup->mediaEffects[i]->flipVertical, mInputMediaEffectGroup->mediaEffects[i]->x, mInputMediaEffectGroup->mediaEffects[i]->y);
                }
            }else{
                if (mInputMediaEffectGroup->mediaEffects[i]->isOpened) {
                    close_input_effect_pipelines_l(mInputMediaEffectGroup->mediaEffects[i]);
                    mInputMediaEffectGroup->mediaEffects[i]->isOpened = false;
                }
            }
        }else if (mInputMediaEffectGroup->mediaEffects[i]->type == MEDIA_EFFECT_TYPE_GIF)
        {
            if (videoFrame->pts>=mInputMediaEffectGroup->mediaEffects[i]->effect_in_pos*1000 && videoFrame->pts<=mInputMediaEffectGroup->mediaEffects[i]->effect_out_pos*1000) {
                if (!mInputMediaEffectGroup->mediaEffects[i]->isOpened) {
                    bool ret = open_input_effect_pipelines_l(mInputMediaEffectGroup->mediaEffects[i]);
                    if (ret) {
                        mInputMediaEffectGroup->mediaEffects[i]->isOpened = true;
                    }else return false;
                }
                
                AnimatedGif* animatedGif = (AnimatedGif*)mInputMediaEffectGroup->mediaEffects[i]->opaque;
                VideoFrame* webpFrame = animatedGif->getAnimatedFrame(videoFrame->pts/1000-mInputMediaEffectGroup->mediaEffects[i]->effect_in_pos);
                
                if (webpFrame) {
                    //do blend
                    mGPUImageOffScreenRenderForOutputProduct->normalBlendToFrameBufferTexture(webpFrame, mInputMediaEffectGroup->mediaEffects[i]->rotation, mInputMediaEffectGroup->mediaEffects[i]->scale, mInputMediaEffectGroup->mediaEffects[i]->flipHorizontal, mInputMediaEffectGroup->mediaEffects[i]->flipVertical, mInputMediaEffectGroup->mediaEffects[i]->x, mInputMediaEffectGroup->mediaEffects[i]->y);
                }
            }else{
                if (mInputMediaEffectGroup->mediaEffects[i]->isOpened) {
                    close_input_effect_pipelines_l(mInputMediaEffectGroup->mediaEffects[i]);
                    mInputMediaEffectGroup->mediaEffects[i]->isOpened = false;
                }
            }
        }
    }
    //output
    VideoFrame* renderedVideoFrame = mGPUImageOffScreenRenderForOutputProduct->pumpFrameBuffer();
    renderedVideoFrame->videoRawType = VIDEOFRAME_RAWTYPE_ABGR;
    renderedVideoFrame->pts = videoFrame->pts/1000;
    renderedVideoFrame->duration = videoFrame->pkt_duration/1000;
    
    if (isNeedConvertVideoFrame(renderedVideoFrame, &mTargetVideoFrameForOutputProduct)) {
        //ColorSpace Convert
        bool ret = false;
        
        if (renderedVideoFrame->videoRawType==VIDEOFRAME_RAWTYPE_ABGR && mTargetVideoFrameForOutputProduct.videoRawType==VIDEOFRAME_RAWTYPE_BGRA){
            ret = mColorSpaceConvertForOutputProduct->ABGRtoARGB_Crop_Scale(renderedVideoFrame, &mTargetVideoFrameForOutputProduct);
        }else if (renderedVideoFrame->videoRawType==VIDEOFRAME_RAWTYPE_ABGR && mTargetVideoFrameForOutputProduct.videoRawType==VIDEOFRAME_RAWTYPE_I420){
            ret = mColorSpaceConvertForOutputProduct->ABGRtoI420_Crop_Scale(renderedVideoFrame, &mTargetVideoFrameForOutputProduct);
        }else {
            //todo
        }
        
        mTargetVideoFrameForOutputProduct.duration = renderedVideoFrame->duration;
        
        if (!ret) {
            LOGE("Video ColorSpaceConvert Fail!!");
            
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_COLORSPACECONVERT_FAIL);
            return false;
        }
        
        if (mOutputProductType == MEDIA_PRODUCT_TYPE_MP4) {
            //encode
            int r = mVideoEncoderForOutputProduct->Encode(mTargetVideoFrameForOutputProduct);
            if (r<0) {
                LOGE("Video Encode Fail!!");
                
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_VIDEO_ENCODE_FAIL);
                
                return false;
            }
        }else if(mOutputProductType == MEDIA_PRODUCT_TYPE_GIF) {
            ret = mAnimatedGifCreater->writeFrame(&mTargetVideoFrameForOutputProduct);
            if (!ret) {
                LOGE("Write Gif Fail!!");
                
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_WRITE_GIF_FAIL);
                
                return false;
            }
        }
        
    }else {
        bool ret = false;
        
        if (mOutputProductType == MEDIA_PRODUCT_TYPE_MP4)
        {
            //encode
            int r = mVideoEncoderForOutputProduct->Encode(*renderedVideoFrame);
            
            if (r<0) {
                LOGE("Video Encode Fail!!");
                
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_VIDEO_ENCODE_FAIL);
                
                return false;
            }
        }else if(mOutputProductType == MEDIA_PRODUCT_TYPE_GIF) {
            ret = mAnimatedGifCreater->writeFrame(renderedVideoFrame);
            if (!ret) {
                LOGE("Write Gif Fail!!");
                
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_WRITE_GIF_FAIL);
                
                return false;
            }else{
                mLastRecordTimeMSForGif += renderedVideoFrame->duration;
                
                if (mLastRecordTimeMSForGif>=mLastSendRecordTimeMSForGif+1000) {
                    mLastSendRecordTimeMSForGif = mLastRecordTimeMSForGif;
                    
                    notifyListener_l(MEDIA_PROCESSER_INFO, MEDIA_PROCESSER_INFO_WRITE_TIMESTAMP, (int)(mLastRecordTimeMSForGif/1000));
                }
            }
        }
    }
    
    return true;
}

bool MediaTimelineMerger::audioFlowing_l(uint8_t* pcmData, int pcmSize, int64_t pts)
{
    mAudioPCMDataPoolForOutputProduct->push(pcmData, pcmSize, pts);
    
    while (true) {
        AudioFrame *targetAudioframe = mAudioPCMDataPoolForOutputProduct->pop(mAudioEncoderForOutputProduct->GetFixedInputFrameSize());
        if (targetAudioframe==NULL) {
            break;
        }else {
            //mix
            for (int i = 0; i<mInputMediaEffectGroup->mediaEffectNum; i++)
            {
                if (mInputMediaEffectGroup->mediaEffects[i]->type == MEDIA_EFFECT_TYPE_AUDIO)
                {
                    if (pts>=mInputMediaEffectGroup->mediaEffects[i]->effect_in_pos*1000 && pts<=mInputMediaEffectGroup->mediaEffects[i]->effect_out_pos*1000) {
                        if (!mInputMediaEffectGroup->mediaEffects[i]->isOpened) {
                            bool ret = open_input_effect_pipelines_l(mInputMediaEffectGroup->mediaEffects[i]);
                            if (ret) {
                                mInputMediaEffectGroup->mediaEffects[i]->isOpened = true;
                            }else return ret;
                        }
                        
                        BackGroundSoundContext *backGroundSoundContext = (BackGroundSoundContext *)mInputMediaEffectGroup->mediaEffects[i]->opaque;
                        if (backGroundSoundContext==NULL) return false;
                        
                        //do mix
                        bool ret = mixAudioFrame_l(targetAudioframe, backGroundSoundContext);
                        if (!ret) {
                            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_AUDIO_ENCODE_FAIL);
                            return ret;
                        }
                    }else{
                        if (mInputMediaEffectGroup->mediaEffects[i]->isOpened) {
                            close_input_effect_pipelines_l(mInputMediaEffectGroup->mediaEffects[i]);
                            mInputMediaEffectGroup->mediaEffects[i]->isOpened = false;
                        }
                    }
                }
            }
            
            bool bRet = mAudioEncoderForOutputProduct->Encode(*targetAudioframe);
            
            if (!bRet) {
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_AUDIO_ENCODE_FAIL);
                
                return false;
            }
        }
    }
    
    return true;
}

bool MediaTimelineMerger::mixAudioFrame_l(AudioFrame* targetAudioFrame, BackGroundSoundContext *backGroundSoundContext)
{
    AudioFrame *backGroundSoundAudioframe = backGroundSoundContext->audioPCMDataPoolForBackGroundSound->pop(mAudioEncoderForOutputProduct->GetFixedInputFrameSize());
    if (backGroundSoundAudioframe!=NULL) {
        //do mix
        int16_t *srcSamples1 = (int16_t *)targetAudioFrame->data;
        int16_t *srcSamples2 = (int16_t *)backGroundSoundAudioframe->data;
        int mixCount = mAudioEncoderForOutputProduct->GetFixedInputFrameSize()/2;
        mixSamples(srcSamples1,srcSamples2,mixCount);
        
        return true;
    }
    
    bool ret = false;
    while (true) {
        FFPacket* ffPacket = backGroundSoundContext->ffmpegReaderForBackGroundSound->getMediaPacket();
        
        if (ffPacket->ret==AVERROR_INVALIDDATA || ffPacket->ret == AVERROR(EAGAIN))
        {
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            
            continue;
        }else if(ffPacket->ret == AVERROR_EOF)
        {
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            
            FFSeekStatus seekStatus = backGroundSoundContext->ffmpegReaderForBackGroundSound->seek(0ll);
            
            if (seekStatus.seekRet) {
                continue;
            }else{
                ret = false;
                break;
            }
        }else if (ffPacket->ret < 0)
        {
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            
            ret = false;
            break;
        }else{
            if(ffPacket->mediaType==1) {
                AVPacket* audioPacket = ffPacket->avPacket;
                
                int iret = backGroundSoundContext->audioDecoderForBackGroundSound->decode(audioPacket);
                av_packet_unref(audioPacket);
                av_freep(&audioPacket);
                
                if (iret<0) {
                    ret = false;
                    break;
                }else if (iret==0) {
                    continue;
                }else {
                    AVFrame* sourceAudioFrame = backGroundSoundContext->audioDecoderForBackGroundSound->getPCMData();
                    if (sourceAudioFrame==NULL) {
                        continue;
                    }else {
                        if(backGroundSoundContext->audioFilterForBackGroundSound->filterIn(sourceAudioFrame))
                        {
                            sourceAudioFrame = backGroundSoundContext->audioFilterForBackGroundSound->filterOut();
                            
                            if (sourceAudioFrame==NULL) {
                                continue;
                            }else {
                                int sourceAudioPcmDataSize = av_samples_get_buffer_size(NULL, av_frame_get_channels(sourceAudioFrame),
                                                                                        sourceAudioFrame->nb_samples,
                                                                                        (enum AVSampleFormat)sourceAudioFrame->format, 1);
                                uint8_t* sourceAudioPcmData = *(sourceAudioFrame->extended_data);
                                
                                if(sourceAudioPcmDataSize<=0 || sourceAudioPcmData==NULL)
                                {
                                    continue;
                                }else{
                                    backGroundSoundContext->audioPCMDataPoolForBackGroundSound->push(sourceAudioPcmData, sourceAudioPcmDataSize, 0);
                                    AudioFrame *backGroundSoundAudioframe = backGroundSoundContext->audioPCMDataPoolForBackGroundSound->pop(mAudioEncoderForOutputProduct->GetFixedInputFrameSize());
                                    
                                    if (backGroundSoundAudioframe==NULL) {
                                        continue;
                                    }else{
                                        //do mix
                                        int16_t *srcSamples1 = (int16_t *)targetAudioFrame->data;
                                        int16_t *srcSamples2 = (int16_t *)backGroundSoundAudioframe->data;
                                        int mixCount = mAudioEncoderForOutputProduct->GetFixedInputFrameSize()/2;
                                        mixSamples(srcSamples1,srcSamples2,mixCount);
                                        
                                        ret = true;
                                        break;
                                    }
                                }
                            }
                            
                        }else continue;
                    }
                }
                
            }else {
                av_packet_unref(ffPacket->avPacket);
                av_freep(&ffPacket->avPacket);
                
                continue;
            }
        }
        
    }
    
    return ret;
}


void MediaTimelineMerger::calculateAudioFramePts(AVFrame *audioFrame, AVStream* audioStreamContext)
{
    int64_t currentPts = AV_NOPTS_VALUE;
    if(av_frame_get_best_effort_timestamp(audioFrame) != AV_NOPTS_VALUE)
    {
        currentPts = av_frame_get_best_effort_timestamp(audioFrame);
    }else if(audioFrame->pts!=AV_NOPTS_VALUE)
    {
        currentPts = audioFrame->pts;
    }else if(audioFrame->pkt_pts!=AV_NOPTS_VALUE)
    {
        currentPts = audioFrame->pkt_pts;
    }else if(audioFrame->pkt_dts!=AV_NOPTS_VALUE)
    {
        currentPts = audioFrame->pkt_dts;
    }
    
    audioFrame->pts = currentPts * AV_TIME_BASE * av_q2d(audioStreamContext->time_base);
}

bool MediaTimelineMerger::isNeedConvertVideoFrame(VideoFrame *sourceFrame, VideoFrame *targetFrame)
{
    if (mOutputProductType == MEDIA_PRODUCT_TYPE_GIF) {
        return false;
    }
    
    if (sourceFrame->rotation!=0 || sourceFrame->width!=targetFrame->width || sourceFrame->height!=targetFrame->height || sourceFrame->videoRawType!=targetFrame->videoRawType) {
        return true;
    }
    
    return false;
}

TextInfo *MediaTimelineMerger::TextEffectParamToBlendTextInfo(MediaEffect *inputTextEffect, int64_t blendPts/*ms*/)
{
    TextInfo *textInfo = NULL;
    
    if (inputTextEffect->text_animation_type==TEXT_ANIMATION_NONE) {
        
        TextContext *textContext = (TextContext *)inputTextEffect->opaque;
        if(textContext==NULL) return NULL;
        
        textInfo =  textContext->textInfo;
    }
    
    return textInfo;
}

