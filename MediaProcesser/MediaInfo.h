//
//  MediaInfo.h
//  MediaStreamer
//
//  Created by Think on 2016/11/28.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaInfo_h
#define MediaInfo_h

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "FFmpegReader.h"
#include "VideoDecoder.h"
#include "ColorSpaceConvert.h"

#ifdef ANDROID
#include "jni.h"
#endif

struct MediaDetailInfo {
    bool hasVideo;
    bool hasAudio;
    int width;
    int height;
    int fps;
    int bps;//kbps
    int64_t duration; //ms
    
    int rotate;
    
    bool isH264Codec;
    bool isAACCodec;
    
    MediaDetailInfo()
    {
        hasVideo = false;
        hasAudio = false;
        
        width = 0;
        height = 0;
        fps = 0;
        bps = 0;
        duration = 0ll;
        
        rotate = 0;
        
        isH264Codec = false;
        isAACCodec = false;
    }
};

class MediaInfo {
    
public:
    static MediaDetailInfo* syncGetMediaDetailInfo(char* inputMediaFile);
    static int64_t syncGetMediaDuration(char* inputMediaFile);
    static int syncGetCoverImage(char* inputFile, VideoFrame* outputImage);
    static int syncGetCoverImageWithPosition(char* inputFile, int64_t seekPositionMS, VideoFrame* outputImage);
    static int syncGetCoverImageToImageFile(char* inputMediaFile, int outputWidth, int outputHeight, char* outputImageFile);
    static int syncGetCoverImageToImageFileWithPosition(char* inputMediaFile, int64_t seekPositionMS, int outputWidth, int outputHeight, char* outputImageFile);
    
#ifdef ANDROID
    MediaInfo(JavaVM *jvm, char* saveDir);
#else
    MediaInfo();
#endif
    ~MediaInfo();

    void setMediaThumbnailsOption(int width, int height, int thumbnailCount);
    
#ifdef IOS
    void setMediaDetailInfoListener(void (*listener)(void*,int64_t,int,int), void* arg);
    void setMediaThumbnailListener(void (*listener)(void*,uint8_t *,int,int,int), void* arg);
    void setMediaInfoStatusListener(void (*listener)(void*,int,int,int), void* arg);
#endif
    
#ifdef ANDROID
    void setMediaListener(jobject thiz, jobject weak_thiz, jmethodID post_detailinfo_event, jmethodID post_thumbnail_event, jmethodID post_status_event);
#endif
    
    void loadAsync(char* inputMediaFile);
    void loadAsync(char* inputMediaFile, int64_t startPos, int64_t endPos);
    void quit();
    
private:
    void notifyWithMediaDetailInfo(int64_t duration, int videoWidth, int videoHeight);
    void notifyWithMediaThumbnail(uint8_t *data, int size, int width, int height);
    void notifyWithStatus(int event, int ext1, int ext2);

private:
#ifdef ANDROID
    JavaVM *mJvm;
    
    char * mSaveDir;
#endif
    
    int mThumbnailWidth;
    int mThumbnailHeight;
    int mThumbnailCount;
    
    char* mInputMediaFile;
    int64_t mStartPos;
    int64_t mEndPos;
    bool mIsSetPosition;
    int64_t mDuration;

#ifdef IOS
    void (*mMediaDetailInfoListener)(void*,int64_t,int,int);
    void* mMediaDetailInfoOwner;
    
    void (*mMediaThumbnailListener)(void*,uint8_t *,int,int,int);
    void* mMediaThumbnailOwner;
    
    void (*mMediaInfoStatusListener)(void*,int,int,int);
    void* mMediaInfoStatusOwner;
#endif
    
#ifdef ANDROID
    jclass      mClass;     // Reference to CameraView class
    jobject     mObject;    // Weak ref to CameraView Java object to call on
    jmethodID   mPostMediaDetailInfoEvent;
    jmethodID   mPostMediaThumbnailEvent;
    jmethodID   mPostMediaStatusEvent;
#endif
private:
    int mThumbnailId;
private:
    //method for thread
    void createProcessThread();
    static void* handleProcessThread(void* ptr);
    void processThreadMain();
    void deleteProcessThread();
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    bool isBreakThread; // critical value
    
private:
    void open_all_pipelines_l();
    void close_all_pipelines_l();
    
    FFmpegReader* mFFmpegReader;
    VideoDecoderType mVideoDecoderType;
    VideoDecoder* mVideoDecoder;
    ColorSpaceConvert* mColorSpaceConvert;
    
private:
    VideoFrame mSourceVideoFrame;
    VideoFrame mTargetVideoFrame;
    void AVFrameToVideoFrame(AVFrame *inAVFrame, VideoFrame *outVideoFrame);
};

#endif /* MediaInfo_h */
