//
//  MediaEffect.h
//  MediaStreamer
//
//  Created by Think on 2017/3/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef MediaEffect_h
#define MediaEffect_h

#include <stdio.h>
#include <stdlib.h>

#define MAX_EFFECT_NUM 64

enum MEDIA_EFFECT_TYPE {
    MEDIA_EFFECT_TYPE_UNKNOWN     = -1,
    MEDIA_EFFECT_TYPE_VIDEO_AUDIO = 0,
    MEDIA_EFFECT_TYPE_VIDEO       = 1,
    MEDIA_EFFECT_TYPE_AUDIO       = 2,
    MEDIA_EFFECT_TYPE_TEXT        = 3,
    MEDIA_EFFECT_TYPE_PNG         = 4,
    MEDIA_EFFECT_TYPE_JPEG        = 5,
    MEDIA_EFFECT_TYPE_FILTER      = 6,
    MEDIA_EFFECT_TYPE_TRANSITION  = 7,
    MEDIA_EFFECT_TYPE_WEBP        = 8,
    MEDIA_EFFECT_TYPE_GIF         = 9,
};

enum GPU_IMAGE_FILTER_TYPE {
    GPU_IMAGE_FILTER_RGB = 0,
    GPU_IMAGE_FILTER_SKETCH = 1,
    GPU_IMAGE_FILTER_AMARO = 2,
    GPU_IMAGE_FILTER_ANTIQUE = 3,
    GPU_IMAGE_FILTER_BLACKCAT = 4,
    GPU_IMAGE_FILTER_BEAUTY = 5,
    GPU_IMAGE_FILTER_BRANNAN = 6,
    GPU_IMAGE_FILTER_N1977 = 7,
    GPU_IMAGE_FILTER_BROOKLYN = 8,
    GPU_IMAGE_FILTER_COOL = 9,
    GPU_IMAGE_FILTER_CRAYON = 10,
};

enum GPU_IMAGE_TRANSITION_TYPE {
    GPU_IMAGE_TRANSITION_NONE = -1,
    GPU_IMAGE_TRANSITION_SLIDE = 0,
};

enum TEXT_ANIMATION_TYPE {
    TEXT_ANIMATION_NONE = 0,
};

struct FontColor
{
    float r;
    float g;
    float b;
    float a;
    
    FontColor()
    {
        r = 1.0f;
        g = 1.0f;
        b = 1.0f;
        a = 1.0f;
    }
};

struct MediaEffect
{
    int iD;
    
    MEDIA_EFFECT_TYPE type;
    
    char *url;
    
    int64_t effect_in_pos; //ms
    int64_t effect_out_pos; //ms
    
    // for background audio
    int64_t startPos; //ms
    int64_t endPos; //ms
    float volume; //
    float speed;
    
    //for overlay
    int x;
    int y;
    int rotation;
    float scale;
    bool flipHorizontal;
    bool flipVertical;
    
    //for text
    char* text;
    char* fontLibPath;
    int fontSize;
    FontColor fontColor;
    int leftMargin;
    int rightMargin;
    int topMargin;
    int bottomMargin;
    int lineSpace;
    int wordSpace;
    TEXT_ANIMATION_TYPE text_animation_type;
    
    //for filter
    GPU_IMAGE_FILTER_TYPE gpu_image_filter_type;
    
    //for transition
    GPU_IMAGE_TRANSITION_TYPE gpu_image_transition_type;
    int transition_source_id;
    
    void* opaque;
    
    bool isOpened;
    
    MediaEffect()
    {
        iD = -1;
        type = MEDIA_EFFECT_TYPE_UNKNOWN;
        url = NULL;
        
        effect_in_pos = -1;
        effect_out_pos = -1;
        
        startPos = -1;
        endPos = -1;
        volume = 1.0f;
        speed = 1.0f;
        
        x = 0;
        y = 0;
        rotation = 0;
        scale = 1.0f;
        flipHorizontal = false;
        flipVertical = false;
        
        text = NULL;
        fontLibPath = NULL;
        fontSize = 16;
        leftMargin = 0;
        rightMargin = 0;
        topMargin = 0;
        bottomMargin = 0;
        lineSpace = 0;
        wordSpace = 0;
        text_animation_type = TEXT_ANIMATION_NONE;
        
        gpu_image_filter_type = GPU_IMAGE_FILTER_RGB;
        
        gpu_image_transition_type = GPU_IMAGE_TRANSITION_NONE;
        transition_source_id = -1;
        
        opaque = NULL;
        
        isOpened = false;
    }
};

struct MediaEffectGroup
{
    MediaEffect *mediaEffects[MAX_EFFECT_NUM];
    int mediaEffectNum;
    
    MediaEffectGroup()
    {
        for(int i = 0; i < MAX_EFFECT_NUM; i++)
        {
            mediaEffects[i] = NULL;
        }
        
        mediaEffectNum = 0;
    }
    
    inline void Clear()
    {
        for(int i = 0; i < MAX_EFFECT_NUM; i++)
        {
            if(mediaEffects[i]!=NULL)
            {
                delete mediaEffects[i];
                mediaEffects[i] = NULL;
            }
        }
        
        mediaEffectNum = 0;
    }
    
    inline void Free()
    {
        for(int i = 0; i < MAX_EFFECT_NUM; i++)
        {
            if(mediaEffects[i]!=NULL)
            {
                if(mediaEffects[i]->url!=NULL)
                {
                    free(mediaEffects[i]->url);
                    mediaEffects[i]->url = NULL;
                }
                
                if(mediaEffects[i]->text!=NULL)
                {
                    free(mediaEffects[i]->text);
                    mediaEffects[i]->text = NULL;
                }
                
                if(mediaEffects[i]->fontLibPath)
                {
                    free(mediaEffects[i]->fontLibPath);
                    mediaEffects[i]->fontLibPath = NULL;
                }
                
                delete mediaEffects[i];
                mediaEffects[i] = NULL;
            }
        }
        
        mediaEffectNum = 0;
    }
};

#endif /* MediaEffect_h */
