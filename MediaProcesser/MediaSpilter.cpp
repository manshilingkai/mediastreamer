//
//  MediaSpliter.cpp
//  MediaStreamer
//
//  Created by Think on 16/11/7.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaSpliter.h"

#ifdef ANDROID
MediaSpliter* MediaSpliter::CreateMediaSpliter(JavaVM *jvm, MediaMaterial *inputMediaMaterial, MEDIA_SPLIT_ALGORITHM algorithm, MediaProductGroup* outputProductGroup)
{
    return NULL;
}
#else

MediaSpliter* MediaSpliter::CreateMediaSpliter(MediaMaterial *inputMediaMaterial, MEDIA_SPLIT_ALGORITHM algorithm, MediaProductGroup* outputProductGroup)
{
    return NULL;
}

#endif

void MediaSpliter::DeleteMediaSpliter(MEDIA_SPLIT_ALGORITHM algorithm, MediaSpliter* mediaSpliter)
{

}
