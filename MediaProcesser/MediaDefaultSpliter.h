//
//  MediaDefaultSpliter.h
//  MediaStreamer
//
//  Created by Think on 2016/12/5.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaDefaultSpliter_h
#define MediaDefaultSpliter_h

#include <stdio.h>

#include "MediaSpliter.h"

#include "TimedEventQueue.h"
#include "MediaDataType.h"
#include "MediaListener.h"

#include "FFmpegReader.h"
#include "VideoDecoder.h"
#include "AudioDecoder.h"
#include "ColorSpaceConvert.h"
#include "AudioFilter.h"
#include "VideoEncoder.h"
#include "AudioEncoder.h"
#include "MediaMuxer.h"
#include "AudioPCMDataPool.h"

#include "NotificationQueue.h"

#ifdef ANDROID
#include "jni.h"
#endif

extern "C" {
#include "libavformat/avformat.h"
}

class MediaDefaultSpliter : public MediaSpliter, MediaListener{
public:
#ifdef ANDROID
    MediaDefaultSpliter(JavaVM *jvm, MediaMaterial *inputMediaMaterial, MediaProductGroup* outputProductGroup);
#else
    MediaDefaultSpliter(MediaMaterial *inputMediaMaterial, MediaProductGroup* outputProductGroup);
#endif
    ~MediaDefaultSpliter();
    
#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#endif
    
#ifdef IOS
    void setListener(void (*listener)(void*,int,int,int), void* arg);
#endif
    
    void start();
    
    void resume();
    void pause();
    
    void stop();
    
    void notify(int event, int ext1, int ext2);
    
private:
    friend struct MediaDefaultSpliterEvent;
    
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    MediaMaterial mInputMediaMaterial;
    MediaProductGroup mOutputProductGroup;
    
    MediaListener *mMediaListener;

    MediaDefaultSpliter(const MediaDefaultSpliter &);
    MediaDefaultSpliter &operator=(const MediaDefaultSpliter &);
    
    pthread_mutex_t mLock;
    pthread_cond_t mStopCondition;
    
    TimedEventQueue mQueue;
    
    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mProcessOnePacketEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mNotifyEvent;
    void onPrepareAsyncEvent();
    void onProcessOnePacketEvent();
    void onStopEvent();
    void onNotifyEvent();
    
    void prepareAsync_l();
    void play_l();
    void pause_l();
    void stop_l();
    
    NotificationQueue mNotificationQueue;
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
    
    void cancelSpliterEvents();
private:
    bool open_all_source_pipelines_l();
    void close_all_source_pipelines_l();
    
    bool open_all_target_pipelines_l(MediaProduct *outputProduct);
    void close_all_target_pipelines_l();
    
private:
    FFmpegReader *mFFmpegReader;
    VideoDecoder *mVideoDecoder;
    VideoDecoderType mVideoDecoderType;
    AudioDecoder *mAudioDecoder;
    ColorSpaceConvert *mColorSpaceConvert;
    AudioFilter *mAudioFilter;
    VideoEncoder *mVideoEncoder;
    VIDEO_ENCODER_TYPE mVideoEncoderType;
    AudioEncoder *mAudioEncoder;
    AUDIO_ENCODER_TYPE mAudioEncoderType;
    MediaMuxer *mMediaMuxer;
    AudioPCMDataPool *mAudioPCMDataPool;
private:
    enum {
        CONNECTING           = 0x01,
        CONNECTED            = 0x02,
        STREAMING            = 0x04,
        PAUSED               = 0x10,
        ERROR                = 0x20,
        END                  = 0x40,
        INITIALIZED          = 0x80,
        ENDING               = 0x100,
    };
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
private:
    int mProductIndex;
    bool is_open_all_target_pipelines;
    
    VideoFrame mSourceVideoFrame;
    VideoFrame mTargetVideoFrame;
    
    bool isNeedConvertVideoFrame(VideoFrame *sourceFrame, VideoFrame *targetFrame);
    
    void audioFlowing_l(AVPacket* audioPacket);
    void videoFlowing_l(AVPacket* videoPacket);
    
    void processVideoOrAudioPacket();
    
    void AVFrameToVideoFrame(AVFrame *inAVFrame, VideoFrame *outVideoFrame);
    
    int64_t mCurrentVideoPts;
    int64_t mCurrentAudioPts;
};


#endif /* MediaDefaultSpliter_h */
