//
//  SLKMediaEffectGroup.m
//  MediaStreamer
//
//  Created by PPTV on 2017/3/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import "SLKMediaEffectGroup.h"

@interface SLKMediaEffectGroup () {
}

@property (nonatomic, strong) NSMutableArray *slkMediaEffectArray;

@end

@implementation SLKMediaEffectGroup

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.slkMediaEffectArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)addMediaEffect:(SLKMediaEffect*)mediaEffect
{
    [self.slkMediaEffectArray addObject:mediaEffect];
}

- (void)insertMediaEffect:(SLKMediaEffect*)mediaEffect atIndex:(NSUInteger)index
{
    [self.slkMediaEffectArray insertObject:mediaEffect atIndex:index];
}

- (void)removeMediaEffect:(SLKMediaEffect*)mediaEffect
{
    [self.slkMediaEffectArray removeObject:mediaEffect];
}

- (void)removeMediaEffectAtIndex:(NSUInteger)index
{
    [self.slkMediaEffectArray removeObjectAtIndex:index];
}

- (void)removeAllMediaEffects
{
    [self.slkMediaEffectArray removeAllObjects];
}

- (NSUInteger)count
{
    return [self.slkMediaEffectArray count];
}

- (SLKMediaEffect*)getMediaEffectAtIndex:(NSUInteger)index
{
    return [self.slkMediaEffectArray objectAtIndex:index];
}

@end
