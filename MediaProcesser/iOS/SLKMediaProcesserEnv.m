//
//  SLKMediaProcesserEnv.m
//  MediaStreamer
//
//  Created by PPTV on 2017/3/29.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import "SLKMediaProcesserEnv.h"

@implementation SLKMediaProcesserEnv

+(BOOL)setupLocale
{
    NSString* resources = [[NSBundle mainBundle ] resourcePath];
    const char* path = [resources cStringUsingEncoding:1];
    setenv("PATH_LOCALE", path, 1);
    
    char* ret_str = setlocale(LC_CTYPE, "zh_CN.UTF-8" );
    if (ret_str==NULL) {
        NSLog(@"setup locale Fail\n");
        return NO;
    }else{
        NSLog(@"setup locale Success : %s\n", ret_str);
        return YES;
    }
}

@end
