//
//  SLKMediaEffect.m
//  MediaStreamer
//
//  Created by Think on 2017/3/14.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import "SLKMediaEffect.h"

@implementation SLKMediaEffect

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.iD = -1;
        self.url = nil;
        self.slk_media_effect_type = SLK_MEDIA_EFFECT_TYPE_UNKNOWN;
        self.effect_in_pos = 0;
        self.effect_out_pos = 0;
        self.startPos = 0;
        self.endPos = 0;
        self.volume = 1.0f;
        self.speed = 1.0f;
        
        self.x = 0;
        self.y = 0;
        self.rotation = 0;
        self.scale = 1.0f;
        self.flipHorizontal = NO;
        self.flipVertical = NO;
        
        self.text = nil;
        self.fontLibPath = nil;
        self.fontSize = 0;
        self.fontColor = nil;
        self.leftMargin = 0;
        self.rightMargin = 0;
        self.topMargin = 0;
        self.bottomMargin = 0;
        self.lineSpace = 0;
        self.wordSpace = 0;
        self.slk_text_animation_type = SLK_TEXT_ANIMATION_NONE;
        
        self.slk_gpu_image_filter_type = SLK_GPU_IMAGE_FILTER_RGB;
        
        self.slk_gpu_image_transition_type = SLK_GPU_IMAGE_TRANSITION_NONE;
        self.transition_source_id = -1;
    }
    
    return self;
}

@end
