//
//  SLKMediaMerger.m
//  MediaStreamer
//
//  Created by Think on 2016/11/22.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "SLKMediaMerger.h"
#include "SLKMediaMergerWrapper.h"
#include <mutex>

@implementation SLKMediaMerger
{
    SLKMediaMergerWrapper* pSLKMediaMergerWrapper;
    std::mutex mSLKMediaMergerWrapperMutex;
    
    dispatch_queue_t notificationQueue;
    
    MEDIA_MERGE_ALGORITHM algorithm;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        notificationQueue = dispatch_queue_create("SLKMediaMergerNotificationQueue", 0);
    }
    
    return self;
}

- (void)initializeWithInputMediaMaterialGroup:(SLKMediaMaterialGroup*)inputSLKMediaMaterialGroup WithInputMediaEffectGroup:(SLKMediaEffectGroup*)inputSLKMediaEffectGroup WithMediaMergeAlgorithm:(SLK_MEDIA_MERGE_ALGORITHM)slkMediaMergeAlgorithm WithOutputMediaProduct:(SLKMediaProduct*)outputSLKMediaProduct
{
    mSLKMediaMergerWrapperMutex.lock();
    
    MediaMaterialGroup inputMediaMaterialGroup;
    MediaEffectGroup inputMediaEffectGroup;
    algorithm = MEDIA_MERGE_ALGORITHM_TIMELINE;
    MediaProduct ouputProduct;
    
    inputMediaMaterialGroup.mediaMaterialNum = [inputSLKMediaMaterialGroup count];
    for (int i=0; i<inputMediaMaterialGroup.mediaMaterialNum; i++) {
        
        SLKMediaMaterial *slkMediaMaterial = [inputSLKMediaMaterialGroup getMediaMaterialAtIndex:i];
        
        inputMediaMaterialGroup.mediaMaterials[i] = new MediaMaterial;
        inputMediaMaterialGroup.mediaMaterials[i]->url = (char*)[[slkMediaMaterial url] UTF8String];
        inputMediaMaterialGroup.mediaMaterials[i]->type = (MEDIA_MATERIAL_TYPE)[slkMediaMaterial slk_media_material_type];
        inputMediaMaterialGroup.mediaMaterials[i]->startPos = [slkMediaMaterial startPos];
        inputMediaMaterialGroup.mediaMaterials[i]->endPos = [slkMediaMaterial endPos];
        inputMediaMaterialGroup.mediaMaterials[i]->volume = [slkMediaMaterial volume];
        inputMediaMaterialGroup.mediaMaterials[i]->speed = [slkMediaMaterial speed];
    }
    
    if (inputSLKMediaEffectGroup) {
        inputMediaEffectGroup.mediaEffectNum = [inputSLKMediaEffectGroup count];
        
        for (int i = 0; i<inputMediaEffectGroup.mediaEffectNum; i++) {
            SLKMediaEffect *slkMediaEffect = [inputSLKMediaEffectGroup getMediaEffectAtIndex:i];
            inputMediaEffectGroup.mediaEffects[i] = new MediaEffect;
            inputMediaEffectGroup.mediaEffects[i]->type = (MEDIA_EFFECT_TYPE)[slkMediaEffect slk_media_effect_type];
            inputMediaEffectGroup.mediaEffects[i]->url = (char*)[[slkMediaEffect url] UTF8String];
            inputMediaEffectGroup.mediaEffects[i]->effect_in_pos = [slkMediaEffect effect_in_pos];
            inputMediaEffectGroup.mediaEffects[i]->effect_out_pos = [slkMediaEffect effect_out_pos];
            inputMediaEffectGroup.mediaEffects[i]->startPos = [slkMediaEffect startPos];
            inputMediaEffectGroup.mediaEffects[i]->endPos = [slkMediaEffect endPos];
            inputMediaEffectGroup.mediaEffects[i]->volume = [slkMediaEffect volume];
            inputMediaEffectGroup.mediaEffects[i]->speed = [slkMediaEffect speed];
            
            inputMediaEffectGroup.mediaEffects[i]->x = [slkMediaEffect x];
            inputMediaEffectGroup.mediaEffects[i]->y = [slkMediaEffect y];
            inputMediaEffectGroup.mediaEffects[i]->rotation = [slkMediaEffect rotation];
            inputMediaEffectGroup.mediaEffects[i]->scale = [slkMediaEffect scale];
            inputMediaEffectGroup.mediaEffects[i]->flipHorizontal = [slkMediaEffect flipHorizontal];
            inputMediaEffectGroup.mediaEffects[i]->flipVertical = [slkMediaEffect flipVertical];
            
            inputMediaEffectGroup.mediaEffects[i]->text = (char*)[[slkMediaEffect text] UTF8String];
            inputMediaEffectGroup.mediaEffects[i]->fontLibPath = (char*)[[slkMediaEffect fontLibPath] UTF8String];
            inputMediaEffectGroup.mediaEffects[i]->fontSize = [slkMediaEffect fontSize];
            FontColor fontColor;
            CGColorRef color = [[slkMediaEffect fontColor] CGColor];
            size_t numComponents = CGColorGetNumberOfComponents(color);
            if (numComponents == 4)
            {
                const CGFloat *components = CGColorGetComponents(color);
                fontColor.r = components[0];
                fontColor.g = components[1];
                fontColor.b = components[2];
                fontColor.a = components[3];
            }else if(numComponents == 3)
            {
                const CGFloat *components = CGColorGetComponents(color);
                fontColor.r = components[0];
                fontColor.g = components[1];
                fontColor.b = components[2];
                fontColor.a = 1.0f;
            }
            inputMediaEffectGroup.mediaEffects[i]->fontColor = fontColor;
            inputMediaEffectGroup.mediaEffects[i]->leftMargin = [slkMediaEffect leftMargin];
            inputMediaEffectGroup.mediaEffects[i]->rightMargin = [slkMediaEffect rightMargin];
            inputMediaEffectGroup.mediaEffects[i]->topMargin = [slkMediaEffect topMargin];
            inputMediaEffectGroup.mediaEffects[i]->bottomMargin = [slkMediaEffect bottomMargin];
            inputMediaEffectGroup.mediaEffects[i]->lineSpace = [slkMediaEffect lineSpace];
            inputMediaEffectGroup.mediaEffects[i]->wordSpace = [slkMediaEffect wordSpace];
            inputMediaEffectGroup.mediaEffects[i]->text_animation_type = (TEXT_ANIMATION_TYPE)[slkMediaEffect slk_text_animation_type];
            
            inputMediaEffectGroup.mediaEffects[i]->gpu_image_filter_type = (GPU_IMAGE_FILTER_TYPE)[slkMediaEffect slk_gpu_image_filter_type];
            inputMediaEffectGroup.mediaEffects[i]->gpu_image_transition_type = (GPU_IMAGE_TRANSITION_TYPE)[slkMediaEffect slk_gpu_image_transition_type];
            inputMediaEffectGroup.mediaEffects[i]->transition_source_id = [slkMediaEffect transition_source_id];
        }
    }else {
        inputMediaEffectGroup.mediaEffectNum = 0;
    }

    
    if (slkMediaMergeAlgorithm==SLK_MEDIA_MERGE_ALGORITHM_TIMELINE) {
        algorithm = MEDIA_MERGE_ALGORITHM_TIMELINE;
    } else if (slkMediaMergeAlgorithm==SLK_MEDIA_MERGE_ALGORITHM_REMUX){
        algorithm = MEDIA_MERGE_ALGORITHM_REMUX;
    } else {
        algorithm = MEDIA_MERGE_ALGORITHM_DEEP_REMUX;
    }
    
    ouputProduct.url = (char*)[[outputSLKMediaProduct url] UTF8String];
    ouputProduct.type = (MEDIA_PRODUCT_TYPE)[outputSLKMediaProduct slk_media_product_type];
    
    ouputProduct.audioOptions.hasAudio = [outputSLKMediaProduct hasAudio];
    ouputProduct.audioOptions.audioSampleRate = 44100;
    ouputProduct.audioOptions.audioNumChannels = 1;
    ouputProduct.audioOptions.audioBitRate = 64; //k
    
    ouputProduct.videoOptions.hasVideo = [outputSLKMediaProduct hasVideo];
    if (ouputProduct.type == MEDIA_PRODUCT_TYPE_MP4) {
        ouputProduct.videoOptions.videoRawType = VIDEOFRAME_RAWTYPE_BGRA;
    }else if(ouputProduct.type == MEDIA_PRODUCT_TYPE_GIF) {
        ouputProduct.videoOptions.videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
    }else {
        ouputProduct.videoOptions.videoRawType = VIDEOFRAME_RAWTYPE_BGRA;
    }
    ouputProduct.videoOptions.videoWidth = (int)([outputSLKMediaProduct videoSize].width);
    ouputProduct.videoOptions.videoHeight = (int)([outputSLKMediaProduct videoSize].height);
    ouputProduct.videoOptions.videoFps = 24;
    ouputProduct.videoOptions.videoEncodeType = VIDEO_HARD_ENCODE;
    ouputProduct.videoOptions.videoProfile = 2; //0:base_line 1:main_profile 2:high_profile
    ouputProduct.videoOptions.videoBitRate = [outputSLKMediaProduct bps] - ouputProduct.audioOptions.audioBitRate;
    ouputProduct.videoOptions.encodeMode = 1;//CBR
    ouputProduct.videoOptions.maxKeyFrameIntervalMs = 5000;
    
    //for X264
    ouputProduct.videoOptions.quality = 0;
    ouputProduct.videoOptions.bStrictCBR = true;
    ouputProduct.videoOptions.deblockingFilterFactor = 0;
    
    if ([outputSLKMediaProduct isAspectFit]) {
        ouputProduct.videoOptions.isAspectFit = true;
    }else{
        ouputProduct.videoOptions.isAspectFit = false;
    }
    
    pSLKMediaMergerWrapper = SLKMediaMergerWrapper_GetInstance(&inputMediaMaterialGroup, &inputMediaEffectGroup, algorithm, &ouputProduct);
    
    inputMediaMaterialGroup.Clear();
    inputMediaEffectGroup.Clear();
    
    if (pSLKMediaMergerWrapper!=NULL) {
        SLKMediaMergerWrapper_setListener(pSLKMediaMergerWrapper, mediamergernotificationListener, (__bridge void*)self);
    }
    
    mSLKMediaMergerWrapperMutex.unlock();
}

void mediamergernotificationListener(void* owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak SLKMediaMerger *thiz = (__bridge SLKMediaMerger*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    __weak typeof(self) wself = self;
    
    dispatch_async(notificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf handleNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
        
    });
}

- (void)handleNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    switch (event) {
        case SLK_MEDIA_PROCESSER_ERROR:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotErrorWithErrorType:)])) {
                    [self.delegate gotErrorWithErrorType:ext1];
                }
            }
            break;
        case SLK_MEDIA_PROCESSER_INFO:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotInfoWithInfoType:InfoValue:)])) {
                    [self.delegate gotInfoWithInfoType:ext1 InfoValue:ext2];
                }
            }
            break;
        case SLK_MEDIA_PROCESSER_END:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(didEnd)])) {
                    [self.delegate didEnd];
                }
            }
            break;
            
        default:
            break;
    }
}

- (void)start
{
    mSLKMediaMergerWrapperMutex.lock();
    
    if (pSLKMediaMergerWrapper!=NULL) {
        SLKMediaMergerWrapper_start(pSLKMediaMergerWrapper);
    }
    
    mSLKMediaMergerWrapperMutex.unlock();
    
}
- (void)resume
{
    mSLKMediaMergerWrapperMutex.lock();
    
    if (pSLKMediaMergerWrapper!=NULL) {
        SLKMediaMergerWrapper_resume(pSLKMediaMergerWrapper);
    }
    
    mSLKMediaMergerWrapperMutex.unlock();
}

- (void)pause
{
    mSLKMediaMergerWrapperMutex.lock();
    
    if (pSLKMediaMergerWrapper!=NULL) {
        SLKMediaMergerWrapper_pause(pSLKMediaMergerWrapper);
    }
    
    mSLKMediaMergerWrapperMutex.unlock();
    
}
- (void)stop
{
    mSLKMediaMergerWrapperMutex.lock();
    
    if (pSLKMediaMergerWrapper!=NULL) {
        SLKMediaMergerWrapper_stop(pSLKMediaMergerWrapper);
    }
    
    mSLKMediaMergerWrapperMutex.unlock();
    
}

- (void)terminate
{
    mSLKMediaMergerWrapperMutex.lock();
    
    if (pSLKMediaMergerWrapper!=NULL) {
        SLKMediaMergerWrapper_ReleaseInstance(&pSLKMediaMergerWrapper, algorithm);
        pSLKMediaMergerWrapper = NULL;
    }
    
    mSLKMediaMergerWrapperMutex.unlock();
    
    dispatch_barrier_sync(notificationQueue, ^{
        NSLog(@"finish all notifications");
    });
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"SLKMediaMerger dealloc");
}


@end
