//
//  SLKMediaMaterialGroup.m
//  MediaStreamer
//
//  Created by Think on 2016/11/22.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "SLKMediaMaterialGroup.h"

@interface SLKMediaMaterialGroup () {
}

@property (nonatomic, strong) NSMutableArray *slkMediaMaterialArray;

@end

@implementation SLKMediaMaterialGroup

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.slkMediaMaterialArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)addMediaMaterial:(SLKMediaMaterial*)mediaMaterial
{
    [self.slkMediaMaterialArray addObject:mediaMaterial];
}

- (void)insertMediaMaterial:(SLKMediaMaterial*)mediaMaterial atIndex:(NSUInteger)index
{
    [self.slkMediaMaterialArray insertObject:mediaMaterial atIndex:index];
}

- (void)removeMediaMaterial:(SLKMediaMaterial*)mediaMaterial
{
    [self.slkMediaMaterialArray removeObject:mediaMaterial];
}

- (void)removeMediaMaterialAtIndex:(NSUInteger)index
{
    [self.slkMediaMaterialArray removeObjectAtIndex:index];
}

- (void)removeAllMediaMaterials
{
    [self.slkMediaMaterialArray removeAllObjects];
}

- (NSUInteger)count
{
    return [self.slkMediaMaterialArray count];
}

- (SLKMediaMaterial*)getMediaMaterialAtIndex:(NSUInteger)index
{
    return [self.slkMediaMaterialArray objectAtIndex:index];
}

@end
