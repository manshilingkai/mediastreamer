//
//  SLKMediaMergerWrapper.cpp
//  MediaStreamer
//
//  Created by Think on 2016/11/21.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "SLKMediaMergerWrapper.h"
#include "MediaMerger.h"

#ifdef __cplusplus
extern "C" {
#endif
    
    struct SLKMediaMergerWrapper{
        MediaMerger* mediaMerger;
        
        SLKMediaMergerWrapper()
        {
            mediaMerger = NULL;
        }
    };
    
    struct SLKMediaMergerWrapper *SLKMediaMergerWrapper_GetInstance(MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup, MEDIA_MERGE_ALGORITHM algorithm, MediaProduct *ouputProduct)
    {
        SLKMediaMergerWrapper* pInstance = new SLKMediaMergerWrapper;
        
        pInstance->mediaMerger = MediaMerger::CreateMediaMerger(inputMediaMaterialGroup, inputMediaEffectGroup, algorithm, ouputProduct);
        
        return pInstance;
    }

    void SLKMediaMergerWrapper_ReleaseInstance(struct SLKMediaMergerWrapper **ppInstance, MEDIA_MERGE_ALGORITHM algorithm)
    {
        SLKMediaMergerWrapper* pInstance  = *ppInstance;
        if (pInstance!=NULL) {
            if (pInstance->mediaMerger!=NULL) {
                MediaMerger::DeleteMediaMerger(algorithm, pInstance->mediaMerger);
                pInstance->mediaMerger = NULL;
            }
            
            delete pInstance;
            pInstance = NULL;
        }
    }
    
    void SLKMediaMergerWrapper_setListener(struct SLKMediaMergerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner)
    {
        if (pInstance!=NULL && pInstance->mediaMerger!=NULL) {
            pInstance->mediaMerger->setListener(listener,owner);
        }
    }
    
    void SLKMediaMergerWrapper_start(struct SLKMediaMergerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->mediaMerger!=NULL) {
            pInstance->mediaMerger->start();
        }
    }
    void SLKMediaMergerWrapper_resume(struct SLKMediaMergerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->mediaMerger!=NULL) {
            pInstance->mediaMerger->resume();
        }
    }
    void SLKMediaMergerWrapper_pause(struct SLKMediaMergerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->mediaMerger!=NULL) {
            pInstance->mediaMerger->pause();
        }
    }
    void SLKMediaMergerWrapper_stop(struct SLKMediaMergerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->mediaMerger!=NULL) {
            pInstance->mediaMerger->stop();
        }
    }

#ifdef __cplusplus
};
#endif
