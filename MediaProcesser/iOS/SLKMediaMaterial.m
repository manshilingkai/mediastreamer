//
//  SLKMediaMaterial.m
//  MediaStreamer
//
//  Created by Think on 2016/11/22.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "SLKMediaMaterial.h"

@implementation SLKMediaMaterial

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.iD = -1;
        self.url = nil;
        self.slk_media_material_type = SLK_MEDIA_MATERIAL_TYPE_UNKNOWN;
        self.startPos = 0;
        self.endPos = 0;
        self.volume = 1.0f;
        self.speed = 1.0f;
    }
    
    return self;
}

@end
