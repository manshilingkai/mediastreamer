//
//  SLKMediaInfoWrapper.cpp
//  MediaStreamer
//
//  Created by Think on 2016/11/29.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "SLKMediaInfoWrapper.h"
#include "MediaInfo.h"

#ifdef __cplusplus
extern "C" {
#endif

    struct SLKMediaInfoWrapper{
        MediaInfo* mediaInfo;
        
        SLKMediaInfoWrapper()
        {
            mediaInfo = NULL;
        }
    };
    
    struct SLKMediaDetailInfoWrapper* SLKMediaInfoWrapper_syncGetMediaDetailInfo(char* inputMediaFile)
    {
        MediaDetailInfo *detailInfo = MediaInfo::syncGetMediaDetailInfo(inputMediaFile);
        
        if (detailInfo) {
            SLKMediaDetailInfoWrapper *detailInfoWrapper = new SLKMediaDetailInfoWrapper;
            detailInfoWrapper->hasVideo = detailInfo->hasVideo;
            detailInfoWrapper->hasAudio = detailInfo->hasAudio;
            detailInfoWrapper->width = detailInfo->width;
            detailInfoWrapper->height = detailInfo->height;
            detailInfoWrapper->fps = detailInfo->fps;
            detailInfoWrapper->bps = detailInfo->bps;
            detailInfoWrapper->duration = detailInfo->duration;
            detailInfoWrapper->rotate = detailInfo->rotate;
            detailInfoWrapper->isH264Codec = detailInfo->isH264Codec;
            detailInfoWrapper->isAACCodec = detailInfo->isAACCodec;
            
            delete detailInfo;
            
            return detailInfoWrapper;
        }
        
        return NULL;
    }
    
    int64_t SLKMediaInfoWrapper_syncGetMediaDuration(char* inputMediaFile)
    {
        return MediaInfo::syncGetMediaDuration(inputMediaFile);
    }

    bool SLKMediaInfoWrapper_syncGetCoverImageToImageFile(char* inputMediaFile, int outputWidth, int outputHeight, char* outputImageFile)
    {
        int ret = MediaInfo::syncGetCoverImageToImageFile(inputMediaFile, outputWidth, outputHeight, outputImageFile);
        
        if (ret) return false;
        else return true;
    }
    
    bool SLKMediaInfoWrapper_syncGetCoverImageToImageFileWithPosition(char* inputMediaFile, int64_t seekPositionMS, int outputWidth, int outputHeight, char* outputImageFile)
    {
        int ret = MediaInfo::syncGetCoverImageToImageFileWithPosition(inputMediaFile, seekPositionMS, outputWidth, outputHeight, outputImageFile);
        
        if (ret) return false;
        else return true;
    }
    
    struct SLKMediaInfoWrapper *SLKMediaInfoWrapper_GetInstance()
    {
        SLKMediaInfoWrapper* pInstance = new SLKMediaInfoWrapper;
        pInstance->mediaInfo = new MediaInfo;
        
        return pInstance;
    }
    
    void SLKMediaInfoWrapper_ReleaseInstance(struct SLKMediaInfoWrapper **ppInstance)
    {
        SLKMediaInfoWrapper* pInstance  = *ppInstance;
        if (pInstance!=NULL) {
            if (pInstance->mediaInfo!=NULL) {
                delete pInstance->mediaInfo;
                pInstance->mediaInfo = NULL;
            }
            
            delete pInstance;
            pInstance = NULL;
        }
    }
    
    void SLKMediaInfoWrapper_setMediaThumbnailsOption(struct SLKMediaInfoWrapper *pInstance, int width, int height, int thumbnailCount)
    {
        if (pInstance!=NULL && pInstance->mediaInfo!=NULL) {
            pInstance->mediaInfo->setMediaThumbnailsOption(width, height, thumbnailCount);
        }
    }
    
    void SLKMediaInfoWrapper_setMediaDetailInfoListener(struct SLKMediaInfoWrapper *pInstance, void (*listener)(void*,int64_t,int,int), void* arg)
    {
        if (pInstance!=NULL && pInstance->mediaInfo!=NULL) {
            pInstance->mediaInfo->setMediaDetailInfoListener(listener, arg);
        }
    }
    void SLKMediaInfoWrapper_setMediaThumbnailListener(struct SLKMediaInfoWrapper *pInstance, void (*listener)(void*,uint8_t *,int,int,int), void* arg)
    {
        if (pInstance!=NULL && pInstance->mediaInfo!=NULL) {
            pInstance->mediaInfo->setMediaThumbnailListener(listener, arg);
        }
    }
    
    void SLKMediaInfoWrapper_setMediaInfoStatusListener(struct SLKMediaInfoWrapper *pInstance, void (*listener)(void*,int,int,int), void* arg)
    {
        if (pInstance!=NULL && pInstance->mediaInfo!=NULL) {
            pInstance->mediaInfo->setMediaInfoStatusListener(listener, arg);
        }
    }
    
    void SLKMediaInfoWrapper_loadAsync(struct SLKMediaInfoWrapper *pInstance, char* inputMediaFile)
    {
        if (pInstance!=NULL && pInstance->mediaInfo!=NULL) {
            pInstance->mediaInfo->loadAsync(inputMediaFile);
        }
    }
    
    void SLKMediaInfoWrapper_loadAsyncWithPosition(struct SLKMediaInfoWrapper *pInstance, char* inputMediaFile, int64_t startPos, int64_t endPos)
    {
        if (pInstance!=NULL && pInstance->mediaInfo!=NULL) {
            pInstance->mediaInfo->loadAsync(inputMediaFile, startPos, endPos);
        }
    }
    
    void SLKMediaInfoWrapper_quit(struct SLKMediaInfoWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->mediaInfo!=NULL) {
            pInstance->mediaInfo->quit();
        }
    }
    
#ifdef __cplusplus
};
#endif
