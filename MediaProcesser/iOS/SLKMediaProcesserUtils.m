//
//  SLKMediaProcesserUtils.m
//  MediaStreamer
//
//  Created by Think on 16/11/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "SLKMediaProcesserUtils.h"

#include "qt-faststart.h"
#include "MediaCoverImage.h"

#include "ImageProcesserUtils.h"

#include "Word2Bitmap.h"

@implementation SLKMediaProcesserUtils

+ (BOOL)qtFaststartWithInputFile:(NSString*)inputFile OutputFile:(NSString*)outputFile
{
    char *argv[3];
    argv[0] = (char *)malloc(16);
    strcpy(argv[0], "qt-faststart");
    
    argv[1] = (char*)[inputFile UTF8String];
    argv[2] = (char*)[outputFile UTF8String];
    
    int ret = qt_faststart_main(3, argv);
    
    if (argv[0]!=NULL) {
        free(argv[0]);
        argv[0] = NULL;
    }
    
    if (ret) {
        return NO;
    }
    
    return YES;
}

+ (CVPixelBufferRef)getCoverImageWithInputFile:(NSString*)inputFile OutputWidth:(int)width OutputHeight:(int)height;
{
    VideoFrame* outputImage = new VideoFrame();
    outputImage->width = width;
    outputImage->height = height;
    outputImage->frameSize = outputImage->width*outputImage->height*4;
    outputImage->data = (uint8_t*)malloc(outputImage->frameSize);
    
    char* inputMediaFile = (char*)[inputFile UTF8String];
    
    int ret = getCoverImage(inputMediaFile, outputImage);
    
    if (ret) {
        if (outputImage!=NULL) {
            if (outputImage->data) {
                free(outputImage->data);
                outputImage->data = NULL;
            }
            delete outputImage;
            outputImage = NULL;
        }
        
        return nil;
    }else {
        NSDictionary *pixelBufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [NSDictionary dictionary], (id)kCVPixelBufferIOSurfacePropertiesKey,
                                               nil];
        
        CVPixelBufferRef pixelBuffer = nil;
        CVPixelBufferCreate(NULL, outputImage->width, outputImage->height, kCVPixelFormatType_32BGRA, (__bridge CFDictionaryRef)pixelBufferAttributes, &pixelBuffer);
        
        CVPixelBufferLockBaseAddress(pixelBuffer,0);
        uint8_t* data = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
//        int frameSize = (int)CVPixelBufferGetDataSize(pixelBuffer);
        int bytesPerRow = (int)CVPixelBufferGetBytesPerRow(pixelBuffer);
        
        if (outputImage->width*4!=bytesPerRow) {
            for (int i = 0; i<outputImage->height; i++) {
                memcpy(data+bytesPerRow*i, outputImage->data+outputImage->width*4*i, outputImage->width*4);
            }
        }else{
            memcpy(data, outputImage->data, outputImage->frameSize);
        }
        
        CVPixelBufferUnlockBaseAddress(pixelBuffer,0);
        
        if (outputImage!=NULL) {
            if (outputImage->data) {
                free(outputImage->data);
                outputImage->data = NULL;
            }
            delete outputImage;
            outputImage = NULL;
        }
        
//        CVPixelBufferCreateWithBytes(kCFAllocatorDefault, outputImage->width, outputImage->height, kCVPixelFormatType_32BGRA, outputImage->data, outputImage->width*4, cvPixelBufferReleaseBytesCallback, (void*)outputImage, nil, &pixelBuffer);
        
        return pixelBuffer;
    }
}

void cvPixelBufferReleaseBytesCallback( void * CV_NULLABLE releaseRefCon, const void * CV_NULLABLE baseAddress )
{
    VideoFrame* outputImage = (VideoFrame*)releaseRefCon;
    
    if (outputImage!=NULL) {
        if (outputImage->data) {
            free(outputImage->data);
            outputImage->data = NULL;
        }
        delete outputImage;
        outputImage = NULL;
    }
}

+ (BOOL)getCoverImageFileWithInputFile:(NSString*)inputFile OutputWidth:(int)width OutputHeight:(int)height OutputPNGFile:(NSString*)outputFile
{

    CVPixelBufferRef pixelBuffer = [SLKMediaProcesserUtils getCoverImageWithInputFile:inputFile OutputWidth:width OutputHeight:height];
    
    if (pixelBuffer==nil) return NO;

    CIImage *ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer];
    
    CIContext *temporaryContext = [CIContext contextWithOptions:nil];
    CGImageRef videoImage = [temporaryContext
                             createCGImage:ciImage
                             fromRect:CGRectMake(0, 0,
                                                 CVPixelBufferGetWidth(pixelBuffer),
                                                 CVPixelBufferGetHeight(pixelBuffer))];
    
    UIImage *uiImage = [UIImage imageWithCGImage:videoImage];
    CGImageRelease(videoImage);
    
    NSData* imageData = UIImagePNGRepresentation(uiImage);
    [imageData writeToFile:outputFile atomically:NO];

    CVPixelBufferRelease(pixelBuffer);
    
    return YES;
}

+ (BOOL)getCoverImageFileWithInputFile:(NSString*)inputFile OutputWidth:(int)width OutputHeight:(int)height OutputJPEGFile:(NSString*)outputFile
{
    CVPixelBufferRef pixelBuffer = [SLKMediaProcesserUtils getCoverImageWithInputFile:inputFile OutputWidth:width OutputHeight:height];
    
    if (pixelBuffer==nil) return NO;
    
    CIImage *ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer];
    
    CIContext *temporaryContext = [CIContext contextWithOptions:nil];
    CGImageRef videoImage = [temporaryContext
                             createCGImage:ciImage
                             fromRect:CGRectMake(0, 0,
                                                 CVPixelBufferGetWidth(pixelBuffer),
                                                 CVPixelBufferGetHeight(pixelBuffer))];
    
    UIImage *uiImage = [UIImage imageWithCGImage:videoImage];
    CGImageRelease(videoImage);
    
    NSData* imageData = UIImageJPEGRepresentation(uiImage,1.0f);
    [imageData writeToFile:outputFile atomically:NO];
    
    CVPixelBufferRelease(pixelBuffer);
    
    return YES;
}

+ (CVPixelBufferRef)getCoverImageWithInputFile:(NSString*)inputFile Position:(NSTimeInterval)position OutputWidth:(int)width OutputHeight:(int)height
{
    VideoFrame* outputImage = new VideoFrame();
    outputImage->width = width;
    outputImage->height = height;
    outputImage->frameSize = outputImage->width*outputImage->height*4;
    outputImage->data = (uint8_t*)malloc(outputImage->frameSize);
    
    char* inputMediaFile = (char*)[inputFile UTF8String];
    
    int ret = getCoverImageWithPosition(inputMediaFile, position, outputImage);
    
    if (ret) {
        if (outputImage!=NULL) {
            if (outputImage->data) {
                free(outputImage->data);
                outputImage->data = NULL;
            }
            delete outputImage;
            outputImage = NULL;
        }
        
        return nil;
    }else {
        NSDictionary *pixelBufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [NSDictionary dictionary], (id)kCVPixelBufferIOSurfacePropertiesKey,
                                               nil];
        
        CVPixelBufferRef pixelBuffer = nil;
        CVPixelBufferCreate(NULL, outputImage->width, outputImage->height, kCVPixelFormatType_32BGRA, (__bridge CFDictionaryRef)pixelBufferAttributes, &pixelBuffer);
        
        CVPixelBufferLockBaseAddress(pixelBuffer,0);
        uint8_t* data = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
//        int frameSize = (int)CVPixelBufferGetDataSize(pixelBuffer);
        int bytesPerRow = (int)CVPixelBufferGetBytesPerRow(pixelBuffer);
        
        if (outputImage->width*4!=bytesPerRow) {
            for (int i = 0; i<outputImage->height; i++) {
                memcpy(data+bytesPerRow*i, outputImage->data+outputImage->width*4*i, outputImage->width*4);
            }
        }else{
            memcpy(data, outputImage->data, outputImage->frameSize);
        }
        
        CVPixelBufferUnlockBaseAddress(pixelBuffer,0);
        
        if (outputImage!=NULL) {
            if (outputImage->data) {
                free(outputImage->data);
                outputImage->data = NULL;
            }
            delete outputImage;
            outputImage = NULL;
        }
        
        return pixelBuffer;
    }

}

+ (BOOL)getCoverImageFileWithInputFile:(NSString*)inputFile Position:(NSTimeInterval)position OutputWidth:(int)width OutputHeight:(int)height OutputPNGFile:(NSString*)outputFile
{
    CVPixelBufferRef pixelBuffer = [SLKMediaProcesserUtils getCoverImageWithInputFile:inputFile Position:position OutputWidth:width OutputHeight:height];
    
    if (pixelBuffer==nil) return NO;
    
    CIImage *ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer];
    
    CIContext *temporaryContext = [CIContext contextWithOptions:nil];
    CGImageRef videoImage = [temporaryContext
                             createCGImage:ciImage
                             fromRect:CGRectMake(0, 0,
                                                 CVPixelBufferGetWidth(pixelBuffer),
                                                 CVPixelBufferGetHeight(pixelBuffer))];
    
    UIImage *uiImage = [UIImage imageWithCGImage:videoImage];
    CGImageRelease(videoImage);
    
    NSData* imageData = UIImagePNGRepresentation(uiImage);
    [imageData writeToFile:outputFile atomically:NO];
    
    CVPixelBufferRelease(pixelBuffer);
    
    return YES;
}

+ (BOOL)getCoverImageFileWithInputFile:(NSString*)inputFile Position:(NSTimeInterval)position OutputWidth:(int)width OutputHeight:(int)height OutputJPEGFile:(NSString*)outputFile
{
    CVPixelBufferRef pixelBuffer = [SLKMediaProcesserUtils getCoverImageWithInputFile:inputFile Position:position OutputWidth:width OutputHeight:height];
    
    if (pixelBuffer==nil) return NO;
    
    CIImage *ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer];
    
    CIContext *temporaryContext = [CIContext contextWithOptions:nil];
    CGImageRef videoImage = [temporaryContext
                             createCGImage:ciImage
                             fromRect:CGRectMake(0, 0,
                                                 CVPixelBufferGetWidth(pixelBuffer),
                                                 CVPixelBufferGetHeight(pixelBuffer))];
    
    UIImage *uiImage = [UIImage imageWithCGImage:videoImage];
    CGImageRelease(videoImage);
    
    NSData* imageData = UIImageJPEGRepresentation(uiImage,1.0f);
    [imageData writeToFile:outputFile atomically:NO];
    
    CVPixelBufferRelease(pixelBuffer);
    
    return YES;
}

+ (BOOL)wordToBitmapWithInputWord:(NSString*)inputWord InputFontSize:(NSInteger)fontSize InputFontColor:(UIColor*)inputFontColor InputFontLibPath:(NSString*)inputFontLibPath OutputPNGFile:(NSString*)outputFile
{
    const char* fontLibPath = [inputFontLibPath UTF8String];
    FontColor fontColor;
    CGColorRef color = [inputFontColor CGColor];
    size_t numComponents = CGColorGetNumberOfComponents(color);
    if (numComponents == 4)
    {
        const CGFloat *components = CGColorGetComponents(color);
        fontColor.r = components[0];
        fontColor.g = components[1];
        fontColor.b = components[2];
        fontColor.a = components[3];
    }else if(numComponents == 3)
    {
        const CGFloat *components = CGColorGetComponents(color);
        fontColor.r = components[0];
        fontColor.g = components[1];
        fontColor.b = components[2];
        fontColor.a = 1.0f;
    }
    Word2Bitmap *pWord2Bitmap = new Word2Bitmap(fontLibPath, (int)fontSize, fontColor);
    bool ret = pWord2Bitmap->initialize();
    
    if (!ret) {
        delete pWord2Bitmap;
        pWord2Bitmap = NULL;
        
        return NO;
    }
    
    const char* inputChar = [inputWord UTF8String];
    size_t size = strlen(inputChar) + 1;
    wchar_t* inputWChar = new wchar_t[size];
    mbstowcs(inputWChar, inputChar, size);
    
    VideoFrame* wordBitmap = pWord2Bitmap->word2Bitmap(inputWChar[0], 1.0f);
    delete [] inputWChar;
    
    pWord2Bitmap->terminate();
    delete pWord2Bitmap;
    pWord2Bitmap = NULL;
    
    if (wordBitmap==NULL) {
        return NO;
    }
    
    int iret = RGBAToPNGFile((char*)wordBitmap->data, wordBitmap->width, wordBitmap->height, (char*)[outputFile UTF8String]);
    if (wordBitmap) {
        if (wordBitmap->data) {
            free(wordBitmap->data);
        }
        delete wordBitmap;
    }
    
    if (iret) {
        return NO;
    }
    
    return YES;
}

+ (CVPixelBufferRef)wordToBitmapWithInputWord:(NSString*)inputWord InputFontSize:(NSInteger)fontSize InputFontColor:(UIColor*)inputFontColor InputFontLibPath:(NSString*)inputFontLibPath
{
    const char* fontLibPath = [inputFontLibPath UTF8String];
    FontColor fontColor;
    CGColorRef color = [inputFontColor CGColor];
    size_t numComponents = CGColorGetNumberOfComponents(color);
    if (numComponents == 4)
    {
        const CGFloat *components = CGColorGetComponents(color);
        fontColor.r = components[0];
        fontColor.g = components[1];
        fontColor.b = components[2];
        fontColor.a = components[3];
    }else if(numComponents == 3)
    {
        const CGFloat *components = CGColorGetComponents(color);
        fontColor.r = components[0];
        fontColor.g = components[1];
        fontColor.b = components[2];
        fontColor.a = 1.0f;
    }
    Word2Bitmap *pWord2Bitmap = new Word2Bitmap(fontLibPath, (int)fontSize, fontColor);
    bool ret = pWord2Bitmap->initialize();
    
    if (!ret) {
        delete pWord2Bitmap;
        pWord2Bitmap = NULL;
        
        return nil;
    }
    
    const char* inputChar = [inputWord UTF8String];
    size_t size = strlen(inputChar) + 1;
    wchar_t* inputWChar = new wchar_t[size];
    mbstowcs(inputWChar, inputChar, size);
    
    VideoFrame* wordBitmap = pWord2Bitmap->word2Bitmap(inputWChar[0], 1.0f);
    delete [] inputWChar;
    
    pWord2Bitmap->terminate();
    delete pWord2Bitmap;
    pWord2Bitmap = NULL;
    
    if (wordBitmap==NULL) {
        return nil;
    }
    
    NSDictionary *pixelBufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                           [NSDictionary dictionary], (id)kCVPixelBufferIOSurfacePropertiesKey,
                                           nil];
    
    CVPixelBufferRef pixelBuffer = nil;
    CVPixelBufferCreate(NULL, wordBitmap->width, wordBitmap->height, kCVPixelFormatType_32BGRA, (__bridge CFDictionaryRef)pixelBufferAttributes, &pixelBuffer);
    
    CVPixelBufferLockBaseAddress(pixelBuffer,0);
    uint8_t* data = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
//    int frameSize = (int)CVPixelBufferGetDataSize(pixelBuffer);
    int bytesPerRow = (int)CVPixelBufferGetBytesPerRow(pixelBuffer);
    
    if (wordBitmap->width*4!=bytesPerRow) {
        for (int i = 0; i<wordBitmap->height; i++) {
            memcpy(data+bytesPerRow*i, wordBitmap->data+wordBitmap->width*4*i, wordBitmap->width*4);
        }
    }else{
        memcpy(data, wordBitmap->data, wordBitmap->frameSize);
    }
    
    CVPixelBufferUnlockBaseAddress(pixelBuffer,0);
    
    if (wordBitmap) {
        if (wordBitmap->data) {
            free(wordBitmap->data);
        }
        delete wordBitmap;
    }
    
    return pixelBuffer;    
}

@end
