//
//  SLKMediaInfo.m
//  MediaStreamer
//
//  Created by Think on 2016/11/29.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "SLKMediaInfo.h"
#include "SLKMediaInfoWrapper.h"
#include <mutex>

@implementation SLKMediaDetailInfo
/*
- (void)dealloc
{
    NSLog(@"SLKMediaDetailInfo dealloc");
}
*/
@end

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation SLKMediaInfo
{
    SLKMediaInfoWrapper* pSLKMediaInfoWrapper;
    std::mutex mSLKMediaInfoWrapperMutex;
    
    dispatch_queue_t notificationQueue;
}

+ (SLKMediaDetailInfo*)syncGetMediaDetailInfoWithInputFile:(NSString*)inputMediaFile
{
    SLKMediaDetailInfoWrapper* detailInfoWrapper = SLKMediaInfoWrapper_syncGetMediaDetailInfo((char*)[inputMediaFile UTF8String]);
    
    if (detailInfoWrapper) {
        SLKMediaDetailInfo *slkMediaDetailInfo = [[SLKMediaDetailInfo alloc] init];

        if (detailInfoWrapper->hasVideo) {
            slkMediaDetailInfo.hasVideo = YES;
        }else{
            slkMediaDetailInfo.hasVideo = NO;
        }
        
        if (detailInfoWrapper->hasAudio) {
            slkMediaDetailInfo.hasAudio = YES;
        }else{
            slkMediaDetailInfo.hasAudio = NO;
        }

        slkMediaDetailInfo.width = detailInfoWrapper->width;
        slkMediaDetailInfo.height = detailInfoWrapper->height;
        slkMediaDetailInfo.fps = detailInfoWrapper->fps;
        slkMediaDetailInfo.kbps = detailInfoWrapper->bps;
        slkMediaDetailInfo.durationMs = detailInfoWrapper->duration;
        
        slkMediaDetailInfo.rotate = detailInfoWrapper->rotate;
        
        if (detailInfoWrapper->isH264Codec) {
            slkMediaDetailInfo.isH264Codec = YES;
        }else{
            slkMediaDetailInfo.isH264Codec = NO;
        }
        
        if (detailInfoWrapper->isAACCodec) {
            slkMediaDetailInfo.isAACCodec = YES;
        }else{
            slkMediaDetailInfo.isAACCodec = NO;
        }
        
        delete detailInfoWrapper;
        
        return slkMediaDetailInfo;
    }
    
    return nil;
}


+ (NSTimeInterval)syncGetMediaDurationWithInputFile:(NSString*)inputMediaFile
{
    return SLKMediaInfoWrapper_syncGetMediaDuration((char*)[inputMediaFile UTF8String]);
}

+ (BOOL)syncGetCoverImageFileWithInputFile:(NSString*)inputMediaFile OutputWidth:(int)width OutputHeight:(int)height OutputPNGFile:(NSString*)outputPNGFile
{
    return SLKMediaInfoWrapper_syncGetCoverImageToImageFile((char*)[inputMediaFile UTF8String], width, height, (char*)[outputPNGFile UTF8String]);
}
+ (BOOL)syncGetCoverImageFileWithInputFile:(NSString*)inputMediaFile Position:(NSTimeInterval)position OutputWidth:(int)width OutputHeight:(int)height OutputPNGFile:(NSString*)outputPNGFile
{
    return SLKMediaInfoWrapper_syncGetCoverImageToImageFileWithPosition((char*)[inputMediaFile UTF8String], position, width, height, (char*)[outputPNGFile UTF8String]);
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        notificationQueue = dispatch_queue_create("SLKMediaInfoNotificationQueue", 0);
    }
    
    return self;
}

- (void)initialize
{
    mSLKMediaInfoWrapperMutex.lock();
    
    pSLKMediaInfoWrapper = SLKMediaInfoWrapper_GetInstance();
    SLKMediaInfoWrapper_setMediaDetailInfoListener(pSLKMediaInfoWrapper, mediaDetailInfoListener, (__bridge void*)self);
    SLKMediaInfoWrapper_setMediaThumbnailListener(pSLKMediaInfoWrapper, mediaThumbnailListener, (__bridge void*)self);
    SLKMediaInfoWrapper_setMediaInfoStatusListener(pSLKMediaInfoWrapper, mediainfonotificationListener, (__bridge void*)self);

    mSLKMediaInfoWrapperMutex.unlock();
}

/////////////////////////////////////////////////////////////////////////////////////
void mediaDetailInfoListener(void* owner, int64_t duration, int width, int height)
{
    @autoreleasepool {
        __weak SLKMediaInfo *thiz = (__bridge SLKMediaInfo*)owner;
        if(thiz!=nil)
        {
            [thiz handleMediaDetailInfoWithDuration:duration WithWidth:width WithHeight:height];
        }
    }
}

- (void)handleMediaDetailInfoWithDuration:(int64_t)duration WithWidth:(int)width WithHeight:(int)height
{
    if (self.delegate!=nil) {
        if (([self.delegate respondsToSelector:@selector(gotMediaDetailInfoWithDuration:WithWidth:WithHeight:)])) {
            [self.delegate gotMediaDetailInfoWithDuration:duration WithWidth:width WithHeight:height];
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////
void mediaThumbnailListener(void* owner, uint8_t *data, int size, int width, int height)
{
    @autoreleasepool {
        __weak SLKMediaInfo *thiz = (__bridge SLKMediaInfo*)owner;
        if(thiz!=nil)
        {
            [thiz handleMediaThumbnailWithData:data WithSize:size WithWidth:width WithHeight:height];
        }
    }
}

- (void)handleMediaThumbnailWithData:(uint8_t*)data WithSize:(int)size WithWidth:(int)width WithHeight:(int)height
{
    NSDictionary *pixelBufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                           [NSDictionary dictionary], (id)kCVPixelBufferIOSurfacePropertiesKey,
                                           nil];
    
    CVPixelBufferRef pixelBuffer = nil;
    CVPixelBufferCreate(NULL, width, height, kCVPixelFormatType_32BGRA, (__bridge CFDictionaryRef)pixelBufferAttributes, &pixelBuffer);
    
    CVPixelBufferLockBaseAddress(pixelBuffer,0);
    uint8_t* dst_data = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
    int dst_frameSize = (int)CVPixelBufferGetDataSize(pixelBuffer);
    int dst_bytesPerRow = (int)CVPixelBufferGetBytesPerRow(pixelBuffer);
    
    if (width*4!=dst_bytesPerRow) {
        for (int i = 0; i<height; i++) {
            memcpy(dst_data+dst_bytesPerRow*i, data+width*4*i, width*4);
        }
    }else{
        memcpy(dst_data, data, size);
    }
    
    CVPixelBufferUnlockBaseAddress(pixelBuffer,0);

    if (self.delegate!=nil) {
        if (([self.delegate respondsToSelector:@selector(gotThumbnailWithCVPixelBuffer:)])) {
            [self.delegate gotThumbnailWithCVPixelBuffer:pixelBuffer];
        }
    }
}


////////////////////////////////////////////////////////////////////////////////////////
void mediainfonotificationListener(void* owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak SLKMediaInfo *thiz = (__bridge SLKMediaInfo*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    __weak typeof(self) wself = self;
    
    dispatch_async(notificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf handleNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
        
    });
}

- (void)handleNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    switch (event) {
        case SLK_MEDIA_PROCESSER_ERROR:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotErrorWithErrorType:)])) {
                    [self.delegate gotErrorWithErrorType:ext1];
                }
            }
            break;
        case SLK_MEDIA_PROCESSER_INFO:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotInfoWithInfoType:InfoValue:)])) {
                    [self.delegate gotInfoWithInfoType:ext1 InfoValue:ext2];
                }
            }
            break;
            case SLK_MEDIA_PROCESSER_END:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(didEnd)])) {
                    [self.delegate didEnd];
                }
            }
            break;
            
        default:
            break;
    }
}
//////////////////////////////////////////////////////////////////////////////////////////////////

- (void)setThumbnailsOptionWithWidth:(int)width WithHeight:(int)height WithThumbnailCount:(int)thumbnailCount
{
    mSLKMediaInfoWrapperMutex.lock();
    
    if (pSLKMediaInfoWrapper!=NULL) {
        SLKMediaInfoWrapper_setMediaThumbnailsOption(pSLKMediaInfoWrapper, width, height, thumbnailCount);
    }
    
    mSLKMediaInfoWrapperMutex.unlock();
}

- (void)loadAsync:(NSString*)inputMediaFile
{
    mSLKMediaInfoWrapperMutex.lock();
    
    if (pSLKMediaInfoWrapper!=NULL) {
        SLKMediaInfoWrapper_loadAsync(pSLKMediaInfoWrapper, (char*)[inputMediaFile UTF8String]);
    }
    
    mSLKMediaInfoWrapperMutex.unlock();
}

- (void)loadAsync:(NSString*)inputMediaFile StartPos:(NSTimeInterval)startPos EndPos:(NSTimeInterval)endPos
{
    mSLKMediaInfoWrapperMutex.lock();
    
    if (pSLKMediaInfoWrapper!=NULL) {
        SLKMediaInfoWrapper_loadAsyncWithPosition(pSLKMediaInfoWrapper, (char*)[inputMediaFile UTF8String], startPos, endPos);
    }
    
    mSLKMediaInfoWrapperMutex.unlock();
}

- (void)quit
{
    mSLKMediaInfoWrapperMutex.lock();
    
    if (pSLKMediaInfoWrapper!=NULL)
    {
        SLKMediaInfoWrapper_quit(pSLKMediaInfoWrapper);
    }
    
    mSLKMediaInfoWrapperMutex.unlock();

}

- (void)terminate
{
    mSLKMediaInfoWrapperMutex.lock();
    
    if (pSLKMediaInfoWrapper!=NULL) {
        SLKMediaInfoWrapper_ReleaseInstance(&pSLKMediaInfoWrapper);
        pSLKMediaInfoWrapper = NULL;
    }
    
    mSLKMediaInfoWrapperMutex.unlock();
    
    dispatch_barrier_sync(notificationQueue, ^{
        NSLog(@"finish all notifications");
    });
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"SLKMediaInfo dealloc");
}

@end
