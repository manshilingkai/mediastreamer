//
//  SLKMediaInfoWrapper.h
//  MediaStreamer
//
//  Created by Think on 2016/11/29.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef SLKMediaInfoWrapper_h
#define SLKMediaInfoWrapper_h

#include <stdio.h>
#include <inttypes.h>

enum slk_media_processer_event_type {
    SLK_MEDIA_PROCESSER_ERROR = 0,
    SLK_MEDIA_PROCESSER_INFO = 1,
    SLK_MEDIA_PROCESSER_END = 2,
};

struct SLKMediaInfoWrapper;
struct SLKMediaDetailInfoWrapper{
    bool hasVideo;
    bool hasAudio;
    int width;
    int height;
    int fps;
    int bps;//kbps
    int64_t duration; //ms
    
    int rotate;
    
    bool isH264Codec;
    bool isAACCodec;
    
    SLKMediaDetailInfoWrapper()
    {
        hasVideo = false;
        hasAudio = false;
        
        width = 0;
        height = 0;
        fps = 0;
        bps = 0;
        duration = 0ll;
        
        rotate = 0;
        
        isH264Codec = false;
        isAACCodec = false;
    }
};

#ifdef __cplusplus
extern "C" {
#endif
    struct SLKMediaDetailInfoWrapper* SLKMediaInfoWrapper_syncGetMediaDetailInfo(char* inputMediaFile);
    int64_t SLKMediaInfoWrapper_syncGetMediaDuration(char* inputMediaFile);
    bool SLKMediaInfoWrapper_syncGetCoverImageToImageFile(char* inputMediaFile, int outputWidth, int outputHeight, char* outputImageFile);
    bool SLKMediaInfoWrapper_syncGetCoverImageToImageFileWithPosition(char* inputMediaFile, int64_t seekPositionMS, int outputWidth, int outputHeight, char* outputImageFile);
    
    struct SLKMediaInfoWrapper *SLKMediaInfoWrapper_GetInstance();
    void SLKMediaInfoWrapper_ReleaseInstance(struct SLKMediaInfoWrapper **ppInstance);
    
    void SLKMediaInfoWrapper_setMediaThumbnailsOption(struct SLKMediaInfoWrapper *pInstance, int width, int height, int thumbnailCount);

    void SLKMediaInfoWrapper_setMediaDetailInfoListener(struct SLKMediaInfoWrapper *pInstance, void (*listener)(void*,int64_t,int,int), void* arg);
    void SLKMediaInfoWrapper_setMediaThumbnailListener(struct SLKMediaInfoWrapper *pInstance, void (*listener)(void*,uint8_t *,int,int,int), void* arg);
    void SLKMediaInfoWrapper_setMediaInfoStatusListener(struct SLKMediaInfoWrapper *pInstance, void (*listener)(void*,int,int,int), void* arg);
    
    void SLKMediaInfoWrapper_loadAsync(struct SLKMediaInfoWrapper *pInstance, char* inputMediaFile);
    void SLKMediaInfoWrapper_loadAsyncWithPosition(struct SLKMediaInfoWrapper *pInstance, char* inputMediaFile, int64_t startPos, int64_t endPos);
    void SLKMediaInfoWrapper_quit(struct SLKMediaInfoWrapper *pInstance);
    
#ifdef __cplusplus
};
#endif

#endif /* SLKMediaInfoWrapper_h */
