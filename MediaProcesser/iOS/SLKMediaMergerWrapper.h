//
//  SLKMediaMergerWrapper.h
//  MediaStreamer
//
//  Created by Think on 2016/11/21.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef SLKMediaMergerWrapper_h
#define SLKMediaMergerWrapper_h

#include <stdio.h>
#include <inttypes.h>

#include "MediaMaterial.h"
#include "MediaEffect.h"
#include "MediaMergeAlgorithm.h"
#include "MediaProduct.h"

enum slk_media_processer_event_type {
    SLK_MEDIA_PROCESSER_ERROR = 0,
    SLK_MEDIA_PROCESSER_INFO = 1,
    SLK_MEDIA_PROCESSER_END = 2,
};

struct SLKMediaMergerWrapper;

#ifdef __cplusplus
extern "C" {
#endif
    struct SLKMediaMergerWrapper *SLKMediaMergerWrapper_GetInstance(MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup, MEDIA_MERGE_ALGORITHM algorithm, MediaProduct *ouputProduct);
    void SLKMediaMergerWrapper_ReleaseInstance(struct SLKMediaMergerWrapper **ppInstance, MEDIA_MERGE_ALGORITHM algorithm);
    
    void SLKMediaMergerWrapper_setListener(struct SLKMediaMergerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner);
    
    void SLKMediaMergerWrapper_start(struct SLKMediaMergerWrapper *pInstance);
    void SLKMediaMergerWrapper_resume(struct SLKMediaMergerWrapper *pInstance);
    void SLKMediaMergerWrapper_pause(struct SLKMediaMergerWrapper *pInstance);
    void SLKMediaMergerWrapper_stop(struct SLKMediaMergerWrapper *pInstance);
        
#ifdef __cplusplus
};
#endif
#endif /* SLKMediaMergerWrapper_h */
