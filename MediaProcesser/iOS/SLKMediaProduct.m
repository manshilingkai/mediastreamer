//
//  SLKMediaProduct.m
//  MediaStreamer
//
//  Created by Think on 2016/11/22.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "SLKMediaProduct.h"

@implementation SLKMediaProduct

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.iD = -1;
        self.url = nil;
        self.slk_media_product_type = SLK_MEDIA_PRODUCT_TYPE_UNKNOWN;
        self.hasVideo = NO;
        self.videoSize = CGSizeMake(0,0);
        self.isAspectFit = NO;
        self.hasAudio = NO;
        self.bps = 0;
    }
    
    return self;
}

@end
