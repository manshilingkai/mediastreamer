//
//  MediaInfo.cpp
//  MediaStreamer
//
//  Created by Think on 2016/11/28.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include <string.h>
#include "MediaInfo.h"
#include "IMediaListener.h"
#include "MediaLog.h"

#ifdef IOS
#include <QuartzCore/QuartzCore.h>
#include <CoreVideo/CoreVideo.h>
#endif

#ifdef ANDROID
#include "AndroidUtils.h"
#include "JNIHelp.h"
#endif

#include "StringUtils.h"
#include "FileUtils.h"
#include "ImageProcesserUtils.h"

#include "MediaCoverImage.h"

#include <sys/resource.h>

MediaDetailInfo* MediaInfo::syncGetMediaDetailInfo(char* inputMediaFile)
{
    FFmpegReader *ffmpegReader = new FFmpegReader(inputMediaFile);
    
    bool bret = ffmpegReader->open();
    if (!bret)
    {
        if (ffmpegReader!=NULL) {
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        return NULL;
    }
    AVFormatContext* avFormatContext = ffmpegReader->getAVFormatContext();
    MediaDetailInfo* detailInfo = new MediaDetailInfo;
    int64_t durationMs = ffmpegReader->getDuration();
    detailInfo->duration = durationMs;
    
    int64_t fileSize = FileUtils::getFileSize(inputMediaFile);
    if (durationMs==0) {
        detailInfo->bps = 0;
    }else{
        detailInfo->bps = (int)(fileSize * 8 / durationMs);
    }
    
    AVStream* videoStream = ffmpegReader->getVideoStreamContext();
    if (videoStream) {
        detailInfo->hasVideo = true;
        
        detailInfo->width = videoStream->codec->width;
        detailInfo->height = videoStream->codec->height;
        
        AVRational fr = av_guess_frame_rate(avFormatContext, videoStream, NULL);
        if(fr.num > 0 && fr.den > 0)
        {
            detailInfo->fps = fr.num/fr.den;
        }
        
        if (videoStream->codec->codec_id==AV_CODEC_ID_H264) {
            detailInfo->isH264Codec = true;
        }
        
        AVDictionaryEntry *m = NULL;
        while((m=av_dict_get(videoStream->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
            if(strcmp(m->key, "rotate")) continue;
            else{
                detailInfo->rotate = atoi(m->value);
            }
        }
    }
    
    AVStream* audioStream = ffmpegReader->getAudioStreamContext();
    if (audioStream) {
        detailInfo->hasAudio = true;
        
        if (audioStream->codec->codec_id==AV_CODEC_ID_AAC) {
            detailInfo->isAACCodec = true;
        }
    }
    
    if (ffmpegReader!=NULL) {
        ffmpegReader->close();
        delete ffmpegReader;
        ffmpegReader = NULL;
    }
    
    return detailInfo;
}

int64_t MediaInfo::syncGetMediaDuration(char* inputMediaFile)
{
    FFmpegReader *ffmpegReader = new FFmpegReader(inputMediaFile);
    
    bool bret = ffmpegReader->open();
    if (!bret)
    {
        if (ffmpegReader!=NULL) {
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        return -1;
    }
    
    int64_t durationMs = ffmpegReader->getDuration();
    
    if (ffmpegReader!=NULL) {
        ffmpegReader->close();
        delete ffmpegReader;
        ffmpegReader = NULL;
    }
    
    return durationMs;
}

int MediaInfo::syncGetCoverImage(char* inputFile, VideoFrame* outputImage)
{
    return getCoverImage(inputFile, outputImage);
}

int MediaInfo::syncGetCoverImageWithPosition(char* inputFile, int64_t seekPositionMS, VideoFrame* outputImage)
{
    return getCoverImageWithPosition(inputFile, seekPositionMS, outputImage);
}

int MediaInfo::syncGetCoverImageToImageFile(char* inputMediaFile, int outputWidth, int outputHeight, char* outputImageFile)
{
    return getCoverImageToImageFile(inputMediaFile, outputWidth, outputHeight, outputImageFile);
}

int MediaInfo::syncGetCoverImageToImageFileWithPosition(char* inputMediaFile, int64_t seekPositionMS, int outputWidth, int outputHeight, char* outputImageFile)
{
    return getCoverImageToImageFileWithPosition(inputMediaFile, seekPositionMS, outputWidth, outputHeight, outputImageFile);
}

#ifdef ANDROID
MediaInfo::MediaInfo(JavaVM *jvm, char* saveDir)
{
    mJvm = jvm;
    mSaveDir = strdup(saveDir);
    
    mObject = NULL;
    mClass = NULL;
    
    mInputMediaFile = NULL;
    mStartPos = 0ll;
    mEndPos = 0ll;
    mIsSetPosition = false;
    mDuration = 0ll;
    
    isBreakThread = false;
    
    //////
    mFFmpegReader = NULL;
    mVideoDecoder = NULL;
    mColorSpaceConvert = NULL;
}
#else
MediaInfo::MediaInfo()
{
    mInputMediaFile = NULL;
    mStartPos = 0ll;
    mEndPos = 0ll;
    mIsSetPosition = false;
    mDuration = 0ll;
    
    mMediaDetailInfoListener = NULL;
    mMediaDetailInfoOwner = NULL;
    
    mMediaThumbnailListener = NULL;
    mMediaThumbnailOwner = NULL;
    
    mMediaInfoStatusListener = NULL;
    mMediaInfoStatusOwner = NULL;
    
    isBreakThread = false;
    
    //////
    mFFmpegReader = NULL;
    mVideoDecoder = NULL;
    mColorSpaceConvert = NULL;
}
#endif

MediaInfo::~MediaInfo()
{
#ifdef ANDROID
    if (mSaveDir!=NULL) {
        free(mSaveDir);
        mSaveDir = NULL;
    }
    
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    
    if (env!=NULL) {
        // remove global references
        env->DeleteGlobalRef(mObject);
        env->DeleteGlobalRef(mClass);
    }
#endif
}


void MediaInfo::setMediaThumbnailsOption(int width, int height, int thumbnailCount)
{
    mThumbnailWidth = width;
    mThumbnailHeight = height;
    mThumbnailCount = thumbnailCount;
}

#ifdef IOS
void MediaInfo::setMediaDetailInfoListener(void (*listener)(void*,int64_t,int,int), void* arg)
{
    mMediaDetailInfoListener = listener;
    mMediaDetailInfoOwner = arg;
}

void MediaInfo::setMediaThumbnailListener(void (*listener)(void*,uint8_t *,int,int,int), void* arg)
{
    mMediaThumbnailListener = listener;
    mMediaThumbnailOwner = arg;
}

void MediaInfo::setMediaInfoStatusListener(void (*listener)(void*,int,int,int), void* arg)
{
    mMediaInfoStatusListener = listener;
    mMediaInfoStatusOwner = arg;
}
#endif

#ifdef ANDROID
void MediaInfo::setMediaListener(jobject thiz, jobject weak_thiz, jmethodID post_detailinfo_event, jmethodID post_thumbnail_event, jmethodID post_status_event)
{
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    
    if (env!=NULL) {
        // Hold onto the MediaPlayer class for use in calling the static method
        // that posts events to the application thread.
        jclass clazz = env->GetObjectClass(thiz);
        if (clazz == NULL) {
            jniThrowException(env, "java/lang/Exception", NULL);
            return;
        }
        
        mClass = (jclass)env->NewGlobalRef(clazz);
        
        // We use a weak reference so the MediaPlayer object can be garbage collected.
        // The reference is only used as a proxy for callbacks.
        mObject  = env->NewGlobalRef(weak_thiz);
        
        mPostMediaDetailInfoEvent = post_detailinfo_event;
        mPostMediaThumbnailEvent = post_thumbnail_event;
        mPostMediaStatusEvent = post_status_event;
    }
}

#endif

void MediaInfo::notifyWithMediaDetailInfo(int64_t duration, int videoWidth, int videoHeight)
{
#ifdef IOS
    if (mMediaDetailInfoListener!=NULL) {
        mMediaDetailInfoListener(mMediaDetailInfoOwner, duration, videoWidth, videoHeight);
    }
#endif

#ifdef ANDROID
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    
    if (env!=NULL)
    {
        env->CallStaticVoidMethod(mClass, mPostMediaDetailInfoEvent, mObject, jlong(duration), videoWidth, videoHeight, 0);
    }
#endif
}

void MediaInfo::notifyWithMediaThumbnail(uint8_t *data, int size, int width, int height)
{
#ifdef IOS

    if (mMediaThumbnailListener!=NULL) {
        mMediaThumbnailListener(mMediaThumbnailOwner, data, size, width, height);
    }
#endif
    
#ifdef ANDROID
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    
    int dirLen = strlen(mSaveDir)+1;
    char imagePath[dirLen+64];
    
    mThumbnailId++;
    if (mSaveDir[dirLen-2]=='/') {
        sprintf(imagePath, "%s%d.png",mSaveDir,mThumbnailId);
    }else{
        sprintf(imagePath, "%s/%d.png",mSaveDir,mThumbnailId);
    }
    
    int ret = RGBAToPNGFile(data, width, height, imagePath);
    
    if (ret) {
        if (env!=NULL)
        {
            env->CallStaticVoidMethod(mClass, mPostMediaStatusEvent, mObject, MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_SAVE_PNG_FAIL, 0, 0);
        }
    }else{
        jstring jimagepath = env->NewStringUTF(imagePath);
        
        if (env!=NULL)
        {
            env->CallStaticVoidMethod(mClass, mPostMediaThumbnailEvent, mObject, jimagepath, 0);
        }
        env->DeleteLocalRef(jimagepath);
    }
#endif

}

void MediaInfo::notifyWithStatus(int event, int ext1, int ext2)
{
#ifdef IOS

    if (mMediaInfoStatusListener!=NULL) {
        mMediaInfoStatusListener(mMediaInfoStatusOwner, event, ext1, ext2);
    }
#endif

#ifdef ANDROID
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    
    if (env!=NULL)
    {
        env->CallStaticVoidMethod(mClass, mPostMediaStatusEvent, mObject, event, ext1, ext2, 0);
    }
#endif
}

void MediaInfo::createProcessThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, &attr, handleProcessThread, this);
    
    pthread_attr_destroy(&attr);
}

void* MediaInfo::handleProcessThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    MediaInfo* mediaInfo = (MediaInfo*)ptr;
    mediaInfo->processThreadMain();
    
    return NULL;
}

void MediaInfo::processThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    open_all_pipelines_l();
    
    int index = -1;
    bool isIdle = true;
    FFSeekStatus seekStatus;
    
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        pthread_mutex_unlock(&mLock);

        if (isIdle) {
            index++;
            
            if (index>=mThumbnailCount) {
                
                notifyWithStatus(MEDIA_PROCESSER_END,0,0);
                
                pthread_mutex_lock(&mLock);
                pthread_cond_wait(&mCondition, &mLock);
                pthread_mutex_unlock(&mLock);
                continue;
            }
            
            isIdle = false;
            seekStatus = mFFmpegReader->seek(mStartPos+mDuration*index/mThumbnailCount);
            if (!seekStatus.seekRet) {
                notifyWithStatus(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_SEEK_INPUT_FILE_FAIL,0);
                pthread_mutex_lock(&mLock);
                pthread_cond_wait(&mCondition, &mLock);
                pthread_mutex_unlock(&mLock);
                continue;
            }else{
                mVideoDecoder->flush();
            }
        }
        if (!isIdle) {
            FFPacket* ffPacket = mFFmpegReader->getMediaPacket();
            
            if (ffPacket->ret==AVERROR_INVALIDDATA || ffPacket->ret == AVERROR(EAGAIN))
            {
                av_packet_unref(ffPacket->avPacket);
                av_freep(&ffPacket->avPacket);
                
                continue;
            }else if(ffPacket->ret == AVERROR_EOF)
            {
                av_packet_unref(ffPacket->avPacket);
                av_freep(&ffPacket->avPacket);
                
                pthread_mutex_lock(&mLock);
                pthread_cond_wait(&mCondition, &mLock);
                pthread_mutex_unlock(&mLock);
                continue;
            }else if (ffPacket->ret < 0)
            {
                av_packet_unref(ffPacket->avPacket);
                av_freep(&ffPacket->avPacket);
                
                notifyWithStatus(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_READ_INPUT_FILE_FAIL, 0);
                
                pthread_mutex_lock(&mLock);
                pthread_cond_wait(&mCondition, &mLock);
                pthread_mutex_unlock(&mLock);

                continue;
            }else{
                if (ffPacket->mediaType==0) {
                    int iret = mVideoDecoder->decode(ffPacket->avPacket);
                    av_packet_unref(ffPacket->avPacket);
                    av_freep(&ffPacket->avPacket);
                    
                    if (iret<0) {
                        notifyWithStatus(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_VIDEO_DECODE_FAIL,0);
                        pthread_mutex_lock(&mLock);
                        pthread_cond_wait(&mCondition, &mLock);
                        pthread_mutex_unlock(&mLock);
                        continue;
                    }else if (iret==0) {
                        continue;
                    }else {
                        AVFrame* videoFrame = mVideoDecoder->getFrame();
                        if (videoFrame==NULL) {
                            continue;
                        }else{
                            if (videoFrame->pkt_pts<seekStatus.seekTargetPos) {
                                mVideoDecoder->clearFrame();
                                continue;
                            }else{
                                AVFrameToVideoFrame(videoFrame, &mSourceVideoFrame);
#ifdef IOS
                                bool ret = mColorSpaceConvert->I420toABGR_Crop_Rotation_Scale(&mSourceVideoFrame, &mTargetVideoFrame, mSourceVideoFrame.rotation);
#endif
#ifdef ANDROID
                                bool ret = mColorSpaceConvert->I420toABGR_Crop_Rotation_Scale(&mSourceVideoFrame, &mTargetVideoFrame, mSourceVideoFrame.rotation);
#endif
                                if (!ret) {
                                    notifyWithStatus(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_COLORSPACECONVERT_FAIL, 0);
                                }else{
                                    notifyWithMediaThumbnail(mTargetVideoFrame.data, mTargetVideoFrame.frameSize, mTargetVideoFrame.width, mTargetVideoFrame.height);
                                }
                                
                                mVideoDecoder->clearFrame();
                                isIdle = true;
                                continue;
                            }
                        }
                    }
                    
                }else {
                    av_packet_unref(ffPacket->avPacket);
                    av_freep(&ffPacket->avPacket);
                    continue;
                }
            }

        }
    }
    
    close_all_pipelines_l();
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void MediaInfo::open_all_pipelines_l()
{
    mFFmpegReader = new FFmpegReader(mInputMediaFile);
    bool bret = mFFmpegReader->open();
    if (!bret) {
        LOGE("Open Input File Fail : %s",mInputMediaFile);
        notifyWithStatus(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL,0);
        pthread_mutex_lock(&mLock);
        pthread_cond_wait(&mCondition, &mLock);
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    AVStream* videoStreamContext = mFFmpegReader->getVideoStreamContext();
    if (videoStreamContext==NULL) {
        LOGE("Open Input File Fail : %s",mInputMediaFile);
        notifyWithStatus(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL,0);
        pthread_mutex_lock(&mLock);
        pthread_cond_wait(&mCondition, &mLock);
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    //open video decoder
    if (videoStreamContext!=NULL) {
#ifdef ANDROID
        mVideoDecoderType = VIDEO_DECODER_FFMPEG;
        if(mVideoDecoderType==VIDEO_DECODER_MEDIACODEC_JAVA)
        {
            mVideoDecoder = VideoDecoder::CreateVideoDecoderWithJniEnv(mVideoDecoderType, mJvm, NULL);
        }else{
            mVideoDecoder = VideoDecoder::CreateVideoDecoder(mVideoDecoderType);
        }
#endif

#ifdef IOS
        mVideoDecoderType=VIDEO_DECODER_FFMPEG;
        mVideoDecoder = VideoDecoder::CreateVideoDecoder(mVideoDecoderType);
#endif
        
        if (!mVideoDecoder->open(videoStreamContext)) {
            notifyWithStatus(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_VIDEO_DECODER_FAIL, 0);
            pthread_mutex_lock(&mLock);
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            return;
        }
        
        AVDictionaryEntry *m = NULL;
        while((m=av_dict_get(videoStreamContext->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
            printf("%s : %s\n", m->key,m->value);
            
            if(strcmp(m->key, "rotate")) continue;
            else{
                int rotate = atoi(m->value);
                mSourceVideoFrame.rotation = rotate;
            }
        }
    }
    
    //open video preprocess
    mColorSpaceConvert = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
    
    //malloc
    mTargetVideoFrame.frameSize = mThumbnailWidth*mThumbnailHeight*4;
    mTargetVideoFrame.data = (uint8_t*)malloc(mTargetVideoFrame.frameSize);
    mTargetVideoFrame.width = mThumbnailWidth;
    mTargetVideoFrame.height = mThumbnailHeight;
    mTargetVideoFrame.rotation = 0;
    
    mSourceVideoFrame.frameSize = videoStreamContext->codec->width*videoStreamContext->codec->height*4;
    mSourceVideoFrame.data = (uint8_t*)malloc(mSourceVideoFrame.frameSize);
    
    if (mIsSetPosition) {
        if (mStartPos<0) {
            mStartPos = 0ll;
        }
        
        if (mEndPos>mFFmpegReader->getDuration()) {
            mEndPos = mFFmpegReader->getDuration();
        }
        
        if (mStartPos>mEndPos) {
            mStartPos = mEndPos;
        }
        
        mDuration = mEndPos - mStartPos;
    }else{
        mStartPos = 0ll;
        mDuration = mFFmpegReader->getDuration();
        mEndPos = mDuration;
    }
    
    notifyWithMediaDetailInfo(mDuration, videoStreamContext->codec->width, videoStreamContext->codec->height);
}

void MediaInfo::close_all_pipelines_l()
{
    //free
    if (mTargetVideoFrame.data!=NULL) {
        free(mTargetVideoFrame.data);
        mTargetVideoFrame.data = NULL;
    }
    
    if (mSourceVideoFrame.data!=NULL) {
        free(mSourceVideoFrame.data);
        mSourceVideoFrame.data = NULL;
    }
    
    if (mColorSpaceConvert!=NULL) {
        ColorSpaceConvert::DeleteColorSpaceConvert(mColorSpaceConvert, LIBYUV);
        mColorSpaceConvert = NULL;
    }
    
    if (mVideoDecoder!=NULL) {
        mVideoDecoder->dispose();
        VideoDecoder::DeleteVideoDecoder(mVideoDecoder, mVideoDecoderType);
        mVideoDecoder = NULL;
    }
    
    if (mFFmpegReader!=NULL) {
        mFFmpegReader->close();
        delete mFFmpegReader;
        mFFmpegReader = NULL;
    }
}

void MediaInfo::deleteProcessThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void MediaInfo::loadAsync(char* inputMediaFile)
{
    mThumbnailId = -1;
    
    mInputMediaFile = strdup(inputMediaFile);
    mStartPos = 0ll;
    mEndPos = 0ll;
    mIsSetPosition = false;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    createProcessThread();
}

void MediaInfo::loadAsync(char* inputMediaFile, int64_t startPos, int64_t endPos)
{
    mThumbnailId = -1;
    
    mInputMediaFile = strdup(inputMediaFile);
    mStartPos = startPos;
    mEndPos = endPos;
    mIsSetPosition = true;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    createProcessThread();
}

void MediaInfo::quit()
{
    deleteProcessThread();
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    if (mInputMediaFile!=NULL) {
        free(mInputMediaFile);
        mInputMediaFile = NULL;
    }
}

void MediaInfo::AVFrameToVideoFrame(AVFrame *inAVFrame, VideoFrame *outVideoFrame)
{
#ifdef IOS
    if (inAVFrame->format==AV_PIX_FMT_VIDEOTOOLBOX && inAVFrame->opaque!=NULL) {
        CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)inAVFrame->opaque;
        
        CVPixelBufferLockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
        uint8_t* plane = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
        int width = (int)CVPixelBufferGetWidth(pixelBuffer);
        int height = (int)CVPixelBufferGetHeight(pixelBuffer);
        int stride = (int)CVPixelBufferGetBytesPerRow(pixelBuffer);
        int frameSize = (int)CVPixelBufferGetDataSize(pixelBuffer);
        int videoRawType;
        int kCVPixelFormatType = CVPixelBufferGetPixelFormatType(pixelBuffer);//
        switch (kCVPixelFormatType) {
            case kCVPixelFormatType_32BGRA:
                videoRawType = VIDEOFRAME_RAWTYPE_BGRA; //BGRA
                break;
            case kCVPixelFormatType_420YpCbCr8BiPlanarFullRange: //NV12
                videoRawType = VIDEOFRAME_RAWTYPE_NV12;
                break;
            default:
                videoRawType = VIDEOFRAME_RAWTYPE_BGRA; //BGRA
                break;
        }
        
        outVideoFrame->width = width;
        outVideoFrame->height = height;
        outVideoFrame->frameSize = outVideoFrame->width * outVideoFrame->height * 4;
        outVideoFrame->videoRawType = videoRawType;
        
        if (stride!=width*4) {
            for (int i = 0; i<height; i++) {
                memcpy(outVideoFrame->data+width*4*i, plane+i*stride, width*4);
            }
        }else{
            memcpy(outVideoFrame->data, plane, frameSize);
        }
        
        CVPixelBufferUnlockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
    }else if(inAVFrame->format==AV_PIX_FMT_YUV420P) {
        VideoFrame* i420 = outVideoFrame;
        AVFrame *decodedFrame = inAVFrame;
        i420->width = decodedFrame->width;
        i420->height = decodedFrame->height;
        i420->frameSize = i420->width*i420->height*3/2;
        i420->videoRawType = VIDEOFRAME_RAWTYPE_I420;
        
        if (decodedFrame->linesize[0]!=decodedFrame->width) {
            //Y
            for (int i=0; i<decodedFrame->height; i++) {
                memcpy(i420->data+i*decodedFrame->width, decodedFrame->data[0]+i*decodedFrame->linesize[0], decodedFrame->width);
            }
            
            //U
            for (int i=0; i<decodedFrame->height/2; i++) {
                memcpy(i420->data+decodedFrame->width*decodedFrame->height+i*decodedFrame->width/2, decodedFrame->data[1]+i*decodedFrame->linesize[1], decodedFrame->width/2);
            }
            
            //V
            for (int i=0; i<decodedFrame->height/2; i++) {
                memcpy(i420->data+decodedFrame->width*decodedFrame->height+decodedFrame->width*decodedFrame->height/4+i*decodedFrame->width/2, decodedFrame->data[2]+i*decodedFrame->linesize[2], decodedFrame->width/2);
            }
        }else{
            memcpy(i420->data, decodedFrame->data[0], decodedFrame->linesize[0]*decodedFrame->height);
            memcpy(i420->data+decodedFrame->linesize[0]*decodedFrame->height, decodedFrame->data[1], decodedFrame->linesize[1]*decodedFrame->height/2);
            memcpy(i420->data+decodedFrame->linesize[0]*decodedFrame->height+decodedFrame->linesize[1]*decodedFrame->height/2, decodedFrame->data[2], decodedFrame->linesize[2]*decodedFrame->height/2);
        }
    }
#endif
    
#ifdef ANDROID
    if(inAVFrame->format==AV_PIX_FMT_YUV420P) {
        VideoFrame* i420 = outVideoFrame;
        AVFrame *decodedFrame = inAVFrame;
        i420->width = decodedFrame->width;
        i420->height = decodedFrame->height;
        i420->frameSize = i420->width*i420->height*3/2;
        i420->videoRawType = VIDEOFRAME_RAWTYPE_I420;
        
        if (decodedFrame->linesize[0]!=decodedFrame->width) {
            //Y
            for (int i=0; i<decodedFrame->height; i++) {
                memcpy(i420->data+i*decodedFrame->width, decodedFrame->data[0]+i*decodedFrame->linesize[0], decodedFrame->width);
            }
            
            //U
            for (int i=0; i<decodedFrame->height/2; i++) {
                memcpy(i420->data+decodedFrame->width*decodedFrame->height+i*decodedFrame->width/2, decodedFrame->data[1]+i*decodedFrame->linesize[1], decodedFrame->width/2);
            }
            
            //V
            for (int i=0; i<decodedFrame->height/2; i++) {
                memcpy(i420->data+decodedFrame->width*decodedFrame->height+decodedFrame->width*decodedFrame->height/4+i*decodedFrame->width/2, decodedFrame->data[2]+i*decodedFrame->linesize[2], decodedFrame->width/2);
            }
        }else{
            memcpy(i420->data, decodedFrame->data[0], decodedFrame->linesize[0]*decodedFrame->height);
            memcpy(i420->data+decodedFrame->linesize[0]*decodedFrame->height, decodedFrame->data[1], decodedFrame->linesize[1]*decodedFrame->height/2);
            memcpy(i420->data+decodedFrame->linesize[0]*decodedFrame->height+decodedFrame->linesize[1]*decodedFrame->height/2, decodedFrame->data[2], decodedFrame->linesize[2]*decodedFrame->height/2);
        }
    }
#endif
}
