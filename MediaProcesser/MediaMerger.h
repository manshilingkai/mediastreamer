//
//  MediaMerger.h
//  MediaStreamer
//
//  Created by Think on 2016/11/17.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaMerger_h
#define MediaMerger_h

#include <stdio.h>

#include "MediaMaterial.h"
#include "MediaEffect.h"
#include "MediaMergeAlgorithm.h"
#include "MediaProduct.h"

#ifdef ANDROID
#include "jni.h"
#endif

class MediaMerger{
    
public:
    virtual ~MediaMerger() {}
    
#ifdef ANDROID
    static MediaMerger* CreateMediaMerger(JavaVM *jvm, MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup, MEDIA_MERGE_ALGORITHM algorithm, MediaProduct* outputProduct);
#else
    static MediaMerger* CreateMediaMerger(MediaMaterialGroup *inputMediaMaterialGroup, MediaEffectGroup *inputMediaEffectGroup, MEDIA_MERGE_ALGORITHM algorithm, MediaProduct* outputProduct);
#endif
    static void DeleteMediaMerger(MEDIA_MERGE_ALGORITHM algorithm, MediaMerger* mediaMerger);
    
#ifdef ANDROID
    virtual void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event) = 0;
#endif

#ifdef IOS
    virtual void setListener(void (*listener)(void*,int,int,int), void* arg) = 0;
#endif
    
    virtual void notify(int event, int ext1, int ext2) = 0;
    
    virtual void start() = 0;
    
    virtual void resume() = 0;
    virtual void pause() = 0;
    
    virtual void stop() = 0;
};

#endif /* MediaMerger_h */
