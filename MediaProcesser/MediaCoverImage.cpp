//
//  MediaCoverImage.cpp
//  MediaStreamer
//
//  Created by Think on 16/11/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaCoverImage.h"
#include "FFmpegReader.h"
#include "VideoDecoder.h"
#include "ColorSpaceConvert.h"
#include "MediaLog.h"

#include "ImageProcesserUtils.h"

VideoFrame* getVideoImageWithPosition(char* inputFile, int64_t seekPositionMS)
{
    FFmpegReader *ffmpegReader = new FFmpegReader(inputFile);
    if (ffmpegReader==NULL) return NULL;
    
    bool bret = ffmpegReader->open();
    if (!bret)
    {
        if (ffmpegReader!=NULL) {
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        return NULL;
    }
    
    AVStream* videoStreamContext = ffmpegReader->getVideoStreamContext();
    if (videoStreamContext==NULL)
    {
        if (ffmpegReader!=NULL) {
            ffmpegReader->close();
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        return NULL;
    }
    
    AVDictionaryEntry *m = NULL;
    int rotate = 0;
    while((m=av_dict_get(videoStreamContext->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        printf("%s : %s\n", m->key,m->value);
        
        if(strcmp(m->key, "rotate")) continue;
        else{
            rotate = atoi(m->value);
        }
    }
    
    FFSeekStatus ffSeekStatus = ffmpegReader->seek(seekPositionMS);
    if (!ffSeekStatus.seekRet)
    {
        if (ffmpegReader!=NULL) {
            ffmpegReader->close();
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        return NULL;
    }
    bool isFlush = true;
    
    VideoDecoder* videoDecoder = VideoDecoder::CreateVideoDecoder(VIDEO_DECODER_FFMPEG);
    bret = videoDecoder->open(videoStreamContext, true);
    
    if (!bret)
    {
        videoDecoder->dispose();
        VideoDecoder::DeleteVideoDecoder(videoDecoder, VIDEO_DECODER_FFMPEG);
        
        if (ffmpegReader!=NULL) {
            ffmpegReader->close();
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        
        return NULL;
    }
    
    FFPacket* ffPacket = NULL;
    while (true) {
        ffPacket = ffmpegReader->getMediaPacket();
        
        if (ffPacket->ret<0)
        {
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            
            videoDecoder->dispose();
            VideoDecoder::DeleteVideoDecoder(videoDecoder, VIDEO_DECODER_FFMPEG);
            
            if (ffmpegReader!=NULL) {
                ffmpegReader->close();
                delete ffmpegReader;
                ffmpegReader = NULL;
            }
            
            return NULL;
        }
        
        if (ffPacket->mediaType!=0) {
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            continue;
        }

        if (isFlush) {
            if (ffPacket->avPacket->flags & AV_PKT_FLAG_KEY) {
                isFlush = false;
            }else{
                av_packet_unref(ffPacket->avPacket);
                av_freep(&ffPacket->avPacket);
                continue;
            }
        }
        
        if (!isFlush) {
            int iret = videoDecoder->decode(ffPacket->avPacket);
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            
            if (iret<0) {
                videoDecoder->dispose();
                VideoDecoder::DeleteVideoDecoder(videoDecoder, VIDEO_DECODER_FFMPEG);
                
                if (ffmpegReader!=NULL) {
                    ffmpegReader->close();
                    delete ffmpegReader;
                    ffmpegReader = NULL;
                }
                
                return NULL;
            }
            
            if (iret==0) {
                continue;
            }

            AVFrame *decodedFrame = videoDecoder->getFrame();
            if (decodedFrame==NULL) {
                continue;
            }else{
                if (decodedFrame->pts-videoStreamContext->start_time * AV_TIME_BASE * av_q2d(videoStreamContext->time_base)>=seekPositionMS*1000) {
                    VideoFrame* i420 = new VideoFrame;
                    i420->width = decodedFrame->width;
                    i420->height = decodedFrame->height;
                    i420->frameSize = i420->width*i420->height*3/2;
                    i420->data = (uint8_t*)malloc(i420->frameSize);
                    
                    if (decodedFrame->linesize[0]!=decodedFrame->width) {
                        //Y
                        for (int i=0; i<decodedFrame->height; i++) {
                            memcpy(i420->data+i*decodedFrame->width, decodedFrame->data[0]+i*decodedFrame->linesize[0], decodedFrame->width);
                        }
                        
                        //U
                        for (int i=0; i<decodedFrame->height/2; i++) {
                            memcpy(i420->data+decodedFrame->width*decodedFrame->height+i*decodedFrame->width/2, decodedFrame->data[1]+i*decodedFrame->linesize[1], decodedFrame->width/2);
                        }
                        
                        //V
                        for (int i=0; i<decodedFrame->height/2; i++) {
                            memcpy(i420->data+decodedFrame->width*decodedFrame->height+decodedFrame->width*decodedFrame->height/4+i*decodedFrame->width/2, decodedFrame->data[2]+i*decodedFrame->linesize[2], decodedFrame->width/2);
                        }
                    }else{
                        memcpy(i420->data, decodedFrame->data[0], decodedFrame->linesize[0]*decodedFrame->height);
                        memcpy(i420->data+decodedFrame->linesize[0]*decodedFrame->height, decodedFrame->data[1], decodedFrame->linesize[1]*decodedFrame->height/2);
                        memcpy(i420->data+decodedFrame->linesize[0]*decodedFrame->height+decodedFrame->linesize[1]*decodedFrame->height/2, decodedFrame->data[2], decodedFrame->linesize[2]*decodedFrame->height/2);
                    }
                    
                    //free decoder and mediafile reader
                    videoDecoder->clearFrame();
                    
                    videoDecoder->dispose();
                    VideoDecoder::DeleteVideoDecoder(videoDecoder, VIDEO_DECODER_FFMPEG);
                    
                    if (ffmpegReader!=NULL) {
                        ffmpegReader->close();
                        delete ffmpegReader;
                        ffmpegReader = NULL;
                    }
                    
                    i420->rotation = rotate;
                    
                    ColorSpaceConvert *colorSpaceConvert = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
                    
                    VideoFrame* outputImage = new VideoFrame();
                    if (rotate==90 || rotate==270 || rotate==-90 || rotate == -270) {
                        outputImage->width = i420->height;
                        outputImage->height = i420->width;
                    }else {
                        outputImage->width = i420->width;
                        outputImage->height = i420->height;
                    }
                    outputImage->frameSize = outputImage->width*outputImage->height*4;
                    outputImage->data = (uint8_t*)malloc(outputImage->frameSize);
#ifdef IOS
//                    bool ret = colorSpaceConvert->I420toABGR_Crop_Rotation_Scale(i420, outputImage, rotate);
                    bool ret = colorSpaceConvert->I420toARGB_Crop_Rotation_Scale(i420, outputImage, rotate);
#endif
#ifdef ANDROID
                    bool ret = colorSpaceConvert->I420toABGR_Crop_Rotation_Scale(i420, outputImage, rotate);
#endif
                    
                    ColorSpaceConvert::DeleteColorSpaceConvert(colorSpaceConvert, LIBYUV);
                    
                    if (i420!=NULL) {
                        if (i420->data) {
                            free(i420->data);
                            i420->data = NULL;
                        }
                        delete i420;
                        i420 = NULL;
                    }
                    
                    if (!ret)
                    {
                        if (outputImage!=NULL) {
                            if (outputImage->data) {
                                free(outputImage->data);
                                outputImage->data = NULL;
                            }
                            delete outputImage;
                            outputImage = NULL;
                        }
                        
                        return NULL;
                    }
                    
                    return outputImage;
                }else{
                    videoDecoder->clearFrame();
                    continue;
                }
            }
        }
        
    }

}

int getCoverImageWithPosition(char* inputFile, int64_t seekPositionMS, VideoFrame* outputImage)
{
    FFmpegReader *ffmpegReader = new FFmpegReader(inputFile);
    if (ffmpegReader==NULL) return -1;
    
    bool bret = ffmpegReader->open();
    if (!bret)
    {
        if (ffmpegReader!=NULL) {
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        return -1;
    }
    
    AVStream* videoStreamContext = ffmpegReader->getVideoStreamContext();
    if (videoStreamContext==NULL)
    {
        if (ffmpegReader!=NULL) {
            ffmpegReader->close();
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        return -1;
    }
    
    AVDictionaryEntry *m = NULL;
    int rotate = 0;
    while((m=av_dict_get(videoStreamContext->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        printf("%s : %s\n", m->key,m->value);
        
        if(strcmp(m->key, "rotate")) continue;
        else{
            rotate = atoi(m->value);
        }
    }
    
    FFSeekStatus ffSeekStatus = ffmpegReader->seek(seekPositionMS);
    if (!ffSeekStatus.seekRet)
    {
        if (ffmpegReader!=NULL) {
            ffmpegReader->close();
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        return -1;
    }
    bool isFlush = true;
    
    VideoDecoder* videoDecoder = VideoDecoder::CreateVideoDecoder(VIDEO_DECODER_FFMPEG);
    bret = videoDecoder->open(videoStreamContext, true);
    
    if (!bret)
    {
        videoDecoder->dispose();
        VideoDecoder::DeleteVideoDecoder(videoDecoder, VIDEO_DECODER_FFMPEG);
        
        if (ffmpegReader!=NULL) {
            ffmpegReader->close();
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        
        return -1;
    }
    
    FFPacket* ffPacket = NULL;
    while (true) {
        ffPacket = ffmpegReader->getMediaPacket();
        
        if (ffPacket->ret<0)
        {
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            
            videoDecoder->dispose();
            VideoDecoder::DeleteVideoDecoder(videoDecoder, VIDEO_DECODER_FFMPEG);
            
            if (ffmpegReader!=NULL) {
                ffmpegReader->close();
                delete ffmpegReader;
                ffmpegReader = NULL;
            }
            
            return -1;
        }
        
        if (ffPacket->mediaType!=0) {
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            continue;
        }

        if (isFlush) {
            if (ffPacket->avPacket->flags & AV_PKT_FLAG_KEY) {
                isFlush = false;
            }else{
                av_packet_unref(ffPacket->avPacket);
                av_freep(&ffPacket->avPacket);
                continue;
            }
        }
        
        if (!isFlush) {
            int iret = videoDecoder->decode(ffPacket->avPacket);
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            
            if (iret<0) {
                videoDecoder->dispose();
                VideoDecoder::DeleteVideoDecoder(videoDecoder, VIDEO_DECODER_FFMPEG);
                
                if (ffmpegReader!=NULL) {
                    ffmpegReader->close();
                    delete ffmpegReader;
                    ffmpegReader = NULL;
                }
                
                return -1;
            }
            
            if (iret==0) {
                continue;
            }

            AVFrame *decodedFrame = videoDecoder->getFrame();
            if (decodedFrame==NULL) {
                continue;
            }else{
                if (decodedFrame->pts-videoStreamContext->start_time * AV_TIME_BASE * av_q2d(videoStreamContext->time_base)>=seekPositionMS*1000) {
                    VideoFrame* i420 = new VideoFrame;
                    i420->width = decodedFrame->width;
                    i420->height = decodedFrame->height;
                    i420->frameSize = i420->width*i420->height*3/2;
                    i420->data = (uint8_t*)malloc(i420->frameSize);
                    
                    if (decodedFrame->linesize[0]!=decodedFrame->width) {
                        //Y
                        for (int i=0; i<decodedFrame->height; i++) {
                            memcpy(i420->data+i*decodedFrame->width, decodedFrame->data[0]+i*decodedFrame->linesize[0], decodedFrame->width);
                        }
                        
                        //U
                        for (int i=0; i<decodedFrame->height/2; i++) {
                            memcpy(i420->data+decodedFrame->width*decodedFrame->height+i*decodedFrame->width/2, decodedFrame->data[1]+i*decodedFrame->linesize[1], decodedFrame->width/2);
                        }
                        
                        //V
                        for (int i=0; i<decodedFrame->height/2; i++) {
                            memcpy(i420->data+decodedFrame->width*decodedFrame->height+decodedFrame->width*decodedFrame->height/4+i*decodedFrame->width/2, decodedFrame->data[2]+i*decodedFrame->linesize[2], decodedFrame->width/2);
                        }
                    }else{
                        memcpy(i420->data, decodedFrame->data[0], decodedFrame->linesize[0]*decodedFrame->height);
                        memcpy(i420->data+decodedFrame->linesize[0]*decodedFrame->height, decodedFrame->data[1], decodedFrame->linesize[1]*decodedFrame->height/2);
                        memcpy(i420->data+decodedFrame->linesize[0]*decodedFrame->height+decodedFrame->linesize[1]*decodedFrame->height/2, decodedFrame->data[2], decodedFrame->linesize[2]*decodedFrame->height/2);
                    }
                    
                    //free decoder and mediafile reader
                    videoDecoder->clearFrame();
                    
                    videoDecoder->dispose();
                    VideoDecoder::DeleteVideoDecoder(videoDecoder, VIDEO_DECODER_FFMPEG);
                    
                    if (ffmpegReader!=NULL) {
                        ffmpegReader->close();
                        delete ffmpegReader;
                        ffmpegReader = NULL;
                    }
                    
                    i420->rotation = rotate;
                    
                    ColorSpaceConvert *colorSpaceConvert = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
#ifdef IOS
//                    bool ret = colorSpaceConvert->I420toABGR_Crop_Rotation_Scale(i420, outputImage, rotate);
                    bool ret = colorSpaceConvert->I420toARGB_Crop_Rotation_Scale(i420, outputImage, rotate);
#endif
#ifdef ANDROID
                    bool ret = colorSpaceConvert->I420toABGR_Crop_Rotation_Scale(i420, outputImage, rotate);
#endif
                    
                    ColorSpaceConvert::DeleteColorSpaceConvert(colorSpaceConvert, LIBYUV);
                    
                    if (i420!=NULL) {
                        if (i420->data) {
                            free(i420->data);
                            i420->data = NULL;
                        }
                        delete i420;
                        i420 = NULL;
                    }
                    
                    if (!ret) return -1;
                    
                    return 0;
                }else{
                    videoDecoder->clearFrame();
                    continue;
                }
            }
        }
        
    }

}

int getCoverImage(char* inputFile, VideoFrame* outputImage)
{
    FFmpegReader *ffmpegReader = new FFmpegReader(inputFile);
    if (ffmpegReader==NULL) return -1;
    
    bool bret = ffmpegReader->open();
    if (!bret)
    {
        if (ffmpegReader!=NULL) {
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        return -1;
    }
    
    AVStream* videoStreamContext = ffmpegReader->getVideoStreamContext();
    if (videoStreamContext==NULL)
    {
        if (ffmpegReader!=NULL) {
            ffmpegReader->close();
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        return -1;
    }
    
    AVDictionaryEntry *m = NULL;
    int rotate = 0;
    while((m=av_dict_get(videoStreamContext->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        printf("%s : %s\n", m->key,m->value);
        
        if(strcmp(m->key, "rotate")) continue;
        else{
            rotate = atoi(m->value);
        }
    }
    
    FFPacket* ffPacket = NULL;
    while (true) {
        ffPacket = ffmpegReader->getMediaPacket();
        
        if (ffPacket->ret<0)
        {
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            
            if (ffmpegReader!=NULL) {
                ffmpegReader->close();
                delete ffmpegReader;
                ffmpegReader = NULL;
            }
            
            return -1;
        }
        
        if (ffPacket->mediaType!=0) {
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            continue;
        }
        
        if (!(ffPacket->avPacket->flags & AV_PKT_FLAG_KEY))
        {
            av_packet_unref(ffPacket->avPacket);
            av_freep(&ffPacket->avPacket);
            continue;
        }else{
            break;
        }
    }
    
    VideoDecoder* videoDecoder = VideoDecoder::CreateVideoDecoder(VIDEO_DECODER_FFMPEG);
    bret = videoDecoder->open(videoStreamContext, true);
    
    if (!bret)
    {
        videoDecoder->dispose();
        VideoDecoder::DeleteVideoDecoder(videoDecoder, VIDEO_DECODER_FFMPEG);
        
        av_packet_unref(ffPacket->avPacket);
        av_freep(&ffPacket->avPacket);
        
        if (ffmpegReader!=NULL) {
            ffmpegReader->close();
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        
        return -1;
    }
    
    int iret = videoDecoder->decode(ffPacket->avPacket);
    av_packet_unref(ffPacket->avPacket);
    av_freep(&ffPacket->avPacket);
    
    if (iret<=0) {
        videoDecoder->dispose();
        VideoDecoder::DeleteVideoDecoder(videoDecoder, VIDEO_DECODER_FFMPEG);
        
        if (ffmpegReader!=NULL) {
            ffmpegReader->close();
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        
        return -1;
    }
    
    AVFrame *decodedFrame = videoDecoder->getFrame();
    if (decodedFrame==NULL) {
        videoDecoder->dispose();
        VideoDecoder::DeleteVideoDecoder(videoDecoder, VIDEO_DECODER_FFMPEG);
        
        if (ffmpegReader!=NULL) {
            ffmpegReader->close();
            delete ffmpegReader;
            ffmpegReader = NULL;
        }
        
        return -1;
    }
    
    VideoFrame* i420 = new VideoFrame;
    i420->width = decodedFrame->width;
    i420->height = decodedFrame->height;
    i420->frameSize = i420->width*i420->height*3/2;
    i420->data = (uint8_t*)malloc(i420->frameSize);
    
    if (decodedFrame->linesize[0]!=decodedFrame->width) {
        //Y
        for (int i=0; i<decodedFrame->height; i++) {
            memcpy(i420->data+i*decodedFrame->width, decodedFrame->data[0]+i*decodedFrame->linesize[0], decodedFrame->width);
        }
        
        //U
        for (int i=0; i<decodedFrame->height/2; i++) {
            memcpy(i420->data+decodedFrame->width*decodedFrame->height+i*decodedFrame->width/2, decodedFrame->data[1]+i*decodedFrame->linesize[1], decodedFrame->width/2);
        }
        
        //V
        for (int i=0; i<decodedFrame->height/2; i++) {
            memcpy(i420->data+decodedFrame->width*decodedFrame->height+decodedFrame->width*decodedFrame->height/4+i*decodedFrame->width/2, decodedFrame->data[2]+i*decodedFrame->linesize[2], decodedFrame->width/2);
        }
    }else{
        memcpy(i420->data, decodedFrame->data[0], decodedFrame->linesize[0]*decodedFrame->height);
        memcpy(i420->data+decodedFrame->linesize[0]*decodedFrame->height, decodedFrame->data[1], decodedFrame->linesize[1]*decodedFrame->height/2);
        memcpy(i420->data+decodedFrame->linesize[0]*decodedFrame->height+decodedFrame->linesize[1]*decodedFrame->height/2, decodedFrame->data[2], decodedFrame->linesize[2]*decodedFrame->height/2);
    }
    
    //free decoder and mediafile reader
    videoDecoder->clearFrame();
    
    videoDecoder->dispose();
    VideoDecoder::DeleteVideoDecoder(videoDecoder, VIDEO_DECODER_FFMPEG);
    
    if (ffmpegReader!=NULL) {
        ffmpegReader->close();
        delete ffmpegReader;
        ffmpegReader = NULL;
    }
    
    i420->rotation = rotate;
    
    ColorSpaceConvert *colorSpaceConvert = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
#ifdef IOS
    bool ret = colorSpaceConvert->I420toABGR_Crop_Rotation_Scale(i420, outputImage, rotate);
#endif
#ifdef ANDROID
    bool ret = colorSpaceConvert->I420toABGR_Crop_Rotation_Scale(i420, outputImage, rotate);
#endif
    
    ColorSpaceConvert::DeleteColorSpaceConvert(colorSpaceConvert, LIBYUV);
    
    if (i420!=NULL) {
        if (i420->data) {
            free(i420->data);
            i420->data = NULL;
        }
        delete i420;
        i420 = NULL;
    }
    
    if (!ret) return -1;
    
    return 0;
}

int getCoverImageToImageFileWithPosition(char* inputMediaFile, int64_t seekPositionMS, int outputWidth, int outputHeight, char* outputImageFile)
{
    VideoFrame* outputImage = new VideoFrame();
    outputImage->width = outputWidth;
    outputImage->height = outputHeight;
    outputImage->frameSize = outputImage->width*outputImage->height*4;
    outputImage->data = (uint8_t*)malloc(outputImage->frameSize);
    
    int iret = getCoverImageWithPosition(inputMediaFile, seekPositionMS, outputImage);
    
    if (iret) {
        if (outputImage!=NULL) {
            if (outputImage->data) {
                free(outputImage->data);
                outputImage->data = NULL;
            }
            delete outputImage;
            outputImage = NULL;
        }
        
        return -1;
    }else{
        iret = RGBAToPNGFile((char*)outputImage->data, outputImage->width, outputImage->height, outputImageFile);
        
        if (outputImage!=NULL) {
            if (outputImage->data) {
                free(outputImage->data);
                outputImage->data = NULL;
            }
            delete outputImage;
            outputImage = NULL;
        }
        
        return iret;
        
    }
}


int getCoverImageToImageFile(char* inputMediaFile, int outputWidth, int outputHeight, char* outputImageFile)
{
    VideoFrame* outputImage = new VideoFrame();
    outputImage->width = outputWidth;
    outputImage->height = outputHeight;
    outputImage->frameSize = outputImage->width*outputImage->height*4;
    outputImage->data = (uint8_t*)malloc(outputImage->frameSize);
    
    int iret = getCoverImage(inputMediaFile, outputImage);
    
    if (iret) {
        if (outputImage!=NULL) {
            if (outputImage->data) {
                free(outputImage->data);
                outputImage->data = NULL;
            }
            delete outputImage;
            outputImage = NULL;
        }
        
        return -1;
    }else{
        iret = RGBAToPNGFile((char*)outputImage->data, outputImage->width, outputImage->height, outputImageFile);
        
        if (outputImage!=NULL) {
            if (outputImage->data) {
                free(outputImage->data);
                outputImage->data = NULL;
            }
            delete outputImage;
            outputImage = NULL;
        }
        
        return iret;
        
    }
}
