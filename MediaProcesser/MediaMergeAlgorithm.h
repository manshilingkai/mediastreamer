//
//  MediaMergeAlgorithm.h
//  MediaStreamer
//
//  Created by Think on 2016/12/5.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaMergeAlgorithm_h
#define MediaMergeAlgorithm_h

enum MEDIA_MERGE_ALGORITHM{
    MEDIA_MERGE_ALGORITHM_TIMELINE = 0,
    MEDIA_MERGE_ALGORITHM_REMUX = 1,
    MEDIA_MERGE_ALGORITHM_DEEP_REMUX = 2,
};

#endif /* MediaMergeAlgorithm_h */
