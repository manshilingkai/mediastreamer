//
//  ImageProcesserUtils.h
//  MediaStreamer
//
//  Created by Think on 2016/11/23.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef ImageProcesserUtils_h
#define ImageProcesserUtils_h

#include <stdio.h>
#include "MediaDataType.h"

#ifdef __cplusplus
extern "C" {
#endif

VideoFrame* PNGImageFileToRGBAVideoFrame(char* inImageFilePath);

int RGBAToPNGFile(char* data, int width, int height, char* path);

#ifdef __cplusplus
};
#endif

#endif /* ImageProcesserUtils_h */
