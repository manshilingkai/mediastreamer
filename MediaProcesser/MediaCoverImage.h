//
//  MediaCoverImage.h
//  MediaStreamer
//
//  Created by Think on 16/11/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaCoverImage_h
#define MediaCoverImage_h

#include <stdio.h>
#include <stdlib.h>

#include "MediaDataType.h"

#ifdef __cplusplus
extern "C" {
#endif

    VideoFrame* getVideoImageWithPosition(char* inputFile, int64_t seekPositionMS);

    int getCoverImage(char* inputFile, VideoFrame* outputImage);

    int getCoverImageWithPosition(char* inputFile, int64_t seekPositionMS, VideoFrame* outputImage);
    
    int getCoverImageToImageFile(char* inputMediaFile, int outputWidth, int outputHeight, char* outputImageFile);
    
    int getCoverImageToImageFileWithPosition(char* inputMediaFile, int64_t seekPositionMS, int outputWidth, int outputHeight, char* outputImageFile);

#ifdef __cplusplus
};
#endif

#endif /* MediaCoverImage_h */
