//
//  MediaSpliter.h
//  MediaStreamer
//
//  Created by Think on 16/11/7.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaSpliter_h
#define MediaSpliter_h

#include <stdio.h>

#include "MediaMaterial.h"
#include "MediaSplitAlgorithm.h"
#include "MediaProduct.h"

#ifdef ANDROID
#include "jni.h"
#endif

class MediaSpliter{
    
public:
    virtual ~MediaSpliter() {}
    
#ifdef ANDROID
    static MediaSpliter* CreateMediaSpliter(JavaVM *jvm, MediaMaterial *inputMediaMaterial, MEDIA_SPLIT_ALGORITHM algorithm, MediaProductGroup* outputProductGroup);
#else
    static MediaSpliter* CreateMediaSpliter(MediaMaterial *inputMediaMaterial, MEDIA_SPLIT_ALGORITHM algorithm, MediaProductGroup* outputProductGroup);
#endif
    static void DeleteMediaSpliter(MEDIA_SPLIT_ALGORITHM algorithm, MediaSpliter* mediaSpliter);
    
#ifdef ANDROID
    virtual void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event) = 0;
#endif
    
#ifdef IOS
    virtual void setListener(void (*listener)(void*,int,int,int), void* arg) = 0;
#endif
    
    virtual void notify(int event, int ext1, int ext2) = 0;
    
    virtual void start() = 0;
    
    virtual void resume() = 0;
    virtual void pause() = 0;
    
    virtual void stop() = 0;
};



#endif /* MediaSpliter_h */
