//
//  qt-faststart.h
//  MediaStreamer
//
//  Created by Think on 16/11/11.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef qt_faststart_h
#define qt_faststart_h

#ifdef __cplusplus
extern "C" {
#endif
    
    int qt_faststart_main(int argc, char *argv[]);
    
#ifdef __cplusplus
};
#endif

#endif /* qt_faststart_h */
