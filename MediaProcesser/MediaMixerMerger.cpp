//
//  MediaMixerMerger.cpp
//  MediaStreamer
//
//  Created by Think on 2016/12/1.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaMixerMerger.h"

#include "AutoLock.h"
#include "MediaLog.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#endif

#ifdef IOS
#include "iOSMediaListener.h"
#include <QuartzCore/QuartzCore.h>
#include <CoreVideo/CoreVideo.h>
#endif

extern "C" {
#include "libavformat/avformat.h"
}

#include "h264_nal.h"
#include "VideoEncoder.h"
#include "AudioMixer.h"

#include "ImageProcesserUtils.h"

struct MediaMixerMergerEvent : public TimedEventQueue::Event {
    MediaMixerMergerEvent(
                            MediaMixerMerger *merger,
                            void (MediaMixerMerger::*method)())
    : mMerger(merger),
    mMethod(method) {
    }
    
protected:
    virtual ~MediaMixerMergerEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mMerger->*mMethod)();
    }
    
private:
    MediaMixerMerger *mMerger;
    void (MediaMixerMerger::*mMethod)();
    
    MediaMixerMergerEvent(const MediaMixerMergerEvent &);
    MediaMixerMergerEvent &operator=(const MediaMixerMergerEvent &);
};

#ifdef ANDROID
MediaMixerMerger::MediaMixerMerger(JavaVM *jvm, MediaMaterialGroup *inputMediaMaterialGroup, MediaProduct* outputProduct)
{    
    mJvm = jvm;
    
    bool hasBackGroundSoundMaterial = false;
    bool hasOverlayMediaMaterialGroup = false;
    for (int i = 0; i < inputMediaMaterialGroup->mediaMaterialNum; i++) {
        if (inputMediaMaterialGroup->mediaMaterials[i]->type==MEDIA_MATERIAL_TYPE_VIDEO_AUDIO) {
            mMainMediaMaterial.iD = inputMediaMaterialGroup->mediaMaterials[i]->iD;
            mMainMediaMaterial.type = inputMediaMaterialGroup->mediaMaterials[i]->type;
            mMainMediaMaterial.url = strdup(inputMediaMaterialGroup->mediaMaterials[i]->url);
            mMainMediaMaterial.startPos = inputMediaMaterialGroup->mediaMaterials[i]->startPos;
            mMainMediaMaterial.endPos = inputMediaMaterialGroup->mediaMaterials[i]->endPos;
            mMainMediaMaterial.weight = inputMediaMaterialGroup->mediaMaterials[i]->weight;
            
            break;
        }
    }
    
    for (int i = 0; i < inputMediaMaterialGroup->mediaMaterialNum; i++) {
        if (inputMediaMaterialGroup->mediaMaterials[i]->type==MEDIA_MATERIAL_TYPE_AUDIO) {
            mBackGroundSoundMaterial.iD = inputMediaMaterialGroup->mediaMaterials[i]->iD;
            mBackGroundSoundMaterial.type = inputMediaMaterialGroup->mediaMaterials[i]->type;
            mBackGroundSoundMaterial.url = strdup(inputMediaMaterialGroup->mediaMaterials[i]->url);
            mBackGroundSoundMaterial.startPos = inputMediaMaterialGroup->mediaMaterials[i]->startPos;
            mBackGroundSoundMaterial.endPos = inputMediaMaterialGroup->mediaMaterials[i]->endPos;
            mBackGroundSoundMaterial.weight = inputMediaMaterialGroup->mediaMaterials[i]->weight;
            
            hasBackGroundSoundMaterial = true;
            
            break;
        }
    }
    
    for (int i = 0; i < inputMediaMaterialGroup->mediaMaterialNum; i++) {
        
        if (inputMediaMaterialGroup->mediaMaterials[i]->type==MEDIA_MATERIAL_TYPE_PNG) {
            mOverlayMediaMaterialGroup.mediaMaterialNum++;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1] = new MediaMaterial;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->iD = inputMediaMaterialGroup->mediaMaterials[i]->iD;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->type = inputMediaMaterialGroup->mediaMaterials[i]->type;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->url = strdup(inputMediaMaterialGroup->mediaMaterials[i]->url);
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->startPos = inputMediaMaterialGroup->mediaMaterials[i]->startPos;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->endPos = inputMediaMaterialGroup->mediaMaterials[i]->endPos;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->weight = inputMediaMaterialGroup->mediaMaterials[i]->weight;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->x = inputMediaMaterialGroup->mediaMaterials[i]->x;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->y = inputMediaMaterialGroup->mediaMaterials[i]->y;
            
            hasOverlayMediaMaterialGroup = true;
        }
    }
    
    if (hasBackGroundSoundMaterial && !hasOverlayMediaMaterialGroup) {
        media_mix_mode = ONLY_AUDIO_MIX;
    }else if(!hasBackGroundSoundMaterial && hasOverlayMediaMaterialGroup) {
        media_mix_mode = ONLY_VIDEO_MIX;
    }else if(hasBackGroundSoundMaterial && hasOverlayMediaMaterialGroup){
        media_mix_mode = VIDEO_AUDIO_MIX;
    }else {
        media_mix_mode = NONE_MIX;
    }
    
    mOutputAudioOptions = outputProduct->audioOptions;
    mOutputVideoOptions = outputProduct->videoOptions;
    mOutputUrl = strdup(outputProduct->url);
    
    mAsyncPrepareEvent = new MediaMixerMergerEvent(this, &MediaMixerMerger::onPrepareAsyncEvent);
    mProcessOnePacketEvent = new MediaMixerMergerEvent(this, &MediaMixerMerger::onProcessOnePacketEvent);
    mStopEvent = new MediaMixerMergerEvent(this, &MediaMixerMerger::onStopEvent);
    mNotifyEvent = new MediaMixerMergerEvent(this, &MediaMixerMerger::onNotifyEvent);
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mStopCondition, NULL);
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();
    
    mFlags = 0;
    
    mFFmpegReaderForMainMediaMaterial = NULL;
    mAudioDecoderForMainMediaMaterial = NULL;
    mAudioFilterForMainMediaMaterial = NULL;
    mAudioPCMDataPoolForMainMediaMaterial = NULL;
    mVideoDecoderForMainMediaMaterial = NULL;
    mVideoFilterForMainMediaMaterial = NULL;
    
    mFFmpegReaderForBackGroundSoundMaterial = NULL;
    mAudioDecoderForBackGroundSoundMaterial = NULL;
    mAudioFilterForBackGroundSoundMaterial = NULL;
    mAudioPCMDataPoolForBackGroundSoundMaterial = NULL;
    
    mAudioEncoder = NULL;
    mAudioEncoderType = FAAC;
    mMediaMuxer = NULL;
    mVideoEncoder = NULL;
    
    mCurrentVideoPtsUs = 0ll;
    mCurrentAudioPtsUs = 0ll;
    mGotFirstVideoFrame = false;
    mFirstVideoFramePtsUs = 0ll;
    mCurrentVideoPositionUs = 0ll;
    
    mAudioStreamContextForMainMediaMaterial = NULL;
    mVideoStreamContextForMainMediaMaterial = NULL;
    mH264Converter = NULL;
    
    mColorSpaceConvert = NULL;
    
    mOverlayVideoFrame.data = (uint8_t*)malloc(MAX_VIDEO_FRAME_SIZE);
    mOverlayVideoFrame.frameSize = MAX_VIDEO_FRAME_SIZE;
    mOverlayVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_BGRA;
}
#else
MediaMixerMerger::MediaMixerMerger(MediaMaterialGroup *inputMediaMaterialGroup, MediaProduct* outputProduct)
{
    bool hasBackGroundSoundMaterial = false;
    bool hasOverlayMediaMaterialGroup = false;
    for (int i = 0; i < inputMediaMaterialGroup->mediaMaterialNum; i++) {
        if (inputMediaMaterialGroup->mediaMaterials[i]->type==MEDIA_MATERIAL_TYPE_VIDEO_AUDIO) {
            mMainMediaMaterial.iD = inputMediaMaterialGroup->mediaMaterials[i]->iD;
            mMainMediaMaterial.type = inputMediaMaterialGroup->mediaMaterials[i]->type;
            mMainMediaMaterial.url = strdup(inputMediaMaterialGroup->mediaMaterials[i]->url);
            mMainMediaMaterial.startPos = inputMediaMaterialGroup->mediaMaterials[i]->startPos;
            mMainMediaMaterial.endPos = inputMediaMaterialGroup->mediaMaterials[i]->endPos;
            mMainMediaMaterial.weight = inputMediaMaterialGroup->mediaMaterials[i]->weight;
            
            break;
        }
    }
    
    for (int i = 0; i < inputMediaMaterialGroup->mediaMaterialNum; i++) {
        if (inputMediaMaterialGroup->mediaMaterials[i]->type==MEDIA_MATERIAL_TYPE_AUDIO) {
            mBackGroundSoundMaterial.iD = inputMediaMaterialGroup->mediaMaterials[i]->iD;
            mBackGroundSoundMaterial.type = inputMediaMaterialGroup->mediaMaterials[i]->type;
            mBackGroundSoundMaterial.url = strdup(inputMediaMaterialGroup->mediaMaterials[i]->url);
            mBackGroundSoundMaterial.startPos = inputMediaMaterialGroup->mediaMaterials[i]->startPos;
            mBackGroundSoundMaterial.endPos = inputMediaMaterialGroup->mediaMaterials[i]->endPos;
            mBackGroundSoundMaterial.weight = inputMediaMaterialGroup->mediaMaterials[i]->weight;
            
            hasBackGroundSoundMaterial = true;
            
            break;
        }
    }
    
    for (int i = 0; i < inputMediaMaterialGroup->mediaMaterialNum; i++) {

        if (inputMediaMaterialGroup->mediaMaterials[i]->type==MEDIA_MATERIAL_TYPE_PNG) {
            mOverlayMediaMaterialGroup.mediaMaterialNum++;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1] = new MediaMaterial;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->iD = inputMediaMaterialGroup->mediaMaterials[i]->iD;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->type = inputMediaMaterialGroup->mediaMaterials[i]->type;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->url = strdup(inputMediaMaterialGroup->mediaMaterials[i]->url);
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->startPos = inputMediaMaterialGroup->mediaMaterials[i]->startPos;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->endPos = inputMediaMaterialGroup->mediaMaterials[i]->endPos;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->weight = inputMediaMaterialGroup->mediaMaterials[i]->weight;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->x = inputMediaMaterialGroup->mediaMaterials[i]->x;
            mOverlayMediaMaterialGroup.mediaMaterials[mOverlayMediaMaterialGroup.mediaMaterialNum-1]->y = inputMediaMaterialGroup->mediaMaterials[i]->y;
            
            hasOverlayMediaMaterialGroup = true;
        }
    }
    
    
    if (hasBackGroundSoundMaterial && !hasOverlayMediaMaterialGroup) {
        media_mix_mode = ONLY_AUDIO_MIX;
    }else if(!hasBackGroundSoundMaterial && hasOverlayMediaMaterialGroup) {
        media_mix_mode = ONLY_VIDEO_MIX;
    }else if(hasBackGroundSoundMaterial && hasOverlayMediaMaterialGroup){
        media_mix_mode = VIDEO_AUDIO_MIX;
    }else {
        media_mix_mode = NONE_MIX;
    }
    
    mOutputAudioOptions = outputProduct->audioOptions;
    mOutputVideoOptions = outputProduct->videoOptions;
    mOutputUrl = strdup(outputProduct->url);
    
    mAsyncPrepareEvent = new MediaMixerMergerEvent(this, &MediaMixerMerger::onPrepareAsyncEvent);
    mProcessOnePacketEvent = new MediaMixerMergerEvent(this, &MediaMixerMerger::onProcessOnePacketEvent);
    mStopEvent = new MediaMixerMergerEvent(this, &MediaMixerMerger::onStopEvent);
    mNotifyEvent = new MediaMixerMergerEvent(this, &MediaMixerMerger::onNotifyEvent);
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mStopCondition, NULL);
    
    mQueue.start();
    
    mFlags = 0;
    
    mFFmpegReaderForMainMediaMaterial = NULL;
    mAudioDecoderForMainMediaMaterial = NULL;
    mAudioFilterForMainMediaMaterial = NULL;
    mAudioPCMDataPoolForMainMediaMaterial = NULL;
    mVideoDecoderForMainMediaMaterial = NULL;
    mVideoFilterForMainMediaMaterial = NULL;
    
    mFFmpegReaderForBackGroundSoundMaterial = NULL;
    mAudioDecoderForBackGroundSoundMaterial = NULL;
    mAudioFilterForBackGroundSoundMaterial = NULL;
    mAudioPCMDataPoolForBackGroundSoundMaterial = NULL;
    
    mAudioEncoder = NULL;
    mAudioEncoderType = AUDIO_TOOL_BOX;
    mMediaMuxer = NULL;
    mVideoEncoder = NULL;
    
    mCurrentVideoPtsUs = 0ll;
    mCurrentAudioPtsUs = 0ll;
    mGotFirstVideoFrame = false;
    mFirstVideoFramePtsUs = 0ll;
    mCurrentVideoPositionUs = 0ll;
    
    mAudioStreamContextForMainMediaMaterial = NULL;
    mVideoStreamContextForMainMediaMaterial = NULL;
    mH264Converter = NULL;
    
    mColorSpaceConvert = NULL;
    
    mOverlayVideoFrame.data = (uint8_t*)malloc(MAX_VIDEO_FRAME_SIZE);
    mOverlayVideoFrame.frameSize = MAX_VIDEO_FRAME_SIZE;
    mOverlayVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_BGRA;
}
#endif

MediaMixerMerger::~MediaMixerMerger()
{
    mQueue.stop(true);
    
    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
    
    if (mProcessOnePacketEvent!=NULL) {
        delete mProcessOnePacketEvent;
        mProcessOnePacketEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    if (mOverlayVideoFrame.data) {
        free(mOverlayVideoFrame.data);
        mOverlayVideoFrame.data = NULL;
    }
    
    if (mOutputUrl) {
        free(mOutputUrl);
        mOutputUrl = NULL;
    }
    
    if (mMainMediaMaterial.url) {
        free(mMainMediaMaterial.url);
        mMainMediaMaterial.url = NULL;
    }
    
    if (mBackGroundSoundMaterial.url) {
        free(mBackGroundSoundMaterial.url);
        mBackGroundSoundMaterial.url = NULL;
    }
    
    mOverlayMediaMaterialGroup.Free();
    
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mStopCondition);
}

#ifdef ANDROID
void MediaMixerMerger::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    modifyFlags(INITIALIZED, SET);
    
}
#endif

#ifdef IOS
void MediaMixerMerger::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new iOSMediaListener(listener,arg);
    
    modifyFlags(INITIALIZED, SET);
}
#endif

void MediaMixerMerger::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void MediaMixerMerger::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
    
    switch (event) {
            case MEDIA_PROCESSER_ERROR:
            notifyListener_l(event, ext1, ext2);
            stop_l();
            break;
            case MEDIA_PROCESSER_INFO:
            notifyListener_l(event, ext1, ext2);
            break;
            
        default:
            notifyListener_l(event, ext1, ext2);
            break;
    }
}

void MediaMixerMerger::start()
{
    LOGI("MediaMixerMerger::start");
    
    AutoLock autoLock(&mLock);
    
    if (mFlags & CONNECTING) {
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    return prepareAsync_l();
}

void MediaMixerMerger::prepareAsync_l()
{
    modifyFlags(END, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(CONNECTING, SET);
    
    mQueue.postEvent(mAsyncPrepareEvent);
}

void MediaMixerMerger::onPrepareAsyncEvent()
{
    AutoLock autoLock(&mLock);
    
    bool ret = open_all_pipelines_l();
    
    if (ret) {
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(CONNECTED, SET);
        
        play_l();
    }
}

bool MediaMixerMerger::open_all_pipelines_l()
{
    //open video preprocess
    
    bool ret = open_all_input_pipelines_l_for_MainMediaMaterial();
    if (!ret) return false;
    
    if (media_mix_mode==ONLY_AUDIO_MIX || media_mix_mode==VIDEO_AUDIO_MIX) {
        ret = open_all_input_pipelines_l_for_BackGroundSoundMaterial();
        if (!ret) return false;
    }
    
    if (media_mix_mode==ONLY_VIDEO_MIX || media_mix_mode==VIDEO_AUDIO_MIX) {
        mColorSpaceConvert = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
        ret = open_all_input_pipelines_l_for_OverlayMediaMaterialGroup();
        if (!ret) return false;
    }

    ret = open_all_ouput_pipelines_l();
    if (!ret) return false;
    
    
    return true;
}

void MediaMixerMerger::close_all_pipelines_l()
{
    close_all_input_pipelines_l_for_MainMediaMaterial();
    
    if (media_mix_mode==ONLY_AUDIO_MIX || media_mix_mode==VIDEO_AUDIO_MIX) {
        close_all_input_pipelines_l_for_BackGroundSoundMaterial();
    }
    if (media_mix_mode==ONLY_VIDEO_MIX || media_mix_mode==VIDEO_AUDIO_MIX) {
        close_all_input_pipelines_l_for_OverlayMediaMaterialGroup();
        if (mColorSpaceConvert!=NULL) {
            ColorSpaceConvert::DeleteColorSpaceConvert(mColorSpaceConvert, LIBYUV);
            mColorSpaceConvert = NULL;
        }
    }
    close_all_output_pipelines_l();
}

bool MediaMixerMerger::open_all_input_pipelines_l_for_MainMediaMaterial()
{
    // open MainMediaMaterial File Reader
    mFFmpegReaderForMainMediaMaterial = new FFmpegReader(mMainMediaMaterial.url);
    bool ret = mFFmpegReaderForMainMediaMaterial->open();
    if (!ret) {
        notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
        stop_l();
        
        return false;
    }
    
    AVStream* audioStreamContext = mFFmpegReaderForMainMediaMaterial->getAudioStreamContext();
    // open MainMediaMaterial Audio Decoder
    if (audioStreamContext!=NULL) {
        mAudioDecoderForMainMediaMaterial = AudioDecoder::CreateAudioDecoder(AUDIO_DECODER_FFMPEG);
        if (!mAudioDecoderForMainMediaMaterial->open(audioStreamContext)) {
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_AUDIO_DECODER_FAIL, 0);
            stop_l();
            return false;
        }
    }
    
    // open MainMediaMaterial Audio Filter
    if (audioStreamContext!=NULL) {
        mAudioFilterForMainMediaMaterial = AudioFilter::CreateAudioFilter(AUDIO_FILTER_FFMPEG);
        
        bool ret = mAudioFilterForMainMediaMaterial->open("atempo=1.0", audioStreamContext->codec->channel_layout, audioStreamContext->codec->channels, audioStreamContext->codec->sample_rate, audioStreamContext->codec->sample_fmt, AV_CH_LAYOUT_MONO, mOutputAudioOptions.audioNumChannels, mOutputAudioOptions.audioSampleRate, AV_SAMPLE_FMT_S16);
        
        if (!ret) {
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_AUDIO_PREPROCESS_FAIL, 0);
            stop_l();
            return false;
        }
    }
    
    mAudioPCMDataPoolForMainMediaMaterial = new AudioPCMDataPool(mOutputAudioOptions.audioSampleRate, mOutputAudioOptions.audioNumChannels*2, 1000);
    
    AVStream* videoStreamContext = mFFmpegReaderForMainMediaMaterial->getVideoStreamContext();
    
    mRotationForMainMediaMaterial = 0;
    
    if (videoStreamContext!=NULL) {
        AVDictionaryEntry *m = NULL;
        while((m=av_dict_get(videoStreamContext->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
//            printf("%s : %s\n", m->key,m->value);
            
            if(strcmp(m->key, "rotate")) continue;
            else{
                mRotationForMainMediaMaterial = atoi(m->value);
                break;
            }
        }
    }
    
//    if (mRotationForMainMediaMaterial!=0 && mRotationForMainMediaMaterial!=360) {
//        if (media_mix_mode==ONLY_AUDIO_MIX) {
//            media_mix_mode = VIDEO_AUDIO_MIX;
//        }
//    }
    
    if (media_mix_mode==ONLY_VIDEO_MIX || media_mix_mode==VIDEO_AUDIO_MIX) {
        //open MainMediaMaterial Video Decoder
        if (videoStreamContext!=NULL) {
#ifdef ANDROID
            mVideoDecoderTypeForMainMediaMaterial = VIDEO_DECODER_FFMPEG;
            if(mVideoDecoderTypeForMainMediaMaterial==VIDEO_DECODER_MEDIACODEC_JAVA)
            {
                mVideoDecoderForMainMediaMaterial = VideoDecoder::CreateVideoDecoderWithJniEnv(mVideoDecoderTypeForMainMediaMaterial, mJvm, NULL);
            }else{
                mVideoDecoderForMainMediaMaterial = VideoDecoder::CreateVideoDecoder(mVideoDecoderTypeForMainMediaMaterial);
            }
#endif
            
#ifdef IOS
            mVideoDecoderTypeForMainMediaMaterial=VIDEO_DECODER_VIDEOTOOLBOX;
            mVideoDecoderForMainMediaMaterial = VideoDecoder::CreateVideoDecoder(mVideoDecoderTypeForMainMediaMaterial);
#endif
            
            if (!mVideoDecoderForMainMediaMaterial->open(videoStreamContext)) {
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_VIDEO_DECODER_FAIL, 0);
                stop_l();
                return false;
            }
        }

        //open MainMediaMaterial Video Filter
        if (videoStreamContext!=NULL)
        {
#ifdef ANDROID
            mVideoFilterSolutionForMainMediaMaterial = VIDEO_FILTER_FFMPEG;
            //        mVideoFilterForMainMediaMaterial = VideoFilter::CreateVideoFilter(mVideoFilterSolutionForMainMediaMaterial);
#endif
#ifdef IOS
            mVideoFilterSolutionForMainMediaMaterial = VIDEO_FILTER_IOS;
            mVideoFilterForMainMediaMaterial = VideoFilter::CreateVideoFilter(mVideoFilterSolutionForMainMediaMaterial);
#endif
            
            if (!mVideoFilterForMainMediaMaterial->open(videoStreamContext)) {
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_VIDEO_PREPROCESS_FAIL, 0);
                stop_l();
                return false;
            }
        }
 
    }

    
    mAudioStreamContextForMainMediaMaterial = audioStreamContext;
    mVideoStreamContextForMainMediaMaterial = videoStreamContext;
    
    return true;
}

void MediaMixerMerger::close_all_input_pipelines_l_for_MainMediaMaterial()
{
    if (mAudioFilterForMainMediaMaterial!=NULL) {
        mAudioFilterForMainMediaMaterial->dispose();
        AudioFilter::DeleteAudioFilter(AUDIO_FILTER_FFMPEG, mAudioFilterForMainMediaMaterial);
        mAudioFilterForMainMediaMaterial = NULL;
    }
    
    if (mAudioDecoderForMainMediaMaterial!=NULL) {
        mAudioDecoderForMainMediaMaterial->dispose();
        AudioDecoder::DeleteAudioDecoder(mAudioDecoderForMainMediaMaterial, AUDIO_DECODER_FFMPEG);
        mAudioDecoderForMainMediaMaterial = NULL;
    }
    
    if (mVideoFilterForMainMediaMaterial!=NULL) {
        mVideoFilterForMainMediaMaterial->dispose();
        VideoFilter::DeleteVideoFilter(mVideoFilterSolutionForMainMediaMaterial, mVideoFilterForMainMediaMaterial);
        mVideoFilterForMainMediaMaterial = NULL;
    }
    
    if (mVideoDecoderForMainMediaMaterial!=NULL) {
        mVideoDecoderForMainMediaMaterial->dispose();
        VideoDecoder::DeleteVideoDecoder(mVideoDecoderForMainMediaMaterial, mVideoDecoderTypeForMainMediaMaterial);
        mVideoDecoderForMainMediaMaterial = NULL;
    }
    
    if (mFFmpegReaderForMainMediaMaterial!=NULL) {
        mFFmpegReaderForMainMediaMaterial->close();
        delete mFFmpegReaderForMainMediaMaterial;
        mFFmpegReaderForMainMediaMaterial = NULL;
    }
    
    if (mAudioPCMDataPoolForMainMediaMaterial!=NULL) {
        delete mAudioPCMDataPoolForMainMediaMaterial;
        mAudioPCMDataPoolForMainMediaMaterial = NULL;
    }
}

bool MediaMixerMerger::open_all_input_pipelines_l_for_BackGroundSoundMaterial()
{
    mFFmpegReaderForBackGroundSoundMaterial = new FFmpegReader(mBackGroundSoundMaterial.url);
    bool ret = mFFmpegReaderForBackGroundSoundMaterial->open();
    if (!ret) {
        notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
        stop_l();
        
        return false;
    }
    
    AVStream* audioStreamContext = mFFmpegReaderForBackGroundSoundMaterial->getAudioStreamContext();
    if (audioStreamContext!=NULL) {
        mAudioDecoderForBackGroundSoundMaterial = AudioDecoder::CreateAudioDecoder(AUDIO_DECODER_FFMPEG);
        if (!mAudioDecoderForBackGroundSoundMaterial->open(audioStreamContext)) {
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_AUDIO_DECODER_FAIL, 0);
            stop_l();
            return false;
        }
    }
    
    if (audioStreamContext!=NULL) {
        mAudioFilterForBackGroundSoundMaterial = AudioFilter::CreateAudioFilter(AUDIO_FILTER_FFMPEG);
        
        bool ret = mAudioFilterForBackGroundSoundMaterial->open("atempo=1.0", audioStreamContext->codec->channel_layout, audioStreamContext->codec->channels, audioStreamContext->codec->sample_rate, audioStreamContext->codec->sample_fmt, AV_CH_LAYOUT_MONO, mOutputAudioOptions.audioNumChannels, mOutputAudioOptions.audioSampleRate, AV_SAMPLE_FMT_S16);
        
        if (!ret) {
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_AUDIO_PREPROCESS_FAIL, 0);
            stop_l();
            return false;
        }
    }
    
    mAudioPCMDataPoolForBackGroundSoundMaterial = new AudioPCMDataPool(mOutputAudioOptions.audioSampleRate, mOutputAudioOptions.audioNumChannels*2, 1000);
    
    return true;
}

void MediaMixerMerger::close_all_input_pipelines_l_for_BackGroundSoundMaterial()
{
    if (mAudioFilterForBackGroundSoundMaterial!=NULL) {
        mAudioFilterForBackGroundSoundMaterial->dispose();
        AudioFilter::DeleteAudioFilter(AUDIO_FILTER_FFMPEG, mAudioFilterForBackGroundSoundMaterial);
        mAudioFilterForBackGroundSoundMaterial = NULL;
    }
    
    if (mAudioDecoderForBackGroundSoundMaterial!=NULL) {
        mAudioDecoderForBackGroundSoundMaterial->dispose();
        AudioDecoder::DeleteAudioDecoder(mAudioDecoderForBackGroundSoundMaterial, AUDIO_DECODER_FFMPEG);
        mAudioDecoderForBackGroundSoundMaterial = NULL;
    }
    
    if (mFFmpegReaderForBackGroundSoundMaterial!=NULL) {
        mFFmpegReaderForBackGroundSoundMaterial->close();
        delete mFFmpegReaderForBackGroundSoundMaterial;
        mFFmpegReaderForBackGroundSoundMaterial = NULL;
    }
    
    if (mAudioPCMDataPoolForBackGroundSoundMaterial!=NULL) {
        delete mAudioPCMDataPoolForBackGroundSoundMaterial;
        mAudioPCMDataPoolForBackGroundSoundMaterial = NULL;
    }
}

bool MediaMixerMerger::open_all_input_pipelines_l_for_OverlayMediaMaterialGroup()
{
    for (int i = 0; i<mOverlayMediaMaterialGroup.mediaMaterialNum; i++) {
        if (mOverlayMediaMaterialGroup.mediaMaterials[i]->type==MEDIA_MATERIAL_TYPE_PNG) {
#ifdef IOS
            VideoFrame *pngVideoFrame = PNGImageFileToARGBVideoFrame(mOverlayMediaMaterialGroup.mediaMaterials[i]->url);
#endif
#ifdef ANDROID
            VideoFrame *pngVideoFrame = PNGImageFileToRGBAVideoFrame(mOverlayMediaMaterialGroup.mediaMaterials[i]->url);
#endif
            mOverlayMediaMaterialGroup.mediaMaterials[i]->opaque = pngVideoFrame;
            
            if (mOverlayMediaMaterialGroup.mediaMaterials[i]->opaque==NULL) {
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL);
                
                stop_l();
                
                return false;
            }
        }
    }
    
    return true;
}

void MediaMixerMerger::close_all_input_pipelines_l_for_OverlayMediaMaterialGroup()
{
    for (int i = 0; i<mOverlayMediaMaterialGroup.mediaMaterialNum; i++) {
        if (mOverlayMediaMaterialGroup.mediaMaterials[i]->opaque!=NULL)
        {
            VideoFrame* videoFrame = (VideoFrame*)mOverlayMediaMaterialGroup.mediaMaterials[i]->opaque;
            if (videoFrame!=NULL) {
                
                if (videoFrame->data!=NULL) {
                    free(videoFrame->data);
                    videoFrame->data = NULL;
                }
                
                delete videoFrame;
                videoFrame = NULL;
            }
        }
    }
}

bool MediaMixerMerger::open_all_ouput_pipelines_l()
{
    if (mOutputAudioOptions.hasAudio) {
        //open audio encoder
        bool bRet = false;
#ifdef ANDROID
        mAudioEncoderType = FAAC;
#endif

#ifdef IOS
        mAudioEncoderType = AUDIO_TOOL_BOX;
#endif
        
        mAudioEncoder = AudioEncoder::CreateAudioEncoder(mAudioEncoderType, mOutputAudioOptions.audioSampleRate, mOutputAudioOptions.audioNumChannels, mOutputAudioOptions.audioBitRate);
        
        bRet = mAudioEncoder->Open();
        
        if (!bRet) {
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_AUDIO_ENCODER_FAIL, 0);
            stop_l();
            return false;
        }
    }
    
    if (mOutputVideoOptions.hasVideo) {
        
        if (media_mix_mode==ONLY_VIDEO_MIX || media_mix_mode==VIDEO_AUDIO_MIX) {
            //open video encoder
            
            mOutputVideoOptions.videoWidth = mVideoStreamContextForMainMediaMaterial->codec->width;
            mOutputVideoOptions.videoHeight = mVideoStreamContextForMainMediaMaterial->codec->height;
            
            bool bRet = false;
#ifdef IOS
            mVideoEncoderType = VIDEO_TOOL_BOX;
            
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mOutputVideoOptions.videoFps, mOutputVideoOptions.videoWidth, mOutputVideoOptions.videoHeight, BASE_LINE, (VIDEO_ENCODE_MODE)mOutputVideoOptions.encodeMode, mOutputVideoOptions.videoBitRate, mOutputVideoOptions.quality, mOutputVideoOptions.maxKeyFrameIntervalMs, ULTRAFAST, mOutputVideoOptions.videoBitRate, mOutputVideoOptions.bStrictCBR, mOutputVideoOptions.deblockingFilterFactor);
            
            bRet = mVideoEncoder->Open();
#endif
            
#ifdef ANDROID
            if (mOutputVideoOptions.videoEncoderType==HARD_ENCODER) {
                mVideoEncoderType = MEDIACODEC;
            }else{
                mVideoEncoderType = X264;
            }
            
            mVideoEncoderType = X264;
            
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mOutputVideoOptions.videoFps, mOutputVideoOptions.videoWidth, mOutputVideoOptions.videoHeight, BASE_LINE, (VIDEO_ENCODE_MODE)mOutputVideoOptions.encodeMode, mOutputVideoOptions.videoBitRate, mOutputVideoOptions.quality, mOutputVideoOptions.maxKeyFrameIntervalMs, ULTRAFAST, mOutputVideoOptions.videoBitRate, mOutputVideoOptions.bStrictCBR, mOutputVideoOptions.deblockingFilterFactor);
            
            mVideoEncoder->registerJavaVMEnv(mJvm);
            bRet = mVideoEncoder->Open();
#endif
            
            if (!bRet) {
                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_VIDEO_ENCODER_FAIL, 0);
                stop_l();
                return false;
            }
            
        }else{
            //open remuxer
            mOutputVideoOptions.hasVideo = true;
            mOutputVideoOptions.videoWidth = mVideoStreamContextForMainMediaMaterial->codec->width;
            mOutputVideoOptions.videoHeight = mVideoStreamContextForMainMediaMaterial->codec->height;
            mOutputVideoOptions.videoBitRate = mVideoStreamContextForMainMediaMaterial->codec->bit_rate/1024;
            mOutputVideoOptions.videoFps = 20;
            
            AVRational fr = av_guess_frame_rate(mFFmpegReaderForMainMediaMaterial->getAVFormatContext(), mVideoStreamContextForMainMediaMaterial, NULL);
            if(fr.num > 0 && fr.den > 0)
            {
                mOutputVideoOptions.videoFps = fr.num/fr.den;
                if(mOutputVideoOptions.videoFps > 100 || mOutputVideoOptions.videoFps <= 0)
                {
                    mOutputVideoOptions.videoFps = 25;
                }
            }
            
            
            //init H264 AVBitStreamFilterContext
            mH264Converter = av_bitstream_filter_init("h264_mp4toannexb");
        }
        
        mOutputVideoOptions.rotation = mRotationForMainMediaMaterial;
    }
    
    // open output file
    mMediaMuxer = MediaMuxer::CreateMediaMuxer(FFMPEG_PROCESSER, MP4, mOutputUrl, &mOutputVideoOptions, &mOutputAudioOptions);
    mMediaMuxer->setListener(this);
#ifdef ANDROID
    mMediaMuxer->registerJavaVMEnv(mJvm);
#endif
    bool ret = mMediaMuxer->prepare();
    if (!ret) {
        notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_OPEN_OUTPUT_FILE_FAIL, 0);
        stop_l();
        return false;
    }
    mMediaMuxer->start();
    
    // set AudioEncoder OutputHandler
    if (mOutputAudioOptions.hasAudio) {
        mAudioEncoder->SetOutputHandler(mMediaMuxer);
    }
    
    if (mOutputVideoOptions.hasVideo) {
        if (media_mix_mode==ONLY_VIDEO_MIX || media_mix_mode==VIDEO_AUDIO_MIX)
        {
            // set VideoEncoder OutputHandler
            mVideoEncoder->SetOutputHandler(mMediaMuxer);
        }else{
            //Push H264 Headers to MediaMuxer
            LOGD("Push H264 Header(SPS and PPS) to MediaMuxer");
            
            VideoPacket HeaderPacket;
            
            HeaderPacket.nal_Num = 1;
            Nal *pNal = new Nal;
            pNal->size = MAX_H264_HEADER_SIZE;
            pNal->data = (uint8_t*)malloc(pNal->size);
            HeaderPacket.nals.push_back(pNal);
            HeaderPacket.type = H264_TYPE_SPS_PPS;
            
            const uint8_t *extraData = mVideoStreamContextForMainMediaMaterial->codec->extradata;
            uint32_t extraDataSize = mVideoStreamContextForMainMediaMaterial->codec->extradata_size;
            
            if (extraData && extraDataSize && extraData[0]==1) {
                uint32_t buf_size = extraDataSize + FF_INPUT_BUFFER_PADDING_SIZE;
                uint8_t *sps_pps_data = (uint8_t*)malloc(buf_size);
                uint32_t sps_pps_size = extraDataSize;
                
                uint32_t nal_size;
                convert_sps_pps(extraData, extraDataSize, sps_pps_data, buf_size, &sps_pps_size, &nal_size);
                
                HeaderPacket.nals[0]->size = sps_pps_size;
                memcpy(HeaderPacket.nals[0]->data, sps_pps_data, sps_pps_size);
                
                if (sps_pps_data!=NULL) {
                    free(sps_pps_data);
                    sps_pps_data = NULL;
                }
                
            }else{
                HeaderPacket.nals[0]->size = extraDataSize;
                memcpy(HeaderPacket.nals[0]->data, extraData, extraDataSize);
            }
            
            if (mMediaMuxer!=NULL) {
                mMediaMuxer->pushH264Header(&HeaderPacket);
            }
            
            HeaderPacket.Free();

        }
    }
    
    return true;
}

void MediaMixerMerger::close_all_output_pipelines_l()
{
    if (mAudioEncoder!=NULL) {
        mAudioEncoder->Close();
        AudioEncoder::DeleteAudioEncoder(mAudioEncoder, mAudioEncoderType);
        mAudioEncoder = NULL;
    }
    
    if (mVideoEncoder!=NULL) {
        mVideoEncoder->Close();
        VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
        mVideoEncoder = NULL;
    }
    
    if (mMediaMuxer!=NULL) {
        mMediaMuxer->stop();
        MediaMuxer::DeleteMediaMuxer(mMediaMuxer, FFMPEG_PROCESSER);
        mMediaMuxer = NULL;
    }
    
    if(mH264Converter!=NULL)
    {
        av_bitstream_filter_close(mH264Converter);
        mH264Converter = NULL;
    }
}

void MediaMixerMerger::resume()
{
    AutoLock autoLock(&mLock);
    
    play_l();
}

void MediaMixerMerger::play_l()
{
    if (mFlags & STREAMING) {
        return;
    }
    
    if (!(mFlags & CONNECTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    modifyFlags(CONNECTED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
    
    modifyFlags(STREAMING, SET);
}

void MediaMixerMerger::pause()
{
    AutoLock autoLock(&mLock);
    
    pause_l();
}

void MediaMixerMerger::pause_l()
{
    if (!(mFlags & STREAMING)) {
        return;
    }
    
    modifyFlags(STREAMING, CLEAR);
    
    mQueue.cancelEvent(mProcessOnePacketEvent->eventID());
    
    modifyFlags(PAUSED, SET);
    
    return;
}

void MediaMixerMerger::stop()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & END) {
        return;
    }
    
    if (mFlags & ENDING) {
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
}

void MediaMixerMerger::stop_l()
{
    modifyFlags(ENDING, ASSIGN);
    
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void MediaMixerMerger::onStopEvent()
{
    AutoLock autoLock(&mLock);
    
    close_all_pipelines_l();
    
    cancelMergerEvents();
    
    mNotificationQueue.flush();
    
    modifyFlags(END, ASSIGN);
    
    notifyListener_l(MEDIA_PROCESSER_END);
    
    pthread_cond_broadcast(&mStopCondition);
}

void MediaMixerMerger::notifyListener_l(int event, int ext1, int ext2)
{
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void MediaMixerMerger::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
            case SET:
            mFlags |= value;
            break;
            case CLEAR:
            mFlags &= ~value;
            break;
            case ASSIGN:
            mFlags = value;
            break;
    }
}

void MediaMixerMerger::cancelMergerEvents()
{
    mQueue.cancelEvent(mProcessOnePacketEvent->eventID());
    mQueue.cancelEvent(mNotifyEvent->eventID());
    mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
    mQueue.cancelEvent(mStopEvent->eventID());
}

void MediaMixerMerger::onProcessOnePacketEvent()
{
    AutoLock autoLock(&mLock);
    processVideoOrAudioPacket();
}

void MediaMixerMerger::processVideoOrAudioPacket()
{
    FFPacket* ffPacket = mFFmpegReaderForMainMediaMaterial->getMediaPacket();
    
    if (ffPacket->mediaType==-1) {
        av_packet_unref(ffPacket->avPacket);
        av_freep(ffPacket->avPacket);
        
        mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
        return;
    }
    
    if (ffPacket->ret==AVERROR_INVALIDDATA || ffPacket->ret == AVERROR(EAGAIN))
    {
        av_packet_unref(ffPacket->avPacket);
        av_freep(ffPacket->avPacket);
        
        mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
        return;
    }else if(ffPacket->ret == AVERROR_EOF)
    {
        av_packet_unref(ffPacket->avPacket);
        av_freep(ffPacket->avPacket);
        
        LOGD("ENDING...");
        
        stop_l();
        
        return;
    }else if (ffPacket->ret < 0)
    {
        av_packet_unref(ffPacket->avPacket);
        av_freep(ffPacket->avPacket);
        
        notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_READ_INPUT_FILE_FAIL);
        stop_l();
        
        return;
    }else{
        if (ffPacket->mediaType==0) {
            videoFlowing_l(ffPacket->avPacket);
        }else if(ffPacket->mediaType==1) {
            audioFlowing_l(ffPacket->avPacket);
        }
    }
}

void MediaMixerMerger::audioFlowing_l(AVPacket* audioPacket)
{
    int iret = mAudioDecoderForMainMediaMaterial->decode(audioPacket);
    av_packet_unref(audioPacket);
    av_freep(audioPacket);
    
    if (iret<0) {
        notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_AUDIO_DECODE_FAIL);
        stop_l();
        
        return;
    }else if (iret==0) {
        mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
        return;
    }else {
        AVFrame* sourceAudioFrame = mAudioDecoderForMainMediaMaterial->getPCMData();
        if (sourceAudioFrame==NULL) {
            mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
            return;
        }else {
            //do filter
            if(mAudioFilterForMainMediaMaterial->filterIn(sourceAudioFrame))
            {
                sourceAudioFrame = mAudioFilterForMainMediaMaterial->filterOut();
                
                if (sourceAudioFrame==NULL) {
                    mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                    return;
                }else{
                    calculateAudioFramePts(sourceAudioFrame);
                    
                    mCurrentAudioPtsUs = sourceAudioFrame->pts;
                    
                    int sourceAudioPcmDataSize = av_samples_get_buffer_size(NULL, av_frame_get_channels(sourceAudioFrame),
                                                                            sourceAudioFrame->nb_samples,
                                                                            (enum AVSampleFormat)sourceAudioFrame->format, 1);
                    uint8_t* sourceAudioPcmData = *(sourceAudioFrame->extended_data);
                    
                    if(sourceAudioPcmDataSize>0 && sourceAudioPcmData!=NULL)
                    {
                        mAudioPCMDataPoolForMainMediaMaterial->push(sourceAudioPcmData, sourceAudioPcmDataSize, mCurrentAudioPtsUs);
                        
                        while (true) {
                            AudioFrame *targetAudioframe = mAudioPCMDataPoolForMainMediaMaterial->pop(mAudioEncoder->GetFixedInputFrameSize());
                            
                            if (targetAudioframe==NULL) {
                                break;
                            }else {
                                bool bRet = false;
                                if (media_mix_mode==ONLY_AUDIO_MIX || media_mix_mode==VIDEO_AUDIO_MIX) {
                                    //Mix Audio
                                    bRet = mixAudioFrame_l(targetAudioframe);
                                    
                                    if (!bRet) {
                                        notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_AUDIO_ENCODE_FAIL);
                                        stop_l();
                                        
                                        return;
                                    }
                                }
                                
                                //Encode
                                bRet = mAudioEncoder->Encode(*targetAudioframe);
                                
                                if (!bRet) {
                                    notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_AUDIO_ENCODE_FAIL);
                                    stop_l();
                                    
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
    return;
}

bool MediaMixerMerger::mixAudioFrame_l(AudioFrame* mainMaterialAudioFrame)
{
    AudioFrame *backGroundSoundAudioframe = mAudioPCMDataPoolForBackGroundSoundMaterial->pop(mAudioEncoder->GetFixedInputFrameSize());
    if (backGroundSoundAudioframe!=NULL) {
        //do mix
        int16_t *srcSamples1 = (int16_t *)mainMaterialAudioFrame->data;
        int16_t *srcSamples2 = (int16_t *)backGroundSoundAudioframe->data;
        int mixCount = mAudioEncoder->GetFixedInputFrameSize()/2;
//        mixSamples(srcSamples1,srcSamples2,mixCount);
        mixSamplesWithGain(srcSamples1,srcSamples2,mixCount,mMainMediaMaterial.weight,mBackGroundSoundMaterial.weight);
        
        return true;
    }
    
    bool ret = false;
    while (true) {
        FFPacket* ffPacket = mFFmpegReaderForBackGroundSoundMaterial->getMediaPacket();
        
        if (ffPacket->ret==AVERROR_INVALIDDATA || ffPacket->ret == AVERROR(EAGAIN))
        {
            av_packet_unref(ffPacket->avPacket);
            av_freep(ffPacket->avPacket);
            
            continue;
        }else if(ffPacket->ret == AVERROR_EOF)
        {
            av_packet_unref(ffPacket->avPacket);
            av_freep(ffPacket->avPacket);
            
            FFSeekStatus seekStatus = mFFmpegReaderForBackGroundSoundMaterial->seek(0ll);
            
            if (seekStatus.seekRet) {
                continue;
            }else{
                ret = false;
                break;
            }
        }else if (ffPacket->ret < 0)
        {
            av_packet_unref(ffPacket->avPacket);
            av_freep(ffPacket->avPacket);
            
            ret = false;
            break;
        }else{
            if(ffPacket->mediaType==1) {
                AVPacket* audioPacket = ffPacket->avPacket;
                
                int iret = mAudioDecoderForBackGroundSoundMaterial->decode(audioPacket);
                av_packet_unref(audioPacket);
                av_freep(audioPacket);
                
                if (iret<0) {
                    ret = false;
                    break;
                }else if (iret==0) {
                    continue;
                }else {
                    AVFrame* sourceAudioFrame = mAudioDecoderForBackGroundSoundMaterial->getPCMData();
                    if (sourceAudioFrame==NULL) {
                        continue;
                    }else {
                        if(mAudioFilterForBackGroundSoundMaterial->filterIn(sourceAudioFrame))
                        {
                            sourceAudioFrame = mAudioFilterForBackGroundSoundMaterial->filterOut();
                            
                            if (sourceAudioFrame==NULL) {
                                continue;
                            }else {
                                int sourceAudioPcmDataSize = av_samples_get_buffer_size(NULL, av_frame_get_channels(sourceAudioFrame),
                                                                                        sourceAudioFrame->nb_samples,
                                                                                        (enum AVSampleFormat)sourceAudioFrame->format, 1);
                                uint8_t* sourceAudioPcmData = *(sourceAudioFrame->extended_data);
                                
                                if(sourceAudioPcmDataSize<=0 || sourceAudioPcmData==NULL)
                                {
                                    continue;
                                }else{
                                    mAudioPCMDataPoolForBackGroundSoundMaterial->push(sourceAudioPcmData, sourceAudioPcmDataSize, 0);
                                    AudioFrame *backGroundSoundAudioframe = mAudioPCMDataPoolForBackGroundSoundMaterial->pop(mAudioEncoder->GetFixedInputFrameSize());
                                    
                                    if (backGroundSoundAudioframe==NULL) {
                                        continue;
                                    }else{
                                        //do mix
                                        int16_t *srcSamples1 = (int16_t *)mainMaterialAudioFrame->data;
                                        int16_t *srcSamples2 = (int16_t *)backGroundSoundAudioframe->data;
                                        int mixCount = mAudioEncoder->GetFixedInputFrameSize()/2;
                                        mixSamplesWithGain(srcSamples1,srcSamples2,mixCount,mMainMediaMaterial.weight,mBackGroundSoundMaterial.weight);
                                        
                                        ret = true;
                                        break;
                                    }
                                }
                            }
                            
                        }else continue;
                    }
                }
                
            }else {
                av_packet_unref(ffPacket->avPacket);
                av_freep(ffPacket->avPacket);
                
                continue;
            }
        }

    }
    
    return ret;
}

void MediaMixerMerger::remuxVideoPacket(AVPacket* videoPacket)
{
    VideoPacket h264Body;
    
    mCurrentVideoPtsUs = videoPacket->pts * AV_TIME_BASE * av_q2d(mVideoStreamContextForMainMediaMaterial->time_base);
    h264Body.pts = mCurrentVideoPtsUs/1000;
    
    if (videoPacket->flags & AV_PKT_FLAG_KEY) {
        h264Body.type = H264_TYPE_I;
    }else {
        h264Body.type = H264_TYPE_P;
    }
    
    uint8_t *filtered_data = NULL;
    int filtered_data_size = 0;
    
    if(mH264Converter!=NULL)
    {
        av_bitstream_filter_filter(mH264Converter, mVideoStreamContextForMainMediaMaterial->codec, NULL, &filtered_data, &filtered_data_size, videoPacket->data, videoPacket->size, videoPacket->flags & AV_PKT_FLAG_KEY);
        
        av_packet_unref(videoPacket);
        av_freep(videoPacket);
    }
    
    Nal *pNal = new Nal;
    pNal->data = filtered_data;
    pNal->size = filtered_data_size;
    
    h264Body.nals.push_back(pNal);
    h264Body.nal_Num = 1;
    
    if (mMediaMuxer!=NULL) {
        mMediaMuxer->pushH264Body(&h264Body);
    }

    if (filtered_data) {
        av_free(filtered_data);
    }
    
    h264Body.Clear();
    
    mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
    return;

}


void MediaMixerMerger::videoFlowing_l(AVPacket* videoPacket)
{
    if (media_mix_mode==ONLY_VIDEO_MIX || media_mix_mode==VIDEO_AUDIO_MIX) {
        int iret = mVideoDecoderForMainMediaMaterial->decode(videoPacket);
        av_packet_unref(videoPacket);
        av_freep(videoPacket);
        
        if (iret<0) {
            notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_VIDEO_DECODE_FAIL);
            stop_l();
            
            return;
        }else if (iret==0) {
            mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
            return;
        }else {
            AVFrame* videoFrame = mVideoDecoderForMainMediaMaterial->getFrame();
            if (videoFrame==NULL) {
                mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
                return;
            }else{
                mCurrentVideoPtsUs = videoFrame->pts;
                
                if (!mGotFirstVideoFrame) {
                    mGotFirstVideoFrame = true;
                    mFirstVideoFramePtsUs = videoFrame->pts;
                }
                
                mCurrentVideoPositionUs = videoFrame->pts - mFirstVideoFramePtsUs;
#ifdef IOS
                mVideoFilterForMainMediaMaterial->videoIn(videoFrame);

                for (int i = 0; i<mOverlayMediaMaterialGroup.mediaMaterialNum; i++) {
                    if (mCurrentVideoPositionUs>=mOverlayMediaMaterialGroup.mediaMaterials[i]->startPos*1000 && mCurrentVideoPositionUs<=mOverlayMediaMaterialGroup.mediaMaterials[i]->endPos*1000) {
                        VideoFrame* pngVideoFrame = (VideoFrame *)mOverlayMediaMaterialGroup.mediaMaterials[i]->opaque;
                        if(pngVideoFrame->videoRawType==VIDEOFRAME_RAWTYPE_ARGB && mOverlayVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_BGRA){
                            
                            // for iOS
                            
                            mOverlayVideoFrame.width = pngVideoFrame->width;
                            mOverlayVideoFrame.height = pngVideoFrame->height;
                            mOverlayVideoFrame.frameSize = mOverlayVideoFrame.width * mOverlayVideoFrame.height * 4;
                            
                            bool ret = mColorSpaceConvert->BGRAtoARGB_Crop_Scale(pngVideoFrame, &mOverlayVideoFrame);
                            
                            if (!ret) {
                                LOGE("Video ColorSpaceConvert Fail!!");
                                
                                notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_COLORSPACECONVERT_FAIL);
                                stop_l();
                                
                                return;
                            }
                            
                            mVideoFilterForMainMediaMaterial->overlayIn(&mOverlayVideoFrame, mOverlayMediaMaterialGroup.mediaMaterials[i]->x, mOverlayMediaMaterialGroup.mediaMaterials[i]->y);
                            
                        }
                    }
                }
                
                AVFrame *outVideoFrame = mVideoFilterForMainMediaMaterial->videoOut();
                mVideoDecoderForMainMediaMaterial->clearFrame();

                //encode
                bool ret = mVideoEncoder->Encode(outVideoFrame->opaque, mCurrentVideoPtsUs/1000);
#endif
                
#ifdef ANDROID
                //todo
                mVideoDecoderForMainMediaMaterial->clearFrame();
                bool ret = true;
#endif
                if (!ret) {
                    LOGE("Video Encode Fail!!");
                    
                    notifyListener_l(MEDIA_PROCESSER_ERROR, MEDIA_PROCESSER_ERROR_VIDEO_ENCODE_FAIL);
                    stop_l();
                    
                    return;
                }
            }
            
        }
        
        mQueue.postEventWithDelay(mProcessOnePacketEvent, 0ll);
        return;
    }else{
        remuxVideoPacket(videoPacket);
    }
}

void MediaMixerMerger::calculateAudioFramePts(AVFrame *audioFrame)
{
    int64_t currentPts = AV_NOPTS_VALUE;
    if(av_frame_get_best_effort_timestamp(audioFrame) != AV_NOPTS_VALUE)
    {
        currentPts = av_frame_get_best_effort_timestamp(audioFrame);
    }else if(audioFrame->pts!=AV_NOPTS_VALUE)
    {
        currentPts = audioFrame->pts;
    }else if(audioFrame->pkt_pts!=AV_NOPTS_VALUE)
    {
        currentPts = audioFrame->pkt_pts;
    }else if(audioFrame->pkt_dts!=AV_NOPTS_VALUE)
    {
        currentPts = audioFrame->pkt_dts;
    }
    
    audioFrame->pts = currentPts * AV_TIME_BASE * av_q2d(mAudioStreamContextForMainMediaMaterial->time_base);
}
