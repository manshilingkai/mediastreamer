//
//  Word2Bitmap.h
//  MediaStreamer
//
//  Created by Think on 2017/3/28.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef Word2Bitmap_h
#define Word2Bitmap_h

#include <stdio.h>
#include "MediaDataType.h"
#include "MediaEffect.h"

extern "C"
{
#include <ft2build.h>
#include FT_FREETYPE_H
}

class Word2Bitmap {
public:
    Word2Bitmap(const char* fontLibPath, int fontSize, FontColor fontColor);
    ~Word2Bitmap();
    
    bool initialize();
    void terminate();
    
    void setFontSize(int fontSize);
    void setFontColor(FontColor fontColor);
    
    VideoFrame* word2Bitmap(wchar_t inputWord, float alpha);
private:
    FT_Library library;
    FT_Face face;
    
    char* mFontLibPath;
    int mFontSize;
    FontColor mFontColor;
};

#endif /* Word2Bitmap_h */
