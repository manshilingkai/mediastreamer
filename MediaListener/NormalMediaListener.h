//
//  NormalMediaListener.h
//  MediaStreamer
//
//  Created by Think on 2018/6/8.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef NormalMediaListener_h
#define NormalMediaListener_h

#include <stdio.h>

#include "IMediaListener.h"

class NormalMediaListener : public IMediaListener
{
public:
    NormalMediaListener(void (*listener)(void*,int,int,int), void* arg);
    ~NormalMediaListener();
    
    void notify(int event, int ext1, int ext2);
    
private:
    void (*mListener)(void*,int,int,int);
    void* owner;
};

#endif /* NormalMediaListener_h */
