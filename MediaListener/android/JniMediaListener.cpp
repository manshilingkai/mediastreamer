//
//  JniMediaListener.cpp
//
//  Created by Think on 16/2/23.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "JniMediaListener.h"
#include "AndroidUtils.h"
#include "JNIHelp.h"

JniMediaListener::JniMediaListener(JavaVM *jvm, jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    mJvm = jvm;
    
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    
    if (env!=NULL) {
        // Hold onto the MediaPlayer class for use in calling the static method
        // that posts events to the application thread.
        jclass clazz = env->GetObjectClass(thiz);
        if (clazz == NULL) {
            jniThrowException(env, "java/lang/Exception", NULL);
            return;
        }
        
        mClass = (jclass)env->NewGlobalRef(clazz);
        
        // We use a weak reference so the MediaPlayer object can be garbage collected.
        // The reference is only used as a proxy for callbacks.
        mObject  = env->NewGlobalRef(weak_thiz);
        
        mPostEvent = post_event;
    }
}

JniMediaListener::~JniMediaListener()
{
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    
    if (env!=NULL) {
        // remove global references
        env->DeleteGlobalRef(mObject);
        env->DeleteGlobalRef(mClass);
    }
}

void JniMediaListener::notify(int msg, int ext1, int ext2)
{
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    
    if (env!=NULL)
    {
        env->CallStaticVoidMethod(mClass, mPostEvent, mObject, msg, ext1, ext2, 0);
    }
}