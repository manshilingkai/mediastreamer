//
//  NotificationQueue.cpp
//  MediaStreamer
//
//  Created by Think on 16/9/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "NotificationQueue.h"

NotificationQueue::NotificationQueue()
{
    pthread_mutex_init(&mLock, NULL);
}

NotificationQueue::~NotificationQueue()
{
    pthread_mutex_destroy(&mLock);
}

void NotificationQueue::push(Notification* notification)
{
    pthread_mutex_lock(&mLock);
    
    mNotificationQueue.push(notification);
    
    pthread_mutex_unlock(&mLock);
}

Notification* NotificationQueue::pop()
{
    Notification *notification = NULL;
    
    pthread_mutex_lock(&mLock);
    
    if (!mNotificationQueue.empty()) {
        notification = mNotificationQueue.front();
        mNotificationQueue.pop();
    }
    
    pthread_mutex_unlock(&mLock);
    
    return notification;
}

void NotificationQueue::flush()
{
    pthread_mutex_lock(&mLock);
    Notification *notification = NULL;
    while (!mNotificationQueue.empty()) {
        notification = mNotificationQueue.front();
        if (notification!=NULL) {
            delete notification;
            notification = NULL;
        }
        mNotificationQueue.pop();
    }
    pthread_mutex_unlock(&mLock);
}
