//
//  NotificationQueue.hpp
//  MediaStreamer
//
//  Created by Think on 16/9/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef NotificationQueue_h
#define NotificationQueue_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include <queue>

using namespace std;

struct Notification
{
    int event;
    int ext1;
    int ext2;
    
    Notification()
    {
        event = 0;
        ext1 = 0;
        ext2 = 0;
    }
};

class NotificationQueue {
public:
    NotificationQueue();
    ~NotificationQueue();
    
    void push(Notification* notification);
    
    Notification* pop();
    
    void flush();
private:
    pthread_mutex_t mLock;
    queue<Notification*> mNotificationQueue;
};

#endif /* NotificationQueue_h */
