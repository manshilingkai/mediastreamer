//
//  iOSMediaListener.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef iOSMediaListener_h
#define iOSMediaListener_h

#include <stdio.h>
#include "IMediaListener.h"

class iOSMediaListener : public IMediaListener
{
public:
    iOSMediaListener(void (*listener)(void*,int,int,int), void* arg);
    ~iOSMediaListener();

    void notify(int event, int ext1, int ext2);

private:
    void (*mListener)(void*,int,int,int);
    void* owner;
};

#endif /* iOSMediaListener_h */
