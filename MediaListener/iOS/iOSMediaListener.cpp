//
//  iOSMediaListener.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "iOSMediaListener.h"

iOSMediaListener::iOSMediaListener(void (*listener)(void*,int,int,int), void* arg)
{
    this->mListener = listener;
    this->owner = arg;
}

iOSMediaListener::~iOSMediaListener()
{
    this->mListener = NULL;
    this->owner = NULL;
}

void iOSMediaListener::notify(int event, int ext1, int ext2)
{
    if (this->mListener!=NULL) {
        this->mListener(owner,event,ext1,ext2);
    }
}