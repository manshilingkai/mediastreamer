//
//  NormalMediaListener.cpp
//  MediaStreamer
//
//  Created by Think on 2018/6/8.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "NormalMediaListener.h"

NormalMediaListener::NormalMediaListener(void (*listener)(void*,int,int,int), void* arg)
{
    this->mListener = listener;
    this->owner = arg;
}

NormalMediaListener::~NormalMediaListener()
{
    this->mListener = NULL;
    this->owner = NULL;
}

void NormalMediaListener::notify(int event, int ext1, int ext2)
{
    if (this->mListener!=NULL) {
        this->mListener(owner,event,ext1,ext2);
    }
}
