//
//  SysSpeechRecognizer.h
//  MediaStreamer
//
//  Created by Think on 2019/10/24.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SysSpeechRecognizerDelegate <NSObject>
@required
- (void)onSpeechRecognizerAuthorizationStatus:(int)status;
- (void)onSpeechRecognizerRecognitionResult:(NSString*)result;
@optional
@end

@interface SysSpeechRecognizer : NSObject

- (instancetype) init;

- (void)requestRecognitionWithLocalAudioFile:(NSString*)localAudioFilePath;

@property (nonatomic, weak) id<SysSpeechRecognizerDelegate> delegate;

@end
