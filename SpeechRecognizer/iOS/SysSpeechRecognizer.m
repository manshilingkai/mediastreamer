//
//  SysSpeechRecognizer.m
//  MediaStreamer
//
//  Created by Think on 2019/10/24.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "SysSpeechRecognizer.h"
#import <Speech/Speech.h>

@implementation SysSpeechRecognizer
{
    
}

- (instancetype) init;
{
    self = [super init];
    if (self) {
        _delegate = nil;
    }
    
    return self;
}

- (void)requestRecognitionWithLocalAudioFile:(NSString*)localAudioFilePath
{
      [SFSpeechRecognizer requestAuthorization:^(SFSpeechRecognizerAuthorizationStatus status) {
          dispatch_async(dispatch_get_main_queue(), ^{
              switch (status) {
                  case SFSpeechRecognizerAuthorizationStatusNotDetermined:
                      NSLog(@"语音识别未授权");
                      if (_delegate!=nil) {
                          [_delegate onSpeechRecognizerAuthorizationStatus:SFSpeechRecognizerAuthorizationStatusNotDetermined];
                      }
                      
                      break;
                  case SFSpeechRecognizerAuthorizationStatusDenied:
                      NSLog(@"用户未授权使用语音识别");
                      if (_delegate!=nil) {
                          [_delegate onSpeechRecognizerAuthorizationStatus:SFSpeechRecognizerAuthorizationStatusDenied];
                      }
                      break;
                  case SFSpeechRecognizerAuthorizationStatusRestricted:
                      NSLog(@"语音识别在这台设备上受到限制");
                      if (_delegate!=nil) {
                          [_delegate onSpeechRecognizerAuthorizationStatus:SFSpeechRecognizerAuthorizationStatusRestricted];
                      }
                      break;
                  case SFSpeechRecognizerAuthorizationStatusAuthorized:
                      NSLog(@"语音识别已授权");
                      if (_delegate!=nil) {
                          [_delegate onSpeechRecognizerAuthorizationStatus:SFSpeechRecognizerAuthorizationStatusAuthorized];
                      }
                      
                      NSLocale *local =[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
                      SFSpeechRecognizer *localRecognizer =[[SFSpeechRecognizer alloc] initWithLocale:local];
                      NSURL *url = [[NSURL alloc] initFileURLWithPath:localAudioFilePath];
                      if (!url)
                      {
                          if (_delegate!=nil) {
                              [_delegate onSpeechRecognizerRecognitionResult:nil];
                          }
                          return;
                      }
                      SFSpeechURLRecognitionRequest *res =[[SFSpeechURLRecognitionRequest alloc] initWithURL:url];
                      [localRecognizer recognitionTaskWithRequest:res resultHandler:^(SFSpeechRecognitionResult * _Nullable result, NSError * _Nullable error) {
                          if (error) {
                              NSLog(@"语音识别解析失败: [%@]",error);
                              if (_delegate!=nil) {
                                  [_delegate onSpeechRecognizerRecognitionResult:nil];
                              }
                          }else{
                              NSLog(@"语音识别解析成功: [%@]",result.bestTranscription.formattedString);
                              if (_delegate!=nil) {
                                  [_delegate onSpeechRecognizerRecognitionResult:result.bestTranscription.formattedString];
                              }
                          }
                      }];
                      
                      break;
              }
          });
      }];
}

@end
