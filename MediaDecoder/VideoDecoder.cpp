//
//  VideoDecoder.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "VideoDecoder.h"
#include "FFVideoDecoder.h"

#ifdef IOS
//#include "VideoToolBoxDecoder.h"
#include "FFVTBDecoder.h"
#endif

#ifdef ANDROID
#include "MediaCodecDecoder.h"
#endif

VideoDecoder* VideoDecoder::CreateVideoDecoder(VideoDecoderType type)
{
    if (type==VIDEO_DECODER_FFMPEG) {
        return new FFVideoDecoder;
    }
    
    #ifdef IOS
    if (type==VIDEO_DECODER_VIDEOTOOLBOX) {
//        return new VideoToolBoxDecoder();
        return new FFVTBDecoder();
    }

    #endif
    
    return NULL;
}

#ifdef ANDROID
VideoDecoder* VideoDecoder::CreateVideoDecoderWithJniEnv(VideoDecoderType type, JavaVM *jvm, void *surface)
{
    if (type==VIDEO_DECODER_MEDIACODEC_JAVA) {
        return new MediaCodecDecoder(jvm, surface);
    }
}
#endif

void VideoDecoder::DeleteVideoDecoder(VideoDecoder* videoDecoder, VideoDecoderType type)
{
    if (type==VIDEO_DECODER_FFMPEG) {
        FFVideoDecoder* ffVideoDecoder = (FFVideoDecoder*)videoDecoder;
        delete ffVideoDecoder;
        ffVideoDecoder = NULL;
    }
    
    #ifdef IOS
    if (type==VIDEO_DECODER_VIDEOTOOLBOX) {
//        VideoToolBoxDecoder *videoToolBoxDecoder = (VideoToolBoxDecoder*)videoDecoder;
        FFVTBDecoder* videoToolBoxDecoder = (FFVTBDecoder*)videoDecoder;
        delete videoToolBoxDecoder;
        videoToolBoxDecoder = NULL;
    }
    
    #endif
    
    #ifdef ANDROID
    if (type==VIDEO_DECODER_MEDIACODEC_JAVA) {
        MediaCodecDecoder *mediaCodecDecoder = (MediaCodecDecoder*)videoDecoder;
        delete mediaCodecDecoder;
        mediaCodecDecoder = NULL;
    }
    #endif
}
