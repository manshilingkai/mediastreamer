//
//  FFAudioDecoder.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "FFAudioDecoder.h"
#include "MediaLog.h"

FFAudioDecoder::FFAudioDecoder()
{
    mAudioStream = NULL;
    mCodecContext = NULL;

    mFrame = NULL;
    mGotFrame = 0;
}

FFAudioDecoder::~FFAudioDecoder()
{
}

bool FFAudioDecoder::open(AVStream* audioStreamContext)
{
    mAudioStream = audioStreamContext;
    if (mAudioStream==NULL) {
        LOGE("%s","mAudioStream is null");
        return false;
    }
    
    mCodecContext = mAudioStream->codec;
    if (mCodecContext==NULL) {
        LOGE("%s","mCodecContext is null");
        return false;
    }
    
    mCodecContext->refcounted_frames = 0;
    
#if defined(IOS) || defined(ANDROID)
    AVCodec* codec = NULL;
    if (audioStreamContext->codec->codec_id==AV_CODEC_ID_AAC)
    {
        codec = avcodec_find_decoder_by_name("libfdk_aac");
    }else{
        codec = avcodec_find_decoder(mCodecContext->codec_id);
    }
#else
    AVCodec* codec = avcodec_find_decoder(mCodecContext->codec_id);
#endif
    
    if(codec==NULL)
    {
        LOGE("Failed to find audio decoder:%d", mCodecContext->codec_id);
        return false;
    }
    
//    mCodecContext->flags |= CODEC_FLAG_LOW_DELAY;
//    mCodecContext->flags2 |= CODEC_FLAG2_FAST;
    
    AVDictionary *opts = NULL;
    av_dict_set(&opts, "threads", "auto", 0);
    
    if (avcodec_open2(mCodecContext, codec, &opts) < 0)
    {
        LOGE("Failed to open audio decoder:%d", mCodecContext->codec_id);
        return false;
    }
    
    mFrame = av_frame_alloc();
    mGotFrame = 0;
    
    return true;
}

void FFAudioDecoder::dispose()
{
    if(mAudioStream!=NULL && mCodecContext!=NULL)
    {
        avcodec_close(mCodecContext);
    }
    
    av_frame_free(&mFrame);
}

int FFAudioDecoder::decode(AVPacket* audioPacket)
{
    int iBytesUsed = 0;
    
    /* some codecs will attempt to consume more data than what we gave */
    iBytesUsed = avcodec_decode_audio4(mCodecContext,mFrame,&mGotFrame,audioPacket);
    
    if (iBytesUsed > audioPacket->size)
    {
        LOGW("%s","FFAudioDecoder::Decode - decoder attempted to consume more data than given");
        iBytesUsed = audioPacket->size;
    }
    
    if (iBytesUsed>0 && mGotFrame) {
        return iBytesUsed;
    }else if (iBytesUsed < 0 && !mGotFrame) {
        return iBytesUsed;
    }else {
        return 0;
    }
}

void FFAudioDecoder::flush()
{
    if (mCodecContext) {
        avcodec_flush_buffers(mCodecContext);
    }
}

int FFAudioDecoder::getPCMData(uint8_t** dst)
{
    if(mGotFrame)
    {
        mGotFrame = 0;
        
        int planes = av_sample_fmt_is_planar(mCodecContext->sample_fmt) ? mFrame->channels : 1;
        for (int i=0; i<planes; i++)
            dst[i] = mFrame->extended_data[i];
        
        return mFrame->nb_samples * mFrame->channels * av_get_bytes_per_sample(mCodecContext->sample_fmt);
    }
    
    return 0;
}

AVFrame* FFAudioDecoder::getPCMData()
{
    if (mGotFrame) {
        mGotFrame = 0;

        return mFrame;
    }
    
    return NULL;
}

int FFAudioDecoder::getChannels()
{
    return mCodecContext->channels;
}

uint64_t FFAudioDecoder::GetChannelLayout()
{
    if (mCodecContext->channels && !mCodecContext->channel_layout)
        mCodecContext->channel_layout = av_get_default_channel_layout(mCodecContext->channels);

    return mCodecContext->channel_layout;
}

int FFAudioDecoder::getSampleRate()
{
    return mCodecContext->sample_rate;
}

AVSampleFormat FFAudioDecoder::getSampleFormat()
{
    return mCodecContext->sample_fmt;
}
