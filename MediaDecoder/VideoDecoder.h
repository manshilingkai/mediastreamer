//
//  VideoDecoder.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__VideoDecoder__
#define __MediaPlayer__VideoDecoder__

#include <stdio.h>

extern "C" {
#include "libavformat/avformat.h"
}

#ifdef ANDROID
#include "jni.h"
#endif

enum VideoDecoderType
{
    VIDEO_DECODER_FFMPEG = 0,
    
    VIDEO_DECODER_VIDEOTOOLBOX = 1,
    
    VIDEO_DECODER_MEDIACODEC_JAVA = 2,
    VIDEO_DECODER_MEDIACODEC_NDK = 3,
};

class VideoDecoder
{
public:
    static VideoDecoder* CreateVideoDecoder(VideoDecoderType type);
    
#ifdef ANDROID
    static VideoDecoder* CreateVideoDecoderWithJniEnv(VideoDecoderType type, JavaVM *jvm, void *surface);
#endif
    
    static void DeleteVideoDecoder(VideoDecoder* videoDecoder, VideoDecoderType type);
    
    virtual ~VideoDecoder() {}
    
    /*
     * Open the decoder, returns true on success
     */
    virtual bool open(AVStream* videoStreamContext, bool isRealTime = false) = 0;
    
    /*
     * Dispose, Free all resources
     */
    virtual void dispose() = 0;
    
    /*
     * returns bytes used or -1 on error
     *
     */
    virtual int decode(AVPacket* videoPacket) = 0;
    
    /*
     * the data is valid until the next Decode call
     */
    virtual AVFrame* getFrame() = 0;
    
    /*
     * the data is cleared to zero
     */
    virtual void clearFrame() = 0;
    
    /*
     * flush the decoder.
     */
    virtual void flush() = 0;
    
    /*
     * will be called by video player indicating if a frame will eventually be dropped
     * codec can then skip actually decoding the data, just consume the data set picture headers
     */
    virtual void setDropState(bool bDrop) = 0;
};

#endif /* defined(__MediaPlayer__VideoDecoder__) */
