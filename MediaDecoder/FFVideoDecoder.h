//
//  FFVideoDecoder.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__FFVideoDecoder__
#define __MediaPlayer__FFVideoDecoder__

#include <pthread.h>
#include "VideoDecoder.h"

class FFVideoDecoder : public VideoDecoder
{
public:
    FFVideoDecoder();
    ~FFVideoDecoder();

    bool open(AVStream* videoStreamContext, bool isRealTime = false);
    
    void dispose();
    
    int decode(AVPacket* videoPacket);

    AVFrame* getFrame();
    
    void clearFrame();
    
    void flush();
    
    void setDropState(bool bDrop);
    
private:
    AVStream* mVideoStream;
    AVCodecContext *mCodecContext;
    
    AVFrame *mFrame;
    
    int got_picture;
};

#endif /* defined(__MediaPlayer__FFVideoDecoder__) */
