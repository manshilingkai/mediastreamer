//
//  FFVTBDecoder.h
//  MediaPlayer
//
//  Created by PPTV on 16/9/13.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef FFVTBDecoder_h
#define FFVTBDecoder_h

#include <stdio.h>
#include "VideoDecoder.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/pixdesc.h>
#include <libavutil/hwcontext.h>
#include <libavutil/opt.h>
#include <libavutil/avassert.h>
#include <libavutil/imgutils.h>
}

class FFVTBDecoder : public VideoDecoder
{
public:
    FFVTBDecoder();
    ~FFVTBDecoder();
    
    bool open(AVStream* videoStreamContext, bool isRealTime = false);
    void dispose();
    
    int decode(AVPacket* videoPacket);
    
    AVFrame* getFrame();
    
    void clearFrame();
    
    void flush();
    
    void setDropState(bool bDrop);
    
private:
    static enum AVPixelFormat find_fmt_by_hw_type(const enum AVHWDeviceType type);
    static enum AVPixelFormat get_hw_format(AVCodecContext *ctx, const enum AVPixelFormat *pix_fmts);
    int hw_decoder_init(AVCodecContext *ctx, const enum AVHWDeviceType type);
private:
    void outputFrame();
private:
    AVStream* mVideoStream;
    AVCodecContext *decoder_ctx;
    
    AVFrame *mFrame;
    
    int got_picture;
    
    AVBufferRef *hw_device_ctx;
    static enum AVPixelFormat hw_pix_fmt;
    
private:
    int mVideoRotation;
};

#endif /* FFVTBDecoder_h */
