//
//  FFVTBDecoder.cpp
//  MediaPlayer
//
//  Created by PPTV on 16/9/13.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "FFVTBDecoder.h"
#include "MediaLog.h"

#include <CoreVideo/CoreVideo.h>
#include <CoreMedia/CoreMedia.h>

enum AVPixelFormat FFVTBDecoder::hw_pix_fmt = AV_PIX_FMT_NONE;

enum AVPixelFormat FFVTBDecoder::find_fmt_by_hw_type(const enum AVHWDeviceType type)
{
    enum AVPixelFormat fmt;
    
    switch (type) {
        case AV_HWDEVICE_TYPE_VAAPI:
            fmt = AV_PIX_FMT_VAAPI;
            break;
        case AV_HWDEVICE_TYPE_DXVA2:
            fmt = AV_PIX_FMT_DXVA2_VLD;
            break;
        case AV_HWDEVICE_TYPE_D3D11VA:
            fmt = AV_PIX_FMT_D3D11;
            break;
        case AV_HWDEVICE_TYPE_VDPAU:
            fmt = AV_PIX_FMT_VDPAU;
            break;
        case AV_HWDEVICE_TYPE_VIDEOTOOLBOX:
            fmt = AV_PIX_FMT_VIDEOTOOLBOX;
            break;
        default:
            fmt = AV_PIX_FMT_NONE;
            break;
    }
    
    return fmt;
}

enum AVPixelFormat FFVTBDecoder::get_hw_format(AVCodecContext *ctx,
                                        const enum AVPixelFormat *pix_fmts)
{
    const enum AVPixelFormat *p;
    
    for (p = pix_fmts; *p != -1; p++) {
        if (*p == hw_pix_fmt)
            return *p;
    }
    
    LOGE("Failed to get HW surface format.\n");
    return AV_PIX_FMT_NONE;
}

int FFVTBDecoder::hw_decoder_init(AVCodecContext *ctx, const enum AVHWDeviceType type)
{
    int err = 0;
    
    if ((err = av_hwdevice_ctx_create(&hw_device_ctx, type,
                                      NULL, NULL, 0)) < 0) {
        LOGE("Failed to create specified HW device.\n");
        return err;
    }
    ctx->hw_device_ctx = av_buffer_ref(hw_device_ctx);
    
    return err;
}

FFVTBDecoder::FFVTBDecoder()
{
    mVideoStream = NULL;
    decoder_ctx = NULL;
    
    mFrame = av_frame_alloc();
    
    got_picture = 0;
    
    hw_device_ctx = NULL;
    
    mVideoRotation = 0;
}

FFVTBDecoder::~FFVTBDecoder()
{
    av_frame_free(&mFrame);
}

bool FFVTBDecoder::open(AVStream* videoStreamContext, bool isRealTime)
{
    mVideoStream = videoStreamContext;
    if(mVideoStream==NULL)
    {
        LOGE("%s","mVideoStream is null");
        return false;
    }
    
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(mVideoStream->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            int rotate = atoi(m->value);
            mVideoRotation = rotate;
        }
    }
    
    decoder_ctx = mVideoStream->codec;
    if(decoder_ctx==NULL)
    {
        LOGE("%s","decoder_ctx is null");
        return false;
    }
    
    AVCodec* codec = avcodec_find_decoder(decoder_ctx->codec_id);
    if (codec == NULL)
    {
        LOGE("Failed to find video decoder:%d", decoder_ctx->codec_id);
        return false;
    }
    
    enum AVHWDeviceType type = AV_HWDEVICE_TYPE_VIDEOTOOLBOX;
    hw_pix_fmt = find_fmt_by_hw_type(type);
    
    decoder_ctx->get_format  = get_hw_format;
    decoder_ctx->refcounted_frames = 1;

    if (hw_decoder_init(decoder_ctx, type) < 0)
        return false;
    
    if (avcodec_open2(decoder_ctx, codec, NULL) < 0)
    {
        if (hw_device_ctx) {
            av_buffer_unref(&hw_device_ctx);
            hw_device_ctx = NULL;
        }
        
        LOGE("Failed to open video decoder:%d", decoder_ctx->codec_id);
        return false;
    }
    return true;
}

void FFVTBDecoder::dispose()
{
    if(mVideoStream!=NULL && decoder_ctx!=NULL)
    {
        avcodec_close(decoder_ctx);
    }
    
    if (hw_device_ctx) {
        av_buffer_unref(&hw_device_ctx);
        hw_device_ctx = NULL;
    }
}

void FFVTBDecoder::outputFrame()
{
    got_picture = 0;
    AVFrame *frame = av_frame_alloc();
    int ret = avcodec_receive_frame(decoder_ctx, frame);
    if (ret>=0) {
        got_picture = 1;
        
        mFrame->width = frame->width;
        mFrame->height = frame->height;
        mFrame->format = frame->format;
        mFrame->pts = frame->pts;
        
        int64_t currentPts = AV_NOPTS_VALUE;
        if(av_frame_get_best_effort_timestamp(mFrame) != AV_NOPTS_VALUE)
        {
            currentPts = av_frame_get_best_effort_timestamp(mFrame);
        }else if(mFrame->pts!=AV_NOPTS_VALUE)
        {
            currentPts = mFrame->pts;
        }else if(mFrame->pkt_pts!=AV_NOPTS_VALUE)
        {
            currentPts = mFrame->pkt_pts;
        }else if(mFrame->pkt_dts!=AV_NOPTS_VALUE)
        {
            currentPts = mFrame->pkt_dts;
        }
        
        mFrame->pkt_pts = currentPts;
        mFrame->pts = currentPts * AV_TIME_BASE * av_q2d(mVideoStream->time_base);
        
        int64_t currentDuration = av_frame_get_pkt_duration(mFrame);
        mFrame->pkt_duration = currentDuration * AV_TIME_BASE * av_q2d(mVideoStream->time_base);
        
        mFrame->opaque = CVBufferRetain((CVPixelBufferRef)frame->data[3]);
        
        av_dict_set_int(&mFrame->metadata, "rotate", mVideoRotation,0);
    }
    av_frame_free(&frame);
}

int FFVTBDecoder::decode(AVPacket* videoPacket)
{
    while (true) {
        int ret = avcodec_send_packet(decoder_ctx, videoPacket);
        if (ret>=0) {
            break;
        }else if (ret==AVERROR(EAGAIN)) {
            if (got_picture) {
                got_picture = 0;
                clearFrame();
            }
            outputFrame();
            continue;
        }else {
            //return -1;
            return 0;
        }
    }
    
    if (!got_picture) {
        outputFrame();
    }
    
    if (got_picture) {
        return 1;
    }else{
        return 0;
    }
}

AVFrame* FFVTBDecoder::getFrame()
{
    if (got_picture) {
        got_picture = 0;
        
        return mFrame;
    }else {
        return NULL;
    }
}

void FFVTBDecoder::clearFrame()
{
    av_dict_free(&mFrame->metadata);
    
    if (mFrame->opaque) {
        CVBufferRelease((CVPixelBufferRef)mFrame->opaque);
        mFrame->opaque = NULL;
    }
}

void FFVTBDecoder::flush()
{
    if (decoder_ctx) {
        avcodec_flush_buffers(decoder_ctx);
    }
}

void FFVTBDecoder::setDropState(bool bDrop)
{
    if (decoder_ctx) {
        if( bDrop )
        {
            decoder_ctx->skip_frame = AVDISCARD_NONREF;
            decoder_ctx->skip_idct = AVDISCARD_NONREF;
            decoder_ctx->skip_loop_filter = AVDISCARD_NONREF;
        }
        else
        {
            decoder_ctx->skip_frame = AVDISCARD_DEFAULT;
            decoder_ctx->skip_idct = AVDISCARD_DEFAULT;
            decoder_ctx->skip_loop_filter = AVDISCARD_DEFAULT;
        }
    }
}
