//
//  AudioDecoder.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "AudioDecoder.h"
#include "FFAudioDecoder.h"

AudioDecoder* AudioDecoder::CreateAudioDecoder(AudioDecoderType type)
{
    if (type==AUDIO_DECODER_FFMPEG) {
        return new FFAudioDecoder;
    }
    
    return NULL;
}

void AudioDecoder::DeleteAudioDecoder(AudioDecoder* audioDecoder, AudioDecoderType type)
{
    if (type==AUDIO_DECODER_FFMPEG) {
        FFAudioDecoder* ffAudioDecoder = (FFAudioDecoder*)audioDecoder;
        delete ffAudioDecoder;
        ffAudioDecoder = NULL;
    }
}