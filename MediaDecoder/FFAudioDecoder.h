//
//  FFAudioDecoder.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__FFAudioDecoder__
#define __MediaPlayer__FFAudioDecoder__

#include <stdio.h>

#include "AudioDecoder.h"

class FFAudioDecoder : public AudioDecoder
{
public:
    FFAudioDecoder();
    ~FFAudioDecoder();
    
    bool open(AVStream* audioStreamContext);
    
    void dispose();
    int decode(AVPacket* audioPacket);
    
    void flush();
    
    int getPCMData(uint8_t** dst);
    AVFrame* getPCMData();
    
    int getChannels();
    uint64_t GetChannelLayout();
    int getSampleRate();
    AVSampleFormat getSampleFormat();
private:
    AVStream* mAudioStream;
    AVCodecContext *mCodecContext;
    
    AVFrame *mFrame;
    int mGotFrame;
};

#endif /* defined(__MediaPlayer__FFAudioDecoder__) */
