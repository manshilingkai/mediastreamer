//
//  AudioDecoder.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__AudioDecoder__
#define __MediaPlayer__AudioDecoder__

#include <stdio.h>

extern "C" {
#include "libavformat/avformat.h"
#include "libswresample/swresample.h"
}

enum AudioDecoderType
{
    AUDIO_DECODER_FFMPEG = 0,
};

class AudioDecoder
{
public:
    static AudioDecoder* CreateAudioDecoder(AudioDecoderType type);
    static void DeleteAudioDecoder(AudioDecoder* audioDecoder, AudioDecoderType type);
    
    virtual ~AudioDecoder() {}
    
    /*
     * Open the decoder, returns true on success
     */
    virtual bool open(AVStream* audioStreamContext) = 0;
    
    /*
     * Dispose, Free all resources
     */
    virtual void dispose() = 0;
    
    /*
     * returns bytes used or -1 on error
     *
     */
    virtual int decode(AVPacket* audioPacket) = 0;
    
    /*
     * flush the decoder
     *
     */
    virtual void flush() = 0;
    
    /*
     * returns nr of bytes in decode buffer
     * the data is valid until the next Decode call
     */
    virtual int getPCMData(uint8_t** dst) = 0;
    
    /*
     * the data is valid until the next Decode call
     */
    virtual AVFrame* getPCMData() = 0;
    
    /*
     * returns the nr of channels for the decoded audio stream
     */
    virtual int getChannels() = 0;
    
    /*
     * returns the channel layout
     */
    virtual uint64_t GetChannelLayout() = 0;
    
    /*
     * returns the samplerate for the decoded audio stream
     */
    virtual int getSampleRate() = 0;
    
    /*
     * returns the data format for the decoded audio stream
     */
    virtual enum AVSampleFormat getSampleFormat() = 0;
};

#endif /* defined(__MediaPlayer__AudioDecoder__) */
