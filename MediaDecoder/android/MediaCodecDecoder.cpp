//
//  MediaCodecDecoder.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaCodecDecoder.h"
#include "MediaLog.h"
#include "MediaMath.h"
#include "AndroidUtils.h"

#include "h264_nal.h"

enum MEMBERTYPE
{
    UNKNOWNMEMBER, METHOD, STATIC_METHOD, FIELD
};

struct JavaClassMember
{
    const char *memberName;
    const char *sig;
    const char *className;
    void* jniMemberID;
    MEMBERTYPE type;
};

static struct JavaClassMember members[] = {
    { "toString", "()Ljava/lang/String;", "java/lang/Object", NULL, METHOD },
    
    { "getCodecCount", "()I", "android/media/MediaCodecList", NULL, STATIC_METHOD },
    { "getCodecInfoAt", "(I)Landroid/media/MediaCodecInfo;", "android/media/MediaCodecList", NULL, STATIC_METHOD },
    
    { "isEncoder", "()Z", "android/media/MediaCodecInfo", NULL, METHOD },
    { "getSupportedTypes", "()[Ljava/lang/String;", "android/media/MediaCodecInfo", NULL, METHOD },
    { "getName", "()Ljava/lang/String;", "android/media/MediaCodecInfo", NULL, METHOD },
    { "getCapabilitiesForType", "(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;", "android/media/MediaCodecInfo", NULL, METHOD },
    
    { "profileLevels", "[Landroid/media/MediaCodecInfo$CodecProfileLevel;", "android/media/MediaCodecInfo$CodecCapabilities", NULL, FIELD },
    { "profile", "I", "android/media/MediaCodecInfo$CodecProfileLevel", NULL, FIELD },
    { "level", "I", "android/media/MediaCodecInfo$CodecProfileLevel", NULL, FIELD },
    
    { "createByCodecName", "(Ljava/lang/String;)Landroid/media/MediaCodec;", "android/media/MediaCodec", NULL, STATIC_METHOD },
    { "configure", "(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V", "android/media/MediaCodec", NULL, METHOD },
    { "start", "()V", "android/media/MediaCodec", NULL, METHOD },
    { "stop", "()V", "android/media/MediaCodec", NULL, METHOD },
    { "flush", "()V", "android/media/MediaCodec", NULL, METHOD },
    { "release", "()V", "android/media/MediaCodec", NULL, METHOD },
    { "getOutputFormat", "()Landroid/media/MediaFormat;", "android/media/MediaCodec", NULL, METHOD },
    { "getInputBuffers", "()[Ljava/nio/ByteBuffer;", "android/media/MediaCodec", NULL, METHOD },
    { "getOutputBuffers", "()[Ljava/nio/ByteBuffer;", "android/media/MediaCodec", NULL, METHOD },
    { "dequeueInputBuffer", "(J)I", "android/media/MediaCodec", NULL, METHOD },
    { "dequeueOutputBuffer", "(Landroid/media/MediaCodec$BufferInfo;J)I", "android/media/MediaCodec", NULL, METHOD },
    { "queueInputBuffer", "(IIIJI)V", "android/media/MediaCodec", NULL, METHOD },
    { "releaseOutputBuffer", "(IZ)V", "android/media/MediaCodec", NULL, METHOD },
    
    { "createVideoFormat", "(Ljava/lang/String;II)Landroid/media/MediaFormat;", "android/media/MediaFormat", NULL, STATIC_METHOD },
    { "setInteger", "(Ljava/lang/String;I)V", "android/media/MediaFormat", NULL, METHOD },
    { "getInteger", "(Ljava/lang/String;)I", "android/media/MediaFormat", NULL, METHOD },
    { "setByteBuffer", "(Ljava/lang/String;Ljava/nio/ByteBuffer;)V", "android/media/MediaFormat", NULL, METHOD },
    
    { "<init>", "()V", "android/media/MediaCodec$BufferInfo", NULL, METHOD },
    { "size", "I", "android/media/MediaCodec$BufferInfo", NULL, FIELD },
    { "offset", "I", "android/media/MediaCodec$BufferInfo", NULL, FIELD },
    { "presentationTimeUs", "J", "android/media/MediaCodec$BufferInfo", NULL, FIELD },
    { "flags", "I", "android/media/MediaCodec$BufferInfo", NULL, FIELD },
    
    { "allocateDirect", "(I)Ljava/nio/ByteBuffer;", "java/nio/ByteBuffer", NULL, STATIC_METHOD },
    { "limit", "(I)Ljava/nio/Buffer;", "java/nio/ByteBuffer", NULL, METHOD },
    
    { NULL, NULL, NULL, NULL, UNKNOWNMEMBER },
};

static int jstrcmp(JNIEnv* env, jobject str, const char* str2)
{
    jsize len = env->GetStringUTFLength(str);
    if (len != (jsize) strlen(str2))
        return -1;
    const char *ptr = env->GetStringUTFChars(str, NULL);
    int ret = memcmp(ptr, str2, len);
    env->ReleaseStringUTFChars(str, ptr);
    return ret;
}

MediaCodecDecoder::MediaCodecDecoder(JavaVM *jvm, void *surface)
{
    codecName = NULL;
    
    codec = NULL;
    input_buffers = NULL;
    output_buffers = NULL;
    buffer_info = NULL;
    
    mJvm = jvm;
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    sps_pps_data = NULL;
    sps_pps_size = 0;
    
    mSurface = static_cast<jobject>(surface);
}

MediaCodecDecoder::~MediaCodecDecoder()
{

}

bool MediaCodecDecoder::open(AVStream* videoStreamContext, bool isRealTime)
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    mVideoStream = videoStreamContext;
    
    //for H265:"video/hevc"
    //for H264:"video/avc"
    //for VP8:"video/x-vnd.on2.vp8"
    strcpy(mime, "video/avc");
    
//    mH264Converter = av_bitstream_filter_init("h264_mp4toannexb");

    LOGI("members");
    
    jclass last_class = NULL;
    for (int i = 0; members[i].memberName; i++) {
        if (i == 0 || strcmp(members[i].className, members[i - 1].className))
        {
            //release last_class
            if (last_class!=NULL) {
                mEnv->DeleteLocalRef(last_class);
                last_class = NULL;
            }
            
            last_class = mEnv->FindClass(members[i].className);
        }
        
        if (mEnv->ExceptionOccurred()) {
            LOGI("Unable to find class %s", members[i].className);
            mEnv->ExceptionClear();
            
            return false;
        }
        
        switch (members[i].type) {
            case METHOD:
                members[i].jniMemberID = (void*)
                mEnv->GetMethodID(last_class, members[i].memberName, members[i].sig);
                break;
            case STATIC_METHOD:
                members[i].jniMemberID = (void*)
                mEnv->GetStaticMethodID(last_class, members[i].memberName, members[i].sig);
                break;
            case FIELD:
                members[i].jniMemberID = (void*)
                mEnv->GetFieldID(last_class, members[i].memberName, members[i].sig);
                break;
        }
        
        if (mEnv->ExceptionOccurred()) {
            LOGI("Unable to find the member %s in %s",
                 members[i].memberName, members[i].className);
            mEnv->ExceptionClear();
            
            return false;
        }
    }
    
    //release last_class
    if (last_class!=NULL) {
        mEnv->DeleteLocalRef(last_class);
        last_class = NULL;
    }
    
    int i = -1;
    this->tostring = (jmethodID)members[++i].jniMemberID;
    this->get_codec_count = (jmethodID)members[++i].jniMemberID;
    this->get_codec_info_at = (jmethodID)members[++i].jniMemberID;
    this->is_encoder = (jmethodID)members[++i].jniMemberID;
    this->get_supported_types = (jmethodID)members[++i].jniMemberID;
    this->get_name = (jmethodID)members[++i].jniMemberID;
    this->get_capabilities_for_type = (jmethodID)members[++i].jniMemberID;
    this->profile_levels_field = (jfieldID)members[++i].jniMemberID;
    this->profile_field = (jfieldID)members[++i].jniMemberID;
    this->level_field = (jfieldID)members[++i].jniMemberID;
    this->create_by_codec_name = (jmethodID)members[++i].jniMemberID;
    this->configure = (jmethodID)members[++i].jniMemberID;
    this->start = (jmethodID)members[++i].jniMemberID;
    this->stop = (jmethodID)members[++i].jniMemberID;
    this->jflush = (jmethodID)members[++i].jniMemberID;
    this->release = (jmethodID)members[++i].jniMemberID;
    this->get_output_format = (jmethodID)members[++i].jniMemberID;
    this->get_input_buffers = (jmethodID)members[++i].jniMemberID;
    this->get_output_buffers = (jmethodID)members[++i].jniMemberID;
    this->dequeue_input_buffer = (jmethodID)members[++i].jniMemberID;
    this->dequeue_output_buffer = (jmethodID)members[++i].jniMemberID;
    this->queue_input_buffer = (jmethodID)members[++i].jniMemberID;
    this->release_output_buffer = (jmethodID)members[++i].jniMemberID;
    this->create_video_format = (jmethodID)members[++i].jniMemberID;
    this->set_integer = (jmethodID)members[++i].jniMemberID;
    this->get_integer = (jmethodID)members[++i].jniMemberID;
    this->set_bytebuffer = (jmethodID)members[++i].jniMemberID;
    this->buffer_info_ctor = (jmethodID)members[++i].jniMemberID;
    this->size_field = (jfieldID)members[++i].jniMemberID;
    this->offset_field = (jfieldID)members[++i].jniMemberID;
    this->pts_field = (jfieldID)members[++i].jniMemberID;
    this->flags_field = (jfieldID)members[++i].jniMemberID;
    this->allocate_direct = (jmethodID)members[++i].jniMemberID;
    this->limit = (jmethodID)members[++i].jniMemberID;
    
    
    //get codec name
    jclass media_codec_list_class = mEnv->FindClass("android/media/MediaCodecList");
    int num_codecs = mEnv->CallStaticIntMethod(media_codec_list_class,this->get_codec_count);
    mEnv->DeleteLocalRef(media_codec_list_class);
    
    for (int i = 0; i < num_codecs; i++) {
        jobject info = NULL;
        jobject name = NULL;
        jobject types = NULL;
        jsize name_len = 0;
        int num_types = 0;
        const char *name_ptr = NULL;
        bool found = false;
        
        jclass media_codec_list_class = mEnv->FindClass("android/media/MediaCodecList");
        info = mEnv->CallStaticObjectMethod(media_codec_list_class,this->get_codec_info_at, i);
        mEnv->DeleteLocalRef(media_codec_list_class);

        if (mEnv->CallBooleanMethod(info, this->is_encoder))
        {
            if (info)
                mEnv->DeleteLocalRef(info);
            continue;
        }
        
        types = mEnv->CallObjectMethod(info, this->get_supported_types);
        num_types = mEnv->GetArrayLength((jarray)types);
        name = mEnv->CallObjectMethod(info, this->get_name);
        name_len = mEnv->GetStringUTFLength((jstring)name);
        name_ptr = mEnv->GetStringUTFChars((jstring)name, NULL);
        found = false;
        
        if (!strncmp(name_ptr, "OMX.google.", __MIN(11, name_len)))
        {
            if (name)
            {
                mEnv->ReleaseStringUTFChars((jstring)name, name_ptr);
                mEnv->DeleteLocalRef(name);
            }
            if (types)
                mEnv->DeleteLocalRef(types);
            if (info)
                mEnv->DeleteLocalRef(info);
            
            continue;
        }

        for (int j = 0; j < num_types && !found; j++) {
            jobject type = mEnv->GetObjectArrayElement(types, j);
            if (!jstrcmp(mEnv, type, mime)) {
                found = true;
            }
            mEnv->DeleteLocalRef(type);
        }
        if (found) {
            LOGI("using %.*s", name_len, name_ptr);
            this->codecName = (char*)malloc(name_len + 1);
            memcpy(this->codecName, name_ptr, name_len);
            this->codecName[name_len] = '\0';
        }

        if (name)
        {
            mEnv->ReleaseStringUTFChars((jstring)name, name_ptr);
            mEnv->DeleteLocalRef(name);
        }
        if (types)
            mEnv->DeleteLocalRef(types);
        if (info)
            mEnv->DeleteLocalRef(info);
        
        if (found)
            break;
    }

    if (!this->codecName) {
        LOGD("No suitable codec matching %s was found", mime);
        return false;
    }
    
    LOGI("alloc codec");
    //alloc codec
    jstring codec_name = NULL;
    codec_name = mEnv->NewStringUTF(this->codecName);
    jclass media_codec_class = mEnv->FindClass("android/media/MediaCodec");
    this->codec = mEnv->CallStaticObjectMethod(media_codec_class, this->create_by_codec_name,codec_name);
    mEnv->DeleteLocalRef(media_codec_class);
    mEnv->DeleteLocalRef(codec_name);
    
    if (mEnv->ExceptionOccurred()) {
        LOGI("Exception occurred in MediaCodec.createByCodecName.");
        mEnv->ExceptionClear();
        
        return false;
    }
    
    this->codec = mEnv->NewGlobalRef(this->codec);
    
    LOGI("create format");
    //create format
    jstring jstrMime = NULL;
    jstrMime = mEnv->NewStringUTF(mime);
    jclass media_format_class = mEnv->FindClass("android/media/MediaFormat");
    jobject format = mEnv->CallStaticObjectMethod(media_format_class, this->create_video_format, jstrMime, mVideoStream->codec->width, mVideoStream->codec->height);
    mEnv->DeleteLocalRef(media_format_class);
    mEnv->DeleteLocalRef(jstrMime);
    
//    LOGI("set color format");
//    //set color format
//    char key[64];
//    strcpy(key, "color-format");
//    jstring jstr_color_format_key = mEnv->NewStringUTF(key);
//    jint jint_color_format_value = COLOR_FormatYUV420SemiPlanar;
//    mEnv->CallVoidMethod(format, this->set_integer, jstr_color_format_key, jint_color_format_value);
//    mEnv->DeleteLocalRef(jstr_color_format_key);
    
    LOGI("format csd-0-1");
    uint8_t *extraData = mVideoStream->codec->extradata;
    int extraDataSize = mVideoStream->codec->extradata_size;
    
    if (extraData && extraDataSize && extraData[0]==1) {
        int buf_size = extraDataSize + /*FF_INPUT_BUFFER_PADDING_SIZE*/20;
        sps_pps_data = (uint8_t*)malloc(buf_size);
        sps_pps_size = extraDataSize;
        
        convert_sps_pps(extraData, extraDataSize, sps_pps_data, buf_size, &sps_pps_size, &mNal_size);
        
        LOGI("sps_pps_size:%d",sps_pps_size);
        
        int sps_pos = 0;
        int sps_size = 0;
        int pps_pos = 0;
        int pps_size = 0;
        for (int i=0; i<sps_pps_size-4; i++) {
            if (sps_pps_data[i]==0x00 && sps_pps_data[i+1]==0x00 && sps_pps_data[i+2]==0x00 && sps_pps_data[i+3]==0x01) {
                int32_t nalType = sps_pps_data[i+4]&0x1f;
                if (nalType==7) {
                    sps_pos = i;
                }
                if (nalType==8) {
                    pps_pos = i;
                    
                    sps_size = pps_pos - sps_pos;
                    pps_size = sps_pps_size - sps_size;
                    break;
                }
            }
        }
        
        LOGI("sps_size:%d",sps_size);
        LOGI("pps_size:%d",pps_size);
        
        LOGI("sps_pos:%d",sps_pos);
        LOGI("pps_pos:%d",pps_pos);

        LOGI("sps");
        jclass byte_buffer_class = mEnv->FindClass("java/nio/ByteBuffer");
        jobject sps_bytebuf = mEnv->CallStaticObjectMethod(byte_buffer_class, this->allocate_direct, sps_size);
//        mEnv->DeleteLocalRef(byte_buffer_class);

        uint8_t *sps_ptr = (uint8_t*)mEnv->GetDirectBufferAddress(sps_bytebuf);
        memcpy(sps_ptr, sps_pps_data+sps_pos, sps_size);
        
        mEnv->CallObjectMethod(sps_bytebuf, this->limit, sps_size);
        mEnv->CallVoidMethod(format, this->set_bytebuffer, mEnv->NewStringUTF("csd-0"), sps_bytebuf);
        mEnv->DeleteLocalRef(sps_bytebuf);
        
        LOGI("pps");
//        jclass byte_buffer_class = mEnv->FindClass("java/nio/ByteBuffer");
        jobject pps_bytebuf = mEnv->CallStaticObjectMethod(byte_buffer_class, this->allocate_direct, pps_size);
        mEnv->DeleteLocalRef(byte_buffer_class);
        
        uint8_t *pps_ptr = (uint8_t*)mEnv->GetDirectBufferAddress(pps_bytebuf);
        memcpy(pps_ptr, sps_pps_data+pps_pos, pps_size);
        
        mEnv->CallObjectMethod(pps_bytebuf, this->limit, pps_size);
        mEnv->CallVoidMethod(format, this->set_bytebuffer, mEnv->NewStringUTF("csd-1"), pps_bytebuf);
        mEnv->DeleteLocalRef(pps_bytebuf);
        
    }
    
    //configure
    LOGI("configure");
    if (mSurface) {
        mEnv->CallVoidMethod(this->codec, this->configure, format, mSurface, NULL, 0);
    }else{
        mEnv->CallVoidMethod(this->codec, this->configure, format, NULL, NULL, 0);
    }

    if (mEnv->ExceptionOccurred()) {
        LOGI("Exception occurred in MediaCodec.configure");
        mEnv->ExceptionClear();
        
        return false;
    }
    
    mEnv->DeleteLocalRef(format);
    
    //start
    LOGI("start");
    mEnv->CallVoidMethod(this->codec, this->start);
    if (mEnv->ExceptionOccurred()) {
        LOGI("Exception occurred in MediaCodec.start");
        mEnv->ExceptionClear();
        
        return false;
    }
    
    LOGI("Decoder IO");
    // Decoder IO
    this->input_buffers = mEnv->CallObjectMethod(this->codec, this->get_input_buffers);
    this->input_buffers = mEnv->NewGlobalRef(this->input_buffers);
    
    this->output_buffers = mEnv->CallObjectMethod(this->codec, this->get_output_buffers);
    this->output_buffers = mEnv->NewGlobalRef(this->output_buffers);
    
    jclass buffer_info_class = mEnv->FindClass("android/media/MediaCodec$BufferInfo");
    this->buffer_info = mEnv->NewObject(buffer_info_class, this->buffer_info_ctor);
    mEnv->DeleteLocalRef(buffer_info_class);
    this->buffer_info = mEnv->NewGlobalRef(this->buffer_info);
    
    LOGI("MediaCodecDecoder::Open Success");
    
    mFrame = av_frame_alloc();
    mFrame->width = mVideoStream->codec->width;
    mFrame->height = mVideoStream->codec->height;
    mFrame->data[0] = (uint8_t*)malloc(mFrame->width*mFrame->height);
    mFrame->linesize[0] = mFrame->width;
    mFrame->data[1] = (uint8_t*)malloc(mFrame->width*mFrame->height/4);
    mFrame->linesize[1] = mFrame->width/2;
    mFrame->data[2] = (uint8_t*)malloc(mFrame->width*mFrame->height/4);
    mFrame->linesize[2] = mFrame->width/2;
    
    got_picture = false;
    
    mBasePts = -1;
    
    
    //send sps and pps to decoder
    jlong timeoutUs = 10000;
    
    while (true) {
        int index = mEnv->CallIntMethod(this->codec, this->dequeue_input_buffer, timeoutUs);
        if (mEnv->ExceptionOccurred()) {
            LOGI("Exception occurred in MediaCodec.dequeueInputBuffer");
            mEnv->ExceptionClear();
            return false;
        }
        
        if (index<0) {
            // input buffers is full
            LOGI("input buffers is full");
            this->outputFrame();
            continue;
        }
        
        jobject buf = mEnv->GetObjectArrayElement(this->input_buffers, index);
        jlong size = mEnv->GetDirectBufferCapacity(buf);
        void *bufptr = mEnv->GetDirectBufferAddress(buf);
        
        if (size < sps_pps_size) {
            mEnv->DeleteLocalRef(buf);
            return false;
        }
        
        if (size> sps_pps_size) {
            size = sps_pps_size;
        }
        
        memcpy(bufptr, sps_pps_data, sps_pps_size);
        
        mEnv->CallVoidMethod(this->codec, this->queue_input_buffer, index, 0, sps_pps_size, 0, BUFFER_FLAG_CODEC_CONFIG);
        
        mEnv->DeleteLocalRef(buf);
        if (mEnv->ExceptionOccurred()) {
            LOGI("Exception in MediaCodec.queueInputBuffer");
            mEnv->ExceptionClear();
            return false;
        }
        
        break;
    }
    
    return true;
}

void MediaCodecDecoder::dispose()
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    if (this->codec) {
        LOGI("MediaCodec.flush");
        mEnv->CallVoidMethod(this->codec, this->jflush);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.flush");
            mEnv->ExceptionClear();
        }
        
        LOGI("MediaCodec.stop");
        mEnv->CallVoidMethod(this->codec, this->stop);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.stop");
            mEnv->ExceptionClear();
        }
        LOGI("MediaCodec.release");
        mEnv->CallVoidMethod(this->codec, this->release);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.release");
            mEnv->ExceptionClear();
        }
        
        if (this->input_buffers)
            mEnv->DeleteGlobalRef(this->input_buffers);
        if (this->output_buffers) {
            mEnv->DeleteGlobalRef(this->output_buffers);
        }
        if (this->buffer_info)
            mEnv->DeleteGlobalRef(this->buffer_info);
        
        mEnv->DeleteGlobalRef(this->codec);
    }
    
    if (codecName) {
        free(codecName);
        codecName = NULL;
    }
    
//    if(mH264Converter!=NULL)
//    {
//        av_bitstream_filter_close(mH264Converter);
//        mH264Converter = NULL;
//    }
    
    free(mFrame->data[0]);
    free(mFrame->data[1]);
    free(mFrame->data[2]);
    
    av_frame_free(&mFrame);

    got_picture = false;

    if (sps_pps_data!=NULL) {
        free(sps_pps_data);
    }
    
    LOGI("MediaCodecDecoder::Close Success");
}

void MediaCodecDecoder::outputFrame()
{
    jlong timeoutUs = 50000;
    
    while (true) {
        int index = mEnv->CallIntMethod(this->codec, this->dequeue_output_buffer,this->buffer_info, timeoutUs);
        if (mEnv->ExceptionOccurred()) {
            LOGI("Exception in MediaCodec.dequeueOutputBuffer");
            mEnv->ExceptionClear();
            return;
        }
        
        LOGI("ouputbuffer index:%d",index);
        
        if (index>=0) {
            
            LOGI("have output buffer");
            
            if (mSurface) {
                mEnv->CallVoidMethod(this->codec, this->release_output_buffer, index, true);
                if (mEnv->ExceptionOccurred()) {
                    LOGI("Exception in MediaCodec.releaseOutputBuffer");
                    mEnv->ExceptionClear();
                    return;
                }
                mFrame->flags |= AV_FRAME_FLAG_HAVE_RENDER;
                mFrame->pts = (int64_t) mEnv->GetLongField(this->buffer_info, this->pts_field);
                
                LOGI("got picture, have render");
                
                got_picture = true;
                
                break;
            }else{
                jobject buf = mEnv->GetObjectArrayElement((jobjectArray)this->output_buffers, index);
                void *bufptr = mEnv->GetDirectBufferAddress(buf);
                
                jint size = mEnv->GetIntField(this->buffer_info, this->size_field);
                jlong pts = mEnv->GetIntField(this->buffer_info, this->pts_field);
                
                memcpy(mFrame->data[0], bufptr, mFrame->width*mFrame->height);
                memcpy(mFrame->data[1], bufptr+mFrame->width*mFrame->height, mFrame->width*mFrame->height/4);
                memcpy(mFrame->data[2], bufptr+mFrame->width*mFrame->height+mFrame->width*mFrame->height/4, mFrame->width*mFrame->height/4);
                
                mFrame->pts = (int64_t)pts;
                
                mEnv->CallVoidMethod(this->codec, this->release_output_buffer, index, false);
                mEnv->DeleteLocalRef(buf);
                if (mEnv->ExceptionOccurred()) {
                    LOGI("Exception in MediaCodec.releaseOutputBuffer");
                    mEnv->ExceptionClear();
                    return;
                }
                
                LOGI("got picture");
                
                got_picture = true;
                
                break;
            }

        }else if(index==INFO_OUTPUT_BUFFERS_CHANGED)
        {
            LOGI("output buffers changed");
            
            if (this->output_buffers) {
                mEnv->DeleteGlobalRef(this->output_buffers);
            }
            
            this->output_buffers = mEnv->CallObjectMethod(this->codec, this->get_output_buffers);
            this->output_buffers = mEnv->NewGlobalRef(this->output_buffers);
            
            continue;
        }else if(index==INFO_OUTPUT_FORMAT_CHANGED)
        {
            LOGI("output format changed");
            continue;
        }else
        {
            LOGI("no output buffer");
            got_picture = false;
            break;
        }
    }
}

int MediaCodecDecoder::decode(AVPacket* videoPacket)
{
    
//    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    jlong timeoutUs = 10000;
    
    while (true) {
        int index = mEnv->CallIntMethod(this->codec, this->dequeue_input_buffer, timeoutUs);
        if (mEnv->ExceptionOccurred()) {
            LOGI("Exception occurred in MediaCodec.dequeueInputBuffer");
            mEnv->ExceptionClear();
            return -1;
        }
        
        if (index<0) {
            // input buffers is full
            LOGI("input buffers is full");
            this->outputFrame();
            continue;
        }
        
        LOGI("inputbuffer index:%d",index);
        
        jobject buf = mEnv->GetObjectArrayElement(this->input_buffers, index);
        jlong size = mEnv->GetDirectBufferCapacity(buf);
        void *bufptr = mEnv->GetDirectBufferAddress(buf);

        if (size < videoPacket->size) {
            mEnv->DeleteLocalRef(buf);
            return -1;
        }
            
        if (size> videoPacket->size) {
            size = videoPacket->size;
        }

        memcpy(bufptr, videoPacket->data, videoPacket->size);

        struct H264ConvertState convert_state = { 0, 0 };
        convert_h264_to_annexb(bufptr, size, mNal_size, &convert_state);

//        for (int i=0; i<10; i++) {
//            LOGI("%0x",((uint8_t*)bufptr)[i]);
//        }
        
        videoPacket->pts = videoPacket->pts * AV_TIME_BASE * av_q2d(mVideoStream->time_base);

        LOGI("size:%lld",size);
        
        if (mBasePts==-1) {
            mBasePts = videoPacket->pts;
        }
        
        LOGI("pts:%lld",videoPacket->pts-mBasePts);
        
        if (videoPacket->flags & AV_PKT_FLAG_KEY) {
            mEnv->CallVoidMethod(this->codec, this->queue_input_buffer, index, 0, size, videoPacket->pts-mBasePts, BUFFER_FLAG_KEY_FRAME);
        }else{
            mEnv->CallVoidMethod(this->codec, this->queue_input_buffer, index, 0, size, videoPacket->pts-mBasePts, 0);
        }
        
        mEnv->DeleteLocalRef(buf);
        if (mEnv->ExceptionOccurred()) {
            LOGI("Exception in MediaCodec.queueInputBuffer");
            mEnv->ExceptionClear();
            return -1;
        }
        
        return videoPacket->size;
    }
}

AVFrame* MediaCodecDecoder::getFrame()
{
    if (!got_picture) {
        this->outputFrame();
    }
    
    if (got_picture) {
        got_picture = 0;

        return mFrame;
    }else {
        return NULL;
    }
}

void MediaCodecDecoder::clearFrame()
{

}

void MediaCodecDecoder::flush()
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    LOGI("MediaCodec.flush");
    mEnv->CallVoidMethod(this->codec, this->jflush);
    if (mEnv->ExceptionOccurred()) {
        LOGE("Exception in MediaCodec.flush");
        mEnv->ExceptionClear();
    }
}

void MediaCodecDecoder::setDropState(bool bDrop)
{
}
