//
//  MediaCodecDecoder.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__MediaCodecDecoder__
#define __MediaPlayer__MediaCodecDecoder__

#include <stdio.h>

#include "VideoDecoder.h"
#include "jni.h"

#define INFO_OUTPUT_BUFFERS_CHANGED -3
#define INFO_OUTPUT_FORMAT_CHANGED  -2
#define INFO_TRY_AGAIN_LATER        -1

#define COLOR_FormatYUV420Planar 19
#define COLOR_FormatYUV420PackedPlanar 20
#define COLOR_FormatYUV420SemiPlanar 21

#define AV_FRAME_FLAG_HAVE_RENDER 0x0004 ///< have render

#define BUFFER_FLAG_CODEC_CONFIG 2
#define BUFFER_FLAG_KEY_FRAME 1

class MediaCodecDecoder : public VideoDecoder
{
public:
    MediaCodecDecoder(JavaVM *jvm, void *surface);
    ~MediaCodecDecoder();

    bool open(AVStream* videoStreamContext, bool isRealTime = false);

    void dispose();
    
    int decode(AVPacket* videoPacket);

    AVFrame* getFrame();
    
    void clearFrame();
    
    void flush();
    
    void setDropState(bool bDrop);
private:
    AVFrame *mFrame;
    
    JavaVM* mJvm;
    JNIEnv* mEnv;
    
    jobject mSurface;
    
    jmethodID tostring;
    jmethodID get_codec_count, get_codec_info_at, is_encoder, get_capabilities_for_type;
    jfieldID profile_levels_field, profile_field, level_field;
    jmethodID get_supported_types, get_name;
    jmethodID create_by_codec_name, configure, start, stop, jflush, release;
    jmethodID get_output_format, get_input_buffers, get_output_buffers;
    jmethodID dequeue_input_buffer, dequeue_output_buffer, queue_input_buffer;
    jmethodID release_output_buffer;
    jmethodID create_video_format, set_integer, set_bytebuffer, get_integer;
    jmethodID buffer_info_ctor;
    jmethodID allocate_direct, limit;
    jfieldID size_field, offset_field, pts_field, flags_field;
    
    char mime[64];
    
    char *codecName;
    
    jobject codec;
    jobject input_buffers, output_buffers;
    jobject buffer_info;
    
//    AVBitStreamFilterContext *mH264Converter;
    
    bool got_picture;
    
    AVStream* mVideoStream;
    
    int mNal_size;
    
    uint8_t* sps_pps_data;
    int sps_pps_size;
    
    int64_t mBasePts;

private:
    void outputFrame();
};

#endif /* defined(__MediaPlayer__MediaCodecDecoder__) */
