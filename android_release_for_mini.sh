#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

cd android_mini
./release_for_mini.sh
cd ../../

if [ -d "android_release_for_mini" ]; then
rm -rf android_release_for_mini
fi

mkdir android_release_for_mini
cd android_release_for_mini
mkdir Android_MediaStreamer
cd ..
cp -r MediaStreamer/android_mini/MediaStreamer/sdk/jars/mediastreamer.jar android_release_for_mini/Android_MediaStreamer
cp -r MediaStreamer/android_mini/MediaStreamer/sdk/libs/armeabi-v7a android_release_for_mini/Android_MediaStreamer
cp -r MediaStreamer/android_mini/MediaStreamer/sdk/libs/arm64-v8a android_release_for_mini/Android_MediaStreamer
#cp -r MediaStreamer/android_mini/MediaStreamer/sdk/libs/x86 android_release_for_mini/Android_MediaStreamer

# copy to MediaStreamerDemoStudioForYPP
cp -rf MediaStreamer/android_mini/MediaStreamer/sdk/jars/mediastreamer.jar MediaStreamer/android_mini/MediaStreamerDemoStudioForYPP/mediastreamer/libs
cp -rf MediaStreamer/android_mini/MediaStreamer/sdk/libs/armeabi-v7a MediaStreamer/android_mini/MediaStreamerDemoStudioForYPP/mediastreamer/src/main/jniLibs
cp -rf MediaStreamer/android_mini/MediaStreamer/sdk/libs/arm64-v8a MediaStreamer/android_mini/MediaStreamerDemoStudioForYPP/mediastreamer/src/main/jniLibs
cp -rf MediaStreamer/android_mini/MediaStreamer/sdk/libs/armeabi-v7a/libMediaStreamer.so MediaStreamer/android_mini/MediaStreamerDemoStudioForYPP/mediastreamer/src/main/jniLibs/armeabi
rm -rf MediaStreamer/android_mini/MediaStreamerDemoStudioForYPP/mediastreamer/src/main/jniLibs/armeabi-v7a/libffmpeg_ypp.so
rm -rf MediaStreamer/android_mini/MediaStreamerDemoStudioForYPP/mediastreamer/src/main/jniLibs/arm64-v8a/libffmpeg_ypp.so

cd MediaStreamer
