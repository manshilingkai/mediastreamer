//
//  MediaFile.h
//  MediaPlayer
//
//  Created by Think on 2017/10/19.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef MediaFile_h
#define MediaFile_h

#include <stdio.h>

#ifdef WIN32
#include "w32common.h"
#endif

class MediaFile {
public:
    static bool isExist(const char *filePath);
    static bool deleteFile(const char *filePath);
    static long long getFileSizeWithStat(const char *filePath);
    static long long getFileSizeWithFtell(const char *filePath);
    static long writeDataToDisk(const char *filePath, bool isTxt, u_int8_t *buffer, long size);
    static bool readDataFromDisk(const char *filePath, bool isTxt, u_int8_t **ppBuffer, long* pSize);
    
    static bool backDeleteFile(const char *filePath, long keepSize);
    static bool reverseOrderGeneration(const char *filePath, int numChannels);
    
    MediaFile();
    ~MediaFile();
    
    bool open(const char *filePath, const char *mode);
    void close();
    
    long write(u_int8_t *buffer, long size);
    long read(u_int8_t *buffer, long size);
    
    bool seek(long offset, int fromwhere);
    
    void flush();
private:
    FILE *mStream;
};

#endif /* MediaFile_h */
