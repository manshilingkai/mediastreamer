//
//  MediaDir.h
//  MediaPlayer
//
//  Created by Think on 2017/10/20.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef MediaDir_h
#define MediaDir_h

#include <stdio.h>

class MediaDir {
public:
    static bool isExist(const char *dirPath);
    static bool createDir(const char *dirPath);
    static bool deleteDir(const char *dirPath);
    
    static long long getDirSize(const char *dirPath);
};

#endif /* MediaDir_h */
