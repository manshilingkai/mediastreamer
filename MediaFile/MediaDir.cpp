//
//  MediaDir.cpp
//  MediaPlayer
//
//  Created by Think on 2017/10/20.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "MediaDir.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>

#ifdef WIN32
#include "w32unistd.h"
#include "w32dirent.h"
#include "w32common.h"
#else
#include <unistd.h>
#include <dirent.h>
#endif

#include <string.h>
#include "MediaLog.h"

#include "MediaFile.h"

#define PATH_MAX 1024

bool MediaDir::isExist(const char *dirPath)
{
    if (access(dirPath, F_OK)) return false;
    else return true;
}

bool MediaDir::createDir(const char *dirPath)
{
    if (dirPath==NULL) return false;
    
    if (access(dirPath, F_OK)==0) return true;
    
    int ret = mkdir(dirPath, S_IRWXU);
    
    if (ret) return false;
    else return true;
}

bool MediaDir::deleteDir(const char *dirPath)
{
    if (dirPath==NULL) return false;
    
    DIR *dir;
    struct dirent *entry;
    char path[PATH_MAX];
    
    dir = opendir(dirPath);
    if (dir == NULL) {
        LOGE("opendir %s failed", dirPath);
        return false;
    }
    
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) {
            snprintf(path, (size_t) PATH_MAX, "%s/%s", dirPath, entry->d_name);
            if (entry->d_type == DT_DIR) {
                deleteDir(path);
            } else {
                // delete file
                unlink(path);
            }
        }
    }
    closedir(dir);
    
    // now we can delete the empty dir
    int ret = rmdir(dirPath);
    if (ret) return false;
    else return true;
}

long long MediaDir::getDirSize(const char *dirPath)
{
    if (dirPath==NULL) return 0;
    
    DIR *dir;
    struct dirent *entry;
    char path[PATH_MAX];
    
    dir = opendir(dirPath);
    if (dir == NULL) {
        LOGE("opendir %s failed", dirPath);
        return 0;
    }
    
    long long size = 0;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) {
            snprintf(path, (size_t) PATH_MAX, "%s/%s", dirPath, entry->d_name);
            if (entry->d_type == DT_DIR) {
                size += getDirSize(path);
            } else {
                size += MediaFile::getFileSizeWithStat(path);
            }
        }
    }
    closedir(dir);
    
    return size;
}


