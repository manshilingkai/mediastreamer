//
//  MultiSourceMediaStreamer.h
//  MediaStreamer
//
//  Created by Think on 2018/8/19.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef MultiSourceMediaStreamer_h
#define MultiSourceMediaStreamer_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#include <sys/resource.h>
#endif

#include "TimedEventQueue.h"

#include "MediaStreamer.h"
#include "IMediaListener.h"

#include "MediaDataType.h"
#include "ColorSpaceConvert.h"
#include "VideoEncoder.h"
#include "VideoFrameBufferPool.h"
#include "MediaMuxer.h"
#include "AudioStreamer.h"

#include "NotificationQueue.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include <map>

using namespace std;

class MultiSourceMediaStreamer : public MediaStreamer, IMediaListener
{
public:
#ifdef ANDROID
    MultiSourceMediaStreamer(JavaVM *jvm, const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions);
#else
    MultiSourceMediaStreamer(const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions);
#endif
    ~MultiSourceMediaStreamer();
    
#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#endif
    
    void setListener(void (*listener)(void*,int,int,int), void* arg);
    
    void updateVideoSourceLayout(map<int, VideoSourceLayout*> *layoutDict);
    
    void inputVideoFrame(VideoFrame *inVideoFrame) {}
    void inputYUVVideoFrame(YUVVideoFrame* inVideoFrame) {}
    void inputYUVVideoFrame(YUVVideoFrame* inVideoFrame, int sourceId);

    void inputAudioFrame(AudioFrame *inAudioFrame);
    
    void inputText(char* text, int size);
    
    void start();
    
    void resume();
    void pause();
    
    void stop(bool isCancle = false);
    
    void notify(int event, int ext1, int ext2);
    
    void enableAudio(bool isEnable);
    
#ifdef IOS
    void openVideoChannel();
    void closeVideoChannel();
    
    void setVideoEncodeType(int videoEncodeType);
#endif
    
private:
    // return -1 : Open VideoEncoder Fail
    // return -2 : Open FFmpegMuxer  Fail
    // return -3 : Open AudioCapture Fail
    // return -4 : Open AudioEncoder Fail
    // return -5 : Open AudioFilter  Fail
    // return  0 : Open All Pipelines Success
    int open_all_pipelines_l();
    
    void close_all_pipelines_l();
    
    // return  0 : VideoFrameBufferPool is empty
    // return -1 : ColorSpaceConvert Fail
    // return -2 : Video Encode Fail
    // return  1 : Flowing Success
    int flowing_l();
    
    bool isNeedConvertVideoFrame(VideoFrame *sourceFrame, VideoFrame *targetFrame);
    
    VideoFrame* mergeVideoSource_l();
    
private:
    friend struct MultiSourceMediaStreamerEvent;
    
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    MUXER_FORMAT mMuxerFormat;
    VideoOptions *mVideoOptions;
    AudioOptions *mAudioOptions;
    char* mPublishUrl;
    
    pthread_mutex_t mLock;
    
    VideoFrame mTargetVideoFrame;
    VideoPacket mVideoPacket;
    
    IMediaListener* mMediaListener;
    
    pthread_mutex_t mVideoFrameBufferPoolLock;
    bool canInputVideoFrame;
    map<int, VideoFrameBufferPool*> mVideoFrameBufferPoolDict;
    map<int, VideoSourceLayout*> mLayoutDict;
    
    ColorSpaceConvert *mColorSpaceConvert;
    VideoEncoder *mVideoEncoder;
    MediaMuxer *mMediaMuxer; // critical value
    AudioStreamer *mAudioStreamer; // critical value
    
    VIDEO_ENCODER_TYPE mVideoEncoderType;
    
    enum {
        CONNECTING           = 0x01,
        CONNECTED            = 0x02,
        STREAMING            = 0x04,
        PAUSED               = 0x10,
        ERRORED                = 0x20,
        END                  = 0x40,
        INITIALIZED          = 0x80,
        ENDING               = 0x100,
    };
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
    
    TimedEventQueue mQueue;
    NotificationQueue mNotificationQueue;
    
    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mVideoEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mControlBitrateEvent;
    TimedEventQueue::Event *mNotifyEvent;
    bool mVideoEventPending;
    
    MultiSourceMediaStreamer(const MultiSourceMediaStreamer &);
    MultiSourceMediaStreamer &operator=(const MultiSourceMediaStreamer &);
    
    void prepareAsync_l();
    void play_l();
    void pause_l();
    void stop_l();
    
    void postVideoEvent_l(int64_t delayUs = -1);
    void postControlBitrateEvent_l();
    void postNotifyEvent_l();
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
    
    void onPrepareAsyncEvent();
    void onVideoEvent();
    void onStopEvent();
    void onControlBitrateEvent();
    void onNotifyEvent();
    
    void cancelStreamerEvents();
    
    int64_t mVideoEventTimer_StartTime;
    int64_t mVideoEventTimer_EndTime;
    int64_t mVideoEventTime;
    int64_t mVideoDelayTimeUs;
    
    int mTargetFps;
    
    int mPublishDelayTime;
    
    int mTargetVideoBitrate;
    
    pthread_cond_t mStopCondition;
    
    bool isAudioEnabled;
    
    //for fps statistics
    int mRealFps;
    int flowingFrameCountPerTime;
    int64_t realfps_begin_time;
    
private:
    pthread_mutex_t mMuxerLock;
    pthread_mutex_t mAudioStreamerLock;
private:
#ifdef IOS
    bool isNeedSwitchVideoEncoder;
#endif
private:
    bool gotError;
    
private:
    VideoFrame mMergeVideoFrame;
    
private:
    char* mBackupDir;
    MediaLog* mMediaLog;
};


#endif /* MultiSourceMediaStreamer_h */
