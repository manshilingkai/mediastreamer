//
//  AnimatedImageMediaStreamer.h
//  MediaStreamer
//
//  Created by Think on 2018/9/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef AnimatedImageMediaStreamer_h
#define AnimatedImageMediaStreamer_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#include <sys/resource.h>
#endif

#include "TimedEventQueue.h"

#include "MediaStreamer.h"
#include "IMediaListener.h"

#include "MediaDataType.h"
#include "ColorSpaceConvert.h"
#include "VideoFrameBufferPool.h"

#include "NotificationQueue.h"

#include "AnimatedGifCreater.h"

#include "MediaMuxer.h"

#ifdef ANDROID
#include "jni.h"
#endif

class AnimatedImageMediaStreamer : public MediaStreamer, IMediaListener
{
public:
#ifdef ANDROID
    AnimatedImageMediaStreamer(JavaVM *jvm, const char* publishUrl, VideoOptions videoOptions);
#else
    AnimatedImageMediaStreamer(const char* publishUrl, VideoOptions videoOptions);
#endif
    ~AnimatedImageMediaStreamer();
    
#ifdef ANDROID
    void setEncodeSurfaceCore(void *encodeSurfaceCore) {}
    void setEncodeSurface(void *surface) {}
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#endif
    
    void setListener(void (*listener)(void*,int,int,int), void* arg);
    
    void updateVideoSourceLayout(map<int, VideoSourceLayout*> *layoutDict) {}
    void inputYUVVideoFrame(YUVVideoFrame* inVideoFrame, int sourceId) {}

    bool inputVideoFrame(VideoFrame *inVideoFrame);
    void inputYUVVideoFrame(YUVVideoFrame* inVideoFrame);
    
    void inputAudioFrame(AudioFrame *inAudioFrame) {}
    void inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType) {}
    
    void inputText(char* text, int size) {}
    
    void start();
    
    void resume();
    void pause();
    
    void stop(bool isCancle = false);
    
    void notify(int event, int ext1, int ext2);
    
    void enableAudio(bool isEnable) {}
    
    void mixBGM(int bgmId, char* bgmUrl, float volume, int numberOfLoops) {}
    void playBGM() {}
    void pauseBGM() {}
    void stopBGM() {}
    void setBGMVolume(float volume) {}
    void mixSEM(int semId, char* semUrl, float volume, int numberOfLoops) {}
    
    void netWorkReachabilityNotify(int networkStatus) {}

#ifdef IOS
    void iOSAVAudioSessionInterruption(bool isInterrupting) {}
    
    void iOSDidEnterBackground() {}
    void iOSDidBecomeActive() {}
#endif
    
private:
    // return -1 : Open VideoEncoder Fail
    // return -2 : Open FFmpegMuxer  Fail
    // return -3 : Open AudioCapture Fail
    // return -4 : Open AudioEncoder Fail
    // return -5 : Open AudioFilter  Fail
    // return  0 : Open All Pipelines Success
    int open_all_pipelines_l();
    
    void close_all_pipelines_l();
    
    // return  0 : VideoFrameBufferPool is empty
    // return -1 : ColorSpaceConvert Fail
    // return -2 : Video Encode Fail
    // return  1 : Flowing Success
    int flowing_l();
    
    bool isNeedConvertVideoFrame(VideoFrame *sourceFrame, VideoFrame *targetFrame);
    
private:
    friend struct AnimatedImageMediaStreamerEvent;
    
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    MUXER_FORMAT mMuxerFormat;
    VideoOptions *mVideoOptions;
    char* mPublishUrl;
    
    pthread_mutex_t mLock;
    
    VideoFrame mTargetVideoFrame;
    
    IMediaListener* mMediaListener;
    
    pthread_mutex_t mVideoFrameBufferPoolLock;
    bool canInputVideoFrame;
    VideoFrameBufferPool *mVideoFrameBufferPool; // critical value
    
    ColorSpaceConvert *mColorSpaceConvert;
    
    enum {
        CONNECTING           = 0x01,
        CONNECTED            = 0x02,
        STREAMING            = 0x04,
        PAUSED               = 0x10,
        ERRORED                = 0x20,
        END                  = 0x40,
        INITIALIZED          = 0x80,
        ENDING               = 0x100,
    };
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
    
    TimedEventQueue mQueue;
    NotificationQueue mNotificationQueue;
    
    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mVideoEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mNotifyEvent;
    bool mVideoEventPending;
    
    AnimatedImageMediaStreamer(const AnimatedImageMediaStreamer &);
    AnimatedImageMediaStreamer &operator=(const AnimatedImageMediaStreamer &);
    
    void prepareAsync_l();
    void play_l();
    void pause_l();
    void stop_l();
    
    void postVideoEvent_l(int64_t delayUs = -1);
    void postNotifyEvent_l();
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
    
    void onPrepareAsyncEvent();
    void onVideoEvent();
    void onStopEvent();
    void onNotifyEvent();
    
    void cancelStreamerEvents();
    
    int64_t mVideoEventTimer_StartTime;
    int64_t mVideoEventTimer_EndTime;
    int64_t mVideoEventTime;
    int64_t mVideoDelayTimeUs;
    
    int mTargetFps;
        
    int mTargetVideoBitrate;
    
    pthread_cond_t mStopCondition;
    
    //for fps statistics
    int mRealFps;
    int flowingFrameCountPerTime;
    int64_t realfps_begin_time;
private:
    bool gotError;
    
private:
    AnimatedGifCreater* mAnimatedGifCreater;
    AnimatedGifCreaterType mAnimatedGifCreaterType;
    
private:
    int64_t mStartTimeMs;
    bool isGotFirstFrame;
    int64_t mLastSendRecordTimeMs;
};


#endif /* AnimatedImageMediaStreamer_h */
