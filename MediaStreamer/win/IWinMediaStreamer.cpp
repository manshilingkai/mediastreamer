#include <windows.h>
#include "IWinMediaStreamer.h"
#include "WinMediaStreamer.h"

BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

MEDIASTREAMER_API IWinMediaStreamer* APIENTRY CreateWinMediaStreamerInstance()
{
	return new WinMediaStreamer;
}

MEDIASTREAMER_API void APIENTRY DestroyWinMediaStreamerInstance(IWinMediaStreamer** ppInstance)
{
	if (ppInstance)
	{
		IWinMediaStreamer* pInstance = *ppInstance;
		if (pInstance)
		{
			pInstance->Release();
			pInstance = NULL;
		}
	}
}

MEDIASTREAMER_API IWinVideoCapturer* APIENTRY CreateWinVideoCapturerInstance()
{
	return new WinVideoCapturer;
}

MEDIASTREAMER_API void APIENTRY DestroyWinVideoCapturerInstance(IWinVideoCapturer** ppInstance)
{
	if (ppInstance)
	{
		IWinVideoCapturer* pInstance = *ppInstance;
		if (pInstance)
		{
			pInstance->Release();
			pInstance = NULL;
		}
	}
}