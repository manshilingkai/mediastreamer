#include "IWinMediaStreamer.h"

#include "VideoCapture.h"
#include "MediaStreamer.h"

#include "NotificationQueue.h"

class WinMediaStreamer : public IWinMediaStreamer
{
public:
	WinMediaStreamer();
	~WinMediaStreamer();

	virtual void initialize(const char* publishUrl, WinVideoOptions videoOptions, WinAudioOptions audioOptions, WinMediaStreamerType type = WIN_MEDIA_STREAMER_SLK, const char *backupDir = NULL);

	virtual void setListener(void(*listener)(void*, int, int, int), void* arg);
	virtual void setListener(IWinMediaStreamerListener *listener);

	virtual void inputVideoFrame(WinVideoFrame *inVideoFrame);
	virtual void inputYUVVideoFrame(WinYUVVideoFrame* inVideoFrame);
	virtual void inputAudioFrame(WinAudioFrame *inAudioFrame);
	virtual void inputFloatAudioFrame(float *data, int count, float duration, uint64_t pts, int sampleRate, int channels, int bitsPerChannel, bool isBigEndian);
	virtual void inputText(char* text, int size);

	virtual void start();

	virtual void resume();
	virtual void pause();

	virtual void stop(bool isCancle = false);

	virtual void enableAudio(bool isEnable);

	virtual void terminate();

	virtual void Release();

	virtual void* getCore();

	void HandleWinMediaStreamerNotificationListener(int event, int ext1, int ext2);

private:
	MediaStreamer* mMediaStreamer;
	WinMediaStreamerType mType;
private:
	pthread_t mThread;
	pthread_cond_t mCondition;
	pthread_mutex_t mLock;
	bool isBreakThread;

	void createNotificationListenerThread();
	static void* handleNotificationListenerThread(void* ptr);
	void NotificationListenerMain();
	void deleteNotificationListenerThread();

	NotificationQueue mNotificationQueue;

	pthread_mutex_t mListenerLock;
	void(*mListener)(void*, int, int, int);
	void* mOwner;
	IWinMediaStreamerListener *mIWinMediaStreamerListener;
};

class WinVideoCapturer : public IWinVideoCapturer
{
public:
	WinVideoCapturer();
	~WinVideoCapturer();

	virtual void initialize(WIN_VIDEO_CAPTURE_TYPE type, int videoWidth, int videoHeight, int videoFps);

	virtual void StartRecording();
	virtual void StopRecording();

	virtual void linkPreview(void* display);
	virtual void unlinkPreview();
	virtual void onPreviewResize(int w, int h);

	virtual void linkMediaStreamer(IWinMediaStreamer* winMediaStreamer);
	virtual void unlinkMediaStreamer();

	virtual void terminate();

	virtual void Release();
private:
	VideoCapture *mVideoCapture;
	WIN_VIDEO_CAPTURE_TYPE mType;
};