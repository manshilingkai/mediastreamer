#include "WinMediaStreamer.h"
#include <stdio.h>
#include "MediaLog.h"

#include "AudioSampleFormatConvert.h"

WinMediaStreamer::WinMediaStreamer()
{
	LOGD("WinMediaStreamer::WinMediaStreamer \n");

	mMediaStreamer = NULL;

	pthread_mutex_init(&mListenerLock, NULL);
	mListener = NULL;
	mOwner = NULL;
	mIWinMediaStreamerListener = NULL;
}

WinMediaStreamer::~WinMediaStreamer()
{
	LOGD("WinMediaStreamer::~WinMediaStreamer \n");

	this->terminate();

	pthread_mutex_destroy(&mListenerLock);
}

void WinMediaStreamerNotificationListener(void* owner, int event, int ext1, int ext2)
{
	WinMediaStreamer* thiz = (WinMediaStreamer*)owner;
	if (thiz)
	{
		thiz->HandleWinMediaStreamerNotificationListener(event, ext1, ext2);
	}
}

void WinMediaStreamer::HandleWinMediaStreamerNotificationListener(int event, int ext1, int ext2)
{
	Notification *notification = new Notification();
	notification->event = event;
	notification->ext1 = ext1;
	notification->ext2 = ext2;
	mNotificationQueue.push(notification);

	pthread_cond_signal(&mCondition);
}


void WinMediaStreamer::initialize(const char* publishUrl, WinVideoOptions videoOptions, WinAudioOptions audioOptions, WinMediaStreamerType type, const char *backupDir)
{
	LOGD("WinMediaStreamer::initialize \n");

	isBreakThread = false;
	pthread_cond_init(&mCondition, NULL);
	pthread_mutex_init(&mLock, NULL);
	createNotificationListenerThread();

	VideoOptions coreVideoOptions;
	coreVideoOptions.hasVideo = videoOptions.hasVideo;
	coreVideoOptions.videoEncodeType = videoOptions.videoEncodeType;

	coreVideoOptions.videoWidth = videoOptions.videoWidth;
	coreVideoOptions.videoHeight = videoOptions.videoHeight;
	coreVideoOptions.videoFps = videoOptions.videoFps;
	coreVideoOptions.videoRawType = videoOptions.videoRawType;

	coreVideoOptions.videoProfile = videoOptions.videoProfile;
	coreVideoOptions.videoBitRate = videoOptions.videoBitRate;
	coreVideoOptions.encodeMode = videoOptions.encodeMode;
	coreVideoOptions.maxKeyFrameIntervalMs = videoOptions.maxKeyFrameIntervalMs;

	coreVideoOptions.networkAdaptiveMinFps = videoOptions.networkAdaptiveMinFps;
	coreVideoOptions.networkAdaptiveMinVideoBitrate = videoOptions.networkAdaptiveMinVideoBitrate;
	coreVideoOptions.isEnableNetworkAdaptive = videoOptions.isEnableNetworkAdaptive;

	coreVideoOptions.quality = videoOptions.quality;
	coreVideoOptions.bStrictCBR = videoOptions.bStrictCBR;
	coreVideoOptions.deblockingFilterFactor = videoOptions.deblockingFilterFactor;

	coreVideoOptions.rotation = videoOptions.rotation;

	AudioOptions coreAudioOptions;
	coreAudioOptions.hasAudio = audioOptions.hasAudio;

	coreAudioOptions.audioSampleRate = audioOptions.audioSampleRate;
	coreAudioOptions.audioNumChannels = audioOptions.audioNumChannels;
	coreAudioOptions.audioBitRate = audioOptions.audioBitRate;

	coreAudioOptions.isRealTime = audioOptions.isRealTime;
	coreAudioOptions.isExternalAudioInput = audioOptions.isExternalAudioInput;

	mType = type;
	mMediaStreamer = MediaStreamer::CreateMediaStreamer((MediaStreamerType)type, publishUrl, backupDir, coreVideoOptions, coreAudioOptions, NULL, 3);
	mMediaStreamer->setListener(WinMediaStreamerNotificationListener, this);
}

void WinMediaStreamer::setListener(void(*listener)(void*, int, int, int), void* arg)
{
	LOGD("WinMediaStreamer::setListener \n");

	pthread_mutex_lock(&mListenerLock);
	mListener = listener;
	mOwner = arg;
	mIWinMediaStreamerListener = NULL;
	pthread_mutex_unlock(&mListenerLock);
}

void WinMediaStreamer::setListener(IWinMediaStreamerListener *listener)
{
	LOGD("WinMediaStreamer::setListener \n");

	pthread_mutex_lock(&mListenerLock);
	mListener = NULL;
	mOwner = NULL;
	mIWinMediaStreamerListener = listener;
	pthread_mutex_unlock(&mListenerLock);
}

void WinMediaStreamer::inputVideoFrame(WinVideoFrame *inVideoFrame)
{
	LOGD("WinMediaStreamer::inputVideoFrame \n");
	
	if (mMediaStreamer)
	{
		VideoFrame coreVideoFrame;
		coreVideoFrame.data = inVideoFrame->data;
		coreVideoFrame.frameSize = inVideoFrame->frameSize;
		coreVideoFrame.width = inVideoFrame->width;
		coreVideoFrame.height = inVideoFrame->height;
		coreVideoFrame.pts = inVideoFrame->pts;
		coreVideoFrame.duration = inVideoFrame->duration;
		coreVideoFrame.rotation = inVideoFrame->rotation;
		coreVideoFrame.videoRawType = inVideoFrame->videoRawType;
		coreVideoFrame.isLastVideoFrame = inVideoFrame->isLastVideoFrame;
		coreVideoFrame.isKeepFullContent = inVideoFrame->isKeepFullContent;

		mMediaStreamer->inputVideoFrame(&coreVideoFrame);
	}
}

void WinMediaStreamer::inputYUVVideoFrame(WinYUVVideoFrame* inVideoFrame)
{
	LOGD("WinMediaStreamer::inputYUVVideoFrame \n");

	if (mMediaStreamer)
	{
		YUVVideoFrame coreVideoFrame;
		for (size_t i = 0; i < YUV_NUM_DATA_POINTERS; i++)
		{
			coreVideoFrame.data[i] = inVideoFrame->data[i];
			coreVideoFrame.linesize[i] = inVideoFrame->linesize[i];
		}
		coreVideoFrame.planes = inVideoFrame->planes;
		coreVideoFrame.width = inVideoFrame->width;
		coreVideoFrame.height = inVideoFrame->height;
		coreVideoFrame.pts = inVideoFrame->pts;
		coreVideoFrame.duration = inVideoFrame->duration;
		coreVideoFrame.rotation = inVideoFrame->rotation;
		coreVideoFrame.videoRawType = inVideoFrame->videoRawType;

		mMediaStreamer->inputYUVVideoFrame(&coreVideoFrame);
	}
}

void WinMediaStreamer::inputAudioFrame(WinAudioFrame *inAudioFrame)
{
	LOGD("WinMediaStreamer::inputAudioFrame \n");

	if (mMediaStreamer)
	{
		AudioFrame coreAudioFrame;
		coreAudioFrame.data = inAudioFrame->data;
		coreAudioFrame.frameSize = inAudioFrame->frameSize;
		coreAudioFrame.duration = inAudioFrame->duration;
		coreAudioFrame.pts = inAudioFrame->pts;
		coreAudioFrame.sampleRate = inAudioFrame->sampleRate;
		coreAudioFrame.channels = inAudioFrame->channels;
		coreAudioFrame.bitsPerChannel = inAudioFrame->bitsPerChannel;
		coreAudioFrame.isBigEndian = inAudioFrame->isBigEndian;

		mMediaStreamer->inputAudioFrame(&coreAudioFrame);
	}
}

void WinMediaStreamer::inputFloatAudioFrame(float *data, int count, float duration, uint64_t pts, int sampleRate, int channels, int bitsPerChannel, bool isBigEndian)
{
	LOGD("WinMediaStreamer::inputFloatAudioFrame \n");

	if (data == NULL || count <= 0) return;

	if (mMediaStreamer)
	{
		short *out = new short[count];
		src_float_to_short_array(data, out, count);

		AudioFrame coreAudioFrame;
		coreAudioFrame.data = (uint8_t*)out;
		coreAudioFrame.frameSize = count *2;
		coreAudioFrame.duration = duration;
		coreAudioFrame.pts = pts;
		coreAudioFrame.sampleRate = sampleRate;
		coreAudioFrame.channels = channels;
		coreAudioFrame.bitsPerChannel = bitsPerChannel;
		coreAudioFrame.isBigEndian = isBigEndian;

		mMediaStreamer->inputAudioFrame(&coreAudioFrame);

		delete []out;
	}
}

void WinMediaStreamer::inputText(char* text, int size)
{
	LOGD("WinMediaStreamer::inputText \n");

	if (mMediaStreamer)
	{
		mMediaStreamer->inputText(text, size);
	}
}

void WinMediaStreamer::start()
{
	LOGD("WinMediaStreamer::start \n");

	if (mMediaStreamer)
	{
		mMediaStreamer->start();
	}
}

void WinMediaStreamer::resume()
{
	LOGD("WinMediaStreamer::resume \n");

	if (mMediaStreamer)
	{
		mMediaStreamer->resume();
	}
}

void WinMediaStreamer::pause()
{
	LOGD("WinMediaStreamer::pause \n");

	if (mMediaStreamer)
	{
		mMediaStreamer->pause();
	}
}

void WinMediaStreamer::stop(bool isCancle)
{
	LOGD("WinMediaStreamer::stop \n");

	if (mMediaStreamer)
	{
		mMediaStreamer->stop(isCancle);
	}
}

void WinMediaStreamer::enableAudio(bool isEnable)
{
	LOGD("WinMediaStreamer::enableAudio \n");

	if (mMediaStreamer)
	{
		mMediaStreamer->enableAudio(isEnable);
	}
}

void WinMediaStreamer::terminate()
{
	LOGD("WinMediaStreamer::terminate \n");

	if (mMediaStreamer)
	{
		MediaStreamer::DeleteMediaStreamer(mMediaStreamer, (MediaStreamerType)mType);
		mMediaStreamer = NULL;
	}

	deleteNotificationListenerThread();
	pthread_cond_destroy(&mCondition);
	pthread_mutex_destroy(&mLock);
	isBreakThread = false;
}

void WinMediaStreamer::Release()
{
	LOGD("WinMediaStreamer::Release \n");
	delete this;
}

void* WinMediaStreamer::getCore()
{
	LOGD("WinMediaStreamer::getCore \n");

	return (void*)mMediaStreamer;
}

void WinMediaStreamer::createNotificationListenerThread()
{
#ifndef WIN32
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&mThread, &attr, handleNotificationListenerThread, this);
	pthread_attr_destroy(&attr);
#else
	pthread_create(&mThread, NULL, handleNotificationListenerThread, this);
#endif
}

void* WinMediaStreamer::handleNotificationListenerThread(void* ptr)
{
	WinMediaStreamer *winMediaStreamer = (WinMediaStreamer *)ptr;
	winMediaStreamer->NotificationListenerMain();
	return NULL;
}

void WinMediaStreamer::NotificationListenerMain()
{
	while (true)
	{
		pthread_mutex_lock(&mLock);
		if (isBreakThread)
		{
			pthread_mutex_unlock(&mLock);
			break;
		}
		pthread_mutex_unlock(&mLock);

		Notification* notification = mNotificationQueue.pop();
		if (notification)
		{
			int event = notification->event;
			int ext1 = notification->ext1;
			int ext2 = notification->ext2;

			if (notification != NULL) {
				delete notification;
				notification = NULL;
			}

			pthread_mutex_lock(&mListenerLock);
			if (this->mListener != NULL) {
				this->mListener(this->mOwner, event, ext1, ext2);
			}
			else if (this->mIWinMediaStreamerListener != NULL) {
				switch (event)
				{
				case WIN_MEDIA_STREAMER_CONNECTING:
					this->mIWinMediaStreamerListener->onMediaStreamerConnecting();
					break;
				case WIN_MEDIA_STREAMER_CONNECTED:
					this->mIWinMediaStreamerListener->onMediaStreamerConnected();
					break;
				case WIN_MEDIA_STREAMER_STREAMING:
					this->mIWinMediaStreamerListener->onMediaStreamerStreaming();
					break;
				case WIN_MEDIA_STREAMER_ERROR:
					this->mIWinMediaStreamerListener->onMediaStreamerError(ext1);
					break;
				case WIN_MEDIA_STREAMER_INFO:
					this->mIWinMediaStreamerListener->onMediaStreamerInfo(ext1, ext2);
					break;
				case WIN_MEDIA_STREAMER_END:
					this->mIWinMediaStreamerListener->onMediaStreamerEnd();
					break;
				case WIN_MEDIA_STREAMER_PAUSED:
					this->mIWinMediaStreamerListener->onMediaStreamerPaused();
					break;
				default:
					break;
				}
			}
			pthread_mutex_unlock(&mListenerLock);
			continue;
		}
		else {
			pthread_mutex_lock(&mLock);
			pthread_cond_wait(&mCondition, &mLock);
			pthread_mutex_unlock(&mLock);
			continue;
		}
	}

	mNotificationQueue.flush();
}

void WinMediaStreamer::deleteNotificationListenerThread()
{
	pthread_mutex_lock(&mLock);
	isBreakThread = true;
	pthread_mutex_unlock(&mLock);

	pthread_cond_signal(&mCondition);

	pthread_join(mThread, NULL);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WinVideoCapturer::WinVideoCapturer()
{
	LOGD("WinVideoCapturer::WinVideoCapturer \n");

	mVideoCapture = NULL;
}

WinVideoCapturer::~WinVideoCapturer()
{
	LOGD("WinVideoCapturer::~WinVideoCapturer \n");

	this->terminate();
}

void WinVideoCapturer::initialize(WIN_VIDEO_CAPTURE_TYPE type, int videoWidth, int videoHeight, int videoFps)
{
	LOGD("WinVideoCapturer::initialize \n");

	if (type == WIN_GDI_DESKTOP_CAPTURE)
	{
		mType = WIN_GDI_DESKTOP_CAPTURE;
		mVideoCapture = VideoCapture::CreateVideoCapture(WIN_NATIVE_GDI_DESKTOP_CAPTURE, NULL, videoWidth, videoHeight, videoFps, -1);
		mVideoCapture->Init();
	}
}

void WinVideoCapturer::StartRecording()
{
	LOGD("WinVideoCapturer::StartRecording \n");

	if (mVideoCapture)
	{
		mVideoCapture->StartRecording();
	}
}

void WinVideoCapturer::StopRecording()
{
	LOGD("WinVideoCapturer::StopRecording \n");

	if (mVideoCapture)
	{
		mVideoCapture->StopRecording();
	}
}

void WinVideoCapturer::linkPreview(void* display)
{
	LOGD("WinVideoCapturer::linkPreview \n");

	if (mVideoCapture)
	{
		mVideoCapture->linkPreview(display);
	}
}

void WinVideoCapturer::unlinkPreview()
{
	LOGD("WinVideoCapturer::unlinkPreview \n");

	if (mVideoCapture)
	{
		mVideoCapture->unlinkPreview();
	}
}

void WinVideoCapturer::onPreviewResize(int w, int h)
{
	LOGD("WinVideoCapturer::onPreviewResize \n");

	if (mVideoCapture)
	{
		mVideoCapture->onPreviewResize(w,h);
	}
}

void WinVideoCapturer::linkMediaStreamer(IWinMediaStreamer* winMediaStreamer)
{
	LOGD("WinVideoCapturer::linkMediaStreamer \n");

	if (mVideoCapture)
	{
		mVideoCapture->linkMediaStreamer((MediaStreamer*)(winMediaStreamer->getCore()));
	}
}

void WinVideoCapturer::unlinkMediaStreamer()
{
	LOGD("WinVideoCapturer::unlinkMediaStreamer \n");

	if (mVideoCapture)
	{
		mVideoCapture->unlinkMediaStreamer();
	}
}

void WinVideoCapturer::terminate()
{
	LOGD("WinVideoCapturer::terminate \n");

	if (mVideoCapture)
	{
		mVideoCapture->Terminate();

		if (mType == WIN_GDI_DESKTOP_CAPTURE)
		{
			VideoCapture::DeleteVideoCapture(mVideoCapture, WIN_NATIVE_GDI_DESKTOP_CAPTURE);
			mVideoCapture = NULL;
		}
	}
}

void WinVideoCapturer::Release()
{
	LOGD("WinVideoCapturer::Release \n");

	delete this;
}