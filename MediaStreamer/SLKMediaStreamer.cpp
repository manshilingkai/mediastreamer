//
//  SLKMediaStreamer.cpp
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "SLKMediaStreamer.h"
#include "AutoLock.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#include "AndroidUtils.h"
#include "JNIHelp.h"
#endif

#ifdef IOS
#include "iOSMediaListener.h"
#include <VideoToolbox/VTErrors.h>
#endif

#include "NormalMediaListener.h"

#include "Mediatimer.h"

#include "MediaFile.h"

#include "ImageProcesserUtils.h"

struct SLKMediaStreamerEvent : public TimedEventQueue::Event {
    SLKMediaStreamerEvent(
                        SLKMediaStreamer *streamer,
                        void (SLKMediaStreamer::*method)())
    : mStreamer(streamer),
    mMethod(method) {
    }
    
protected:
    virtual ~SLKMediaStreamerEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mStreamer->*mMethod)();
    }
    
private:
    SLKMediaStreamer *mStreamer;
    void (SLKMediaStreamer::*mMethod)();
    
    SLKMediaStreamerEvent(const SLKMediaStreamerEvent &);
    SLKMediaStreamerEvent &operator=(const SLKMediaStreamerEvent &);
};

#ifdef ANDROID
SLKMediaStreamer::SLKMediaStreamer(JavaVM *jvm, const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions, int reConnectTimes)
{
    if(backupDir)
    {
        mBackupDir = strdup(backupDir);
    }else{
        mBackupDir = NULL;
    }
    
    if(mBackupDir)
    {
#ifdef LOG_MULTIPLE_FILE
        mMediaLog = new MediaLog(mBackupDir);
#else
        mMediaLog = MediaLog::getInstance(mBackupDir);
        mMediaLog->checkSize();
#endif
    }else{
        mMediaLog = NULL;
    }
    
    LOGI("SLKMediaStreamer::SLKMediaStreamer Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::SLKMediaStreamer Enter");
    }
    
    mJvm = jvm;
    mEncodeSurface = NULL;
    mEncodeSurfaceCore = NULL;
    
    mVideoOptions = new VideoOptions;
    mAudioOptions = new AudioOptions;
    
    mVideoOptions->hasVideo = videoOptions.hasVideo;
    mVideoOptions->videoEncodeType = videoOptions.videoEncodeType;
    mVideoOptions->videoWidth = videoOptions.videoWidth;
    mVideoOptions->videoHeight = videoOptions.videoHeight;
    mVideoOptions->videoFps = videoOptions.videoFps;
    mVideoOptions->videoRawType = videoOptions.videoRawType;
    
    mVideoOptions->videoProfile = videoOptions.videoProfile;
    mVideoOptions->videoBitRate = videoOptions.videoBitRate;
    mVideoOptions->encodeMode = videoOptions.encodeMode;
    mVideoOptions->maxKeyFrameIntervalMs = videoOptions.maxKeyFrameIntervalMs;

    mVideoOptions->networkAdaptiveMinFps = videoOptions.networkAdaptiveMinFps;
    mVideoOptions->networkAdaptiveMinVideoBitrate = videoOptions.networkAdaptiveMinVideoBitrate;
    mVideoOptions->isEnableNetworkAdaptive = videoOptions.isEnableNetworkAdaptive;
    
    mVideoOptions->quality = videoOptions.quality;
    mVideoOptions->bStrictCBR = videoOptions.bStrictCBR;
    mVideoOptions->deblockingFilterFactor = videoOptions.deblockingFilterFactor;
    
    mAudioOptions->hasAudio = audioOptions.hasAudio;
    mAudioOptions->audioSampleRate = audioOptions.audioSampleRate;
    mAudioOptions->audioNumChannels = audioOptions.audioNumChannels;
    mAudioOptions->audioBitRate = audioOptions.audioBitRate;
    mAudioOptions->isExternalAudioInput = audioOptions.isExternalAudioInput;
    
    if(publishUrl)
    {
        mPublishUrl = strdup(publishUrl);
    }else{
        mPublishUrl = NULL;
    }
    
    mMuxerFormat = MP4;
    if (!strncmp(mPublishUrl, "rtmp://", 7)) {
        mMuxerFormat = RTMP;
    }else if(!strncmp(mPublishUrl, "rtsp://", 7)) {
        mMuxerFormat = RTSP;
        mAudioOptions->isRealTime = true;
        mAudioOptions->audioSampleRate = 8000;
    }
    
    if (mVideoOptions->videoEncodeType==VIDEO_HARD_ENCODE) {
        mVideoEncoderType = MEDIACODEC;
    }else{
        mVideoEncoderType = X264;
    }
    
    if (mAudioOptions->isRealTime) {
        mVideoEncoderType = VP8;
    }
        
    mTargetVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2);
    mTargetVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2;
    mTargetVideoFrame.width = mVideoOptions->videoWidth;
    mTargetVideoFrame.height = mVideoOptions->videoHeight;
    mTargetVideoFrame.rotation = 0;
    mTargetVideoFrame.videoRawType = mVideoOptions->videoRawType;
    
    mMediaListenerPlatformType = -1;
    mMediaListener = NULL;
    
    mVideoFrameBufferPool = NULL;
    if (mAudioOptions->isRealTime) {
        mVideoFrameBufferPool = new VideoFrameBufferPool(2);
    }else{
        mVideoFrameBufferPool = new VideoFrameBufferPool();
    }
    pthread_mutex_init(&mVideoFrameBufferPoolLock, NULL);
    canInputVideoFrame = false;
    
    mColorSpaceConvert = NULL;
    mVideoEncoder = NULL;
    mMediaMuxer = NULL;
    mAudioStreamer = NULL;
    
    mAsyncPrepareEvent = NULL;
    mVideoEvent = NULL;
    mStopEvent = NULL;
    mControlBitrateEvent = NULL;
    mNotifyEvent = NULL;
    mRePublishEvent = NULL;
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mStopCondition, NULL);
    
    mVideoEvent = new SLKMediaStreamerEvent(this, &SLKMediaStreamer::onVideoEvent);
    mVideoEventPending = false;
    mControlBitrateEvent = new SLKMediaStreamerEvent(this, &SLKMediaStreamer::onControlBitrateEvent);
    mNotifyEvent = new SLKMediaStreamerEvent(this, &SLKMediaStreamer::onNotifyEvent);
    mAsyncPrepareEvent = new SLKMediaStreamerEvent(this, &SLKMediaStreamer::onPrepareAsyncEvent);
    mStopEvent = new SLKMediaStreamerEvent(this, &SLKMediaStreamer::onStopEvent);
    mRePublishEvent = new SLKMediaStreamerEvent(this, &SLKMediaStreamer::onRePublishEvent);
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();
    
    mFlags = 0;
    
    mVideoEventTimer_StartTime = 0;
    mVideoEventTimer_EndTime = 0;
    mVideoEventTime = 0;
    
    pthread_mutex_init(&mVideoDelayTimeUsLock, NULL);
    mVideoDelayTimeUs = 0;
    
    mTargetFps = mVideoOptions->videoFps;
    
    mPublishDelayTimeMs = 0ll;
    
    mTargetVideoBitrate = mVideoOptions->videoBitRate;
    
    isAudioEnabled = true;
    
    mRealFps = 0;
    flowingFrameCountPerTime = 0;
    realfps_begin_time=0;
    
    pthread_mutex_init(&mMuxerLock, NULL);
    pthread_mutex_init(&mAudioStreamerLock, NULL);
    
    gotError = false;
    
    isPushImage = false;
    pushImageFile = NULL;
    pushImageVideoFrame = NULL;
    
    isInterrupt = false;
    pthread_mutex_init(&mInterruptLock, NULL);
    
    mReconnectTimes = reConnectTimes;
    
    isNotReachable = false;
    
    MediaTimer::open();
    
    LOGI("SLKMediaStreamer::SLKMediaStreamer Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::SLKMediaStreamer Leave");
    }
}

#else
SLKMediaStreamer::SLKMediaStreamer(const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions, char* backgroundPngCover, int reConnectTimes)
{
    if(backupDir)
    {
        mBackupDir = strdup(backupDir);
    }else{
        mBackupDir = NULL;
    }
    
    if(mBackupDir)
    {
#ifdef LOG_MULTIPLE_FILE
        mMediaLog = new MediaLog(mBackupDir);
#else
        mMediaLog = MediaLog::getInstance(mBackupDir);
        mMediaLog->checkSize();
#endif
    }else{
        mMediaLog = NULL;
    }
    
    LOGI("SLKMediaStreamer::SLKMediaStreamer Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::SLKMediaStreamer Enter");
    }
    
    mVideoOptions = new VideoOptions;
    mAudioOptions = new AudioOptions;
    
    mVideoOptions->hasVideo = videoOptions.hasVideo;
    mVideoOptions->videoEncodeType = videoOptions.videoEncodeType;
    mVideoOptions->videoWidth = videoOptions.videoWidth;
    mVideoOptions->videoHeight = videoOptions.videoHeight;
    mVideoOptions->videoFps = videoOptions.videoFps;
    mVideoOptions->videoRawType = videoOptions.videoRawType;
    
    mVideoOptions->videoProfile = videoOptions.videoProfile;
    mVideoOptions->videoBitRate = videoOptions.videoBitRate;
    mVideoOptions->encodeMode = videoOptions.encodeMode;
    mVideoOptions->maxKeyFrameIntervalMs = videoOptions.maxKeyFrameIntervalMs;

    mVideoOptions->networkAdaptiveMinFps = videoOptions.networkAdaptiveMinFps;
    mVideoOptions->networkAdaptiveMinVideoBitrate = videoOptions.networkAdaptiveMinVideoBitrate;
    mVideoOptions->isEnableNetworkAdaptive = videoOptions.isEnableNetworkAdaptive;
    
    mVideoOptions->quality = videoOptions.quality;
    mVideoOptions->bStrictCBR = videoOptions.bStrictCBR;
    mVideoOptions->deblockingFilterFactor = videoOptions.deblockingFilterFactor;
        
    mAudioOptions->hasAudio = audioOptions.hasAudio;
    mAudioOptions->audioSampleRate = audioOptions.audioSampleRate;
    mAudioOptions->audioNumChannels = audioOptions.audioNumChannels;
    mAudioOptions->audioBitRate = audioOptions.audioBitRate;
    mAudioOptions->isExternalAudioInput = audioOptions.isExternalAudioInput;
    
    if(publishUrl)
    {
        mPublishUrl = strdup(publishUrl);
    }else{
        mPublishUrl = NULL;
    }
    
    mMuxerFormat = MP4;
    if (!strncmp(mPublishUrl, "rtmp://", 7)) {
        mMuxerFormat = RTMP;
    }else if(!strncmp(mPublishUrl, "rtsp://", 7)) {
        mMuxerFormat = RTSP;
        mAudioOptions->isRealTime = true;
        mAudioOptions->audioSampleRate = 8000;
    }
    
    if (mVideoOptions->videoEncodeType==VIDEO_HARD_ENCODE) {
#ifdef IOS
        mVideoEncoderType = VIDEO_TOOL_BOX;
#else
        mVideoEncoderType = X264;
#endif
    }else{
        mVideoEncoderType = X264;
    }
    
    if (mAudioOptions->isRealTime) {
        mVideoEncoderType = VP8;
    }
        
    if (mVideoOptions->hasVideo && mVideoOptions->videoRawType!=VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF) {
        if (mVideoEncoderType==VIDEO_TOOL_BOX) {
            // NV12 Or BGRA
            mTargetVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*4);
            mTargetVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*4;
            mTargetVideoFrame.width = mVideoOptions->videoWidth;
            mTargetVideoFrame.height = mVideoOptions->videoHeight;
            mTargetVideoFrame.rotation = 0;
            mTargetVideoFrame.videoRawType = mVideoOptions->videoRawType;
        }else{
            mTargetVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2);
            mTargetVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2;
            mTargetVideoFrame.width = mVideoOptions->videoWidth;
            mTargetVideoFrame.height = mVideoOptions->videoHeight;
            mTargetVideoFrame.rotation = 0;
            mTargetVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_I420;
        }
    }

    mMediaListenerPlatformType = -1;
    mMediaListener = NULL;
    
    mVideoFrameBufferPool = NULL;
    if (mVideoOptions->hasVideo) {
        if (mVideoOptions->videoRawType==VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF) {
            mVideoFrameBufferPool = new VideoFrameBufferPool(2, VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF);
        }else{
            if (mAudioOptions->isRealTime) {
                mVideoFrameBufferPool = new VideoFrameBufferPool(2);
            }else{
                mVideoFrameBufferPool = new VideoFrameBufferPool();
            }
        }
    }
    pthread_mutex_init(&mVideoFrameBufferPoolLock, NULL);
    canInputVideoFrame = false;
    
    mColorSpaceConvert = NULL;
    mVideoEncoder = NULL;
    mMediaMuxer = NULL;
    mAudioStreamer = NULL;
        
    mAsyncPrepareEvent = NULL;
    mVideoEvent = NULL;
    mStopEvent = NULL;
    mControlBitrateEvent = NULL;
    mNotifyEvent = NULL;
    mRePublishEvent = NULL;
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mStopCondition, NULL);
    
    mVideoEvent = new SLKMediaStreamerEvent(this, &SLKMediaStreamer::onVideoEvent);
    mVideoEventPending = false;
    mControlBitrateEvent = new SLKMediaStreamerEvent(this, &SLKMediaStreamer::onControlBitrateEvent);
    mNotifyEvent = new SLKMediaStreamerEvent(this, &SLKMediaStreamer::onNotifyEvent);
    mAsyncPrepareEvent = new SLKMediaStreamerEvent(this, &SLKMediaStreamer::onPrepareAsyncEvent);
    mStopEvent = new SLKMediaStreamerEvent(this, &SLKMediaStreamer::onStopEvent);
    mRePublishEvent = new SLKMediaStreamerEvent(this, &SLKMediaStreamer::onRePublishEvent);
    
    mQueue.start();
    
    mFlags = 0;
    
    mVideoEventTimer_StartTime = 0;
    mVideoEventTimer_EndTime = 0;
    mVideoEventTime = 0;
    
    pthread_mutex_init(&mVideoDelayTimeUsLock, NULL);
    mVideoDelayTimeUs = 0;
    
    mTargetFps = mVideoOptions->videoFps;
    
    mPublishDelayTimeMs = 0ll;
    
    mTargetVideoBitrate = mVideoOptions->videoBitRate;
    
    isAudioEnabled = true;
    
    mRealFps = 0;
    flowingFrameCountPerTime = 0;
    realfps_begin_time=0;
    
    pthread_mutex_init(&mMuxerLock, NULL);
    pthread_mutex_init(&mAudioStreamerLock, NULL);
    
    gotError = false;
    
    isPushImage = false;
    pushImageFile = NULL;
    pushImageVideoFrame = NULL;
    if (backgroundPngCover) {
        pushImageFile = strdup(backgroundPngCover);
    }
    
#ifdef IOS
    isNeedResurrectiOSVTB = false;
#endif
    
    isInterrupt = false;
    pthread_mutex_init(&mInterruptLock, NULL);
    
    mReconnectTimes = reConnectTimes;
    
    isNotReachable = false;

    MediaTimer::open();
    
    LOGI("SLKMediaStreamer::SLKMediaStreamer Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::SLKMediaStreamer Leave");
    }
}
#endif

SLKMediaStreamer::~SLKMediaStreamer()
{
    stop(false);
    
    LOGI("SLKMediaStreamer::~SLKMediaStreamer Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::~SLKMediaStreamer Enter");
    }
    
    mQueue.stop(true);
    
    LOGI("SLKMediaStreamer::~SLKMediaStreamer TimedEventQueue Stoped");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::~SLKMediaStreamer TimedEventQueue Stoped");
    }
    
    mNotificationQueue.flush();
    
    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
    
    if (mVideoEvent!=NULL) {
        delete mVideoEvent;
        mVideoEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mControlBitrateEvent!=NULL) {
        delete mControlBitrateEvent;
        mControlBitrateEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    if (mRePublishEvent!=NULL) {
        delete mRePublishEvent;
        mRePublishEvent = NULL;
    }
    
    pthread_cond_destroy(&mStopCondition);
    pthread_mutex_destroy(&mLock);
    
    if (mVideoFrameBufferPool!=NULL) {
        delete mVideoFrameBufferPool;
        mVideoFrameBufferPool = NULL;
    }
    pthread_mutex_destroy(&mVideoFrameBufferPoolLock);
    canInputVideoFrame = false;
    
    if (mTargetVideoFrame.data!=NULL) {
        free(mTargetVideoFrame.data);
        mTargetVideoFrame.data = NULL;
    }
        
    if (mMediaListenerPlatformType==0) {
#ifdef ANDROID
        JniMediaListener *jniListener = (JniMediaListener*)mMediaListener;
        if (jniListener!=NULL) {
            delete jniListener;
            jniListener = NULL;
        }
#endif
    }else if (mMediaListenerPlatformType==1) {
#ifdef IOS
        iOSMediaListener *iOSListener = (iOSMediaListener*)mMediaListener;
        if (iOSListener!=NULL) {
            delete iOSListener;
            iOSListener = NULL;
        }
#endif
    }else if (mMediaListenerPlatformType==2) {
        NormalMediaListener* normalMediaListener = (NormalMediaListener*)mMediaListener;
        if (normalMediaListener!=NULL) {
            delete normalMediaListener;
            normalMediaListener = NULL;
        }
    }
    
    mMediaListener = NULL;
    
#ifdef ANDROID
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    if (mEncodeSurface!=NULL) {
        env->DeleteGlobalRef(static_cast<jobject>(mEncodeSurface));
        mEncodeSurface = NULL;
    }
    
    if (mEncodeSurfaceCore!=NULL) {
        env->DeleteGlobalRef(static_cast<jobject>(mEncodeSurfaceCore));
        mEncodeSurfaceCore = NULL;
    }
#endif
    
    pthread_mutex_destroy(&mVideoDelayTimeUsLock);
        
    pthread_mutex_destroy(&mMuxerLock);
    pthread_mutex_destroy(&mAudioStreamerLock);
    
    pthread_mutex_destroy(&mInterruptLock);
    
    if (mPublishUrl) {
        free(mPublishUrl);
        mPublishUrl = NULL;
    }
    
    if (mVideoOptions!=NULL) {
        delete mVideoOptions;
        mVideoOptions = NULL;
    }
    
    if (mAudioOptions!=NULL) {
        delete mAudioOptions;
        mAudioOptions = NULL;
    }
    
    if (pushImageFile) {
        free(pushImageFile);
        pushImageFile = NULL;
    }
    
    MediaTimer::close();
    
    LOGI("SLKMediaStreamer::~SLKMediaStreamer Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::~SLKMediaStreamer Leave");
    }
    
#ifdef LOG_MULTIPLE_FILE
    if (mMediaLog) {
        delete mMediaLog;
        mMediaLog = NULL;
    }
#else
    if (mMediaLog) {
        mMediaLog->flush();
    }
    //    MediaLog::unrefInstance();
#endif
    
    if(mBackupDir)
    {
        free(mBackupDir);
        mBackupDir = NULL;
    }
}

#ifdef ANDROID
void SLKMediaStreamer::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    LOGI("SLKMediaStreamer::setListener");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::setListener");
    }
    
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    mMediaListenerPlatformType = 0; //JNI
    
    modifyFlags(INITIALIZED, SET);
}

void SLKMediaStreamer::setEncodeSurfaceCore(void *encodeSurfaceCore)
{
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    if (mEncodeSurfaceCore!=NULL) {
        env->DeleteGlobalRef(static_cast<jobject>(mEncodeSurfaceCore));
        mEncodeSurfaceCore = NULL;
    }
    
    if (encodeSurfaceCore!=NULL) {
        mEncodeSurfaceCore = (void*)env->NewGlobalRef(static_cast<jobject>(encodeSurfaceCore));
        jclass clazz = env->GetObjectClass(static_cast<jobject>(mEncodeSurfaceCore));
        if (clazz == NULL) {
            jniThrowException(env, "java/lang/Exception", NULL);
            return;
        }
        mEncodeSurfaceCoreClassRequestRenderMethod = env->GetMethodID(clazz, "requestRender", "(J)V");
    }
}

void SLKMediaStreamer::setEncodeSurface(void *surface)
{
    JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
    if (mEncodeSurface!=NULL) {
        env->DeleteGlobalRef(static_cast<jobject>(mEncodeSurface));
        mEncodeSurface = NULL;
    }
    
    if (surface!=NULL) {
        mEncodeSurface = (void*)env->NewGlobalRef(static_cast<jobject>(surface));
    }
}
#endif

void SLKMediaStreamer::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    LOGI("SLKMediaStreamer::setListener");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::setListener");
    }
    
    AutoLock autoLock(&mLock);
    
#ifdef IOS
    mMediaListener = new iOSMediaListener(listener,arg);
    mMediaListenerPlatformType = 1; //iOS
#else
    mMediaListener = new NormalMediaListener(listener,arg);
    mMediaListenerPlatformType = 2; //Normal
#endif
    
    modifyFlags(INITIALIZED, SET);
}

void SLKMediaStreamer::notifyListener_l(int event, int ext1, int ext2)
{
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void SLKMediaStreamer::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    postNotifyEvent_l();
}

void SLKMediaStreamer::postNotifyEvent_l()
{
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void SLKMediaStreamer::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;

    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
    
    switch (event) {
        case MEDIA_STREAMER_ERROR:
            modifyFlags(ERRORED, ASSIGN);
            notifyListener_l(event, ext1, ext2);
            gotError = true;
            stop_l();
            break;
        case MEDIA_STREAMER_INFO:
            notifyListener_l(event, ext1, ext2);
            break;
        case MEDIA_STREAMER_COMMAND:
            if (ext1 == MEDIA_STREAMER_COMMAND_NETWORK_REACHABILITY) {
                processNetWorkReachability(ext2);
            }
            break;
        default:
            notifyListener_l(event, ext1, ext2);
            break;
    }
}

void SLKMediaStreamer::enableAudio(bool isEnable)
{
    AutoLock autoLock(&mLock);
    if (isAudioEnabled!=isEnable) {
        isAudioEnabled = isEnable;
        
        if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
            mAudioStreamer->enableSilence(!isAudioEnabled);
        }
    }
}

void SLKMediaStreamer::mixBGM(int bgmId, char* bgmUrl, float volume, int numberOfLoops)
{
    AutoLock autoLock(&mLock);
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        mAudioStreamer->mixBGM(bgmId, bgmUrl, volume, numberOfLoops);
    }
}

void SLKMediaStreamer::playBGM()
{
    AutoLock autoLock(&mLock);
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        mAudioStreamer->playBGM();
    }
}

void SLKMediaStreamer::pauseBGM()
{
    AutoLock autoLock(&mLock);
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        mAudioStreamer->pauseBGM();
    }
}

void SLKMediaStreamer::stopBGM()
{
    AutoLock autoLock(&mLock);
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        mAudioStreamer->stopBGM();
    }
}

void SLKMediaStreamer::setBGMVolume(float volume)
{
    AutoLock autoLock(&mLock);
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        mAudioStreamer->setBGMVolume(volume);
    }
}

void SLKMediaStreamer::mixSEM(int semId, char* semUrl, float volume, int numberOfLoops)
{
    AutoLock autoLock(&mLock);
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        mAudioStreamer->mixSEM(semId, semUrl, volume, numberOfLoops);
    }
}

bool SLKMediaStreamer::inputVideoFrame(VideoFrame *inVideoFrame)
{
    if (inVideoFrame==NULL) return false;
    
    bool ret = false;
    
    if (mVideoOptions->hasVideo) {
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        if (canInputVideoFrame && mVideoFrameBufferPool!=NULL) {
            inVideoFrame->pts = MediaTimer::GetNowMediaTimeMS();
            ret = mVideoFrameBufferPool->push(inVideoFrame);
        }
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    }
    
    return ret;
}

void SLKMediaStreamer::inputYUVVideoFrame(YUVVideoFrame* inVideoFrame)
{
    if (inVideoFrame==NULL) return;
    
    if (mVideoOptions->hasVideo) {
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        if (canInputVideoFrame && mVideoFrameBufferPool!=NULL) {
            inVideoFrame->pts = MediaTimer::GetNowMediaTimeMS();
            mVideoFrameBufferPool->push(inVideoFrame);
        }
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    }
}

void SLKMediaStreamer::inputAudioFrame(AudioFrame *inAudioFrame)
{
    if (inAudioFrame==NULL) return;
    
    if (mAudioOptions->hasAudio && mAudioOptions->isExternalAudioInput) {
        pthread_mutex_lock(&mAudioStreamerLock);
        if (mAudioStreamer!=NULL) {
            inAudioFrame->pts = MediaTimer::GetNowMediaTimeMS();
            mAudioStreamer->inputAudioFrame(inAudioFrame);
        }
        pthread_mutex_unlock(&mAudioStreamerLock);
    }
}

void SLKMediaStreamer::inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType)
{
    if (inAudioFrame==NULL) return;
    
    if (mAudioOptions->hasAudio && mAudioOptions->isExternalAudioInput) {
        pthread_mutex_lock(&mAudioStreamerLock);
        if (mAudioStreamer!=NULL) {
            inAudioFrame->pts = MediaTimer::GetNowMediaTimeMS();
            mAudioStreamer->inputAudioFrame(inAudioFrame, audioSourceType);
        }
        pthread_mutex_unlock(&mAudioStreamerLock);
    }
}

void SLKMediaStreamer::inputText(char* text, int size)
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & STREAMING) {
        if(mMediaMuxer)
        {
            TextPacket textPacket;
            textPacket.data = (uint8_t*)text;
            textPacket.size = size;
            textPacket.pts = MediaTimer::GetNowMediaTimeMS();
            mMediaMuxer->pushTextData(&textPacket);
        }
        return;
    }
}

void SLKMediaStreamer::start()
{
    LOGI("SLKMediaStreamer::start Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::start Enter");
    }
    
    AutoLock autoLock(&mLock);
    
    if (mFlags & CONNECTING) {
        notifyListener_l(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_ALREADY_CONNECTING);
        
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    prepareAsync_l();
    
    LOGI("SLKMediaStreamer::start Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::start Leave");
    }
}

void SLKMediaStreamer::prepareAsync_l()
{
    LOGI("SLKMediaStreamer::prepareAsync_l Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::prepareAsync_l Enter");
    }
    
    modifyFlags(END, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(CONNECTING, SET);
    
    gotError = false;
    
    isNotReachable = false;
    
    mQueue.postEvent(mAsyncPrepareEvent);
    
    LOGI("SLKMediaStreamer::prepareAsync_l Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::prepareAsync_l Leave");
    }
}

void SLKMediaStreamer::onPrepareAsyncEvent()
{
    LOGI("SLKMediaStreamer::onPrepareAsyncEvent");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::onPrepareAsyncEvent");
    }
    
    AutoLock autoLock(&mLock);
    
    notifyListener_l(MEDIA_STREAMER_CONNECTING);
    int ret = open_all_pipelines_l();
    if (ret>=0) {
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(CONNECTED, SET);
        
        notifyListener_l(MEDIA_STREAMER_CONNECTED);
        
        mMediaMuxer->start();
        
        play_l();
    }else {
        if (ret==-6) {
            //Exit
            modifyFlags(CONNECTING, CLEAR);
            modifyFlags(ERRORED, SET);
            gotError = true;
            return;
        }
        
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(ERRORED, SET);
        notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_CONNECT_FAIL);
        gotError = true;
        stop_l();
    }
}

void SLKMediaStreamer::resume()
{
    AutoLock autoLock(&mLock);
    return play_l();
}

void SLKMediaStreamer::play_l()
{
    LOGI("SLKMediaStreamer::play_l Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::play_l Enter");
    }
    
    if (mFlags & STREAMING) {
        LOGW("%s","SLKMediaStreamer is streaming");
        if (mMediaLog) {
            mMediaLog->writeLog("SLKMediaStreamer is streaming");
        }
        return;
    }
    
    if (!(mFlags & CONNECTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    modifyFlags(CONNECTED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    //Timing Begin
    MediaTimer::onLine();
    
    //open audio data source
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        int ret = mAudioStreamer->start();
        if (ret==-1) {
            notifyListener_l(MEDIA_STREAMER_WARN, MEDIA_STREAMER_WARN_AUDIO_CAPTURE_START_FAIL);

//            modifyFlags(ERRORED, SET);
//            gotError = true;
//            stop_l();
//
//            return;
        }
    }
    
    if (mVideoOptions->hasVideo) {
        //open video data source
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        if (mVideoFrameBufferPool!=NULL) {
            mVideoFrameBufferPool->flush();
            canInputVideoFrame = true;
        }
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
        
        postVideoEvent_l();
        
        if (mMuxerFormat==RTMP || mMuxerFormat==RTSP) {
            if (mVideoOptions->isEnableNetworkAdaptive) {
                postControlBitrateEvent_l();
            }
        }
    }
    
    modifyFlags(STREAMING, SET);
    
    notifyListener_l(MEDIA_STREAMER_STREAMING);
    
    LOGI("SLKMediaStreamer::play_l Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::play_l Leave");
    }
}

void SLKMediaStreamer::pause()
{
    AutoLock autoLock(&mLock);
    return pause_l();
}

void SLKMediaStreamer::pause_l()
{
    if (!(mFlags & STREAMING)) {
        return;
    }
    
    modifyFlags(STREAMING, CLEAR);
    
    pause_ll();
    
    modifyFlags(PAUSED, SET);
    
    notifyListener_l(MEDIA_STREAMER_PAUSED);
    
    return;
}

void SLKMediaStreamer::pause_ll()
{
    if (mVideoOptions->hasVideo) {
        if (mMuxerFormat==RTMP || mMuxerFormat==RTSP) {
            if (mVideoOptions->isEnableNetworkAdaptive) {
                mQueue.cancelEvent(mControlBitrateEvent->eventID());
            }
        }
    }
    
    mQueue.cancelEvent(mVideoEvent->eventID());
    mVideoEventPending = false;
    
    //close audio data source
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        int ret = mAudioStreamer->pause();
        
        if (ret==-1) {
            
            notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_AUDIO_CAPTURE_STOP_FAIL);
            
            modifyFlags(ERRORED, SET);
            gotError = true;
            stop_l();
            
            return;
        }
    }
    
    if (mVideoOptions->hasVideo) {
        //close video data source
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        if (mVideoFrameBufferPool!=NULL) {
            mVideoFrameBufferPool->flush();
            canInputVideoFrame = false;
        }
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    }

    //Timing End
    MediaTimer::offLine();
}

void SLKMediaStreamer::cancelStreamerEvents()
{
    mQueue.cancelEvent(mVideoEvent->eventID());
    mVideoEventPending = false;
    mQueue.cancelEvent(mControlBitrateEvent->eventID());
    mQueue.cancelEvent(mNotifyEvent->eventID());
    mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
    mQueue.cancelEvent(mStopEvent->eventID());
    mQueue.cancelEvent(mRePublishEvent->eventID());
}

void SLKMediaStreamer::stop(bool isCancle)
{
    LOGI("SLKMediaStreamer::stop Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::stop Enter");
    }
    
    pthread_mutex_lock(&mMuxerLock);
    if (mMediaMuxer!=NULL) {
        mMediaMuxer->interrupt();
    }
    pthread_mutex_unlock(&mMuxerLock);
    
    pthread_mutex_lock(&mInterruptLock);
    isInterrupt = true;
    pthread_mutex_unlock(&mInterruptLock);
    
    AutoLock autoLock(&mLock);
    
    pthread_mutex_lock(&mInterruptLock);
    isInterrupt = false;
    pthread_mutex_unlock(&mInterruptLock);
    
    if (mFlags & END) {
        return;
    }
    
    if (mFlags & ENDING) {
        notifyListener_l(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_ALREADY_ENDING);
        
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    if (isCancle) {
        gotError = true;
    }
    
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
    
    if (isCancle) {
        if (mMuxerFormat == MP4) {
            //delete local mp4 file
            if (MediaFile::isExist(mPublishUrl)) {
                MediaFile::deleteFile(mPublishUrl);
            }
        }
    }
    
    LOGI("SLKMediaStreamer::stop Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::stop Leave");
    }
}

void SLKMediaStreamer::stop_l()
{
    LOGI("SLKMediaStreamer::stop_l");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::stop_l");
    }
    
    modifyFlags(ENDING, ASSIGN);
    
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void SLKMediaStreamer::onStopEvent()
{
    LOGI("SLKMediaStreamer::onStopEvent");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::onStopEvent");
    }
    
    AutoLock autoLock(&mLock);
    
    close_all_pipelines_l();
    
    cancelStreamerEvents();
    mNotificationQueue.flush();
    
    //close video data source
    pthread_mutex_lock(&mVideoFrameBufferPoolLock);
    if (mVideoFrameBufferPool!=NULL) {
        mVideoFrameBufferPool->flush();
        canInputVideoFrame = false;
    }
    pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    
    isNotReachable = false;
    
    //Timing End
    MediaTimer::offLine();
    
    modifyFlags(END, ASSIGN);
    
    if (!gotError) {
        notifyListener_l(MEDIA_STREAMER_END);
    }else{
        if (mMuxerFormat == MP4) {
            //delete local file
            if (MediaFile::isExist(mPublishUrl)) {
                MediaFile::deleteFile(mPublishUrl);
            }
        }
    }
    gotError = false;

    pthread_cond_broadcast(&mStopCondition);
}

void SLKMediaStreamer::postVideoEvent_l(int64_t delayUs)
{
    if (mVideoEventPending) {
        return;
    }
    mVideoEventPending = true;
    mQueue.postEventWithDelay(mVideoEvent, delayUs < 0 ? 0 : delayUs);
}

void SLKMediaStreamer::onVideoEvent()
{
    AutoLock autoLock(&mLock);
    
    mVideoEventTimer_StartTime = GetNowUs();
    
    if (!mVideoEventPending) {
        // The event has been cancelled in reset_l() but had already // been scheduled for execution at that time.
        return;
    }
    mVideoEventPending = false;
    
    int ret = flowing_l();
    
    mVideoEventTimer_EndTime = GetNowUs();

    mVideoEventTime = mVideoEventTimer_EndTime-mVideoEventTimer_StartTime;
    
    if (ret==0) {
        int64_t waitTimeUs = 0ll;
        if (mAudioOptions->isRealTime) {
            waitTimeUs = 1*1000; //1ms
        }else{
            waitTimeUs = 10*1000; //10ms
        }
        postVideoEvent_l(waitTimeUs);
        
        pthread_mutex_lock(&mVideoDelayTimeUsLock);
        mVideoDelayTimeUs = mVideoDelayTimeUs + waitTimeUs + mVideoEventTime;
        pthread_mutex_unlock(&mVideoDelayTimeUsLock);
        
        return;
    }else if(ret<0) {
        modifyFlags(STREAMING, CLEAR);
        modifyFlags(ERRORED, SET);
        
        if(ret==-1)
        {
            notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_COLORSPACECONVERT_FAIL);
        }else if(ret==-2)
        {
            notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_VIDEO_ENCODE_FAIL);
        }else {
            notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_UNKNOWN);
        }
        
        gotError = true;
        stop_l();
        
        return;
    }else {
        pthread_mutex_lock(&mVideoDelayTimeUsLock);
        int64_t delayUs = 1000*1000/mTargetFps-mVideoEventTime-mVideoDelayTimeUs;
        if (delayUs>=0) {
            mVideoDelayTimeUs = 0;
        }else {
            mVideoDelayTimeUs = -delayUs;
        }
        pthread_mutex_unlock(&mVideoDelayTimeUsLock);

        postVideoEvent_l(delayUs);
    }
}

void SLKMediaStreamer::postControlBitrateEvent_l()
{
    mQueue.postEventWithDelay(mControlBitrateEvent, 4000*1000);
}

void SLKMediaStreamer::onControlBitrateEvent()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & STREAMING) {
        mPublishDelayTimeMs = mMediaMuxer->getPublishDelayTimeMs();
        
        if (mPublishDelayTimeMs>=0) {
            char log[1024];
            LOGD("Publish Delay Time : %lld", mPublishDelayTimeMs);
            sprintf(log, "Publish Delay Time : %lld", mPublishDelayTimeMs);
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
            
            int oldTargetVideoBitrate = mTargetVideoBitrate;
            
            if (mPublishDelayTimeMs>=200 && mPublishDelayTimeMs<1000) {
                return;
            }

            if (mPublishDelayTimeMs>=1000) {
                mTargetVideoBitrate -= mPublishDelayTimeMs / 10;
            }
            
            if (mPublishDelayTimeMs<200) {
                mTargetVideoBitrate += 100;//+100k
            }
            
            if (mTargetVideoBitrate>=mVideoOptions->videoBitRate) {
                mTargetVideoBitrate = mVideoOptions->videoBitRate;
            }
            
            if (mTargetVideoBitrate<=mVideoOptions->networkAdaptiveMinVideoBitrate) {
                mTargetVideoBitrate = mVideoOptions->networkAdaptiveMinVideoBitrate;
            }
            
            int upOrDownBitrate = mTargetVideoBitrate - oldTargetVideoBitrate;
            
            if (upOrDownBitrate>=100 || upOrDownBitrate<=-100) {
                if (mVideoEncoder) {
                    mVideoEncoder->SetBitRate(mTargetVideoBitrate);
                    mVideoEncoder->SetVBVBufferSize(mTargetVideoBitrate);
                    mVideoEncoder->ReconfigEncodeParam();
                }
            }

            if (upOrDownBitrate>=100) {
                LOGD("Up Video Bitrate From %d To %d",oldTargetVideoBitrate, mTargetVideoBitrate);
                sprintf(log, "Up Video Bitrate From %d To %d",oldTargetVideoBitrate, mTargetVideoBitrate);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                notifyListener_l(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_PUBLISH_UP_BITRATE, mTargetVideoBitrate);

                int upFrames = upOrDownBitrate / 100;
                mTargetFps += upFrames;
            }
            
            if (upOrDownBitrate<=-100) {
                LOGD("Down Video Bitrate From %d To %d",oldTargetVideoBitrate, mTargetVideoBitrate);
                sprintf(log, "Down Video Bitrate From %d To %d",oldTargetVideoBitrate, mTargetVideoBitrate);
                if (mMediaLog) {
                    mMediaLog->writeLog(log);
                }
                
                notifyListener_l(MEDIA_STREAMER_INFO,MEDIA_STREAMER_INFO_PUBLISH_DOWN_BITRATE, mTargetVideoBitrate);
                
                int downFrames = upOrDownBitrate / -100;
                mTargetFps -= downFrames;
            }
            
            if (mTargetFps>=mVideoOptions->videoFps) {
                mTargetFps = mVideoOptions->videoFps;
            }
            
            if (mTargetFps<=mVideoOptions->networkAdaptiveMinFps) {
                mTargetFps = mVideoOptions->networkAdaptiveMinFps;
            }
            
            if (mPublishDelayTimeMs>=30000) {
                modifyFlags(ERRORED, ASSIGN);
                notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_POOR_NETWORK);
                gotError = true;
                stop_l();
                
                return;
            }
        }
    }
    
    mQueue.postEventWithDelay(mControlBitrateEvent, 4000*1000);
}

void SLKMediaStreamer::postRePublishEvent_l()
{
    mQueue.cancelEvent(mRePublishEvent->eventID());
    mQueue.postEventWithDelay(mRePublishEvent, 0);
}

void SLKMediaStreamer::onRePublishEvent()
{
    AutoLock autoLock(&mLock);

    if (!(mFlags & STREAMING) && !(mFlags & PAUSED) && !(mFlags & ERRORED)) {
        return;
    }
    
    LOGD("SLKMediaStreamer RePublishing");
    
    bool isPausedState = false;
    if (mFlags & PAUSED) {
        isPausedState = true;
    }
    
    pause_l();
    
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        mAudioStreamer->setMediaMuxer(NULL);
    }
    
    if (mMediaMuxer!=NULL) {
        mMediaMuxer->interrupt();
        mMediaMuxer->stop();
        LOGD("MediaMuxer stop Success");
        
        pthread_mutex_lock(&mMuxerLock);
        MediaMuxer::DeleteMediaMuxer(mMediaMuxer, FFMPEG_STREAMER);
        mMediaMuxer = NULL;
        pthread_mutex_unlock(&mMuxerLock);
        LOGD("MediaMuxer Release Success");
    }
    
    if (mMuxerFormat==RTMP || mMuxerFormat==RTSP) {
        int currentConnectTimes = 0;
        int reConnectRet = 0;
        while (currentConnectTimes<=mReconnectTimes) {
            pthread_mutex_lock(&mInterruptLock);
            if (isInterrupt) {
                isInterrupt = false;
                pthread_mutex_unlock(&mInterruptLock);
                reConnectRet = -2;
                break;
            }
            pthread_mutex_unlock(&mInterruptLock);
                
            //open MediaMuxer
            pthread_mutex_lock(&mMuxerLock);
            mMediaMuxer = MediaMuxer::CreateMediaMuxer(FFMPEG_STREAMER, mMuxerFormat, mPublishUrl, mMediaLog, mVideoOptions, mAudioOptions);
            pthread_mutex_unlock(&mMuxerLock);
            mMediaMuxer->setListener(this);
#ifdef ANDROID
            mMediaMuxer->registerJavaVMEnv(mJvm);
#endif
            LOGD("Prepareing MediaMuxer");
            int iRet = mMediaMuxer->prepare();
            reConnectRet = iRet;
            if (iRet>=0) {
                LOGD("Prepare FFmpegMuxer Success");
                break;
            }else{
                if (iRet==-2) {
                    break;
                }else{
                    if (mMediaMuxer!=NULL) {
                        mMediaMuxer->interrupt();
                        mMediaMuxer->stop();
                        
                        pthread_mutex_lock(&mMuxerLock);
                        MediaMuxer::DeleteMediaMuxer(mMediaMuxer, FFMPEG_STREAMER);
                        mMediaMuxer = NULL;
                        pthread_mutex_unlock(&mMuxerLock);
                    }
                        
                    LOGE("Prepare FFmpegMuxer Fail, Retry");
                    currentConnectTimes++;
                    notifyListener_l(MEDIA_STREAMER_WARN, MEDIA_STREAMER_WARN_RECONNECTING, currentConnectTimes);
                    continue;
                }
            }
        }

        if (reConnectRet<0) {
            if (reConnectRet==-2) {
                LOGW("Immediate exit was requested");
                modifyFlags(ERRORED, ASSIGN);
                gotError = true;
                return;
            }else{
                LOGE("Prepare FFmpegMuxer Fail");
                notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_CONNECT_FAIL);
                
                modifyFlags(ERRORED, ASSIGN);
                gotError = true;
                stop_l();
                return;
            }
        }else{
            if (isNotReachable) {
                isNotReachable = false;
                notifyListener_l(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_NETWORK_REACHABLE, -1);
            }
        }
    }else{
        //open MediaMuxer
        pthread_mutex_lock(&mMuxerLock);
        mMediaMuxer = MediaMuxer::CreateMediaMuxer(FFMPEG_STREAMER, mMuxerFormat, mPublishUrl, mMediaLog, mVideoOptions, mAudioOptions);
        pthread_mutex_unlock(&mMuxerLock);
        mMediaMuxer->setListener(this);
#ifdef ANDROID
        mMediaMuxer->registerJavaVMEnv(mJvm);
#endif
        LOGD("Prepareing MediaMuxer");
        int iRet = mMediaMuxer->prepare();
        if (iRet<0) {
            if (iRet==-2) {
                LOGW("Immediate exit was requested");
                modifyFlags(ERRORED, ASSIGN);
                gotError = true;
                return;
            }
                
            LOGE("Prepare FFmpegMuxer Fail");
            notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_CONNECT_FAIL);
            
            modifyFlags(ERRORED, ASSIGN);
            gotError = true;
            stop_l();
            return;
        }
        LOGD("Prepare FFmpegMuxer Success");
    }

    if (mVideoOptions->hasVideo && mVideoEncoder!=NULL) {
        mVideoEncoder->Close();
        LOGD("VideoEncoder Close Success");
        VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
        mVideoEncoder = NULL;
        LOGD("VideoEncoder Release Success");
    }
    
    if (mVideoOptions->hasVideo) {
        //open VideoEncoder
        mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, mVideoOptions->videoRawType, VIDEO_ENCODE_PROFILE(mVideoOptions->videoProfile), (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
#ifdef ANDROID
        mVideoEncoder->registerJavaVMEnv(mJvm);
        if (mVideoEncoderType==MEDIACODEC) {
            mVideoEncoder->setEncodeSurface(mEncodeSurface);
        }
#endif
        bool ret = mVideoEncoder->Open();
        
#ifdef IOS
        if (!ret && mVideoEncoderType==VIDEO_TOOL_BOX)
        {
            if (mVideoEncoder!=NULL) {
                mVideoEncoder->Close();
                VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            }
            
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, mVideoOptions->videoRawType, VIDEO_ENCODE_PROFILE(mVideoOptions->videoProfile), (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
                
            ret = mVideoEncoder->Open();
        }
#endif
            
#ifdef ANDROID
        if (!ret && mVideoEncoderType==MEDIACODEC) {
                
            if (mVideoEncoder!=NULL) {
                    mVideoEncoder->Close();
                    VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            }
            
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, mVideoOptions->videoRawType, VIDEO_ENCODE_PROFILE(mVideoOptions->videoProfile), (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
                
            mVideoEncoder->registerJavaVMEnv(mJvm);
            mVideoEncoder->setEncodeSurface(mEncodeSurface);
            ret = mVideoEncoder->Open();
        }
#endif

        if (!ret) {
            LOGE("Open VideoEncoder Fail");
            notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_OPEN_VIDEO_ENCODER_FAIL);
            
            modifyFlags(ERRORED, ASSIGN);
            gotError = true;
            stop_l();
            return;
        }
    }
    
    // set VideoEncoder OutputHandler
    if (mVideoOptions->hasVideo && mVideoEncoder!=NULL) {
        mVideoEncoder->SetOutputHandler(mMediaMuxer);
    }
    
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        mAudioStreamer->setMediaMuxer(mMediaMuxer);
    }
    
    pthread_mutex_lock(&mVideoDelayTimeUsLock);
    mVideoDelayTimeUs = 0;
    pthread_mutex_unlock(&mVideoDelayTimeUsLock);
    
    mMediaMuxer->start();
    
    if (!isPausedState) {
        play_l();
    }
    
    LOGD("SLKMediaStreamer RePublish Success");
}

void SLKMediaStreamer::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

#ifdef IOS
void SLKMediaStreamer::iOSAVAudioSessionInterruption(bool isInterrupting)
{
    if (mAudioOptions->hasAudio) {
        pthread_mutex_lock(&mAudioStreamerLock);
        if (mAudioStreamer!=NULL) {
            mAudioStreamer->iOSAVAudioSessionInterruption(isInterrupting);
        }
        pthread_mutex_unlock(&mAudioStreamerLock);
    }
}

void SLKMediaStreamer::iOSDidEnterBackground()
{
    if (mVideoOptions->hasVideo && mVideoEncoderType==VIDEO_TOOL_BOX) {
        pthread_mutex_lock(&mMuxerLock);
        if (mMediaMuxer!=NULL) {
            mMediaMuxer->interrupt();
        }
        pthread_mutex_unlock(&mMuxerLock);
        
        pthread_mutex_lock(&mInterruptLock);
        isInterrupt = true;
        pthread_mutex_unlock(&mInterruptLock);
        
        AutoLock autoLock(&mLock);
        
        pthread_mutex_lock(&mInterruptLock);
        isInterrupt = false;
        pthread_mutex_unlock(&mInterruptLock);
        
        if (mVideoEncoder!=NULL) {
            mVideoEncoder->Close();
            VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            mVideoEncoder = NULL;
        }
        mVideoEncoderType = X264;
        
        if (mTargetVideoFrame.data!=NULL) {
            free(mTargetVideoFrame.data);
            mTargetVideoFrame.data = NULL;
        }
        
        if (mVideoOptions->hasVideo && mVideoOptions->videoRawType!=VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF) {
            if (mVideoEncoderType==VIDEO_TOOL_BOX) {
                // NV12 Or BGRA
                mTargetVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*4);
                mTargetVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*4;
                mTargetVideoFrame.width = mVideoOptions->videoWidth;
                mTargetVideoFrame.height = mVideoOptions->videoHeight;
                mTargetVideoFrame.rotation = 0;
                mTargetVideoFrame.videoRawType = mVideoOptions->videoRawType;
            }else{
                mTargetVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2);
                mTargetVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2;
                mTargetVideoFrame.width = mVideoOptions->videoWidth;
                mTargetVideoFrame.height = mVideoOptions->videoHeight;
                mTargetVideoFrame.rotation = 0;
                mTargetVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_I420;
            }
        }
        
        isNeedResurrectiOSVTB = true;
        
        isPushImage = true;
        pushImageVideoFrame = NULL;
        
        postRePublishEvent_l();
    }
}

void SLKMediaStreamer::iOSDidBecomeActive()
{
    if (isNeedResurrectiOSVTB) {
        isNeedResurrectiOSVTB = false;
        
        pthread_mutex_lock(&mMuxerLock);
        if (mMediaMuxer!=NULL) {
            mMediaMuxer->interrupt();
        }
        pthread_mutex_unlock(&mMuxerLock);
        
        pthread_mutex_lock(&mInterruptLock);
        isInterrupt = true;
        pthread_mutex_unlock(&mInterruptLock);
        
        AutoLock autoLock(&mLock);
        
        pthread_mutex_lock(&mInterruptLock);
        isInterrupt = false;
        pthread_mutex_unlock(&mInterruptLock);
        
        if (mVideoEncoder!=NULL) {
            mVideoEncoder->Close();
            VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            mVideoEncoder = NULL;
        }
        mVideoEncoderType = VIDEO_TOOL_BOX;
        
        if (mTargetVideoFrame.data!=NULL) {
            free(mTargetVideoFrame.data);
            mTargetVideoFrame.data = NULL;
        }
        
        if (mVideoOptions->hasVideo && mVideoOptions->videoRawType!=VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF) {
            if (mVideoEncoderType==VIDEO_TOOL_BOX) {
                // NV12 Or BGRA
                mTargetVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*4);
                mTargetVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*4;
                mTargetVideoFrame.width = mVideoOptions->videoWidth;
                mTargetVideoFrame.height = mVideoOptions->videoHeight;
                mTargetVideoFrame.rotation = 0;
                mTargetVideoFrame.videoRawType = mVideoOptions->videoRawType;
            }else{
                mTargetVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2);
                mTargetVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2;
                mTargetVideoFrame.width = mVideoOptions->videoWidth;
                mTargetVideoFrame.height = mVideoOptions->videoHeight;
                mTargetVideoFrame.rotation = 0;
                mTargetVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_I420;
            }
        }
        
        isPushImage = false;
        pushImageVideoFrame = NULL;
        
        postRePublishEvent_l();
        return;
    }
    
    if (mVideoOptions->hasVideo) {
        pthread_mutex_lock(&mVideoDelayTimeUsLock);
        mVideoDelayTimeUs = 0;
        pthread_mutex_unlock(&mVideoDelayTimeUsLock);
    }
}
#endif

void SLKMediaStreamer::netWorkReachabilityNotify(int networkStatus)
{
    notify(MEDIA_STREAMER_COMMAND, MEDIA_STREAMER_COMMAND_NETWORK_REACHABILITY, networkStatus);
}

void SLKMediaStreamer::processNetWorkReachability(int networkStatus)
{
    if (!(mFlags & STREAMING) && !(mFlags & PAUSED)) {
        return;
    }
    
    if (networkStatus == NotReachable) {
        if (!isNotReachable) {
            if (mMediaMuxer) {
                mMediaMuxer->setListener(NULL);
            }
            pause_ll();
            modifyFlags(ERRORED, ASSIGN);

            isNotReachable = true;
            notifyListener_l(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_NETWORK_UNREACHABLE);
        }
    }else {
        if (isNotReachable) {
            isNotReachable = false;
            notifyListener_l(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_NETWORK_REACHABLE, networkStatus);
            
            postRePublishEvent_l();
        }
    }
}

// return -1 : Open VideoEncoder Fail
// return -2 : Open FFmpegMuxer  Fail
// return -3 : Open AudioCapture Fail
// return -4 : Open AudioEncoder Fail
// return -5 : Open AudioFilter  Fail
// return -6 : Open FFmpegMuxer  Exit
int SLKMediaStreamer::open_all_pipelines_l()
{
    LOGI("SLKMediaStreamer::open_all_pipelines_l Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::open_all_pipelines_l Enter");
    }
    
    if (mVideoOptions->hasVideo) {
        if (mVideoOptions->videoRawType!=VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF) {
            //open ColorSpaceConvert
            mColorSpaceConvert = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
        }
        //open VideoEncoder
        char log[1024];
        LOGI("mVideoOptions->videoBitRate:%d",mVideoOptions->videoBitRate);
        sprintf(log, "mVideoOptions->videoBitRate:%d",mVideoOptions->videoBitRate);
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
        mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, mVideoOptions->videoRawType, VIDEO_ENCODE_PROFILE(mVideoOptions->videoProfile), (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
#ifdef ANDROID
        mVideoEncoder->registerJavaVMEnv(mJvm);
        if (mVideoEncoderType==MEDIACODEC) {
            mVideoEncoder->setEncodeSurface(mEncodeSurface);
        }
#endif
        bool ret = mVideoEncoder->Open();
        
#ifdef IOS
        if (!ret && mVideoEncoderType==VIDEO_TOOL_BOX)
        {
            if (mVideoEncoder!=NULL) {
                mVideoEncoder->Close();
                VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            }
                        
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, mVideoOptions->videoRawType, VIDEO_ENCODE_PROFILE(mVideoOptions->videoProfile), (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
            
            ret = mVideoEncoder->Open();
        }
#endif
        
#ifdef ANDROID
        if (!ret && mVideoEncoderType==MEDIACODEC) {
            
            if (mVideoEncoder!=NULL) {
                mVideoEncoder->Close();
                VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            }
                        
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, mVideoOptions->videoRawType, VIDEO_ENCODE_PROFILE(mVideoOptions->videoProfile), (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
            
            mVideoEncoder->registerJavaVMEnv(mJvm);
            mVideoEncoder->setEncodeSurface(mEncodeSurface);
            ret = mVideoEncoder->Open();
        }
#endif

        if (!ret) {
            LOGE("Open VideoEncoder Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Open VideoEncoder Fail");
            }
            return -1;
        }
        LOGI("Open VideoEncoder Success");
        if (mMediaLog) {
            mMediaLog->writeLog("Open VideoEncoder Success");
        }
    }
    
    if (mMuxerFormat==RTMP || mMuxerFormat==RTSP) {
        int currentConnectTimes = 0;
        int reConnectRet = 0;
        while (currentConnectTimes<=mReconnectTimes) {
            pthread_mutex_lock(&mInterruptLock);
            if (isInterrupt) {
                isInterrupt = false;
                pthread_mutex_unlock(&mInterruptLock);
                reConnectRet = -2;
                break;
            }
            pthread_mutex_unlock(&mInterruptLock);
            
            //open MediaMuxer
            pthread_mutex_lock(&mMuxerLock);
            mMediaMuxer = MediaMuxer::CreateMediaMuxer(FFMPEG_STREAMER, mMuxerFormat, mPublishUrl, mMediaLog, mVideoOptions, mAudioOptions);
            pthread_mutex_unlock(&mMuxerLock);
            mMediaMuxer->setListener(this);
#ifdef ANDROID
            mMediaMuxer->registerJavaVMEnv(mJvm);
#endif
            LOGD("Prepareing MediaMuxer");
            int iRet = mMediaMuxer->prepare();
            reConnectRet = iRet;
            if (iRet>=0) {
                LOGD("Prepare FFmpegMuxer Success");
                if (mMediaLog) {
                    mMediaLog->writeLog("Prepare FFmpegMuxer Success");
                }
                break;
            }else{
                if (iRet==-2) {
                    break;
                }else{
                    if (mMediaMuxer!=NULL) {
                        mMediaMuxer->interrupt();
                        mMediaMuxer->stop();
                        
                        pthread_mutex_lock(&mMuxerLock);
                        MediaMuxer::DeleteMediaMuxer(mMediaMuxer, FFMPEG_STREAMER);
                        mMediaMuxer = NULL;
                        pthread_mutex_unlock(&mMuxerLock);
                    }
                    
                    LOGE("Prepare FFmpegMuxer Fail, Retry");
                    if (mMediaLog) {
                        mMediaLog->writeLog("Prepare FFmpegMuxer Fail, Retry");
                    }
                    currentConnectTimes++;
                    notifyListener_l(MEDIA_STREAMER_WARN, MEDIA_STREAMER_WARN_RECONNECTING, currentConnectTimes);
                    continue;
                }
            }
        }
        
        if (reConnectRet<0) {
            if (reConnectRet==-2) {
                LOGW("Immediate exit was requested");
                return -6;
            }else{
                LOGE("Prepare FFmpegMuxer Fail");
                if (mMediaLog) {
                    mMediaLog->writeLog("Prepare FFmpegMuxer Fail");
                }
                return -2;
            }
        }
    }else{
        //open MediaMuxer
        pthread_mutex_lock(&mMuxerLock);
        mMediaMuxer = MediaMuxer::CreateMediaMuxer(FFMPEG_STREAMER, mMuxerFormat, mPublishUrl, mMediaLog, mVideoOptions, mAudioOptions);
        pthread_mutex_unlock(&mMuxerLock);
        mMediaMuxer->setListener(this);
#ifdef ANDROID
        mMediaMuxer->registerJavaVMEnv(mJvm);
#endif
        LOGD("Prepareing MediaMuxer");
        int iRet = mMediaMuxer->prepare();
        if (iRet<0) {
            if (iRet==-2) {
                LOGW("Immediate exit was requested");
                return -6;
            }else{
                LOGE("Prepare FFmpegMuxer Fail");
                if (mMediaLog) {
                    mMediaLog->writeLog("Prepare FFmpegMuxer Fail");
                }
                return -2;
            }
        }
        LOGD("Prepare FFmpegMuxer Success");
        if (mMediaLog) {
            mMediaLog->writeLog("Prepare FFmpegMuxer Success");
        }
    }
    
    // set VideoEncoder OutputHandler
    if (mVideoOptions->hasVideo && mVideoEncoder!=NULL) {
        mVideoEncoder->SetOutputHandler(mMediaMuxer);
    }
    
    //open AudioStreamer
    if (mAudioOptions->hasAudio) {
        LOGD("Has Audio Streamer");
        pthread_mutex_lock(&mAudioStreamerLock);
        mAudioStreamer = AudioStreamer::CreateAudioStreamer(AUDIO_STREAMER_OWN, mAudioOptions, mMediaLog);
        pthread_mutex_unlock(&mAudioStreamerLock);

#ifdef ANDROID
        mAudioStreamer->registerJavaVMEnv(mJvm);
#endif
        mAudioStreamer->setListener(this);
        mAudioStreamer->setMediaMuxer(mMediaMuxer);
        mAudioStreamer->enableSilence(!isAudioEnabled);
        
        int iRet = mAudioStreamer->prepare();
        if (iRet==-1) {
            LOGE("Open AudioCapture Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Open AudioCapture Fail");
            }
            return -3;
        }
        if (iRet==-2) {
            LOGE("Open AudioEncoder Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Open AudioEncoder Fail");
            }
            return -4;
        }
        if (iRet==-3) {
            LOGE("Open AudioFilter Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Open AudioFilter Fail");
            }
            return -5;
        }
        LOGI("Open AudioStreamer Success");
        if (mMediaLog) {
            mMediaLog->writeLog("Open AudioStreamer Success");
        }
    }
    
    LOGI("SLKMediaStreamer::open_all_pipelines_l Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKMediaStreamer::open_all_pipelines_l Leave");
    }
    
    return 0;
}

void SLKMediaStreamer::close_all_pipelines_l()
{
    LOGD("close_all_pipelines_l Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("close_all_pipelines_l Enter");
    }
    
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        mAudioStreamer->stop();
        pthread_mutex_lock(&mAudioStreamerLock);
        AudioStreamer::DeleteAudioStreamer(mAudioStreamer, AUDIO_STREAMER_OWN);
        mAudioStreamer = NULL;
        pthread_mutex_unlock(&mAudioStreamerLock);
        LOGD("AudioStreamer Release Success");
        if (mMediaLog) {
            mMediaLog->writeLog("AudioStreamer Release Success");
        }
    }
    
    if (mVideoOptions->hasVideo && mVideoEncoder!=NULL) {
        mVideoEncoder->Close();
        LOGD("VideoEncoder Close Success");
        if (mMediaLog) {
            mMediaLog->writeLog("VideoEncoder Close Success");
        }
        VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
        mVideoEncoder = NULL;
        LOGD("VideoEncoder Release Success");
        if (mMediaLog) {
            mMediaLog->writeLog("VideoEncoder Release Success");
        }
    }
    
    if (mMediaMuxer!=NULL) {
        mMediaMuxer->stop();
        LOGD("MediaMuxer stop Success");
        if (mMediaLog) {
            mMediaLog->writeLog("MediaMuxer stop Success");
        }
        
        pthread_mutex_lock(&mMuxerLock);
        MediaMuxer::DeleteMediaMuxer(mMediaMuxer, FFMPEG_STREAMER);
        mMediaMuxer = NULL;
        pthread_mutex_unlock(&mMuxerLock);
        LOGD("MediaMuxer Release Success");
        if (mMediaLog) {
            mMediaLog->writeLog("MediaMuxer Release Success");
        }
    }

    if (mVideoOptions->hasVideo && mColorSpaceConvert!=NULL) {
        ColorSpaceConvert::DeleteColorSpaceConvert(mColorSpaceConvert, LIBYUV);
        mColorSpaceConvert = NULL;
        LOGD("ColorSpaceConvert Release Success");
        if (mMediaLog) {
            mMediaLog->writeLog("ColorSpaceConvert Release Success");
        }
    }
    
    LOGD("close_all_pipelines_l Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("close_all_pipelines_l Leave");
    }
}

// Android-SoftEncode
int SLKMediaStreamer::flowing_l()
{
    if (!mVideoOptions->hasVideo || mVideoEncoder==NULL) {
        return 0;
    }
    
#ifdef ANDROID
    if (mVideoEncoderType==MEDIACODEC) {
        bool ret = false;
        
        //encode
        ret = mVideoEncoder->Encode();
        JNIEnv* env = AndroidUtils::getJNIEnv(mJvm);
        env->CallVoidMethod(static_cast<jobject>(this->mEncodeSurfaceCore), this->mEncodeSurfaceCoreClassRequestRenderMethod, jlong(MediaTimer::GetNowMediaTimeMS()));
        
        if (!ret) {
            LOGE("Video Encode Fail!!");
            if (mMediaLog) {
                mMediaLog->writeLog("Video Encode Fail!!");
            }
            return -2;
        }
        
        //for fps statistics
        flowingFrameCountPerTime++;
        if (realfps_begin_time==0) {
            realfps_begin_time = MediaTimer::GetNowMediaTimeMS();
        }
        
        int64_t realfps_duration = MediaTimer::GetNowMediaTimeMS()-realfps_begin_time;
        if (realfps_duration>=1000) {
            mRealFps = flowingFrameCountPerTime*1000/realfps_duration;
            
            realfps_begin_time = 0;
            flowingFrameCountPerTime = 0;
            
            notifyListener_l(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS, mRealFps);
        }
        
        return 1;
    }
#endif

    if (isPushImage) {
        if (pushImageVideoFrame==NULL) {
            if (pushImageFile) {
                VideoFrame * imagevideoFrame = PNGImageFileToRGBAVideoFrame(pushImageFile);
                if (imagevideoFrame) {
                    bool ret = false;
                    if(imagevideoFrame->videoRawType==VIDEOFRAME_RAWTYPE_RGBA && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420){
                        ret = mColorSpaceConvert->ABGRtoI420_Crop_Scale(imagevideoFrame, &mTargetVideoFrame);
                    }else if(imagevideoFrame->videoRawType==VIDEOFRAME_RAWTYPE_RGBA && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_BGRA){
                        ret = mColorSpaceConvert->RGBAtoBGRA_Crop_Scale(imagevideoFrame, &mTargetVideoFrame);
                    }
                    
                    if (ret) {
                        pushImageVideoFrame = &mTargetVideoFrame;
                    }
                    
                    if (imagevideoFrame->data) {
                        free(imagevideoFrame->data);
                        imagevideoFrame->data = NULL;
                    }
                    
                    delete imagevideoFrame;
                    imagevideoFrame = NULL;
                }
            }
            
            if (pushImageVideoFrame==NULL) {
                if (mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420) {
                    memset(mTargetVideoFrame.data, 0, mTargetVideoFrame.width*mTargetVideoFrame.height);
                    memset(mTargetVideoFrame.data + mTargetVideoFrame.width*mTargetVideoFrame.height, 0x80, mTargetVideoFrame.width*mTargetVideoFrame.height/4);
                    memset(mTargetVideoFrame.data + mTargetVideoFrame.width*mTargetVideoFrame.height + mTargetVideoFrame.width*mTargetVideoFrame.height/4, 0x80, mTargetVideoFrame.width*mTargetVideoFrame.height/4);
                }else if(mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_BGRA) {
                    memset(mTargetVideoFrame.data, 0, mTargetVideoFrame.width*mTargetVideoFrame.height*3);
                    memset(mTargetVideoFrame.data+mTargetVideoFrame.width*mTargetVideoFrame.height*3, 255, mTargetVideoFrame.width*mTargetVideoFrame.height);
                }
                pushImageVideoFrame = &mTargetVideoFrame;
            }
        }
        
        //encode
        pushImageVideoFrame->pts = MediaTimer::GetNowMediaTimeMS();
        int ret = mVideoEncoder->Encode(*pushImageVideoFrame);
        
#ifdef IOS
        if (ret<0 && ret!=kVTInvalidSessionErr && mVideoEncoderType==VIDEO_TOOL_BOX)
        {
            LOGD("Video Encode Fail, Then Recreate VideoToolBox Encoder and Reencode");
            if (mMediaLog) {
                mMediaLog->writeLog("Video Encode Fail, Then Recreate VideoToolBox Encoder and Reencode");
            }
            if (mVideoEncoder!=NULL) {
                mVideoEncoder->Close();
                VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            }
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, mVideoOptions->videoRawType, VIDEO_ENCODE_PROFILE(mVideoOptions->videoProfile), (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
            if(mVideoEncoder->Open())
            {
                // set VideoEncoder OutputHandler
                mVideoEncoder->SetOutputHandler(mMediaMuxer);
                ret = mVideoEncoder->Encode(*pushImageVideoFrame);
            }
        }
        
        if (ret==kVTInvalidSessionErr) {
            return 0;
        }
#endif
        
        if (ret<0) {
            LOGE("Video Encode Fail!!");
            if (mMediaLog) {
                mMediaLog->writeLog("Video Encode Fail!!");
            }
            return -2;
        }
        
        //for fps statistics
        flowingFrameCountPerTime++;
        if (realfps_begin_time==0) {
            realfps_begin_time = MediaTimer::GetNowMediaTimeMS();
        }
        
        int64_t realfps_duration = MediaTimer::GetNowMediaTimeMS()-realfps_begin_time;
        if (realfps_duration>=1000) {
            mRealFps = flowingFrameCountPerTime*1000/realfps_duration;
            
            realfps_begin_time = 0;
            flowingFrameCountPerTime = 0;
            
            notifyListener_l(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS, mRealFps);
        }
        
        return 1;
    }
    
    //Get One Video Frame
    pthread_mutex_lock(&mVideoFrameBufferPoolLock);
    VideoFrame * videoFrame = mVideoFrameBufferPool->front();
    if (videoFrame==NULL)
    {
//        LOGD("VideoFrameBufferPool is empty");
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
        return 0;
    }
    pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    
    if (mVideoOptions->videoRawType!=VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF && isNeedConvertVideoFrame(videoFrame, &mTargetVideoFrame)) {
        //ColorSpace Convert
        bool r = false;
        if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV21 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420) {
            r = mColorSpaceConvert->NV21toI420_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, videoFrame->rotation);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV21 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_NV12){
            r = mColorSpaceConvert->NV21toNV12_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, videoFrame->rotation);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV21 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_NV21){
            r = mColorSpaceConvert->NV21toNV21_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, videoFrame->rotation);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_RGBA && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420){
//            ret = mColorSpaceConvert->ABGRtoI420_Crop(videoFrame, &mTargetVideoFrame);
            if (videoFrame->isKeepFullContent) {
                r = mColorSpaceConvert->ABGRtoI420_Scale(videoFrame, &mTargetVideoFrame);
            }else{
                r = mColorSpaceConvert->ABGRtoI420_Crop_Scale(videoFrame, &mTargetVideoFrame);
            }
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_BGRA && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_BGRA){
            r = mColorSpaceConvert->BGRAtoBGRA_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_BGRA && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420){
            r = mColorSpaceConvert->ARGBtoI420_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV12 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420){
            r = mColorSpaceConvert->NV12toI420_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, videoFrame->rotation);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_I420 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420){
            r = mColorSpaceConvert->I420toI420_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_I420 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_BGRA){
            r = mColorSpaceConvert->I420toBGRA_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_RGBA && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_NV21){
            r = mColorSpaceConvert->ARGBtoNV21_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV12 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_NV12){
            r = mColorSpaceConvert->NV12toNV12_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, videoFrame->rotation);
        }else {
            //todo
        }
        
        //pop the Video Frame
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        mVideoFrameBufferPool->pop();
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);

        if (!r) {
            LOGE("ColorSpaceConvert Fail!!");
            if (mMediaLog) {
                mMediaLog->writeLog("ColorSpaceConvert Fail!!");
            }
            return -1;
        }
        
        //encode
        int ret = mVideoEncoder->Encode(mTargetVideoFrame);
        
#ifdef IOS
        if (ret<0 && ret!=kVTInvalidSessionErr && mVideoEncoderType==VIDEO_TOOL_BOX)
        {
            LOGD("Video Encode Fail, Then Recreate VideoToolBox Encoder and Reencode");
            if (mMediaLog) {
                mMediaLog->writeLog("Video Encode Fail, Then Recreate VideoToolBox Encoder and Reencode");
            }
            
            if (mVideoEncoder!=NULL) {
                mVideoEncoder->Close();
                VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            }
            
//            mVideoEncoderType = X264;
            
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, mVideoOptions->videoRawType, VIDEO_ENCODE_PROFILE(mVideoOptions->videoProfile), (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
            
            if(mVideoEncoder->Open())
            {
                // set VideoEncoder OutputHandler
                mVideoEncoder->SetOutputHandler(mMediaMuxer);
                ret = mVideoEncoder->Encode(mTargetVideoFrame);
            }
        }
        
        if (ret==kVTInvalidSessionErr) {
            return 0;
        }
#endif
        
        if (ret<0) {
            LOGE("Video Encode Fail!!");
            if (mMediaLog) {
                mMediaLog->writeLog("Video Encode Fail!!");
            }
            return -2;
        }
        
    }else {
        //encode
        int ret = mVideoEncoder->Encode(*videoFrame);
        
#ifdef IOS
        if (ret<0 && ret!=kVTInvalidSessionErr && mVideoEncoderType==VIDEO_TOOL_BOX)
        {
            LOGD("Video Encode Fail, Then Recreate VideoToolBox Encoder and Reencode");
            if (mMediaLog) {
                mMediaLog->writeLog("Video Encode Fail, Then Recreate VideoToolBox Encoder and Reencode");
            }
            
            if (mVideoEncoder!=NULL) {
                mVideoEncoder->Close();
                VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            }
            
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, mVideoOptions->videoRawType, VIDEO_ENCODE_PROFILE(mVideoOptions->videoProfile), (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
            
            if(mVideoEncoder->Open())
            {
                // set VideoEncoder OutputHandler
                mVideoEncoder->SetOutputHandler(mMediaMuxer);
                ret = mVideoEncoder->Encode(*videoFrame);
            }
        }
#endif
        
        //pop the Video Frame
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        mVideoFrameBufferPool->pop();
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
        
#ifdef IOS
        if (ret==kVTInvalidSessionErr) {
            return 0;
        }
#endif
        
        if (ret<0) {
            LOGE("Video Encode Fail!!");
            if (mMediaLog) {
                mMediaLog->writeLog("Video Encode Fail!!");
            }
            return -2;
        }
    }
    
    //for fps statistics
    flowingFrameCountPerTime++;
    if (realfps_begin_time==0) {
        realfps_begin_time = MediaTimer::GetNowMediaTimeMS();
    }
    
    int64_t realfps_duration = MediaTimer::GetNowMediaTimeMS()-realfps_begin_time;
    if (realfps_duration>=1000) {
        mRealFps = flowingFrameCountPerTime*1000/realfps_duration;
        
        realfps_begin_time = 0;
        flowingFrameCountPerTime = 0;
        
        notifyListener_l(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS, mRealFps);
    }
    
    return 1;
}

bool SLKMediaStreamer::isNeedConvertVideoFrame(VideoFrame *sourceFrame, VideoFrame *targetFrame)
{
    if (sourceFrame->rotation!=0 || sourceFrame->width!=targetFrame->width || sourceFrame->height!=targetFrame->height || sourceFrame->videoRawType!=targetFrame->videoRawType) {
        return true;
    }
    
    return false;
}
