//
//  AudioStreamer.cpp
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "AudioStreamer.h"
#include "SLKAudioStreamer.h"

AudioStreamer* AudioStreamer::CreateAudioStreamer(AudioStreamerType type, AudioOptions *audioOptions, MediaLog* mediaLog)
{
    if (type==AUDIO_STREAMER_OWN) {
        return new SLKAudioStreamer(audioOptions, mediaLog);
    }
    
    return NULL;
}

void AudioStreamer::DeleteAudioStreamer(AudioStreamer *audioStreamer, AudioStreamerType type)
{
    if (type==AUDIO_STREAMER_OWN) {
        SLKAudioStreamer *slkAudioStreamer = (SLKAudioStreamer *)audioStreamer;
        delete slkAudioStreamer;
        slkAudioStreamer = NULL;
    }
}
