//
//  MediaStreamer.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaStreamer_h
#define MediaStreamer_h

#include <stdio.h>

#include "IMediaListener.h"

#include "MediaDataType.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include <map>

using namespace std;

enum MediaStreamerType
{
    MEDIA_STREAMER_SLK = 0,
    MEDIA_STREAMER_MULTISOURCE = 1,
    MEDIA_STREAMER_ANIMATEDIMAGE = 2,
};

enum NetworkStatus{
    NotReachable = 0,
    ReachableViaWiFi = 2,
    ReachableViaWWAN = 1
};

class MediaStreamer
{
public:
    virtual ~MediaStreamer() {}
    
#ifdef ANDROID
    static MediaStreamer* CreateMediaStreamer(MediaStreamerType type, JavaVM *jvm, const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions, int reConnectTimes);
#else
    static MediaStreamer* CreateMediaStreamer(MediaStreamerType type, const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions, char* backgroundPngCover, int reConnectTimes);
#endif
    static void DeleteMediaStreamer(MediaStreamer *mediaStreamer, MediaStreamerType type);
    
#ifdef ANDROID
    virtual void setEncodeSurfaceCore(void *encodeSurfaceCore) = 0;
    virtual void setEncodeSurface(void *surface) = 0;
    
    virtual void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event) = 0;
#endif

    virtual void setListener(void (*listener)(void*,int,int,int), void* arg) = 0;
    
    virtual void updateVideoSourceLayout(map<int, VideoSourceLayout*> *layoutDict) = 0;
    virtual void inputYUVVideoFrame(YUVVideoFrame* inVideoFrame, int sourceId) = 0;

    virtual bool inputVideoFrame(VideoFrame *inVideoFrame) = 0;
    virtual void inputYUVVideoFrame(YUVVideoFrame* inVideoFrame) = 0;
    
    virtual void inputAudioFrame(AudioFrame *inAudioFrame) = 0;
    virtual void inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType) = 0;
    
    virtual void inputText(char* text, int size) = 0;
    
    virtual void start() = 0;
    
    virtual void resume() = 0;
    virtual void pause() = 0;
    
    virtual void stop(bool isCancle = false) = 0;
    
    virtual void notify(int event, int ext1, int ext2) = 0;
    
    virtual void enableAudio(bool isEnable) = 0;
    
    virtual void mixBGM(int bgmId, char* bgmUrl, float volume, int numberOfLoops) = 0;
    virtual void playBGM() = 0;
    virtual void pauseBGM() = 0;
    virtual void stopBGM() = 0;
    virtual void setBGMVolume(float volume) = 0;
    virtual void mixSEM(int semId, char* semUrl, float volume, int numberOfLoops) = 0;
        
#ifdef IOS
    virtual void iOSAVAudioSessionInterruption(bool isInterrupting) = 0;
    
    virtual void iOSDidEnterBackground() = 0;
    virtual void iOSDidBecomeActive() = 0;
#endif
    // 0 : NotReachable
    // 1 : ReachableViaWWAN
    // 2 : ReachableViaWiFi
    virtual void netWorkReachabilityNotify(int networkStatus) = 0;
};

#endif /* MediaStreamer_h */
