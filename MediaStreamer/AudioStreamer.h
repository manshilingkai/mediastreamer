//
//  AudioStreamer.h
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef AudioStreamer_h
#define AudioStreamer_h

#include <stdio.h>
#include "IMediaListener.h"
#include "MediaMuxer.h"

enum AudioStreamerType
{
    AUDIO_STREAMER_OWN = 0,
    AUDIO_STREAMER_SYS = 1,
};

class AudioStreamer {
public:
    virtual ~AudioStreamer() {}
    
    static AudioStreamer* CreateAudioStreamer(AudioStreamerType type, AudioOptions *audioOptions, MediaLog* mediaLog);
    static void DeleteAudioStreamer(AudioStreamer *audioStreamer, AudioStreamerType type);
    
#ifdef ANDROID
    virtual void registerJavaVMEnv(JavaVM *jvm) = 0;
#endif
    
    virtual void setListener(IMediaListener *mediaListener) = 0;
    
    virtual void setMediaMuxer(MediaMuxer *mediaMuxer) = 0;
    
    virtual void inputAudioFrame(AudioFrame *inAudioFrame) = 0;
    virtual void inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType) = 0;
    
    // -1:Open AudioCapture Fail
    // -2:Open AudioEncoder Fail
    virtual int prepare() = 0;
    
    // -1:Start AudioCapture Fail
    virtual int start() = 0;
    
    // -1:Stop AudioCapture Fail
    virtual int pause() = 0;
    
    virtual void stop() = 0;
    
    virtual void enableSilence(bool isEnable) = 0;
    
    virtual void mixBGM(int bgmId, char* bgmUrl, float volume, int numberOfLoops) = 0;
    virtual void playBGM() = 0;
    virtual void pauseBGM() = 0;
    virtual void stopBGM() = 0;
    virtual void setBGMVolume(float volume) = 0;
    virtual void mixSEM(int semId, char* semUrl, float volume, int numberOfLoops) = 0;
    
#ifdef IOS
    virtual void iOSAVAudioSessionInterruption(bool isInterrupting) = 0;
#endif
};

#endif /* AudioStreamer_h */
