//
//  SLKAudioStreamer.cpp
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "SLKAudioStreamer.h"
#include "MediaLog.h"
#include "MediaTimer.h"
//------------------------------------------------------------------------------------------------------------------//

MixAudioResourceCommandQueue::MixAudioResourceCommandQueue()
{
    pthread_mutex_init(&mLock, NULL);
}

MixAudioResourceCommandQueue::~MixAudioResourceCommandQueue()
{
    flush();
    pthread_mutex_destroy(&mLock);
}

void MixAudioResourceCommandQueue::push(MixAudioResourceCommand *command)
{
    pthread_mutex_lock(&mLock);
    
    mMixAudioResourceCommandQueue.push(command);

    pthread_mutex_unlock(&mLock);
}

MixAudioResourceCommand* MixAudioResourceCommandQueue::pop()
{
    MixAudioResourceCommand *command = NULL;

    pthread_mutex_lock(&mLock);

    if (!mMixAudioResourceCommandQueue.empty()) {
        command = mMixAudioResourceCommandQueue.front();
        mMixAudioResourceCommandQueue.pop();
    }

    pthread_mutex_unlock(&mLock);

    return command;
}

void MixAudioResourceCommandQueue::flush()
{
    pthread_mutex_lock(&mLock);
    MixAudioResourceCommand *command = NULL;
    while (!mMixAudioResourceCommandQueue.empty()) {
        command = mMixAudioResourceCommandQueue.front();
        if (command!=NULL) {
            command->Free();
            delete command;
            command = NULL;
        }
        mMixAudioResourceCommandQueue.pop();
    }
    
    pthread_mutex_unlock(&mLock);
}

//--------------------------------------------------------------------------------------------------------------------------//

SLKAudioStreamer::SLKAudioStreamer(AudioOptions *audioOptions, MediaLog* mediaLog)
{
#ifdef ANDROID
    mJvm = NULL;
#endif
    
    mMediaLog = mediaLog;

    isRealTime = audioOptions->isRealTime;
    isExternalAudioInput = audioOptions->isExternalAudioInput;
    
    sampleRate = audioOptions->audioSampleRate;
    numChannels = audioOptions->audioNumChannels;
    bitRate = audioOptions->audioBitRate;
    
    mMediaListener = NULL;

    mAudioCapture = NULL;
    pthread_mutex_init(&mAudioCaptureLock, NULL);
    
    mAudioEncoder = NULL;
    
    mMediaMuxer = NULL;
    pthread_mutex_init(&mMediaMuxerLock, NULL);
    hasPushAudioHeaderToMediaMuxer = false;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    isBreakThread = false;
    isAudioStreaming = false;
    
    isEnableSilence = false;

    isEnableVirtualAudioCapture = false;
    pthread_mutex_init(&mVirtualAudioCaptureLock, NULL);
    
    mBGMAudioPlayer = NULL;
    mSEMAudioPlayer = NULL;
    
//    mVoiceActivityDetection = NULL;
}

SLKAudioStreamer::~SLKAudioStreamer()
{
    pthread_mutex_destroy(&mVirtualAudioCaptureLock);
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
        
    pthread_mutex_destroy(&mMediaMuxerLock);
    
    pthread_mutex_destroy(&mAudioCaptureLock);
}

#ifdef ANDROID
void SLKAudioStreamer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void SLKAudioStreamer::setListener(IMediaListener *mediaListener)
{
    mMediaListener = mediaListener;
}

void SLKAudioStreamer::setMediaMuxer(MediaMuxer *mediaMuxer)
{
    pthread_mutex_lock(&mMediaMuxerLock);
    mMediaMuxer = mediaMuxer;
    hasPushAudioHeaderToMediaMuxer = false;
    pthread_mutex_unlock(&mMediaMuxerLock);
}

void SLKAudioStreamer::inputAudioFrame(AudioFrame *inAudioFrame)
{
    if (isExternalAudioInput) {
        pthread_mutex_lock(&mAudioCaptureLock);
        if (mAudioCapture!=NULL) {
            mAudioCapture->inputAudioFrame(inAudioFrame);
        }
        pthread_mutex_unlock(&mAudioCaptureLock);
    }
}

void SLKAudioStreamer::inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType)
{
    if (isExternalAudioInput) {
        pthread_mutex_lock(&mAudioCaptureLock);
        if (mAudioCapture!=NULL) {
            mAudioCapture->inputAudioFrame(inAudioFrame, audioSourceType);
        }
        pthread_mutex_unlock(&mAudioCaptureLock);
    }
}

int SLKAudioStreamer::prepare()
{
    LOGI("SLKAudioStreamer::prepare");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKAudioStreamer::prepare");
    }
    
    isBreakThread = false;
    isAudioStreaming = false;
    
    createAudioStreamerThread();

    // open AudioEncoder
#if defined(ANDROID) || defined(MAC) || defined(WIN32)
    if (isRealTime) {
        audio_encoder_type = OPENCOREAMRNB;
    }else{
//        audio_encoder_type = FAAC;
        audio_encoder_type = FDK_AAC;
    }
#endif

#ifdef IOS
    if (isRealTime) {
        audio_encoder_type = OPENCOREAMRNB;
    }else{
        if (numChannels==1) {
            audio_encoder_type = AUDIO_TOOL_BOX;
        }else{
            audio_encoder_type = FDK_AAC;
        }        
    }
#endif
    
    mAudioEncoder = AudioEncoder::CreateAudioEncoder(audio_encoder_type, sampleRate, numChannels, bitRate);
    if (mAudioEncoder!=NULL) {
        if(!mAudioEncoder->Open())
        {
            LOGE("SLKAudioStreamer Open AudioEncoder Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("SLKAudioStreamer Open AudioEncoder Fail");
            }
            
            return -2;
        }
        LOGD("Open AudioEncoder Success");
        if (mMediaLog) {
            mMediaLog->writeLog("Open AudioEncoder Success");
        }
    }
    
    // open AudioCapture
#ifdef ANDROID
    if (isRealTime) {
        audio_capture_type = AUDIORECORD;
    }else{
        audio_capture_type = AUDIORECORD;//OPENSLES;
    }
#endif
    
#ifdef IOS
    audio_capture_type = AUDIOUNIT;
#endif
    
#ifdef MAC
    audio_capture_type = AUDIO_CAPTURE_MAC;
#endif

#ifdef WIN32
	audio_capture_type = AUDIO_CAPTURE_WIN_WAVE;
#endif
    
    if (isExternalAudioInput) {
        audio_capture_type = EXTERNAL_INPUT;
//#ifdef WIN32
//		audio_capture_type = DISCRETE_EXTERNAL_INPUT;
//#endif
    }
    
    pthread_mutex_lock(&mAudioCaptureLock);
    mAudioCapture = AudioCapture::CreateAudioCapture(audio_capture_type, NULL, sampleRate, numChannels);
    LOGD("[AudioEncoder FixedInputFrameSize]:%d",mAudioEncoder->GetFixedInputFrameSize());
    mAudioCapture->setRecBufSizeInBytes(mAudioEncoder->GetFixedInputFrameSize());
    pthread_mutex_unlock(&mAudioCaptureLock);
    if (mAudioCapture!=NULL) {
#ifdef ANDROID
        mAudioCapture->registerJavaVMEnv(mJvm);
#endif
        LOGD("Init AudioCapture");
        int ret = mAudioCapture->Init();
        if (ret) {
            LOGE("Init AudioCapture Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Init AudioCapture Fail");
            }
            
            return -1;
        }
        LOGD("Init AudioCapture Success");
        if (mMediaLog) {
            mMediaLog->writeLog("Init AudioCapture Success");
        }
    }
    
    mVirtualAudioCaptureFrame.frameSize = mAudioEncoder->GetFixedInputFrameSize();
    mVirtualAudioCaptureFrame.data = (uint8_t*)malloc(mVirtualAudioCaptureFrame.frameSize);
    memset(mVirtualAudioCaptureFrame.data, 0, mVirtualAudioCaptureFrame.frameSize);
    mVirtualAudioCaptureFrame.duration = float(mVirtualAudioCaptureFrame.frameSize*1000)/float(sampleRate*numChannels*2);
    
    AudioRenderConfigure configure;
    configure.channelCount = numChannels;
    configure.sampleRate = sampleRate;
    if (configure.channelCount<=1) {
        configure.channelLayout = AV_CH_LAYOUT_MONO;
    }else{
        configure.channelLayout = AV_CH_LAYOUT_STEREO;
    }
#ifdef ANDROID
    mBGMAudioPlayer = new AudioPlayer(mJvm, AUDIO_PLAYER_INTERNAL_RENDER_EXTERNAL_MIX, configure);
    mBGMAudioPlayer->setInternalListener(this);
    mSEMAudioPlayer = new AudioPlayer(mJvm, AUDIO_PLAYER_INTERNAL_RENDER_EXTERNAL_MIX, configure);
    mSEMAudioPlayer->setInternalListener(this);
#else
    mBGMAudioPlayer = new AudioPlayer(AUDIO_PLAYER_INTERNAL_RENDER_EXTERNAL_MIX, configure);
    mBGMAudioPlayer->setInternalListener(this);
    mSEMAudioPlayer = new AudioPlayer(AUDIO_PLAYER_INTERNAL_RENDER_EXTERNAL_MIX, configure);
    mSEMAudioPlayer->setInternalListener(this);
#endif
    
//    mVoiceActivityDetection = new VoiceActivityDetection();
//    bool ret = mVoiceActivityDetection->open();
//    if (!ret) {
//        delete mVoiceActivityDetection;
//        mVoiceActivityDetection = NULL;
//    }
    return 0;
}

int SLKAudioStreamer::start()
{
    pthread_mutex_lock(&mLock);
    if (isAudioStreaming) {
        pthread_mutex_unlock(&mLock);
        return 0;
    }
    pthread_mutex_unlock(&mLock);
    
    LOGI("SLKAudioStreamer::start");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKAudioStreamer::start");
    }
    
    int ret = mAudioCapture->StartRecording();
    if (ret) {
        LOGW("AudioCapture Start Fail");
        if (mMediaLog) {
            mMediaLog->writeLog("AudioCapture Start Fail");
        }
        
        pthread_mutex_lock(&mVirtualAudioCaptureLock);
        isEnableVirtualAudioCapture = true;
        pthread_mutex_unlock(&mVirtualAudioCaptureLock);
        
//        return -1;
    }
    
    MixAudioResourceCommand* mixAudioResourceCommand = new MixAudioResourceCommand;
    mixAudioResourceCommand->type = PLAY_ALL;
    mMixAudioResourceCommandQueue.push(mixAudioResourceCommand);
    
    pthread_mutex_lock(&mLock);
    isAudioStreaming = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    return ret;
}

int SLKAudioStreamer::pause()
{
    pthread_mutex_lock(&mLock);
    if (!isAudioStreaming) {
        pthread_mutex_unlock(&mLock);
        return 0;
    }
    pthread_mutex_unlock(&mLock);
    
    int ret = mAudioCapture->StopRecording();
    if (ret) {
        LOGE("AudioCapture Stop Fail");
        if (mMediaLog) {
            mMediaLog->writeLog("AudioCapture Stop Fail");
        }
        
        return -1;
    }
    
    MixAudioResourceCommand* mixAudioResourceCommand = new MixAudioResourceCommand;
    mixAudioResourceCommand->type = PAUSE_ALL;
    mMixAudioResourceCommandQueue.push(mixAudioResourceCommand);
    
    pthread_mutex_lock(&mLock);
    isAudioStreaming = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    return 0;
}


void SLKAudioStreamer::stop()
{
    deleteAudioStreamerThread();
    
    if (mAudioCapture!=NULL) {
        mAudioCapture->StopRecording();
        mAudioCapture->Terminate();
        pthread_mutex_lock(&mAudioCaptureLock);
        AudioCapture::DeleteAudioCapture(mAudioCapture, audio_capture_type);
        mAudioCapture = NULL;
        pthread_mutex_unlock(&mAudioCaptureLock);
    }
    
    if (mAudioEncoder!=NULL) {
        mAudioEncoder->Close();
        AudioEncoder::DeleteAudioEncoder(mAudioEncoder, audio_encoder_type);
        mAudioEncoder = NULL;
    }
    
    if (mVirtualAudioCaptureFrame.data) {
        free(mVirtualAudioCaptureFrame.data);
        mVirtualAudioCaptureFrame.data = NULL;
    }
    
    if (mBGMAudioPlayer) {
        mBGMAudioPlayer->stop();
        delete mBGMAudioPlayer;
        mBGMAudioPlayer = NULL;
    }
    
    if (mSEMAudioPlayer) {
        mSEMAudioPlayer->stop();
        delete mSEMAudioPlayer;
        mSEMAudioPlayer = NULL;
    }
    
//    if (mVoiceActivityDetection) {
//        delete mVoiceActivityDetection;
//        mVoiceActivityDetection = NULL;
//    }
}

void SLKAudioStreamer::enableSilence(bool isEnable)
{
    pthread_mutex_lock(&mLock);
    
    isEnableSilence = isEnable;
    
    if (isEnableSilence) {
        LOGD("Enable Silence");
        if (mMediaLog) {
            mMediaLog->writeLog("Enable Silence");
        }
    }else{
        LOGD("Disable Silence");
        if (mMediaLog) {
            mMediaLog->writeLog("Disable Silence");
        }
    }
    
    pthread_mutex_unlock(&mLock);
}

void SLKAudioStreamer::onMediaCallback(void* owner, int sourceId, int event, int ext1, int ext2)
{
    if (event==AUDIO_PLAYER_ERROR || event==AUDIO_PLAYER_PLAYBACK_COMPLETE) {
        if (mMediaListener!=NULL) {
            if (owner == mBGMAudioPlayer) {
                mMediaListener->notify(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_BGM_EOF, sourceId);
            }
            if (owner == mSEMAudioPlayer) {
                mMediaListener->notify(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_SEM_EOF, sourceId);
            }
        }
    }
}

void SLKAudioStreamer::mixBGM(int bgmId, char* bgmUrl, float volume, int numberOfLoops)
{
    if (bgmId<0) return;
    
    if (bgmUrl==NULL) return;
    
    if (volume<0) {
        volume = 0;
    }
    if (volume>=2.0f) {
        volume = 2.0f;
    }
    if (numberOfLoops<0) {
        numberOfLoops = -1;
    }
    
    MixAudioResourceCommandMixBgm* mixAudioResourceCommandMixBgm = new MixAudioResourceCommandMixBgm;
    mixAudioResourceCommandMixBgm->bgmId = bgmId;
    mixAudioResourceCommandMixBgm->url = strdup(bgmUrl);
    mixAudioResourceCommandMixBgm->volume = volume;
    mixAudioResourceCommandMixBgm->numberOfLoops = numberOfLoops;
    
    MixAudioResourceCommand* mixAudioResourceCommand = new MixAudioResourceCommand;
    mixAudioResourceCommand->type = MIX_BGM;
    mixAudioResourceCommand->content = mixAudioResourceCommandMixBgm;
    
    mMixAudioResourceCommandQueue.push(mixAudioResourceCommand);
}

void SLKAudioStreamer::playBGM()
{
    MixAudioResourceCommand* mixAudioResourceCommand = new MixAudioResourceCommand;
    mixAudioResourceCommand->type = PLAY_BGM;
    
    mMixAudioResourceCommandQueue.push(mixAudioResourceCommand);
}

void SLKAudioStreamer::pauseBGM()
{
    MixAudioResourceCommand* mixAudioResourceCommand = new MixAudioResourceCommand;
    mixAudioResourceCommand->type = PAUSE_BGM;
    
    mMixAudioResourceCommandQueue.push(mixAudioResourceCommand);
}

void SLKAudioStreamer::stopBGM()
{
    MixAudioResourceCommand* mixAudioResourceCommand = new MixAudioResourceCommand;
    mixAudioResourceCommand->type = STOP_BGM;
    
    mMixAudioResourceCommandQueue.push(mixAudioResourceCommand);
}

void SLKAudioStreamer::setBGMVolume(float volume)
{
    MixAudioResourceCommandSetBgmVolume* mixAudioResourceCommandSetBgmVolume = new MixAudioResourceCommandSetBgmVolume;
    mixAudioResourceCommandSetBgmVolume->volume = volume;
    
    MixAudioResourceCommand* mixAudioResourceCommand = new MixAudioResourceCommand;
    mixAudioResourceCommand->type = SET_BGM_VOLUME;
    mixAudioResourceCommand->content = mixAudioResourceCommandSetBgmVolume;
    
    mMixAudioResourceCommandQueue.push(mixAudioResourceCommand);
}

void SLKAudioStreamer::mixSEM(int semId, char* semUrl, float volume, int numberOfLoops)
{
    if (semId<0) return;
    
    if (semUrl==NULL) return;
    
    if (volume<0) {
        volume = 0;
    }
    if (volume>=2.0f) {
        volume = 2.0f;
    }
    if (numberOfLoops<0) {
        numberOfLoops = -1;
    }
    
    MixAudioResourceCommandMixSem* mixAudioResourceCommandMixSem = new MixAudioResourceCommandMixSem;
    mixAudioResourceCommandMixSem->semId = semId;
    mixAudioResourceCommandMixSem->url = strdup(semUrl);
    mixAudioResourceCommandMixSem->volume = volume;
    mixAudioResourceCommandMixSem->numberOfLoops = numberOfLoops;
    
    MixAudioResourceCommand* mixAudioResourceCommand = new MixAudioResourceCommand;
    mixAudioResourceCommand->type = MIX_SEM;
    mixAudioResourceCommand->content = mixAudioResourceCommandMixSem;
    
    mMixAudioResourceCommandQueue.push(mixAudioResourceCommand);
}

void SLKAudioStreamer::handleMixAudioResourceCommand()
{
    while (true) {
        MixAudioResourceCommand* mixAudioResourceCommand = mMixAudioResourceCommandQueue.pop();
        if (mixAudioResourceCommand==NULL) {
            break;
        }
        if (mixAudioResourceCommand->type==PLAY_ALL) {
            if (mBGMAudioPlayer) {
                mBGMAudioPlayer->play();
            }
            if (mSEMAudioPlayer) {
                mSEMAudioPlayer->play();
            }
        }else if(mixAudioResourceCommand->type==PAUSE_ALL) {
            if (mBGMAudioPlayer) {
                mBGMAudioPlayer->pause();
            }
            if (mSEMAudioPlayer) {
                mSEMAudioPlayer->pause();
            }
        }else if (mixAudioResourceCommand->type==MIX_BGM) {
            MixAudioResourceCommandMixBgm* mixAudioResourceCommandMixBgm = (MixAudioResourceCommandMixBgm*)mixAudioResourceCommand->content;
            if (mixAudioResourceCommandMixBgm) {
                if (mBGMAudioPlayer) {
                    mBGMAudioPlayer->stop();
                    mBGMAudioPlayer->setDataSourceWithId(mixAudioResourceCommandMixBgm->url, mixAudioResourceCommandMixBgm->bgmId);
                    if (mixAudioResourceCommandMixBgm->numberOfLoops<0) {
                        mBGMAudioPlayer->setLooping(true);
                    }
                    mBGMAudioPlayer->setVolume(mixAudioResourceCommandMixBgm->volume);
                    mBGMAudioPlayer->prepareAsyncToPlay();
                }
            }
        }else if (mixAudioResourceCommand->type==PLAY_BGM) {
            if (mBGMAudioPlayer) {
                mBGMAudioPlayer->play();
            }
        }else if (mixAudioResourceCommand->type==PAUSE_BGM) {
            if (mBGMAudioPlayer) {
                mBGMAudioPlayer->pause();
            }
        }else if (mixAudioResourceCommand->type==SET_BGM_VOLUME) {
            MixAudioResourceCommandSetBgmVolume* mixAudioResourceCommandSetBgmVolume = (MixAudioResourceCommandSetBgmVolume*)mixAudioResourceCommand->content;
            if (mixAudioResourceCommandSetBgmVolume) {
                if (mBGMAudioPlayer) {
                    mBGMAudioPlayer->setVolume(mixAudioResourceCommandSetBgmVolume->volume);
                }
            }
        }else if (mixAudioResourceCommand->type==STOP_BGM) {
            if (mBGMAudioPlayer) {
                mBGMAudioPlayer->stop();
            }
        }else if (mixAudioResourceCommand->type==MIX_SEM) {
            MixAudioResourceCommandMixSem* mixAudioResourceCommandMixSem = (MixAudioResourceCommandMixSem*)mixAudioResourceCommand->content;
            if (mixAudioResourceCommandMixSem) {
                if (mSEMAudioPlayer) {
                    mSEMAudioPlayer->stop();
                    mSEMAudioPlayer->setDataSourceWithId(mixAudioResourceCommandMixSem->url, mixAudioResourceCommandMixSem->semId);
                    mSEMAudioPlayer->setVolume(mixAudioResourceCommandMixSem->volume);
                    mSEMAudioPlayer->prepareAsyncToPlay();
                }
            }
        }
        
        mixAudioResourceCommand->Free();
        delete mixAudioResourceCommand;
        mixAudioResourceCommand = NULL;
    }
}

void SLKAudioStreamer::mixAudioResourceWithMicAudioFrame(AudioFrame* micAudioFrame)
{
    if (micAudioFrame==NULL) return;
    
    if (mBGMAudioPlayer) {
        mBGMAudioPlayer->mixWithExternal(micAudioFrame->data, micAudioFrame->frameSize);
    }
    
    if (mSEMAudioPlayer) {
        mSEMAudioPlayer->mixWithExternal(micAudioFrame->data, micAudioFrame->frameSize);
    }
}

void SLKAudioStreamer::createAudioStreamerThread()
{
#ifndef WIN32
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&mThread, &attr, handleAudioStreamerThread, this);
	pthread_attr_destroy(&attr);
#else
	pthread_create(&mThread, NULL, handleAudioStreamerThread, this);
#endif
}

void* SLKAudioStreamer::handleAudioStreamerThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    SLKAudioStreamer* audioStreamer = (SLKAudioStreamer*)ptr;
    audioStreamer->audioStreamerThreadMain();
    
    return NULL;
}

void SLKAudioStreamer::audioStreamerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env=NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    LOGD("SLKAudioStreamer::audioStreamerThreadMain");
    if (mMediaLog) {
        mMediaLog->writeLog("SLKAudioStreamer::audioStreamerThreadMain");
    }
    
    pthread_mutex_lock(&mLock);
    if (!isAudioStreaming) {
        pthread_cond_wait(&mCondition, &mLock);
    }
    pthread_mutex_unlock(&mLock);
    
    pthread_mutex_lock(&mMediaMuxerLock);
    if (!hasPushAudioHeaderToMediaMuxer) {
        if (isRealTime) {
            if (mMediaMuxer!=NULL && mAudioEncoder!=NULL) {
                mMediaMuxer->pushAMRHeader(mAudioEncoder->GetHeaders());
                hasPushAudioHeaderToMediaMuxer = true;
                
                LOGD("Push AMR Header to MediaMuxer");
                if (mMediaLog) {
                    mMediaLog->writeLog("Push AMR Header to MediaMuxer");
                }
            }
        }else{
            if (mMediaMuxer!=NULL && mAudioEncoder!=NULL) {
                mMediaMuxer->pushAACHeader(mAudioEncoder->GetHeaders());
                hasPushAudioHeaderToMediaMuxer = true;
                
                LOGD("Push AAC Header to MediaMuxer");
                if (mMediaLog) {
                    mMediaLog->writeLog("Push AAC Header to MediaMuxer");
                }
            }
        }
    }
    pthread_mutex_unlock(&mMediaMuxerLock);
    
    int detectedNoAudioFrameCount = 0;
    // loop
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        handleMixAudioResourceCommand();
        
        if (!isAudioStreaming) {
//            pthread_cond_wait(&mCondition, &mLock);
            int64_t reltime = 20 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        pthread_mutex_lock(&mMediaMuxerLock);
        if (!hasPushAudioHeaderToMediaMuxer) {
            if (isRealTime) {
                if (mMediaMuxer!=NULL && mAudioEncoder!=NULL) {
                    mMediaMuxer->pushAMRHeader(mAudioEncoder->GetHeaders());
                    hasPushAudioHeaderToMediaMuxer = true;
                    
                    LOGD("Push AMR Header to MediaMuxer");
                    if (mMediaLog) {
                        mMediaLog->writeLog("Push AMR Header to MediaMuxer");
                    }
                }
            }else{
                if (mMediaMuxer!=NULL && mAudioEncoder!=NULL) {
                    mMediaMuxer->pushAACHeader(mAudioEncoder->GetHeaders());
                    hasPushAudioHeaderToMediaMuxer = true;
                    
                    LOGD("Push AAC Header to MediaMuxer");
                    if (mMediaLog) {
                        mMediaLog->writeLog("Push AAC Header to MediaMuxer");
                    }
                }
            }
        }
        pthread_mutex_unlock(&mMediaMuxerLock);
        
        int ret = flowing_l();
        if (ret==-2) {
            LOGD("Audio Filter Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Audio Filter Fail");
            }
            
            if (mMediaListener!=NULL) {
                mMediaListener->notify(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_AUDIO_FILTER_FAIL, 0);
            }
            
            pthread_mutex_lock(&mLock);
            isAudioStreaming = false;
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else if (ret==-1) {
            LOGD("Audio Encode Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Audio Encode Fail");
            }
            
            if (mMediaListener!=NULL) {
                mMediaListener->notify(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_AUDIO_ENCODE_FAIL, 0);
            }
            
            pthread_mutex_lock(&mLock);
            isAudioStreaming = false;
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else if(ret==0) {
            detectedNoAudioFrameCount++;
            
            if (detectedNoAudioFrameCount>=500) {
                LOGD("Audio Input Data is Lost");
                if (mMediaLog) {
                    mMediaLog->writeLog("Audio Input Data is Lost");
                }
                if (mMediaListener!=NULL) {
                    mMediaListener->notify(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_AUDIO_INPUT_DATA_LOST, 0);
                }
                
                detectedNoAudioFrameCount = 0;
            }
            
            pthread_mutex_lock(&mLock);
            
            int64_t reltime = 10 * 1000 * 1000ll;
            struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
            pthread_mutex_unlock(&mLock);
            continue;
        }else{
            detectedNoAudioFrameCount = 0;
        }
    }
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

void SLKAudioStreamer::deleteAudioStreamerThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

#ifdef IOS
void SLKAudioStreamer::iOSAVAudioSessionInterruption(bool isInterrupting)
{
    pthread_mutex_lock(&mAudioCaptureLock);
    if (mAudioCapture) {
        mAudioCapture->iOSAVAudioSessionInterruption(isInterrupting);
    }
    pthread_mutex_unlock(&mAudioCaptureLock);
    
    if (isInterrupting) {
        LOGD("iOS AVAudioSession Interruption Began, So Switch To Virtual Audio Capture");
        if (mMediaLog) {
            mMediaLog->writeLog("iOS AVAudioSession Interruption Began, So Switch To Virtual Audio Capture");
        }
        pthread_mutex_lock(&mVirtualAudioCaptureLock);
        isEnableVirtualAudioCapture = true;
        pthread_mutex_unlock(&mVirtualAudioCaptureLock);
    }else{
        LOGD("iOS AVAudioSession Interruption Ended, So Switch To Normal Audio Capture");
        if (mMediaLog) {
            mMediaLog->writeLog("iOS AVAudioSession Interruption Ended, So Switch To Normal Audio Capture");
        }
        pthread_mutex_lock(&mVirtualAudioCaptureLock);
        isEnableVirtualAudioCapture = false;
        pthread_mutex_unlock(&mVirtualAudioCaptureLock);
    }
}
#endif

AudioFrame* SLKAudioStreamer::virtualAudioCapture_l()
{
    if (float(MediaTimer::GetNowMediaTimeMS() - mAudioPacket.pts) >= mVirtualAudioCaptureFrame.duration) {
        mVirtualAudioCaptureFrame.pts = mAudioPacket.pts+mVirtualAudioCaptureFrame.duration;
        return &mVirtualAudioCaptureFrame;
    }else{
        return NULL;
    }
}

int SLKAudioStreamer::flowing_l()
{
    pthread_mutex_lock(&mVirtualAudioCaptureLock);
    if (isEnableVirtualAudioCapture) {
        pthread_mutex_unlock(&mVirtualAudioCaptureLock);
        
        AudioFrame* audioFrame = virtualAudioCapture_l();
        if (audioFrame==NULL) {
            return 0;
        }
        
        if (isEnableSilence) {
            memset(audioFrame->data, 0, audioFrame->frameSize);
        }
        
        mixAudioResourceWithMicAudioFrame(audioFrame);
        
        bool bRet = mAudioEncoder->Encode(*audioFrame, mAudioPacket);
        
        if (!bRet) {
            return -1;
        }
        
        pthread_mutex_lock(&mMediaMuxerLock);
        if (mMediaMuxer!=NULL && hasPushAudioHeaderToMediaMuxer) {
            mMediaMuxer->pushAACBody(&mAudioPacket);
        }
        pthread_mutex_unlock(&mMediaMuxerLock);

        return 1;
    }else{
        pthread_mutex_unlock(&mVirtualAudioCaptureLock);
        
        AudioFrame* audioFrame = mAudioCapture->frontAudioFrame();
        if (audioFrame==NULL) {
            return 0;
        }
        
        if (isEnableSilence) {
            memset(audioFrame->data, 0, audioFrame->frameSize);
        }else{
//            int ret = mVoiceActivityDetection->process(sampleRate, numChannels, (char*)audioFrame->data, audioFrame->frameSize);
//            LOGD("VoiceActivityDetection Return : %d", ret);
        }
        
        mixAudioResourceWithMicAudioFrame(audioFrame);
        
        bool bRet = mAudioEncoder->Encode(*audioFrame, mAudioPacket);
        mAudioCapture->popAudioFrame();
        
        if (!bRet) {
            return -1;
        }
        
        pthread_mutex_lock(&mMediaMuxerLock);
        if (mMediaMuxer!=NULL && hasPushAudioHeaderToMediaMuxer) {
            mMediaMuxer->pushAACBody(&mAudioPacket);
        }
        pthread_mutex_unlock(&mMediaMuxerLock);

        return 1;
    }
}
