//
//  MultiSourceMediaStreamer.cpp
//  MediaStreamer
//
//  Created by Think on 2018/8/19.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "MultiSourceMediaStreamer.h"
#include "MediaLog.h"
#include "AutoLock.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#endif

#ifdef IOS
#include "iOSMediaListener.h"
#endif

#include "NormalMediaListener.h"

#include "Mediatimer.h"

#include "MediaFile.h"

struct MultiSourceMediaStreamerEvent : public TimedEventQueue::Event {
    MultiSourceMediaStreamerEvent(
                          MultiSourceMediaStreamer *streamer,
                          void (MultiSourceMediaStreamer::*method)())
    : mStreamer(streamer),
    mMethod(method) {
    }
    
protected:
    virtual ~MultiSourceMediaStreamerEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mStreamer->*mMethod)();
    }
    
private:
    MultiSourceMediaStreamer *mStreamer;
    void (MultiSourceMediaStreamer::*mMethod)();
    
    MultiSourceMediaStreamerEvent(const MultiSourceMediaStreamerEvent &);
    MultiSourceMediaStreamerEvent &operator=(const MultiSourceMediaStreamerEvent &);
};

#ifdef ANDROID
MultiSourceMediaStreamer::MultiSourceMediaStreamer(JavaVM *jvm, const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions)
{
    if(backupDir)
    {
        mBackupDir = strdup(backupDir);
    }else{
        mBackupDir = NULL;
    }
    
    if(mBackupDir)
    {
#ifdef LOG_MULTIPLE_FILE
        mMediaLog = new MediaLog(mBackupDir);
#else
        mMediaLog = MediaLog::getInstance(mBackupDir);
        mMediaLog->checkSize();
#endif
    }else{
        mMediaLog = NULL;
    }
    
    LOGI("MultiSourceMediaStreamer::MultiSourceMediaStreamer Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::MultiSourceMediaStreamer Enter");
    }
    
    mJvm = jvm;
    
    mVideoOptions = new VideoOptions;
    mAudioOptions = new AudioOptions;
    
    mVideoOptions->hasVideo = videoOptions.hasVideo;
    mVideoOptions->videoEncodeType = videoOptions.videoEncodeType;
    if (videoOptions.videoWidth%2==0) {
        mVideoOptions->videoWidth = videoOptions.videoWidth;
    }else{
        mVideoOptions->videoWidth = videoOptions.videoWidth + 1;
    }
    if (videoOptions.videoHeight%2==0) {
        mVideoOptions->videoHeight = videoOptions.videoHeight;
    }else{
        mVideoOptions->videoHeight = videoOptions.videoHeight + 1;
    }
    mVideoOptions->videoFps = videoOptions.videoFps;
    mVideoOptions->videoBitRate = videoOptions.videoBitRate;
    mVideoOptions->videoRawType = videoOptions.videoRawType;
    mVideoOptions->encodeMode = videoOptions.encodeMode;
    mVideoOptions->quality = videoOptions.quality;
    mVideoOptions->maxKeyFrameIntervalMs = videoOptions.maxKeyFrameIntervalMs;
    mVideoOptions->bStrictCBR = videoOptions.bStrictCBR;
    mVideoOptions->deblockingFilterFactor = videoOptions.deblockingFilterFactor;
    
    mAudioOptions->hasAudio = audioOptions.hasAudio;
    mAudioOptions->audioSampleRate = audioOptions.audioSampleRate;
    mAudioOptions->audioNumChannels = audioOptions.audioNumChannels;
    mAudioOptions->audioBitRate = audioOptions.audioBitRate;
    mAudioOptions->isExternalAudioInput = audioOptions.isExternalAudioInput;
    
    mPublishUrl = strdup(publishUrl);
    
    mMuxerFormat = MP4;
    if (!strncmp(mPublishUrl, "rtmp://", 7)) {
        mMuxerFormat = RTMP;
    }else if(!strncmp(mPublishUrl, "rtsp://", 7)) {
        mMuxerFormat = RTSP;
        mAudioOptions->isRealTime = true;
        mAudioOptions->audioSampleRate = 8000;
    }
    
    if (mVideoOptions->videoEncodeType==VIDEO_HARD_ENCODE) {
        mVideoEncoderType = MEDIACODEC;
    }else{
        mVideoEncoderType = X264;
    }
    
    if (mAudioOptions->isRealTime) {
        mVideoEncoderType = VP8;
    }
    
    mTargetVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2);
    mTargetVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2;
    mTargetVideoFrame.width = mVideoOptions->videoWidth;
    mTargetVideoFrame.height = mVideoOptions->videoHeight;
    mTargetVideoFrame.rotation = 0;
    mTargetVideoFrame.videoRawType = mVideoOptions->videoRawType;
    
    mMergeVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2);
    mMergeVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2;
    mMergeVideoFrame.width = mVideoOptions->videoWidth;
    mMergeVideoFrame.height = mVideoOptions->videoHeight;
    mMergeVideoFrame.rotation = 0;
    mMergeVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_I420;
    
    mMediaListener = NULL;
    
    mColorSpaceConvert = NULL;
    mVideoEncoder = NULL;
    mMediaMuxer = NULL;
    mAudioStreamer = NULL;
    
    pthread_mutex_init(&mLock, NULL);
    
    mAsyncPrepareEvent = NULL;
    mVideoEvent = NULL;
    mStopEvent = NULL;
    mControlBitrateEvent = NULL;
    
    mVideoEvent = new MultiSourceMediaStreamerEvent(this, &MultiSourceMediaStreamer::onVideoEvent);
    mVideoEventPending = false;
    mControlBitrateEvent = new MultiSourceMediaStreamerEvent(this, &MultiSourceMediaStreamer::onControlBitrateEvent);
    mNotifyEvent = new MultiSourceMediaStreamerEvent(this, &MultiSourceMediaStreamer::onNotifyEvent);
    mAsyncPrepareEvent = new MultiSourceMediaStreamerEvent(this, &MultiSourceMediaStreamer::onPrepareAsyncEvent);
    mStopEvent = new MultiSourceMediaStreamerEvent(this, &MultiSourceMediaStreamer::onStopEvent);
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();
    
    mVideoEventTimer_StartTime = 0;
    mVideoEventTimer_EndTime = 0;
    mVideoEventTime = 0;
    mVideoDelayTimeUs = 0;
    
    mTargetFps = mVideoOptions->videoFps;
    
    mPublishDelayTime = 0;
    
    mTargetVideoBitrate = mVideoOptions->videoBitRate;
    
    mFlags = 0;
    
    pthread_cond_init(&mStopCondition, NULL);
    
    isAudioEnabled = true;
    
    mRealFps = 0;
    flowingFrameCountPerTime = 0;
    realfps_begin_time=0;
    
    pthread_mutex_init(&mMuxerLock, NULL);
    pthread_mutex_init(&mAudioStreamerLock, NULL);
    
    pthread_mutex_init(&mVideoFrameBufferPoolLock, NULL);
    canInputVideoFrame = false;
    
    gotError = false;
    
    MediaTimer::open();
    
    LOGI("MultiSourceMediaStreamer::MultiSourceMediaStreamer Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::MultiSourceMediaStreamer Leave");
    }
}

#else
MultiSourceMediaStreamer::MultiSourceMediaStreamer(const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions)
{
    if(backupDir)
    {
        mBackupDir = strdup(backupDir);
    }else{
        mBackupDir = NULL;
    }
    
    if(mBackupDir)
    {
#ifdef LOG_MULTIPLE_FILE
        mMediaLog = new MediaLog(mBackupDir);
#else
        mMediaLog = MediaLog::getInstance(mBackupDir);
        mMediaLog->checkSize();
#endif
    }else{
        mMediaLog = NULL;
    }
    
    LOGI("MultiSourceMediaStreamer::MultiSourceMediaStreamer Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::MultiSourceMediaStreamer Enter");
    }
    
    mVideoOptions = new VideoOptions;
    mAudioOptions = new AudioOptions;
    
    mVideoOptions->hasVideo = videoOptions.hasVideo;
    mVideoOptions->videoEncodeType = videoOptions.videoEncodeType;
    if (videoOptions.videoWidth%2==0) {
        mVideoOptions->videoWidth = videoOptions.videoWidth;
    }else{
        mVideoOptions->videoWidth = videoOptions.videoWidth + 1;
    }
    if (videoOptions.videoHeight%2==0) {
        mVideoOptions->videoHeight = videoOptions.videoHeight;
    }else{
        mVideoOptions->videoHeight = videoOptions.videoHeight + 1;
    }
    mVideoOptions->videoFps = videoOptions.videoFps;
    mVideoOptions->videoBitRate = videoOptions.videoBitRate;
    mVideoOptions->videoRawType = videoOptions.videoRawType;
    
    mVideoOptions->encodeMode = videoOptions.encodeMode;
    mVideoOptions->quality = videoOptions.quality;
    mVideoOptions->maxKeyFrameIntervalMs = videoOptions.maxKeyFrameIntervalMs;
    mVideoOptions->bStrictCBR = videoOptions.bStrictCBR;
    mVideoOptions->deblockingFilterFactor = videoOptions.deblockingFilterFactor;
    
    mAudioOptions->hasAudio = audioOptions.hasAudio;
    mAudioOptions->audioSampleRate = audioOptions.audioSampleRate;
    mAudioOptions->audioNumChannels = audioOptions.audioNumChannels;
    mAudioOptions->audioBitRate = audioOptions.audioBitRate;
    mAudioOptions->isExternalAudioInput = audioOptions.isExternalAudioInput;
    
    mPublishUrl = strdup(publishUrl);
    
    mMuxerFormat = MP4;
    if (!strncmp(mPublishUrl, "rtmp://", 7)) {
        mMuxerFormat = RTMP;
    }else if(!strncmp(mPublishUrl, "rtsp://", 7)) {
        mMuxerFormat = RTSP;
        mAudioOptions->isRealTime = true;
        mAudioOptions->audioSampleRate = 8000;
    }
    
    if (mVideoOptions->videoEncodeType==VIDEO_HARD_ENCODE) {
#ifdef IOS
        mVideoEncoderType = VIDEO_TOOL_BOX;
#else
        mVideoEncoderType = X264;
#endif
    }else{
        mVideoEncoderType = X264;
    }
    
    if (mAudioOptions->isRealTime) {
        mVideoEncoderType = VP8;
    }
    
    if (mVideoEncoderType==VIDEO_TOOL_BOX) {
        mTargetVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*4);
        mTargetVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*4;
        mTargetVideoFrame.width = mVideoOptions->videoWidth;
        mTargetVideoFrame.height = mVideoOptions->videoHeight;
        mTargetVideoFrame.rotation = 0;
        mTargetVideoFrame.videoRawType = mVideoOptions->videoRawType;
    }else{
        mTargetVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2);
        mTargetVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2;
        mTargetVideoFrame.width = mVideoOptions->videoWidth;
        mTargetVideoFrame.height = mVideoOptions->videoHeight;
        mTargetVideoFrame.rotation = 0;
        mTargetVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_I420;
    }
    
    mMergeVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2);
    mMergeVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*3/2;
    mMergeVideoFrame.width = mVideoOptions->videoWidth;
    mMergeVideoFrame.height = mVideoOptions->videoHeight;
    mMergeVideoFrame.rotation = 0;
    mMergeVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_I420;
    
    mMediaListener = NULL;
    
    mColorSpaceConvert = NULL;
    mVideoEncoder = NULL;
    mMediaMuxer = NULL;
    mAudioStreamer = NULL;
    
    pthread_mutex_init(&mLock, NULL);
    
    mAsyncPrepareEvent = NULL;
    mVideoEvent = NULL;
    mStopEvent = NULL;
    mControlBitrateEvent = NULL;
    
    mVideoEvent = new MultiSourceMediaStreamerEvent(this, &MultiSourceMediaStreamer::onVideoEvent);
    mVideoEventPending = false;
    mControlBitrateEvent = new MultiSourceMediaStreamerEvent(this, &MultiSourceMediaStreamer::onControlBitrateEvent);
    mNotifyEvent = new MultiSourceMediaStreamerEvent(this, &MultiSourceMediaStreamer::onNotifyEvent);
    mAsyncPrepareEvent = new MultiSourceMediaStreamerEvent(this, &MultiSourceMediaStreamer::onPrepareAsyncEvent);
    mStopEvent = new MultiSourceMediaStreamerEvent(this, &MultiSourceMediaStreamer::onStopEvent);
    
    mQueue.start();
    
    mVideoEventTimer_StartTime = 0;
    mVideoEventTimer_EndTime = 0;
    mVideoEventTime = 0;
    mVideoDelayTimeUs = 0;
    
    mTargetFps = mVideoOptions->videoFps;
    
    mPublishDelayTime = 0;
    
    mTargetVideoBitrate = mVideoOptions->videoBitRate;
    
    mFlags = 0;
    
    pthread_cond_init(&mStopCondition, NULL);
    
    isAudioEnabled = true;
    
    mRealFps = 0;
    flowingFrameCountPerTime = 0;
    realfps_begin_time=0;
    
#ifdef IOS
    isNeedSwitchVideoEncoder = false;
#endif
    
    pthread_mutex_init(&mMuxerLock, NULL);
    pthread_mutex_init(&mAudioStreamerLock, NULL);
    
    pthread_mutex_init(&mVideoFrameBufferPoolLock, NULL);
    canInputVideoFrame = false;
    
    gotError = false;
    
    MediaTimer::open();
    
    LOGI("MultiSourceMediaStreamer::MultiSourceMediaStreamer Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::MultiSourceMediaStreamer Leave");
    }
}
#endif

MultiSourceMediaStreamer::~MultiSourceMediaStreamer()
{
    LOGI("MultiSourceMediaStreamer::~MultiSourceMediaStreamer Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::~MultiSourceMediaStreamer Enter");
    }
    
    mQueue.stop(true);
    mNotificationQueue.flush();
    
    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
    
    if (mVideoEvent!=NULL) {
        delete mVideoEvent;
        mVideoEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mControlBitrateEvent!=NULL) {
        delete mControlBitrateEvent;
        mControlBitrateEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    pthread_mutex_destroy(&mLock);
    
    for(map<int, VideoSourceLayout*>::iterator it = mLayoutDict.begin(); it != mLayoutDict.end(); ++it) {
        VideoSourceLayout* layout = it->second;
        if (layout) {
            delete layout;
        }
    }
    mLayoutDict.clear();
    
    for (map<int, VideoFrameBufferPool*>::iterator it = mVideoFrameBufferPoolDict.begin(); it != mVideoFrameBufferPoolDict.end(); ++it) {
        VideoFrameBufferPool* videoFrameBufferPool = it->second;
        if (videoFrameBufferPool) {
            delete videoFrameBufferPool;
        }
    }
    mVideoFrameBufferPoolDict.clear();
    
    mVideoPacket.Clear();
    
    if (mTargetVideoFrame.data!=NULL) {
        free(mTargetVideoFrame.data);
        mTargetVideoFrame.data = NULL;
    }
    
    if (mMergeVideoFrame.data!=NULL) {
        free(mMergeVideoFrame.data);
        mMergeVideoFrame.data = NULL;
    }
    
#ifdef ANDROID
    JniMediaListener *jniListener = (JniMediaListener*)mMediaListener;
    if (jniListener!=NULL) {
        delete jniListener;
        jniListener = NULL;
    }
#endif
    
#ifdef IOS
    iOSMediaListener *iOSListener = (iOSMediaListener*)mMediaListener;
    if (iOSListener!=NULL) {
        delete iOSListener;
        iOSListener = NULL;
    }
#endif
    
#ifdef MAC
    NormalMediaListener* normalMediaListener = (NormalMediaListener*)mMediaListener;
    if (normalMediaListener!=NULL) {
        delete normalMediaListener;
        normalMediaListener = NULL;
    }
#endif
    
    mMediaListener = NULL;
    
    if (mPublishUrl) {
        free(mPublishUrl);
        mPublishUrl = NULL;
    }
    
    if (mVideoOptions!=NULL) {
        delete mVideoOptions;
        mVideoOptions = NULL;
    }
    
    if (mAudioOptions!=NULL) {
        delete mAudioOptions;
        mAudioOptions = NULL;
    }
    
    pthread_cond_destroy(&mStopCondition);
    
    pthread_mutex_destroy(&mMuxerLock);
    pthread_mutex_destroy(&mAudioStreamerLock);
    
    pthread_mutex_destroy(&mVideoFrameBufferPoolLock);
    canInputVideoFrame = false;
        
    MediaTimer::close();
    
    LOGI("MultiSourceMediaStreamer::~MultiSourceMediaStreamer Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::~MultiSourceMediaStreamer Leave");
    }
    
#ifdef LOG_MULTIPLE_FILE
    if (mMediaLog) {
        delete mMediaLog;
        mMediaLog = NULL;
    }
#else
    if (mMediaLog) {
        mMediaLog->flush();
    }
    //    MediaLog::unrefInstance();
#endif
    
    if(mBackupDir)
    {
        free(mBackupDir);
        mBackupDir = NULL;
    }
}

#ifdef ANDROID
void MultiSourceMediaStreamer::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    LOGI("MultiSourceMediaStreamer::setListener");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::setListener");
    }
    
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    modifyFlags(INITIALIZED, SET);
}
#endif

void MultiSourceMediaStreamer::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);
    
#ifdef IOS
    mMediaListener = new iOSMediaListener(listener,arg);
#else
    mMediaListener = new NormalMediaListener(listener,arg);
#endif
    
    modifyFlags(INITIALIZED, SET);
}

void MultiSourceMediaStreamer::notifyListener_l(int event, int ext1, int ext2)
{
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void MultiSourceMediaStreamer::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    postNotifyEvent_l();
}

void MultiSourceMediaStreamer::postNotifyEvent_l()
{
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void MultiSourceMediaStreamer::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
    
    switch (event) {
        case MEDIA_STREAMER_ERROR:
            modifyFlags(ERRORED, ASSIGN);
            notifyListener_l(event, ext1, ext2);
            gotError = true;
            stop_l();
            break;
        case MEDIA_STREAMER_INFO:
            if (ext1==MEDIA_STREAMER_INFO_PUBLISH_DELAY_TIME) {
                mPublishDelayTime = ext2;
                postControlBitrateEvent_l();
            }
            notifyListener_l(event, ext1, ext2);
            break;
            
        default:
            notifyListener_l(event, ext1, ext2);
            break;
    }
    
}

void MultiSourceMediaStreamer::enableAudio(bool isEnable)
{
    AutoLock autoLock(&mLock);
    if (isAudioEnabled!=isEnable) {
        isAudioEnabled = isEnable;
        
        if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
            mAudioStreamer->enableSilence(!isAudioEnabled);
        }
    }
}

void MultiSourceMediaStreamer::updateVideoSourceLayout(map<int, VideoSourceLayout*> *layoutDict)
{
    pthread_mutex_lock(&mVideoFrameBufferPoolLock);
    
    for(map<int, VideoSourceLayout*>::iterator it = mLayoutDict.begin(); it != mLayoutDict.end(); ++it) {
        VideoSourceLayout* layout = it->second;
        if (layout) {
            delete layout;
        }
    }
    mLayoutDict.clear();
    
    for (map<int, VideoSourceLayout*>::iterator it = layoutDict->begin(); it != layoutDict->end(); ++it) {
        int sourceId = it->first;
        VideoSourceLayout* sourceLayout = it->second;
        
        VideoSourceLayout* layout = new VideoSourceLayout;
        *layout = *sourceLayout;
        
        mLayoutDict[sourceId] = layout;
        
        if (mVideoFrameBufferPoolDict.find(sourceId)==mVideoFrameBufferPoolDict.end())
        {
            mVideoFrameBufferPoolDict[sourceId] = new VideoFrameBufferPool;
        }
    }
    
    map<int, VideoFrameBufferPool*>::iterator it = mVideoFrameBufferPoolDict.begin();
    map<int, VideoFrameBufferPool*>::iterator it_erase;
    
    while (it != mVideoFrameBufferPoolDict.end()) {
        it_erase = it;
        it++;
        
        int sourceId = it_erase->first;
        if (mLayoutDict.find(sourceId)==mLayoutDict.end()) {
            VideoFrameBufferPool* videoFrameBufferPool = it->second;
            if (videoFrameBufferPool!=NULL) {
                delete videoFrameBufferPool;
            }
            
            mVideoFrameBufferPoolDict.erase(it_erase);
        }
    }
    
    pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
}

void MultiSourceMediaStreamer::inputYUVVideoFrame(YUVVideoFrame* inVideoFrame, int sourceId)
{
    if (inVideoFrame==NULL) return;
    
    if (mVideoOptions->hasVideo) {
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        
        if (canInputVideoFrame) {
            if (mVideoFrameBufferPoolDict.find(sourceId)!=mVideoFrameBufferPoolDict.end()) {
                VideoFrameBufferPool *videoFrameBufferPool = mVideoFrameBufferPoolDict[sourceId];
                if (videoFrameBufferPool!=NULL) {
                    inVideoFrame->pts = MediaTimer::GetNowMediaTimeMS();
                    videoFrameBufferPool->push(inVideoFrame);
                }
            }
        }
        
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    }
}

void MultiSourceMediaStreamer::inputAudioFrame(AudioFrame *inAudioFrame)
{
    if (inAudioFrame==NULL) return;
    
    if (mAudioOptions->hasAudio && mAudioOptions->isExternalAudioInput) {
        pthread_mutex_lock(&mAudioStreamerLock);
        if (mAudioStreamer!=NULL) {
            inAudioFrame->pts = MediaTimer::GetNowMediaTimeMS();
            mAudioStreamer->inputAudioFrame(inAudioFrame);
        }
        pthread_mutex_unlock(&mAudioStreamerLock);
    }
}

void MultiSourceMediaStreamer::inputText(char* text, int size)
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & STREAMING) {
        
        if(mMediaMuxer)
        {
            TextPacket textPacket;
            textPacket.data = (uint8_t*)text;
            textPacket.size = size;
            textPacket.pts = MediaTimer::GetNowMediaTimeMS();
            mMediaMuxer->pushTextData(&textPacket);
        }
        
        return;
    }else return;
}

void MultiSourceMediaStreamer::start()
{
    LOGI("MultiSourceMediaStreamer::start");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::start");
    }
    
    AutoLock autoLock(&mLock);
    
    if (mFlags & CONNECTING) {
        notifyListener_l(MEDIA_STREAMER_INFO,MEDIA_STREAMER_INFO_ALREADY_CONNECTING);
        
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    return prepareAsync_l();
}

void MultiSourceMediaStreamer::prepareAsync_l()
{
    LOGI("MultiSourceMediaStreamer::prepareAsync_l");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::prepareAsync_l");
    }
    
    modifyFlags(END, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(CONNECTING, SET);
    
    gotError = false;
    
    mQueue.postEvent(mAsyncPrepareEvent);
}

void MultiSourceMediaStreamer::onPrepareAsyncEvent()
{
    LOGI("MultiSourceMediaStreamer::onPrepareAsyncEvent");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::onPrepareAsyncEvent");
    }
    
    AutoLock autoLock(&mLock);
    
    //connecting
    notifyListener_l(MEDIA_STREAMER_CONNECTING);
    int ret = open_all_pipelines_l();
    if (ret>=0) {
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(CONNECTED, SET);
        
        mMediaMuxer->start();
        
        notifyListener_l(MEDIA_STREAMER_CONNECTED);
        
        play_l();
    }else {
        notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_CONNECT_FAIL);
        
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(ERRORED, SET);
        gotError = false;
        stop_l();
    }
}

void MultiSourceMediaStreamer::pause()
{
    AutoLock autoLock(&mLock);
    return pause_l();
}

void MultiSourceMediaStreamer::pause_l()
{
    if (!(mFlags & STREAMING)) {
        return;
    }
    
    modifyFlags(STREAMING, CLEAR);
    
    mQueue.cancelEvent(mVideoEvent->eventID());
    mVideoEventPending = false;
    
    //close audio data source
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        int ret = mAudioStreamer->pause();
        
        if (ret==-1) {
            
            notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_AUDIO_CAPTURE_STOP_FAIL);
            
            modifyFlags(ERRORED, SET);
            gotError = true;
            stop_l();
            
            return;
        }
    }
    
    if (mVideoOptions->hasVideo) {
        //close video data source
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        for (map<int, VideoFrameBufferPool*>::iterator it = mVideoFrameBufferPoolDict.begin(); it != mVideoFrameBufferPoolDict.end(); ++it) {
            VideoFrameBufferPool* videoFrameBufferPool = it->second;
            if (videoFrameBufferPool) {
                videoFrameBufferPool->flush();
            }
        }
        canInputVideoFrame = false;
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    }
    
    //Timing End
    MediaTimer::offLine();
    
    modifyFlags(PAUSED, SET);
    
    notifyListener_l(MEDIA_STREAMER_PAUSED);
    
    return;
}

void MultiSourceMediaStreamer::resume()
{
    AutoLock autoLock(&mLock);
    
    return play_l();
}


void MultiSourceMediaStreamer::play_l()
{
    LOGI("MultiSourceMediaStreamer::play_l Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::play_l Enter");
    }
    
    if (mFlags & STREAMING) {
        LOGW("%s","MultiSourceMediaStreamer is streaming");
        return;
    }
    
    if (!(mFlags & CONNECTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    modifyFlags(CONNECTED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    //Timing Begin
    MediaTimer::onLine();
    
    //open audio data source
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        int ret = mAudioStreamer->start();
        if (ret==-1) {
            
            notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_AUDIO_CAPTURE_START_FAIL);
            
            modifyFlags(ERRORED, SET);
            gotError = true;
            stop_l();
            
            return;
        }
    }
    
    if (mVideoOptions->hasVideo) {
        //open video data source
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        for (map<int, VideoFrameBufferPool*>::iterator it = mVideoFrameBufferPoolDict.begin(); it != mVideoFrameBufferPoolDict.end(); ++it) {
            VideoFrameBufferPool* videoFrameBufferPool = it->second;
            if (videoFrameBufferPool) {
                videoFrameBufferPool->flush();
            }
        }
        canInputVideoFrame = true;
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
        
        postVideoEvent_l();
    }
    
    modifyFlags(STREAMING, SET);
    
    notifyListener_l(MEDIA_STREAMER_STREAMING);
    
    LOGI("MultiSourceMediaStreamer::play_l Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::play_l Leave");
    }
}

void MultiSourceMediaStreamer::cancelStreamerEvents()
{
    mQueue.cancelEvent(mVideoEvent->eventID());
    mVideoEventPending = false;
    mQueue.cancelEvent(mControlBitrateEvent->eventID());
    mQueue.cancelEvent(mNotifyEvent->eventID());
    mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
    mQueue.cancelEvent(mStopEvent->eventID());
}

void MultiSourceMediaStreamer::stop(bool isCancle)
{
    LOGI("MultiSourceMediaStreamer::stop");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::stop");
    }
    
    pthread_mutex_lock(&mMuxerLock);
    if (mMediaMuxer!=NULL) {
        mMediaMuxer->interrupt();
    }
    pthread_mutex_unlock(&mMuxerLock);
    
    AutoLock autoLock(&mLock);
    
    if (mFlags & END) {
        return;
    }
    
    if (mFlags & ENDING) {
        notifyListener_l(MEDIA_STREAMER_INFO,MEDIA_STREAMER_INFO_ALREADY_ENDING);
        
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    if (isCancle) {
        gotError = true;
    }
    
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
    
    if (isCancle) {
        if (mMuxerFormat == MP4) {
            //delete local mp4 file
            if (MediaFile::isExist(mPublishUrl)) {
                MediaFile::deleteFile(mPublishUrl);
            }
        }
    }
}

void MultiSourceMediaStreamer::stop_l()
{
    LOGI("MultiSourceMediaStreamer::stop_l");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::stop_l");
    }
    
    modifyFlags(ENDING, ASSIGN);
    
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void MultiSourceMediaStreamer::postVideoEvent_l(int64_t delayUs)
{
    if (mVideoEventPending) {
        return;
    }
    mVideoEventPending = true;
    mQueue.postEventWithDelay(mVideoEvent, delayUs < 0 ? 0 : delayUs);
}

void MultiSourceMediaStreamer::postControlBitrateEvent_l()
{
    mQueue.postEventWithDelay(mControlBitrateEvent, 0);
}

void MultiSourceMediaStreamer::onControlBitrateEvent()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & STREAMING) {
        if (mPublishDelayTime) {
            
            int oldTargetVideoBitrate = mTargetVideoBitrate;
            
            if (mPublishDelayTime<1000 && mPublishDelayTime>=200) {
                return;
            }
            
            if (mPublishDelayTime>=1000) {
                mTargetVideoBitrate -= mPublishDelayTime / 10;
            }
            
            if (mPublishDelayTime<200) {
                mTargetVideoBitrate += 100;//+100k
            }
            
            if (mTargetVideoBitrate>=mVideoOptions->videoBitRate) {
                mTargetVideoBitrate = mVideoOptions->videoBitRate;
            }
            
            if (mTargetVideoBitrate<=100) {
                mTargetVideoBitrate = 100;
            }
            
            int upOrDownBitrate = mTargetVideoBitrate - oldTargetVideoBitrate;
            
            if (upOrDownBitrate>=100 || upOrDownBitrate<=-100) {
                mVideoEncoder->SetBitRate(mTargetVideoBitrate);
                mVideoEncoder->SetVBVBufferSize(mTargetVideoBitrate);
                mVideoEncoder->ReconfigEncodeParam();
            }
            
            if (upOrDownBitrate>=100) {
                notifyListener_l(MEDIA_STREAMER_INFO,MEDIA_STREAMER_INFO_PUBLISH_UP_BITRATE, mTargetVideoBitrate);
            }
            
            if (upOrDownBitrate<=-100) {
                notifyListener_l(MEDIA_STREAMER_INFO,MEDIA_STREAMER_INFO_PUBLISH_DOWN_BITRATE, mTargetVideoBitrate);
            }
            
            //drop frames
            if (mPublishDelayTime<5000) {
                mTargetFps = mVideoOptions->videoFps;
            }else if (mPublishDelayTime>=5000 && mPublishDelayTime<10000) {
                mTargetFps = mVideoOptions->videoFps-5;
            }else if (mPublishDelayTime>=10000 && mPublishDelayTime<20000) {
                mTargetFps = mVideoOptions->videoFps-10;
            }else if (mPublishDelayTime>=20000 && mPublishDelayTime<30000)
            {
                mTargetFps = mVideoOptions->videoFps-20;
            }else if (mPublishDelayTime>=30000)
            {
                mTargetFps = 1;
            }
            
            if (mTargetFps<1) {
                mTargetFps = 1;
            }
        }
    }
    
}

void MultiSourceMediaStreamer::onStopEvent()
{
    LOGI("MultiSourceMediaStreamer::onStopEvent");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::onStopEvent");
    }
    
    AutoLock autoLock(&mLock);
    
    close_all_pipelines_l();
    
    cancelStreamerEvents();
    mNotificationQueue.flush();
    
    //close video data source
    pthread_mutex_lock(&mVideoFrameBufferPoolLock);
    for (map<int, VideoFrameBufferPool*>::iterator it = mVideoFrameBufferPoolDict.begin(); it != mVideoFrameBufferPoolDict.end(); ++it) {
        VideoFrameBufferPool* videoFrameBufferPool = it->second;
        if (videoFrameBufferPool) {
            videoFrameBufferPool->flush();
        }
    }
    canInputVideoFrame = false;
    pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    
    //Timing End
    MediaTimer::offLine();
    
    modifyFlags(END, ASSIGN);
    
    if (!gotError) {
        notifyListener_l(MEDIA_STREAMER_END);
    }else{
        if (mMuxerFormat == MP4) {
            //delete local file
            if (MediaFile::isExist(mPublishUrl)) {
                MediaFile::deleteFile(mPublishUrl);
            }
        }
    }
    gotError = false;
    pthread_cond_broadcast(&mStopCondition);
}

void MultiSourceMediaStreamer::onVideoEvent()
{
    AutoLock autoLock(&mLock);
    
    mVideoEventTimer_StartTime = GetNowUs();
    
    if (!mVideoEventPending) {
        // The event has been cancelled in reset_l() but had already // been scheduled for execution at that time.
        return;
    }
    mVideoEventPending = false;
    
    int ret = flowing_l();
    
    mVideoEventTimer_EndTime = GetNowUs();
    
    mVideoEventTime = mVideoEventTimer_EndTime-mVideoEventTimer_StartTime;
    
    if (ret==0) {
        int64_t waitTimeUs = 0ll;
        if (mAudioOptions->isRealTime) {
            waitTimeUs = 1*1000; //1ms
        }else{
            waitTimeUs = 10*1000; //10ms
        }
        postVideoEvent_l(waitTimeUs);
        
        mVideoDelayTimeUs = mVideoDelayTimeUs + waitTimeUs + mVideoEventTime;
        
        return;
        
    }else if(ret<0) {
        modifyFlags(STREAMING, CLEAR);
        modifyFlags(ERRORED, SET);
        
        if(ret==-1)
        {
            notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_COLORSPACECONVERT_FAIL);
        }
        
        if(ret==-2)
        {
            notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_VIDEO_ENCODE_FAIL);
        }
        gotError = true;
        stop_l();
        
        return;
    }else {
        
        int64_t delayUs = 1000*1000/mTargetFps-mVideoEventTime-mVideoDelayTimeUs;
        
        if (delayUs>=0) {
            mVideoDelayTimeUs = 0;
        }else {
            mVideoDelayTimeUs = -delayUs;
        }
        
        postVideoEvent_l(delayUs);
    }
}

void MultiSourceMediaStreamer::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

#ifdef IOS
void MultiSourceMediaStreamer::openVideoChannel()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & ERRORED || mFlags & ENDING || mFlags & END)
    {
        return;
    }
    
    if (mVideoOptions->hasVideo) {
        //open video data source
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        if (mVideoFrameBufferPool!=NULL) {
            if(!canInputVideoFrame)
            {
                mVideoFrameBufferPool->flush();
                canInputVideoFrame = true;
            }
        }
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
        
        if (mVideoEncoder==NULL) {
            
            //open VideoEncoder
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, BASE_LINE, (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
            
            bool ret = mVideoEncoder->Open();
            
            if (!ret) {
                LOGE("Open VideoEncoder Fail");
                mVideoEncoder->Close();
                VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
                mVideoEncoder = NULL;
                
                notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_OPEN_VIDEO_ENCODER_FAIL);
                
                modifyFlags(ERRORED, SET);
                gotError = true;
                stop_l();
                
                return;
            }
            
            // set VideoEncoder OutputHandler
            if (mVideoOptions->hasVideo) {
                mVideoEncoder->SetOutputHandler(mMediaMuxer);
            }
        }
        
        mQueue.cancelEvent(mVideoEvent->eventID());
        mVideoEventPending = false;
        
        postVideoEvent_l();
    }
}

void MultiSourceMediaStreamer::closeVideoChannel()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & ERRORED || mFlags & ENDING || mFlags & END)
    {
        return;
    }
    
    if (mVideoOptions->hasVideo) {
        //close video data source
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        if (mVideoFrameBufferPool!=NULL) {
            mVideoFrameBufferPool->flush();
            canInputVideoFrame = false;
        }
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    }
    
    if (mVideoOptions->hasVideo && mVideoEncoder!=NULL) {
        mVideoEncoder->Close();
        VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
        mVideoEncoder = NULL;
    }
    
    mQueue.cancelEvent(mVideoEvent->eventID());
    mVideoEventPending = false;
}

void MultiSourceMediaStreamer::setVideoEncodeType(int videoEncodeType)
{
    AutoLock autoLock(&mLock);
    
    if (videoEncodeType==VIDEO_HARD_ENCODE) {
        if(mVideoEncoderType == VIDEO_TOOL_BOX)
        {
            isNeedSwitchVideoEncoder = false;
        }else{
            isNeedSwitchVideoEncoder = true;
        }
    }else{
        if(mVideoEncoderType == VIDEO_TOOL_BOX)
        {
            isNeedSwitchVideoEncoder = true;
        }else{
            isNeedSwitchVideoEncoder = false;
        }
    }
}

#endif

// return -1 : Open VideoEncoder Fail
// return -2 : Open FFmpegMuxer  Fail
// return -3 : Open AudioCapture Fail
// return -4 : Open AudioEncoder Fail
// return -5 : Open AudioFilter  Fail
int MultiSourceMediaStreamer::open_all_pipelines_l()
{
    LOGI("MultiSourceMediaStreamer::open_all_pipelines_l");
    if (mMediaLog) {
        mMediaLog->writeLog("MultiSourceMediaStreamer::open_all_pipelines_l");
    }
    
    if (mVideoOptions->hasVideo) {
        //open ColorSpaceConvert
        mColorSpaceConvert = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
        
        //open VideoEncoder
        LOGI("mVideoOptions->videoBitRate:%d",mVideoOptions->videoBitRate);
        mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, BASE_LINE, (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
#ifdef ANDROID
        mVideoEncoder->registerJavaVMEnv(mJvm);
#endif
        bool ret = mVideoEncoder->Open();
        
#ifdef IOS
        if (!ret && mVideoEncoderType==VIDEO_TOOL_BOX)
        {
            if (mVideoEncoder!=NULL) {
                mVideoEncoder->Close();
                VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            }
            
            mVideoEncoderType = X264;
            
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, BASE_LINE, (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
            
            ret = mVideoEncoder->Open();
        }
#endif
        
#ifdef ANDROID
        if (!ret && mVideoEncoderType==MEDIACODEC) {
            
            if (mVideoEncoder!=NULL) {
                mVideoEncoder->Close();
                VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            }
            
            mVideoEncoderType = X264;
            
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, BASE_LINE, (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
            
            mVideoEncoder->registerJavaVMEnv(mJvm);
            ret = mVideoEncoder->Open();
        }
#endif
        
#ifdef IOS
        isNeedSwitchVideoEncoder = false;
#endif
        
        if (!ret) {
            LOGE("Open VideoEncoder Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Open VideoEncoder Fail");
            }
            return -1;
        }
        LOGI("Open VideoEncoder Success");
        if (mMediaLog) {
            mMediaLog->writeLog("Open VideoEncoder Success");
        }
    }
    
    //open MediaMuxer
    pthread_mutex_lock(&mMuxerLock);
    mMediaMuxer = MediaMuxer::CreateMediaMuxer(FFMPEG_STREAMER, mMuxerFormat, mPublishUrl, mMediaLog, mVideoOptions, mAudioOptions);
    pthread_mutex_unlock(&mMuxerLock);
    mMediaMuxer->setListener(this);
#ifdef ANDROID
    mMediaMuxer->registerJavaVMEnv(mJvm);
#endif
    bool ret = mMediaMuxer->prepare();
    if (!ret) {
        LOGE("Open FFmpegMuxer Fail");
        if (mMediaLog) {
            mMediaLog->writeLog("Open FFmpegMuxer Fail");
        }
        return -2;
    }
    LOGI("Open FFmpegMuxer Success");
    if (mMediaLog) {
        mMediaLog->writeLog("Open FFmpegMuxer Success");
    }
    
    // set VideoEncoder OutputHandler
    if (mVideoOptions->hasVideo) {
        mVideoEncoder->SetOutputHandler(mMediaMuxer);
    }
    
    //open AudioStreamer
    if (mAudioOptions->hasAudio) {
        pthread_mutex_lock(&mAudioStreamerLock);
        mAudioStreamer = AudioStreamer::CreateAudioStreamer(AUDIO_STREAMER_OWN, mAudioOptions, mMediaLog);
        pthread_mutex_unlock(&mAudioStreamerLock);
        
#ifdef ANDROID
        mAudioStreamer->registerJavaVMEnv(mJvm);
#endif
        mAudioStreamer->setListener(this);
        mAudioStreamer->setMediaMuxer(mMediaMuxer);
        mAudioStreamer->enableSilence(!isAudioEnabled);
        
        int iRet = mAudioStreamer->prepare();
        if (iRet==-1) {
            LOGE("Open AudioCapture Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Open AudioCapture Fail");
            }
            return -3;
        }
        if (iRet==-2) {
            LOGE("Open AudioEncoder Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Open AudioEncoder Fail");
            }
            return -4;
        }
        if (iRet==-3) {
            LOGE("Open AudioFilter Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Open AudioFilter Fail");
            }
            return -5;
        }
        LOGI("Open AudioStreamer Success");
        if (mMediaLog) {
            mMediaLog->writeLog("Open AudioStreamer Success");
        }
    }
    
    return 0;
}

void MultiSourceMediaStreamer::close_all_pipelines_l()
{
    LOGD("close_all_pipelines_l");
    if (mMediaLog) {
        mMediaLog->writeLog("close_all_pipelines_l");
    }
    
    if (mAudioOptions->hasAudio && mAudioStreamer!=NULL) {
        mAudioStreamer->stop();
        pthread_mutex_lock(&mAudioStreamerLock);
        AudioStreamer::DeleteAudioStreamer(mAudioStreamer, AUDIO_STREAMER_OWN);
        mAudioStreamer = NULL;
        pthread_mutex_unlock(&mAudioStreamerLock);
        LOGD("AudioStreamer Release Success");
        if (mMediaLog) {
            mMediaLog->writeLog("AudioStreamer Release Success");
        }
    }
    
    if (mVideoOptions->hasVideo && mVideoEncoder!=NULL) {
        mVideoEncoder->Close();
        LOGD("VideoEncoder Close Success");
        VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
        mVideoEncoder = NULL;
        LOGD("VideoEncoder Release Success");
        if (mMediaLog) {
            mMediaLog->writeLog("VideoEncoder Release Success");
        }
    }
    
    if (mMediaMuxer!=NULL) {
        mMediaMuxer->stop();
        LOGD("MediaMuxer stop Success");
        if (mMediaLog) {
            mMediaLog->writeLog("MediaMuxer stop Success");
        }
        
        pthread_mutex_lock(&mMuxerLock);
        MediaMuxer::DeleteMediaMuxer(mMediaMuxer, FFMPEG_STREAMER);
        mMediaMuxer = NULL;
        pthread_mutex_unlock(&mMuxerLock);
        LOGD("MediaMuxer Release Success");
        if (mMediaLog) {
            mMediaLog->writeLog("MediaMuxer Release Success");
        }
    }
    
    if (mVideoOptions->hasVideo && mColorSpaceConvert!=NULL) {
        ColorSpaceConvert::DeleteColorSpaceConvert(mColorSpaceConvert, LIBYUV);
        mColorSpaceConvert = NULL;
        LOGD("ColorSpaceConvert Release Success");
        if (mMediaLog) {
            mMediaLog->writeLog("ColorSpaceConvert Release Success");
        }
    }
}

VideoFrame* MultiSourceMediaStreamer::mergeVideoSource_l()
{
    memset(mMergeVideoFrame.data, 0, mMergeVideoFrame.width*mMergeVideoFrame.height);
    memset(mMergeVideoFrame.data + mMergeVideoFrame.width*mMergeVideoFrame.height, 0x80, mMergeVideoFrame.width*mMergeVideoFrame.height/4);
    memset(mMergeVideoFrame.data + mMergeVideoFrame.width*mMergeVideoFrame.height + mMergeVideoFrame.width*mMergeVideoFrame.height/4, 0x80, mMergeVideoFrame.width*mMergeVideoFrame.height/4);
    
    int maxLayers = mLayoutDict.size();
    for (int i = 0; i<maxLayers; i++) {
        for(map<int, VideoSourceLayout*>::iterator it = mLayoutDict.begin(); it != mLayoutDict.end(); ++it) {
            VideoSourceLayout* layout = it->second;
            if (layout) {
                if (layout->Zorder == i) {
                    int sourceId = it->first;
                    if (mVideoFrameBufferPoolDict.find(sourceId)!=mVideoFrameBufferPoolDict.end()) {
                        VideoFrameBufferPool* videoFrameBufferPool = mVideoFrameBufferPoolDict[sourceId];
                        if (videoFrameBufferPool) {
                            VideoFrame* sourceVideoFrame = videoFrameBufferPool->front();
                            if (sourceVideoFrame) {
                                
                                if (mMergeVideoFrame.width-sourceVideoFrame->width<0) {
                                    LOGE("mMergeVideoFrame Width < sourceVideoFrame Width");
                                    if (mMediaLog) {
                                        mMediaLog->writeLog("MergeVideoFrame Width < sourceVideoFrame Width");
                                    }
                                    break;
                                }
                                
                                if (mMergeVideoFrame.height-sourceVideoFrame->height<0) {
                                    LOGE("mMergeVideoFrame Height < sourceVideoFrame Height");
                                    if (mMediaLog) {
                                        mMediaLog->writeLog("mMergeVideoFrame Height < sourceVideoFrame Height");
                                    }
                                    break;
                                }
                                
                                int x = layout->x;
                                int y = layout->y;
                                
                                if (x<0) {
                                    x = 0;
                                }
                                
                                if (y<0) {
                                    y = 0;
                                }
                                
                                if (x>mMergeVideoFrame.width-sourceVideoFrame->width) {
                                    x = mMergeVideoFrame.width-sourceVideoFrame->width;
                                }
                                
                                if (y>mMergeVideoFrame.height-sourceVideoFrame->height) {
                                    y = mMergeVideoFrame.height-sourceVideoFrame->height;
                                }
                                
                                if (x%2!=0) {
                                    x = x-1;
                                }
                                
                                if (y%2!=0) {
                                    y = y-1;
                                }
                                
                                if (x<0) {
                                    x = 0;
                                }
                                
                                if (y<0) {
                                    y = 0;
                                }
                                
                                //Y
                                int Y_offset_x = x;
                                int Y_offset_y = y;
                                //U
                                int U_offset_x = Y_offset_x/2;
                                int U_offset_y = Y_offset_y/2;
                                //V
                                int V_offset_x = Y_offset_x/2;
                                int V_offset_y = Y_offset_y/2;
                                
                                //Y
                                for (int i = 0; i<sourceVideoFrame->height; i++) {
                                    memcpy(mMergeVideoFrame.data + (Y_offset_y+i)*mMergeVideoFrame.width + Y_offset_x, sourceVideoFrame->data + i*sourceVideoFrame->width, sourceVideoFrame->width);
                                }
                                
                                //U
                                for (int i = 0; i<sourceVideoFrame->height/2; i++) {
                                    memcpy(mMergeVideoFrame.data + mMergeVideoFrame.width*mMergeVideoFrame.height + (U_offset_y+i)*mMergeVideoFrame.width/2 + U_offset_x, sourceVideoFrame->data+sourceVideoFrame->width*sourceVideoFrame->height + i*sourceVideoFrame->width/2, sourceVideoFrame->width/2);
                                }
                                
                                //V
                                for (int i = 0; i<sourceVideoFrame->height/2; i++) {
                                    memcpy(mMergeVideoFrame.data+ mMergeVideoFrame.width*mMergeVideoFrame.height + mMergeVideoFrame.width*mMergeVideoFrame.height/4 + (V_offset_y+i)*mMergeVideoFrame.width/2 + V_offset_x, sourceVideoFrame->data+sourceVideoFrame->width*sourceVideoFrame->height+sourceVideoFrame->width*sourceVideoFrame->height/4+i*sourceVideoFrame->width/2, sourceVideoFrame->width/2);
                                }
                                
                                if (!sourceVideoFrame->isLastVideoFrame) {
                                    videoFrameBufferPool->pop();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    
    return &mMergeVideoFrame;
}

// Android-SoftEncode
int MultiSourceMediaStreamer::flowing_l()
{
    //Get One Video Frame
    pthread_mutex_lock(&mVideoFrameBufferPoolLock);
    VideoFrame * videoFrame = mergeVideoSource_l();
    if (videoFrame==NULL)
    {
        //        LOGD("VideoFrameBufferPool is empty");
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
        return 0;
    }
    pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    
    if (isNeedConvertVideoFrame(videoFrame, &mTargetVideoFrame)) {
        //ColorSpace Convert
        bool ret = false;
        
        if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV21 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420) {
            ret = mColorSpaceConvert->NV21toI420_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, videoFrame->rotation);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV21 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_NV12){
            ret = mColorSpaceConvert->NV21toNV12_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, videoFrame->rotation);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV21 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_NV21){
            ret = mColorSpaceConvert->NV21toNV21_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, videoFrame->rotation);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_RGBA && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420){
            ret = mColorSpaceConvert->ABGRtoI420_Crop(videoFrame, &mTargetVideoFrame);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_BGRA && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_BGRA){
            ret = mColorSpaceConvert->BGRAtoBGRA_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_BGRA && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420){
            ret = mColorSpaceConvert->ARGBtoI420_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV12 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420){
            ret = mColorSpaceConvert->NV12toI420_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, videoFrame->rotation);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_I420 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420){
            ret = mColorSpaceConvert->I420toI420_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_I420 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_BGRA){
            ret = mColorSpaceConvert->I420toBGRA_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else {
            //todo
        }
        
        if (!ret) {
            LOGE("ColorSpaceConvert Fail!!");
            if (mMediaLog) {
                mMediaLog->writeLog("ColorSpaceConvert Fail!!");
            }
            return -1;
        }
        
        if (!mVideoEncoder) {
            LOGW("VideoEncoder is closed");
            if (mMediaLog) {
                mMediaLog->writeLog("VideoEncoder is closed");
            }
            return 0;
        }
        
#ifdef IOS
        if(isNeedSwitchVideoEncoder)
        {
            isNeedSwitchVideoEncoder = false;
            
            if (mVideoEncoder!=NULL) {
                mVideoEncoder->Close();
                VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            }
            
            if(mVideoEncoderType==VIDEO_TOOL_BOX)
            {
                mVideoEncoderType = X264;
            }else{
                mVideoEncoderType = VIDEO_TOOL_BOX;
            }
            
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, BASE_LINE, (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
            
            ret = mVideoEncoder->Open();
            
            if(ret)
            {
                // set VideoEncoder OutputHandler
                if (mVideoOptions->hasVideo) {
                    mVideoEncoder->SetOutputHandler(mMediaMuxer);
                }
            }else{
                LOGE("Video Encode Fail!!");
                if (mMediaLog) {
                    mMediaLog->writeLog("Video Encode Fail!!");
                }
                return -2;
            }
        }
#endif
        
        //encode
        ret = mVideoEncoder->Encode(mTargetVideoFrame);
        
#ifdef IOS
        if (!ret && mVideoEncoderType==VIDEO_TOOL_BOX)
        {
            if (mVideoEncoder!=NULL) {
                mVideoEncoder->Close();
                VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            }
            
            mVideoEncoderType = X264;
            
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, BASE_LINE, (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
            
            ret = mVideoEncoder->Open();
            if(ret)
            {
                // set VideoEncoder OutputHandler
                if (mVideoOptions->hasVideo) {
                    mVideoEncoder->SetOutputHandler(mMediaMuxer);
                }
                ret = mVideoEncoder->Encode(mTargetVideoFrame);
            }
        }
#endif
        
        if (!ret) {
            LOGE("Video Encode Fail!!");
            if (mMediaLog) {
                mMediaLog->writeLog("Video Encode Fail!!");
            }
            return -2;
        }
        
    }else {
        
        if (!mVideoEncoder) {
            LOGW("VideoEncoder is closed");
            if (mMediaLog) {
                mMediaLog->writeLog("VideoEncoder is closed");
            }
            return 0;
        }
        
        bool ret = false;
        
#ifdef IOS
        if(isNeedSwitchVideoEncoder)
        {
            isNeedSwitchVideoEncoder = false;
            
            if (mVideoEncoder!=NULL) {
                mVideoEncoder->Close();
                VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            }
            
            if(mVideoEncoderType==VIDEO_TOOL_BOX)
            {
                mVideoEncoderType = X264;
            }else{
                mVideoEncoderType = VIDEO_TOOL_BOX;
            }
            
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, BASE_LINE, (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
            
            ret = mVideoEncoder->Open();
            
            if(ret)
            {
                // set VideoEncoder OutputHandler
                if (mVideoOptions->hasVideo) {
                    mVideoEncoder->SetOutputHandler(mMediaMuxer);
                }
            }else{
                LOGE("Video Encode Fail!!");
                if (mMediaLog) {
                    mMediaLog->writeLog("Video Encode Fail!!");
                }
                return -2;
            }
        }
#endif
        
        //encode
        ret = mVideoEncoder->Encode(*videoFrame);
        
#ifdef IOS
        if (!ret && mVideoEncoderType==VIDEO_TOOL_BOX)
        {
            if (mVideoEncoder!=NULL) {
                mVideoEncoder->Close();
                VideoEncoder::DeleteVideoEncoder(mVideoEncoder, mVideoEncoderType);
            }
            
            mVideoEncoderType = X264;
            
            mVideoEncoder = VideoEncoder::CreateVideoEncoder(mVideoEncoderType, mVideoOptions->videoFps, mVideoOptions->videoWidth, mVideoOptions->videoHeight, BASE_LINE, (VIDEO_ENCODE_MODE)mVideoOptions->encodeMode, mVideoOptions->videoBitRate, mVideoOptions->quality, mVideoOptions->maxKeyFrameIntervalMs, ULTRAFAST, mVideoOptions->videoBitRate, mVideoOptions->bStrictCBR, mVideoOptions->deblockingFilterFactor);
            
            ret = mVideoEncoder->Open();
            if(ret)
            {
                // set VideoEncoder OutputHandler
                if (mVideoOptions->hasVideo) {
                    mVideoEncoder->SetOutputHandler(mMediaMuxer);
                }
                ret = mVideoEncoder->Encode(*videoFrame);
            }
        }
#endif
        
        if (!ret) {
            LOGE("Video Encode Fail!!");
            if (mMediaLog) {
                mMediaLog->writeLog("Video Encode Fail!!");
            }
            return -2;
        }
        
    }
    
    //for fps statistics
    flowingFrameCountPerTime++;
    if (realfps_begin_time==0) {
        realfps_begin_time = MediaTimer::GetNowMediaTimeMS();
    }
    
    int64_t realfps_duration = MediaTimer::GetNowMediaTimeMS()-realfps_begin_time;
    if (realfps_duration>=1000) {
        mRealFps = flowingFrameCountPerTime*1000/realfps_duration;
        
        realfps_begin_time = 0;
        flowingFrameCountPerTime = 0;
        
        notifyListener_l(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS, mRealFps);
    }
    
    return 1;
}

bool MultiSourceMediaStreamer::isNeedConvertVideoFrame(VideoFrame *sourceFrame, VideoFrame *targetFrame)
{
    if (sourceFrame->rotation!=0 || sourceFrame->width!=targetFrame->width || sourceFrame->height!=targetFrame->height || sourceFrame->videoRawType!=targetFrame->videoRawType) {
        return true;
    }
    
    return false;
}
