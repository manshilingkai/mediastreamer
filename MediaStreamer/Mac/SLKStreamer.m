//
//  SLKStreamer.m
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "SLKStreamer.h"
#include "SLKMediaStreamerWrapper.h"
#include <mutex>

static int MEDIASTREAMER_STATE_IDLE = 0;
static int MEDIASTREAMER_STATE_INITIALIZED = 1;
static int MEDIASTREAMER_STATE_STARTING = 2;
static int MEDIASTREAMER_STATE_STARTED = 3;
static int MEDIASTREAMER_STATE_PAUSED = 4;

@implementation SLKStreamer
{
    SLKMediaStreamerWrapper* pSLKMediaStreamerWrapper;
    std::mutex mSLKMediaStreamerWrapperMutex;
    
    dispatch_queue_t notificationQueue;
    
    BOOL isInterrupt;
    BOOL hasAudio;
    
    int currentStreamerState;
    
    dispatch_source_t timer;
    bool isTiming;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pSLKMediaStreamerWrapper = NULL;
        
        isInterrupt = NO;
        hasAudio = NO;
        currentStreamerState = MEDIASTREAMER_STATE_IDLE;
        
        timer =  nil;
        isTiming = false;
        
        notificationQueue = dispatch_queue_create("SLKStreamerNotificationQueue", 0);
    }
    
    return self;
}

- (void)initializeWithOptions:(SLKStreamerOptions*)options
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_IDLE) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if([options hasAudio])
    {
        hasAudio = YES;
    }
    
    VideoOptions videoOptions;
    AudioOptions audioOptions;
    
    audioOptions.hasAudio = [options hasAudio];
    audioOptions.audioSampleRate = 44100;
    audioOptions.audioNumChannels = 2;
    audioOptions.audioBitRate = [options audioBitrate]; //k
    
    videoOptions.hasVideo = [options hasVideo];
    videoOptions.videoEncodeType = [options videoEncodeType];
    videoOptions.videoWidth = [options videoSize].width;
    videoOptions.videoHeight = [options videoSize].height;
    videoOptions.videoFps = [options fps];
    if (videoOptions.videoEncodeType==VIDEO_HARD_ENCODE) {
        videoOptions.videoRawType = VIDEOFRAME_RAWTYPE_BGRA;
    }else {
        videoOptions.videoRawType = VIDEOFRAME_RAWTYPE_I420;
    }
    videoOptions.videoProfile = 2; //0:base_line 1:main_profile 2:high_profile
    videoOptions.videoBitRate = [options videoBitrate];
    videoOptions.encodeMode = 1;//CBR
    videoOptions.maxKeyFrameIntervalMs = [options maxKeyFrameIntervalMs];;
    
    videoOptions.networkAdaptiveMinFps = [options networkAdaptiveMinFps];
    videoOptions.networkAdaptiveMinVideoBitrate = [options networkAdaptiveMinVideoBitrate];
    if ([options isEnableNetworkAdaptive]) {
        videoOptions.isEnableNetworkAdaptive = true;
    }else {
        videoOptions.isEnableNetworkAdaptive = false;
    }
    
    //for X264
    videoOptions.quality = 0;
    videoOptions.bStrictCBR = true;
    videoOptions.deblockingFilterFactor = 0;
    
#if TARGET_IPHONE_SIMULATOR
    videoOptions.hasVideo = NO;
    videoOptions.videoEncodeType=VIDEO_SOFT_ENCODE;
#endif
    
    if ([options backupDir]==nil) {
        pSLKMediaStreamerWrapper = GetInstance([[options publishUrl] UTF8String], NULL, videoOptions, audioOptions);
    }else{
        pSLKMediaStreamerWrapper = GetInstance([[options publishUrl] UTF8String], [[options backupDir] UTF8String], videoOptions, audioOptions);
    }
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_setListener(pSLKMediaStreamerWrapper, notificationListener, (__bridge void*)self);
    }
    
    currentStreamerState = MEDIASTREAMER_STATE_INITIALIZED;
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

void notificationListener(void* owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak SLKStreamer *thiz = (__bridge SLKStreamer*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    __weak typeof(self) wself = self;
    
    dispatch_async(notificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf handleNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    });
}

- (void)handleNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    mSLKMediaStreamerWrapperMutex.lock();
    if (currentStreamerState==MEDIASTREAMER_STATE_IDLE) {
        mSLKMediaStreamerWrapperMutex.unlock();
        return;
    }
    mSLKMediaStreamerWrapperMutex.unlock();
    
    switch (event) {
        case SLK_MEDIA_STREAMER_CONNECTING:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotConnecting)])) {
                    [self.delegate gotConnecting];
                }
            }
            break;
        case SLK_MEDIA_STREAMER_CONNECTED:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(didConnected)])) {
                    [self.delegate didConnected];
                }
            }
            break;
            
        case SLK_MEDIA_STREAMER_STREAMING:
            mSLKMediaStreamerWrapperMutex.lock();
            currentStreamerState = MEDIASTREAMER_STATE_STARTED;
            mSLKMediaStreamerWrapperMutex.unlock();
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotStreaming)])) {
                    [self.delegate gotStreaming];
                }
            }
            break;
        case SLK_MEDIA_STREAMER_PAUSED:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(didPaused)])) {
                    [self.delegate didPaused];
                }
            }
            break;
            
        case SLK_MEDIA_STREAMER_END:
            mSLKMediaStreamerWrapperMutex.lock();
            currentStreamerState = MEDIASTREAMER_STATE_INITIALIZED;
            mSLKMediaStreamerWrapperMutex.unlock();
            
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(didEndStreaming)])) {
                    [self.delegate didEndStreaming];
                }
            }
            break;
        case SLK_MEDIA_STREAMER_ERROR:
            mSLKMediaStreamerWrapperMutex.lock();
            currentStreamerState = MEDIASTREAMER_STATE_INITIALIZED;
            mSLKMediaStreamerWrapperMutex.unlock();
            
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotStreamerErrorWithErrorType:)])) {
                    [self.delegate gotStreamerErrorWithErrorType:ext1];
                }
            }
            break;
        case SLK_MEDIA_STREAMER_INFO:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotStreamerInfoWithInfoType:InfoValue:)])) {
                    [self.delegate gotStreamerInfoWithInfoType:ext1 InfoValue:ext2];
                }
            }
            break;

        default:
            break;
    }
}

- (void)pushPixelBuffer:(CVPixelBufferRef)pixelBuffer
               Rotation:(int)rotation
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        CVPixelBufferLockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
        uint8_t* data = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
        int frameSize = (int)CVPixelBufferGetDataSize(pixelBuffer);
        int width = (int)CVPixelBufferGetWidth(pixelBuffer);
        int height = (int)CVPixelBufferGetHeight(pixelBuffer);
        int videoRawType;
        int kCVPixelFormatType = CVPixelBufferGetPixelFormatType(pixelBuffer);//
        switch (kCVPixelFormatType) {
            case kCVPixelFormatType_32BGRA:
                videoRawType = VIDEOFRAME_RAWTYPE_BGRA; //BGRA
                break;
            case kCVPixelFormatType_420YpCbCr8BiPlanarFullRange: //NV12
                videoRawType = VIDEOFRAME_RAWTYPE_NV12;
                break;
            default:
                videoRawType = VIDEOFRAME_RAWTYPE_BGRA; //BGRA
                break;
        }
        SLKMediaStreamerWrapper_inputVideoFrame(pSLKMediaStreamerWrapper,data,frameSize,width,height,videoRawType,rotation);
        CVPixelBufferUnlockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();

}

- (void)pushText:(NSString*)text
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_inputText(pSLKMediaStreamerWrapper, (char*)[text UTF8String], [text length]+1);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)start
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_INITIALIZED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_start(pSLKMediaStreamerWrapper);
    }
    
    currentStreamerState = MEDIASTREAMER_STATE_STARTING;
    
    mSLKMediaStreamerWrapperMutex.unlock();

}
- (void)resume
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_PAUSED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_resume(pSLKMediaStreamerWrapper);
    }
    
    currentStreamerState = MEDIASTREAMER_STATE_STARTED;
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)pause
{
    mSLKMediaStreamerWrapperMutex.lock();

    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_pause(pSLKMediaStreamerWrapper);
    }
    
    currentStreamerState = MEDIASTREAMER_STATE_PAUSED;
    
    mSLKMediaStreamerWrapperMutex.unlock();

}
- (void)stop
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState == MEDIASTREAMER_STATE_IDLE || currentStreamerState == MEDIASTREAMER_STATE_INITIALIZED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_stop(pSLKMediaStreamerWrapper);
    }
    
    currentStreamerState = MEDIASTREAMER_STATE_INITIALIZED;
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)enableAudio:(BOOL)isEnable
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState == MEDIASTREAMER_STATE_IDLE) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_enableAudio(pSLKMediaStreamerWrapper, isEnable);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)terminate
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState == MEDIASTREAMER_STATE_IDLE) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        ReleaseInstance(&pSLKMediaStreamerWrapper);
        pSLKMediaStreamerWrapper = NULL;
    }
    
    currentStreamerState = MEDIASTREAMER_STATE_IDLE;
    
    mSLKMediaStreamerWrapperMutex.unlock();
        
    dispatch_barrier_sync(notificationQueue, ^{
        NSLog(@"finish all notifications");
    });
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"SLKStreamer dealloc");
}

@end
