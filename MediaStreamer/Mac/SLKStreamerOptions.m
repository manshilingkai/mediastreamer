//
//  SLKStreamerOptions.m
//  MediaStreamer
//
//  Created by Think on 2016/12/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "SLKStreamerOptions.h"

@implementation SLKStreamerOptions

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.publishUrl = nil;
        self.backupDir = NSTemporaryDirectory();
        
        self.hasVideo = YES;
        self.videoEncodeType = VIDEO_HARDWARE_ENCODE;
        self.videoSize = CGSizeMake(480,640);
        self.fps = 20;
        self.videoBitrate = 500;
        self.maxKeyFrameIntervalMs = 3000;
        self.networkAdaptiveMinFps = 12;
        self.networkAdaptiveMinVideoBitrate = 200;
        self.isEnableNetworkAdaptive = YES;
        
        self.hasAudio = YES;
        self.audioBitrate = 32; //high:64
    }
    
    return self;
}

@end
