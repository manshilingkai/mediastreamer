//
//  SLKMediaStreamerWrapper.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef SLKMediaStreamerWrapper_h
#define SLKMediaStreamerWrapper_h

#include <stdio.h>
#include <inttypes.h>

#include "MediaDataType.h"

struct SLKMediaStreamerWrapper;

enum slk_media_streamer_event_type {
    SLK_MEDIA_STREAMER_CONNECTING = 0,
    SLK_MEDIA_STREAMER_CONNECTED = 1,
    SLK_MEDIA_STREAMER_STREAMING = 2,
    SLK_MEDIA_STREAMER_ERROR = 3,
    SLK_MEDIA_STREAMER_INFO = 4,
    SLK_MEDIA_STREAMER_END = 5,
    SLK_MEDIA_STREAMER_PAUSED = 6,
};

#ifdef __cplusplus
extern "C" {
#endif
    struct SLKMediaStreamerWrapper *GetInstance(const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions);
    void ReleaseInstance(struct SLKMediaStreamerWrapper **ppInstance);
    
    void SLKMediaStreamerWrapper_setListener(struct SLKMediaStreamerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner);
    
    void SLKMediaStreamerWrapper_inputVideoFrame(struct SLKMediaStreamerWrapper *pInstance, uint8_t* data, int frameSize, int width, int height, int videoRawType, int rotation);
    
    void SLKMediaStreamerWrapper_inputText(struct SLKMediaStreamerWrapper *pInstance, char* text, int size);
    
    void SLKMediaStreamerWrapper_start(struct SLKMediaStreamerWrapper *pInstance);
    void SLKMediaStreamerWrapper_resume(struct SLKMediaStreamerWrapper *pInstance);
    void SLKMediaStreamerWrapper_pause(struct SLKMediaStreamerWrapper *pInstance);
    void SLKMediaStreamerWrapper_stop(struct SLKMediaStreamerWrapper *pInstance);
    
    void SLKMediaStreamerWrapper_enableAudio(struct SLKMediaStreamerWrapper *pInstance, bool isEnable);

#ifdef __cplusplus
};
#endif

#endif /* SLKMediaStreamerWrapper_h */
