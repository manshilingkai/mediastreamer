//
//  SLKMediaStreamerWrapper.cpp
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "SLKMediaStreamerWrapper.h"
#include "SLKMediaStreamer.h"

#ifdef __cplusplus
extern "C" {
#endif

struct SLKMediaStreamerWrapper{
    SLKMediaStreamer* mediaStreamer;
    
    SLKMediaStreamerWrapper()
    {
        mediaStreamer = NULL;
    }
};

struct SLKMediaStreamerWrapper *GetInstance(const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions)
{
    SLKMediaStreamerWrapper* pInstance = new SLKMediaStreamerWrapper;
    
    pInstance->mediaStreamer = new SLKMediaStreamer(publishUrl,backupDir,videoOptions,audioOptions);
    
    return pInstance;
}

void ReleaseInstance(struct SLKMediaStreamerWrapper **ppInstance)
{
    SLKMediaStreamerWrapper* pInstance  = *ppInstance;
    if (pInstance!=NULL) {
        if (pInstance->mediaStreamer!=NULL) {
            delete pInstance->mediaStreamer;
            pInstance->mediaStreamer = NULL;
        }
        
        delete pInstance;
        pInstance = NULL;
    }
}

void SLKMediaStreamerWrapper_setListener(struct SLKMediaStreamerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->setListener(listener,owner);
    }
}

void SLKMediaStreamerWrapper_inputVideoFrame(struct SLKMediaStreamerWrapper *pInstance, uint8_t* data, int frameSize, int width, int height, int videoRawType, int rotation)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        VideoFrame videoFrame;
        videoFrame.data = data;
        videoFrame.frameSize = frameSize;
        videoFrame.width = width;
        videoFrame.height = height;
        videoFrame.videoRawType = videoRawType;
        videoFrame.rotation = rotation;
        
        pInstance->mediaStreamer->inputVideoFrame(&videoFrame);
    }
}

void SLKMediaStreamerWrapper_inputText(struct SLKMediaStreamerWrapper *pInstance, char* text, int size)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->inputText(text, size);
    }
}

    
void SLKMediaStreamerWrapper_start(struct SLKMediaStreamerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->start();
    }
}

void SLKMediaStreamerWrapper_resume(struct SLKMediaStreamerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->resume();
    }
}

void SLKMediaStreamerWrapper_pause(struct SLKMediaStreamerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->pause();
    }
}

void SLKMediaStreamerWrapper_stop(struct SLKMediaStreamerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->stop();
    }
}
    
void SLKMediaStreamerWrapper_enableAudio(struct SLKMediaStreamerWrapper *pInstance, bool isEnable)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->enableAudio(isEnable);
    }
}

#ifdef __cplusplus
};
#endif
