//
//  SLKAudioStreamer.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef SLKAudioStreamer_h
#define SLKAudioStreamer_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#include <sys/resource.h>
#endif

#include "IMediaListener.h"
#include "MediaMuxer.h"
#include "AudioCapture.h"
#include "AudioEncoder.h"

#include "AudioStreamer.h"

#ifdef ANDROID
#include "jni.h"
#endif

//#include "VoiceActivityDetection.h"

//------------------------------------------------------------------------------------------------------------------//
#include "AudioPlayer.h"
#include <queue>
using namespace std;

enum MixAudioResourceCommandType{
    MIX_BGM = 0,
    STOP_BGM = 1,
    MIX_SEM = 2,
    PAUSE_ALL = 3,
    PLAY_ALL = 4,
    PLAY_BGM = 5,
    PAUSE_BGM = 6,
    SET_BGM_VOLUME = 7,
};

struct MixAudioResourceCommandMixBgm {
    int bgmId;
    char* url;
    float volume;
    int numberOfLoops;
    
    MixAudioResourceCommandMixBgm()
    {
        bgmId = -1;
        url = NULL;
        volume = 1.0f;
        numberOfLoops = 0;
    }
    
    inline void Free()
    {
        bgmId = -1;

        if(url)
        {
            free(url);
            url = NULL;
        }
        
        volume = 1.0f;
        numberOfLoops = 0;
    }
};

struct MixAudioResourceCommandMixSem {
    int semId;
    char* url;
    float volume;
    int numberOfLoops;
    
    MixAudioResourceCommandMixSem()
    {
        semId = -1;
        url = NULL;
        volume = 1.0f;
        numberOfLoops = 0;
    }
    
    inline void Free()
    {
        semId = -1;

        if(url)
        {
            free(url);
            url = NULL;
        }
        
        volume = 1.0f;
        numberOfLoops = 0;
    }
};

struct MixAudioResourceCommandSetBgmVolume {
    float volume;
    
    MixAudioResourceCommandSetBgmVolume()
    {
        volume = 1.0f;
    }
};

struct MixAudioResourceCommand {
    int type;
    void* content;
    
    MixAudioResourceCommand()
    {
        type = -1;
        content = NULL;
    }
    
    inline void Free()
    {
        if(type == MIX_BGM)
        {
            MixAudioResourceCommandMixBgm* mixAudioResourceCommandMixBgm = (MixAudioResourceCommandMixBgm*)content;
            if(mixAudioResourceCommandMixBgm)
            {
                mixAudioResourceCommandMixBgm->Free();
                delete mixAudioResourceCommandMixBgm;
                mixAudioResourceCommandMixBgm = NULL;
            }
        }else if (type == MIX_SEM)
        {
            MixAudioResourceCommandMixSem* mixAudioResourceCommandMixSem = (MixAudioResourceCommandMixSem*)content;
            if(mixAudioResourceCommandMixSem)
            {
                mixAudioResourceCommandMixSem->Free();
                delete mixAudioResourceCommandMixSem;
                mixAudioResourceCommandMixSem = NULL;
            }
        }else if (type == SET_BGM_VOLUME)
        {
            MixAudioResourceCommandSetBgmVolume* mixAudioResourceCommandSetBgmVolume = (MixAudioResourceCommandSetBgmVolume*)content;
            if (mixAudioResourceCommandSetBgmVolume) {
                delete mixAudioResourceCommandSetBgmVolume;
                mixAudioResourceCommandSetBgmVolume = NULL;
            }
        }
    }
};

class MixAudioResourceCommandQueue
{
public:
    MixAudioResourceCommandQueue();
    ~MixAudioResourceCommandQueue();
    
    void push(MixAudioResourceCommand *command);
    MixAudioResourceCommand* pop();
    
    void flush();
private:
    pthread_mutex_t mLock;
    queue<MixAudioResourceCommand*> mMixAudioResourceCommandQueue;
};

//--------------------------------------------------------------------------------------------------------------------------//

class SLKAudioStreamer : public AudioStreamer, IMediaCallback
{
public:
    SLKAudioStreamer(AudioOptions *audioOptions, MediaLog* mediaLog);
    ~SLKAudioStreamer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
    void setListener(IMediaListener *mediaListener);
    void setMediaMuxer(MediaMuxer *mediaMuxer);
    
    void inputAudioFrame(AudioFrame *inAudioFrame);
    void inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType);
    
    // -1:Open AudioCapture Fail
    // -2:Open AudioEncoder Fail
    // -3:Open AudioFilter  Fail
    int prepare();
    
    // -1:Start AudioCapture Fail
    int start();
    
    // -1:Stop AudioCapture Fail
    int pause();
    
    void stop();
    
    void enableSilence(bool isEnable);
    
    void mixBGM(int bgmId, char* bgmUrl, float volume, int numberOfLoops);
    void playBGM();
    void pauseBGM();
    void stopBGM();
    void setBGMVolume(float volume);
    void mixSEM(int semId, char* semUrl, float volume, int numberOfLoops);

#ifdef IOS
    void iOSAVAudioSessionInterruption(bool isInterrupting);
#endif
    
    void onMediaCallback(void* owner, int sourceId, int event, int ext1, int ext2);
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    bool isRealTime;
    bool isExternalAudioInput;
    
    int sampleRate;
    int numChannels;
    int bitRate;

    IMediaListener *mMediaListener;

    pthread_mutex_t mAudioCaptureLock;
    AudioCapture *mAudioCapture; // critical value
    AudioEncoder *mAudioEncoder;
    pthread_mutex_t mMediaMuxerLock;
    MediaMuxer *mMediaMuxer;
    bool hasPushAudioHeaderToMediaMuxer;

    //method for thread
    void createAudioStreamerThread();
    static void* handleAudioStreamerThread(void* ptr);
    void audioStreamerThreadMain();
    void deleteAudioStreamerThread();
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    bool isBreakThread; // critical value
    bool isAudioStreaming;
    
    // -2 : Audio Filter Fail
    // -1 : Audio Encode Fail
    // 0 : No Audio Frame
    int flowing_l();
    AudioPacket mAudioPacket;
    
    AUDIO_ENCODER_TYPE audio_encoder_type;
    AUDIO_CAPTURE_TYPE audio_capture_type;
    
    bool isEnableSilence;
    
private:
    AudioFrame* virtualAudioCapture_l();
    bool isEnableVirtualAudioCapture;
    pthread_mutex_t mVirtualAudioCaptureLock;
    AudioFrame mVirtualAudioCaptureFrame;
    
private:
    MediaLog* mMediaLog;
    
private:
    MixAudioResourceCommandQueue mMixAudioResourceCommandQueue;
    void handleMixAudioResourceCommand();
    AudioPlayer* mBGMAudioPlayer;
    AudioPlayer* mSEMAudioPlayer;
    void mixAudioResourceWithMicAudioFrame(AudioFrame* micAudioFrame);
    
private:
//    VoiceActivityDetection* mVoiceActivityDetection;
};

#endif /* SLKAudioStreamer_h */
