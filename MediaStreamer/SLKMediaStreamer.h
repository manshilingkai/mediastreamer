//
//  SLKMediaStreamer.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef SLKMediaStreamer_h
#define SLKMediaStreamer_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#include <sys/resource.h>
#endif

#include "TimedEventQueue.h"

#include "MediaStreamer.h"
#include "IMediaListener.h"

#include "MediaDataType.h"
#include "VideoFrameBufferPool.h"
#include "ColorSpaceConvert.h"
#include "VideoEncoder.h"
#include "MediaMuxer.h"
#include "AudioStreamer.h"

#include "NotificationQueue.h"

#include "MediaLog.h"

#ifdef ANDROID
#include "jni.h"
#endif

class SLKMediaStreamer : public MediaStreamer, IMediaListener
{
public:
#ifdef ANDROID
    SLKMediaStreamer(JavaVM *jvm, const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions, int reConnectTimes);
#else
    SLKMediaStreamer(const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions, char* backgroundPngCover, int reConnectTimes);
#endif
    ~SLKMediaStreamer();
    
#ifdef ANDROID
    void setEncodeSurfaceCore(void *encodeSurfaceCore);
    void setEncodeSurface(void *surface);
    
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#endif
    
    void setListener(void (*listener)(void*,int,int,int), void* arg);
    
    void updateVideoSourceLayout(map<int, VideoSourceLayout*> *layoutDict) {}
    void inputYUVVideoFrame(YUVVideoFrame* inVideoFrame, int sourceId) {}
    
    bool inputVideoFrame(VideoFrame *inVideoFrame);
    void inputYUVVideoFrame(YUVVideoFrame* inVideoFrame);

    void inputAudioFrame(AudioFrame *inAudioFrame);
    void inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType);
    
    void inputText(char* text, int size);
    
    void start();
    
    void resume();
    void pause();
    
    void stop(bool isCancle = false);
    
    void notify(int event, int ext1, int ext2);
    
    void enableAudio(bool isEnable);
    
    void mixBGM(int bgmId, char* bgmUrl, float volume, int numberOfLoops);
    void playBGM();
    void pauseBGM();
    void stopBGM();
    void setBGMVolume(float volume);
    void mixSEM(int semId, char* semUrl, float volume, int numberOfLoops);

#ifdef IOS    
    void iOSAVAudioSessionInterruption(bool isInterrupting);
    
    void iOSDidEnterBackground();
    void iOSDidBecomeActive();
#endif
    
    void netWorkReachabilityNotify(int networkStatus);
    
private:
    // return -1 : Open VideoEncoder Fail
    // return -2 : Open FFmpegMuxer  Fail
    // return -3 : Open AudioCapture Fail
    // return -4 : Open AudioEncoder Fail
    // return -5 : Open AudioFilter  Fail
    // return -6 : Open FFmpegMuxer  Exit
    // return  0 : Open All Pipelines Success
    int open_all_pipelines_l();
    
    void close_all_pipelines_l();
    
    // return  0 : VideoFrameBufferPool is empty
    // return -1 : ColorSpaceConvert Fail
    // return -2 : Video Encode Fail
    // return  1 : Flowing Success
    int flowing_l();
    
    bool isNeedConvertVideoFrame(VideoFrame *sourceFrame, VideoFrame *targetFrame);
    
private:
    friend struct SLKMediaStreamerEvent;
    
#ifdef ANDROID
    JavaVM *mJvm;
    
    void* mEncodeSurfaceCore;
    jmethodID mEncodeSurfaceCoreClassRequestRenderMethod;
    void* mEncodeSurface;
#endif
    
    MUXER_FORMAT mMuxerFormat;
    VideoOptions *mVideoOptions;
    AudioOptions *mAudioOptions;
    char* mPublishUrl;
        
    VideoFrame mTargetVideoFrame;
    
    //0 : JNI
    //1 : iOS
    //2 : Normal
    int mMediaListenerPlatformType;
    IMediaListener* mMediaListener;
    
    pthread_mutex_t mVideoFrameBufferPoolLock;
    bool canInputVideoFrame;
    VideoFrameBufferPool *mVideoFrameBufferPool; // critical value
    
    ColorSpaceConvert *mColorSpaceConvert;
    VideoEncoder *mVideoEncoder;
    MediaMuxer *mMediaMuxer; // critical value
    AudioStreamer *mAudioStreamer; // critical value

    VIDEO_ENCODER_TYPE mVideoEncoderType;
    
    enum {
        CONNECTING           = 0x01,
        CONNECTED            = 0x02,
        STREAMING            = 0x04,
        PAUSED               = 0x10,
        ERRORED              = 0x20,
        END                  = 0x40,
        INITIALIZED          = 0x80,
        ENDING               = 0x100,
    };
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
    
    pthread_mutex_t mLock;
    pthread_cond_t mStopCondition;
    
    TimedEventQueue mQueue;
    NotificationQueue mNotificationQueue;
    
    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mVideoEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mControlBitrateEvent;
    TimedEventQueue::Event *mNotifyEvent;
    TimedEventQueue::Event *mRePublishEvent;
    bool mVideoEventPending;
    
    SLKMediaStreamer(const SLKMediaStreamer &);
    SLKMediaStreamer &operator=(const SLKMediaStreamer &);
    
    void prepareAsync_l();
    void play_l();
    void pause_l();
    void pause_ll();
    void stop_l();

    void postVideoEvent_l(int64_t delayUs = -1);
    void postControlBitrateEvent_l();
    void postNotifyEvent_l();
    void postRePublishEvent_l();
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);

    void onPrepareAsyncEvent();
    void onVideoEvent();
    void onStopEvent();
    void onControlBitrateEvent();
    void onNotifyEvent();
    void onRePublishEvent();
    
    void cancelStreamerEvents();
    
    int64_t mVideoEventTimer_StartTime;
    int64_t mVideoEventTimer_EndTime;
    int64_t mVideoEventTime;
    pthread_mutex_t mVideoDelayTimeUsLock;
    int64_t mVideoDelayTimeUs;

    int mTargetFps;
    
    int64_t mPublishDelayTimeMs;
    
    int mTargetVideoBitrate;
    
    bool isAudioEnabled;
    
    //for fps statistics
    int mRealFps;
    int flowingFrameCountPerTime;
    int64_t realfps_begin_time;
    
private:
    pthread_mutex_t mMuxerLock;
    pthread_mutex_t mAudioStreamerLock;
private:
    bool gotError;
    
private:
    char* mBackupDir;
    MediaLog* mMediaLog;
    
private:
    bool isPushImage;
    char* pushImageFile;
    VideoFrame* pushImageVideoFrame;
private:
#ifdef IOS
    bool isNeedResurrectiOSVTB;
#endif
private:
    //interrupt
    bool isInterrupt;
    pthread_mutex_t mInterruptLock;
private:
    int mReconnectTimes;
private:
    void processNetWorkReachability(int networkStatus);
    bool isNotReachable;
};


#endif /* SLKMediaStreamer_h */
