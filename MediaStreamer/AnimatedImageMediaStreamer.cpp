//
//  AnimatedImageMediaStreamer.cpp
//  MediaStreamer
//
//  Created by Think on 2018/9/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "AnimatedImageMediaStreamer.h"
#include "MediaLog.h"
#include "AutoLock.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#endif

#ifdef IOS
#include "iOSMediaListener.h"
#endif

#include "NormalMediaListener.h"
#include "Mediatimer.h"
#include "MediaFile.h"
#include "StringUtils.h"
#include "MediaMuxer.h"

struct AnimatedImageMediaStreamerEvent : public TimedEventQueue::Event {
    AnimatedImageMediaStreamerEvent(
                          AnimatedImageMediaStreamer *streamer,
                          void (AnimatedImageMediaStreamer::*method)())
    : mStreamer(streamer),
    mMethod(method) {
    }
    
protected:
    virtual ~AnimatedImageMediaStreamerEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mStreamer->*mMethod)();
    }
    
private:
    AnimatedImageMediaStreamer *mStreamer;
    void (AnimatedImageMediaStreamer::*mMethod)();
    
    AnimatedImageMediaStreamerEvent(const AnimatedImageMediaStreamerEvent &);
    AnimatedImageMediaStreamerEvent &operator=(const AnimatedImageMediaStreamerEvent &);
};

#ifdef ANDROID
AnimatedImageMediaStreamer::AnimatedImageMediaStreamer(JavaVM *jvm, const char* publishUrl, VideoOptions videoOptions)
{
    LOGI("AnimatedImageMediaStreamer::AnimatedImageMediaStreamer");
    
    mJvm = jvm;
    
    mVideoOptions = new VideoOptions;
    
    mVideoOptions->hasVideo = videoOptions.hasVideo;
    mVideoOptions->videoEncodeType = videoOptions.videoEncodeType;
    mVideoOptions->videoWidth = videoOptions.videoWidth;
    mVideoOptions->videoHeight = videoOptions.videoHeight;
    mVideoOptions->videoFps = videoOptions.videoFps;
    mVideoOptions->videoBitRate = videoOptions.videoBitRate;
    mVideoOptions->videoRawType = videoOptions.videoRawType;
    mVideoOptions->encodeMode = videoOptions.encodeMode;
    mVideoOptions->quality = videoOptions.quality;
    mVideoOptions->maxKeyFrameIntervalMs = videoOptions.maxKeyFrameIntervalMs;
    mVideoOptions->bStrictCBR = videoOptions.bStrictCBR;
    mVideoOptions->deblockingFilterFactor = videoOptions.deblockingFilterFactor;
    
    mPublishUrl = strdup(publishUrl);
    
    mMuxerFormat = GIF;
    char dst[8];
    StringUtils::right(dst, mPublishUrl, 4);
    if (!strcmp(dst, ".gif") || !strcmp(dst, ".GIF")) {
        mMuxerFormat = GIF;
    }
    
    mTargetVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*4);
    mTargetVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*4;
    mTargetVideoFrame.width = mVideoOptions->videoWidth;
    mTargetVideoFrame.height = mVideoOptions->videoHeight;
    mTargetVideoFrame.rotation = 0;
    mTargetVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
    
    mMediaListener = NULL;
    
    mVideoFrameBufferPool = NULL;
    mColorSpaceConvert = NULL;
    
    pthread_mutex_init(&mLock, NULL);
    
    mAsyncPrepareEvent = NULL;
    mVideoEvent = NULL;
    mStopEvent = NULL;
    
    mVideoEvent = new AnimatedImageMediaStreamerEvent(this, &AnimatedImageMediaStreamer::onVideoEvent);
    mVideoEventPending = false;
    mNotifyEvent = new AnimatedImageMediaStreamerEvent(this, &AnimatedImageMediaStreamer::onNotifyEvent);
    mAsyncPrepareEvent = new AnimatedImageMediaStreamerEvent(this, &AnimatedImageMediaStreamer::onPrepareAsyncEvent);
    mStopEvent = new AnimatedImageMediaStreamerEvent(this, &AnimatedImageMediaStreamer::onStopEvent);
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();
    
    mVideoFrameBufferPool = new VideoFrameBufferPool();
    
    mVideoEventTimer_StartTime = 0;
    mVideoEventTimer_EndTime = 0;
    mVideoEventTime = 0;
    mVideoDelayTimeUs = 0;
    
    mTargetFps = mVideoOptions->videoFps;
    mTargetVideoBitrate = mVideoOptions->videoBitRate;
    
    mFlags = 0;
    
    pthread_cond_init(&mStopCondition, NULL);

    mRealFps = 0;
    flowingFrameCountPerTime = 0;
    realfps_begin_time=0;
    
    pthread_mutex_init(&mVideoFrameBufferPoolLock, NULL);
    canInputVideoFrame = false;
    
    gotError = false;
    
    mAnimatedGifCreater = NULL;
    mAnimatedGifCreaterType = GIF_H;
    
    mStartTimeMs = 0ll;
    isGotFirstFrame = false;
    mLastSendRecordTimeMs = 0ll;
    
    MediaTimer::open();
}

#else
AnimatedImageMediaStreamer::AnimatedImageMediaStreamer(const char* publishUrl, VideoOptions videoOptions)
{
    mVideoOptions = new VideoOptions;
    
    mVideoOptions->hasVideo = videoOptions.hasVideo;
    mVideoOptions->videoEncodeType = videoOptions.videoEncodeType;
    mVideoOptions->videoWidth = videoOptions.videoWidth;
    mVideoOptions->videoHeight = videoOptions.videoHeight;
    mVideoOptions->videoFps = videoOptions.videoFps;
    mVideoOptions->videoBitRate = videoOptions.videoBitRate;
    mVideoOptions->videoRawType = videoOptions.videoRawType;
    
    mVideoOptions->encodeMode = videoOptions.encodeMode;
    mVideoOptions->quality = videoOptions.quality;
    mVideoOptions->maxKeyFrameIntervalMs = videoOptions.maxKeyFrameIntervalMs;
    mVideoOptions->bStrictCBR = videoOptions.bStrictCBR;
    mVideoOptions->deblockingFilterFactor = videoOptions.deblockingFilterFactor;
    
    mPublishUrl = strdup(publishUrl);
    
    mMuxerFormat = GIF;
    char dst[8];
    StringUtils::right(dst, mPublishUrl, 4);
    if (!strcmp(dst, ".gif") || !strcmp(dst, ".GIF")) {
        mMuxerFormat = GIF;
    }
    
    mTargetVideoFrame.data = (uint8_t*)malloc(mVideoOptions->videoWidth*mVideoOptions->videoHeight*4);
    mTargetVideoFrame.frameSize = mVideoOptions->videoWidth*mVideoOptions->videoHeight*4;
    mTargetVideoFrame.width = mVideoOptions->videoWidth;
    mTargetVideoFrame.height = mVideoOptions->videoHeight;
    mTargetVideoFrame.rotation = 0;
    mTargetVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
    
    mMediaListener = NULL;
    
    mVideoFrameBufferPool = NULL;
    mColorSpaceConvert = NULL;
    
    pthread_mutex_init(&mLock, NULL);
    
    mAsyncPrepareEvent = NULL;
    mVideoEvent = NULL;
    mStopEvent = NULL;
    
    mVideoEvent = new AnimatedImageMediaStreamerEvent(this, &AnimatedImageMediaStreamer::onVideoEvent);
    mVideoEventPending = false;
    mNotifyEvent = new AnimatedImageMediaStreamerEvent(this, &AnimatedImageMediaStreamer::onNotifyEvent);
    mAsyncPrepareEvent = new AnimatedImageMediaStreamerEvent(this, &AnimatedImageMediaStreamer::onPrepareAsyncEvent);
    mStopEvent = new AnimatedImageMediaStreamerEvent(this, &AnimatedImageMediaStreamer::onStopEvent);
    
    mQueue.start();
    
    mVideoFrameBufferPool = new VideoFrameBufferPool();
    
    mVideoEventTimer_StartTime = 0;
    mVideoEventTimer_EndTime = 0;
    mVideoEventTime = 0;
    mVideoDelayTimeUs = 0;
    
    mTargetFps = mVideoOptions->videoFps;
    mTargetVideoBitrate = mVideoOptions->videoBitRate;
    
    mFlags = 0;
    
    pthread_cond_init(&mStopCondition, NULL);
    
    mRealFps = 0;
    flowingFrameCountPerTime = 0;
    realfps_begin_time=0;
    
    pthread_mutex_init(&mVideoFrameBufferPoolLock, NULL);
    canInputVideoFrame = false;
    
    gotError = false;
    
    mAnimatedGifCreater = NULL;
    mAnimatedGifCreaterType = GIF_H;
    
    mStartTimeMs = 0ll;
    isGotFirstFrame = false;
    mLastSendRecordTimeMs = 0ll;
    
    MediaTimer::open();
}
#endif

AnimatedImageMediaStreamer::~AnimatedImageMediaStreamer()
{
    LOGI("AnimatedImageMediaStreamer::~AnimatedImageMediaStreamer");
    
    mQueue.stop(true);
    mNotificationQueue.flush();
    
    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
    
    if (mVideoEvent!=NULL) {
        delete mVideoEvent;
        mVideoEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    pthread_mutex_destroy(&mLock);
    
    if (mVideoFrameBufferPool!=NULL) {
        delete mVideoFrameBufferPool;
        mVideoFrameBufferPool = NULL;
    }
    
    if (mTargetVideoFrame.data!=NULL) {
        free(mTargetVideoFrame.data);
        mTargetVideoFrame.data = NULL;
    }
    
#ifdef ANDROID
    JniMediaListener *jniListener = (JniMediaListener*)mMediaListener;
    if (jniListener!=NULL) {
        delete jniListener;
        jniListener = NULL;
    }
#endif
    
#ifdef IOS
    iOSMediaListener *iOSListener = (iOSMediaListener*)mMediaListener;
    if (iOSListener!=NULL) {
        delete iOSListener;
        iOSListener = NULL;
    }
#endif
    
#ifdef MAC
    NormalMediaListener* normalMediaListener = (NormalMediaListener*)mMediaListener;
    if (normalMediaListener!=NULL) {
        delete normalMediaListener;
        normalMediaListener = NULL;
    }
#endif
    
    mMediaListener = NULL;
    
    if (mPublishUrl) {
        free(mPublishUrl);
        mPublishUrl = NULL;
    }
    
    if (mVideoOptions!=NULL) {
        delete mVideoOptions;
        mVideoOptions = NULL;
    }
    
    pthread_cond_destroy(&mStopCondition);
    
    pthread_mutex_destroy(&mVideoFrameBufferPoolLock);
    canInputVideoFrame = false;
    
    MediaTimer::close();
}

#ifdef ANDROID
void AnimatedImageMediaStreamer::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    LOGI("AnimatedImageMediaStreamer::setListener");
    
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    modifyFlags(INITIALIZED, SET);
}
#endif

void AnimatedImageMediaStreamer::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);
    
#ifdef IOS
    mMediaListener = new iOSMediaListener(listener,arg);
#else
    mMediaListener = new NormalMediaListener(listener,arg);
#endif
    
    modifyFlags(INITIALIZED, SET);
}

void AnimatedImageMediaStreamer::notifyListener_l(int event, int ext1, int ext2)
{
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void AnimatedImageMediaStreamer::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    postNotifyEvent_l();
}

void AnimatedImageMediaStreamer::postNotifyEvent_l()
{
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void AnimatedImageMediaStreamer::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
    
    switch (event) {
        case MEDIA_STREAMER_ERROR:
            modifyFlags(ERRORED, ASSIGN);
            notifyListener_l(event, ext1, ext2);
            gotError = true;
            stop_l();
            break;
        case MEDIA_STREAMER_INFO:
            notifyListener_l(event, ext1, ext2);
            break;
            
        default:
            notifyListener_l(event, ext1, ext2);
            break;
    }
    
}

bool AnimatedImageMediaStreamer::inputVideoFrame(VideoFrame *inVideoFrame)
{
    if (inVideoFrame==NULL) return false;
    
    bool ret  = false;
    if (mVideoOptions->hasVideo) {
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        if (canInputVideoFrame && mVideoFrameBufferPool!=NULL) {
            inVideoFrame->pts = MediaTimer::GetNowMediaTimeMS();
            ret = mVideoFrameBufferPool->push(inVideoFrame);
        }
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    }
    return ret;
}

void AnimatedImageMediaStreamer::inputYUVVideoFrame(YUVVideoFrame* inVideoFrame)
{
    if (inVideoFrame==NULL) return;
    
    if (mVideoOptions->hasVideo) {
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        if (canInputVideoFrame && mVideoFrameBufferPool!=NULL) {
            inVideoFrame->pts = MediaTimer::GetNowMediaTimeMS();
            mVideoFrameBufferPool->push(inVideoFrame);
        }
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    }
}

void AnimatedImageMediaStreamer::start()
{
    LOGI("AnimatedImageMediaStreamer::start");
    
    AutoLock autoLock(&mLock);
    
    if (mFlags & CONNECTING) {
        notifyListener_l(MEDIA_STREAMER_INFO,MEDIA_STREAMER_INFO_ALREADY_CONNECTING);
        
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    return prepareAsync_l();
}

void AnimatedImageMediaStreamer::prepareAsync_l()
{
    LOGI("AnimatedImageMediaStreamer::prepareAsync_l Start");
    
    modifyFlags(END, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(CONNECTING, SET);
    
    gotError = false;
    
    mQueue.postEvent(mAsyncPrepareEvent);
    
    LOGI("AnimatedImageMediaStreamer::prepareAsync_l End");
    
}

void AnimatedImageMediaStreamer::onPrepareAsyncEvent()
{
    LOGI("AnimatedImageMediaStreamer::onPrepareAsyncEvent");
    
    AutoLock autoLock(&mLock);
    
    //connecting
    notifyListener_l(MEDIA_STREAMER_CONNECTING);
    int ret = open_all_pipelines_l();
    if (ret>=0) {
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(CONNECTED, SET);
        
        notifyListener_l(MEDIA_STREAMER_CONNECTED);
        
        play_l();
    }else {
        notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_CONNECT_FAIL);
        
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(ERRORED, SET);
        gotError = false;
        stop_l();
    }
}

void AnimatedImageMediaStreamer::pause()
{
    AutoLock autoLock(&mLock);
    return pause_l();
}

void AnimatedImageMediaStreamer::pause_l()
{
    if (!(mFlags & STREAMING)) {
        return;
    }
    
    modifyFlags(STREAMING, CLEAR);
    
    mQueue.cancelEvent(mVideoEvent->eventID());
    mVideoEventPending = false;
    
    if (mVideoOptions->hasVideo) {
        //close video data source
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        if (mVideoFrameBufferPool!=NULL) {
            mVideoFrameBufferPool->flush();
            canInputVideoFrame = false;
        }
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    }
    
    //Timing End
    MediaTimer::offLine();
    
    modifyFlags(PAUSED, SET);
    
    notifyListener_l(MEDIA_STREAMER_PAUSED);
    
    return;
}

void AnimatedImageMediaStreamer::resume()
{
    AutoLock autoLock(&mLock);
    
    return play_l();
}


void AnimatedImageMediaStreamer::play_l()
{
    LOGI("AnimatedImageMediaStreamer::play_l Start");
    
    if (mFlags & STREAMING) {
        LOGW("%s","AnimatedImageMediaStreamer is streaming");
        return;
    }
    
    if (!(mFlags & CONNECTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    modifyFlags(CONNECTED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    //Timing Begin
    MediaTimer::onLine();
    
    if (mVideoOptions->hasVideo) {
        //open video data source
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        if (mVideoFrameBufferPool!=NULL) {
            mVideoFrameBufferPool->flush();
            canInputVideoFrame = true;
        }
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
        
        postVideoEvent_l();
    }
    
    modifyFlags(STREAMING, SET);
    
    notifyListener_l(MEDIA_STREAMER_STREAMING);
    
    LOGI("AnimatedImageMediaStreamer::play_l End");
}

void AnimatedImageMediaStreamer::cancelStreamerEvents()
{
    mQueue.cancelEvent(mVideoEvent->eventID());
    mVideoEventPending = false;
    mQueue.cancelEvent(mNotifyEvent->eventID());
    mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
    mQueue.cancelEvent(mStopEvent->eventID());
}

void AnimatedImageMediaStreamer::stop(bool isCancle)
{
    LOGI("AnimatedImageMediaStreamer::stop");
    
    AutoLock autoLock(&mLock);
    
    if (mFlags & END) {
        return;
    }
    
    if (mFlags & ENDING) {
        notifyListener_l(MEDIA_STREAMER_INFO,MEDIA_STREAMER_INFO_ALREADY_ENDING);
        
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    if (isCancle) {
        gotError = true;
    }
    
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
    
    if (isCancle) {
        if (mMuxerFormat == MP4 || mMuxerFormat==GIF) {
            //delete local mp4 file
            if (MediaFile::isExist(mPublishUrl)) {
                MediaFile::deleteFile(mPublishUrl);
            }
        }
    }
}

void AnimatedImageMediaStreamer::stop_l()
{
    LOGI("AnimatedImageMediaStreamer::stop_l");
    
    modifyFlags(ENDING, ASSIGN);
    
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void AnimatedImageMediaStreamer::postVideoEvent_l(int64_t delayUs)
{
    if (mVideoEventPending) {
        return;
    }
    mVideoEventPending = true;
    mQueue.postEventWithDelay(mVideoEvent, delayUs < 0 ? 0 : delayUs);
}

void AnimatedImageMediaStreamer::onStopEvent()
{
    LOGI("AnimatedImageMediaStreamer::onStopEvent");
    
    AutoLock autoLock(&mLock);
    
    close_all_pipelines_l();
    
    cancelStreamerEvents();
    mNotificationQueue.flush();
    
    //close video data source
    pthread_mutex_lock(&mVideoFrameBufferPoolLock);
    if (mVideoFrameBufferPool!=NULL) {
        mVideoFrameBufferPool->flush();
        canInputVideoFrame = false;
    }
    pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    
    //Timing End
    MediaTimer::offLine();
    
    modifyFlags(END, ASSIGN);
    
    if (!gotError) {
        notifyListener_l(MEDIA_STREAMER_END);
    }else{
        if (mMuxerFormat == MP4 || mMuxerFormat == GIF) {
            //delete local file
            if (MediaFile::isExist(mPublishUrl)) {
                MediaFile::deleteFile(mPublishUrl);
            }
        }
    }
    gotError = false;
    pthread_cond_broadcast(&mStopCondition);
}

void AnimatedImageMediaStreamer::onVideoEvent()
{
    AutoLock autoLock(&mLock);
    
    mVideoEventTimer_StartTime = GetNowUs();
    
    if (!mVideoEventPending) {
        // The event has been cancelled in reset_l() but had already // been scheduled for execution at that time.
        return;
    }
    mVideoEventPending = false;
    
    int ret = flowing_l();
    
    mVideoEventTimer_EndTime = GetNowUs();
    
    mVideoEventTime = mVideoEventTimer_EndTime-mVideoEventTimer_StartTime;
    
    if (ret==0) {
        int64_t waitTimeUs = 0ll;
        waitTimeUs = 10*1000; //10ms
        postVideoEvent_l(waitTimeUs);
        
        mVideoDelayTimeUs = mVideoDelayTimeUs + waitTimeUs + mVideoEventTime;
        
        return;
        
    }else if(ret<0) {
        modifyFlags(STREAMING, CLEAR);
        modifyFlags(ERRORED, SET);
        
        if(ret==-1)
        {
            notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_COLORSPACECONVERT_FAIL);
        }
        
        if(ret==-2)
        {
            notifyListener_l(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_VIDEO_ENCODE_FAIL);
        }
        gotError = true;
        stop_l();
        
        return;
    }else {
        
        int64_t delayUs = 1000*1000/mTargetFps-mVideoEventTime-mVideoDelayTimeUs;
        
        if (delayUs>=0) {
            mVideoDelayTimeUs = 0;
        }else {
            mVideoDelayTimeUs = -delayUs;
        }
        
        postVideoEvent_l(delayUs);
    }
}

void AnimatedImageMediaStreamer::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

// return -1 : Open VideoEncoder Fail
// return -2 : Open FFmpegMuxer  Fail
// return -3 : Open AudioCapture Fail
// return -4 : Open AudioEncoder Fail
// return -5 : Open AudioFilter  Fail
int AnimatedImageMediaStreamer::open_all_pipelines_l()
{
    LOGI("AnimatedImageMediaStreamer::open_all_pipelines_l");
    
    if (mVideoOptions->hasVideo) {
        //open ColorSpaceConvert
        mColorSpaceConvert = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
        
        if (mMuxerFormat == GIF) {
            mAnimatedGifCreaterType = GIF_H;
            mAnimatedGifCreater = AnimatedGifCreater::CreateAnimatedGifCreater(mAnimatedGifCreaterType, mPublishUrl);
            mAnimatedGifCreater->open(mVideoOptions->videoWidth, mVideoOptions->videoHeight, 1000/mVideoOptions->videoFps);
            mTargetVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
            if (mAnimatedGifCreaterType==GIF_H) {
                mTargetVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_ABGR;
            }else if(mAnimatedGifCreaterType==GIFFLEN) {
                mTargetVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_BGRA;
            }
        }
    }
    
    return 0;
}

void AnimatedImageMediaStreamer::close_all_pipelines_l()
{
    LOGD("close_all_pipelines_l");
    
    if (mVideoOptions->hasVideo && mAnimatedGifCreater!=NULL) {
        AnimatedGifCreater::DeleteAnimatedGifCreater(mAnimatedGifCreater, GIF_H);
        mAnimatedGifCreater = NULL;
        LOGD("AnimatedGifCreater Release Success");
    }
    
    if (mVideoOptions->hasVideo && mColorSpaceConvert!=NULL) {
        ColorSpaceConvert::DeleteColorSpaceConvert(mColorSpaceConvert, LIBYUV);
        mColorSpaceConvert = NULL;
        LOGD("ColorSpaceConvert Release Success");
    }
}

// Android-SoftEncode
int AnimatedImageMediaStreamer::flowing_l()
{
    //Get One Video Frame
    pthread_mutex_lock(&mVideoFrameBufferPoolLock);
    VideoFrame * videoFrame = mVideoFrameBufferPool->front();
    if (videoFrame==NULL)
    {
        //        LOGD("VideoFrameBufferPool is empty");
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
        return 0;
    }
    pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
    
    if (!isGotFirstFrame) {
        isGotFirstFrame = true;
        mStartTimeMs = videoFrame->pts;
    }
    
    int64_t lastRecordTimeMs = videoFrame->pts - mStartTimeMs;
    
    if ((lastRecordTimeMs-mLastSendRecordTimeMs)>=100/*100ms*/) {
        mLastSendRecordTimeMs = lastRecordTimeMs;
        notifyListener_l(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_PUBLISH_TIME, (int)(mLastSendRecordTimeMs/100));
    }
    
    if (isNeedConvertVideoFrame(videoFrame, &mTargetVideoFrame)) {
        //ColorSpace Convert
        bool ret = false;
        
        if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV21 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420) {
            ret = mColorSpaceConvert->NV21toI420_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, videoFrame->rotation);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV21 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_NV12){
            ret = mColorSpaceConvert->NV21toNV12_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, videoFrame->rotation);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV21 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_NV21){
            ret = mColorSpaceConvert->NV21toNV21_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, videoFrame->rotation);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_RGBA && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420){
            ret = mColorSpaceConvert->ABGRtoI420_Crop(videoFrame, &mTargetVideoFrame);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_BGRA && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_BGRA){
            ret = mColorSpaceConvert->BGRAtoBGRA_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_BGRA && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420){
            ret = mColorSpaceConvert->ARGBtoI420_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV12 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420){
            ret = mColorSpaceConvert->NV12toI420_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, videoFrame->rotation);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_I420 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_I420){
            ret = mColorSpaceConvert->I420toI420_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_I420 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_BGRA){
            ret = mColorSpaceConvert->I420toBGRA_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_I420 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_RGBA){
            ret = mColorSpaceConvert->I420toRGBA_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_I420 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_ABGR){
            ret = mColorSpaceConvert->I420toABGR_Crop_Rotation_Scale(videoFrame, &mTargetVideoFrame, 0);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV12 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_RGBA){
            ret = mColorSpaceConvert->NV12toRGBA_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV12 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_BGRA){
            ret = mColorSpaceConvert->NV12toBGRA_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV12 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_ARGB){
            ret = mColorSpaceConvert->NV12toARGB_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_NV12 && mTargetVideoFrame.videoRawType==VIDEOFRAME_RAWTYPE_ABGR){
            ret = mColorSpaceConvert->NV12toABGR_Crop_Scale(videoFrame, &mTargetVideoFrame);
        }else {
            //todo
        }
        
        //pop the Video Frame
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        mVideoFrameBufferPool->pop();
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
        
        
        if (!ret) {
            LOGE("ColorSpaceConvert Fail!!");
            return -1;
        }
        
        if (!mAnimatedGifCreater) {
            LOGW("AnimatedGifCreater is closed");
            return 0;
        }
        
        ret = mAnimatedGifCreater->writeFrame(&mTargetVideoFrame);
        if (!ret) {
            LOGE("AnimatedGifCreater WriteFrame Fail!!");
            return -2;
        }
        
    }else {
        if (!mAnimatedGifCreater) {
            LOGW("AnimatedGifCreater is closed");
            return 0;
        }
        
        bool ret = mAnimatedGifCreater->writeFrame(videoFrame);
        
        //pop the Video Frame
        pthread_mutex_lock(&mVideoFrameBufferPoolLock);
        mVideoFrameBufferPool->pop();
        pthread_mutex_unlock(&mVideoFrameBufferPoolLock);
        
        if (!ret) {
            LOGE("AnimatedGifCreater WriteFrame Fail!!");
            return -2;
        }
    }
    
    //for fps statistics
    flowingFrameCountPerTime++;
    if (realfps_begin_time==0) {
        realfps_begin_time = MediaTimer::GetNowMediaTimeMS();
    }
    
    int64_t realfps_duration = MediaTimer::GetNowMediaTimeMS()-realfps_begin_time;
    if (realfps_duration>=1000) {
        mRealFps = flowingFrameCountPerTime*1000/realfps_duration;
        
        realfps_begin_time = 0;
        flowingFrameCountPerTime = 0;
        
        notifyListener_l(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS, mRealFps);
    }
    
    return 1;
}

bool AnimatedImageMediaStreamer::isNeedConvertVideoFrame(VideoFrame *sourceFrame, VideoFrame *targetFrame)
{
    if (sourceFrame->rotation!=0 || sourceFrame->width!=targetFrame->width || sourceFrame->height!=targetFrame->height || sourceFrame->videoRawType!=targetFrame->videoRawType) {
        return true;
    }
    
    return false;
}
