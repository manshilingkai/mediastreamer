//
//  MediaStreamer.cpp
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaStreamer.h"
#include "SLKMediaStreamer.h"
#include "AnimatedImageMediaStreamer.h"

#ifdef ANDROID
MediaStreamer* MediaStreamer::CreateMediaStreamer(MediaStreamerType type, JavaVM *jvm, const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions, int reConnectTimes)
{
    if (type == MEDIA_STREAMER_SLK) {
        return new SLKMediaStreamer(jvm, publishUrl, backupDir, videoOptions, audioOptions, reConnectTimes);
    }
    
    if (type == MEDIA_STREAMER_ANIMATEDIMAGE) {
        return new AnimatedImageMediaStreamer(jvm, publishUrl, videoOptions);
    }
    
    return NULL;
}
#else
MediaStreamer* MediaStreamer::CreateMediaStreamer(MediaStreamerType type, const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions, char* backgroundPngCover, int reConnectTimes)
{
    if (type == MEDIA_STREAMER_SLK) {
        return new SLKMediaStreamer(publishUrl, backupDir, videoOptions, audioOptions, backgroundPngCover, reConnectTimes);
    }
    
    if (type == MEDIA_STREAMER_ANIMATEDIMAGE) {
        return new AnimatedImageMediaStreamer(publishUrl, videoOptions);
    }
    
    return NULL;
}
#endif

void MediaStreamer::DeleteMediaStreamer(MediaStreamer *mediaStreamer, MediaStreamerType type)
{
    if (type == MEDIA_STREAMER_SLK) {
        SLKMediaStreamer* slkMediaStreamer = (SLKMediaStreamer*)mediaStreamer;
        delete slkMediaStreamer;
        slkMediaStreamer = NULL;
    }
    
    if (type == MEDIA_STREAMER_ANIMATEDIMAGE) {
        AnimatedImageMediaStreamer* animatedImageMediaStreamer = (AnimatedImageMediaStreamer*)mediaStreamer;
        delete animatedImageMediaStreamer;
        animatedImageMediaStreamer = NULL;
    }
}
