//
//  SLKStreamerOptions.h
//  MediaStreamer
//
//  Created by Think on 2016/12/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <AVFoundation/AVFoundation.h>

typedef enum VIDEO_ENCODE_TYPE{
    VIDEO_HARDWARE_ENCODE = 0,
    VIDEO_SOFTWARE_ENCODE = 1
}VIDEO_ENCODE_TYPE;

typedef enum VIDEO_CAPTURE_SOURCE {
    CAMERA_CAPTURE_SOURCE = 0,
    SCREEN_CAPTURE_SOURCE = 1,
}VIDEO_CAPTURE_SOURCE;

typedef enum AUDIO_CAPTURE_SOURCE {
    INTERNAL_MIC_SOURCE = 0,
    EXTERNAL_AUDIO_SOURCE = 1,
}AUDIO_CAPTURE_SOURCE;

@interface SLKStreamerOptions : NSObject

@property (nonatomic, strong) NSString *publishUrl;
@property (nonatomic, strong) NSString *backupDir;

@property (nonatomic) BOOL hasVideo;
@property (nonatomic) VIDEO_CAPTURE_SOURCE videoCaptureSource;
@property (nonatomic) VIDEO_ENCODE_TYPE videoEncodeType;
@property (nonatomic) CGSize videoSize;
@property (nonatomic) NSInteger fps;
@property (nonatomic) NSInteger videoBitrate;
@property (nonatomic) NSInteger maxKeyFrameIntervalMs;
@property (nonatomic) NSInteger networkAdaptiveMinFps;
@property (nonatomic) NSInteger networkAdaptiveMinVideoBitrate;
@property (nonatomic) BOOL isEnableNetworkAdaptive;
@property (nonatomic, strong) NSString *backgroundPngCover;

@property (nonatomic) BOOL hasAudio;
@property (nonatomic) AUDIO_CAPTURE_SOURCE audioCaptureSource;
@property (nonatomic) BOOL isControlAudioSession;
@property (nonatomic) NSInteger audioBitrate;
@property (nonatomic) BOOL isEnableAGC;

@property (nonatomic) NSInteger reConnectTimes;
@property (nonatomic) BOOL isEnablePerformanceMonitor;

@end
