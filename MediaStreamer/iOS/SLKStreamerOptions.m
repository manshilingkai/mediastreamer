//
//  SLKStreamerOptions.m
//  MediaStreamer
//
//  Created by Think on 2016/12/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "SLKStreamerOptions.h"

@implementation SLKStreamerOptions

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.publishUrl = nil;
        self.backupDir = NSTemporaryDirectory();
        
        self.hasVideo = YES;
        self.videoCaptureSource = CAMERA_CAPTURE_SOURCE;
        self.videoEncodeType = VIDEO_HARDWARE_ENCODE;
        self.videoSize = CGSizeMake(480,640);
        self.fps = 20;
        self.videoBitrate = 500;
        self.maxKeyFrameIntervalMs = 3000;
        self.networkAdaptiveMinFps = 12;
        self.networkAdaptiveMinVideoBitrate = 200;
        self.isEnableNetworkAdaptive = YES;
        self.backgroundPngCover = nil;
        
        self.hasAudio = YES;
        self.audioCaptureSource = INTERNAL_MIC_SOURCE;
        self.isControlAudioSession = NO;
        self.audioBitrate = 32; //high:64
        self.isEnableAGC = NO;
        
        self.reConnectTimes = 3;
        self.isEnablePerformanceMonitor = NO; //todo
    }
    
    return self;
}

@end
