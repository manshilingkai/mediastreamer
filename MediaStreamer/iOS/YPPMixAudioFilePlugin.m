//
//  YPPMixAudioFilePlugin.m
//  MediaStreamer
//
//  Created by Think on 2019/11/27.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "YPPMixAudioFilePlugin.h"
#import "AudioPlayerCommon.h"
#include "AudioPlayerWrapper.h"
#include <mutex>

@implementation YPPMixAudioFilePluginOptions
- (instancetype) init
{
    self = [super init];
    if (self) {
        self.pluginType = YPPMixAudioFilePlugin_SEM;
        self.sampleRate = 44100;
        self.channelCount = 2;
        self.externalRender = NO;
    }
    
    return self;
}
@end

@implementation YPPMixAudioFilePlugin
{
    int sampleRate;
    int channelCount;
    BOOL externalRender;
    
    AudioPlayerWrapper *pAudioPlayerWrapper;
    
    dispatch_queue_t mixAudioFilePluginWorkQueue;
    
    std::mutex mMixAudioFilePluginMutex;
    
    int mPluginType;
}

- (instancetype) initWithOptions:(YPPMixAudioFilePluginOptions*)options
{
    self = [super init];
    if (self) {
        mPluginType = [options pluginType];
        sampleRate = [options sampleRate];
        channelCount = [options channelCount];
        externalRender = [options externalRender];
        
        if (externalRender) {
            pAudioPlayerWrapper = AudioPlayer_GetInstanceWithRenderAndMixOptions(sampleRate, channelCount, true);
        }else {
            pAudioPlayerWrapper = AudioPlayer_GetInstanceWithRenderAndMixOptions(sampleRate, channelCount, false);
        }
        
        if (pAudioPlayerWrapper) {
            AudioPlayer_setListener(pAudioPlayerWrapper, audioPlayerPluginNotificationListener, (__bridge void*)self);
        }
        
        mixAudioFilePluginWorkQueue = dispatch_queue_create("MixAudioFilePluginWorkQueue", 0);
    }
    
    return self;
}

void audioPlayerPluginNotificationListener(void* owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak YPPMixAudioFilePlugin *thiz = (__bridge YPPMixAudioFilePlugin*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchAudioPlayerPluginNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchAudioPlayerPluginNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    if (event==AUDIO_PLAYER_ERROR || event==AUDIO_PLAYER_PLAYBACK_COMPLETE) {
        dispatch_async(mixAudioFilePluginWorkQueue, ^{
            mMixAudioFilePluginMutex.lock();
            if (pAudioPlayerWrapper) {
                AudioPlayer_stop(pAudioPlayerWrapper);
            }
            mMixAudioFilePluginMutex.unlock();
            
            if (mPluginType==YPPMixAudioFilePlugin_BGM) {
                if (event==AUDIO_PLAYER_ERROR) {
                    if (self.delegate!=nil) {
                        if (([self.delegate respondsToSelector:@selector(onMixAudioFileError:ErrorType:)])) {
                            [self.delegate onMixAudioFileError:mPluginType ErrorType:ext1];
                        }
                    }
                }else if(event==AUDIO_PLAYER_PLAYBACK_COMPLETE)
                {
                    if (self.delegate!=nil) {
                        if (([self.delegate respondsToSelector:@selector(onMixAudioFileEnd:)])) {
                            [self.delegate onMixAudioFileEnd:mPluginType];
                        }
                    }
                }
            }
        });
    }
}

- (void)playAudioFile:(NSString*)audioFilePath
{
    mMixAudioFilePluginMutex.lock();
    if (pAudioPlayerWrapper) {
        AudioPlayer_stop(pAudioPlayerWrapper);
    }
    mMixAudioFilePluginMutex.unlock();
    
    dispatch_barrier_sync(mixAudioFilePluginWorkQueue, ^{
        NSLog(@"Drain MixAudioFilePluginWorkQueue");
    });
    
    mMixAudioFilePluginMutex.lock();
    if (pAudioPlayerWrapper) {
        AudioPlayer_setDataSource(pAudioPlayerWrapper, (char*)[audioFilePath UTF8String]);
        AudioPlayer_prepareAsyncToPlay(pAudioPlayerWrapper);
    }
    mMixAudioFilePluginMutex.unlock();
}

- (void)playAudioFile:(NSString*)audioFilePath Volume:(float)volume
{
    mMixAudioFilePluginMutex.lock();
    if (pAudioPlayerWrapper) {
        AudioPlayer_stop(pAudioPlayerWrapper);
    }
    mMixAudioFilePluginMutex.unlock();
    
    dispatch_barrier_sync(mixAudioFilePluginWorkQueue, ^{
        NSLog(@"Drain MixAudioFilePluginWorkQueue");
    });
    
    mMixAudioFilePluginMutex.lock();
    if (pAudioPlayerWrapper) {
        AudioPlayer_setDataSource(pAudioPlayerWrapper, (char*)[audioFilePath UTF8String]);
        AudioPlayer_setVolume(pAudioPlayerWrapper, volume);
        AudioPlayer_prepareAsyncToPlay(pAudioPlayerWrapper);
    }
    mMixAudioFilePluginMutex.unlock();
}

- (void)prepareToPlay:(NSString*)audioFilePath
{
//    mMixAudioFilePluginMutex.lock();
//    if (pAudioPlayerWrapper) {
//        AudioPlayer_stop(pAudioPlayerWrapper);
//    }
//    mMixAudioFilePluginMutex.unlock();
    
    dispatch_barrier_sync(mixAudioFilePluginWorkQueue, ^{
        NSLog(@"Drain MixAudioFilePluginWorkQueue");
    });
    
    mMixAudioFilePluginMutex.lock();
    if (pAudioPlayerWrapper) {
        AudioPlayer_setDataSource(pAudioPlayerWrapper, (char*)[audioFilePath UTF8String]);
        AudioPlayer_prepareAsyncToPlay(pAudioPlayerWrapper);
    }
    mMixAudioFilePluginMutex.unlock();
}

- (void)prepareToPlay:(NSString*)audioFilePath Volume:(float)volume
{
//    mMixAudioFilePluginMutex.lock();
//    if (pAudioPlayerWrapper) {
//        AudioPlayer_stop(pAudioPlayerWrapper);
//    }
//    mMixAudioFilePluginMutex.unlock();
    
    dispatch_barrier_sync(mixAudioFilePluginWorkQueue, ^{
        NSLog(@"Drain MixAudioFilePluginWorkQueue");
    });
    
    mMixAudioFilePluginMutex.lock();
    if (pAudioPlayerWrapper) {
        AudioPlayer_setDataSource(pAudioPlayerWrapper, (char*)[audioFilePath UTF8String]);
        AudioPlayer_setVolume(pAudioPlayerWrapper, volume);
        AudioPlayer_prepareAsyncToPlay(pAudioPlayerWrapper);
    }
    mMixAudioFilePluginMutex.unlock();
}

- (void)setVolume:(float)volume
{
    mMixAudioFilePluginMutex.lock();
    if (pAudioPlayerWrapper) {
        AudioPlayer_setVolume(pAudioPlayerWrapper, volume);
    }
    mMixAudioFilePluginMutex.unlock();
}

- (void)play
{
    mMixAudioFilePluginMutex.lock();
    if (pAudioPlayerWrapper) {
        AudioPlayer_play(pAudioPlayerWrapper);
    }
    mMixAudioFilePluginMutex.unlock();
}

- (void)pause
{
    mMixAudioFilePluginMutex.lock();
    if (pAudioPlayerWrapper) {
        AudioPlayer_pause(pAudioPlayerWrapper);
    }
    mMixAudioFilePluginMutex.unlock();
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    mMixAudioFilePluginMutex.lock();
    if (pAudioPlayerWrapper) {
        AudioPlayer_seekTo(pAudioPlayerWrapper, seekPosMs);
    }
    mMixAudioFilePluginMutex.unlock();
}

- (void)stop
{
    mMixAudioFilePluginMutex.lock();
    if (pAudioPlayerWrapper) {
        AudioPlayer_stop(pAudioPlayerWrapper);
    }
    mMixAudioFilePluginMutex.unlock();
    
    dispatch_barrier_sync(mixAudioFilePluginWorkQueue, ^{
        NSLog(@"Drain MixAudioFilePluginWorkQueue");
    });
}

- (NSTimeInterval)currentPlaybackTimeMs
{
    NSTimeInterval currentPosition = 0;
    
    mMixAudioFilePluginMutex.lock();
    
    if (pAudioPlayerWrapper!=NULL) {
        currentPosition = AudioPlayer_getPlayTimeMs(pAudioPlayerWrapper);
    }
    
    mMixAudioFilePluginMutex.unlock();
    
    return currentPosition;
}

- (NSTimeInterval)durationMs
{
    NSTimeInterval dur = 0;
    
    mMixAudioFilePluginMutex.lock();
    
    if (pAudioPlayerWrapper!=NULL) {
        dur = AudioPlayer_getDurationMs(pAudioPlayerWrapper);
    }
    
    mMixAudioFilePluginMutex.unlock();
    
    return dur;
}

- (AudioBuffer*)mixWithExternal:(AudioBuffer*)audioBuffer
{
    void* audio = audioBuffer->mData;
    int size = audioBuffer->mDataByteSize;
    
    mMixAudioFilePluginMutex.lock();
    if (pAudioPlayerWrapper) {
        AudioPlayer_mixWithExternal(pAudioPlayerWrapper, (char*)audio, size);
    }
    mMixAudioFilePluginMutex.unlock();
    
    return audioBuffer;
}

- (void)drainPCMDataFromExternal:(void *)pData dataLen:(int *)pDataLen
{
    mMixAudioFilePluginMutex.lock();
    if (pAudioPlayerWrapper) {
        int ret = AudioPlayer_drainPCMDataFromExternal(pAudioPlayerWrapper, &pData, *pDataLen);
        if (ret<*pDataLen) {
            char* data = (char*)pData;
            memset(data+ret, 0, *pDataLen-ret);
        }
    }
    mMixAudioFilePluginMutex.unlock();
}

- (void)dealloc
{
    if (pAudioPlayerWrapper) {
        AudioPlayer_ReleaseInstance(&pAudioPlayerWrapper);
        pAudioPlayerWrapper = NULL;
    }
    
    dispatch_barrier_sync(mixAudioFilePluginWorkQueue, ^{
        NSLog(@"Drain MixAudioFilePluginWorkQueue");
    });
    
    NSLog(@"YPPMixAudioFilePlugin dealloc");
}

@end
