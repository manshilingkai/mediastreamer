//
//  SLKStreamer.m
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "SLKStreamer.h"

#ifdef ENABLE_SCREEN_CAPTURE
#import <ReplayKit/ReplayKit.h>
#include <OpenGLES/ES2/glext.h>
#import <QuartzCore/QuartzCore.h>
#include <CoreVideo/CoreVideo.h>
#endif

#include "SLKMediaStreamerWrapper.h"
#include <mutex>

#import "Reachability.h"

static int MEDIASTREAMER_STATE_IDLE = 0;
static int MEDIASTREAMER_STATE_INITIALIZED = 1;
static int MEDIASTREAMER_STATE_STARTING = 2;
static int MEDIASTREAMER_STATE_STARTED = 3;
static int MEDIASTREAMER_STATE_PAUSED = 4;

@interface SLKStreamer () {}
//KeepALive
@property (nonatomic) UIBackgroundTaskIdentifier    backgroundTaskIdentifier;
@property (nonatomic, strong) AVAudioPlayer         *keepALiveAudioPlayer;
//Reachability
-(void)reachabilityChanged:(NSNotification*)note;
@property(nonatomic, strong) Reachability *baiduReach;
@property(nonatomic) NetworkStatus currentNetworkStatus;
@end

@implementation SLKStreamer
{
    SLKMediaStreamerWrapper* pSLKMediaStreamerWrapper;
    std::mutex mSLKMediaStreamerWrapperMutex;
    
    dispatch_queue_t notificationQueue;
    
    BOOL isInterrupt;
    BOOL hasAudio;
    BOOL isControlAudioSession;
    BOOL isExternalAudioInput;
    
#ifdef ENABLE_SCREEN_CAPTURE
    int outputScreenWidth;
    int outputScreenHeight;
    CGImagePropertyOrientation outputScreenOrientation;
    CIContext *ciContext;
    EAGLContext* eaglContext;
#endif
    
    int currentStreamerState;
    
    dispatch_source_t timer;
    bool isTiming;
    
    int mBgmId;
    int mSemId;
    
    char* mWavData;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pSLKMediaStreamerWrapper = NULL;
        
        isInterrupt = NO;
        hasAudio = NO;
        isControlAudioSession = NO;
        isExternalAudioInput = NO;
        
#ifdef ENABLE_SCREEN_CAPTURE
        outputScreenWidth = 0;
        outputScreenHeight = 0;
        outputScreenOrientation = kCGImagePropertyOrientationUp;
        ciContext = nil;
        eaglContext = nil;
#endif
        
        currentStreamerState = MEDIASTREAMER_STATE_IDLE;
        
        timer =  nil;
        isTiming = false;
                
        mBgmId = 0;
        mSemId = 0;
        
        notificationQueue = dispatch_queue_create("SLKStreamerNotificationQueue", 0);
        
        mWavData = NULL;
        self.backgroundTaskIdentifier = UIBackgroundTaskInvalid;
        self.keepALiveAudioPlayer = nil;
        
        self.baiduReach = nil;
        self.currentNetworkStatus = NotReachable;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillResignActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillTerminateNotification object:nil];
    }
    
    return self;
}

- (void)audioSessionInterrupt:(NSNotification *)notification
{
    int reason = [[[notification userInfo] valueForKey:AVAudioSessionInterruptionTypeKey] intValue];
    switch (reason) {
        case AVAudioSessionInterruptionTypeBegan: {
            
            mSLKMediaStreamerWrapperMutex.lock();
            if (pSLKMediaStreamerWrapper!=NULL) {
                SLKMediaStreamerWrapper_iOSAVAudioSessionInterruption(pSLKMediaStreamerWrapper,true);
            }
            isInterrupt = YES;
            mSLKMediaStreamerWrapperMutex.unlock();

            NSError *error = nil;
            if (NO == [[AVAudioSession sharedInstance] setActive:NO error:&error]) {
                NSLog(@"AVAudioSession.setActive(NO) failed: %@\n", error ? [error localizedDescription] : @"nil");
            }
            NSLog(@"AVAudioSessionInterruptionTypeBegan");
            
            [self startForceHandleAVAudioSessionTask];
            
            break;
        }
        case AVAudioSessionInterruptionTypeEnded: {
            
            [self stopForceHandleAVAudioSessionTask];
            
            NSError *error = nil;
            if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers error:&error]) {
                NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
            }
            error = nil;
            if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeVoiceChat error:&error]) {
                NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
            }
            while (true) {
                error = nil;
                BOOL ret = [[AVAudioSession sharedInstance] setActive:YES error:&error];
                if (ret) {
                    break;
                }else{
                    NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
                    
                    error = nil;
                    if (NO == [[AVAudioSession sharedInstance] setActive:NO error:&error]) {
                        NSLog(@"AVAudioSession.setActive(NO) failed: %@\n", error ? [error localizedDescription] : @"nil");
                    }
                }
            }
            
            mSLKMediaStreamerWrapperMutex.lock();
            if (pSLKMediaStreamerWrapper!=NULL) {
                SLKMediaStreamerWrapper_iOSAVAudioSessionInterruption(pSLKMediaStreamerWrapper,false);
            }
            isInterrupt = NO;
            mSLKMediaStreamerWrapperMutex.unlock();
            
            NSLog(@"AVAudioSessionInterruptionTypeEnded");
            
            break;
        }
    }
}

- (void)initializeWithOptions:(SLKStreamerOptions*)options
{
    BOOL isNeedMonitorNetWork = NO;
    
    mSLKMediaStreamerWrapperMutex.lock();
    if (currentStreamerState != MEDIASTREAMER_STATE_IDLE) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if([options hasAudio])
    {
        hasAudio = YES;
    }
    if (options.audioCaptureSource==EXTERNAL_AUDIO_SOURCE) {
        isControlAudioSession = NO;
    }else{
        isControlAudioSession = options.isControlAudioSession;
    }
    
    VideoOptions videoOptions;
    AudioOptions audioOptions;
    
    audioOptions.hasAudio = [options hasAudio];
    if (options.audioCaptureSource==INTERNAL_MIC_SOURCE) {
        audioOptions.isExternalAudioInput = false;
        isExternalAudioInput = NO;
    }else{
        audioOptions.isExternalAudioInput = true;
        isExternalAudioInput = YES;
    }
    audioOptions.audioSampleRate = 44100;
    audioOptions.audioNumChannels = 2;
    if (options.audioCaptureSource==INTERNAL_MIC_SOURCE) {
        audioOptions.audioNumChannels = 2;
    }else if(options.audioCaptureSource==EXTERNAL_AUDIO_SOURCE) {
        audioOptions.audioNumChannels = 1;
    }
    audioOptions.audioBitRate = [options audioBitrate]; //k
    if ([options isEnableAGC]) {
        audioOptions.audioSampleRate = 48000;
    }
    
    videoOptions.hasVideo = [options hasVideo];
    videoOptions.videoEncodeType = [options videoEncodeType];
    videoOptions.videoWidth = [options videoSize].width;
    videoOptions.videoHeight = [options videoSize].height;
    videoOptions.videoFps = [options fps];
    if (videoOptions.videoEncodeType==VIDEO_HARD_ENCODE) {
        if (options.videoCaptureSource==CAMERA_CAPTURE_SOURCE) {
            videoOptions.videoRawType = VIDEOFRAME_RAWTYPE_BGRA;
        }else {
            videoOptions.videoRawType = VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF;
        }
    }else {
        videoOptions.videoRawType = VIDEOFRAME_RAWTYPE_I420;
    }
    videoOptions.videoProfile = 0; //0:base_line 1:main_profile 2:high_profile
    videoOptions.videoBitRate = [options videoBitrate];
    videoOptions.encodeMode = 1;//CBR
    videoOptions.maxKeyFrameIntervalMs = [options maxKeyFrameIntervalMs];;
    
    videoOptions.networkAdaptiveMinFps = [options networkAdaptiveMinFps];
    videoOptions.networkAdaptiveMinVideoBitrate = [options networkAdaptiveMinVideoBitrate];
    if ([options isEnableNetworkAdaptive]) {
        videoOptions.isEnableNetworkAdaptive = true;
    }else {
        videoOptions.isEnableNetworkAdaptive = false;
    }
    
    //for X264
    videoOptions.quality = 0;
    videoOptions.bStrictCBR = true;
    videoOptions.deblockingFilterFactor = 0;
    
#ifdef ENABLE_SCREEN_CAPTURE
    outputScreenWidth = videoOptions.videoWidth;
    outputScreenHeight = videoOptions.videoHeight;
    outputScreenOrientation = kCGImagePropertyOrientationUp;
    eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    NSDictionary *cioptions = @{ kCIContextWorkingColorSpace : [NSNull null] };
    ciContext = [CIContext contextWithEAGLContext:eaglContext options:cioptions];
#endif
    
#if TARGET_IPHONE_SIMULATOR
    videoOptions.hasVideo = NO;
    videoOptions.videoEncodeType=VIDEO_SOFT_ENCODE;
#endif
    
    char* backup_dir = NULL;
    char* background_png_cover = NULL;
    if ([options backupDir]) {
        backup_dir = (char*)[[options backupDir] UTF8String];
    }
    if ([options backgroundPngCover]) {
        background_png_cover = (char*)[[options backgroundPngCover] UTF8String];
    }
    pSLKMediaStreamerWrapper = GetInstance([[options publishUrl] UTF8String], backup_dir, videoOptions, audioOptions, background_png_cover, [options reConnectTimes]);

    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_setListener(pSLKMediaStreamerWrapper, notificationListener, (__bridge void*)self);
    }
    
    if (hasAudio && isControlAudioSession) {
        NSError *error = nil;
        if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers error:&error]) {
            NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
        }
        
        error = nil;
        if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeVoiceChat error:&error]) {
            NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
        }
        
        error = nil;
        if (NO == [[AVAudioSession sharedInstance] setActive:YES error:&error]) {
            NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioSessionInterrupt:) name:AVAudioSessionInterruptionNotification object:nil];
    }
    
    if ([[options publishUrl] hasPrefix:@"rtmp://"] || [[options publishUrl] hasPrefix:@"rtsp://"]) {
        isNeedMonitorNetWork = YES;
    }else{
        isNeedMonitorNetWork = NO;
    }
    
    mBgmId = 0;
    mSemId = 0;
    
    currentStreamerState = MEDIASTREAMER_STATE_INITIALIZED;
    
    mSLKMediaStreamerWrapperMutex.unlock();
    
    if (isNeedMonitorNetWork) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChanged:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
        self.baiduReach = [Reachability reachabilityWithHostname:@"www.baidu.com"];
        [self.baiduReach startNotifier];
    }
}

-(void)reachabilityChanged:(NSNotification*)note
{
    Reachability* reach = [note object];
    if(reach == self.baiduReach)
    {
        BOOL isReachable = [reach isReachable];
        NetworkStatus currentReachabilityStatus = reach.currentReachabilityStatus;
        
        __weak __block typeof(self) weakself = self;
        dispatch_async(notificationQueue, ^{
            if(isReachable)
            {
                if (weakself.currentNetworkStatus==NotReachable) {
                    // NetWork Connected
                    if (currentReachabilityStatus==ReachableViaWiFi) {
                        mSLKMediaStreamerWrapperMutex.lock();
                        if (pSLKMediaStreamerWrapper!=NULL) {
                            SLKMediaStreamerWrapper_netWorkReachabilityNotify(pSLKMediaStreamerWrapper, SLKNetworkReachableViaWiFi);
                        }
                        mSLKMediaStreamerWrapperMutex.unlock();
                    }else if(currentReachabilityStatus==ReachableViaWWAN) {
                        mSLKMediaStreamerWrapperMutex.lock();
                        if (pSLKMediaStreamerWrapper!=NULL) {
                            SLKMediaStreamerWrapper_netWorkReachabilityNotify(pSLKMediaStreamerWrapper, SLKNetworkReachableViaWWAN);
                        }
                        mSLKMediaStreamerWrapperMutex.unlock();
                    }
                }
                
                if (weakself.currentNetworkStatus!=NotReachable && currentReachabilityStatus!=weakself.currentNetworkStatus) {
                    // Switch NetWork From Wifi To 4G Or From 4G To Wifi
                    mSLKMediaStreamerWrapperMutex.lock();
                    if (pSLKMediaStreamerWrapper!=NULL) {
                        SLKMediaStreamerWrapper_netWorkReachabilityNotify(pSLKMediaStreamerWrapper, SLKNetworkNotReachable);
                    }
                    mSLKMediaStreamerWrapperMutex.unlock();
                    
                    if (currentReachabilityStatus==ReachableViaWiFi) {
                        mSLKMediaStreamerWrapperMutex.lock();
                        if (pSLKMediaStreamerWrapper!=NULL) {
                            SLKMediaStreamerWrapper_netWorkReachabilityNotify(pSLKMediaStreamerWrapper, SLKNetworkReachableViaWiFi);
                        }
                        mSLKMediaStreamerWrapperMutex.unlock();
                    }else if(currentReachabilityStatus==ReachableViaWWAN) {
                        mSLKMediaStreamerWrapperMutex.lock();
                        if (pSLKMediaStreamerWrapper!=NULL) {
                            SLKMediaStreamerWrapper_netWorkReachabilityNotify(pSLKMediaStreamerWrapper, SLKNetworkReachableViaWWAN);
                        }
                        mSLKMediaStreamerWrapperMutex.unlock();
                    }
                }
            }
            else
            {
                if (weakself.currentNetworkStatus!=NotReachable) {
                    // NetWork Broken
                    mSLKMediaStreamerWrapperMutex.lock();
                    if (pSLKMediaStreamerWrapper!=NULL) {
                        SLKMediaStreamerWrapper_netWorkReachabilityNotify(pSLKMediaStreamerWrapper, SLKNetworkNotReachable);
                    }
                    mSLKMediaStreamerWrapperMutex.unlock();
                }
            }
            weakself.currentNetworkStatus = currentReachabilityStatus;
        });
    }
}

- (void)KeepALiveAudioPlayer_Play
{
    if (!self.keepALiveAudioPlayer) {
        if (mWavData) {
            free(mWavData);
            mWavData = NULL;
        }
        mWavData = (char*)malloc(44100*16/8+44);
        SLKMediaStreamerWrapper_CreateWavData(&mWavData, 44100*16/8+44, 1, 44100);
        self.keepALiveAudioPlayer = [[AVAudioPlayer alloc] initWithData: [NSData dataWithBytes:mWavData length:176444] error:nil];
        self.keepALiveAudioPlayer.numberOfLoops = -1;
        self.keepALiveAudioPlayer.volume = 0.0f;
        [self.keepALiveAudioPlayer prepareToPlay];
    }else{
        [self.keepALiveAudioPlayer play];
    }
}

- (void)KeepALiveAudioPlayer_Pause
{
    if (self.keepALiveAudioPlayer) {
        [self.keepALiveAudioPlayer pause];
    }
}

- (void)KeepALiveAudioPlayer_Stop
{
    if (self.keepALiveAudioPlayer) {
        [self.keepALiveAudioPlayer stop];
        self.keepALiveAudioPlayer = nil;
        
        if (mWavData) {
            free(mWavData);
            mWavData = NULL;
        }
    }
}

- (void)applicationDidEnterBackground {
    if (UIDevice.currentDevice.systemVersion.floatValue < 13.0)
    {
        self.backgroundTaskIdentifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
            [self KeepALiveAudioPlayer_Play];
            if (self.backgroundTaskIdentifier != UIBackgroundTaskInvalid) {
                [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTaskIdentifier];
                self.backgroundTaskIdentifier = UIBackgroundTaskInvalid;
            }
        }];
    }else{
        self.backgroundTaskIdentifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
            if (self.backgroundTaskIdentifier != UIBackgroundTaskInvalid) {
                [self KeepALiveAudioPlayer_Play];
                [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTaskIdentifier];
                self.backgroundTaskIdentifier = UIBackgroundTaskInvalid;
            }
        }];
    }
}

- (void)applicationWillEnterForeground {
    [self KeepALiveAudioPlayer_Pause];
    [[UIApplication sharedApplication] endBackgroundTask: self.backgroundTaskIdentifier];
}

- (void)applicationActiveNotification: (NSNotification*) notification {
    
    if ([notification.name isEqualToString:UIApplicationWillTerminateNotification]) {
        [self terminate];
        return;
    }
    
    if([notification.name isEqualToString:UIApplicationWillResignActiveNotification] || [notification.name isEqualToString:UIApplicationDidEnterBackgroundNotification]) {
        
        if ([notification.name isEqualToString:UIApplicationWillResignActiveNotification]) {
            NSLog(@"UIApplicationWillResignActiveNotification");
        }
        if ([notification.name isEqualToString:UIApplicationDidEnterBackgroundNotification]) {
            NSLog(@"UIApplicationDidEnterBackgroundNotification");
            [self applicationDidEnterBackground];
        }
    } else if([notification.name isEqualToString:UIApplicationWillEnterForegroundNotification] || [notification.name isEqualToString:UIApplicationDidBecomeActiveNotification]) {
        
        if ([notification.name isEqualToString:UIApplicationWillEnterForegroundNotification]) {
            NSLog(@"UIApplicationWillEnterForegroundNotification");
            [self applicationWillEnterForeground];
        }
        if ([notification.name isEqualToString:UIApplicationDidBecomeActiveNotification]) {
            NSLog(@"UIApplicationDidBecomeActiveNotification");
        }
    }
}

void notificationListener(void* owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak SLKStreamer *thiz = (__bridge SLKStreamer*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    __weak typeof(self) wself = self;
    
    dispatch_async(notificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf handleNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    });
}

- (void)handleNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    mSLKMediaStreamerWrapperMutex.lock();
    if (currentStreamerState==MEDIASTREAMER_STATE_IDLE || currentStreamerState==MEDIASTREAMER_STATE_INITIALIZED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        return;
    }
    mSLKMediaStreamerWrapperMutex.unlock();
    
    switch (event) {
        case SLK_MEDIA_STREAMER_CONNECTING:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotConnecting)])) {
                    [self.delegate gotConnecting];
                }
            }
            break;
        case SLK_MEDIA_STREAMER_CONNECTED:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(didConnected)])) {
                    [self.delegate didConnected];
                }
            }
            break;
            
        case SLK_MEDIA_STREAMER_STREAMING:
            mSLKMediaStreamerWrapperMutex.lock();
            currentStreamerState = MEDIASTREAMER_STATE_STARTED;
            mSLKMediaStreamerWrapperMutex.unlock();
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotStreaming)])) {
                    [self.delegate gotStreaming];
                }
            }
            break;
        case SLK_MEDIA_STREAMER_PAUSED:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(didPaused)])) {
                    [self.delegate didPaused];
                }
            }
            break;
            
        case SLK_MEDIA_STREAMER_END:
            mSLKMediaStreamerWrapperMutex.lock();
            currentStreamerState = MEDIASTREAMER_STATE_INITIALIZED;
            mSLKMediaStreamerWrapperMutex.unlock();
            
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(didEndStreaming)])) {
                    [self.delegate didEndStreaming];
                }
            }
            break;
        case SLK_MEDIA_STREAMER_ERROR:
            mSLKMediaStreamerWrapperMutex.lock();
            currentStreamerState = MEDIASTREAMER_STATE_INITIALIZED;
            mSLKMediaStreamerWrapperMutex.unlock();
            
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotStreamerErrorWithErrorType:)])) {
                    [self.delegate gotStreamerErrorWithErrorType:ext1];
                }
            }
            break;
        case SLK_MEDIA_STREAMER_INFO:
            if (ext1==SLK_MEDIA_STREAMER_INFO_BGM_EOF) {
                mSLKMediaStreamerWrapperMutex.lock();
                if (ext2!=mBgmId)
                {
                    mSLKMediaStreamerWrapperMutex.unlock();
                    return;
                }else {
                    mSLKMediaStreamerWrapperMutex.unlock();
                }
            }else if(ext1==SLK_MEDIA_STREAMER_INFO_SEM_EOF) {
                mSLKMediaStreamerWrapperMutex.lock();
                if (ext2!=mSemId)
                {
                    mSLKMediaStreamerWrapperMutex.unlock();
                    return;
                }else{
                    mSLKMediaStreamerWrapperMutex.unlock();
                }
            }
            
            if (ext1==SLK_MEDIA_STREAMER_INFO_NETWORK_REACHABLE) {
                if (ext2<0) {
                    if (self.currentNetworkStatus==ReachableViaWiFi) {
                        ext2 = SLKNetworkReachableViaWiFi;
                    }else if (self.currentNetworkStatus==ReachableViaWWAN) {
                        ext2 = SLKNetworkReachableViaWWAN;
                    }
                }
            }
            
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotStreamerInfoWithInfoType:InfoValue:)])) {
                    [self.delegate gotStreamerInfoWithInfoType:ext1 InfoValue:ext2];
                }
            }
            break;
        case SLK_MEDIA_STREAMER_WARN:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotStreamerWarnWithWarnType:WarnValue:)])) {
                    [self.delegate gotStreamerWarnWithWarnType:ext1 WarnValue:ext2];
                }
            }
            break;

        default:
            break;
    }
}

- (void)pushPixelBuffer:(CVPixelBufferRef)pixelBuffer
               Rotation:(int)rotation
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        CVPixelBufferLockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
        uint8_t* data = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
        int frameSize = (int)CVPixelBufferGetDataSize(pixelBuffer);
        int width = (int)CVPixelBufferGetWidth(pixelBuffer);
        int height = (int)CVPixelBufferGetHeight(pixelBuffer);
        int videoRawType;
        int kCVPixelFormatType = CVPixelBufferGetPixelFormatType(pixelBuffer);//
        switch (kCVPixelFormatType) {
            case kCVPixelFormatType_32BGRA:
                videoRawType = VIDEOFRAME_RAWTYPE_BGRA; //BGRA
                break;
            case kCVPixelFormatType_420YpCbCr8BiPlanarFullRange: //NV12
                videoRawType = VIDEOFRAME_RAWTYPE_NV12;
                break;
            default:
                videoRawType = VIDEOFRAME_RAWTYPE_BGRA; //BGRA
                break;
        }
        SLKMediaStreamerWrapper_inputVideoFrame(pSLKMediaStreamerWrapper,data,frameSize,width,height,videoRawType,rotation);
        CVPixelBufferUnlockBaseAddress(pixelBuffer,kCVPixelBufferLock_ReadOnly);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)pushAudioSourceBuffer:(CMSampleBufferRef)sampleBuffer WithAudioSourceType:(int)audioSourceType
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        return;
    }
        
    if (isExternalAudioInput && pSLKMediaStreamerWrapper!=NULL) {
        if (CMSampleBufferDataIsReady(sampleBuffer) == FALSE)
        {
            NSLog(@"CMSampleBufferData Is Not Ready");
            mSLKMediaStreamerWrapperMutex.unlock();
            return;
        }
        
        AudioStreamBasicDescription inAudioStreamBasicDescription = *CMAudioFormatDescriptionGetStreamBasicDescription((CMAudioFormatDescriptionRef)CMSampleBufferGetFormatDescription(sampleBuffer));
        int sampleRate = inAudioStreamBasicDescription.mSampleRate;
        int channels = inAudioStreamBasicDescription.mChannelsPerFrame;
        int bitsPerChannel = inAudioStreamBasicDescription.mBitsPerChannel;
        int formatFlags = inAudioStreamBasicDescription.mFormatFlags;
        bool isBigEndian = false;
        if((formatFlags & kAudioFormatFlagIsBigEndian) != 0 ) {
            isBigEndian = true;
        }
        
        AudioBufferList audioBufferList;
        CMBlockBufferRef blockBuffer;

        CMSampleBufferGetAudioBufferListWithRetainedBlockBuffer(sampleBuffer, NULL, &audioBufferList, sizeof(audioBufferList), NULL, NULL, 0, &blockBuffer);
        
        for( int i=0; i<audioBufferList.mNumberBuffers; i++) {
            AudioBuffer audioBuffer = audioBufferList.mBuffers[i];
            void* audio = audioBuffer.mData;
            if (audioSourceType==SLKAudioSourceLocalMic) {
                SLKMediaStreamerWrapper_inputAudioFrameWithSourceType(pSLKMediaStreamerWrapper, AudioFrameSourceMic, (uint8_t *)audio, audioBuffer.mDataByteSize, 0, sampleRate, channels, bitsPerChannel, isBigEndian);
            }else if(audioSourceType==SLKAudioSourceRemote) {
                SLKMediaStreamerWrapper_inputAudioFrameWithSourceType(pSLKMediaStreamerWrapper, AudioFrameSourceRemote, (uint8_t *)audio, audioBuffer.mDataByteSize, 0, sampleRate, channels, bitsPerChannel, isBigEndian);
            }
        }
        CFRelease(blockBuffer);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();

}

- (void)pushVideoSampleBuffer:(CMSampleBufferRef)sampleBuffer VideoOrientation:(int)videoOrientation
{
#ifdef ENABLE_SCREEN_CAPTURE
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        CVPixelBufferRef pixelBuffer = [self resizeAndRotateVideoSampleBuffer:sampleBuffer VideoOrientation:(CGImagePropertyOrientation)videoOrientation];
        if (pixelBuffer) {
            bool ret = SLKMediaStreamerWrapper_inputCVPixelBufferRef(pSLKMediaStreamerWrapper, (void*)pixelBuffer, VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF);
            if (!ret) {
                CVBufferRelease(pixelBuffer);
            }
        }
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
#endif
}

- (CVPixelBufferRef)resizeAndRotateVideoSampleBuffer:(CMSampleBufferRef)sampleBuffer VideoOrientation:(CGImagePropertyOrientation)videoOrientation
{
#ifdef ENABLE_SCREEN_CAPTURE
    #ifdef __IPHONE_11_1
    if (UIDevice.currentDevice.systemVersion.floatValue >= 11.1) {
        CFStringRef RPVideoSampleOrientationKeyRef = (__bridge CFStringRef)RPVideoSampleOrientationKey;
        NSNumber *orientation = (NSNumber *)CMGetAttachment(sampleBuffer, RPVideoSampleOrientationKeyRef,NULL);
        if (orientation!=nil) {
            outputScreenOrientation = (CGImagePropertyOrientation)[orientation integerValue];
        }
    }
    #endif
    
    outputScreenOrientation = videoOrientation;

    CIImage *outputImage = nil;
    CVPixelBufferRef sourcePixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CIImage *sourceImage = [CIImage imageWithCVPixelBuffer:sourcePixelBuffer];
    sourceImage = [sourceImage imageByApplyingOrientation:outputScreenOrientation];
    CGFloat outputWidth  = outputScreenWidth;
    CGFloat outputHeight = outputScreenHeight;
    CGFloat inputWidth = sourceImage.extent.size.width;
    CGFloat inputHeight = sourceImage.extent.size.height;
    CGAffineTransform tranfrom = CGAffineTransformMakeScale(outputWidth/inputWidth, outputHeight/inputHeight);
    outputImage = [sourceImage imageByApplyingTransform:tranfrom];
    
    NSDictionary* pixelBufferOptions = @{
        (NSString*) kCVPixelBufferWidthKey : @(outputScreenWidth),
        (NSString*) kCVPixelBufferHeightKey : @(outputScreenHeight),
        (NSString*) kCVPixelBufferOpenGLESCompatibilityKey : @YES,
        (NSString*) kCVPixelBufferIOSurfacePropertiesKey : @{}
    };
    CVPixelBufferRef outputPixelBuffer = nil;
    CVReturn ret = CVPixelBufferCreate(kCFAllocatorDefault, outputScreenWidth, outputScreenHeight, kCVPixelFormatType_32BGRA, (__bridge CFDictionaryRef)pixelBufferOptions, &outputPixelBuffer);

    if (ret!=noErr) {
        return nil;
    }
    
    EAGLContext* current = [EAGLContext currentContext];
    [EAGLContext setCurrentContext:eaglContext];
    [ciContext render:outputImage toCVPixelBuffer:outputPixelBuffer bounds:outputImage.extent colorSpace:CGColorSpaceCreateDeviceRGB()];
    [EAGLContext setCurrentContext:current];
    
    return outputPixelBuffer;
#endif
    return nil;
}

- (void)pushAudioSampleBuffer:(CMSampleBufferRef)sampleBuffer
{
#ifdef ENABLE_SCREEN_CAPTURE
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        return;
    }
    
    if (isExternalAudioInput && pSLKMediaStreamerWrapper!=NULL) {
        
        if (CMSampleBufferDataIsReady(sampleBuffer) == FALSE)
        {
            NSLog(@"CMSampleBufferData Is Not Ready");
            mSLKMediaStreamerWrapperMutex.unlock();
            return;
        }
        
        AudioStreamBasicDescription inAudioStreamBasicDescription = *CMAudioFormatDescriptionGetStreamBasicDescription((CMAudioFormatDescriptionRef)CMSampleBufferGetFormatDescription(sampleBuffer));
        int sampleRate = inAudioStreamBasicDescription.mSampleRate;
        int channels = inAudioStreamBasicDescription.mChannelsPerFrame;
        int bitsPerChannel = inAudioStreamBasicDescription.mBitsPerChannel;
        int formatFlags = inAudioStreamBasicDescription.mFormatFlags;
        bool isBigEndian = false;
        if((formatFlags & kAudioFormatFlagIsBigEndian) != 0 ) {
            isBigEndian = true;
        }
        
//        NSLog(@"sampleRate : %d",sampleRate);
//        NSLog(@"channels : %d",channels);
//        NSLog(@"bitsPerChannel : %d",bitsPerChannel);
//        NSLog(@"formatFlags : %d",formatFlags);
//
//        if ((formatFlags & kAudioFormatFlagIsFloat) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsFloat");
//        }else if((formatFlags & kAudioFormatFlagIsBigEndian) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsBigEndian");
//        }else if((formatFlags & kAudioFormatFlagIsSignedInteger) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsSignedInteger");
//        }else if((formatFlags & kAudioFormatFlagIsPacked) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsPacked");
//        }else if((formatFlags & kAudioFormatFlagIsAlignedHigh) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsAlignedHigh");
//        }else if((formatFlags & kAudioFormatFlagIsNonInterleaved) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsNonInterleaved");
//        }else if((formatFlags & kAudioFormatFlagIsNonMixable) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsNonMixable");
//        }
        
        AudioBufferList audioBufferList;
        CMBlockBufferRef blockBuffer;
        
        CMSampleBufferGetAudioBufferListWithRetainedBlockBuffer(sampleBuffer, NULL, &audioBufferList, sizeof(audioBufferList), NULL, NULL, 0, &blockBuffer);
        
        for( int i=0; i<audioBufferList.mNumberBuffers; i++) {
            AudioBuffer audioBuffer = audioBufferList.mBuffers[i];
            void* audio = audioBuffer.mData;
            SLKMediaStreamerWrapper_inputAudioFrame(pSLKMediaStreamerWrapper, (uint8_t *)audio, audioBuffer.mDataByteSize, 0, sampleRate, channels, bitsPerChannel, isBigEndian);
        }
        CFRelease(blockBuffer);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
#endif
}

- (void)pushAudioSampleBuffer:(CMSampleBufferRef)sampleBuffer WithType:(int)sampleBufferType
{
#ifdef ENABLE_SCREEN_CAPTURE
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        return;
    }
    
    if (isExternalAudioInput && pSLKMediaStreamerWrapper!=NULL) {
        
        if (CMSampleBufferDataIsReady(sampleBuffer) == FALSE)
        {
            NSLog(@"CMSampleBufferData Is Not Ready");
            mSLKMediaStreamerWrapperMutex.unlock();
            return;
        }
        
        AudioStreamBasicDescription inAudioStreamBasicDescription = *CMAudioFormatDescriptionGetStreamBasicDescription((CMAudioFormatDescriptionRef)CMSampleBufferGetFormatDescription(sampleBuffer));
        int sampleRate = inAudioStreamBasicDescription.mSampleRate;
        int channels = inAudioStreamBasicDescription.mChannelsPerFrame;
        int bitsPerChannel = inAudioStreamBasicDescription.mBitsPerChannel;
        int formatFlags = inAudioStreamBasicDescription.mFormatFlags;
        bool isBigEndian = false;
        if((formatFlags & kAudioFormatFlagIsBigEndian) != 0 ) {
            isBigEndian = true;
        }
        
//        NSLog(@"sampleRate : %d",sampleRate);
//        NSLog(@"channels : %d",channels);
//        NSLog(@"bitsPerChannel : %d",bitsPerChannel);
//        NSLog(@"formatFlags : %d",formatFlags);
//        if ((formatFlags & kAudioFormatFlagIsFloat) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsFloat");
//        }else if((formatFlags & kAudioFormatFlagIsBigEndian) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsBigEndian");
//        }else if((formatFlags & kAudioFormatFlagIsSignedInteger) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsSignedInteger");
//        }else if((formatFlags & kAudioFormatFlagIsPacked) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsPacked");
//        }else if((formatFlags & kAudioFormatFlagIsAlignedHigh) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsAlignedHigh");
//        }else if((formatFlags & kAudioFormatFlagIsNonInterleaved) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsNonInterleaved");
//        }else if((formatFlags & kAudioFormatFlagIsNonMixable) != 0 ) {
//            NSLog(@"kAudioFormatFlagIsNonMixable");
//        }
        
        AudioBufferList audioBufferList;
        CMBlockBufferRef blockBuffer;
        
        CMSampleBufferGetAudioBufferListWithRetainedBlockBuffer(sampleBuffer, NULL, &audioBufferList, sizeof(audioBufferList), NULL, NULL, 0, &blockBuffer);
        
        for( int i=0; i<audioBufferList.mNumberBuffers; i++) {
            AudioBuffer audioBuffer = audioBufferList.mBuffers[i];
            void* audio = audioBuffer.mData;
            if (sampleBufferType==RPSampleBufferTypeAudioMic) {
                SLKMediaStreamerWrapper_inputAudioFrameWithSourceType(pSLKMediaStreamerWrapper, AudioFrameSourceMic, (uint8_t *)audio, audioBuffer.mDataByteSize, 0, sampleRate, channels, bitsPerChannel, isBigEndian);
//                NSLog(@"SLKMediaStreamerWrapper_inputAudioFrameWithSourceType AudioFrameSourceMic : %d", audioBuffer.mDataByteSize);
            }else if(sampleBufferType==RPSampleBufferTypeAudioApp) {
                SLKMediaStreamerWrapper_inputAudioFrameWithSourceType(pSLKMediaStreamerWrapper, AudioFrameSourceApp, (uint8_t *)audio, audioBuffer.mDataByteSize, 0, sampleRate, channels, bitsPerChannel, isBigEndian);
//                NSLog(@"SLKMediaStreamerWrapper_inputAudioFrameWithSourceType AudioFrameSourceApp : %d", audioBuffer.mDataByteSize);
            }
        }
        CFRelease(blockBuffer);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
#endif
}

- (void)pushText:(NSString*)text
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_inputText(pSLKMediaStreamerWrapper, (char*)[text UTF8String], [text length]+1);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)start
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_INITIALIZED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (hasAudio && isControlAudioSession) {
        NSError *error = nil;
        if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers error:&error]) {
            NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
        }
        error = nil;
        if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeVoiceChat error:&error]) {
            NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
        }
        
        while (true) {
            error = nil;
            BOOL ret = [[AVAudioSession sharedInstance] setActive:YES error:&error];
            if (ret) {
                break;
            }else{
                NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
                
                error = nil;
                if (NO == [[AVAudioSession sharedInstance] setActive:NO error:&error]) {
                    NSLog(@"AVAudioSession.setActive(NO) failed: %@\n", error ? [error localizedDescription] : @"nil");
                }
            }
        }
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_start(pSLKMediaStreamerWrapper);
    }
    
    currentStreamerState = MEDIASTREAMER_STATE_STARTING;
    
    mSLKMediaStreamerWrapperMutex.unlock();

}
- (void)resume
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_PAUSED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_resume(pSLKMediaStreamerWrapper);
    }
    
    currentStreamerState = MEDIASTREAMER_STATE_STARTED;
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)pause
{
    mSLKMediaStreamerWrapperMutex.lock();

    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_pause(pSLKMediaStreamerWrapper);
    }
    
    currentStreamerState = MEDIASTREAMER_STATE_PAUSED;
    
    mSLKMediaStreamerWrapperMutex.unlock();

}
- (void)stop
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState == MEDIASTREAMER_STATE_IDLE || currentStreamerState == MEDIASTREAMER_STATE_INITIALIZED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_stop(pSLKMediaStreamerWrapper);
    }
    
    currentStreamerState = MEDIASTREAMER_STATE_INITIALIZED;
    
    mSLKMediaStreamerWrapperMutex.unlock();
    
    [self stopForceHandleAVAudioSessionTask];
}

- (BOOL)isStreaming
{
    BOOL ret = NO;
    mSLKMediaStreamerWrapperMutex.lock();
    if (currentStreamerState == MEDIASTREAMER_STATE_STARTED || currentStreamerState == MEDIASTREAMER_STATE_PAUSED) {
        ret = YES;
    }
    mSLKMediaStreamerWrapperMutex.unlock();
    
    return ret;
}

- (void)enableAudio:(BOOL)isEnable
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState == MEDIASTREAMER_STATE_IDLE) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_enableAudio(pSLKMediaStreamerWrapper, isEnable);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)mixBGM:(NSString*)bgmUrl Volume:(float)volume Loop:(int)numberOfLoops
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED && currentStreamerState != MEDIASTREAMER_STATE_PAUSED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        mBgmId++;
        SLKMediaStreamerWrapper_mixBGM(pSLKMediaStreamerWrapper, mBgmId, (char*)[bgmUrl UTF8String], volume, numberOfLoops);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)playBGM
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED && currentStreamerState != MEDIASTREAMER_STATE_PAUSED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_playBGM(pSLKMediaStreamerWrapper);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)pauseBGM
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED && currentStreamerState != MEDIASTREAMER_STATE_PAUSED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_pauseBGM(pSLKMediaStreamerWrapper);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)stopBGM
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED && currentStreamerState != MEDIASTREAMER_STATE_PAUSED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_stopBGM(pSLKMediaStreamerWrapper);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)setBGMVolume:(float)volume
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED && currentStreamerState != MEDIASTREAMER_STATE_PAUSED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_setBGMVolume(pSLKMediaStreamerWrapper, volume);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)mixSEM:(NSString*)semUrl Volume:(float)volume Loop:(int)numberOfLoops
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState != MEDIASTREAMER_STATE_STARTED && currentStreamerState != MEDIASTREAMER_STATE_PAUSED) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        mSemId++;
        SLKMediaStreamerWrapper_mixSEM(pSLKMediaStreamerWrapper, mSemId, (char*)[semUrl UTF8String], volume, numberOfLoops);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)iOSDidEnterBackground
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState == MEDIASTREAMER_STATE_IDLE) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_iOSDidEnterBackground(pSLKMediaStreamerWrapper);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)iOSDidBecomeActive
{
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState == MEDIASTREAMER_STATE_IDLE) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        SLKMediaStreamerWrapper_iOSDidBecomeActive(pSLKMediaStreamerWrapper);
    }
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)terminate
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    if (self.baiduReach) {
        [self.baiduReach stopNotifier];
        self.baiduReach = nil;
    }
    
    mSLKMediaStreamerWrapperMutex.lock();
    
    if (currentStreamerState == MEDIASTREAMER_STATE_IDLE) {
        mSLKMediaStreamerWrapperMutex.unlock();
        NSLog(@"Error State: %d", currentStreamerState);
        return;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionInterruptionNotification object:nil];
    
    if (pSLKMediaStreamerWrapper!=NULL) {
        ReleaseInstance(&pSLKMediaStreamerWrapper);
        pSLKMediaStreamerWrapper = NULL;
    }

#ifdef ENABLE_SCREEN_CAPTURE
    eaglContext = nil;
#endif
    
    currentStreamerState = MEDIASTREAMER_STATE_IDLE;
    
    mSLKMediaStreamerWrapperMutex.unlock();
    
    [self stopForceHandleAVAudioSessionTask];
        
    dispatch_barrier_sync(notificationQueue, ^{
        NSLog(@"finish all notifications");
    });
}

- (void)startForceHandleAVAudioSessionTask
{
    mSLKMediaStreamerWrapperMutex.lock();

    NSTimeInterval period = 1.0;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, period * NSEC_PER_SEC, 0.0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(timer, ^{
        mSLKMediaStreamerWrapperMutex.lock();
        NSError *error = nil;
        if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers error:&error]) {
            NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
        }
        error = nil;
        if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeVoiceChat error:&error]) {
            NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
        }
        int retry_times = 10;
        while (retry_times) {
            error = nil;
            BOOL ret = [[AVAudioSession sharedInstance] setActive:YES error:&error];
            if (ret) {
                NSLog(@"Force Handle AVAudioSession Success");
                
                if (pSLKMediaStreamerWrapper!=NULL) {
                    SLKMediaStreamerWrapper_iOSAVAudioSessionInterruption(pSLKMediaStreamerWrapper,false);
                }
                
 //               dispatch_suspend(timer);
                dispatch_source_cancel(timer);
                isTiming = false;
                break;
            }else{
                NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
                error = nil;
                if (NO == [[AVAudioSession sharedInstance] setActive:NO error:&error]) {
                    NSLog(@"AVAudioSession.setActive(NO) failed: %@\n", error ? [error localizedDescription] : @"nil");
                }
            }
            retry_times--;
        }
        
        mSLKMediaStreamerWrapperMutex.unlock();
    });
    dispatch_resume(timer);
    
    isTiming = true;
    
    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)stopForceHandleAVAudioSessionTask
{
    mSLKMediaStreamerWrapperMutex.lock();

    if (isTiming) {
//        dispatch_suspend(timer);
        dispatch_source_cancel(timer);
        isTiming  = false;
    }

    mSLKMediaStreamerWrapperMutex.unlock();
}

- (void)dealloc
{
    [self terminate];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    
    
    [self KeepALiveAudioPlayer_Stop];
    mWavData = NULL;
    self.backgroundTaskIdentifier = UIBackgroundTaskInvalid;
    self.keepALiveAudioPlayer = nil;
    
    NSLog(@"SLKStreamer dealloc");
}

@end
