//
//  SLKCameraView.m
//  MediaStreamer
//
//  Created by Think on 2018/7/20.
//  Copyright © 2018年 Cell. All rights reserved.
//

#import "SLKCameraView.h"
#import "GPUImage.h"
#include <mutex>

#ifdef ENABLE_PRIVATE_FACE_DETECTION
#import "PrivateFaceDetection.h"
#endif

@implementation CameraOptions

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.cameraSessionPreset = AVCaptureSessionPreset1280x720;
        self.cameraPosition = AVCaptureDevicePositionFront;
        self.torchMode = AVCaptureTorchModeAuto;
        self.cameraZoomFactor = 0.0f;
        self.cameraFrameRate = 30;
        self.faceDetectionType = THIRDPARTY_FACE_DETECTION;
    }
    
    return self;
}
@end


@interface SLKCameraView () <SLKStreamerDelegate, GPUImageVideoCameraPrivateFaceDetectionDelegate, GPUImageVideoCameraDelegate, GPUImageVideoCameraExternalFilterDelegate>{
}

@property (nonatomic, strong) GPUImageVideoCamera   *videoCamera;
@property (nonatomic, strong) SLKStreamer           *slkStreamer;
//@property (nonatomic, strong) GPUImageBeauty2Filter *beautyFilter;
@property (nonatomic, strong) GPUImageFilter        *workFilter;
@property (nonatomic, strong) GPUImageRawDataOutput *rawDataOutput;
@property (nonatomic, strong) GPUImageMirrorFilter  *mirrorFilter;

#ifdef ENABLE_PRIVATE_FACE_DETECTION
@property (nonatomic, strong) PrivateFaceDetection  *privateFaceDetection;
#endif

@property (nonatomic, strong) SLKStreamerOptions    *mSLKStreamerOptions;
@end

static int SCV_IDLE = 0;
static int SCV_INITIALIZED = 1;
static int SCV_STARTED = 2;
static int SCV_PAUSED = 3;

@implementation SLKCameraView
{
    int currentState;
        
    AVCaptureTorchMode mTorchMode;
    
    CGFloat mCameraZoomFactor;
    
    BOOL isLivePublish;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.videoCamera = nil;
        self.slkStreamer = nil;
        self.workFilter = nil;
        self.rawDataOutput = nil;
        self.mirrorFilter = nil;
        
#ifdef ENABLE_PRIVATE_FACE_DETECTION
        self.privateFaceDetection = nil;
#endif
        
        self.mSLKStreamerOptions = nil;
        
        _delegate = nil;
        
        mTorchMode = AVCaptureTorchModeAuto;
        mCameraZoomFactor = 0.0f;
        
        isLivePublish = NO;
        
        currentState = SCV_IDLE;
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.videoCamera = nil;
        self.slkStreamer = nil;
        self.workFilter = nil;
        self.rawDataOutput = nil;
        self.mirrorFilter = nil;
        
#ifdef ENABLE_PRIVATE_FACE_DETECTION
        self.privateFaceDetection = nil;
#endif
        
        self.mSLKStreamerOptions = nil;
        
        _delegate = nil;
        
        mTorchMode = AVCaptureTorchModeAuto;
        mCameraZoomFactor = 0.0f;
        
        isLivePublish = NO;
                
        currentState = SCV_IDLE;
    }
    
    return self;
}

- (void)initialize:(CameraOptions*)options
{
    if (currentState!=SCV_IDLE) {
        return;
    }
    
//    self.videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:options.cameraSessionPreset cameraPosition:options.cameraPosition];
//    self.videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
//    self.videoCamera.frameRate = options.cameraFrameRate;
//    self.videoCamera.horizontallyMirrorFrontFacingCamera = YES;
    
    if ([options faceDetectionType] == PRIVATE_FACE_DETECTION) {
        self.videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:options.cameraSessionPreset cameraPosition:options.cameraPosition InnerFaceDetection:YES];
        self.videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
        self.videoCamera.frameRate = options.cameraFrameRate;
        self.videoCamera.horizontallyMirrorFrontFacingCamera = YES;
        self.videoCamera.private_face_detection_delegate = self;
        
#ifdef ENABLE_PRIVATE_FACE_DETECTION
        self.privateFaceDetection = [[PrivateFaceDetection alloc] init];
        [self.privateFaceDetection initialize];
#endif
    }else if([options faceDetectionType] == SYSTEM_FACE_DETECTION) {
        self.videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:options.cameraSessionPreset cameraPosition:options.cameraPosition];
        self.videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
        self.videoCamera.frameRate = options.cameraFrameRate;
        self.videoCamera.horizontallyMirrorFrontFacingCamera = YES;
        
        self.videoCamera.delegate = self;
    }else if([options faceDetectionType] == THIRDPARTY_FACE_DETECTION) {
        self.videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:options.cameraSessionPreset cameraPosition:options.cameraPosition];
        self.videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
        self.videoCamera.frameRate = options.cameraFrameRate;
        self.videoCamera.horizontallyMirrorFrontFacingCamera = YES;
        
        self.videoCamera.external_filter_delegate = self;
    }

    self.workFilter = [[GPUImageRGBFilter alloc] init];
    self.mirrorFilter = [[GPUImageMirrorFilter alloc] init];
    
    [self.videoCamera addTarget:self.workFilter];
    [self.workFilter addTarget:self];
    
    mTorchMode = options.torchMode;
    if (self.videoCamera.inputCamera!=nil) {
        NSError * error = nil;
        if([self.videoCamera.inputCamera lockForConfiguration:&error]){
            if ([self.videoCamera.inputCamera isTorchModeSupported:mTorchMode]) {
                [self.videoCamera.inputCamera setTorchMode:mTorchMode];
            }
            
            AVCaptureFlashMode setFlashMode;
            if(mTorchMode == AVCaptureTorchModeAuto) {
                setFlashMode = AVCaptureFlashModeAuto;
            } else if(mTorchMode == AVCaptureTorchModeOn) {
                setFlashMode = AVCaptureFlashModeOn;
            } else {
                setFlashMode = AVCaptureFlashModeOff;
            }
            
            if ([self.videoCamera.inputCamera isFlashModeSupported:setFlashMode]) {
                [self.videoCamera.inputCamera setFlashMode:setFlashMode];
            }
            
            [self.videoCamera.inputCamera unlockForConfiguration];
        }
    }
    
    mCameraZoomFactor = options.cameraZoomFactor;
    if (mCameraZoomFactor<0.0f) {
        mCameraZoomFactor = 0.0f;
    }
    if (mCameraZoomFactor>1.0f) {
        mCameraZoomFactor = 1.0f;
    }
    if (self.videoCamera.inputCamera!=nil) {
        NSError * error = nil;
        if([self.videoCamera.inputCamera lockForConfiguration:&error]){
            if (self.videoCamera.inputCamera.activeFormat.videoMaxZoomFactor > 1.0f) { //is Support Zoom
                CGFloat zoomFactor = pow(MIN(self.videoCamera.inputCamera.activeFormat.videoMaxZoomFactor, 4.0f), mCameraZoomFactor);
                [self.videoCamera.inputCamera rampToVideoZoomFactor:zoomFactor withRate:1.0f];
            }
            [self.videoCamera.inputCamera unlockForConfiguration];
        }
    }
    
    [self.videoCamera startCameraCapture];
    
    self.slkStreamer = [[SLKStreamer alloc] init];
    self.slkStreamer.delegate = self;
    
    self.mSLKStreamerOptions = nil;
        
    currentState = SCV_INITIALIZED;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationActiveNotification:) name:UIApplicationWillTerminateNotification object:nil];
}

- (void)iOSDidEnterBackground
{
    if (currentState==SCV_STARTED || currentState==SCV_PAUSED)
    {
        if (self.slkStreamer!=nil) {
            [self.slkStreamer iOSDidEnterBackground];
        }
    }
}

- (void)iOSDidBecomeActive
{
    if (currentState==SCV_STARTED || currentState==SCV_PAUSED)
    {
        if (self.slkStreamer!=nil) {
            [self.slkStreamer iOSDidBecomeActive];
        }
    }
}

- (void)applicationActiveNotification: (NSNotification*) notification {
    
    if ([notification.name isEqualToString:UIApplicationWillTerminateNotification]) {
        [self terminate];
        return;
    }
    
    if([notification.name isEqualToString:UIApplicationWillResignActiveNotification] || [notification.name isEqualToString:UIApplicationDidEnterBackgroundNotification]) {
        
        if ([notification.name isEqualToString:UIApplicationWillResignActiveNotification]) {
            NSLog(@"UIApplicationWillResignActiveNotification");
        }
        if ([notification.name isEqualToString:UIApplicationDidEnterBackgroundNotification]) {
            NSLog(@"UIApplicationDidEnterBackgroundNotification");
            if (currentState==SCV_IDLE || currentState==SCV_INITIALIZED) {
                return;
            }
            if (isLivePublish) {
                [self iOSDidEnterBackground];
            }else{
                [self stopPublish];
            }
        }
    } else if([notification.name isEqualToString:UIApplicationWillEnterForegroundNotification] || [notification.name isEqualToString:UIApplicationDidBecomeActiveNotification]) {
        
        if ([notification.name isEqualToString:UIApplicationWillEnterForegroundNotification]) {
            NSLog(@"UIApplicationWillEnterForegroundNotification");
        }
        
        if ([notification.name isEqualToString:UIApplicationDidBecomeActiveNotification]) {
            NSLog(@"UIApplicationDidBecomeActiveNotification");
            if (currentState==SCV_IDLE || currentState==SCV_INITIALIZED) {
                return;
            }
            if (isLivePublish) {
                [self iOSDidBecomeActive];
            }
        }
    }
}

- (void)setDisplayScalingMode:(int)mode
{
    if (mode == DISPLAY_SCALING_MODE_SCALE_TO_FILL) {
        self.fillMode = kGPUImageFillModeStretch;
    }else if (mode == DISPLAY_SCALING_MODE_SCALE_TO_FIT) {
        self.fillMode = kGPUImageFillModePreserveAspectRatio;
    }else if (mode == DISPLAY_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING) {
        self.fillMode = kGPUImageFillModePreserveAspectRatioAndFill;
    }
}

- (void)switchCamera
{
    if (currentState==SCV_IDLE) {
        return;
    }
    
    if (self.videoCamera!=nil) {
        [self.videoCamera rotateCamera];
    }
}

- (void)mirror
{
    if (currentState==SCV_IDLE) {
        return;
    }
    
    if (self.videoCamera!=nil) {
        if (self.videoCamera.frontFacingCameraPresent) {
            self.videoCamera.horizontallyMirrorFrontFacingCamera = !self.videoCamera.horizontallyMirrorFrontFacingCamera;
        }
        if (self.videoCamera.backFacingCameraPresent) {
            self.videoCamera.horizontallyMirrorRearFacingCamera = !self.videoCamera.horizontallyMirrorRearFacingCamera;
        }
    }
}

- (void)switchTorchMode
{
    if (currentState == SCV_IDLE) {
        return;
    }
    
    if (mTorchMode == AVCaptureTorchModeAuto) {
        mTorchMode = AVCaptureTorchModeOn;
    }else if(mTorchMode == AVCaptureTorchModeOn) {
        mTorchMode = AVCaptureTorchModeOff;
    }else if(mTorchMode == AVCaptureTorchModeOff) {
        mTorchMode = AVCaptureTorchModeAuto;
    }
    
    if (self.videoCamera!=nil && self.videoCamera.inputCamera!=nil) {
        NSError * error = nil;
        if([self.videoCamera.inputCamera lockForConfiguration:&error]){
            if ([self.videoCamera.inputCamera isTorchModeSupported:mTorchMode]) {
                [self.videoCamera.inputCamera setTorchMode:mTorchMode];
            }
            
            AVCaptureFlashMode setFlashMode;
            if(mTorchMode == AVCaptureTorchModeAuto) {
                setFlashMode = AVCaptureFlashModeAuto;
            } else if(mTorchMode == AVCaptureTorchModeOn){
                setFlashMode = AVCaptureFlashModeOn;
            } else {
                setFlashMode = AVCaptureFlashModeOff;
            }
            
            if ([self.videoCamera.inputCamera isFlashModeSupported:setFlashMode]) {
                [self.videoCamera.inputCamera setFlashMode:setFlashMode];
            }
            
            [self.videoCamera.inputCamera unlockForConfiguration];
        }
    }
}

- (void)setCameraZoomFactor:(CGFloat)factor
{
    if (currentState == SCV_IDLE) {
        return;
    }
    
    mCameraZoomFactor = factor;
    if (mCameraZoomFactor<0.0f) {
        mCameraZoomFactor = 0.0f;
    }
    if (mCameraZoomFactor>1.0f) {
        mCameraZoomFactor = 1.0f;
    }
    if (self.videoCamera!=nil && self.videoCamera.inputCamera!=nil) {
        NSError * error = nil;
        if([self.videoCamera.inputCamera lockForConfiguration:&error]){
            if (self.videoCamera.inputCamera.activeFormat.videoMaxZoomFactor > 1.0f) { //is Support Zoom
                CGFloat zoomFactor = pow(MIN(self.videoCamera.inputCamera.activeFormat.videoMaxZoomFactor, 4.0f), mCameraZoomFactor);
                [self.videoCamera.inputCamera rampToVideoZoomFactor:zoomFactor withRate:1.0f];
            }
            [self.videoCamera.inputCamera unlockForConfiguration];
        }
    }
}

- (void)setImageFilter:(int)filterType
{
    if (currentState==SCV_IDLE) {
        return;
    }
    
    [self.mirrorFilter removeAllTargets];
    [self.workFilter removeAllTargets];
    [self.videoCamera removeAllTargets];
    
    if (filterType == IMAGE_FILTER_SKETCH) {
        self.workFilter = [[GPUImageSketchFilter alloc] init];
    }else if(filterType == IMAGE_FILTER_COLORINVERT) {
        self.workFilter = [[GPUImageColorInvertFilter alloc] init];
    }else if (filterType == IMAGE_FILTER_GRAYSCALE) {
        self.workFilter = [[GPUImageGrayscaleFilter alloc] init];
    }else if (filterType == IMAGE_FILTER_PIXELLATE) {
        self.workFilter = [[GPUImagePixellateFilter alloc] init];
    }else if (filterType == IMAGE_FILTER_EMBOSS) {
        self.workFilter = [[GPUImageEmbossFilter alloc] init];
    }else if (filterType == IMAGE_FILTER_BEAUTY) {
        self.workFilter = [[GPUImageBeauty2Filter alloc] init];
    }else{
        self.workFilter = [[GPUImageRGBFilter alloc] init];
    }
    
    [self.workFilter addTarget:self];
    if (self.rawDataOutput!=nil) {
//        [self.workFilter addTarget:self.rawDataOutput];
        [self.workFilter addTarget:self.mirrorFilter];
        [self.mirrorFilter addTarget:self.rawDataOutput];
    }
    [self.videoCamera addTarget:self.workFilter];
}

- (void)mirrorPublish
{
    if (currentState==SCV_IDLE) {
        return;
    }
    
    if (self.mirrorFilter!=nil) {
        if (self.mirrorFilter.horizontalMirror) {
            [self.mirrorFilter setHorizontalMirror:NO];
        }else{
            [self.mirrorFilter setHorizontalMirror:YES];
        }
    }
}

- (UIImage *)takePhoto
{
    UIGraphicsBeginImageContext(self.frame.size);
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:NO];
    UIImage *image =  UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)pushPixelBuffer:(CVPixelBufferRef)pixelBuffer
               Rotation:(int)rotation
{
    if (self.slkStreamer!=nil) {
        [self.slkStreamer pushPixelBuffer:pixelBuffer Rotation:0];
    }
}

- (void)startPublish:(SLKStreamerOptions*)options
{
    [self stopPublish];
    
    if (currentState!=SCV_INITIALIZED) {
        return;
    }
    
    self.mSLKStreamerOptions = options;
    
    self.rawDataOutput = [[GPUImageRawDataOutput alloc] initWithImageSize:options.videoSize resultsInBGRAFormat:YES];
    [self.workFilter addTarget:self.mirrorFilter];
    [self.mirrorFilter addTarget:self.rawDataOutput];
//    [self.workFilter addTarget:self.rawDataOutput];
    __weak GPUImageRawDataOutput *weakOutput = self.rawDataOutput;
    __weak typeof(self) wself = self;
    [self.rawDataOutput setNewFrameAvailableBlock:^{
        __strong GPUImageRawDataOutput *strongOutput = weakOutput;
        __strong typeof(wself) strongSelf = wself;
        [strongOutput lockFramebufferForReading];
        GLubyte *outputBytes = [strongOutput rawBytesForImage];
        NSInteger bytesPerRow = [strongOutput bytesPerRowInOutput];
        CVPixelBufferRef pixelBuffer = NULL;
        CVPixelBufferCreateWithBytes(kCFAllocatorDefault, options.videoSize.width, options.videoSize.height, kCVPixelFormatType_32BGRA, outputBytes, bytesPerRow, nil, nil, nil, &pixelBuffer);
        [strongOutput unlockFramebufferAfterReading];
        if(pixelBuffer == NULL) {
            return ;
        }
        [strongSelf pushPixelBuffer:pixelBuffer Rotation:0];
        CVPixelBufferRelease(pixelBuffer);
    }];
    
    if ([[options publishUrl] hasPrefix:@"rtmp://"]) {
        isLivePublish = YES;
    }else {
        isLivePublish = NO;
    }

    [self.slkStreamer initializeWithOptions:options];
    [self.slkStreamer start];
    
    currentState = SCV_STARTED;
}

- (void)MakebackgroundPngCover:(NSString*)backgroundPngCover
{
    NSDictionary *pixelBufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   [NSDictionary dictionary], (id)kCVPixelBufferIOSurfacePropertiesKey,
                                                   nil];
    
    CVPixelBufferRef pixelBuffer = nil;
    CVPixelBufferCreate(NULL, 720, 1280, kCVPixelFormatType_32BGRA, (__bridge CFDictionaryRef)pixelBufferAttributes, &pixelBuffer);
    
    CVPixelBufferLockBaseAddress(pixelBuffer,0);
    uint8_t* data = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
    int frameSize = (int)CVPixelBufferGetDataSize(pixelBuffer);
    memset(data, 0, frameSize);
    for (int i = 0; i<frameSize; i+=4) {
        data[i+3] = 0xFF;
    }
    CVPixelBufferUnlockBaseAddress(pixelBuffer,0);

    CIImage *ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer];
    
    CIContext *temporaryContext = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [temporaryContext
                             createCGImage:ciImage
                             fromRect:CGRectMake(0, 0,
                                                 CVPixelBufferGetWidth(pixelBuffer),
                                                 CVPixelBufferGetHeight(pixelBuffer))];
    CVPixelBufferRelease(pixelBuffer);
    UIImage *uiImage = [UIImage imageWithCGImage:cgImage];
    NSData* imageData = UIImagePNGRepresentation(uiImage);
    [imageData writeToFile:backgroundPngCover atomically:NO];
    CGImageRelease(cgImage);
}

- (void)pausePublish
{
    if (currentState!=SCV_STARTED) {
        return;
    }
    
    if (self.slkStreamer!=nil) {
        [self.slkStreamer pause];
    }
    
    currentState = SCV_PAUSED;
}

- (void)resumePublish
{
    if (currentState!=SCV_PAUSED) {
        return;
    }
    
    if (self.slkStreamer!=nil) {
        [self.slkStreamer resume];
    }
    
    currentState = SCV_STARTED;
}

- (void)stopPublish
{
    if (currentState==SCV_IDLE || currentState==SCV_INITIALIZED) {
        return;
    }
    
    if (self.slkStreamer!=nil) {
        [self.slkStreamer stop];
        [self.slkStreamer terminate];
    }
    
    [self.mirrorFilter removeTarget:self.rawDataOutput];
    [self.workFilter removeTarget:self.mirrorFilter];
    
    isLivePublish = NO;
    
    currentState = SCV_INITIALIZED;
}

- (BOOL)isPublishing
{
    BOOL ret = NO;
    
    if (currentState==SCV_IDLE || currentState==SCV_INITIALIZED) {
        return ret;
    }
    
    if (self.slkStreamer!=nil) {
        ret = [self.slkStreamer isStreaming];
    }
    
    return ret;
}

- (void)enableAudio:(BOOL)isEnable
{
    if (currentState==SCV_STARTED || currentState==SCV_PAUSED)
    {
        if (self.slkStreamer!=nil) {
            [self.slkStreamer enableAudio:isEnable];
        }
    }
}

- (void)mixBGM:(NSString*)bgmUrl Volume:(float)volume Loop:(int)numberOfLoops
{
    if (currentState==SCV_STARTED || currentState==SCV_PAUSED) {
        if (self.slkStreamer!=nil) {
            [self.slkStreamer mixBGM:bgmUrl Volume:volume Loop:numberOfLoops];
        }
    }
}

- (void)playBGM
{
    if (currentState==SCV_STARTED || currentState==SCV_PAUSED) {
        if (self.slkStreamer!=nil) {
            [self.slkStreamer playBGM];
        }
    }
}

- (void)pauseBGM
{
    if (currentState==SCV_STARTED || currentState==SCV_PAUSED) {
        if (self.slkStreamer!=nil) {
            [self.slkStreamer pauseBGM];
        }
    }
}

- (void)stopBGM
{
    if (currentState==SCV_STARTED || currentState==SCV_PAUSED) {
        if (self.slkStreamer!=nil) {
            [self.slkStreamer stopBGM];
        }
    }
}

- (void)setBGMVolume:(float)volume
{
    if (currentState==SCV_STARTED || currentState==SCV_PAUSED) {
        if (self.slkStreamer!=nil) {
            [self.slkStreamer setBGMVolume:volume];
        }
    }
}

- (void)mixSEM:(NSString*)semUrl Volume:(float)volume Loop:(int)numberOfLoops
{
    if (currentState==SCV_STARTED || currentState==SCV_PAUSED) {
        if (self.slkStreamer!=nil) {
            [self.slkStreamer mixSEM:semUrl Volume:volume Loop:numberOfLoops];
        }
    }
}

- (void)pushText:(NSString*)text
{
    if (currentState==SCV_STARTED || currentState==SCV_PAUSED) {
        if (self.slkStreamer!=nil) {
            [self.slkStreamer pushText:text];
        }
    }
}

- (void)terminate
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
        
    if (currentState==SCV_IDLE) {
        return;
    }
    
    [self.videoCamera stopCameraCapture];
    [self.videoCamera removeAllTargets];
    [self.videoCamera removeInputsAndOutputs];
    
    [self.mirrorFilter removeAllTargets];
    [self.workFilter removeAllTargets];
    
#ifdef ENABLE_PRIVATE_FACE_DETECTION
    if (self.privateFaceDetection) {
        [self.privateFaceDetection terminate];
        self.privateFaceDetection = nil;
    }
#endif
    
    if (self.slkStreamer!=nil) {
        [self.slkStreamer stop];
        [self.slkStreamer terminate];
        self.slkStreamer = nil;
    }
    
    self.mSLKStreamerOptions = nil;
        
    currentState=SCV_IDLE;
}

- (void)gotConnecting
{
    if (self.delegate!=nil) {
        if (([self.delegate respondsToSelector:@selector(onConnecting)])) {
            [self.delegate onConnecting];
        }
    }
}

- (void)didConnected
{
    if (self.delegate!=nil) {
        if (([self.delegate respondsToSelector:@selector(onConnected)])) {
            [self.delegate onConnected];
        }
    }
}

- (void)gotStreaming
{
    if (self.delegate!=nil) {
        if (([self.delegate respondsToSelector:@selector(onStreaming)])) {
            [self.delegate onStreaming];
        }
    }
}

- (void)didPaused
{
    if (self.delegate!=nil) {
        if (([self.delegate respondsToSelector:@selector(onPaused)])) {
            [self.delegate onPaused];
        }
    }
}

- (void)gotStreamerErrorWithErrorType:(int)errorType
{
    if (self.delegate!=nil) {
        if (([self.delegate respondsToSelector:@selector(onError:)])) {
            [self.delegate onError:errorType];
        }
    }
}

- (void)gotStreamerWarnWithWarnType:(int)warnType WarnValue:(int)warnValue
{
    if (self.delegate!=nil) {
        if (([self.delegate respondsToSelector:@selector(onWarn:WarnValue:)])) {
            [self.delegate onWarn:warnType WarnValue:warnValue];
        }
    }
}

- (void)gotStreamerInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    if (self.delegate!=nil) {
        if (([self.delegate respondsToSelector:@selector(onInfo:InfoValue:)])) {
            [self.delegate onInfo:infoType InfoValue:infoValue];
        }
    }
}

- (void)didEndStreaming
{
    if (self.delegate!=nil) {
        if (([self.delegate respondsToSelector:@selector(onEnd)])) {
            [self.delegate onEnd];
        }
    }
}

- (void)willOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer FaceRects:(NSArray<NSValue *> *)rects
{
#ifdef ENABLE_PRIVATE_FACE_DETECTION
    if (self.privateFaceDetection) {
        [self.privateFaceDetection processSampleBuffer:sampleBuffer FaceRects:rects];
    }
#endif
}

- (void)willOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
{
    //todo
}

- (CVPixelBufferRef)filterCameraPixelBuffer:(CVPixelBufferRef)pixelBuffer
{
    if (self.delegate!=nil) {
        if (([self.delegate respondsToSelector:@selector(onProcessPixelBuffer:)])) {
            return [self.delegate onProcessPixelBuffer:pixelBuffer];
        }
    }
    
    return pixelBuffer;
}

+ (void)setScreenOn:(BOOL)on
{
    [UIApplication sharedApplication].idleTimerDisabled = on;
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"SLKCameraView dealloc");
}

@end
