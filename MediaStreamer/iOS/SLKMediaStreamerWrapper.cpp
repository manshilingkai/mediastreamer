//
//  SLKMediaStreamerWrapper.cpp
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "SLKMediaStreamerWrapper.h"
#include "SLKMediaStreamer.h"
#include "WAVFile.h"

#ifdef __cplusplus
extern "C" {
#endif

struct SLKMediaStreamerWrapper{
    SLKMediaStreamer* mediaStreamer;
    
    SLKMediaStreamerWrapper()
    {
        mediaStreamer = NULL;
    }
};

struct SLKMediaStreamerWrapper *GetInstance(const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions, char* backgroundPngCover, int reConnectTimes)
{
    SLKMediaStreamerWrapper* pInstance = new SLKMediaStreamerWrapper;
    
    pInstance->mediaStreamer = new SLKMediaStreamer(publishUrl,backupDir,videoOptions,audioOptions, backgroundPngCover, reConnectTimes);
    
    return pInstance;
}

void ReleaseInstance(struct SLKMediaStreamerWrapper **ppInstance)
{
    SLKMediaStreamerWrapper* pInstance  = *ppInstance;
    if (pInstance!=NULL) {
        if (pInstance->mediaStreamer!=NULL) {
            delete pInstance->mediaStreamer;
            pInstance->mediaStreamer = NULL;
        }
        
        delete pInstance;
        pInstance = NULL;
    }
}

void SLKMediaStreamerWrapper_setListener(struct SLKMediaStreamerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->setListener(listener,owner);
    }
}

void SLKMediaStreamerWrapper_inputVideoFrame(struct SLKMediaStreamerWrapper *pInstance, uint8_t* data, int frameSize, int width, int height, int videoRawType, int rotation)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        VideoFrame videoFrame;
        videoFrame.data = data;
        videoFrame.frameSize = frameSize;
        videoFrame.width = width;
        videoFrame.height = height;
        videoFrame.videoRawType = videoRawType;
        videoFrame.rotation = rotation;
        
        pInstance->mediaStreamer->inputVideoFrame(&videoFrame);
    }
}

bool SLKMediaStreamerWrapper_inputCVPixelBufferRef(struct SLKMediaStreamerWrapper *pInstance, void* cvPixelBufferBuf, int videoRawType)
{
    bool ret = false;
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        VideoFrame videoFrame;
        videoFrame.videoRawType = videoRawType;
        videoFrame.opaque = cvPixelBufferBuf;
        
        ret = pInstance->mediaStreamer->inputVideoFrame(&videoFrame);
    }
    
    return ret;
}

void SLKMediaStreamerWrapper_inputAudioFrame(struct SLKMediaStreamerWrapper *pInstance, uint8_t *data, int size, uint64_t pts, int sampleRate, int channels, int bitsPerChannel, bool isBigEndian)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        AudioFrame audioFrame;
        audioFrame.data = data;
        audioFrame.frameSize = size;
        audioFrame.pts = pts;
        audioFrame.sampleRate = sampleRate;
        audioFrame.channels = channels;
        audioFrame.bitsPerChannel = bitsPerChannel;
        audioFrame.isBigEndian = isBigEndian;
        
        pInstance->mediaStreamer->inputAudioFrame(&audioFrame);
    }
}

void SLKMediaStreamerWrapper_inputAudioFrameWithSourceType(struct SLKMediaStreamerWrapper *pInstance, int audioSourceType, uint8_t *data, int size, uint64_t pts, int sampleRate, int channels, int bitsPerChannel, bool isBigEndian)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        AudioFrame audioFrame;
        audioFrame.data = data;
        audioFrame.frameSize = size;
        audioFrame.pts = pts;
        audioFrame.sampleRate = sampleRate;
        audioFrame.channels = channels;
        audioFrame.bitsPerChannel = bitsPerChannel;
        audioFrame.isBigEndian = isBigEndian;
        
        pInstance->mediaStreamer->inputAudioFrame(&audioFrame, audioSourceType);
    }
}

void SLKMediaStreamerWrapper_inputText(struct SLKMediaStreamerWrapper *pInstance, char* text, int size)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->inputText(text, size);
    }
}

    
void SLKMediaStreamerWrapper_start(struct SLKMediaStreamerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->start();
    }
}

void SLKMediaStreamerWrapper_resume(struct SLKMediaStreamerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->resume();
    }
}

void SLKMediaStreamerWrapper_pause(struct SLKMediaStreamerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->pause();
    }
}

void SLKMediaStreamerWrapper_stop(struct SLKMediaStreamerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->stop();
    }
}
    
void SLKMediaStreamerWrapper_enableAudio(struct SLKMediaStreamerWrapper *pInstance, bool isEnable)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->enableAudio(isEnable);
    }
}

void SLKMediaStreamerWrapper_mixBGM(struct SLKMediaStreamerWrapper *pInstance, int bgmId, char* bgmUrl, float volume, int numberOfLoops)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->mixBGM(bgmId, bgmUrl, volume, numberOfLoops);
    }
}

void SLKMediaStreamerWrapper_playBGM(struct SLKMediaStreamerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->playBGM();
    }
}

void SLKMediaStreamerWrapper_pauseBGM(struct SLKMediaStreamerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->pauseBGM();
    }
}

void SLKMediaStreamerWrapper_stopBGM(struct SLKMediaStreamerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->stopBGM();
    }
}

void SLKMediaStreamerWrapper_setBGMVolume(struct SLKMediaStreamerWrapper *pInstance, float volume)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->setBGMVolume(volume);
    }
}

void SLKMediaStreamerWrapper_mixSEM(struct SLKMediaStreamerWrapper *pInstance, int semId, char* semUrl, float volume, int numberOfLoops)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->mixSEM(semId, semUrl, volume, numberOfLoops);
    }
}

void SLKMediaStreamerWrapper_iOSAVAudioSessionInterruption(struct SLKMediaStreamerWrapper *pInstance, bool isInterrupting)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->iOSAVAudioSessionInterruption(isInterrupting);
    }
}

void SLKMediaStreamerWrapper_iOSDidEnterBackground(struct SLKMediaStreamerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->iOSDidEnterBackground();
    }
}

void SLKMediaStreamerWrapper_iOSDidBecomeActive(struct SLKMediaStreamerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->iOSDidBecomeActive();
    }
}

void SLKMediaStreamerWrapper_netWorkReachabilityNotify(struct SLKMediaStreamerWrapper *pInstance, int networkStatus)
{
    if (pInstance!=NULL && pInstance->mediaStreamer!=NULL) {
        pInstance->mediaStreamer->netWorkReachabilityNotify(networkStatus);
    }
}

int SLKMediaStreamerWrapper_CreateWavData(char** wavData, int len, int channels, int sampleRate)
{
    return WAVFile::CreateWavData(wavData, len, channels, sampleRate);
}

#ifdef __cplusplus
};
#endif
