//
//  SLKMediaStreamerWrapper.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef SLKMediaStreamerWrapper_h
#define SLKMediaStreamerWrapper_h

#include <stdio.h>
#include <inttypes.h>

#include "MediaDataType.h"

struct SLKMediaStreamerWrapper;

enum slk_media_streamer_event_type {
    SLK_MEDIA_STREAMER_CONNECTING = 0,
    SLK_MEDIA_STREAMER_CONNECTED = 1,
    SLK_MEDIA_STREAMER_STREAMING = 2,
    SLK_MEDIA_STREAMER_ERROR = 3,
    SLK_MEDIA_STREAMER_INFO = 4,
    SLK_MEDIA_STREAMER_END = 5,
    SLK_MEDIA_STREAMER_PAUSED = 6,
    SLK_MEDIA_STREAMER_WARN = 7,
};

#ifdef __cplusplus
extern "C" {
#endif
    struct SLKMediaStreamerWrapper *GetInstance(const char* publishUrl, const char *backupDir, VideoOptions videoOptions, AudioOptions audioOptions, char* backgroundPngCover, int reConnectTimes);
    void ReleaseInstance(struct SLKMediaStreamerWrapper **ppInstance);
    
    void SLKMediaStreamerWrapper_setListener(struct SLKMediaStreamerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner);
    
    void SLKMediaStreamerWrapper_inputVideoFrame(struct SLKMediaStreamerWrapper *pInstance, uint8_t* data, int frameSize, int width, int height, int videoRawType, int rotation);
    bool SLKMediaStreamerWrapper_inputCVPixelBufferRef(struct SLKMediaStreamerWrapper *pInstance, void* cvPixelBufferBuf, int videoRawType);

    void SLKMediaStreamerWrapper_inputAudioFrame(struct SLKMediaStreamerWrapper *pInstance, uint8_t *data, int size, uint64_t pts, int sampleRate, int channels, int bitsPerChannel, bool isBigEndian);
    void SLKMediaStreamerWrapper_inputAudioFrameWithSourceType(struct SLKMediaStreamerWrapper *pInstance, int audioSourceType, uint8_t *data, int size, uint64_t pts, int sampleRate, int channels, int bitsPerChannel, bool isBigEndian);

    void SLKMediaStreamerWrapper_inputText(struct SLKMediaStreamerWrapper *pInstance, char* text, int size);
    
    void SLKMediaStreamerWrapper_start(struct SLKMediaStreamerWrapper *pInstance);
    void SLKMediaStreamerWrapper_resume(struct SLKMediaStreamerWrapper *pInstance);
    void SLKMediaStreamerWrapper_pause(struct SLKMediaStreamerWrapper *pInstance);
    void SLKMediaStreamerWrapper_stop(struct SLKMediaStreamerWrapper *pInstance);
    
    void SLKMediaStreamerWrapper_enableAudio(struct SLKMediaStreamerWrapper *pInstance, bool isEnable);
    void SLKMediaStreamerWrapper_mixBGM(struct SLKMediaStreamerWrapper *pInstance, int bgmId, char* bgmUrl, float volume, int numberOfLoops);
    void SLKMediaStreamerWrapper_playBGM(struct SLKMediaStreamerWrapper *pInstance);
    void SLKMediaStreamerWrapper_pauseBGM(struct SLKMediaStreamerWrapper *pInstance);
    void SLKMediaStreamerWrapper_stopBGM(struct SLKMediaStreamerWrapper *pInstance);
    void SLKMediaStreamerWrapper_setBGMVolume(struct SLKMediaStreamerWrapper *pInstance, float volume);
    void SLKMediaStreamerWrapper_mixSEM(struct SLKMediaStreamerWrapper *pInstance, int semId, char* semUrl, float volume, int numberOfLoops);
    
    void SLKMediaStreamerWrapper_iOSAVAudioSessionInterruption(struct SLKMediaStreamerWrapper *pInstance, bool isInterrupting);
    void SLKMediaStreamerWrapper_iOSDidEnterBackground(struct SLKMediaStreamerWrapper *pInstance);
    void SLKMediaStreamerWrapper_iOSDidBecomeActive(struct SLKMediaStreamerWrapper *pInstance);

    void SLKMediaStreamerWrapper_netWorkReachabilityNotify(struct SLKMediaStreamerWrapper *pInstance, int networkStatus);

    int SLKMediaStreamerWrapper_CreateWavData(char** wavData, int len, int channels, int sampleRate);
#ifdef __cplusplus
};
#endif

#endif /* SLKMediaStreamerWrapper_h */
