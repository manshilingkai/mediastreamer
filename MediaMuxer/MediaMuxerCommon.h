//
//  MediaMuxerCommon.h
//  MediaStreamer
//
//  Created by Think on 2019/7/1.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef MediaMuxerCommon_h
#define MediaMuxerCommon_h

#include <stdio.h>

enum MUXER_TYPE
{
    FFMPEG_STREAMER = 0,
    LIBRTMP = 1,
    FFMPEG_PROCESSER = 2
};

enum MUXER_FORMAT
{
    MP4 = 0,
    RTMP = 1,
    RTSP = 2,
    
    GIF = 10,
};

enum MUXER_DATA
{
    NONE = 0,
    ONLY_AUDIO = 1,
    ONLY_VIDEO = 2,
    AUDIO_VIDEO = 3
};

#endif /* MediaMuxerCommon_h */
