//
//  MediaMuxer.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaStreamer__MediaMuxer__
#define __MediaStreamer__MediaMuxer__

#include "MediaDataType.h"
#include "IMediaListener.h"

#include "MediaLog.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include "MediaMuxerCommon.h"

class MediaMuxer
{
public:
    virtual ~MediaMuxer() {}
    
    static MediaMuxer* CreateMediaMuxer(MUXER_TYPE muxerType, MUXER_FORMAT muxerFormat, char *publishUrl, MediaLog* mediaLog, VideoOptions *videoOptions, AudioOptions *audioOptions);
    static void DeleteMediaMuxer(MediaMuxer* mediaMuxer, MUXER_TYPE muxerType);
    
#ifdef ANDROID
    virtual void registerJavaVMEnv(JavaVM *jvm) = 0;
#endif
    
    virtual void setListener(IMediaListener *mediaListener) = 0;
    
    // 0 : Success
    // -1 : Fail
    // -2 : Exit
    virtual int prepare() = 0;
    virtual void start() = 0;
    virtual void stop() = 0;
    
    virtual void pushH264Header(VideoPacket *h264Header) = 0;
    virtual void pushH264Body(VideoPacket *h264Body) = 0;
    
    virtual void pushVP8Header(VideoPacket *vp8Header) = 0;
    virtual void pushVP8Body(VideoPacket *vp8Body) = 0;
    
    virtual void pushAACHeader(AudioPacket *aacHeader) = 0;
    virtual void pushAACBody(AudioPacket *aacBody) = 0;
    
    virtual void pushAMRHeader(AudioPacket *amrHeader) = 0;
    virtual void pushAMRBody(AudioPacket *amrBody) = 0;
    
    virtual void pushTextData(TextPacket* textPacket) = 0;
    
    virtual void interrupt() = 0;
    
    virtual int64_t getPublishDelayTimeMs() = 0;
};

#endif /* defined(__MediaStreamer__MediaMuxer__) */
