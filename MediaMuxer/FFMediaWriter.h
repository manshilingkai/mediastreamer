//
//  FFMediaWriter.h
//  MediaStreamer
//
//  Created by Think on 2019/7/2.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef FFMediaWriter_h
#define FFMediaWriter_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

extern "C" {
#include "libavutil/error.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
}

#ifdef ANDROID
#include "jni.h"
#endif

#include "MediaMuxerCommon.h"
#include "MediaDataType.h"

class FFMediaWriter {
public:
#ifdef ANDROID
    FFMediaWriter(JavaVM *jvm, char *publishUrl, int format, bool hasVideo, int width, int height, int fps, int videoRotation, uint8_t* sps, int sps_len, uint8_t *pps, int pps_len, bool hasAudio, int samplerate, int channels, int audioSampleFormat, uint8_t* asc, int asc_len);
#else
    FFMediaWriter(char *publishUrl, int format, bool hasVideo, int width, int height, int fps, int videoRotation, uint8_t* sps, int sps_len, uint8_t *pps, int pps_len, bool hasAudio, int samplerate, int channels, int audioSampleFormat, uint8_t* asc, int asc_len);
#endif
    ~FFMediaWriter();
    
    bool open();
    void close();
    
    bool writeAVPacket(uint8_t *data, int size, int64_t pts, int64_t dts, int packet_type);
    
    void interrupt();
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    uint8_t* mSps;
    int mSpsLen;
    uint8_t* mPps;
    int mPpsLen;
    uint8_t* mAsc;
    int mAscLen;
    
    bool isLocalFile;
    
    // context -- must be memory-managed
    char *output_fmt_name;
    char *output_url;
    AVFormatContext *output_fmt_ctx;
    AVRational *device_time_base;
    
    // for convenience
    int video_stream_index;
    AVStream *video_stream;
    AVCodec *video_codec;
    
    int audio_stream_index;
    AVStream *audio_stream;
    AVCodec *audio_codec;
    
    int data_stream_index;
    AVStream *data_stream;
    AVCodec *data_codec;
    
    // video config
    enum AVCodecID video_codec_id;
    enum AVPixelFormat video_pix_fmt;
    int video_width;
    int video_height;
    int video_fps;
    int video_bit_rate;
    int video_rotation;
    
    // audio config
    enum AVCodecID audio_codec_id;
    enum AVSampleFormat audio_sample_fmt;
    int audio_sample_rate;
    int audio_num_channels;
    int audio_bit_rate;
    
    //data config
    enum AVCodecID data_codec_id;
    
    MUXER_DATA muxerData;
    
    bool GotAudioHeader;
    bool GotVideoHeader;
    bool HasWriteHeader;
    bool GotFirstKeyFrame;
    int64_t FirstKeyFramePts;
    int64_t FirstKeyFrameDts;
    
    bool HasWritenFirstKeyFrame;
    
    bool GotFirstAudioPacket;
    int64_t FirstAudioPacketPts;
    int64_t FirstAudioPacketDts;
    
    //method for ffmpeg
    void init_ffmpeg();
    void init_device_time_base();
    void init_output_fmt_context();
    
    AVStream* add_stream(enum AVCodecID codec_id);
    void add_video_stream();
    void add_audio_stream();
    void add_data_stream();
    
    int open_output_url();
    
    void set_audio_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size);
    void set_video_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size);
    void log_codec_attributes(AVCodecContext *codec);
    
    int write_header();
    
    void rescale_packet(AVStream *st, AVPacket *packet);
    
    int write_packet(AVPacket *packet);
    int write_trailer();
    
    // return negative : write fail
    // return 0 : no write
    // return 1 : write media body success
    int write_media_packet(uint8_t *data, int data_size, uint64_t pts, uint64_t dts, int is_video, int is_video_keyframe);
    
    int write_data_packet(uint8_t *data, int data_size, uint64_t pts, uint64_t dts);
    
    // return negative : write fail
    // return 0 : no write or write headers success
    // return 1 : write media body success
    int mux_media_packet(MediaPacket *mediaPacket);
    
    char av_err_str[AV_ERROR_MAX_STRING_SIZE];
    char *av_err2string(int errnum);
    
    static int interruptCallback(void* opaque);
    int interruptCallbackMain();
    int isInterrupt; // critical value
    
    pthread_mutex_t mLock;
    
private:
    uint64_t current_video_pts;
    uint64_t current_video_dts;
    uint64_t last_video_dts;
    
    uint64_t current_audio_pts;
    uint64_t current_audio_dts;
    uint64_t last_audio_dts;
    
private:
    bool isOpened;
};

#endif /* FFMediaWriter_h */
