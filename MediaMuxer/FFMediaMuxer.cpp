//
//  FFMediaMuxer.cpp
//  MediaStreamer
//
//  Created by Think on 2019/6/28.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include <string.h>

#include "FFMediaMuxer.h"
#include "MediaLog.h"
#include "AVCUtils.h"
#include "FFLog.h"
#include "MediaTime.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#include "AndroidUtils.h"
#include "JNIHelp.h"
#endif

#ifdef IOS
#include "iOSMediaListener.h"
#endif

#include "NormalMediaListener.h"

#ifdef ANDROID
FFMediaMuxer::FFMediaMuxer(JavaVM *jvm, char *publishUrl, int format, bool hasVideo, int width, int height, int fps, int videoRotation, int videoBitrate, uint8_t* sps, int sps_len, uint8_t *pps, int pps_len, bool hasAudio, int samplerate, int channels, int audioSampleFormat, int audioBitrate, uint8_t* asc, int asc_len)
{
    mJvm = jvm;
    
    output_fmt_name = NULL;
    output_url = NULL;
    output_fmt_ctx = NULL;
    device_time_base = NULL;
    
    video_stream = NULL;
    video_codec = NULL;
    audio_stream = NULL;
    audio_codec = NULL;
    
    GotAudioHeader = false;
    GotVideoHeader = false;
    HasWriteHeader = false;
    GotFirstKeyFrame = false;
    FirstKeyFramePts = 0;
    FirstKeyFrameDts = 0;
    GotFirstAudioPacket = false;
    FirstAudioPacketPts = 0;
    FirstAudioPacketDts = 0;
    
    HasWritenFirstKeyFrame = false;
    
    mMediaListener = NULL;
    
    // defaults -- likely not overridden
    video_codec_id = AV_CODEC_ID_H264;
    video_pix_fmt = AV_PIX_FMT_YUV420P;
    audio_codec_id = AV_CODEC_ID_AAC;
    audio_sample_fmt = AV_SAMPLE_FMT_S16;
    
    data_codec_id = AV_CODEC_ID_TEXT;
    
    // propagate the configuration
    if (format==MP4) {
        isLocalFile = true;
        output_fmt_name = av_strdup("mp4");
    }else if(format==RTMP) {
        isLocalFile = false;
        output_fmt_name = av_strdup("flv");
    } else {
        isLocalFile = true;
        output_fmt_name = av_strdup("mp4");
    }
    output_url = av_strdup(publishUrl);
    
    video_width = width;
    video_height = height;
    video_fps = fps;
    video_bit_rate = videoBitrate*1024;
    video_rotation = videoRotation;
    
    audio_sample_rate = samplerate;
    audio_num_channels = channels;
    audio_bit_rate = audioBitrate*1024;
    audio_sample_fmt = (AVSampleFormat)audioSampleFormat;
    
    if (hasVideo && hasAudio) {
        muxerData = AUDIO_VIDEO;
    }else if(hasVideo && !hasAudio) {
        muxerData = ONLY_VIDEO;
    }else if(!hasVideo && hasAudio) {
        muxerData = ONLY_AUDIO;
    }else {
        muxerData = NONE;
    }
    
    isThreadLive = false;
    
    isInterrupt = 0;
    pthread_mutex_init(&mInterruptLock, NULL);
    
    // init param
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    mSpsLen = sps_len;
    mSps = (uint8_t*)malloc(mSpsLen);
    memcpy(mSps, sps, mSpsLen);
    mPpsLen = pps_len;
    mPps = (uint8_t*)malloc(mPpsLen);
    memcpy(mPps, pps, mPpsLen);
    mAscLen = asc_len;
    mAsc = (uint8_t*)malloc(mAscLen);
    memcpy(mAsc, asc, mAscLen);
    
    current_video_pts = 0;
    current_video_dts = 0;
    last_video_dts = 0;
    
    current_audio_pts = 0;
    current_audio_dts = 0;
    last_audio_dts = 0;
}
#else
FFMediaMuxer::FFMediaMuxer(char *publishUrl, int format, bool hasVideo, int width, int height, int fps, int videoRotation, int videoBitrate, uint8_t* sps, int sps_len, uint8_t *pps, int pps_len, bool hasAudio, int samplerate, int channels, int audioSampleFormat, int audioBitrate, uint8_t* asc, int asc_len)
{
    output_fmt_name = NULL;
    output_url = NULL;
    output_fmt_ctx = NULL;
    device_time_base = NULL;
    
    video_stream = NULL;
    video_codec = NULL;
    audio_stream = NULL;
    audio_codec = NULL;
    
    GotAudioHeader = false;
    GotVideoHeader = false;
    HasWriteHeader = false;
    GotFirstKeyFrame = false;
    FirstKeyFramePts = 0;
    FirstKeyFrameDts = 0;
    
    GotFirstAudioPacket = false;
    FirstAudioPacketPts = 0;
    FirstAudioPacketDts = 0;
    
    HasWritenFirstKeyFrame = false;
    
    mMediaListener = NULL;
    
    // defaults -- likely not overridden
    video_codec_id = AV_CODEC_ID_H264;
    video_pix_fmt = AV_PIX_FMT_YUV420P;
    audio_codec_id = AV_CODEC_ID_AAC;
    audio_sample_fmt = AV_SAMPLE_FMT_S16;
    
    data_codec_id = AV_CODEC_ID_TEXT;
    
    // propagate the configuration
    if (format==MP4) {
        isLocalFile = true;
        output_fmt_name = av_strdup("mp4");
    }else if(format==RTMP) {
        isLocalFile = false;
        output_fmt_name = av_strdup("flv");
    } else {
        isLocalFile = true;
        output_fmt_name = av_strdup("mp4");
    }
    output_url = av_strdup(publishUrl);
    
    video_width = width;
    video_height = height;
    video_fps = fps;
    video_bit_rate = videoBitrate*1024;
    video_rotation = videoRotation;
    
    audio_sample_rate = samplerate;
    audio_num_channels = channels;
    audio_bit_rate = audioBitrate*1024;
    audio_sample_fmt = (AVSampleFormat)audioSampleFormat;
    
    if (hasVideo && hasAudio) {
        muxerData = AUDIO_VIDEO;
    }else if(hasVideo && !hasAudio) {
        muxerData = ONLY_VIDEO;
    }else if(!hasVideo && hasAudio) {
        muxerData = ONLY_AUDIO;
    }else {
        muxerData = NONE;
    }
    
    isThreadLive = false;
    
    isInterrupt = 0;
    pthread_mutex_init(&mInterruptLock, NULL);

    // init param
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    mSpsLen = sps_len;
    mSps = (uint8_t*)malloc(mSpsLen);
    memcpy(mSps, sps, mSpsLen);
    mPpsLen = pps_len;
    mPps = (uint8_t*)malloc(mPpsLen);
    memcpy(mPps, pps, mPpsLen);
    mAscLen = asc_len;
    mAsc = (uint8_t*)malloc(mAscLen);
    memcpy(mAsc, asc, mAscLen);
    
    current_video_pts = 0;
    current_video_dts = 0;
    last_video_dts = 0;
    
    current_audio_pts = 0;
    current_audio_dts = 0;
    last_audio_dts = 0;
}
#endif

FFMediaMuxer::~FFMediaMuxer()
{
    stop();
    
#ifdef ANDROID
    JniMediaListener *jniListener = (JniMediaListener*)mMediaListener;
    if (jniListener!=NULL) {
        delete jniListener;
        jniListener = NULL;
    }
#else
#ifdef IOS
    iOSMediaListener *iOSListener = (iOSMediaListener*)mMediaListener;
    if (iOSListener!=NULL) {
        delete iOSListener;
        iOSListener = NULL;
    }
#else
    NormalMediaListener* normalMediaListener = (NormalMediaListener*)mMediaListener;
    if (normalMediaListener!=NULL) {
        delete normalMediaListener;
        normalMediaListener = NULL;
    }
#endif
#endif
    
    mMediaListener = NULL;
    
    if (mSps) {
        free(mSps);
        mSps = NULL;
    }
    
    if (mPps) {
        free(mPps);
        mPps = NULL;
    }
    
    if (mAsc) {
        free(mAsc);
        mAsc = NULL;
    }
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    pthread_mutex_destroy(&mInterruptLock);

    if (output_fmt_name) av_free(output_fmt_name);
    if (output_url) av_free(output_url);
    
    mMediaPacketQueue.flush();
}

#ifdef ANDROID
void FFMediaMuxer::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
}
#else
void FFMediaMuxer::setListener(void (*listener)(void*,int,int,int), void* arg)
{
#ifdef IOS
    mMediaListener = new iOSMediaListener(listener,arg);
#else
    mMediaListener = new NormalMediaListener(listener,arg);
#endif
}
#endif

void FFMediaMuxer::start()
{
    if (isThreadLive) return;
    
    isBreakThread = false;
    isMuxing = false;
    isWaittingMediaPacket = false;
    
    this->createMuxerThread();
    isThreadLive = true;
}

void FFMediaMuxer::stop()
{
    if (!isThreadLive) return;
    this->deleteMuxerThread();
    isThreadLive = false;
    
    LOGD("FFMediaMuxer stoped");
}

void FFMediaMuxer::createMuxerThread()
{
#ifndef WIN32
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&mThread, &attr, handleMuxerThread, this);
    pthread_attr_destroy(&attr);
#else
    pthread_create(&mThread, NULL, handleMuxerThread, this);
#endif
}

void* FFMediaMuxer::handleMuxerThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
    FFMediaMuxer* ffMediaMuxer = (FFMediaMuxer*)ptr;
    ffMediaMuxer->muxerThreadMain();
    
    return NULL;
}

void FFMediaMuxer::muxerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    // for bitrate statistics
    int64_t av_bitrate_begin_time = 0;
    int64_t av_bitrate_datasize = 0;
    int realtimeBitrateKbps = 0; // kbps
    
    bool gotError = false;
    
    // initialize FFmpeg
    init_ffmpeg();
    
    // initialize our device time_base
    init_device_time_base();
    
    // initialize our output format context
    init_output_fmt_context();
    
    // set up the streams
    if (muxerData==ONLY_VIDEO || muxerData==AUDIO_VIDEO) {
        add_video_stream();
    }
    
    if (muxerData==ONLY_AUDIO || muxerData==AUDIO_VIDEO) {
        add_audio_stream();
    }
    
    if (mMediaListener!=NULL) {
        mMediaListener->notify(MEDIA_MUXER_CONNECTING,0,0);
    }
    int rc = open_output_url();
    if (rc < 0){
        LOGE("ERROR: open_output_url error -- %s", av_err2string(rc));
        if (mMediaListener!=NULL) {
            mMediaListener->notify(MEDIA_MUXER_ERROR, MEDIA_MUXER_ERROR_CONNECT_FAIL, 0);
        }
        isMuxing = false;
        gotError = true;
    }else{
        //push video header and audio header into MediaPacketQueue
        MediaPacket *videoHeaderPacket = new MediaPacket;
        videoHeaderPacket->packetType = VIDEO_H264_SPS_PPS;
        videoHeaderPacket->size = mSpsLen + mPpsLen;
        videoHeaderPacket->data = (uint8_t*)malloc(videoHeaderPacket->size);
        memcpy(videoHeaderPacket->data, mSps, mSpsLen);
        memcpy(videoHeaderPacket->data+mSpsLen, mPps, mPpsLen);
        mMediaPacketQueue.push(videoHeaderPacket);
        
        MediaPacket *audioHeaderPacket = new MediaPacket;
        audioHeaderPacket->packetType = AUDIO_AAC_HEADER;
        audioHeaderPacket->size = mAscLen;
        audioHeaderPacket->data = (uint8_t*)malloc(audioHeaderPacket->size);
        memcpy(audioHeaderPacket->data, mAsc, mAscLen);
        mMediaPacketQueue.push(audioHeaderPacket);
        
        if (mMediaListener!=NULL) {
            mMediaListener->notify(MEDIA_MUXER_CONNECTED,0,0);
        }
        
        isMuxing = true;
    }
    
    // for local mp4 file record time statistics
    uint64_t lastSendRecordTimeMS = 0;
    uint64_t lastRecordTimeMS = 0;
    
    // loop
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            if (isLocalFile && !gotError) {
                while (true) {
                    MediaPacket *packet = mMediaPacketQueue.pop();
                    if (packet==NULL) break;
                    
                    int ret = mux_media_packet(packet);
                    
                    //Free MediaPacket
                    if (packet->data!=NULL) {
                        free(packet->data);
                        packet->data = NULL;
                    }
                    delete packet;
                    packet = NULL;
                    
                    if (ret<0) break;
                }
                
                write_trailer();
            }
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!isMuxing) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        MediaPacket *packet = mMediaPacketQueue.pop();
        if (packet!=NULL) {
            int ret = mux_media_packet(packet);
            if (ret<0) {
                //Free MediaPacket
                if (packet->data!=NULL) {
                    free(packet->data);
                    packet->data = NULL;
                }
                delete packet;
                packet = NULL;
                
                if (ret==AVERROR_EXIT) {
                    LOGW("Immediate exit was requested");
                }else{
                    LOGE("mux media packet fail");
                    if (mMediaListener!=NULL) {
                        mMediaListener->notify(MEDIA_MUXER_ERROR, MEDIA_MUXER_ERROR_MUX_FAIL, 0);
                    }
                }
                
                pthread_mutex_lock(&mLock);
                isMuxing = false;
                gotError = true;
                pthread_mutex_unlock(&mLock);
                continue;
            }
            
            if (ret==1) {
                if (isLocalFile) {
                    // for local mp4 file record time statistics
                    lastRecordTimeMS = packet->pts>lastRecordTimeMS?packet->pts:lastRecordTimeMS;
                    if ((lastRecordTimeMS-lastSendRecordTimeMS)>=1000) {
                        lastSendRecordTimeMS = lastRecordTimeMS;
                        if (mMediaListener!=NULL) {
                            mMediaListener->notify(MEDIA_MUXER_INFO, MEDIA_MUXER_INFO_PUBLISH_TIME, (int)(lastRecordTimeMS/100)); //100ms
                        }
                    }
                }else{
                    //for bitrate
                    if(packet->size>=0)
                    {
                        av_bitrate_datasize += packet->size;
                    }
                    if (av_bitrate_begin_time==0) {
                        av_bitrate_begin_time = GetNowMs();
                    }
                    int64_t av_bitrate_duration = GetNowMs()-av_bitrate_begin_time;
                    if (av_bitrate_duration>=1000) {
                        realtimeBitrateKbps = (int)(av_bitrate_datasize * 8 * 1000 / 1024 / av_bitrate_duration);
                        
                        av_bitrate_begin_time = 0;
                        av_bitrate_datasize = 0;
                        
                        if (mMediaListener!=NULL) {
                            mMediaListener->notify(MEDIA_MUXER_INFO, MEDIA_MUXER_INFO_PUBLISH_REAL_BITRATE, realtimeBitrateKbps);
                        }
                    }
                }
            }
            
            //Free MediaPacket
            if (packet->data!=NULL) {
                free(packet->data);
                packet->data = NULL;
            }
            delete packet;
            packet = NULL;
            
        }else {
            pthread_mutex_lock(&mLock);
            isWaittingMediaPacket = true;
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
        }
    }
    
    if (!HasWritenFirstKeyFrame) {
        //todo for local record file
        //delete local record file because the file has only header no body
    }
    
    mMediaPacketQueue.flush();
    
    // close the output file
    if (!(output_fmt_ctx->oformat->flags & AVFMT_NOFILE)) {
        avio_close(output_fmt_ctx->pb);
    }
    
    // clean up memory
    if (device_time_base) av_free(device_time_base);
    
    if (video_stream && video_stream->codec) {
        avcodec_close(video_stream->codec);
    }
    
    if (audio_stream && audio_stream->codec) {
        avcodec_close(audio_stream->codec);
    }
    
    if (output_fmt_ctx) avformat_free_context(output_fmt_ctx);
    
    if (mMediaListener!=NULL) {
        mMediaListener->notify(MEDIA_MUXER_END, 0, 0);
    }
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
    
}

void FFMediaMuxer::deleteMuxerThread()
{
    LOGD("FFMediaMuxer::deleteMuxerThread");
    
    this->interrupt();

    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
        
    pthread_join(mThread, NULL);
}

void FFMediaMuxer::writeAVPacket(uint8_t *data, int size, int64_t pts, int64_t dts, int packet_type)
{
    if (packet_type==VIDEO_H264_KEY_FRAME) {
        MediaPacket *videoKeyPacket = new MediaPacket;
        videoKeyPacket->packetType = VIDEO_H264_KEY_FRAME;
        videoKeyPacket->size = size;
        videoKeyPacket->data = (uint8_t*)malloc(videoKeyPacket->size);
        memcpy(videoKeyPacket->data, data, size);
        videoKeyPacket->pts = pts;
        videoKeyPacket->dts = dts;

        mMediaPacketQueue.push(videoKeyPacket);
    }else if(packet_type==VIDEO_H264_P_OR_B_FRAME) {
        if (AVCUtils::avc_keyframe(data, size)) {
            MediaPacket *videoKeyPacket = new MediaPacket;
            videoKeyPacket->packetType = VIDEO_H264_KEY_FRAME;
            videoKeyPacket->size = size;
            videoKeyPacket->data = (uint8_t*)malloc(videoKeyPacket->size);
            memcpy(videoKeyPacket->data, data, size);
            videoKeyPacket->pts = pts;
            videoKeyPacket->dts = dts;
            
            mMediaPacketQueue.push(videoKeyPacket);
        }else{
            MediaPacket *videoPacket = new MediaPacket;
            videoPacket->packetType = VIDEO_H264_P_OR_B_FRAME;
            videoPacket->size = size;
            videoPacket->data = (uint8_t*)malloc(videoPacket->size);
            memcpy(videoPacket->data, data, size);
            videoPacket->pts = pts;
            videoPacket->dts = dts;
            
            mMediaPacketQueue.push(videoPacket);
        }
    }else if(packet_type==AUDIO_AAC_BODY) {
        MediaPacket *audioPacket = new MediaPacket;
        audioPacket->packetType = AUDIO_AAC_BODY;
        audioPacket->size = size;
        audioPacket->data = (uint8_t*)malloc(audioPacket->size);
        memcpy(audioPacket->data, data, size);
        audioPacket->pts = pts;
        audioPacket->dts = dts;

        mMediaPacketQueue.push(audioPacket);
    }
    
    pthread_mutex_lock(&mLock);
    if(isWaittingMediaPacket)
    {
        isWaittingMediaPacket = false;
        pthread_cond_signal(&mCondition);
    }
    pthread_mutex_unlock(&mLock);
}

int64_t FFMediaMuxer::getPublishDelayTimeMs()
{
    return mMediaPacketQueue.duration();
}

// return negative : write fail
// return 0 : no write or write headers success
// return 1 : write media body success
int FFMediaMuxer::mux_media_packet(MediaPacket *mediaPacket)
{
    int ret = 0;
    switch (mediaPacket->packetType) {
        case VIDEO_H264_SPS_PPS:
            if(HasWriteHeader) return 0;
            if(muxerData==ONLY_AUDIO) return 0;
            set_video_codec_extradata(mediaPacket->data, mediaPacket->size);
            GotVideoHeader = true;
            if (muxerData==ONLY_VIDEO || GotAudioHeader) {
                ret = write_header();
                
                if (ret>=0) {
                    HasWriteHeader = true;
                }
                
                return ret;
            }
            return 0;
        case AUDIO_AAC_HEADER:
            if(HasWriteHeader) return 0;
            if(muxerData==ONLY_VIDEO) return 0;
            set_audio_codec_extradata(mediaPacket->data, mediaPacket->size);
            GotAudioHeader = true;
            if (muxerData==ONLY_AUDIO || GotVideoHeader) {
                ret = write_header();
                
                if (ret>=0) {
                    HasWriteHeader = true;
                }
                
                return ret;
            }
            return 0;
        case VIDEO_H264_KEY_FRAME:
            if(muxerData==ONLY_AUDIO) return 0;

            if (HasWriteHeader) {
                if (!GotFirstKeyFrame) {
                    GotFirstKeyFrame = true;
                    FirstKeyFramePts = mediaPacket->pts;
                    FirstKeyFrameDts = mediaPacket->dts;
                }
                
                current_video_pts = mediaPacket->pts-FirstKeyFramePts;
                current_video_dts = mediaPacket->dts-FirstKeyFrameDts;
                if (current_video_dts<=last_video_dts) {
                    current_video_dts = last_video_dts + 1000/video_fps;
                    last_video_dts = current_video_dts;
                }else{
                    last_video_dts = current_video_dts;
                }
                
                mediaPacket->pts = current_video_pts;
                mediaPacket->dts = current_video_dts;
                
                int ret = write_media_packet(mediaPacket->data, mediaPacket->size, mediaPacket->pts, mediaPacket->dts, true, true);
                
                if (!HasWritenFirstKeyFrame && ret>0) {
                    HasWritenFirstKeyFrame = true;
                    if (mMediaListener!=NULL) {
                        mMediaListener->notify(MEDIA_MUXER_STREAMING, 0, 0);
                    }
                }
                
                return ret;
            }
            return 0;
        case VIDEO_H264_P_OR_B_FRAME:
            if(muxerData==ONLY_AUDIO) return 0;

            if (HasWriteHeader && HasWritenFirstKeyFrame) {
                
                current_video_pts = mediaPacket->pts-FirstKeyFramePts;
                current_video_dts = mediaPacket->dts-FirstKeyFrameDts;
                if (current_video_dts<=last_video_dts) {
                    current_video_dts = last_video_dts + 1000/video_fps;
                    last_video_dts = current_video_dts;
                }else{
                    last_video_dts = current_video_dts;
                }
                
                mediaPacket->pts = current_video_pts;
                mediaPacket->dts = current_video_dts;
                
                int ret = write_media_packet(mediaPacket->data, mediaPacket->size, mediaPacket->pts, mediaPacket->dts, true, false);
                
                return ret;
            }
            return 0;
        case AUDIO_AAC_BODY:
            if(muxerData==ONLY_VIDEO) return 0;

            if (HasWriteHeader && (muxerData==ONLY_AUDIO || (muxerData==AUDIO_VIDEO && HasWritenFirstKeyFrame))) {
                if (!GotFirstAudioPacket) {
                    GotFirstAudioPacket = true;
                    FirstAudioPacketPts = mediaPacket->pts;
                    FirstAudioPacketDts = mediaPacket->dts;
                }
                
                current_audio_pts = mediaPacket->pts-FirstAudioPacketPts;
                current_audio_dts = current_audio_pts;
                if (current_audio_dts<=last_audio_dts) {
                    current_audio_dts = last_audio_dts + mediaPacket->size * 1000 / audio_sample_rate / audio_num_channels / av_get_bytes_per_sample(audio_sample_fmt);
                    last_audio_dts = current_audio_dts;
                }else{
                    last_audio_dts = current_audio_dts;
                }
                
                mediaPacket->pts = current_audio_dts;
                mediaPacket->dts = current_audio_dts;
                
                int ret = write_media_packet(mediaPacket->data, mediaPacket->size, mediaPacket->pts, mediaPacket->dts, false, false);
                
                return ret;
            }
            return 0;
        case TEXT:
            if (HasWriteHeader && HasWritenFirstKeyFrame) {
                int ret = write_data_packet(mediaPacket->data, mediaPacket->size, mediaPacket->pts, mediaPacket->dts);
                
                return ret;
                
            }
            return 0;
        default:
            return -1;
    }
}

void FFMediaMuxer::init_ffmpeg()
{
    // initialize FFmpeg
    av_register_all();
    avformat_network_init();
    avcodec_register_all();
    
    FFLog::setLogLevel(AV_LOG_WARNING);
}

void FFMediaMuxer::init_device_time_base()
{
    // timestamps from the device should be in microseconds
    device_time_base = (AVRational*)av_malloc(sizeof(AVRational));
    device_time_base->num = 1;
    device_time_base->den = 1000000;
}

void FFMediaMuxer::init_output_fmt_context()
{
    int rc;
    AVOutputFormat *fmt;
    
    LOGD("init_output_fmt_context format: %s path: %s", output_fmt_name, output_url);
    rc = avformat_alloc_output_context2(&output_fmt_ctx, NULL, output_fmt_name, output_url);
    if (rc < 0) {
        LOGE("Error getting format context for output path: %s", av_err2string(rc));
    }
    
    output_fmt_ctx->start_time_realtime = 0;
    
    fmt = output_fmt_ctx->oformat;
    fmt->video_codec = video_codec_id;
    fmt->audio_codec = audio_codec_id;
    
    LOGD("fmt->name: %s", fmt->name);
    LOGD("fmt->long_name: %s", fmt->long_name);
    LOGD("fmt->mime_type: %s", fmt->mime_type);
    LOGD("fmt->extensions: %s", fmt->extensions);
    LOGD("fmt->audio_codec: %d", fmt->audio_codec);
    LOGD("fmt->video_codec: %d", fmt->video_codec);
    LOGD("fmt->subtitle_codec: %d", fmt->subtitle_codec);
    LOGD("fmt->flags: %d", fmt->flags);
}

AVStream* FFMediaMuxer::add_stream(enum AVCodecID codec_id)
{
    AVStream *st;
    AVCodec *codec;
    AVCodecContext *c;
    
    codec = avcodec_find_decoder(codec_id);
    if (!codec) {
        LOGE("ERROR: add_stream -- codec %d not found", codec_id);
    }
    LOGD("codec->name: %s", codec->name);
    LOGD("codec->long_name: %s", codec->long_name);
    LOGD("codec->type: %d", codec->type);
    LOGD("codec->id: %d", codec->id);
    LOGD("codec->capabilities: %d", codec->capabilities);
    
    st = avformat_new_stream(output_fmt_ctx, codec);
    if (!st) {
        LOGE("ERROR: add_stream -- could not allocate new stream");
        return NULL;
    }
    // TODO: need avcodec_get_context_defaults3?
    //avcodec_get_context_defaults3(st->codec, codec);
    st->id = output_fmt_ctx->nb_streams-1;
    c = st->codec;
    LOGI("add_stream at index %d", st->index);
    
    // Some formats want stream headers to be separate.
    if (output_fmt_ctx->oformat->flags & AVFMT_GLOBALHEADER) {
        LOGD("add_stream: using separate headers");
        c->flags |= CODEC_FLAG_GLOBAL_HEADER;
    }
    
    LOGD("add_stream st: %p", st);
    return st;
}

void FFMediaMuxer::add_video_stream()
{
    AVCodecContext *c;
    
    video_stream = add_stream(video_codec_id);
    
    char rotationDegree[8];
    sprintf(rotationDegree, "%d", video_rotation);
    
    av_dict_set(&(video_stream->metadata), "rotate", rotationDegree, 0);
    
    video_stream_index = video_stream->index;
    c = video_stream->codec;
    
    // video parameters
    c->codec_id = video_codec_id;
    c->pix_fmt = video_pix_fmt;
    c->width = video_width;
    c->height = video_height;
    c->bit_rate = video_bit_rate;
    
    // timebase: This is the fundamental unit of time (in seconds) in terms
    // of which frame timestamps are represented. For fixed-fps content,
    // timebase should be 1/framerate and timestamp increments should be
    // identical to 1.
    c->time_base.den = video_fps;
    c->time_base.num = 1;
}

void FFMediaMuxer::add_audio_stream()
{
    AVCodecContext *c;
    
    audio_stream = add_stream(audio_codec_id);
    audio_stream_index = audio_stream->index;
    c = audio_stream->codec;
    
    // audio parameters
    c->strict_std_compliance = FF_COMPLIANCE_UNOFFICIAL; // for native aac support
    c->sample_fmt  = audio_sample_fmt;
    c->sample_rate = audio_sample_rate;
    c->channels    = audio_num_channels;
    c->bit_rate    = audio_bit_rate;
    //c->time_base.num = 1;
    //c->time_base.den = c->sample_rate;
}

void FFMediaMuxer::add_data_stream()
{
    AVCodecContext *c;
    data_stream = add_stream(data_codec_id);
    data_stream_index = data_stream->index;
    c = data_stream->codec;
    c->codec_type = AVMEDIA_TYPE_SUBTITLE;
}


int FFMediaMuxer::open_output_url()
{
    output_fmt_ctx->interrupt_callback.callback = interruptCallback;
    output_fmt_ctx->interrupt_callback.opaque = this;
    
    if (!(output_fmt_ctx->oformat->flags & AVFMT_NOFILE)) {
        LOGD("Opening output file for writing at path %s", output_url);
        if(isLocalFile)
        {
            return avio_open(&output_fmt_ctx->pb, output_url, AVIO_FLAG_WRITE | AVIO_FLAG_NONBLOCK);
        }else{
            return avio_open2(&output_fmt_ctx->pb, output_url, AVIO_FLAG_WRITE, &output_fmt_ctx->interrupt_callback, NULL);
        }
    } else {
        LOGD("This format does not require a file.");
        return 0;
    }
}

void FFMediaMuxer::set_audio_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size)
{
    // this will automatically be freed by avformat_free_context()
    audio_stream->codec->extradata = (uint8_t *)av_malloc(codec_extradata_size);
    audio_stream->codec->extradata_size = codec_extradata_size;
    memcpy(audio_stream->codec->extradata, codec_extradata, codec_extradata_size);
    
    log_codec_attributes(audio_stream->codec);
}

void FFMediaMuxer::set_video_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size)
{
    // this will automatically be freed by avformat_free_context()
    video_stream->codec->extradata = (uint8_t *)av_malloc(codec_extradata_size);
    video_stream->codec->extradata_size = codec_extradata_size;
    memcpy(video_stream->codec->extradata, codec_extradata, codec_extradata_size);
    
    log_codec_attributes(video_stream->codec);
}

void FFMediaMuxer::log_codec_attributes(AVCodecContext *codec)
{
    int i;
    
    if (codec->codec_type==AVMEDIA_TYPE_AUDIO) {
        LOGD("audio_stream->codec->codec_type: %d", codec->codec_type);
        LOGD("audio_stream->codec->codec_id: %d", codec->codec_id);
        LOGD("audio_stream->codec->codec_tag: %d", codec->codec_tag);
        LOGD("audio_stream->codec->stream_codec_tag: %d", codec->stream_codec_tag);
        LOGD("audio_stream->codec->flags: %d", codec->flags);
        LOGD("audio_stream->codec->flags2: %d", codec->flags2);
        LOGD("audio_stream->codec->extradata_size: %d", codec->extradata_size);
        for (i=0; i<codec->extradata_size; ++i) {
            LOGD("audio_stream->codec->extradata byte: 0x%x", codec->extradata[i]);
        }
    }
    
    if (codec->codec_type==AVMEDIA_TYPE_VIDEO) {
        LOGD("video_stream->codec->codec_type: %d", codec->codec_type);
        LOGD("video_stream->codec->codec_id: %d", codec->codec_id);
        LOGD("video_stream->codec->codec_tag: %d", codec->codec_tag);
        LOGD("video_stream->codec->stream_codec_tag: %d", codec->stream_codec_tag);
        LOGD("video_stream->codec->flags: %d", codec->flags);
        LOGD("video_stream->codec->flags2: %d", codec->flags2);
        LOGD("video_stream->codec->extradata_size: %d", codec->extradata_size);
        for (i=0; i<codec->extradata_size; ++i) {
            LOGD("video_stream->codec->extradata byte: 0x%x", codec->extradata[i]);
        }
    }
}

int FFMediaMuxer::write_header()
{
    LOGD("Writing header ...");
    AVDictionary* options = NULL;
    if (isLocalFile) {
        av_dict_set_int(&options, "use_editlist", 0, 0);
    }
    int rc = avformat_write_header(output_fmt_ctx, &options);
    if (rc < 0) {
        LOGE("Error writing header: %s", av_err2string(rc));
    }
    return rc;
}

void FFMediaMuxer::rescale_packet(AVStream *st, AVPacket *packet)
{
    //    LOGD("time bases: stream=%d/%d, codec=%d/%d, device=%d/%d",
    //         st->time_base.num, st->time_base.den,
    //         st->codec->time_base.num, st->codec->time_base.den,
    //         (*device_time_base).num, (*device_time_base).den);
    
    packet->pts = av_rescale_q(packet->pts, *(device_time_base), st->time_base);
    packet->dts = av_rescale_q(packet->dts, *(device_time_base), st->time_base);
}

int FFMediaMuxer::write_packet(AVPacket *packet)
{
    //    LOGD("start writing frame to stream %d: (pts=%lld, size=%d)",packet->stream_index, packet->pts, packet->size);
    
    int rc = av_interleaved_write_frame(output_fmt_ctx, packet);
    if (rc < 0){
        LOGE("ERROR: write_packet stream (stream %d) -- %s",packet->stream_index, av_err2string(rc));
    }
    
    //    LOGD("end writing frame to stream %d: (pts=%lld, size=%d)",packet->stream_index, packet->pts, packet->size);
    
    return rc;
}

int FFMediaMuxer::write_trailer()
{
    LOGD("Writing trailer ...");
    int rc = av_write_trailer(output_fmt_ctx);
    if (rc < 0) {
        LOGE("Error writing trailer: %s", av_err2string(rc));
    }
    
    return rc;
}

// return negative : write fail
// return 0 : no write
// return 1 : write media body success
int FFMediaMuxer::write_media_packet(uint8_t *data, int data_size, uint64_t pts, uint64_t dts, int is_video, int is_video_keyframe)
{
    if (data==NULL || data_size==0) {
        return 0;
    }
    
    AVPacket *packet = (AVPacket *)av_malloc(sizeof(AVPacket));
    if (!packet) {
        LOGE("ERROR: write_media_packet couldn't allocate memory for the AVPacket");
        return -1;
    }
    
    av_init_packet(packet);
    
    if (is_video) {
        packet->stream_index = video_stream_index;
        if (is_video_keyframe) {
            packet->flags |= AV_PKT_FLAG_KEY;
        }
    } else {
        packet->stream_index = audio_stream_index;
    }
    packet->size = data_size;
    packet->pts = pts*1000;
    packet->dts = dts*1000;
    packet->data = data;
    
    AVStream *st = output_fmt_ctx->streams[packet->stream_index];
    
    /*
     // filter the packet (if necessary)
     int filterRet = filter_packet(st, packet);
     if (filterRet<0) {
     av_free(packet);
     return filterRet;
     }
     */
    
    // rescale the timing information for the packet
    rescale_packet(st, packet);
    
    if (packet->pts<0) {
        packet->pts = 0;
    }
    
    if (packet->dts<0) {
        packet->dts = 0;
    }
    
    // write the packet
    int ret = write_packet(packet);
    /*
     // clean up
     if(filterRet>0)
     {
     av_free_packet(packet);
     }
     */
    av_freep(&packet);
    
    if (ret>=0) {
        return 1;
    }
    
    return ret;
}

int FFMediaMuxer::write_data_packet(uint8_t *data, int data_size, uint64_t pts, uint64_t dts)
{
    AVPacket *packet = (AVPacket *)av_malloc(sizeof(AVPacket));
    if (!packet) {
        LOGE("ERROR: write_data_packet couldn't allocate memory for the AVPacket");
        return -1;
    }
    
    av_init_packet(packet);
    
    packet->stream_index = data_stream_index;
    packet->size = data_size;
    packet->pts = pts*1000;
    packet->dts = dts*1000;
    packet->data = data;
    
    AVStream *st = output_fmt_ctx->streams[packet->stream_index];
    rescale_packet(st, packet);
    int ret = write_packet(packet);
    av_freep(&packet);
    
    if (ret>=0) {
        return 1;
    }
    
    return ret;
}

char *FFMediaMuxer::av_err2string(int errnum)
{
    return av_make_error_string(av_err_str, AV_ERROR_MAX_STRING_SIZE, errnum);
}

int FFMediaMuxer::interruptCallback(void* opaque)
{
    FFMediaMuxer* thiz = (FFMediaMuxer* )opaque;
    
    return thiz->interruptCallbackMain();
}

int FFMediaMuxer::interruptCallbackMain()
{
    int ret = 0;
    
    pthread_mutex_lock(&mInterruptLock);
    ret = isInterrupt;
    pthread_mutex_unlock(&mInterruptLock);
    
    return ret;
}

void FFMediaMuxer::interrupt()
{
    pthread_mutex_lock(&mInterruptLock);
    isInterrupt = 1;
    pthread_mutex_unlock(&mInterruptLock);
}
