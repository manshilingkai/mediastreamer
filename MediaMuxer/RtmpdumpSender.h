//
//  RtmpdumpSender.h
//  MediaStreamer
//
//  Created by 施灵凯 on 15/3/4.
//  Copyright (c) 2015年 bolome. All rights reserved.
//

#ifndef __MediaStreamer__RtmpdumpSender__
#define __MediaStreamer__RtmpdumpSender__

#include "MediaMuxer.h"
#include "rtmp.h"
#include "MediaDataType.h"

#define MAX_RTMP_AAC_BODY_SIZE MAX_AAC_DATA_SIZE-7+2
#define MAX_RTMP_H264_BODY_SIZE MAX_H264_DATA_SIZE-4+9

class RtmpdumpSender : public MediaMuxer
{
public:
    RtmpdumpSender(char *rtmp_url);
    ~RtmpdumpSender();

    int mux_media_packet(MediaPacket *mediaPacket);

private:
    int send_h264_header(MediaPacket *h264Header);
    int send_h264_data(MediaPacket *h264Body);
    int send_aac_header(MediaPacket *aacHeader);
    int send_aac_data(MediaPacket *aacBody);
    
    RTMP *rtmp;
    RTMPPacket rtmpAACPacket;
    RTMPPacket rtmpH264Packet;
    
    char *rtmpAACBody;
    char *rtmpH264Body;
    
    int64_t startTimeStamp;
};


#endif /* defined(__MediaStreamer__RtmpdumpSender__) */
