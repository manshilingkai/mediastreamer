//
//  FFmpegMuxer.cpp
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "FFmpegMuxer.h"
#include "MediaLog.h"
#include "AVCUtils.h"
#include "MediaTimer.h"
#include "FFLog.h"

FFmpegMuxer::FFmpegMuxer(MUXER_FORMAT muxerFormat, char *publishUrl, MediaLog* mediaLog, VideoOptions *videoOptions, AudioOptions *audioOptions)
{
    mMediaLog = mediaLog;
    
    output_fmt_name = NULL;
    output_url = NULL;
    output_fmt_ctx = NULL;
    device_time_base = NULL;

    video_stream = NULL;
    video_codec = NULL;
    audio_stream = NULL;
    audio_codec = NULL;
    data_stream = NULL;
    data_codec = NULL;
    
    bsfc = NULL;
    
    GotAudioHeader = false;
    GotVideoHeader = false;
    HasWriteHeader = false;
    GotFirstKeyFrame = false;
    FirstKeyFramePts = 0;
    FirstKeyFrameDts = 0;
    
    HasWritenFirstKeyFrame = false;
    
    GotFirstAudioPacket = false;
    FirstAudioPacketPts = 0;
    FirstAudioPacketDts = 0;
    
    pthread_mutex_init(&mMediaListenerLock, NULL);
    mMediaListener = NULL;
    
    // defaults -- likely not overridden
    video_codec_id = AV_CODEC_ID_H264;
    video_pix_fmt = AV_PIX_FMT_YUV420P;
    audio_codec_id = AV_CODEC_ID_AAC;
    audio_sample_fmt = AV_SAMPLE_FMT_S16;
    
    data_codec_id = AV_CODEC_ID_TEXT;
    
    if (audioOptions->isRealTime) {
        video_codec_id = AV_CODEC_ID_VP8;
        audio_codec_id = AV_CODEC_ID_AMR_NB;
    }
    
    // propagate the configuration
    if (muxerFormat==MP4) {
        isLocalFile = true;
        output_fmt_name = av_strdup("mp4");
    }else if(muxerFormat==RTMP) {
        isLocalFile = false;
        output_fmt_name = av_strdup("flv");
    }else if(muxerFormat==RTSP) {
        isLocalFile = false;
        output_fmt_name = av_strdup("rtsp");
    }else {
        isLocalFile = true;
        output_fmt_name = av_strdup("mp4");
    }
    output_url = av_strdup(publishUrl);
    
    video_width = videoOptions->videoWidth;
    video_height = videoOptions->videoHeight;
    video_fps = videoOptions->videoFps;
    video_bit_rate = videoOptions->videoBitRate*1024;

    audio_sample_rate = audioOptions->audioSampleRate;
    audio_num_channels = audioOptions->audioNumChannels;
    audio_bit_rate = audioOptions->audioBitRate*1024;
    
    if (videoOptions->hasVideo && audioOptions->hasAudio) {
        muxerData = AUDIO_VIDEO;
    }else if(videoOptions->hasVideo && !audioOptions->hasAudio) {
        muxerData = ONLY_VIDEO;
    }else if(!videoOptions->hasVideo && audioOptions->hasAudio) {
        muxerData = ONLY_AUDIO;
    }else {
        muxerData = NONE;
    }
    
    isThreadLive = false;
    
    isInterrupt = 0;
    
    // for bitrate
    av_bitrate_begin_time = 0;
    av_bitrate_datasize = 0;
    mRealtimeBitrate = 0;
    
    // for delay
    av_delay_begin_time = 0;
    
    // init param
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    
    current_video_pts = 0;
    current_video_dts = 0;
    last_video_dts = 0;
    
    current_audio_pts = 0;
    current_audio_dts = 0;
    last_audio_dts = 0;
}

FFmpegMuxer::~FFmpegMuxer()
{
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    if (output_fmt_name) av_free(output_fmt_name);
    if (output_url) av_free(output_url);
    
    pthread_mutex_destroy(&mMediaListenerLock);
}

int FFmpegMuxer::prepare()
{
    char log[1024];

    // initialize FFmpeg
    init_ffmpeg();
    
    // initialize our device time_base
    init_device_time_base();
    
    // initialize our output format context
    init_output_fmt_context();
    
    // set up the streams
    if (muxerData==ONLY_VIDEO || muxerData==AUDIO_VIDEO) {
        add_video_stream();
    }

    if (muxerData==ONLY_AUDIO || muxerData==AUDIO_VIDEO) {
        add_audio_stream();
    }
    
    if (strcmp(output_fmt_name, "mp4")!=0) {
//        add_data_stream();
    }
        
    int rc = open_output_url();
    if (rc < 0){
        LOGE("ERROR: open_output_url error -- %s", av_err2string(rc));
        sprintf(log, "ERROR: open_output_url error -- %s", av_err2string(rc));
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
        
        if (rc==AVERROR_EXIT) {
            return -2;
        }
        
        return -1;
    }
    
    bsfc = av_bitstream_filter_init("aac_adtstoasc");
    
    if (!bsfc) {
        LOGE("Error creating aac_adtstoasc bitstream filter.");
        if (mMediaLog) {
            mMediaLog->writeLog("Error creating aac_adtstoasc bitstream filter.");
        }
        return -1;
    }
    
    isBreakThread = false;
    isMuxing = false;
    isWaittingMediaPacket = false;
    
    this->createMuxerThread();
    isThreadLive = true;
    
    return 0;
}

void FFmpegMuxer::start()
{
    pthread_mutex_lock(&mLock);
    isMuxing = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void FFmpegMuxer::stop()
{
    if (isThreadLive) {
        this->deleteMuxerThread();
    }
    
    mMediaPacketQueue.flush();
    
    if(bsfc!=NULL)
    {
        av_bitstream_filter_close(bsfc);
        bsfc=NULL;
    }
    
    // close the output file
    if (!(output_fmt_ctx->oformat->flags & AVFMT_NOFILE)) {
        avio_close(output_fmt_ctx->pb);
    }
    
    // clean up memory
    if (device_time_base) av_free(device_time_base);
    
    if (video_stream && video_stream->codec) {
        avcodec_close(video_stream->codec);
    }

    if (audio_stream && audio_stream->codec) {
        avcodec_close(audio_stream->codec);
    }
    
    if (data_stream && data_stream->codec) {
        avcodec_close(data_stream->codec);
    }

    if (output_fmt_ctx) avformat_free_context(output_fmt_ctx);
}

void FFmpegMuxer::createMuxerThread()
{
#ifndef WIN32
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&mThread, &attr, handleMuxerThread, this);
	pthread_attr_destroy(&attr);
#else
	pthread_create(&mThread, NULL, handleMuxerThread, this);
#endif
}

void* FFmpegMuxer::handleMuxerThread(void* ptr)
{
#ifdef ANDROID
    LOGD("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGD("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif

    FFmpegMuxer* ffmpegMuxer = (FFmpegMuxer*)ptr;
    ffmpegMuxer->muxerThreadMain();

    return NULL;
}

void FFmpegMuxer::muxerThreadMain()
{
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("%s: AttachCurrentThread() failed", __FUNCTION__);
            return;
        }
    }
#endif
    
    // for local mp4 file record time statistics
    uint64_t lastSendRecordTimeUS = 0;
    uint64_t lastRecordTimeUS = 0;
    
    int lowRealBitrateTimes = 0;
    
    // loop
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            if (isLocalFile) {
                while (true) {
                    MediaPacket *packet = mMediaPacketQueue.pop();
                    if (packet==NULL) break;
                    
                    int ret = mux_media_packet(packet);
                    
                    //Free MediaPacket
                    if (packet->data!=NULL) {
                        free(packet->data);
                        packet->data = NULL;
                    }
                    delete packet;
                    packet = NULL;
                    
                    if (ret<0) break;
                }
                
                write_trailer();
            }
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!isMuxing) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        MediaPacket *packet = mMediaPacketQueue.pop();
        if (packet!=NULL) {
            int ret = mux_media_packet(packet);
            if (ret<0) {
                //Free MediaPacket
                if (packet->data!=NULL) {
                    free(packet->data);
                    packet->data = NULL;
                }
                delete packet;
                packet = NULL;
                
                if (ret == AVERROR_EXIT) {
                    LOGW("Immediate exit was requested");
                    if (mMediaLog) {
                        mMediaLog->writeLog("Immediate exit was requested");
                    }
                }else{
                    LOGE("mux media packet fail");
                    if (mMediaLog) {
                        mMediaLog->writeLog("mux media packet fail");
                    }
                    pthread_mutex_lock(&mMediaListenerLock);
                    if (mMediaListener!=NULL) {
                        mMediaListener->notify(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_MUX_FAIL, 0);
                    }
                    pthread_mutex_unlock(&mMediaListenerLock);
                }
                
                pthread_mutex_lock(&mLock);
                isMuxing = false;
                pthread_mutex_unlock(&mLock);
                continue;
            }
            
            if (ret==1) {
                //for bitrate
                if(packet->size>=0)
                {
                    av_bitrate_datasize += packet->size;
                }
                if (av_bitrate_begin_time==0) {
                    av_bitrate_begin_time = MediaTimer::GetNowMediaTimeMS();
                }
                int64_t av_bitrate_duration = MediaTimer::GetNowMediaTimeMS()-av_bitrate_begin_time;
                if (av_bitrate_duration>=1000) {
                    mRealtimeBitrate = (int)(av_bitrate_datasize * 8 * 1000 / 1024 / av_bitrate_duration);
                    
                    av_bitrate_begin_time = 0;
                    av_bitrate_datasize = 0;
                    
                    pthread_mutex_lock(&mMediaListenerLock);
                    if (mMediaListener!=NULL) {
                        mMediaListener->notify(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_PUBLISH_REAL_BITRATE, mRealtimeBitrate);
                    }
                    pthread_mutex_unlock(&mMediaListenerLock);
                }
                
                if(isLocalFile)
                {
                    // for local mp4 file record time statistics
                    if (packet->packetType==AUDIO_AAC_BODY) {
                        lastRecordTimeUS += (uint64_t)(packet->duration*1000);
                    }
                    if ((lastRecordTimeUS-lastSendRecordTimeUS)>=100000/*100ms*/) {
                        lastSendRecordTimeUS = lastRecordTimeUS;
                        pthread_mutex_lock(&mMediaListenerLock);
                        if (mMediaListener!=NULL) {
                            mMediaListener->notify(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_PUBLISH_TIME, (int)(lastSendRecordTimeUS/100000));//100ms
                        }
                        pthread_mutex_unlock(&mMediaListenerLock);
                    }
                }else{
                    //for delay
                    if (av_delay_begin_time==0) {
                        av_delay_begin_time = MediaTimer::GetNowMediaTimeMS();
                    }
                    
                    if (MediaTimer::GetNowMediaTimeMS()-av_delay_begin_time>=2000) {
                        pthread_mutex_lock(&mMediaListenerLock);
                        if (mMediaListener!=NULL) {
                            mMediaListener->notify(MEDIA_STREAMER_INFO, MEDIA_STREAMER_INFO_PUBLISH_DELAY_TIME, (int)mMediaPacketQueue.duration());
                        }
                        pthread_mutex_unlock(&mMediaListenerLock);
                        
                        av_delay_begin_time = 0;
                    }
                }
            }
            
            //Free MediaPacket
            if (packet->data!=NULL) {
                free(packet->data);
                packet->data = NULL;
            }
            delete packet;
            packet = NULL;

//            if (lowRealBitrateTimes >=7) {
//                if (mMediaListener!=NULL) {
//                    mMediaListener->notify(MEDIA_STREAMER_ERROR, MEDIA_STREAMER_ERROR_POOR_NETWORK,0);
//                }
//                lowRealBitrateTimes = 0;
//                
//                pthread_mutex_lock(&mLock);
//                isMuxing = false;
//                pthread_mutex_unlock(&mLock);
//                continue;
//            }
        }else {
            pthread_mutex_lock(&mLock);
            isWaittingMediaPacket = true;
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
        }
    }
    
    if (!HasWritenFirstKeyFrame) {
        //todo for local record file
        //delete local record file because the file has only header no body
    }

#ifdef ANDROID
    if (mJvm!=NULL) {
        if(mJvm->DetachCurrentThread()!=JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
    
}

void FFmpegMuxer::deleteMuxerThread()
{
    LOGD("FFmpegMuxer::deleteMuxerThread");
    if (mMediaLog) {
        mMediaLog->writeLog("FFmpegMuxer::deleteMuxerThread");
    }
    
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);

    this->interrupt();

    pthread_join(mThread, NULL);
}

void FFmpegMuxer::pushH264Header(VideoPacket *h264Header)
{
    if(h264Header==NULL) return;
    
    MediaPacket *packet = new MediaPacket;
    packet->packetType = VIDEO_H264_SPS_PPS;
    
//    packet->size = h264Header->nals[0]->size + h264Header->nals[1]->size;
//    packet->data = (uint8_t*)malloc(packet->size);
//    memcpy(packet->data, h264Header->nals[0]->data, h264Header->nals[0]->size);
//    memcpy(packet->data+h264Header->nals[0]->size, h264Header->nals[1]->data, h264Header->nals[1]->size);
    
    for (int i = 0; i < h264Header->nal_Num; i++) {
        packet->size += h264Header->nals[i]->size;
    }
    packet->data = (uint8_t*)malloc(packet->size);
    long readPos = 0;
    for (int i = 0; i < h264Header->nal_Num; i++) {
        memcpy(packet->data+readPos, h264Header->nals[i]->data, h264Header->nals[i]->size);
        readPos += h264Header->nals[i]->size;
    }
    
    packet->pts = 0;
    packet->dts = 0;
    
    mMediaPacketQueue.push(packet);
    
    pthread_mutex_lock(&mLock);
    if(isWaittingMediaPacket)
    {
        isWaittingMediaPacket = false;
        pthread_cond_signal(&mCondition);
    }
    pthread_mutex_unlock(&mLock);
}

void FFmpegMuxer::pushH264Body(VideoPacket *h264Body)
{
    if (h264Body==NULL) return;
    
    MediaPacket *packet = new MediaPacket;
    
    for (int i = 0; i < h264Body->nal_Num; i++) {
        packet->size += h264Body->nals[i]->size;
    }
    packet->data = (uint8_t*)malloc(packet->size);
    long readPos = 0;
    for (int i = 0; i < h264Body->nal_Num; i++) {
        memcpy(packet->data+readPos, h264Body->nals[i]->data, h264Body->nals[i]->size);
        readPos += h264Body->nals[i]->size;
    }
    
    packet->pts = h264Body->pts;
    packet->dts = h264Body->dts;
    
    if (h264Body->type == H264_TYPE_I || AVCUtils::avc_keyframe(packet->data, packet->size)) {
        packet->packetType = VIDEO_H264_KEY_FRAME;
    }else {
        packet->packetType = VIDEO_H264_P_OR_B_FRAME;
    }
    
    mMediaPacketQueue.push(packet);
    
    pthread_mutex_lock(&mLock);
    if(isWaittingMediaPacket)
    {
        isWaittingMediaPacket = false;
        pthread_cond_signal(&mCondition);
    }
    pthread_mutex_unlock(&mLock);
}

void FFmpegMuxer::pushVP8Header(VideoPacket *vp8Header)
{
    if(vp8Header==NULL) return;
    
    MediaPacket *packet = new MediaPacket;
    packet->packetType = VIDEO_VP8_HEADER;
    
    for (int i = 0; i < vp8Header->nal_Num; i++) {
        packet->size += vp8Header->nals[i]->size;
    }
    packet->data = (uint8_t*)malloc(packet->size);
    long readPos = 0;
    for (int i = 0; i < vp8Header->nal_Num; i++) {
        memcpy(packet->data+readPos, vp8Header->nals[i]->data, vp8Header->nals[i]->size);
        readPos += vp8Header->nals[i]->size;
    }
    
    packet->pts = 0;
    packet->dts = 0;
    
    mMediaPacketQueue.push(packet);
    
    pthread_mutex_lock(&mLock);
    if(isWaittingMediaPacket)
    {
        isWaittingMediaPacket = false;
        pthread_cond_signal(&mCondition);
    }
    pthread_mutex_unlock(&mLock);
}

void FFmpegMuxer::pushVP8Body(VideoPacket *vp8Body)
{
    if (vp8Body==NULL) return;
    
    MediaPacket *packet = new MediaPacket;
    
    for (int i = 0; i < vp8Body->nal_Num; i++) {
        packet->size += vp8Body->nals[i]->size;
    }
    packet->data = (uint8_t*)malloc(packet->size);
    long readPos = 0;
    for (int i = 0; i < vp8Body->nal_Num; i++) {
        memcpy(packet->data+readPos, vp8Body->nals[i]->data, vp8Body->nals[i]->size);
        readPos += vp8Body->nals[i]->size;
    }
    
    packet->pts = vp8Body->pts;
    packet->dts = vp8Body->dts;
    
    if (vp8Body->type == VP8_TYPE_KEYFRAME) {
        packet->packetType = VIDEO_VP8_KEY_FRAME;
    }else {
        packet->packetType = VIDEO_VP8_NON_KEY_FRAME;
    }
    
    mMediaPacketQueue.push(packet);
    
    pthread_mutex_lock(&mLock);
    if(isWaittingMediaPacket)
    {
        isWaittingMediaPacket = false;
        pthread_cond_signal(&mCondition);
    }
    pthread_mutex_unlock(&mLock);
}

void FFmpegMuxer::pushAACHeader(AudioPacket *aacHeader)
{
    if (aacHeader==NULL) return;
    
    MediaPacket *packet = new MediaPacket;
    packet->packetType = AUDIO_AAC_HEADER;
    packet->size = aacHeader->size;
    packet->data = (uint8_t*)malloc(packet->size);
    memcpy(packet->data, aacHeader->data, aacHeader->size);
    
    packet->pts = 0;
    packet->dts = 0;
    
    mMediaPacketQueue.push(packet);
    
    pthread_mutex_lock(&mLock);
    if(isWaittingMediaPacket)
    {
        isWaittingMediaPacket = false;
        pthread_cond_signal(&mCondition);
    }
    pthread_mutex_unlock(&mLock);
}

void FFmpegMuxer::pushAACBody(AudioPacket *aacBody)
{
    if (aacBody==NULL || aacBody->data==NULL || aacBody->size==0) return;
    
    MediaPacket *packet = new MediaPacket;
    packet->packetType = AUDIO_AAC_BODY;
    packet->size = aacBody->size;
    packet->data = (uint8_t*)malloc(packet->size);
    memcpy(packet->data, aacBody->data, aacBody->size);
    
    packet->pts = aacBody->pts;
    packet->dts = aacBody->dts;
    
    packet->duration = aacBody->duration;
    
    mMediaPacketQueue.push(packet);
    
    pthread_mutex_lock(&mLock);
    if(isWaittingMediaPacket)
    {
        isWaittingMediaPacket = false;
        pthread_cond_signal(&mCondition);
    }
    pthread_mutex_unlock(&mLock);
}

void FFmpegMuxer::pushAMRHeader(AudioPacket *amrHeader)
{
    if (amrHeader==NULL) return;
    
    MediaPacket *packet = new MediaPacket;
    packet->packetType = AUDIO_AMR_HEADER;
    packet->size = amrHeader->size;
    packet->data = (uint8_t*)malloc(packet->size);
    memcpy(packet->data, amrHeader->data, amrHeader->size);
    
    packet->pts = 0;
    packet->dts = 0;
    
    mMediaPacketQueue.push(packet);
    
    pthread_mutex_lock(&mLock);
    if(isWaittingMediaPacket)
    {
        isWaittingMediaPacket = false;
        pthread_cond_signal(&mCondition);
    }
    pthread_mutex_unlock(&mLock);
}

void FFmpegMuxer::pushAMRBody(AudioPacket *amrBody)
{
    if (amrBody==NULL || amrBody->data==NULL || amrBody->size==0) return;
    
    MediaPacket *packet = new MediaPacket;
    packet->packetType = AUDIO_AMR_BODY;
    packet->size = amrBody->size;
    packet->data = (uint8_t*)malloc(packet->size);
    memcpy(packet->data, amrBody->data, amrBody->size);
    
    packet->pts = amrBody->pts;
    packet->dts = amrBody->dts;
    
    packet->duration = amrBody->duration;
    
    mMediaPacketQueue.push(packet);
    
    pthread_mutex_lock(&mLock);
    if(isWaittingMediaPacket)
    {
        isWaittingMediaPacket = false;
        pthread_cond_signal(&mCondition);
    }
    pthread_mutex_unlock(&mLock);
}

void FFmpegMuxer::pushTextData(TextPacket* textPacket)
{
    if (textPacket==NULL || textPacket->data==NULL || textPacket->size==0) return;
    
    MediaPacket *packet = new MediaPacket;
    packet->packetType = TEXT;
    packet->size = textPacket->size;
    packet->data = (uint8_t*)malloc(packet->size);
    memset(packet->data, 0, packet->size);
    memcpy(packet->data, textPacket->data, textPacket->size);
    
    packet->pts = textPacket->pts;
    packet->dts = textPacket->pts;
    
    mMediaPacketQueue.push(packet);
    
    pthread_mutex_lock(&mLock);
    if(isWaittingMediaPacket)
    {
        isWaittingMediaPacket = false;
        pthread_cond_signal(&mCondition);
    }
    pthread_mutex_unlock(&mLock);
}

int64_t FFmpegMuxer::getPublishDelayTimeMs()
{
    return mMediaPacketQueue.duration();
}

// return negative : write fail
// return 0 : no write or write headers success
// return 1 : write media body success
int FFmpegMuxer::mux_media_packet(MediaPacket *mediaPacket)
{
    int ret = 0;
    switch (mediaPacket->packetType) {
        case VIDEO_VP8_HEADER:
        case VIDEO_H264_SPS_PPS:
            if(HasWriteHeader) return 0;
            if(muxerData==ONLY_AUDIO) return 0;
            set_video_codec_extradata(mediaPacket->data, mediaPacket->size);
            GotVideoHeader = true;
            if (muxerData==ONLY_VIDEO || GotAudioHeader) {
                ret = write_header();
                
                if (ret>=0) {
                    HasWriteHeader = true;
                }
                
                return ret;
            }
            return 0;
        case AUDIO_AMR_HEADER:
        case AUDIO_AAC_HEADER:
            if(HasWriteHeader) return 0;
            if(muxerData==ONLY_VIDEO) return 0;
            set_audio_codec_extradata(mediaPacket->data, mediaPacket->size);
            GotAudioHeader = true;
            if (muxerData==ONLY_AUDIO || GotVideoHeader) {
                ret = write_header();
                
                if (ret>=0) {
                    HasWriteHeader = true;
                }
                
                return ret;
            }
            return 0;
        case VIDEO_VP8_KEY_FRAME:
        case VIDEO_H264_KEY_FRAME:
            if(muxerData==ONLY_AUDIO) return 0;
            
            if (HasWriteHeader) {
                if (!GotFirstKeyFrame) {
                    GotFirstKeyFrame = true;
                    FirstKeyFramePts = mediaPacket->pts;
                    FirstKeyFrameDts = mediaPacket->dts;
                }

                current_video_pts = mediaPacket->pts-FirstKeyFramePts;
                current_video_dts = mediaPacket->dts-FirstKeyFrameDts;
                if (current_video_dts<=last_video_dts) {
                    current_video_dts = last_video_dts + 1000/video_fps;
                    last_video_dts = current_video_dts;
                }else{
                    last_video_dts = current_video_dts;
                }
                
                mediaPacket->pts = current_video_pts;
                mediaPacket->dts = current_video_dts;
                
                int ret = write_media_packet(mediaPacket->data, mediaPacket->size, mediaPacket->pts, mediaPacket->dts, true, true);
                
                if (!HasWritenFirstKeyFrame && ret>0) {
                    HasWritenFirstKeyFrame = true;
                }
                
                return ret;
            }
            return 0;
        case VIDEO_VP8_NON_KEY_FRAME:
        case VIDEO_H264_P_OR_B_FRAME:
            if(muxerData==ONLY_AUDIO) return 0;

            if (HasWriteHeader && HasWritenFirstKeyFrame) {
                
                current_video_pts = mediaPacket->pts-FirstKeyFramePts;
                current_video_dts = mediaPacket->dts-FirstKeyFrameDts;
                if (current_video_dts<=last_video_dts) {
                    current_video_dts = last_video_dts + 1000/video_fps;
                    last_video_dts = current_video_dts;
                }else{
                    last_video_dts = current_video_dts;
                }
                
                mediaPacket->pts = current_video_pts;
                mediaPacket->dts = current_video_dts;
                
                int ret = write_media_packet(mediaPacket->data, mediaPacket->size, mediaPacket->pts, mediaPacket->dts, true, false);
                
                return ret;
            }
            return 0;
        case AUDIO_AMR_BODY:
        case AUDIO_AAC_BODY:
            if(muxerData==ONLY_VIDEO) return 0;
            
            if (HasWriteHeader && (muxerData==ONLY_AUDIO || (muxerData==AUDIO_VIDEO && HasWritenFirstKeyFrame))) {

                if (!GotFirstAudioPacket) {
                    GotFirstAudioPacket = true;
                    FirstAudioPacketPts = mediaPacket->pts;
                    FirstAudioPacketDts = mediaPacket->dts;
                }
                
                current_audio_pts = mediaPacket->pts-FirstAudioPacketPts;
                current_audio_dts = current_audio_pts;
                if (current_audio_dts<=last_audio_dts) {
                    current_audio_dts = last_audio_dts + mediaPacket->size * 1000 / audio_sample_rate / audio_num_channels / av_get_bytes_per_sample(audio_sample_fmt);
                    last_audio_dts = current_audio_dts;
                }else{
                    last_audio_dts = current_audio_dts;
                }
                
                mediaPacket->pts = current_audio_dts;
                mediaPacket->dts = current_audio_dts;
                
                int ret = write_media_packet(mediaPacket->data, mediaPacket->size, mediaPacket->pts, mediaPacket->dts, false, false);
                
                return ret;
            }
            return 0;
        case TEXT:
            if (HasWriteHeader) {
                int ret = write_data_packet(mediaPacket->data, mediaPacket->size, mediaPacket->pts, mediaPacket->dts);
                
                return ret;
                
            }
            return 0;
        default:
            return -1;
    }
}

void FFmpegMuxer::init_ffmpeg()
{
    // initialize FFmpeg
    av_register_all();
    avformat_network_init();
    avcodec_register_all();
    
    FFLog::setLogLevel(AV_LOG_WARNING);
}

void FFmpegMuxer::init_device_time_base()
{
    // timestamps from the device should be in microseconds
    device_time_base = (AVRational*)av_malloc(sizeof(AVRational));
    device_time_base->num = 1;
    device_time_base->den = 1000000;
}

void FFmpegMuxer::init_output_fmt_context()
{
    char log[2048];

    int rc;
    AVOutputFormat *fmt;
    
    LOGD("init_output_fmt_context format: %s path: %s", output_fmt_name, output_url);
    sprintf(log, "init_output_fmt_context format: %s path: %s", output_fmt_name, output_url);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    rc = avformat_alloc_output_context2(&output_fmt_ctx, NULL, output_fmt_name, output_url);
    if (rc < 0) {
        LOGE("Error getting format context for output path: %s", av_err2string(rc));
        sprintf(log, "Error getting format context for output path: %s", av_err2string(rc));
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
    }
    
    output_fmt_ctx->start_time_realtime = 0;
    
    fmt = output_fmt_ctx->oformat;
    fmt->video_codec = video_codec_id;
    fmt->audio_codec = audio_codec_id;
    
    LOGD("fmt->name: %s", fmt->name);
    LOGD("fmt->long_name: %s", fmt->long_name);
    LOGD("fmt->mime_type: %s", fmt->mime_type);
    LOGD("fmt->extensions: %s", fmt->extensions);
    LOGD("fmt->audio_codec: %d", fmt->audio_codec);
    LOGD("fmt->video_codec: %d", fmt->video_codec);
    LOGD("fmt->subtitle_codec: %d", fmt->subtitle_codec);
    LOGD("fmt->flags: %d", fmt->flags);
}

AVStream* FFmpegMuxer::add_stream(enum AVCodecID codec_id)
{
    char log[1024];

    AVStream *st;
    AVCodec *codec;
    AVCodecContext *c;
    
    codec = avcodec_find_decoder(codec_id);
    if (!codec) {
        LOGE("ERROR: add_stream -- codec %d not found", codec_id);
        sprintf(log, "ERROR: add_stream -- codec %d not found", codec_id);
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
    }
    LOGD("codec->name: %s", codec->name);
    LOGD("codec->long_name: %s", codec->long_name);
    LOGD("codec->type: %d", codec->type);
    LOGD("codec->id: %d", codec->id);
    LOGD("codec->capabilities: %d", codec->capabilities);
    
    st = avformat_new_stream(output_fmt_ctx, codec);
    if (!st) {
        LOGE("ERROR: add_stream -- could not allocate new stream");
        if (mMediaLog) {
            mMediaLog->writeLog("ERROR: add_stream -- could not allocate new stream");
        }
        return NULL;
    }
    // TODO: need avcodec_get_context_defaults3?
    //avcodec_get_context_defaults3(st->codec, codec);
    st->id = output_fmt_ctx->nb_streams-1;
    c = st->codec;
    LOGI("add_stream at index %d", st->index);
    sprintf(log, "add_stream at index %d", st->index);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    
    // Some formats want stream headers to be separate.
    if (output_fmt_ctx->oformat->flags & AVFMT_GLOBALHEADER) {
        LOGD("add_stream: using separate headers");
        c->flags |= CODEC_FLAG_GLOBAL_HEADER;
    }
    
    LOGD("add_stream st: %p", st);
    sprintf(log, "add_stream st: %p", st);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    return st;
}

void FFmpegMuxer::add_video_stream()
{
    AVCodecContext *c;
    
    video_stream = add_stream(video_codec_id);
    video_stream_index = video_stream->index;
    c = video_stream->codec;
    
    // video parameters
    c->codec_id = video_codec_id;
    c->pix_fmt = video_pix_fmt;
    c->width = video_width;
    c->height = video_height;
    c->bit_rate = video_bit_rate;
    
    // timebase: This is the fundamental unit of time (in seconds) in terms
    // of which frame timestamps are represented. For fixed-fps content,
    // timebase should be 1/framerate and timestamp increments should be
    // identical to 1.
    c->time_base.den = video_fps;
    c->time_base.num = 1;
}

void FFmpegMuxer::add_audio_stream()
{
    AVCodecContext *c;
    
    audio_stream = add_stream(audio_codec_id);
    audio_stream_index = audio_stream->index;
    c = audio_stream->codec;
    
    // audio parameters
    c->strict_std_compliance = FF_COMPLIANCE_UNOFFICIAL; // for native aac support
    c->sample_fmt  = audio_sample_fmt;
    c->sample_rate = audio_sample_rate;
    c->channels    = audio_num_channels;
    c->bit_rate    = audio_bit_rate;
    //c->time_base.num = 1;
    //c->time_base.den = c->sample_rate;
}

void FFmpegMuxer::add_data_stream()
{
    AVCodecContext *c;
    data_stream = add_stream(data_codec_id);
    data_stream_index = data_stream->index;
    c = data_stream->codec;
    
    c->codec_type = AVMEDIA_TYPE_SUBTITLE;
}

int FFmpegMuxer::open_output_url()
{
    char log[2048];

    output_fmt_ctx->interrupt_callback.callback = interruptCallback;
    output_fmt_ctx->interrupt_callback.opaque = this;
    
    AVDictionary* options = NULL;
    av_dict_set(&options, "rtsp_transport", "udp", 0);
    av_dict_set(&options, "fflags", "nobuffer", 0);
    
    if (!(output_fmt_ctx->oformat->flags & AVFMT_NOFILE)) {
        LOGD("Opening output file for writing at path %s", output_url);
        
        sprintf(log, "Opening output file for writing at path %s", output_url);
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
        
        if(isLocalFile)
        {
            return avio_open(&output_fmt_ctx->pb, output_url, AVIO_FLAG_WRITE | AVIO_FLAG_NONBLOCK);
        }else{
            return avio_open2(&output_fmt_ctx->pb, output_url, AVIO_FLAG_WRITE | AVIO_FLAG_NONBLOCK, &output_fmt_ctx->interrupt_callback, &options);
        }
    } else {
        LOGD("This format does not require a file.");
        if (mMediaLog) {
            mMediaLog->writeLog("This format does not require a file.");
        }
        return 0;
    }
}

void FFmpegMuxer::set_audio_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size)
{
    // this will automatically be freed by avformat_free_context()
    audio_stream->codec->extradata = (uint8_t *)av_malloc(codec_extradata_size);
    audio_stream->codec->extradata_size = codec_extradata_size;
    memcpy(audio_stream->codec->extradata, codec_extradata, codec_extradata_size);
    
    log_codec_attributes(audio_stream->codec);
}

void FFmpegMuxer::set_video_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size)
{
    // this will automatically be freed by avformat_free_context()
    video_stream->codec->extradata = (uint8_t *)av_malloc(codec_extradata_size);
    video_stream->codec->extradata_size = codec_extradata_size;
    memcpy(video_stream->codec->extradata, codec_extradata, codec_extradata_size);
    
    log_codec_attributes(video_stream->codec);
}

void FFmpegMuxer::log_codec_attributes(AVCodecContext *codec)
{
    int i;
    
    if (codec->codec_type==AVMEDIA_TYPE_AUDIO) {
        LOGD("audio_stream->codec->codec_type: %d", codec->codec_type);
        LOGD("audio_stream->codec->codec_id: %d", codec->codec_id);
        LOGD("audio_stream->codec->codec_tag: %d", codec->codec_tag);
        LOGD("audio_stream->codec->stream_codec_tag: %d", codec->stream_codec_tag);
        LOGD("audio_stream->codec->flags: %d", codec->flags);
        LOGD("audio_stream->codec->flags2: %d", codec->flags2);
        LOGD("audio_stream->codec->extradata_size: %d", codec->extradata_size);
        for (i=0; i<codec->extradata_size; ++i) {
            LOGD("audio_stream->codec->extradata byte: 0x%x", codec->extradata[i]);
        }
    }
    
    if (codec->codec_type==AVMEDIA_TYPE_VIDEO) {
        LOGD("video_stream->codec->codec_type: %d", codec->codec_type);
        LOGD("video_stream->codec->codec_id: %d", codec->codec_id);
        LOGD("video_stream->codec->codec_tag: %d", codec->codec_tag);
        LOGD("video_stream->codec->stream_codec_tag: %d", codec->stream_codec_tag);
        LOGD("video_stream->codec->flags: %d", codec->flags);
        LOGD("video_stream->codec->flags2: %d", codec->flags2);
        LOGD("video_stream->codec->extradata_size: %d", codec->extradata_size);
        for (i=0; i<codec->extradata_size; ++i) {
            LOGD("video_stream->codec->extradata byte: 0x%x", codec->extradata[i]);
        }
    }
}

int FFmpegMuxer::write_header()
{
    LOGD("Writing header ...");
    if (mMediaLog) {
        mMediaLog->writeLog("Writing header ...");
    }
    
    AVDictionary* options = NULL;
//    av_dict_set(&options, "movflags", "faststart", 0);
    
    int rc = avformat_write_header(output_fmt_ctx, &options);
    if (rc < 0) {
        char log[1024];
        LOGE("Error writing header: %s", av_err2string(rc));
        sprintf(log, "Error writing header: %s", av_err2string(rc));
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
    }
    return rc;
}

int FFmpegMuxer::filter_packet(AVStream *st, AVPacket *packet)
{
    int rc = 0;
    uint8_t *filtered_data = NULL;
    int filtered_data_size = 0;
    
    if (st->codec->codec_id == audio_codec_id) {
        rc = av_bitstream_filter_filter(bsfc, st->codec, NULL,
                                        &filtered_data, &filtered_data_size,
                                        packet->data, packet->size,
                                        packet->flags & AV_PKT_FLAG_KEY);

        if (rc < 0) {
            char log[1024];
            LOGE("ERROR: Failed to filter bitstream -- %s", av_err2string(rc));
            sprintf(log, "ERROR: Failed to filter bitstream -- %s", av_err2string(rc));
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
        }
        
        if (rc>0) {
            packet->data = filtered_data;
            packet->size = filtered_data_size;
        }
        
        return rc;
    } else {
        return 0;
    }
}

void FFmpegMuxer::rescale_packet(AVStream *st, AVPacket *packet)
{
//    LOGD("time bases: stream=%d/%d, codec=%d/%d, device=%d/%d",
//         st->time_base.num, st->time_base.den,
//         st->codec->time_base.num, st->codec->time_base.den,
//         (*device_time_base).num, (*device_time_base).den);
    
    packet->pts = av_rescale_q(packet->pts, *(device_time_base), st->time_base);
    packet->dts = av_rescale_q(packet->dts, *(device_time_base), st->time_base);
}

int FFmpegMuxer::write_packet(AVPacket *packet)
{
//    LOGD("start writing frame to stream %d: (pts=%lld, size=%d)",packet->stream_index, packet->pts, packet->size);
    
    int rc = av_interleaved_write_frame(output_fmt_ctx, packet);
    if (rc < 0){
        char log[1024];
        LOGE("ERROR: write_packet stream (stream %d) -- %s",packet->stream_index, av_err2string(rc));
        sprintf(log, "ERROR: write_packet stream (stream %d) -- %s",packet->stream_index, av_err2string(rc));
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
    }
    
//    LOGD("end writing frame to stream %d: (pts=%lld, size=%d)",packet->stream_index, packet->pts, packet->size);
    
    return rc;
}

int FFmpegMuxer::write_trailer()
{
    LOGD("Writing trailer ...");
    if (mMediaLog) {
        mMediaLog->writeLog("Writing trailer ...");
    }
    int rc = av_write_trailer(output_fmt_ctx);
    if (rc < 0) {
        char log[1024];
        LOGE("Error writing trailer: %s", av_err2string(rc));
        sprintf(log, "Error writing trailer: %s", av_err2string(rc));
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
    }
    
    return rc;
}

// return negative : write fail
// return 0 : no write
// return 1 : write media body success
int FFmpegMuxer::write_media_packet(uint8_t *data, int data_size, uint64_t pts, uint64_t dts, int is_video, int is_video_keyframe)
{
    if (data==NULL || data_size==0) {
        return 0;
    }
 
    AVPacket *packet = (AVPacket *)av_malloc(sizeof(AVPacket));
    if (!packet) {
        LOGE("ERROR: write_media_packet couldn't allocate memory for the AVPacket");
        if (mMediaLog) {
            mMediaLog->writeLog("ERROR: write_media_packet couldn't allocate memory for the AVPacket");
        }
        return -1;
    }

    av_init_packet(packet);
    
    if (is_video) {
        packet->stream_index = video_stream_index;
        if (is_video_keyframe) {
            packet->flags |= AV_PKT_FLAG_KEY;
        }
    } else {
        packet->stream_index = audio_stream_index;
    }
    packet->size = data_size;
    packet->pts = pts*1000;
    packet->dts = dts*1000;
    packet->data = data;

    AVStream *st = output_fmt_ctx->streams[packet->stream_index];
    
    /*
    // filter the packet (if necessary)
    int filterRet = filter_packet(st, packet);
    if (filterRet<0) {
        av_free(packet);
        return filterRet;
    }
    */
    
    // rescale the timing information for the packet
    rescale_packet(st, packet);
    
    if (packet->pts<0) {
        packet->pts = 0;
    }
    
    if (packet->dts<0) {
        packet->dts = 0;
    }
    
    // write the packet
    int ret = write_packet(packet);
/*
    // clean up
    if(filterRet>0)
    {
        av_free_packet(packet);
    }
*/
    av_freep(&packet);
    
    if (ret>=0) {
        return 1;
    }
    
    return ret;
}

int FFmpegMuxer::write_data_packet(uint8_t *data, int data_size, uint64_t pts, uint64_t dts)
{
    if (data==NULL || data_size<=0) {
        return 0;
    }
    
    AVPacket *packet = (AVPacket *)av_malloc(sizeof(AVPacket));
    if (!packet) {
        LOGE("ERROR: write_data_packet couldn't allocate memory for the AVPacket");
        return -1;
    }
    
    av_init_packet(packet);
    
    packet->stream_index = data_stream_index;
    packet->size = data_size;
    packet->pts = pts*1000;
    packet->dts = dts*1000;
    packet->data = data;
    
    AVStream *st = output_fmt_ctx->streams[packet->stream_index];
    rescale_packet(st, packet);
    int ret = write_packet(packet);
    av_freep(&packet);
    
    if (ret>=0) {
        return 1;
    }
    
    return ret;
}

char *FFmpegMuxer::av_err2string(int errnum)
{
    return av_make_error_string(av_err_str, AV_ERROR_MAX_STRING_SIZE, errnum);
}

#ifdef ANDROID
void FFmpegMuxer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

void FFmpegMuxer::setListener(IMediaListener *mediaListener)
{
    pthread_mutex_lock(&mMediaListenerLock);
    mMediaListener = mediaListener;
    pthread_mutex_unlock(&mMediaListenerLock);
}

int FFmpegMuxer::interruptCallback(void* opaque)
{
    FFmpegMuxer* thiz = (FFmpegMuxer* )opaque;
    
    return thiz->interruptCallbackMain();
}

int FFmpegMuxer::interruptCallbackMain()
{
    int ret = 0;
    
    pthread_mutex_lock(&mLock);
    ret = isInterrupt;
    pthread_mutex_unlock(&mLock);
    
    return ret;
}

void FFmpegMuxer::interrupt()
{
    pthread_mutex_lock(&mLock);
    isInterrupt = 1;
    pthread_mutex_unlock(&mLock);
}
