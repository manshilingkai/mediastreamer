//
//  MediaMuxer.cpp
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include <stdio.h>

#include "MediaMuxer.h"
#include "FFmpegMuxer.h"
#include "FFmpegWriter.h"
#include "MediaLog.h"

MediaMuxer* MediaMuxer::CreateMediaMuxer(MUXER_TYPE muxerType, MUXER_FORMAT muxerFormat, char *publishUrl, MediaLog* mediaLog, VideoOptions *videoOptions, AudioOptions *audioOptions)
{
    if (muxerType==FFMPEG_STREAMER) {
        return new FFmpegMuxer(muxerFormat, publishUrl, mediaLog, videoOptions, audioOptions);
    }
    
    if (muxerType==FFMPEG_PROCESSER) {
        return new FFmpegWriter(muxerFormat, publishUrl, videoOptions, audioOptions);
    }
    
    return NULL;
}

void MediaMuxer::DeleteMediaMuxer(MediaMuxer* mediaMuxer, MUXER_TYPE muxerType)
{
    if (muxerType==FFMPEG_STREAMER) {
        FFmpegMuxer* ffmpegMuxer = (FFmpegMuxer*)mediaMuxer;
        delete ffmpegMuxer;
        ffmpegMuxer = NULL;
    }
    
    if (muxerType==FFMPEG_PROCESSER) {
        FFmpegWriter* ffmpegWriter = (FFmpegWriter*)mediaMuxer;
        delete ffmpegWriter;
        ffmpegWriter = NULL;
    }
}
