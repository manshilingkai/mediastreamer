//
//  FFmpegMuxer.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef FFmpegMuxer_h
#define FFmpegMuxer_h

#include <stdio.h>

#include "MediaMuxer.h"

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#include <sys/resource.h>
#endif

#include "MediaPacketQueue.h"

extern "C" {
#include "libavutil/error.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
}

class FFmpegMuxer : public MediaMuxer
{
public:
    FFmpegMuxer(MUXER_FORMAT muxerFormat, char *publishUrl, MediaLog* mediaLog, VideoOptions *videoOptions, AudioOptions *audioOptions);
    ~FFmpegMuxer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
    void setListener(IMediaListener *mediaListener);
    
    int prepare();
    void start();
    void stop();
    
    void pushH264Header(VideoPacket *h264Header);
    void pushH264Body(VideoPacket *h264Body);
    
    void pushVP8Header(VideoPacket *vp8Header);
    void pushVP8Body(VideoPacket *vp8Body);
    
    void pushAACHeader(AudioPacket *aacHeader);
    void pushAACBody(AudioPacket *aacBody);
    
    void pushAMRHeader(AudioPacket *amrHeader);
    void pushAMRBody(AudioPacket *amrBody);
    
    void pushTextData(TextPacket* textPacket);

    void interrupt();
    
    int64_t getPublishDelayTimeMs();

private:

#ifdef ANDROID
    JavaVM *mJvm;
#endif
    
    bool isLocalFile;
    
    // context -- must be memory-managed
    char *output_fmt_name;
    char *output_url;
    AVFormatContext *output_fmt_ctx;
    AVRational *device_time_base;
    
    // for convenience
    int video_stream_index;
    AVStream *video_stream;
    AVCodec *video_codec;
    
    int audio_stream_index;
    AVStream *audio_stream;
    AVCodec *audio_codec;
    
    int data_stream_index;
    AVStream *data_stream;
    AVCodec *data_codec;
    
    // video config
    enum AVCodecID video_codec_id;
    enum AVPixelFormat video_pix_fmt;
    int video_width;
    int video_height;
    int video_fps;
    int video_bit_rate;
    
    // audio config
    enum AVCodecID audio_codec_id;
    enum AVSampleFormat audio_sample_fmt;
    int audio_sample_rate;
    int audio_num_channels;
    int audio_bit_rate;
    
    //data config
    enum AVCodecID data_codec_id;
    
    // filter
    AVBitStreamFilterContext* bsfc;
    
    MUXER_DATA muxerData;

    bool GotAudioHeader;
    bool GotVideoHeader;
    bool HasWriteHeader;
    bool GotFirstKeyFrame;
    int64_t FirstKeyFramePts;
    int64_t FirstKeyFrameDts;
    
    bool HasWritenFirstKeyFrame;
    
    bool GotFirstAudioPacket;
    int64_t FirstAudioPacketPts;
    int64_t FirstAudioPacketDts;
    
    //method for ffmpeg
    void init_ffmpeg();
    void init_device_time_base();
    void init_output_fmt_context();
    
    AVStream* add_stream(enum AVCodecID codec_id);
    void add_video_stream();
    void add_audio_stream();
    void add_data_stream();
    
    int open_output_url();
    
    void set_audio_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size);
    void set_video_codec_extradata(uint8_t *codec_extradata, int codec_extradata_size);
    void log_codec_attributes(AVCodecContext *codec);
    
    int write_header();
    
    int filter_packet(AVStream *st, AVPacket *packet);
    void rescale_packet(AVStream *st, AVPacket *packet);
    
    int write_packet(AVPacket *packet);
    int write_trailer();
    
    // return negative : write fail
    // return 0 : no write
    // return 1 : write media body success
    int write_media_packet(uint8_t *data, int data_size, uint64_t pts, uint64_t dts, int is_video, int is_video_keyframe);
    
    int write_data_packet(uint8_t *data, int data_size, uint64_t pts, uint64_t dts);
    
    //method for thread
    bool isThreadLive;
    void createMuxerThread();
    static void* handleMuxerThread(void* ptr);
    void muxerThreadMain();
    void deleteMuxerThread();
    
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    bool isBreakThread; // critical value
    bool isMuxing; // critical value
    bool isWaittingMediaPacket; // critical value
    
    MediaPacketQueue mMediaPacketQueue;
    
    // return negative : write fail
    // return 0 : no write or write headers success
    // return 1 : write media body success
    int mux_media_packet(MediaPacket *mediaPacket);
    
    char av_err_str[AV_ERROR_MAX_STRING_SIZE];
    char *av_err2string(int errnum);
    
    pthread_mutex_t mMediaListenerLock;
    IMediaListener *mMediaListener;
        
    static int interruptCallback(void* opaque);
    int interruptCallbackMain();
    int isInterrupt; // critical value
    
    // for bitrate statistics
    int64_t av_bitrate_begin_time;
    int64_t av_bitrate_datasize;
    int mRealtimeBitrate; // kbps // critical value
    
    // for delay statistics
    int64_t av_delay_begin_time;
private:
    MediaLog* mMediaLog;
private:
    uint64_t current_video_pts;
    uint64_t current_video_dts;
    uint64_t last_video_dts;
    
    uint64_t current_audio_pts;
    uint64_t current_audio_dts;
    uint64_t last_audio_dts;
};


#endif /* FFmpegMuxer_h */
