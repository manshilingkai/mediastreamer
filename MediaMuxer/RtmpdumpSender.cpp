//
//  RtmpdumpSender.cpp
//  MediaStreamer
//
//  Created by 施灵凯 on 15/3/4.
//  Copyright (c) 2015年 bolome. All rights reserved.
//

#include "RtmpdumpSender.h"
#include "Media_Log.h"
#include "AudioEncoder.h"
#include "VideoEncoder.h"

#include "MediaTime.h"

RtmpdumpSender::RtmpdumpSender(char* rtmp_url)
{
    rtmp = RTMP_Alloc();
    RTMP_Init(rtmp);
    
    if (RTMP_SetupURL(rtmp,rtmp_url) == FALSE) {
        LOGE("%s","RTMP_SetupURL() failed!");
        RTMP_Free(rtmp);
        return;
    }
    
    RTMP_EnableWrite(rtmp);
    
    if (RTMP_Connect(rtmp, NULL) == FALSE) {
        LOGE("%s","RTMP_Connect() failed!");
        RTMP_Free(rtmp);
        return;
    }
    
    if (RTMP_ConnectStream(rtmp,0) == FALSE) {
        LOGE("%s","RTMP_ConnectStream() failed!");
        RTMP_Close(rtmp);
        RTMP_Free(rtmp);
        return;
    }
    
    char *ptr = (char*)malloc(MAX_RTMP_AAC_BODY_SIZE + RTMP_MAX_HEADER_SIZE);
    rtmpAACBody = ptr + RTMP_MAX_HEADER_SIZE;
    
    ptr = (char*)malloc(MAX_RTMP_H264_BODY_SIZE + RTMP_MAX_HEADER_SIZE);
    rtmpH264Body = ptr + RTMP_MAX_HEADER_SIZE;
    
    startTimeStamp = GetNowMs();
}

RtmpdumpSender::~RtmpdumpSender()
{
    RTMP_Close(rtmp);
    RTMP_Free(rtmp);
    
    free(rtmpAACBody-RTMP_MAX_HEADER_SIZE);
    free(rtmpH264Body-RTMP_MAX_HEADER_SIZE);
}

int RtmpdumpSender::mux_media_packet(MediaPacket *mediaPacket)
{
    switch (mediaPacket->packetType) {
        case VIDEO_H264_SPS_PPS:
            return send_h264_header(mediaPacket);
        case VIDEO_H264_KEY_FRAME:
            return send_h264_data(mediaPacket);
        case VIDEO_H264_P_OR_B_FRAME:
            return send_h264_data(mediaPacket);
        case AUDIO_AAC_HEADER:
            return send_aac_header(mediaPacket);
        case AUDIO_AAC_BODY:
            return send_aac_data(mediaPacket);
        default:
            return -1;
    }
}

int RtmpdumpSender::send_h264_header(MediaPacket *h264Header)
{
    // parse h264 header
    uint8_t *sps = h264Header->data + 4;
    uint8_t *pps = NULL;
    int sps_len = 0;
    int pps_len = 0;
    
    for (int offset=4; offset <= h264Header->size-4; offset++) {
        if(h264Header->data[offset] == 0x00
           && h264Header->data[offset+1] == 0x00
           && h264Header->data[offset+2] == 0x00
           && h264Header->data[offset+3] == 0x01)
        {
            pps = h264Header->data + offset + 4;
            
            sps_len = offset - 4;
            pps_len = h264Header->size - offset - 4;
            
            break;
        }
    }
    
    //
    memset(&rtmpH264Packet, 0, sizeof(RTMPPacket));
    memset(rtmpH264Body, 0, MAX_RTMP_H264_BODY_SIZE);
    
    unsigned char * body = (unsigned char *)rtmpH264Body;
    
    int i = 0;
    body[i++] = 0x17;
    body[i++] = 0x00;
    
    body[i++] = 0x00;
    body[i++] = 0x00;
    body[i++] = 0x00;
    
    /*AVCDecoderConfigurationRecord*/
    body[i++] = 0x01;
    body[i++] = sps[1];
    body[i++] = sps[2];
    body[i++] = sps[3];
    body[i++] = 0xff;
    
    /*sps*/
    body[i++] = 0xe1;
    body[i++] = (sps_len >> 8) & 0xff;
    body[i++] = sps_len & 0xff;
    memcpy(&body[i],sps,sps_len);
    i +=  sps_len;
    
    /*pps*/
    body[i++]   = 0x01;
    body[i++] = (pps_len >> 8) & 0xff;
    body[i++] = (pps_len) & 0xff;
    memcpy(&body[i],pps,pps_len);
    i +=  pps_len;
    
    rtmpH264Packet.m_packetType = RTMP_PACKET_TYPE_VIDEO;
    rtmpH264Packet.m_nBodySize = i;
    rtmpH264Packet.m_nChannel = 0x04;
    rtmpH264Packet.m_nTimeStamp = 0;
    rtmpH264Packet.m_hasAbsTimestamp = 0;
    rtmpH264Packet.m_headerType = RTMP_PACKET_SIZE_MEDIUM;
    rtmpH264Packet.m_nInfoField2 = rtmp->m_stream_id;
    rtmpH264Packet.m_body = rtmpH264Body;
    
    if (RTMP_IsConnected(rtmp)) {
        if(RTMP_SendPacket(rtmp,&rtmpH264Packet,TRUE)) return 0;
        else return -1;
    }else return -1;
}

int RtmpdumpSender::send_h264_data(MediaPacket *h264Body)
{
    unsigned char *buf;
    long len;
    
    buf = h264Body->data;
    len = h264Body->size;

    /*去掉帧界定符*/
    if (buf[2] == 0x00) { /*00 00 00 01*/
        buf += 4;
        len -= 4;
    } else if (buf[2] == 0x01){ /*00 00 01*/
        buf += 3;
        len -= 3;
    }
    
    int type;
//    long timeoffset;
    unsigned char *body;
    
    type = buf[0]&0x1f;
//    timeoffset = GetNowMs() - startTimeStamp;
    
    memset(&rtmpH264Packet, 0, sizeof(RTMPPacket));
    memset(rtmpH264Body, 0, MAX_RTMP_H264_BODY_SIZE);

    body = (unsigned char *)rtmpH264Body;
    
    /*key frame*/
    body[0] = 0x27;
    if (type == VIDEO_NAL_SLICE_IDR) {
        body[0] = 0x17;
    }
    
    body[1] = 0x01;   /*nal unit*/
    body[2] = 0x00;
    body[3] = 0x00;
    body[4] = 0x00;
    
    body[5] = (len >> 24) & 0xff;
    body[6] = (len >> 16) & 0xff;
    body[7] = (len >>  8) & 0xff;
    body[8] = (len ) & 0xff;
    
    /*copy data*/
    memcpy(&body[9],buf,len);
    
    rtmpH264Packet.m_hasAbsTimestamp = 0;
    rtmpH264Packet.m_packetType = RTMP_PACKET_TYPE_VIDEO;
    rtmpH264Packet.m_nInfoField2 = rtmp->m_stream_id;
    rtmpH264Packet.m_nChannel = 0x04;
    rtmpH264Packet.m_headerType = RTMP_PACKET_SIZE_LARGE;
    rtmpH264Packet.m_nTimeStamp = /*timeoffset*/h264Body->pts;
    rtmpH264Packet.m_body = rtmpH264Body;
    rtmpH264Packet.m_nBodySize = len + 9;
    
    if (RTMP_IsConnected(rtmp)) {
        if(RTMP_SendPacket(rtmp,&rtmpH264Packet,TRUE)) return 0;
        else return -1;
    }else return -1;
}

int RtmpdumpSender::send_aac_header(MediaPacket *aacHeader)
{
    unsigned char *spec_buf = aacHeader->data;
    int spec_len = aacHeader->size;
    
    unsigned char * body;
    int len;
    
    len = spec_len;  /*spec data长度,一般是2*/
    
    memset(&rtmpAACPacket, 0, sizeof(RTMPPacket));
    memset(rtmpAACBody, 0, MAX_RTMP_AAC_BODY_SIZE);
    
    body = (unsigned char *)rtmpAACBody;
    
    /*AF 00 + AAC RAW data*/
    body[0] = 0xAF;
    body[1] = 0x00;
    memcpy(&body[2],spec_buf,len); /*spec_buf是AAC sequence header数据*/
    
    rtmpAACPacket.m_packetType = RTMP_PACKET_TYPE_AUDIO;
    rtmpAACPacket.m_nBodySize = len+2;
    rtmpAACPacket.m_nChannel = 0x04;
    rtmpAACPacket.m_nTimeStamp = 0;
    rtmpAACPacket.m_hasAbsTimestamp = 0;
    rtmpAACPacket.m_headerType = RTMP_PACKET_SIZE_LARGE;
    rtmpAACPacket.m_nInfoField2 = rtmp->m_stream_id;
    rtmpAACPacket.m_body = rtmpAACBody;
    
    if (RTMP_IsConnected(rtmp)) {
        if(RTMP_SendPacket(rtmp,&rtmpAACPacket,TRUE)) return 0;
        else return -1;
    }else return -1;
}

int RtmpdumpSender::send_aac_data(MediaPacket *aacBody)
{
    unsigned char *buf;
    long len;
    
    buf = aacBody->data;
    len = aacBody->size;
    
    buf += 7;
    len -= 7;
    
    memset(&rtmpAACPacket, 0, sizeof(RTMPPacket));
    memset(rtmpAACBody, 0, MAX_RTMP_AAC_BODY_SIZE);
    
    unsigned char * body;
    body = (unsigned char *)rtmpAACBody;
    
//    long timeoffset;
//    timeoffset = GetNowMs() - startTimeStamp;
    
    /*AF 01 + AAC RAW data*/
    body[0] = 0xAF;
    body[1] = 0x01;
    memcpy(&body[2],buf,len);
    
    rtmpAACPacket.m_packetType = RTMP_PACKET_TYPE_AUDIO;
    rtmpAACPacket.m_nBodySize = len+2;
    rtmpAACPacket.m_nChannel = 0x04;
    rtmpAACPacket.m_nTimeStamp = /*timeoffset*/aacBody->pts;
    rtmpAACPacket.m_hasAbsTimestamp = 0;
    rtmpAACPacket.m_headerType = RTMP_PACKET_SIZE_MEDIUM;
    rtmpAACPacket.m_nInfoField2 = rtmp->m_stream_id;
    rtmpAACPacket.m_body = rtmpAACBody;
    
    if (RTMP_IsConnected(rtmp)) {
        if(RTMP_SendPacket(rtmp,&rtmpAACPacket,TRUE)) return 0;
        else return -1;
    }else return -1;
}