//
//  DlibFaceDetection.h
//  MediaStreamer
//
//  Created by Think on 2019/12/19.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef DlibFaceDetection_h
#define DlibFaceDetection_h

#include <stdio.h>

#include "DlibFaceDetectionCommon.h"

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing.h>
#include <dlib/image_io.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>

class DlibFaceDetection {
public:
    DlibFaceDetection(char* modelFilePath);
    ~DlibFaceDetection();
    
    void processPixelBuffer(char* buffer, int width, int height, std::vector<DlibFaceDetectionRect> rects);
    void processImage(char* inputImagePath, char* outputPath);
private:
    dlib::shape_predictor sp;
    dlib::frontal_face_detector detector;
private:
    static std::vector<dlib::rectangle> convertToDlibRectangle(std::vector<DlibFaceDetectionRect> rects);
};

#endif /* DlibFaceDetection_h */
