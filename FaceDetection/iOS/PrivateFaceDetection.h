//
//  PrivateFaceDetection.h
//  MediaStreamer
//
//  Created by Think on 2019/12/20.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>

NS_ASSUME_NONNULL_BEGIN

@interface PrivateFaceDetection : NSObject

- (instancetype) init;

- (void)initialize;
- (void)initialize:(NSString*)modelFilePath;

- (void)processSampleBuffer:(CMSampleBufferRef)sampleBuffer FaceRects:(NSArray<NSValue *> *)rects;
- (void)processWithInputImage:(NSString*)inputImagePath WithOutputImage:(NSString*)outputImagePath;

- (void)terminate;

@end

NS_ASSUME_NONNULL_END
