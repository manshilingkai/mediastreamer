//
//  PrivateFaceDetection.m
//  MediaStreamer
//
//  Created by Think on 2019/12/20.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "PrivateFaceDetection.h"
#include "DlibFaceDetectionWrapper.h"
#import <UIKit/UIKit.h>

@interface PrivateFaceDetection ()
+ (std::vector<DlibFaceDetectionRect>)convertCGRectValueArray:(NSArray<NSValue *> *)rects;

@end

@implementation PrivateFaceDetection
{
    DlibFaceDetectionWrapper *pDlibFaceDetectionWrapper;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pDlibFaceDetectionWrapper = NULL;
    }
    
    return self;
}

- (void)initialize
{
    NSString* modelFilePath = [[NSBundle mainBundle] pathForResource:@"shape_predictor_68_face_landmarks" ofType:@"dat"];
    pDlibFaceDetectionWrapper = DlibFaceDetection_GetInstance((char*)[modelFilePath UTF8String]);
}

- (void)initialize:(NSString*)modelFilePath
{
    pDlibFaceDetectionWrapper = DlibFaceDetection_GetInstance((char*)[modelFilePath UTF8String]);
}

- (void)processSampleBuffer:(CMSampleBufferRef)sampleBuffer FaceRects:(NSArray<NSValue *> *)rects
{
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    CVPixelBufferLockBaseAddress(imageBuffer, 0);

    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    char *baseBuffer = (char *)CVPixelBufferGetBaseAddress(imageBuffer);
    
    std::vector<DlibFaceDetectionRect> dlibFaceDetectionRects = [PrivateFaceDetection convertCGRectValueArray:rects];
    if (pDlibFaceDetectionWrapper) {
        DlibFaceDetection_processPixelBuffer(pDlibFaceDetectionWrapper, baseBuffer, (int)width, (int)height, dlibFaceDetectionRects);
    }
    
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
}

- (void)processWithInputImage:(NSString*)inputImagePath WithOutputImage:(NSString*)outputImagePath
{
    if (pDlibFaceDetectionWrapper) {
        DlibFaceDetection_processImage(pDlibFaceDetectionWrapper, (char*)[inputImagePath UTF8String], (char*)[outputImagePath UTF8String]);
    }
}

- (void)terminate
{
    if (pDlibFaceDetectionWrapper) {
        DlibFaceDetection_ReleaseInstance(&pDlibFaceDetectionWrapper);
        pDlibFaceDetectionWrapper = NULL;
    }
}

+ (std::vector<DlibFaceDetectionRect>)convertCGRectValueArray:(NSArray<NSValue *> *)rects
{
    std::vector<DlibFaceDetectionRect> myConvertedRects;
    for (NSValue *rectValue in rects) {
        CGRect rect = [rectValue CGRectValue];
        
        DlibFaceDetectionRect myConvertedRect;
        myConvertedRect.x = rect.origin.x;
        myConvertedRect.y = rect.origin.y;
        myConvertedRect.w = rect.size.width;
        myConvertedRect.h = rect.size.height;

        myConvertedRects.push_back(myConvertedRect);
    }
    return myConvertedRects;
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"PrivateFaceDetection dealloc");
}


@end
