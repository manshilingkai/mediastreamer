//
//  DlibFaceDetectionWrapper.h
//  MediaStreamer
//
//  Created by Think on 2019/12/19.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef DlibFaceDetectionWrapper_h
#define DlibFaceDetectionWrapper_h

#include <stdio.h>
#include <vector>

#include "DlibFaceDetectionCommon.h"

struct DlibFaceDetectionWrapper;

#ifdef __cplusplus
extern "C" {
#endif

struct DlibFaceDetectionWrapper *DlibFaceDetection_GetInstance(char* modelFilePath);
void DlibFaceDetection_ReleaseInstance(struct DlibFaceDetectionWrapper **ppInstance);

void DlibFaceDetection_processPixelBuffer(struct DlibFaceDetectionWrapper *pInstance, char* buffer, int width, int height, std::vector<DlibFaceDetectionRect> rects);
void DlibFaceDetection_processImage(struct DlibFaceDetectionWrapper *pInstance, char* inputImagePath, char* outputPath);

#ifdef __cplusplus
};
#endif

#endif /* DlibFaceDetectionWrapper_h */
