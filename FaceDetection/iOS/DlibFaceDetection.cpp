//
//  DlibFaceDetection.cpp
//  MediaStreamer
//
//  Created by Think on 2019/12/19.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "DlibFaceDetection.h"
#include "MediaLog.h"

DlibFaceDetection::DlibFaceDetection(char* modelFilePath)
{
    dlib::deserialize(modelFilePath) >> sp;
    detector = dlib::get_frontal_face_detector();
}

DlibFaceDetection::~DlibFaceDetection()
{
    
}

void DlibFaceDetection::processPixelBuffer(char* buffer, int width, int height, std::vector<DlibFaceDetectionRect> rects)
{
    if (buffer==NULL || width<=0 || height<=0) return;
    
    dlib::array2d<dlib::bgr_pixel> img;

    // set_size expects rows, cols format
    img.set_size(height, width);
    
    // copy PixelBuffer data into dlib image format
    img.reset();
    long position = 0;
    while (img.move_next()) {
        dlib::bgr_pixel& pixel = img.element();

        // assuming bgra format here
        long bufferLocation = position * 4; //(row * width + column) * 4;
        char b = buffer[bufferLocation];
        char g = buffer[bufferLocation + 1];
        char r = buffer[bufferLocation + 2];
        //        we do not need this
        //        char a = baseBuffer[bufferLocation + 3];
        
        dlib::bgr_pixel newpixel(b, g, r);
        pixel = newpixel;
        
        position++;
    }
    
    // convert the face bounds list to dlib format
    std::vector<dlib::rectangle> convertedRectangles = convertToDlibRectangle(rects);
    
    // for every detected face
    for (unsigned long j = 0; j < convertedRectangles.size(); ++j)
    {
        dlib::rectangle oneFaceRect = convertedRectangles[j];
        LOGD("Left : %ld", oneFaceRect.left());
        LOGD("Top : %ld", oneFaceRect.top());
        LOGD("Right : %ld", oneFaceRect.right());
        LOGD("Bottom : %ld", oneFaceRect.bottom());
        LOGD("Width : %ld", oneFaceRect.right() - oneFaceRect.left());
        LOGD("Height : %ld", oneFaceRect.bottom() - oneFaceRect.top());
        
        // detect all landmarks
        dlib::full_object_detection shape = sp(img, oneFaceRect);
        
        // and draw them into the image (samplebuffer)
        for (unsigned long k = 0; k < shape.num_parts(); k++) {
            dlib::point p = shape.part(k);
            draw_solid_circle(img, p, 2, dlib::rgb_pixel(0, 255, 0));
        }
    }
    
    // copy dlib image data back into PixelBuffer
    img.reset();
    position = 0;
    while (img.move_next()) {
        dlib::bgr_pixel& pixel = img.element();
        
        // assuming bgra format here
        long bufferLocation = position * 4; //(row * width + column) * 4;
        buffer[bufferLocation] = pixel.blue;
        buffer[bufferLocation + 1] = pixel.green;
        buffer[bufferLocation + 2] = pixel.red;
        //        we do not need this
        //        char a = baseBuffer[bufferLocation + 3];
        
        position++;
    }
}

void DlibFaceDetection::processImage(char* inputImagePath, char* outputPath)
{/*
    //creat image
    dlib::array2d<dlib::rgb_pixel> img;
    
    //load ios image
    dlib::load_image(img,inputImagePath);
    
    //dlib face detection
    std::vector<dlib::rectangle> dets = detector(img);
    LOGD("Detected Face Num : %ld", dets.size());
    
    for (unsigned long j = 0; j < dets.size(); ++j) {
        dlib::full_object_detection shape = sp(img, dets[j]);
        // and draw them into the image
        for (unsigned long k = 0; k < shape.num_parts(); k++) {
            dlib::point p = shape.part(k);
            dlib::draw_solid_circle(img, p, 2, dlib::rgb_pixel(0, 255, 0));
        }
    }
    dlib::save_jpeg(img, outputPath);
*/}

std::vector<dlib::rectangle> DlibFaceDetection::convertToDlibRectangle(std::vector<DlibFaceDetectionRect> rects)
{
    std::vector<dlib::rectangle> myConvertedRects;
    
    std::vector<DlibFaceDetectionRect>::iterator it;
    for (it = rects.begin(); it!=rects.end(); it++) {
        long left = (*it).x;
        long top = (*it).y;
        long right = left + (*it).w;
        long bottom = top + (*it).h;
        dlib::rectangle dlibRect(left, top, right, bottom);
        
        myConvertedRects.push_back(dlibRect);
    }
    
    return myConvertedRects;
}
