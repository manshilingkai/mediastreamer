//
//  DlibFaceDetectionWrapper.cpp
//  MediaStreamer
//
//  Created by Think on 2019/12/19.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "DlibFaceDetectionWrapper.h"
#include "DlibFaceDetection.h"

#ifdef __cplusplus
extern "C" {
#endif

struct DlibFaceDetectionWrapper{
    DlibFaceDetection* dlibFaceDetection;
    
    DlibFaceDetectionWrapper()
    {
        dlibFaceDetection = NULL;
    }
};

struct DlibFaceDetectionWrapper *DlibFaceDetection_GetInstance(char* modelFilePath)
{
    DlibFaceDetectionWrapper* pInstance = new DlibFaceDetectionWrapper;
    pInstance->dlibFaceDetection = new DlibFaceDetection(modelFilePath);
    
    return pInstance;
}

void DlibFaceDetection_ReleaseInstance(struct DlibFaceDetectionWrapper **ppInstance)
{
    DlibFaceDetectionWrapper* pInstance  = *ppInstance;
    if (pInstance!=NULL) {
        if (pInstance->dlibFaceDetection!=NULL) {
            delete pInstance->dlibFaceDetection;
            pInstance->dlibFaceDetection = NULL;
        }
        
        delete pInstance;
        pInstance = NULL;
    }
}

void DlibFaceDetection_processPixelBuffer(struct DlibFaceDetectionWrapper *pInstance, char* buffer, int width, int height, std::vector<DlibFaceDetectionRect> rects)
{
    if (pInstance!=NULL && pInstance->dlibFaceDetection!=NULL) {
        return pInstance->dlibFaceDetection->processPixelBuffer(buffer, width, height, rects);
    }
}

void DlibFaceDetection_processImage(struct DlibFaceDetectionWrapper *pInstance, char* inputImagePath, char* outputPath)
{
    if (pInstance!=NULL && pInstance->dlibFaceDetection!=NULL) {
        return pInstance->dlibFaceDetection->processImage(inputImagePath, outputPath);
    }
}

#ifdef __cplusplus
};
#endif
