//
//  DlibFaceDetectionCommon.h
//  MediaStreamer
//
//  Created by Think on 2019/12/19.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef DlibFaceDetectionCommon_h
#define DlibFaceDetectionCommon_h

#include <stdio.h>

struct DlibFaceDetectionRect {
    long x;
    long y;
    long w;
    long h;
    
    DlibFaceDetectionRect()
    {
        x = 0;
        y = 0;
        w = 0;
        h = 0;
    }
};

#endif /* DlibFaceDetectionCommon_h */
