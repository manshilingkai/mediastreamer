//
//  AnimatedWebp.cpp
//  MediaStreamer
//
//  Created by Think on 2017/4/5.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "AnimatedWebp.h"
#include "MediaLog.h"

AnimatedWebp::AnimatedWebp(char* filePath)
{
    mFilePath = strdup(filePath);
    
    mCurrentAnimatedWebpPts = 0ll;
    mAnimatedWebpDuration = 0ll;
}

AnimatedWebp::~AnimatedWebp()
{
    if (mFilePath) {
        free(mFilePath);
        mFilePath = NULL;
    }
}

bool AnimatedWebp::loadAnimation()
{
    int ret = ReadAnimatedImage(mFilePath, &mAnimatedImage, 0 , NULL);
    
    if (ret)
    {
        for (int i = 0; i<mAnimatedImage.num_frames; i++) {
            mAnimatedWebpDuration += mAnimatedImage.frames[i].duration;
        }
        
        return true;
    }
    else
    {
        return false;
    }
}

void AnimatedWebp::freeAnimation()
{
    ClearAnimatedImage(&mAnimatedImage);
}

VideoFrame* AnimatedWebp::getAnimatedFrame(int64_t pts)
{
    mImageFrame.width = mAnimatedImage.canvas_width;
    mImageFrame.height = mAnimatedImage.canvas_height;
    int64_t animatedWebpPts = pts%mAnimatedWebpDuration;
    mImageFrame.pts = animatedWebpPts;
    
    int foundIndex = 0;
    for (int i = 0; i<mAnimatedImage.num_frames; i++) {
        animatedWebpPts -= mAnimatedImage.frames[i].duration;
        
        if (animatedWebpPts<=0) {
            foundIndex = i;
            break;
        }
    }
    
    mImageFrame.data = mAnimatedImage.frames[foundIndex].rgba;
    mImageFrame.frameSize = mImageFrame.width * mImageFrame.height;
    mImageFrame.videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
    
    return &mImageFrame;
}
