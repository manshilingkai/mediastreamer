//
//  AnimatedWebp.h
//  MediaStreamer
//
//  Created by Think on 2017/4/5.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef AnimatedWebp_h
#define AnimatedWebp_h

#include <stdio.h>

extern "C"
{
#include "anim_util.h"
}

#include "MediaDataType.h"

class AnimatedWebp {
public:
    AnimatedWebp(char* filePath);
    ~AnimatedWebp();
    
    bool loadAnimation();
    void freeAnimation();
    
    VideoFrame* getAnimatedFrame(int64_t pts);//ms
private:
    char* mFilePath;
    AnimatedImage mAnimatedImage;
    VideoFrame mImageFrame;
    
    int64_t mCurrentAnimatedWebpPts;
    int64_t mAnimatedWebpDuration;
};

#endif /* AnimatedWebp_h */
