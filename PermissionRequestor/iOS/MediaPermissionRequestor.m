//
//  MediaPermissionRequestor.m
//  MediaStreamer
//
//  Created by Think on 2019/12/6.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "MediaPermissionRequestor.h"
#import <AVFoundation/AVFoundation.h>

@implementation MediaPermissionRequestor

+(void)requestCameraPermission:(void (^)(BOOL granted))handler
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_7_0
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusNotDetermined) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (handler) {
                handler(granted);
            }
        }];
        if (handler) {
            handler(NO);
        }
    } else if (authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied) {
        if (handler) {
            handler(NO);
        }
    } else {
        if (handler) {
            handler(YES);
        }
    }
#else
    if (handler) {
        handler(YES);
    }
#endif
}

+(void)requestMicphonePermission:(void (^)(BOOL granted))handler
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_8_0
    AVAudioSessionRecordPermission permissionStatus = [[AVAudioSession sharedInstance] recordPermission];
    if (permissionStatus == AVAudioSessionRecordPermissionUndetermined) {
        [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
            if (handler) {
                handler(granted);
            }
        }];
    } else if (permissionStatus == AVAudioSessionRecordPermissionDenied) {
        if (handler) {
            handler(NO);
        }
    } else {
        if (handler) {
            handler(YES);
        }
    }
#else
    if (handler) {
        handler(YES);
    }
#endif
}

@end
