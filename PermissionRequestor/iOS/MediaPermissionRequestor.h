//
//  MediaPermissionRequestor.h
//  MediaStreamer
//
//  Created by Think on 2019/12/6.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MediaPermissionRequestor : NSObject

+(void)requestCameraPermission:(void (^)(BOOL granted))handler;
+(void)requestMicphonePermission:(void (^)(BOOL granted))handler;

@end
