#include "audio_mixer_manager_mac.h"
#include <unistd.h>  // getpid()
#include "MediaLog.h"

AudioMixerManagerMac::AudioMixerManagerMac()
: _inputDeviceID(kAudioObjectUnknown),
_noInputChannels(0) {
}

AudioMixerManagerMac::~AudioMixerManagerMac() {
    Close();
}

// ============================================================================
//	                                PUBLIC METHODS
// ============================================================================

int32_t AudioMixerManagerMac::Close() {
    CloseMicrophone();
    
    return 0;
}

int32_t AudioMixerManagerMac::CloseMicrophone() {
    
    _inputDeviceID = kAudioObjectUnknown;
    _noInputChannels = 0;
    
    return 0;
}

int32_t AudioMixerManagerMac::OpenMicrophone(AudioDeviceID deviceID) {
    
    OSStatus err = noErr;
    UInt32 size = 0;
    pid_t hogPid = -1;
    
    _inputDeviceID = deviceID;
    
    // Check which process, if any, has hogged the device.
    AudioObjectPropertyAddress propertyAddress = {
        kAudioDevicePropertyHogMode, kAudioDevicePropertyScopeInput, 0};
    size = sizeof(hogPid);
    
    err = AudioObjectGetPropertyData(
                                     _inputDeviceID, &propertyAddress, 0, NULL, &size, &hogPid);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    if (hogPid == -1) {
        LOGI("No process has hogged the input device");
    }
    // getpid() is apparently "always successful"
    else if (hogPid == getpid()) {
        LOGI("Our process has hogged the input device");
    } else {
        LOGW("Another process (pid = %d) has hogged the input device",
             static_cast<int>(hogPid));
        
        return -1;
    }
    
    // get number of channels from stream format
    propertyAddress.mSelector = kAudioDevicePropertyStreamFormat;
    
    // Get the stream format, to be able to read the number of channels.
    AudioStreamBasicDescription streamFormat;
    size = sizeof(AudioStreamBasicDescription);
    memset(&streamFormat, 0, size);
    err = AudioObjectGetPropertyData(
                                     _inputDeviceID, &propertyAddress, 0, NULL, &size, &streamFormat);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    _noInputChannels = streamFormat.mChannelsPerFrame;
    
    return 0;
}

bool AudioMixerManagerMac::MicrophoneIsInitialized() const {
    return (_inputDeviceID != kAudioObjectUnknown);
}

int32_t AudioMixerManagerMac::StereoRecordingIsAvailable(bool& available) {
    if (_inputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }
    
    available = (_noInputChannels == 2);
    return 0;
}

int32_t AudioMixerManagerMac::MicrophoneMuteIsAvailable(bool& available) {
    if (_inputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }
    
    OSStatus err = noErr;
    
    // Does the capture device have a master mute control?
    // If so, use it exclusively.
    AudioObjectPropertyAddress propertyAddress = {
        kAudioDevicePropertyMute, kAudioDevicePropertyScopeInput, 0};
    Boolean isSettable = false;
    err = AudioObjectIsPropertySettable(_inputDeviceID, &propertyAddress,
                                        &isSettable);
    if (err == noErr && isSettable) {
        available = true;
        return 0;
    }
    
    // Otherwise try to set each channel.
    for (UInt32 i = 1; i <= _noInputChannels; i++) {
        propertyAddress.mElement = i;
        isSettable = false;
        err = AudioObjectIsPropertySettable(_inputDeviceID, &propertyAddress,
                                            &isSettable);
        if (err != noErr || !isSettable) {
            available = false;
            LOGW("Mute cannot be set for output channel %d, err=%d", i, err);
            return -1;
        }
    }
    
    available = true;
    return 0;
}

int32_t AudioMixerManagerMac::SetMicrophoneMute(bool enable) {
    if (_inputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }
    
    OSStatus err = noErr;
    UInt32 size = 0;
    UInt32 mute = enable ? 1 : 0;
    bool success = false;
    
    // Does the capture device have a master mute control?
    // If so, use it exclusively.
    AudioObjectPropertyAddress propertyAddress = {
        kAudioDevicePropertyMute, kAudioDevicePropertyScopeInput, 0};
    Boolean isSettable = false;
    err = AudioObjectIsPropertySettable(_inputDeviceID, &propertyAddress,
                                        &isSettable);
    if (err == noErr && isSettable) {
        size = sizeof(mute);
        err = AudioObjectSetPropertyData(
                                         _inputDeviceID, &propertyAddress, 0, NULL, size, &mute);
        if (err != noErr) {
            LOGE("[%s:%s:%d] AudioObjectSetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        
        return 0;
    }
    
    // Otherwise try to set each channel.
    for (UInt32 i = 1; i <= _noInputChannels; i++) {
        propertyAddress.mElement = i;
        isSettable = false;
        err = AudioObjectIsPropertySettable(_inputDeviceID, &propertyAddress,
                                            &isSettable);
        if (err == noErr && isSettable) {
            size = sizeof(mute);
            err = AudioObjectSetPropertyData(
                                             _inputDeviceID, &propertyAddress, 0, NULL, size, &mute);
            if (err != noErr) {
                LOGE("[%s:%s:%d] AudioObjectSetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
                return -1;
            }else {
                success = true;
            }
        }
    }
    
    if (!success) {
        LOGW("Unable to set mute on any input channel");
        return -1;
    }
    
    return 0;
}

int32_t AudioMixerManagerMac::MicrophoneMute(bool& enabled) const {
    if (_inputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }
    
    OSStatus err = noErr;
    UInt32 size = 0;
    unsigned int channels = 0;
    UInt32 channelMuted = 0;
    UInt32 muted = 0;
    
    // Does the device have a master volume control?
    // If so, use it exclusively.
    AudioObjectPropertyAddress propertyAddress = {
        kAudioDevicePropertyMute, kAudioDevicePropertyScopeInput, 0};
    Boolean hasProperty =
    AudioObjectHasProperty(_inputDeviceID, &propertyAddress);
    if (hasProperty) {
        size = sizeof(muted);
        err = AudioObjectGetPropertyData(
                                         _inputDeviceID, &propertyAddress, 0, NULL, &size, &muted);
        
        if (err != noErr) {
            LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        
        // 1 means muted
        enabled = static_cast<bool>(muted);
    } else {
        muted = 1;
        // Otherwise check if all channels are muted.
        for (UInt32 i = 1; i <= _noInputChannels; i++) {
            channelMuted = 0;
            propertyAddress.mElement = i;
            hasProperty = AudioObjectHasProperty(_inputDeviceID, &propertyAddress);
            if (hasProperty) {
                size = sizeof(channelMuted);
                err = AudioObjectGetPropertyData(
                                                 _inputDeviceID, &propertyAddress, 0, NULL, &size, &channelMuted);
                if (err != noErr) {
                    LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
                    return -1;
                }
                
                muted = (muted && channelMuted);
                channels++;
            }
        }
        
        if (channels == 0) {
            LOGW("Unable to get mute for any channel");
            return -1;
        }
        
        assert(channels > 0);
        // 1 means muted
        enabled = static_cast<bool>(muted);
    }
    
    return 0;
}

int32_t AudioMixerManagerMac::MicrophoneVolumeIsAvailable(bool& available) {
    if (_inputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }
    
    OSStatus err = noErr;
    
    // Does the capture device have a master volume control?
    // If so, use it exclusively.
    AudioObjectPropertyAddress propertyAddress = {
        kAudioDevicePropertyVolumeScalar, kAudioDevicePropertyScopeInput, 0};
    Boolean isSettable = false;
    err = AudioObjectIsPropertySettable(_inputDeviceID, &propertyAddress,
                                        &isSettable);
    if (err == noErr && isSettable) {
        available = true;
        return 0;
    }
    
    // Otherwise try to set each channel.
    for (UInt32 i = 1; i <= _noInputChannels; i++) {
        propertyAddress.mElement = i;
        isSettable = false;
        err = AudioObjectIsPropertySettable(_inputDeviceID, &propertyAddress,
                                            &isSettable);
        if (err != noErr || !isSettable) {
            available = false;
            LOGW(" Volume cannot be set for input channel %d, err=%d", i, err);
            return -1;
        }
    }
    
    available = true;
    return 0;
}

int32_t AudioMixerManagerMac::SetMicrophoneVolume(uint32_t volume) {
    if (_inputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }
    
    OSStatus err = noErr;
    UInt32 size = 0;
    bool success = false;
    
    // volume range is 0.0 - 1.0, convert from 0 - 255
    const Float32 vol = (Float32)(volume / 255.0);
    
    assert(vol <= 1.0 && vol >= 0.0);
    
    // Does the capture device have a master volume control?
    // If so, use it exclusively.
    AudioObjectPropertyAddress propertyAddress = {
        kAudioDevicePropertyVolumeScalar, kAudioDevicePropertyScopeInput, 0};
    Boolean isSettable = false;
    err = AudioObjectIsPropertySettable(_inputDeviceID, &propertyAddress,
                                        &isSettable);
    if (err == noErr && isSettable) {
        size = sizeof(vol);
        
        err = AudioObjectSetPropertyData(_inputDeviceID, &propertyAddress, 0, NULL, size, &vol);
        
        if (err != noErr) {
            LOGE("[%s:%s:%d] AudioObjectSetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        
        return 0;
    }
    
    // Otherwise try to set each channel.
    for (UInt32 i = 1; i <= _noInputChannels; i++) {
        propertyAddress.mElement = i;
        isSettable = false;
        err = AudioObjectIsPropertySettable(_inputDeviceID, &propertyAddress,
                                            &isSettable);
        if (err == noErr && isSettable) {
            size = sizeof(vol);
            
            err = AudioObjectSetPropertyData(_inputDeviceID, &propertyAddress, 0, NULL, size, &vol);
            
            if (err != noErr) {
                LOGE("[%s:%s:%d] AudioObjectSetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
                return -1;
            }else {
                success = true;
            }
        }
    }
    
    if (!success) {
        LOGW("Unable to set a level on any input channel");
        return -1;
    }
    
    return 0;
}

int32_t AudioMixerManagerMac::MicrophoneVolume(uint32_t& volume) const {
    if (_inputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }
    
    OSStatus err = noErr;
    UInt32 size = 0;
    unsigned int channels = 0;
    Float32 channelVol = 0;
    Float32 volFloat32 = 0;
    
    // Does the device have a master volume control?
    // If so, use it exclusively.
    AudioObjectPropertyAddress propertyAddress = {
        kAudioDevicePropertyVolumeScalar, kAudioDevicePropertyScopeInput, 0};
    Boolean hasProperty =
    AudioObjectHasProperty(_inputDeviceID, &propertyAddress);
    if (hasProperty) {
        size = sizeof(volFloat32);
        err = AudioObjectGetPropertyData(
                                         _inputDeviceID, &propertyAddress, 0, NULL, &size, &volFloat32);
        if (err != noErr) {
            LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        
        // vol 0.0 to 1.0 -> convert to 0 - 255
        volume = static_cast<uint32_t>(volFloat32 * 255 + 0.5);
    } else {
        // Otherwise get the average volume across channels.
        volFloat32 = 0;
        for (UInt32 i = 1; i <= _noInputChannels; i++) {
            channelVol = 0;
            propertyAddress.mElement = i;
            hasProperty = AudioObjectHasProperty(_inputDeviceID, &propertyAddress);
            if (hasProperty) {
                size = sizeof(channelVol);
                err = AudioObjectGetPropertyData(
                                                 _inputDeviceID, &propertyAddress, 0, NULL, &size, &channelVol);
                if (err != noErr) {
                    LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
                    return -1;
                }
                
                volFloat32 += channelVol;
                channels++;
            }
        }
        
        if (channels == 0) {
            LOGW("Unable to get a level on any channel");
            return -1;
        }
        
        assert(channels > 0);
        // vol 0.0 to 1.0 -> convert to 0 - 255
        volume = static_cast<uint32_t>(255 * volFloat32 / channels + 0.5);
    }
    
    return 0;
}

int32_t AudioMixerManagerMac::MaxMicrophoneVolume(uint32_t& maxVolume) const {
    if (_inputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }
    
    // volume range is 0.0 to 1.0
    // we convert that to 0 - 255
    maxVolume = 255;
    
    return 0;
}

int32_t AudioMixerManagerMac::MinMicrophoneVolume(uint32_t& minVolume) const {
    if (_inputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }
    
    // volume range is 0.0 to 1.0
    // we convert that to 0 - 10
    minVolume = 0;
    
    return 0;
}

int32_t AudioMixerManagerMac::MicrophoneVolumeStepSize(
                                                       uint16_t& stepSize) const {
    if (_inputDeviceID == kAudioObjectUnknown) {
        LOGW("device ID has not been set");
        return -1;
    }
    
    // volume range is 0.0 to 1.0
    // we convert that to 0 - 10
    stepSize = 1;
    
    return 0;
}
// EOF
