#include "audio_device_mac.h"
#include "pa_ringbuffer.h"

#include <ApplicationServices/ApplicationServices.h>
#include <libkern/OSAtomic.h>  // OSAtomicCompareAndSwap()
#include <mach/mach.h>         // mach_task_self()
#include <sys/sysctl.h>        // sysctlbyname()

#include "MediaLog.h"
#include "MediaTimer.h"

enum { MaxNumberDevices = 64 };

void AudioDeviceMac::AtomicSet32(int32_t* theValue, int32_t newValue) {
    while (1) {
        int32_t oldValue = *theValue;
        if (OSAtomicCompareAndSwap32Barrier(oldValue, newValue, theValue) == true) {
            return;
        }
    }
}

int32_t AudioDeviceMac::AtomicGet32(int32_t* theValue) {
    while (1) {
        int32_t value = *theValue;
        if (OSAtomicCompareAndSwap32Barrier(value, value, theValue) == true) {
            return value;
        }
    }
}

int AudioDeviceMac::isLittleEndian()
{
    union w
    {
        int a;
        char b;
    }c;
    c.a = 1;
    return (c.b == 1);
}

AudioDeviceMac::AudioDeviceMac(int sampleRate, int numChannels)
: _mixerManager(),
_inputDeviceIndex(0),
_inputDeviceID(kAudioObjectUnknown),
_inputDeviceIsSpecified(false),
_captureBufData(NULL),
_initialized(false),
_isShutDown(false),
_recording(false),
_recIsInitialized(false),
_AGC(false),
_captureDeviceIsAlive(1),
_macBookPro(false),
_macBookProPanRight(false),
_captureLatencyUs(0),
_captureDelayUs(0),
_paCaptureBuffer(NULL),
_captureBufSizeSamples(0),
get_mic_volume_counter_ms_(0) {
    memset(&_inStreamFormat, 0, sizeof(AudioStreamBasicDescription));
    memset(&_inDesiredFormat, 0, sizeof(AudioStreamBasicDescription));
    
    _sampleRate = sampleRate;
    _recChannels = numChannels;
    
    _recBufSizeInBytes = 0;
}

AudioDeviceMac::~AudioDeviceMac() {
    
    if (!_isShutDown) {
        Terminate();
    }
    
    if (_paCaptureBuffer) {
        delete _paCaptureBuffer;
        _paCaptureBuffer = NULL;
    }
    
    if (_captureBufData) {
        delete[] _captureBufData;
        _captureBufData = NULL;
    }
}

void AudioDeviceMac::setRecBufSizeInBytes(int bufSizeInBytes)
{
    _recBufSizeInBytes = bufSizeInBytes;
}

// ============================================================================
//                                     API
// ============================================================================

int AudioDeviceMac::Init() {
    if (_initialized) {
        return 0;
    }
    
    OSStatus err = noErr;
    
    _isShutDown = false;
    
    // PortAudio ring buffers require an elementCount which is a power of two.
    
    if (_captureBufData == NULL) {
        UInt32 powerOfTwo = 1;
        while (powerOfTwo < REC_BUF_SIZE_IN_SAMPLES) {
            powerOfTwo <<= 1;
        }
        _captureBufSizeSamples = powerOfTwo;
        _captureBufData = new Float32[_captureBufSizeSamples];
    }
    
    if (_paCaptureBuffer == NULL) {
        _paCaptureBuffer = new PaUtilRingBuffer;
        PaRingBufferSize bufSize = -1;
        bufSize = PaUtil_InitializeRingBuffer(_paCaptureBuffer, sizeof(Float32),
                                    _captureBufSizeSamples, _captureBufData);
        if (bufSize == -1) {
            LOGE("PaUtil_InitializeRingBuffer() error");
            return -1;
        }
    }
    
    // Setting RunLoop to NULL here instructs HAL to manage its own thread for
    // notifications. This was the default behaviour on OS X 10.5 and earlier,
    // but now must be explicitly specified. HAL would otherwise try to use the
    // main thread to issue notifications.
    AudioObjectPropertyAddress propertyAddress = {
        kAudioHardwarePropertyRunLoop, kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster};
    CFRunLoopRef runLoop = NULL;
    UInt32 size = sizeof(CFRunLoopRef);
    err = AudioObjectSetPropertyData(kAudioObjectSystemObject, &propertyAddress, 0, NULL, size, &runLoop);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectSetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    // Listen for any device changes.
    propertyAddress.mSelector = kAudioHardwarePropertyDevices;
    err = AudioObjectAddPropertyListener(kAudioObjectSystemObject, &propertyAddress, &objectListenerProc, this);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectSetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    
    // Determine if this is a MacBook Pro
    _macBookPro = false;
    _macBookProPanRight = false;
    char buf[128];
    size_t length = sizeof(buf);
    memset(buf, 0, length);
    
    int intErr = sysctlbyname("hw.model", buf, &length, NULL, 0);
    if (intErr != 0) {
        LOGE("Error in sysctlbyname(): %d", err);
    } else {
        LOGI("Hardware model: %s", buf);
        if (strncmp(buf, "MacBookPro", 10) == 0) {
            _macBookPro = true;
        }
    }
    
    get_mic_volume_counter_ms_ = 0;
    
    mAudioFrame.data = (uint8_t*)malloc(_recBufSizeInBytes);
    mAudioFrame.frameSize = _recBufSizeInBytes;
    mAudioFrame.pts = 0;
    mAudioFrame.duration = 0;
    
    _initialized = true;
    
    return 0;
}

int AudioDeviceMac::Terminate() {
    if (!_initialized) {
        return 0;
    }
    
    if (_recording) {
        LOGE("Recording must be stopped");
        return -1;
    }
    
    _mixerManager.Close();
    
    OSStatus err = noErr;
    int retVal = 0;
    
    AudioObjectPropertyAddress propertyAddress = {
        kAudioHardwarePropertyDevices, kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster};
    err = AudioObjectRemovePropertyListener(
                                            kAudioObjectSystemObject, &propertyAddress, &objectListenerProc, this);
    if (err != noErr) {
        LOGW("[%s:%s:%d] AudioObjectRemovePropertyListener return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    
    err = AudioHardwareUnload();
    if (err != noErr) {
        LOGE("Error in AudioHardwareUnload() : %d", err);
        retVal = -1;
    }
    
    if(mAudioFrame.data!=NULL)
    {
        free(mAudioFrame.data);
        mAudioFrame.data = NULL;
    }
    
    _isShutDown = true;
    _initialized = false;
    _inputDeviceIsSpecified = false;
    
    return retVal;
}

bool AudioDeviceMac::Initialized() {
    return (_initialized);
}

int32_t AudioDeviceMac::MicrophoneIsAvailable(bool& available) {
    bool wasInitialized = _mixerManager.MicrophoneIsInitialized();
    
    // Make an attempt to open up the
    // input mixer corresponding to the currently selected output device.
    //
    if (!wasInitialized && InitMicrophone() == -1) {
        available = false;
        return 0;
    }
    
    // Given that InitMicrophone was successful, we know that a valid microphone
    // exists.
    available = true;
    
    // Close the initialized input mixer
    //
    if (!wasInitialized) {
        _mixerManager.CloseMicrophone();
    }
    
    return 0;
}

int32_t AudioDeviceMac::InitMicrophone() {
    if (_recording) {
        return -1;
    }
    
    if (InitDevice(_inputDeviceIndex, _inputDeviceID, true) == -1) {
        return -1;
    }
    
    if (_mixerManager.OpenMicrophone(_inputDeviceID) == -1) {
        return -1;
    }
    
    return 0;
}

bool AudioDeviceMac::MicrophoneIsInitialized() const {
    return (_mixerManager.MicrophoneIsInitialized());
}

//todo
int32_t AudioDeviceMac::MicrophoneMuteIsAvailable(bool& available) {
    bool isAvailable(false);
    bool wasInitialized = _mixerManager.MicrophoneIsInitialized();
    
    // Make an attempt to open up the
    // input mixer corresponding to the currently selected input device.
    //
    if (!wasInitialized && InitMicrophone() == -1) {
        // If we end up here it means that the selected microphone has no volume
        // control, hence it is safe to state that there is no boost control
        // already at this stage.
        available = false;
        return 0;
    }
    
    // Check if the selected microphone has a mute control
    //
    _mixerManager.MicrophoneMuteIsAvailable(isAvailable);
    available = isAvailable;
    
    // Close the initialized input mixer
    //
    if (!wasInitialized) {
        _mixerManager.CloseMicrophone();
    }
    
    return 0;
}

int32_t AudioDeviceMac::SetMicrophoneMute(bool enable) {
    return (_mixerManager.SetMicrophoneMute(enable));
}

int32_t AudioDeviceMac::MicrophoneMute(bool& enabled) const {
    bool muted(0);
    
    if (_mixerManager.MicrophoneMute(muted) == -1) {
        return -1;
    }
    
    enabled = muted;
    return 0;
}

int32_t AudioDeviceMac::StereoRecordingIsAvailable(bool& available) {
    bool isAvailable(false);
    bool wasInitialized = _mixerManager.MicrophoneIsInitialized();
    
    if (!wasInitialized && InitMicrophone() == -1) {
        // Cannot open the specified device
        available = false;
        return 0;
    }
    
    // Check if the selected microphone can record stereo
    //
    _mixerManager.StereoRecordingIsAvailable(isAvailable);
    available = isAvailable;
    
    // Close the initialized input mixer
    //
    if (!wasInitialized) {
        _mixerManager.CloseMicrophone();
    }
    
    return 0;
}

int32_t AudioDeviceMac::SetStereoRecording(bool enable) {
    if (enable)
        _recChannels = 2;
    else
        _recChannels = 1;
    
    return 0;
}

int32_t AudioDeviceMac::StereoRecording(bool& enabled) const {
    if (_recChannels == 2)
        enabled = true;
    else
        enabled = false;
    
    return 0;
}

int32_t AudioDeviceMac::SetAGC(bool enable) {
    _AGC = enable;
    
    return 0;
}

bool AudioDeviceMac::AGC() const {
    return _AGC;
}

int32_t AudioDeviceMac::MicrophoneVolumeIsAvailable(bool& available) {
    bool wasInitialized = _mixerManager.MicrophoneIsInitialized();
    
    // Make an attempt to open up the
    // input mixer corresponding to the currently selected output device.
    //
    if (!wasInitialized && InitMicrophone() == -1) {
        // If we end up here it means that the selected microphone has no volume
        // control.
        available = false;
        return 0;
    }
    
    // Given that InitMicrophone was successful, we know that a volume control
    // exists
    //
    available = true;
    
    // Close the initialized input mixer
    //
    if (!wasInitialized) {
        _mixerManager.CloseMicrophone();
    }
    
    return 0;
}

int32_t AudioDeviceMac::SetMicrophoneVolume(uint32_t volume) {
    return (_mixerManager.SetMicrophoneVolume(volume));
}

int32_t AudioDeviceMac::MicrophoneVolume(uint32_t& volume) const {
    uint32_t level(0);
    
    if (_mixerManager.MicrophoneVolume(level) == -1) {
        LOGW("failed to retrive current microphone level");
        return -1;
    }
    
    volume = level;
    return 0;
}

int32_t AudioDeviceMac::MaxMicrophoneVolume(uint32_t& maxVolume) const {
    uint32_t maxVol(0);
    
    if (_mixerManager.MaxMicrophoneVolume(maxVol) == -1) {
        return -1;
    }
    
    maxVolume = maxVol;
    return 0;
}

int32_t AudioDeviceMac::MinMicrophoneVolume(uint32_t& minVolume) const {
    uint32_t minVol(0);
    
    if (_mixerManager.MinMicrophoneVolume(minVol) == -1) {
        return -1;
    }
    
    minVolume = minVol;
    return 0;
}

int32_t AudioDeviceMac::MicrophoneVolumeStepSize(uint16_t& stepSize) const {
    uint16_t delta(0);
    
    if (_mixerManager.MicrophoneVolumeStepSize(delta) == -1) {
        return -1;
    }
    
    stepSize = delta;
    return 0;
}

int32_t AudioDeviceMac::RecordingDeviceName(uint16_t index, char name[kAdmMaxDeviceNameSize]) {
    const uint16_t nDevices(RecordingDevices());
    
    if ((index > (nDevices - 1)) || (name == NULL)) {
        return -1;
    }
    
    memset(name, 0, kAdmMaxDeviceNameSize);
    
    return GetDeviceName(kAudioDevicePropertyScopeInput, index, name);
}

int16_t AudioDeviceMac::RecordingDevices() {
    AudioDeviceID recDevices[MaxNumberDevices];
    return GetNumberDevices(kAudioDevicePropertyScopeInput, recDevices,
                            MaxNumberDevices);
}

int32_t AudioDeviceMac::SetRecordingDevice(uint16_t index) {
    if (_recIsInitialized) {
        return -1;
    }
    
    AudioDeviceID recDevices[MaxNumberDevices];
    uint32_t nDevices = GetNumberDevices(kAudioDevicePropertyScopeInput,
                                         recDevices, MaxNumberDevices);
    
    LOGI("number of availiable waveform-audio input devices is %u", nDevices);
    
    if (index > (nDevices - 1)) {
        LOGE("device index is out of range [0,%u]", (nDevices - 1));
        return -1;
    }
    
    _inputDeviceIndex = index;
    _inputDeviceIsSpecified = true;
    
    return 0;
}

int32_t AudioDeviceMac::RecordingIsAvailable(bool& available) {
    available = true;
    
    // Try to initialize the recording side
    if (InitRecording() == -1) {
        available = false;
    }
    
    // We destroy the IOProc created by InitRecording() in implInDeviceIOProc().
    // We must actually start recording here in order to have the IOProc
    // deleted by calling StopRecording().
    if (StartRecording() == -1) {
        available = false;
    }
    
    // Cancel effect of initialization
    if (StopRecording() == -1) {
        available = false;
    }
    
    return 0;
}

int32_t AudioDeviceMac::InitRecording() {
    if (_recording) {
        return -1;
    }
    
    if (!_inputDeviceIsSpecified) {
        return -1;
    }
    
    if (_recIsInitialized) {
        return 0;
    }
    
    // Initialize the microphone (devices might have been added or removed)
    if (InitMicrophone() == -1) {
        LOGW("InitMicrophone() failed");
    }
    
    OSStatus err = noErr;
    UInt32 size = 0;
    
    PaUtil_FlushRingBuffer(_paCaptureBuffer);
    
    _captureDelayUs = 0;
    _captureLatencyUs = 0;
    _captureDeviceIsAlive = 1;
    
    // Get current stream description
    AudioObjectPropertyAddress propertyAddress = {
        kAudioDevicePropertyStreamFormat, kAudioDevicePropertyScopeInput, 0};
    memset(&_inStreamFormat, 0, sizeof(_inStreamFormat));
    size = sizeof(_inStreamFormat);
    err = AudioObjectGetPropertyData(_inputDeviceID, &propertyAddress, 0, NULL, &size, &_inStreamFormat);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    if (_inStreamFormat.mFormatID != kAudioFormatLinearPCM) {
        LOGE("Unacceptable input stream format -> mFormatID : %d", _inStreamFormat.mFormatID);
        return -1;
    }
    
    if (_inStreamFormat.mChannelsPerFrame > N_DEVICE_CHANNELS) {
        LOGE("Too many channels on input device (mChannelsPerFrame = %d)",
             _inStreamFormat.mChannelsPerFrame);
        return -1;
    }
    
    const int io_block_size_samples = _inStreamFormat.mChannelsPerFrame *
    _inStreamFormat.mSampleRate / 100 * N_BLOCKS_IO;
    if (io_block_size_samples > _captureBufSizeSamples) {
        LOGE("Input IO block size (%d) is larger than ring buffer (%u)",
             io_block_size_samples, _captureBufSizeSamples);
        return -1;
    }
    
    LOGI("Input stream format:");
    LOGI("mSampleRate = %f, mChannelsPerFrame = %u", _inStreamFormat.mSampleRate, _inStreamFormat.mChannelsPerFrame);
    LOGI("mBytesPerPacket = %u, mFramesPerPacket = %u", _inStreamFormat.mBytesPerPacket, _inStreamFormat.mFramesPerPacket);
    LOGI("mBytesPerFrame = %u, mBitsPerChannel = %u", _inStreamFormat.mBytesPerFrame, _inStreamFormat.mBitsPerChannel);
    LOGI("mFormatFlags = %u", _inStreamFormat.mFormatFlags);
    LOGI("mFormatID = %u",_inStreamFormat.mFormatID);
    
    // Our preferred format to work with
    if (_inStreamFormat.mChannelsPerFrame >= 2 && (_recChannels == 2)) {
        _inDesiredFormat.mChannelsPerFrame = 2;
    } else {
        // Disable stereo recording when we only have one channel on the device.
        _inDesiredFormat.mChannelsPerFrame = 1;
        _recChannels = 1;
        LOGI("Stereo recording unavailable on this device");
    }
    
    _inDesiredFormat.mSampleRate = /*N_REC_SAMPLES_PER_SEC*/_sampleRate;
    _inDesiredFormat.mBytesPerPacket =
    _inDesiredFormat.mChannelsPerFrame * sizeof(SInt16);
    _inDesiredFormat.mFramesPerPacket = 1;
    _inDesiredFormat.mBytesPerFrame =
    _inDesiredFormat.mChannelsPerFrame * sizeof(SInt16);
    _inDesiredFormat.mBitsPerChannel = sizeof(SInt16) * 8;
    
    _inDesiredFormat.mFormatFlags =
    kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
    if (!isLittleEndian()) {
        _inDesiredFormat.mFormatFlags |= kLinearPCMFormatFlagIsBigEndian;
    }

    _inDesiredFormat.mFormatID = kAudioFormatLinearPCM;
    
    err = AudioConverterNew(&_inStreamFormat, &_inDesiredFormat, &_captureConverter);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioConverterNew return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    // First try to set buffer size to desired value (10 ms * N_BLOCKS_IO)
    // TODO(xians): investigate this block.
    UInt32 bufByteCount =
    (UInt32)((_inStreamFormat.mSampleRate / 1000.0) * 10.0 * N_BLOCKS_IO *
             _inStreamFormat.mChannelsPerFrame * sizeof(Float32));
    if (_inStreamFormat.mFramesPerPacket != 0) {
        if (bufByteCount % _inStreamFormat.mFramesPerPacket != 0) {
            bufByteCount =
            ((UInt32)(bufByteCount / _inStreamFormat.mFramesPerPacket) + 1) *
            _inStreamFormat.mFramesPerPacket;
        }
    }
    
    // Ensure the buffer size is within the acceptable range provided by the
    // device.
    propertyAddress.mSelector = kAudioDevicePropertyBufferSizeRange;
    AudioValueRange range;
    size = sizeof(range);
    err = AudioObjectGetPropertyData(_inputDeviceID, &propertyAddress, 0, NULL, &size, &range);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    if (range.mMinimum > bufByteCount) {
        bufByteCount = range.mMinimum;
    } else if (range.mMaximum < bufByteCount) {
        bufByteCount = range.mMaximum;
    }
    
    propertyAddress.mSelector = kAudioDevicePropertyBufferSize;
    size = sizeof(bufByteCount);
    err = AudioObjectSetPropertyData(_inputDeviceID, &propertyAddress, 0, NULL, size, &bufByteCount);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectSetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    // Get capture device latency
    propertyAddress.mSelector = kAudioDevicePropertyLatency;
    UInt32 latency = 0;
    size = sizeof(UInt32);
    err = AudioObjectGetPropertyData(_inputDeviceID, &propertyAddress, 0, NULL, &size, &latency);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    _captureLatencyUs = (UInt32)((1.0e6 * latency) / _inStreamFormat.mSampleRate);
    
    // Get capture stream latency
    propertyAddress.mSelector = kAudioDevicePropertyStreams;
    AudioStreamID stream = 0;
    size = sizeof(AudioStreamID);
    err = AudioObjectGetPropertyData(_inputDeviceID, &propertyAddress, 0, NULL, &size, &stream);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    propertyAddress.mSelector = kAudioStreamPropertyLatency;
    size = sizeof(UInt32);
    latency = 0;
    err = AudioObjectGetPropertyData(_inputDeviceID, &propertyAddress, 0, NULL, &size, &latency);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    _captureLatencyUs +=
    (UInt32)((1.0e6 * latency) / _inStreamFormat.mSampleRate);
    
    // Listen for format changes
    // TODO(xians): should we be using kAudioDevicePropertyDeviceHasChanged?
    propertyAddress.mSelector = kAudioDevicePropertyStreamFormat;
    err = AudioObjectAddPropertyListener(_inputDeviceID, &propertyAddress, &objectListenerProc, this);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectAddPropertyListener return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    // Listen for processor overloads
    propertyAddress.mSelector = kAudioDeviceProcessorOverload;
    err = AudioObjectAddPropertyListener(_inputDeviceID, &propertyAddress, &objectListenerProc, this);
    if (err != noErr) {
        LOGW("[%s:%s:%d] AudioObjectAddPropertyListener return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    
    err = AudioDeviceCreateIOProcID(_inputDeviceID, inDeviceIOProc, this, &_inDeviceIOProcID);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioDeviceCreateIOProcID return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    // Mark recording side as initialized
    _recIsInitialized = true;
    
    return 0;
}

int AudioDeviceMac::StartRecording() {
    
    int32_t ret = SetRecordingDevice(0);
    if (ret) {
        LOGE("SetRecordingDevice Fail");
        return -1;
    }
    
    ret = InitRecording();
    if (ret) {
        LOGE("InitRecording Fail");
        return -1;
    }
    
    if (!_recIsInitialized) {
        return -1;
    }
    
    if (_recording) {
        return 0;
    }
    
    if (!_initialized) {
        return -1;
    }
    
    OSStatus err = noErr;
    err = AudioDeviceStart(_inputDeviceID, _inDeviceIOProcID);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioDeviceStart return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    
    _recording = true;
    
    return 0;
}

int AudioDeviceMac::StopRecording() {
    if (!_recIsInitialized) {
        return 0;
    }
    
    OSStatus err = noErr;
    
    // Stop device
    int32_t captureDeviceIsAlive = AtomicGet32(&_captureDeviceIsAlive);
    if (_recording && captureDeviceIsAlive == 1) {
        _recording = false;
        
        err = AudioDeviceStop(_inputDeviceID, _inDeviceIOProcID);
        if (err != noErr) {
            LOGW("[%s:%s:%d] AudioDeviceStop return %d", __FILE__,__FUNCTION__,__LINE__,err);
        }
        err = AudioDeviceDestroyIOProcID(_inputDeviceID, _inDeviceIOProcID);
        if (err != noErr) {
            LOGW("[%s:%s:%d] AudioDeviceDestroyIOProcID return %d", __FILE__,__FUNCTION__,__LINE__,err);
        }
        
        LOGD("Recording stopped");
    }
    
    // Setting this signal will allow the worker thread to be stopped.
    AtomicSet32(&_captureDeviceIsAlive, 0);
    
    err = AudioConverterDispose(_captureConverter);
    if (err != noErr) {
        LOGW("[%s:%s:%d] AudioConverterDispose return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    
    // Remove listeners.
    AudioObjectPropertyAddress propertyAddress = {
        kAudioDevicePropertyStreamFormat, kAudioDevicePropertyScopeInput, 0};
    err = AudioObjectRemovePropertyListener(_inputDeviceID, &propertyAddress, &objectListenerProc, this);
    if (err != noErr) {
        LOGW("[%s:%s:%d] AudioObjectRemovePropertyListener return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    propertyAddress.mSelector = kAudioDeviceProcessorOverload;
    err = AudioObjectRemovePropertyListener(_inputDeviceID, &propertyAddress, &objectListenerProc, this);
    if (err != noErr) {
        LOGW("[%s:%s:%d] AudioObjectRemovePropertyListener return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    _recIsInitialized = false;
    _recording = false;
    
    return 0;
}

bool AudioDeviceMac::RecordingIsInitialized() const {
    return (_recIsInitialized);
}

bool AudioDeviceMac::Recording() {
    return (_recording);
}

int32_t AudioDeviceMac::RecordingDelay(uint16_t& delayMS) const {
    int32_t captureDelayUs = AtomicGet32(&_captureDelayUs);
    delayMS =
    static_cast<uint16_t>(1e-3 * (captureDelayUs + _captureLatencyUs) + 0.5);
    return 0;
}

void AudioDeviceMac::setVolume(float volume)
{
    SetMicrophoneVolume(volume*255);
}

// ============================================================================
//                                 Private Methods
// ============================================================================

int32_t AudioDeviceMac::GetNumberDevices(const AudioObjectPropertyScope scope,
                                         AudioDeviceID scopedDeviceIds[],
                                         const uint32_t deviceListLength) {
    OSStatus err = noErr;
    
    AudioObjectPropertyAddress propertyAddress = {
        kAudioHardwarePropertyDevices, kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster};
    UInt32 size = 0;
    err = AudioObjectGetPropertyDataSize(
                                         kAudioObjectSystemObject, &propertyAddress, 0, NULL, &size);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyDataSize return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    if (size == 0) {
        LOGW("No devices");
        return 0;
    }
    
    AudioDeviceID* deviceIds = (AudioDeviceID*)malloc(size);
    UInt32 numberDevices = size / sizeof(AudioDeviceID);
    AudioBufferList* bufferList = NULL;
    UInt32 numberScopedDevices = 0;
    
    // First check if there is a default device and list it
    UInt32 hardwareProperty = 0;
    if (scope == kAudioDevicePropertyScopeOutput) {
        hardwareProperty = kAudioHardwarePropertyDefaultOutputDevice;
    } else {
        hardwareProperty = kAudioHardwarePropertyDefaultInputDevice;
    }
    
    AudioObjectPropertyAddress propertyAddressDefault = {
        hardwareProperty, kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster};
    
    AudioDeviceID usedID;
    UInt32 uintSize = sizeof(UInt32);
    err = AudioObjectGetPropertyData(kAudioObjectSystemObject,
                                     &propertyAddressDefault, 0,
                                     NULL, &uintSize, &usedID);

    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }

    if (usedID != kAudioDeviceUnknown) {
        scopedDeviceIds[numberScopedDevices] = usedID;
        numberScopedDevices++;
    } else {
        LOGW("GetNumberDevices(): Default device unknown");
    }
    
    // Then list the rest of the devices
    bool listOK = true;
    err = AudioObjectGetPropertyData(kAudioObjectSystemObject, &propertyAddress, 0, NULL, &size, deviceIds);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
    }
    if (err != noErr) {
        listOK = false;
    } else {
        propertyAddress.mSelector = kAudioDevicePropertyStreamConfiguration;
        propertyAddress.mScope = scope;
        propertyAddress.mElement = 0;
        for (UInt32 i = 0; i < numberDevices; i++) {
            // Check for input channels
            err = AudioObjectGetPropertyDataSize(deviceIds[i], &propertyAddress, 0, NULL, &size);
            if (err != noErr) {
                LOGE("[%s:%s:%d] AudioObjectGetPropertyDataSize return %d", __FILE__,__FUNCTION__,__LINE__,err);
            }
            if (err == kAudioHardwareBadDeviceError) {
                // This device doesn't actually exist; continue iterating.
                continue;
            } else if (err != noErr) {
                listOK = false;
                break;
            }
            
            bufferList = (AudioBufferList*)malloc(size);
            err = AudioObjectGetPropertyData(deviceIds[i], &propertyAddress, 0, NULL, &size, bufferList);
            if (err != noErr) {
                LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            }
            if (err != noErr) {
                listOK = false;
                break;
            }
            
            if (bufferList->mNumberBuffers > 0) {
                if (numberScopedDevices >= deviceListLength) {
                    LOGE("Device list is not long enough");
                    listOK = false;
                    break;
                }
                
                scopedDeviceIds[numberScopedDevices] = deviceIds[i];
                numberScopedDevices++;
            }
            
            free(bufferList);
            bufferList = NULL;
        }  // for
    }
    
    if (!listOK) {
        if (deviceIds) {
            free(deviceIds);
            deviceIds = NULL;
        }
        
        if (bufferList) {
            free(bufferList);
            bufferList = NULL;
        }
        
        return -1;
    }
    
    // Happy ending
    if (deviceIds) {
        free(deviceIds);
        deviceIds = NULL;
    }
    
    return numberScopedDevices;
}

int32_t AudioDeviceMac::GetDeviceName(const AudioObjectPropertyScope scope,
                                      const uint16_t index,
                                      char* name) {
    OSStatus err = noErr;
    UInt32 len = kAdmMaxDeviceNameSize;
    AudioDeviceID deviceIds[MaxNumberDevices];
    
    int numberDevices = GetNumberDevices(scope, deviceIds, MaxNumberDevices);
    if (numberDevices < 0) {
        return -1;
    } else if (numberDevices == 0) {
        LOGE("No devices");
        return -1;
    }
    
    // If the number is below the number of devices, assume it's "WEBRTC ID"
    // otherwise assume it's a CoreAudio ID
    AudioDeviceID usedID;
    
    // Check if there is a default device
    bool isDefaultDevice = false;
    if (index == 0) {
        UInt32 hardwareProperty = 0;
        if (scope == kAudioDevicePropertyScopeOutput) {
            hardwareProperty = kAudioHardwarePropertyDefaultOutputDevice;
        } else {
            hardwareProperty = kAudioHardwarePropertyDefaultInputDevice;
        }
        AudioObjectPropertyAddress propertyAddress = {
            hardwareProperty, kAudioObjectPropertyScopeGlobal,
            kAudioObjectPropertyElementMaster};
        UInt32 size = sizeof(UInt32);
        err = AudioObjectGetPropertyData(kAudioObjectSystemObject, &propertyAddress, 0, NULL, &size, &usedID);
        if (err != noErr) {
            LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        if (usedID == kAudioDeviceUnknown) {
            LOGW("GetDeviceName(): Default device unknown");
        } else {
            isDefaultDevice = true;
        }
    }
    
    AudioObjectPropertyAddress propertyAddress = {kAudioDevicePropertyDeviceName,
        scope, 0};
    
    if (isDefaultDevice) {
        char devName[len];
        
        err = AudioObjectGetPropertyData(usedID, &propertyAddress,
                                         0, NULL, &len, devName);
        if (err != noErr) {
            LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        sprintf(name, "default (%s)", devName);
    } else {
        if (index < numberDevices) {
            usedID = deviceIds[index];
        } else {
            usedID = index;
        }
        
        err = AudioObjectGetPropertyData(usedID, &propertyAddress,
                                         0, NULL, &len, name);
        if (err != noErr) {
            LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
    }
    
    return 0;
}

int32_t AudioDeviceMac::InitDevice(const uint16_t userDeviceIndex,
                                   AudioDeviceID& deviceId,
                                   const bool isInput) {
    OSStatus err = noErr;
    UInt32 size = 0;
    AudioObjectPropertyScope deviceScope;
    AudioObjectPropertySelector defaultDeviceSelector;
    AudioDeviceID deviceIds[MaxNumberDevices];
    
    if (isInput) {
        deviceScope = kAudioDevicePropertyScopeInput;
        defaultDeviceSelector = kAudioHardwarePropertyDefaultInputDevice;
    } else {
        deviceScope = kAudioDevicePropertyScopeOutput;
        defaultDeviceSelector = kAudioHardwarePropertyDefaultOutputDevice;
    }
    
    AudioObjectPropertyAddress propertyAddress = {
        defaultDeviceSelector, kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster};
    
    // Get the actual device IDs
    int numberDevices = GetNumberDevices(deviceScope, deviceIds, MaxNumberDevices);
    if (numberDevices < 0) {
        return -1;
    } else if (numberDevices == 0) {
        LOGE("InitDevice(): No devices");
        return -1;
    }
    
    bool isDefaultDevice = false;
    deviceId = kAudioDeviceUnknown;
    if (userDeviceIndex == 0) {
        // Try to use default system device
        size = sizeof(AudioDeviceID);
        err = AudioObjectGetPropertyData(kAudioObjectSystemObject, &propertyAddress, 0, NULL, &size, &deviceId);
        if (err != noErr) {
            LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        if (deviceId == kAudioDeviceUnknown) {
            LOGW("No default device exists");
        } else {
            isDefaultDevice = true;
        }
    }
    
    if (!isDefaultDevice) {
        deviceId = deviceIds[userDeviceIndex];
    }
    
    // Obtain device name and manufacturer for logging.
    // Also use this as a test to ensure a user-set device ID is valid.
    char devName[128];
    char devManf[128];
    memset(devName, 0, sizeof(devName));
    memset(devManf, 0, sizeof(devManf));
    
    propertyAddress.mSelector = kAudioDevicePropertyDeviceName;
    propertyAddress.mScope = deviceScope;
    propertyAddress.mElement = 0;
    size = sizeof(devName);
    err = AudioObjectGetPropertyData(deviceId, &propertyAddress, 0, NULL, &size, devName);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    propertyAddress.mSelector = kAudioDevicePropertyDeviceManufacturer;
    size = sizeof(devManf);
    err = AudioObjectGetPropertyData(deviceId, &propertyAddress, 0, NULL, &size, devManf);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    if (isInput) {
        LOGI("Input device: %s %s", devManf, devName);
    } else {
        LOGI("Output device: %s %s", devManf, devName);
    }
    
    return 0;
}

OSStatus AudioDeviceMac::objectListenerProc(
                                            AudioObjectID objectId,
                                            UInt32 numberAddresses,
                                            const AudioObjectPropertyAddress addresses[],
                                            void* clientData) {
    AudioDeviceMac* ptrThis = (AudioDeviceMac*)clientData;
    ptrThis->implObjectListenerProc(objectId, numberAddresses, addresses);
    
    // AudioObjectPropertyListenerProc functions are supposed to return 0
    return 0;
}

OSStatus AudioDeviceMac::implObjectListenerProc(
                                                const AudioObjectID objectId,
                                                const UInt32 numberAddresses,
                                                const AudioObjectPropertyAddress addresses[]) {
    for (UInt32 i = 0; i < numberAddresses; i++) {
        if (addresses[i].mSelector == kAudioHardwarePropertyDevices) {
            HandleDeviceChange();
        } else if (addresses[i].mSelector == kAudioDevicePropertyStreamFormat) {
            HandleStreamFormatChange(objectId, addresses[i]);
        } else if (addresses[i].mSelector == kAudioDeviceProcessorOverload) {
            HandleProcessorOverload(addresses[i]);
        }
    }
    
    return 0;
}

int32_t AudioDeviceMac::HandleDeviceChange() {
    OSStatus err = noErr;
    
    // A device has changed. Check if our registered devices have been removed.
    // Ensure the devices have been initialized, meaning the IDs are valid.
    if (MicrophoneIsInitialized()) {
        AudioObjectPropertyAddress propertyAddress = {
            kAudioDevicePropertyDeviceIsAlive, kAudioDevicePropertyScopeInput, 0};
        UInt32 deviceIsAlive = 1;
        UInt32 size = sizeof(UInt32);
        err = AudioObjectGetPropertyData(_inputDeviceID, &propertyAddress, 0, NULL,
                                         &size, &deviceIsAlive);
        
        if (err == kAudioHardwareBadDeviceError || deviceIsAlive == 0) {
            LOGW("Capture device is not alive (probably removed)");
            AtomicSet32(&_captureDeviceIsAlive, 0);
            _mixerManager.CloseMicrophone();
        } else if (err != noErr) {
            LOGE("Error in AudioDeviceGetProperty() : %d", err);
            return -1;
        }
    }
    
    return 0;
}

int32_t AudioDeviceMac::HandleStreamFormatChange(
                                                 const AudioObjectID objectId,
                                                 const AudioObjectPropertyAddress propertyAddress) {
    OSStatus err = noErr;
    
    if (objectId != _inputDeviceID) {
        return 0;
    }
    
    // Get the new device format
    AudioStreamBasicDescription streamFormat;
    UInt32 size = sizeof(streamFormat);
    err = AudioObjectGetPropertyData(objectId, &propertyAddress, 0, NULL, &size, &streamFormat);
    if (err != noErr) {
        LOGE("[%s:%s:%d] AudioObjectGetPropertyData return %d", __FILE__,__FUNCTION__,__LINE__,err);
        return -1;
    }
    if (streamFormat.mFormatID != kAudioFormatLinearPCM) {
        LOGE("Unacceptable input stream format -> mFormatID : %d", streamFormat.mFormatID);
        return -1;
    }
    
    if (streamFormat.mChannelsPerFrame > N_DEVICE_CHANNELS) {
        LOGE("Too many channels on device (mChannelsPerFrame = %d)", streamFormat.mChannelsPerFrame);
        return -1;
    }
    
    LOGI("Stream format:");
    LOGI("mSampleRate = %f, mChannelsPerFrame = %u",
         streamFormat.mSampleRate, streamFormat.mChannelsPerFrame);
    LOGI("mBytesPerPacket = %u, mFramesPerPacket = %u",
         streamFormat.mBytesPerPacket, streamFormat.mFramesPerPacket);
    LOGI("mBytesPerFrame = %u, mBitsPerChannel = %u",
         streamFormat.mBytesPerFrame, streamFormat.mBitsPerChannel);
    LOGI("mFormatFlags = %u", streamFormat.mFormatFlags);
    LOGI("mFormatID : %d", streamFormat.mFormatID);
    
    if (propertyAddress.mScope == kAudioDevicePropertyScopeInput) {
        const int io_block_size_samples = streamFormat.mChannelsPerFrame *
        streamFormat.mSampleRate / 100 * N_BLOCKS_IO;
        if (io_block_size_samples > _captureBufSizeSamples) {
            LOGE("Input IO block size (%d) is larger than ring buffer (%u)", io_block_size_samples, _captureBufSizeSamples);
            return -1;
        }
        
        memcpy(&_inStreamFormat, &streamFormat, sizeof(streamFormat));
        
        if (_inStreamFormat.mChannelsPerFrame >= 2 && (_recChannels == 2)) {
            _inDesiredFormat.mChannelsPerFrame = 2;
        } else {
            // Disable stereo recording when we only have one channel on the device.
            _inDesiredFormat.mChannelsPerFrame = 1;
            _recChannels = 1;
            LOGI("Stereo recording unavailable on this device");
        }
        
        // Recreate the converter with the new format
        // TODO(xians): make this thread safe
        err = AudioConverterDispose(_captureConverter);
        if (err != noErr) {
            LOGE("[%s:%s:%d] AudioConverterDispose return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
        err = AudioConverterNew(&streamFormat, &_inDesiredFormat, &_captureConverter);
        if (err != noErr) {
            LOGE("[%s:%s:%d] AudioConverterNew return %d", __FILE__,__FUNCTION__,__LINE__,err);
            return -1;
        }
    }
    return 0;
}

int32_t AudioDeviceMac::HandleProcessorOverload(
                                                const AudioObjectPropertyAddress propertyAddress) {
    // TODO(xians): we probably want to notify the user in some way of the
    // overload. However, the Windows interpretations of these errors seem to
    // be more severe than what ProcessorOverload is thrown for.
    //
    // We don't log the notification, as it's sent from the HAL's IO thread. We
    // don't want to slow it down even further.
    if (propertyAddress.mScope == kAudioDevicePropertyScopeInput) {
        // WEBRTC_TRACE(kTraceWarning, kTraceAudioDevice, _id, "Capture processor
        // overload");
        //_callback->ProblemIsReported(
        // SndCardStreamObserver::ERecordingProblem);
    } else {
        // WEBRTC_TRACE(kTraceWarning, kTraceAudioDevice, _id,
        // "Render processor overload");
        //_callback->ProblemIsReported(
        // SndCardStreamObserver::EPlaybackProblem);
    }
    
    return 0;
}

// ============================================================================
//                                  Thread Methods
// ============================================================================

OSStatus AudioDeviceMac::inDeviceIOProc(AudioDeviceID,
                                        const AudioTimeStamp*,
                                        const AudioBufferList* inputData,
                                        const AudioTimeStamp* inputTime,
                                        AudioBufferList*,
                                        const AudioTimeStamp*,
                                        void* clientData) {
    AudioDeviceMac* ptrThis = (AudioDeviceMac*)clientData;
    ptrThis->implInDeviceIOProc(inputData, inputTime);
    
    // AudioDeviceIOProc functions are supposed to return 0
    return 0;
}

OSStatus AudioDeviceMac::inConverterProc(
                                         AudioConverterRef,
                                         UInt32* numberDataPackets,
                                         AudioBufferList* data,
                                         AudioStreamPacketDescription** /*dataPacketDescription*/,
                                         void* userData) {
    AudioDeviceMac* ptrThis = static_cast<AudioDeviceMac*>(userData);
    return ptrThis->implInConverterProc(numberDataPackets, data);
}

OSStatus AudioDeviceMac::implInDeviceIOProc(const AudioBufferList* inputData,
                                            const AudioTimeStamp* inputTime) {
    OSStatus err = noErr;
    UInt64 inputTimeNs = AudioConvertHostTimeToNanos(inputTime->mHostTime);
    UInt64 nowNs = AudioConvertHostTimeToNanos(AudioGetCurrentHostTime());
    
    if (!_recording) {
        // Allow above checks to avoid a timeout on stopping capture.
        return 0;
    }
    
    PaRingBufferSize bufSizeSamples = PaUtil_GetRingBufferReadAvailable(_paCaptureBuffer);
    
    int32_t captureDelayUs = static_cast<int32_t>(1e-3 * (nowNs - inputTimeNs) + 0.5);
    captureDelayUs += static_cast<int32_t>((1.0e6 * bufSizeSamples) /
                                           _inStreamFormat.mChannelsPerFrame /
                                           _inStreamFormat.mSampleRate +
                                           0.5);
    
    AtomicSet32(&_captureDelayUs, captureDelayUs);
    
    if (inputData->mNumberBuffers != 1) {
        LOGE("inputData->mNumberBuffers != 1");
    }
//    PaRingBufferSize numSamples = inputData->mBuffers->mDataByteSize *
//    _inStreamFormat.mChannelsPerFrame /
//    _inStreamFormat.mBytesPerPacket;
    
    PaRingBufferSize numSamples = inputData->mBuffers->mDataByteSize / (_inStreamFormat.mBitsPerChannel/8);
    PaUtil_WriteRingBuffer(_paCaptureBuffer, inputData->mBuffers->mData,
                           numSamples);
    
    return err;
}

OSStatus AudioDeviceMac::implInConverterProc(UInt32* numberDataPackets,
                                             AudioBufferList* data) {
    if (data->mNumberBuffers != 1) {
        LOGE("data->mNumberBuffers != 1");
    }
    PaRingBufferSize numSamples = *numberDataPackets * _inStreamFormat.mChannelsPerFrame;
    
    // Pass the read pointer directly to the converter to avoid a memcpy.
    void* dummyPtr;
    PaRingBufferSize dummySize;
    PaUtil_GetRingBufferReadRegions(_paCaptureBuffer, numSamples,
                                    &data->mBuffers->mData, &numSamples,
                                    &dummyPtr, &dummySize);
    PaUtil_AdvanceRingBufferReadIndex(_paCaptureBuffer, numSamples);
    
    data->mBuffers->mNumberChannels = _inStreamFormat.mChannelsPerFrame;
    *numberDataPackets = numSamples / _inStreamFormat.mChannelsPerFrame;
    data->mBuffers->mDataByteSize = *numberDataPackets * _inStreamFormat.mBytesPerFrame;
//    data->mBuffers->mDataByteSize = *numberDataPackets * _inStreamFormat.mBytesPerPacket;
    return 0;
}

AudioFrame* AudioDeviceMac::frontAudioFrame()
{
    PaRingBufferSize numSamples = _recBufSizeInBytes/sizeof(SInt16);
    if (PaUtil_GetRingBufferReadAvailable(_paCaptureBuffer) < numSamples) {
        return NULL;
    }
    
    OSStatus err = noErr;
    SInt16 recordBuffer[numSamples];
    UInt32 size = numSamples/_inDesiredFormat.mChannelsPerFrame;
    
    AudioBufferList engineBuffer;
    engineBuffer.mNumberBuffers = 1;  // Interleaved channels.
    engineBuffer.mBuffers->mNumberChannels = _inDesiredFormat.mChannelsPerFrame;
    engineBuffer.mBuffers->mDataByteSize = _recBufSizeInBytes;
    engineBuffer.mBuffers->mData = recordBuffer;
    
    err = AudioConverterFillComplexBuffer(_captureConverter, inConverterProc,
                                          this, &size, &engineBuffer, NULL);
    
    if (err != noErr) {
        if (err == 1) {
            // This is our own error.
            return NULL;
        } else {
            LOGE("Error in AudioConverterFillComplexBuffer() : %d", err);
            return NULL;
        }
    }
    
    memcpy(mAudioFrame.data, recordBuffer, _recBufSizeInBytes);
    mAudioFrame.frameSize = _recBufSizeInBytes;
    mAudioFrame.duration = float(_recBufSizeInBytes*1000)/float(_sampleRate*_recChannels*sizeof(SInt16));
    int32_t captureDelayUs = AtomicGet32(&_captureDelayUs);
    mAudioFrame.pts = MediaTimer::GetNowMediaTimeMS()-static_cast<int32_t>(1e-3 * (captureDelayUs + _captureLatencyUs) + 0.5);
    
    return &mAudioFrame;
}

void AudioDeviceMac::popAudioFrame()
{
    //no need do anything.
}
