
#ifndef AUDIO_DEVICE_MAC_H
#define AUDIO_DEVICE_MAC_H

#include <memory>

#include "audio_mixer_manager_mac.h"

#include <AudioToolbox/AudioConverter.h>
#include <CoreAudio/CoreAudio.h>

#include "AudioCapture.h"

struct PaUtilRingBuffer;

const uint32_t N_REC_SAMPLES_PER_SEC = 44100;

const uint32_t N_REC_CHANNELS = 1;   // default is mono recording
const uint32_t N_DEVICE_CHANNELS = 64;

const int kBufferSizeMs = 10;

const uint32_t ENGINE_REC_BUF_SIZE_IN_SAMPLES =
N_REC_SAMPLES_PER_SEC * kBufferSizeMs / 1000;

const int N_BLOCKS_IO = 2;
const int N_BUFFERS_IN = 10;   // Must be at least N_BLOCKS_IO.

const uint32_t REC_BUF_SIZE_IN_SAMPLES =
ENGINE_REC_BUF_SIZE_IN_SAMPLES * N_REC_CHANNELS * N_BUFFERS_IN;

const int kGetMicVolumeIntervalMs = 1000;

const int kAdmMaxDeviceNameSize = 128;

class AudioDeviceMac : public AudioCapture {
public:
    AudioDeviceMac(int sampleRate, int numChannels);
    ~AudioDeviceMac();
    
    void setRecBufSizeInBytes(int bufSizeInBytes);
    
    // Main initializaton and termination
    int Init();
    int Terminate();
    bool Initialized();
    
    // Audio transport control
    int StartRecording();
    int StopRecording();
    bool Recording();
    
    void setVolume(float volume);

    void inputAudioFrame(AudioFrame *inAudioFrame) {};

    AudioFrame* frontAudioFrame();
    void popAudioFrame();
private:
    // Device enumeration
    int16_t RecordingDevices();
    int32_t RecordingDeviceName(uint16_t index, char name[kAdmMaxDeviceNameSize]);
    
    // Device selection
    int32_t SetRecordingDevice(uint16_t index);
    
    // Audio transport initialization
    int32_t RecordingIsAvailable(bool& available);
    int32_t InitRecording();
    bool RecordingIsInitialized() const;
    
    // Microphone Automatic Gain Control (AGC)
    int32_t SetAGC(bool enable);
    bool AGC() const;
    
    // Audio mixer initialization
    int32_t InitMicrophone();
    bool MicrophoneIsInitialized() const;
    
    // Microphone volume controls
    int32_t MicrophoneVolumeIsAvailable(bool& available);
    int32_t SetMicrophoneVolume(uint32_t volume);
    int32_t MicrophoneVolume(uint32_t& volume) const;
    int32_t MaxMicrophoneVolume(uint32_t& maxVolume) const;
    int32_t MinMicrophoneVolume(uint32_t& minVolume) const;
    int32_t MicrophoneVolumeStepSize(uint16_t& stepSize) const;
    
    // Microphone mute control
    int32_t MicrophoneMuteIsAvailable(bool& available);
    int32_t SetMicrophoneMute(bool enable);
    int32_t MicrophoneMute(bool& enabled) const;
    
    // Stereo support
    int32_t StereoRecordingIsAvailable(bool& available);
    int32_t SetStereoRecording(bool enable);
    int32_t StereoRecording(bool& enabled) const;
    
    // Delay information and control
    int32_t RecordingDelay(uint16_t& delayMS) const;

private:
    int32_t MicrophoneIsAvailable(bool& available);
    
    static void AtomicSet32(int32_t* theValue, int32_t newValue);
    static int32_t AtomicGet32(int32_t* theValue);
    static int isLittleEndian();

    int32_t GetNumberDevices(const AudioObjectPropertyScope scope,
                             AudioDeviceID scopedDeviceIds[],
                             const uint32_t deviceListLength);
    
    int32_t GetDeviceName(const AudioObjectPropertyScope scope,
                          const uint16_t index,
                          char* name);
    
    int32_t InitDevice(uint16_t userDeviceIndex,
                       AudioDeviceID& deviceId,
                       bool isInput);
    
    static OSStatus objectListenerProc(
                                       AudioObjectID objectId,
                                       UInt32 numberAddresses,
                                       const AudioObjectPropertyAddress addresses[],
                                       void* clientData);
    
    OSStatus implObjectListenerProc(AudioObjectID objectId,
                                    UInt32 numberAddresses,
                                    const AudioObjectPropertyAddress addresses[]);
    
    int32_t HandleDeviceChange();
    
    int32_t HandleStreamFormatChange(AudioObjectID objectId,
                                     AudioObjectPropertyAddress propertyAddress);
    
    int32_t HandleDataSourceChange(AudioObjectID objectId,
                                   AudioObjectPropertyAddress propertyAddress);
    
    int32_t HandleProcessorOverload(AudioObjectPropertyAddress propertyAddress);
    
    static OSStatus inDeviceIOProc(AudioDeviceID device,
                                   const AudioTimeStamp* now,
                                   const AudioBufferList* inputData,
                                   const AudioTimeStamp* inputTime,
                                   AudioBufferList* outputData,
                                   const AudioTimeStamp* outputTime,
                                   void* clientData);
    
    static OSStatus inConverterProc(
                                    AudioConverterRef audioConverter,
                                    UInt32* numberDataPackets,
                                    AudioBufferList* data,
                                    AudioStreamPacketDescription** dataPacketDescription,
                                    void* inUserData);
    
    OSStatus implInDeviceIOProc(const AudioBufferList* inputData,
                                const AudioTimeStamp* inputTime);
    
    OSStatus implInConverterProc(UInt32* numberDataPackets,
                                 AudioBufferList* data);
    
    AudioMixerManagerMac _mixerManager;
    
    uint16_t _inputDeviceIndex;
    AudioDeviceID _inputDeviceID;
#if __MAC_OS_X_VERSION_MAX_ALLOWED >= 1050
    AudioDeviceIOProcID _inDeviceIOProcID;
#endif
    bool _inputDeviceIsSpecified;
    
    uint8_t _recChannels;
    
    Float32* _captureBufData;
    int _captureBufSizeSamples;

    bool _initialized;
    bool _isShutDown;
    bool _recording;
    bool _recIsInitialized;
    bool _AGC;
    
    // Atomically set varaibles
    int32_t _captureDeviceIsAlive;
    
    bool _macBookPro;
    bool _macBookProPanRight;
    
    AudioConverterRef _captureConverter;
    
    AudioStreamBasicDescription _inStreamFormat;
    AudioStreamBasicDescription _inDesiredFormat;
    
    uint32_t _captureLatencyUs;
    
    // Atomically set variables
    mutable int32_t _captureDelayUs;
    
    PaUtilRingBuffer* _paCaptureBuffer;
    
    int get_mic_volume_counter_ms_;
    
private:
    int _sampleRate;
    int _recBufSizeInBytes;
    
    AudioFrame mAudioFrame;
};

#endif  // AUDIO_DEVICE_MAC_H_
