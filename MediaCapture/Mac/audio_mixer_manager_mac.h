#ifndef AUDIO_MIXER_MANAGER_MAC_H
#define AUDIO_MIXER_MANAGER_MAC_H

#include <CoreAudio/CoreAudio.h>

class AudioMixerManagerMac {
public:
    int32_t OpenMicrophone(AudioDeviceID deviceID);
    int32_t StereoRecordingIsAvailable(bool& available);
    int32_t MicrophoneMuteIsAvailable(bool& available);
    int32_t SetMicrophoneMute(bool enable);
    int32_t MicrophoneMute(bool& enabled) const;
    int32_t MicrophoneVolumeIsAvailable(bool& available);
    int32_t SetMicrophoneVolume(uint32_t volume);
    int32_t MicrophoneVolume(uint32_t& volume) const;
    int32_t MaxMicrophoneVolume(uint32_t& maxVolume) const;
    int32_t MinMicrophoneVolume(uint32_t& minVolume) const;
    int32_t MicrophoneVolumeStepSize(uint16_t& stepSize) const;
    int32_t Close();
    int32_t CloseMicrophone();
    bool MicrophoneIsInitialized() const;
    
public:
    AudioMixerManagerMac();
    ~AudioMixerManagerMac();
    
private:
    AudioDeviceID _inputDeviceID;
    uint16_t _noInputChannels;
};

#endif  // AUDIO_MIXER_MAC_H
