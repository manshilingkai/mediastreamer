#ifndef AUDIO_MIXER_MANAGER_WIN_H
#define AUDIO_MIXER_MANAGER_WIN_H

#include <winsock2.h>
#include <Windows.h>
#include <mmsystem.h>
#include <stdint.h>

enum WindowsDeviceType {
	kDefaultCommunicationDevice = -1,
	kDefaultDevice = -2
};

class AudioMixerManagerWin
{
public:
	enum { MAX_NUMBER_MIXER_DEVICES = 40 };
	enum { MAX_NUMBER_OF_LINE_CONTROLS = 20 };
	enum { MAX_NUMBER_OF_MULTIPLE_ITEMS = 20 };
	struct SpeakerLineInfo
	{
		DWORD dwLineID;
		bool  speakerIsValid;
		DWORD dwVolumeControlID;
		bool  volumeControlIsValid;
		DWORD dwMuteControlID;
		bool  muteControlIsValid;
	};
	struct MicrophoneLineInfo
	{
		DWORD dwLineID;
		bool  microphoneIsValid;
		DWORD dwVolumeControlID;
		bool  volumeControlIsValid;
		DWORD dwMuteControlID;
		bool  muteControlIsValid;
		DWORD dwOnOffControlID;
		bool  onOffControlIsValid;
	};
public:
	int32_t EnumerateAll();
	int32_t EnumerateMicrophones();
	int32_t OpenMicrophone(WindowsDeviceType device);
	int32_t OpenMicrophone(uint16_t index);
	int32_t MicrophoneMuteIsAvailable(bool& available);
	int32_t SetMicrophoneMute(bool enable);
	int32_t MicrophoneMute(bool& enabled) const;
	int32_t MicrophoneBoostIsAvailable(bool& available);
	int32_t SetMicrophoneBoost(bool enable);
	int32_t MicrophoneBoost(bool& enabled) const;
	int32_t MicrophoneVolumeIsAvailable(bool& available);
	int32_t SetMicrophoneVolume(uint32_t volume);
	int32_t MicrophoneVolume(uint32_t& volume) const;
	int32_t MaxMicrophoneVolume(uint32_t& maxVolume) const;
	int32_t MinMicrophoneVolume(uint32_t& minVolume) const;
	int32_t MicrophoneVolumeStepSize(uint16_t& stepSize) const;
	int32_t Close();
	int32_t CloseMicrophone();
	bool MicrophoneIsInitialized() const;
	UINT Devices() const;

private:
	UINT DestinationLines(UINT mixId) const;
	UINT SourceLines(UINT mixId, DWORD destId) const;
	bool GetCapabilities(UINT mixId, MIXERCAPS& caps, bool trace = false) const;
	bool GetDestinationLineInfo(UINT mixId, DWORD destId, MIXERLINE& line, bool trace = false) const;
	bool GetSourceLineInfo(UINT mixId, DWORD destId, DWORD srcId, MIXERLINE& line, bool trace = false) const;

	bool GetAllLineControls(UINT mixId, const MIXERLINE& line, MIXERCONTROL* controlArray, bool trace = false) const;
	bool GetLineControl(UINT mixId, DWORD dwControlID, MIXERCONTROL& control) const;
	bool GetControlDetails(UINT mixId, MIXERCONTROL& controlArray, bool trace = false) const;
	bool GetUnsignedControlValue(UINT mixId, DWORD dwControlID, DWORD& dwValue) const;
	bool SetUnsignedControlValue(UINT mixId, DWORD dwControlID, DWORD dwValue) const;
	bool SetBooleanControlValue(UINT mixId, DWORD dwControlID, bool value) const;
	bool GetBooleanControlValue(UINT mixId, DWORD dwControlID, bool& value) const;
	bool GetSelectedMuxSource(UINT mixId, DWORD dwControlID, DWORD cMultipleItems, UINT& index) const;

private:
	void ClearMicrophoneState();
	void ClearMicrophoneState(UINT idx);
	bool MicrophoneIsValid(UINT idx) const;
	UINT ValidMicrophones() const;

	void TraceStatusAndSupportFlags(DWORD fdwLine) const;
	void TraceTargetType(DWORD dwType) const;
	void TraceComponentType(DWORD dwComponentType) const;
	void TraceControlType(DWORD dwControlType) const;
	void TraceControlStatusAndSupportFlags(DWORD fdwControl) const;
	void TraceWaveInError(MMRESULT error) const;
	// Converts from wide-char to UTF-8 if UNICODE is defined.
	// Does nothing if UNICODE is undefined.
	char* WideToUTF8(const TCHAR* src) const;

public:
	AudioMixerManagerWin();
	~AudioMixerManagerWin();

private:
	HMIXER                  _inputMixerHandle;
	UINT                    _inputMixerID;
	MicrophoneLineInfo      _microphoneState[MAX_NUMBER_MIXER_DEVICES];
	mutable char            _str[MAXERRORLENGTH];
};

#endif