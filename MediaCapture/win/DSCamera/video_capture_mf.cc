/*
 *  Copyright (c) 2012 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#include "modules/video_capture/windows/video_capture_mf.h"

namespace webrtc {
namespace videocapturemodule {

template <class T> void SafeRelease(T **ppT)
{
    if (*ppT)
    {
        (*ppT)->Release();
        *ppT = NULL;
    }
}

VideoCaptureMF::VideoCaptureMF() :
    _pMfInfo(NULL),
    m_nRefCount(1),
    m_pSource(NULL),
    m_pReader(NULL),
    m_bFirstSample(FALSE),
    m_llBaseTime(0)
{
    _pMfInfo = DeviceInfoMF::Create();
    InitializeCriticalSection(&m_critsec);
}

VideoCaptureMF::VideoCaptureMF(DeviceInfo* pInfo) :
    _pMfInfo(NULL),
    m_nRefCount(1),
    m_pSource(NULL),
    m_pReader(NULL),
    m_bFirstSample(FALSE),
    m_llBaseTime(0)
{
    _pMfInfo = (DeviceInfoMF*)(pInfo);
    InitializeCriticalSection(&m_critsec);
}

VideoCaptureMF::~VideoCaptureMF()
{
    SafeRelease(m_pSource);

    DeleteCriticalSection(&m_critsec);

    if (_pMfInfo) {
      delete _pMfInfo;
    }
}

/////////////// IUnknown methods ///////////////

//-------------------------------------------------------------------
//  AddRef
//-------------------------------------------------------------------

ULONG VideoCaptureMF::AddRef()
{
    return InterlockedIncrement(&m_nRefCount);
}


//-------------------------------------------------------------------
//  Release
//-------------------------------------------------------------------

ULONG VideoCaptureMF::Release()
{
    ULONG uCount = InterlockedDecrement(&m_nRefCount);
    if (uCount == 0)
    {
        delete this;
    }
    return uCount;
}

//-------------------------------------------------------------------
//  QueryInterface
//-------------------------------------------------------------------

HRESULT VideoCaptureMF::QueryInterface(REFIID riid, void** ppv)
{
    static const QITAB qit[] = 
    {
        QITABENT(VideoCaptureMF, IMFSourceReaderCallback),
        { 0 },
    };
    return QISearch(this, qit, riid, ppv);
}

// IMFSourceReaderCallback methods
STDMETHODIMP VideoCaptureMF::OnReadSample(
    HRESULT hrStatus,
    DWORD dwStreamIndex,
    DWORD dwStreamFlags,
    LONGLONG llTimestamp,
    IMFSample *pSample // Can be NULL
    )
{
    EnterCriticalSection(&m_critsec);

    bool bIsCapturing = (m_pReader != NULL);
    if (!bIsCapturing) {
        LeaveCriticalSection(&m_critsec);
        return S_OK;
    }

    HRESULT hr = S_OK;

    if (FAILED(hrStatus))
    {
        hr = hrStatus;
        RTC_LOG(LS_ERROR) << "OnReadSample fail";
        LeaveCriticalSection(&m_critsec);
        return hr;
    }

    if (dwStreamFlags & MF_SOURCE_READERF_STREAMTICK)
    {
      // Read another sample.
      hr = m_pReader->ReadSample(
          (DWORD)MF_SOURCE_READER_FIRST_VIDEO_STREAM,
          0,
          NULL,   // actual
          NULL,   // flags
          NULL,   // timestamp
          NULL    // sample
          );

      LeaveCriticalSection(&m_critsec);
      return hr;
    }

    if (pSample) {
        if (m_bFirstSample)
        {
            m_llBaseTime = llTimeStamp;
            m_bFirstSample = FALSE;
        }

        // rebase the time stamp
        llTimeStamp -= m_llBaseTime;

        hr = pSample->SetSampleTime(llTimeStamp);

        if (FAILED(hr)) {
          RTC_LOG(LS_ERROR) << "IMFSample -> SetSampleTime fail";
          LeaveCriticalSection(&m_critsec);
          return hr;
        }

        IMFMediaBuffer* buffer;
        hr = IMFSample_ConvertToContiguousBuffer(pSample, &buffer);
        if (FAILED(hr)) {
          RTC_LOG(LS_ERROR) << "IMFSample_ConvertToContiguousBuffer fail";
          LeaveCriticalSection(&m_critsec);
          return hr;
        }

        BYTE* data;
        DWORD size;
        hr = IMFMediaBuffer_Lock(buffer, &data, NULL, &size);
        if (FAILED(hr)) {
          RTC_LOG(LS_ERROR) << "IMFMediaBuffer_Lock fail";
          LeaveCriticalSection(&m_critsec);
          return hr;
        }

        IncomingFrame(data, size, _requestedCapability, llTimeStamp);

        IMFMediaBuffer_Unlock(buffer);
        IMFMediaBuffer_Release(buffer);
    }

    // Read another sample.
    hr = m_pReader->ReadSample(
        (DWORD)MF_SOURCE_READER_FIRST_VIDEO_STREAM,
        0,
        NULL,   // actual
        NULL,   // flags
        NULL,   // timestamp
        NULL    // sample
        );

    LeaveCriticalSection(&m_critsec);
    return hr;
}

int32_t VideoCaptureMF::Init(const char* deviceUniqueIdUTF8) {

  IMFMediaSource* device;

  {
    IMFAttributes* attr;

    HRESULT hr = MFCreateAttributes(&attr, 2);
    if (FAILED(hr)) {
      RTC_LOG(LS_ERROR) << "MFCreateAttributes fail!";
      return -1;
    }

    hr = IMFAttributes_SetGUID(attr, &MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE, &MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_VIDCAP_GUID);
    if (FAILED(hr)) {
      IMFAttributes_Release(attr);
      RTC_LOG(LS_ERROR) << "IMFAttributes_SetGUID fail!";
      return -1;
    }

    hr = IMFAttributes_SetString(attr, &MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_VIDCAP_SYMBOLIC_LINK, to_wide_string(deviceUniqueIdUTF8).c_str());
    if (FAILED(hr)) {
      IMFAttributes_Release(attr);
      RTC_LOG(LS_ERROR) << "IMFAttributes_SetString fail!";
      return -1;
    }

    hr = MFCreateDeviceSource(attr, &device);
    if (FAILED(hr)) {
      IMFAttributes_Release(attr);
      RTC_LOG(LS_ERROR) << "MFCreateDeviceSource fail!";
      return -1;
    }

    IMFAttributes_Release(attr);
  }

  m_pSource = device;

  return 0;
}

int32_t VideoCaptureMF::StartCapture(const VideoCaptureCapability& capability) {
  if (m_pSource == NULL) {
      RTC_LOG(LS_ERROR) << "not initialized yet!";
      return -1;
  }

  _requestedCapability = capability;

  EnterCriticalSection(&m_critsec);

  HRESULT hr = OpenSourceReader(m_pSource);

  if (FAILED(hr)) {
    RTC_LOG(LS_ERROR) << "OpenSourceReader fail";
    LeaveCriticalSection(&m_critsec);
    return -1;
  }

  hr = ConfigureSourceReader(m_pReader, capability);
  if (FAILED(hr)) {
    SafeRelease(&m_pReader);
    RTC_LOG(LS_ERROR) << "ConfigureSourceReader fail";
    LeaveCriticalSection(&m_critsec);
    return -1;
  }

  m_bFirstSample = TRUE;
  m_llBaseTime = 0;

  // Request the first video frame.
  hr = m_pReader->ReadSample(
      (DWORD)MF_SOURCE_READER_FIRST_VIDEO_STREAM,
      0,
      NULL,
      NULL,
      NULL,
      NULL
      );

  if (FAILED(hr)) {
    RTC_LOG(LS_ERROR) << "IMFSourceReader -> ReadSample fail";
    LeaveCriticalSection(&m_critsec);
    return -1;
  }

  LeaveCriticalSection(&m_critsec);

  return 0;
}

int32_t VideoCaptureMF::StopCapture() {
  EnterCriticalSection(&m_critsec);

  SafeRelease(&m_pReader);

  LeaveCriticalSection(&m_critsec);
  
  return 0;
}

bool VideoCaptureMF::CaptureStarted() {
  EnterCriticalSection(&m_critsec);
  
  bool bIsCapturing = (m_pReader != NULL);

  LeaveCriticalSection(&m_critsec);

  return bIsCapturing;
}

int32_t VideoCaptureMF::CaptureSettings(VideoCaptureCapability& settings) {
  settings = _requestedCapability;
  return 0;
}

HRESULT VideoCaptureMF::OpenSourceReader(IMFMediaSource *pSource) {
    HRESULT hr = S_OK;

    IMFAttributes *pAttributes = NULL;

    hr = MFCreateAttributes(&pAttributes, 2);

    if (FAILED(hr)) {
      RTC_LOG(LS_ERROR) << "MFCreateAttributes fail!";
      return hr;
    }

    hr = pAttributes->SetUnknown(MF_SOURCE_READER_ASYNC_CALLBACK, this);

    if (FAILED(hr)) {
      SafeRelease(&pAttributes);
      RTC_LOG(LS_ERROR) << "IMFAttributes -> SetUnknown fail!";
      return hr;
    }

    hr = MFCreateSourceReaderFromMediaSource(
        pSource,
        pAttributes,
        &m_pReader
        );

    if (FAILED(hr)) {
      SafeRelease(&pAttributes);
      RTC_LOG(LS_ERROR) << "MFCreateSourceReaderFromMediaSource fail!";
      return hr;
    }

    SafeRelease(&pAttributes);
    return hr;
}

HRESULT VideoCaptureMF::ConfigureSourceReader(IMFSourceReader *pReader, const VideoCaptureCapability& capability)
{
  IMFMediaType* type;

  HRESULT hr = MFCreateMediaType(&type);
  if (FAILED(hr)) {
    RTC_LOG(LS_ERROR) << "MFCreateMediaType fail!";
    return hr;
  }

  hr = IMFMediaType_SetGUID(type, &MF_MT_MAJOR_TYPE, &MFMediaType_Video);
  if (FAILED(hr)) {
    IMFMediaType_Release(type);
    RTC_LOG(LS_ERROR) << "IMFMediaType_SetGUID fail!";
    return hr;
  }

  GUID subtype = { 0 };
  if (capability.videoType == VideoType::kI420) {
    subtype == MFVideoFormat_I420;
  }else if (capability.videoType == VideoType::kIYUV) {
    subtype = MFVideoFormat_IYUV;
  }else if (capability.videoType == VideoType::kRGB24) {
    subtype = MFVideoFormat_RGB24;
  }else if (capability.videoType == VideoType::kYUY2) {
    subtype = MFVideoFormat_YUY2;
  }else if (capability.videoType == VideoType::kRGB565) {
    subtype = MFVideoFormat_RGB565;
  }else if (capability.videoType == VideoType::kMJPEG) {
    subtype = MFVideoFormat_MJPG;
  }else if (capability.videoType == VideoType::kUYVY) {
    subtype = MFVideoFormat_UYVY;
  }else if (capability.videoType == VideoType::kYV12) {
    subtype = MFVideoFormat_YV12;
  }else if (capability.videoType == VideoType::kNV12) {
    subtype = MFVideoFormat_NV12;
  }else if (capability.videoType == VideoType::kNV21) {
    subtype = MFVideoFormat_NV21;
  }else {
    IMFMediaType_Release(type);
    RTC_LOG(LS_ERROR) << "unknown MF_MT_SUBTYPE";
    return E_FAIL;
  }

  hr = IMFMediaType_SetGUID(type, &MF_MT_SUBTYPE, &subtype);
  if (FAILED(hr)) {
    IMFMediaType_Release(type);
    RTC_LOG(LS_ERROR) << "IMFMediaType_SetGUID fail!";
    return hr;
  }

  // you can also set desired width/height here
  UINT32 width = capability.width;
  UINT32 height = capability.height;
  hr = MFSetAttributeSize(type, MF_MT_FRAME_SIZE, &width, &height);
  if (FAILED(hr)) {
    IMFMediaType_Release(type);
    RTC_LOG(LS_ERROR) << "MFSetAttributeSize fail!";
    return hr;
  }

  hr = MFSetAttributeRatio(type, MF_MT_FRAME_RATE, capability.maxFPS, 1);
  if (FAILED(hr)) {
    IMFMediaType_Release(type);
    RTC_LOG(LS_ERROR) << "MFSetAttributeRatio fail!";
    return hr;
  }

  hr = IMFSourceReader_SetCurrentMediaType(pReader, MF_SOURCE_READER_FIRST_VIDEO_STREAM, NULL, type);
  if (FAILED(hr)) {
    IMFMediaType_Release(type);
    RTC_LOG(LS_ERROR) << "IMFSourceReader_SetCurrentMediaType fail!";
    return hr;
  }

  IMFMediaType_Release(type);

  return S_OK;
}

}  // namespace videocapturemodule
}  // namespace webrtc
