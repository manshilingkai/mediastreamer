/*
 *  Copyright (c) 2012 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#ifndef MODULES_VIDEO_CAPTURE_WINDOWS_VIDEO_CAPTURE_MF_H_
#define MODULES_VIDEO_CAPTURE_WINDOWS_VIDEO_CAPTURE_MF_H_

#include "modules/video_capture/video_capture_impl.h"
#include "modules/video_capture/windows/device_info_mf.h"
#include <shlwapi.h>

namespace webrtc {
namespace videocapturemodule {

// VideoCapture implementation that uses the Media Foundation API on Windows.
// This will replace the DirectShow based implementation on Vista and higher.
// TODO(tommi): Finish implementing and switch out the DS in the factory method
// for supported platforms.
class VideoCaptureMF : public VideoCaptureImpl, IMFSourceReaderCallback {
 public:
  VideoCaptureMF();
  VideoCaptureMF(DeviceInfo* pInfo);

  int32_t Init(const char* deviceUniqueIdUTF8);

  // Overrides from VideoCaptureImpl.
  int32_t StartCapture(const VideoCaptureCapability& capability) override;
  int32_t StopCapture() override;
  bool CaptureStarted() override;
  int32_t CaptureSettings(VideoCaptureCapability& settings) override;  // NOLINT

 protected:
  ~VideoCaptureMF() override;

 private:
    // IUnknown methods
    STDMETHODIMP QueryInterface(REFIID iid, void** ppv);
    STDMETHODIMP_(ULONG) AddRef();
    STDMETHODIMP_(ULONG) Release();

    // IMFSourceReaderCallback methods
    STDMETHODIMP OnReadSample(
        HRESULT hrStatus,
        DWORD dwStreamIndex,
        DWORD dwStreamFlags,
        LONGLONG llTimestamp,
        IMFSample *pSample
    );

    STDMETHODIMP OnEvent(DWORD, IMFMediaEvent *)
    {
        return S_OK;
    }

    STDMETHODIMP OnFlush(DWORD)
    {
        return S_OK;
    }
private:
    HRESULT OpenSourceReader(IMFMediaSource *pSource);
    HRESULT ConfigureSourceReader(IMFSourceReader *pReader, const VideoCaptureCapability& capability);

    DeviceInfoMF* _pMfInfo;

    long                    m_nRefCount;        // Reference count.
    CRITICAL_SECTION        m_critsec;

    IMFMediaSource          *m_pSource;
    IMFSourceReader         *m_pReader;

    BOOL                    m_bFirstSample;
    LONGLONG                m_llBaseTime;
};

}  // namespace videocapturemodule
}  // namespace webrtc

#endif  // MODULES_VIDEO_CAPTURE_WINDOWS_VIDEO_CAPTURE_MF_H_
