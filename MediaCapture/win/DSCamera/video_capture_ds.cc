/*
 *  Copyright (c) 2012 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#include "video_capture_ds.h"

#include <dvdmedia.h>  // VIDEOINFOHEADER2

#include "help_functions_ds.h"
#include "sink_filter_ds.h"
#include "MediaLog.h"

#include <comdef.h>

namespace webrtc {
namespace videocapturemodule {
VideoCaptureDS::VideoCaptureDS()
    : _deviceUniqueId(NULL),
      _captureFilter(NULL),
      _graphBuilder(NULL),
      _mediaControl(NULL),
      _inputSendPin(NULL),
      _outputCapturePin(NULL),
      _dvFilter(NULL),
      _inputDvPin(NULL),
      _outputDvPin(NULL),
      m_pKsCtrl(NULL) {}

VideoCaptureDS::~VideoCaptureDS() {
  if (_mediaControl) {
    _mediaControl->Stop();
  }
  if (_graphBuilder) {
    if (sink_filter_)
      _graphBuilder->RemoveFilter(sink_filter_.get());
    if (_captureFilter)
      _graphBuilder->RemoveFilter(_captureFilter);
    if (_dvFilter)
      _graphBuilder->RemoveFilter(_dvFilter);
  }
  RELEASE_AND_CLEAR(_inputSendPin);
  RELEASE_AND_CLEAR(_outputCapturePin);

  RELEASE_AND_CLEAR(m_pKsCtrl);
  RELEASE_AND_CLEAR(_captureFilter);  // release the capture device
  RELEASE_AND_CLEAR(_dvFilter);

  RELEASE_AND_CLEAR(_mediaControl);

  RELEASE_AND_CLEAR(_inputDvPin);
  RELEASE_AND_CLEAR(_outputDvPin);

  RELEASE_AND_CLEAR(_graphBuilder);

  if (_deviceUniqueId)
    delete[] _deviceUniqueId;
}

int32_t VideoCaptureDS::Init(const char* deviceUniqueIdUTF8) {
  const int32_t nameLength = (int32_t)strlen((char*)deviceUniqueIdUTF8);
  if (nameLength >= kVideoCaptureUniqueNameLength)
    return -1;

  // Store the device name
  _deviceUniqueId = new (std::nothrow) char[nameLength + 1];
  memcpy(_deviceUniqueId, deviceUniqueIdUTF8, nameLength + 1);

  if (_dsInfo.Init() != 0)
    return -1;

  _captureFilter = _dsInfo.GetDeviceFilter(deviceUniqueIdUTF8);
  if (!_captureFilter) {
    LOGI("Failed to create capture filter.");
    return -1;
  }

  HRESULT hr = _captureFilter->QueryInterface(IID_IKsControl, (void**)&m_pKsCtrl);
  if (FAILED(hr) || m_pKsCtrl == NULL) {
    m_pKsCtrl = NULL;
    LOGE("Failed to create Ks Control.");
  }

  // Get the interface for DirectShow's GraphBuilder
  hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER,
                                IID_IGraphBuilder, (void**)&_graphBuilder);
  if (FAILED(hr)) {
    LOGI("Failed to create graph builder.");
    return -1;
  }

  hr = _graphBuilder->QueryInterface(IID_IMediaControl, (void**)&_mediaControl);
  if (FAILED(hr)) {
    LOGI("Failed to create media control builder.");
    return -1;
  }
  hr = _graphBuilder->AddFilter(_captureFilter, CAPTURE_FILTER_NAME);
  if (FAILED(hr)) {
    LOGI("Failed to add the capture device to the graph.");
    return -1;
  }

  _outputCapturePin = GetOutputPin(_captureFilter, PIN_CATEGORY_CAPTURE);
  if (!_outputCapturePin) {
    LOGI("Failed to get output capture pin");
    return -1;
  }

  // Create the sink filte used for receiving Captured frames.
  sink_filter_ = new ComRefCount<CaptureSinkFilter>(this);

  hr = _graphBuilder->AddFilter(sink_filter_.get(), SINK_FILTER_NAME);
  if (FAILED(hr)) {
    LOGI("Failed to add the send filter to the graph.");
    return -1;
  }

  _inputSendPin = GetInputPin(sink_filter_.get());
  if (!_inputSendPin) {
    LOGI("Failed to get input send pin");
    return -1;
  }

  // Temporary connect here.
  // This is done so that no one else can use the capture device.
  if (SetCameraOutput(_requestedCapability) != 0) {
    return -1;
  }
  hr = _mediaControl->Pause();
  if (FAILED(hr)) {
    LOGI("Failed to Pause the Capture device. Is it already occupied? %ld", hr);
    return -1;
  }
  LOGI("Capture device '%s' initialized.", deviceUniqueIdUTF8);
  return 0;
}

int32_t VideoCaptureDS::StartCapture(const VideoCaptureCapability& capability) {
  if (capability != _requestedCapability) {
    DisconnectGraph();

    if (SetCameraOutput(capability) != 0) {
      return -1;
    }
  }
  HRESULT hr = _mediaControl->Run();
  if (FAILED(hr)) {
    LOGI("Failed to start the Capture device.");
    return -1;
  }
  return 0;
}

int32_t VideoCaptureDS::StopCapture() {
  HRESULT hr = _mediaControl->Pause();
  if (FAILED(hr)) {
    LOGI("Failed to stop the capture graph. %ld", hr);
    return -1;
  }
  return 0;
}

bool VideoCaptureDS::CaptureStarted() {
  OAFilterState state = 0;
  HRESULT hr = _mediaControl->GetState(1000, &state);
  if (hr != S_OK && hr != VFW_S_CANT_CUE) {
    LOGI("Failed to get the CaptureStarted status");
  }
  LOGI("CaptureStarted %ld", state);
  return state == State_Running;
}

int32_t VideoCaptureDS::CaptureSettings(VideoCaptureCapability& settings) {
  settings = _requestedCapability;
  return 0;
}

void VideoCaptureDS::SendCtrl(VCM_CTRL ctrl, int32_t value) {
  HRESULT hr = -1;
  if (ctrl == VCM_CTRL_ENCODE_IDR) {
    if (value > 0) {
      hr = SetIDR(0, value);
    }else {
      hr = RequestKeyFrame(0);
    }
  }else if (ctrl == VCM_CTRL_ENCODE_BITRATE) {
    	hr = SetBitrate(0, 0, 0, 0, value);
  }else if (ctrl == VCM_CTRL_ENCODE_FRAMERATE) {
      hr = SetFrameRate(0, value);
  }

  if (FAILED(hr)) {
    LOGE("SendCtrl failed! hr = %ld [ %d -- %d ]",hr,ctrl,value);
  }else {
    LOGD("SendCtrl success! hr = %ld [ %d -- %d ]",hr,ctrl,value);
  }
}

int32_t VideoCaptureDS::SetCameraOutput(
    const VideoCaptureCapability& requestedCapability) {
  // Get the best matching capability
  VideoCaptureCapability capability;
  int32_t capabilityIndex;

  // Store the new requested size
  _requestedCapability = requestedCapability;
  // Match the requested capability with the supported.
  if ((capabilityIndex = _dsInfo.GetBestMatchedCapability(
           _deviceUniqueId, _requestedCapability, capability)) < 0) {
    return -1;
  }
  // Reduce the frame rate if possible.
  if (capability.maxFPS > requestedCapability.maxFPS) {
    capability.maxFPS = requestedCapability.maxFPS;
  } else if (capability.maxFPS <= 0) {
    capability.maxFPS = 30;
  }

  // Convert it to the windows capability index since they are not nexessary
  // the same
  VideoCaptureCapabilityWindows windowsCapability;
  if (_dsInfo.GetWindowsCapability(capabilityIndex, windowsCapability) != 0) {
    return -1;
  }

  IAMStreamConfig* streamConfig = NULL;
  AM_MEDIA_TYPE* pmt = NULL;
  VIDEO_STREAM_CONFIG_CAPS caps;

  HRESULT hr = _outputCapturePin->QueryInterface(IID_IAMStreamConfig,
                                                 (void**)&streamConfig);
  if (hr) {
    LOGI("Can't get the Capture format settings.");
    return -1;
  }

  // Get the windows capability from the capture device
  bool isDVCamera = false;
  hr = streamConfig->GetStreamCaps(windowsCapability.directShowCapabilityIndex,
                                   &pmt, reinterpret_cast<BYTE*>(&caps));
  if (hr == S_OK) {
    if (pmt->formattype == FORMAT_VideoInfo2) {
      VIDEOINFOHEADER2* h = reinterpret_cast<VIDEOINFOHEADER2*>(pmt->pbFormat);
      if (capability.maxFPS > 0 && windowsCapability.supportFrameRateControl) {
        h->AvgTimePerFrame = REFERENCE_TIME(10000000.0 / capability.maxFPS);
      }
    } else {
      VIDEOINFOHEADER* h = reinterpret_cast<VIDEOINFOHEADER*>(pmt->pbFormat);
      if (capability.maxFPS > 0 && windowsCapability.supportFrameRateControl) {
        h->AvgTimePerFrame = REFERENCE_TIME(10000000.0 / capability.maxFPS);
      }
    }

    // Set the sink filter to request this capability
    sink_filter_->SetRequestedCapability(capability);
    // Order the capture device to use this capability
    hr += streamConfig->SetFormat(pmt);

    // Check if this is a DV camera and we need to add MS DV Filter
    if (pmt->subtype == MEDIASUBTYPE_dvsl ||
        pmt->subtype == MEDIASUBTYPE_dvsd || pmt->subtype == MEDIASUBTYPE_dvhd)
      isDVCamera = true;  // This is a DV camera. Use MS DV filter
  }
  RELEASE_AND_CLEAR(streamConfig);

  if (FAILED(hr)) {
    LOGI("Failed to set capture device output format");
    return -1;
  }

  if (isDVCamera) {
    hr = ConnectDVCamera();
  } else {
    hr = _graphBuilder->ConnectDirect(_outputCapturePin, _inputSendPin, NULL);
  }
  if (hr != S_OK) {
    LOGI("Failed to connect the Capture graph %ld", hr);
    return -1;
  }
  return 0;
}

HRESULT VideoCaptureDS::GetIDR(BYTE byStream, WORD *pdwIDR)
{
	byte byData[60] = { 0 };
	int hr;

	if (byStream > 2 || !pdwIDR)
	{
		return -1;
	}

	hr = ExtensionCtrl(XU_CONTROL_ENCODE_IDR, KSPROPERTY_TYPE_GET, byData, sizeof(byData));
	if (SUCCEEDED(hr))
	{
		*pdwIDR = byData[3 * byStream + 1] | (byData[3 * byStream + 2] << 8);
	}

	return hr;
}

HRESULT VideoCaptureDS::SetIDR(BYTE byStream, WORD dwIDR)
{
	byte byData[60] = { 0 };
	int nControl;

	if (byStream > 2 || dwIDR < 0 || dwIDR > 65535)
	{
		return -1;
	}

	byData[0] = byStream;	// 0-主码流，1-子码流，2-三码流
	byData[1] = dwIDR & 0xFF;			// 0 - encode IDR immediately, 1-65535:gop of IDR((byte2<<8) | byte1)
	byData[2] = (dwIDR >> 8) & 0xFF;
	nControl = XU_CONTROL_ENCODE_IDR;

	return ExtensionCtrl(nControl, KSPROPERTY_TYPE_SET, byData, sizeof(byData));
}

HRESULT VideoCaptureDS::RequestKeyFrame(BYTE byStream)
{
	byte byData[60] = { 0 };
	WORD dwGOP = 0;
	int nControl;

	if (byStream > 2)
	{
		return -1;
	}

	byData[0] = byStream;	// 0-主码流，1-子码流，2-三码流
	byData[1] = dwGOP & 0xFF;			// 0 - encode IDR immediately, 1-65535:gop of IDR((byte2<<8) | byte1)
	byData[2] = (dwGOP >> 8) & 0xFF;
	nControl = XU_CONTROL_ENCODE_IDR;

	return ExtensionCtrl(nControl, KSPROPERTY_TYPE_SET, byData, sizeof(byData));
}

HRESULT VideoCaptureDS::GetBitrate(BYTE byStream, BYTE *pbyRCMode, BYTE *pbyMinQP, BYTE *pbyMaxQP, UINT *pnBitrate)
{
	HRESULT hr;
	byte byData[60] = { 0 };

	if (byStream > 2 || NULL == pbyRCMode || NULL == pbyMinQP || NULL == pbyMaxQP || NULL == pnBitrate)
	{
		return -1;
	}

	hr = ExtensionCtrl(XU_CONTROL_ENCODE_BITSRATE, KSPROPERTY_TYPE_GET, byData, sizeof(byData));
	if (FAILED(hr))
	{
		return hr;
	}

	*pbyRCMode = (byData[8 * byStream + 1]);
	*pbyMinQP = byData[8 * byStream + 2];
	*pbyMaxQP = byData[8 * byStream + 3];
	*pnBitrate = byData[8 * byStream + 4]
		| (byData[8 * byStream + 5] << 8)
		| (byData[8 * byStream + 6] << 16)
		| (byData[8 * byStream + 7] << 24);

	return hr;
}

HRESULT VideoCaptureDS::SetBitrate(BYTE byStream, BYTE byRCMode, BYTE byMinQP, BYTE byMaxQP, UINT nBitrate)
{
	HRESULT hr;
	byte byData[60] = { 0 };
	int nControl;

	if (byStream > 2)
	{
		return -1;
	}

	byData[0] = byStream;					// 0-主码流，1-子码流，2-三码流;
	byData[1] = byRCMode;					// 0-CBR, 1-VBR, 2-FIXQP
	byData[2] = byMinQP;					// Min QP
	byData[3] = byMaxQP;					// Max QP
	byData[4] = nBitrate & 0xff;
	byData[5] = (nBitrate >> 8) & 0xff;
	byData[6] = (nBitrate >> 16) & 0xff;
	byData[7] = (nBitrate >> 24) & 0xff;
	nControl = XU_CONTROL_ENCODE_BITSRATE;

	hr = ExtensionCtrl(nControl, KSPROPERTY_TYPE_SET, byData, sizeof(byData));
	return hr;
}

HRESULT VideoCaptureDS::GetFrameRate(BYTE byStream, BYTE *pbyFrameRate)
{
	HRESULT hr;
	byte byData[60] = { 0 };

	if (byStream > 2 || NULL == pbyFrameRate)
	{
		return -1;
	}

	hr = ExtensionCtrl(XU_CONTROL_ENCODE_FRAMERATE, KSPROPERTY_TYPE_GET, byData, sizeof(byData));
	if (FAILED(hr))
	{
		return hr;
	}

	*pbyFrameRate = byData[2 * byStream + 1];

	return hr;
}

HRESULT VideoCaptureDS::SetFrameRate(BYTE byStream, BYTE byFrameRate)
{
	BYTE byData[60];

	if (byStream > 2 || byFrameRate < 1 || byFrameRate > 30)
	{
		return -1;
	}

	byData[0] = byStream;
	byData[1] = byFrameRate;
	
	return ExtensionCtrl(XU_CONTROL_ENCODE_FRAMERATE, KSPROPERTY_TYPE_SET, byData, sizeof(byData));
}

HRESULT VideoCaptureDS::ExtensionCtrl(int type, ULONG flag, byte* data, ULONG length)
{
	HRESULT hr = -1;
	KSP_NODE knod;
	ULONG dwRsz = 0;

  if (_captureFilter == NULL || m_pKsCtrl == NULL) {
    return hr;
  }

	knod.Property.Set = PROPSETID_VIDCAP_EXTENSION_UNIT;
	knod.Property.Id = type;
	knod.Property.Flags = (flag | KSPROPERTY_TYPE_TOPOLOGY);
	knod.NodeId = 2;
	knod.Reserved = 0;
  
	hr = m_pKsCtrl->KsProperty((PKSPROPERTY)&knod, sizeof(knod), (PVOID)data, length, &dwRsz);
	if (FAILED(hr))
	{
    LOGE("KsProperty failed! hr = %ld",hr);
	}

	return hr;
}

HRESULT VideoCaptureDS::ExtensionCtrl(GUID id, ULONG nodeid, int type, ULONG flag, byte* data, ULONG length)
{
	HRESULT hr = -1;
	KSP_NODE knod;
	ULONG dwRsz = 0;

  if (_captureFilter == NULL || m_pKsCtrl == NULL) {
    return hr;
  }

	knod.Property.Set = id;
	knod.Property.Id = type;
	knod.Property.Flags = (flag | KSPROPERTY_TYPE_TOPOLOGY);
	knod.NodeId = nodeid;
	knod.Reserved = 0;

	hr = m_pKsCtrl->KsProperty((PKSPROPERTY)&knod, sizeof(knod), (PVOID)data, length, &dwRsz);
	if (FAILED(hr))
	{
    LOGE("KsProperty failed! hr = %ld",hr);
	}

	return hr;
}


int32_t VideoCaptureDS::DisconnectGraph() {
  HRESULT hr = _mediaControl->Stop();
  hr += _graphBuilder->Disconnect(_outputCapturePin);
  hr += _graphBuilder->Disconnect(_inputSendPin);

  // if the DV camera filter exist
  if (_dvFilter) {
    _graphBuilder->Disconnect(_inputDvPin);
    _graphBuilder->Disconnect(_outputDvPin);
  }
  if (hr != S_OK) {
    LOGW("Failed to Stop the Capture device for reconfiguration %ld", hr);
    return -1;
  }
  return 0;
}

HRESULT VideoCaptureDS::ConnectDVCamera() {
  HRESULT hr = S_OK;

  if (!_dvFilter) {
    hr = CoCreateInstance(CLSID_DVVideoCodec, NULL, CLSCTX_INPROC,
                          IID_IBaseFilter, (void**)&_dvFilter);
    if (hr != S_OK) {
      LOGI("Failed to create the dv decoder: %ld", hr);
      return hr;
    }
    hr = _graphBuilder->AddFilter(_dvFilter, L"VideoDecoderDV");
    if (hr != S_OK) {
      LOGI("Failed to add the dv decoder to the graph: %ld", hr);
      return hr;
    }
    _inputDvPin = GetInputPin(_dvFilter);
    if (_inputDvPin == NULL) {
      LOGI("Failed to get input pin from DV decoder");
      return -1;
    }
    _outputDvPin = GetOutputPin(_dvFilter, GUID_NULL);
    if (_outputDvPin == NULL) {
      LOGI("Failed to get output pin from DV decoder");
      return -1;
    }
  }
  hr = _graphBuilder->ConnectDirect(_outputCapturePin, _inputDvPin, NULL);
  if (hr != S_OK) {
    LOGI("Failed to connect capture device to the dv devoder: %ld", hr);
    return hr;
  }

  hr = _graphBuilder->ConnectDirect(_outputDvPin, _inputSendPin, NULL);
  if (hr != S_OK) {
    if (hr == HRESULT_FROM_WIN32(ERROR_TOO_MANY_OPEN_FILES)) {
      LOGI("Failed to connect the capture device, busy");
    } else {
      LOGI("Failed to connect capture device to the send graph: %ld",hr);
    }
  }
  return hr;
}

int32_t VideoCaptureDS::IncomingFrame(uint8_t* videoFrame,
                                        size_t videoFrameLength,
                                        const VideoCaptureCapability& frameInfo,
                                        int64_t captureTime /*=0*/) {

  const int32_t width = frameInfo.width;
  const int32_t height = frameInfo.height;
  if (width == 0 || height == 0 || videoFrameLength == 0) {
    LOGW("0-length incoming frame.");
    return -1;
  }

  // Not encoded, convert to I420.
  if (frameInfo.videoType != VideoType::kMJPEG &&
      CalcBufferSize(frameInfo.videoType, width, abs(height)) !=
          videoFrameLength) {
    LOGE("Wrong incoming frame length.");
    return -1;
  }

  int stride_y = width;
  int stride_uv = (width + 1) / 2;
  int target_width = width;
  int target_height = abs(height);

  //todo

  return 0;
}
}  // namespace videocapturemodule
}  // namespace webrtc
