/*
 *  Copyright (c) 2012 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#include "modules/video_capture/windows/device_info_mf.h"

#include <string>
#include <locale>
#include <codecvt>

namespace webrtc {
namespace videocapturemodule {

// convert string to wstring 
inline std::wstring to_wide_string(const std::string& input) 
{
  std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
  return converter.from_bytes(input);
} 

// convert wstring to string 
inline std::string to_byte_string(const std::wstring& input)
{
  //std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter; 
  std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
  return converter.to_bytes(input);
}

template <class T> void SafeRelease(T **ppT)
{
    if (*ppT)
    {
        (*ppT)->Release();
        *ppT = NULL;
    }
}

// static
DeviceInfoMF* DeviceInfoMF::Create() {
  DeviceInfoMF* mfInfo = new DeviceInfoMF();
  if (!mfInfo || mfInfo->Init() != 0) {
    delete mfInfo;
    mfInfo = NULL;
  }
  return mfInfo;
}

DeviceInfoMF::DeviceInfoMF() : 
    _CoUninitializeIsRequired(true), 
    _MFStartuped(false),
    m_cDevices(0), 
    m_ppDevices(NULL)
{
  // 1) Initialize the COM library (make Windows load the DLLs).
  //
  // CoInitializeEx must be called at least once, and is usually called only
  // once, for each thread that uses the COM library. Multiple calls to
  // CoInitializeEx by the same thread are allowed as long as they pass the same
  // concurrency flag, but subsequent valid calls return S_FALSE. To close the
  // COM library gracefully on a thread, each successful call to CoInitializeEx,
  // including any call that returns S_FALSE, must be balanced by a
  // corresponding call to CoUninitialize.
  //

  /*Apartment-threading, while allowing for multiple threads of execution,
   serializes all incoming calls by requiring that calls to methods of objects
   created by this thread always run on the same thread the apartment/thread
   that created them. In addition, calls can arrive only at message-queue
   boundaries (i.e., only during a PeekMessage, SendMessage, DispatchMessage,
   etc.). Because of this serialization, it is not typically necessary to write
   concurrency control into the code for the object, other than to avoid calls
   to PeekMessage and SendMessage during processing that must not be interrupted
   by other method invocations or calls to other objects in the same
   apartment/thread.*/

  /// CoInitializeEx(NULL, COINIT_APARTMENTTHREADED ); //|
  /// COINIT_SPEED_OVER_MEMORY
  HRESULT hr = CoInitializeEx(
      NULL, COINIT_MULTITHREADED);  // Use COINIT_MULTITHREADED since Voice
                                    // Engine uses COINIT_MULTITHREADED
  if (FAILED(hr)) {
    // Avoid calling CoUninitialize() since CoInitializeEx() failed.
    _CoUninitializeIsRequired = false;

    if (hr == RPC_E_CHANGED_MODE) {
      // Calling thread has already initialized COM to be used in a
      // single-threaded apartment (STA). We are then prevented from using STA.
      // Details: hr = 0x80010106 <=> "Cannot change thread mode after it is
      // set".
      //
      RTC_LOG(LS_WARNING) << __FUNCTION__
                       << ": CoInitializeEx(NULL, COINIT_APARTMENTTHREADED)"
                       << " => RPC_E_CHANGED_MODE, error 0x" << rtc::ToHex(hr);
    }
  }
}

DeviceInfoMF::~DeviceInfoMF() 
{
  Release();

  if (_CoUninitializeIsRequired) {
    CoUninitialize();
  }
}

int32_t DeviceInfoMF::Init() 
{
  HRESULT hr = MFStartup(MF_VERSION, MFSTARTUP_NOSOCKET);

  if (FAILED(hr)) {
    _MFStartuped = false;
    RTC_LOG(LS_ERROR) << "MFStartup fail!";
    return -1;
  }

  _MFStartuped = true;

  hr = EnumerateDevices();

  if (FAILED(hr)) {
    MFShutdown();
    _MFStartuped = false;
    RTC_LOG(LS_ERROR) << "EnumerateDevices fail!";
    return -1;
  }

  return 0;
}

void DeviceInfoMF::Release()
{
  if (_MFStartuped) {
    MFShutdown();
    _MFStartuped = false;
  }
}

uint32_t DeviceInfoMF::NumberOfDevices() {
  return Count();
}

int32_t DeviceInfoMF::GetDeviceName(uint32_t deviceNumber,
                                    char* deviceNameUTF8,
                                    uint32_t deviceNameLength,
                                    char* deviceUniqueIdUTF8,
                                    uint32_t deviceUniqueIdUTF8Length,
                                    char* productUniqueIdUTF8,
                                    uint32_t productUniqueIdUTF8Length) {

    if (deviceNumber >= Count()) {
        RTC_LOG(LS_ERROR) << "invalid device number!";
        return -1;
    }

    std::string device_name;
    bool found = false;
    for (auto iter = _DeviceNameMap.begin(); iter != _DeviceNameMap.end(); ++iter) {
      if (iter->second == deviceNumber) {
        device_name = iter->first;
        found = true;
        break;
      }
    }

    if (!found) {
        RTC_LOG(LS_ERROR) << "not found device number : " << deviceNumber;
        return -1;
    }

    strncpy(deviceNameUTF8, device_name, deviceNameLength);
    deviceNameUTF8[deviceNameLength - 1] = '\0';

    strncpy(deviceUniqueIdUTF8, device_name, deviceUniqueIdUTF8Length);
    deviceUniqueIdUTF8[deviceUniqueIdUTF8Length - 1] = '\0';

    if (productUniqueIdUTF8) {
        productUniqueIdUTF8[0] = '\0';
    }

    return 0;
}

int32_t DeviceInfoMF::DisplayCaptureSettingsDialogBox(
    const char* deviceUniqueIdUTF8,
    const char* dialogTitleUTF8,
    void* parentWindow,
    uint32_t positionX,
    uint32_t positionY) {
  return -1;
}

void DeviceInfoMF::Clear()
{
  for (UINT32 i = 0; i < m_cDevices; i++)
  {
      SafeRelease(&m_ppDevices[i]);
  }
  CoTaskMemFree(m_ppDevices);
  m_ppDevices = NULL;

  m_cDevices = 0;
}

HRESULT DeviceInfoMF::EnumerateDevices()
{
  HRESULT hr = S_OK;
  IMFAttributes *pAttributes = NULL;

  // Initialize an attribute store. We will use this to 
  // specify the enumeration parameters.
  hr = MFCreateAttributes(&pAttributes, 1);

  if (FAILED(hr)) {
    RTC_LOG(LS_ERROR) << "MFCreateAttributes fail!";
    return hr;
  }

  // Ask for source type = video capture devices
  hr = pAttributes->SetGUID(
      MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE, 
      MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_VIDCAP_GUID
      );
  
  if (FAILED(hr)) {
    SafeRelease(&pAttributes);
    RTC_LOG(LS_ERROR) << "IMFAttributes -> SetGUID fail!";
    return hr;
  }

  // Enumerate devices.
  hr = MFEnumDeviceSources(pAttributes, &m_ppDevices, &m_cDevices);
  SafeRelease(&pAttributes);

  if (FAILED(hr)) {
    RTC_LOG(LS_ERROR) << "MFEnumDeviceSources fail!";
    return hr;
  }

  for (size_t i = 0; i < m_cDevices; i++)
  {
    WCHAR *pszName = NULL;
    HRESULT hr = GetMFDeviceName(i, &pszName);
    if (FAILED(hr)) {
      RTC_LOG(LS_WARNING) << "[ Device Number : " << i << "] GetMFDeviceName fail!"
    }else {
      std::string device_name = to_byte_string(pszName);
      RTC_LOG(LS_INFO) << "[ Device Number : " << i << "] GetMFDeviceName : " << device_name;
      std::pair<std::string, int> mapPair = std::pair<std::string, int>(device_name, i);
      _DeviceNameMap.insert(mapPair);
    }
  }

  Clear();

  return S_OK;
}

HRESULT DeviceInfoMF::GetMFDeviceName(UINT32 index, WCHAR **ppszName)
{
    if (index >= Count())
    {
        return E_INVALIDARG;
    }

    HRESULT hr = S_OK;

    hr = m_ppDevices[index]->GetAllocatedString(
        MF_DEVSOURCE_ATTRIBUTE_FRIENDLY_NAME, 
        ppszName, 
        NULL
        );

    return hr;
}

int32_t DeviceInfoMF::CreateCapabilityMap(const char* deviceUniqueIdUTF8)
{
  std::string deviceName(deviceUniqueIdUTF8);
  std::map<std::string, int>::iterator it = _DeviceNameMap.find(deviceName);
  if (it == _DeviceNameMap.end()) {
    RTC_LOG(LS_ERROR) << "not found device name : " << deviceName;
    return -1;
  }
  
  IMFMediaSource* device;

  {
    IMFAttributes* attr;

    HRESULT hr = MFCreateAttributes(&attr, 2);
    if (FAILED(hr)) {
      RTC_LOG(LS_ERROR) << "MFCreateAttributes fail!";
      return -1;
    }

    hr = IMFAttributes_SetGUID(attr, &MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE, &MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_VIDCAP_GUID);
    if (FAILED(hr)) {
      IMFAttributes_Release(attr);
      RTC_LOG(LS_ERROR) << "IMFAttributes_SetGUID fail!";
      return -1;
    }

    hr = IMFAttributes_SetString(attr, &MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_VIDCAP_SYMBOLIC_LINK, to_wide_string(deviceName).c_str());
    if (FAILED(hr)) {
      IMFAttributes_Release(attr);
      RTC_LOG(LS_ERROR) << "IMFAttributes_SetString fail!";
      return -1;
    }

    hr = MFCreateDeviceSource(attr, &device);
    if (FAILED(hr)) {
      IMFAttributes_Release(attr);
      RTC_LOG(LS_ERROR) << "MFCreateDeviceSource fail!";
      return -1;
    }

    IMFAttributes_Release(attr);
  }

  IMFSourceReader* reader;
  
  hr = MFCreateSourceReaderFromMediaSource(device, NULL, &reader);
  if (FAILED(hr)) {
    IMFMediaSource_Release(device);
    RTC_LOG(LS_ERROR) << "MFCreateSourceReaderFromMediaSource fail!";
    return -1;
  }

  IMFMediaSource_Release(device);

  DWORD stream_index = 0;
  IMFMediaType *type = NULL;
  while (SUCCEEDED(reader->GetNativeMediaType(MF_SOURCE_READER_FIRST_VIDEO_STREAM, stream_index, &type)))
  {
    VideoCaptureCapability capability;

    UINT32 width, height;
    hr = MFGetAttributeSize(type, MF_MT_FRAME_SIZE, &width, &height);
    if (FAILED(hr)) {
      RTC_LOG(LS_WARNING) << "MFGetAttributeSize failed";
      continue;
    }

    capability.width = width;
    capability.height = height;

    UINT32 numerator, denominator;
    hr = MFGetAttributeRatio(type, MF_MT_FRAME_RATE, &numerator,
                             &denominator);
    if (FAILED(hr)) {
      RTC_LOG(LS_WARNING) << "MFGetAttributeRatio failed";
      continue;
    }

    float fps = denominator ? static_cast<float>(numerator) / denominator : 0.0f;
    capability.maxFPS = fps;

    GUID type_guid;
    hr = type->GetGUID(MF_MT_SUBTYPE, &type_guid);
    if (FAILED(hr)) {
      RTC_LOG(LS_WARNING) << "IMFMediaType -> GetGUID failed";
      continue;
    }

    if (type_guid == MFVideoFormat_I420) {
      capability.videoType = VideoType::kI420;
    }else if (type_guid == MFVideoFormat_IYUV) {
      capability.videoType = VideoType::kIYUV;
    }else if (type_guid == MFVideoFormat_RGB24) {
      capability.videoType = VideoType::kRGB24;
    }else if (type_guid == MFVideoFormat_YUY2) {
      capability.videoType = VideoType::kYUY2;
    }else if (type_guid == MFVideoFormat_RGB565) {
      capability.videoType = VideoType::kRGB565;
    }else if (type_guid == MFVideoFormat_MJPG) {
      capability.videoType = VideoType::kMJPEG;
    }else if (type_guid == MFVideoFormat_UYVY) {
      capability.videoType = VideoType::kUYVY;
    }else if (type_guid == MFVideoFormat_YV12) {
      capability.videoType = VideoType::kYV12;
    }else if (type_guid == MFVideoFormat_NV12) {
      capability.videoType = VideoType::kNV12;
    }else if (type_guid == MFVideoFormat_NV21) {
      capability.videoType = VideoType::kNV21;
    }else {
      RTC_LOG(LS_WARNING) << "unknown MF_MT_SUBTYPE";
      continue;
    }

    _captureCapabilities.push_back(capability);

    type->Release();
    stream_index++;
  }

  IMFSourceReader_Release(reader);

  RTC_LOG(LS_INFO) << "CreateCapabilityMap " << _captureCapabilities.size();
  return static_cast<int32_t>(_captureCapabilities.size());}

}  // namespace videocapturemodule
}  // namespace webrtc
