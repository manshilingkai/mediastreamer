/*
 *  Copyright (c) 2012 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#ifndef MODULES_VIDEO_CAPTURE_MAIN_SOURCE_WINDOWS_VIDEO_CAPTURE_DS_H_
#define MODULES_VIDEO_CAPTURE_MAIN_SOURCE_WINDOWS_VIDEO_CAPTURE_DS_H_

#include "scoped_refptr.h"
#include "device_info_ds.h"

#include <Ks.h>
#include <KsProxy.h>

#define CAPTURE_FILTER_NAME L"VideoCaptureFilter"
#define SINK_FILTER_NAME L"SinkFilter"

namespace webrtc {
namespace videocapturemodule {

///////////////////////////////////////////////////////////////////////////////////////////
#define STATIC_PROPSETID_VIDCAP_EXTENSION_UNIT \
	0xA29E7641, 0xDE04, 0x47e3, 0x8b, 0x2b, 0xf4, 0x34, 0x1a, 0xff, 0x00, 0x3b
DEFINE_GUIDSTRUCT("A29E7641-DE04-47E3-8B2B-F4341AFF003B", PROPSETID_VIDCAP_EXTENSION_UNIT);
#define PROPSETID_VIDCAP_EXTENSION_UNIT DEFINE_GUIDNAMED(PROPSETID_VIDCAP_EXTENSION_UNIT)

typedef enum XU_CONTROL {
	XU_CONTROL_ENCODE_IDR = 0x0D,
	XU_CONTROL_ENCODE_BITSRATE = 0x0F,
	XU_CONTROL_ENCODE_FRAMERATE = 0x11,
};
///////////////////////////////////////////////////////////////////////////////////////////

enum VCM_CTRL {
	VCM_CTRL_ENCODE_IDR = 0,
	VCM_CTRL_ENCODE_BITRATE = 1,
	VCM_CTRL_ENCODE_FRAMERATE = 2,
};

// Forward declaraion
class CaptureSinkFilter;

class VideoCaptureDS {
 public:
  VideoCaptureDS();

  virtual int32_t Init(const char* deviceUniqueIdUTF8);

  /*************************************************************************
   *
   *   Start/Stop
   *
   *************************************************************************/
  int32_t StartCapture(const VideoCaptureCapability& capability);
  int32_t StopCapture();

  /**************************************************************************
   *
   *   Properties of the set device
   *
   **************************************************************************/

  bool CaptureStarted();
  int32_t CaptureSettings(VideoCaptureCapability& settings);

  void SendCtrl(VCM_CTRL ctrl, int32_t value = 0);

  // |capture_time| must be specified in NTP time format in milliseconds.
  int32_t IncomingFrame(uint8_t* videoFrame,
                        size_t videoFrameLength,
                        const VideoCaptureCapability& frameInfo,
                        int64_t captureTime = 0);

protected:
  char* _deviceUniqueId;  // current Device unique name;
  VideoCaptureCapability _requestedCapability;  // Should be set by platform
                                                // dependent code in
                                                // StartCapture.
 protected:
  ~VideoCaptureDS();

  // Help functions

  int32_t SetCameraOutput(const VideoCaptureCapability& requestedCapability);
  int32_t DisconnectGraph();
  HRESULT ConnectDVCamera();

  DeviceInfoDS _dsInfo;

  IBaseFilter* _captureFilter;
  IGraphBuilder* _graphBuilder;
  IMediaControl* _mediaControl;
  rtc::scoped_refptr<CaptureSinkFilter> sink_filter_;
  IPin* _inputSendPin;
  IPin* _outputCapturePin;

  // Microsoft DV interface (external DV cameras)
  IBaseFilter* _dvFilter;
  IPin* _inputDvPin;
  IPin* _outputDvPin;

  HRESULT GetIDR(BYTE byStream, WORD *pdwIDR);
  HRESULT SetIDR(BYTE byStream, WORD dwIDR);
  HRESULT RequestKeyFrame(BYTE byStream);
	HRESULT GetBitrate(BYTE byStream, BYTE *pbyRCMode, BYTE *pbyMinQP, BYTE *pbyMaxQP, UINT *pnBitrate);
	HRESULT SetBitrate(BYTE byStream, BYTE byRCMode, BYTE byMinQP, BYTE byMaxQP, UINT nBitrate);
  HRESULT GetFrameRate(BYTE byStream, BYTE *pbyFrameRate);
  HRESULT SetFrameRate(BYTE byStream, BYTE byFrameRate);
	HRESULT ExtensionCtrl(int type, ULONG flag, byte* data, ULONG length);
	HRESULT ExtensionCtrl(GUID id, ULONG nodeid, int type, ULONG flag, byte* data, ULONG length);
	IKsControl *m_pKsCtrl;
};
}  // namespace videocapturemodule
}  // namespace webrtc
#endif  // MODULES_VIDEO_CAPTURE_MAIN_SOURCE_WINDOWS_VIDEO_CAPTURE_DS_H_
