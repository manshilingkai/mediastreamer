#include "WinWaveAudioCapture.h"
#include "AudioMixerManagerWin.h"

#include <winsock2.h>
#include <windows.h>
#include <objbase.h>    // CoTaskMemAlloc, CoTaskMemFree
#include <strsafe.h>    // StringCchCopy(), StringCchCat(), StringCchPrintf()
#include <assert.h>

#include "MediaLog.h"
#include "MediaTimer.h"

// Avoids the need of Windows 7 SDK
#ifndef WAVE_MAPPED_DEFAULT_COMMUNICATION_DEVICE
#define WAVE_MAPPED_DEFAULT_COMMUNICATION_DEVICE   0x0010
#endif

// Supported in Windows Vista and Windows 7.
// http://msdn.microsoft.com/en-us/library/dd370819(v=VS.85).aspx
// Taken from Mmddk.h.
#define DRV_RESERVED                      0x0800
#define DRV_QUERYFUNCTIONINSTANCEID       (DRV_RESERVED + 17)
#define DRV_QUERYFUNCTIONINSTANCEIDSIZE   (DRV_RESERVED + 18)

#define POW2(A) (2 << ((A) - 1))

// ============================================================================
//                            Construction & Destruction
// ============================================================================

// ----------------------------------------------------------------------------
//  AudioDeviceWindowsWave - ctor
// ----------------------------------------------------------------------------

WinWaveAudioCapture::WinWaveAudioCapture(int sampleRate, int numChannels) :
	_usingInputDeviceIndex(false),
	_inputDevice(kDefaultDevice),
	_inputDeviceIndex(0),
	_inputDeviceIsSpecified(false),
	_initialized(false),
	_recIsInitialized(false),
	_recording(false),
	_hWaveIn(NULL),
	_minMicVolume(0),
	_maxMicVolume(0)
{
	sampleRate_ = sampleRate;
	_recChannels = numChannels;
	recBufSizeInBytes_ = 0;

	for (size_t i = 0; i < N_BUFFERS_IN; i++)
	{
		_recBuffer[i] = NULL;
	}

	_read_samples = 0;

	LOGD("%s created", __FUNCTION__);
}


// ----------------------------------------------------------------------------
//  AudioDeviceWindowsWave - dtor
// ----------------------------------------------------------------------------

WinWaveAudioCapture::~WinWaveAudioCapture()
{
	LOGD("%s destroyed", __FUNCTION__);

	Terminate();
}

void WinWaveAudioCapture::setRecBufSizeInBytes(int bufSizeInBytes)
{
	recBufSizeInBytes_ = bufSizeInBytes;
}

// ----------------------------------------------------------------------------
//  Init
// ----------------------------------------------------------------------------

int WinWaveAudioCapture::Init(AudioCaptureMode mode) {
	if (_initialized) {
		return 0;
	}

	_mixerManager.EnumerateAll();

	for (size_t i = 0; i < N_BUFFERS_IN; i++)
	{
		_recBuffer[i] = (int8_t*)malloc(sampleRate_ * _recChannels * 2 / 100);
	}

	pthread_mutex_init(&mLock, NULL);
	pthread_cond_init(&mCondition, NULL);

	fifoSize = recBufSizeInBytes_*4;
	fifo = (uint8_t *)malloc(fifoSize);

	write_pos = 0;
	read_pos = 0;
	cache_len = 0;

	mAudioFrame.data = (uint8_t*)malloc(recBufSizeInBytes_);
	mAudioFrame.frameSize = recBufSizeInBytes_;
	mAudioFrame.pts = 0;
	mAudioFrame.duration = 0;

	_initialized = true;

	return 0;
}


// ----------------------------------------------------------------------------
//  Terminate
// ----------------------------------------------------------------------------

int WinWaveAudioCapture::Terminate()
{
	if (!_initialized)
	{
		return 0;
	}

	_mixerManager.Close();

	for (size_t i = 0; i < N_BUFFERS_IN; i++)
	{
		if (_recBuffer[i])
		{
			free(_recBuffer[i]);
			_recBuffer[i] = NULL;
		}
	}

	pthread_mutex_destroy(&mLock);
	pthread_cond_destroy(&mCondition);

	if (fifo != NULL) {
		free(fifo);
		fifo = NULL;
	}

	write_pos = 0;
	read_pos = 0;
	cache_len = 0;

	if (mAudioFrame.data != NULL)
	{
		free(mAudioFrame.data);
		mAudioFrame.data = NULL;
	}

	_initialized = false;
	_inputDeviceIsSpecified = false;

	return 0;
}

// ----------------------------------------------------------------------------
//  Initialized
// ----------------------------------------------------------------------------

bool WinWaveAudioCapture::Initialized()
{
	return (_initialized);
}

// ----------------------------------------------------------------------------
//  InitMicrophone
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::InitMicrophone()
{
	if (_recording)
	{
		return -1;
	}

	if (_mixerManager.EnumerateMicrophones() == -1)
	{
		// failed to locate any valid/controllable microphone
		return -1;
	}

	if (IsUsingInputDeviceIndex())
	{
		if (_mixerManager.OpenMicrophone(InputDeviceIndex()) == -1)
		{
			return -1;
		}
	}
	else
	{
		if (_mixerManager.OpenMicrophone(InputDevice()) == -1)
		{
			return -1;
		}
	}

	uint32_t maxVol = 0;
	if (_mixerManager.MaxMicrophoneVolume(maxVol) == -1)
	{
		LOGW("unable to retrieve max microphone volume");
	}
	_maxMicVolume = maxVol;

	uint32_t minVol = 0;
	if (_mixerManager.MinMicrophoneVolume(minVol) == -1)
	{
		LOGW("unable to retrieve min microphone volume");
	}
	_minMicVolume = minVol;

	return 0;
}

// ----------------------------------------------------------------------------
//  MicrophoneIsInitialized
// ----------------------------------------------------------------------------

bool WinWaveAudioCapture::MicrophoneIsInitialized() const
{
	return (_mixerManager.MicrophoneIsInitialized());
}


// ----------------------------------------------------------------------------
//  MicrophoneMuteIsAvailable
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::MicrophoneMuteIsAvailable(bool& available)
{

	bool isAvailable(false);

	// Enumerate all avaliable microphones and make an attempt to open up the
	// input mixer corresponding to the currently selected input device.
	//
	if (InitMicrophone() == -1)
	{
		// If we end up here it means that the selected microphone has no volume
		// control, hence it is safe to state that there is no boost control
		// already at this stage.
		available = false;
		return 0;
	}

	// Check if the selected microphone has a mute control
	//
	_mixerManager.MicrophoneMuteIsAvailable(isAvailable);
	available = isAvailable;

	// Close the initialized input mixer
	//
	_mixerManager.CloseMicrophone();

	return 0;
}


// ----------------------------------------------------------------------------
//  SetMicrophoneMute
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::SetMicrophoneMute(bool enable)
{
	return (_mixerManager.SetMicrophoneMute(enable));
}

// ----------------------------------------------------------------------------
//  MicrophoneMute
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::MicrophoneMute(bool& enabled) const
{

	bool muted(0);

	if (_mixerManager.MicrophoneMute(muted) == -1)
	{
		return -1;
	}

	enabled = muted;
	return 0;
}


// ----------------------------------------------------------------------------
//  MicrophoneBoostIsAvailable
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::MicrophoneBoostIsAvailable(bool& available)
{

	bool isAvailable(false);

	// Enumerate all avaliable microphones and make an attempt to open up the
	// input mixer corresponding to the currently selected input device.
	//
	if (InitMicrophone() == -1)
	{
		// If we end up here it means that the selected microphone has no volume
		// control, hence it is safe to state that there is no boost control
		// already at this stage.
		available = false;
		return 0;
	}

	// Check if the selected microphone has a boost control
	//
	_mixerManager.MicrophoneBoostIsAvailable(isAvailable);
	available = isAvailable;

	// Close the initialized input mixer
	//
	_mixerManager.CloseMicrophone();

	return 0;
}

// ----------------------------------------------------------------------------
//  SetMicrophoneBoost
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::SetMicrophoneBoost(bool enable)
{

	return (_mixerManager.SetMicrophoneBoost(enable));
}

// ----------------------------------------------------------------------------
//  MicrophoneBoost
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::MicrophoneBoost(bool& enabled) const
{

	bool onOff(0);

	if (_mixerManager.MicrophoneBoost(onOff) == -1)
	{
		return -1;
	}

	enabled = onOff;
	return 0;
}

// ----------------------------------------------------------------------------
//  StereoRecordingIsAvailable
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::StereoRecordingIsAvailable(bool& available)
{
	available = true;
	return 0;
}

// ----------------------------------------------------------------------------
//  SetStereoRecording
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::SetStereoRecording(bool enable)
{

	if (enable)
		_recChannels = 2;
	else
		_recChannels = 1;

	return 0;
}

// ----------------------------------------------------------------------------
//  StereoRecording
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::StereoRecording(bool& enabled) const
{

	if (_recChannels == 2)
		enabled = true;
	else
		enabled = false;

	return 0;
}


// ----------------------------------------------------------------------------
//  MicrophoneVolumeIsAvailable
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::MicrophoneVolumeIsAvailable(bool& available)
{

	bool isAvailable(false);

	// Enumerate all avaliable microphones and make an attempt to open up the
	// input mixer corresponding to the currently selected output device.
	//
	if (InitMicrophone() == -1)
	{
		// Failed to find valid microphone
		available = false;
		return 0;
	}

	// Check if the selected microphone has a volume control
	//
	_mixerManager.MicrophoneVolumeIsAvailable(isAvailable);
	available = isAvailable;

	// Close the initialized input mixer
	//
	_mixerManager.CloseMicrophone();

	return 0;
}

// ----------------------------------------------------------------------------
//  SetMicrophoneVolume
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::SetMicrophoneVolume(uint32_t volume)
{
	return (_mixerManager.SetMicrophoneVolume(volume));
}

// ----------------------------------------------------------------------------
//  MicrophoneVolume
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::MicrophoneVolume(uint32_t& volume) const
{
	uint32_t level(0);

	if (_mixerManager.MicrophoneVolume(level) == -1)
	{
		LOGW("failed to retrive current microphone level");
		return -1;
	}

	volume = level;
	return 0;
}

// ----------------------------------------------------------------------------
//  MaxMicrophoneVolume
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::MaxMicrophoneVolume(uint32_t& maxVolume) const
{
	// _maxMicVolume can be zero in AudioMixerManager::MaxMicrophoneVolume():
	// (1) API GetLineControl() returns failure at querying the max Mic level.
	// (2) API GetLineControl() returns maxVolume as zero in rare cases.
	// Both cases show we don't have access to the mixer controls.
	// We return -1 here to indicate that.
	if (_maxMicVolume == 0)
	{
		return -1;
	}

	maxVolume = _maxMicVolume;;
	return 0;
}

// ----------------------------------------------------------------------------
//  MinMicrophoneVolume
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::MinMicrophoneVolume(uint32_t& minVolume) const
{
	minVolume = _minMicVolume;
	return 0;
}

// ----------------------------------------------------------------------------
//  MicrophoneVolumeStepSize
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::MicrophoneVolumeStepSize(uint16_t& stepSize) const
{

	uint16_t delta(0);

	if (_mixerManager.MicrophoneVolumeStepSize(delta) == -1)
	{
		return -1;
	}

	stepSize = delta;
	return 0;
}


// ----------------------------------------------------------------------------
//  RecordingDeviceName
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::RecordingDeviceName(
	uint16_t index,
	char name[kAdmMaxDeviceNameSize],
	char guid[kAdmMaxGuidSize])
{

	uint16_t nDevices(RecordingDevices());

	// Special fix for the case when the user asks for the name of the default device.
	//
	if (index == (uint16_t)(-1))
	{
		index = 0;
	}

	if ((index > (nDevices - 1)) || (name == NULL))
	{
		return -1;
	}

	memset(name, 0, kAdmMaxDeviceNameSize);

	if (guid != NULL)
	{
		memset(guid, 0, kAdmMaxGuidSize);
	}

	WAVEINCAPSW caps;    // szPname member (product name (NULL terminated) is a WCHAR
	MMRESULT res;

	res = waveInGetDevCapsW(index, &caps, sizeof(WAVEINCAPSW));
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("waveInGetDevCapsW() failed (err=%d)", res);
		return -1;
	}
	if (WideCharToMultiByte(CP_UTF8, 0, caps.szPname, -1, name, kAdmMaxDeviceNameSize, NULL, NULL) == 0)
	{
		LOGE("WideCharToMultiByte(CP_UTF8) failed with error code %d - 1", GetLastError());
	}

	if (guid == NULL)
	{
		return 0;
	}

	// It is possible to get the unique endpoint ID string using the Wave API.
	// However, it is only supported on Windows Vista and Windows 7.

	size_t cbEndpointId(0);

	// Get the size (including the terminating null) of the endpoint ID string of the waveOut device.
	// Windows Vista supports the DRV_QUERYFUNCTIONINSTANCEIDSIZE and DRV_QUERYFUNCTIONINSTANCEID messages.
	res = waveInMessage((HWAVEIN)IntToPtr(index),
		DRV_QUERYFUNCTIONINSTANCEIDSIZE,
		(DWORD_PTR)&cbEndpointId, NULL);
	if (res != MMSYSERR_NOERROR)
	{
		// DRV_QUERYFUNCTIONINSTANCEIDSIZE is not supported <=> earlier version of Windows than Vista
		LOGI("waveInMessage(DRV_QUERYFUNCTIONINSTANCEIDSIZE) failed (err=%d)", res);
		TraceWaveInError(res);
		// Best we can do is to copy the friendly name and use it as guid
		if (WideCharToMultiByte(CP_UTF8, 0, caps.szPname, -1, guid, kAdmMaxGuidSize, NULL, NULL) == 0)
		{
			LOGE("WideCharToMultiByte(CP_UTF8) failed with error code %d - 2", GetLastError());
		}
		return 0;
	}

	// waveOutMessage(DRV_QUERYFUNCTIONINSTANCEIDSIZE) worked => we are on a Vista or Windows 7 device

	WCHAR *pstrEndpointId = NULL;
	pstrEndpointId = (WCHAR*)CoTaskMemAlloc(cbEndpointId);

	// Get the endpoint ID string for this waveOut device.
	res = waveInMessage((HWAVEIN)IntToPtr(index),
		DRV_QUERYFUNCTIONINSTANCEID,
		(DWORD_PTR)pstrEndpointId,
		cbEndpointId);
	if (res != MMSYSERR_NOERROR)
	{
		LOGI("waveInMessage(DRV_QUERYFUNCTIONINSTANCEID) failed (err=%d)", res);
		TraceWaveInError(res);
		// Best we can do is to copy the friendly name and use it as guid
		if (WideCharToMultiByte(CP_UTF8, 0, caps.szPname, -1, guid, kAdmMaxGuidSize, NULL, NULL) == 0)
		{
			LOGE("WideCharToMultiByte(CP_UTF8) failed with error code %d - 3", GetLastError());
		}
		CoTaskMemFree(pstrEndpointId);
		return 0;
	}

	if (WideCharToMultiByte(CP_UTF8, 0, pstrEndpointId, -1, guid, kAdmMaxGuidSize, NULL, NULL) == 0)
	{
		LOGE("WideCharToMultiByte(CP_UTF8) failed with error code %d - 4", GetLastError());
	}
	CoTaskMemFree(pstrEndpointId);

	return 0;
}

// ----------------------------------------------------------------------------
//  RecordingDevices
// ----------------------------------------------------------------------------

int16_t WinWaveAudioCapture::RecordingDevices()
{
	return (waveInGetNumDevs());
}

// ----------------------------------------------------------------------------
//  SetRecordingDevice I (II)
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::SetRecordingDevice(uint16_t index)
{
	if (_recIsInitialized)
	{
		return -1;
	}

	UINT nDevices = waveInGetNumDevs();
	LOGI("number of availiable waveform-audio input devices is %u", nDevices);

	if (index < 0 || index >(nDevices - 1))
	{
		LOGE("device index is out of range [0,%u]", (nDevices - 1));
		return -1;
	}

	_usingInputDeviceIndex = true;
	_inputDeviceIndex = index;
	_inputDeviceIsSpecified = true;

	return 0;
}

// ----------------------------------------------------------------------------
//  SetRecordingDevice II (II)
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::SetRecordingDevice(WindowsDeviceType device)
{
	if (device == kDefaultDevice)
	{
	}
	else if (device == kDefaultCommunicationDevice)
	{
	}

	if (_recIsInitialized)
	{
		return -1;
	}

	_usingInputDeviceIndex = false;
	_inputDevice = device;
	_inputDeviceIsSpecified = true;

	return 0;
}


// ----------------------------------------------------------------------------
//  RecordingIsAvailable
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::RecordingIsAvailable(bool& available)
{
	available = false;

	// Try to initialize the recording side
	int32_t res = InitRecording();

	// Cancel effect of initialization
	StopRecording();

	if (res != -1)
	{
		available = true;
	}

	return 0;
}

// ----------------------------------------------------------------------------
//  InitRecording
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::InitRecording()
{
	if (_recording)
	{
		return -1;
	}

	if (!_inputDeviceIsSpecified)
	{
		return -1;
	}

	if (_recIsInitialized)
	{
		return 0;
	}

	// Initialize the microphone (devices might have been added or removed)
	if (InitMicrophone() == -1)
	{
		LOGW("InitMicrophone() failed");
	}

	// Enumerate all availiable input devices
	EnumerateRecordingDevices();

	// Start by closing any existing wave-input devices
	//
	MMRESULT res(MMSYSERR_ERROR);

	if (_hWaveIn != NULL)
	{
		res = waveInClose(_hWaveIn);
		if (MMSYSERR_NOERROR != res)
		{
			LOGW("waveInClose() failed (err=%d)", res);
			TraceWaveInError(res);
		}
	}

	// Set the input wave format
	//
	WAVEFORMATEX waveFormat;

	waveFormat.wFormatTag = WAVE_FORMAT_PCM;
	waveFormat.nChannels = _recChannels;  // mono <=> 1, stereo <=> 2
	waveFormat.nSamplesPerSec = sampleRate_;
	waveFormat.wBitsPerSample = 16;
	waveFormat.nBlockAlign = waveFormat.nChannels * (waveFormat.wBitsPerSample / 8);
	waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
	waveFormat.cbSize = 0;

	// Open the given waveform-audio input device for recording
	//
	HWAVEIN hWaveIn(NULL);

	if (IsUsingInputDeviceIndex())
	{
		// verify settings first
		res = waveInOpen(NULL, _inputDeviceIndex, &waveFormat, 0, 0, CALLBACK_NULL | WAVE_FORMAT_QUERY);
		if (MMSYSERR_NOERROR == res)
		{
			// open the given waveform-audio input device for recording
			res = waveInOpen(&hWaveIn, _inputDeviceIndex, &waveFormat, (DWORD_PTR)waveInProc, (DWORD_PTR) this, CALLBACK_FUNCTION);
			LOGI("opening input device corresponding to device ID %u", _inputDeviceIndex);
		}
	}
	else
	{
		if (_inputDevice == kDefaultCommunicationDevice)
		{
			// check if it is possible to open the default communication device (supported on Windows 7)
			res = waveInOpen(NULL, WAVE_MAPPER, &waveFormat, 0, 0, CALLBACK_NULL | WAVE_MAPPED_DEFAULT_COMMUNICATION_DEVICE | WAVE_FORMAT_QUERY);
			if (MMSYSERR_NOERROR == res)
			{
				// if so, open the default communication device for real
				res = waveInOpen(&hWaveIn, WAVE_MAPPER, &waveFormat, (DWORD_PTR)waveInProc, (DWORD_PTR) this, CALLBACK_FUNCTION | WAVE_MAPPED_DEFAULT_COMMUNICATION_DEVICE);
				LOGI("opening default communication device");
			}
			else
			{
				// use default device since default communication device was not avaliable
				res = waveInOpen(&hWaveIn, WAVE_MAPPER, &waveFormat, (DWORD_PTR)waveInProc, (DWORD_PTR) this, CALLBACK_FUNCTION);
				LOGI("unable to open default communication device => using default instead");
			}
		}
		else if (_inputDevice == kDefaultDevice)
		{
			// open default device since it has been requested
			res = waveInOpen(NULL, WAVE_MAPPER, &waveFormat, 0, 0, CALLBACK_NULL | WAVE_FORMAT_QUERY);
			if (MMSYSERR_NOERROR == res)
			{
				res = waveInOpen(&hWaveIn, WAVE_MAPPER, &waveFormat, (DWORD_PTR)waveInProc, (DWORD_PTR) this, CALLBACK_FUNCTION);
				LOGI("opening default input device");
			}
		}
	}

	if (MMSYSERR_NOERROR != res)
	{
		LOGE("waveInOpen() failed (err=%d)", res);
		TraceWaveInError(res);
		return -1;
	}

	// Log information about the aquired input device
	//
	WAVEINCAPS caps;

	res = waveInGetDevCaps((UINT_PTR)hWaveIn, &caps, sizeof(WAVEINCAPS));
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("waveInGetDevCaps() failed (err=%d)", res);
		TraceWaveInError(res);
	}

	UINT deviceID(0);
	res = waveInGetID(hWaveIn, &deviceID);
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("waveInGetID() failed (err=%d)", res);
		TraceWaveInError(res);
	}
	LOGI("utilized device ID : %u", deviceID);
	LOGI("product name       : %s", caps.szPname);

	// Store valid handle for the open waveform-audio input device
	_hWaveIn = hWaveIn;

	// Store the input wave header as well
	_waveFormatIn = waveFormat;

	_recBufIndex = 0;
	_recBufCount = 0;

	createAudioCaptureThread();

	// Mark recording side as initialized
	_recIsInitialized = true;

	return 0;
}

// ----------------------------------------------------------------------------
//  StartRecording
// ----------------------------------------------------------------------------

int WinWaveAudioCapture::StartRecording()
{
	if (!_initialized) return -1;

	if (!_recIsInitialized)
	{
		int32_t ret = SetRecordingDevice(0);
		if (ret) {
			LOGE("SetRecordingDevice Fail");
			return -1;
		}

		ret = InitRecording();
		if (ret) {
			LOGE("InitRecording Fail");
			return -1;
		}
	}

	if (!_recIsInitialized)
	{
		return -1;
	}

	if (_recording)
	{
		return 0;
	}

	if (PrepareStartRecording() == 0)
	{
		_recording = true;
	}
	else
	{
		LOGE("failed to activate recording");
		return -1;
	}

	return 0;
}

// ----------------------------------------------------------------------------
//  StopRecording
// ----------------------------------------------------------------------------

int WinWaveAudioCapture::StopRecording()
{
	if (!_initialized) return -1;

	if (!_recIsInitialized)
	{
		return 0;
	}

	deleteAudioCaptureThread();

	if (_hWaveIn == NULL)
	{
		return -1;
	}

	bool wasRecording = _recording;
	_recIsInitialized = false;
	_recording = false;

	MMRESULT res;

	// Stop waveform-adio input. If there are any buffers in the queue, the
	// current buffer will be marked as done (the dwBytesRecorded member in
	// the header will contain the length of data), but any empty buffers in
	// the queue will remain there.
	//
	res = waveInStop(_hWaveIn);
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("waveInStop() failed (err=%d)", res);
		TraceWaveInError(res);
	}

	// Stop input on the given waveform-audio input device and resets the current
	// position to zero. All pending buffers are marked as done and returned to
	// the application.
	//
	res = waveInReset(_hWaveIn);
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("waveInReset() failed (err=%d)", res);
		TraceWaveInError(res);
	}

	// Clean up the preparation performed by the waveInPrepareHeader function.
	// Only unprepare header if recording was ever started (and headers are prepared).
	//
	if (wasRecording)
	{
		LOGI("waveInUnprepareHeader() will be performed");
		for (int n = 0; n < N_BUFFERS_IN; n++)
		{
			res = waveInUnprepareHeader(_hWaveIn, &_waveHeaderIn[n], sizeof(WAVEHDR));
			if (MMSYSERR_NOERROR != res)
			{
				LOGW("waveInUnprepareHeader() failed (err=%d)", res);
				TraceWaveInError(res);
			}
		}
	}

	// Close the given waveform-audio input device.
	//
	res = waveInClose(_hWaveIn);
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("waveInClose() failed (err=%d)", res);
		TraceWaveInError(res);
	}

	// Set the wave input handle to NULL
	//
	_hWaveIn = NULL;
	LOGI("_hWaveIn is now set to NULL");

	return 0;
}

// ----------------------------------------------------------------------------
//  RecordingIsInitialized
// ----------------------------------------------------------------------------

bool WinWaveAudioCapture::RecordingIsInitialized() const
{
	return (_recIsInitialized);
}

// ----------------------------------------------------------------------------
//  Recording
// ----------------------------------------------------------------------------

bool WinWaveAudioCapture::Recording()
{
	return (_recording);
}

// ----------------------------------------------------------------------------
//  EnumerateRecordingDevices
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::EnumerateRecordingDevices()
{
	uint16_t nDevices(RecordingDevices());
	LOGI("===============================================================");
	LOGI("#input devices: %u", nDevices);

	WAVEINCAPS caps;
	MMRESULT res;

	for (UINT deviceID = 0; deviceID < nDevices; deviceID++)
	{
		res = waveInGetDevCaps(deviceID, &caps, sizeof(WAVEINCAPS));
		if (res != MMSYSERR_NOERROR)
		{
			LOGW("waveInGetDevCaps() failed (err=%d)", res);
		}

		LOGI("===============================================================");
		LOGI("Device ID %u:", deviceID);
		LOGI("manufacturer ID      : %u", caps.wMid);
		LOGI("product ID           : %u", caps.wPid);
		LOGI("version of driver    : %u.%u", HIBYTE(caps.vDriverVersion), LOBYTE(caps.vDriverVersion));
		LOGI("product name         : %s", caps.szPname);
		LOGI("dwFormats            : 0x%x", caps.dwFormats);
		if (caps.dwFormats & WAVE_FORMAT_4S16 || caps.dwFormats & WAVE_FORMAT_44S16)
		{
			LOGI("44.1kHz,stereo,16bit : SUPPORTED");
		}
		else
		{
			LOGW("44.1kHz,stereo,16bit  : *NOT* SUPPORTED");
		}
		if (caps.dwFormats & WAVE_FORMAT_4M16 || caps.dwFormats & WAVE_FORMAT_44M16)
		{
			LOGI("44.1kHz,mono,16bit   : SUPPORTED");
		}
		else
		{
			LOGW("44.1kHz,mono,16bit    : *NOT* SUPPORTED");
		}
		LOGI("wChannels            : %u", caps.wChannels);
	}

	return 0;
}


// ----------------------------------------------------------------------------
//  TraceSupportFlags
// ----------------------------------------------------------------------------

void WinWaveAudioCapture::TraceSupportFlags(DWORD dwSupport) const
{
	TCHAR buf[256];

	StringCchPrintf(buf, 128, TEXT("support flags        : 0x%x "), dwSupport);

	if (dwSupport & WAVECAPS_PITCH)
	{
		// supports pitch control
		StringCchCat(buf, 256, TEXT("(PITCH)"));
	}
	if (dwSupport & WAVECAPS_PLAYBACKRATE)
	{
		// supports playback rate control
		StringCchCat(buf, 256, TEXT("(PLAYBACKRATE)"));
	}
	if (dwSupport & WAVECAPS_VOLUME)
	{
		// supports volume control
		StringCchCat(buf, 256, TEXT("(VOLUME)"));
	}
	if (dwSupport & WAVECAPS_LRVOLUME)
	{
		// supports separate left and right volume control
		StringCchCat(buf, 256, TEXT("(LRVOLUME)"));
	}
	if (dwSupport & WAVECAPS_SYNC)
	{
		// the driver is synchronous and will block while playing a buffer
		StringCchCat(buf, 256, TEXT("(SYNC)"));
	}
	if (dwSupport & WAVECAPS_SAMPLEACCURATE)
	{
		// returns sample-accurate position information
		StringCchCat(buf, 256, TEXT("(SAMPLEACCURATE)"));
	}

	LOGI("%S", buf);
}


// ----------------------------------------------------------------------------
//  TraceWaveInError
// ----------------------------------------------------------------------------

void WinWaveAudioCapture::TraceWaveInError(MMRESULT error) const
{
	TCHAR buf[MAXERRORLENGTH];
	TCHAR msg[MAXERRORLENGTH];

	StringCchPrintf(buf, MAXERRORLENGTH, TEXT("Error details: "));
	waveInGetErrorText(error, msg, MAXERRORLENGTH);
	StringCchCat(buf, MAXERRORLENGTH, msg);
	LOGI("%S", buf);
}


// ----------------------------------------------------------------------------
//  PrepareStartRecording
// ----------------------------------------------------------------------------

int32_t WinWaveAudioCapture::PrepareStartRecording()
{
	if (_hWaveIn == NULL)
	{
		return -1;
	}

	MMRESULT res;
	MMTIME mmtime;
	mmtime.wType = TIME_SAMPLES;

	res = waveInGetPosition(_hWaveIn, &mmtime, sizeof(mmtime));
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("waveInGetPosition(TIME_SAMPLES) failed (err=%d)", res);
		TraceWaveInError(res);
	}
	else {
		_read_samples = mmtime.u.sample;
		LOGD("Before WaveInStart, have samples:%d", _read_samples);
	}

	for (int n = 0; n < N_BUFFERS_IN; n++)
	{
		const uint8_t nBytesPerSample = 2 * _recChannels;

		// set up the input wave header
		_waveHeaderIn[n].lpData = reinterpret_cast<LPSTR>(_recBuffer[n]);
		_waveHeaderIn[n].dwBufferLength = nBytesPerSample * (sampleRate_ / 100);
		_waveHeaderIn[n].dwFlags = 0;
		_waveHeaderIn[n].dwBytesRecorded = 0;
		_waveHeaderIn[n].dwUser = 0;

		memset(_recBuffer[n], 0, nBytesPerSample * (sampleRate_ / 100));

		// prepare a buffer for waveform-audio input
		res = waveInPrepareHeader(_hWaveIn, &_waveHeaderIn[n], sizeof(WAVEHDR));
		if (MMSYSERR_NOERROR != res)
		{
			LOGW("waveInPrepareHeader(%d) failed (err=%d)", n, res);
			TraceWaveInError(res);
		}

		// send an input buffer to the given waveform-audio input device
		res = waveInAddBuffer(_hWaveIn, &_waveHeaderIn[n], sizeof(WAVEHDR));
		if (MMSYSERR_NOERROR != res)
		{
			LOGW("waveInAddBuffer(%d) failed (err=%d)", n, res);
			TraceWaveInError(res);
		}
	}

	// start input on the given waveform-audio input device
	res = waveInStart(_hWaveIn);
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("waveInStart() failed (err=%d)", res);
		TraceWaveInError(res);
	}

	return 0;
}

void CALLBACK WinWaveAudioCapture::waveInProc(
	HWAVEIN   hwi,
	UINT      uMsg,
	DWORD_PTR dwInstance,
	DWORD_PTR dwParam1,
	DWORD_PTR dwParam2
)
{
	if (uMsg != WIM_DATA) return;

	WinWaveAudioCapture *thiz = (WinWaveAudioCapture *)dwInstance;
	if (thiz)
	{
		thiz->handleWaveInProc();
	}
}

void WinWaveAudioCapture::handleWaveInProc()
{
	pthread_mutex_lock(&mLock);

	_recBufCount++;

	pthread_mutex_unlock(&mLock);

	pthread_cond_signal(&mCondition);
}

void WinWaveAudioCapture::createAudioCaptureThread()
{
	mIsBreakThread = false;
	pthread_create(&mThread, NULL, handleAudioCaptureThread, this);
}

void* WinWaveAudioCapture::handleAudioCaptureThread(void* ptr)
{
	WinWaveAudioCapture *audioCapture = (WinWaveAudioCapture *)ptr;
	audioCapture->audioCaptureThreadMain();

	return NULL;
}

void WinWaveAudioCapture::audioCaptureThreadMain()
{
	while (true)
	{
		pthread_mutex_lock(&mLock);

		if (mIsBreakThread)
		{
			pthread_mutex_unlock(&mLock);

			break;
		}

		if (_recBufCount>0 && fifoSize-cache_len >= (sampleRate_ / 100) * (2 * _recChannels))
		{
			uint8_t* data = (uint8_t*)_waveHeaderIn[_recBufIndex].lpData;
			int size = _waveHeaderIn[_recBufIndex].dwBytesRecorded;

			if (size>fifoSize - write_pos)
			{
				memcpy(fifo + write_pos, data, fifoSize - write_pos);
				memcpy(fifo, data + fifoSize - write_pos, size - (fifoSize - write_pos));
				write_pos = size - (fifoSize - write_pos);
			}
			else
			{
				memcpy(fifo + write_pos, data, size);
				write_pos = write_pos + size;
			}

			cache_len = cache_len + size;
			pthread_mutex_unlock(&mLock);

			MMRESULT res = waveInAddBuffer(_hWaveIn, &(_waveHeaderIn[_recBufIndex]), sizeof(WAVEHDR));
			if (MMSYSERR_NOERROR != res)
			{
				LOGE("waveInAddBuffer(%d) failed (err=%d)", _recBufIndex, res);
				TraceWaveInError(res);

				continue;
			}

			_recBufIndex = (_recBufIndex + 1) % N_BUFFERS_IN;

			pthread_mutex_lock(&mLock);
			_recBufCount--;
			pthread_mutex_unlock(&mLock);
		}
		else {
			pthread_cond_wait(&mCondition, &mLock);
			pthread_mutex_unlock(&mLock);
			continue;
		}
	}
}

void WinWaveAudioCapture::deleteAudioCaptureThread()
{
	pthread_mutex_lock(&mLock);
	mIsBreakThread = true;
	pthread_mutex_unlock(&mLock);

	pthread_cond_signal(&mCondition);

	pthread_join(mThread, NULL);

	mIsBreakThread = false;
}

AudioFrame* WinWaveAudioCapture::frontAudioFrame()
{
	AudioFrame* audioFrame = NULL;

	pthread_mutex_lock(&mLock);

	if (cache_len >= recBufSizeInBytes_) {

		if (recBufSizeInBytes_>fifoSize - read_pos)
		{
			memcpy(mAudioFrame.data, fifo + read_pos, fifoSize - read_pos);
			memcpy(mAudioFrame.data + fifoSize - read_pos, fifo, recBufSizeInBytes_ - (fifoSize - read_pos));
			read_pos = recBufSizeInBytes_ - (fifoSize - read_pos);
		}
		else
		{
			memcpy(mAudioFrame.data, fifo + read_pos, recBufSizeInBytes_);
			read_pos = read_pos + recBufSizeInBytes_;
		}

		mAudioFrame.frameSize = recBufSizeInBytes_;
		mAudioFrame.duration = float(recBufSizeInBytes_ * 1000) / float(sampleRate_ * _recChannels * 2);
		mAudioFrame.pts = MediaTimer::GetNowMediaTimeMS() - _recBufCount * 10 - cache_len * 1000 / sampleRate_ * _recChannels * 2;
		cache_len = cache_len - recBufSizeInBytes_;

		audioFrame = &mAudioFrame;

		pthread_mutex_unlock(&mLock);

		pthread_cond_signal(&mCondition);
	}
	else {
		pthread_mutex_unlock(&mLock);
	}

	return audioFrame;
}

void WinWaveAudioCapture::popAudioFrame()
{

}

void WinWaveAudioCapture::setVolume(float volume)
{
	SetMicrophoneVolume(volume*255);
}
