#ifndef WIN_WAVE_AUDIO_CAPTURE_H
#define WIN_WAVE_AUDIO_CAPTURE_H

#include <memory>
#include "AudioMixerManagerWin.h"

#include "AudioCapture.h"

#include "w32pthreads.h"

#pragma comment( lib, "winmm.lib" )

enum { N_BUFFERS_IN = 16 };

static const int kAdmMaxDeviceNameSize = 128;
static const int kAdmMaxGuidSize = 128;

class WinWaveAudioCapture : public AudioCapture
{
public:
	WinWaveAudioCapture(int sampleRate, int numChannels);
	~WinWaveAudioCapture();

	void setRecBufSizeInBytes(int bufSizeInBytes);

	void setPCMDataOutputer(IPCMDataOutputer *outputer) {};

	// Main initializaton and termination
	int Init(AudioCaptureMode mode = DEFAULT_MODE);
	int Terminate();
	bool Initialized();

	// Audio transport control
	int StartRecording();
	int StopRecording();
	bool Recording();

	void setVolume(float volume);

	void inputAudioFrame(AudioFrame *inAudioFrame) {};
	void inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType) {};

	AudioFrame* frontAudioFrame();
	void popAudioFrame();

private:
	int16_t RecordingDevices();
	int32_t RecordingDeviceName(
		uint16_t index,
		char name[kAdmMaxDeviceNameSize],
		char guid[kAdmMaxGuidSize]);

	int32_t SetRecordingDevice(uint16_t index);
	int32_t SetRecordingDevice(WindowsDeviceType device);

	int32_t RecordingIsAvailable(bool& available);
	int32_t InitRecording();
	bool RecordingIsInitialized() const;

	int32_t InitMicrophone();
	bool MicrophoneIsInitialized() const;

	// Microphone volume controls
	int32_t MicrophoneVolumeIsAvailable(bool& available);
	int32_t SetMicrophoneVolume(uint32_t volume);
	int32_t MicrophoneVolume(uint32_t& volume) const;
	int32_t MaxMicrophoneVolume(uint32_t& maxVolume) const;
	int32_t MinMicrophoneVolume(uint32_t& minVolume) const;
	int32_t MicrophoneVolumeStepSize(uint16_t& stepSize) const;

	// Microphone mute control
	int32_t MicrophoneMuteIsAvailable(bool& available);
	int32_t SetMicrophoneMute(bool enable);
	int32_t MicrophoneMute(bool& enabled) const;

	// Microphone boost control
	int32_t MicrophoneBoostIsAvailable(bool& available);
	int32_t SetMicrophoneBoost(bool enable);
	int32_t MicrophoneBoost(bool& enabled) const;

	int32_t StereoRecordingIsAvailable(bool& available);
	int32_t SetStereoRecording(bool enable);
	int32_t StereoRecording(bool& enabled) const;
private:
	bool IsUsingInputDeviceIndex() const { return _usingInputDeviceIndex; }
	WindowsDeviceType InputDevice() const { return _inputDevice; }
	uint16_t InputDeviceIndex() const { return _inputDeviceIndex; }
private:
	int32_t EnumerateRecordingDevices();
	void TraceSupportFlags(DWORD dwSupport) const;
	void TraceWaveInError(MMRESULT error) const;
	int32_t PrepareStartRecording();
private:
	AudioMixerManagerWin                    _mixerManager;
	bool                                    _usingInputDeviceIndex;
	WindowsDeviceType                       _inputDevice;
	uint16_t                                _inputDeviceIndex;
	bool                                    _inputDeviceIsSpecified;

	WAVEFORMATEX                            _waveFormatIn;

	HWAVEIN                                 _hWaveIn;

	WAVEHDR                                 _waveHeaderIn[N_BUFFERS_IN];

	int8_t                                  *_recBuffer[N_BUFFERS_IN];
private:
	bool                                    _initialized;
	bool                                    _recording;
	bool                                    _recIsInitialized;

	uint32_t                                _minMicVolume;
	uint32_t                                _maxMicVolume;

	uint32_t                                _read_samples;

	uint16_t                                _recBufIndex;          // record buffer index
	uint16_t                                _recBufCount;

private:
	static void CALLBACK waveInProc(
		HWAVEIN   hwi,
		UINT      uMsg,
		DWORD_PTR dwInstance,
		DWORD_PTR dwParam1,
		DWORD_PTR dwParam2
	);

	void handleWaveInProc();

private:
	int sampleRate_;
	int _recChannels;
	int recBufSizeInBytes_;

private:
	pthread_t mThread;
	pthread_cond_t mCondition;
	pthread_mutex_t mLock;
	bool mIsBreakThread;

	void createAudioCaptureThread();
	static void* handleAudioCaptureThread(void* ptr);
	void audioCaptureThreadMain();
	void deleteAudioCaptureThread();

private:
	uint8_t *fifo;
	int fifoSize;

	int write_pos;
	int read_pos;
	int cache_len;

	AudioFrame mAudioFrame;
};

#endif