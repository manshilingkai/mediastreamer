#include "WinGdiDesktopCapture.h"
#include "MediaLog.h"
#include "libyuv.h"
#include "MediaTime.h"

WinGdiDesktopCapture::WinGdiDesktopCapture(int videoWidth, int videoHeight, int videoFps, HWND devId)
{
	mDeviceId = devId;
	mVideoWidth = videoWidth;
	mVideoHeight = videoHeight;
	mVideoFps = videoFps;

	initialized_ = false;
	recording_ = false;

	pthread_mutex_init(&mLock, NULL);
	pthread_cond_init(&mCondition, NULL);

	isBreakThread = false;

	mVideoCaptureEventTimer_StartTime = 0;
	mVideoCaptureEventTimer_EndTime = 0;
	mVideoCaptureEventTime = 0;
	mVideoDelayTimeUs = 0;

	sourceCaptureBuffer = NULL;
	sourceCaptureBufferSize = 0;
	targetCaptureBuffer = NULL;
	targetCaptureBufferSize = 0;

	pthread_mutex_init(&mMediaStreamerLock, NULL);
	mMediaStreamer = NULL;
}

WinGdiDesktopCapture::~WinGdiDesktopCapture()
{
	Terminate();

	pthread_mutex_destroy(&mMediaStreamerLock);

	pthread_mutex_destroy(&mLock);
	pthread_cond_destroy(&mCondition);
}

int WinGdiDesktopCapture::Init()
{
	if (initialized_) return 0;

	if (mDeviceId == NULL)
	{
		mDeviceId = ::GetDesktopWindow();
	}
	else {
		BringWindowToTop(mDeviceId);
		SetForegroundWindow(mDeviceId);
	}

	isBreakThread = false;

	mVideoCaptureEventTimer_StartTime = 0;
	mVideoCaptureEventTimer_EndTime = 0;
	mVideoCaptureEventTime = 0;
	mVideoDelayTimeUs = 0;

	if (sourceCaptureBuffer)
	{
		free(sourceCaptureBuffer);
		sourceCaptureBuffer = NULL;
	}
	sourceCaptureBufferSize = 0;

	if (targetCaptureBuffer)
	{
		free(targetCaptureBuffer);
		targetCaptureBuffer = NULL;
	}
	targetCaptureBufferSize = mVideoWidth * mVideoHeight * 4;
	targetCaptureBuffer = (unsigned char*)malloc(targetCaptureBufferSize);

	createVideoCaptureThread();

	initialized_ = true;

	return 0;
}

int WinGdiDesktopCapture::Terminate()
{
	if (!initialized_) return 0;

	deleteVideoCaptureThread();

	if (sourceCaptureBuffer)
	{
		free(sourceCaptureBuffer);
		sourceCaptureBuffer = NULL;
	}
	sourceCaptureBufferSize = 0;

	if (targetCaptureBuffer)
	{
		free(targetCaptureBuffer);
		targetCaptureBuffer = NULL;
	}
	targetCaptureBufferSize = 0;

	initialized_ = false;

	return 0;
}

bool WinGdiDesktopCapture::Initialized()
{
	return initialized_;
}

int WinGdiDesktopCapture::StartRecording()
{
	if (!initialized_) return -1;

	pthread_mutex_lock(&mLock);
	recording_ = true;
	pthread_mutex_unlock(&mLock);

	pthread_cond_signal(&mCondition);

	return 0;
}

int WinGdiDesktopCapture::StopRecording()
{
	if (!initialized_) return -1;

	pthread_mutex_lock(&mLock);
	recording_ = false;
	pthread_mutex_unlock(&mLock);

	pthread_cond_signal(&mCondition);

	return 0;
}

bool WinGdiDesktopCapture::Recording()
{
	if (!initialized_) return false;

	bool isRecording = false;

	pthread_mutex_lock(&mLock);
	isRecording = recording_;
	pthread_mutex_unlock(&mLock);

	return isRecording;
}

void WinGdiDesktopCapture::linkPreview(void* display)
{

}

void WinGdiDesktopCapture::unlinkPreview()
{

}

void WinGdiDesktopCapture::onPreviewResize(int w, int h)
{

}

void WinGdiDesktopCapture::linkMediaStreamer(MediaStreamer* mediaStreamer)
{
	pthread_mutex_lock(&mMediaStreamerLock);

	mMediaStreamer = mediaStreamer;

	pthread_mutex_unlock(&mMediaStreamerLock);
}

void WinGdiDesktopCapture::unlinkMediaStreamer()
{
	pthread_mutex_lock(&mMediaStreamerLock);

	mMediaStreamer = NULL;

	pthread_mutex_unlock(&mMediaStreamerLock);
}

void WinGdiDesktopCapture::createVideoCaptureThread()
{
	pthread_create(&mThread, NULL, handleVideoCaptureThread, this);
}

void* WinGdiDesktopCapture::handleVideoCaptureThread(void* ptr)
{
	WinGdiDesktopCapture *audioCapture = (WinGdiDesktopCapture *)ptr;
	audioCapture->VideoCaptureThreadMain();

	return NULL;
}

void WinGdiDesktopCapture::VideoCaptureThreadMain()
{
	bool isIdle = false;
	while (true) {
		pthread_mutex_lock(&mLock);
		if (isBreakThread) {
			pthread_mutex_unlock(&mLock);
			break;
		}
		if (!recording_) {
			pthread_cond_wait(&mCondition, &mLock);
			pthread_mutex_unlock(&mLock);
			continue;
		}

		if (isIdle) {
			pthread_cond_wait(&mCondition, &mLock);
			pthread_mutex_unlock(&mLock);
			continue;
		}
		pthread_mutex_unlock(&mLock);

		mVideoCaptureEventTimer_StartTime = GetNowUs();
		int ret = flowing();
		mVideoCaptureEventTimer_EndTime = GetNowUs();
		mVideoCaptureEventTime = mVideoCaptureEventTimer_EndTime - mVideoCaptureEventTimer_StartTime;
		if (!ret) {
			int64_t idleTimeUs = 1000 * 1000 / mVideoFps - mVideoCaptureEventTime - mVideoDelayTimeUs;

			if (idleTimeUs >= 0) {
				mVideoDelayTimeUs = 0;
			}
			else {
				mVideoDelayTimeUs = -idleTimeUs;
			}

			if (idleTimeUs>0) {
				pthread_mutex_lock(&mLock);
				int64_t reltime = idleTimeUs * 1000ll;
				struct timespec ts;
				ts.tv_sec = reltime / 1000000000ll;
				ts.tv_nsec = reltime % 1000000000ll;
				pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
				pthread_mutex_unlock(&mLock);
			}
			continue;
		}
		else {
			LOGE("Capture Video Fail");
			isIdle = true;
			continue;
		}
	}

	if (sourceCaptureBuffer)
	{
		free(sourceCaptureBuffer);
		sourceCaptureBuffer = NULL;
	}
	sourceCaptureBufferSize = 0;
}

void WinGdiDesktopCapture::deleteVideoCaptureThread()
{
	pthread_mutex_lock(&mLock);
	isBreakThread = true;
	pthread_mutex_unlock(&mLock);

	pthread_cond_signal(&mCondition);

	pthread_join(mThread, NULL);
}

int WinGdiDesktopCapture::flowing()
{
	int ret = 0;

	if (::IsWindow(mDeviceId))
	{
		HDC hdc = GetWindowDC(mDeviceId);
		if (hdc)
		{
			HDC hdcMem = CreateCompatibleDC(hdc);
			if (hdcMem)
			{
				RECT rc;
				GetWindowRect(mDeviceId, &rc);
				int windowWidth = rc.right - rc.left;
				int windowHeight = rc.bottom - rc.top;

				HBITMAP hbitmap = CreateCompatibleBitmap(hdc, windowWidth, windowHeight);
				if (hbitmap)
				{
					HBITMAP hOldBmp = static_cast<HBITMAP>(SelectObject(hdcMem, hbitmap));
					if (GetDesktopWindow() == mDeviceId)
					{
						//StretchBlt(hdcMem,  0, 0, windowWidth, windowHeight, hdc, 0, windowHeight-1, windowWidth, -windowHeight, SRCCOPY);//Rotate 180
						BitBlt(hdcMem, 0, 0, windowWidth, windowHeight, hdc, 0, 0, SRCCOPY | CAPTUREBLT);
					}
					else
					{
						PrintWindow(mDeviceId, hdcMem, 0);
					}

					// draw cursor
					CURSORINFO pci;
					pci.cbSize = sizeof(CURSORINFO);
					if (GetCursorInfo(&pci))
					{
						DrawIconEx(hdcMem,
							pci.ptScreenPos.x,
							pci.ptScreenPos.y,
							pci.hCursor,
							32,
							32,
							0,
							0,
							DI_DEFAULTSIZE | DI_NORMAL);
					}

					SelectObject(hdcMem, hOldBmp);
					BITMAP bmp;
					::GetObject(hbitmap, sizeof(bmp), &bmp);
					int bytesPixel = bmp.bmBitsPixel / 8;

					BITMAPINFO bih;
					bih.bmiHeader.biSize = sizeof(bih.bmiHeader);
					bih.bmiHeader.biWidth = bmp.bmWidth;
					bih.bmiHeader.biHeight = bmp.bmHeight;
					bih.bmiHeader.biPlanes = 1;
					bih.bmiHeader.biBitCount = bmp.bmBitsPixel;
					bih.bmiHeader.biCompression = BI_RGB;
					bih.bmiHeader.biSizeImage = bmp.bmWidth * bytesPixel * bmp.bmHeight;
					bih.bmiHeader.biClrUsed = 0;
					bih.bmiHeader.biClrImportant = 0;

					int sourceWidth = bmp.bmWidth;
					int sourceHeight = bmp.bmHeight;

					if (sourceCaptureBufferSize < bih.bmiHeader.biSizeImage + 1024)
					{
						uint8_t* pBuffer = (uint8_t*)malloc(bih.bmiHeader.biSizeImage + 1024);
						if (pBuffer)
						{
							if (sourceCaptureBuffer)
							{
								free(sourceCaptureBuffer);
								sourceCaptureBuffer = NULL;
							}

							sourceCaptureBuffer = pBuffer;
							sourceCaptureBufferSize = bih.bmiHeader.biSizeImage + 1024;
						}
					}

					if (sourceCaptureBufferSize > bih.bmiHeader.biSizeImage)
					{
						::GetBitmapBits(hbitmap, bih.bmiHeader.biSizeImage, sourceCaptureBuffer);

						libyuv::ARGBScale(sourceCaptureBuffer, sourceWidth * 4, sourceWidth, sourceHeight,
							targetCaptureBuffer, mVideoWidth * 4, mVideoWidth, mVideoHeight, libyuv::kFilterBox);

						VideoFrame inVideoFrame;
						inVideoFrame.data = targetCaptureBuffer;
						inVideoFrame.frameSize = mVideoWidth * mVideoHeight * 4;
						inVideoFrame.videoRawType = VIDEOFRAME_RAWTYPE_BGRA;// VIDEOFRAME_RAWTYPE_ARGB;
						inVideoFrame.width = mVideoWidth;
						inVideoFrame.height = mVideoHeight;
						inVideoFrame.isKeepFullContent = true;

						pthread_mutex_lock(&mMediaStreamerLock);
						if (mMediaStreamer!=NULL)
						{
							mMediaStreamer->inputVideoFrame(&inVideoFrame);
						}
						pthread_mutex_unlock(&mMediaStreamerLock);
					}

					DeleteObject(hbitmap);
				}
				else {
					LOGE("CreateCompatibleBitmap Fail!");
					ret = -1;
				}

				DeleteObject(hdcMem);
			}
			else
			{
				LOGE("CreateCompatibleDC Fail!");
				ret = -1;
			}

			ReleaseDC(mDeviceId, hdc);
		}
		else
		{
			LOGE("GetWindowDC Fail!");
			ret = -1;
		}
	}
	else
	{
		LOGE("is not window");
		ret = -1;
	}

	return ret;
}
