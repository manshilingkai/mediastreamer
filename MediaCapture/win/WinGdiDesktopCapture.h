#ifndef WIN_GDI_DESKTOP_CAPTURE_H
#define WIN_GDI_DESKTOP_CAPTURE_H

#include "VideoCapture.h"
#include <windows.h>

#include "w32pthreads.h"

class WinGdiDesktopCapture : public VideoCapture
{
public:
	WinGdiDesktopCapture(int videoWidth, int videoHeight, int videoFps, HWND devId);
	~WinGdiDesktopCapture();

	int Init();
	int Terminate();
	bool Initialized();

	int StartRecording();
	int StopRecording();
	bool Recording();

	void linkPreview(void* display);
	void unlinkPreview();
	void onPreviewResize(int w, int h);

	void linkMediaStreamer(MediaStreamer* mediaStreamer);
	void unlinkMediaStreamer();

	HWND mDeviceId;
	int mVideoWidth;
	int mVideoHeight;
	int mVideoFps;

private:
	bool initialized_;
	bool recording_;

private:
	pthread_mutex_t mLock;
	pthread_cond_t mCondition;

	//method for thread
	void createVideoCaptureThread();
	static void* handleVideoCaptureThread(void* ptr);
	void VideoCaptureThreadMain();
	void deleteVideoCaptureThread();
	pthread_t mThread;
	bool isBreakThread; // critical value

private:
	int64_t mVideoCaptureEventTimer_StartTime;
	int64_t mVideoCaptureEventTimer_EndTime;
	int64_t mVideoCaptureEventTime;
	int64_t mVideoDelayTimeUs;

private:
	int flowing();

	unsigned char* sourceCaptureBuffer;
	unsigned int sourceCaptureBufferSize;
	unsigned char* targetCaptureBuffer;
	unsigned int targetCaptureBufferSize;

private:
	pthread_mutex_t mMediaStreamerLock;
	MediaStreamer* mMediaStreamer;
};

#endif