//
//  DiscreteExternalAudioInput.cpp
//  MediaStreamer
//
//  Created by slklovewyy on 2020/6/15.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "DiscreteExternalAudioInput.h"
#include "MediaLog.h"
#include "MediaTimer.h"

#include "AudioChannelConvert.h"
#include "AudioSampleFormatConvert.h"
#include "AudioEndianConvert.h"
#include "AudioMixer.h"

DiscreteExternalAudioInput::DiscreteExternalAudioInput(int sampleRate, int numChannels)
{
    initialized_ = false;
    recording_ = false;
    pthread_mutex_init(&mRecordingLock, NULL);

    sampleRate_ = sampleRate;
    numChannels_ = numChannels;
    recBufSizeInBytes_ = 0;
    
    float_pcm_data = NULL;
    float_pcm_data_size = 0;
    int16_pcm_data = NULL;
    int16_pcm_data_size = 0;
    
    mResampler = NULL;
    input_rate = 0;
    output_rate = 0;
    
    mPrimaryAudioSourceType = -1;

    pthread_mutex_init(&mAudioFrameQueueLock, NULL);
    mCurrentPtsMs = 0;
    
    mAVAudioFifo = av_audio_fifo_alloc(AV_SAMPLE_FMT_S16, numChannels_, 1);
}

DiscreteExternalAudioInput::~DiscreteExternalAudioInput()
{
    Terminate();
    
    if (mAVAudioFifo) {
        av_audio_fifo_free(mAVAudioFifo);
        mAVAudioFifo = NULL;
    }
    
    pthread_mutex_destroy(&mAudioFrameQueueLock);
    
    if (mResampler) {
        delete mResampler;
        mResampler = NULL;
    }
    
    if (float_pcm_data) {
        delete [] float_pcm_data;
        float_pcm_data = NULL;
    }
    
    if (int16_pcm_data) {
        delete [] int16_pcm_data;
        int16_pcm_data = NULL;
    }
    
    pthread_mutex_destroy(&mRecordingLock);
}

void DiscreteExternalAudioInput::setRecBufSizeInBytes(int bufSizeInBytes)
{
    recBufSizeInBytes_ = bufSizeInBytes;
}

int DiscreteExternalAudioInput::Init(AudioCaptureMode mode)
{
    if (initialized_) {
        LOGW("Already initialized");
        return 0;
    }
    
    mAudioFrame.data = (uint8_t*)malloc(recBufSizeInBytes_);
    mAudioFrame.frameSize = recBufSizeInBytes_;
    mAudioFrame.pts = MediaTimer::GetNowMediaTimeMS();
    mAudioFrame.duration = float(recBufSizeInBytes_*1000)/float(sampleRate_*numChannels_*2);
    memset(mAudioFrame.data, 0, recBufSizeInBytes_);
    
    initialized_ = true;
    
    return 0;
}

int DiscreteExternalAudioInput::Terminate()
{
    if (!initialized_) {
        LOGW("Already terminated");
        return 0;
    }
    
    if(mAudioFrame.data!=NULL)
    {
        free(mAudioFrame.data);
        mAudioFrame.data = NULL;
    }
    
    pthread_mutex_lock(&mAudioFrameQueueLock);
    while (!mAudioFrameQueue.empty()) {
        AudioFrame* inAudioFrame = mAudioFrameQueue.front();
        mAudioFrameQueue.pop();
        if (inAudioFrame->data) {
            free(inAudioFrame->data);
            inAudioFrame->data = NULL;
        }
        delete inAudioFrame;
        inAudioFrame = NULL;
    }
    pthread_mutex_unlock(&mAudioFrameQueueLock);
    
    initialized_ = false;
    
    return 0;
}

int DiscreteExternalAudioInput::StartRecording()
{
    if (recording_) {
        LOGW("Recording already started");
        return 0;
    }
    
    pthread_mutex_lock(&mRecordingLock);
    recording_ = true;
    pthread_mutex_unlock(&mRecordingLock);
    
    return 0;
}

int DiscreteExternalAudioInput::StopRecording()
{
    if (!recording_) {
        LOGW("Recording already stoped");
        return 0;
    }
    
    pthread_mutex_lock(&mRecordingLock);
    recording_ = false;
    pthread_mutex_unlock(&mRecordingLock);
    
    pthread_mutex_lock(&mAudioFrameQueueLock);
    while (!mAudioFrameQueue.empty()) {
        AudioFrame* inAudioFrame = mAudioFrameQueue.front();
        mAudioFrameQueue.pop();
        if (inAudioFrame->data) {
            free(inAudioFrame->data);
            inAudioFrame->data = NULL;
        }
        delete inAudioFrame;
        inAudioFrame = NULL;
    }
    pthread_mutex_unlock(&mAudioFrameQueueLock);
    
    return 0;
}

void DiscreteExternalAudioInput::inputAudioFrame(AudioFrame *inAudioFrame)
{
    pthread_mutex_lock(&mRecordingLock);
    if (!recording_) {
        pthread_mutex_unlock(&mRecordingLock);
        return;
    }
    pthread_mutex_unlock(&mRecordingLock);

    pthread_mutex_lock(&mAudioFrameQueueLock);
    AudioFrame *audioFrame = new AudioFrame;
    audioFrame->frameSize = inAudioFrame->frameSize;
    audioFrame->data = (uint8_t *)malloc(audioFrame->frameSize);
    memcpy(audioFrame->data, inAudioFrame->data, audioFrame->frameSize);
    audioFrame->duration = inAudioFrame->duration;
    audioFrame->pts = inAudioFrame->pts;
    audioFrame->sampleRate = inAudioFrame->sampleRate;
    audioFrame->channels = inAudioFrame->channels;
    audioFrame->bitsPerChannel = inAudioFrame->bitsPerChannel;
    audioFrame->isBigEndian = inAudioFrame->isBigEndian;
    mAudioFrameQueue.push(audioFrame);
    pthread_mutex_unlock(&mAudioFrameQueueLock);
}

void DiscreteExternalAudioInput::inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType)
{
    this->inputAudioFrame(inAudioFrame);
}

long DiscreteExternalAudioInput::getLatencyMs()
{
    return av_audio_fifo_size(mAVAudioFifo) * 1000 / sampleRate_;
}

AudioFrame* DiscreteExternalAudioInput::frontAudioFrame()
{
    memset(mAudioFrame.data, 0, recBufSizeInBytes_);
    if (float(MediaTimer::GetNowMediaTimeMS() - mAudioFrame.pts) >= mAudioFrame.duration) {
        mAudioFrame.pts = mAudioFrame.pts+mAudioFrame.duration;
    }else{
        return NULL;
    }

    pthread_mutex_lock(&mAudioFrameQueueLock);
    while (!mAudioFrameQueue.empty()) {
        AudioFrame* inAudioFrame = mAudioFrameQueue.front();
        mAudioFrameQueue.pop();
        pthread_mutex_unlock(&mAudioFrameQueueLock);
        
        int16_t* out = NULL;
        int out_size = 0;

        if (inAudioFrame->isBigEndian) {
            big_endian_to_little_endian((int16_t*)inAudioFrame->data, inAudioFrame->frameSize/2);
        }
        
        if (inAudioFrame->channels==2 && numChannels_==1) {
            int16_t* stereo = (int16_t*)inAudioFrame->data;
            int samples_per_channel = inAudioFrame->frameSize/2/inAudioFrame->channels;
            
            if (int16_pcm_data==NULL) {
                int16_pcm_data = new int16_t[samples_per_channel];
                int16_pcm_data_size = samples_per_channel;
            }
            if (int16_pcm_data_size<samples_per_channel) {
                if (int16_pcm_data) {
                    delete [] int16_pcm_data;
                    int16_pcm_data = NULL;
                }
                int16_pcm_data = new int16_t[samples_per_channel];
                int16_pcm_data_size = samples_per_channel;
            }
            int16_t* mono = int16_pcm_data;
            int mono_size = samples_per_channel;
            ff_stereo_to_mono(mono, stereo, samples_per_channel);
            
            if (inAudioFrame->sampleRate!=sampleRate_) {
                if (float_pcm_data==NULL) {
                    float_pcm_data = new float[mono_size];
                    float_pcm_data_size = mono_size;
                }
                if (float_pcm_data_size<mono_size) {
                    if (float_pcm_data) {
                        delete [] float_pcm_data;
                        float_pcm_data = NULL;
                    }
                    float_pcm_data = new float[mono_size];
                    float_pcm_data_size = mono_size;
                }
                
                src_short_to_float_array(mono,float_pcm_data,mono_size);
                
                if (mResampler==NULL) {
                    mResampler = new musly::resampler(inAudioFrame->sampleRate, sampleRate_);
                    input_rate = inAudioFrame->sampleRate;
                    output_rate = sampleRate_;
                }
                if (input_rate!=inAudioFrame->sampleRate || output_rate!=sampleRate_) {
                    if (mResampler) {
                        delete mResampler;
                        mResampler = NULL;
                    }
                    
                    mResampler = new musly::resampler(inAudioFrame->sampleRate, sampleRate_);
                    input_rate = inAudioFrame->sampleRate;
                    output_rate = sampleRate_;
                }
                
                std::vector<float> resampled_float_pcm_data = mResampler->resample(float_pcm_data, mono_size);
                size_t resampled_float_pcm_data_size = resampled_float_pcm_data.size();
                if (int16_pcm_data_size<resampled_float_pcm_data_size) {
                    if (int16_pcm_data) {
                        delete [] int16_pcm_data;
                        int16_pcm_data = NULL;
                    }
                    int16_pcm_data = new int16_t[resampled_float_pcm_data_size];
                    int16_pcm_data_size = (int)resampled_float_pcm_data_size;
                }
                    
                src_float_to_short_array(resampled_float_pcm_data.data(), int16_pcm_data, (int)resampled_float_pcm_data_size);
                    
                out = int16_pcm_data;
                out_size = (int)resampled_float_pcm_data_size;
            }else{
                out = mono;
                out_size = mono_size;
            }
            
        }else if(inAudioFrame->channels==1 && numChannels_==2){
            int16_t* mono = (int16_t*)inAudioFrame->data;
            int samples_per_channel = inAudioFrame->frameSize/2/inAudioFrame->channels;
            
            if (int16_pcm_data==NULL) {
                int16_pcm_data = new int16_t[numChannels_*samples_per_channel];
                int16_pcm_data_size = numChannels_*samples_per_channel;
            }
            if (int16_pcm_data_size<numChannels_*samples_per_channel) {
                if (int16_pcm_data) {
                    delete [] int16_pcm_data;
                    int16_pcm_data = NULL;
                }
                int16_pcm_data = new int16_t[numChannels_*samples_per_channel];
                int16_pcm_data_size = numChannels_*samples_per_channel;
            }
            
            int16_t* stereo = int16_pcm_data;
            int stereo_size = numChannels_*samples_per_channel;
            ff_mono_to_stereo(stereo, mono, samples_per_channel);
            
            if (inAudioFrame->sampleRate!=sampleRate_) {
                if (float_pcm_data==NULL) {
                    float_pcm_data = new float[stereo_size];
                    float_pcm_data_size = stereo_size;
                }
                if (float_pcm_data_size<stereo_size) {
                    if (float_pcm_data) {
                        delete [] float_pcm_data;
                        float_pcm_data = NULL;
                    }
                    float_pcm_data = new float[stereo_size];
                    float_pcm_data_size = stereo_size;
                }
                
                src_short_to_float_array(stereo,float_pcm_data,stereo_size);
                
                if (mResampler==NULL) {
                    mResampler = new musly::resampler(inAudioFrame->sampleRate, sampleRate_);
                    input_rate = inAudioFrame->sampleRate;
                    output_rate = sampleRate_;
                }
                if (input_rate!=inAudioFrame->sampleRate || output_rate!=sampleRate_) {
                    if (mResampler) {
                        delete mResampler;
                        mResampler = NULL;
                    }
                    
                    mResampler = new musly::resampler(inAudioFrame->sampleRate, sampleRate_);
                    input_rate = inAudioFrame->sampleRate;
                    output_rate = sampleRate_;
                }
                
                std::vector<float> resampled_float_pcm_data = mResampler->resample(float_pcm_data, stereo_size);
                size_t resampled_float_pcm_data_size = resampled_float_pcm_data.size();
                if (int16_pcm_data_size<resampled_float_pcm_data_size) {
                    if (int16_pcm_data) {
                        delete [] int16_pcm_data;
                        int16_pcm_data = NULL;
                    }
                    int16_pcm_data = new int16_t[resampled_float_pcm_data_size];
                    int16_pcm_data_size = (int)resampled_float_pcm_data_size;
                }
                
                src_float_to_short_array(resampled_float_pcm_data.data(), int16_pcm_data, (int)resampled_float_pcm_data_size);
                
                out = int16_pcm_data;
                out_size = (int)resampled_float_pcm_data_size;
            }else{
                out = stereo;
                out_size = stereo_size;
            }
        }else{
            int16_t* input = (int16_t*)inAudioFrame->data;
            int input_size = inAudioFrame->frameSize/2;
            
            if (inAudioFrame->sampleRate!=sampleRate_) {
                if (float_pcm_data==NULL) {
                    float_pcm_data = new float[input_size];
                    float_pcm_data_size = input_size;
                }
                if (float_pcm_data_size<input_size) {
                    if (float_pcm_data) {
                        delete [] float_pcm_data;
                        float_pcm_data = NULL;
                    }
                    float_pcm_data = new float[input_size];
                    float_pcm_data_size = input_size;
                }
                
                src_short_to_float_array(input,float_pcm_data,input_size);
                
                if (mResampler==NULL) {
                    mResampler = new musly::resampler(inAudioFrame->sampleRate, sampleRate_);
                    input_rate = inAudioFrame->sampleRate;
                    output_rate = sampleRate_;
                }
                if (input_rate!=inAudioFrame->sampleRate || output_rate!=sampleRate_) {
                    if (mResampler) {
                        delete mResampler;
                        mResampler = NULL;
                    }
                    
                    mResampler = new musly::resampler(inAudioFrame->sampleRate, sampleRate_);
                    input_rate = inAudioFrame->sampleRate;
                    output_rate = sampleRate_;
                }
                
                std::vector<float> resampled_float_pcm_data = mResampler->resample(float_pcm_data, input_size);
                size_t resampled_float_pcm_data_size = resampled_float_pcm_data.size();
                if (int16_pcm_data_size<resampled_float_pcm_data_size) {
                    if (int16_pcm_data) {
                        delete [] int16_pcm_data;
                        int16_pcm_data = NULL;
                    }
                    int16_pcm_data = new int16_t[resampled_float_pcm_data_size];
                    int16_pcm_data_size = (int)resampled_float_pcm_data_size;
                }
                
                src_float_to_short_array(resampled_float_pcm_data.data(), int16_pcm_data, (int)resampled_float_pcm_data_size);
                
                out = int16_pcm_data;
                out_size = (int)resampled_float_pcm_data_size;
            }else{
                out = (int16_t*)inAudioFrame->data;
                out_size = inAudioFrame->frameSize/2;
            }
        }
        
        uint8_t* data = (uint8_t*)out;
        int size = out_size*2;
        
        mCurrentPtsMs = inAudioFrame->pts;
        av_audio_fifo_write(mAVAudioFifo, (void**)&data, size/numChannels_/sizeof(int16_t));
        
        if (inAudioFrame->data) {
            free(inAudioFrame->data);
            inAudioFrame->data = NULL;
            delete inAudioFrame;
            inAudioFrame = NULL;
        }
        
        pthread_mutex_lock(&mAudioFrameQueueLock);
    }
    pthread_mutex_unlock(&mAudioFrameQueueLock);

    av_audio_fifo_read(mAVAudioFifo, (void**)&mAudioFrame.data, recBufSizeInBytes_/numChannels_/sizeof(int16_t));

    return &mAudioFrame;
}

void DiscreteExternalAudioInput::popAudioFrame()
{
    //no need do anything.
}
