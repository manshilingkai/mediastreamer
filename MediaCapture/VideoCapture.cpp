//
//  VideoCapture.cpp
//  MediaStreamer
//
//  Created by Think on 2018/8/15.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "VideoCapture.h"

#if defined(WIN) || defined(MAC)
#include "FFmpegVideoCapture.h"
#endif

#ifdef WIN32
#include "WinGdiDesktopCapture.h"
#endif

VideoCapture* VideoCapture::CreateVideoCapture(VIDEO_CAPTURE_TYPE type, char* capturer, int videoWidth, int videoHeight, int videoFps, int sourceId)
{
#if defined(WIN) || defined(MAC)
    return new FFmpegVideoCapture(type, capturer, videoWidth, videoHeight, videoFps, sourceId);
#endif
    
#ifdef WIN32
	if (type==WIN_NATIVE_GDI_DESKTOP_CAPTURE)
	{
		return new WinGdiDesktopCapture(videoWidth, videoHeight, videoFps, NULL);
	}
#endif

    return NULL;
}

void VideoCapture::DeleteVideoCapture(VideoCapture* videoCapture, VIDEO_CAPTURE_TYPE type)
{
#if defined(WIN) || defined(MAC)
    if (type == FFMPEG_DSHOW_VIDEO || type == FFMPEG_GDIGRAB_VIDEO || type == FFMPEG_AVFOUNDATION_VIDEO) {
        FFmpegVideoCapture* ffmpegVideoCapture = (FFmpegVideoCapture*)videoCapture;
        delete ffmpegVideoCapture;
        ffmpegVideoCapture = NULL;
    }
#endif

#ifdef WIN32
	if (type == WIN_NATIVE_GDI_DESKTOP_CAPTURE)
	{
		WinGdiDesktopCapture* winGdiDesktopCapture = (WinGdiDesktopCapture*)videoCapture;
		if (winGdiDesktopCapture)
		{
			delete winGdiDesktopCapture;
			winGdiDesktopCapture = NULL;
		}
	}
#endif
}
