//
//  FFmpegAudioCapture.h
//  MediaStreamer
//
//  Created by Think on 2018/8/15.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef FFmpegAudioCapture_h
#define FFmpegAudioCapture_h

#include <stdio.h>
#include <pthread.h>

#include "AudioCapture.h"

extern "C" {
#include "libavutil/error.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavdevice/avdevice.h"
#include "libavfilter/avfilter.h"
}

#include "FFAudioFilter.h"

class FFmpegAudioCapture : public AudioCapture {
public:
    FFmpegAudioCapture(AUDIO_CAPTURE_TYPE type, char* capturer, int sampleRate, int numChannels);
    ~FFmpegAudioCapture();
    
    void setRecBufSizeInBytes(int bufSizeInBytes);
    
    int Init();
    int Terminate();
    bool Initialized() { return initialized_; }
    
    int StartRecording();
    int StopRecording();
    bool Recording();
    
    void setVolume(float volume);
    
    void inputAudioFrame(AudioFrame *inAudioFrame) {};
    
    AudioFrame* frontAudioFrame();
    void popAudioFrame();
    
private:
    static void initFFEnv();
private:
    //method for thread
    void createFFmpegAudioCaptureThread();
    static void* handleFFmpegAudioCaptureThread(void* ptr);
    void FFmpegAudioCaptureThreadMain();
    void deleteFFmpegAudioCaptureThread();
    pthread_t mThread;
    bool isBreakThread; // critical value
private:
    int64_t getLatency();
    
    int CalculateNumFifoBuffersNeeded();
    
    bool initialized_;
    bool recording_; // critical value
    
    int sampleRate_;
    int numChannels_;
    int recBufSizeInBytes_;
    
    uint8_t *fifo;
    int fifoSize;
    
    void allocBuffers();
    void freeBuffers();
    void flushBuffers();
    
    pthread_mutex_t mLock;
    pthread_cond_t mCondition;
    bool isWaitting;
    
    int write_pos;
    int read_pos;
    int cache_len;
    
    AudioFrame mAudioFrame;
private:
    AUDIO_CAPTURE_TYPE mAudioCaptureType;
    char* mCapturer;
private:
    AVFormatContext *mInputAVFormatContext;
    int mInputAudioStreamIndex;
private:
    FFAudioFilter* mFFAudioFilter;
    float mVolume; // critical value
    bool isNeedUpdateAudioFilter; // critical value
private:
    int flowing();
};

#endif /* FFmpegAudioCapture_h */
