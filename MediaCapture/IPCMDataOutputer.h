//
//  IPCMDataOutputer.h
//  MediaStreamer
//
//  Created by Think on 2019/7/9.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef IPCMDataOutputer_h
#define IPCMDataOutputer_h

#include <stdio.h>
#include <stdint.h>

class IPCMDataOutputer {
public:
    virtual ~IPCMDataOutputer() {}
    virtual void onOutputPCMData(uint8_t * data, int size) = 0;
};

#endif /* IPCMDataOutputer_h */
