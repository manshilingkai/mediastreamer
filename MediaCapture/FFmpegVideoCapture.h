//
//  FFmpegVideoCapture.h
//  MediaStreamer
//
//  Created by Think on 2018/8/17.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef FFmpegVideoCapture_h
#define FFmpegVideoCapture_h

#include <stdio.h>
#include <pthread.h>

#include "VideoCapture.h"

extern "C" {
#include "libavutil/error.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavdevice/avdevice.h"
#include "libavfilter/avfilter.h"
#include "libswscale/swscale.h"
}

extern "C" {
#include <SDL.h>
#include <SDL_thread.h>
}

class FFmpegVideoCapture : public VideoCapture {
public:
    FFmpegVideoCapture(VIDEO_CAPTURE_TYPE type, char* capturer, int videoWidth, int videoHeight, int videoFps, int sourceId);
    ~FFmpegVideoCapture();
    
    int Init();
    int Terminate();
    bool Initialized() { return initialized_; }

    int StartRecording();
    int StopRecording();
    bool Recording();
    
    void linkPreview(void* display);
    void unlinkPreview();
    void onPreviewResize(int w, int h);
    
    void linkMediaStreamer(MediaStreamer* mediaStreamer);
    void unlinkMediaStreamer();
private:
    static void initFFEnv();
private:
    bool initialized_;
    bool recording_;
private:
    int initGDIGrab(char *handleName);
    int initDShow(char *videoCapturerName);
    int initAVFoundation(char *videoCapturerName);
    void initVideoPreProcesserForPreview();
    
    int flowing();
private:
    int mSourceId;
    VIDEO_CAPTURE_TYPE mVideoCaptureType;
    char* mCapturer;
    int mVideoWidth;
    int mVideoHeight;
    int mVideoFps;
    
    pthread_mutex_t mLock;
    pthread_cond_t mCondition;
private:
    AVFormatContext *mInputAVFormatContext;
    int mInputVideoStreamIndex;
private:
    SwsContext *img_convert_ctx_for_preview;
    uint8_t* convertedVideoBufferForPreview;
private:
    //method for thread
    void createFFmpegVideoCaptureThread();
    static void* handleFFmpegVideoCaptureThread(void* ptr);
    void FFmpegVideoCaptureThreadMain();
    void deleteFFmpegVideoCaptureThread();
    pthread_t mThread;
    bool isBreakThread; // critical value
private:
    int64_t mVideoCaptureEventTimer_StartTime;
    int64_t mVideoCaptureEventTimer_EndTime;
    int64_t mVideoCaptureEventTime;
    int64_t mVideoDelayTimeUs;
private:
    pthread_mutex_t mPreviewLock;
    bool isPreviewLinked;
    bool openPreview(void *display);
    void closePreview();
private:
    void* mDisplay;
    SDL_Window *window;
    SDL_Renderer *renderer;
    SDL_Texture *videoTexture;
    int win_width;
    int win_height;
    bool windowSizeUpdated;
private:
    void calculate_display_rect(SDL_Rect *rect,
                                int scr_xleft, int scr_ytop, int scr_width, int scr_height,
                                int pic_width, int pic_height);
    int upload_texture(SDL_Texture **tex, AVFrame *frame);
    int realloc_texture(SDL_Texture **texture, Uint32 new_format, int new_width, int new_height, SDL_BlendMode blendmode, int init_texture);
private:
    pthread_mutex_t mMediaStreamerLock;
    MediaStreamer* mMediaStreamer;
    SwsContext *img_convert_ctx_for_mediastreamer;
    uint8_t* convertedVideoBufferForMediaStreamer;
    int convertedVideoWidthForMediaStreamer;
    int convertedVideoHeightForMediaStreamer;
};

#endif /* FFmpegVideoCapture_h */
