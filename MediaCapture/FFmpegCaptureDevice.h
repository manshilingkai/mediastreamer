//
//  FFmpegCaptureDevice.h
//  MediaStreamer
//
//  Created by Think on 2018/8/27.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef FFmpegCaptureDevice_h
#define FFmpegCaptureDevice_h

#include <stdio.h>
#include <stdlib.h>

#define MAX_FFCAPDEVICE_NUM 32
#define FFCAPDEVICE_TYPE_VIDEO 0
#define FFCAPDEVICE_TYPE_AUDIO 1

#define MAX_FFCAPDEVICE_ABILITY_ITEM_NUM 1024
#define MAX_FFCAPDEVICE_ABILITY_SET_NUM 64

struct FFCapDeviceInfo {
    int types[MAX_FFCAPDEVICE_NUM];    //0-Video device, 1-Audio device
    char *deviceNames[MAX_FFCAPDEVICE_NUM];
    int count;
    char *deviceNameUTFs[MAX_FFCAPDEVICE_NUM];
    bool deviceAvailables[MAX_FFCAPDEVICE_NUM];
    
    FFCapDeviceInfo()
    {
        for(int i = 0; i < MAX_FFCAPDEVICE_NUM; i++)
        {
            types[i] = -1;
            deviceNames[i] = NULL;
            deviceNameUTFs[i] = NULL;
            deviceAvailables[i] = false;
        }
        
        count = 0;
    }
    
    inline void Free()
    {
        for(int i = 0; i < MAX_FFCAPDEVICE_NUM; i++)
        {
            types[i] = -1;
            deviceAvailables[i] = false;
            
            if (deviceNames[i])
            {
                free(deviceNames[i]);
            }
            
            if (deviceNameUTFs[i])
            {
                free(deviceNameUTFs[i]);
            }
            
            deviceNames[i] = NULL;
            deviceNameUTFs[i] = NULL;
        }
        
        count = 0;
    }
};

struct FFCapVideoDeviceAbilityItem {
    int width;
    int height;
    
    int fps;
    
    FFCapVideoDeviceAbilityItem()
    {
        width = 0;
        height = 0;
        
        fps = 0;
    }
};

struct FFCapVideoDeviceAbilitySet {
    int pixel_format;
    FFCapVideoDeviceAbilityItem *videoDeviceAbilityItems[MAX_FFCAPDEVICE_ABILITY_ITEM_NUM];
    int count;
    
    FFCapVideoDeviceAbilitySet()
    {
        pixel_format = -1;
        
        for(int i = 0; i < MAX_FFCAPDEVICE_ABILITY_ITEM_NUM; i++)
        {
            videoDeviceAbilityItems[i] = NULL;
        }
        
        count = 0;
    }
    
    inline void Free()
    {
        pixel_format = -1;

        for(int i = 0; i < MAX_FFCAPDEVICE_ABILITY_ITEM_NUM; i++)
        {
            if(videoDeviceAbilityItems[i])
            {
                delete videoDeviceAbilityItems[i];
                videoDeviceAbilityItems[i] = NULL;
            }
        }
        
        count = 0;
    }
};

struct FFCapVideoDeviceAbility {
    char *deviceName;
    char *deviceNameUTF;
    
    FFCapVideoDeviceAbilitySet *videoDeviceAbilitySets[MAX_FFCAPDEVICE_ABILITY_SET_NUM];
    
    int count;
    
    FFCapVideoDeviceAbility()
    {
        deviceName = NULL;
        deviceNameUTF = NULL;
        
        for (int i = 0; i < MAX_FFCAPDEVICE_ABILITY_SET_NUM; i++)
        {
            videoDeviceAbilitySets[i] = NULL;
        }
        
        count = 0;
    }
    
    inline void Free()
    {
        if (deviceName)
        {
            free(deviceName);
            deviceName = NULL;
        }
        
        if (deviceNameUTF)
        {
            free(deviceNameUTF);
            deviceNameUTF = NULL;
        }
        
        for (int i = 0; i < MAX_FFCAPDEVICE_ABILITY_SET_NUM; i++)
        {
            if (videoDeviceAbilitySets[i])
            {
                videoDeviceAbilitySets[i]->Free();
                delete videoDeviceAbilitySets[i];
                videoDeviceAbilitySets[i] = NULL;
            }
        }
        
        count = 0;
    }
};

class FFCapDevice {
public:
    FFCapDevice();
    ~FFCapDevice();
    
    FFCapDeviceInfo *listFFCapDeviceInfo();
    FFCapVideoDeviceAbility *listFFCapVideoDeviceAbility(char* videoDeviceName);
private:
    static void FFCapDeviceInfoCallBack(void* opaque, int type, char* deviceName);
    void HandleFFCapDeviceInfoCallBack(int type, char* deviceName);
    
    void checkAllDevicesAvailable();
    
    FFCapDeviceInfo *ffCapDeviceInfo;
private:
    static void FFCapVideoDeviceAbilityItemCallBack(void* opaque, int pixel_format, int w, int h, int fps);
    void HandleFFCapVideoDeviceAbilityItemCallBack(int pixel_format, int w, int h, int fps);
    
    FFCapVideoDeviceAbility *ffCapVideoDeviceAbility;
};

#endif /* FFmpegCaptureDevice_h */
