//
//  FFmpegVideoCapture.cpp
//  MediaStreamer
//
//  Created by Think on 2018/8/17.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "FFmpegVideoCapture.h"
#include "MediaLog.h"
#include "MediaTime.h"

FFmpegVideoCapture::FFmpegVideoCapture(VIDEO_CAPTURE_TYPE type, char* capturer, int videoWidth, int videoHeight, int videoFps, int sourceId)
{
    mSourceId = sourceId;
    mVideoCaptureType = type;
    mCapturer = strdup(capturer);
    
    mVideoWidth = videoWidth;
    mVideoHeight = videoHeight;
    mVideoFps = videoFps;
    
    mInputAVFormatContext = NULL;
    mInputVideoStreamIndex = -1;
    
    img_convert_ctx_for_preview = NULL;
    convertedVideoBufferForPreview = NULL;
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mCondition, NULL);
    
    initialized_ = false;
    recording_ = false;
    
    mVideoCaptureEventTimer_StartTime = 0ll;
    mVideoCaptureEventTimer_EndTime = 0ll;
    mVideoCaptureEventTime = 0ll;
    mVideoDelayTimeUs = 0ll;
    
    isBreakThread = false;
    
    pthread_mutex_init(&mPreviewLock, NULL);
    isPreviewLinked = false;
    
    mDisplay = NULL;
    window = NULL;
    renderer = NULL;
    videoTexture = NULL;
    
    win_width = 0;
    win_height = 0;
    windowSizeUpdated = false;
    
    pthread_mutex_init(&mMediaStreamerLock, NULL);
    mMediaStreamer = NULL;
    img_convert_ctx_for_mediastreamer = NULL;
    convertedVideoBufferForMediaStreamer = NULL;
    convertedVideoWidthForMediaStreamer = 0;
    convertedVideoHeightForMediaStreamer = 0;
}

FFmpegVideoCapture::~FFmpegVideoCapture()
{
    pthread_mutex_destroy(&mMediaStreamerLock);
    
    pthread_mutex_destroy(&mPreviewLock);
    
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mCondition);
    
    if (mCapturer) {
        free(mCapturer);
        mCapturer = NULL;
    }
}

void FFmpegVideoCapture::initFFEnv()
{
    av_register_all();
    avfilter_register_all();
    avdevice_register_all();
    
    av_log_set_level(AV_LOG_WARNING);
}

void FFmpegVideoCapture::createFFmpegVideoCaptureThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, &attr, handleFFmpegVideoCaptureThread, this);
    
    pthread_attr_destroy(&attr);
}

void* FFmpegVideoCapture::handleFFmpegVideoCaptureThread(void* ptr)
{
    FFmpegVideoCapture* ffVideoCapture = (FFmpegVideoCapture*)ptr;
    ffVideoCapture->FFmpegVideoCaptureThreadMain();
    
    return NULL;
}

void FFmpegVideoCapture::FFmpegVideoCaptureThreadMain()
{
    bool isIdle = false;
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        if (!recording_) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        if (isIdle) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        mVideoCaptureEventTimer_StartTime = GetNowUs();
        int ret = flowing();
        mVideoCaptureEventTimer_EndTime = GetNowUs();
        mVideoCaptureEventTime = mVideoCaptureEventTimer_EndTime - mVideoCaptureEventTimer_StartTime;
        if (ret==0) {
            int64_t idleTimeUs = 10 * 1000; //10ms

            pthread_mutex_lock(&mLock);
            int64_t reltime = idleTimeUs * 1000ll;
            struct timespec ts;
            ts.tv_sec  = reltime/1000000000ll;
            ts.tv_nsec = reltime%1000000000ll;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
            pthread_mutex_unlock(&mLock);
            
            mVideoDelayTimeUs = mVideoDelayTimeUs + mVideoCaptureEventTime + idleTimeUs;
            continue;
        }else if (ret==1) {
            int64_t idleTimeUs = 1000*1000/mVideoFps-mVideoCaptureEventTime-mVideoDelayTimeUs;
            
            if (idleTimeUs>=0) {
                mVideoDelayTimeUs = 0;
            }else {
                mVideoDelayTimeUs = -idleTimeUs;
            }
            
            if (idleTimeUs>0) {
                pthread_mutex_lock(&mLock);
                int64_t reltime = idleTimeUs * 1000ll;
                struct timespec ts;
                ts.tv_sec  = reltime/1000000000ll;
                ts.tv_nsec = reltime%1000000000ll;
                pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
                pthread_mutex_unlock(&mLock);
            }
            continue;
        }else {
            LOGE("FFmpeg Capture Video Fail");
            isIdle = true;
            continue;
        }
    }
    
    if (img_convert_ctx_for_mediastreamer) {
        if (img_convert_ctx_for_mediastreamer != NULL)
        {
            sws_freeContext(img_convert_ctx_for_mediastreamer);
            img_convert_ctx_for_mediastreamer = NULL;
        }
        if (convertedVideoBufferForMediaStreamer) {
            av_free(convertedVideoBufferForMediaStreamer);
            convertedVideoBufferForMediaStreamer = NULL;
        }
    }
}

void FFmpegVideoCapture::deleteFFmpegVideoCaptureThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

int FFmpegVideoCapture::Init()
{
    if (initialized_) {
        LOGW("Already initialized");
        return 0;
    }
    
    int ret = -1;
    if (mVideoCaptureType==FFMPEG_GDIGRAB_VIDEO) {
        ret = initGDIGrab(mCapturer);
    }else if(mVideoCaptureType==FFMPEG_DSHOW_VIDEO){
        ret = initDShow(mCapturer);
    }else if(mVideoCaptureType==FFMPEG_AVFOUNDATION_VIDEO){
        ret = initAVFoundation(mCapturer);
    }
    
    if (ret) {
        return -1;
    }
    
    initVideoPreProcesserForPreview();
    
    createFFmpegVideoCaptureThread();
    
    initialized_ = true;

    return 0;
}

int FFmpegVideoCapture::Terminate()
{
    if (!initialized_) {
        LOGW("Already terminated");
        return 0;
    }
    
    deleteFFmpegVideoCaptureThread();
    
    if (img_convert_ctx_for_preview != NULL)
    {
        sws_freeContext(img_convert_ctx_for_preview);
        img_convert_ctx_for_preview = NULL;
    }
    if (convertedVideoBufferForPreview) {
        av_free(convertedVideoBufferForPreview);
        convertedVideoBufferForPreview = NULL;
    }
    
    avcodec_close(mInputAVFormatContext->streams[mInputVideoStreamIndex]->codec);
    avformat_close_input(&mInputAVFormatContext);
    avformat_free_context(mInputAVFormatContext);
    
    mInputAVFormatContext = NULL;
    mInputVideoStreamIndex = -1;
    
    initialized_ = false;
    
    return 0;
}

int FFmpegVideoCapture::StartRecording()
{
    pthread_mutex_lock(&mLock);
    recording_ = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    return 0;
}

int FFmpegVideoCapture::StopRecording()
{
    pthread_mutex_lock(&mLock);
    recording_ = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    return 0;
}

bool FFmpegVideoCapture::Recording()
{
    return recording_;
}

void FFmpegVideoCapture::linkPreview(void* display)
{
    pthread_mutex_lock(&mPreviewLock);
    
    closePreview();
    bool ret = openPreview(display);
    
    if (ret)
    {
        isPreviewLinked = true;
    }
    
    pthread_mutex_unlock(&mPreviewLock);
}

void FFmpegVideoCapture::unlinkPreview()
{
    pthread_mutex_lock(&mPreviewLock);
    
    closePreview();
    isPreviewLinked = false;
    
    pthread_mutex_unlock(&mPreviewLock);
}

void FFmpegVideoCapture::onPreviewResize(int w, int h)
{
    pthread_mutex_lock(&mPreviewLock);
    
    if (isPreviewLinked) {
        int windowWidth = 0;
        int windowHeight = 0;
        SDL_GetWindowSize(window, &windowWidth, &windowHeight);
        if (windowWidth!=win_width || windowHeight!=win_height) {
            win_width = windowWidth;
            win_height = windowHeight;
            windowSizeUpdated = true;
        }
    }
    
    pthread_mutex_unlock(&mPreviewLock);
}

void FFmpegVideoCapture::linkMediaStreamer(MediaStreamer* mediaStreamer)
{
    pthread_mutex_lock(&mMediaStreamerLock);
    
    mMediaStreamer = mediaStreamer;
    
    pthread_mutex_unlock(&mMediaStreamerLock);
}

void FFmpegVideoCapture::unlinkMediaStreamer()
{
    pthread_mutex_lock(&mMediaStreamerLock);
    
    mMediaStreamer = NULL;
    
    pthread_mutex_unlock(&mMediaStreamerLock);
}

int FFmpegVideoCapture::initGDIGrab(char *handleName)
{
    char video_size[16] = { 0 };
    sprintf(video_size, "%dx%d", mVideoWidth, mVideoHeight);
    
    char frameRate[8] = { 0 };
    sprintf(frameRate, "%d", mVideoFps);
    
    char *title = handleName;

    AVDictionary* options = NULL;
    av_dict_set(&options, "fflags", "nobuffer", 0);
    av_dict_set(&options, "framerate", frameRate, 0);
    av_dict_set(&options, "video_size", video_size, 0);

    int ret = -1;
    AVFormatContext *fmt_ctx = avformat_alloc_context();
    AVInputFormat *ifmt = av_find_input_format("gdigrab");
    if ((ret = avformat_open_input(&fmt_ctx, title, ifmt, &options)) != 0){
        avformat_free_context(fmt_ctx);
        
        LOGE("Cannot open input Source [gdigrab:%s]\n", title);
        return -1;
    }
    
    AVCodec *dec = NULL;
    ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
    if (ret < 0) {
        avformat_close_input(&fmt_ctx);
        avformat_free_context(fmt_ctx);
        
        LOGE("Cannot find a video stream in the input Source [gdigrab:%s]\n", title);
        return -1;
    }
    
    int video_stream_index = ret;
    AVCodecContext *pVideoCodecCtx = fmt_ctx->streams[video_stream_index]->codec;
    if ((ret = avcodec_open2(pVideoCodecCtx, dec, NULL)) < 0) {
        
        avformat_close_input(&fmt_ctx);
        avformat_free_context(fmt_ctx);
        
        LOGE("Cannot open video decoder [gdigrab:%s]\n", title);
        return -1;
    }
    
    mInputAVFormatContext = fmt_ctx;
    mInputVideoStreamIndex = video_stream_index;
    
    return 0;
}

int FFmpegVideoCapture::initDShow(char *videoCapturerName)
{
    char video_size[16] = { 0 };
    sprintf(video_size, "%dx%d", mVideoWidth, mVideoHeight);
    char frameRate[8] = { 0 };
    sprintf(frameRate, "%d", mVideoFps);
    
    AVDictionary* options = NULL;
    av_dict_set(&options, "fflags", "nobuffer", 0);
    av_dict_set(&options, "framerate", frameRate, 0);
    av_dict_set(&options, "video_size", video_size, 0);
    
    AVFormatContext *fmt_ctx = avformat_alloc_context();

    int ret = -1;
    AVInputFormat *ifmt = av_find_input_format("dshow");
    
    char videoDShowName[1024] = { 0 };
    sprintf(videoDShowName, "video=%s", videoCapturerName);
    
    if (avformat_open_input(&fmt_ctx, videoDShowName, ifmt, &options) != 0) {
        avformat_free_context(fmt_ctx);
        
        LOGE("Cannot open input Source [%s]\n", videoCapturerName);
        return -1;
    }
    
    if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0) {
        
        avformat_close_input(&fmt_ctx);
        avformat_free_context(fmt_ctx);
        
        LOGE("Cannot find stream information [%s]\n", videoCapturerName);
        return -1;
    }
    
    AVCodec *dec = NULL;
    ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
    if (ret < 0) {
        avformat_close_input(&fmt_ctx);
        avformat_free_context(fmt_ctx);
        
        LOGE("Cannot find a video stream in the input Source [%s]\n", videoCapturerName);
        
        return ret;
    }
    
    int video_stream_index = ret;
    AVCodecContext *pVideoCodecCtx = fmt_ctx->streams[video_stream_index]->codec;
    
    if ((ret = avcodec_open2(pVideoCodecCtx, dec, NULL)) < 0) {
        
        avformat_close_input(&fmt_ctx);
        avformat_free_context(fmt_ctx);
        
        LOGE("Cannot open video decoder [%s]\n", videoCapturerName);
        return ret;
    }
    
    mInputAVFormatContext = fmt_ctx;
    mInputVideoStreamIndex = video_stream_index;
    
    return 0;
}

int FFmpegVideoCapture::initAVFoundation(char *videoCapturerName)
{
    char video_size[16] = { 0 };
    sprintf(video_size, "%dx%d", mVideoWidth, mVideoHeight);
    char frameRate[8] = { 0 };
    sprintf(frameRate, "%d", mVideoFps);
    
    AVDictionary* options = NULL;
    av_dict_set(&options, "fflags", "nobuffer", 0);
    av_dict_set(&options, "framerate", frameRate, 0);
    av_dict_set(&options, "video_size", video_size, 0);
    
    AVFormatContext *fmt_ctx = avformat_alloc_context();
    
    int ret = -1;
    AVInputFormat *ifmt = av_find_input_format("avfoundation");
    
    char videoAVFoundationName[1024] = { 0 };
    sprintf(videoAVFoundationName, "video=%s", videoCapturerName);
    
    if (avformat_open_input(&fmt_ctx, videoAVFoundationName, ifmt, &options) != 0) {
        avformat_free_context(fmt_ctx);
        
        LOGE("Cannot open input Source [%s]\n", videoCapturerName);
        return -1;
    }
    
    if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0) {
        
        avformat_close_input(&fmt_ctx);
        avformat_free_context(fmt_ctx);
        
        LOGE("Cannot find stream information [%s]\n", videoCapturerName);
        return -1;
    }
    
    AVCodec *dec = NULL;
    ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
    if (ret < 0) {
        avformat_close_input(&fmt_ctx);
        avformat_free_context(fmt_ctx);
        
        LOGE("Cannot find a video stream in the input Source [%s]\n", videoCapturerName);
        
        return ret;
    }
    
    int video_stream_index = ret;
    AVCodecContext *pVideoCodecCtx = fmt_ctx->streams[video_stream_index]->codec;
    
    if ((ret = avcodec_open2(pVideoCodecCtx, dec, NULL)) < 0) {
        
        avformat_close_input(&fmt_ctx);
        avformat_free_context(fmt_ctx);
        
        LOGE("Cannot open video decoder [%s]\n", videoCapturerName);
        return ret;
    }
    
    mInputAVFormatContext = fmt_ctx;
    mInputVideoStreamIndex = video_stream_index;
    
    return 0;
}

void FFmpegVideoCapture::initVideoPreProcesserForPreview()
{
    AVCodecContext *pVideoCodecCtx = mInputAVFormatContext->streams[mInputVideoStreamIndex]->codec;
    
    //SWS_BILINEAR
    img_convert_ctx_for_preview = sws_getContext(pVideoCodecCtx->width, pVideoCodecCtx->height, pVideoCodecCtx->pix_fmt,
                             mVideoWidth, mVideoHeight, AV_PIX_FMT_YUV420P,
                             SWS_BICUBIC, NULL, NULL, NULL);
    
    int size = avpicture_get_size((AVPixelFormat)AV_PIX_FMT_YUV420P, mVideoWidth, mVideoHeight);
    convertedVideoBufferForPreview = (uint8_t *)av_malloc(size);
}

//-1 : Error
//0 : No Video Frame
//1 : Normal
int FFmpegVideoCapture::flowing()
{
    AVCodecContext *pVideoCodecCtx = mInputAVFormatContext->streams[mInputVideoStreamIndex]->codec;
    pVideoCodecCtx->refcounted_frames = 0;
    
    AVPacket pkt;
    int ret = -1;
    if ((ret = av_read_frame(mInputAVFormatContext, &pkt)) < 0) {
        return 0;
    }
    
    AVStream *in_stream = mInputAVFormatContext->streams[pkt.stream_index];
    if (pkt.dts != AV_NOPTS_VALUE && in_stream->start_time != AV_NOPTS_VALUE) {
        pkt.dts -= in_stream->start_time;
    }
    if (pkt.pts != AV_NOPTS_VALUE && in_stream->start_time != AV_NOPTS_VALUE) {
        pkt.pts -= in_stream->start_time;
    }
    av_packet_rescale_ts(&pkt, in_stream->time_base, in_stream->codec->time_base);
    
    int got_frame = 0;
    AVFrame *pFrame = av_frame_alloc();
    ret = avcodec_decode_video2(pVideoCodecCtx, pFrame, &got_frame, &pkt);
    av_free_packet(&pkt);
    if (ret < 0) {
        if (ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN)) {
            av_frame_free(&pFrame);
            return 0;
        }else{
            av_frame_free(&pFrame);
            LOGE("avcodec_decode_video2 fail!");
            return -1;
        }
    }else if (!got_frame) {
        av_frame_free(&pFrame);
        return 0;
    }
    
    pFrame->pts = av_frame_get_best_effort_timestamp(pFrame);

    AVFrame *convertedVideoFrameForPreview = av_frame_alloc();
    convertedVideoFrameForPreview->width = mVideoWidth;
    convertedVideoFrameForPreview->height = mVideoHeight;
    convertedVideoFrameForPreview->format = AV_PIX_FMT_YUV420P;
    avpicture_fill((AVPicture *)convertedVideoFrameForPreview, convertedVideoBufferForPreview, (AVPixelFormat)convertedVideoFrameForPreview->format, convertedVideoFrameForPreview->width, convertedVideoFrameForPreview->height);
    sws_scale(img_convert_ctx_for_preview, pFrame->data, pFrame->linesize, 0, pFrame->height, convertedVideoFrameForPreview->data, convertedVideoFrameForPreview->linesize);
    convertedVideoFrameForPreview->pict_type = AV_PICTURE_TYPE_NONE;
    convertedVideoFrameForPreview->pts = pFrame->pts;
    av_frame_free(&pFrame);
    
    bool isNeedReCreatePreProcesserForMediaStreamer = false;
    
    //flowing to Preview
    pthread_mutex_lock(&mPreviewLock);
    if (isPreviewLinked) {
        //load
        upload_texture(&videoTexture, convertedVideoFrameForPreview);
        //draw
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        
        SDL_Rect rect;
        
        calculate_display_rect(&rect, 0, 0, win_width, win_height, convertedVideoFrameForPreview->width, convertedVideoFrameForPreview->height);
        
        SDL_RenderCopyEx(renderer, videoTexture, NULL, &rect, 0, NULL, SDL_FLIP_NONE);
        SDL_RenderPresent(renderer);
        
        if (windowSizeUpdated) {
            windowSizeUpdated = false;
            isNeedReCreatePreProcesserForMediaStreamer = true;
        }
    }
    pthread_mutex_unlock(&mPreviewLock);
    
    //flowing to MediaStreamer
    pthread_mutex_lock(&mMediaStreamerLock);
    
    if (mMediaStreamer) {
        //
        if (isNeedReCreatePreProcesserForMediaStreamer) {
            if (img_convert_ctx_for_mediastreamer) {
                if (img_convert_ctx_for_mediastreamer != NULL)
                {
                    sws_freeContext(img_convert_ctx_for_mediastreamer);
                    img_convert_ctx_for_mediastreamer = NULL;
                }
                if (convertedVideoBufferForMediaStreamer) {
                    av_free(convertedVideoBufferForMediaStreamer);
                    convertedVideoBufferForMediaStreamer = NULL;
                }
            }
        }
        
        if (img_convert_ctx_for_mediastreamer==NULL) {
            
            if (win_width%2==0) {
                convertedVideoWidthForMediaStreamer = win_width;
            }else{
                convertedVideoWidthForMediaStreamer = win_width + 1;
            }
            
            if (win_height%2==0) {
                convertedVideoHeightForMediaStreamer = win_height;
            }else{
                convertedVideoHeightForMediaStreamer = win_height + 1;
            }
            
            img_convert_ctx_for_mediastreamer = sws_getContext(mVideoWidth, mVideoHeight, AV_PIX_FMT_YUV420P,
                                                               convertedVideoWidthForMediaStreamer, convertedVideoHeightForMediaStreamer, AV_PIX_FMT_YUV420P,
                                                               SWS_BICUBIC, NULL, NULL, NULL);
            
            int size = avpicture_get_size((AVPixelFormat)AV_PIX_FMT_YUV420P, convertedVideoWidthForMediaStreamer, convertedVideoHeightForMediaStreamer);
            convertedVideoBufferForMediaStreamer = (uint8_t *)av_malloc(size);
        }

        //
        AVFrame *convertedVideoFrameForMediaStreamer = av_frame_alloc();
        convertedVideoFrameForMediaStreamer->width = convertedVideoWidthForMediaStreamer;
        convertedVideoFrameForMediaStreamer->height = convertedVideoHeightForMediaStreamer;
        convertedVideoFrameForMediaStreamer->format = AV_PIX_FMT_YUV420P;
        avpicture_fill((AVPicture *)convertedVideoFrameForMediaStreamer, convertedVideoBufferForMediaStreamer, (AVPixelFormat)convertedVideoFrameForMediaStreamer->format, convertedVideoFrameForMediaStreamer->width, convertedVideoFrameForMediaStreamer->height);
        sws_scale(img_convert_ctx_for_mediastreamer, convertedVideoFrameForPreview->data, convertedVideoFrameForPreview->linesize, 0, convertedVideoFrameForPreview->height, convertedVideoFrameForMediaStreamer->data, convertedVideoFrameForMediaStreamer->linesize);
        convertedVideoFrameForMediaStreamer->pict_type = AV_PICTURE_TYPE_NONE;
        convertedVideoFrameForMediaStreamer->pts = convertedVideoFrameForPreview->pts;
        
        //
        YUVVideoFrame videoFrame;
        videoFrame.planes = 3;
        for (int i = 0; i<videoFrame.planes; i++) {
            videoFrame.data[i] = convertedVideoFrameForMediaStreamer->data[i];
            videoFrame.linesize[i] = convertedVideoFrameForMediaStreamer->linesize[i];
        }
        videoFrame.width = convertedVideoFrameForMediaStreamer->width;
        videoFrame.height = convertedVideoFrameForMediaStreamer->height;
        videoFrame.videoRawType = VIDEOFRAME_RAWTYPE_I420;
        videoFrame.rotation = 0;
        
        mMediaStreamer->inputYUVVideoFrame(&videoFrame, mSourceId);
        
        //
        av_frame_free(&convertedVideoFrameForMediaStreamer);
    }
    
    pthread_mutex_unlock(&mMediaStreamerLock);
    
    av_frame_free(&convertedVideoFrameForPreview);
    
    return 1;
}

bool FFmpegVideoCapture::openPreview(void *display)
{
    mDisplay = display;
    
    window = SDL_CreateWindowFrom(mDisplay);
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
    
    SDL_RendererInfo renderer_info = { 0 };
    if (window) {
        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        if (!renderer) {
            LOGW("Failed to initialize a hardware accelerated renderer: %s\n", SDL_GetError());
            renderer = SDL_CreateRenderer(window, -1, 0);
        }
        
        if (renderer) {
            if (!SDL_GetRendererInfo(renderer, &renderer_info))
                LOGV("Initialized %s renderer.\n", renderer_info.name);
        }
    }
    
    if (!window || !renderer || !renderer_info.num_texture_formats) {
        LOGE("Failed to create window or renderer: %s", SDL_GetError());
        
        if (renderer)
        {
            SDL_DestroyRenderer(renderer);
            renderer = NULL;
        }
        
        if (window)
        {
            SDL_DestroyWindow(window);
            window = NULL;
        }
        
        return false;
    }
    
    int windowWidth = 0;
    int windowHeight = 0;
    SDL_GetWindowSize(window, &windowWidth, &windowHeight);
    if (windowWidth!=win_width || windowHeight!=win_height) {
        win_width = windowWidth;
        win_height = windowHeight;
        windowSizeUpdated = true;
    }
    
    return true;
}

void FFmpegVideoCapture::closePreview()
{
    if (videoTexture)
    {
        SDL_DestroyTexture(videoTexture);
        videoTexture = NULL;
    }
    
    if (renderer)
    {
        SDL_DestroyRenderer(renderer);
        renderer = NULL;
    }
    
    if (window)
    {
        SDL_DestroyWindow(window);
        window = NULL;
    }
    
    mDisplay = NULL;
    
    win_width = 0;
    win_height = 0;
}

void FFmpegVideoCapture::calculate_display_rect(SDL_Rect *rect,
                                            int scr_xleft, int scr_ytop, int scr_width, int scr_height,
                                            int pic_width, int pic_height)
{
    float aspect_ratio;
    int width, height, x, y;
    
    aspect_ratio = (float)pic_width / (float)pic_height;
    
    /* XXX: we suppose the screen has a 1.0 pixel ratio */
    height = scr_height;
    width = lrint(height * aspect_ratio) & ~1;
    if (width > scr_width) {
        width = scr_width;
        height = lrint(width / aspect_ratio) & ~1;
    }
    x = (scr_width - width) / 2;
    y = (scr_height - height) / 2;
    rect->x = scr_xleft + x;
    rect->y = scr_ytop + y;
    rect->w = FFMAX(width, 1);
    rect->h = FFMAX(height, 1);
}

int FFmpegVideoCapture::upload_texture(SDL_Texture **tex, AVFrame *frame)
{
    int ret = -1;
    if (frame->format==AV_PIX_FMT_YUV420P || frame->format==AV_PIX_FMT_YUVJ420P) {
        Uint32 sdl_pix_fmt = SDL_PIXELFORMAT_YV12;
        SDL_BlendMode sdl_blendmode = SDL_BLENDMODE_NONE;
        if (realloc_texture(tex, sdl_pix_fmt, frame->width, frame->height, sdl_blendmode, 0) < 0)
            return -1;
        
        ret = SDL_UpdateYUVTexture(*tex, NULL, frame->data[0], frame->linesize[0],
                                   frame->data[1], frame->linesize[1],
                                   frame->data[2], frame->linesize[2]);
        
        return ret;
    }
    
    return ret;
}

int FFmpegVideoCapture::realloc_texture(SDL_Texture **texture, Uint32 new_format, int new_width, int new_height, SDL_BlendMode blendmode, int init_texture)
{
    Uint32 format;
    int access, w, h;
    if (!*texture || SDL_QueryTexture(*texture, &format, &access, &w, &h) < 0 || new_width != w || new_height != h || new_format != format) {
        void *pixels;
        int pitch;
        if (*texture)
            SDL_DestroyTexture(*texture);
        if (!(*texture = SDL_CreateTexture(renderer, new_format, SDL_TEXTUREACCESS_STREAMING, new_width, new_height)))
            return -1;
        if (SDL_SetTextureBlendMode(*texture, blendmode) < 0)
            return -1;
        if (init_texture) {
            if (SDL_LockTexture(*texture, NULL, &pixels, &pitch) < 0)
                return -1;
            memset(pixels, 0, pitch * new_height);
            SDL_UnlockTexture(*texture);
        }
        LOGD("Created %dx%d texture with %s.\n", new_width, new_height, SDL_GetPixelFormatName(new_format));
    }
    return 0;
}
