//
//  AudioCapture.h
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef AudioCapture_h
#define AudioCapture_h

#include <stdio.h>
#include "MediaDataType.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include "IPCMDataOutputer.h"

enum AUDIO_CAPTURE_TYPE
{
    AUDIO_CAPTURE_UNKNOWN = -1,
    OPENSLES = 0, //for android native api
    AUDIOUNIT = 1, //for iOS native api
    AUDIORECORD = 2, //for android java api
    
    EXTERNAL_INPUT = 3, //for external input pcm data
    
    FFMPEG_DSHOW_AUDIO = 4, //for Win ffmpeg api
    FFMPEG_AVFOUNDATION_AUDIO = 5, //for Mac ffmpeg api
    
    AUDIO_CAPTURE_MAC = 6,

	AUDIO_CAPTURE_WIN_WAVE = 7,

	DISCRETE_EXTERNAL_INPUT = 8, //for discrete external input pcm data
};

enum AudioCaptureMode
{
    DEFAULT_MODE = 0,
    VOIP_MODE = 1,
};

class AudioCapture {
public:
    virtual ~AudioCapture() {}
    
    static AudioCapture* CreateAudioCapture(AUDIO_CAPTURE_TYPE type, char* capturer, int sampleRate, int numChannels);
    static void DeleteAudioCapture(AudioCapture* audioCapture, AUDIO_CAPTURE_TYPE type);
    
#ifdef ANDROID
    virtual void registerJavaVMEnv(JavaVM *jvm) = 0;
#endif
    
    virtual void setPCMDataOutputer(IPCMDataOutputer *outputer) = 0;
    
    virtual void setRecBufSizeInBytes(int bufSizeInBytes) = 0;
    
    virtual int Init(AudioCaptureMode mode = DEFAULT_MODE) = 0;
    virtual int Terminate() = 0;
    virtual bool Initialized() = 0;
    
    virtual int StartRecording() = 0;
    virtual int StopRecording() = 0;
    virtual bool Recording() = 0;
    
    virtual void setVolume(float volume) = 0;
    
    virtual void inputAudioFrame(AudioFrame *inAudioFrame) = 0;
    virtual void inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType) = 0;

    virtual AudioFrame* frontAudioFrame() = 0;
    virtual void popAudioFrame() = 0;
    
#ifdef IOS
    virtual void iOSAVAudioSessionInterruption(bool isInterrupting) = 0;
#endif
};

#endif /* AudioCapture_h */
