//
//  FFmpegAudioCapture.cpp
//  MediaStreamer
//
//  Created by Think on 2018/8/15.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "FFmpegAudioCapture.h"
#include "MediaLog.h"
#include "MediaTimer.h"

FFmpegAudioCapture::FFmpegAudioCapture(AUDIO_CAPTURE_TYPE type, char* capturer, int sampleRate, int numChannels)
{
    mAudioCaptureType = type;
    mCapturer = strdup(capturer);
    
    isBreakThread = false;
    
    initialized_ = false;
    recording_ = false;
    
    sampleRate_ = sampleRate;
    numChannels_ = numChannels;
    recBufSizeInBytes_ = 0;
    
    fifo = NULL;
    fifoSize = 0;
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mCondition, NULL);
    isWaitting = false;
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    mInputAVFormatContext = NULL;
    mInputAudioStreamIndex = -1;
    
    mFFAudioFilter = NULL;
    
    mVolume = 1.0f;
    isNeedUpdateAudioFilter = false;
}

FFmpegAudioCapture::~FFmpegAudioCapture()
{
    Terminate();
    
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mCondition);
    
    if(mCapturer)
    {
        free(mCapturer);
        mCapturer = NULL;
    }
}

void FFmpegAudioCapture::initFFEnv()
{
    av_register_all();
    avfilter_register_all();
    avdevice_register_all();

    av_log_set_level(AV_LOG_WARNING);
}

void FFmpegAudioCapture::allocBuffers()
{
    fifoSize = recBufSizeInBytes_*CalculateNumFifoBuffersNeeded();
    fifo = (uint8_t *)malloc(fifoSize);
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    mAudioFrame.data = (uint8_t*)malloc(recBufSizeInBytes_);
    mAudioFrame.frameSize = recBufSizeInBytes_;
    mAudioFrame.pts = 0;
    mAudioFrame.duration = 0;
}

void FFmpegAudioCapture::freeBuffers()
{
    if (fifo!=NULL) {
        free(fifo);
        fifo = NULL;
    }
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    if(mAudioFrame.data!=NULL)
    {
        free(mAudioFrame.data);
        mAudioFrame.data = NULL;
    }
}

void FFmpegAudioCapture::flushBuffers()
{
    pthread_mutex_lock(&mLock);
    cache_len = 0;
    write_pos = 0;
    read_pos = 0;
    pthread_mutex_unlock(&mLock);
}

void FFmpegAudioCapture::setRecBufSizeInBytes(int bufSizeInBytes)
{
    recBufSizeInBytes_ = bufSizeInBytes;
}

int FFmpegAudioCapture::Init()
{
    if (initialized_) {
        LOGW("Already initialized");
        return 0;
    }
    
    AVDictionary* options = NULL;
    AVFormatContext *fmt_ctx = avformat_alloc_context();
    AVInputFormat *ifmt = NULL;
    if (mAudioCaptureType==FFMPEG_DSHOW_AUDIO) {
        ifmt = av_find_input_format("dshow");
    }else if(mAudioCaptureType==FFMPEG_AVFOUNDATION_AUDIO) {
        ifmt = av_find_input_format("avfoundation");
    }
    
    char audioCapturerName[1024] = { 0 };
    sprintf(audioCapturerName, "audio=%s", mCapturer);

    if (avformat_open_input(&fmt_ctx, audioCapturerName, ifmt, &options) != 0)
    {
        avformat_free_context(fmt_ctx);
        LOGE("Cannot open input Source [%s]\n", mCapturer);
        return -1;
    }
    
    if (avformat_find_stream_info(fmt_ctx, NULL) < 0)
    {
        avformat_close_input(&fmt_ctx);
        avformat_free_context(fmt_ctx);
        LOGE("Cannot find stream information [%s]\n", mCapturer);
        return -1;
    }
    
    AVCodec *dec = NULL;
    int audio_stream_index = -1;
    int ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_AUDIO, -1, -1, &dec, 0);
    if (ret<0) {
        avformat_close_input(&fmt_ctx);
        avformat_free_context(fmt_ctx);
        
        LOGE("Cannot find a audio stream in the input source [%s]\n", mCapturer);
        return -1;
    }else{
        audio_stream_index = ret;
        AVCodecContext *pAudioCodecCtx = fmt_ctx->streams[audio_stream_index]->codec;
        if (avcodec_open2(pAudioCodecCtx, dec, NULL) < 0) {
            avformat_close_input(&fmt_ctx);
            avformat_free_context(fmt_ctx);
            
            LOGE("Cannot open audio decoder [%s]\n", mCapturer);
            return -1;
        }
    }
    
    mFFAudioFilter = new FFAudioFilter();
    char afiltersDesc[256] = {0};
    av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "volume=%f", mVolume);
    AVCodecContext *pAudioCodecCtx = mInputAVFormatContext->streams[mInputAudioStreamIndex]->codec;
    uint64_t output_channel_layout = AV_CH_LAYOUT_MONO;
    if (numChannels_==1) {
        output_channel_layout = AV_CH_LAYOUT_MONO;
    }else if(numChannels_ == 2) {
        output_channel_layout = AV_CH_LAYOUT_STEREO;
    }
    bool bRet = mFFAudioFilter->open(afiltersDesc, pAudioCodecCtx->channel_layout, pAudioCodecCtx->channels, pAudioCodecCtx->sample_rate, pAudioCodecCtx->sample_fmt, output_channel_layout, numChannels_, sampleRate_, AV_SAMPLE_FMT_S16);
    
    if (!bRet) {
        avcodec_close(pAudioCodecCtx);
        avformat_close_input(&fmt_ctx);
        avformat_free_context(fmt_ctx);
        
        mFFAudioFilter->dispose();
        delete mFFAudioFilter;
        mFFAudioFilter = NULL;
        
        LOGE("Open Audio Filter Fail");
        return -1;
    }
    
    mInputAVFormatContext = fmt_ctx;
    mInputAudioStreamIndex = audio_stream_index;
    
    allocBuffers();
    
    createFFmpegAudioCaptureThread();
    
    initialized_ = true;
    
    return 0;
}

void FFmpegAudioCapture::createFFmpegAudioCaptureThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, &attr, handleFFmpegAudioCaptureThread, this);
    
    pthread_attr_destroy(&attr);
}

void* FFmpegAudioCapture::handleFFmpegAudioCaptureThread(void* ptr)
{
    FFmpegAudioCapture* ffAudioCapture = (FFmpegAudioCapture*)ptr;
    ffAudioCapture->FFmpegAudioCaptureThreadMain();
    
    return NULL;
}

void FFmpegAudioCapture::FFmpegAudioCaptureThreadMain()
{
    bool isIdle = false;
    
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        if (!recording_) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        
        if (isIdle) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }

        if (isNeedUpdateAudioFilter) {
            isNeedUpdateAudioFilter = false;
            
            char afiltersDesc[256] = {0};
            av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "volume=%f", mVolume);
            pthread_mutex_unlock(&mLock);
            
            if (mFFAudioFilter!=NULL) {
                mFFAudioFilter->dispose();
                
                AVCodecContext *pAudioCodecCtx = mInputAVFormatContext->streams[mInputAudioStreamIndex]->codec;
                uint64_t output_channel_layout = AV_CH_LAYOUT_MONO;
                if (numChannels_==1) {
                    output_channel_layout = AV_CH_LAYOUT_MONO;
                }else if(numChannels_ == 2) {
                    output_channel_layout = AV_CH_LAYOUT_STEREO;
                }
                bool bRet = mFFAudioFilter->open(afiltersDesc, pAudioCodecCtx->channel_layout, pAudioCodecCtx->channels, pAudioCodecCtx->sample_rate, pAudioCodecCtx->sample_fmt, output_channel_layout, numChannels_, sampleRate_, AV_SAMPLE_FMT_S16);
                if (!bRet) {
                    LOGE("Open Audio Filter Fail");
                    isIdle = true;
                    continue;
                }
            }
        }else {
            pthread_mutex_unlock(&mLock);
        }
        
        int ret = flowing();
        if (ret==-1) {
            LOGE("FFmpeg Capture Audio Fail");
            isIdle = true;
            continue;
        }else if (ret == 0) {
            pthread_mutex_lock(&mLock);
            
            int64_t reltime = 10 * 1000 * 1000ll;
            struct timespec ts;
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
            
            pthread_mutex_unlock(&mLock);
            continue;
        }
    }
}

void FFmpegAudioCapture::deleteFFmpegAudioCaptureThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

int FFmpegAudioCapture::Terminate()
{
    if (!initialized_) {
        LOGW("Already terminated");
        return 0;
    }
    
    deleteFFmpegAudioCaptureThread();
    
    freeBuffers();
    
    if (mFFAudioFilter!=NULL) {
        mFFAudioFilter->dispose();
        delete mFFAudioFilter;
        mFFAudioFilter = NULL;
    }

    avcodec_close(mInputAVFormatContext->streams[mInputAudioStreamIndex]->codec);
    avformat_close_input(&mInputAVFormatContext);
    avformat_free_context(mInputAVFormatContext);
    
    mInputAVFormatContext = NULL;
    mInputAudioStreamIndex = -1;
    
    initialized_ = false;
    
    return 0;
}

int FFmpegAudioCapture::StartRecording()
{
    pthread_mutex_lock(&mLock);
    recording_ = true;
    pthread_mutex_unlock(&mLock);

    pthread_cond_signal(&mCondition);
    
    return 0;
}

int FFmpegAudioCapture::StopRecording()
{
    pthread_mutex_lock(&mLock);
    recording_ = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    return 0;
}

bool FFmpegAudioCapture::Recording()
{
    return recording_;
}

void FFmpegAudioCapture::setVolume(float volume)
{
    if (volume<0.0f || volume == mVolume) {
        return;
    }
    
    pthread_mutex_lock(&mLock);
    
    mVolume = volume;
    isNeedUpdateAudioFilter = true;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

int64_t FFmpegAudioCapture::getLatency()
{
    return static_cast<int64_t>(cache_len*1000000/sampleRate_*1*2);
}

int FFmpegAudioCapture::CalculateNumFifoBuffersNeeded() {
    return 4;
}

//-1 : Error
//0 : No Audio Frame
//1 : Normal
//2 : Loop
int FFmpegAudioCapture::flowing()
{
    AVCodecContext *pAudioCodecCtx = mInputAVFormatContext->streams[mInputAudioStreamIndex]->codec;
    
    AVPacket pkt;
    int ret = -1;
    if ((ret = av_read_frame(mInputAVFormatContext, &pkt)) < 0) {
        return 0;
    }
    
    AVStream *in_stream = mInputAVFormatContext->streams[pkt.stream_index];
    if (pkt.dts != AV_NOPTS_VALUE && in_stream->start_time != AV_NOPTS_VALUE) {
        pkt.dts -= in_stream->start_time;
    }
    if (pkt.pts != AV_NOPTS_VALUE && in_stream->start_time != AV_NOPTS_VALUE) {
        pkt.pts -= in_stream->start_time;
    }
    av_packet_rescale_ts(&pkt, in_stream->time_base, in_stream->codec->time_base);
    
    int got_frame = 0;
    AVFrame *pFrame = av_frame_alloc();
    ret = avcodec_decode_audio4(pAudioCodecCtx, pFrame, &got_frame, &pkt);
    av_free_packet(&pkt);
    if (ret < 0) {
        if (ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN)) {
            av_frame_free(&pFrame);
            return 0;
        }else{
            av_frame_free(&pFrame);
            return -1;
        }
    }else if (!got_frame)
    {
        av_frame_free(&pFrame);
        return 0;
    }
    
    pFrame->pts = av_frame_get_best_effort_timestamp(pFrame);
    
    //do audio filter
    AVFrame *filterFrame = NULL;
    if (mFFAudioFilter != NULL) {
        //do filter
        if (mFFAudioFilter->filterIn(pFrame))
        {
            AVFrame* outFrame = mFFAudioFilter->filterOut();
            if (outFrame != NULL) {
                filterFrame = outFrame;
            }
        }
    }
    av_frame_free(&pFrame);
    if (filterFrame==NULL)
    {
        //loop
        return 2;
    }
    
    int size = av_samples_get_buffer_size(NULL, av_frame_get_channels(filterFrame), filterFrame->nb_samples, (enum AVSampleFormat)filterFrame->format, 1);
    uint8_t* data = *(filterFrame->extended_data);
    
    pthread_mutex_lock(&mLock);
    if(cache_len + size>fifoSize)
    {
        // is full
    }else
    {
        if(size>fifoSize-write_pos)
        {
            memcpy(fifo+write_pos,data,fifoSize-write_pos);
            memcpy(fifo,data+fifoSize-write_pos,size-(fifoSize-write_pos));
            write_pos = size-(fifoSize-write_pos);
        }else
        {
            memcpy(fifo+write_pos,data,size);
            write_pos = write_pos+size;
        }
        
        cache_len = cache_len+size;
    }
    
    if (isWaitting) {
        isWaitting = false;
        pthread_cond_signal(&mCondition);
    }
    pthread_mutex_unlock(&mLock);
    
    return 1;
}

AudioFrame* FFmpegAudioCapture::frontAudioFrame()
{
    AudioFrame* audioFrame = NULL;
    
    while (true) {
        pthread_mutex_lock(&mLock);
        
        if (cache_len >= recBufSizeInBytes_) {
            
            if(recBufSizeInBytes_>fifoSize-read_pos)
            {
                memcpy(mAudioFrame.data,fifo+read_pos,fifoSize-read_pos);
                memcpy(mAudioFrame.data+fifoSize-read_pos,fifo,recBufSizeInBytes_-(fifoSize-read_pos));
                read_pos = recBufSizeInBytes_-(fifoSize-read_pos);
            }else
            {
                memcpy(mAudioFrame.data,fifo+read_pos,recBufSizeInBytes_);
                read_pos = read_pos + recBufSizeInBytes_;
            }
            
            mAudioFrame.frameSize = recBufSizeInBytes_;
            mAudioFrame.duration = float(recBufSizeInBytes_*1000)/float(sampleRate_*1*2);
            
            mAudioFrame.pts = MediaTimer::GetNowMediaTimeMS();
            cache_len = cache_len - recBufSizeInBytes_;
            
            audioFrame = &mAudioFrame;
            
            pthread_mutex_unlock(&mLock);
            
            break;
            
        }else{
            isWaitting = true;
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
    }
    
    return audioFrame;
}

void FFmpegAudioCapture::popAudioFrame()
{
    //no need do anything.
}
