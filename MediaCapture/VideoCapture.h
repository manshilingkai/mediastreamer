//
//  VideoCapture.h
//  MediaStreamer
//
//  Created by Think on 2018/8/15.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef VideoCapture_h
#define VideoCapture_h

#include <stdio.h>

#include "MediaDataType.h"
#include "MediaStreamer.h"

enum VIDEO_CAPTURE_TYPE
{
    FFMPEG_DSHOW_VIDEO = 1, //for Win ffmpeg api
    FFMPEG_GDIGRAB_VIDEO = 2, //for Win ffmpeg api
    FFMPEG_AVFOUNDATION_VIDEO = 3, //for Mac ffmpeg api

	WIN_NATIVE_GDI_DESKTOP_CAPTURE = 4,
};

class VideoCapture {
public:
    virtual ~VideoCapture() {}

    static VideoCapture* CreateVideoCapture(VIDEO_CAPTURE_TYPE type, char* capturer, int videoWidth, int videoHeight, int videoFps, int sourceId);
    static void DeleteVideoCapture(VideoCapture* videoCapture, VIDEO_CAPTURE_TYPE type);

    virtual int Init() = 0;
    virtual int Terminate() = 0;
    virtual bool Initialized() = 0;

    virtual int StartRecording() = 0;
    virtual int StopRecording() = 0;
    virtual bool Recording() = 0;

    virtual void linkPreview(void* display) = 0;
    virtual void unlinkPreview() = 0;
    virtual void onPreviewResize(int w, int h) = 0;

    virtual void linkMediaStreamer(MediaStreamer* mediaStreamer) = 0;
    virtual void unlinkMediaStreamer() = 0;
};

#endif /* VideoCapture_h */
