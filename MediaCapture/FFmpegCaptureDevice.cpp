//
//  FFmpegCaptureDevice.cpp
//  MediaStreamer
//
//  Created by Think on 2018/8/27.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "FFmpegCaptureDevice.h"

extern "C" {
#include "libavutil/error.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavdevice/avdevice.h"
#include "libavfilter/avfilter.h"
#include "libswscale/swscale.h"
}

#include "MediaLog.h"

FFCapDevice::FFCapDevice()
{
    av_register_all();
    avfilter_register_all();
    avdevice_register_all();
    
    av_log_set_level(AV_LOG_WARNING);

    ffCapDeviceInfo = new FFCapDeviceInfo;
    ffCapVideoDeviceAbility = new FFCapVideoDeviceAbility;
}

FFCapDevice::~FFCapDevice()
{
    ffCapDeviceInfo->Free();
    delete ffCapDeviceInfo;
    
    ffCapVideoDeviceAbility->Free();
    delete ffCapVideoDeviceAbility;
}

FFCapDeviceInfo *FFCapDevice::listFFCapDeviceInfo()
{
    AVFormatContext *pFmtCtx = avformat_alloc_context();
    AVDictionary *options = NULL;
    av_dict_set(&options, "list_devices", "true", 0);
    AVInputFormat *iformat = NULL;
#ifdef WIN
    iformat = av_find_input_format("dshow");
#endif

#ifdef MAC
    iformat = av_find_input_format("avfoundation");
#endif
    
#ifdef WIN
    pFmtCtx->dShowDeviceInfo_callback.callback = FFCapDeviceInfoCallBack;
    pFmtCtx->dShowDeviceInfo_callback.opaque = this;
#endif
    
#ifdef MAC
    pFmtCtx->ffCapDeviceInfoCallback.callback = FFCapDeviceInfoCallBack;
    pFmtCtx->ffCapDeviceInfoCallback.opaque = this;
#endif
    
    ffCapDeviceInfo->Free();

    int ret = avformat_open_input(&pFmtCtx, "video=dummy", iformat, &options);

    if (ret != AVERROR_EXIT) {
        if (pFmtCtx) {
            avformat_free_context(pFmtCtx);
        }
        
        LOGE("List FFmpeg Capture Device Fail!\n");
        
        return NULL;
    }
    
    if (ffCapDeviceInfo->count == 0) {
        LOGW("No FFmpeg Capture Device!\n");
        
        if (pFmtCtx) {
            avformat_free_context(pFmtCtx);
        }
        
        return NULL;
    }
    
    if (pFmtCtx) {
        avformat_free_context(pFmtCtx);
    }
    
    checkAllDevicesAvailable();
    
    return ffCapDeviceInfo;
}

void FFCapDevice::FFCapDeviceInfoCallBack(void* opaque, int type, char* deviceName)
{
    FFCapDevice* ffCapDevice = (FFCapDevice*)opaque;
    ffCapDevice->HandleFFCapDeviceInfoCallBack(type, deviceName);
}

void FFCapDevice::HandleFFCapDeviceInfoCallBack(int type, char* deviceName)
{
    ffCapDeviceInfo->types[ffCapDeviceInfo->count] = type;
    ffCapDeviceInfo->deviceNames[ffCapDeviceInfo->count] = strdup(deviceName);
#ifdef WIN
    ffCapDeviceInfo->deviceNameUTFs[ffCapDeviceInfo->count] = UTF8ToANSI(ffCapDeviceInfo->deviceNames[ffCapDeviceInfo->count]);
#else
    ffCapDeviceInfo->deviceNameUTFs[ffCapDeviceInfo->count] = strdup(ffCapDeviceInfo->deviceNames[ffCapDeviceInfo->count]);
#endif
    LOGD("%s",ffCapDeviceInfo->deviceNameUTFs[ffCapDeviceInfo->count]);
    
    ffCapDeviceInfo->count++;
}

void FFCapDevice::checkAllDevicesAvailable()
{
    for (int i=0; i<ffCapDeviceInfo->count; i++) {
        if (ffCapDeviceInfo->types[i] == FFCAPDEVICE_TYPE_VIDEO) {
            AVDictionary* options = NULL;
            AVFormatContext *fmt_ctx = avformat_alloc_context();
            int ret = -1;
            AVInputFormat *ifmt = NULL;
#ifdef WIN
            ifmt = av_find_input_format("dshow");
#endif
            
#ifdef MAC
            ifmt = av_find_input_format("avfoundation");
#endif
            char videoCaptureName[1024] = { 0 };
            char *videoDeviceName = ffCapDeviceInfo->deviceNames[i];
            sprintf(videoCaptureName, "video=%s", videoDeviceName);

            if (avformat_open_input(&fmt_ctx, videoCaptureName, ifmt, &options) != 0) {
                avformat_free_context(fmt_ctx);
                
                LOGE("Cannot Open Input Device [%s]\n", videoDeviceName);
                
                ffCapDeviceInfo->deviceAvailables[i] = false;
                
                continue;
            }
            
            if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0) {
                
                avformat_close_input(&fmt_ctx);
                avformat_free_context(fmt_ctx);
                
                LOGE("Cannot Find Device Information [%s]\n", videoDeviceName);
                
                ffCapDeviceInfo->deviceAvailables[i] = false;
                
                continue;
            }
            
            AVCodec *dec;
            // select the video stream
            ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
            if (ret < 0) {
                avformat_close_input(&fmt_ctx);
                avformat_free_context(fmt_ctx);
                
                LOGE("Cannot Find Video Stream In The Input Device [%s]\n", videoDeviceName);
                
                ffCapDeviceInfo->deviceAvailables[i] = false;
                
                continue;
            }else{
                int video_stream_index = ret;
                AVCodecContext *pVideoCodecCtx = fmt_ctx->streams[video_stream_index]->codec;
                
                // init the video decoder
                if ((ret = avcodec_open2(pVideoCodecCtx, dec, NULL)) < 0) {
                    
                    avformat_close_input(&fmt_ctx);
                    avformat_free_context(fmt_ctx);
                    
                    LOGE("Cannot Open Video Decoder [%s]\n", videoDeviceName);
                    
                    ffCapDeviceInfo->deviceAvailables[i] = false;
                    
                    continue;
                }
                
                //free
                avcodec_close(pVideoCodecCtx);
                avformat_close_input(&fmt_ctx);
                avformat_free_context(fmt_ctx);
                
                ffCapDeviceInfo->deviceAvailables[i] = true;
                
                continue;
            }
        }else if(ffCapDeviceInfo->types[i] == FFCAPDEVICE_TYPE_AUDIO)
        {
            AVDictionary* options = NULL;
            AVFormatContext *fmt_ctx = avformat_alloc_context();
            AVInputFormat *ifmt = NULL;
#ifdef WIN
            ifmt = av_find_input_format("dshow");
#endif
            
#ifdef MAC
            ifmt = av_find_input_format("avfoundation");
#endif
            
            char audioCaptureName[1024] = { 0 };
            char* audioDeviceName = ffCapDeviceInfo->deviceNames[i];
            sprintf(audioCaptureName, "audio=%s", audioDeviceName);
            
            if (avformat_open_input(&fmt_ctx, audioCaptureName, ifmt, &options) != 0) {
                avformat_free_context(fmt_ctx);
                
                LOGE("Cannot Open Input Device [%s]\n", audioDeviceName);
                
                ffCapDeviceInfo->deviceAvailables[i] = false;
                
                continue;
            }
            
            int ret = -1;
            if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0) {
                
                avformat_close_input(&fmt_ctx);
                avformat_free_context(fmt_ctx);
                
                LOGE("Cannot Find Device Information [%s]\n", audioDeviceName);
                
                ffCapDeviceInfo->deviceAvailables[i] = false;
                
                continue;
            }
            
            AVCodec *dec;
            // select the audio stream
            ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_AUDIO, -1, -1, &dec, 0);
            if (ret < 0) {
                avformat_close_input(&fmt_ctx);
                avformat_free_context(fmt_ctx);
                
                LOGE("Cannot Find Audio Stream In The Input Device [%s]\n", audioDeviceName);
                
                ffCapDeviceInfo->deviceAvailables[i] = false;
                
                continue;
            }else{
                int audio_stream_index = ret;
                AVCodecContext *pAudioCodecCtx = fmt_ctx->streams[audio_stream_index]->codec;
                
                // init the audio decoder
                if ((ret = avcodec_open2(pAudioCodecCtx, dec, NULL)) < 0) {
                    avformat_close_input(&fmt_ctx);
                    avformat_free_context(fmt_ctx);
                    
                    LOGE("Cannot Open Audio Decoder [%s]\n", audioDeviceName);
                    
                    ffCapDeviceInfo->deviceAvailables[i] = false;
                    
                    continue;
                }
                
                //free
                avcodec_close(pAudioCodecCtx);
                avformat_close_input(&fmt_ctx);
                avformat_free_context(fmt_ctx);
                
                ffCapDeviceInfo->deviceAvailables[i] = true;
                
                continue;
            }
        }
    }
}

FFCapVideoDeviceAbility *FFCapDevice::listFFCapVideoDeviceAbility(char* videoDeviceName)
{
    AVFormatContext *pFmtCtx = avformat_alloc_context();
    AVDictionary *options = NULL;
    av_dict_set(&options, "list_options", "true", 0);
    AVInputFormat *iformat = NULL;
#ifdef WIN
    iformat = av_find_input_format("dshow");
#endif
    
#ifdef MAC
    iformat = av_find_input_format("avfoundation");
#endif
    
    char videoCaptureName[1024] = { 0 };
    sprintf(videoCaptureName, "video=%s", videoDeviceName);
    
#ifdef WIN
    pFmtCtx->dShowVideoDeviceAbilityItem_callback.callback = FFCapVideoDeviceAbilityItemCallBack;
    pFmtCtx->dShowVideoDeviceAbilityItem_callback.opaque = this;
#endif
    
    ffCapVideoDeviceAbility->Free();
    
    int ret = avformat_open_input(&pFmtCtx, videoCaptureName, iformat, &options);

    if (ret != AVERROR_EXIT)
    {
        if (pFmtCtx)
        {
            avformat_free_context(pFmtCtx);
        }
        
        LOGE("List FFmpeg Video Capture Device Ability Set Fail!\n");
        
        return NULL;
    }
    
    if (ffCapVideoDeviceAbility->count == 0)
    {
        LOGW("No FFmpeg Video  Capture Device Ability Set!\n");
        
        if (pFmtCtx)
        {
            avformat_free_context(pFmtCtx);
        }
        
        return NULL;
    }
    
    if (pFmtCtx)
    {
        avformat_free_context(pFmtCtx);
    }
    
    ffCapVideoDeviceAbility->deviceName = strdup(videoDeviceName);
#ifdef WIN
    ffCapVideoDeviceAbility->deviceNameUTF = UTF8ToANSI(videoDeviceName);
#else
    ffCapVideoDeviceAbility->deviceNameUTF = strdup(videoDeviceName);
#endif
    
    return ffCapVideoDeviceAbility;
}


void FFCapDevice::FFCapVideoDeviceAbilityItemCallBack(void* opaque, int pixel_format, int w, int h, int fps)
{
    FFCapDevice* ffCapDevice = (FFCapDevice*)opaque;
    ffCapDevice->HandleFFCapVideoDeviceAbilityItemCallBack(pixel_format, w, h, fps);
}

void FFCapDevice::HandleFFCapVideoDeviceAbilityItemCallBack(int pixel_format, int w, int h, int fps)
{
    for (int i=0; i<ffCapVideoDeviceAbility->count; i++) {
        FFCapVideoDeviceAbilitySet *ffCapVideoDeviceAbilitySet = ffCapVideoDeviceAbility->videoDeviceAbilitySets[i];
        if (ffCapVideoDeviceAbilitySet->pixel_format == pixel_format) {
            for (int j = 0; j<ffCapVideoDeviceAbilitySet->count; j++) {
                FFCapVideoDeviceAbilityItem *ffCapVideoDeviceAbilityItem = ffCapVideoDeviceAbilitySet->videoDeviceAbilityItems[j];
                if (ffCapVideoDeviceAbilityItem->width == w && ffCapVideoDeviceAbilityItem->height == h && ffCapVideoDeviceAbilityItem->fps == fps) {
                    return;
                }
            }
            
            if (ffCapVideoDeviceAbilitySet->count>MAX_FFCAPDEVICE_ABILITY_ITEM_NUM) {
                return;
            }
            
            FFCapVideoDeviceAbilityItem *ffCapVideoDeviceAbilityItem = new FFCapVideoDeviceAbilityItem;
            ffCapVideoDeviceAbilityItem->width = w;
            ffCapVideoDeviceAbilityItem->height = h;
            ffCapVideoDeviceAbilityItem->fps = fps;
            
            ffCapVideoDeviceAbilitySet->videoDeviceAbilityItems[ffCapVideoDeviceAbilitySet->count] = ffCapVideoDeviceAbilityItem;
            
            ffCapVideoDeviceAbilitySet->count++;
            
            LOGD("width:%d height:%d fps:%d", w, h, fps);
            
            return;
        }
    }
    
    if (ffCapVideoDeviceAbility->count>MAX_FFCAPDEVICE_ABILITY_SET_NUM) {
        return;
    }
    
    FFCapVideoDeviceAbilitySet *ffCapVideoDeviceAbilitySet = new FFCapVideoDeviceAbilitySet;
    ffCapVideoDeviceAbilitySet->pixel_format = pixel_format;
    
    ffCapVideoDeviceAbility->videoDeviceAbilitySets[ffCapVideoDeviceAbility->count] = ffCapVideoDeviceAbilitySet;
    ffCapVideoDeviceAbility->count++;
    
    LOGD("pixel_format:%d", pixel_format);
    
    FFCapVideoDeviceAbilityItem *ffCapVideoDeviceAbilityItem = new FFCapVideoDeviceAbilityItem;
    ffCapVideoDeviceAbilityItem->width = w;
    ffCapVideoDeviceAbilityItem->height = h;
    ffCapVideoDeviceAbilityItem->fps = fps;
    
    ffCapVideoDeviceAbilitySet->videoDeviceAbilityItems[ffCapVideoDeviceAbilitySet->count] = ffCapVideoDeviceAbilityItem;
    
    ffCapVideoDeviceAbilitySet->count++;

    LOGD("width:%d height:%d fps:%d", w, h, fps);
}
