//
//  ExternalAudioInput.h
//  MediaStreamer
//
//  Created by Think on 2018/6/8.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef ExternalAudioInput_h
#define ExternalAudioInput_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include "AudioCapture.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include "resampler.h"

class ExternalAudioInput : public AudioCapture {
public:
    ExternalAudioInput(int sampleRate, int numChannels);
    ~ExternalAudioInput();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm) {};
#endif
    
    void setPCMDataOutputer(IPCMDataOutputer *outputer) {}

    void setRecBufSizeInBytes(int bufSizeInBytes);
    
    // Main initializaton and termination
    int Init(AudioCaptureMode mode = DEFAULT_MODE);
    int Terminate();
    bool Initialized() { return initialized_; }
    
    // Audio transport control
    int StartRecording();
    int StopRecording();
    bool Recording() { return recording_; }
    
    void setVolume(float volume) {}
    
    void inputAudioFrame(AudioFrame *inAudioFrame);
    void inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType);
    
    AudioFrame* frontAudioFrame();
    void popAudioFrame();
    
#ifdef IOS
    void iOSAVAudioSessionInterruption(bool isInterrupting) {}
#endif
    
private:
    int64_t getLatency();
    
    int CalculateNumFifoBuffersNeeded();
    
    bool initialized_;
    bool recording_;
    pthread_mutex_t mRecordingLock;
    
    int sampleRate_;
    int numChannels_;
    int recBufSizeInBytes_;
    
    uint8_t *fifo;
    int fifoSize;
    
    void allocBuffers();
    void freeBuffers();
    void flushBuffers();
    
    pthread_mutex_t mLock;
    pthread_cond_t mCondition;
    bool isWaitting;
    
    int write_pos;
    int read_pos;
    int cache_len;
    
    AudioFrame mAudioFrame;
private:
    float* float_pcm_data;
    int float_pcm_data_size;
    int16_t* int16_pcm_data;
    int int16_pcm_data_size;
    
    musly::resampler* mResampler;
    int input_rate;
    int output_rate;
private:
    int mPrimaryAudioSourceType;
    
    int mSecondaryAudioSourceType;
    ExternalAudioInput* mSecondaryExternalAudioInput;
    pthread_mutex_t mSecondaryExternalAudioInputLock;
};

#endif /* ExternalAudioInput_h */
