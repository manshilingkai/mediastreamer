//
//  ExternalAudioInput.cpp
//  MediaStreamer
//
//  Created by Think on 2018/6/8.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "ExternalAudioInput.h"
#include "MediaLog.h"
#include "MediaTimer.h"

#include "AudioChannelConvert.h"
#include "AudioSampleFormatConvert.h"
#include "AudioEndianConvert.h"
#include "AudioMixer.h"

ExternalAudioInput::ExternalAudioInput(int sampleRate, int numChannels)
{
    sampleRate_ = sampleRate;
    numChannels_ = numChannels;
    
    recBufSizeInBytes_ = 0;
    
    initialized_ = false;
    recording_ = false;
    
    isWaitting = false;
    
    pthread_mutex_init(&mRecordingLock, NULL);
    
    float_pcm_data = NULL;
    float_pcm_data_size = 0;
    int16_pcm_data = NULL;
    int16_pcm_data_size = 0;
    
    mResampler = NULL;
    input_rate = 0;
    output_rate = 0;
    
    mPrimaryAudioSourceType = -1;
    mSecondaryAudioSourceType = -1;
    mSecondaryExternalAudioInput = NULL;
    pthread_mutex_init(&mSecondaryExternalAudioInputLock, NULL);
}

ExternalAudioInput::~ExternalAudioInput()
{
    Terminate();
    
    pthread_mutex_destroy(&mRecordingLock);
    
    if (float_pcm_data) {
        delete [] float_pcm_data;
        float_pcm_data = NULL;
    }
    
    if (int16_pcm_data) {
        delete [] int16_pcm_data;
        int16_pcm_data = NULL;
    }
    
    if (mResampler) {
        delete mResampler;
        mResampler = NULL;
    }
    
    if (mSecondaryExternalAudioInput) {
        mSecondaryExternalAudioInput->StopRecording();
        mSecondaryExternalAudioInput->Terminate();
        delete mSecondaryExternalAudioInput;
        mSecondaryExternalAudioInput = NULL;
    }
    pthread_mutex_destroy(&mSecondaryExternalAudioInputLock);
}

void ExternalAudioInput::allocBuffers()
{
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mCondition, NULL);
    
    fifoSize = recBufSizeInBytes_*CalculateNumFifoBuffersNeeded();
    fifo = (uint8_t *)malloc(fifoSize);
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    mAudioFrame.data = (uint8_t*)malloc(recBufSizeInBytes_);
    mAudioFrame.frameSize = recBufSizeInBytes_;
    mAudioFrame.pts = 0;
    mAudioFrame.duration = 0;
}

void ExternalAudioInput::freeBuffers()
{
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mCondition);
    
    if (fifo!=NULL) {
        free(fifo);
        fifo = NULL;
    }
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    if(mAudioFrame.data!=NULL)
    {
        free(mAudioFrame.data);
        mAudioFrame.data = NULL;
    }
}

void ExternalAudioInput::flushBuffers()
{
    pthread_mutex_lock(&mLock);
    cache_len = 0;
    write_pos = 0;
    read_pos = 0;
    pthread_mutex_unlock(&mLock);
}

void ExternalAudioInput::setRecBufSizeInBytes(int bufSizeInBytes)
{
    recBufSizeInBytes_ = bufSizeInBytes;
}

int ExternalAudioInput::Init(AudioCaptureMode mode)
{
    if (initialized_) {
        LOGW("Already initialized");
        return 0;
    }
    
    allocBuffers();
    
    initialized_ = true;
    
    return 0;
}

int ExternalAudioInput::Terminate()
{
    if (!initialized_) {
        LOGW("Already terminated");
        return 0;
    }
    
    freeBuffers();
    
    initialized_ = false;
    
    return 0;
}

int ExternalAudioInput::StartRecording()
{
    if (recording_) {
        LOGW("Recording already started");
        return 0;
    }
    
    pthread_mutex_lock(&mRecordingLock);
    recording_ = true;
    pthread_mutex_unlock(&mRecordingLock);
    
    pthread_mutex_lock(&mSecondaryExternalAudioInputLock);
    if (mSecondaryExternalAudioInput) {
        mSecondaryExternalAudioInput->StartRecording();
    }
    pthread_mutex_unlock(&mSecondaryExternalAudioInputLock);
    
    return 0;
}

int ExternalAudioInput::StopRecording()
{
    if (!recording_) {
        LOGW("Recording already stoped");
        return 0;
    }
    
    pthread_mutex_lock(&mRecordingLock);
    recording_ = false;
    pthread_mutex_unlock(&mRecordingLock);
    
    flushBuffers();
    
    pthread_mutex_lock(&mSecondaryExternalAudioInputLock);
    if (mSecondaryExternalAudioInput) {
        mSecondaryExternalAudioInput->StopRecording();
    }
    pthread_mutex_unlock(&mSecondaryExternalAudioInputLock);
    
    return 0;
}

void ExternalAudioInput::inputAudioFrame(AudioFrame *inAudioFrame)
{
    pthread_mutex_lock(&mRecordingLock);
    if (!recording_) {
        pthread_mutex_unlock(&mRecordingLock);
        return;
    }
    pthread_mutex_unlock(&mRecordingLock);

    int16_t* out = NULL;
    int out_size = 0;

    if (inAudioFrame->isBigEndian) {
        big_endian_to_little_endian((int16_t*)inAudioFrame->data, inAudioFrame->frameSize/2);
    }
    
    if (inAudioFrame->channels==2 && numChannels_==1) {
        int16_t* stereo = (int16_t*)inAudioFrame->data;
        int samples_per_channel = inAudioFrame->frameSize/2/inAudioFrame->channels;
                
        if (int16_pcm_data==NULL) {
            int16_pcm_data = new int16_t[samples_per_channel];
            int16_pcm_data_size = samples_per_channel;
        }
        if (int16_pcm_data_size<samples_per_channel) {
            if (int16_pcm_data) {
                delete [] int16_pcm_data;
                int16_pcm_data = NULL;
            }
            int16_pcm_data = new int16_t[samples_per_channel];
            int16_pcm_data_size = samples_per_channel;
        }
        int16_t* mono = int16_pcm_data;
        int mono_size = samples_per_channel;
        ff_stereo_to_mono(mono, stereo, samples_per_channel);
        
        if (inAudioFrame->sampleRate!=sampleRate_) {
            if (float_pcm_data==NULL) {
                float_pcm_data = new float[mono_size];
                float_pcm_data_size = mono_size;
            }
            if (float_pcm_data_size<mono_size) {
                if (float_pcm_data) {
                    delete [] float_pcm_data;
                    float_pcm_data = NULL;
                }
                float_pcm_data = new float[mono_size];
                float_pcm_data_size = mono_size;
            }
            
            src_short_to_float_array(mono,float_pcm_data,mono_size);
            
            if (mResampler==NULL) {
                mResampler = new musly::resampler(inAudioFrame->sampleRate, sampleRate_);
                input_rate = inAudioFrame->sampleRate;
                output_rate = sampleRate_;
            }
            if (input_rate!=inAudioFrame->sampleRate || output_rate!=sampleRate_) {
                if (mResampler) {
                    delete mResampler;
                    mResampler = NULL;
                }
                
                mResampler = new musly::resampler(inAudioFrame->sampleRate, sampleRate_);
                input_rate = inAudioFrame->sampleRate;
                output_rate = sampleRate_;
            }
            
            std::vector<float> resampled_float_pcm_data = mResampler->resample(float_pcm_data, mono_size);
            size_t resampled_float_pcm_data_size = resampled_float_pcm_data.size();
            if (int16_pcm_data_size<resampled_float_pcm_data_size) {
                if (int16_pcm_data) {
                    delete [] int16_pcm_data;
                    int16_pcm_data = NULL;
                }
                int16_pcm_data = new int16_t[resampled_float_pcm_data_size];
                int16_pcm_data_size = (int)resampled_float_pcm_data_size;
            }
            
            src_float_to_short_array(resampled_float_pcm_data.data(), int16_pcm_data, (int)resampled_float_pcm_data_size);
            
            out = int16_pcm_data;
            out_size = (int)resampled_float_pcm_data_size;
        }else{
            out = mono;
            out_size = mono_size;
        }
        
    }else if(inAudioFrame->channels==1 && numChannels_==2){
        int16_t* mono = (int16_t*)inAudioFrame->data;
        int samples_per_channel = inAudioFrame->frameSize/2/inAudioFrame->channels;
                
        if (int16_pcm_data==NULL) {
            int16_pcm_data = new int16_t[numChannels_*samples_per_channel];
            int16_pcm_data_size = numChannels_*samples_per_channel;
        }
        if (int16_pcm_data_size<numChannels_*samples_per_channel) {
            if (int16_pcm_data) {
                delete [] int16_pcm_data;
                int16_pcm_data = NULL;
            }
            int16_pcm_data = new int16_t[numChannels_*samples_per_channel];
            int16_pcm_data_size = numChannels_*samples_per_channel;
        }
        
        int16_t* stereo = int16_pcm_data;
        int stereo_size = numChannels_*samples_per_channel;
        ff_mono_to_stereo(stereo, mono, samples_per_channel);
        
        if (inAudioFrame->sampleRate!=sampleRate_) {
            if (float_pcm_data==NULL) {
                float_pcm_data = new float[stereo_size];
                float_pcm_data_size = stereo_size;
            }
            if (float_pcm_data_size<stereo_size) {
                if (float_pcm_data) {
                    delete [] float_pcm_data;
                    float_pcm_data = NULL;
                }
                float_pcm_data = new float[stereo_size];
                float_pcm_data_size = stereo_size;
            }
            
            src_short_to_float_array(stereo,float_pcm_data,stereo_size);
            
            if (mResampler==NULL) {
                mResampler = new musly::resampler(inAudioFrame->sampleRate, sampleRate_);
                input_rate = inAudioFrame->sampleRate;
                output_rate = sampleRate_;
            }
            if (input_rate!=inAudioFrame->sampleRate || output_rate!=sampleRate_) {
                if (mResampler) {
                    delete mResampler;
                    mResampler = NULL;
                }
                
                mResampler = new musly::resampler(inAudioFrame->sampleRate, sampleRate_);
                input_rate = inAudioFrame->sampleRate;
                output_rate = sampleRate_;
            }
            
            std::vector<float> resampled_float_pcm_data = mResampler->resample(float_pcm_data, stereo_size);
            size_t resampled_float_pcm_data_size = resampled_float_pcm_data.size();
            if (int16_pcm_data_size<resampled_float_pcm_data_size) {
                if (int16_pcm_data) {
                    delete [] int16_pcm_data;
                    int16_pcm_data = NULL;
                }
                int16_pcm_data = new int16_t[resampled_float_pcm_data_size];
                int16_pcm_data_size = (int)resampled_float_pcm_data_size;
            }
            
            src_float_to_short_array(resampled_float_pcm_data.data(), int16_pcm_data, (int)resampled_float_pcm_data_size);
            
            out = int16_pcm_data;
            out_size = (int)resampled_float_pcm_data_size;
        }else{
            out = stereo;
            out_size = stereo_size;
        }
    }else{
        int16_t* input = (int16_t*)inAudioFrame->data;
        int input_size = inAudioFrame->frameSize/2;
                
        if (inAudioFrame->sampleRate!=sampleRate_) {
            if (float_pcm_data==NULL) {
                float_pcm_data = new float[input_size];
                float_pcm_data_size = input_size;
            }
            if (float_pcm_data_size<input_size) {
                if (float_pcm_data) {
                    delete [] float_pcm_data;
                    float_pcm_data = NULL;
                }
                float_pcm_data = new float[input_size];
                float_pcm_data_size = input_size;
            }
            
            src_short_to_float_array(input,float_pcm_data,input_size);
            
            if (mResampler==NULL) {
                mResampler = new musly::resampler(inAudioFrame->sampleRate, sampleRate_);
                input_rate = inAudioFrame->sampleRate;
                output_rate = sampleRate_;
            }
            if (input_rate!=inAudioFrame->sampleRate || output_rate!=sampleRate_) {
                if (mResampler) {
                    delete mResampler;
                    mResampler = NULL;
                }
                
                mResampler = new musly::resampler(inAudioFrame->sampleRate, sampleRate_);
                input_rate = inAudioFrame->sampleRate;
                output_rate = sampleRate_;
            }
            
            std::vector<float> resampled_float_pcm_data = mResampler->resample(float_pcm_data, input_size);
            size_t resampled_float_pcm_data_size = resampled_float_pcm_data.size();
            if (int16_pcm_data_size<resampled_float_pcm_data_size) {
                if (int16_pcm_data) {
                    delete [] int16_pcm_data;
                    int16_pcm_data = NULL;
                }
                int16_pcm_data = new int16_t[resampled_float_pcm_data_size];
                int16_pcm_data_size = (int)resampled_float_pcm_data_size;
            }
            
            src_float_to_short_array(resampled_float_pcm_data.data(), int16_pcm_data, (int)resampled_float_pcm_data_size);
            
            out = int16_pcm_data;
            out_size = (int)resampled_float_pcm_data_size;
        }else{
            out = (int16_t*)inAudioFrame->data;
            out_size = inAudioFrame->frameSize/2;
        }
    }
    
    uint8_t* data = (uint8_t*)out;
    int size = out_size*2;
    
//    uint8_t* data = (uint8_t*)inAudioFrame->data;
//    int size = inAudioFrame->frameSize;
    
    pthread_mutex_lock(&mLock);
    if(cache_len + size>fifoSize)
    {
        // is full
    }else
    {
        if(size>fifoSize-write_pos)
        {
            memcpy(fifo+write_pos,data,fifoSize-write_pos);
            memcpy(fifo,data+fifoSize-write_pos,size-(fifoSize-write_pos));
            write_pos = size-(fifoSize-write_pos);
        }else
        {
            memcpy(fifo+write_pos,data,size);
            write_pos = write_pos+size;
        }
        
        cache_len = cache_len+size;
    }
    pthread_mutex_unlock(&mLock);
}

void ExternalAudioInput::inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType)
{
    pthread_mutex_lock(&mRecordingLock);
    if (!recording_) {
        pthread_mutex_unlock(&mRecordingLock);
        return;
    }
    pthread_mutex_unlock(&mRecordingLock);
    
    if (mPrimaryAudioSourceType==-1 && mSecondaryAudioSourceType==-1) {
        mPrimaryAudioSourceType = audioSourceType;
    }else if (mPrimaryAudioSourceType!=-1 && mSecondaryAudioSourceType==-1) {
        if (mPrimaryAudioSourceType!=audioSourceType) {
            mSecondaryAudioSourceType = audioSourceType;
            pthread_mutex_lock(&mSecondaryExternalAudioInputLock);
            if (mSecondaryExternalAudioInput) {
                mSecondaryExternalAudioInput->StopRecording();
                mSecondaryExternalAudioInput->Terminate();
                delete mSecondaryExternalAudioInput;
                mSecondaryExternalAudioInput = NULL;
            }
            mSecondaryExternalAudioInput = new ExternalAudioInput(sampleRate_, numChannels_);
            mSecondaryExternalAudioInput->setRecBufSizeInBytes(recBufSizeInBytes_);
            mSecondaryExternalAudioInput->Init();
            mSecondaryExternalAudioInput->StartRecording();
            pthread_mutex_unlock(&mSecondaryExternalAudioInputLock);
        }
    }
    
    if (mPrimaryAudioSourceType==audioSourceType) {
        this->inputAudioFrame(inAudioFrame);
    }else if (mSecondaryAudioSourceType==audioSourceType){
        pthread_mutex_lock(&mSecondaryExternalAudioInputLock);
        if (mSecondaryExternalAudioInput) {
            mSecondaryExternalAudioInput->inputAudioFrame(inAudioFrame);
        }
        pthread_mutex_unlock(&mSecondaryExternalAudioInputLock);
    }
}

int64_t ExternalAudioInput::getLatency()
{
    return static_cast<int64_t>(cache_len*1000000/sampleRate_*numChannels_*2);
}

int ExternalAudioInput::CalculateNumFifoBuffersNeeded() {
    return 8;
}

AudioFrame* ExternalAudioInput::frontAudioFrame()
{
    AudioFrame* audioFrame = NULL;
    
    pthread_mutex_lock(&mLock);
    if (cache_len >= recBufSizeInBytes_) {
        
        if(recBufSizeInBytes_>fifoSize-read_pos)
        {
            memcpy(mAudioFrame.data,fifo+read_pos,fifoSize-read_pos);
            memcpy(mAudioFrame.data+fifoSize-read_pos,fifo,recBufSizeInBytes_-(fifoSize-read_pos));
            read_pos = recBufSizeInBytes_-(fifoSize-read_pos);
        }else
        {
            memcpy(mAudioFrame.data,fifo+read_pos,recBufSizeInBytes_);
            read_pos = read_pos + recBufSizeInBytes_;
        }
        
        mAudioFrame.frameSize = recBufSizeInBytes_;
        mAudioFrame.duration = float(recBufSizeInBytes_*1000)/float(sampleRate_*numChannels_*2);
        
        mAudioFrame.pts = MediaTimer::GetNowMediaTimeMS();
        cache_len = cache_len - recBufSizeInBytes_;
        
        audioFrame = &mAudioFrame;
    }
    pthread_mutex_unlock(&mLock);
    
    AudioFrame* primaryAudioFrame = audioFrame;
    AudioFrame* secondaryAudioFrame = NULL;
    pthread_mutex_lock(&mSecondaryExternalAudioInputLock);
    if (mSecondaryExternalAudioInput) {
        secondaryAudioFrame = mSecondaryExternalAudioInput->frontAudioFrame();
    }
    pthread_mutex_unlock(&mSecondaryExternalAudioInputLock);
    if (primaryAudioFrame && secondaryAudioFrame) {
        mixSamples((int16_t *)primaryAudioFrame->data, (int16_t *)secondaryAudioFrame->data, recBufSizeInBytes_/2);
    }
    
    return audioFrame;
}

void ExternalAudioInput::popAudioFrame()
{
    //no need do anything.
}
