//
//  JniAudioCapturer.h
//  AndroidMediaStreamer
//
//  Created by Think on 2017/1/12.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef JniAudioCapturer_h
#define JniAudioCapturer_h

#include <stdio.h>
#include <pthread.h>

#include "AudioCapture.h"

class JniAudioCapturer : public AudioCapture{
public:
    JniAudioCapturer(int sampleRate, int numChannels);
    ~JniAudioCapturer();
    
    void registerJavaVMEnv(JavaVM *jvm);
    
    void setPCMDataOutputer(IPCMDataOutputer *outputer);
    
    void setRecBufSizeInBytes(int bufSizeInBytes);
    
    // Main initializaton and termination
    int Init(AudioCaptureMode mode = DEFAULT_MODE);
    int Terminate();
    bool Initialized() { return initialized_; }
    
    // Audio transport control
    int StartRecording();
    int StopRecording();
    bool Recording() { return recording_; }
    
    void setVolume(float volume) {}
    
    void inputAudioFrame(AudioFrame *inAudioFrame) {};
    void inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType) {};
    
    AudioFrame* frontAudioFrame();
    void popAudioFrame();
    
    void OnCacheDirectBufferAddress(jobject byte_buffer);
    void OnDataIsRecorded(int length);
private:
    JavaVM *mJvm;
    JNIEnv *mEnv;
    
    bool initialized_;
    bool recording_;
    
    int sampleRate_;
    int numChannels_;
    
    int recBufSizeInBytes_;

    //
    jobject audioCapturerJavaObject;
    int direct_buffer_capacity_in_bytes_;

    void* direct_buffer_address_;

    //fifo
    uint8_t *fifo;
    int fifoSize;
    
    void allocBuffers();
    void freeBuffers();
    void flushBuffers();
    
    pthread_mutex_t mLock;
    pthread_cond_t mCondition;
    bool isWaitting;
    
    int write_pos;
    int read_pos;
    int cache_len;
    
    AudioFrame mOutputAudioFrame;
    
private:
    IPCMDataOutputer *mPCMDataOutputer;
};

#endif /* JniAudioCapturer_h */
