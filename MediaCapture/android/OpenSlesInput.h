//
//  OpenSlesInput.h
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef OpenSlesInput_h
#define OpenSlesInput_h

#include <stdio.h>

#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#include <SLES/OpenSLES_AndroidConfiguration.h>

#include "AudioFrameBufferPool.h"
#include "AudioCapture.h"

// OpenSL implementation that facilitate capturing PCM data from an android
// device's microphone.
// This class is Thread-compatible. I.e. Given an instance of this class, calls
// to non-const methods require exclusive access to the object.
class OpenSlesInput : public AudioCapture{
public:
    OpenSlesInput(int sampleRate, int numChannels);
    ~OpenSlesInput();
    
    void registerJavaVMEnv(JavaVM *jvm);
    
    void setPCMDataOutputer(IPCMDataOutputer *outputer);
    
    void setRecBufSizeInBytes(int bufSizeInBytes);
    
    // Main initializaton and termination
    int Init(AudioCaptureMode mode = DEFAULT_MODE);
    int Terminate();
    bool Initialized() { return initialized_; }
    
    // Audio transport control
    int StartRecording();
    int StopRecording();
    bool Recording() { return recording_; }
    
    void setVolume(float volume) {}
    
    void inputAudioFrame(AudioFrame *inAudioFrame) {};
    void inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType) {};
    
    AudioFrame* frontAudioFrame();
    void popAudioFrame();

private:
    enum {
        kDefaultSampleRate = 44100,
        kNumChannels = 2
    };

    enum {
        kNumInterfaces = 2,
        kDefaultBufSizeInSamples = kDefaultSampleRate * 10 / 1000,
        kDefaultBufSizeInBytes = kNumChannels * kDefaultBufSizeInSamples * sizeof(int16_t),
        // Keep as few OpenSL buffers as possible to avoid wasting memory. 2 is
        // minimum for playout. Keep 2 for recording as well.
        kNumOpenSlBuffers = 2,
        kNumRecBuffers = 3,
    };
    
    int sampleRate_;
    int numChannels_;
    int recBufSizeInBytes_;
    
    SLDataFormat_PCM CreatePcmConfiguration(int sample_rate);
    
    int CalculateNumFifoBuffersNeeded();
    void AllocateBuffers();
    void FreeBuffers();
    bool EnqueueAllBuffers();
    // This function also configures the audio recorder, e.g. sample rate to use
    // etc, so it should be called when starting recording.
    bool CreateAudioRecorder();
    void DestroyAudioRecorder();

    static void RecorderSimpleBufferQueueCallback(
                                                  SLAndroidSimpleBufferQueueItf queueItf,
                                                  void* pContext);
    // This function must not take any locks or do any heavy work. It is a
    // requirement for the OpenSL implementation to work as intended. The reason
    // for this is that taking locks exposes the OpenSL thread to the risk of
    // priority inversion.
    void RecorderSimpleBufferQueueCallbackHandler(
                                                  SLAndroidSimpleBufferQueueItf queueItf);

    bool initialized_;
    // This member controls the starting and stopping of recording audio to the
    // the device.
    bool recording_;
    
    // OpenSL handles
    SLObjectItf sles_engine_;
    SLEngineItf sles_engine_itf_;
    SLObjectItf sles_recorder_;
    SLRecordItf sles_recorder_itf_;
    SLAndroidSimpleBufferQueueItf sles_recorder_sbq_itf_;
    
    // Index in |rec_buf_| pointing to the audio buffer that will be ready the
    // next time RecorderSimpleBufferQueueCallbackHandler is invoked.
    // Ready means buffer contains audio data from the device.
    int active_queue_;
    uint8_t *rec_buf_[kNumOpenSlBuffers+kNumRecBuffers];
    AudioFrameBufferPool *fifo_;
    // Only one thread, T1, may push and only one thread, T2, may pull. T1 may or
    // may not be the same thread as T2. T2 is the process thread and T1 is the
    // OpenSL thread.
    int num_fifo_buffers_needed_;
    
    AudioFrame audioFrame;
    
    pthread_mutex_t mLock;
    pthread_cond_t mCondition;
    bool isWaittiing;
    
private:
    IPCMDataOutputer *mPCMDataOutputer;
};

#endif /* OpenSlesInput_h */
