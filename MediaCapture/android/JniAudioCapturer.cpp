//
//  JniAudioCapturer.cpp
//  AndroidMediaStreamer
//
//  Created by Think on 2017/1/12.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include <assert.h>
#include "JniAudioCapturer.h"
#include "AndroidUtils.h"
#include "MediaLog.h"
#include "MediaTimer.h"

#include "android_slkmedia_mediastreamer.h"
//use : g_audio_capturer_class

JniAudioCapturer::JniAudioCapturer(int sampleRate, int numChannels)
{
    mJvm = NULL;
    mEnv = NULL;
    
    initialized_ = false;
    recording_ = false;
    
    sampleRate_ = sampleRate;
    numChannels_ = numChannels;
    
    recBufSizeInBytes_ = 0;
    
    direct_buffer_capacity_in_bytes_ = 0;
    
    direct_buffer_address_ = NULL;
    
    mPCMDataOutputer = NULL;
}

JniAudioCapturer::~JniAudioCapturer()
{
    Terminate();
}

void JniAudioCapturer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
    mEnv = AndroidUtils::getJNIEnv(mJvm);
}

void JniAudioCapturer::setPCMDataOutputer(IPCMDataOutputer *outputer)
{
    mPCMDataOutputer = outputer;
}

void JniAudioCapturer::setRecBufSizeInBytes(int bufSizeInBytes)
{
    recBufSizeInBytes_ = bufSizeInBytes;
}

int JniAudioCapturer::Init(AudioCaptureMode mode)
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    if (initialized_) {
        LOGW("Already initialized");
        return 0;
    }
    
    jmethodID audio_capturer_init_method = mEnv->GetMethodID(g_audio_capturer_class, "<init>", "(J)V");
    if (mEnv->ExceptionOccurred()) {

        LOGE("Fail to GetMethodID for class [android/slkmedia/mediastreamer/audiocapture/AudioCapturer] and method [<init>]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    jobject audioCapturerObject = mEnv->NewObject(g_audio_capturer_class, audio_capturer_init_method, (jlong)this);
    if (mEnv->ExceptionOccurred()) {
        
        LOGE("Fail to NewObject for Class [android/slkmedia/mediastreamer/audiocapture/AudioCapturer]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    this->audioCapturerJavaObject = mEnv->NewGlobalRef(audioCapturerObject);
    mEnv->DeleteLocalRef(audioCapturerObject);

    jmethodID audio_capturer_initRecording_method = mEnv->GetMethodID(g_audio_capturer_class, "initRecording", "(II)I");
    if (mEnv->ExceptionOccurred()) {
        mEnv->DeleteGlobalRef(this->audioCapturerJavaObject);

        LOGE("Fail to GetMethodID for class [android/slkmedia/mediastreamer/audiocapture/AudioCapturer] and method [initRecording]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    int frames_per_buffer = mEnv->CallIntMethod(this->audioCapturerJavaObject, audio_capturer_initRecording_method, sampleRate_, numChannels_);
    if (mEnv->ExceptionOccurred()) {
        mEnv->DeleteGlobalRef(this->audioCapturerJavaObject);

        LOGE("Fail to CallIntMethod for class [android/slkmedia/mediastreamer/audiocapture/AudioCapturer] and method [initRecording]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    if (frames_per_buffer < 0) {
        mEnv->DeleteGlobalRef(this->audioCapturerJavaObject);

        LOGE("InitRecording failed!");
        return -1;
    }
    
    int bytes_per_frame = numChannels_ * sizeof(int16_t);
    if (direct_buffer_capacity_in_bytes_!=frames_per_buffer * bytes_per_frame) {
        mEnv->DeleteGlobalRef(this->audioCapturerJavaObject);

        LOGE("direct_buffer_capacity_in_bytes_!=frames_per_buffer * bytes_per_frame");
        
        return -1;
    }
    
    allocBuffers();
    
    initialized_ = true;
    return 0;
}

int JniAudioCapturer::StartRecording()
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    if (recording_) {
        LOGW("Recording already started");
        return 0;
    }
    
    jmethodID audio_capturer_startRecording_method = mEnv->GetMethodID(g_audio_capturer_class, "startRecording", "()Z");
    if (mEnv->ExceptionOccurred()) {
        LOGE("Fail to GetMethodID for class [android/slkmedia/mediastreamer/audiocapture/AudioCapturer] and method [startRecording]");
        mEnv->ExceptionClear();
        
        return -1;
    }

    jboolean ret = mEnv->CallBooleanMethod(this->audioCapturerJavaObject, audio_capturer_startRecording_method);
    if (mEnv->ExceptionOccurred()) {
        LOGE("Fail to CallBooleanMethod for class [android/slkmedia/mediastreamer/audiocapture/AudioCapturer] and method [startRecording]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    if (ret==JNI_FALSE) {
        LOGE("startRecording failed!");

        return -1;
    }
    
    recording_ = true;
    
    return 0;
}

int JniAudioCapturer::StopRecording()
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    if (!recording_) {
        LOGW("Recording already stoped");
        return 0;
    }
    
    jmethodID audio_capturer_stopRecording_method = mEnv->GetMethodID(g_audio_capturer_class, "stopRecording", "()Z");
    if (mEnv->ExceptionOccurred()) {
        LOGE("Fail to GetMethodID for class [android/slkmedia/mediastreamer/audiocapture/AudioCapturer] and method [stopRecording]");
        mEnv->ExceptionClear();
        
        return -1;
    }

    jboolean ret = mEnv->CallBooleanMethod(this->audioCapturerJavaObject, audio_capturer_stopRecording_method);
    if (mEnv->ExceptionOccurred()) {
        LOGE("Fail to CallBooleanMethod for class [android/slkmedia/mediastreamer/audiocapture/AudioCapturer] and method [stopRecording]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    if (ret==JNI_FALSE) {
        LOGE("stopRecording failed!");
        
        return -1;
    }
    //
    flushBuffers();

    recording_ = false;
    
    return 0;
}

int JniAudioCapturer::Terminate()
{
    StopRecording();
    
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    if (!initialized_) {
        LOGW("Already terminated");
        return 0;
    }
    
    jmethodID audio_capturer_terminateRecording_method = mEnv->GetMethodID(g_audio_capturer_class, "terminateRecording", "()V");
    if (mEnv->ExceptionOccurred()) {
        
        LOGE("Fail to GetMethodID for class [android/slkmedia/mediastreamer/audiocapture/AudioCapturer] and method [terminateRecording]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    mEnv->CallVoidMethod(this->audioCapturerJavaObject,audio_capturer_terminateRecording_method);
    if (mEnv->ExceptionOccurred()) {
        LOGE("Fail to CallVoidMethod for class [android/slkmedia/mediastreamer/audiocapture/AudioCapturer] and method [terminateRecording]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    mEnv->DeleteGlobalRef(this->audioCapturerJavaObject);

    //
    freeBuffers();

    initialized_ = false;
    return 0;
}

void JniAudioCapturer::OnCacheDirectBufferAddress(jobject byte_buffer)
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    direct_buffer_address_ = mEnv->GetDirectBufferAddress(byte_buffer);
    jlong capacity = mEnv->GetDirectBufferCapacity(byte_buffer);

    LOGD("direct buffer capacity: %lld", capacity);
    direct_buffer_capacity_in_bytes_ = static_cast<size_t>(capacity);
}

void JniAudioCapturer::OnDataIsRecorded(int length)
{
    uint8_t* data = (uint8_t*)direct_buffer_address_;
    int size = length;
    
    if (recording_)
    {
        if (mPCMDataOutputer!=NULL) {
            mPCMDataOutputer->onOutputPCMData(data, size);
        }else{
            pthread_mutex_lock(&mLock);
            
            if(cache_len + size>fifoSize)
            {
                // is full
                LOGW("is full");
            }else
            {
                if(size>fifoSize-write_pos)
                {
                    memcpy(fifo+write_pos,data,fifoSize-write_pos);
                    memcpy(fifo,data+fifoSize-write_pos,size-(fifoSize-write_pos));
                    write_pos = size-(fifoSize-write_pos);
                }else
                {
                    memcpy(fifo+write_pos,data,size);
                    write_pos = write_pos+size;
                }
                
                cache_len = cache_len+size;
            }
            
            if (isWaitting) {
                isWaitting = false;
                pthread_cond_signal(&mCondition);
            }
            
            pthread_mutex_unlock(&mLock);
        }
    }
}

AudioFrame* JniAudioCapturer::frontAudioFrame()
{
    AudioFrame* audioFrame = NULL;
    
    while (true) {
        pthread_mutex_lock(&mLock);
        
        if (cache_len >= recBufSizeInBytes_) {
            
            if(recBufSizeInBytes_>fifoSize-read_pos)
            {
                memcpy(mOutputAudioFrame.data,fifo+read_pos,fifoSize-read_pos);
                memcpy(mOutputAudioFrame.data+fifoSize-read_pos,fifo,recBufSizeInBytes_-(fifoSize-read_pos));
                read_pos = recBufSizeInBytes_-(fifoSize-read_pos);
            }else
            {
                memcpy(mOutputAudioFrame.data,fifo+read_pos,recBufSizeInBytes_);
                read_pos = read_pos + recBufSizeInBytes_;
            }
            
            mOutputAudioFrame.frameSize = recBufSizeInBytes_;
            mOutputAudioFrame.duration = float(recBufSizeInBytes_*1000)/float(sampleRate_*numChannels_*2);
            
            mOutputAudioFrame.pts = MediaTimer::GetNowMediaTimeMS();
            cache_len = cache_len - recBufSizeInBytes_;
            
            audioFrame = &mOutputAudioFrame;
            
            pthread_mutex_unlock(&mLock);
            
            break;
            
        }else{
            isWaitting = true;
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
    }
    
    return audioFrame;
}

void JniAudioCapturer::popAudioFrame()
{
    //no need do anything.
}

void JniAudioCapturer::allocBuffers()
{
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mCondition, NULL);
    
    fifoSize = recBufSizeInBytes_*4;
    fifo = (uint8_t *)malloc(fifoSize);
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    mOutputAudioFrame.data = (uint8_t*)malloc(recBufSizeInBytes_);
    mOutputAudioFrame.frameSize = recBufSizeInBytes_;
    mOutputAudioFrame.pts = 0;
    mOutputAudioFrame.duration = 0;
    
    isWaitting = false;
}

void JniAudioCapturer::freeBuffers()
{
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mCondition);
    
    if (fifo!=NULL) {
        free(fifo);
        fifo = NULL;
    }
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    if(mOutputAudioFrame.data!=NULL)
    {
        free(mOutputAudioFrame.data);
        mOutputAudioFrame.data = NULL;
    }
}

void JniAudioCapturer::flushBuffers()
{
    pthread_mutex_lock(&mLock);
    cache_len = 0;
    write_pos = 0;
    read_pos = 0;
    pthread_mutex_unlock(&mLock);
}
