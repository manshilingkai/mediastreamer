//
//  OpenSlesInput.cpp
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "OpenSlesInput.h"
#include "MediaLog.h"
#include "MediaTimer.h"
#include <assert.h>

#define VOID_RETURN
#define OPENSL_RETURN_ON_FAILURE(op, ret_val)                    \
  do {                                                           \
    SLresult err = (op);                                         \
    if (err != SL_RESULT_SUCCESS) {                              \
      LOGE("OpenSL error: %d", err);                             \
      assert(false);                                             \
      return ret_val;                                            \
    }                                                            \
} while (0)

static const SLEngineOption kOption[] = {
    { SL_ENGINEOPTION_THREADSAFE, static_cast<SLuint32>(SL_BOOLEAN_TRUE) },
};

OpenSlesInput::OpenSlesInput(int sampleRate, int numChannels)
{
    initialized_ = false;
    recording_ = false;
    
    sles_engine_ = NULL;
    sles_engine_itf_ = NULL;
    sles_recorder_ = NULL;
    sles_recorder_itf_ = NULL;
    sles_recorder_sbq_itf_ = NULL;
    
    active_queue_ = 0;
    fifo_ = NULL;
    num_fifo_buffers_needed_ = 0;
    
    sampleRate_ = sampleRate;
    numChannels_ = numChannels;
    
    recBufSizeInBytes_ = 0;
    isWaittiing = false;
    
    mPCMDataOutputer = NULL;
}

OpenSlesInput::~OpenSlesInput()
{
    Terminate();
}

void OpenSlesInput::registerJavaVMEnv(JavaVM *jvm)
{
}

void OpenSlesInput::setPCMDataOutputer(IPCMDataOutputer *outputer)
{
    mPCMDataOutputer = outputer;
}

void OpenSlesInput::setRecBufSizeInBytes(int bufSizeInBytes)
{
    recBufSizeInBytes_ = bufSizeInBytes;
}

int OpenSlesInput::Init(AudioCaptureMode mode)
{
    if (initialized_) {
        LOGW("Already initialized");
        return 0;
    }
    
    // Set up OpenSL engine.
    OPENSL_RETURN_ON_FAILURE(slCreateEngine(&sles_engine_, 1, kOption, 0,
                                            NULL, NULL),
                             -1);
    OPENSL_RETURN_ON_FAILURE((*sles_engine_)->Realize(sles_engine_,
                                                      SL_BOOLEAN_FALSE),
                             -1);
    OPENSL_RETURN_ON_FAILURE((*sles_engine_)->GetInterface(sles_engine_,
                                                           SL_IID_ENGINE,
                                                           &sles_engine_itf_),
                             -1);
    AllocateBuffers();
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mCondition, NULL);
    
    initialized_ = true;
    return 0;
}

int OpenSlesInput::Terminate() {
    StopRecording();
    
    // It is assumed that the caller has stopped recording before terminating.
    if (!initialized_) {
        LOGW("Already terminated");
        return 0;
    }
    
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mCondition);
    
    FreeBuffers();
    
    (*sles_engine_)->Destroy(sles_engine_);
    
    initialized_ = false;
    return 0;
}

int OpenSlesInput::StartRecording() {

    if (recording_) {
        LOGW("Recording already started");
        return 0;
    }
    
    if (!CreateAudioRecorder()) {
        return -1;
    }
    // Setup to receive buffer queue event callbacks.
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_recorder_sbq_itf_)->RegisterCallback(
                                                                         sles_recorder_sbq_itf_,
                                                                         RecorderSimpleBufferQueueCallback,
                                                                         this),
                             -1);
    if (!EnqueueAllBuffers()) {
        return -1;
    }

    OPENSL_RETURN_ON_FAILURE(
                             (*sles_recorder_itf_)->SetRecordState(sles_recorder_itf_,
                                                                   SL_RECORDSTATE_RECORDING),
                             false);

    recording_ = true;
    
    return 0;
}

int OpenSlesInput::StopRecording()
{
    if (!recording_) {
        LOGW("Recording already stoped");
        return 0;
    }
    
    if (sles_recorder_itf_) {
        OPENSL_RETURN_ON_FAILURE(
                                 (*sles_recorder_itf_)->SetRecordState(sles_recorder_itf_,
                                                                       SL_RECORDSTATE_STOPPED),
                                 VOID_RETURN);
    }
    
    DestroyAudioRecorder();
    
    fifo_->flush();
    
    recording_ = false;
    
    return 0;
}

int OpenSlesInput::CalculateNumFifoBuffersNeeded() {
    num_fifo_buffers_needed_ = 4;
    return num_fifo_buffers_needed_;
}

void OpenSlesInput::AllocateBuffers() {
    for (int i =0; i<kNumOpenSlBuffers+kNumRecBuffers; i++) {
        rec_buf_[i] = (uint8_t *)malloc(recBufSizeInBytes_);
    }
    fifo_ = new AudioFrameBufferPool(recBufSizeInBytes_,CalculateNumFifoBuffersNeeded());
}

void OpenSlesInput::FreeBuffers() {
    for (int i =0; i<kNumOpenSlBuffers+kNumRecBuffers; i++) {
        if (rec_buf_[i]!=NULL) {
            free(rec_buf_[i]);
            rec_buf_[i] = NULL;
        }
    }
    
    if (fifo_!=NULL) {
        delete fifo_;
        fifo_ = NULL;
    }
}

bool OpenSlesInput::EnqueueAllBuffers()
{
    active_queue_ = 0;
    
    for (int i = 0; i < kNumOpenSlBuffers; ++i) {
        memset(rec_buf_[i], 0, recBufSizeInBytes_);
        OPENSL_RETURN_ON_FAILURE(
                                 (*sles_recorder_sbq_itf_)->Enqueue(
                                                                    sles_recorder_sbq_itf_,
                                                                    reinterpret_cast<void*>(rec_buf_[i]),
                                                                    recBufSizeInBytes_),
                                 false);
    }
    
    return true;
}

bool OpenSlesInput::CreateAudioRecorder() {
    SLDataLocator_IODevice micLocator = {
        SL_DATALOCATOR_IODEVICE, SL_IODEVICE_AUDIOINPUT,
        SL_DEFAULTDEVICEID_AUDIOINPUT, NULL };
    SLDataSource audio_source = { &micLocator, NULL };
    SLDataLocator_AndroidSimpleBufferQueue simple_buf_queue = {
        SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE,
        static_cast<SLuint32>(kNumOpenSlBuffers+kNumRecBuffers)
    };
    SLDataFormat_PCM configuration = CreatePcmConfiguration(sampleRate_);
    SLDataSink audio_sink = { &simple_buf_queue, &configuration };
    // Interfaces for recording android audio data and Android are needed.
    // Note the interfaces still need to be initialized. This only tells OpenSl
    // that the interfaces will be needed at some point.
    const SLInterfaceID id[kNumInterfaces] = {
        SL_IID_ANDROIDSIMPLEBUFFERQUEUE, SL_IID_ANDROIDCONFIGURATION };
    const SLboolean req[kNumInterfaces] = {
        SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE };
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_engine_itf_)->CreateAudioRecorder(sles_engine_itf_,
                                                                      &sles_recorder_,
                                                                      &audio_source,
                                                                      &audio_sink,
                                                                      kNumInterfaces,
                                                                      id,
                                                                      req),
                             false);
    // Realize the recorder in synchronous mode.
    OPENSL_RETURN_ON_FAILURE((*sles_recorder_)->Realize(sles_recorder_,
                                                        SL_BOOLEAN_FALSE),
                             false);
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_recorder_)->GetInterface(sles_recorder_, SL_IID_RECORD,
                                                             static_cast<void*>(&sles_recorder_itf_)),
                             false);
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_recorder_)->GetInterface(
                                                             sles_recorder_,
                                                             SL_IID_ANDROIDSIMPLEBUFFERQUEUE,
                                                             static_cast<void*>(&sles_recorder_sbq_itf_)),
                             false);
    return true;
}

void OpenSlesInput::DestroyAudioRecorder() {
    if (sles_recorder_sbq_itf_) {
        // Release all buffers currently queued up.
        OPENSL_RETURN_ON_FAILURE(
                                 (*sles_recorder_sbq_itf_)->Clear(sles_recorder_sbq_itf_),
                                 VOID_RETURN);
        sles_recorder_sbq_itf_ = NULL;
    }
    sles_recorder_itf_ = NULL;
    if (!sles_recorder_) {
        (*sles_recorder_)->Destroy(sles_recorder_);
        sles_recorder_ = NULL;
    }
}

void OpenSlesInput::RecorderSimpleBufferQueueCallback(
                                                      SLAndroidSimpleBufferQueueItf queue_itf,
                                                      void* context) {
    OpenSlesInput* audio_device = reinterpret_cast<OpenSlesInput*>(context);
    audio_device->RecorderSimpleBufferQueueCallbackHandler(queue_itf);
}

void OpenSlesInput::RecorderSimpleBufferQueueCallbackHandler(
                                                             SLAndroidSimpleBufferQueueItf queue_itf) {

    uint8_t* audio = rec_buf_[active_queue_];
    // There is at least one spot available in the fifo.
    if (mPCMDataOutputer!=NULL) {
        mPCMDataOutputer->onOutputPCMData(audio, recBufSizeInBytes_);
    }else{
        audioFrame.data = audio;
        audioFrame.frameSize = recBufSizeInBytes_;
        audioFrame.duration = float(recBufSizeInBytes_*1000)/float(sampleRate_*numChannels_*2);
        audioFrame.pts = MediaTimer::GetNowMediaTimeMS();
        bool ret = fifo_->push(&audioFrame);
        if (ret) {
            pthread_mutex_lock(&mLock);
            if (isWaittiing) {
                isWaittiing = false;
                pthread_cond_signal(&mCondition);
            }
            pthread_mutex_unlock(&mLock);
        }
    }
    active_queue_ = (active_queue_ + 1) % (kNumOpenSlBuffers+kNumRecBuffers);
    // active_queue_ is indexing the next buffer to record to. Since the current
    // buffer has been recorded it means that the buffer index
    // kNumOpenSlBuffers - 1 past |active_queue_| contains the next free buffer.
    // Since |fifo_| wasn't at capacity, at least one buffer is free to be used.
    int next_free_buffer = (active_queue_ + kNumOpenSlBuffers - 1) % (kNumOpenSlBuffers+kNumRecBuffers);
    
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_recorder_sbq_itf_)->Enqueue(
                                                                sles_recorder_sbq_itf_,
                                                                reinterpret_cast<void*>(rec_buf_[next_free_buffer]),
                                                                recBufSizeInBytes_),
                             VOID_RETURN);
}

SLDataFormat_PCM OpenSlesInput::CreatePcmConfiguration(int sample_rate)
{
    SLDataFormat_PCM configuration;
    configuration.formatType = SL_DATAFORMAT_PCM;
    configuration.numChannels = numChannels_;
    // According to the opensles documentation in the ndk:
    // samplesPerSec is actually in units of milliHz, despite the misleading name.
    // It further recommends using constants. However, this would lead to a lot
    // of boilerplate code so it is not done here.
    configuration.samplesPerSec = sample_rate * 1000;
    configuration.bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_16;
    configuration.containerSize = SL_PCMSAMPLEFORMAT_FIXED_16;
    configuration.channelMask = SL_SPEAKER_FRONT_CENTER;
    if (2 == configuration.numChannels) {
        configuration.channelMask =
        SL_SPEAKER_FRONT_LEFT | SL_SPEAKER_FRONT_RIGHT;
    }
    configuration.endianness = SL_BYTEORDER_LITTLEENDIAN;
    return configuration;
}

AudioFrame* OpenSlesInput::frontAudioFrame()
{
    AudioFrame* audioFrame = NULL;
    while (true) {
        audioFrame = fifo_->front();
        if (audioFrame!=NULL) {
            break;
        }else {
            pthread_mutex_lock(&mLock);
            isWaittiing = true;
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
    }
    return audioFrame;
}

void OpenSlesInput::popAudioFrame()
{
    fifo_->pop();
}
