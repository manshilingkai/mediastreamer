//
//  ExternalAudioInputPlus.h
//  MediaStreamer
//
//  Created by slklovewyy on 2020/4/22.
//  Copyright © 2020 bolome. All rights reserved.
//

#ifndef ExternalAudioInputPlus_h
#define ExternalAudioInputPlus_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include "AudioCapture.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include "resampler.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
#include <libavutil/avstring.h>
}

#include <queue>

using namespace std;

class ExternalAudioInputPlus : public AudioCapture {
public:
    ExternalAudioInputPlus(int sampleRate, int numChannels);
    ~ExternalAudioInputPlus();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm) {};
#endif
    
    void setPCMDataOutputer(IPCMDataOutputer *outputer) {}

    void setRecBufSizeInBytes(int bufSizeInBytes);
    
    // Main initializaton and termination
    int Init(AudioCaptureMode mode = DEFAULT_MODE);
    int Terminate();
    bool Initialized() { return initialized_; }
    
    // Audio transport control
    int StartRecording();
    int StopRecording();
    bool Recording() { return recording_; }
    
    void setVolume(float volume) {}
    
    void inputAudioFrame(AudioFrame *inAudioFrame);
    void inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType);
    
    AudioFrame* frontAudioFrame();
    void popAudioFrame();
    
#ifdef IOS
    void iOSAVAudioSessionInterruption(bool isInterrupting) {}
#endif

private:
    long getLatencyMs();
    
private:
    bool initialized_;
    bool recording_;
    pthread_mutex_t mRecordingLock;
    
    int sampleRate_;
    int numChannels_;
    int recBufSizeInBytes_;
private:
    float* float_pcm_data;
    int float_pcm_data_size;
    int16_t* int16_pcm_data;
    int int16_pcm_data_size;
    
    musly::resampler* mResampler;
    int input_rate;
    int output_rate;
private:
    int mPrimaryAudioSourceType;
    
    int mSecondaryAudioSourceType;
    ExternalAudioInputPlus* mSecondaryExternalAudioInput;
    pthread_mutex_t mSecondaryExternalAudioInputLock;
private:
    pthread_mutex_t mAudioFrameQueueLock;
    queue<AudioFrame*> mAudioFrameQueue;
    int64_t mCurrentPtsMs;
    
    AVAudioFifo* mAVAudioFifo;
    
    AudioFrame mAudioFrame;
};

#endif /* ExternalAudioInputPlus_h */
