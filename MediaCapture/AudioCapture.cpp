//
//  AudioCapture.cpp
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "AudioCapture.h"
#ifdef ANDROID
#include "OpenSlesInput.h"
#include "JniAudioCapturer.h"
#endif

#ifdef IOS
#include "AudioUnitInput.h"
#endif

#include "ExternalAudioInputPlus.h"
#include "DiscreteExternalAudioInput.h"

#if defined(WIN) || defined(MAC)
#include "FFmpegAudioCapture.h"
#endif

#ifdef MAC
#include "audio_device_mac.h"
#endif

#ifdef WIN32
#include "WinWaveAudioCapture.h"
#endif

#include "MediaLog.h"

AudioCapture* AudioCapture::CreateAudioCapture(AUDIO_CAPTURE_TYPE type, char* capturer, int sampleRate, int numChannels)
{
#ifdef ANDROID
    if (type == OPENSLES) {
        LOGD("AudioCapture:[OPENSLES]");
        return new OpenSlesInput(sampleRate, numChannels);
    }
    if (type == AUDIORECORD) {
        LOGD("AudioCapture:[AUDIORECORD]");
        return new JniAudioCapturer(sampleRate, numChannels);
    }
#endif
#ifdef IOS
    if (type == AUDIOUNIT) {
        LOGD("AudioCapture:[AUDIOUNIT]");
        return new AudioUnitInput(sampleRate, numChannels);
    }
#endif
    
#ifdef MAC
    if (type==FFMPEG_AVFOUNDATION_AUDIO) {
        LOGD("AudioCapture:[FFMPEG_AVFOUNDATION_AUDIO]");
        return new FFmpegAudioCapture(type, capturer, sampleRate, numChannels);
    }
    if (type==AUDIO_CAPTURE_MAC) {
        LOGD("AudioCapture:[AUDIO_CAPTURE_MAC]");
        return new AudioDeviceMac(sampleRate, numChannels);
    }
#endif
    
#ifdef WIN
    if (type==FFMPEG_DSHOW_AUDIO) {
        LOGD("AudioCapture:[FFMPEG_DSHOW_AUDIO]");
        return new FFmpegAudioCapture(type, capturer, sampleRate, numChannels);
    }
#endif
    
#ifdef WIN32
	if (type == AUDIO_CAPTURE_WIN_WAVE) {
		LOGD("AudioCapture:[AUDIO_CAPTURE_WIN_WAVE]");
		return new WinWaveAudioCapture(sampleRate, numChannels);
	}
#endif

    if (type == EXTERNAL_INPUT) {
        LOGD("AudioCapture:[EXTERNAL_INPUT]");
        return new ExternalAudioInputPlus(sampleRate, numChannels);
    } else if (type == DISCRETE_EXTERNAL_INPUT) {
		LOGD("AudioCapture:[DISCRETE_EXTERNAL_INPUT]");
		return new DiscreteExternalAudioInput(sampleRate, numChannels);
	}
    
    return NULL;
}

void AudioCapture::DeleteAudioCapture(AudioCapture* audioCapture, AUDIO_CAPTURE_TYPE type)
{
#ifdef ANDROID
    if (type == OPENSLES) {
        OpenSlesInput *openSlesInput = (OpenSlesInput *)audioCapture;
        delete openSlesInput;
        openSlesInput = NULL;
    }
    if (type == AUDIORECORD) {
        JniAudioCapturer *jniAudioCapturer = (JniAudioCapturer *)audioCapture;
        delete jniAudioCapturer;
        jniAudioCapturer = NULL;
    }
#endif
#ifdef IOS
    if (type == AUDIOUNIT) {
        AudioUnitInput *audioUnitInput = (AudioUnitInput *)audioCapture;
        delete audioUnitInput;
        audioUnitInput = NULL;
    }
#endif
    
#ifdef MAC
    if (type == FFMPEG_AVFOUNDATION_AUDIO) {
        FFmpegAudioCapture* ffAudioCapture = (FFmpegAudioCapture*)audioCapture;
        delete ffAudioCapture;
        ffAudioCapture = NULL;
    }
    if (type == AUDIO_CAPTURE_MAC) {
        AudioDeviceMac* audioDeviceMac = (AudioDeviceMac*)audioCapture;
        delete audioDeviceMac;
        audioDeviceMac = NULL;
    }
#endif

#ifdef WIN
    if (type == FFMPEG_DSHOW_AUDIO) {
        FFmpegAudioCapture* ffAudioCapture = (FFmpegAudioCapture*)audioCapture;
        delete ffAudioCapture;
        ffAudioCapture = NULL;
    }
#endif
    
#ifdef WIN32
	if (type == AUDIO_CAPTURE_WIN_WAVE) {
		WinWaveAudioCapture* winWaveAudioCapture = (WinWaveAudioCapture*)audioCapture;
		if (winWaveAudioCapture)
		{
			delete winWaveAudioCapture;
			winWaveAudioCapture = NULL;
		}
	}
#endif

    if (type == EXTERNAL_INPUT) {
        ExternalAudioInputPlus* externalAudioInput = (ExternalAudioInputPlus *)audioCapture;
        delete externalAudioInput;
        externalAudioInput = NULL;
    }
	else if (type == DISCRETE_EXTERNAL_INPUT) {
		DiscreteExternalAudioInput* discreteExternalAudioInput = (DiscreteExternalAudioInput *)audioCapture;
		delete discreteExternalAudioInput;
		discreteExternalAudioInput = NULL;
	}
}
