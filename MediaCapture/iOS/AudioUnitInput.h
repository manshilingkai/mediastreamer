//
//  AudioUnitInput.h
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef AudioUnitInput_h
#define AudioUnitInput_h

#include <stdio.h>

#include <pthread.h>

#include <AudioUnit/AudioUnit.h>
#include "AudioCapture.h"

class AudioUnitInput : public AudioCapture
{
public:
    AudioUnitInput(int sampleRate, int numChannels);
    ~AudioUnitInput();
    
    void setPCMDataOutputer(IPCMDataOutputer *outputer);
    
    void setRecBufSizeInBytes(int bufSizeInBytes);
    
    // Main initializaton and termination
    int Init(AudioCaptureMode mode = DEFAULT_MODE);
    int Terminate();
    bool Initialized() { return initialized_; }
    
    // Audio transport control
    int StartRecording();
    int StopRecording();
    bool Recording() { return recording_; }
    
    void setVolume(float volume) {}
    
    void inputAudioFrame(AudioFrame *inAudioFrame) {};
    void inputAudioFrame(AudioFrame *inAudioFrame, int audioSourceType) {};
    
    AudioFrame* frontAudioFrame();
    void popAudioFrame();
    
    void iOSAVAudioSessionInterruption(bool isInterrupting);
    
private:
    static OSStatus RecordProcess(void *inRefCon,
                                  AudioUnitRenderActionFlags *ioActionFlags,
                                  const AudioTimeStamp *timeStamp,
                                  UInt32 inBusNumber,
                                  UInt32 inNumberFrames,
                                  AudioBufferList *ioData);
    OSStatus RecordProcessImpl(AudioUnitRenderActionFlags *ioActionFlags,
                               const AudioTimeStamp *timeStamp,
                               uint32_t inBusNumber,
                               uint32_t inNumberFrames);
    
    int64_t getLatency();
    
    int CalculateNumFifoBuffersNeeded();
    
    bool initialized_;
    bool recording_;
    pthread_mutex_t mRecordingLock;
    
    AudioUnit _auVoiceProcessing;
    pthread_mutex_t mAUVoiceProcessingLock;
    
    int sampleRate_;
    int numChannels_;
    int recBufSizeInBytes_;
    
    void *_audioInterruptionObserver;
    
    uint8_t *fifo;
    int fifoSize;
    
    void allocBuffers();
    void freeBuffers();
    void flushBuffers();
    
    pthread_mutex_t mLock;
    pthread_cond_t mCondition;
    bool isWaitting;
    
    int write_pos;
    int read_pos;
    int cache_len;
    
    int64_t lastPts;
    
    AudioFrame mAudioFrame;
    
private:
    IPCMDataOutputer *mPCMDataOutputer;
};

#endif /* AudioUnitInput_h */
