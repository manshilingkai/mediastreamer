//
//  AudioUnitInput.cpp
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>

#include "AudioUnitInput.h"
#include "MediaLog.h"
#include "MediaTimer.h"

AudioUnitInput::AudioUnitInput(int sampleRate, int numChannels)
{
    sampleRate_ = sampleRate;
    numChannels_ = numChannels;
    
    recBufSizeInBytes_ = 0;
    
    _auVoiceProcessing = NULL;
    pthread_mutex_init(&mAUVoiceProcessingLock, NULL);
    
    _audioInterruptionObserver = NULL;
    
    initialized_ = false;
    recording_ = false;
    pthread_mutex_init(&mRecordingLock, NULL);
    
    lastPts = 0;
    
    isWaitting = false;
    
    mPCMDataOutputer = NULL;
}

AudioUnitInput::~AudioUnitInput()
{
    Terminate();
    
    pthread_mutex_destroy(&mAUVoiceProcessingLock);
    
    pthread_mutex_destroy(&mRecordingLock);
}

void AudioUnitInput::setPCMDataOutputer(IPCMDataOutputer *outputer)
{
    mPCMDataOutputer = outputer;
}

void AudioUnitInput::allocBuffers()
{
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mCondition, NULL);
    
    fifoSize = recBufSizeInBytes_*CalculateNumFifoBuffersNeeded();
    if (fifoSize<=0) {
        fifo = NULL;
    }else{
        fifo = (uint8_t *)malloc(fifoSize);
    }
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    if (recBufSizeInBytes_<=0) {
        mAudioFrame.data = NULL;
    }else{
        mAudioFrame.data = (uint8_t*)malloc(recBufSizeInBytes_);
    }
    mAudioFrame.frameSize = recBufSizeInBytes_;
    mAudioFrame.pts = 0;
    mAudioFrame.duration = 0;
}

void AudioUnitInput::freeBuffers()
{
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mCondition);
    
    if (fifo!=NULL) {
        free(fifo);
        fifo = NULL;
    }
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    if(mAudioFrame.data!=NULL)
    {
        free(mAudioFrame.data);
        mAudioFrame.data = NULL;
    }
}

void AudioUnitInput::flushBuffers()
{
    pthread_mutex_lock(&mLock);
    cache_len = 0;
    write_pos = 0;
    read_pos = 0;
    pthread_mutex_unlock(&mLock);
}


void AudioUnitInput::setRecBufSizeInBytes(int bufSizeInBytes)
{
    recBufSizeInBytes_ = bufSizeInBytes;
}

int AudioUnitInput::Init(AudioCaptureMode mode)
{
    if (initialized_) {
        LOGW("Already initialized");
        return 0;
    }
    
    OSStatus result = -1;
    // Check if already initialized
    if (NULL != _auVoiceProcessing) {
        // We already have initialized before and created any of the audio unit,
        // check that all exist
        LOGW("Already initialized");
        // todo: Call AudioUnitReset() here and empty all buffers?
        return 0;
    }
    // Create Voice Processing Audio Unit
    AudioComponentDescription desc;
    AudioComponent comp;
    desc.componentType = kAudioUnitType_Output;
    if (mode == VOIP_MODE) {
        desc.componentSubType = kAudioUnitSubType_VoiceProcessingIO;
    }else{
        desc.componentSubType = kAudioUnitSubType_RemoteIO;
    }
    desc.componentManufacturer = kAudioUnitManufacturer_Apple;
    desc.componentFlags = 0;
    desc.componentFlagsMask = 0;
    comp = AudioComponentFindNext(NULL, &desc);
    if (NULL == comp) {
        LOGE("Could not find audio component for Audio Unit");
        return -1;
    }
    
    pthread_mutex_lock(&mAUVoiceProcessingLock);
    result = AudioComponentInstanceNew(comp, &_auVoiceProcessing);
    if (0 != result) {
        LOGE("Could not create Audio Unit instance (result=%d)",
                     result);
        pthread_mutex_unlock(&mAUVoiceProcessingLock);
        return -1;
    }
    
    /*
    // Set preferred hardware sample rate to 16 kHz
    NSError* error = nil;
    AVAudioSession* session = [AVAudioSession sharedInstance];
    Float64 preferredSampleRate(16000.0);
    [session setPreferredSampleRate:preferredSampleRate
                              error:&error];
    if (error != nil) {
        const char* errorString = [[error localizedDescription] UTF8String];
        LOGE("Could not set preferred sample rate: %s", errorString);
    }
    
    error = nil;
    if (session.category != AVAudioSessionCategoryPlayAndRecord) {
//        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
        [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetooth error:&error];
        if (error != nil) {
            const char* errorString = [[error localizedDescription] UTF8String];
            LOGE("Could not set category: %s", errorString);
        }
    }
    
    error = nil;
    // Make the setMode:error: and setCategory:error: calls only if necessary.
    // Non-obviously, setting them to the value they already have will clear
    // transient properties (such as PortOverride) that some other component may
    // have set up.
    if (session.mode != AVAudioSessionModeVoiceChat) {
        [session setMode:AVAudioSessionModeVoiceChat error:&error];
        if (error != nil) {
            const char* errorString = [[error localizedDescription] UTF8String];
            LOGE("Could not set mode: %s", errorString);
        }
    }
    */
    
    //////////////////////
    // Setup Voice Processing Audio Unit
    // Note: For Signal Processing AU element 0 is output bus, element 1 is
    //       input bus for global scope element is irrelevant (always use
    //       element 0)
    // Enable IO on both elements
    // todo: Below we just log and continue upon error. We might want
    //       to close AU and return error for some cases.
    // todo: Log info about setup.
    UInt32 enableIO = 1;
    result = AudioUnitSetProperty(_auVoiceProcessing,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Input,
                                  1,  // input bus
                                  &enableIO,
                                  sizeof(enableIO));
    if (0 != result) {
        LOGE("Could not enable IO on input (result=%d)", result);
    }

    UInt32 playFlag = 0;
    AudioUnitSetProperty(_auVoiceProcessing,
                         kAudioOutputUnitProperty_EnableIO,
                         kAudioUnitScope_Output,
                         0,
                         &playFlag,
                         sizeof(playFlag));
    
    if (mode == VOIP_MODE) {
        //turn on agc
        UInt32 turnOnAGC = 1;
        result = AudioUnitSetProperty(_auVoiceProcessing,
                                      kAUVoiceIOProperty_VoiceProcessingEnableAGC,
                                      kAudioUnitScope_Global,
                                      1,
                                      &turnOnAGC,
                                      sizeof(turnOnAGC));
        if (0 != result) {
            LOGE("Could not turn on agc (result=%d)", result);
        }
    }

    // Disable AU buffer allocation for the recorder, we allocate our own
    UInt32 flag = 0;
    result = AudioUnitSetProperty(
                                  _auVoiceProcessing, kAudioUnitProperty_ShouldAllocateBuffer,
                                  kAudioUnitScope_Output,  1, &flag, sizeof(flag));
    if (0 != result) {
        LOGE("Could not disable AU buffer allocation (result=%d)",
                     result);
        // Should work anyway
    }
    // Set recording callback
    AURenderCallbackStruct auCbS;
    memset(&auCbS, 0, sizeof(auCbS));
    auCbS.inputProc = RecordProcess;
    auCbS.inputProcRefCon = this;
    result = AudioUnitSetProperty(_auVoiceProcessing,
                                  kAudioOutputUnitProperty_SetInputCallback,
                                  kAudioUnitScope_Global, 1,
                                  &auCbS, sizeof(auCbS));
    if (0 != result) {
        LOGE("  Could not set record callback for Audio Unit (result=%d)",
                     result);
    }

    // Get stream format for in/1
    AudioStreamBasicDescription recordingDesc;
    UInt32 size = sizeof(recordingDesc);
    result = AudioUnitGetProperty(_auVoiceProcessing,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input, 1, &recordingDesc,
                                  &size);
    if (0 != result) {
        LOGE("Could not get stream format Audio Unit in/1 (result=%d)",
                     result);
    }
    LOGI("  Audio Unit recording opened in sampling rate %f",
                 recordingDesc.mSampleRate);
    recordingDesc.mSampleRate = (Float64)sampleRate_;
    // Set stream format for out/1 (use same sampling frequency as for in/1)
    if (numChannels_==1) {
        recordingDesc.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger
        | kLinearPCMFormatFlagIsPacked
        | kLinearPCMFormatFlagIsNonInterleaved;
    }else{
        recordingDesc.mFormatID = kAudioFormatLinearPCM;
        recordingDesc.mFormatFlags = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagsNativeEndian | kAudioFormatFlagIsPacked;
    }

    recordingDesc.mFramesPerPacket = 1;
    recordingDesc.mChannelsPerFrame = numChannels_;
    recordingDesc.mBitsPerChannel = 16;
    recordingDesc.mBytesPerFrame = recordingDesc.mBitsPerChannel / 8 * recordingDesc.mChannelsPerFrame;
    recordingDesc.mBytesPerPacket = recordingDesc.mBytesPerFrame * recordingDesc.mFramesPerPacket;
    result = AudioUnitSetProperty(_auVoiceProcessing,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Output, 1, &recordingDesc,
                                  size);
    if (0 != result) {
        LOGE("  Could not set stream format Audio Unit out/1 (result=%d)",
                     result);
    }
    // Initialize here already to be able to get/set stream properties.
    result = AudioUnitInitialize(_auVoiceProcessing);
    if (0 != result) {
        LOGE("  Could not init Audio Unit (result=%d)", result);
        
        if (_auVoiceProcessing) {
            AudioComponentInstanceDispose(_auVoiceProcessing);
            _auVoiceProcessing = NULL;
        }

        pthread_mutex_unlock(&mAUVoiceProcessingLock);
        return -1;
    }
    
    pthread_mutex_unlock(&mAUVoiceProcessingLock);
    
    /*
    // Get hardware sample rate for logging (see if we get what we asked for)
    double sampleRate = session.sampleRate;
    LOGD("  Current HW sample rate is %f",sampleRate);
    
    // Listen to audio interruptions.
    NSNotificationCenter* center = [NSNotificationCenter defaultCenter];
    id observer =
    [center addObserverForName:AVAudioSessionInterruptionNotification
                        object:nil
                         queue:[NSOperationQueue mainQueue]
                    usingBlock:^(NSNotification* notification) {
        NSNumber* typeNumber =
            [notification userInfo][AVAudioSessionInterruptionTypeKey];
            AVAudioSessionInterruptionType type =
            (AVAudioSessionInterruptionType)[typeNumber unsignedIntegerValue];
        switch (type) {
            case AVAudioSessionInterruptionTypeBegan:
            // At this point our audio session has been deactivated and the
            // audio unit render callbacks no longer occur. Nothing to do.
            break;
            case AVAudioSessionInterruptionTypeEnded: {
                NSError* error = nil;
                AVAudioSession* session = [AVAudioSession sharedInstance];
                [session setActive:YES
                             error:&error];
                if (error != nil) {
                LOGW("Error activating audio session");
                }
                // Post interruption the audio unit render callbacks don't
                // automatically continue, so we restart the unit manually here.
                AudioOutputUnitStop(_auVoiceProcessing);
                AudioOutputUnitStart(_auVoiceProcessing);
                break;
            }
        }
     }];
    // Increment refcount on observer using ARC bridge. Instance variable is a
    // void* instead of an id because header is included in other pure C++
    // files.
    _audioInterruptionObserver = (__bridge_retained void*)observer;
    // Activate audio session.
    error = nil;
    [session setActive:YES
                 error:&error];
    if (error != nil) {
        LOGW("Error activating audio session");
    }
    */

    allocBuffers();
    
    initialized_ = true;
    
    return 0;
}

int AudioUnitInput::Terminate()
{
    if (!initialized_) {
        LOGW("Already terminated");
        return 0;
    }
    
    /*
    if (_audioInterruptionObserver != NULL) {
        NSNotificationCenter* center = [NSNotificationCenter defaultCenter];
        // Transfer ownership of observer back to ARC, which will dealloc the
        // observer once it exits this scope.
        id observer = (__bridge_transfer id)_audioInterruptionObserver;
        [center removeObserver:observer];
        _audioInterruptionObserver = NULL;
    }*/
    
    // Close and delete AU
    pthread_mutex_lock(&mAUVoiceProcessingLock);
    OSStatus result = -1;
    if (NULL != _auVoiceProcessing) {
        result = AudioOutputUnitStop(_auVoiceProcessing);
        if (0 != result) {
            LOGE("Error stopping Audio Unit (result=%d)", result);
        }
        
        result = AudioUnitUninitialize(_auVoiceProcessing);
        if (0 != result) {
            LOGE("Error Uninitialize Audio Unit (result=%d)", result);
        }
        
        result = AudioComponentInstanceDispose(_auVoiceProcessing);
        if (0 != result) {
            LOGW("Error disposing Audio Unit (result=%d)", result);
        }
        _auVoiceProcessing = NULL;
    }
    pthread_mutex_unlock(&mAUVoiceProcessingLock);
    
    freeBuffers();
    
    initialized_ = false;
    
    return 0;
}

int AudioUnitInput::StartRecording()
{
    pthread_mutex_lock(&mRecordingLock);
    if (recording_) {
        pthread_mutex_unlock(&mRecordingLock);
        LOGW("Recording already started");
        return 0;
    }
    pthread_mutex_unlock(&mRecordingLock);
    
    LOGD("Starting Audio Unit");
    if (_auVoiceProcessing!=NULL) {
        OSStatus result = AudioOutputUnitStart(_auVoiceProcessing);
        if (0 != result) {
            LOGE("  Error starting Audio Unit (result=%d)", result);
            return -1;
        }
        
        pthread_mutex_lock(&mRecordingLock);
        recording_ = true;
        pthread_mutex_unlock(&mRecordingLock);

        return 0;
    }else return -1;
}

int AudioUnitInput::StopRecording()
{
    pthread_mutex_lock(&mRecordingLock);
    if (!recording_) {
        pthread_mutex_unlock(&mRecordingLock);
        LOGW("Recording already stoped");
        return 0;
    }
    pthread_mutex_unlock(&mRecordingLock);
    
    LOGD("Stopping Audio Unit");
    if (_auVoiceProcessing!=NULL) {
        OSStatus result = AudioOutputUnitStop(_auVoiceProcessing);
        if (0 != result) {
            LOGE("  Error stopping Audio Unit (result=%d)", result);
            return -1;
        }
        
        pthread_mutex_lock(&mRecordingLock);
        recording_ = false;
        pthread_mutex_unlock(&mRecordingLock);

        flushBuffers();
        
        return 0;
    }else return -1;
}

int64_t AudioUnitInput::getLatency()
{
    int64_t _recordingDelayHWAndOS = 0;
    
    // HW input latency
    Float32 f32(0);
    UInt32 size = sizeof(f32);
    OSStatus result = AudioSessionGetProperty(
                                              kAudioSessionProperty_CurrentHardwareInputLatency, &size, &f32);
    if (0 != result) {
        LOGE("error HW latency (result=%d)", result);
    }
    _recordingDelayHWAndOS += static_cast<int>(f32 * 1000000);
    
    // HW buffer duration
    f32 = 0;
    result = AudioSessionGetProperty(
                                     kAudioSessionProperty_CurrentHardwareIOBufferDuration, &size, &f32);
    if (0 != result) {
        LOGE("error HW buffer duration (result=%d)", result);
    }
    _recordingDelayHWAndOS += static_cast<int>(f32 * 1000000);
    
    // AU latency
    Float64 f64(0);
    size = sizeof(f64);
    result = AudioUnitGetProperty(_auVoiceProcessing, kAudioUnitProperty_Latency,
                                  kAudioUnitScope_Global, 0, &f64, &size);
    if (0 != result) {
        LOGE("error AU latency (result=%d)", result);
    }
    _recordingDelayHWAndOS += static_cast<int>(f64 * 1000000);

    return static_cast<int64_t>(_recordingDelayHWAndOS + cache_len*1000000/sampleRate_*numChannels_*2);
}

int AudioUnitInput::CalculateNumFifoBuffersNeeded() {
    return 4;
}

OSStatus
AudioUnitInput::RecordProcess(void *inRefCon,
                              AudioUnitRenderActionFlags *ioActionFlags,
                              const AudioTimeStamp *inTimeStamp,
                              UInt32 inBusNumber,
                              UInt32 inNumberFrames,
                              AudioBufferList *ioData) {
    AudioUnitInput* ptrThis = static_cast<AudioUnitInput*>(inRefCon);
    return ptrThis->RecordProcessImpl(ioActionFlags,
                                      inTimeStamp,
                                      inBusNumber,
                                      inNumberFrames);
}

OSStatus
AudioUnitInput::RecordProcessImpl(AudioUnitRenderActionFlags *ioActionFlags,
                                  const AudioTimeStamp *inTimeStamp,
                                  uint32_t inBusNumber,
                                  uint32_t inNumberFrames) {
    // Setup some basic stuff
    // Use temp buffer not to lock up recording buffer more than necessary
    // todo: Make dataTmp a member variable with static size that holds
    //       max possible frames?
    int16_t* dataTmp = new int16_t[numChannels_*inNumberFrames];
    memset(dataTmp, 0, 2*numChannels_*inNumberFrames);
    AudioBufferList abList;
    abList.mNumberBuffers = 1;
    abList.mBuffers[0].mData = dataTmp;
    abList.mBuffers[0].mDataByteSize = 2*numChannels_*inNumberFrames;  // 2 bytes/sample
    abList.mBuffers[0].mNumberChannels = numChannels_;
    // Get data from mic
    OSStatus res = AudioUnitRender(_auVoiceProcessing,
                                   ioActionFlags, inTimeStamp,
                                   inBusNumber, inNumberFrames, &abList);
    if (res != 0) {
        LOGW("Error getting rec data, error = %d", res);
        delete [] dataTmp;
        return 0;
    }
    
    uint8_t* data = (uint8_t*)dataTmp;
    int size = 2*numChannels_*inNumberFrames;
    
    pthread_mutex_lock(&mRecordingLock);
    if (recording_)
    {
        pthread_mutex_unlock(&mRecordingLock);
        
        if (mPCMDataOutputer!=NULL) {
            mPCMDataOutputer->onOutputPCMData(data, size);
        }else{
            pthread_mutex_lock(&mLock);
            
            if(cache_len + size>fifoSize)
            {
                // is full
            }else
            {
                if(size>fifoSize-write_pos)
                {
                    memcpy(fifo+write_pos,data,fifoSize-write_pos);
                    memcpy(fifo,data+fifoSize-write_pos,size-(fifoSize-write_pos));
                    write_pos = size-(fifoSize-write_pos);
                }else
                {
                    memcpy(fifo+write_pos,data,size);
                    write_pos = write_pos+size;
                }
                
                cache_len = cache_len+size;
            }
            
            if (isWaitting) {
                isWaitting = false;
                pthread_cond_signal(&mCondition);
            }
            
            pthread_mutex_unlock(&mLock);
        }

    }else{
        pthread_mutex_unlock(&mRecordingLock);
    }

    delete [] dataTmp;
    return 0;
}

AudioFrame* AudioUnitInput::frontAudioFrame()
{
    NSError *error = nil;
    BOOL ret = [[AVAudioSession sharedInstance] setActive:YES error:&error];
    if (ret) {
    }else{
        LOGE("AVAudioSession sharedInstance setActive:YES Fail");
    }
    
    /*
    AudioFrame* audioFrame = NULL;
    
    while (true) {
        pthread_mutex_lock(&mLock);
        
        if (cache_len >= recBufSizeInBytes_) {
            
            if(recBufSizeInBytes_>fifoSize-read_pos)
            {
                memcpy(mAudioFrame.data,fifo+read_pos,fifoSize-read_pos);
                memcpy(mAudioFrame.data+fifoSize-read_pos,fifo,recBufSizeInBytes_-(fifoSize-read_pos));
                read_pos = recBufSizeInBytes_-(fifoSize-read_pos);
            }else
            {
                memcpy(mAudioFrame.data,fifo+read_pos,recBufSizeInBytes_);
                read_pos = read_pos + recBufSizeInBytes_;
            }
            
            mAudioFrame.frameSize = recBufSizeInBytes_;
            mAudioFrame.duration = float(recBufSizeInBytes_*1000)/float(sampleRate_*1*2);

            mAudioFrame.pts = MediaTimer::GetNowMediaTimeMS();
            cache_len = cache_len - recBufSizeInBytes_;
            
            audioFrame = &mAudioFrame;
            
            pthread_mutex_unlock(&mLock);
            
            break;
            
        }else{
            isWaitting = true;
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            
            continue;
        }
    }
    
    return audioFrame;
     */
    
    AudioFrame* audioFrame = NULL;
    
    pthread_mutex_lock(&mLock);
    
    if (cache_len >= recBufSizeInBytes_) {
        
        if(recBufSizeInBytes_>fifoSize-read_pos)
        {
            memcpy(mAudioFrame.data,fifo+read_pos,fifoSize-read_pos);
            memcpy(mAudioFrame.data+fifoSize-read_pos,fifo,recBufSizeInBytes_-(fifoSize-read_pos));
            read_pos = recBufSizeInBytes_-(fifoSize-read_pos);
        }else
        {
            memcpy(mAudioFrame.data,fifo+read_pos,recBufSizeInBytes_);
            read_pos = read_pos + recBufSizeInBytes_;
        }
        
        mAudioFrame.frameSize = recBufSizeInBytes_;
        mAudioFrame.duration = float(recBufSizeInBytes_*1000)/float(sampleRate_*numChannels_*2);
        
        mAudioFrame.pts = MediaTimer::GetNowMediaTimeMS();
        cache_len = cache_len - recBufSizeInBytes_;
        
        audioFrame = &mAudioFrame;
    }
    pthread_mutex_unlock(&mLock);
    
    return audioFrame;
}

void AudioUnitInput::popAudioFrame()
{
    //no need do anything.
}

void AudioUnitInput::iOSAVAudioSessionInterruption(bool isInterrupting)
{
    flushBuffers();
    
    pthread_mutex_lock(&mAUVoiceProcessingLock);
    if (isInterrupting) {
        if (_auVoiceProcessing) {
            OSStatus result = AudioOutputUnitStop(_auVoiceProcessing);
            if (0 != result) {
                LOGE("Error stoping Audio Unit (result=%d)", result);
            }
        }
    }else{
        if (_auVoiceProcessing) {
            OSStatus result = AudioOutputUnitStart(_auVoiceProcessing);
            if (0 != result) {
                LOGE("Error starting Audio Unit (result=%d)", result);
            }
        }
    }
    pthread_mutex_unlock(&mAUVoiceProcessingLock);
}
