// MediaStreamerTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "IWinMediaStreamer.h"

void MediaStreamerTestListener(void* owner, int event, int ext1, int ext2)
{
	if (event == WIN_MEDIA_STREAMER_CONNECTING)
	{
		printf("WIN_MEDIA_STREAMER_CONNECTING \n");
	}
	else if (event == WIN_MEDIA_STREAMER_CONNECTED)
	{
		printf("WIN_MEDIA_STREAMER_CONNECTED \n");
	}
	else if (event == WIN_MEDIA_STREAMER_STREAMING)
	{
		printf("WIN_MEDIA_STREAMER_STREAMING \n");
	}
	else if (event == WIN_MEDIA_STREAMER_ERROR)
	{
		printf("WIN_MEDIA_STREAMER_ERROR ErrorType:%d \n", ext1);
	}
	else if (event == WIN_MEDIA_STREAMER_INFO)
	{
		if (ext1 == WIN_MEDIA_STREAMER_INFO_PUBLISH_REAL_BITRATE) {
			printf("Real Bitrate:%d \n",ext2);
		}

		if (ext1 == WIN_MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS) {
			printf("Real Fps:%d \n",ext2);
		}

		if (ext1 == WIN_MEDIA_STREAMER_INFO_PUBLISH_DELAY_TIME) {
			printf("buffer cache duration : %d \n",ext2);
		}

		if (ext1 == WIN_MEDIA_STREAMER_INFO_PUBLISH_DOWN_BITRATE) {
			printf("down target bitrate to : %d \n",ext2);
		}

		if (ext1 == WIN_MEDIA_STREAMER_INFO_PUBLISH_UP_BITRATE) {
			printf("up target bitrate to : %d \n",ext2);
		}

		if (ext1 == WIN_MEDIA_STREAMER_INFO_PUBLISH_TIME) {
			printf("Record Time:%f S \n",(float)(ext2)/10.0f);
		}
	}
	else if (event == WIN_MEDIA_STREAMER_END)
	{
		printf("WIN_MEDIA_STREAMER_END \n");
	}
	else if (event == WIN_MEDIA_STREAMER_PAUSED)
	{
		printf("WIN_MEDIA_STREAMER_PAUSED \n");
	}
}

int main()
{
	IWinVideoCapturer* winVideoCapturer = CreateWinVideoCapturerInstance();
	winVideoCapturer->initialize(WIN_GDI_DESKTOP_CAPTURE, 1280, 720, 18);

	IWinMediaStreamer* winMediaStreamer = CreateWinMediaStreamerInstance();
	const char* publishUrl = "C://tmp//test.mp4";
	WinVideoOptions videoOptions;
	videoOptions.videoWidth = 640;
	videoOptions.videoHeight = 480;
	videoOptions.videoFps = 18;
	videoOptions.videoBitRate = 1024;
	videoOptions.networkAdaptiveMinFps = 10;
	videoOptions.networkAdaptiveMinVideoBitrate = 400;
	WinAudioOptions audioOptions;
	winMediaStreamer->initialize(publishUrl, videoOptions, audioOptions);
	winMediaStreamer->setListener(MediaStreamerTestListener, winMediaStreamer);

	winVideoCapturer->StartRecording();
	winVideoCapturer->linkMediaStreamer(winMediaStreamer);
	winMediaStreamer->start();

	getchar();

	winMediaStreamer->stop();
	winVideoCapturer->unlinkMediaStreamer();
	winVideoCapturer->StopRecording();
	winVideoCapturer->terminate();
	DestroyWinVideoCapturerInstance(&winVideoCapturer);
	winMediaStreamer->terminate();
	DestroyWinMediaStreamerInstance(&winMediaStreamer);

	getchar();

    return 0;
}

