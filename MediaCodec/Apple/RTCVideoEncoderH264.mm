/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 *
 */

#import "RTCVideoEncoderH264.h"

#import <VideoToolbox/VideoToolbox.h>
#include <vector>

#if defined(WEBRTC_IOS)
#import "helpers/UIDevice+RTCDevice.h"
#endif
#import "RTCCodecSpecificInfoH264.h"
#import "RTCH264ProfileLevelId.h"
#import "api/peerconnection/RTCRtpFragmentationHeader+Private.h"
#import "api/peerconnection/RTCVideoCodecInfo+Private.h"
#import "base/RTCCodecSpecificInfo.h"
#import "base/RTCI420Buffer.h"
#import "base/RTCVideoEncoder.h"
#import "base/RTCVideoFrame.h"
#import "base/RTCVideoFrameBuffer.h"
#import "components/video_frame_buffer/RTCCVPixelBuffer.h"
#import "helpers.h"

#include "common_video/h264/h264_bitstream_parser.h"
#include "common_video/h264/profile_level_id.h"
#include "common_video/include/bitrate_adjuster.h"
#include "modules/include/module_common_types.h"
#include "modules/video_coding/include/video_error_codes.h"
#include "rtc_base/buffer.h"
#include "rtc_base/logging.h"
#include "rtc_base/timeutils.h"
#include "sdk/objc/components/video_codec/nalu_rewriter.h"
#include "third_party/libyuv/include/libyuv/convert_from.h"

#if defined(WEBRTC_MAC) && !defined(WEBRTC_IOS)
#include "sdk/objc/components/video_codec/objc_h265.h"
#endif

// This is a min version of macOS where we want to support SVC encoding via
// EnableLowLatencyRateControl flag. The flag is actually supported since 11.3,
// but there we see frame drops even with ample bitrate budget. Excessive frame
// drops were fixed in 12.0.1.
#define LOW_LATENCY_FLAG_AVAILABLE_VER 12.0.1

@interface NweRTCVideoEncoderH264 ()

- (void)frameWasEncoded:(OSStatus)status
                  flags:(VTEncodeInfoFlags)infoFlags
           sampleBuffer:(CMSampleBufferRef)sampleBuffer
      codecSpecificInfo:(id<NweRTCCodecSpecificInfo>)codecSpecificInfo
                  width:(int32_t)width
                 height:(int32_t)height
           renderTimeMs:(int64_t)renderTimeMs
              timestamp:(uint32_t)timestamp
               rotation:(RTCVideoRotation)rotation;

@end

namespace {  // anonymous namespace

// The ratio between kVTCompressionPropertyKey_DataRateLimits and
// kVTCompressionPropertyKey_AverageBitRate. The data rate limit is set higher
// than the average bit rate to avoid undershooting the target.
const float kLimitToAverageBitRateFactor = 1.5f;
// These thresholds deviate from the default h264 QP thresholds, as they
// have been found to work better on devices that support VideoToolbox
const int kLowH264QpThreshold = 34;
const int kHighH264QpThreshold = 39;

const int kLowH264QpThresholdForHWSmartBitrateHD = 31;
const int kHighH264QpThresholdForHWSmartBitrateHD = 39;

const OSType kNV12PixelFormat = kCVPixelFormatType_420YpCbCr8BiPlanarFullRange;

const int AUTO_BR_ADJUSTER = -1;
const int BASE_BR_ADJUSTER = 0;
const int DYNAMIC_BR_ADJUSTER = 1;

// Struct that we pass to the encoder per frame to encode. We receive it again
// in the encoder callback.
struct RTCFrameEncodeParams {
  RTCFrameEncodeParams(NweRTCVideoEncoderH264 *e,
                       NweRTCCodecSpecificInfoH264 *csi,
                       int32_t w,
                       int32_t h,
                       int64_t rtms,
                       uint32_t ts,
                       RTCVideoRotation r)
      : encoder(e), width(w), height(h), render_time_ms(rtms), timestamp(ts), rotation(r) {
    if (csi) {
      codecSpecificInfo = csi;
    } else {
      codecSpecificInfo = [[NweRTCCodecSpecificInfoH264 alloc] init];
    }
  }

  NweRTCVideoEncoderH264 *encoder;
  NweRTCCodecSpecificInfoH264 *codecSpecificInfo;
  int32_t width;
  int32_t height;
  int64_t render_time_ms;
  uint32_t timestamp;
  RTCVideoRotation rotation;
};

// We receive I420Frames as input, but we need to feed CVPixelBuffers into the
// encoder. This performs the copy and format conversion.
// TODO(tkchin): See if encoder will accept i420 frames and compare performance.
bool CopyVideoFrameToNV12PixelBuffer(id<NweRTCI420Buffer> frameBuffer, CVPixelBufferRef pixelBuffer) {
  RTC_DCHECK(pixelBuffer);
  RTC_DCHECK_EQ(CVPixelBufferGetPixelFormatType(pixelBuffer), kNV12PixelFormat);
  RTC_DCHECK_EQ(CVPixelBufferGetHeightOfPlane(pixelBuffer, 0), frameBuffer.height);
  RTC_DCHECK_EQ(CVPixelBufferGetWidthOfPlane(pixelBuffer, 0), frameBuffer.width);

  CVReturn cvRet = CVPixelBufferLockBaseAddress(pixelBuffer, 0);
  if (cvRet != kCVReturnSuccess) {
    RTC_LOG(LS_ERROR) << "Failed to lock base address: " << cvRet;
    return false;
  }
  uint8_t *dstY = reinterpret_cast<uint8_t *>(CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0));
  int dstStrideY = CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 0);
  uint8_t *dstUV = reinterpret_cast<uint8_t *>(CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 1));
  int dstStrideUV = CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 1);
  // Convert I420 to NV12.
  int ret = libyuv::I420ToNV12(frameBuffer.dataY,
                               frameBuffer.strideY,
                               frameBuffer.dataU,
                               frameBuffer.strideU,
                               frameBuffer.dataV,
                               frameBuffer.strideV,
                               dstY,
                               dstStrideY,
                               dstUV,
                               dstStrideUV,
                               frameBuffer.width,
                               frameBuffer.height);
  CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
  if (ret) {
    RTC_LOG(LS_ERROR) << "Error converting I420 VideoFrame to NV12 :" << ret;
    return false;
  }
  return true;
}

CVPixelBufferRef CreatePixelBuffer(CVPixelBufferPoolRef pixel_buffer_pool) {
  if (!pixel_buffer_pool) {
    RTC_LOG(LS_ERROR) << "Failed to get pixel buffer pool.";
    return nullptr;
  }
  CVPixelBufferRef pixel_buffer;
  CVReturn ret = CVPixelBufferPoolCreatePixelBuffer(nullptr, pixel_buffer_pool, &pixel_buffer);
  if (ret != kCVReturnSuccess) {
    RTC_LOG(LS_ERROR) << "Failed to create pixel buffer: " << ret;
    // We probably want to drop frames here, since failure probably means
    // that the pool is empty.
    return nullptr;
  }
  return pixel_buffer;
}

// This is the callback function that VideoToolbox calls when encode is
// complete. From inspection this happens on its own queue.
void compressionOutputCallback(void *encoder,
                               void *params,
                               OSStatus status,
                               VTEncodeInfoFlags infoFlags,
                               CMSampleBufferRef sampleBuffer) {
  if (!params) {
    // If there are pending callbacks when the encoder is destroyed, this can happen.
    return;
  }
    
  std::unique_ptr<RTCFrameEncodeParams> encodeParams(
      reinterpret_cast<RTCFrameEncodeParams *>(params));
  [encodeParams->encoder frameWasEncoded:status
                                   flags:infoFlags
                            sampleBuffer:sampleBuffer
                       codecSpecificInfo:encodeParams->codecSpecificInfo
                                   width:encodeParams->width
                                  height:encodeParams->height
                            renderTimeMs:encodeParams->render_time_ms
                               timestamp:encodeParams->timestamp
                                rotation:encodeParams->rotation];
}

// Extract VideoToolbox profile out of the webrtc::SdpVideoFormat. If there is
// no specific VideoToolbox profile for the specified level, AutoLevel will be
// returned. The user must initialize the encoder with a resolution and
// framerate conforming to the selected H264 level regardless.
CFStringRef ExtractProfile(webrtc::SdpVideoFormat videoFormat) {
  const absl::optional<webrtc::H264::ProfileLevelId> profile_level_id =
      webrtc::H264::ParseSdpProfileLevelId(videoFormat.parameters);
  RTC_DCHECK(profile_level_id);
  switch (profile_level_id->profile) {
    case webrtc::H264::kProfileConstrainedBaseline:
    case webrtc::H264::kProfileBaseline:
      switch (profile_level_id->level) {
        case webrtc::H264::kLevel3:
          return kVTProfileLevel_H264_Baseline_3_0;
        case webrtc::H264::kLevel3_1:
          return kVTProfileLevel_H264_Baseline_3_1;
        case webrtc::H264::kLevel3_2:
          return kVTProfileLevel_H264_Baseline_3_2;
        case webrtc::H264::kLevel4:
          return kVTProfileLevel_H264_Baseline_4_0;
        case webrtc::H264::kLevel4_1:
          return kVTProfileLevel_H264_Baseline_4_1;
        case webrtc::H264::kLevel4_2:
          return kVTProfileLevel_H264_Baseline_4_2;
        case webrtc::H264::kLevel5:
          return kVTProfileLevel_H264_Baseline_5_0;
        case webrtc::H264::kLevel5_1:
          return kVTProfileLevel_H264_Baseline_5_1;
        case webrtc::H264::kLevel5_2:
          return kVTProfileLevel_H264_Baseline_5_2;
        case webrtc::H264::kLevel1:
        case webrtc::H264::kLevel1_b:
        case webrtc::H264::kLevel1_1:
        case webrtc::H264::kLevel1_2:
        case webrtc::H264::kLevel1_3:
        case webrtc::H264::kLevel2:
        case webrtc::H264::kLevel2_1:
        case webrtc::H264::kLevel2_2:
          return kVTProfileLevel_H264_Baseline_AutoLevel;
      }

    case webrtc::H264::kProfileMain:
      switch (profile_level_id->level) {
        case webrtc::H264::kLevel3:
          return kVTProfileLevel_H264_Main_3_0;
        case webrtc::H264::kLevel3_1:
          return kVTProfileLevel_H264_Main_3_1;
        case webrtc::H264::kLevel3_2:
          return kVTProfileLevel_H264_Main_3_2;
        case webrtc::H264::kLevel4:
          return kVTProfileLevel_H264_Main_4_0;
        case webrtc::H264::kLevel4_1:
          return kVTProfileLevel_H264_Main_4_1;
        case webrtc::H264::kLevel4_2:
          return kVTProfileLevel_H264_Main_4_2;
        case webrtc::H264::kLevel5:
          return kVTProfileLevel_H264_Main_5_0;
        case webrtc::H264::kLevel5_1:
          return kVTProfileLevel_H264_Main_5_1;
        case webrtc::H264::kLevel5_2:
          return kVTProfileLevel_H264_Main_5_2;
        case webrtc::H264::kLevel1:
        case webrtc::H264::kLevel1_b:
        case webrtc::H264::kLevel1_1:
        case webrtc::H264::kLevel1_2:
        case webrtc::H264::kLevel1_3:
        case webrtc::H264::kLevel2:
        case webrtc::H264::kLevel2_1:
        case webrtc::H264::kLevel2_2:
          return kVTProfileLevel_H264_Main_AutoLevel;
      }

    case webrtc::H264::kProfileConstrainedHigh:
    case webrtc::H264::kProfileHigh:
      switch (profile_level_id->level) {
        case webrtc::H264::kLevel3:
          return kVTProfileLevel_H264_High_3_0;
        case webrtc::H264::kLevel3_1:
          return kVTProfileLevel_H264_High_3_1;
        case webrtc::H264::kLevel3_2:
          return kVTProfileLevel_H264_High_3_2;
        case webrtc::H264::kLevel4:
          return kVTProfileLevel_H264_High_4_0;
        case webrtc::H264::kLevel4_1:
          return kVTProfileLevel_H264_High_4_1;
        case webrtc::H264::kLevel4_2:
          return kVTProfileLevel_H264_High_4_2;
        case webrtc::H264::kLevel5:
          return kVTProfileLevel_H264_High_5_0;
        case webrtc::H264::kLevel5_1:
          return kVTProfileLevel_H264_High_5_1;
        case webrtc::H264::kLevel5_2:
          return kVTProfileLevel_H264_High_5_2;
        case webrtc::H264::kLevel1:
        case webrtc::H264::kLevel1_b:
        case webrtc::H264::kLevel1_1:
        case webrtc::H264::kLevel1_2:
        case webrtc::H264::kLevel1_3:
        case webrtc::H264::kLevel2:
        case webrtc::H264::kLevel2_1:
        case webrtc::H264::kLevel2_2:
          return kVTProfileLevel_H264_High_AutoLevel;
      }
  }
}
}  // namespace

@implementation NweRTCVideoEncoderH264 {
  NweRTCVideoCodecInfo *_codecInfo;
  std::unique_ptr<webrtc::BitrateAdjuster> _bitrateAdjuster;
  uint32_t _targetBitrateBps;
  uint32_t _encoderFrameRate;
  uint32_t _encoderBitrateBps;
  RTCH264PacketizationMode _packetizationMode;
  CFStringRef _profile;
  RTCVideoEncoderCallback _callback;
  int32_t _width;
  int32_t _height;
  VTCompressionSessionRef _compressionSession;
  CVPixelBufferPoolRef _pixelBufferPool;
  RTCVideoCodecMode _mode;

  webrtc::H264BitstreamParser _h264BitstreamParser;
  std::vector<uint8_t> _frameScaleBuffer;

  BOOL _enableLowLatencyRateControl;
  BOOL _supportSvc;
  BOOL _realEnableSvc;
  float _svcBaselayerFramerateFraction;
  float _svcBaselayerBitrateFraction;
  float _svcMinBaselayerBitrateFraction;
  bool _debug;
  float _encodeBaseLayerBitRateFraction;
  absl::optional<uint16_t> svc_last_base_frame_idx_;
}

// .5 is set as a mininum to prevent overcompensating for large temporary
// overshoots. We don't want to degrade video quality too badly.
// .95 is set to prevent oscillations. When a lower bitrate is set on the
// encoder than previously set, its output seems to have a brief period of
// drastically reduced bitrate, so we want to avoid that. In steady state
// conditions, 0.95 seems to give us better overall bitrate over long periods
// of time.
- (instancetype)initWithCodecInfo:(NweRTCVideoCodecInfo *)codecInfo {
  if (self = [super init]) {
    _debug = false;
    _codecInfo = codecInfo;
    _bitrateAdjuster.reset(new webrtc::BitrateAdjuster(0, 0, 0, .5, 1.0, true, kLowH264QpThresholdForHWSmartBitrateHD, kHighH264QpThresholdForHWSmartBitrateHD, webrtc::kH264, false));
    _packetizationMode = RTCH264PacketizationModeNonInterleaved;
    _profile = ExtractProfile([codecInfo nativeSdpVideoFormat]);
    RTC_LOG(LS_INFO) << "Using profile " << CFStringToString(_profile);
    RTC_CHECK([codecInfo.name isEqualToString:kNweRTCVideoCodecH264Name]);
  }
  return self;
}

- (void)dealloc {
  [self destroyCompressionSession];
  _callback = nullptr;
}

- (NSInteger)startEncodeWithSettings:(NweRTCVideoEncoderSettings *)settings
                       numberOfCores:(int)numberOfCores {
  RTC_DCHECK(settings);
  RTC_DCHECK([settings.name isEqualToString:kNweRTCVideoCodecH264Name]);

  _width = settings.width;
  _height = settings.height;
  _mode = settings.mode;
  //open svc， must open lowLatency
  _enableLowLatencyRateControl = settings.supportSvc ? settings.supportSvc : settings.supportLowLatency;
  _supportSvc = settings.supportSvc;
  _supportSvc = NO;
  _svcBaselayerFramerateFraction = settings.svcBaselayerFramerateFraction;
  _svcBaselayerBitrateFraction = settings.svcBaselayerBitrateFraction;
  _svcMinBaselayerBitrateFraction = settings.svcMinBaselayerBitrateFraction;
    
#if defined(WEBRTC_IOS)
  BOOL blackList = [[UIDevice machineName] isEqualToString:@"iPhone14,6"];//iphone se 3
  _enableLowLatencyRateControl = blackList ? false : _enableLowLatencyRateControl;
  _supportSvc = blackList ? false : settings.supportSvc;
#endif

  bool enableBitrateAdjuster = false;
  if (settings.bitrateAdjusterType == AUTO_BR_ADJUSTER) {
    enableBitrateAdjuster = (_mode == RTCVideoCodecModeScreensharing) ? false : true;
  }else if (settings.bitrateAdjusterType == BASE_BR_ADJUSTER) {
    enableBitrateAdjuster = false;
  }else if (settings.bitrateAdjusterType == DYNAMIC_BR_ADJUSTER){
    enableBitrateAdjuster = true;
  }else {
    enableBitrateAdjuster = (_mode == RTCVideoCodecModeScreensharing) ? false : true;
  }

  RTC_LOG(LS_INFO) << "settings -> BitrateAdjusterType : " << settings.bitrateAdjusterType << ", MinAdjustedBitratePct : " << settings.minAdjustedBitratePct << ", MaxAdjustedBitratePct : " << settings.maxAdjustedBitratePct
  << ", ScreenSharing : " << ((_mode == RTCVideoCodecModeScreensharing) ? "true" : "false") << ", EnableBitrateAdjuster : " << ((enableBitrateAdjuster) ? "true" : "false") << ", enableLowLatencyRateControl:" << _enableLowLatencyRateControl << ", supportSvc:" << _supportSvc 
  << ", svcBaselayerFramerateFraction:" << _svcBaselayerFramerateFraction << ", svcBaselayerBitrateFraction:" << _svcBaselayerBitrateFraction << ", svcMinBaselayerBitrateFraction:" << _svcMinBaselayerBitrateFraction;
  RTC_LOG(LS_INFO) << "settings -> SaveRatePerQp : " << settings.saveRatePerQp;
  RTC_LOG(LS_INFO) << "settings -> maxSaveRate : " << settings.maxSaveRate;
  RTC_LOG(LS_INFO) << "settings -> allowOverShotWhenNonWeakNetwork : " << settings.allowOverShotWhenNonWeakNetwork;
  RTC_LOG(LS_INFO) << "settings -> maxTargetBitrateBps : " << settings.maxTargetBitrateBps;


  float minAdjustedBitratePct = 0.5f;
  float maxAdjustedBitratePct = 1.0f;
  bool enable_dynamic_adjust_max_bitrate_pct = true;
  float fraction = 0.5f;
#if defined(WEBRTC_IOS)
  NSString* systemVersionStr = [[UIDevice currentDevice] systemVersion];
  if(systemVersionStr!=nil && [systemVersionStr containsString: @"16.4"]) {
    maxAdjustedBitratePct = 5.0f;
    enable_dynamic_adjust_max_bitrate_pct = false;
    fraction = 0.8f;
    RTC_LOG(LS_INFO) << "Current System is : 16.4.x, so set maxAdjustedBitratePct : " << maxAdjustedBitratePct;
  }
#endif

  if (settings.minAdjustedBitratePct > 0) {
    minAdjustedBitratePct = settings.minAdjustedBitratePct;
  }
  if (settings.maxAdjustedBitratePct > 0) {
    maxAdjustedBitratePct = settings.maxAdjustedBitratePct;
    enable_dynamic_adjust_max_bitrate_pct = false;
  }

  bool isScreenShare = _mode == RTCVideoCodecModeScreensharing?true:false;
  float saveRatePerQp = 0;
  float maxSaveRate = 0;
  if(settings.saveRatePerQp > 0 && settings.saveRatePerQp < 1.0f) {
    saveRatePerQp = settings.saveRatePerQp;
  }
  if(settings.maxSaveRate > 0 && settings.maxSaveRate < 1.0f) {
    maxSaveRate = settings.maxSaveRate;
  }

  _bitrateAdjuster.reset(new webrtc::BitrateAdjuster(settings.maxTargetBitrateBps, _width, _height, minAdjustedBitratePct, maxAdjustedBitratePct, enable_dynamic_adjust_max_bitrate_pct, kLowH264QpThresholdForHWSmartBitrateHD, kHighH264QpThresholdForHWSmartBitrateHD, webrtc::kH264, enableBitrateAdjuster, isScreenShare, saveRatePerQp, maxSaveRate, fraction, settings.allowOverShotWhenNonWeakNetwork));

  // We can only set average bitrate on the HW encoder.
  _targetBitrateBps = settings.startBitrate * 1000;  // startBitrate is in kbps.
  _bitrateAdjuster->SetOriginalTargetBitrateBps(_targetBitrateBps);
  _encoderFrameRate = settings.maxFramerate;

  // TODO(tkchin): Try setting payload size via
  // kVTCompressionPropertyKey_MaxH264SliceBytes.

  return [self resetCompressionSessionWithPixelFormat:kNV12PixelFormat];
}

- (NSInteger)encode:(NweRTCVideoFrame *)frame
    codecSpecificInfo:(nullable id<NweRTCCodecSpecificInfo>)codecSpecificInfo
           frameTypes:(NSArray<NSNumber *> *)frameTypes {
  RTC_DCHECK_EQ(frame.width, _width);
  RTC_DCHECK_EQ(frame.height, _height);
  if (!_callback || !_compressionSession) {
    return WEBRTC_VIDEO_CODEC_UNINITIALIZED;
  }
  BOOL isKeyframeRequired = NO;
   
    
#if defined(WEBRTC_MAC)
    //https://groups.google.com/forum/#!searchin/discuss-webrtc/CVPixelBufferPoolGetPixelBufferAttributes%7Csort:date/discuss-webrtc/AVeyMXnM0gY/AoNV5kQYAgAJ
    _pixelBufferPool = VTCompressionSessionGetPixelBufferPool(_compressionSession);
#endif
    
  // Get a pixel buffer from the pool and copy frame data over.
  if ([self resetCompressionSessionIfNeededWithFrame:frame]) {
    isKeyframeRequired = YES;
  }

  CVPixelBufferRef pixelBuffer = nullptr;
  if ([frame.buffer isKindOfClass:[NweRTCCVPixelBuffer class]]) {
    // Native frame buffer
    NweRTCCVPixelBuffer *rtcPixelBuffer = (NweRTCCVPixelBuffer *)frame.buffer;
    if (![rtcPixelBuffer requiresCropping]) {
      // This pixel buffer might have a higher resolution than what the
      // compression session is configured to. The compression session can
      // handle that and will output encoded frames in the configured
      // resolution regardless of the input pixel buffer resolution.
      pixelBuffer = rtcPixelBuffer.pixelBuffer;
      CVBufferRetain(pixelBuffer);
    } else {
      // Cropping required, we need to crop and scale to a new pixel buffer.
      pixelBuffer = CreatePixelBuffer(_pixelBufferPool);
      if (!pixelBuffer) {
        return WEBRTC_VIDEO_CODEC_ERROR;
      }
      int dstWidth = CVPixelBufferGetWidth(pixelBuffer);
      int dstHeight = CVPixelBufferGetHeight(pixelBuffer);
      if ([rtcPixelBuffer requiresScalingToWidth:dstWidth height:dstHeight]) {
        int size =
            [rtcPixelBuffer bufferSizeForCroppingAndScalingToWidth:dstWidth height:dstHeight];
        _frameScaleBuffer.resize(size);
      } else {
        _frameScaleBuffer.clear();
      }
      _frameScaleBuffer.shrink_to_fit();
      if (![rtcPixelBuffer cropAndScaleTo:pixelBuffer withTempBuffer:_frameScaleBuffer.data()]) {
        CVBufferRelease(pixelBuffer);
        return WEBRTC_VIDEO_CODEC_ERROR;
      }
    }
  }

  if (!pixelBuffer) {
    // We did not have a native frame buffer
    pixelBuffer = CreatePixelBuffer(_pixelBufferPool);
    if (!pixelBuffer) {
      return WEBRTC_VIDEO_CODEC_ERROR;
    }
    RTC_DCHECK(pixelBuffer);
    if (!CopyVideoFrameToNV12PixelBuffer([frame.buffer toI420], pixelBuffer)) {
      RTC_LOG(LS_ERROR) << "Failed to copy frame data.";
      CVBufferRelease(pixelBuffer);
      return WEBRTC_VIDEO_CODEC_ERROR;
    }
  }

  // Check if we need a keyframe.
  if (!isKeyframeRequired && frameTypes) {
    for (NSNumber *frameType in frameTypes) {
      if ((RTCFrameType)frameType.intValue == RTCFrameTypeVideoFrameKey) {
        isKeyframeRequired = YES;
        break;
      }
    }
  }

  CMTime presentationTimeStamp = CMTimeMake(frame.timeStampNs / rtc::kNumNanosecsPerMillisec, 1000);
  CFDictionaryRef frameProperties = nullptr;
  if (isKeyframeRequired) {
    if(_debug){
        RTC_LOG(LS_INFO) << "Request keyframe";
    }
    CFTypeRef keys[] = {kVTEncodeFrameOptionKey_ForceKeyFrame};
    CFTypeRef values[] = {kCFBooleanTrue};
    frameProperties = CreateCFTypeDictionary(keys, values, 1);
  }

  std::unique_ptr<RTCFrameEncodeParams> encodeParams;
  encodeParams.reset(new RTCFrameEncodeParams(self,
                                              codecSpecificInfo,
                                              _width,
                                              _height,
                                              frame.timeStampNs / rtc::kNumNanosecsPerMillisec,
                                              frame.timeStamp,
                                              frame.rotation));
  encodeParams->codecSpecificInfo.packetizationMode = _packetizationMode;

  // Update the bitrate if needed.
  [self setBitrateBps:_bitrateAdjuster->GetAdjustedBitrateBps() frameRate:_encoderFrameRate];

  OSStatus status = VTCompressionSessionEncodeFrame(_compressionSession,
                                                    pixelBuffer,
                                                    presentationTimeStamp,
                                                    kCMTimeInvalid,
                                                    frameProperties,
                                                    encodeParams.release(),
                                                    nullptr);
  if (frameProperties) {
    CFRelease(frameProperties);
  }
  if (pixelBuffer) {
    CVBufferRelease(pixelBuffer);
  }

  if (status == kVTInvalidSessionErr) {
    // This error occurs when entering foreground after backgrounding the app.
    RTC_LOG(LS_ERROR) << "Invalid compression session, resetting.";
    [self resetCompressionSessionWithPixelFormat:[self pixelFormatOfFrame:frame]];

    return WEBRTC_VIDEO_CODEC_NO_OUTPUT;
  } else if (status != noErr) {
    RTC_LOG(LS_ERROR) << "Failed to encode frame with code: " << status;
    return WEBRTC_VIDEO_CODEC_ERROR;
  }
  return WEBRTC_VIDEO_CODEC_OK;
}

- (void)setCallback:(nullable RTCVideoEncoderCallback)callback {
  _callback = callback;
}

- (int)setBitrate:(uint32_t)bitrateKbit framerate:(uint32_t)framerate {
  _targetBitrateBps = 1000 * bitrateKbit;
  _bitrateAdjuster->SetOriginalTargetBitrateBps(_targetBitrateBps);
  [self setBitrateBps:_bitrateAdjuster->GetAdjustedBitrateBps() frameRate:framerate];
  return WEBRTC_VIDEO_CODEC_OK;
}

#pragma mark - Private

- (NSInteger)releaseEncoder {
  // Need to destroy so that the session is invalidated and won't use the
  // callback anymore. Do not remove callback until the session is invalidated
  // since async encoder callbacks can occur until invalidation.
  [self destroyCompressionSession];
  _callback = nullptr;
  return WEBRTC_VIDEO_CODEC_OK;
}

- (OSType)pixelFormatOfFrame:(NweRTCVideoFrame *)frame {
  // Use NV12 for non-native frames.
  if ([frame.buffer isKindOfClass:[NweRTCCVPixelBuffer class]]) {
    NweRTCCVPixelBuffer *rtcPixelBuffer = (NweRTCCVPixelBuffer *)frame.buffer;
    return CVPixelBufferGetPixelFormatType(rtcPixelBuffer.pixelBuffer);
  }

  return kNV12PixelFormat;
}

- (BOOL)resetCompressionSessionIfNeededWithFrame:(NweRTCVideoFrame *)frame {
  BOOL resetCompressionSession = NO;

  // If we're capturing native frames in another pixel format than the compression session is
  // configured with, make sure the compression session is reset using the correct pixel format.
  OSType framePixelFormat = [self pixelFormatOfFrame:frame];

  if (_compressionSession) {
    // The pool attribute `kCVPixelBufferPixelFormatTypeKey` can contain either an array of pixel
    // formats or a single pixel format.
    NSDictionary *poolAttributes =
        (__bridge NSDictionary *)CVPixelBufferPoolGetPixelBufferAttributes(_pixelBufferPool);
    id pixelFormats =
        [poolAttributes objectForKey:(__bridge NSString *)kCVPixelBufferPixelFormatTypeKey];
    NSArray<NSNumber *> *compressionSessionPixelFormats = nil;
    if ([pixelFormats isKindOfClass:[NSArray class]]) {
      compressionSessionPixelFormats = (NSArray *)pixelFormats;
    } else if ([pixelFormats isKindOfClass:[NSNumber class]]) {
      compressionSessionPixelFormats = @[ (NSNumber *)pixelFormats ];
    }

    if (![compressionSessionPixelFormats
            containsObject:[NSNumber numberWithLong:framePixelFormat]]) {
      resetCompressionSession = YES;
      RTC_LOG(LS_INFO) << "Resetting compression session due to non-matching pixel format.";
    }
  } else {
    resetCompressionSession = YES;
  }

  if (resetCompressionSession) {
    [self resetCompressionSessionWithPixelFormat:framePixelFormat];
  }
  return resetCompressionSession;
}

- (int)resetCompressionSessionWithPixelFormat:(OSType)framePixelFormat {
  [self destroyCompressionSession];

  // Set source image buffer attributes. These attributes will be present on
  // buffers retrieved from the encoder's pixel buffer pool.
  const size_t attributesSize = 3;
  CFTypeRef keys[attributesSize] = {
#if defined(WEBRTC_IOS)
    kCVPixelBufferOpenGLESCompatibilityKey,
#elif defined(WEBRTC_MAC)
    kCVPixelBufferOpenGLCompatibilityKey,
#endif
    kCVPixelBufferIOSurfacePropertiesKey,
    kCVPixelBufferPixelFormatTypeKey
  };
  CFDictionaryRef ioSurfaceValue = CreateCFTypeDictionary(nullptr, nullptr, 0);
  int64_t pixelFormatType = framePixelFormat;
  CFNumberRef pixelFormat = CFNumberCreate(nullptr, kCFNumberLongType, &pixelFormatType);
  CFTypeRef values[attributesSize] = {kCFBooleanTrue, ioSurfaceValue, pixelFormat};
  CFDictionaryRef sourceAttributes = CreateCFTypeDictionary(keys, values, attributesSize);
  if (ioSurfaceValue) {
    CFRelease(ioSurfaceValue);
    ioSurfaceValue = nullptr;
  }
  if (pixelFormat) {
    CFRelease(pixelFormat);
    pixelFormat = nullptr;
  }
  CFMutableDictionaryRef encoder_specs = nullptr;
#if defined(WEBRTC_MAC) && !defined(WEBRTC_IOS)
  // Currently hw accl is supported above 360p on mac, below 360p
  // the compression session will be created with hw accl disabled.
  encoder_specs = CFDictionaryCreateMutable(
      nullptr, 1, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
  CFDictionarySetValue(encoder_specs,
                       kVTVideoEncoderSpecification_EnableHardwareAcceleratedVideoEncoder,
                       kCFBooleanTrue);
  if (@available(macOS LOW_LATENCY_FLAG_AVAILABLE_VER, *)) {
    if (_enableLowLatencyRateControl) {
      // CFDictionarySetValue(encoder_specs,kVTVideoEncoderSpecification_EnableLowLatencyRateControl,kCFBooleanTrue);
    }
  }
#else
  if (@available(iOS 15.0, *)) {
    if (_enableLowLatencyRateControl) {
      encoder_specs = CFDictionaryCreateMutable(kCFAllocatorDefault, 1, NULL, NULL);
      // CFDictionarySetValue(encoder_specs,kVTVideoEncoderSpecification_EnableLowLatencyRateControl,kCFBooleanTrue);
    }
  }
#endif
  OSStatus status =
      VTCompressionSessionCreate(nullptr,  // use default allocator
                                 _width,
                                 _height,
                                 kCMVideoCodecType_H264,
                                 encoder_specs,  // use hardware accelerated encoder if available
                                 sourceAttributes,
                                 nullptr,  // use default compressed data allocator
                                 compressionOutputCallback,
                                 nullptr,
                                 &_compressionSession);
  if (sourceAttributes) {
    CFRelease(sourceAttributes);
    sourceAttributes = nullptr;
  }
  if (encoder_specs) {
    CFRelease(encoder_specs);
    encoder_specs = nullptr;
  }
  if (status != noErr) {
    RTC_LOG(LS_ERROR) << "Failed to create compression session: " << status;
    return WEBRTC_VIDEO_CODEC_ERROR;
  }
#if defined(WEBRTC_MAC) && !defined(WEBRTC_IOS)
  if (!isAppleMSeriesDevice()) {
    CFBooleanRef hwaccl_enabled = nullptr;
    status = VTSessionCopyProperty(_compressionSession,
                                  kVTCompressionPropertyKey_UsingHardwareAcceleratedVideoEncoder,
                                  nullptr,
                                  &hwaccl_enabled);
    if (status == noErr && (CFBooleanGetValue(hwaccl_enabled))) {
      RTC_LOG(LS_INFO) << "Compression session created with hw accl enabled";
    } else {
      RTC_LOG(LS_INFO) << "Compression session created with hw accl disabled, will fallback to software encoder.";
      return WEBRTC_VIDEO_CODEC_FALLBACK_SOFTWARE;
    }
  }
#endif
  [self configureCompressionSession];

  // The pixel buffer pool is dependent on the compression session so if the session is reset, the
  // pool should be reset as well.
  _pixelBufferPool = VTCompressionSessionGetPixelBufferPool(_compressionSession);

  return WEBRTC_VIDEO_CODEC_OK;
}

- (void)configureCompressionSession {
  RTC_DCHECK(_compressionSession);
#if defined(WEBRTC_MAC) && !defined(WEBRTC_IOS)
  SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_RealTime, false);
#else
  SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_RealTime, true);
#endif
  //SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_ProfileLevel, _profile);
  SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_ProfileLevel, kVTProfileLevel_H264_High_AutoLevel);
  SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_AllowFrameReordering, false);
  [self setEncoderBitrateBps:_targetBitrateBps frameRate:_encoderFrameRate];
  // TODO(tkchin): Look at entropy mode and colorspace matrices.
  // TODO(tkchin): Investigate to see if there's any way to make this work.
  // May need it to interop with Android. Currently this call just fails.
  // On inspecting encoder output on iOS8, this value is set to 6.
  // internal::SetVTSessionProperty(compression_session_,
  //     kVTCompressionPropertyKey_MaxFrameDelayCount,
  //     1);

  // Set a relatively large value for keyframe emission (900 frames or 30 seconds).
  SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_MaxKeyFrameInterval, 900);
  SetVTSessionProperty(
      _compressionSession, kVTCompressionPropertyKey_MaxKeyFrameIntervalDuration, 30);
#if defined(WEBRTC_IOS)
    if(@available(iOS 14.2, *)){
        VTCompressionSessionPrepareToEncodeFrames(_compressionSession);
        RTC_LOG(LS_INFO) << "Use VTCompressionSessionPrepareToEncodeFrames";
    }else {
        RTC_LOG(LS_INFO) << "Not use VTCompressionSessionPrepareToEncodeFrames";
    }
#endif

   if (_enableLowLatencyRateControl && _supportSvc) {
      float settingFraction = _svcBaselayerBitrateFraction;
      if(_encodeBaseLayerBitRateFraction != 0.0) {
        settingFraction = _encodeBaseLayerBitRateFraction;
      }

#if defined(WEBRTC_MAC) && !defined(WEBRTC_IOS)
      if (@available(macOS LOW_LATENCY_FLAG_AVAILABLE_VER, *)) {
        _realEnableSvc = YES;
        // SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_BaseLayerFrameRateFraction, (float)_svcBaselayerFramerateFraction);
        // SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_BaseLayerBitRateFraction, (float)settingFraction);
      }
#else
      if (@available(iOS 15.0, *)) {
        _realEnableSvc = YES;
        // SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_BaseLayerFrameRateFraction, (float)_svcBaselayerFramerateFraction);
        // SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_BaseLayerBitRateFraction, (float)settingFraction);
      }
#endif
  }

}

- (void)destroyCompressionSession {
  if (_compressionSession) {
    VTCompressionSessionInvalidate(_compressionSession);
    CFRelease(_compressionSession);
    _compressionSession = nullptr;
    _pixelBufferPool = nullptr;
  }
}

- (NSString *)implementationName {
  return @"vtb[h264]";
}

- (void)setBitrateBps:(uint32_t)bitrateBps frameRate:(uint32_t)frameRate {
  if (_encoderBitrateBps != bitrateBps || _encoderFrameRate != frameRate) {
    [self setEncoderBitrateBps:bitrateBps frameRate:frameRate];
  }
}

- (int)setSVCParam:(RTCEncodeSVCInfo *)svcInfo {
  if(_debug){
     RTC_LOG(LS_INFO) << "setSVCParam: " << [[svcInfo description] UTF8String];
  }

  if(!_enableLowLatencyRateControl || !_supportSvc || !_realEnableSvc){
    return -1;
  }

  float bitRateFraction = svcInfo.baselayer_bitrate_fraction;
  float settingFraction = bitRateFraction;
  //恢复初始值
  if(bitRateFraction == 0.0) {
    settingFraction = _svcBaselayerBitrateFraction;
  }

  //开启svc
  if (svcInfo.enable_svc) {
      if(_encodeBaseLayerBitRateFraction != settingFraction){
#if defined(WEBRTC_MAC) && !defined(WEBRTC_IOS)
         if (@available(macOS LOW_LATENCY_FLAG_AVAILABLE_VER, *)) {
           //帧率比设置固定
          //  SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_BaseLayerFrameRateFraction, (float)_svcBaselayerFramerateFraction);
           //码率比设置动态变化
          //  SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_BaseLayerBitRateFraction, (float)settingFraction);
         }
#else
         if (@available(iOS 15.0, *)) {
           //帧率比设置固定
          //  SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_BaseLayerFrameRateFraction, (float)_svcBaselayerFramerateFraction);
           //码率比设置动态变化
          //  SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_BaseLayerBitRateFraction, (float)settingFraction);
         }
#endif
          _encodeBaseLayerBitRateFraction = settingFraction;
      }
  }else{
      //关闭svc
#if defined(WEBRTC_MAC) && !defined(WEBRTC_IOS)
      if (@available(macOS LOW_LATENCY_FLAG_AVAILABLE_VER, *)) {
        // SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_BaseLayerFrameRateFraction, (float)1.0);
        // SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_BaseLayerBitRateFraction, (float)1.0);
      }
#else
      if (@available(iOS 15.0, *)) {
        // SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_BaseLayerFrameRateFraction, (float)1.0);
        // SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_BaseLayerBitRateFraction, (float)1.0);
      }
#endif
      _encodeBaseLayerBitRateFraction = 1.0;
  }
  return 0;
}

- (RTCEncodeSVCCapabilitiesInfo*)GetSVCCapabilitiesInfo{
  RTCEncodeSVCCapabilitiesInfo* info = [[RTCEncodeSVCCapabilitiesInfo alloc] init];
  info.is_support_svc = _realEnableSvc ? true : false;
  info.max_layer_nums = _realEnableSvc ? 2 : 1;
  info.is_support_dynamic_enable_svc = _realEnableSvc ? true : false;
  info.is_support_dynamic_set_bitrate_fraction = _realEnableSvc ? true : false;
  info.base_vs_enhance_bitrate_fraction_start = _realEnableSvc ? _svcBaselayerBitrateFraction : 0;
  info.base_vs_enhance_bitrate_fraction_min = _realEnableSvc ? _svcMinBaselayerBitrateFraction : 0;
  return info;
}

- (void)setEncoderBitrateBps:(uint32_t)bitrateBps frameRate:(uint32_t)frameRate {
  if (_compressionSession) {
    SetVTSessionProperty(_compressionSession, kVTCompressionPropertyKey_AverageBitRate, bitrateBps);
    SetVTSessionProperty(
        _compressionSession, kVTCompressionPropertyKey_ExpectedFrameRate, frameRate);

    // TODO(tkchin): Add a helper method to set array value.
    int64_t dataLimitBytesPerSecondValue =
        static_cast<int64_t>(bitrateBps * kLimitToAverageBitRateFactor / 8);
    CFNumberRef bytesPerSecond =
        CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt64Type, &dataLimitBytesPerSecondValue);
    int64_t oneSecondValue = 1;
    CFNumberRef oneSecond =
        CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt64Type, &oneSecondValue);
    const void *nums[2] = {bytesPerSecond, oneSecond};
    CFArrayRef dataRateLimits = CFArrayCreate(nullptr, nums, 2, &kCFTypeArrayCallBacks);
    OSStatus status = VTSessionSetProperty(
        _compressionSession, kVTCompressionPropertyKey_DataRateLimits, dataRateLimits);
    if (bytesPerSecond) {
      CFRelease(bytesPerSecond);
    }
    if (oneSecond) {
      CFRelease(oneSecond);
    }
    if (dataRateLimits) {
      CFRelease(dataRateLimits);
    }
    if (status != noErr) {
      RTC_LOG(LS_ERROR) << "Failed to set data rate limit with code: " << status;
    }

    _encoderBitrateBps = bitrateBps;
    _encoderFrameRate = frameRate;
  }
}

- (void)frameWasEncoded:(OSStatus)status
                  flags:(VTEncodeInfoFlags)infoFlags
           sampleBuffer:(CMSampleBufferRef)sampleBuffer
      codecSpecificInfo:(id<NweRTCCodecSpecificInfo>)codecSpecificInfo
                  width:(int32_t)width
                 height:(int32_t)height
           renderTimeMs:(int64_t)renderTimeMs
              timestamp:(uint32_t)timestamp
               rotation:(RTCVideoRotation)rotation {
  RTCVideoEncoderCallback callback = _callback;
  if(!callback) {
    return;
  }
                 
  if (status != noErr) {
    RTC_LOG(LS_ERROR) << "H264 encode failed with code: " << status;
    return;
  }
  if (infoFlags & kVTEncodeInfo_FrameDropped) {
    RTC_LOG(LS_INFO) << "H264 encode dropped frame.";
    return;
  }

  BOOL isKeyframe = NO;
  BOOL isEnhanceFrame = NO;
  CFArrayRef attachments = CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, 0);
  if (attachments != nullptr && CFArrayGetCount(attachments)) {
    CFDictionaryRef attachment =
        static_cast<CFDictionaryRef>(CFArrayGetValueAtIndex(attachments, 0));
    isKeyframe = !CFDictionaryContainsKey(attachment, kCMSampleAttachmentKey_NotSync);
    if(_realEnableSvc){
        CFBooleanRef isDependedOnByOthers = (CFBooleanRef)CFDictionaryGetValue(attachment, kCMSampleAttachmentKey_IsDependedOnByOthers);
        isEnhanceFrame = !(BOOL)CFBooleanGetValue(isDependedOnByOthers);
    }
  }

  if (isKeyframe) {
    RTC_LOG(LS_INFO) << "Generated keyframe";
  }

 if (_debug && isEnhanceFrame) {
   RTC_LOG(LS_INFO) << "Generated enhanceLayerframe";
 }else if(_debug && !isKeyframe){
   RTC_LOG(LS_INFO) << "Generated baseLayerframe";
 }
    
  // Convert the sample buffer into a buffer suitable for RTP packetization.
  // TODO(tkchin): Allocate buffers through a pool.
  std::unique_ptr<rtc::Buffer> buffer(new rtc::Buffer());
  NweRTCRtpFragmentationHeader *header;
  {
    std::unique_ptr<webrtc::RTPFragmentationHeader> header_cpp;
    bool result =
        H264CMSampleBufferToAnnexBBuffer(sampleBuffer, isKeyframe, buffer.get(), &header_cpp);
    header = [[NweRTCRtpFragmentationHeader alloc] initWithNativeFragmentationHeader:header_cpp.get()];
    if (!result) {
      return;
    }
  }

  NweRTCEncodedImage *frame = [[NweRTCEncodedImage alloc] init];
  frame.buffer = [NSData dataWithBytesNoCopy:buffer->data() length:buffer->size() freeWhenDone:NO];
  frame.encodedWidth = width;
  frame.encodedHeight = height;
  frame.completeFrame = YES;
  frame.frameType = isKeyframe ? RTCFrameTypeVideoFrameKey : RTCFrameTypeVideoFrameDelta;
  frame.captureTimeMs = renderTimeMs;
  frame.timeStamp = timestamp;
  frame.rotation = rotation;
  frame.contentType = (_mode == RTCVideoCodecModeScreensharing) ? RTCVideoContentTypeScreenshare :
                                                                  RTCVideoContentTypeUnspecified;
  frame.flags = webrtc::VideoSendTiming::kInvalid;

  frame.targetEncBps = _bitrateAdjuster->GetOriginalTargetBitrateBps();
  frame.savedTargetEncBps = _bitrateAdjuster->GetSavedTargetBitrateBps();
  frame.adjustedEncBps = _encoderBitrateBps;
  frame.hwSmartBitrateHD = _bitrateAdjuster->isEnableHWSmartBitrateHD();
  frame.isBitrateUnderShot = NO;
  frame.isLowBattery =  NO;

  int qp = 0;
  _h264BitstreamParser.ParseBitstream(buffer->data(), buffer->size());
  _h264BitstreamParser.GetLastSliceQp(&qp);
  frame.qp = @(qp);
    
  webrtc::H264BitstreamParser::TemporalType temporalType = _h264BitstreamParser.GetTemporalType();
  std::string type = "";
  switch (temporalType) {
    case webrtc::H264BitstreamParser::TemporalType::kNormal:
        type = "kNormal";
        break;
    case webrtc::H264BitstreamParser::TemporalType::kIdr:
        type = "kIdr";
        break;
    case webrtc::H264BitstreamParser::TemporalType::kSVCBase:
        type = "kSVCBase";
        break;
    case webrtc::H264BitstreamParser::TemporalType::kSVCEnhance1:
        type = "kSVCEnhance1";
        break;
    default:
        break;
  }
  if(_debug){
      RTC_LOG(LS_INFO) << "ParseBitstream: " << type << ", realEncBps:" << buffer->size();
  }

  _bitrateAdjuster->ReportQP(qp);

  
  NweRTCCodecSpecificInfoH264 *codecSpecInfo = (NweRTCCodecSpecificInfoH264 *)codecSpecificInfo;
  codecSpecInfo.enable_svc = _realEnableSvc;
  if (codecSpecInfo.enable_svc) {
      [self MakeSvcParam:codecSpecInfo];
  }

  BOOL res = callback(frame, codecSpecInfo, header);
  if (!res) {
    RTC_LOG(LS_ERROR) << "Encode callback failed";
    return;
  }
  _bitrateAdjuster->Update(frame.buffer.length);
}

- (void)MakeSvcParam:(id<NweRTCCodecSpecificInfo>)codecSpecificInfo{
    webrtc::H264BitstreamParser::TemporalType ttype = _h264BitstreamParser.GetTemporalType();
    NweRTCCodecSpecificInfoH264 *info = (NweRTCCodecSpecificInfoH264 *)codecSpecificInfo;
    if(ttype == webrtc::H264BitstreamParser::TemporalType::kSVCEnhance1) {
      info.temporal_idx = 1;
    } else {
      info.temporal_idx = 0;
    }

    if(ttype == webrtc::H264BitstreamParser::TemporalType::kIdr) {
      info.use_ltr_idx = info.cur_frame_idx;
      svc_last_base_frame_idx_ = info.cur_frame_idx;
    } else if(info.temporal_idx == 0) {
      if(svc_last_base_frame_idx_) {
        info.use_ltr_idx = *svc_last_base_frame_idx_;
      } else {
        RTC_LOG(LS_ERROR) << "[SVC]sender, empty svc_last_base_frame_idx_ 1 cur_idx="<<info.cur_frame_idx;
      }
      svc_last_base_frame_idx_ = info.cur_frame_idx;
    } else {
      if(svc_last_base_frame_idx_) {
        info.use_ltr_idx = *svc_last_base_frame_idx_;
      } else {
        RTC_LOG(LS_ERROR) << "[SVC]sender, empty svc_last_base_frame_idx_ 2 cur_idx="<<info.cur_frame_idx;
      }
    }
}

- (nullable NweRTCVideoEncoderQpThresholds *)scalingSettings {
  return [[NweRTCVideoEncoderQpThresholds alloc] initWithThresholdsLow:kLowH264QpThreshold
                                                               high:kHighH264QpThreshold];
}

@end
