#include "modules/video_coding/codecs/winhw/fallback/fallback_videodecoder.h"

#include <algorithm>
#include <limits>

extern "C" {
#include "third_party/ffmpeg/libavcodec/avcodec.h"
#include "third_party/ffmpeg/libavformat/avformat.h"
#include "third_party/ffmpeg/libavutil/imgutils.h"
}  // extern "C"

#include "api/video/i420_buffer.h"
#include "common_video/include/video_frame_buffer.h"
#include "rtc_base/checks.h"
#include "rtc_base/criticalsection.h"
#include "rtc_base/keep_ref_until_done.h"
#include "rtc_base/logging.h"
#include "system_wrappers/include/metrics.h"
#include "third_party/libyuv/include/libyuv/planar_functions.h"
#include "third_party/libyuv/include/libyuv/scale.h"
#include "api/video/video_frame.h"

namespace netease {

namespace {

const AVPixelFormat kPixelFormatDefault = AV_PIX_FMT_YUV420P;
const AVPixelFormat kPixelFormatFullRange = AV_PIX_FMT_YUVJ420P;
const size_t kYPlaneIndex = 0;
const size_t kUPlaneIndex = 1;
const size_t kVPlaneIndex = 2;

}  // namespace

int FallbackVideoDecoder::AVGetBuffer2(
    AVCodecContext* context, AVFrame* av_frame, int flags) {
  // Set in |InitDecode|.
  FallbackVideoDecoder* decoder = static_cast<FallbackVideoDecoder*>(context->opaque);
  // DCHECK values set in |InitDecode|.
  RTC_DCHECK(decoder);
  // Necessary capability to be allowed to provide our own buffers.
  RTC_DCHECK(context->codec->capabilities | AV_CODEC_CAP_DR1);

  // Limited or full range YUV420 is expected.
  RTC_CHECK(context->pix_fmt == kPixelFormatDefault ||
            context->pix_fmt == kPixelFormatFullRange);

  // |av_frame->width| and |av_frame->height| are set by FFmpeg. These are the
  // actual image's dimensions and may be different from |context->width| and
  // |context->coded_width| due to reordering.
  int width = av_frame->width;
  int height = av_frame->height;
  // See |lowres|, if used the decoder scales the image by 1/2^(lowres). This
  // has implications on which resolutions are valid, but we don't use it.
  RTC_CHECK_EQ(context->lowres, 0);
  // Adjust the |width| and |height| to values acceptable by the decoder.
  // Without this, FFmpeg may overflow the buffer. If modified, |width| and/or
  // |height| are larger than the actual image and the image has to be cropped
  // (top-left corner) after decoding to avoid visible borders to the right and
  // bottom of the actual image.
  avcodec_align_dimensions(context, &width, &height);

  RTC_CHECK_GE(width, 0);
  RTC_CHECK_GE(height, 0);
  int ret = av_image_check_size(static_cast<unsigned int>(width),
                                static_cast<unsigned int>(height), 0, nullptr);
  if (ret < 0) {
    RTC_LOG(LS_ERROR) << "Invalid picture size " << width << "x" << height;
    return ret;
  }

  // The video frame is stored in |frame_buffer|. |av_frame| is FFmpeg's version
  // of a video frame and will be set up to reference |frame_buffer|'s data.

  // FFmpeg expects the initial allocation to be zero-initialized according to
  // http://crbug.com/390941. Our pool is set up to zero-initialize new buffers.
  // TODO(nisse): Delete that feature from the video pool, instead add
  // an explicit call to InitializeData here.
  rtc::scoped_refptr<webrtc::I420Buffer> frame_buffer =
      decoder->pool_.CreateBuffer(width, height);
  if (frame_buffer == nullptr) {
    RTC_LOG(LS_ERROR) << " CreateBuffer Fail With Width:" << width << " Height:" << height;
    return -1;
  }

  int y_size = width * height;
  int uv_size = frame_buffer->ChromaWidth() * frame_buffer->ChromaHeight();
  // DCHECK that we have a continuous buffer as is required.
  RTC_DCHECK_EQ(frame_buffer->DataU(), frame_buffer->DataY() + y_size);
  RTC_DCHECK_EQ(frame_buffer->DataV(), frame_buffer->DataU() + uv_size);
  int total_size = y_size + 2 * uv_size;

  av_frame->format = context->pix_fmt;
  av_frame->reordered_opaque = context->reordered_opaque;

  // Set |av_frame| members as required by FFmpeg.
  av_frame->data[kYPlaneIndex] = frame_buffer->MutableDataY();
  av_frame->linesize[kYPlaneIndex] = frame_buffer->StrideY();
  av_frame->data[kUPlaneIndex] = frame_buffer->MutableDataU();
  av_frame->linesize[kUPlaneIndex] = frame_buffer->StrideU();
  av_frame->data[kVPlaneIndex] = frame_buffer->MutableDataV();
  av_frame->linesize[kVPlaneIndex] = frame_buffer->StrideV();
  RTC_DCHECK_EQ(av_frame->extended_data, av_frame->data);

  // Create a VideoFrame object, to keep a reference to the buffer.
  // TODO(nisse): The VideoFrame's timestamp and rotation info is not used.
  // Refactor to do not use a VideoFrame object at all.
  av_frame->buf[0] = av_buffer_create(
      av_frame->data[kYPlaneIndex],
      total_size,
      AVFreeBuffer2,
      static_cast<void*>(new webrtc::VideoFrame(frame_buffer,
                                        webrtc::kVideoRotation_0,
                                        0 /* timestamp_us */)),
      0);
  RTC_CHECK(av_frame->buf[0]);
  return 0;
}

void FallbackVideoDecoder::AVFreeBuffer2(void* opaque, uint8_t* data) {
  // The buffer pool recycles the buffer used by |video_frame| when there are no
  // more references to it. |video_frame| is a thin buffer holder and is not
  // recycled.
  webrtc::VideoFrame* video_frame = static_cast<webrtc::VideoFrame*>(opaque);
  delete video_frame;
}

FallbackVideoDecoder::FallbackVideoDecoder(AVCodecID codecId) : pool_(true, 30)
{
  m_CodecId = codecId;
  m_bDecoderInited = false;

  av_context_.reset();
  av_frame_.reset();

  if (m_CodecId == AV_CODEC_ID_HEVC) {
      m_CodecFriendlyName = "H265 Decoder [Fallback]";
  }else if(m_CodecId == AV_CODEC_ID_MJPEG) {
      m_CodecFriendlyName = "MJPEG Decoder [Fallback]";
  }else {
      m_CodecFriendlyName = "H264 Decoder [Fallback]";
  }

    av_log_set_level(AV_LOG_WARNING);
    av_log_set_callback(av_log_callback);
}

void FallbackVideoDecoder::av_log_callback(void *ptr, int level, const char *fmt, va_list vl)
{
    if (level>av_log_get_level()) return;
    
    va_list vl2;
    char line[1024];
    static int print_prefix = 1;
    
    va_copy(vl2, vl);
    av_log_format_line(ptr, level, fmt, vl2, line, sizeof(line), &print_prefix);
    va_end(vl2);

    line[1023] = '\0';
    std::string line_str = line;
    
    if (level<=AV_LOG_ERROR) {
      RTC_LOG(LS_ERROR) << line_str;
    }else if(level>AV_LOG_ERROR && level<=AV_LOG_WARNING) {
      RTC_LOG(LS_WARNING) << line_str;
    }else if(level>AV_LOG_WARNING && level<=AV_LOG_INFO ) {
      RTC_LOG(LS_INFO) << line_str;
    }else if(level>AV_LOG_INFO && level<=AV_LOG_VERBOSE) {
      RTC_LOG(LS_VERBOSE) << line_str;
    }else{
      RTC_LOG(LS_SENSITIVE) << line_str;
    }
}


FallbackVideoDecoder::~FallbackVideoDecoder()
{
  this->endDecode();
}

bool FallbackVideoDecoder::init()
{
  this->endDecode();
  RTC_DCHECK(!av_context_);

  RTC_LOG(LS_INFO) << "init success";
  
  return true;
}

bool FallbackVideoDecoder::setConfig(const VideoDecoderConfig &config)
{
  m_config = config;
  return true;
}

bool FallbackVideoDecoder::beginDecode()
{
  if(m_bDecoderInited) return true;

// Initialize AVCodecContext.
  av_context_.reset(avcodec_alloc_context3(nullptr));

  av_context_->codec_type = AVMEDIA_TYPE_VIDEO;
  av_context_->codec_id = m_CodecId;
  av_context_->coded_width = m_config.width;
  av_context_->coded_height = m_config.height;
  av_context_->pix_fmt = kPixelFormatDefault;
  av_context_->extradata = nullptr;
  av_context_->extradata_size = 0;

  // If this is ever increased, look at |av_context_->thread_safe_callbacks| and
  // make it possible to disable the thread checker in the frame buffer pool.
  // av_context_->thread_count = 1;
  // av_context_->thread_type = FF_THREAD_SLICE;

  av_context_->thread_count = 8;
  av_context_->thread_type = FF_THREAD_SLICE; // FF_THREAD_FRAME;

  if(m_CodecId == AV_CODEC_ID_MJPEG) {
    av_context_->thread_count = 1;
    av_context_->thread_type = FF_THREAD_FRAME;
  }

  // Function used by FFmpeg to get buffers to store decoded frames in.
  av_context_->get_buffer2 = AVGetBuffer2;
  // |get_buffer2| is called with the context, there |opaque| can be used to get
  // a pointer |this|.
  av_context_->opaque = this;

  AVCodec* codec = avcodec_find_decoder(av_context_->codec_id);
  if (!codec) {
    // This is an indication that FFmpeg has not been initialized or it has not
    // been compiled/initialized with the correct set of codecs.
    RTC_LOG(LS_ERROR) << "FFmpeg H.265 decoder not found.";
    av_context_.reset();
    av_frame_.reset();
    return false;
  }
  int res = avcodec_open2(av_context_.get(), codec, nullptr);
  if (res < 0) {
    RTC_LOG(LS_ERROR) << "avcodec_open2 error: " << res;
    av_context_.reset();
    av_frame_.reset();
    return false;
  }

  av_frame_.reset(av_frame_alloc());

  RTC_LOG(LS_INFO) << "beginDecode success";

  m_bDecoderInited = true;

  return true;
}

bool FallbackVideoDecoder::addEncodedFrame(const VideoEncodedFrame &frame)
{
  if (!m_bDecoderInited) {
      RTC_LOG(LS_WARNING) << "Please Call beginDecode()";
      return true;
  }

  AVPacket packet;
  av_init_packet(&packet);
  packet.data = frame.frameData.ptr;
  packet.size = frame.frameSize;
  int64_t frame_timestamp_us = frame.pts;
  av_context_->reordered_opaque = frame_timestamp_us;

  int result = avcodec_send_packet(av_context_.get(), &packet);
  if (result < 0) {
    RTC_LOG(LS_ERROR) << "avcodec_send_packet error: " << result;
    return false;
  }

  return true;
}

bool FallbackVideoDecoder::getRawFrame(VideoRawFrame &frame)
{
  if (!m_bDecoderInited) {
      RTC_LOG(LS_WARNING) << "Please Call beginDecode()";
      return true;
  }

  int result = avcodec_receive_frame(av_context_.get(), av_frame_.get());
  if (result < 0) {
    RTC_LOG(LS_ERROR) << "avcodec_receive_frame error: " << result;
    return false;
  }

  // Obtain the |video_frame| containing the decoded image.
  webrtc::VideoFrame* input_frame =
      static_cast<webrtc::VideoFrame*>(av_buffer_get_opaque(av_frame_->buf[0]));
  RTC_DCHECK(input_frame);

  rtc::scoped_refptr<webrtc::I420BufferInterface> i420_buffer =
      input_frame->video_frame_buffer()->GetI420();
  RTC_CHECK_EQ(av_frame_->data[kYPlaneIndex], i420_buffer->DataY());
  RTC_CHECK_EQ(av_frame_->data[kUPlaneIndex], i420_buffer->DataU());
  RTC_CHECK_EQ(av_frame_->data[kVPlaneIndex], i420_buffer->DataV());

  rtc::scoped_refptr<webrtc::I420Buffer> frame_buffer = pool_.CreateBuffer(av_frame_->width, av_frame_->height);
  if (frame_buffer == nullptr)
  {
    av_frame_unref(av_frame_.get());
    input_frame = nullptr;
    
    RTC_LOG(LS_ERROR) << "CreateBuffer Fail With Width:" << av_frame_->width << " Height:" << av_frame_->height;
    return false;
  }
  
  libyuv::I420Copy(i420_buffer->DataY(), i420_buffer->StrideY(),
              i420_buffer->DataU(), i420_buffer->StrideU(),
              i420_buffer->DataV(), i420_buffer->StrideV(),
              frame_buffer->MutableDataY(), frame_buffer->StrideY(),
              frame_buffer->MutableDataU(), frame_buffer->StrideU(),
              frame_buffer->MutableDataV(), frame_buffer->StrideV(),
              av_frame_->width, av_frame_->height);

  frame.width = av_frame_->width;
  frame.height = av_frame_->height;
  frame.timestamp = av_frame_->reordered_opaque;

	frame.format = FOURCC_I420;
  frame.frame_buffer = frame_buffer;

  // Stop referencing it, possibly freeing |input_frame|.
  av_frame_unref(av_frame_.get());
  input_frame = nullptr;

  return true;
}

bool FallbackVideoDecoder::endDecode()
{
  if(!m_bDecoderInited) return true;

  av_context_.reset();
  av_frame_.reset();

  m_bDecoderInited = false;
  
  return true;
}

VIDEO_STATNDARD FallbackVideoDecoder::getVideoStandard()
{
	if (m_CodecId == AV_CODEC_ID_HEVC)
		return VideoStandard_ITU_H265;
	else if (m_CodecId == AV_CODEC_ID_H264)
		return VideoStandard_ITU_H264;
  else 
    return VideoStandard_UNKNOW;
}

VIDEO_CODEC_PROVIDER FallbackVideoDecoder::getCodecId()
{
	if (m_CodecId == AV_CODEC_ID_HEVC)
		return FB_CODEC_FF_HEVC;
  else if (m_CodecId == AV_CODEC_ID_MJPEG)
    return FB_CODEC_FF_MJPEG;
	else
		return FB_CODEC_FF;

}

const char* FallbackVideoDecoder::getCodecFriendlyName()
{
    return m_CodecFriendlyName.c_str();
}

const std::string FallbackVideoDecoder::getCodecGPUName()
{
    return "";
}

CODEC_RETCODE CreateFallbackVideoDecoder(VIDEO_CODEC_PROVIDER codec, HWVideoDecoder** ppVideoDecoder)
{
  RTC_LOG(LS_INFO) << "CODEC_RETCODE CreateFallbackVideoDecoder";

	AVCodecID codecId;
	if (codec == FB_CODEC_FF){
		codecId = AV_CODEC_ID_H264;
		RTC_LOG(LS_INFO) << "[FallbackVideoDecoder] H264";
	}
	else if (codec == FB_CODEC_FF_HEVC){
		codecId = AV_CODEC_ID_HEVC;
		RTC_LOG(LS_INFO) << "[FallbackVideoDecoder] HEVC";
	}
	else if (codec == FB_CODEC_FF_MJPEG){
		codecId = AV_CODEC_ID_MJPEG;
		RTC_LOG(LS_INFO) << "[FallbackVideoDecoder] MJPEG";
	}
	else
		return CODEC_ERR_UNSUPPORTED;

	FallbackVideoDecoder *decoder = new FallbackVideoDecoder(codecId);
	if (decoder && decoder->init()) {
		*ppVideoDecoder = decoder;
		return CODEC_OK; // init succeeded
	}

	delete decoder;
	return CODEC_ERR_UNKNOWN;
}

} // namespace netease