#pragma once

#include "modules/video_coding/codecs/winhw/libhwcodec.h"

extern "C" {
#include "third_party/ffmpeg/libavcodec/avcodec.h"
#include "third_party/ffmpeg/libavformat/avformat.h"
}  // extern "C"

#include "common_video/include/i420_buffer_pool.h"

namespace netease {

struct AVCodecContextDeleter {
  void operator()(AVCodecContext* ptr) const { avcodec_free_context(&ptr); }
};
struct AVFrameDeleter {
  void operator()(AVFrame* ptr) const { av_frame_free(&ptr); }
};

class FallbackVideoDecoder: public netease::HWVideoDecoder
{
public:
	FallbackVideoDecoder(AVCodecID codecId);
	~FallbackVideoDecoder();

	// implement VideoDecoder interface
	bool init();
	bool setConfig(const VideoDecoderConfig &config);
	bool beginDecode();
	bool addEncodedFrame(const VideoEncodedFrame &frame);
	bool getRawFrame(VideoRawFrame &frame);
	bool endDecode();
	VIDEO_STATNDARD getVideoStandard();
	VIDEO_CODEC_PROVIDER getCodecId();
	const char* getCodecFriendlyName();
	const std::string getCodecGPUName();
private:
  // Called by FFmpeg when it needs a frame buffer to store decoded frames in.
  // The |VideoFrame| returned by FFmpeg at |Decode| originate from here. Their
  // buffers are reference counted and freed by FFmpeg using |AVFreeBuffer2|.
  static int AVGetBuffer2(AVCodecContext* context,
                          AVFrame* av_frame,
                          int flags);
  // Called by FFmpeg when it is done with a video frame, see |AVGetBuffer2|.
  static void AVFreeBuffer2(void* opaque, uint8_t* data);

  static void av_log_callback(void *ptr, int level, const char *fmt, va_list vl);
private:
  AVCodecID m_CodecId;
  bool m_bDecoderInited = false;
  VideoDecoderConfig m_config;

  webrtc::I420BufferPool pool_;
  std::unique_ptr<AVCodecContext, AVCodecContextDeleter> av_context_;
  std::unique_ptr<AVFrame, AVFrameDeleter> av_frame_;

	std::string m_CodecFriendlyName;
};

CODEC_RETCODE CreateFallbackVideoDecoder(VIDEO_CODEC_PROVIDER codec, HWVideoDecoder** ppVideoDecoder);

} // namespace netease