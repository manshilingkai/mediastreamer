#include "modules/video_coding/codecs/winhw/amd/amd_videodecoder.h"
#include "rtc_base/logging.h"
#include "modules/video_coding/codecs/winhw/win/gpu_info.h"

namespace netease {

AMDVideoDecoder::AMDVideoDecoder(AMF_STREAM_CODEC_ID_ENUM codecId)
{
    m_CodecId = codecId;

    m_amfContext = NULL;
    m_amfDecoder = NULL;

    m_isRefAMFFactory = false;
    m_bDecoderInited = false;

    m_isOutputRawFrameFilled = false;

    m_ouputAMFSurface = NULL;

	m_HWGPUName = "AMD";
    std::vector<GpuInfo>& available_gpuInfos = GPUInfoHandler::GetInstance().GetAvailableGpuInfos();
	for (GpuInfo gpuInfo : available_gpuInfos) {
        if (gpuInfo.provider == GPU_AMD) {
            m_HWGPUName = gpuInfo.name;
        }
    }

    if (m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC)
    {
        m_CodecFriendlyName = "H265 Decoder [" + m_HWGPUName + "]";
    }else {
        m_CodecFriendlyName = "H264 Decoder [" + m_HWGPUName + "]";
    }
}

AMDVideoDecoder::~AMDVideoDecoder()
{
    this->endDecode();

    if (m_amfContext)
    {
        m_amfContext->Terminate();
        m_amfContext = NULL;
    }

    if (m_isRefAMFFactory)
    {
        g_AMFFactory.Terminate();
        m_isRefAMFFactory = false;
    }
}

bool AMDVideoDecoder::init()
{
    AMF_RESULT res = AMF_OK;
    res = g_AMFFactory.Init();

    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "AMF Failed to initialize, ErrorCode: " << res;
        return false;
    }

    m_isRefAMFFactory = true;

    res = g_AMFFactory.GetFactory()->CreateContext(&m_amfContext);
    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "AMF Failed to CreateContext, ErrorCode: " << res;
        return false;
    }

    m_memoryTypeOut = amf::AMF_MEMORY_UNKNOWN;
    bool ret = initDX12();
    if (!ret) ret = initDX11();
    if (!ret) ret = initDX9();
    
    return ret;
}

bool AMDVideoDecoder::initDX12()
{
    AMF_RESULT res = AMF_OK;
    amf::AMFContext2Ptr context2(m_amfContext);
    if(context2 == nullptr)
    {
        RTC_LOG(LS_ERROR) << "amf::AMFContext2 is missing";
        return false;
    }
    res = context2->InitDX12(NULL);
    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "amf::AMFContext2 InitDX12 Fail, ErrorCode: " << res;
        return false;
    }

    m_memoryTypeOut = amf::AMF_MEMORY_DX12;
    return true;
}

bool AMDVideoDecoder::initDX11()
{
    AMF_RESULT res = AMF_OK;
    res = m_amfContext->InitDX11(NULL);
    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "amf::AMFContext InitDX11 Fail, ErrorCode: " << res;
        return false;
    }

    m_memoryTypeOut = amf::AMF_MEMORY_DX11;
    return true;
}

bool AMDVideoDecoder::initDX9()
{
    AMF_RESULT res = AMF_OK;
    res = m_amfContext->InitDX9(NULL);
    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "amf::AMFContext InitDX9 Fail, ErrorCode: " << res;
        return false;
    }

    m_memoryTypeOut = amf::AMF_MEMORY_DX9;
    return true;
}

bool AMDVideoDecoder::setConfig(const VideoDecoderConfig &config)
{
    m_config = config;
    return true;
}

bool AMDVideoDecoder::beginDecode()
{
    if (m_bDecoderInited) return true;

    AMF_RESULT res = AMF_OK;
    if (m_CodecId == AMF_STREAM_CODEC_ID_H264_AVC)
    {
		res = g_AMFFactory.GetFactory()->CreateComponent(m_amfContext, AMFVideoDecoderUVD_H264_AVC, &m_amfDecoder);
    }else if (m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC)
    {
        res = g_AMFFactory.GetFactory()->CreateComponent(m_amfContext, AMFVideoDecoderHW_H265_HEVC, &m_amfDecoder);
    }

    if (res != AMF_OK || m_amfDecoder==NULL)
    {
        RTC_LOG(LS_ERROR) << "amf::CreateComponent Fail, ErrorCode: " << res;
        return false;
    }
    
    // res = m_amfDecoder->SetProperty(AMF_TIMESTAMP_MODE, amf_int64(AMF_TS_DECODE));
    res = m_amfDecoder->SetProperty(AMF_TIMESTAMP_MODE, amf_int64(AMF_TS_PRESENTATION));
    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "amf::SetProperty AMF_TIMESTAMP_MODE Fail, ErrorCode: " << res;
        return false;
    }

    m_formatOut = amf::AMF_SURFACE_YUV420P;
    res = m_amfDecoder->Init(m_formatOut, m_config.width, m_config.height);
    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "amf::AMFComponent Init Fail With amf::AMF_SURFACE_YUV420P, ErrorCode: " << res;
        
        m_formatOut = amf::AMF_SURFACE_NV12;
        res = m_amfDecoder->Init(m_formatOut, m_config.width, m_config.height);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_ERROR) << "amf::AMFComponent Init Fail With amf::AMF_SURFACE_NV12, ErrorCode: " << res;
            return false;
        }else {
            RTC_LOG(LS_INFO) << "amf::AMFComponent Init Success With amf::AMF_SURFACE_NV12";
        }
    }else {
        RTC_LOG(LS_INFO) << "amf::AMFComponent Init Success With amf::AMF_SURFACE_YUV420P";
    }

    m_isOutputRawFrameFilled = false;

    RTC_LOG(LS_INFO) << "AMDVideoDecoder beginDecode success";

    m_bDecoderInited = true;
    return true;
}

bool AMDVideoDecoder::addEncodedFrame(const VideoEncodedFrame &frame)
{
    if (!m_bDecoderInited) {
        RTC_LOG(LS_WARNING) << "Please Call beginDecode()";
        return true;
    }

    m_isOutputRawFrameFilled = false;
    bool ret = this->inputEncodedFrame(frame);
    if (!ret) return false;
    if (!m_isOutputRawFrameFilled)
    {
        ret = this->outputRawFrame(m_outputRawFrame);
        if (!ret) return false;
    }
    return true;
}

bool AMDVideoDecoder::inputEncodedFrame(const VideoEncodedFrame &frame)
{
    while (true)
    {
        amf::AMFBufferPtr encodedAMFBuffer = NULL;
        AMF_RESULT ar = m_amfContext->AllocBuffer(amf::AMF_MEMORY_HOST, frame.frameSize, &encodedAMFBuffer);
        if (ar != AMF_OK || encodedAMFBuffer == NULL)
        {
            RTC_LOG(LS_ERROR) << "amf::AMFContext AllocBuffer Fail, ErrorCode: " << ar;
            return false;
        }

        amf_uint8 *data = (amf_uint8*)encodedAMFBuffer->GetNative();
        memcpy(data, frame.frameData.ptr, frame.frameSize);
        encodedAMFBuffer->SetPts(frame.pts);
        // amf::AMFData* encodedAMFData = encodedAMFBuffer.Detach();
        
        AMF_RESULT res = m_amfDecoder->SubmitInput(encodedAMFBuffer);
        if (res == AMF_OK) return true;
        
        if (res == AMF_NEED_MORE_INPUT)
        {
            RTC_LOG(LS_WARNING) << "amf::AMFComponent SubmitInput Success, But Need More Input";
            return true;
        }else if(res == AMF_INPUT_FULL || res == AMF_DECODER_NO_FREE_SURFACES)
        {
            if (res == AMF_INPUT_FULL)
            {
                RTC_LOG(LS_WARNING) << "amf::AMFComponent SubmitInput Fail, Input Queue is Full, try to get ready surfaces with outputRawFrame() and repeat submission";
            }else if(res == AMF_DECODER_NO_FREE_SURFACES)
            {
                RTC_LOG(LS_WARNING) << "amf::AMFComponent SubmitInput Fail, No Free Surfaces, try to get ready surfaces with outputRawFrame() and repeat submission";
            }

            bool ret = this->outputRawFrame(m_outputRawFrame);
            if (!ret) return false;

            continue;
        }else
        {
            RTC_LOG(LS_ERROR) << "amf::AMFComponent SubmitInput Fail, ErrorCode: " << res;
            return false;
        }
    }
}

bool AMDVideoDecoder::outputRawFrame(VideoRawFrame &frame)
{
    while (true)
    {
        if (m_ouputAMFSurface)
        {
            m_ouputAMFSurface.Release();
            m_ouputAMFSurface = NULL;
        }
        
        amf::AMFDataPtr surfaceData = NULL;
        AMF_RESULT res = m_amfDecoder->QueryOutput(&surfaceData);
        if (res == AMF_OK)
        {
            if (surfaceData==NULL)
            {
                RTC_LOG(LS_WARNING) << "amf::AMFComponent QueryOutput Success, But No Output Surface Data ";
                return true;
            }

            res = surfaceData->Convert(amf::AMF_MEMORY_HOST); // convert to system memory
            if (res != AMF_OK)
            {
                RTC_LOG(LS_ERROR) << "amf::AMFData Convert Fail, ErrorCode: " << res;
                return false;
            }
        
            amf::AMFSurfacePtr surface(surfaceData); // query for surface interface
            frame.timestamp = surfaceData->GetPts();
            if (m_formatOut == amf::AMF_SURFACE_YUV420P)
            {
                frame.format = FOURCC_I420;

                amf::AMFPlane *y_amf_plane = surface->GetPlane(amf::AMF_PLANE_Y);
                amf_uint8 *data     = reinterpret_cast<amf_uint8*>(y_amf_plane->GetNative());
                amf_int32 offsetX   = y_amf_plane->GetOffsetX();
                amf_int32 offsetY   = y_amf_plane->GetOffsetY();
                amf_int32 pixelSize = y_amf_plane->GetPixelSizeInBytes();
                amf_int32 height    = y_amf_plane->GetHeight();
                amf_int32 width     = y_amf_plane->GetWidth();
                amf_int32 pitchH    = y_amf_plane->GetHPitch();

                frame.width = pixelSize * width;
                frame.height = height;

                frame.strides[0] = pitchH;
                frame.planeptrs[0].ptr = data + offsetY * pitchH + offsetX * pixelSize;

                amf::AMFPlane *u_amf_plane = surface->GetPlane(amf::AMF_PLANE_U);
                data     = reinterpret_cast<amf_uint8*>(u_amf_plane->GetNative());
                offsetX   = u_amf_plane->GetOffsetX();
                offsetY   = u_amf_plane->GetOffsetY();
                pixelSize = u_amf_plane->GetPixelSizeInBytes();
                height    = u_amf_plane->GetHeight();
                width     = u_amf_plane->GetWidth();
                pitchH    = u_amf_plane->GetHPitch();
                frame.strides[1] = pitchH;
                frame.planeptrs[1].ptr = data + offsetY * pitchH + offsetX * pixelSize;

                amf::AMFPlane *v_amf_plane = surface->GetPlane(amf::AMF_PLANE_V);
                data     = reinterpret_cast<amf_uint8*>(v_amf_plane->GetNative());
                offsetX   = v_amf_plane->GetOffsetX();
                offsetY   = v_amf_plane->GetOffsetY();
                pixelSize = v_amf_plane->GetPixelSizeInBytes();
                height    = v_amf_plane->GetHeight();
                width     = v_amf_plane->GetWidth();
                pitchH    = v_amf_plane->GetHPitch();
                frame.strides[2] = pitchH;
                frame.planeptrs[2].ptr = data + offsetY * pitchH + offsetX * pixelSize;

                m_ouputAMFSurface = surface;
                m_isOutputRawFrameFilled = true;
                return true;
            }else if (m_formatOut == amf::AMF_SURFACE_NV12)
            {
                frame.format = FOURCC_NV12;

                amf::AMFPlane *y_amf_plane = surface->GetPlane(amf::AMF_PLANE_Y);
                amf_uint8 *data     = reinterpret_cast<amf_uint8*>(y_amf_plane->GetNative());
                amf_int32 offsetX   = y_amf_plane->GetOffsetX();
                amf_int32 offsetY   = y_amf_plane->GetOffsetY();
                amf_int32 pixelSize = y_amf_plane->GetPixelSizeInBytes();
                amf_int32 height    = y_amf_plane->GetHeight();
                amf_int32 width     = y_amf_plane->GetWidth();
                amf_int32 pitchH    = y_amf_plane->GetHPitch();

                frame.width = pixelSize * width;
                frame.height = height;

                frame.strides[0] = pitchH;
                frame.planeptrs[0].ptr = data + offsetY * pitchH + offsetX * pixelSize;

                amf::AMFPlane *uv_amf_plane = surface->GetPlane(amf::AMF_PLANE_UV);
                data     = reinterpret_cast<amf_uint8*>(uv_amf_plane->GetNative());
                offsetX   = uv_amf_plane->GetOffsetX();
                offsetY   = uv_amf_plane->GetOffsetY();
                pixelSize = uv_amf_plane->GetPixelSizeInBytes();
                height    = uv_amf_plane->GetHeight();
                width     = uv_amf_plane->GetWidth();
                pitchH    = uv_amf_plane->GetHPitch();
                frame.strides[1] = pitchH;
                frame.planeptrs[1].ptr = data + offsetY * pitchH + offsetX * pixelSize;

                m_ouputAMFSurface = surface;
                m_isOutputRawFrameFilled = true;
                return true;
            }else {
                RTC_LOG(LS_ERROR) << "UnSupported amf::AMF_SURFACE_FORMAT : " << m_formatOut;
                return false;
            }
        }else if (res == AMF_REPEAT)
        {
            continue;
        }else {
            RTC_LOG(LS_ERROR) << "amf::AMFComponent QueryOutput Fail, ErrorCode: " << res;
            return false;
        }
    }
}

bool AMDVideoDecoder::getRawFrame(VideoRawFrame &frame)
{
    if (m_isOutputRawFrameFilled)
    {
        frame.format = m_outputRawFrame.format;
        frame.timestamp = m_outputRawFrame.timestamp;
        frame.width = m_outputRawFrame.width;
        frame.height = m_outputRawFrame.height;
        frame.strides[0] = m_outputRawFrame.strides[0];
        frame.planeptrs[0].ptr = m_outputRawFrame.planeptrs[0].ptr;
        frame.strides[1] = m_outputRawFrame.strides[1];
        frame.planeptrs[1].ptr = m_outputRawFrame.planeptrs[1].ptr;
        frame.strides[2] = m_outputRawFrame.strides[2];
        frame.planeptrs[2].ptr = m_outputRawFrame.planeptrs[2].ptr;

        m_isOutputRawFrameFilled = false;
        return true;
    }else {
        RTC_LOG(LS_WARNING) << "no output VideoRawFrame";
		return false;
    }
}

bool AMDVideoDecoder::endDecode()
{
    if (!m_bDecoderInited) return true;
    
    m_amfDecoder->Drain();
    m_amfDecoder->Flush();

    if (m_memoryTypeOut == amf::AMF_MEMORY_DX12)
    {
        amf::AMFComputePtr pCompute;
        if (m_amfContext->GetCompute(amf::AMF_MEMORY_DX12, &pCompute) == AMF_OK)
        {
            pCompute->FinishQueue();
        }
    }

    if (m_ouputAMFSurface)
    {
        m_ouputAMFSurface.Release();
        m_ouputAMFSurface = NULL;
    }

    m_amfDecoder->Terminate();
    m_amfDecoder = NULL;
    
    m_bDecoderInited = false;

    RTC_LOG(LS_INFO) << "AMDVideoDecoder endDecode success";

    return true;
}

VIDEO_STATNDARD AMDVideoDecoder::getVideoStandard()
{
	if (m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC)
		return VideoStandard_ITU_H265;
	else
		return VideoStandard_ITU_H264;
}

VIDEO_CODEC_PROVIDER AMDVideoDecoder::getCodecId()
{
	if (m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC)
		return HW_CODEC_AMD_HEVC;
	else
		return HW_CODEC_AMD;
}

const char* AMDVideoDecoder::getCodecFriendlyName()
{
    return m_CodecFriendlyName.c_str();
}

const std::string AMDVideoDecoder::getCodecGPUName()
{
    return m_HWGPUName;
}

CODEC_RETCODE CreateAMDVideoDecoder(VIDEO_CODEC_PROVIDER codec, HWVideoDecoder** ppVideoDecoder)
{
    RTC_LOG(LS_INFO) << "CODEC_RETCODE CreateAMDVideoDecoder";

	AMF_STREAM_CODEC_ID_ENUM codecId;
	if (codec == HW_CODEC_AMD){
		codecId = AMF_STREAM_CODEC_ID_H264_AVC;
		RTC_LOG(LS_INFO) << "[AMDVideoDecoder] H264";
	}
	else if (codec == HW_CODEC_AMD_HEVC){
		codecId = AMF_STREAM_CODEC_ID_H265_HEVC;
		RTC_LOG(LS_INFO) << "[AMDVideoDecoder] HEVC";
	}
	else
		return CODEC_ERR_UNSUPPORTED;

	AMDVideoDecoder *decoder = new AMDVideoDecoder(codecId);
	if (decoder && decoder->init()) {
		*ppVideoDecoder = decoder;
		return CODEC_OK; // init succeeded
	}

	delete decoder;
	return CODEC_ERR_UNKNOWN;
}

} // namespace netease