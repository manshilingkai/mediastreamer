#include "modules/video_coding/codecs/winhw/amd/amd_videocodec_capability.h"

#include "third_party/amf/include/components/VideoDecoderUVD.h"
#include "third_party/amf/include/components/Component.h"

#include "third_party/amf/include/components/VideoEncoderVCE.h"
#include "third_party/amf/include/components/VideoEncoderHEVC.h"
#include "modules/video_coding/codecs/winhw/amd/amf/common/AMFFactory.h"
#include "modules/video_coding/codecs/winhw/amd/amf/common/Thread.h"

#include "rtc_base/logging.h"

namespace netease {

void getSVCCapability(const wchar_t *componentID, amf::AMFContext* pContext, bool isEncoder, bool isH264, int &supportedMaxTemporalLayers)
{
    AMF_RESULT res = AMF_OK;
    amf::AMFCapsPtr codecCaps;
    amf::AMFComponentPtr pCodec = NULL;
    res = g_AMFFactory.GetFactory()->CreateComponent(pContext, componentID, &pCodec);
    if(res !=AMF_OK || pCodec == NULL)
    {
        RTC_LOG(LS_ERROR) << "AMF Failed to CreateComponent";
        return;
    }

    if (pCodec->GetCaps(&codecCaps) == AMF_OK)
    {
        amf::AMF_ACCELERATION_TYPE accelType = codecCaps->GetAccelerationType();
        if (accelType == amf::AMF_ACCEL_HARDWARE || accelType == amf::AMF_ACCEL_GPU) {
            amf_int64 max_temporal_layers = -1;
            if (isEncoder)
            {
                if (isH264) {
                    codecCaps->GetProperty(AMF_VIDEO_ENCODER_CAP_MAX_TEMPORAL_LAYERS, &max_temporal_layers);
                }
            }
            supportedMaxTemporalLayers = max_temporal_layers;
            
            pCodec->Terminate();
            pCodec = NULL;
            return;
        }else {
            if (accelType == amf::AMF_ACCEL_NOT_SUPPORTED)
            {
                RTC_LOG(LS_ERROR) << "amf::AMF_ACCEL_NOT_SUPPORTED";
            }else if (accelType == amf::AMF_ACCEL_SOFTWARE)
            {
                RTC_LOG(LS_ERROR) << "amf::AMF_ACCEL_SOFTWARE";
            }
            
            pCodec->Terminate();
            pCodec = NULL;
            return;
        }
    }
    else
    {
        RTC_LOG(LS_ERROR) << "AMF Failed to GetCaps";
        pCodec->Terminate();
        pCodec = NULL;
        return;
    }
}

bool isAMDSupportH26XCodec(const wchar_t *componentID, amf::AMFContext* pContext, bool isEncoder, bool isH264, int &supportedMaxInstance, int &minSupportedWidth, int &minSupportedHeight, int &maxSupportedWidth, int& maxSupportedHeight, int64_t& maxEncodeBitrateBps)
{
    supportedMaxInstance = -1;
    minSupportedWidth = -1;
    minSupportedHeight = -1;
    maxSupportedWidth = -1;
    maxSupportedHeight = -1;
    maxEncodeBitrateBps = -1;

    AMF_RESULT res = AMF_OK;
    amf::AMFCapsPtr codecCaps;
    amf::AMFComponentPtr pCodec = NULL;
    res = g_AMFFactory.GetFactory()->CreateComponent(pContext, componentID, &pCodec);
    if(res !=AMF_OK || pCodec == NULL)
    {
        RTC_LOG(LS_ERROR) << "AMF Failed to CreateComponent";
        return false;
    }

    if (pCodec->GetCaps(&codecCaps) == AMF_OK)
    {
        amf::AMF_ACCELERATION_TYPE accelType = codecCaps->GetAccelerationType();
        if (accelType == amf::AMF_ACCEL_HARDWARE || accelType == amf::AMF_ACCEL_GPU)
        {
            amf_int64 max_num_of_streams_for_codec = 0;
            if (isEncoder)
            {
                if (isH264) {
                    res = codecCaps->GetProperty(AMF_VIDEO_ENCODER_CAP_NUM_OF_STREAMS, &max_num_of_streams_for_codec);
                }else {
                    res = codecCaps->GetProperty(AMF_VIDEO_ENCODER_HEVC_CAP_NUM_OF_STREAMS, &max_num_of_streams_for_codec);
                }
            }else {
                res = codecCaps->GetProperty(AMF_VIDEO_DECODER_CAP_NUM_OF_STREAMS, &max_num_of_streams_for_codec);
            }
            
            if (res == AMF_OK && max_num_of_streams_for_codec <= 0)
            {
                pCodec->Terminate();
                pCodec = NULL;
                return false;
            }
            supportedMaxInstance = max_num_of_streams_for_codec;

            if (isEncoder)
            {
                amf_int64 max_bitrate_bps = 0;
                if (isH264)
                {
                    res = codecCaps->GetProperty(AMF_VIDEO_ENCODER_CAP_MAX_BITRATE, &max_bitrate_bps);
                }else {
                    res = codecCaps->GetProperty(AMF_VIDEO_ENCODER_HEVC_CAP_MAX_BITRATE, &max_bitrate_bps);
                }

                if (res == AMF_OK && max_bitrate_bps > 0)
                {
                    maxEncodeBitrateBps = max_bitrate_bps;
                }
            }

            amf::AMFIOCapsPtr inputCaps = NULL;
            if (codecCaps->GetInputCaps(&inputCaps) == AMF_OK)
            {
                if (inputCaps != NULL)
                {
                    amf_int32 minWidth = 0, maxWidth = 0;
                    inputCaps->GetWidthRange(&minWidth, &maxWidth);
                    amf_int32 minHeight = 0, maxHeight = 0;
                    inputCaps->GetHeightRange(&minHeight, &maxHeight);

                    minSupportedWidth = minWidth;
                    minSupportedHeight = minHeight;
                    maxSupportedWidth = maxWidth;
                    maxSupportedHeight = maxHeight;
                }
            }
            
            pCodec->Terminate();
            pCodec = NULL;
            return true;
        }else {
            if (accelType == amf::AMF_ACCEL_NOT_SUPPORTED)
            {
                RTC_LOG(LS_ERROR) << "amf::AMF_ACCEL_NOT_SUPPORTED";
            }else if (accelType == amf::AMF_ACCEL_SOFTWARE)
            {
                RTC_LOG(LS_ERROR) << "amf::AMF_ACCEL_SOFTWARE";
            }
            
            pCodec->Terminate();
            pCodec = NULL;
            return false;
        }
    }
    else
    {
        RTC_LOG(LS_ERROR) << "AMF Failed to GetCaps";
        pCodec->Terminate();
        pCodec = NULL;
        return false;
    }
}

bool getAMDEncodeAndDecodeCapability(bool &isSupportH264Encode, bool &isSupportH265Encode, bool &isSupportH264Decode, bool &isSupportH265Decode, 
                                    int &supportedMaxInstanceForH264Encode, int &supportedMaxInstanceForH264Decode, int &supportedMaxInstanceForH265Encode, int &supportedMaxInstanceForH265Decode, 
                                    int &minSupportedWidthForH264Encode, int &minSupportedHeightForH264Encode, int &maxSupportedWidthForH264Encode, int &maxSupportedHeightForH264Encode, 
                                    int &minSupportedWidthForH264Decode, int &minSupportedHeightForH264Decode, int &maxSupportedWidthForH264Decode, int &maxSupportedHeightForH264Decode, 
                                    int &minSupportedWidthForH265Encode, int &minSupportedHeightForH265Encode, int &maxSupportedWidthForH265Encode, int &maxSupportedHeightForH265Encode, 
                                    int &minSupportedWidthForH265Decode, int &minSupportedHeightForH265Decode, int &maxSupportedWidthForH265Decode, int &maxSupportedHeightForH265Decode, 
                                    int64_t &maxEncodeBitrateBpsForH264, int64_t &maxEncodeBitrateBpsForH265, int &supportedMaxTemporalLayersForH264, int &supportedMaxTemporalLayersForH265)
{
    isSupportH264Encode = false;
    isSupportH265Encode = false;
    isSupportH264Decode = false;
    isSupportH265Decode = false;

    supportedMaxInstanceForH264Encode = -1;
    supportedMaxInstanceForH264Decode = -1;
    supportedMaxInstanceForH265Encode = -1;
    supportedMaxInstanceForH265Decode = -1;

    minSupportedWidthForH264Encode = -1;
    minSupportedHeightForH264Encode = -1;
    maxSupportedWidthForH264Encode = -1;
    maxSupportedHeightForH264Encode = -1;

    minSupportedWidthForH264Decode = -1;
    minSupportedHeightForH264Decode = -1;
    maxSupportedWidthForH264Decode = -1;
    maxSupportedHeightForH264Decode = -1;

    minSupportedWidthForH265Encode = -1;
    minSupportedHeightForH265Encode = -1;
    maxSupportedWidthForH265Encode = -1;
    maxSupportedHeightForH265Encode = -1;

    minSupportedWidthForH265Decode = -1;
    minSupportedHeightForH265Decode = -1;
    maxSupportedWidthForH265Decode = -1;
    maxSupportedHeightForH265Decode = -1;

    maxEncodeBitrateBpsForH264 = -1;
    maxEncodeBitrateBpsForH265 = -1;

    supportedMaxTemporalLayersForH264 = -1;
    supportedMaxTemporalLayersForH265 = -1;

    AMF_RESULT res = AMF_OK;
    res = g_AMFFactory.Init();
    if(res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "AMF Failed to initialize";
        return false;
    }

    g_AMFFactory.GetDebug()->AssertsEnable(false);

    amf::AMFContextPtr pContext;
    res = g_AMFFactory.GetFactory()->CreateContext(&pContext);
    if(res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "AMF Failed to CreateContext";
        pContext.Release();
        g_AMFFactory.Terminate();
        return false;
    }
    
    OSVERSIONINFO osvi;
    memset(&osvi, 0, sizeof(osvi));
    osvi.dwOSVersionInfoSize = sizeof(osvi);
    GetVersionEx(&osvi);

    if (osvi.dwMajorVersion < 6)
    {
        RTC_LOG(LS_ERROR) << "This version of Windows is too old";
        pContext->Terminate();
        pContext.Release();
        g_AMFFactory.Terminate();
        return false;
    }

    bool deviceInit = false;
    if (osvi.dwMinorVersion >= 2) //  Win 8 or Win Server 2012 or newer
    {
        amf::AMFContext2Ptr pContext2(pContext);
        if (pContext2 != nullptr)
        {
            res = pContext2->InitDX12(NULL);
            if (res == AMF_OK)
            {
                deviceInit = true;
            }
        }

        if (!deviceInit)
        {
            deviceInit = (pContext->InitDX11(NULL) == AMF_OK);
        }
    }
    
    if (!deviceInit)
    {
        deviceInit = (pContext->InitDX9(NULL) == AMF_OK);
    }

    if (deviceInit)
    {
        int64_t v;
        isSupportH264Encode = isAMDSupportH26XCodec(AMFVideoEncoderVCE_AVC, pContext, true, true, supportedMaxInstanceForH264Encode, minSupportedWidthForH264Encode, minSupportedHeightForH264Encode, maxSupportedWidthForH264Encode, maxSupportedHeightForH264Encode, maxEncodeBitrateBpsForH264);
        isSupportH265Encode = isAMDSupportH26XCodec(AMFVideoEncoder_HEVC, pContext, true, false, supportedMaxInstanceForH265Encode, minSupportedWidthForH265Encode, minSupportedHeightForH265Encode, maxSupportedWidthForH265Encode, maxSupportedHeightForH265Encode, maxEncodeBitrateBpsForH265);
        isSupportH264Decode = isAMDSupportH26XCodec(AMFVideoDecoderUVD_H264_AVC, pContext, false, true, supportedMaxInstanceForH264Decode, minSupportedWidthForH264Decode, minSupportedHeightForH264Decode, maxSupportedWidthForH264Decode, maxSupportedHeightForH264Decode, v);
        isSupportH265Decode = isAMDSupportH26XCodec(AMFVideoDecoderHW_H265_HEVC, pContext, false, false, supportedMaxInstanceForH265Decode, minSupportedWidthForH265Decode, minSupportedHeightForH265Decode, maxSupportedWidthForH265Decode, maxSupportedHeightForH265Decode, v);
        getSVCCapability(AMFVideoEncoderVCE_SVC, pContext, true, true, supportedMaxTemporalLayersForH264);
        getSVCCapability(AMFVideoEncoder_HEVC, pContext, true, false, supportedMaxTemporalLayersForH265);
    }

    pContext->Terminate();
    pContext.Release();
    g_AMFFactory.Terminate();

    return true;
}

HWVideoCodecCapability GetAMDVideoCodecCapability(bool isEnableHWVideoCodec)
{
    HWVideoCodecCapability amdVideoCodecCapability;
    if (isEnableHWVideoCodec)
    {
        getAMDEncodeAndDecodeCapability(amdVideoCodecCapability.isSupportH264Encode, amdVideoCodecCapability.isSupportH265Encode, amdVideoCodecCapability.isSupportH264Decode, amdVideoCodecCapability.isSupportH265Decode, 
                                    amdVideoCodecCapability.supportedMaxInstanceForH264Encode, amdVideoCodecCapability.supportedMaxInstanceForH264Decode, amdVideoCodecCapability.supportedMaxInstanceForH265Encode, amdVideoCodecCapability.supportedMaxInstanceForH265Decode, 
                                    amdVideoCodecCapability.minSupportedWidthForH264Encode, amdVideoCodecCapability.minSupportedHeightForH264Encode, amdVideoCodecCapability.maxSupportedWidthForH264Encode, amdVideoCodecCapability.maxSupportedHeightForH264Encode, 
                                    amdVideoCodecCapability.minSupportedWidthForH264Decode, amdVideoCodecCapability.minSupportedHeightForH264Decode, amdVideoCodecCapability.maxSupportedWidthForH264Decode, amdVideoCodecCapability.maxSupportedHeightForH264Decode, 
                                    amdVideoCodecCapability.minSupportedWidthForH265Encode, amdVideoCodecCapability.minSupportedHeightForH265Encode, amdVideoCodecCapability.maxSupportedWidthForH265Encode, amdVideoCodecCapability.maxSupportedHeightForH265Encode, 
                                    amdVideoCodecCapability.minSupportedWidthForH265Decode, amdVideoCodecCapability.minSupportedHeightForH265Decode, amdVideoCodecCapability.maxSupportedWidthForH265Decode, amdVideoCodecCapability.maxSupportedHeightForH265Decode, 
                                    amdVideoCodecCapability.maxEncodeBitrateBpsForH264, amdVideoCodecCapability.maxEncodeBitrateBpsForH265, amdVideoCodecCapability.supportedMaxTemporalLayersForH264, amdVideoCodecCapability.supportedMaxTemporalLayersForH265);
    }else {
        amdVideoCodecCapability.isSupportH264Encode = false;
        amdVideoCodecCapability.isSupportH264Decode = false;
        amdVideoCodecCapability.isSupportH265Encode = false;
        amdVideoCodecCapability.isSupportH265Decode = false;
    }

    return amdVideoCodecCapability;
}

AMDVideoCodecCapability &AMDVideoCodecCapability::GetInstance()
{
    static AMDVideoCodecCapability capability;
    return capability;
}

AMDVideoCodecCapability::AMDVideoCodecCapability()
{
    isEnableHWCodec = false;
    hwVideoCodecCapability = GetAMDVideoCodecCapability(isEnableHWCodec);

    isMonitorCodecInstance = false;
    usedMaxInstanceForH264Encode = 0;
    usedMaxInstanceForH264Decode = 0;
    usedMaxInstanceForH265Encode = 0;
    usedMaxInstanceForH265Decode = 0;
}

AMDVideoCodecCapability::~AMDVideoCodecCapability()
{
}

HWVideoCodecCapability AMDVideoCodecCapability::GetHWVideoCodecCapability()
{
//    RTC_LOG(LS_INFO) << " isSupportH264Encode : " << hwVideoCodecCapability.isSupportH264Encode;
//    RTC_LOG(LS_INFO) << " isSupportH264Decode : " << hwVideoCodecCapability.isSupportH264Decode;
//    RTC_LOG(LS_INFO) << " isSupportH265Encode : " << hwVideoCodecCapability.isSupportH265Encode;
//    RTC_LOG(LS_INFO) << " isSupportH265Decode : " << hwVideoCodecCapability.isSupportH265Decode;

//    RTC_LOG(LS_INFO) << " supportedMaxInstanceForH264Encode : " << hwVideoCodecCapability.supportedMaxInstanceForH264Encode;
//    RTC_LOG(LS_INFO) << " supportedMaxInstanceForH264Decode : " << hwVideoCodecCapability.supportedMaxInstanceForH264Decode;
//    RTC_LOG(LS_INFO) << " supportedMaxInstanceForH265Encode : " << hwVideoCodecCapability.supportedMaxInstanceForH265Encode;
//    RTC_LOG(LS_INFO) << " supportedMaxInstanceForH265Decode : " << hwVideoCodecCapability.supportedMaxInstanceForH265Decode;

//    RTC_LOG(LS_INFO) << " minSupportedWidthForH264Encode : " << hwVideoCodecCapability.minSupportedWidthForH264Encode;
//    RTC_LOG(LS_INFO) << " minSupportedHeightForH264Encode : " << hwVideoCodecCapability.minSupportedHeightForH264Encode;
//    RTC_LOG(LS_INFO) << " maxSupportedWidthForH264Encode : " << hwVideoCodecCapability.maxSupportedWidthForH264Encode;
//    RTC_LOG(LS_INFO) << " maxSupportedHeightForH264Encode : " << hwVideoCodecCapability.maxSupportedHeightForH264Encode;

//    RTC_LOG(LS_INFO) << " minSupportedWidthForH264Decode : " << hwVideoCodecCapability.minSupportedWidthForH264Decode;
//    RTC_LOG(LS_INFO) << " minSupportedHeightForH264Decode : " << hwVideoCodecCapability.minSupportedHeightForH264Decode;
//    RTC_LOG(LS_INFO) << " maxSupportedWidthForH264Decode : " << hwVideoCodecCapability.maxSupportedWidthForH264Decode;
//    RTC_LOG(LS_INFO) << " maxSupportedHeightForH264Decode : " << hwVideoCodecCapability.maxSupportedHeightForH264Decode;

//    RTC_LOG(LS_INFO) << " minSupportedWidthForH265Encode : " << hwVideoCodecCapability.minSupportedWidthForH265Encode;
//    RTC_LOG(LS_INFO) << " minSupportedHeightForH265Encode : " << hwVideoCodecCapability.minSupportedHeightForH265Encode;
//    RTC_LOG(LS_INFO) << " maxSupportedWidthForH265Encode : " << hwVideoCodecCapability.maxSupportedWidthForH265Encode;
//    RTC_LOG(LS_INFO) << " maxSupportedHeightForH265Encode : " << hwVideoCodecCapability.maxSupportedHeightForH265Encode;

//    RTC_LOG(LS_INFO) << " minSupportedWidthForH265Decode : " << hwVideoCodecCapability.minSupportedWidthForH265Decode;
//    RTC_LOG(LS_INFO) << " minSupportedHeightForH265Decode : " << hwVideoCodecCapability.minSupportedHeightForH265Decode;
//    RTC_LOG(LS_INFO) << " maxSupportedWidthForH265Decode : " << hwVideoCodecCapability.maxSupportedWidthForH265Decode;
//    RTC_LOG(LS_INFO) << " maxSupportedHeightForH265Decode : " << hwVideoCodecCapability.maxSupportedHeightForH265Decode;

//    RTC_LOG(LS_INFO) << " maxEncodeBitrateBpsForH264 : " << hwVideoCodecCapability.maxEncodeBitrateBpsForH264;
//    RTC_LOG(LS_INFO) << " maxEncodeBitrateBpsForH265 : " << hwVideoCodecCapability.maxEncodeBitrateBpsForH265;

    return hwVideoCodecCapability;
}


bool AMDVideoCodecCapability::refCodecInstance(bool isEncoder, bool isH264)
{
    if (!isMonitorCodecInstance) return true;

    capLock.lock();
    if (isEncoder && isH264)
    {
        if (hwVideoCodecCapability.supportedMaxInstanceForH264Encode >= 0 && usedMaxInstanceForH264Encode + 1 > hwVideoCodecCapability.supportedMaxInstanceForH264Encode)
        {
            capLock.unlock();
            return false;
        }
        
        usedMaxInstanceForH264Encode++;
        capLock.unlock();
        return true;
    }

    if (isEncoder && !isH264)
    {
        if (hwVideoCodecCapability.supportedMaxInstanceForH265Encode >= 0 && usedMaxInstanceForH265Encode + 1 > hwVideoCodecCapability.supportedMaxInstanceForH265Encode)
        {
            capLock.unlock();
            return false;
        }
        
        usedMaxInstanceForH265Encode++;
        capLock.unlock();
        return true;
    }

    if (!isEncoder && isH264)
    {
        if (hwVideoCodecCapability.supportedMaxInstanceForH264Decode >=0 && usedMaxInstanceForH264Decode + 1 > hwVideoCodecCapability.supportedMaxInstanceForH264Decode)
        {
            capLock.unlock();
            return false;
        }
        
        usedMaxInstanceForH264Decode++;
        capLock.unlock();
        return true;
    }

    if (!isEncoder && !isH264)
    {
        if (hwVideoCodecCapability.supportedMaxInstanceForH265Decode >= 0 && usedMaxInstanceForH265Decode + 1 > hwVideoCodecCapability.supportedMaxInstanceForH265Decode)
        {
            capLock.unlock();
            return false;
        }
        
        usedMaxInstanceForH265Decode++;
        capLock.unlock();
        return true;
    }

    capLock.unlock();
    return false;
}

void AMDVideoCodecCapability::unrefCodecInstance(bool isEncoder, bool isH264)
{
    if (!isMonitorCodecInstance) return;

    capLock.lock();
    if (isEncoder && isH264)
    {
        usedMaxInstanceForH264Encode--;
    }

    if (isEncoder && !isH264)
    {
        usedMaxInstanceForH265Encode--;
    }

    if (!isEncoder && isH264)
    {
        usedMaxInstanceForH264Decode--;
    }

    if (!isEncoder && !isH264)
    {
        usedMaxInstanceForH265Decode--;
    }
    capLock.unlock();
}

} // namespace netease
