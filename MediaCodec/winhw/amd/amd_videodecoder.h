#pragma once

#include "modules/video_coding/codecs/winhw/libhwcodec.h"

#ifdef _WIN32
#include <tchar.h>
#include <d3d9.h>
#include <d3d11.h>
#endif
#include "third_party/amf/include/components/VideoDecoderUVD.h"
#include "third_party/amf/include/components/Component.h"
#include "modules/video_coding/codecs/winhw/amd/amf/common/AMFFactory.h"
#include "modules/video_coding/codecs/winhw/amd/amf/common/Thread.h"

namespace netease {

class AMDVideoDecoder: public netease::HWVideoDecoder
{
public:
	AMDVideoDecoder(AMF_STREAM_CODEC_ID_ENUM codecId);
	~AMDVideoDecoder();

	// implement VideoDecoder interface
	bool init();
	bool setConfig(const VideoDecoderConfig &config);
	bool beginDecode();
	bool addEncodedFrame(const VideoEncodedFrame &frame);
	bool getRawFrame(VideoRawFrame &frame);
	bool endDecode();
	VIDEO_STATNDARD getVideoStandard();
	VIDEO_CODEC_PROVIDER getCodecId();
	const char* getCodecFriendlyName();
	const std::string getCodecGPUName();
private:
    bool initDX12();
    bool initDX11();
    bool initDX9();
    bool inputEncodedFrame(const VideoEncodedFrame &frame);
    bool outputRawFrame(VideoRawFrame &frame);
private:
    AMF_STREAM_CODEC_ID_ENUM m_CodecId;
    VideoDecoderConfig m_config;
    amf::AMFContextPtr m_amfContext;
    amf::AMFComponentPtr m_amfDecoder;
    amf::AMF_SURFACE_FORMAT m_formatOut;
    amf::AMF_MEMORY_TYPE m_memoryTypeOut;
    bool m_isRefAMFFactory = false;
    bool m_bDecoderInited = false;
    VideoRawFrame m_outputRawFrame;
    bool m_isOutputRawFrameFilled;
    amf::AMFSurfacePtr m_ouputAMFSurface;
	std::string m_HWGPUName;
	std::string m_CodecFriendlyName;
};

CODEC_RETCODE CreateAMDVideoDecoder(VIDEO_CODEC_PROVIDER codec, HWVideoDecoder** ppVideoDecoder);

} // namespace netease