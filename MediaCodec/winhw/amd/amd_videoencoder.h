#pragma once

#include "modules/video_coding/codecs/winhw/libhwcodec.h"

#ifdef _WIN32
#include <tchar.h>
#include <d3d9.h>
#include <d3d11.h>
#endif
#include "third_party/amf/include/components/VideoEncoderVCE.h"
#include "third_party/amf/include/components/VideoEncoderHEVC.h"
#include "modules/video_coding/codecs/winhw/amd/amf/common/AMFFactory.h"
#include "modules/video_coding/codecs/winhw/amd/amf/common/Thread.h"
#include "common_video/include/bitrate_adjuster.h"

namespace netease {

class AMDVideoEncoder: public netease::HWVideoEncoder
{
public:
	AMDVideoEncoder(AMF_STREAM_CODEC_ID_ENUM codecId);
	~AMDVideoEncoder();

	// implement VideoEncoder interface
	bool init();
	bool setConfig(const VideoEncoderConfig &config);
	bool beginEncode();
	bool flushEncode();
	bool addRawFrame(const VideoRawFrame &frame);
	bool getEncodedFrame(VideoEncodedFrame &frame);
	bool endEncode();
	bool reset();
	bool resetBitrate(uint32_t bitrateBps);
	bool resetFramerate(uint32_t fps);
	void forceIntraFrame();
	void resetTemporalSVC(bool isEnableTemporalSVC, uint32_t numTemporalLayers);
	TemporalSVCCap getTemporalSVCCap();
	HWQPThreshold getHWQPThreshold();
	VIDEO_STATNDARD getVideoStandard();
	VIDEO_CODEC_PROVIDER getCodecId();
	const char* getCodecFriendlyName();
private:
    bool initDX12();
    bool initDX11();
    bool initDX9();
	bool inputRawFrame(const VideoRawFrame &frame);
	bool outputEncodedFrame(VideoEncodedFrame &frame);

	uint32_t ltrUseFrameIdxToLtrUseFrameBitmap(int ltrUseFrameIdx);
	int ltrUseFrameBitmapToLtrUseFrameIdx(uint32_t ltrUseFrameBitmap);
private:
    AMF_STREAM_CODEC_ID_ENUM m_CodecId;
	VideoEncoderConfig m_config;
    amf::AMFContextPtr m_amfContext;
    amf::AMFComponentPtr m_amfEncoder;
    amf::AMF_SURFACE_FORMAT m_formatIn;
    amf::AMF_MEMORY_TYPE m_memoryTypeIn;
    bool m_isRefAMFFactory = false;
    bool m_bEncoderInited = false;
	VideoEncodedFrame m_outputEncodedFrame;
	bool m_isOutputEncodedFrameFilled;
	amf::AMFBufferPtr m_encodedAMFBuffer;

	bool m_bForceIntraFrame;
	bool m_bResetBitrate;
	bool m_bResetFramerate;

	webrtc::BitrateAdjuster *m_pBitrateAdjuster;
	uint32_t m_encoderBitrateBps;
private:
    bool m_bIsEnableLTR;
    int m_ltrNumFrames;

	bool m_bResetTemporalSVC;
    TemporalSVCCap m_TemporalSVCCap;

	std::string m_HWGPUName;
	std::string m_CodecFriendlyName;
};

CODEC_RETCODE CreateAMDVideoEncoder(VIDEO_CODEC_PROVIDER codec, HWVideoEncoder** ppVideoEncoder);

} // namespace netease