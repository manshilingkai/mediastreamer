#pragma once
#include "modules/video_coding/codecs/winhw/libhwcodec_capability.h"
#include <mutex>

namespace netease {

class AMDVideoCodecCapability
{
public:
    static AMDVideoCodecCapability &GetInstance();
    HWVideoCodecCapability GetHWVideoCodecCapability();
    bool refCodecInstance(bool isEncoder, bool isH264);
    void unrefCodecInstance(bool isEncoder, bool isH264);
private:
    AMDVideoCodecCapability();
    ~AMDVideoCodecCapability();
    AMDVideoCodecCapability(const AMDVideoCodecCapability &capability);
    const AMDVideoCodecCapability &operator=(const AMDVideoCodecCapability &capability);
    
    HWVideoCodecCapability hwVideoCodecCapability;

    bool isMonitorCodecInstance;
    int usedMaxInstanceForH264Encode;
    int usedMaxInstanceForH264Decode;
    int usedMaxInstanceForH265Encode;
    int usedMaxInstanceForH265Decode;
    std::mutex capLock;

    bool isEnableHWCodec;
};

} // namespace netease