#include "modules/video_coding/codecs/winhw/amd/amd_videoencoder.h"
#include "rtc_base/logging.h"
#include "modules/video_coding/codecs/winhw/win/gpu_info.h"
#include "modules/video_coding/codecs/winhw/amd/amd_videocodec_capability.h"

#ifdef ENABLE_LIBYUV
#include "third_party/libyuv/include/libyuv/convert_from.h"
#endif

namespace netease {

AMDVideoEncoder::AMDVideoEncoder(AMF_STREAM_CODEC_ID_ENUM codecId)
{
    m_CodecId = codecId;

    m_amfContext = NULL;
    m_amfEncoder = NULL;
    
    m_isRefAMFFactory = false;
    m_bEncoderInited = false;

    m_isOutputEncodedFrameFilled = false;
    m_encodedAMFBuffer = NULL;

    m_bForceIntraFrame = false;
	m_bResetBitrate = false;
    m_bResetFramerate = false;

    // int32_t supported_max_encode_bitrate_bps = -1;
    // if (m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC) {
    //     supported_max_encode_bitrate_bps = AMDVideoCodecCapability::GetInstance().GetHWVideoCodecCapability().maxEncodeBitrateBpsForH265;
    // }else {
    //     supported_max_encode_bitrate_bps = AMDVideoCodecCapability::GetInstance().GetHWVideoCodecCapability().maxEncodeBitrateBpsForH264;
    // }
    
    m_pBitrateAdjuster = NULL;
    m_encoderBitrateBps = 0;

    m_bIsEnableLTR = false;
    m_ltrNumFrames = 0;
	m_HWGPUName = "AMD";
    std::vector<GpuInfo>& available_gpuInfos = GPUInfoHandler::GetInstance().GetAvailableGpuInfos();
	for (GpuInfo gpuInfo : available_gpuInfos) {
        if (gpuInfo.provider == GPU_AMD) {
            m_HWGPUName = gpuInfo.name;
        }
    }

    if (m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC)
    {
        m_CodecFriendlyName = "H265 Encoder [" + m_HWGPUName + "]";
    }else {
        m_CodecFriendlyName = "H264 Encoder [" + m_HWGPUName + "]";
    }
}

AMDVideoEncoder::~AMDVideoEncoder()
{
    this->endEncode();

    if (m_amfContext)
    {
        m_amfContext->Terminate();
        m_amfContext = NULL;
    }

    if (m_isRefAMFFactory)
    {
        g_AMFFactory.Terminate();
        m_isRefAMFFactory = false;
    }

    if (m_pBitrateAdjuster != NULL) {
        delete m_pBitrateAdjuster;
        m_pBitrateAdjuster = NULL;
    }
}

bool AMDVideoEncoder::init()
{
    AMF_RESULT res = AMF_OK;
    res = g_AMFFactory.Init();

    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "AMF Failed to initialize, ErrorCode: " << res;
        return false;
    }

    m_isRefAMFFactory = true;

    res = g_AMFFactory.GetFactory()->CreateContext(&m_amfContext);
    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "AMF Failed to CreateContext, ErrorCode: " << res;
        return false;
    }

    m_memoryTypeIn = amf::AMF_MEMORY_UNKNOWN;
    bool ret = initDX12();
    if (!ret) ret = initDX11();
    if (!ret) ret = initDX9();
    
    return ret;
}

bool AMDVideoEncoder::initDX12()
{
    AMF_RESULT res = AMF_OK;
    amf::AMFContext2Ptr context2(m_amfContext);
    if(context2 == nullptr)
    {
        RTC_LOG(LS_ERROR) << "amf::AMFContext2 is missing";
        return false;
    }
    res = context2->InitDX12(NULL);
    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "amf::AMFContext2 InitDX12 Fail, ErrorCode: " << res;
        return false;
    }

    m_memoryTypeIn = amf::AMF_MEMORY_DX12;
    return true;
}

bool AMDVideoEncoder::initDX11()
{
    AMF_RESULT res = AMF_OK;
    res = m_amfContext->InitDX11(NULL);
    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "amf::AMFContext InitDX11 Fail, ErrorCode: " << res;
        return false;
    }

    m_memoryTypeIn = amf::AMF_MEMORY_DX11;
    return true;
}

bool AMDVideoEncoder::initDX9()
{
    AMF_RESULT res = AMF_OK;
    res = m_amfContext->InitDX9(NULL);
    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "amf::AMFContext InitDX9 Fail, ErrorCode: " << res;
        return false;
    }

    m_memoryTypeIn = amf::AMF_MEMORY_DX9;
    return true;
}

bool AMDVideoEncoder::setConfig(const VideoEncoderConfig &config)
{
    m_config = config;
    if (m_pBitrateAdjuster != NULL) {
        delete m_pBitrateAdjuster;
        m_pBitrateAdjuster = NULL;
    }

    HWQPThreshold hwQPThreshold = getHWQPThreshold();
    m_pBitrateAdjuster = new webrtc::BitrateAdjuster(config.minAdjustedBitratePct, config.maxAdjustedBitratePct, config.enableDynamicAdjustMaxBitratePct, 
                                                     hwQPThreshold.lowQpThreshold, hwQPThreshold.highQpThreshold, config.enableBitrateAdjuster);
    m_pBitrateAdjuster->SetTargetBitrateBps(m_config.avgBitrate);
    m_encoderBitrateBps = m_pBitrateAdjuster->GetAdjustedBitrateBps();

    return true;
}

bool AMDVideoEncoder::beginEncode()
{
    if (m_bEncoderInited) return true;

    AMF_RESULT res = AMF_OK;
    m_TemporalSVCCap.is_support_temporal_svc = false;
    if (m_CodecId == AMF_STREAM_CODEC_ID_H264_AVC)
    {
        if (m_config.isEnableTemporalSVC)
        {
            int supportedMaxTemporalLayers = -1;
            if (m_CodecId == AMF_STREAM_CODEC_ID_H264_AVC) {
                supportedMaxTemporalLayers = AMDVideoCodecCapability::GetInstance().GetHWVideoCodecCapability().supportedMaxTemporalLayersForH264;
            }else if (m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC) {
                supportedMaxTemporalLayers = AMDVideoCodecCapability::GetInstance().GetHWVideoCodecCapability().supportedMaxTemporalLayersForH265;
            }

            if (supportedMaxTemporalLayers <= 1) {
                res = g_AMFFactory.GetFactory()->CreateComponent(m_amfContext, AMFVideoEncoderVCE_AVC, &m_amfEncoder);
            }else {
                m_TemporalSVCCap.is_support_temporal_svc = true;
                m_TemporalSVCCap.supported_max_temporal_layers = supportedMaxTemporalLayers;
                m_TemporalSVCCap.is_support_dynamic_enable_temporal_svc = false;
                m_TemporalSVCCap.is_support_dynamic_set_bitrate_fraction = false;

                res = g_AMFFactory.GetFactory()->CreateComponent(m_amfContext, AMFVideoEncoderVCE_SVC, &m_amfEncoder);
            }
        }else {
            res = g_AMFFactory.GetFactory()->CreateComponent(m_amfContext, AMFVideoEncoderVCE_AVC, &m_amfEncoder);
        }
    }else if (m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC)
    {
        if (m_config.isEnableTemporalSVC) {
            RTC_LOG(LS_WARNING) << "not support svc for amd hevc";
        }
        res = g_AMFFactory.GetFactory()->CreateComponent(m_amfContext, AMFVideoEncoder_HEVC, &m_amfEncoder);
    }

    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "amf::CreateComponent Fail, ErrorCode: " << res;
        return false;
    }

    if (m_CodecId == AMF_STREAM_CODEC_ID_H264_AVC)
    {
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_USAGE, AMF_VIDEO_ENCODER_USAGE_ULTRA_LOW_LATENCY);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_USAGE) Fail, ErrorCode: " << res;
        }
        
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_B_PIC_PATTERN, m_config.numBFrame);
        // do not check error for AMF_VIDEO_ENCODER_B_PIC_PATTERN - can be not supported
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_QUALITY_PRESET, AMF_VIDEO_ENCODER_QUALITY_PRESET_SPEED);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_QUALITY_PRESET) Fail, ErrorCode: " << res;
        }
        // res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_LOWLATENCY_MODE, true);
        // if (res != AMF_OK)
        // {
        //     RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_LOWLATENCY_MODE) Fail, ErrorCode: " << res;
        // }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_TARGET_BITRATE, m_encoderBitrateBps);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_TARGET_BITRATE) Fail, ErrorCode: " << res;
            return false;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_PEAK_BITRATE, m_encoderBitrateBps);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_PEAK_BITRATE) Fail, ErrorCode: " << res;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_FRAMESIZE, ::AMFConstructSize(m_config.width, m_config.height));
        if (res != AMF_OK)
        {
            RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_FRAMESIZE) Fail, ErrorCode: " << res;
            return false;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_FRAMERATE, ::AMFConstructRate(m_config.fps, 1));
        if (res != AMF_OK)
        {
            RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_FRAMERATE) Fail, ErrorCode: " << res;
            return false;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_PROFILE, AMF_VIDEO_ENCODER_PROFILE_MAIN);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_PROFILE) Fail, ErrorCode: " << res;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_PROFILE_LEVEL, 31);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_PROFILE_LEVEL) Fail, ErrorCode: " << res;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_IDR_PERIOD, m_config.gop);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_IDR_PERIOD) Fail, ErrorCode: " << res;
            return false;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_RATE_CONTROL_METHOD, AMF_VIDEO_ENCODER_RATE_CONTROL_METHOD_CBR);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_RATE_CONTROL_METHOD) Fail, ErrorCode: " << res;
            return false;
        }
        m_bIsEnableLTR = m_config.isEnableLTR;
        if (m_config.isEnableLTR)
        {
            res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_MAX_LTR_FRAMES, m_config.defaultMaxLTRFrames);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_MAX_LTR_FRAMES) Fail, ErrorCode: " << res;
                m_ltrNumFrames = 0;
            }else {
                m_ltrNumFrames = m_config.defaultMaxLTRFrames;
            }
        }
        if (m_bIsEnableLTR && m_ltrNumFrames > 0)
        {
            res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_LTR_MODE, AMF_VIDEO_ENCODER_LTR_MODE_RESET_UNUSED);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_LTR_MODE) Fail, ErrorCode: " << res;
            }
        }
        if (m_config.isEnableTemporalSVC && m_TemporalSVCCap.is_support_temporal_svc)
        {
            res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_NUM_TEMPORAL_ENHANCMENT_LAYERS, std::min(m_config.numTemporalLayers, m_TemporalSVCCap.supported_max_temporal_layers)-1);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_NUM_TEMPORAL_ENHANCMENT_LAYERS) Fail, ErrorCode: " << res;
            }
        }
    }else if(m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC)
    {
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_USAGE, AMF_VIDEO_ENCODER_HEVC_USAGE_ULTRA_LOW_LATENCY);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_USAGE) Fail, ErrorCode: " << res;
        }
        
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_QUALITY_PRESET, AMF_VIDEO_ENCODER_HEVC_QUALITY_PRESET_SPEED);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_QUALITY_PRESET) Fail, ErrorCode: " << res;
        }
        // res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_LOWLATENCY_MODE, true);
        // if (res != AMF_OK)
        // {
        //     RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_LOWLATENCY_MODE) Fail, ErrorCode: " << res;
        // }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_TARGET_BITRATE, m_encoderBitrateBps);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_TARGET_BITRATE) Fail, ErrorCode: " << res;
            return false;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_PEAK_BITRATE, m_encoderBitrateBps);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_PEAK_BITRATE) Fail, ErrorCode: " << res;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_FRAMESIZE, ::AMFConstructSize(m_config.width, m_config.height));
        if (res != AMF_OK)
        {
            RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_FRAMESIZE) Fail, ErrorCode: " << res;
            return false;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_FRAMERATE, ::AMFConstructRate(m_config.fps, 1));
        if (res != AMF_OK)
        {
            RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_FRAMERATE) Fail, ErrorCode: " << res;
            return false;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_TIER, AMF_VIDEO_ENCODER_HEVC_TIER_HIGH);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_TIER) Fail, ErrorCode: " << res;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_PROFILE_LEVEL, AMF_LEVEL_5_1);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_PROFILE_LEVEL) Fail, ErrorCode: " << res;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_GOP_SIZE, m_config.gop);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_IDR_PERIOD) Fail, ErrorCode: " << res;
            return false;
        }
        res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_RATE_CONTROL_METHOD, AMF_VIDEO_ENCODER_HEVC_RATE_CONTROL_METHOD_CBR);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_RATE_CONTROL_METHOD) Fail, ErrorCode: " << res;
            return false;
        }

        m_bIsEnableLTR = m_config.isEnableLTR;
        if (m_config.isEnableLTR)
        {
            res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_MAX_LTR_FRAMES, m_config.defaultMaxLTRFrames);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_MAX_LTR_FRAMES) Fail, ErrorCode: " << res;
                m_ltrNumFrames = 0;
            }else {
                m_ltrNumFrames = m_config.defaultMaxLTRFrames;
            }
        }
        if (m_bIsEnableLTR && m_ltrNumFrames > 0)
        {
            res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_LTR_MODE, AMF_VIDEO_ENCODER_HEVC_LTR_MODE_RESET_UNUSED);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_LTR_MODE) Fail, ErrorCode: " << res;
            }
        }
    }

    m_formatIn = amf::AMF_SURFACE_NV12;
    res = m_amfEncoder->Init(m_formatIn, m_config.width, m_config.height);
    if (res != AMF_OK)
    {
        RTC_LOG(LS_ERROR) << "amf::AMFComponent Init Fail With amf::AMF_SURFACE_NV12, ErrorCode: " << res;
        return false;
    }else {
        RTC_LOG(LS_INFO) << "amf::AMFComponent Init Success With amf::AMF_SURFACE_NV12";
    }

    RTC_LOG(LS_INFO) << "AMDVideoEncoder beginEncode success";

    m_isOutputEncodedFrameFilled = false;

    m_bEncoderInited = true;

    return true;
}

bool AMDVideoEncoder::flushEncode()
{
    m_amfEncoder->Drain();
    m_amfEncoder->Flush();

    return true;
}

uint32_t AMDVideoEncoder::ltrUseFrameIdxToLtrUseFrameBitmap(int ltrUseFrameIdx)
{
    uint32_t bitmap = 0x00000001;
    if (ltrUseFrameIdx < 0 || ltrUseFrameIdx > m_ltrNumFrames-1) return 0;
    else if (ltrUseFrameIdx==0) return bitmap;
    else {
        bitmap = bitmap << ltrUseFrameIdx;
        return bitmap;
    }
}

int AMDVideoEncoder::ltrUseFrameBitmapToLtrUseFrameIdx(uint32_t ltrUseFrameBitmap)
{
    int ltrUseFrameIdx = -1;
    while (ltrUseFrameBitmap)
    {
        ltrUseFrameBitmap >>= 1;
        ltrUseFrameIdx++;
    }
    
    if (ltrUseFrameIdx>=m_ltrNumFrames)
    {
        return -1;
    }
    
    return ltrUseFrameIdx;
}

bool AMDVideoEncoder::addRawFrame(const VideoRawFrame &frame)
{
    m_isOutputEncodedFrameFilled = false;
    bool ret = this->inputRawFrame(frame);
    if (!ret) return false;
    else {
        ret = outputEncodedFrame(m_outputEncodedFrame);
        if (!ret) return false;
    }
    return true;
}

bool AMDVideoEncoder::inputRawFrame(const VideoRawFrame &frame)
{
    if (frame.isEnableLTR != m_bIsEnableLTR)
    {
        m_config.isEnableLTR = frame.isEnableLTR;
        this->reset();
    }

    AMF_RESULT res = AMF_OK;
    amf::AMFSurfacePtr inputAMFSurface = NULL;
    res = m_amfContext->AllocSurface(amf::AMF_MEMORY_HOST, m_formatIn, m_config.width, m_config.height, &inputAMFSurface);
    if (res != AMF_OK || inputAMFSurface == NULL)
    {
        RTC_LOG(LS_ERROR) << "amf::AMFContext AllocSurface Fail, ErrorCode: " << res;
        return false;
    }

    if (frame.format == FOURCC_NV12)
    {
	    size_t planeCount = inputAMFSurface->GetPlanesCount();
	    for (uint8_t i = 0; i < planeCount; i++) {
		    amf::AMFPlanePtr plane  = inputAMFSurface->GetPlaneAt(i);
		    // int32_t          width  = plane->GetWidth();
		    int32_t          height = plane->GetHeight();
		    int32_t          hpitch = plane->GetHPitch();

			void* plane_nat = plane->GetNative();
			for (int32_t py = 0; py < height; py++) {
				int32_t plane_off = py * hpitch;
                int32_t frame_off = py * frame.strides[i];
				std::memcpy(static_cast<void*>(static_cast<uint8_t*>(plane_nat) + plane_off),
							static_cast<void*>(frame.planeptrs[i].ptr + frame_off), frame.strides[i]);
			}
	    }
    }else if (frame.format == FOURCC_I420)
    {
        /*I420 to NV12*/
#ifdef ENABLE_LIBYUV
        const uint8_t* src_y = frame.planeptrs[0].ptr;
        int src_stride_y = frame.strides[0];
        const uint8_t* src_u = frame.planeptrs[1].ptr;
        int src_stride_u = frame.strides[1];
        const uint8_t* src_v = frame.planeptrs[2].ptr;
        int src_stride_v = frame.strides[2];

		amf::AMFPlanePtr dst_y_plane  = inputAMFSurface->GetPlaneAt(0);
        uint8_t* dst_y = (uint8_t *)(dst_y_plane->GetNative());
        int dst_stride_y = dst_y_plane->GetHPitch();
        amf::AMFPlanePtr dst_uv_plane  = inputAMFSurface->GetPlaneAt(1);
        uint8_t* dst_uv = (uint8_t *)(dst_uv_plane->GetNative());
        int dst_stride_uv = dst_uv_plane->GetHPitch();
        int width = frame.width;
        int height = frame.height;

        libyuv::I420ToNV12(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, dst_y, dst_stride_y, dst_uv, dst_stride_uv, width, height);
#else
        uint8_t *srcPtr, *dstPtr;
	    uint16_t dstPitch;
	    uint16_t w = frame.width;
	    uint16_t h = frame.height;
	    uint32_t i = 0;

		// load Y
		amf::AMFPlanePtr dst_y_plane  = inputAMFSurface->GetPlaneAt(0);
		srcPtr = frame.planeptrs[0].ptr;
		dstPtr = (uint8_t *)(dst_y_plane->GetNative());
        dstPitch = dst_y_plane->GetHPitch();
		for (i = 0; i < h; i++) {
			memcpy(dstPtr, srcPtr, w);
			srcPtr += frame.strides[0];
			dstPtr += dstPitch;
		}
		uint8_t buf[2048]; // maximum supported chroma width for nv12
		uint32_t j = 0;
		w /= 2;
		h /= 2;
		if (w > 2048) {
			RTC_LOG(LS_ERROR) << "[AMDVideoEncoder] width=" << w << " is too big.";
			return false;
		}
		
		// load U
    	amf::AMFPlanePtr dst_uv_plane  = inputAMFSurface->GetPlaneAt(1);
		srcPtr = frame.planeptrs[1].ptr;
		dstPtr = (uint8_t *)(dst_uv_plane->GetNative());
        dstPitch = dst_uv_plane->GetHPitch();
		for (i = 0; i < h; i++)
		{
			memcpy(buf, srcPtr, w);
			srcPtr += frame.strides[1];
			for (j = 0; j < w; j++)
			{
				dstPtr[i * dstPitch + j * 2] = buf[j];
			}
		}
		// load V
		srcPtr = frame.planeptrs[2].ptr;
		for (i = 0; i < h; i++)
		{
			memcpy(buf, srcPtr, w);
			srcPtr += frame.strides[2];
			for (j = 0; j < w; j++)
			{
				dstPtr[i * dstPitch + j * 2 + 1] = buf[j];
			}
		}
#endif
    }

    res = inputAMFSurface->Convert(m_memoryTypeIn);
	if (res != AMF_OK) {
        RTC_LOG(LS_ERROR) << "amf::AMFSurface Convert Fail, ErrorCode: " << res;
		return false;
	}

    inputAMFSurface->SetPts(frame.timestamp);

    if (m_bResetBitrate)
    {
        m_bResetBitrate = false;
        m_encoderBitrateBps = m_pBitrateAdjuster->GetAdjustedBitrateBps();
        if (m_CodecId == AMF_STREAM_CODEC_ID_H264_AVC) {
            res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_TARGET_BITRATE, m_encoderBitrateBps);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_TARGET_BITRATE) Fail, ErrorCode: " << res;
                return false;
            }
            res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_PEAK_BITRATE,m_encoderBitrateBps);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_PEAK_BITRATE) Fail, ErrorCode: " << res;
            }
        }else if(m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC) {
            res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_TARGET_BITRATE, m_encoderBitrateBps);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_TARGET_BITRATE) Fail, ErrorCode: " << res;
                return false;
            }
            res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_PEAK_BITRATE, m_encoderBitrateBps);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_PEAK_BITRATE) Fail, ErrorCode: " << res;
            }
        }
    }

    if (m_bResetFramerate)
    {
        m_bResetFramerate = false;
        if (m_CodecId == AMF_STREAM_CODEC_ID_H264_AVC)
        {
            res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_FRAMERATE, ::AMFConstructRate(m_config.fps, 1));
            if (res != AMF_OK)
            {
                RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_FRAMERATE) Fail, ErrorCode: " << res;
            }
        }else if (m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC)
        {
            res = m_amfEncoder->SetProperty(AMF_VIDEO_ENCODER_HEVC_FRAMERATE, ::AMFConstructRate(m_config.fps, 1));
            if (res != AMF_OK)
            {
                RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_FRAMERATE) Fail, ErrorCode: " << res;
            }
        }
    }
    
    if (m_bForceIntraFrame)
    {
        m_bForceIntraFrame = false;
        if (m_CodecId == AMF_STREAM_CODEC_ID_H264_AVC) {
            res = inputAMFSurface->SetProperty(AMF_VIDEO_ENCODER_FORCE_PICTURE_TYPE, AMF_VIDEO_ENCODER_PICTURE_TYPE_IDR);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_FORCE_PICTURE_TYPE) Fail, ErrorCode: " << res;
                return false;
            }
            res = inputAMFSurface->SetProperty(AMF_VIDEO_ENCODER_INSERT_SPS, true);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_INSERT_SPS) Fail, ErrorCode: " << res;
            }
            res = inputAMFSurface->SetProperty(AMF_VIDEO_ENCODER_INSERT_PPS, true);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_INSERT_PPS) Fail, ErrorCode: " << res;
            }
        }else if(m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC) {
            res = inputAMFSurface->SetProperty(AMF_VIDEO_ENCODER_HEVC_FORCE_PICTURE_TYPE, AMF_VIDEO_ENCODER_HEVC_PICTURE_TYPE_IDR);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_FORCE_PICTURE_TYPE) Fail, ErrorCode: " << res;
                return false;
            }
            res = inputAMFSurface->SetProperty(AMF_VIDEO_ENCODER_HEVC_INSERT_HEADER, true);
            if (res != AMF_OK)
            {
                RTC_LOG(LS_ERROR) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_INSERT_HEADER) Fail, ErrorCode: " << res;
            }
        }
    }

    if (m_bIsEnableLTR)
    {
        if (m_CodecId == AMF_STREAM_CODEC_ID_H264_AVC) {
            if (frame.ltrMarkFrame)
            {
                res = inputAMFSurface->SetProperty(AMF_VIDEO_ENCODER_MARK_CURRENT_WITH_LTR_INDEX, frame.ltrMarkFrameIdx);
                if (res != AMF_OK)
                {
                    RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_MARK_CURRENT_WITH_LTR_INDEX) Fail, ErrorCode: " << res;
                }
            }

            if (frame.ltrUseFrame)
            {
                res = inputAMFSurface->SetProperty(AMF_VIDEO_ENCODER_FORCE_LTR_REFERENCE_BITFIELD, ltrUseFrameIdxToLtrUseFrameBitmap(frame.ltrUseFrameIdx));
                if (res != AMF_OK)
                {
                    RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_FORCE_LTR_REFERENCE_BITFIELD) Fail, ErrorCode: " << res;
                }
            }
            
        }else if(m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC) {
            if (frame.ltrMarkFrame)
            {
                res = inputAMFSurface->SetProperty(AMF_VIDEO_ENCODER_HEVC_MARK_CURRENT_WITH_LTR_INDEX, frame.ltrMarkFrameIdx);
                if (res != AMF_OK)
                {
                    RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_MARK_CURRENT_WITH_LTR_INDEX) Fail, ErrorCode: " << res;
                }
            }

            if (frame.ltrUseFrame)
            {
                res = inputAMFSurface->SetProperty(AMF_VIDEO_ENCODER_HEVC_FORCE_LTR_REFERENCE_BITFIELD, ltrUseFrameIdxToLtrUseFrameBitmap(frame.ltrUseFrameIdx));
                if (res != AMF_OK)
                {
                    RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_FORCE_LTR_REFERENCE_BITFIELD) Fail, ErrorCode: " << res;
                }
            }
        }
    }

    if (m_CodecId == AMF_STREAM_CODEC_ID_H264_AVC) {
        res = inputAMFSurface->SetProperty(AMF_VIDEO_ENCODER_STATISTICS_FEEDBACK, true);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_STATISTICS_FEEDBACK) Fail, ErrorCode: " << res;
        }
    }else if(m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC) {
        res = inputAMFSurface->SetProperty(AMF_VIDEO_ENCODER_HEVC_STATISTICS_FEEDBACK, true);
        if (res != AMF_OK)
        {
            RTC_LOG(LS_WARNING) << "amf::SetProperty(AMF_VIDEO_ENCODER_HEVC_STATISTICS_FEEDBACK) Fail, ErrorCode: " << res;
        }
    }
    res = m_amfEncoder->SubmitInput(inputAMFSurface);
    if (res == AMF_OK) return true;
    
    if (res == AMF_NEED_MORE_INPUT)
    {
        RTC_LOG(LS_WARNING) << "amf::AMFComponent SubmitInput Success, But Need More Input";
        return true;
    }else if(res == AMF_INPUT_FULL)
    {
        RTC_LOG(LS_WARNING) << "amf::AMFComponent SubmitInput Fail, Input Queue is Full, Drop One Raw Frame";
        return true;
    }else
    {
        RTC_LOG(LS_ERROR) << "amf::AMFComponent SubmitInput Fail, ErrorCode: " << res;
        return false;
    }
}

bool AMDVideoEncoder::outputEncodedFrame(VideoEncodedFrame &frame)
{
    AMF_RESULT res = AMF_OK;
    while(true)
    {
        if (m_encodedAMFBuffer)
        {
            m_encodedAMFBuffer.Release();
            m_encodedAMFBuffer = NULL;
        }

        amf::AMFDataPtr encodedAMFData = NULL;
        res = m_amfEncoder->QueryOutput(&encodedAMFData);
        if(res==AMF_OK && encodedAMFData!=NULL)
        {
            amf::AMFBufferPtr encodedAMFBuffer(encodedAMFData);

            frame.frameData.ptr = reinterpret_cast<uint8_t*> (encodedAMFBuffer->GetNative());
            frame.frameSize = encodedAMFBuffer->GetSize();
            frame.pts = encodedAMFBuffer->GetPts();

            if (m_CodecId == AMF_STREAM_CODEC_ID_H264_AVC)
            {
                uint64_t pktType = AMF_VIDEO_ENCODER_OUTPUT_DATA_TYPE_P;
	            AMF_RESULT pt_res = encodedAMFData->GetProperty(AMF_VIDEO_ENCODER_OUTPUT_DATA_TYPE, &pktType);
                if (pt_res == AMF_OK)
                {
                    if (pktType == AMF_VIDEO_ENCODER_OUTPUT_DATA_TYPE_IDR)
                    {
                        frame.frameType = IDR_Frame;
                    }else if(pktType == AMF_VIDEO_ENCODER_OUTPUT_DATA_TYPE_I)
                    {
                        frame.frameType = I_Frame;
                    }else if(pktType == AMF_VIDEO_ENCODER_OUTPUT_DATA_TYPE_P)
                    {
                        frame.frameType = P_Frame;
                    }else if(pktType == AMF_VIDEO_ENCODER_OUTPUT_DATA_TYPE_B)
                    {
                        frame.frameType = B_Frame;
                    }
                }

                if (m_bIsEnableLTR) {
                    uint32_t ltrFrameIdx = -1;
                    AMF_RESULT ltr_res = encodedAMFData->GetProperty(AMF_VIDEO_ENCODER_OUTPUT_MARKED_LTR_INDEX, &ltrFrameIdx);
                    if (ltr_res == AMF_OK && ltrFrameIdx >= 0)
                    {
                        frame.isLTRFrame = true;
                        frame.ltrFrameIdx = ltrFrameIdx;
                    }
                    uint32_t ltrUseFrameBitmap = 0;
                    ltr_res = encodedAMFData->GetProperty(AMF_VIDEO_ENCODER_OUTPUT_REFERENCED_LTR_INDEX_BITFIELD, &ltrUseFrameBitmap);
                    if (ltr_res == AMF_OK)
                    {
                        frame.ltrUseFrameIdx = ltrUseFrameBitmapToLtrUseFrameIdx(ltrUseFrameBitmap);
                    }
                }

                uint32_t qp = 0;
                AMF_RESULT qp_res = encodedAMFData->GetProperty(AMF_VIDEO_ENCODER_STATISTIC_AVERAGE_QP, &qp);
                if (qp_res == AMF_OK)
                {
                    frame.qp = qp;
                    //RTC_LOG(LS_INFO) << "avc qp : " << qp;
                }else {
                    frame.qp = 0;
                    RTC_LOG(LS_WARNING) << "amf::GetProperty(AMF_VIDEO_ENCODER_STATISTIC_AVERAGE_QP) Fail, ErrorCode: " << qp_res;
                }
                
            }else if(m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC)
            {
                uint64_t pktType = AMF_VIDEO_ENCODER_HEVC_OUTPUT_DATA_TYPE_P;
	            AMF_RESULT pt_res = encodedAMFData->GetProperty(AMF_VIDEO_ENCODER_HEVC_OUTPUT_DATA_TYPE, &pktType);
                if (pt_res == AMF_OK)
                {
                    if (pktType == AMF_VIDEO_ENCODER_HEVC_OUTPUT_DATA_TYPE_IDR)
                    {
                        frame.frameType = IDR_Frame;
                    }else if(pktType == AMF_VIDEO_ENCODER_HEVC_OUTPUT_DATA_TYPE_I)
                    {
                        frame.frameType = I_Frame;
                    }else if(pktType == AMF_VIDEO_ENCODER_HEVC_OUTPUT_DATA_TYPE_P)
                    {
                        frame.frameType = P_Frame;
                    }else
                    {
                        frame.frameType = B_Frame;
                    }
                }

                if (m_bIsEnableLTR) {
                    uint32_t ltrFrameIdx = -1;
                    AMF_RESULT ltr_res = encodedAMFData->GetProperty(AMF_VIDEO_ENCODER_HEVC_OUTPUT_MARKED_LTR_INDEX, &ltrFrameIdx);
                    if (ltr_res == AMF_OK && ltrFrameIdx >= 0)
                    {
                        frame.isLTRFrame = true;
                        frame.ltrFrameIdx = ltrFrameIdx;
                    }
                    uint32_t ltrUseFrameBitmap = 0;
                    ltr_res = encodedAMFData->GetProperty(AMF_VIDEO_ENCODER_HEVC_OUTPUT_REFERENCED_LTR_INDEX_BITFIELD, &ltrUseFrameBitmap);
                    if (ltr_res == AMF_OK)
                    {
                        frame.ltrUseFrameIdx = ltrUseFrameBitmapToLtrUseFrameIdx(ltrUseFrameBitmap);
                    }
                }

                uint32_t qp = 0;
                AMF_RESULT qp_res = encodedAMFData->GetProperty(AMF_VIDEO_ENCODER_HEVC_STATISTIC_AVERAGE_QP, &qp);
                if (qp_res == AMF_OK)
                {
                    frame.qp = qp;
                    //RTC_LOG(LS_INFO) << "hevc qp : " << qp;
                }else {
                    frame.qp = 0;
                    RTC_LOG(LS_WARNING) << "amf::GetProperty(AMF_VIDEO_ENCODER_HEVC_STATISTIC_AVERAGE_QP) Fail, ErrorCode: " << qp_res;
                }
            }

            m_encodedAMFBuffer = encodedAMFBuffer;

            m_isOutputEncodedFrameFilled = true;
            return true;
        }else if (res == AMF_REPEAT)
        {
            continue;
        }else {
            RTC_LOG(LS_ERROR) << "amf::AMFComponent QueryOutput Fail, ErrorCode: " << res;
            return false;
        }
    }
}

bool AMDVideoEncoder::getEncodedFrame(VideoEncodedFrame &frame)
{
    if (m_isOutputEncodedFrameFilled)
    {
        frame.frameType = m_outputEncodedFrame.frameType;
        frame.qp = m_outputEncodedFrame.qp;
        frame.pts = m_outputEncodedFrame.pts;
        frame.frameSize = m_outputEncodedFrame.frameSize;
        frame.frameData.ptr = m_outputEncodedFrame.frameData.ptr;
        frame.adjustedEncBitrateBps = m_pBitrateAdjuster->GetAdjustedBitrateBps();

        m_pBitrateAdjuster->ReportQP(frame.qp);
        m_pBitrateAdjuster->Update(frame.frameSize);

        m_isOutputEncodedFrameFilled = false;

        return true;
    }else {
        return false;
    }
}

bool AMDVideoEncoder::endEncode()
{
    if (!m_bEncoderInited) return true;

    this->flushEncode();

    if (m_encodedAMFBuffer)
    {
        m_encodedAMFBuffer.Release();
        m_encodedAMFBuffer = NULL;
    }

    m_amfEncoder->Terminate();
    m_amfEncoder = NULL;

    m_bEncoderInited = false;

    RTC_LOG(LS_INFO) << "AMDVideoEncoder endEncode success";

    return true;
}

bool AMDVideoEncoder::reset()
{
    this->endEncode();
    return this->beginEncode();
}

bool AMDVideoEncoder::resetBitrate(uint32_t bitrateBps)
{
    m_pBitrateAdjuster->SetTargetBitrateBps(bitrateBps);

    if (m_encoderBitrateBps != m_pBitrateAdjuster->GetAdjustedBitrateBps())
	{
		m_bResetBitrate = true;
	}

	return true;
}

bool AMDVideoEncoder::resetFramerate(uint32_t fps)
{
	if (m_config.fps != fps)
	{
		m_config.fps = fps;
		m_bResetFramerate = true;
	}

	return true;
}

void AMDVideoEncoder::forceIntraFrame()
{
    m_bForceIntraFrame = true;
}

void AMDVideoEncoder::resetTemporalSVC(bool isEnableTemporalSVC, uint32_t numTemporalLayers)
{
    if (isEnableTemporalSVC != m_config.isEnableTemporalSVC || numTemporalLayers != m_config.numTemporalLayers)
    {
        m_config.isEnableTemporalSVC = isEnableTemporalSVC;
        m_config.numTemporalLayers = numTemporalLayers;

        m_bResetTemporalSVC = true;
    }
}

TemporalSVCCap AMDVideoEncoder::getTemporalSVCCap()
{
    return m_TemporalSVCCap;
}

HWQPThreshold AMDVideoEncoder::getHWQPThreshold()
{
    HWQPThreshold hwQPThreshold;

    if (m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC) {
        hwQPThreshold.lowQpThreshold = 24;
        hwQPThreshold.highQpThreshold = 37;
    }else {
        hwQPThreshold.lowQpThreshold = 24;
        hwQPThreshold.highQpThreshold = 37;
    }

	return hwQPThreshold;
}

VIDEO_STATNDARD AMDVideoEncoder::getVideoStandard()
{
	if (m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC)
		return VideoStandard_ITU_H265;
	else
		return VideoStandard_ITU_H264;
}

VIDEO_CODEC_PROVIDER AMDVideoEncoder::getCodecId()
{
	if (m_CodecId == AMF_STREAM_CODEC_ID_H265_HEVC)
		return HW_CODEC_AMD_HEVC;
	else
		return HW_CODEC_AMD;
}

const char* AMDVideoEncoder::getCodecFriendlyName()
{
    return m_CodecFriendlyName.c_str();
}

CODEC_RETCODE CreateAMDVideoEncoder(VIDEO_CODEC_PROVIDER codec, HWVideoEncoder** ppVideoEncoder)
{
    RTC_LOG(LS_INFO) << "CODEC_RETCODE CreateAMDVideoEncoder";

	AMF_STREAM_CODEC_ID_ENUM codecId;
	if (codec == HW_CODEC_AMD){
		codecId = AMF_STREAM_CODEC_ID_H264_AVC;
		RTC_LOG(LS_INFO) << "[AMDVideoEncoder] H264";
	}
	else if (codec == HW_CODEC_AMD_HEVC){
		codecId = AMF_STREAM_CODEC_ID_H265_HEVC;
		RTC_LOG(LS_INFO) << "[AMDVideoEncoder] HEVC";
	}
	else
		return CODEC_ERR_UNSUPPORTED;

	AMDVideoEncoder *encoder = new AMDVideoEncoder(codecId);
	if (encoder && encoder->init()) {
		*ppVideoEncoder = encoder;
		return CODEC_OK; // init succeeded
	}

	delete encoder;
	return CODEC_ERR_UNKNOWN;
}

} // namespace netease