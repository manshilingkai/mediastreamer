#pragma once

#include <stdint.h>
#include <string.h>
#include <tchar.h>
#include <iostream>
#include "common_video/include/i420_buffer_pool.h"

#ifndef ENABLE_LIBYUV
#define ENABLE_LIBYUV
#endif

namespace netease {

const int kDefaultHWCodecVideoMaxFramerate = 60;

const int AUTO_BR_ADJUSTER = -1;
const int BASE_BR_ADJUSTER = 0;
const int DYNAMIC_BR_ADJUSTER = 1;

enum VIDEO_CODEC_PROVIDER
{
	HW_CODEC_UNSUPPORTED = 0,
	HW_CODEC_NVIDIA = 1,
	HW_CODEC_NVIDIA_HEVC = 2,
	HW_CODEC_INTEL_QUICKSYNC = 3,
	HW_CODEC_INTEL_QUICKSYNC_HEVC = 4,
	HW_CODEC_AMD = 5,
	HW_CODEC_AMD_HEVC = 6,

	HW_CODEC_AUTOSELECT_H264 = 100,
	HW_CODEC_AUTOSELECT_HEVC = 101,
};

enum VIDEO_STATNDARD
{
	VideoStandard_UNKNOW,
	VideoStandard_ITU_H264,
	VideoStandard_ITU_H265,
};

enum HW_CODEC_PICTURE_FLAG
{
	FLAG_EOS = 1,
	FLAG_KEY_FRAME,
};

enum ENCODED_FRAME_TYPE
{
	ERR_Frame = -1,
	I_Frame = 0,
	P_Frame = 1,
	B_Frame = 2,
	IDR_Frame = 3,
};

// we set color format values according to FOURCC : http://www.fourcc.org/yuv.php
// this format currently supported
enum FOURCC_ColorFormat
{
	FOURCC_NULL = 0x00000000,
	FOURCC_I420 = 0x30323449,
	FOURCC_NV12 = 0x3231564E,
	FOURCC_AYUV = 0x56555941,
	FOURCC_I444 = FOURCC_AYUV+1,
};

typedef union {
	uint8_t *ptr;
	uint64_t pos;
	uint64_t num;
} x86_64_ptr_t;

struct VideoRawFrame
{
	VideoRawFrame()
	{
		memset(this, 0, sizeof(VideoRawFrame));
		format = FOURCC_NULL;
		frame_buffer = nullptr;
	}

	bool isEOS() const
	{
		return (flags & FLAG_EOS) != 0;
	}

	bool isKeyFrame() const
	{
		return (flags & FLAG_KEY_FRAME) != 0;
	}

	FOURCC_ColorFormat format;
	uint32_t width;
	uint32_t height;
	x86_64_ptr_t planeptrs[4];
	uint32_t strides[4];
	// flags(such as EOS)
	uint32_t flags;
	int64_t timestamp;
	uint32_t frameOrder;

    int	ROI_Regions;//max 10 regions
    int ROI_Loc[40];//4 values for each region x,y,right,bottom

	bool isEnableLTR;
	bool ltrMarkFrame;
	bool ltrUseFrame;
	int ltrMarkFrameIdx;
	int ltrUseFrameIdx;
	
	rtc::scoped_refptr<webrtc::I420Buffer> frame_buffer;
};

struct VideoEncodedFrame
{
	VideoEncodedFrame()
	{
		memset(this, 0, sizeof(VideoEncodedFrame));
		sid = 0xff;
		tid = 0xff;

		isLTRFrame = false;
		ltrFrameIdx = 0;
		ltrUseFrameIdx = -1; //Value of -1 if no LTR frames were used
	}

	void Release()
	{
		if (frameData.ptr) {
			delete[] frameData.ptr;
			frameData.ptr = NULL;
		}
		frameSize = 0;
	}

	x86_64_ptr_t frameData; // data pointer
	uint32_t frameSize; // encoded length
	ENCODED_FRAME_TYPE frameType;
	VIDEO_STATNDARD standard;
	int64_t dts;
	int64_t pts;
	int32_t sid; // for SVC encode 
	int32_t tid; // for SVC encode 
	int32_t nalRefIdc;
	int32_t nalType;
	int32_t qp;

	bool isLTRFrame;
    uint32_t ltrFrameIdx;
    uint32_t ltrUseFrameIdx;

	uint32_t temporalId;

	int32_t adjustedEncBitrateBps;
};

struct VideoDecoderConfig
{
	VideoDecoderConfig()
	{
		memset(this, 0, sizeof(*this));
	}

	uint32_t width;
	uint32_t height;
	uint32_t fps;
};

struct VideoEncoderConfig
{
	VideoEncoderConfig()
	{
		memset(this, 0, sizeof(*this));
        this->pixFormat = FOURCC_I420;
		this->defaultMaxLTRFrames = 32;
		this->numTemporalLayers = 2;
		this->enableBitrateAdjuster = false;
		this->minAdjustedBitratePct = 0.5f;
		this->maxAdjustedBitratePct = 1.0f;
		this->enableDynamicAdjustMaxBitratePct = true;
	}

	enum tagRcMode{
		RC_CQP = 0,
		RC_VBR,
		RC_CBR,
		RC_VBR_MINQP,
		RC_2PASS_IMG,
		RC_2PASS,
		RC_2PASS_VBR,
		RC_CRF,
	};

    FOURCC_ColorFormat pixFormat;
	uint32_t width;
	uint32_t height;
	uint32_t fps;
	uint32_t gop; // the interval of IDR(key) frames
	uint32_t numBFrame; // max consecutive B frames
	uint32_t rcMode; // rate control mode (cbr recommended) 
					// 0=fixed QP, 1=VBR, 2=CBR, 3=VBR_MINQP, 
					// 4=Multi pass encoding optimized for image quality, 
					// 5=Multi pass encoding optimized for maintaining frame size,
					// 6=Multi pass VBR
	uint32_t avgBitrate; // average bitrate
	uint32_t maxBitrate; // maximum bitrate
	uint32_t minBitrate; // minimum bitrate
	uint32_t outputPkgMode; // 0 : annex b, 1 : mp4
	char configString[1024];// option strings to config more parameters.

	int  qp;
    int  rf;  // in CRF mode, Ratefactor constant: targets a certain constant "quality"
	int	 thread_count;

	bool isEnableLTR;
	int defaultMaxLTRFrames;

	bool isEnableTemporalSVC;
	int numTemporalLayers;

	// HardWare Encoder Bitrate Adjuster Parameter
	bool enableBitrateAdjuster;
	float minAdjustedBitratePct;
    float maxAdjustedBitratePct;
    bool enableDynamicAdjustMaxBitratePct;
};

struct TemporalSVCCap {
	TemporalSVCCap()
	{
		is_support_temporal_svc = false;
		supported_max_temporal_layers = 2;
		is_support_dynamic_enable_temporal_svc = false;
		is_support_dynamic_set_bitrate_fraction = false;
		base_vs_enhance_bitrate_fraction_default = 0.6;
	}

    bool is_support_temporal_svc;
    int32_t supported_max_temporal_layers;
    bool is_support_dynamic_enable_temporal_svc;
    bool is_support_dynamic_set_bitrate_fraction;
    float base_vs_enhance_bitrate_fraction_default;// 5:5 = 0.5； 6:4 = 0.6； 7:3 = 0.7； 8:2 = 0.8； 9:1 = 0.9
};

static const int kLowH26XQpThreshold = 24;
static const int kHighH26XQpThreshold = 37;

struct HWQPThreshold
{
	int lowQpThreshold;
	int highQpThreshold;

	HWQPThreshold()
	{
		lowQpThreshold = kLowH26XQpThreshold;
		highQpThreshold = kHighH26XQpThreshold;
	}
};

static const char * const vidformat_names[] = { "component", "pal", "ntsc", "secam", "mac", "undef", 0 };
static const char * const fullrange_names[] = { "off", "on", 0 };
static const char * const colorprim_names[] = { "", "bt709", "undef", "", "bt470m", "bt470bg",
"smpte170m", "smpte240m", "film", "bt2020", 0 };
static const char * const transfer_names[] = { "", "bt709", "undef", "", "bt470m", "bt470bg", 
"smpte170m", "smpte240m", "linear", "log100", "log316",
"iec61966-2-4", "bt1361e", "iec61966-2-1", "bt2020-10", "bt2020-12", 0 };
static const char * const colmatrix_names[] = { "GBR", "bt709", "undef", "", "fcc", "bt470bg", 
"smpte170m", "smpte240m", "YCgCo", "bt2020nc", "bt2020c", 0 };
static const char * const directmode_names[]  = {"auto", "disable", "spatial", "temporal", 0};

//////////////////////////////////////////////////////////////////////////
// the main video encoder interface
class HWVideoEncoder
{
public:
	virtual ~HWVideoEncoder() {}

	virtual bool init() = 0;
	virtual bool setConfig(const VideoEncoderConfig &config) = 0;
	virtual bool beginEncode() = 0;
	virtual bool flushEncode() = 0;
	virtual bool addRawFrame(const VideoRawFrame &frame) = 0;
	virtual bool getEncodedFrame(VideoEncodedFrame &frame) = 0;
	virtual bool endEncode() = 0;
	virtual bool reset() = 0;
	virtual bool resetBitrate(uint32_t bitrateBps) = 0;
	virtual bool resetFramerate(uint32_t fps) = 0;
	virtual void forceIntraFrame() = 0;
	virtual void resetTemporalSVC(bool isEnableTemporalSVC, uint32_t numTemporalLayers) = 0;
	virtual TemporalSVCCap getTemporalSVCCap() = 0;
	virtual HWQPThreshold getHWQPThreshold() = 0;
	virtual VIDEO_STATNDARD getVideoStandard() = 0;
	virtual VIDEO_CODEC_PROVIDER getCodecId() = 0;
	virtual const char* getCodecFriendlyName() = 0;
};

//////////////////////////////////////////////////////////////////////////
// the main video decoder interface
class HWVideoDecoder
{
public:
    virtual ~HWVideoDecoder() {}

	virtual bool init() = 0;
	virtual bool setConfig(const VideoDecoderConfig &config) = 0;
	virtual bool beginDecode() = 0;
	virtual bool addEncodedFrame(const VideoEncodedFrame &frame) = 0;
	virtual bool getRawFrame(VideoRawFrame &frame) = 0;
	virtual bool endDecode() = 0;
	virtual VIDEO_STATNDARD getVideoStandard() = 0;
	virtual VIDEO_CODEC_PROVIDER getCodecId() = 0;
	virtual const char* getCodecFriendlyName() = 0;
    virtual const std::string getCodecGPUName() = 0;
};

enum CODEC_RETCODE
{
	CODEC_OK = 0,
	CODEC_ERR_UNSUPPORTED,
	CODEC_ERR_UNKNOWN,
	CODEC_LOAD_LIBRARY_ERROR,
	CODEC_CREATE_SESSION_FAILED,
	CODEC_ERR_HW_LACKOFABILITY = 100,
	CODEC_ERR_HW_DRIVER_OUTOFDATE = 110
};

void GetResolutionFromString(std::string& s, int& width, int& height);

CODEC_RETCODE CreateHWVideoEncoder(
    VIDEO_CODEC_PROVIDER *pCodec,
    HWVideoEncoder ** ppVideoEncoder, int videoEncodeWidth, int videoEncodeHeight, int hwH26XEncListStrategy = 0, std::string hwH26XEncAdaptationList="", bool isEnableTemporalSVC = false, int numTemporalLayers = 2);
void ReleaseHWVideoEncoder(VIDEO_CODEC_PROVIDER codec, HWVideoEncoder * pVideoEncoder);

CODEC_RETCODE CreateHWVideoDecoder(
    VIDEO_CODEC_PROVIDER *pCodec,
    HWVideoDecoder ** ppVideoDecoder, int videoDecodeWidth, int videoDecodeHeight, int hwH26XDecListStrategy = 0, std::string hwH26XDecAdaptationList= "");
void ReleaseHWVideoDecoder(VIDEO_CODEC_PROVIDER codec, HWVideoDecoder * pVideoDecoder);

} // namespace netease