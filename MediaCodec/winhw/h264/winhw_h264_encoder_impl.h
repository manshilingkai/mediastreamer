/*
 *  Copyright (c) 2021 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 *
 */

#ifndef MODULES_VIDEO_CODING_CODECS_WIN_HW_H264_WIN_H264_ENCODER_IMPL_H_
#define MODULES_VIDEO_CODING_CODECS_WIN_HW_H264_WIN_H264_ENCODER_IMPL_H_

#include <memory>
#include <vector>

#include "api/video/i420_buffer.h"
#include "common_video/h264/h264_bitstream_parser.h"
#include "modules/video_coding/codecs/winhw/h264/include/winhwh264.h"
#include "modules/video_coding/utility/quality_scaler.h"
#include "modules/video_coding/codecs/winhw/h264/winhw_h264_encoder.h"
#include "media/base/videoadapter.h"
#include "system_wrappers/include/clock.h"
#include "media/engine/engine_parameter_collection.h"

namespace webrtc {

class WinHwH264EncoderImpl : public WinHwH264Encoder {
 public:
  struct LayerConfig {
    int simulcast_idx = 0;
    int width = -1;
    int height = -1;
    bool sending = true;
    bool key_frame_request = false;
    float max_frame_rate = 0;
    uint32_t target_bps = 0;
    uint32_t max_bps = 0;
    bool frame_dropping_on = false;
    int key_frame_interval = 0;
    uint16_t forwardflag = 0x0000;

    void SetStreamState(bool send_stream);
  };

 public:
  explicit WinHwH264EncoderImpl(const cricket::VideoCodec& codec);
  ~WinHwH264EncoderImpl() override;

  // |max_payload_size| is ignored.
  // The following members of |codec_settings| are used. The rest are ignored.
  // - codecType (must be kVideoCodecH264)
  // - targetBitrate
  // - maxFramerate
  // - width
  // - height
  int32_t InitEncode(const VideoCodec* codec_settings,
                     int32_t number_of_cores,
                     size_t max_payload_size) override;
  int32_t Release() override;

  int32_t RegisterEncodeCompleteCallback(
      EncodedImageCallback* callback) override;
  int32_t SetRateAllocation(VideoBitrateAllocation& bitrate_allocation,
                            uint32_t framerate) override;

  //For Dump&Replace
  int32_t GetRates(uint32_t* bitrate, uint32_t* framerate) override;
  int32_t SetRates(uint32_t bitrate, uint32_t framerate) override;
  int32_t SetSvcParam(const webrtc::SVCSpecificInfo* svc_specific_info) override;
  SVCCapabilitiesInfo GetSVCCapabilitiesInfo() const override;

  // The result of encoding - an EncodedImage and RTPFragmentationHeader - are
  // passed to the encode complete callback.
  int32_t Encode(const VideoFrame& frame,
                 const CodecSpecificInfo* codec_specific_info,
                 const std::vector<FrameType>* frame_types) override;

  EncoderInfo GetEncoderInfo() const override;

  // Exposed for testing.
  H264PacketizationMode PacketizationModeForTesting() const {
    return packetization_mode_;
  }

  void SetEncoderParam(EncoderEncodeParam param) override;

  bool UpdateLowStreamFramerate(int fps) override;

  bool UpdatePreferHwEncoder(int index, bool prefer) override;

  void SetSceneFramerateIn(int fps) override;
 private:
  WinHwH264EncoderParam CreateEncoderParams(size_t i) const;

  webrtc::H264BitstreamParser h264_bitstream_parser_;
  // Reports statistics with histograms.
  void ReportInit();
  void ReportError();

  std::vector<WindowsHwH264Encoder*> encoders_;
  std::vector<WinHwH264SourcePicture> pictures_;
  std::vector<rtc::scoped_refptr<I420Buffer>> downscaled_buffers_;
  std::vector<LayerConfig> configurations_;
  std::vector<EncodedImage> encoded_images_;
  std::vector<std::unique_ptr<uint8_t[]>> encoded_image_buffers_;

  VideoCodec codec_;
  H264PacketizationMode packetization_mode_;
  size_t max_payload_size_;
  int32_t number_of_cores_;
  EncodedImageCallback* encoded_image_callback_;

  bool has_reported_init_;
  bool has_reported_error_;
  cricket::VideoAdapter videoAdapter_;
  EncoderEncodeParam h264_encode_param_;

  absl::optional<int> low_stream_framerate_;
  int64_t last_time_;
  Clock* clock_;
  int screen_init_framerate_;

  uint32_t target_bitrate_for_replace_;
  uint32_t target_framerate_for_replace_;

private:
    webrtc::EngineParameterCollection* ne_parameters_ = nullptr;
    void SetHWH264EncodeAdaptation(std::string hwH264EncAdaptation);
    int hwH264EncListStrategy = 0;
    std::string hwH264EncAdaptationList = "";
private:
  void MakeSvcParam(CodecSpecificInfo& info, bool isKeyFrame);
  absl::optional<uint16_t> svc_last_base_frame_idx_;
};

}  // namespace webrtc

#endif
