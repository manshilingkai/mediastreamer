#include "modules/video_coding/codecs/winhw/h264/winhw_h264_encoder.h"
#include "rtc_base/logging.h"

#define WIN_HW_H264_ENCODE_MAX_NO_OUTPUT_RETRY_TIMES 20

namespace webrtc {

WindowsHwH264Encoder::WindowsHwH264Encoder() : isInitialized_(false),
  avcSeqId_(0),
  requestingAvcKeyrame_(false) {
    pVideoEncoder = nullptr;
    mVideoCodecProvider = netease::HW_CODEC_AUTOSELECT_H264;
    mNoOutputRetryTimes = 0;
}

WindowsHwH264Encoder::~WindowsHwH264Encoder() {
  Uninitialize();
}

int WindowsHwH264Encoder::Initialize(const WinHwH264EncoderParam &param) {
  if (isInitialized_)
    Uninitialize();
  if (!ConfigParam(param))
    return -1;

  CreateAvcEncoderParams();

  netease::CODEC_RETCODE enc_ret = netease::CreateHWVideoEncoder(&mVideoCodecProvider, &pVideoEncoder, videoEncoderParam.width, videoEncoderParam.height, param.hwH264EncListStrategy, param.hwH264EncAdaptationList, videoEncoderParam.isEnableTemporalSVC, videoEncoderParam.numTemporalLayers);
  if (enc_ret != netease::CODEC_OK )
  {
    pVideoEncoder = nullptr;
    RTC_LOG(LS_ERROR) << __FUNCTION__ << " failed to create win hw h264 encoder";
    return -1;
  }

  pVideoEncoder->setConfig(videoEncoderParam);
  bool b_ret = pVideoEncoder->beginEncode();
  if (!b_ret)
  {
    netease::ReleaseHWVideoEncoder(mVideoCodecProvider, pVideoEncoder);
    pVideoEncoder = nullptr;
    RTC_LOG(LS_ERROR) << __FUNCTION__ << " failed to start win hw h264 encoder";
    return -1;
  }

  frame_extra_infos_ = std::make_shared<std::deque<WinHwH264FrameExtraInfo>>();

  RTC_LOG(LS_INFO) << __FUNCTION__ << " win hw h264 encoder initialized. ";
  isInitialized_ = true;
  return 0;
}

void WindowsHwH264Encoder::Uninitialize() {
  if (isInitialized_) {
    if (pVideoEncoder) {
      netease::ReleaseHWVideoEncoder(mVideoCodecProvider, pVideoEncoder);
      pVideoEncoder = nullptr;
    }
    frame_extra_infos_.reset();
  }

  avcSeqId_ = 0;
  requestingAvcKeyrame_ = false;
  isInitialized_ = false;

  mNoOutputRetryTimes = 0;
}

int WindowsHwH264Encoder::EncodeFrame(const WinHwH264SourcePicture* pSrcPic, WinHwH264FrameBitStream* pBs) {
  if (!pSrcPic || !pBs || !isInitialized_)
    return -1;
  InitFrameBitStream(pBs);
  return EncodeFrameAvc(pSrcPic, pBs);
}

int WindowsHwH264Encoder::ForceIntraFrame() {
  if (!isInitialized_)
    return -1;

  requestingAvcKeyrame_ = true;
  return 0;
}

int WindowsHwH264Encoder::SetOption(WinHwH264ReconfigOption option, void* value) {
  if (!value || !isInitialized_)
    return -1;

  switch(option) {
  case WinHwH264ReconfigOption::kBitrate: {
      config_.iTargetBps = *((int32_t *)value);
      videoEncoderParam.avgBitrate = config_.iTargetBps;
      if (pVideoEncoder) {
        pVideoEncoder->resetBitrate(videoEncoderParam.avgBitrate);
      }
      
      RTC_LOG(LS_INFO) << "reconig win hw h264 encoder, bitrate: " << config_.iTargetBps;
      return 0;
  }
  case WinHwH264ReconfigOption::kFrameRate: {
      config_.fMaxFps = *((float *)value);
      int fps = config_.fMaxFps;
      videoEncoderParam.fps = fps < netease::kDefaultHWCodecVideoMaxFramerate ? fps : netease::kDefaultHWCodecVideoMaxFramerate;
      if (pVideoEncoder)
      {
        pVideoEncoder->resetFramerate(videoEncoderParam.fps);
      }

      RTC_LOG(LS_INFO) << "reconig win hw h264 encoder, fps: " << config_.fMaxFps;
      return 0;
  }
  default:
    return -1;
  }
}

int WindowsHwH264Encoder::SetTemporalSVC(bool isEnableTemporalSVC, uint32_t numTemporalLayers)
{
  if (!isInitialized_ || pVideoEncoder==NULL)
    return -1;

  pVideoEncoder->resetTemporalSVC(isEnableTemporalSVC, numTemporalLayers);
  
  return 0;
}

WinHwH264TemporalSVCCap WindowsHwH264Encoder::GetTemporalSVCCap()
{
  WinHwH264TemporalSVCCap winHwH264TemporalSVCCap;
  if (pVideoEncoder)
  {
    netease::TemporalSVCCap temporalSVCCap = pVideoEncoder->getTemporalSVCCap();
    winHwH264TemporalSVCCap.is_support_temporal_svc = temporalSVCCap.is_support_temporal_svc;
    winHwH264TemporalSVCCap.supported_max_temporal_layers = temporalSVCCap.supported_max_temporal_layers;
    winHwH264TemporalSVCCap.is_support_dynamic_enable_temporal_svc = temporalSVCCap.is_support_dynamic_enable_temporal_svc;
    winHwH264TemporalSVCCap.is_support_dynamic_set_bitrate_fraction = temporalSVCCap.is_support_dynamic_set_bitrate_fraction;
    winHwH264TemporalSVCCap.base_vs_enhance_bitrate_fraction_default = temporalSVCCap.base_vs_enhance_bitrate_fraction_default;
  }
  
  return winHwH264TemporalSVCCap;
}

bool WindowsHwH264Encoder::ConfigParam(const WinHwH264EncoderParam &param) {
  if (param.iTemporalLayerNum < 1
      || param.iTemporalLayerNum > 4
      || param.fMaxFps > 120
      || param.iHeight * param.iWidth < 16 * 16
      || param.iHeight * param.iWidth > 3840 * 2160
      || param.iTargetBps > param.iMaxBps
      || param.iMaxSliceSize < 0) {
    RTC_LOG(LS_ERROR) << __FUNCTION__ << " invalid param";
    return false;
  }

  config_ = param;
  return true;
}

void WindowsHwH264Encoder::CreateAvcEncoderParams() {
  videoEncoderParam.width = config_.iWidth;
  videoEncoderParam.height = config_.iHeight;
  int fps = config_.fMaxFps;
  videoEncoderParam.fps = fps < netease::kDefaultHWCodecVideoMaxFramerate ? fps : netease::kDefaultHWCodecVideoMaxFramerate;
  videoEncoderParam.avgBitrate = config_.iTargetBps;
  videoEncoderParam.gop = config_.iGop;
  videoEncoderParam.numBFrame = 0;
  videoEncoderParam.rcMode = netease::VideoEncoderConfig::RC_CBR;

  if (config_.bitrateAdjusterType == netease::BASE_BR_ADJUSTER) {
    videoEncoderParam.enableBitrateAdjuster = false;
  }else if (config_.bitrateAdjusterType == netease::DYNAMIC_BR_ADJUSTER) {
    videoEncoderParam.enableBitrateAdjuster = true;
  }else {
    videoEncoderParam.enableBitrateAdjuster = false;
  }

  RTC_LOG(LS_INFO) << "isScreenShare : " << config_.isScreenShare << " enableBitrateAdjuster : " << videoEncoderParam.enableBitrateAdjuster;

  float minAdjustedBitratePct = 0.5f;
  float maxAdjustedBitratePct = 1.0f;
  bool enable_dynamic_adjust_max_bitrate_pct = true;

  if (config_.minAdjustedBitratePct > 0) {
    minAdjustedBitratePct = config_.minAdjustedBitratePct;
  }
  if (config_.maxAdjustedBitratePct > 0) {
    maxAdjustedBitratePct = config_.maxAdjustedBitratePct;
    enable_dynamic_adjust_max_bitrate_pct = false;
  }

  videoEncoderParam.minAdjustedBitratePct = minAdjustedBitratePct;
  videoEncoderParam.maxAdjustedBitratePct = maxAdjustedBitratePct;
  videoEncoderParam.enableDynamicAdjustMaxBitratePct = enable_dynamic_adjust_max_bitrate_pct;

  videoEncoderParam.isEnableTemporalSVC = config_.bEnableTemporalSVC;
  videoEncoderParam.numTemporalLayers = config_.iTemporalLayerNum;
}

int WindowsHwH264Encoder::EncodeFrameAvc(const WinHwH264SourcePicture* pSrcPic, WinHwH264FrameBitStream* pBs)
{
  netease::VideoRawFrame rawFrame;
  rawFrame.width = pSrcPic->iPicWidth;
  rawFrame.height = pSrcPic->iPicHeight;
  if (pSrcPic->eColorFormat == webrtc::WinHwH264ColorFormat::kI420)
  {
    rawFrame.format = netease::FOURCC_I420;
    rawFrame.planeptrs[0].ptr = pSrcPic->pData[0];
    rawFrame.planeptrs[1].ptr = pSrcPic->pData[1];
    rawFrame.planeptrs[2].ptr = pSrcPic->pData[2];
    rawFrame.strides[0] = pSrcPic->iStride[0];
    rawFrame.strides[1] = pSrcPic->iStride[1];
    rawFrame.strides[2] = pSrcPic->iStride[2];
  }else if (pSrcPic->eColorFormat == webrtc::WinHwH264ColorFormat::kNV12)
  {
    rawFrame.format = netease::FOURCC_NV12;
    rawFrame.planeptrs[0].ptr = pSrcPic->pData[0];
    rawFrame.planeptrs[1].ptr = pSrcPic->pData[1];
    rawFrame.strides[0] = pSrcPic->iStride[0];
    rawFrame.strides[1] = pSrcPic->iStride[1];
  }
  rawFrame.timestamp = pSrcPic->capture_time_ms;

  if (requestingAvcKeyrame_)
  {
    requestingAvcKeyrame_ = false;
    pVideoEncoder->forceIntraFrame();
  }
  
  bool ret = pVideoEncoder->addRawFrame(rawFrame);
  if (!ret)
  {
    RTC_LOG(LS_ERROR) << __FUNCTION__ << " failed to add raw frame";
    return -1;
  }

  WinHwH264FrameExtraInfo inFrameExtraInfo;
  inFrameExtraInfo.timestamp = pSrcPic->timestamp;
  inFrameExtraInfo.ntp_time_ms = pSrcPic->ntp_time_ms;
  inFrameExtraInfo.capture_time_ms = pSrcPic->capture_time_ms;
  inFrameExtraInfo.rotation = pSrcPic->rotation;
  inFrameExtraInfo.cur_frame_idx = pSrcPic->cur_frame_idx;
  if(frame_extra_infos_.get() == nullptr) {
    RTC_LOG(LS_WARNING) << "WindowsHwH264Encoder FrameExtraInfo is null, new object.";
    frame_extra_infos_ = std::make_shared<std::deque<WinHwH264FrameExtraInfo>>();
  }
  frame_extra_infos_->push_back(inFrameExtraInfo);
  
  netease::VideoEncodedFrame encodedFrame;
  ret = pVideoEncoder->getEncodedFrame(encodedFrame);
  if (!ret)
  {
    mNoOutputRetryTimes++;
    if (mNoOutputRetryTimes > WIN_HW_H264_ENCODE_MAX_NO_OUTPUT_RETRY_TIMES)
    {
        RTC_LOG(LS_ERROR) << __FUNCTION__ << " no output encoded frame : exceeded maximum number of retries";
        return -7;
    }else {
        RTC_LOG(LS_WARNING) << __FUNCTION__ << " no output encoded frame";
        return 0;
    }
  }

  while (!frame_extra_infos_->empty() &&
          frame_extra_infos_->front().capture_time_ms < encodedFrame.pts) {
    frame_extra_infos_->pop_front();
  }

  if (frame_extra_infos_->empty() ||
      frame_extra_infos_->front().capture_time_ms != encodedFrame.pts) {
    RTC_LOG(LS_WARNING)
        << "WindowsHwH264Encoder produced an unexpected frame with timestamp: "
        << encodedFrame.pts;
  }
  WinHwH264FrameExtraInfo outFrameExtraInfo = std::move(frame_extra_infos_->front());
  frame_extra_infos_->pop_front();

  mNoOutputRetryTimes = 0;

  h264_bitstream_parser_.ParseBitstream(encodedFrame.frameData.ptr, encodedFrame.frameSize, true);
  if (encodedFrame.frameType == netease::IDR_Frame)
  {
    pBs->isKeyFrame = true;
  } else {
    pBs->isKeyFrame = false;
  }

  bool has_sps = false;
  bool has_pps = false;
  bool is_idr = h264_bitstream_parser_.isLastIdr(has_sps, has_pps);
  if (is_idr)
  {
    pBs->isKeyFrame = true;
  }
  
  std::vector<H264::NaluIndex> nalu_indices = h264_bitstream_parser_.GetLastNaluIndices();
  uint32_t nals_count = nalu_indices.size();

  uint32_t FrameSizeInBytes = 0;

  // -3 : remove sps&pps
  // -2 : remove pps
  // -1 : remove sps
  //  0 : no
  //  1 : insert sps
  //  2 : insert pps
  //  3 : insert sps&pps
  int flag = 0;
  if (is_idr)
  {
    if (has_sps && has_pps) {
      flag = 0;
    }else if (has_sps && !has_pps) {
      flag = 2;
    }else if (!has_sps && has_pps) {
      flag = 1;
    }else {
      flag = 3;
    }
  }else {
    if (has_sps && has_pps) {
      flag = -3;
    }else if (has_sps && !has_pps) {
      flag = -1;
    }else if (!has_sps && has_pps) {
      flag = -2;
    }else {
      flag = 0;
    }
  }

  if (flag == 3) {
    pBs->iNalCount = nals_count+2;
  }else if (flag == 2) {
    pBs->iNalCount = nals_count+1;
  }else if (flag == 1) {
    pBs->iNalCount = nals_count+1;
  }else if (flag == 0) {
    pBs->iNalCount = nals_count;
  }else if (flag == -1) {
    pBs->iNalCount = nals_count-1;
  }else if (flag == -2) {
    pBs->iNalCount = nals_count-1;
  }else if (flag == -3) {
    pBs->iNalCount = nals_count-2;
  }

  if (pBs->iNalCount <= 0)
  {
      RTC_LOG(LS_WARNING) << __FUNCTION__ << "redundance bitstream, ignore it";
      return 0;
  }
  
  pBs->iTemporalLayerId = 0;
  pBs->iSubSeqId = avcSeqId_++;
  pBs->uiTimeStamp = encodedFrame.pts;
    
  if (bsBuf_.size() < (size_t)pBs->iNalCount) {
    bsBuf_.resize((size_t)pBs->iNalCount);
    nalLens_.resize((size_t)pBs->iNalCount);
  }

  uint32_t read_start_idx = 0;
  uint32_t write_start_idx = 0;
  if (flag == 3) {
    size_t sps_size = 0;
    bsBuf_[0] = h264_bitstream_parser_.GetLastSps(sps_size);
    nalLens_[0] = sps_size;
    FrameSizeInBytes += nalLens_[0];

    size_t pps_size = 0;
    bsBuf_[1] = h264_bitstream_parser_.GetLastPps(pps_size);
    nalLens_[1] = pps_size;
    FrameSizeInBytes += nalLens_[1];

    RTC_LOG(LS_INFO) << "sps size : " << sps_size << " pps size : " << pps_size;

    if (bsBuf_[0] == NULL || nalLens_[0] == 0 || bsBuf_[1] == NULL || nalLens_[1] == 0)
    {
        RTC_LOG(LS_ERROR) << __FUNCTION__ << " no backup sps and pps to put into idr which has no sps and pps";
        return -4;
    }

    read_start_idx = 0;
    write_start_idx = 2;
  }else if (flag == 1) {
    size_t sps_size = 0;
    bsBuf_[0] = h264_bitstream_parser_.GetLastSps(sps_size);
    nalLens_[0] = sps_size;
    FrameSizeInBytes += nalLens_[0];

    RTC_LOG(LS_INFO) << " sps size : " << sps_size;

    if (bsBuf_[0] == NULL || nalLens_[0] == 0)
    {
        RTC_LOG(LS_ERROR) << __FUNCTION__ << " no backup sps to put into idr which has no sps";
        return -6;
    }
    read_start_idx = 0;
    write_start_idx = 1;
  }else if (flag == 0) {
    read_start_idx = 0;
    write_start_idx = 0;
  }else if (flag == -1) {
    read_start_idx = 1;
    write_start_idx = 0;
  }else if (flag == -2) {
    read_start_idx = 1;
    write_start_idx = 0;
  }else if (flag == -3) {
    read_start_idx = 2;
    write_start_idx = 0;
  }

  uint8_t* bitstream = encodedFrame.frameData.ptr;
  for (uint32_t i = read_start_idx; i < nals_count; ++i) {
    H264::NaluIndex index = nalu_indices[i];
    bsBuf_[write_start_idx+i] = &bitstream[index.start_offset];
    nalLens_[write_start_idx+i] = index.nalu_size;
    FrameSizeInBytes += nalLens_[write_start_idx+i];
  }

  pBs->iFrameSizeInBytes = FrameSizeInBytes;
  pBs->pNalLengthInByte = &nalLens_[0];
  pBs->ppBsBuf = &bsBuf_[0];

  int qp = 0;
  h264_bitstream_parser_.GetLastSliceQp(&qp);
  //RTC_LOG(LS_INFO) << __FUNCTION__ << " win hw h264 enc qp [from bitstream] : " << qp;

  if(encodedFrame.qp > 0) {
    qp = encodedFrame.qp;
  }

  pBs->iFrameQp = (int)qp;
  pBs->iTargetBit = config_.iTargetBps;
  pBs->iTotalBit = config_.iTargetBps;
  pBs->fQpAvgAq = (int)qp;
  pBs->iAvelineQp = (int)qp;
  pBs->frameCost = 0;
  pBs->iCntUnrefP = 0;

  pBs->adjustedEncBitrateBps = encodedFrame.adjustedEncBitrateBps;

  pBs->timestamp = outFrameExtraInfo.timestamp;
  pBs->ntp_time_ms = outFrameExtraInfo.ntp_time_ms;
  pBs->capture_time_ms = outFrameExtraInfo.capture_time_ms;
  pBs->rotation = outFrameExtraInfo.rotation;
  pBs->cur_frame_idx = outFrameExtraInfo.cur_frame_idx;
  absl::optional<SvcExtensionParser::SvcExtensionState> svc_extension = h264_bitstream_parser_.ParsedSvcExtension();
  if (svc_extension) {
    pBs->enable_svc = svc_extension->svc_extension_flag?true:false;
    if (svc_extension->svc_extension_flag) {
      pBs->temporal_id = svc_extension->temporal_id;
    }
  }else {
    pBs->enable_svc = false;
    pBs->temporal_id = 0;
  }
  
  return encodedFrame.frameSize;
}

void WindowsHwH264Encoder::InitFrameBitStream(WinHwH264FrameBitStream* pBs) const {
  if (!pBs)
    return;
  pBs->isKeyFrame = false;
  pBs->iFrameSizeInBytes = 0;
  pBs->iNalCount = 0;
  pBs->iSubSeqId = 0;
  pBs->iTemporalLayerId = 0;
  pBs->pNalLengthInByte = nullptr;
  pBs->ppBsBuf = nullptr;
  pBs->uiTimeStamp = 0;
}

const char* WindowsHwH264Encoder::getEncoderFriendlyName()
{
  if (pVideoEncoder) {
    pVideoEncoder->getCodecFriendlyName();
  }else {
    return "unknown";
  }
}

WinHwH264QPThreshold WindowsHwH264Encoder::getWinHwH264QPThreshold()
{
  WinHwH264QPThreshold winHwH264QPThreshold;
  if (pVideoEncoder) {
    netease::HWQPThreshold hwH264QPThreshold = pVideoEncoder->getHWQPThreshold();
    winHwH264QPThreshold.lowQpThreshold = hwH264QPThreshold.lowQpThreshold;
    winHwH264QPThreshold.highQpThreshold = hwH264QPThreshold.highQpThreshold;
  }
  return winHwH264QPThreshold;
}

}
