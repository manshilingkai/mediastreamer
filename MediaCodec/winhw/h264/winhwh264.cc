/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 *
 */

#include "modules/video_coding/codecs/winhw/h264/include/winhwh264.h"

#include "api/video_codecs/sdp_video_format.h"
#include "media/base/h264_profile_level_id.h"

#include "modules/video_coding/codecs/winhw/h264/winhw_h264_encoder_impl.h"
#include "modules/video_coding/codecs/winhw/h264/winhw_h264_decoder_impl.h"
#include "modules/video_coding/codecs/winhw/libhwcodec_capability.h"

#include "absl/memory/memory.h"
#include "rtc_base/checks.h"
#include "rtc_base/logging.h"

namespace webrtc {

namespace {

SdpVideoFormat CreateWinHwH264Format(H264::Profile profile,
                                H264::Level level,
                                const std::string& packetization_mode) {
  const absl::optional<std::string> profile_string =
      H264::ProfileLevelIdToString(H264::ProfileLevelId(profile, level));
  RTC_CHECK(profile_string);
  return SdpVideoFormat(
      cricket::kH264CodecName,
      {{cricket::kH264FmtpProfileLevelId, *profile_string},
       {cricket::kH264FmtpLevelAsymmetryAllowed, "1"},
       {cricket::kH264FmtpPacketizationMode, packetization_mode},
       {"support-svc", "0"},
       {"svc-max-layer-num", "2"}});
}

}  // namespace

std::vector<SdpVideoFormat> SupportedWinHwH264Codecs() {
  // We only support encoding Constrained Baseline Profile (CBP), but the
  // decoder supports more profiles. We can list all profiles here that are
  // supported by the decoder and that are also supersets of CBP, i.e. the
  // decoder for that profile is required to be able to decode CBP. This means
  // we can encode and send CBP even though we negotiated a potentially
  // higher profile. See the H264 spec for more information.
  //
  // We support both packetization modes 0 (mandatory) and 1 (optional,
  // preferred).
  return {
      CreateWinHwH264Format(H264::kProfileConstrainedBaseline, H264::kLevel3_1, "1"),
        };
}

std::unique_ptr<WinHwH264Encoder> WinHwH264Encoder::Create(
    const cricket::VideoCodec& codec) {
  if(!WinHwH264Encoder::IsSupported()) {
    return nullptr;
  }
  RTC_LOG(LS_INFO) << "Creating WinHwH264EncoderImpl.";
  return absl::make_unique<WinHwH264EncoderImpl>(codec);
}

bool WinHwH264Encoder::IsSupported()
{
  // netease::HWVideoCodecCapability hwVideoCodecCapability = netease::GetWinHWVideoCodecCapability();
  // return hwVideoCodecCapability.isSupportH264Encode;
  return true;
}

std::unique_ptr<WinHwH264Decoder> WinHwH264Decoder::Create() {
  if(!WinHwH264Decoder::IsSupported()) {
    return nullptr;
  }
  RTC_LOG(LS_INFO) << "Creating WinHwH264DecoderImpl.";
  return absl::make_unique<WinHwH264DecoderImpl>();
}

bool WinHwH264Decoder::IsSupported()
{
  // netease::HWVideoCodecCapability hwVideoCodecCapability = netease::GetWinHWVideoCodecCapability();
  // return hwVideoCodecCapability.isSupportH264Decode;
  return true;
}

}  // namespace webrtc
