#include "modules/video_coding/codecs/winhw/h264/winhw_h264_decoder_impl.h"

#include <algorithm>
#include <limits>

#include "api/video/color_space.h"
#include "api/video/i420_buffer.h"
#include "common_video/include/video_frame_buffer.h"
#include "rtc_base/checks.h"
#include "rtc_base/criticalsection.h"
#include "rtc_base/keep_ref_until_done.h"
#include "rtc_base/logging.h"
#include "system_wrappers/include/metrics.h"
#include "third_party/libyuv/include/libyuv/planar_functions.h"
#include "third_party/libyuv/include/libyuv/scale.h"
#include "modules/video_coding/codecs/winhw/libhwcodec_capability.h"
#include "rtc_base/timeutils.h"

#define WIN_HW_H264_DECODE_MAX_NO_OUTPUT_RETRY_TIMES 20

namespace webrtc {

namespace {

// Used by histograms. Values of entries should not be changed.
enum WinHwH264DecoderImplEvent {
  kWinHwH264DecoderEventInit = 0,
  kWinHwH264DecoderEventError = 1,
  kWinHwH264DecoderEventMax = 16,
};

}  // namespace

WinHwH264DecoderImpl::WinHwH264DecoderImpl() : pool_(true, 12),
                                     decoded_image_callback_(nullptr),
                                     has_reported_init_(false),
                                     has_reported_error_(false),
                                     is_initialized_(false) {
  pVideoDecoder = nullptr;
  mVideoCodecProvider = netease::HW_CODEC_AUTOSELECT_H264;
  mNoOutputRetryTimes = 0;
}

WinHwH264DecoderImpl::~WinHwH264DecoderImpl() {
  Release();
}

int32_t WinHwH264DecoderImpl::InitDecode(const VideoCodec* codec_settings,
                                    int32_t number_of_cores) {
  ReportInit();
  if (codec_settings &&
      codec_settings->codecType != kVideoCodecH264) {
    ReportError();
    return WEBRTC_VIDEO_CODEC_ERR_PARAMETER;
  }

  int32_t ret = Release();
  if (ret != WEBRTC_VIDEO_CODEC_OK) {
    ReportError();
    return ret;
  }

  ne_parameters_ = (webrtc::EngineParameterCollection*)(codec_settings->ne_parameters);
  if(ne_parameters_)
  {
    ne_parameters_->video.h264HWDecAdaptation.Connect(
      std::bind(&WinHwH264DecoderImpl::SetHWH264DecodeAdaptation,
      this, std::placeholders::_1), reinterpret_cast<size_t>(this), true);
  }

  RTC_LOG(LS_INFO) << "codec_settings->width : " << codec_settings->width;
  RTC_LOG(LS_INFO) << "codec_settings->height : " << codec_settings->height;
  h264_bitstream_width = codec_settings->width;
  h264_bitstream_height = codec_settings->height;

  netease::CODEC_RETCODE codec_ret = netease::CreateHWVideoDecoder(&mVideoCodecProvider, &pVideoDecoder, h264_bitstream_width, h264_bitstream_height, hwH264DecListStrategy, hwH264DecAdaptationList);
  if (codec_ret != netease::CODEC_OK) {
      pVideoDecoder = nullptr;
      ReportError();
      return WEBRTC_VIDEO_CODEC_FALLBACK_SOFTWARE;
  }

  netease::VideoDecoderConfig config;
  config.width = h264_bitstream_width;
  config.height = h264_bitstream_height;
  pVideoDecoder->setConfig(config);
  bool b_ret = pVideoDecoder->beginDecode();
  if(!b_ret) {
      netease::ReleaseHWVideoDecoder(mVideoCodecProvider, pVideoDecoder);
      pVideoDecoder = nullptr;
      ReportError();
      return WEBRTC_VIDEO_CODEC_FALLBACK_SOFTWARE;
  }
    
  is_initialized_ = true;

  return WEBRTC_VIDEO_CODEC_OK;
}

int32_t WinHwH264DecoderImpl::Release() {
  if (is_initialized_) {
      if (pVideoDecoder)
      {
        netease::ReleaseHWVideoDecoder(mVideoCodecProvider, pVideoDecoder);
        pVideoDecoder = nullptr;
      }

      is_initialized_ = false;
  }

  if(ne_parameters_) {
    ne_parameters_->video.h264HWDecAdaptation.Disconnect(reinterpret_cast<size_t>(this));
  }

  ne_parameters_ = nullptr;

  hwH264DecListStrategy = 0;
  hwH264DecAdaptationList = "";

  mNoOutputRetryTimes = 0;

  return WEBRTC_VIDEO_CODEC_OK;
}

void WinHwH264DecoderImpl::SetHWH264DecodeAdaptation(std::string hwH264DecAdaptation)
{
    RTC_LOG(LS_INFO) << "SetHWH264DecodeAdaptation : " << hwH264DecAdaptation;
    if (!hwH264DecAdaptation.empty())
    {
      char first = hwH264DecAdaptation.at(0);
      if (first=='0' || first=='1') {
        hwH264DecListStrategy = first - '0';
        hwH264DecAdaptationList = hwH264DecAdaptation.substr(3);
      }else {
        hwH264DecListStrategy = 1;
        hwH264DecAdaptationList = "";
      }
    }
    RTC_LOG(LS_INFO) << "hwH264DecListStrategy : " << hwH264DecListStrategy
                     << " hwH264DecAdaptationList : " << hwH264DecAdaptationList;
}

int32_t WinHwH264DecoderImpl::RegisterDecodeCompleteCallback(
    DecodedImageCallback* callback) {
  decoded_image_callback_ = callback;
  return WEBRTC_VIDEO_CODEC_OK;
}

int32_t WinHwH264DecoderImpl::Decode(const EncodedImage& input_image,
                                bool /*missing_frames*/,
                                const CodecSpecificInfo* codec_specific_info,
                                int64_t /*render_time_ms*/) {
  int64_t decode_start_time_ms = rtc::TimeMillis();

  if (!IsInitialized()) {
    ReportError();
    return WEBRTC_VIDEO_CODEC_UNINITIALIZED;
  }

  if (!decoded_image_callback_) {
    RTC_LOG(LS_WARNING)
        << "InitDecode() has been called, but a callback function "
           "has not been set with RegisterDecodeCompleteCallback()";
    ReportError();
    return WEBRTC_VIDEO_CODEC_UNINITIALIZED;
  }

  if (!input_image._buffer || !input_image._length) {
    ReportError();
    return WEBRTC_VIDEO_CODEC_ERR_PARAMETER;
  }

  if (codec_specific_info &&
      codec_specific_info->codecType != kVideoCodecH264) {
    ReportError();
    return WEBRTC_VIDEO_CODEC_ERR_PARAMETER;
  }

  h264_bitstream_parser_.ParseBitstream(input_image._buffer,
                                        input_image._length);
  int bitstream_width = 0;
  int bitstream_height = 0;
  if(h264_bitstream_parser_.GetLastResolution(&bitstream_width, &bitstream_height)) {
    if(bitstream_width!=h264_bitstream_width || bitstream_height!=h264_bitstream_height) {
        h264_bitstream_width = bitstream_width;
        h264_bitstream_height = bitstream_height;

        RTC_LOG(LS_INFO) << "Reset Win Hw H264 Decoder";
        pVideoDecoder->endDecode();
        netease::VideoDecoderConfig config;
        config.width = h264_bitstream_width;
        config.height = h264_bitstream_height;
        pVideoDecoder->setConfig(config);
        bool b_ret = pVideoDecoder->beginDecode();
        if (!b_ret) {
          RTC_LOG(LS_ERROR) << "failed to reset Win Hw H264 Decoder";
          ReportError();
          return WEBRTC_VIDEO_CODEC_FALLBACK_SOFTWARE;
        }
    }
  }

  netease::VideoEncodedFrame encodedFrame;
  encodedFrame.frameData.ptr = input_image._buffer;
  encodedFrame.frameSize = input_image._length;
  encodedFrame.dts = input_image.ntp_time_ms_ * 1000;
  encodedFrame.pts = input_image.ntp_time_ms_ * 1000;
  bool hasSps = false;
  bool hasPps = false;
  if (input_image._frameType == kVideoFrameKey && h264_bitstream_parser_.isLastIdr(hasSps, hasPps))
  {
    encodedFrame.frameType = netease::IDR_Frame;
  }else {
    encodedFrame.frameType = netease::P_Frame;
  }
  bool dec_ret = pVideoDecoder->addEncodedFrame(encodedFrame);
  if(!dec_ret) {
    RTC_LOG(LS_ERROR) << "failed to decode";
    ReportError();
    return WEBRTC_VIDEO_CODEC_FALLBACK_SOFTWARE;
  }

  netease::VideoRawFrame rawFrame;
  dec_ret = pVideoDecoder->getRawFrame(rawFrame);
  if (!dec_ret)
  {
    mNoOutputRetryTimes++;
    if (mNoOutputRetryTimes > WIN_HW_H264_DECODE_MAX_NO_OUTPUT_RETRY_TIMES)
    {
        RTC_LOG(LS_ERROR) << "no output VideoRawFrame : exceeded maximum number of retries";
        ReportError();
        return WEBRTC_VIDEO_CODEC_FALLBACK_SOFTWARE;
    }else if (mNoOutputRetryTimes > 1) {
        RTC_LOG(LS_WARNING) << "no output VideoRawFrame";
        return WEBRTC_VIDEO_CODEC_NO_OUTPUT;
    }else {
        RTC_LOG(LS_WARNING) << "no output VideoRawFrame";
        return WEBRTC_VIDEO_CODEC_OK;
    }
  }

  mNoOutputRetryTimes = 0;

  if (h264_bitstream_width != rawFrame.width || h264_bitstream_height != rawFrame.height)
  {
    RTC_LOG(LS_WARNING) << "Inconsistent width and height -->>" << " h264_bitstream_width : " << h264_bitstream_width << " h264_bitstream_height : " << h264_bitstream_height << " rawFrame.width : " << rawFrame.width << " rawFrame.height : " << rawFrame.height;
  }

  rtc::scoped_refptr<I420Buffer> frame_buffer;
  if (rawFrame.frame_buffer==nullptr)
  {
    frame_buffer = pool_.CreateBuffer(rawFrame.width, rawFrame.height);
    if (frame_buffer == nullptr)
    {
      RTC_LOG(LS_WARNING) << "Drop Frame -->>" << " CreateBuffer Fail With Width:" << rawFrame.width << " Height:" << rawFrame.height;
      // ReportError();
      // return WEBRTC_VIDEO_CODEC_FALLBACK_SOFTWARE;
        return WEBRTC_VIDEO_CODEC_NO_OUTPUT;
    }
  
    if (rawFrame.format == netease::FOURCC_I420)
    {
      libyuv::I420Copy((const uint8_t*)rawFrame.planeptrs[0].ptr, rawFrame.strides[0],
                     (const uint8_t*)rawFrame.planeptrs[1].ptr, rawFrame.strides[1],
                     (const uint8_t*)rawFrame.planeptrs[2].ptr, rawFrame.strides[2],
                     frame_buffer->MutableDataY(), frame_buffer->StrideY(),
                     frame_buffer->MutableDataU(), frame_buffer->StrideU(),
                     frame_buffer->MutableDataV(), frame_buffer->StrideV(),
                     rawFrame.width, rawFrame.height);
    }else if(rawFrame.format == netease::FOURCC_NV12)
    {
      libyuv::NV12ToI420((const uint8_t*)rawFrame.planeptrs[0].ptr, rawFrame.strides[0],
                         (const uint8_t*)rawFrame.planeptrs[1].ptr, rawFrame.strides[1],
                         frame_buffer->MutableDataY(), frame_buffer->StrideY(),
                         frame_buffer->MutableDataU(), frame_buffer->StrideU(),
                         frame_buffer->MutableDataV(), frame_buffer->StrideV(),
                         rawFrame.width, rawFrame.height);
    }
  }else {
    frame_buffer = rawFrame.frame_buffer;
  }
  
  VideoFrame decoded_frame =
      VideoFrame::Builder()
          .set_video_frame_buffer(frame_buffer)
          .set_timestamp_us(input_image.ntp_time_ms_ * 1000)
          .set_timestamp_rtp(input_image.Timestamp())
          .build();

  absl::optional<uint8_t> qp;
  int qp_int;
  if (h264_bitstream_parser_.GetLastSliceQp(&qp_int)) {
      qp.emplace(qp_int);
  }

  absl::optional<SeiParser::SeiState>& sei = h264_bitstream_parser_.ParsedSei();
  if (sei && !sei->internal_sei.empty()) {
    decoded_frame.set_sei_internal_data(sei->internal_sei);
  }

  decoded_image_callback_->Decoded(decoded_frame, absl::nullopt, qp);

  int64_t decode_stop_time_ms = rtc::TimeMillis();
  if (decode_stop_time_ms - decode_start_time_ms > 50)
  {
      RTC_LOG(LS_WARNING) << "winhw decode cost time : " << decode_stop_time_ms - decode_start_time_ms;
  }
  
  return WEBRTC_VIDEO_CODEC_OK;
}

const char* WinHwH264DecoderImpl::ImplementationName() const {
  if (pVideoDecoder) {
    return pVideoDecoder->getCodecFriendlyName();
  } else {
    return "unknown";
  }
}

bool WinHwH264DecoderImpl::IsInitialized() const {
  return is_initialized_;
}

void WinHwH264DecoderImpl::ReportInit() {
  if (has_reported_init_)
    return;
  RTC_HISTOGRAM_ENUMERATION("WebRTC.Video.WinHwH264DecoderImpl.Event",
                            kWinHwH264DecoderEventInit,
                            kWinHwH264DecoderEventMax);
  has_reported_init_ = true;
}

void WinHwH264DecoderImpl::ReportError() {
  if (has_reported_error_)
    return;
  RTC_HISTOGRAM_ENUMERATION("WebRTC.Video.WinHwH264DecoderImpl.Event",
                            kWinHwH264DecoderEventError,
                            kWinHwH264DecoderEventMax);
  has_reported_error_ = true;
}

}  // namespace webrtc
