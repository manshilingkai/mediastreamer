/*
 *  Copyright (c) 2021 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 *
 */

#ifndef MODULES_VIDEO_CODING_CODECS_WINHW_H264_WINHW_H264_DECODER_IMPL_H_
#define MODULES_VIDEO_CODING_CODECS_WINHW_H264_WINHW_H264_DECODER_IMPL_H_

#include <memory>

#include "modules/video_coding/codecs/winhw/h264/include/winhwh264.h"
#include "common_video/h264/h264_bitstream_parser.h"
#include "common_video/include/i420_buffer_pool.h"

#include "modules/video_coding/codecs/winhw/libhwcodec.h"
#include "media/engine/engine_parameter_collection.h"

namespace webrtc {

class WinHwH264DecoderImpl : public WinHwH264Decoder
{
public:
    WinHwH264DecoderImpl();
    ~WinHwH264DecoderImpl() override;

    int32_t InitDecode(const VideoCodec* codec_settings, int32_t number_of_cores) override;
    int32_t Release() override;

    int32_t RegisterDecodeCompleteCallback(DecodedImageCallback* callback) override;

    int32_t Decode(const EncodedImage& input_image,
                bool /*missing_frames*/,
                const CodecSpecificInfo* codec_specific_info = nullptr,
                int64_t render_time_ms = -1) override;

    const char* ImplementationName() const override;

private:
    bool IsInitialized() const;

    // Reports statistics with histograms.
    void ReportInit();
    void ReportError();

    I420BufferPool pool_;

    DecodedImageCallback* decoded_image_callback_;

    bool has_reported_init_;
    bool has_reported_error_;

    webrtc::H264BitstreamParser h264_bitstream_parser_;
    int h264_bitstream_width = 0;
    int h264_bitstream_height = 0;

    bool is_initialized_;
private:
    netease::HWVideoDecoder *pVideoDecoder;
    netease::VIDEO_CODEC_PROVIDER mVideoCodecProvider;
    int mNoOutputRetryTimes = 0;
private:
    webrtc::EngineParameterCollection* ne_parameters_ = nullptr;
    void SetHWH264DecodeAdaptation(std::string hwH264DecAdaptation);
    int hwH264DecListStrategy = 0;
    std::string hwH264DecAdaptationList = "";
};

}  // namespace webrtc

#endif