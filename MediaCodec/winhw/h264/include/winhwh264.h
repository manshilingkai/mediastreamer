/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 *
 */

#ifndef MODULES_VIDEO_CODING_CODECS_WINHW_H264_INCLUDE_WINHWH264_H_
#define MODULES_VIDEO_CODING_CODECS_WINHW_H264_INCLUDE_WINHWH264_H_

#include <memory>
#include <vector>

#include "media/base/codec.h"
#include "modules/video_coding/include/video_codec_interface.h"
#include "rtc_base/system/rtc_export.h"

namespace webrtc {

struct SdpVideoFormat;

// Returns a vector with all supported internal H264 profiles that we can
// negotiate in SDP, in order of preference.
std::vector<SdpVideoFormat> SupportedWinHwH264Codecs();

class RTC_EXPORT WinHwH264Encoder : public VideoEncoder {
 public:
  static std::unique_ptr<WinHwH264Encoder> Create(const cricket::VideoCodec& codec);
  static bool IsSupported();
  ~WinHwH264Encoder() override {}
};

class RTC_EXPORT WinHwH264Decoder : public VideoDecoder {
 public:
  static std::unique_ptr<WinHwH264Decoder> Create();
  static bool IsSupported();
  ~WinHwH264Decoder() override {}
};

}  // namespace webrtc

#endif  // MODULES_VIDEO_CODING_CODECS_WINHW_H264_INCLUDE_WINHWH264_H_
