/*
 *  Copyright (c) 2021 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 *
 */

#include "modules/video_coding/codecs/winhw/h264/winhw_h264_encoder_impl.h"

#include <limits>
#include <string>

#include "absl/strings/match.h"
#include "common_video/libyuv/include/webrtc_libyuv.h"
#include "modules/video_coding/utility/simulcast_rate_allocator.h"
#include "modules/video_coding/utility/simulcast_utility.h"
#include "rtc_base/checks.h"
#include "rtc_base/logging.h"
#include "rtc_base/timeutils.h"
#include "system_wrappers/include/metrics.h"
#include "third_party/libyuv/include/libyuv/convert.h"
#include "third_party/libyuv/include/libyuv/scale.h"

namespace webrtc {

namespace {

// Used by histograms. Values of entries should not be changed.
enum WinHwH264EncoderImplEvent {
  kWinHwH264EncoderEventInit = 0,
  kWinHwH264EncoderEventError = 1,
  kWinHwH264EncoderEventMax = 16,
};

FrameType ConvertToVideoFrameType(bool is_key_frame) {
  if (is_key_frame) {
    return kVideoFrameKey;
  } else {
    return kVideoFrameDelta;
  }
}

}  // namespace

// Helper method used by H264EncoderImpl::Encode.
// Copies the encoded bytes from |info| to |encoded_image| and updates the
// fragmentation information of |frag_header|. The |encoded_image->_buffer| may
// be deleted and reallocated if a bigger buffer is required.
//
// After OpenH264 encoding, the encoded bytes are stored in |info| spread out
// over a number of layers and "NAL units". Each NAL unit is a fragment starting
// with the four-byte start code {0,0,0,1}. All of this data (including the
// start codes) is copied to the |encoded_image->_buffer| and the |frag_header|
// is updated to point to each fragment, with offsets and lengths set as to
// exclude the start codes.
static void RtpFragmentize(EncodedImage* encoded_image,
                           std::unique_ptr<uint8_t[]>* encoded_image_buffer,
                           const VideoFrameBuffer& frame_buffer,
                           WinHwH264FrameBitStream* info,
                           RTPFragmentationHeader* frag_header) {
  // Calculate minimum buffer size required to hold encoded data.
  size_t required_size = 0;
  size_t fragments_count = 0;
  for (int nal = 0; nal < info->iNalCount; ++nal) {
    ++fragments_count;
    // 1 more byte to protect 3 bytes start code condition
    required_size += info->pNalLengthInByte[nal] + 1;
  }
  if (encoded_image->_size < required_size) {
    // Increase buffer size. Allocate enough to hold an unencoded image, this
    // should be more than enough to hold any encoded data of future frames of
    // the same size (avoiding possible future reallocation due to variations in
    // required size).
    encoded_image->_size = CalcBufferSize(
        VideoType::kI420, frame_buffer.width(), frame_buffer.height());
    if (encoded_image->_size < required_size) {
      // Encoded data > unencoded data. Allocate required bytes.
      RTC_LOG(LS_WARNING)
          << "Encoding produced more bytes than the original image "
          << "data! Original bytes: " << encoded_image->_size
          << ", encoded bytes: " << required_size << ".";
      encoded_image->_size = required_size;
    }
    encoded_image->_buffer = new uint8_t[encoded_image->_size];
    encoded_image_buffer->reset(encoded_image->_buffer);
  }

  // Iterate layers and NAL units, note each NAL unit as a fragment and copy
  // the data to |encoded_image->_buffer|.
  const uint8_t start_code[4] = {0, 0, 0, 1};
  frag_header->VerifyAndAllocateFragmentationHeader(fragments_count);
  encoded_image->_length = 0;

  for (int nal = 0; nal < info->iNalCount; ++nal) {
    if (info->pNalLengthInByte[nal] < 4) {
      RTC_LOG(LS_ERROR) << __FUNCTION__ << " invalid nal unit size";
      return;
    }

    if (info->ppBsBuf[nal][0] == start_code[0]
        && info->ppBsBuf[nal][1] == start_code[1]
        && info->ppBsBuf[nal][2] == start_code[2]
        && info->ppBsBuf[nal][3] == start_code[3]) {
      frag_header->fragmentationOffset[nal] =
        encoded_image->_length + sizeof(start_code);
      frag_header->fragmentationLength[nal] =
        info->pNalLengthInByte[nal] - sizeof(start_code);
    } else if (info->ppBsBuf[nal][0] == start_code[1] // 3 bytes start code
        && info->ppBsBuf[nal][1] == start_code[2]
        && info->ppBsBuf[nal][2] == start_code[3]) {
      encoded_image->_buffer[encoded_image->_length++] = start_code[0];
      frag_header->fragmentationOffset[nal] =
        encoded_image->_length + sizeof(start_code) - 1;
      frag_header->fragmentationLength[nal] =
        info->pNalLengthInByte[nal] - sizeof(start_code) + 1;
    }
    memcpy(encoded_image->_buffer + encoded_image->_length, info->ppBsBuf[nal],
          info->pNalLengthInByte[nal]);
    encoded_image->_length += info->pNalLengthInByte[nal];
  }
}

WinHwH264EncoderImpl::WinHwH264EncoderImpl(const cricket::VideoCodec& codec)
    : packetization_mode_(H264PacketizationMode::SingleNalUnit),
      max_payload_size_(0),
      number_of_cores_(0),
      encoded_image_callback_(nullptr),
      has_reported_init_(false),
      has_reported_error_(false) {
  RTC_CHECK(absl::EqualsIgnoreCase(codec.name, cricket::kH264CodecName));
  std::string packetization_mode_string;
  if (codec.GetParam(cricket::kH264FmtpPacketizationMode,
                     &packetization_mode_string) &&
      packetization_mode_string == "1") {
    packetization_mode_ = H264PacketizationMode::NonInterleaved;
  }
  downscaled_buffers_.reserve(kMaxSimulcastStreams - 1);
  encoded_images_.reserve(kMaxSimulcastStreams);
  encoded_image_buffers_.reserve(kMaxSimulcastStreams);
  encoders_.reserve(kMaxSimulcastStreams);
  configurations_.reserve(kMaxSimulcastStreams);
}

WinHwH264EncoderImpl::~WinHwH264EncoderImpl() {
  Release();
}

int32_t WinHwH264EncoderImpl::InitEncode(const VideoCodec* inst,
                                    int32_t number_of_cores,
                                    size_t max_payload_size) {
  ReportInit();
  if (!inst || inst->codecType != kVideoCodecH264) {
    ReportError();
    return WEBRTC_VIDEO_CODEC_ERR_PARAMETER;
  }
  if (inst->maxFramerate == 0) {
    ReportError();
    return WEBRTC_VIDEO_CODEC_ERR_PARAMETER;
  }
  if (inst->width < 1 || inst->height < 1) {
    ReportError();
    return WEBRTC_VIDEO_CODEC_ERR_PARAMETER;
  }

  int32_t release_ret = Release();
  if (release_ret != WEBRTC_VIDEO_CODEC_OK) {
    ReportError();
    return release_ret;
  }

  ne_parameters_ = (webrtc::EngineParameterCollection*)(h264_encode_param_.ne_parameters);
  if(ne_parameters_)
  {
    ne_parameters_->video.h264HWEncAdaptation.Connect(
      std::bind(&WinHwH264EncoderImpl::SetHWH264EncodeAdaptation,
      this, std::placeholders::_1), reinterpret_cast<size_t>(this), true);
  }

  int number_of_streams = SimulcastUtility::NumberOfSimulcastStreams(*inst);
  bool doing_simulcast = (number_of_streams > 1);

  if (doing_simulcast && (!SimulcastUtility::ValidSimulcastResolutions(
                              *inst, number_of_streams) ||
                          !SimulcastUtility::ValidSimulcastTemporalLayers(
                              *inst, number_of_streams))) {
    return WEBRTC_VIDEO_CODEC_ERR_SIMULCAST_PARAMETERS_NOT_SUPPORTED;
  }
  downscaled_buffers_.resize(number_of_streams - 1);
  encoded_images_.resize(number_of_streams);
  encoded_image_buffers_.resize(number_of_streams);
  encoders_.resize(number_of_streams);
  pictures_.resize(number_of_streams);
  configurations_.resize(number_of_streams);

  number_of_cores_ = number_of_cores;
  max_payload_size_ = max_payload_size;
  codec_ = *inst;

  // Code expects simulcastStream resolutions to be correct, make sure they are
  // filled even when there are no simulcast layers.
  if (codec_.numberOfSimulcastStreams == 0) {
    codec_.simulcastStream[0].width = codec_.width;
    codec_.simulcastStream[0].height = codec_.height;
  }

  for (int i = 0, idx = number_of_streams - 1; i < number_of_streams;
       ++i, --idx) {
    // Temporal layers still not supported.
    if (inst->simulcastStream[i].numberOfTemporalLayers > 1) {
      Release();
      return WEBRTC_VIDEO_CODEC_ERR_SIMULCAST_PARAMETERS_NOT_SUPPORTED;
    }

    WindowsHwH264Encoder* windows_hw_h264_encoder = new WindowsHwH264Encoder();
    RTC_DCHECK(windows_hw_h264_encoder);

    // Store h264 encoder.
    encoders_[i] = windows_hw_h264_encoder;

    // Set internal settings from codec_settings
    configurations_[i].simulcast_idx = idx;
    configurations_[i].sending = false;
    configurations_[i].width = codec_.simulcastStream[idx].width;
    configurations_[i].height = codec_.simulcastStream[idx].height;
    configurations_[i].max_frame_rate = codec_.simulcastStream[idx].maxFramerate;
    configurations_[i].frame_dropping_on = codec_.H264()->frameDroppingOn;
    configurations_[i].key_frame_interval = codec_.H264()->keyFrameInterval;

    // Create downscaled image buffers.
    if (i > 0) {
      downscaled_buffers_[i - 1] = I420Buffer::Create(
          configurations_[i].width, configurations_[i].height,
          configurations_[i].width, configurations_[i].width / 2,
          configurations_[i].width / 2);
      
    }

    // Codec_settings uses kbits/second; encoder uses bits/second.
    configurations_[i].max_bps = codec_.simulcastStream[idx].maxBitrate * 1000;
    configurations_[i].target_bps = codec_.simulcastStream[idx].targetBitrate * 1000;

    RTC_LOG(LS_INFO) << "initEncode Windows Hw H264 encoder width: " << configurations_[i].width << " height: " << configurations_[i].height
                    << " startBps: " << configurations_[i].target_bps << " maxBps: " << configurations_[i].max_bps
                    << " max_frame_rate: " << configurations_[i].max_frame_rate;

    // Create encoder parameters based on the layer configuration.
    WinHwH264EncoderParam encoder_params = CreateEncoderParams(i);
    encoder_params.bEnableTemporalSVC = codec_.supportSvc;
    encoder_params.iTemporalLayerNum = 2;
    encoder_params.bitrateAdjusterType = codec_.bitrateAdjusterType;
    encoder_params.minAdjustedBitratePct = codec_.minAdjustedBitratePct;
    encoder_params.maxAdjustedBitratePct = codec_.maxAdjustedBitratePct;
    if (codec_.mode == VideoCodecMode::kRealtimeVideo) {
      encoder_params.isScreenShare = false;
    } else if (codec_.mode == VideoCodecMode::kScreensharing) {
      encoder_params.isScreenShare = true;
    }else {
      encoder_params.isScreenShare = false;
    }

    // Initialize.
    if (windows_hw_h264_encoder->Initialize(encoder_params) != 0) {
      RTC_LOG(LS_ERROR) << "Failed to initialize Windows Hw H264 encoder";
      Release();
      ReportError();
      return WEBRTC_VIDEO_CODEC_FALLBACK_SOFTWARE;
    }

    // Initialize encoded image. Default buffer size: size of unencoded data.
    encoded_images_[i]._size =
        CalcBufferSize(VideoType::kI420, codec_.simulcastStream[idx].width,
                       codec_.simulcastStream[idx].height);
    encoded_images_[i]._buffer = new uint8_t[encoded_images_[i]._size];
    encoded_image_buffers_[i].reset(encoded_images_[i]._buffer);
    encoded_images_[i]._completeFrame = true;
    encoded_images_[i]._encodedWidth = codec_.simulcastStream[idx].width;
    encoded_images_[i]._encodedHeight = codec_.simulcastStream[idx].height;
    encoded_images_[i]._length = 0;

    // NOTE (qijiyue): for low stream, user VideoAdapter to control frame rate
    // RTC_LOG(LS_INFO) << "i = " << i << ", codec_.simulcastStream[" << idx
    //                  << "].height = " << codec_.simulcastStream[idx].height
    //                  << ", max_framerate = " << codec_.simulcastStream[idx].maxFramerate;
    if (i > 0) {
      low_stream_framerate_ = configurations_[i].max_frame_rate;
      // RTC_LOG(LS_INFO) << "qijiyue: low_stream_framerate_ = "
      //                  << *low_stream_framerate_
      //                  << ", maxFramerate = " << codec_.maxFramerate;
      videoAdapter_.OnOutputFormatRequest(absl::nullopt, absl::nullopt,
                                                    low_stream_framerate_); 
    }
  }

  SimulcastRateAllocator init_allocator(codec_);
  VideoBitrateAllocation allocation = init_allocator.GetAllocation(
      codec_.startBitrate * 1000, codec_.maxFramerate);
  return SetRateAllocation(allocation, codec_.maxFramerate);
}

int32_t WinHwH264EncoderImpl::Release() {
  while (!encoders_.empty()) {
    WindowsHwH264Encoder* windows_hw_h264_encoder = encoders_.back();
    if (windows_hw_h264_encoder) {
      windows_hw_h264_encoder->Uninitialize();
      delete windows_hw_h264_encoder;
    }
    encoders_.pop_back();
  }
  downscaled_buffers_.clear();
  configurations_.clear();
  encoded_images_.clear();
  encoded_image_buffers_.clear();
  pictures_.clear();

  if(ne_parameters_) {
    ne_parameters_->video.h264HWEncAdaptation.Disconnect(reinterpret_cast<size_t>(this));
  }

  ne_parameters_ = nullptr;
  hwH264EncListStrategy = 0;
  hwH264EncAdaptationList = "";

  return WEBRTC_VIDEO_CODEC_OK;
}

int32_t WinHwH264EncoderImpl::RegisterEncodeCompleteCallback(
    EncodedImageCallback* callback) {
  encoded_image_callback_ = callback;
  return WEBRTC_VIDEO_CODEC_OK;
}

int32_t WinHwH264EncoderImpl::SetRateAllocation(
    VideoBitrateAllocation& bitrate,
    uint32_t new_framerate) {
  if (encoders_.empty())
    return WEBRTC_VIDEO_CODEC_UNINITIALIZED;

  if (new_framerate < 1)
    return WEBRTC_VIDEO_CODEC_ERR_PARAMETER;

  if (bitrate.get_sum_bps() == 0) {
    // Encoder paused, turn off all encoding.
    for (size_t i = 0; i < configurations_.size(); ++i)
      configurations_[i].SetStreamState(false);
    return WEBRTC_VIDEO_CODEC_OK;
  }

  uint16_t forwardflag_layer0;
  bitrate.GetSpatialLayerForwardFlags(forwardflag_layer0);
    
  // At this point, bitrate allocation should already match codec settings.
  if (codec_.maxBitrate > 0)
    RTC_DCHECK_LE(bitrate.get_sum_kbps(), codec_.maxBitrate);
  RTC_DCHECK_GE(bitrate.get_sum_kbps(), codec_.minBitrate);
  if (codec_.numberOfSimulcastStreams > 0)
    RTC_DCHECK_GE(bitrate.get_sum_kbps(), codec_.simulcastStream[0].minBitrate);

  codec_.maxFramerate = new_framerate;

  bool print_bitrate = false;
  clock_ = Clock::GetRealTimeClock();
  int64_t now_time = clock_->TimeInMilliseconds();
  if(now_time - last_time_ > 2000){
    last_time_ = now_time;
    print_bitrate = true;
  }

  size_t stream_idx = encoders_.size() - 1;
  for (size_t i = 0; i < encoders_.size(); ++i, --stream_idx) {
    // Update layer config.
    configurations_[i].target_bps = bitrate.GetSpatialLayerSum(stream_idx);
    if (i > 0) {
      configurations_[i].max_frame_rate =
        static_cast<float>(*low_stream_framerate_);
    } else {
      configurations_[i].max_frame_rate = static_cast<float>(new_framerate);
    }
    if(stream_idx == 0){
      configurations_[stream_idx].forwardflag = forwardflag_layer0;
    }else if(stream_idx == 1){
      configurations_[stream_idx].forwardflag = 0x0000;
    }
      
    if (configurations_[i].target_bps) {
      configurations_[i].SetStreamState(true);

      // Update h264 encoder.
      int32_t bitrate = static_cast<int32_t>(configurations_[i].target_bps);
      if(print_bitrate){
        RTC_LOG(LS_INFO) << i << " : bitrate(H264) : " << bitrate;
      }

      target_bitrate_for_replace_ = bitrate;
      target_framerate_for_replace_ = configurations_[i].max_frame_rate;
      encoders_[i]->SetOption(WinHwH264ReconfigOption::kBitrate, &bitrate);
      encoders_[i]->SetOption(WinHwH264ReconfigOption::kFrameRate,
                              &configurations_[i].max_frame_rate);
    } else {
      configurations_[i].SetStreamState(false);
    }
  }

  return WEBRTC_VIDEO_CODEC_OK;
}

int32_t WinHwH264EncoderImpl::GetRates(uint32_t* bitrate, uint32_t* framerate) {
  if (bitrate && framerate) {
    *bitrate = target_bitrate_for_replace_;
    *framerate = target_framerate_for_replace_;
    return WEBRTC_VIDEO_CODEC_OK;
  }
  return WEBRTC_VIDEO_CODEC_ERROR;
}

int32_t WinHwH264EncoderImpl::SetRates(uint32_t bitrate, uint32_t framerate) {
  for (size_t i = 0; i < encoders_.size(); ++i) {
//    RTC_LOG(LS_INFO) << i << "[WILL][SetRates][H264EncoderImpl]bitrate(H264) : " << bitrate;
    configurations_[i].target_bps = bitrate;
    if (i > 0) {
      configurations_[i].max_frame_rate =
        static_cast<float>(*low_stream_framerate_);
    } else {
      configurations_[i].max_frame_rate = static_cast<float>(framerate);
    }
    configurations_[i].SetStreamState(true);
    encoders_[i]->SetOption(WinHwH264ReconfigOption::kBitrate, &bitrate);
    encoders_[i]->SetOption(WinHwH264ReconfigOption::kFrameRate, &configurations_[i].max_frame_rate);
  }
  return WEBRTC_VIDEO_CODEC_OK;
}

int32_t WinHwH264EncoderImpl::SetSvcParam(const webrtc::SVCSpecificInfo* svc_specific_info){
  if (svc_specific_info == NULL) return WEBRTC_VIDEO_CODEC_ERROR;
  
  if (encoders_.size() > 0) {
    encoders_[0]->SetTemporalSVC(svc_specific_info->enable_svc, svc_specific_info->layer_nums);
  }

  return WEBRTC_VIDEO_CODEC_OK;
}

webrtc::VideoEncoder::SVCCapabilitiesInfo WinHwH264EncoderImpl::GetSVCCapabilitiesInfo() const {
  SVCCapabilitiesInfo info = SVCCapabilitiesInfo();
  if (encoders_.size() > 0) {
    WinHwH264TemporalSVCCap winHwH264TemporalSVCCap = encoders_[0]->GetTemporalSVCCap();
    info.is_support_svc = winHwH264TemporalSVCCap.is_support_temporal_svc;
    info.max_layer_nums = winHwH264TemporalSVCCap.supported_max_temporal_layers;
    info.is_support_dynamic_enable_svc = winHwH264TemporalSVCCap.is_support_dynamic_enable_temporal_svc;
    info.is_support_dynamic_set_bitrate_fraction = winHwH264TemporalSVCCap.is_support_dynamic_set_bitrate_fraction;
    info.base_vs_enhance_bitrate_fraction_start = winHwH264TemporalSVCCap.base_vs_enhance_bitrate_fraction_default;
    info.base_vs_enhance_bitrate_fraction_min = winHwH264TemporalSVCCap.base_vs_enhance_bitrate_fraction_default;
  }
  return info;
}

int32_t WinHwH264EncoderImpl::Encode(const VideoFrame& input_frame,
                                const CodecSpecificInfo* codec_specific_info,
                                const std::vector<FrameType>* frame_types) {
  int64_t encode_start_time_ms = rtc::TimeMillis();

  if (encoders_.empty()) {
    ReportError();
    return WEBRTC_VIDEO_CODEC_UNINITIALIZED;
  }
  if (!encoded_image_callback_) {
    RTC_LOG(LS_WARNING)
        << "InitEncode() has been called, but a callback function "
        << "has not been set with RegisterEncodeCompleteCallback()";
    ReportError();
    return WEBRTC_VIDEO_CODEC_UNINITIALIZED;
  }

  rtc::scoped_refptr<const I420BufferInterface> frame_buffer =
      input_frame.video_frame_buffer()->ToI420();

  bool send_key_frame = false;
  for (size_t i = 0; i < configurations_.size(); ++i) {
    if (configurations_[i].key_frame_request && configurations_[i].sending) {
      send_key_frame = true;
      break;
    }
  }
  if (!send_key_frame && frame_types) {
    for (size_t i = 0; i < frame_types->size() && i < configurations_.size();
         ++i) {
      if ((*frame_types)[i] == kVideoFrameKey && configurations_[i].sending) {
        send_key_frame = true;
        break;
      }
    }
  }

  RTC_DCHECK_EQ(configurations_[0].width, frame_buffer->width());
  RTC_DCHECK_EQ(configurations_[0].height, frame_buffer->height());

  size_t num_spatial_layers = configurations_.size();

  pictures_[0].iStride[0] = frame_buffer->StrideY();
  pictures_[0].iStride[1] = frame_buffer->StrideU();
  pictures_[0].iStride[2] = frame_buffer->StrideV();
  pictures_[0].pData[0] = const_cast<uint8_t*>(frame_buffer->DataY());
  pictures_[0].pData[1] = const_cast<uint8_t*>(frame_buffer->DataU());
  pictures_[0].pData[2] = const_cast<uint8_t*>(frame_buffer->DataV());

  // Encode image for each layer.
  for (size_t i = 0; i < encoders_.size(); ++i) {

    // NOTE (qijiyue): put these skip logic before I420Scale to reduce cpu consumption
    if (!configurations_[i].sending) {
      continue;
    }
    if (frame_types != nullptr) {
      // Skip frame?
      if ((*frame_types)[i] == kEmptyFrame) {
        continue;
      }
    }
    if (i > 0) {
      const int in_width = input_frame.width();
      const int in_height = input_frame.height();
      int croped_width;
      int croped_height;
      int out_width;
      int out_height;
      if (!videoAdapter_.AdaptFrameResolution(in_width, in_height,
          input_frame.timestamp_us() * rtc::kNumNanosecsPerMicrosec,
          &croped_width, &croped_height, &out_width, &out_height)
          && !send_key_frame) {
        continue;
      }
    }
    // EncodeFrame input.
    pictures_[i].eColorFormat = WinHwH264ColorFormat::kI420;
    pictures_[i].iPicWidth = configurations_[i].width;
    pictures_[i].iPicHeight = configurations_[i].height;
    pictures_[i].timestamp = input_frame.timestamp();
    pictures_[i].ntp_time_ms = input_frame.ntp_time_ms();
    pictures_[i].capture_time_ms = input_frame.render_time_ms();
    pictures_[i].rotation = (int)input_frame.rotation();
    pictures_[i].cur_frame_idx = codec_specific_info?codec_specific_info->cur_frame_idx:0;
    // Downscale images on second and ongoing layers.

    if (i != 0) {
      pictures_[i].iStride[0] = downscaled_buffers_[i - 1]->StrideY();
      pictures_[i].iStride[1] = downscaled_buffers_[i - 1]->StrideU();
      pictures_[i].iStride[2] = downscaled_buffers_[i - 1]->StrideV();
      pictures_[i].pData[0] =
          const_cast<uint8_t*>(downscaled_buffers_[i - 1]->DataY());
      pictures_[i].pData[1] =
          const_cast<uint8_t*>(downscaled_buffers_[i - 1]->DataU());
      pictures_[i].pData[2] =
          const_cast<uint8_t*>(downscaled_buffers_[i - 1]->DataV());
      // Scale the image down a number of times by downsampling factor.
      libyuv::I420Scale(pictures_[i - 1].pData[0], pictures_[i - 1].iStride[0],
                        pictures_[i - 1].pData[1], pictures_[i - 1].iStride[1],
                        pictures_[i - 1].pData[2], pictures_[i - 1].iStride[2],
                        configurations_[i - 1].width,
                        configurations_[i - 1].height, pictures_[i].pData[0],
                        pictures_[i].iStride[0], pictures_[i].pData[1],
                        pictures_[i].iStride[1], pictures_[i].pData[2],
                        pictures_[i].iStride[2], configurations_[i].width,
                        configurations_[i].height, libyuv::kFilterBilinear);
    }
    pictures_[i].pData[3] = nullptr;
    pictures_[i].iStride[3] = 0;

    if (send_key_frame) {
      // API doc says ForceIntraFrame(false) does nothing, but calling this
      // function forces a key frame regardless of the |bIDR| argument's value.
      // (If every frame is a key frame we get lag/delays.)
      encoders_[i]->ForceIntraFrame();
      configurations_[i].key_frame_request = false;
    }

    // EncodeFrame output.
    WinHwH264FrameBitStream info;
    int enc_ret = encoders_[i]->EncodeFrame(&pictures_[i], &info);
    if (enc_ret < 0) {
      RTC_LOG(LS_ERROR)
          << "Win Hw H264 Encoder EncodeFrame : failed, returned " << enc_ret
          << ".";
      ReportError();
      return WEBRTC_VIDEO_CODEC_FALLBACK_SOFTWARE;
    }
    if (enc_ret == 0) {
      RTC_LOG(LS_INFO)
          << "Win Hw H264 Encoder EncodeFrame : no output, returned " << enc_ret
          << ".";
      continue;
    }

    encoded_images_[i]._encodedWidth = configurations_[i].width;
    encoded_images_[i]._encodedHeight = configurations_[i].height;
    encoded_images_[i].SetTimestamp(info.timestamp);
    encoded_images_[i].ntp_time_ms_ = info.ntp_time_ms;
    encoded_images_[i].capture_time_ms_ = info.capture_time_ms;
    encoded_images_[i].rotation_ = webrtc::VideoRotation(info.rotation);
    encoded_images_[i].content_type_ =
            (codec_.mode == VideoCodecMode::kScreensharing)
            ? VideoContentType::SCREENSHARE
            : VideoContentType::UNSPECIFIED;
    encoded_images_[i].timing_.flags = VideoSendTiming::kInvalid;
    encoded_images_[i]._frameType = ConvertToVideoFrameType(info.isKeyFrame);

    encoded_images_[i].SetSpatialIndex(configurations_[i].simulcast_idx);

    if(encoders_.size() == 1){
      if(i == 0){
        encoded_images_[0].forwardflags_ = configurations_[0].forwardflag;
      }
    }else if(encoders_.size() == 2){
      if(configurations_[0].sending){
          encoded_images_[1].forwardflags_ = configurations_[0].forwardflag;
          encoded_images_[0].forwardflags_ = configurations_[1].forwardflag;
      }
      else if(configurations_[1].sending){
        if(configurations_[1].forwardflag > 0){
          encoded_images_[1].forwardflags_ = configurations_[1].forwardflag;
          encoded_images_[0].forwardflags_ = configurations_[0].forwardflag;
        }
        else if(configurations_[0].forwardflag > 0){
          encoded_images_[1].forwardflags_ = configurations_[0].forwardflag;
          encoded_images_[0].forwardflags_ = configurations_[1].forwardflag;
        }
      }
    }

    // NOTE (qijiyue) get actual target enc bitrate very frame
    encoded_images_[i].target_bitrate_ = configurations_[i].target_bps;
    // NOTE (qijiyue) get hw enc flag
    encoded_images_[i].hw_enc_ = false;
    // Split encoded image up into fragments. This also updates
    encoded_images_[i].iFrameQp = info.iFrameQp;
    encoded_images_[i].iTargetBit = info.iTargetBit;
    encoded_images_[i].iTotalBit = info.iTotalBit;
    encoded_images_[i].fQpAvgAq = info.fQpAvgAq;
    encoded_images_[i].frameCost = info.frameCost;
    encoded_images_[i].iAvelineQp = info.iAvelineQp;
    encoded_images_[i].iCntUnrefP = info.iCntUnrefP;
    // encoded_images_[i].iHashHit = info.iHashHit;
    // encoded_images_[i].iCrossHit = info.iCrossHit;

    // |encoded_image_|.
    RTPFragmentationHeader frag_header;
    RtpFragmentize(&encoded_images_[i], &encoded_image_buffers_[i],
                   *frame_buffer, &info, &frag_header);

    // Encoder can skip frames to save bandwidth in which case
    // |encoded_images_[i]._length| == 0.
    if (encoded_images_[i]._length > 0) {
      {
        ///sei for screen
        bool valid_sei_interval = true;
        int scene_type_write_sei = 0;
        bool print_bitrate = false;
        clock_ = Clock::GetRealTimeClock();
        int64_t now_time = clock_->TimeInMilliseconds();
        if(now_time - last_time_ > 5000){
          last_time_ = now_time;
          print_bitrate = true;
        }
        
        if((h264_encode_param_.screenSceneType == 1 && screen_init_framerate_ <= 5)){
          valid_sei_interval = true;
          scene_type_write_sei = 1;
          encoded_images_[i].sei_w_frame_cnt = 0;
        }
        else{
          if(encoded_images_[i].sei_w_frame_cnt <= 10){
             encoded_images_[i].sei_w_frame_cnt++;
           }
        }
        
        if(encoded_images_[i].sei_w_frame_cnt > 10){
          valid_sei_interval = false;
        }
        
        if((configurations_[i].max_frame_rate <= 15) && (print_bitrate == true)){
          RTC_LOG(LS_INFO) << "seiInfo: cnt: " << encoded_images_[i].sei_w_frame_cnt << " scenetype: " << h264_encode_param_.screenSceneType << " fps: " << screen_init_framerate_ << " typeen: " << scene_type_write_sei << " seiwrite: " << valid_sei_interval;
        }
       
        if (valid_sei_interval) {
          std::vector<uint8_t> interval_sei;
          interval_sei.push_back(scene_type_write_sei);
          bool need_reset_buffer = h264_bitstream_parser_.AppendSei(
              interval_sei, SeiParser::NERTC_SEI_TYPE_INTERNAL,
              &(encoded_images_[i]._buffer), &(encoded_images_[i]._length),
              &(encoded_images_[i]._size));
          if (need_reset_buffer) {
            encoded_image_buffers_[i].reset(encoded_images_[i]._buffer);
          }
          const std::vector<H264::NaluIndex> nalu_idxs =
          H264::FindNaluIndices(encoded_images_[i]._buffer,
          encoded_images_[i]._length);
          frag_header.VerifyAndAllocateFragmentationHeader(nalu_idxs.size());
          for (size_t i = 0; i < nalu_idxs.size(); i++) {
            frag_header.fragmentationOffset[i] = 
              nalu_idxs[i].payload_start_offset;
            frag_header.fragmentationLength[i] = nalu_idxs[i].payload_size;
            frag_header.fragmentationPlType[i] = 0;
            frag_header.fragmentationTimeDiff[i] = 0;
          }
        }

      }

      encoded_images_[i].qp_ = static_cast<int>(encoded_images_[i].fQpAvgAq);
      // encoded_images_[i].target_bitrate_ = info.adjustedEncBitrateBps;

      // Deliver encoded image.
      CodecSpecificInfo codec_specific;
      codec_specific.codecType = kVideoCodecH264;
      codec_specific.codecSpecific.H264.packetization_mode =
          packetization_mode_;
      codec_specific.codecSpecific.H264.num_spatial_layers = num_spatial_layers;

      if (i==0) {
        codec_specific.enable_svc = info.enable_svc;
        codec_specific.temporal_idx = info.temporal_id;
        codec_specific.cur_frame_idx = info.cur_frame_idx;
        if(codec_specific.enable_svc) {
          MakeSvcParam(codec_specific, info.isKeyFrame);
        }
      }

      encoded_image_callback_->OnEncodedImage(encoded_images_[i],
                                              &codec_specific, &frag_header);
    }
  }

  int64_t encode_stop_time_ms = rtc::TimeMillis();
  if (encode_stop_time_ms - encode_start_time_ms > 50)
  {
      RTC_LOG(LS_WARNING) << "winhw encode cost time : " << encode_stop_time_ms - encode_start_time_ms;
  }

  return WEBRTC_VIDEO_CODEC_OK;
}

// Initialization parameters.
WinHwH264EncoderParam WinHwH264EncoderImpl::CreateEncoderParams(size_t i) const {
  WinHwH264EncoderParam encoder_params;
  if (codec_.mode == VideoCodecMode::kRealtimeVideo) {
  } else if (codec_.mode == VideoCodecMode::kScreensharing) {
  } else {
    RTC_NOTREACHED();
  }

  encoder_params.eRCMode = WinHwH264RcMode::kCBR;
  encoder_params.iWidth = configurations_[i].width;
  encoder_params.iHeight = configurations_[i].height;
  encoder_params.iTargetBps = configurations_[i].target_bps;
  encoder_params.iMaxBps = configurations_[i].max_bps;
  encoder_params.fMaxFps = configurations_[i].max_frame_rate;
  encoder_params.bEnableFrameSkip = configurations_[i].frame_dropping_on;
  encoder_params.iGop = configurations_[i].key_frame_interval;
  encoder_params.iMaxSliceSize = 0;
  if (packetization_mode_ == H264PacketizationMode::SingleNalUnit)
    encoder_params.iMaxSliceSize = max_payload_size_;
  encoder_params.iTemporalLayerNum = 1;
  encoder_params.hwH264EncListStrategy = hwH264EncListStrategy;
  encoder_params.hwH264EncAdaptationList = hwH264EncAdaptationList;
  return encoder_params;
}

void WinHwH264EncoderImpl::ReportInit() {
  if (has_reported_init_)
    return;
  RTC_HISTOGRAM_ENUMERATION("WebRTC.Video.WinHwH264EncoderImpl.Event",
                            kWinHwH264EncoderEventInit, kWinHwH264EncoderEventMax);
  has_reported_init_ = true;
}

void WinHwH264EncoderImpl::ReportError() {
  if (has_reported_error_)
    return;
  RTC_HISTOGRAM_ENUMERATION("WebRTC.Video.WinHwH264EncoderImpl.Event",
                            kWinHwH264EncoderEventError, kWinHwH264EncoderEventMax);
  has_reported_error_ = true;
}

VideoEncoder::EncoderInfo WinHwH264EncoderImpl::GetEncoderInfo() const {
  EncoderInfo info;
  info.supports_native_handle = false;
  info.implementation_name = encoders_.empty() ? "unknown" : encoders_[0]->getEncoderFriendlyName();
  WinHwH264QPThreshold winHwH264QPThreshold;
  if (!encoders_.empty())
  {
    winHwH264QPThreshold = encoders_[0]->getWinHwH264QPThreshold();
  }
  info.scaling_settings =
      VideoEncoder::ScalingSettings(winHwH264QPThreshold.lowQpThreshold, winHwH264QPThreshold.highQpThreshold);
  return info;
}

void WinHwH264EncoderImpl::LayerConfig::SetStreamState(bool send_stream) {
  if (send_stream && !sending) {
    // Need a key frame if we have not sent this stream before.
    key_frame_request = true;
  }
  sending = send_stream;
}

void WinHwH264EncoderImpl::SetEncoderParam(EncoderEncodeParam param) {
  h264_encode_param_ = param;
  RTC_LOG(LS_INFO) << "set encoder param preset= "
                    << param.preset
                    << " profile: " << param.profile
                    << "  ,screenType: " << param.screenSceneType
                    << "  ,cameraType: " << param.cameraSceneType;
}

void WinHwH264EncoderImpl::SetHWH264EncodeAdaptation(std::string hwH264EncAdaptation)
{
    RTC_LOG(LS_INFO) << "SetHWH264EncodeAdaptation : " << hwH264EncAdaptation;
    if (!hwH264EncAdaptation.empty())
    {
      char first = hwH264EncAdaptation.at(0);
      if (first=='0' || first=='1') {
        hwH264EncListStrategy = first - '0';
        hwH264EncAdaptationList = hwH264EncAdaptation.substr(3);
      }else {
        hwH264EncListStrategy = 1;
        hwH264EncAdaptationList = "";
      }
    }
    RTC_LOG(LS_INFO) << "hwH264EncListStrategy : " << hwH264EncListStrategy
                     << " hwH264EncAdaptationList : " << hwH264EncAdaptationList;
}

bool WinHwH264EncoderImpl::UpdateLowStreamFramerate(int fps) {
  RTC_LOG(LS_INFO) << "WinHwH264EncoderImpl::UpdateLowStreamFramerate, fps = " << fps;
  if (codec_.numberOfSimulcastStreams <= 1) {
    RTC_LOG(LS_INFO) << "should not UpdateLowStreamFramerate for single stream";
    return false;
  }
  if (fps > codec_.simulcastStream[0].maxFramerate) {
    RTC_LOG(LS_INFO) << "Invalid low fps(" << fps << "), maxfps = "
                     << codec_.simulcastStream[0].maxFramerate;
    return false;
  }
  if (low_stream_framerate_ == fps) {
    RTC_LOG(LS_INFO) << "Invalid low fps(" << fps
                     << "), low_stream_framerate_ = " << *low_stream_framerate_;
    return false;
  }
  low_stream_framerate_ = std::min(static_cast<int>(codec_.maxFramerate), fps);
  videoAdapter_.OnOutputFormatRequest(absl::nullopt, absl::nullopt,
                                                      low_stream_framerate_);
  return true;
}

bool WinHwH264EncoderImpl::UpdatePreferHwEncoder(int index, bool prefer) {
  return false;
}

void WinHwH264EncoderImpl::SetSceneFramerateIn(int fps){
  RTC_LOG(LS_INFO) << "WinHwH264EncoderImpl::SetSceneFramerateIn, fps = " << fps;
  screen_init_framerate_ = fps;
}

void WinHwH264EncoderImpl::MakeSvcParam(CodecSpecificInfo& info, bool isKeyFrame) {
  if(isKeyFrame) {
    info.use_ltr_idx = info.cur_frame_idx;
    svc_last_base_frame_idx_ = info.cur_frame_idx;
  } else if(info.temporal_idx == 0) {
    if(svc_last_base_frame_idx_) {
      info.use_ltr_idx = *svc_last_base_frame_idx_;
    } else {
      RTC_LOG_T(LS_ERROR) << "[SVC]sender, empty svc_last_base_frame_idx_ 1 cur_idx="<<info.cur_frame_idx;
    }
    svc_last_base_frame_idx_ = info.cur_frame_idx;
  } else {
    if(svc_last_base_frame_idx_) {
      info.use_ltr_idx = *svc_last_base_frame_idx_;
    } else {
      RTC_LOG_T(LS_ERROR) << "[SVC]sender, empty svc_last_base_frame_idx_ 2 cur_idx="<<info.cur_frame_idx;
    }
  }
}

}  // namespace webrtc
