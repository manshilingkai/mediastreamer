#ifndef MODULES_VIDEO_CODING_CODECS_WINHW_H264_WINHW_H264_ENCODER_H_
#define MODULES_VIDEO_CODING_CODECS_WINHW_H264_WINHW_H264_ENCODER_H_

#include <memory>
#include <vector>
#include <deque>

#include "api/video/i420_buffer.h"
#include "api/video_codecs/video_codec.h"
#include "modules/video_coding/codecs/winhw/h264/include/winhwh264.h"
#include "modules/video_coding/codecs/winhw/libhwcodec.h"
#include "system_wrappers/include/clock.h"
#include "common_video/h264/h264_bitstream_parser.h"

namespace webrtc {

enum class WinHwH264ReconfigOption {
  kBitrate,      // int32_t *value
  kFrameRate,    // float *value
};

enum class WinHwH264RcMode {
  kCBR,
  kABR,
};

enum class WinHwH264ColorFormat { kI420, kNV12 };


typedef struct WinHwH264EncoderParam {
  WinHwH264RcMode     eRCMode;            ///< ineffective when iTemporalLayerNum > 1
  int                 iWidth;
  int                 iHeight;
  int                 iTargetBps;
  int                 iMaxBps;
  int                 iGop;
  float               fMaxFps;
  size_t              iMaxSliceSize;      ///< 0 means no limit
  bool                bEnableFrameSkip;   ///< ineffective when iTemporalLayerNum == 1
  bool                bEnableTemporalSVC;
  int                 iTemporalLayerNum;  ///< should be within [1,4]
  //int             iSpatialLayerNum; ///< WebRTC has its own solution for simulcast

  int                 hwH264EncListStrategy;
  std::string         hwH264EncAdaptationList;

  int                 bitrateAdjusterType;
  float               minAdjustedBitratePct;
  float               maxAdjustedBitratePct;
  bool                isScreenShare;
} WinHwH264EncoderParam;

typedef struct WinHwH264SourcePicture {
  WinHwH264ColorFormat eColorFormat;   ///< support yuv420 only currently
  int       iStride[4];            ///< stride for each plane pData
  unsigned char*  pData[4];        ///< plane pData
  int       iPicWidth;             ///< luma picture width in x coordinate
  int       iPicHeight;            ///< luma picture height in y coordinate

  uint32_t timestamp;
  int64_t ntp_time_ms;
  int64_t capture_time_ms;
  int rotation;
  uint16_t cur_frame_idx;
} WinHwH264SourcePicture;

typedef struct WinHwH264FrameBitStream {
  bool  isKeyFrame;
  int   iNalCount;              ///< number of NAL coded already
  int*  pNalLengthInByte;       ///< length of NAL size in byte
  unsigned char**  ppBsBuf;     ///< buffer of bitstream contained
  int iFrameSizeInBytes;
  long long uiTimeStamp;
  uint8_t iTemporalLayerId;     ///< layer index
  int iSubSeqId;                ///< frame index in temporal layer
  int             iFrameQp;
  int             iTargetBit;
  int             iTotalBit;
  float           fQpAvgAq;
  int             iCntUnrefP;
  double          frameCost;
  int             iAvelineQp;
	int32_t adjustedEncBitrateBps;

  uint32_t timestamp;
  int64_t ntp_time_ms;
  int64_t capture_time_ms;
  int rotation;
  uint16_t cur_frame_idx;

  bool enable_svc;
  uint16_t temporal_id;// svc temporal id
} WinHwH264FrameBitStream;

typedef struct WinHwH264QPThreshold {
	int lowQpThreshold;
	int highQpThreshold;

  WinHwH264QPThreshold()
  {
    lowQpThreshold = netease::kLowH26XQpThreshold;
    highQpThreshold = netease::kHighH26XQpThreshold;
  }
} WinHwH264QPThreshold;

typedef struct WinHwH264TemporalSVCCap {
    WinHwH264TemporalSVCCap()
    {
      is_support_temporal_svc = false;
      supported_max_temporal_layers = 2;
      is_support_dynamic_enable_temporal_svc = false;
      is_support_dynamic_set_bitrate_fraction = false;
      base_vs_enhance_bitrate_fraction_default = 0.6;
    }

    bool is_support_temporal_svc;
    int32_t supported_max_temporal_layers;
    bool is_support_dynamic_enable_temporal_svc;
    bool is_support_dynamic_set_bitrate_fraction;
    float base_vs_enhance_bitrate_fraction_default;// 5:5 = 0.5； 6:4 = 0.6； 7:3 = 0.7； 8:2 = 0.8； 9:1 = 0.9
} WinHwH264TemporalSVCCap;

typedef struct WinHwH264FrameExtraInfo {
  WinHwH264FrameExtraInfo()
  {
    timestamp = 0;
    ntp_time_ms = 0;
    capture_time_ms = 0;
    rotation = 0;
    cur_frame_idx = 0;
  }

  uint32_t timestamp;
  int64_t ntp_time_ms;
  int64_t capture_time_ms;
  int rotation;
  uint16_t cur_frame_idx;
};

class WindowsHwH264Encoder {
 public:
  WindowsHwH264Encoder();
  ~WindowsHwH264Encoder();

  /**
  * @brief  Initilaize encoder
  * @param  param: parameter for encoder
  * @return 0 - success; otherwise - failed;
  */
  int Initialize(const WinHwH264EncoderParam &param);

  /// uninitialize the encoder
  void Uninitialize();

  /**
  * @brief Encode one frame
  * @param pSrcPic: input source picture
  * @param pBs: output bit stream
  * @return  0 - success; otherwise -failed;
  */
  int EncodeFrame(const WinHwH264SourcePicture* pSrcPic, WinHwH264FrameBitStream* pBs);

  /**
  * @brief  Force encoder to encode frame as IDR frame
  * @return 0 - success; otherwise - failed;
  */
  int ForceIntraFrame();

  /**
  * @brief   Set option for encoder, detail option type, please refer to enumurate WinHwH264ReconfigOption.
  * @param   option: option for encoder such as Frame Rate, Bitrate
  * @param   value: address of option value
  * @return  0 - success; otherwise - failed;
  */
  int SetOption(WinHwH264ReconfigOption option, void* value);

  int SetTemporalSVC(bool isEnableTemporalSVC, uint32_t numTemporalLayers);
  WinHwH264TemporalSVCCap GetTemporalSVCCap();

	const char* getEncoderFriendlyName();

  WinHwH264QPThreshold getWinHwH264QPThreshold();

 private:
  bool ConfigParam(const WinHwH264EncoderParam &param);
  void CreateAvcEncoderParams();
  int EncodeFrameAvc(const WinHwH264SourcePicture* pSrcPic, WinHwH264FrameBitStream* pBs);
  void InitFrameBitStream(WinHwH264FrameBitStream* pBs) const;

 private:
  WinHwH264EncoderParam config_;
  bool isInitialized_;
  std::vector<unsigned char *> bsBuf_;
  std::vector<int> nalLens_;
  uint16_t avcSeqId_;
  bool requestingAvcKeyrame_;
private:
  webrtc::H264BitstreamParser h264_bitstream_parser_;
  netease::VideoEncoderConfig videoEncoderParam;
  netease::HWVideoEncoder *pVideoEncoder;
  netease::VIDEO_CODEC_PROVIDER mVideoCodecProvider;
  int mNoOutputRetryTimes = 0;
private:
  std::shared_ptr<std::deque<WinHwH264FrameExtraInfo>> frame_extra_infos_;
};

} // namespace webrtc

#endif
