#include "modules/video_coding/codecs/winhw/nvidia/nvidia_videoencoder.h"
#include "rtc_base/logging.h"
#include "modules/video_coding/codecs/winhw/win/gpu_info.h"

#define NVENCAPI_CHECK_VERSION(major, minor) \
    ((major) < NVENCAPI_MAJOR_VERSION || ((major) == NVENCAPI_MAJOR_VERSION && (minor) <= NVENCAPI_MINOR_VERSION))

#define nv_min(a,b) (((a) < (b)) ? (a) : (b))

namespace netease {

NvidiaVideoEncoder::NvidiaVideoEncoder(GUID codecId)
    :m_bIsEnablePerf(false)
{
    m_CudaDll = NULL;
    m_NvEncDll = NULL;
    m_NvEncFuncs = { NV_ENCODE_API_FUNCTION_LIST_VER };

    mCodecId = codecId;

    m_CuContext = NULL;
    m_pNvEncoderCuda = NULL;
    m_bEncoderInited = false;

    m_pOutputBuffer = NULL;
    m_OutputBufferSize = 0;

    m_bForceIntraFrame = false;
	m_bResetBitrate = false;
    m_bResetFramerate = false;

    m_pBitrateAdjuster = NULL;
    m_encoderBitrateBps = 0;

    m_bIsEnableLTR = false;
    m_ltrNumFrames = 0;

    m_bResetTemporalSVC = false;
    m_TemporalSVCCap.base_vs_enhance_bitrate_fraction_default = 0.6;

    m_HWGPUName = "NVIDIA";
    std::vector<GpuInfo>& available_gpuInfos = GPUInfoHandler::GetInstance().GetAvailableGpuInfos();
	for (GpuInfo gpuInfo : available_gpuInfos) {
        if (gpuInfo.provider == GPU_NVIDIA) {
            m_HWGPUName = gpuInfo.name;
        }
    }

    if (mCodecId == NV_ENC_CODEC_HEVC_GUID)
    {
        m_CodecFriendlyName = "H265 Encoder [" + m_HWGPUName + "]";
    }else {
        m_CodecFriendlyName = "H264 Encoder [" + m_HWGPUName + "]";
    }
}

NvidiaVideoEncoder::~NvidiaVideoEncoder()
{
    this->endEncode();

    if (m_CuContext)
    {
        CUresult ret = m_CudaDll->cuCtxDestroy(m_CuContext);
        if (ret != CUDA_SUCCESS)
        {
            RTC_LOG(LS_ERROR) << " cuCtxDestroy fail. error code : " << ret;
        }
    }

    if (m_NvEncDll)
    {
        nvenc_free_functions(&m_NvEncDll);
        m_NvEncDll = NULL;
    }

    if (m_CudaDll)
    {
        cuda_free_functions(&m_CudaDll);
        m_CudaDll = NULL;
    }
    
    if (m_pBitrateAdjuster != NULL) {
        delete m_pBitrateAdjuster;
        m_pBitrateAdjuster = NULL;
    }
}

// implement VideoEncoder interface
bool NvidiaVideoEncoder::init()
{
    int dll_ret = cuda_load_functions(&m_CudaDll, NULL);
    if (dll_ret < 0)
    {
        m_CudaDll = NULL;
        RTC_LOG(LS_ERROR) << "Could not dynamically load CUDA, ErroCode: " << dll_ret;
        return false;
    }

    dll_ret = nvenc_load_functions(&m_NvEncDll, NULL);
    if (dll_ret < 0)
    {
        m_NvEncDll = NULL;
        RTC_LOG(LS_ERROR) << "Could not dynamically load NVENC, ErroCode: " << dll_ret;
        nvencPrintDriverRequirement();
        return false;
    }

    NVENCSTATUS err;
    uint32_t nvenc_max_ver = 0;
    err = m_NvEncDll->NvEncodeAPIGetMaxSupportedVersion(&nvenc_max_ver);
    if (err != NV_ENC_SUCCESS) {
        RTC_LOG(LS_ERROR) << "Failed to query nvenc max version, ErrorCode: " << err;
        return false;
    }

    RTC_LOG(LS_INFO) << "Loaded Nvenc version " << (nvenc_max_ver >> 4) << "." << (nvenc_max_ver & 0xf);

    if ((NVENCAPI_MAJOR_VERSION << 4 | NVENCAPI_MINOR_VERSION) > nvenc_max_ver) {
        RTC_LOG(LS_ERROR) << "Driver does not support the required nvenc API version. " << "Required: " << NVENCAPI_MAJOR_VERSION << "." << NVENCAPI_MINOR_VERSION << " Found: " << (nvenc_max_ver >> 4) << "." << (nvenc_max_ver & 0xf);
        // nvencPrintDriverRequirement();
        // return false;
    }

    nvencapi_version = nvenc_api_ver(nvenc_max_ver >> 4, nvenc_max_ver & 0xf);
    RTC_LOG(LS_INFO) << "nvencapi_version : " << nvencapi_version;
    nvencapi_version_major = (uint32_t)(nvenc_max_ver >> 4);
	nvencapi_version_minor = (uint8_t)(nvenc_max_ver & 0xf);
    m_NvEncFuncs.version = nvencapi_struct_version(nvencapi_version, 2);
    err = m_NvEncDll->NvEncodeAPICreateInstance(&m_NvEncFuncs);
    if (err != NV_ENC_SUCCESS) {
        RTC_LOG(LS_ERROR) << "Failed to create nvenc instance, ErrorCode: " << err;
        return false;
    }

    CUresult ret = m_CudaDll->cuInit(0);
    if (ret!=CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuInit fail. error code : " << ret;
        return false;
    }
    
    int gpu_count = 0;
    ret = m_CudaDll->cuDeviceGetCount(&gpu_count);
    if (ret!=CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGetCount fail. error code : " << ret;
        return false;
    }

    if (gpu_count <= 0)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGetCount get gpu count is " << gpu_count;
        return false;
    }

    bool b_ret = false;
    for (int gpu_index = 0; gpu_index < gpu_count; gpu_index++)
    {
        b_ret = createCudaContext(&m_CuContext, gpu_index, 0);
        if (!b_ret)
        {
            RTC_LOG(LS_WARNING) << " createCudaContext fail for gpu index : " << gpu_index;
        }else {
            RTC_LOG(LS_INFO) << " createCudaContext success for gpu index : " << gpu_index;
            break;
        }
    }
    
    if (!b_ret) return false;

    RTC_LOG(LS_INFO) << "init success";
    
    return true;
}

bool NvidiaVideoEncoder::createCudaContext(CUcontext* cuContext, int indexGpu, unsigned int flags)
{
    CUdevice cuDevice = 0;

    CUresult ret = m_CudaDll->cuDeviceGet(&cuDevice, indexGpu);
    if (ret != CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGet fail. error code : " << ret;
        return false;
    }
    
    char szDeviceName[80];
    ret = m_CudaDll->cuDeviceGetName(szDeviceName, sizeof(szDeviceName), cuDevice);
    if (ret != CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGetName fail. error code : " << ret;
        return false;
    }
    
    RTC_LOG(LS_INFO) << "GPU in use: " << szDeviceName;

    ret = m_CudaDll->cuCtxCreate(cuContext, flags, cuDevice);
    if (ret != CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuCtxCreate fail. error code : " << ret;
        return false;
    }

    return true;
}

void NvidiaVideoEncoder::nvencPrintDriverRequirement()
{
#if NVENCAPI_CHECK_VERSION(11, 2)
    const char *minver = "(unknown)";
#elif NVENCAPI_CHECK_VERSION(11, 1)
# if defined(_WIN32) || defined(__CYGWIN__)
    const char *minver = "471.41";
# else
    const char *minver = "470.57.02";
# endif
#elif NVENCAPI_CHECK_VERSION(11, 0)
# if defined(_WIN32) || defined(__CYGWIN__)
    const char *minver = "456.71";
# else
    const char *minver = "455.28";
# endif
#elif NVENCAPI_CHECK_VERSION(10, 0)
# if defined(_WIN32) || defined(__CYGWIN__)
    const char *minver = "450.51";
# else
    const char *minver = "445.87";
# endif
#elif NVENCAPI_CHECK_VERSION(9, 1)
# if defined(_WIN32) || defined(__CYGWIN__)
    const char *minver = "436.15";
# else
    const char *minver = "435.21";
# endif
#elif NVENCAPI_CHECK_VERSION(9, 0)
# if defined(_WIN32) || defined(__CYGWIN__)
    const char *minver = "418.81";
# else
    const char *minver = "418.30";
# endif
#elif NVENCAPI_CHECK_VERSION(8, 2)
# if defined(_WIN32) || defined(__CYGWIN__)
    const char *minver = "397.93";
# else
    const char *minver = "396.24";
#endif
#elif NVENCAPI_CHECK_VERSION(8, 1)
# if defined(_WIN32) || defined(__CYGWIN__)
    const char *minver = "390.77";
# else
    const char *minver = "390.25";
# endif
#else
# if defined(_WIN32) || defined(__CYGWIN__)
    const char *minver = "378.66";
# else
    const char *minver = "378.13";
# endif
#endif
    std::string minver_str = minver;
    RTC_LOG(LS_WARNING) << " The minimum required Nvidia driver for nvenc is " << minver_str << " or newer";
}

uint32_t NvidiaVideoEncoder::ltrUseFrameIdxToLtrUseFrameBitmap(int ltrUseFrameIdx)
{
    uint32_t bitmap = 0x00000001;
    if (ltrUseFrameIdx < 0 || ltrUseFrameIdx > m_ltrNumFrames-1) return 0;
    else if (ltrUseFrameIdx==0) return bitmap;
    else {
        bitmap = bitmap << ltrUseFrameIdx;
        return bitmap;
    }
}

int NvidiaVideoEncoder::ltrUseFrameBitmapToLtrUseFrameIdx(uint32_t ltrUseFrameBitmap)
{
    int ltrUseFrameIdx = -1;
    while (ltrUseFrameBitmap)
    {
        ltrUseFrameBitmap >>= 1;
        ltrUseFrameIdx++;
    }
    
    if (ltrUseFrameIdx>=m_ltrNumFrames)
    {
        return -1;
    }
    
    return ltrUseFrameIdx;
}

bool NvidiaVideoEncoder::setConfig(const VideoEncoderConfig &config)
{
    m_config = config;

    if (m_pBitrateAdjuster != NULL) {
        delete m_pBitrateAdjuster;
        m_pBitrateAdjuster = NULL;
    }

    HWQPThreshold hwQPThreshold = getHWQPThreshold();
    m_pBitrateAdjuster = new webrtc::BitrateAdjuster(config.minAdjustedBitratePct, config.maxAdjustedBitratePct, config.enableDynamicAdjustMaxBitratePct, 
                                                     hwQPThreshold.lowQpThreshold, hwQPThreshold.highQpThreshold, config.enableBitrateAdjuster);
    m_pBitrateAdjuster->SetTargetBitrateBps(m_config.avgBitrate);
    m_encoderBitrateBps = m_pBitrateAdjuster->GetAdjustedBitrateBps();

    return true;
}

bool NvidiaVideoEncoder::beginEncode()
{
    if (m_bEncoderInited) return true;

    NV_ENC_BUFFER_FORMAT eFormat = NV_ENC_BUFFER_FORMAT_IYUV;

    if (m_config.pixFormat == FOURCC_I420)
    {
        eFormat = NV_ENC_BUFFER_FORMAT_IYUV;
    }else if (m_config.pixFormat == FOURCC_NV12)
    {
        eFormat = NV_ENC_BUFFER_FORMAT_NV12;
    }
    
    m_pNvEncoderCuda = new NvEncoderCuda(m_CudaDll, m_CuContext, nvencapi_version_major, nvencapi_version_minor, &m_NvEncFuncs, m_config.width, m_config.height, eFormat, 0);
    if (m_pNvEncoderCuda->GotError())
    {
        delete m_pNvEncoderCuda;
        m_pNvEncoderCuda = NULL;

        RTC_LOG(LS_ERROR) << "NvEncoderCuda got error";
        return false;
    }
    
    m_NVInitializeParams = { NV_ENC_INITIALIZE_PARAMS_VER };
    m_NVInitializeParams.version = (nvencapi_struct_version(nvencapi_version, 5) | ( 1u<<31 ));
    m_NVEncodeConfig = { NV_ENC_CONFIG_VER };
    m_NVEncodeConfig.version = (nvencapi_struct_version(nvencapi_version, 7) | ( 1u<<31 ));
    m_NVInitializeParams.encodeConfig = &m_NVEncodeConfig;
    GUID presetGuid = NV_ENC_PRESET_P3_GUID;
    if (mCodecId == NV_ENC_CODEC_HEVC_GUID) {
      presetGuid = NV_ENC_PRESET_P2_GUID;
    }
    bool ret = m_pNvEncoderCuda->CreateDefaultEncoderParams(&m_NVInitializeParams, mCodecId, presetGuid, NV_ENC_TUNING_INFO_ULTRA_LOW_LATENCY);
    if (!ret)
    {
        RTC_LOG(LS_ERROR) << "CreateDefaultEncoderParams Fail";
        delete m_pNvEncoderCuda;
        m_pNvEncoderCuda = NULL;
        return false;
    }
    
    m_NVInitializeParams.frameRateNum = m_config.fps;
    m_NVInitializeParams.frameRateDen = 1;
    m_NVInitializeParams.enableEncodeAsync = 1;
    m_NVEncodeConfig.gopLength = m_config.gop;//NVENC_INFINITE_GOPLENGTH;
    m_NVEncodeConfig.frameIntervalP = 1;
    m_NVEncodeConfig.frameFieldMode = NV_ENC_PARAMS_FRAME_FIELD_MODE_FRAME;
    if (mCodecId == NV_ENC_CODEC_H264_GUID)
    {
        // yuv420p
        m_NVEncodeConfig.encodeCodecConfig.h264Config.chromaFormatIDC = 1;

        m_NVEncodeConfig.encodeCodecConfig.h264Config.idrPeriod = m_NVEncodeConfig.gopLength;
        m_NVEncodeConfig.encodeCodecConfig.h264Config.sliceMode = 3;
        m_NVEncodeConfig.encodeCodecConfig.h264Config.sliceModeData = 1;
        m_NVEncodeConfig.profileGUID = NV_ENC_H264_PROFILE_HIGH_GUID;
        m_NVEncodeConfig.encodeCodecConfig.h264Config.level = NV_ENC_LEVEL_AUTOSELECT;

        m_NVEncodeConfig.encodeCodecConfig.h264Config.repeatSPSPPS = 1;
        m_NVEncodeConfig.encodeCodecConfig.h264Config.disableSPSPPS = 0;

        //Consistency check (general, H.264 / AVC)
        m_NVEncodeConfig.encodeCodecConfig.h264Config.bdirectMode = NV_ENC_H264_BDIRECT_MODE_TEMPORAL;
        m_NVEncodeConfig.encodeCodecConfig.h264Config.entropyCodingMode = NV_ENC_H264_ENTROPY_CODING_MODE_CABAC;
        if (m_NVEncodeConfig.frameIntervalP - 1 <= 0) {
            m_NVEncodeConfig.encodeCodecConfig.h264Config.bdirectMode = NV_ENC_H264_BDIRECT_MODE_DISABLE;
        }

        m_NVEncodeConfig.encodeCodecConfig.h264Config.enableVFR = 1;

        m_bIsEnableLTR = m_config.isEnableLTR;
        if (m_config.isEnableLTR)
        {
            m_NVEncodeConfig.encodeCodecConfig.h264Config.enableLTR = 1;
            m_NVEncodeConfig.encodeCodecConfig.h264Config.ltrTrustMode = 0;

            m_ltrNumFrames = 0;
            m_pNvEncoderCuda->GetCapabilityValue(NV_ENC_CODEC_H264_GUID, NV_ENC_CAPS_NUM_MAX_LTR_FRAMES, &m_ltrNumFrames);
            m_NVEncodeConfig.encodeCodecConfig.h264Config.ltrNumFrames = m_ltrNumFrames;

            if (m_NVEncodeConfig.encodeCodecConfig.h264Config.ltrNumFrames <= 0)
            {
                m_NVEncodeConfig.encodeCodecConfig.h264Config.enableLTR = 0;
                m_NVEncodeConfig.encodeCodecConfig.h264Config.ltrTrustMode = 0;
            }
        }

        if (m_config.isEnableTemporalSVC) {
            int is_support_temporal_svc = 0;
            int num_max_temporal_layers = 0;
            m_pNvEncoderCuda->GetCapabilityValue(NV_ENC_CODEC_H264_GUID, NV_ENC_CAPS_SUPPORT_TEMPORAL_SVC, &is_support_temporal_svc);
            if (is_support_temporal_svc)
            {
                m_pNvEncoderCuda->GetCapabilityValue(NV_ENC_CODEC_H264_GUID, NV_ENC_CAPS_NUM_MAX_TEMPORAL_LAYERS, &num_max_temporal_layers);
            }
            RTC_LOG(LS_INFO) << "is_support_temporal_svc : " << is_support_temporal_svc << " num_max_temporal_layers : " << num_max_temporal_layers;
            if (is_support_temporal_svc && num_max_temporal_layers >= 2)
            {
                m_NVEncodeConfig.encodeCodecConfig.h264Config.enableTemporalSVC = 1;
                m_NVEncodeConfig.encodeCodecConfig.h264Config.disableSVCPrefixNalu = 0;
                m_NVEncodeConfig.encodeCodecConfig.h264Config.enableScalabilityInfoSEI = 0;
                m_NVEncodeConfig.encodeCodecConfig.h264Config.numTemporalLayers = nv_min(m_config.numTemporalLayers, num_max_temporal_layers);
                m_NVEncodeConfig.encodeCodecConfig.h264Config.maxTemporalLayers = num_max_temporal_layers;

                m_TemporalSVCCap.is_support_temporal_svc = true;
                m_TemporalSVCCap.supported_max_temporal_layers = num_max_temporal_layers;
                m_TemporalSVCCap.is_support_dynamic_enable_temporal_svc = true;
                m_TemporalSVCCap.is_support_dynamic_set_bitrate_fraction = false;
            }else {
                m_NVEncodeConfig.encodeCodecConfig.h264Config.enableTemporalSVC = 0;

                m_TemporalSVCCap.is_support_temporal_svc = false;
            }
        }else {
            m_NVEncodeConfig.encodeCodecConfig.h264Config.enableTemporalSVC = 0;

            m_TemporalSVCCap.is_support_temporal_svc = false;
        }
    }else {
        // yuv420p
        m_NVEncodeConfig.encodeCodecConfig.hevcConfig.chromaFormatIDC = 1;

        m_NVEncodeConfig.encodeCodecConfig.hevcConfig.idrPeriod = m_NVEncodeConfig.gopLength;
        m_NVEncodeConfig.encodeCodecConfig.hevcConfig.sliceMode = 3;
        m_NVEncodeConfig.encodeCodecConfig.hevcConfig.sliceModeData = 1;
        m_NVEncodeConfig.profileGUID = NV_ENC_HEVC_PROFILE_MAIN_GUID;
        m_NVEncodeConfig.encodeCodecConfig.hevcConfig.tier = NV_ENC_TIER_HEVC_MAIN;

        m_NVEncodeConfig.encodeCodecConfig.hevcConfig.repeatSPSPPS = 1;
        m_NVEncodeConfig.encodeCodecConfig.hevcConfig.disableSPSPPS = 0;

        m_bIsEnableLTR = m_config.isEnableLTR;
        if (m_config.isEnableLTR) {
            m_NVEncodeConfig.encodeCodecConfig.hevcConfig.enableLTR = 1;
            m_NVEncodeConfig.encodeCodecConfig.hevcConfig.ltrTrustMode = 0;

            m_ltrNumFrames = 0;
            m_pNvEncoderCuda->GetCapabilityValue(NV_ENC_CODEC_HEVC_GUID, NV_ENC_CAPS_NUM_MAX_LTR_FRAMES, &m_ltrNumFrames);
            m_NVEncodeConfig.encodeCodecConfig.hevcConfig.ltrNumFrames = m_ltrNumFrames;

            if (m_NVEncodeConfig.encodeCodecConfig.hevcConfig.ltrNumFrames <= 0)
            {
                m_NVEncodeConfig.encodeCodecConfig.hevcConfig.enableLTR = 0;
                m_NVEncodeConfig.encodeCodecConfig.hevcConfig.ltrTrustMode = 0;
            }
        }

        m_TemporalSVCCap.is_support_temporal_svc = false;
        if (m_config.isEnableTemporalSVC) {
            RTC_LOG(LS_WARNING) << "not support svc for nvenc hevc";
        }
    }
    m_NVEncodeConfig.rcParams.rateControlMode = NV_ENC_PARAMS_RC_CBR;
    m_NVEncodeConfig.rcParams.multiPass = NV_ENC_TWO_PASS_FULL_RESOLUTION;
    m_NVEncodeConfig.rcParams.lowDelayKeyFrameScale = 1;

    m_NVEncodeConfig.rcParams.averageBitRate = m_encoderBitrateBps;
    // m_NVEncodeConfig.rcParams.maxBitRate = m_NVEncodeConfig.rcParams.averageBitRate;
    // m_NVEncodeConfig.rcParams.vbvBufferSize = m_NVEncodeConfig.rcParams.averageBitRate;

    ret = m_pNvEncoderCuda->CreateEncoder(&m_NVInitializeParams);
    if (!ret)
    {
        RTC_LOG(LS_ERROR) << "CreateEncoder Fail";
        delete m_pNvEncoderCuda;
        m_pNvEncoderCuda = NULL;
        return false;
    }

    m_pOutputBuffer = NULL;
    m_OutputBufferSize = 0;

	RTC_LOG(LS_INFO) << "beginEncode success.";

    if (m_bIsEnablePerf)
    {
        m_NvPerf.init();
    }

    m_bEncoderInited = true;

    return true;
}

bool NvidiaVideoEncoder::flushEncode()
{
    std::vector<NvEncOutputPacket> vPacket;
    bool ret = m_pNvEncoderCuda->EndEncode(vPacket);
    if (!ret) {
        RTC_LOG(LS_WARNING) << "EndEncode fail";
        return false;
    }

    for (NvEncOutputPacket &packet : vPacket)
    {
        if (!packet.buffer.empty() && packet.buffer.size()>0)
        {
            m_pVideoPackets.push_back(packet);
        }
    }

    return true;
}

bool NvidiaVideoEncoder::addRawFrame(const VideoRawFrame &frame)
{
    void* srcPlanePtrs[4];
    uint32_t srcPlanePitchs[4];

    int planes = 3;
    if (frame.format == FOURCC_I420)
    {
        planes = 3;
    }else if(frame.format == FOURCC_NV12)
    {
        planes = 2;
    }

    for (int i = 0; i < planes; i++)
    {
        srcPlanePtrs[i] = frame.planeptrs[i].ptr;
        srcPlanePitchs[i] = frame.strides[i];
    }

    NV_ENC_PIC_PARAMS picParams = {NV_ENC_PIC_PARAMS_VER};
    picParams.version = (nvencapi_struct_version(nvencapi_version, 4) | ( 1u<<31 ));
    picParams.encodePicFlags = 0;
    picParams.inputTimeStamp = frame.timestamp;
    if (mCodecId == NV_ENC_CODEC_H264_GUID) {
	    picParams.codecPicParams.h264PicParams.sliceMode = 3;
	    picParams.codecPicParams.h264PicParams.sliceModeData = 1;
    }else {
        picParams.codecPicParams.hevcPicParams.sliceMode = 3;
	    picParams.codecPicParams.hevcPicParams.sliceModeData = 1;
    }

    const NvEncInputFrame* encoderInputFrame =  m_pNvEncoderCuda->GetNextInputFrame();
    if(encoderInputFrame == NULL) {
        RTC_LOG(LS_ERROR) << "GetNextInputFrame Return NULL";
        return false;
    }
    
    bool ret = NvEncoderCuda::CopyToDeviceFrame(m_CudaDll, m_CuContext,
                srcPlanePtrs,
                srcPlanePitchs,
                (CUdeviceptr)encoderInputFrame->inputPtr,
                (int)encoderInputFrame->pitch,
                m_pNvEncoderCuda->GetEncodeWidth(),
                m_pNvEncoderCuda->GetEncodeHeight(),
                CU_MEMORYTYPE_HOST,
                encoderInputFrame->bufferFormat,
                encoderInputFrame->chromaOffsets,
                encoderInputFrame->numChromaPlanes);
    if (!ret) {
        RTC_LOG(LS_ERROR) << "CopyToDeviceFrame fail";
        return false;
    }
    
    if(m_bResetBitrate || m_bForceIntraFrame || m_bResetFramerate || m_bIsEnableLTR != frame.isEnableLTR || m_bResetTemporalSVC) {
        NV_ENC_RECONFIGURE_PARAMS reconfigureParams = {NV_ENC_RECONFIGURE_PARAMS_VER};
        reconfigureParams.version = (nvencapi_struct_version(nvencapi_version, 1) | ( 1u<<31 ));
        memcpy(&reconfigureParams.reInitEncodeParams, &m_NVInitializeParams, sizeof(m_NVInitializeParams));
        NV_ENC_CONFIG reInitCodecConfig = { NV_ENC_CONFIG_VER };
        reInitCodecConfig.version = (nvencapi_struct_version(nvencapi_version, 7) | ( 1u<<31 ));
        memcpy(&reInitCodecConfig, m_NVInitializeParams.encodeConfig, sizeof(reInitCodecConfig));
        reconfigureParams.reInitEncodeParams.encodeConfig = &reInitCodecConfig;

        if (m_bResetBitrate)
        {
            m_encoderBitrateBps = m_pBitrateAdjuster->GetAdjustedBitrateBps();
            m_NVEncodeConfig.rcParams.averageBitRate = m_encoderBitrateBps;
            // m_NVEncodeConfig.rcParams.maxBitRate = m_NVEncodeConfig.rcParams.averageBitRate;
            // m_NVEncodeConfig.rcParams.vbvBufferSize = m_NVEncodeConfig.rcParams.averageBitRate;
            reconfigureParams.reInitEncodeParams.encodeConfig->rcParams.averageBitRate = m_NVEncodeConfig.rcParams.averageBitRate;
            // reconfigureParams.reInitEncodeParams.encodeConfig->rcParams.maxBitRate = m_NVEncodeConfig.rcParams.maxBitRate;
            // reconfigureParams.reInitEncodeParams.encodeConfig->rcParams.vbvBufferSize = m_NVEncodeConfig.rcParams.vbvBufferSize;
        }

        if (m_bResetFramerate)
        {
            m_NVInitializeParams.frameRateNum = m_config.fps;
            m_NVInitializeParams.frameRateDen = 1;

            reconfigureParams.reInitEncodeParams.frameRateNum = m_NVInitializeParams.frameRateNum;
            reconfigureParams.reInitEncodeParams.frameRateDen = 1;
        }

        if (m_bForceIntraFrame)
        {
            reconfigureParams.forceIDR = true;
        }

        if (m_bForceIntraFrame)
        {
			picParams.encodePicFlags = NV_ENC_PIC_FLAG_FORCEIDR;
        }

        if (m_bIsEnableLTR != frame.isEnableLTR)
        {
            m_bIsEnableLTR = frame.isEnableLTR;
            if (frame.isEnableLTR) {
              if (mCodecId == NV_ENC_CODEC_H264_GUID) {
                m_NVEncodeConfig.encodeCodecConfig.h264Config.enableLTR = 1;
                m_NVEncodeConfig.encodeCodecConfig.h264Config.ltrTrustMode = 0;

                m_ltrNumFrames = 0;
                m_pNvEncoderCuda->GetCapabilityValue(
                    NV_ENC_CODEC_H264_GUID, NV_ENC_CAPS_NUM_MAX_LTR_FRAMES,
                    &m_ltrNumFrames);
                m_NVEncodeConfig.encodeCodecConfig.h264Config.ltrNumFrames =
                    m_ltrNumFrames;

                if (m_NVEncodeConfig.encodeCodecConfig.h264Config
                        .ltrNumFrames <= 0) {
                  m_NVEncodeConfig.encodeCodecConfig.h264Config.enableLTR = 0;
                  m_NVEncodeConfig.encodeCodecConfig.h264Config.ltrTrustMode =
                      0;
                }

                reconfigureParams.reInitEncodeParams.encodeConfig
                    ->encodeCodecConfig.h264Config.enableLTR =
                    m_NVEncodeConfig.encodeCodecConfig.h264Config.enableLTR;
                reconfigureParams.reInitEncodeParams.encodeConfig
                    ->encodeCodecConfig.h264Config.ltrTrustMode =
                    m_NVEncodeConfig.encodeCodecConfig.h264Config.ltrTrustMode;
              } else {
                m_NVEncodeConfig.encodeCodecConfig.hevcConfig.enableLTR = 1;
                m_NVEncodeConfig.encodeCodecConfig.hevcConfig.ltrTrustMode = 0;

                m_ltrNumFrames = 0;
                m_pNvEncoderCuda->GetCapabilityValue(
                    NV_ENC_CODEC_HEVC_GUID, NV_ENC_CAPS_NUM_MAX_LTR_FRAMES,
                    &m_ltrNumFrames);
                m_NVEncodeConfig.encodeCodecConfig.hevcConfig.ltrNumFrames =
                    m_ltrNumFrames;

                if (m_NVEncodeConfig.encodeCodecConfig.hevcConfig
                        .ltrNumFrames <= 0) {
                  m_NVEncodeConfig.encodeCodecConfig.hevcConfig.enableLTR = 0;
                  m_NVEncodeConfig.encodeCodecConfig.hevcConfig.ltrTrustMode =
                      0;
                }

                reconfigureParams.reInitEncodeParams.encodeConfig
                    ->encodeCodecConfig.hevcConfig.enableLTR =
                    m_NVEncodeConfig.encodeCodecConfig.hevcConfig.enableLTR;
                reconfigureParams.reInitEncodeParams.encodeConfig
                    ->encodeCodecConfig.hevcConfig.ltrTrustMode =
                    m_NVEncodeConfig.encodeCodecConfig.hevcConfig.ltrTrustMode;
              }
            } else {
              if (mCodecId == NV_ENC_CODEC_H264_GUID) {
                m_NVEncodeConfig.encodeCodecConfig.h264Config.enableLTR = 0;
                m_NVEncodeConfig.encodeCodecConfig.h264Config.ltrTrustMode = 0;

                reconfigureParams.reInitEncodeParams.encodeConfig
                    ->encodeCodecConfig.h264Config.enableLTR =
                    m_NVEncodeConfig.encodeCodecConfig.h264Config.enableLTR;
                reconfigureParams.reInitEncodeParams.encodeConfig
                    ->encodeCodecConfig.h264Config.ltrTrustMode =
                    m_NVEncodeConfig.encodeCodecConfig.h264Config.ltrTrustMode;
              } else {
                m_NVEncodeConfig.encodeCodecConfig.hevcConfig.enableLTR = 0;
                m_NVEncodeConfig.encodeCodecConfig.hevcConfig.ltrTrustMode = 0;

                reconfigureParams.reInitEncodeParams.encodeConfig
                    ->encodeCodecConfig.hevcConfig.enableLTR =
                    m_NVEncodeConfig.encodeCodecConfig.hevcConfig.enableLTR;
                reconfigureParams.reInitEncodeParams.encodeConfig
                    ->encodeCodecConfig.hevcConfig.ltrTrustMode =
                    m_NVEncodeConfig.encodeCodecConfig.hevcConfig.ltrTrustMode;
              }
            }
        }

        if (m_bResetTemporalSVC)
        {
            if (m_config.isEnableTemporalSVC) {
                if (mCodecId == NV_ENC_CODEC_H264_GUID) {
                    int is_support_temporal_svc = 0;
                    int num_max_temporal_layers = 0;
                    m_pNvEncoderCuda->GetCapabilityValue(NV_ENC_CODEC_H264_GUID, NV_ENC_CAPS_SUPPORT_TEMPORAL_SVC, &is_support_temporal_svc);
                    if (is_support_temporal_svc)
                    {
                        m_pNvEncoderCuda->GetCapabilityValue(NV_ENC_CODEC_H264_GUID, NV_ENC_CAPS_NUM_MAX_TEMPORAL_LAYERS, &num_max_temporal_layers);
                    }

                    if (is_support_temporal_svc && num_max_temporal_layers >= 2)
                    {
                        m_NVEncodeConfig.encodeCodecConfig.h264Config.enableTemporalSVC = 1;
                        m_NVEncodeConfig.encodeCodecConfig.h264Config.disableSVCPrefixNalu = 0;
                        m_NVEncodeConfig.encodeCodecConfig.h264Config.enableScalabilityInfoSEI = 0;
                        m_NVEncodeConfig.encodeCodecConfig.h264Config.numTemporalLayers = nv_min(m_config.numTemporalLayers, num_max_temporal_layers);
                        m_NVEncodeConfig.encodeCodecConfig.h264Config.maxTemporalLayers = num_max_temporal_layers;
                    
                        reconfigureParams.reInitEncodeParams.encodeConfig->encodeCodecConfig.h264Config.enableTemporalSVC = m_NVEncodeConfig.encodeCodecConfig.h264Config.enableTemporalSVC;
                        reconfigureParams.reInitEncodeParams.encodeConfig->encodeCodecConfig.h264Config.disableSVCPrefixNalu = m_NVEncodeConfig.encodeCodecConfig.h264Config.disableSVCPrefixNalu;
                        reconfigureParams.reInitEncodeParams.encodeConfig->encodeCodecConfig.h264Config.enableScalabilityInfoSEI = m_NVEncodeConfig.encodeCodecConfig.h264Config.enableScalabilityInfoSEI;
                        reconfigureParams.reInitEncodeParams.encodeConfig->encodeCodecConfig.h264Config.numTemporalLayers = m_NVEncodeConfig.encodeCodecConfig.h264Config.numTemporalLayers;
                        reconfigureParams.reInitEncodeParams.encodeConfig->encodeCodecConfig.h264Config.maxTemporalLayers = m_NVEncodeConfig.encodeCodecConfig.h264Config.maxTemporalLayers;

                        // m_TemporalSVCCap.is_support_temporal_svc = true;
                        // m_TemporalSVCCap.supported_max_temporal_layers = num_max_temporal_layers;
                        // m_TemporalSVCCap.is_support_dynamic_enable_temporal_svc = true;
                        // m_TemporalSVCCap.is_support_dynamic_set_bitrate_fraction = false;
                    }else {
                        m_NVEncodeConfig.encodeCodecConfig.h264Config.enableTemporalSVC = 0;
                        reconfigureParams.reInitEncodeParams.encodeConfig->encodeCodecConfig.h264Config.enableTemporalSVC = m_NVEncodeConfig.encodeCodecConfig.h264Config.enableTemporalSVC;

                        // m_TemporalSVCCap.is_support_temporal_svc = false;
                    }
                }else {
                    // m_TemporalSVCCap.is_support_temporal_svc = false;
                    RTC_LOG(LS_WARNING) << "not support svc for nvenc hevc";
                }
            }else {
                if (mCodecId == NV_ENC_CODEC_H264_GUID) {
                    m_NVEncodeConfig.encodeCodecConfig.h264Config.enableTemporalSVC = 0;
                    reconfigureParams.reInitEncodeParams.encodeConfig->encodeCodecConfig.h264Config.enableTemporalSVC = m_NVEncodeConfig.encodeCodecConfig.h264Config.enableTemporalSVC;

                    // m_TemporalSVCCap.is_support_temporal_svc = false;
                }else {
                    // m_TemporalSVCCap.is_support_temporal_svc = false;
                    RTC_LOG(LS_WARNING) << "not support svc for nvenc hevc";
                }
            }
        }
        
        bool ret = m_pNvEncoderCuda->Reconfigure(&reconfigureParams);
        if (!ret)
        {
            RTC_LOG(LS_ERROR) << "Reconfigure fail";
            return false;
        }
        
        m_bResetBitrate = false;
        m_bResetFramerate = false;
        m_bForceIntraFrame = false;
        m_bResetTemporalSVC = false;
    }
    
    if (m_bIsEnableLTR)
    {
        if (mCodecId == NV_ENC_CODEC_H264_GUID) {
            if (m_NVEncodeConfig.encodeCodecConfig.h264Config.enableLTR)
            {
	            picParams.codecPicParams.h264PicParams.ltrMarkFrame = frame.ltrMarkFrame?1:0;
                picParams.codecPicParams.h264PicParams.ltrUseFrames = frame.ltrUseFrame?1:0;
                picParams.codecPicParams.h264PicParams.ltrMarkFrameIdx = frame.ltrMarkFrameIdx;
                picParams.codecPicParams.h264PicParams.ltrUseFrameBitmap = ltrUseFrameIdxToLtrUseFrameBitmap(frame.ltrUseFrameIdx);
            }
        }else {
            if (m_NVEncodeConfig.encodeCodecConfig.hevcConfig.enableLTR)
            {
                picParams.codecPicParams.hevcPicParams.ltrMarkFrame = frame.ltrMarkFrame?1:0;
	            picParams.codecPicParams.hevcPicParams.ltrUseFrames = frame.ltrUseFrame?1:0;
                picParams.codecPicParams.hevcPicParams.ltrMarkFrameIdx = frame.ltrMarkFrameIdx;
                picParams.codecPicParams.hevcPicParams.ltrUseFrameBitmap = ltrUseFrameIdxToLtrUseFrameBitmap(frame.ltrUseFrameIdx);
            }
        }
    }
    
    std::vector<NvEncOutputPacket> vPacket;
    ret = m_pNvEncoderCuda->EncodeFrame(vPacket, &picParams);
    if (!ret)
    {
        RTC_LOG(LS_ERROR) << "EncodeFrame Fail";
        return false;
    }

    if (m_pNvEncoderCuda->GotError()) {
        RTC_LOG(LS_ERROR) << "NvEncoderCuda got error";
        return false;
    }

    for (NvEncOutputPacket &packet : vPacket)
    {
        if (!packet.buffer.empty() && packet.buffer.size()>0)
        {
            if (packet.buffer.size() > m_config.width*m_config.height*3/2)
            {
                RTC_LOG(LS_ERROR) << "EncodeFrame Fail. NvEncOutputPacket is Too big.";
                return false;
            }
            
            m_pVideoPackets.push_back(packet);
        }

        {
            std::vector<uint8_t> temp;
            packet.buffer.swap(temp);
        }
    }

    {
        std::vector<NvEncOutputPacket> temp;
        vPacket.swap(temp);
    }

    return true;
}

bool NvidiaVideoEncoder::getEncodedFrame(VideoEncodedFrame &frame)
{
    if (m_bIsEnablePerf)
    {
        float nv_gpu_usage = m_NvPerf.getGpuUsage();
        RTC_LOG(LS_INFO) << "nv_gpu_usage : " << nv_gpu_usage;
    }

    if (m_pVideoPackets.empty()) {
        RTC_LOG(LS_WARNING) << "No Filled OutBuffer";
        return false;
    }else {
        std::vector<NvEncOutputPacket>::iterator it = m_pVideoPackets.begin();
        NvEncOutputPacket outputPacket = *it;
        uint8_t* data = outputPacket.buffer.data();
        int size = outputPacket.buffer.size();
        if (m_OutputBufferSize<size)
        {
            if (m_pOutputBuffer)
            {
                delete [] m_pOutputBuffer;
                m_pOutputBuffer = NULL;
            }

            m_pOutputBuffer = new uint8_t[size];
            m_OutputBufferSize = size;
        }
        
        memcpy(m_pOutputBuffer, data, size);

        if (outputPacket.type == NV_ENC_PIC_TYPE_IDR)
        {
            frame.frameType = IDR_Frame;
        }else if(outputPacket.type == NV_ENC_PIC_TYPE_I)
        {
            frame.frameType = I_Frame;
        }else if(outputPacket.type == NV_ENC_PIC_TYPE_P)
        {
            frame.frameType = P_Frame;
        }else if(outputPacket.type == NV_ENC_PIC_TYPE_B)
        {
            frame.frameType = B_Frame;
        }else {
            frame.frameType = P_Frame;
        }
        
        frame.qp = outputPacket.qp;
        frame.pts = outputPacket.timeStamp;
        frame.dts = outputPacket.timeStamp;
        frame.isLTRFrame = outputPacket.isLTRFrame;
        frame.ltrFrameIdx = outputPacket.ltrFrameIdx;
        frame.ltrUseFrameIdx = ltrUseFrameBitmapToLtrUseFrameIdx(outputPacket.ltrFrameBitmap);
        if (m_NVEncodeConfig.encodeCodecConfig.h264Config.enableTemporalSVC)
        {
            frame.temporalId = outputPacket.temporalId;
        }
        
        // RTC_LOG(LS_INFO) << "nvidia qp : " << frame.qp;

        {
            std::vector<uint8_t> temp;
            outputPacket.buffer.swap(temp);
        }
        m_pVideoPackets.erase(it);

        frame.frameData.ptr = m_pOutputBuffer;
        frame.frameSize = size;
	    frame.standard = (mCodecId == NV_ENC_CODEC_HEVC_GUID) ? VideoStandard_ITU_H265 : VideoStandard_ITU_H264;

        frame.adjustedEncBitrateBps = m_pBitrateAdjuster->GetAdjustedBitrateBps();

        m_pBitrateAdjuster->ReportQP(frame.qp);
        m_pBitrateAdjuster->Update(frame.frameSize);

        return true;
    }
}

bool NvidiaVideoEncoder::endEncode()
{
    if (!m_bEncoderInited) return true;
    
    std::vector<NvEncOutputPacket> vPacket;
    if (m_pNvEncoderCuda)
    {
        m_pNvEncoderCuda->EndEncode(vPacket);
        m_pNvEncoderCuda->DestroyEncoder();
        delete m_pNvEncoderCuda;
        m_pNvEncoderCuda = NULL;
    }

    {
        std::vector<NvEncOutputPacket> temp;
        vPacket.swap(temp);
    }

    if (m_pOutputBuffer)
    {
        delete [] m_pOutputBuffer;
        m_pOutputBuffer = NULL;
    }
    m_OutputBufferSize = 0;

    {
        std::vector<NvEncOutputPacket> temp;
        m_pVideoPackets.swap(temp);
    }

    m_bEncoderInited = false;

    RTC_LOG(LS_INFO) << "endEncode success.";

    if (m_bIsEnablePerf)
    {
        m_NvPerf.release();
    }

    return true;
}

bool NvidiaVideoEncoder::reset()
{
    this->endEncode();
    return this->beginEncode();
}

bool NvidiaVideoEncoder::resetBitrate(uint32_t bitrateBps)
{
    m_pBitrateAdjuster->SetTargetBitrateBps(bitrateBps);

    if (m_encoderBitrateBps != m_pBitrateAdjuster->GetAdjustedBitrateBps())
	{
		m_bResetBitrate = true;
	}

	return true;
}

bool NvidiaVideoEncoder::resetFramerate(uint32_t fps)
{
    if (m_config.fps != fps)
    {
        m_config.fps = fps;
        m_bResetFramerate = true;
    }
    
    return true;
}

void NvidiaVideoEncoder::forceIntraFrame()
{
    m_bForceIntraFrame = true;
}

void NvidiaVideoEncoder::resetTemporalSVC(bool isEnableTemporalSVC, uint32_t numTemporalLayers)
{
    if (isEnableTemporalSVC != m_config.isEnableTemporalSVC || numTemporalLayers != m_config.numTemporalLayers)
    {
        m_config.isEnableTemporalSVC = isEnableTemporalSVC;
        m_config.numTemporalLayers = numTemporalLayers;

        m_bResetTemporalSVC = true;
    }
}

TemporalSVCCap NvidiaVideoEncoder::getTemporalSVCCap()
{
    return m_TemporalSVCCap;
}

HWQPThreshold NvidiaVideoEncoder::getHWQPThreshold()
{
    HWQPThreshold hwQPThreshold;

    if (mCodecId == NV_ENC_CODEC_HEVC_GUID) {
        hwQPThreshold.lowQpThreshold = 24;
        hwQPThreshold.highQpThreshold = 37;
    }else {
        hwQPThreshold.lowQpThreshold = 24;
        hwQPThreshold.highQpThreshold = 37;
    }

	return hwQPThreshold;
}

VIDEO_STATNDARD NvidiaVideoEncoder::getVideoStandard()
{
    if (mCodecId == NV_ENC_CODEC_HEVC_GUID)
		return VideoStandard_ITU_H265;
	else
		return VideoStandard_ITU_H264;
}

VIDEO_CODEC_PROVIDER NvidiaVideoEncoder::getCodecId()
{
    if (mCodecId == NV_ENC_CODEC_HEVC_GUID)
		return HW_CODEC_NVIDIA_HEVC;
	else
		return HW_CODEC_NVIDIA;
}

const char* NvidiaVideoEncoder::getCodecFriendlyName()
{
    return m_CodecFriendlyName.c_str();
}

CODEC_RETCODE CreateNvidiaVideoEncoder(VIDEO_CODEC_PROVIDER codec, HWVideoEncoder** ppVideoEncoder)
{
    RTC_LOG(LS_INFO) << "CODEC_RETCODE CreateNvidiaVideoEncoder";

	GUID codecId;
	if (codec == HW_CODEC_NVIDIA){
		codecId = NV_ENC_CODEC_H264_GUID;
		RTC_LOG(LS_INFO) << "[NvidiaVideoEncoder] H264";
	}
	else if (codec == HW_CODEC_NVIDIA_HEVC){
		codecId = NV_ENC_CODEC_HEVC_GUID;
		RTC_LOG(LS_INFO) << "[NvidiaVideoEncoder] HEVC";
	}
	else
		return CODEC_ERR_UNSUPPORTED;

	NvidiaVideoEncoder *encoder = new NvidiaVideoEncoder(codecId);
	if (encoder && encoder->init()) {
		*ppVideoEncoder = encoder;
		return CODEC_OK; // init succeeded
	}

	delete encoder;
	return CODEC_ERR_UNKNOWN;
}

} // namespace netease