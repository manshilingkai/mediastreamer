#include "modules/video_coding/codecs/winhw/nvidia/nvidia_videocodec_capability.h"
#include "third_party/nv-codec-headers/include/ffnvcodec/dynlink_loader.h"
#include "rtc_base/logging.h"

namespace netease {

bool createCudaContext(CudaFunctions *m_CudaDll, CUcontext* cuContext, int indexGpu, unsigned int flags)
{
    CUdevice cuDevice = 0;

    CUresult ret = m_CudaDll->cuDeviceGet(&cuDevice, indexGpu);
    if (ret != CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGet fail. error code : " << ret;
        return false;
    }
    
    char szDeviceName[80];
    ret = m_CudaDll->cuDeviceGetName(szDeviceName, sizeof(szDeviceName), cuDevice);
    if (ret != CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGetName fail. error code : " << ret;
        return false;
    }
    
    RTC_LOG(LS_INFO) << "GPU in use: " << szDeviceName;

    ret = m_CudaDll->cuCtxCreate(cuContext, flags, cuDevice);
    if (ret != CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuCtxCreate fail. error code : " << ret;
        return false;
    }

    return true;
}

void releaseNVEncode(CudaFunctions *m_CudaDll, NvencFunctions *m_NvEncDll, CUcontext m_CuContext, NV_ENCODE_API_FUNCTION_LIST *m_NvEncFuncs, void *hEncoder)
{
    if (m_NvEncFuncs)
    {
        if (hEncoder)
        {
            m_NvEncFuncs->nvEncDestroyEncoder(hEncoder);
            hEncoder = NULL;        
        }
    }

    if (m_CuContext)
    {
        CUresult ret = m_CudaDll->cuCtxDestroy(m_CuContext);
        if (ret != CUDA_SUCCESS)
        {
            RTC_LOG(LS_ERROR) << " cuCtxDestroy fail. error code : " << ret;
        }
    }

    if (m_NvEncDll)
    {
        nvenc_free_functions(&m_NvEncDll);
        m_NvEncDll = NULL;
    }

    if (m_CudaDll)
    {
        cuda_free_functions(&m_CudaDll);
        m_CudaDll = NULL;
    }
}

bool getNVEncodeCapability(bool &isSupportH264Encode, bool &isSupportH265Encode, 
                           int &supportedMaxInstanceForH264Encode, int &supportedMaxInstanceForH265Encode,
                           int &minSupportedWidthForH264Encode, int &minSupportedHeightForH264Encode,
                           int &maxSupportedWidthForH264Encode, int &maxSupportedHeightForH264Encode,
                           int &minSupportedWidthForH265Encode, int &minSupportedHeightForH265Encode,
                           int &maxSupportedWidthForH265Encode, int &maxSupportedHeightForH265Encode,
                           int &supportedMaxLTRFramesForH264, int &supportedMaxLTRFramesForH265,
                           int &supportedMaxTemporalLayersForH264)
{
    isSupportH264Encode = false;
    isSupportH265Encode = false;

    supportedMaxInstanceForH264Encode = -1;
    supportedMaxInstanceForH265Encode = -1;

    minSupportedWidthForH264Encode = -1;
    minSupportedHeightForH264Encode = -1;
    maxSupportedWidthForH264Encode = -1;
    maxSupportedHeightForH264Encode = -1;

    minSupportedWidthForH265Encode = -1;
    minSupportedHeightForH265Encode = -1;
    maxSupportedWidthForH265Encode = -1;
    maxSupportedHeightForH265Encode = -1;

    supportedMaxLTRFramesForH264 = -1;
    supportedMaxLTRFramesForH265 = -1;

    supportedMaxTemporalLayersForH264 = -1;

    CudaFunctions *m_CudaDll = NULL;
    NvencFunctions *m_NvEncDll = NULL;
    CUcontext m_CuContext = NULL;
	NV_ENCODE_API_FUNCTION_LIST m_NvEncFuncs  = { NV_ENCODE_API_FUNCTION_LIST_VER };
    void *hEncoder = NULL;

    int dll_ret = cuda_load_functions(&m_CudaDll, NULL);
    if (dll_ret < 0)
    {
        m_CudaDll = NULL;
        RTC_LOG(LS_ERROR) << "Could not dynamically load CUDA, ErroCode: " << dll_ret;
        releaseNVEncode(m_CudaDll, m_NvEncDll, m_CuContext, &m_NvEncFuncs, hEncoder);
        return false;
    }

    dll_ret = nvenc_load_functions(&m_NvEncDll, NULL);
    if (dll_ret < 0)
    {
        m_NvEncDll = NULL;
        RTC_LOG(LS_ERROR) << "Could not dynamically load NVENC, ErroCode: " << dll_ret;
        releaseNVEncode(m_CudaDll, m_NvEncDll, m_CuContext, &m_NvEncFuncs, hEncoder);
        return false;
    }

    NVENCSTATUS err;
    uint32_t nvenc_max_ver;
    err = m_NvEncDll->NvEncodeAPIGetMaxSupportedVersion(&nvenc_max_ver);
    if (err != NV_ENC_SUCCESS) {
        RTC_LOG(LS_ERROR) << "Failed to query nvenc max version, ErrorCode: " << err;
        releaseNVEncode(m_CudaDll, m_NvEncDll, m_CuContext, &m_NvEncFuncs, hEncoder);
        return false;
    }

    RTC_LOG(LS_INFO) << "Loaded Nvenc version " << (nvenc_max_ver >> 4) << "." << (nvenc_max_ver & 0xf);

    if ((NVENCAPI_MAJOR_VERSION << 4 | NVENCAPI_MINOR_VERSION) > nvenc_max_ver) {
        RTC_LOG(LS_ERROR) << "Driver does not support the required nvenc API version. " << "Required: " << NVENCAPI_MAJOR_VERSION << "." << NVENCAPI_MINOR_VERSION << " Found: " << (nvenc_max_ver >> 4) << "." << (nvenc_max_ver & 0xf);
        // releaseNVEncode(m_CudaDll, m_NvEncDll, m_CuContext, &m_NvEncFuncs, hEncoder);
        // return false;
    }

    RTC_LOG(LS_INFO) << "NVENCAPI_VERSION : " << NVENCAPI_VERSION;
    uint32_t nvencapi_version = nvenc_api_ver(nvenc_max_ver >> 4, nvenc_max_ver & 0xf);
    RTC_LOG(LS_INFO) << "nvencapi_version : " << nvencapi_version;
    m_NvEncFuncs.version = nvencapi_struct_version(nvencapi_version, 2);
    err = m_NvEncDll->NvEncodeAPICreateInstance(&m_NvEncFuncs);
    if (err != NV_ENC_SUCCESS) {
        RTC_LOG(LS_ERROR) << "Failed to create nvenc instance, ErrorCode: " << err;
        releaseNVEncode(m_CudaDll, m_NvEncDll, m_CuContext, &m_NvEncFuncs, hEncoder);
        return false;
    }

    CUresult ret = m_CudaDll->cuInit(0);
    if (ret!=CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuInit fail. error code : " << ret;
        releaseNVEncode(m_CudaDll, m_NvEncDll, m_CuContext, &m_NvEncFuncs, hEncoder);
        return false;
    }
    
    int gpu_count = 0;
    ret = m_CudaDll->cuDeviceGetCount(&gpu_count);
    if (ret!=CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGetCount fail. error code : " << ret;
        releaseNVEncode(m_CudaDll, m_NvEncDll, m_CuContext, &m_NvEncFuncs, hEncoder);
        return false;
    }

    if (gpu_count <= 0)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGetCount get gpu count is " << gpu_count;
        releaseNVEncode(m_CudaDll, m_NvEncDll, m_CuContext, &m_NvEncFuncs, hEncoder);
        return false;
    }

    bool b_ret = false;
    for (int gpu_index = 0; gpu_index < gpu_count; gpu_index++)
    {
        b_ret = createCudaContext(m_CudaDll, &m_CuContext, gpu_index, 0);
        if (!b_ret)
        {
            RTC_LOG(LS_WARNING) << " createCudaContext fail for gpu index : " << gpu_index;
        }else {
            RTC_LOG(LS_INFO) << " createCudaContext success for gpu index : " << gpu_index;
            break;
        }
    }
    
    if (!b_ret) {
        releaseNVEncode(m_CudaDll, m_NvEncDll, m_CuContext, &m_NvEncFuncs, hEncoder);
        return false;
    }

    NV_ENC_OPEN_ENCODE_SESSION_EX_PARAMS encodeSessionExParams = { NV_ENC_OPEN_ENCODE_SESSION_EX_PARAMS_VER };
    encodeSessionExParams.device = m_CuContext;
    encodeSessionExParams.deviceType = NV_ENC_DEVICE_TYPE_CUDA;
    encodeSessionExParams.apiVersion = nvencapi_version;
    encodeSessionExParams.version = nvencapi_struct_version(nvencapi_version, 1);
    NVENCSTATUS errorCode;
    if (m_NvEncFuncs.nvEncOpenEncodeSessionEx)
    {
        errorCode = m_NvEncFuncs.nvEncOpenEncodeSessionEx(&encodeSessionExParams, &hEncoder);
    }else if (m_NvEncFuncs.nvEncOpenEncodeSession)
    {
        errorCode = m_NvEncFuncs.nvEncOpenEncodeSession(m_CuContext, NV_ENC_DEVICE_TYPE_CUDA, &hEncoder);
    }else {
        errorCode = NV_ENC_ERR_UNIMPLEMENTED;
    }
    // NVENCSTATUS errorCode = m_NvEncFuncs.nvEncOpenEncodeSessionEx(&encodeSessionExParams, &hEncoder);
    if( errorCode != NV_ENC_SUCCESS) {
        if (errorCode == NV_ENC_ERR_INVALID_VERSION) {
            RTC_LOG(LS_ERROR) << "nvEncOpenEncodeSessionEx Or nvEncOpenEncodeSession Fail. Error Code : NV_ENC_ERR_INVALID_VERSION";
        }else {
            RTC_LOG(LS_ERROR) << "nvEncOpenEncodeSessionEx Or nvEncOpenEncodeSession Fail. Error Code : " << errorCode;
        }
        
        releaseNVEncode(m_CudaDll, m_NvEncDll, m_CuContext, &m_NvEncFuncs, hEncoder);
        return false;
    }

    NVENCSTATUS nvStatus = NV_ENC_SUCCESS;
    uint32_t encodeGUIDCount = 0;
    if (NV_ENC_SUCCESS != (nvStatus = m_NvEncFuncs.nvEncGetEncodeGUIDCount(hEncoder, &encodeGUIDCount))) {
        RTC_LOG(LS_ERROR) << "nvEncGetEncodeGUIDCount Fail";
        releaseNVEncode(m_CudaDll, m_NvEncDll, m_CuContext, &m_NvEncFuncs, hEncoder);
        return false;
    }

    uint32_t uArraysize = 0;
    GUID guid_init = { 0 };
    std::vector<GUID> list_codecs;
    list_codecs.resize(encodeGUIDCount, guid_init);
    if (NV_ENC_SUCCESS != (nvStatus = m_NvEncFuncs.nvEncGetEncodeGUIDs(hEncoder, &list_codecs[0], encodeGUIDCount, &uArraysize))) {
        RTC_LOG(LS_ERROR) << "nvEncGetEncodeGUIDs Fail";
        releaseNVEncode(m_CudaDll, m_NvEncDll, m_CuContext, &m_NvEncFuncs, hEncoder);
        return false;
    }

    // NV_ENC_CODEC_H264_GUID
    // NV_ENC_CODEC_HEVC_GUID
    for (auto codec : list_codecs) {
        if (codec == NV_ENC_CODEC_H264_GUID)
        {
          RTC_LOG(LS_INFO) << "Support NV_ENC_CODEC_H264_GUID";
          isSupportH264Encode = true;
        }
        if (codec == NV_ENC_CODEC_HEVC_GUID)
        {
          RTC_LOG(LS_INFO) << "Support NV_ENC_CODEC_HEVC_GUID";
          isSupportH265Encode = true;
        }
    }

    if (isSupportH264Encode)
    {
        NV_ENC_CAPS capsToQuery = NV_ENC_CAPS_SUPPORT_DYN_BITRATE_CHANGE;
        NV_ENC_CAPS_PARAM capsParam = { NV_ENC_CAPS_PARAM_VER };
        capsParam.capsToQuery = capsToQuery;
        capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
        int v = 0;
        nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_H264_GUID, &capsParam, &v);
        if (nvStatus == NV_ENC_SUCCESS && v == 0)
        {
            isSupportH264Encode = false;
            RTC_LOG(LS_WARNING) << "Dynamic Encode bitrate change not supported For H264";
        }

        if (isSupportH264Encode)
        {
            NV_ENC_CAPS capsToQuery = NV_ENC_CAPS_SUPPORTED_RATECONTROL_MODES;
            NV_ENC_CAPS_PARAM capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);

            int v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_H264_GUID, &capsParam, &v);
            if (nvStatus == NV_ENC_SUCCESS && (v & NV_ENC_PARAMS_RC_CBR) == 0)
            {
                isSupportH264Encode = false;
                RTC_LOG(LS_WARNING) << "CBR not supported For H264";
            }
        }

        if (isSupportH264Encode)
        {
            NV_ENC_CAPS capsToQuery = NV_ENC_CAPS_NUM_ENCODER_ENGINES;
            NV_ENC_CAPS_PARAM capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);

            int v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_H264_GUID, &capsParam, &v);
            RTC_LOG(LS_INFO) << " NV H264 Encoder Engines : " << v;
            if (nvStatus == NV_ENC_SUCCESS)
            {
                if (v <= 0)
                {
//                    isSupportH264Encode = false;
                }else {
                    supportedMaxInstanceForH264Encode = v;
                }
            }
        }

        if (isSupportH264Encode)
        {
            NV_ENC_CAPS capsToQuery = NV_ENC_CAPS_WIDTH_MIN;
            NV_ENC_CAPS_PARAM capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);

            int v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_H264_GUID, &capsParam, &v);
            if (nvStatus == NV_ENC_SUCCESS) {
                minSupportedWidthForH264Encode = v;
            }

            capsToQuery = NV_ENC_CAPS_HEIGHT_MIN;
            capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
            v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_H264_GUID, &capsParam, &v);
            if (nvStatus == NV_ENC_SUCCESS) {
                minSupportedHeightForH264Encode = v;
            }

            capsToQuery = NV_ENC_CAPS_WIDTH_MAX;
            capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
            v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_H264_GUID, &capsParam, &v);
            if (nvStatus == NV_ENC_SUCCESS) {
                maxSupportedWidthForH264Encode = v;
            }

            capsToQuery = NV_ENC_CAPS_HEIGHT_MAX;
            capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
            v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_H264_GUID, &capsParam, &v);
            if (nvStatus == NV_ENC_SUCCESS) {
                maxSupportedHeightForH264Encode = v;
            }
        }

        if (isSupportH264Encode)
        {
            NV_ENC_CAPS capsToQuery = NV_ENC_CAPS_NUM_MAX_LTR_FRAMES;
            NV_ENC_CAPS_PARAM capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
            int v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_H264_GUID, &capsParam, &v);
            if (nvStatus == NV_ENC_SUCCESS) {
                supportedMaxLTRFramesForH264 = v;
            }
        }

        if (isSupportH264Encode)
        {
            int is_support_temporal_svc = 0;
            NV_ENC_CAPS capsToQuery = NV_ENC_CAPS_SUPPORT_TEMPORAL_SVC;
            NV_ENC_CAPS_PARAM capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
            int v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_H264_GUID, &capsParam, &v);
            if (nvStatus == NV_ENC_SUCCESS) {
                is_support_temporal_svc = v;
            }

            if (is_support_temporal_svc)
            {
                NV_ENC_CAPS capsToQuery = NV_ENC_CAPS_NUM_MAX_TEMPORAL_LAYERS;
                NV_ENC_CAPS_PARAM capsParam = { NV_ENC_CAPS_PARAM_VER };
                capsParam.capsToQuery = capsToQuery;
                capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
                int v = 0;
                nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_H264_GUID, &capsParam, &v);
                if (nvStatus == NV_ENC_SUCCESS) {
                    supportedMaxTemporalLayersForH264 = v;
                }
            }
            RTC_LOG(LS_INFO) << "is_support_temporal_svc : " << is_support_temporal_svc << " supportedMaxTemporalLayersForH264 : " << supportedMaxTemporalLayersForH264;
        }
    }

    if (isSupportH265Encode)
    {
        NV_ENC_CAPS capsToQuery = NV_ENC_CAPS_SUPPORT_DYN_BITRATE_CHANGE;
        NV_ENC_CAPS_PARAM capsParam = { NV_ENC_CAPS_PARAM_VER };
        capsParam.capsToQuery = capsToQuery;
        capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
        int v = 0;
        nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_HEVC_GUID, &capsParam, &v);
        if (nvStatus == NV_ENC_SUCCESS && v == 0)
        {
            isSupportH265Encode = false;
            RTC_LOG(LS_WARNING) << "Dynamic Encode bitrate change not supported For H265";
        }

        if (isSupportH265Encode)
        {
            NV_ENC_CAPS capsToQuery = NV_ENC_CAPS_SUPPORTED_RATECONTROL_MODES;
            NV_ENC_CAPS_PARAM capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
            int v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_HEVC_GUID, &capsParam, &v);
            if (nvStatus == NV_ENC_SUCCESS && (v & NV_ENC_PARAMS_RC_CBR) == 0)
            {
                isSupportH265Encode = false;
                RTC_LOG(LS_WARNING) << "CBR not supported For H265";
            }
        }

        if (isSupportH265Encode)
        {
            NV_ENC_CAPS capsToQuery = NV_ENC_CAPS_NUM_ENCODER_ENGINES;
            NV_ENC_CAPS_PARAM capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
            int v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_HEVC_GUID, &capsParam, &v);
            RTC_LOG(LS_INFO) << " NV H265 Encoder Engines : " << v;
            if (nvStatus == NV_ENC_SUCCESS)
            {
                if (v <= 0)
                {
                    // isSupportH265Encode = false;
                }else {
                    supportedMaxInstanceForH265Encode = v;
                }
            }
        }

        if (isSupportH265Encode)
        {
            NV_ENC_CAPS capsToQuery = NV_ENC_CAPS_WIDTH_MIN;
            NV_ENC_CAPS_PARAM capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
            int v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_HEVC_GUID, &capsParam, &v);
            if (nvStatus == NV_ENC_SUCCESS) {
                minSupportedWidthForH265Encode = v;
            }

            capsToQuery = NV_ENC_CAPS_HEIGHT_MIN;
            capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
            v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_HEVC_GUID, &capsParam, &v);
            if (nvStatus == NV_ENC_SUCCESS) {
                minSupportedHeightForH265Encode = v;
            }

            capsToQuery = NV_ENC_CAPS_WIDTH_MAX;
            capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
            v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_HEVC_GUID, &capsParam, &v);
            if (nvStatus == NV_ENC_SUCCESS) {
                maxSupportedWidthForH265Encode = v;
            }

            capsToQuery = NV_ENC_CAPS_HEIGHT_MAX;
            capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
            v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_HEVC_GUID, &capsParam, &v);
            if (nvStatus == NV_ENC_SUCCESS) {
                maxSupportedHeightForH265Encode = v;
            }
        }

        if (isSupportH265Encode)
        {
            NV_ENC_CAPS capsToQuery = NV_ENC_CAPS_NUM_MAX_LTR_FRAMES;
            NV_ENC_CAPS_PARAM capsParam = { NV_ENC_CAPS_PARAM_VER };
            capsParam.capsToQuery = capsToQuery;
            capsParam.version = nvencapi_struct_version(nvencapi_version, 1);
            int v = 0;
            nvStatus = m_NvEncFuncs.nvEncGetEncodeCaps(hEncoder, NV_ENC_CODEC_HEVC_GUID, &capsParam, &v);
            if (nvStatus == NV_ENC_SUCCESS) {
                supportedMaxLTRFramesForH265 = v;
            }
        }
    }
    
    releaseNVEncode(m_CudaDll, m_NvEncDll, m_CuContext, &m_NvEncFuncs, hEncoder);
    return true;
}

void releaseNVDecode(CudaFunctions *m_CudaDll, CuvidFunctions *m_CuvidDll, CUcontext m_CuContext, CUvideoctxlock m_ctxLock)
{
    if (m_CudaDll)
    {
        m_CudaDll->cuCtxPopCurrent(NULL);
    }
    
    if (m_CuvidDll)
    {
        if (m_ctxLock)
        {
            m_CuvidDll->cuvidCtxLockDestroy(m_ctxLock);
        }
    }

    if (m_CuContext)
    {
        CUresult ret = m_CudaDll->cuCtxDestroy(m_CuContext);
        if (ret != CUDA_SUCCESS)
        {
            RTC_LOG(LS_ERROR) << " cuCtxDestroy fail. error code : " << ret;
        }
        m_CuContext = NULL;
    }

    if (m_CuvidDll)
    {
        cuvid_free_functions(&m_CuvidDll);
        m_CuvidDll = NULL;
    }

    if (m_CudaDll)
    {
        cuda_free_functions(&m_CudaDll);
        m_CudaDll = NULL;
    }
}

bool getNVDecodeCapability(bool &isSupportH264Decode, bool &isSupportH265Decode, 
                           int &supportedMaxInstanceForH264Decode, int &supportedMaxInstanceForH265Decode,
                           int &minSupportedWidthForH264Decode, int &minSupportedHeightForH264Decode,
                           int &maxSupportedWidthForH264Decode, int &maxSupportedHeightForH264Decode,
                           int &minSupportedWidthForH265Decode, int &minSupportedHeightForH265Decode,
                           int &maxSupportedWidthForH265Decode, int &maxSupportedHeightForH265Decode)
{
    isSupportH264Decode = false;
    isSupportH265Decode = false;

    supportedMaxInstanceForH264Decode = -1;
    supportedMaxInstanceForH265Decode = -1;

    minSupportedWidthForH264Decode = -1;
    minSupportedHeightForH264Decode = -1;
    maxSupportedWidthForH264Decode = -1;
    maxSupportedHeightForH264Decode = -1;

    minSupportedWidthForH265Decode = -1;
    minSupportedHeightForH265Decode = -1;
    maxSupportedWidthForH265Decode = -1;
    maxSupportedHeightForH265Decode = -1;

    CudaFunctions *m_CudaDll = NULL;
    CuvidFunctions *m_CuvidDll = NULL;
    CUcontext m_CuContext = NULL;
    CUvideoctxlock m_ctxLock = NULL;

    int dll_ret = cuda_load_functions(&m_CudaDll, NULL);
    if (dll_ret < 0)
    {
        m_CudaDll = NULL;
        RTC_LOG(LS_ERROR) << "Could not dynamically load CUDA, ErroCode: " << dll_ret;
        releaseNVDecode(m_CudaDll, m_CuvidDll, m_CuContext, m_ctxLock);
        return false;
    }
    dll_ret = cuvid_load_functions(&m_CuvidDll, NULL);
    if (dll_ret < 0)
    {
        m_CuvidDll = NULL;
        RTC_LOG(LS_ERROR) << "Could not dynamically load CUVID, ErroCode: " << dll_ret;
        releaseNVDecode(m_CudaDll, m_CuvidDll, m_CuContext, m_ctxLock);
        return false;
    }

    CUresult ret = m_CudaDll->cuInit(0);
    if (ret!=CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuInit fail. error code : " << ret;
        releaseNVDecode(m_CudaDll, m_CuvidDll, m_CuContext, m_ctxLock);
        return false;
    }
    
    int gpu_count = 0;
    ret = m_CudaDll->cuDeviceGetCount(&gpu_count);
    if (ret!=CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGetCount fail. error code : " << ret;
        releaseNVDecode(m_CudaDll, m_CuvidDll, m_CuContext, m_ctxLock);
        return false;
    }

    RTC_LOG(LS_INFO) << " cuDeviceGetCount get gpu count is " << gpu_count;

    if (gpu_count <= 0)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGetCount get gpu count is " << gpu_count;
        releaseNVDecode(m_CudaDll, m_CuvidDll, m_CuContext, m_ctxLock);
        return false;
    }

    bool b_ret = false;
    for (int gpu_index = 0; gpu_index < gpu_count; gpu_index++)
    {
        b_ret = createCudaContext(m_CudaDll, &m_CuContext, gpu_index, 0);
        if (!b_ret)
        {
            RTC_LOG(LS_WARNING) << " createCudaContext fail for gpu index : " << gpu_index;
        }else {
            RTC_LOG(LS_INFO) << " createCudaContext success for gpu index : " << gpu_index;
            break;
        }
    }
    
    if (!b_ret) {
        releaseNVDecode(m_CudaDll, m_CuvidDll, m_CuContext, m_ctxLock);
        return false;
    }

    CUresult errorCode = m_CuvidDll->cuvidCtxLockCreate(&m_ctxLock, m_CuContext);
    if( errorCode != CUDA_SUCCESS) {
        releaseNVDecode(m_CudaDll, m_CuvidDll, m_CuContext, m_ctxLock);
        return false;
    }

    CUresult err__ = m_CudaDll->cuCtxPushCurrent(m_CuContext);
    if (err__ != CUDA_SUCCESS)
    {
        releaseNVDecode(m_CudaDll, m_CuvidDll, m_CuContext, m_ctxLock);
        return false;
    }

    CUVIDDECODECAPS decodecaps;
    memset(&decodecaps, 0, sizeof(decodecaps));
    decodecaps.eCodecType = cudaVideoCodec_H264;
    decodecaps.eChromaFormat = cudaVideoChromaFormat_420;
    decodecaps.nBitDepthMinus8 = 0;
    errorCode = m_CuvidDll->cuvidGetDecoderCaps(&decodecaps);
    if( errorCode != CUDA_SUCCESS) {
        releaseNVDecode(m_CudaDll, m_CuvidDll, m_CuContext, m_ctxLock);
        return false;
    }

    isSupportH264Decode = decodecaps.bIsSupported;
    RTC_LOG(LS_INFO) << " NV isSupportH264Decode : " << isSupportH264Decode;
    RTC_LOG(LS_INFO) << " NV H264 nNumNVDECs : " << decodecaps.nNumNVDECs;
    if (isSupportH264Decode)
    {
        supportedMaxInstanceForH264Decode = decodecaps.nNumNVDECs;

        minSupportedWidthForH264Decode = decodecaps.nMinWidth;
        minSupportedHeightForH264Decode = decodecaps.nMinHeight;
        maxSupportedWidthForH264Decode = decodecaps.nMaxWidth;
        maxSupportedHeightForH264Decode = decodecaps.nMaxHeight;
    }

    memset(&decodecaps, 0, sizeof(decodecaps));
    decodecaps.eCodecType = cudaVideoCodec_HEVC;
    decodecaps.eChromaFormat = cudaVideoChromaFormat_420;
    decodecaps.nBitDepthMinus8 = 0;
    errorCode = m_CuvidDll->cuvidGetDecoderCaps(&decodecaps);
    if( errorCode != CUDA_SUCCESS) {
        releaseNVDecode(m_CudaDll, m_CuvidDll, m_CuContext, m_ctxLock);
        return false;
    }
    
    isSupportH265Decode = decodecaps.bIsSupported;
    RTC_LOG(LS_INFO) << " NV isSupportH265Decode : " << isSupportH265Decode;
    RTC_LOG(LS_INFO) << " NV H265 nNumNVDECs : " << decodecaps.nNumNVDECs;
    if (isSupportH265Decode)
    {
        supportedMaxInstanceForH265Decode = decodecaps.nNumNVDECs;

        minSupportedWidthForH265Decode = decodecaps.nMinWidth;
        minSupportedHeightForH265Decode = decodecaps.nMinHeight;
        maxSupportedWidthForH265Decode = decodecaps.nMaxWidth;
        maxSupportedHeightForH265Decode = decodecaps.nMaxHeight;
    }

    releaseNVDecode(m_CudaDll, m_CuvidDll, m_CuContext, m_ctxLock);

    return true;
}

HWVideoCodecCapability GetNvidiaVideoCodecCapability(bool isEnableHWVideoCodec)
{
    HWVideoCodecCapability nvidiaVideoCodecCapability;
    if (isEnableHWVideoCodec)
    {
        getNVEncodeCapability(nvidiaVideoCodecCapability.isSupportH264Encode, nvidiaVideoCodecCapability.isSupportH265Encode, 
                          nvidiaVideoCodecCapability.supportedMaxInstanceForH264Encode, nvidiaVideoCodecCapability.supportedMaxInstanceForH265Encode,
                          nvidiaVideoCodecCapability.minSupportedWidthForH264Encode, nvidiaVideoCodecCapability.minSupportedHeightForH264Encode,
                          nvidiaVideoCodecCapability.maxSupportedWidthForH264Encode, nvidiaVideoCodecCapability.maxSupportedHeightForH264Encode,
                          nvidiaVideoCodecCapability.minSupportedWidthForH265Encode, nvidiaVideoCodecCapability.minSupportedHeightForH265Encode,
                          nvidiaVideoCodecCapability.maxSupportedWidthForH265Encode, nvidiaVideoCodecCapability.maxSupportedHeightForH265Encode,
                          nvidiaVideoCodecCapability.supportedMaxLTRFramesForH264, nvidiaVideoCodecCapability.supportedMaxLTRFramesForH265,
                          nvidiaVideoCodecCapability.supportedMaxTemporalLayersForH264);
        getNVDecodeCapability(nvidiaVideoCodecCapability.isSupportH264Decode, nvidiaVideoCodecCapability.isSupportH265Decode, 
                          nvidiaVideoCodecCapability.supportedMaxInstanceForH264Decode, nvidiaVideoCodecCapability.supportedMaxInstanceForH265Decode,
                          nvidiaVideoCodecCapability.minSupportedWidthForH264Decode, nvidiaVideoCodecCapability.minSupportedHeightForH264Decode,
                          nvidiaVideoCodecCapability.maxSupportedWidthForH264Decode, nvidiaVideoCodecCapability.maxSupportedHeightForH264Decode,
                          nvidiaVideoCodecCapability.minSupportedWidthForH265Decode, nvidiaVideoCodecCapability.minSupportedHeightForH265Decode,
                          nvidiaVideoCodecCapability.maxSupportedWidthForH265Decode, nvidiaVideoCodecCapability.maxSupportedHeightForH265Decode);

    }else {
        nvidiaVideoCodecCapability.isSupportH264Encode = false;
        nvidiaVideoCodecCapability.isSupportH264Decode = false;
        nvidiaVideoCodecCapability.isSupportH265Encode = false;
        nvidiaVideoCodecCapability.isSupportH265Decode = false;
    }

    return nvidiaVideoCodecCapability;
}

NvidiaVideoCodecCapability &NvidiaVideoCodecCapability::GetInstance()
{
    static NvidiaVideoCodecCapability capability;
    return capability;
}

NvidiaVideoCodecCapability::NvidiaVideoCodecCapability()
{
    isEnableHWCodec = true;

    hwVideoCodecCapability = GetNvidiaVideoCodecCapability(isEnableHWCodec);
    
    isMonitorCodecInstance = false;
    usedMaxInstanceForH264Encode = 0;
    usedMaxInstanceForH264Decode = 0;
    usedMaxInstanceForH265Encode = 0;
    usedMaxInstanceForH265Decode = 0;
}

NvidiaVideoCodecCapability::~NvidiaVideoCodecCapability()
{
}

HWVideoCodecCapability NvidiaVideoCodecCapability::GetHWVideoCodecCapability()
{
    RTC_LOG(LS_INFO) << " isSupportH264Encode : " << hwVideoCodecCapability.isSupportH264Encode;
    RTC_LOG(LS_INFO) << " isSupportH264Decode : " << hwVideoCodecCapability.isSupportH264Decode;
    RTC_LOG(LS_INFO) << " isSupportH265Encode : " << hwVideoCodecCapability.isSupportH265Encode;
    RTC_LOG(LS_INFO) << " isSupportH265Decode : " << hwVideoCodecCapability.isSupportH265Decode;

    RTC_LOG(LS_INFO) << " supportedMaxInstanceForH264Encode : " << hwVideoCodecCapability.supportedMaxInstanceForH264Encode;
    RTC_LOG(LS_INFO) << " supportedMaxInstanceForH264Decode : " << hwVideoCodecCapability.supportedMaxInstanceForH264Decode;
    RTC_LOG(LS_INFO) << " supportedMaxInstanceForH265Encode : " << hwVideoCodecCapability.supportedMaxInstanceForH265Encode;
    RTC_LOG(LS_INFO) << " supportedMaxInstanceForH265Decode : " << hwVideoCodecCapability.supportedMaxInstanceForH265Decode;

    RTC_LOG(LS_INFO) << " minSupportedWidthForH264Encode : " << hwVideoCodecCapability.minSupportedWidthForH264Encode;
    RTC_LOG(LS_INFO) << " minSupportedHeightForH264Encode : " << hwVideoCodecCapability.minSupportedHeightForH264Encode;
    RTC_LOG(LS_INFO) << " maxSupportedWidthForH264Encode : " << hwVideoCodecCapability.maxSupportedWidthForH264Encode;
    RTC_LOG(LS_INFO) << " maxSupportedHeightForH264Encode : " << hwVideoCodecCapability.maxSupportedHeightForH264Encode;

    RTC_LOG(LS_INFO) << " minSupportedWidthForH264Decode : " << hwVideoCodecCapability.minSupportedWidthForH264Decode;
    RTC_LOG(LS_INFO) << " minSupportedHeightForH264Decode : " << hwVideoCodecCapability.minSupportedHeightForH264Decode;
    RTC_LOG(LS_INFO) << " maxSupportedWidthForH264Decode : " << hwVideoCodecCapability.maxSupportedWidthForH264Decode;
    RTC_LOG(LS_INFO) << " maxSupportedHeightForH264Decode : " << hwVideoCodecCapability.maxSupportedHeightForH264Decode;

    RTC_LOG(LS_INFO) << " minSupportedWidthForH265Encode : " << hwVideoCodecCapability.minSupportedWidthForH265Encode;
    RTC_LOG(LS_INFO) << " minSupportedHeightForH265Encode : " << hwVideoCodecCapability.minSupportedHeightForH265Encode;
    RTC_LOG(LS_INFO) << " maxSupportedWidthForH265Encode : " << hwVideoCodecCapability.maxSupportedWidthForH265Encode;
    RTC_LOG(LS_INFO) << " maxSupportedHeightForH265Encode : " << hwVideoCodecCapability.maxSupportedHeightForH265Encode;

    RTC_LOG(LS_INFO) << " minSupportedWidthForH265Decode : " << hwVideoCodecCapability.minSupportedWidthForH265Decode;
    RTC_LOG(LS_INFO) << " minSupportedHeightForH265Decode : " << hwVideoCodecCapability.minSupportedHeightForH265Decode;
    RTC_LOG(LS_INFO) << " maxSupportedWidthForH265Decode : " << hwVideoCodecCapability.maxSupportedWidthForH265Decode;
    RTC_LOG(LS_INFO) << " maxSupportedHeightForH265Decode : " << hwVideoCodecCapability.maxSupportedHeightForH265Decode;

    RTC_LOG(LS_INFO) << " maxEncodeBitrateBpsForH264 : " << hwVideoCodecCapability.maxEncodeBitrateBpsForH264;
    RTC_LOG(LS_INFO) << " maxEncodeBitrateBpsForH265 : " << hwVideoCodecCapability.maxEncodeBitrateBpsForH265;

    RTC_LOG(LS_INFO) << " supportedMaxLTRFramesForH264 : " << hwVideoCodecCapability.supportedMaxLTRFramesForH264;
    RTC_LOG(LS_INFO) << " supportedMaxLTRFramesForH265 : " << hwVideoCodecCapability.supportedMaxLTRFramesForH265;

    RTC_LOG(LS_INFO) << " supportedMaxTemporalLayersForH264 : " << hwVideoCodecCapability.supportedMaxTemporalLayersForH264;

    return hwVideoCodecCapability;
}

bool NvidiaVideoCodecCapability::refCodecInstance(bool isEncoder, bool isH264)
{
    if (!isMonitorCodecInstance) return true;
    
    capLock.lock();
    if (isEncoder && isH264)
    {
        if (hwVideoCodecCapability.supportedMaxInstanceForH264Encode >= 0 && usedMaxInstanceForH264Encode + 1 > hwVideoCodecCapability.supportedMaxInstanceForH264Encode)
        {
            capLock.unlock();
            return false;
        }
        
        usedMaxInstanceForH264Encode++;
        capLock.unlock();
        return true;
    }

    if (isEncoder && !isH264)
    {
        if (hwVideoCodecCapability.supportedMaxInstanceForH265Encode >= 0 && usedMaxInstanceForH265Encode + 1 > hwVideoCodecCapability.supportedMaxInstanceForH265Encode)
        {
            capLock.unlock();
            return false;
        }
        
        usedMaxInstanceForH265Encode++;
        capLock.unlock();
        return true;
    }

    if (!isEncoder && isH264)
    {
        if (hwVideoCodecCapability.supportedMaxInstanceForH264Decode >=0 && usedMaxInstanceForH264Decode + 1 > hwVideoCodecCapability.supportedMaxInstanceForH264Decode)
        {
            capLock.unlock();
            return false;
        }
        
        usedMaxInstanceForH264Decode++;
        capLock.unlock();
        return true;
    }

    if (!isEncoder && !isH264)
    {
        if (hwVideoCodecCapability.supportedMaxInstanceForH265Decode >= 0 && usedMaxInstanceForH265Decode + 1 > hwVideoCodecCapability.supportedMaxInstanceForH265Decode)
        {
            capLock.unlock();
            return false;
        }
        
        usedMaxInstanceForH265Decode++;
        capLock.unlock();
        return true;
    }

    capLock.unlock();
    return false;
}

void NvidiaVideoCodecCapability::unrefCodecInstance(bool isEncoder, bool isH264)
{
    if (!isMonitorCodecInstance) return;

    capLock.lock();
    if (isEncoder && isH264)
    {
        usedMaxInstanceForH264Encode--;
    }

    if (isEncoder && !isH264)
    {
        usedMaxInstanceForH265Encode--;
    }

    if (!isEncoder && isH264)
    {
        usedMaxInstanceForH264Decode--;
    }

    if (!isEncoder && !isH264)
    {
        usedMaxInstanceForH265Decode--;
    }
    capLock.unlock();
}

} // namespace netease