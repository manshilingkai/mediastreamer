#pragma once

#include "modules/video_coding/codecs/winhw/libhwcodec.h"
#include "modules/video_coding/codecs/winhw/nvidia/NvDecoder.h"

namespace netease {

class NvidiaVideoDecoder: public netease::HWVideoDecoder
{
public:
	NvidiaVideoDecoder(cudaVideoCodec codecId);
	~NvidiaVideoDecoder();

	// implement VideoDecoder interface
	bool init();
	bool setConfig(const VideoDecoderConfig &config);
	bool beginDecode();
	bool addEncodedFrame(const VideoEncodedFrame &frame);
	bool getRawFrame(VideoRawFrame &frame);
	bool endDecode();
	VIDEO_STATNDARD getVideoStandard();
	VIDEO_CODEC_PROVIDER getCodecId();
	const char* getCodecFriendlyName();
	const std::string getCodecGPUName();
private:
    /**
    *   @brief  Utility function to create CUDA context
    *   @param  cuContext - Pointer to CUcontext. Updated by this function.
    *   @param  iGpu      - Device number to get handle for
    */
    bool createCudaContext(CUcontext* cuContext, int indexGpu, unsigned int flags);
private:
    CudaFunctions *m_CudaDll;
	CuvidFunctions *m_CuvidDll;
    cudaVideoCodec m_CodecId;
    CUcontext m_CuContext;
    VideoDecoderConfig m_config;
    NvDecoder *m_pNvDecoder;
	bool m_bDecoderInited = false;
	std::string m_HWGPUName;
	std::string m_CodecFriendlyName;
};

CODEC_RETCODE CreateNvidiaVideoDecoder(VIDEO_CODEC_PROVIDER codec, HWVideoDecoder** ppVideoDecoder);

} // namespace netease