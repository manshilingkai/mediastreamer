#include "modules/video_coding/codecs/winhw/nvidia/nvidia_videodecoder.h"
#include "rtc_base/logging.h"
#include "modules/video_coding/codecs/winhw/win/gpu_info.h"
#include "modules/video_coding/codecs/winhw/nvidia/nvidia_videocodec_capability.h"

namespace netease {

NvidiaVideoDecoder::NvidiaVideoDecoder(cudaVideoCodec codecId)
{
    m_CodecId = codecId;

    m_CudaDll = NULL;
    m_CuvidDll = NULL;

    m_pNvDecoder = NULL;
    m_CuContext = NULL;

    m_bDecoderInited = false;

    m_HWGPUName = "NVIDIA";
    std::vector<GpuInfo>& available_gpuInfos = GPUInfoHandler::GetInstance().GetAvailableGpuInfos();
	for (GpuInfo gpuInfo : available_gpuInfos) {
        if (gpuInfo.provider == GPU_NVIDIA) {
            m_HWGPUName = gpuInfo.name;
        }
    }

    if (m_CodecId == cudaVideoCodec_HEVC)
    {
        m_CodecFriendlyName = "H265 Decoder [" + m_HWGPUName + "]";
    }else {
        m_CodecFriendlyName = "H264 Decoder [" + m_HWGPUName + "]";
    }
}

NvidiaVideoDecoder::~NvidiaVideoDecoder()
{
    this->endDecode();

    if (m_CuContext)
    {
        CUresult ret = m_CudaDll->cuCtxDestroy(m_CuContext);
        if (ret != CUDA_SUCCESS)
        {
            RTC_LOG(LS_ERROR) << " cuCtxDestroy fail. error code : " << ret;
        }
        m_CuContext = NULL;
    }

    if (m_CuvidDll)
    {
        cuvid_free_functions(&m_CuvidDll);
        m_CuvidDll = NULL;
    }

    if (m_CudaDll)
    {
        cuda_free_functions(&m_CudaDll);
        m_CudaDll = NULL;
    }
}

// implement VideoDecoder interface
bool NvidiaVideoDecoder::init()
{
    int dll_ret = cuda_load_functions(&m_CudaDll, NULL);
    if (dll_ret < 0)
    {
        m_CudaDll = NULL;
        RTC_LOG(LS_ERROR) << "Could not dynamically load CUDA, ErroCode: " << dll_ret;
        return false;
    }
    dll_ret = cuvid_load_functions(&m_CuvidDll, NULL);
    if (dll_ret < 0)
    {
        m_CuvidDll = NULL;
        RTC_LOG(LS_ERROR) << "Could not dynamically load CUVID, ErroCode: " << dll_ret;
        return false;
    }

    CUresult ret = m_CudaDll->cuInit(0);
    if (ret!=CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuInit fail. error code : " << ret;
        return false;
    }
    
    int gpu_count = 0;
    ret = m_CudaDll->cuDeviceGetCount(&gpu_count);
    if (ret!=CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGetCount fail. error code : " << ret;
        return false;
    }

    if (gpu_count <= 0)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGetCount get gpu count is " << gpu_count;
        return false;
    }

    bool b_ret = false;
    for (int gpu_index = 0; gpu_index < gpu_count; gpu_index++)
    {
        b_ret = createCudaContext(&m_CuContext, gpu_index, 0);
        if (!b_ret)
        {
            RTC_LOG(LS_WARNING) << " createCudaContext fail for gpu index : " << gpu_index;
        }else {
            RTC_LOG(LS_INFO) << " createCudaContext success for gpu index : " << gpu_index;
            break;
        }
    }
    
    if (!b_ret) return false;

    RTC_LOG(LS_INFO) << "init success";
    
    return true;
}

bool NvidiaVideoDecoder::createCudaContext(CUcontext* cuContext, int indexGpu, unsigned int flags)
{
    CUdevice cuDevice = 0;

    CUresult ret = m_CudaDll->cuDeviceGet(&cuDevice, indexGpu);
    if (ret != CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGet fail. error code : " << ret;
        return false;
    }
    
    char szDeviceName[80];
    ret = m_CudaDll->cuDeviceGetName(szDeviceName, sizeof(szDeviceName), cuDevice);
    if (ret != CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuDeviceGetName fail. error code : " << ret;
        return false;
    }
    
    RTC_LOG(LS_INFO) << "GPU in use: " << szDeviceName;

    ret = m_CudaDll->cuCtxCreate(cuContext, flags, cuDevice);
    if (ret != CUDA_SUCCESS)
    {
        RTC_LOG(LS_ERROR) << " cuCtxCreate fail. error code : " << ret;
        return false;
    }

    return true;
}

bool NvidiaVideoDecoder::setConfig(const VideoDecoderConfig &config)
{
    m_config = config;
    return true;
}

bool NvidiaVideoDecoder::beginDecode()
{
    if(m_bDecoderInited) return true;

	// if (m_CodecId == cudaVideoCodec_H264) {
    //     HWVideoCodecCapability nvidiaHWVideoCodecCapability = NvidiaVideoCodecCapability::GetInstance().GetHWVideoCodecCapability();

	//     int min_width_limit = 0;
	//     int min_height_limit = 0;
    //     GetLimitedResolutionFromString(m_config.minResLimit, min_width_limit, min_height_limit);

	//     uint32_t minSupportedWidth = 0;
	//     uint32_t minSupportedHeight = 0;
	//     if (min_width_limit<=0 || min_height_limit<=0)
	//     {
	// 	    minSupportedWidth = nvidiaHWVideoCodecCapability.minSupportedWidthForH264Decode;
	// 	    minSupportedHeight = nvidiaHWVideoCodecCapability.minSupportedHeightForH264Decode;
	//     }else {
	// 	    minSupportedWidth = min_width_limit+1;
	// 	    minSupportedHeight = min_height_limit+1;
	//     }

	//     if (m_config.width < minSupportedWidth || m_config.height < minSupportedHeight)
	//     {
    //         RTC_LOG(LS_WARNING) << "Min Res Limited. minSupportedWidth : " << minSupportedWidth << " minSupportedHeight : " << minSupportedHeight;
	// 	    return false;
	//     }
    // }

    CUcontext cuContext = m_CuContext;
    bool bUseDeviceFrame = false;
    cudaVideoCodec eCodec= m_CodecId;
    bool bLowLatency = true;
    bool bDeviceFramePitched = false;
    const Rect *pCropRect = NULL;
    const Dim *pResizeDim = NULL;
    int maxWidth = m_config.width;
    int maxHeight = m_config.height;
    unsigned int clkRate = 1000;
    bool force_zero_latency = true;

    m_pNvDecoder = new NvDecoder(m_CudaDll, m_CuvidDll, cuContext, bUseDeviceFrame, eCodec, bLowLatency, 
                                 bDeviceFramePitched, pCropRect, pResizeDim, 
                                 maxWidth, maxHeight, clkRate, force_zero_latency);
    if (m_pNvDecoder->GotError()) {
        delete m_pNvDecoder;
        m_pNvDecoder = NULL;

        RTC_LOG(LS_ERROR) << "NvDecoder got error";
        return false;
    }
    
    RTC_LOG(LS_INFO) << "beginDecode success";

    m_bDecoderInited = true;

    return true;
}

bool NvidiaVideoDecoder::addEncodedFrame(const VideoEncodedFrame &frame)
{
    if (!m_bDecoderInited) {
        RTC_LOG(LS_WARNING) << "Please Call beginDecode()";
        return true;
    }
    
    const uint8_t *pData = frame.frameData.ptr;
    int nSize = frame.frameSize;
    int nFlags = CUVID_PKT_ENDOFPICTURE;
    int64_t nTimestamp = frame.pts;

    m_pNvDecoder->Decode(pData, nSize, nFlags, nTimestamp);
    if (m_pNvDecoder->GotError()) {
        RTC_LOG(LS_ERROR) << "NvDecoder got error";
        return false;
    }

    return true;
}

bool NvidiaVideoDecoder::getRawFrame(VideoRawFrame &frame)
{
    if (!m_bDecoderInited) {
        RTC_LOG(LS_WARNING) << "Please Call beginDecode()";
        return true;
    }

    if (m_pNvDecoder->GotError()) {
        RTC_LOG(LS_ERROR) << "NvDecoder got error";
        return false;
    }

    if (m_pNvDecoder->GetDecodeWidth()<=0)
    {
		RTC_LOG(LS_WARNING) << "m_pNvDecoder->GetDecodeWidth() : " << m_pNvDecoder->GetDecodeWidth();
		return false;
    }

    int64_t timestamp = 0;
    uint8_t* pData = m_pNvDecoder->GetFrame(&timestamp);
    int size = m_pNvDecoder->GetFrameSize();

    if (pData == NULL || size<=0)
    {
		RTC_LOG(LS_WARNING) << "no output VideoRawFrame";
		return false;
    }

    frame.width = m_pNvDecoder->GetDecodeWidth() * m_pNvDecoder->GetBPP();
    frame.height = m_pNvDecoder->GetHeight();
    frame.timestamp = timestamp;

    cudaVideoSurfaceFormat outputFormat = m_pNvDecoder->GetOutputFormat();
    if (outputFormat == cudaVideoSurfaceFormat_NV12 || outputFormat == cudaVideoSurfaceFormat_P016)
    {
		frame.format = FOURCC_NV12;
        frame.planeptrs[0].ptr = pData;
        frame.strides[0] = m_pNvDecoder->GetWidth() * m_pNvDecoder->GetBPP();
        frame.planeptrs[1].ptr = pData + m_pNvDecoder->GetLumaPlaneSize();
        frame.strides[1] = frame.strides[0];
    }else {
		frame.format = FOURCC_I444;
        frame.planeptrs[0].ptr = pData;
        frame.strides[0] = m_pNvDecoder->GetWidth() * m_pNvDecoder->GetBPP();
        frame.planeptrs[1].ptr = pData + m_pNvDecoder->GetLumaPlaneSize();
        frame.strides[1] = frame.strides[0];
        frame.planeptrs[2].ptr = pData + m_pNvDecoder->GetLumaPlaneSize() + m_pNvDecoder->GetChromaPlaneSize() / m_pNvDecoder->GetNumChromaPlanes();
        frame.strides[2] = frame.strides[0];
    }

    return true;
}

bool NvidiaVideoDecoder::endDecode()
{
    if(!m_bDecoderInited) return true;

    if (m_pNvDecoder)
    {
        delete m_pNvDecoder;
        m_pNvDecoder = NULL;
    }

    m_bDecoderInited = false;
    
    return true;
}

VIDEO_STATNDARD NvidiaVideoDecoder::getVideoStandard()
{
	if (m_CodecId == cudaVideoCodec_HEVC)
		return VideoStandard_ITU_H265;
	else
		return VideoStandard_ITU_H264;
}

VIDEO_CODEC_PROVIDER NvidiaVideoDecoder::getCodecId()
{
	if (m_CodecId == cudaVideoCodec_HEVC)
		return HW_CODEC_NVIDIA_HEVC;
	else
		return HW_CODEC_NVIDIA;
}

const char* NvidiaVideoDecoder::getCodecFriendlyName()
{
    return m_CodecFriendlyName.c_str();
}

const std::string NvidiaVideoDecoder::getCodecGPUName()
{
    return m_HWGPUName;
}

CODEC_RETCODE CreateNvidiaVideoDecoder(VIDEO_CODEC_PROVIDER codec, HWVideoDecoder** ppVideoDecoder)
{
    RTC_LOG(LS_INFO) << "CODEC_RETCODE CreateNvidiaVideoDecoder";

	cudaVideoCodec codecId;
	if (codec == HW_CODEC_NVIDIA){
		codecId = cudaVideoCodec_H264;
		RTC_LOG(LS_INFO) << "[NvidiaVideoDecoder] H264";
	}
	else if (codec == HW_CODEC_NVIDIA_HEVC){
		codecId = cudaVideoCodec_HEVC;
		RTC_LOG(LS_INFO) << "[NvidiaVideoDecoder] HEVC";
	}
	else
		return CODEC_ERR_UNSUPPORTED;

	NvidiaVideoDecoder *decoder = new NvidiaVideoDecoder(codecId);
	if (decoder && decoder->init()) {
		*ppVideoDecoder = decoder;
		return CODEC_OK; // init succeeded
	}

	delete decoder;
	return CODEC_ERR_UNKNOWN;
}

} // namespace netease