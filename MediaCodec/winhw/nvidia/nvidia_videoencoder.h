#pragma once

#include "modules/video_coding/codecs/winhw/libhwcodec.h"
#include "modules/video_coding/codecs/winhw/nvidia/NvEncoderCuda.h"
#include "common_video/include/bitrate_adjuster.h"
#include "modules/video_coding/codecs/winhw/perf/nv/nv_perf.h"

namespace netease {

class NvidiaVideoEncoder: public netease::HWVideoEncoder
{
public:
	NvidiaVideoEncoder(GUID codecId);
	~NvidiaVideoEncoder();

	// implement VideoEncoder interface
	bool init();
	bool setConfig(const VideoEncoderConfig &config);
	bool beginEncode();
	bool flushEncode();
	bool addRawFrame(const VideoRawFrame &frame);
	bool getEncodedFrame(VideoEncodedFrame &frame);
	bool endEncode();
	bool reset();
	bool resetBitrate(uint32_t bitrateBps);
	bool resetFramerate(uint32_t fps);
	void forceIntraFrame();
	void resetTemporalSVC(bool isEnableTemporalSVC, uint32_t numTemporalLayers);
	TemporalSVCCap getTemporalSVCCap();
	HWQPThreshold getHWQPThreshold();
	VIDEO_STATNDARD getVideoStandard();
	VIDEO_CODEC_PROVIDER getCodecId();
	const char* getCodecFriendlyName();
private:
    /**
    *   @brief  Utility function to create CUDA context
    *   @param  cuContext - Pointer to CUcontext. Updated by this function.
    *   @param  iGpu      - Device number to get handle for
    */
    bool createCudaContext(CUcontext* cuContext, int indexGpu, unsigned int flags);
	void nvencPrintDriverRequirement();
	uint32_t ltrUseFrameIdxToLtrUseFrameBitmap(int ltrUseFrameIdx);
	int ltrUseFrameBitmapToLtrUseFrameIdx(uint32_t ltrUseFrameBitmap);
private:
    CudaFunctions *m_CudaDll;
    NvencFunctions *m_NvEncDll;
	uint32_t nvencapi_version;
	uint32_t nvencapi_version_major;
	uint8_t nvencapi_version_minor;
	NV_ENCODE_API_FUNCTION_LIST m_NvEncFuncs;
    GUID mCodecId;
	VideoEncoderConfig m_config;
    CUcontext m_CuContext;
	NV_ENC_INITIALIZE_PARAMS m_NVInitializeParams;
	NV_ENC_CONFIG m_NVEncodeConfig;
    NvEncoderCuda* m_pNvEncoderCuda;
	bool m_bEncoderInited;
    std::vector<NvEncOutputPacket> m_pVideoPackets;
    uint8_t* m_pOutputBuffer;
    int m_OutputBufferSize;

	bool m_bForceIntraFrame;
	bool m_bResetBitrate;
	bool m_bResetFramerate;

	webrtc::BitrateAdjuster *m_pBitrateAdjuster;
	uint32_t m_encoderBitrateBps;
    
	bool m_bIsEnableLTR;
    int m_ltrNumFrames;

	bool m_bResetTemporalSVC;
    TemporalSVCCap m_TemporalSVCCap;
private:
    NvPerf m_NvPerf;
	const bool m_bIsEnablePerf;
	std::string m_HWGPUName;
	std::string m_CodecFriendlyName;
};

CODEC_RETCODE CreateNvidiaVideoEncoder(VIDEO_CODEC_PROVIDER codec, HWVideoEncoder** ppVideoEncoder);


} // namespace netease