#pragma once
#include "modules/video_coding/codecs/winhw/libhwcodec_capability.h"
#include <mutex>

namespace netease {

class NvidiaVideoCodecCapability
{
public:
    static NvidiaVideoCodecCapability &GetInstance();
    HWVideoCodecCapability GetHWVideoCodecCapability();
    bool refCodecInstance(bool isEncoder, bool isH264);
    void unrefCodecInstance(bool isEncoder, bool isH264);
private:
    NvidiaVideoCodecCapability();
    ~NvidiaVideoCodecCapability();
    NvidiaVideoCodecCapability(const NvidiaVideoCodecCapability &capability);
    const NvidiaVideoCodecCapability &operator=(const NvidiaVideoCodecCapability &capability);
    
    HWVideoCodecCapability hwVideoCodecCapability;

    bool isMonitorCodecInstance;
    int usedMaxInstanceForH264Encode;
    int usedMaxInstanceForH264Decode;
    int usedMaxInstanceForH265Encode;
    int usedMaxInstanceForH265Decode;
    std::mutex capLock;

    bool isEnableHWCodec;
};

} // namespace netease