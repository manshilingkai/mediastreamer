/*
* Copyright 2017-2021 NVIDIA Corporation.  All rights reserved.
*
* Please refer to the NVIDIA end user license agreement (EULA) associated
* with this source code for terms and conditions that govern your use of
* this software. Any use, reproduction, disclosure, or distribution of
* this software and related documentation outside the terms of the EULA
* is strictly prohibited.
*
*/

#include "modules/video_coding/codecs/winhw/nvidia/NvEncoderCuda.h"

NvEncoderCuda::NvEncoderCuda(CudaFunctions *cudaDll, CUcontext cuContext, uint32_t nvencapi_version_major, uint8_t nvencapi_version_minor, NV_ENCODE_API_FUNCTION_LIST* nvEncFuncs, uint32_t nWidth, uint32_t nHeight, NV_ENC_BUFFER_FORMAT eBufferFormat,
    uint32_t nExtraOutputDelay, bool bMotionEstimationOnly, bool bOutputInVideoMemory):
    NvEncoder(nvencapi_version_major, nvencapi_version_minor, nvEncFuncs, NV_ENC_DEVICE_TYPE_CUDA, cuContext, nWidth, nHeight, eBufferFormat, nExtraOutputDelay, bMotionEstimationOnly, bOutputInVideoMemory),
    m_CudaDll(cudaDll), m_cuContext(cuContext)
{
    if (!m_hEncoder) 
    {
        NVENC_THROW_ERROR("Encoder Initialization failed", NV_ENC_ERR_INVALID_DEVICE);
        m_bGotEncodeError = true;
        return;
    }

    if (!m_cuContext)
    {
        NVENC_THROW_ERROR("Invalid Cuda Context", NV_ENC_ERR_INVALID_DEVICE);
        m_bGotEncodeError = true;
        return;
    }
}

NvEncoderCuda::~NvEncoderCuda()
{
    // ReleaseCudaResources();
}

bool NvEncoderCuda::AllocateInputBuffers(int32_t numInputBuffers)
{
    // for MEOnly mode we need to allocate seperate set of buffers for reference frame
    int numCount = m_bMotionEstimationOnly ? 2 : 1;

    for (int count = 0; count < numCount; count++)
    {
        CUresult err = m_CudaDll->cuCtxPushCurrent(m_cuContext);
        CUDA_DRVAPI_CALL(m_CudaDll, err);
        if (err != CUDA_SUCCESS) {
            m_bGotEncodeError = true;
            return false;
        }
        
        std::vector<void*> inputFrames;
        for (int i = 0; i < numInputBuffers; i++)
        {
            CUdeviceptr pDeviceFrame;
            uint32_t chromaHeight = GetNumChromaPlanes(GetPixelFormat()) * GetChromaHeight(GetPixelFormat(), GetMaxEncodeHeight());
            if (GetPixelFormat() == NV_ENC_BUFFER_FORMAT_YV12 || GetPixelFormat() == NV_ENC_BUFFER_FORMAT_IYUV)
                chromaHeight = GetChromaHeight(GetPixelFormat(), GetMaxEncodeHeight());
            err = m_CudaDll->cuMemAllocPitch((CUdeviceptr *)&pDeviceFrame,
                &m_cudaPitch,
                GetWidthInBytes(GetPixelFormat(), GetMaxEncodeWidth()),
                GetMaxEncodeHeight() + chromaHeight, 16);
            CUDA_DRVAPI_CALL(m_CudaDll, err);
            if (err != CUDA_SUCCESS) {
                m_bGotEncodeError = true;
                return false;
            }
            inputFrames.push_back((void*)pDeviceFrame);
            if (count == 1) {
                recyclableReferenceFrames.push_back((void*)pDeviceFrame);
            }else {
                recyclableInputFrames.push_back((void*)pDeviceFrame);
            }
        }
        err = m_CudaDll->cuCtxPopCurrent(NULL);
        CUDA_DRVAPI_CALL(m_CudaDll, err);
        if (err != CUDA_SUCCESS) {
            m_bGotEncodeError = true;
            return false;
        }

        bool ret = RegisterInputResources(inputFrames,
            NV_ENC_INPUT_RESOURCE_TYPE_CUDADEVICEPTR,
            GetMaxEncodeWidth(),
            GetMaxEncodeHeight(),
            (int)m_cudaPitch,
            GetPixelFormat(),
            (count == 1) ? true : false);
        if (!ret)
        {
            RTC_LOG(LS_ERROR) << "RegisterInputResources fail";
            m_bGotEncodeError = true;
            return false;
        }
    }

    return true;
}

bool NvEncoderCuda::SetIOCudaStreams(NV_ENC_CUSTREAM_PTR inputStream, NV_ENC_CUSTREAM_PTR outputStream)
{
    if (!m_nvenc.nvEncSetIOCudaStreams) return false;

    NVENCSTATUS err = m_nvenc.nvEncSetIOCudaStreams(m_hEncoder, inputStream, outputStream);
    NVENC_API_CALL(err);
    if (err != NV_ENC_SUCCESS) {
        m_bGotEncodeError = true;
        return false;
    }
    return true;
}

void NvEncoderCuda::ReleaseInputBuffers()
{
    ReleaseCudaResources();
}

void NvEncoderCuda::ReleaseCudaResources()
{
    UnregisterInputResources();

    m_CudaDll->cuCtxPushCurrent(m_cuContext);

    for (uint32_t i = 0; i < recyclableInputFrames.size(); ++i)
    {
        if (recyclableInputFrames[i])
        {
            m_CudaDll->cuMemFree(reinterpret_cast<CUdeviceptr>(recyclableInputFrames[i]));
        }
    }
    recyclableInputFrames.clear();
    m_vInputFrames.clear();

    for (uint32_t i = 0; i < recyclableReferenceFrames.size(); ++i)
    {
        if (recyclableReferenceFrames[i])
        {
            m_CudaDll->cuMemFree(reinterpret_cast<CUdeviceptr>(recyclableReferenceFrames[i]));
        }
    }
    recyclableReferenceFrames.clear();
    m_vReferenceFrames.clear();

    m_CudaDll->cuCtxPopCurrent(NULL);
    m_cuContext = nullptr;
}

bool NvEncoderCuda::CopyToDeviceFrame(CudaFunctions *cudaDll, CUcontext device,
    void* srcPlanePtrs[],
    uint32_t srcPlanePitchs[],
    CUdeviceptr pDstFrame,
    uint32_t dstPitch,
    int width,
    int height,
    CUmemorytype srcMemoryType,
    NV_ENC_BUFFER_FORMAT pixelFormat,
    const uint32_t dstChromaOffsets[],
    uint32_t numChromaPlanes,
    bool bUnAlignedDeviceCopy,
    CUstream stream)
{
    if (srcMemoryType != CU_MEMORYTYPE_HOST && srcMemoryType != CU_MEMORYTYPE_DEVICE)
    {
        NVENC_THROW_ERROR("Invalid source memory type for copy", NV_ENC_ERR_INVALID_PARAM);
        return false;
    }

    CUresult err = cudaDll->cuCtxPushCurrent(device);
    CUDA_DRVAPI_CALL(cudaDll, err);
    if (err != CUDA_SUCCESS) {
        return false;
    }

    CUDA_MEMCPY2D m = { 0 };
    m.srcMemoryType = srcMemoryType;
    if (srcMemoryType == CU_MEMORYTYPE_HOST)
    {
        m.srcHost = srcPlanePtrs[0];
    }
    else
    {
        m.srcDevice = (CUdeviceptr)srcPlanePtrs[0];
    }
    m.srcPitch = srcPlanePitchs[0];
    m.dstMemoryType = CU_MEMORYTYPE_DEVICE;
    m.dstDevice = pDstFrame;
    m.dstPitch = dstPitch;
    m.WidthInBytes = NvEncoder::GetWidthInBytes(pixelFormat, width);
    m.Height = height;
    if (bUnAlignedDeviceCopy && srcMemoryType == CU_MEMORYTYPE_DEVICE)
    {
        CUresult err = cudaDll->cuMemcpy2DUnaligned(&m);
        CUDA_DRVAPI_CALL(cudaDll, err);
        if (err != CUDA_SUCCESS) {
            return false;
        }
    }
    else
    {
        CUresult err = stream == NULL? cudaDll->cuMemcpy2D(&m) : cudaDll->cuMemcpy2DAsync(&m, stream);
        CUDA_DRVAPI_CALL(cudaDll, err);
        if (err != CUDA_SUCCESS) {
            return false;
        }
    }

    uint32_t chromaHeight = NvEncoder::GetChromaHeight(pixelFormat, height);
    uint32_t destChromaPitch = NvEncoder::GetChromaPitch(pixelFormat, dstPitch);
    uint32_t chromaWidthInBytes = NvEncoder::GetChromaWidthInBytes(pixelFormat, width);

    for (uint32_t i = 0; i < numChromaPlanes; ++i)
    {
        if (chromaHeight)
        {
            if (srcMemoryType == CU_MEMORYTYPE_HOST)
            {
                m.srcHost = srcPlanePtrs[1+i];
            }
            else
            {
                m.srcDevice = (CUdeviceptr)(srcPlanePtrs[1+i]);
            }
            m.srcPitch = srcPlanePitchs[1+i];

            m.dstDevice = (CUdeviceptr)((uint8_t *)pDstFrame + dstChromaOffsets[i]);
            m.dstPitch = destChromaPitch;
            m.WidthInBytes = chromaWidthInBytes;
            m.Height = chromaHeight;
            if (bUnAlignedDeviceCopy && srcMemoryType == CU_MEMORYTYPE_DEVICE)
            {
                CUresult err = cudaDll->cuMemcpy2DUnaligned(&m);
                CUDA_DRVAPI_CALL(cudaDll, err);
                if (err != CUDA_SUCCESS) {
                    return false;
                }
            }
            else
            {
                CUresult err = stream == NULL? cudaDll->cuMemcpy2D(&m) : cudaDll->cuMemcpy2DAsync(&m, stream);
                CUDA_DRVAPI_CALL(cudaDll, err);
                if (err != CUDA_SUCCESS) {
                    return false;
                }
            }
        }
    }

    err = cudaDll->cuCtxPopCurrent(NULL);
    CUDA_DRVAPI_CALL(cudaDll, err);
    if (err != CUDA_SUCCESS) {
        return false;
    }

    return true;
}

bool NvEncoderCuda::CopyToDeviceFrame(CudaFunctions *cudaDll, CUcontext device,
    void* pSrcFrame,
    uint32_t nSrcPitch,
    CUdeviceptr pDstFrame,
    uint32_t dstPitch,
    int width,
    int height,
    CUmemorytype srcMemoryType,
    NV_ENC_BUFFER_FORMAT pixelFormat,
    const uint32_t dstChromaOffsets[],
    uint32_t numChromaPlanes,
    bool bUnAlignedDeviceCopy,
    CUstream stream)
{
    if (srcMemoryType != CU_MEMORYTYPE_HOST && srcMemoryType != CU_MEMORYTYPE_DEVICE)
    {
        NVENC_THROW_ERROR("Invalid source memory type for copy", NV_ENC_ERR_INVALID_PARAM);
        return false;
    }

    CUresult err = cudaDll->cuCtxPushCurrent(device);
    CUDA_DRVAPI_CALL(cudaDll, err);
    if (err != CUDA_SUCCESS) {
        return false;
    }

    uint32_t srcPitch = nSrcPitch ? nSrcPitch : NvEncoder::GetWidthInBytes(pixelFormat, width);
    CUDA_MEMCPY2D m = { 0 };
    m.srcMemoryType = srcMemoryType;
    if (srcMemoryType == CU_MEMORYTYPE_HOST)
    {
        m.srcHost = pSrcFrame;
    }
    else
    {
        m.srcDevice = (CUdeviceptr)pSrcFrame;
    }
    m.srcPitch = srcPitch;
    m.dstMemoryType = CU_MEMORYTYPE_DEVICE;
    m.dstDevice = pDstFrame;
    m.dstPitch = dstPitch;
    m.WidthInBytes = NvEncoder::GetWidthInBytes(pixelFormat, width);
    m.Height = height;
    if (bUnAlignedDeviceCopy && srcMemoryType == CU_MEMORYTYPE_DEVICE)
    {
        CUresult err = cudaDll->cuMemcpy2DUnaligned(&m);
        CUDA_DRVAPI_CALL(cudaDll, err);
        if (err != CUDA_SUCCESS) {
            return false;
        }
    }
    else
    {
        CUresult err = stream == NULL? cudaDll->cuMemcpy2D(&m) : cudaDll->cuMemcpy2DAsync(&m, stream);
        CUDA_DRVAPI_CALL(cudaDll, err);
        if (err != CUDA_SUCCESS) {
            return false;
        }
    }

    std::vector<uint32_t> srcChromaOffsets;
    NvEncoder::GetChromaSubPlaneOffsets(pixelFormat, srcPitch, height, srcChromaOffsets);
    uint32_t chromaHeight = NvEncoder::GetChromaHeight(pixelFormat, height);
    uint32_t destChromaPitch = NvEncoder::GetChromaPitch(pixelFormat, dstPitch);
    uint32_t srcChromaPitch = NvEncoder::GetChromaPitch(pixelFormat, srcPitch);
    uint32_t chromaWidthInBytes = NvEncoder::GetChromaWidthInBytes(pixelFormat, width);

    for (uint32_t i = 0; i < numChromaPlanes; ++i)
    {
        if (chromaHeight)
        {
            if (srcMemoryType == CU_MEMORYTYPE_HOST)
            {
                m.srcHost = ((uint8_t *)pSrcFrame + srcChromaOffsets[i]);
            }
            else
            {
                m.srcDevice = (CUdeviceptr)((uint8_t *)pSrcFrame + srcChromaOffsets[i]);
            }
            m.srcPitch = srcChromaPitch;

            m.dstDevice = (CUdeviceptr)((uint8_t *)pDstFrame + dstChromaOffsets[i]);
            m.dstPitch = destChromaPitch;
            m.WidthInBytes = chromaWidthInBytes;
            m.Height = chromaHeight;
            if (bUnAlignedDeviceCopy && srcMemoryType == CU_MEMORYTYPE_DEVICE)
            {
                CUresult err = cudaDll->cuMemcpy2DUnaligned(&m);
                CUDA_DRVAPI_CALL(cudaDll, err);
                if (err != CUDA_SUCCESS) {
                    return false;
                }
            }
            else
            {
                CUresult err = stream == NULL? cudaDll->cuMemcpy2D(&m) : cudaDll->cuMemcpy2DAsync(&m, stream);
                CUDA_DRVAPI_CALL(cudaDll, err);
                if (err != CUDA_SUCCESS) {
                    return false;
                }
            }
        }
    }

    err = cudaDll->cuCtxPopCurrent(NULL);
    CUDA_DRVAPI_CALL(cudaDll, err);
    if (err != CUDA_SUCCESS) {
        return false;
    }

    return true;
}

bool NvEncoderCuda::CopyToDeviceFrame(CudaFunctions *cudaDll, CUcontext device,
    void* pSrcFrame,
    uint32_t nSrcPitch,
    CUdeviceptr pDstFrame,
    uint32_t dstPitch,
    int width,
    int height,
    CUmemorytype srcMemoryType,
    NV_ENC_BUFFER_FORMAT pixelFormat,
    CUdeviceptr dstChromaDevicePtrs[],
    uint32_t dstChromaPitch,
    uint32_t numChromaPlanes,
    bool bUnAlignedDeviceCopy)
{
    if (srcMemoryType != CU_MEMORYTYPE_HOST && srcMemoryType != CU_MEMORYTYPE_DEVICE)
    {
        NVENC_THROW_ERROR("Invalid source memory type for copy", NV_ENC_ERR_INVALID_PARAM);
        return false;
    }

    CUresult err = cudaDll->cuCtxPushCurrent(device);
    CUDA_DRVAPI_CALL(cudaDll, err);
    if (err != CUDA_SUCCESS) {
        return false;
    }

    uint32_t srcPitch = nSrcPitch ? nSrcPitch : NvEncoder::GetWidthInBytes(pixelFormat, width);
    CUDA_MEMCPY2D m = { 0 };
    m.srcMemoryType = srcMemoryType;
    if (srcMemoryType == CU_MEMORYTYPE_HOST)
    {
        m.srcHost = pSrcFrame;
    }
    else
    {
        m.srcDevice = (CUdeviceptr)pSrcFrame;
    }
    m.srcPitch = srcPitch;
    m.dstMemoryType = CU_MEMORYTYPE_DEVICE;
    m.dstDevice = pDstFrame;
    m.dstPitch = dstPitch;
    m.WidthInBytes = NvEncoder::GetWidthInBytes(pixelFormat, width);
    m.Height = height;
    if (bUnAlignedDeviceCopy && srcMemoryType == CU_MEMORYTYPE_DEVICE)
    {
        CUresult err = cudaDll->cuMemcpy2DUnaligned(&m);
        CUDA_DRVAPI_CALL(cudaDll, err);
        if (err != CUDA_SUCCESS) {
            return false;
        }
    }
    else
    {
        CUresult err = cudaDll->cuMemcpy2D(&m);
        CUDA_DRVAPI_CALL(cudaDll, err);
        if (err != CUDA_SUCCESS) {
            return false;
        }
    }

    std::vector<uint32_t> srcChromaOffsets;
    NvEncoder::GetChromaSubPlaneOffsets(pixelFormat, srcPitch, height, srcChromaOffsets);
    uint32_t chromaHeight = NvEncoder::GetChromaHeight(pixelFormat, height);
    uint32_t srcChromaPitch = NvEncoder::GetChromaPitch(pixelFormat, srcPitch);
    uint32_t chromaWidthInBytes = NvEncoder::GetChromaWidthInBytes(pixelFormat, width);

    for (uint32_t i = 0; i < numChromaPlanes; ++i)
    {
        if (chromaHeight)
        {
            if (srcMemoryType == CU_MEMORYTYPE_HOST)
            {
                m.srcHost = ((uint8_t *)pSrcFrame + srcChromaOffsets[i]);
            }
            else
            {
                m.srcDevice = (CUdeviceptr)((uint8_t *)pSrcFrame + srcChromaOffsets[i]);
            }
            m.srcPitch = srcChromaPitch;

            m.dstDevice = dstChromaDevicePtrs[i];
            m.dstPitch = dstChromaPitch;
            m.WidthInBytes = chromaWidthInBytes;
            m.Height = chromaHeight;
            if (bUnAlignedDeviceCopy && srcMemoryType == CU_MEMORYTYPE_DEVICE)
            {
                CUresult err = cudaDll->cuMemcpy2DUnaligned(&m);
                CUDA_DRVAPI_CALL(cudaDll, err);
                if (err != CUDA_SUCCESS) {
                    return false;
                }
            }
            else
            {
                CUresult err = cudaDll->cuMemcpy2D(&m);
                CUDA_DRVAPI_CALL(cudaDll, err);
                if (err != CUDA_SUCCESS) {
                    return false;
                }
            }
        }
    }

    err = cudaDll->cuCtxPopCurrent(NULL);
    CUDA_DRVAPI_CALL(cudaDll, err);
    if (err != CUDA_SUCCESS) {
        return false;
    }

    return true;
}
