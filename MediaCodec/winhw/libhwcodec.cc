#include "modules/video_coding/codecs/winhw/libhwcodec.h"
#include "modules/video_coding/codecs/winhw/intel_qsv/intel_qsv_videoencoder.h"
#include "modules/video_coding/codecs/winhw/intel_qsv/intel_qsv_videodecoder.h"
#include "modules/video_coding/codecs/winhw/nvidia/nvidia_videodecoder.h"
#include "modules/video_coding/codecs/winhw/nvidia/nvidia_videoencoder.h"
#include "modules/video_coding/codecs/winhw/amd/amd_videodecoder.h"
#include "modules/video_coding/codecs/winhw/amd/amd_videoencoder.h"
#include "modules/video_coding/codecs/winhw/win/gpu_info.h"
#include "modules/video_coding/codecs/winhw/intel_qsv/intel_qsv_videocodec_capability.h"
#include "modules/video_coding/codecs/winhw/nvidia/nvidia_videocodec_capability.h"
#include "modules/video_coding/codecs/winhw/amd/amd_videocodec_capability.h"
#include "rtc_base/logging.h"
#include "rtc_base/stringutils.h"

namespace netease {

void GetResolutionFromString(std::string& s, int& width, int& height) {  
	width = 0;
	height = 0;
	std::vector<std::string> splited_res;
	std::string delimiter = "x";

	if (s.empty()) {
		RTC_LOG(LS_WARNING) << "GetResolutionFromString : resolution string is empty ";
		return;
	}

	rtc::str_split(s, splited_res, delimiter);
	if (splited_res.size() != 2) {
		RTC_LOG(LS_WARNING) << "GetResolutionFromString : the split result of resolution is illegal";
		return;
	}
	
	for (size_t i = 0; i < splited_res.size(); i++) {
		if (splited_res[i].empty() || splited_res[i].at(0) > '9' || splited_res[i].at(0) < '0') {
			RTC_LOG(LS_WARNING) << "GetResolutionFromString : the format of resolution is illegal";
			return;
		}
	}
	
	width = std::stoi(splited_res[0]);
	height = std::stoi(splited_res[1]);
}

VIDEO_CODEC_PROVIDER AutoSelectVideoCodec(bool isEncoder, bool isH264, int videoCodecWidth, int videoCodecHeight, int hwH26XCodecListStrategy, std::string hwH26XCodecAdaptationList, bool isEnableTemporalSVC = false, int numTemporalLayers = 2)
{
    std::vector<GpuInfo>& available_gpuInfos = GPUInfoHandler::GetInstance().GetAvailableGpuInfos();

	std::vector<std::string> splited_gpuInfos;
    std::string delimiter = ";";
	rtc::str_split(hwH26XCodecAdaptationList, splited_gpuInfos, delimiter);

	std::vector<GpuInfo> gpuInfos;
	for (GpuInfo gpuInfo : available_gpuInfos)
	{
		RTC_LOG(LS_INFO) << "GPU Name : " << gpuInfo.name;

	    bool isInAdaptationList = false;
	    for (size_t i = 0; i < splited_gpuInfos.size(); i++)
	    {
		    if (splited_gpuInfos[i].compare(gpuInfo.name)==0)
		    {
			    isInAdaptationList = true;
			    break;
		    }
	    }

		if (hwH26XCodecListStrategy)
		{
			if (isInAdaptationList)
			{
				RTC_LOG(LS_INFO) << "GPU Name : " << gpuInfo.name << " Is In White List : " << hwH26XCodecAdaptationList;
			    gpuInfos.push_back(gpuInfo);
			}else {
				RTC_LOG(LS_INFO) << "GPU Name : " << gpuInfo.name << " Is Not In White List : " << hwH26XCodecAdaptationList;
			}
		}else {
            if (isInAdaptationList)
			{
				RTC_LOG(LS_INFO) << "GPU Name : " << gpuInfo.name << " Is In Black List : " << hwH26XCodecAdaptationList;
			}else {
				RTC_LOG(LS_INFO) << "GPU Name : " << gpuInfo.name << " Is Not In Black List : " << hwH26XCodecAdaptationList;
			    gpuInfos.push_back(gpuInfo);
			}
		}
	}

    for (GpuInfo gpuInfo : gpuInfos)
    {
        if (gpuInfo.provider == GPU_NVIDIA)
        {
            HWVideoCodecCapability nvidiaHWVideoCodecCapability = NvidiaVideoCodecCapability::GetInstance().GetHWVideoCodecCapability();
			if (isEncoder && isH264)
			{
                if (nvidiaHWVideoCodecCapability.isSupportH264Encode)
			    {
					if (videoCodecWidth >= nvidiaHWVideoCodecCapability.minSupportedWidthForH264Encode && videoCodecHeight >= nvidiaHWVideoCodecCapability.minSupportedHeightForH264Encode)
					{
						if (nvidiaHWVideoCodecCapability.maxSupportedWidthForH264Encode <= 0 || videoCodecWidth <= nvidiaHWVideoCodecCapability.maxSupportedWidthForH264Encode)
						{
							if (nvidiaHWVideoCodecCapability.maxSupportedHeightForH264Encode <= 0 || videoCodecHeight <= nvidiaHWVideoCodecCapability.maxSupportedHeightForH264Encode)
							{
								if (!isEnableTemporalSVC || (isEnableTemporalSVC && numTemporalLayers <= nvidiaHWVideoCodecCapability.supportedMaxTemporalLayersForH264)) {
									if (NvidiaVideoCodecCapability::GetInstance().refCodecInstance(isEncoder, isH264))
									{
										return HW_CODEC_NVIDIA;
									}
								}
							}
						}
					}
			    }
			}else if(!isEncoder && isH264)
			{
				if (nvidiaHWVideoCodecCapability.isSupportH264Decode)
				{
					if (videoCodecWidth >= nvidiaHWVideoCodecCapability.minSupportedWidthForH264Decode && videoCodecHeight >= nvidiaHWVideoCodecCapability.minSupportedHeightForH264Decode)
					{
						if (nvidiaHWVideoCodecCapability.maxSupportedWidthForH264Decode <= 0 || videoCodecWidth <= nvidiaHWVideoCodecCapability.maxSupportedWidthForH264Decode)
						{
							if (nvidiaHWVideoCodecCapability.maxSupportedHeightForH264Decode <= 0 || videoCodecHeight <= nvidiaHWVideoCodecCapability.maxSupportedHeightForH264Decode)
							{
						        if (NvidiaVideoCodecCapability::GetInstance().refCodecInstance(isEncoder, isH264))
						        {
							        return HW_CODEC_NVIDIA;
						        }
							}
						}
					}
				}
			}else if(isEncoder && !isH264)
			{
				if (nvidiaHWVideoCodecCapability.isSupportH265Encode)
			    {
					if (videoCodecWidth >= nvidiaHWVideoCodecCapability.minSupportedWidthForH265Encode && videoCodecHeight >= nvidiaHWVideoCodecCapability.minSupportedHeightForH265Encode)
					{
						if (nvidiaHWVideoCodecCapability.maxSupportedWidthForH265Encode <= 0 || videoCodecWidth <= nvidiaHWVideoCodecCapability.maxSupportedWidthForH265Encode)
						{
							if (nvidiaHWVideoCodecCapability.maxSupportedHeightForH265Encode <= 0 || videoCodecHeight <= nvidiaHWVideoCodecCapability.maxSupportedHeightForH265Encode)
							{
						        if (NvidiaVideoCodecCapability::GetInstance().refCodecInstance(isEncoder, isH264))
						        {
							        return HW_CODEC_NVIDIA_HEVC;
						        }
							}
						}
					}
			    }
			}else if(!isEncoder && !isH264)
			{
				if (nvidiaHWVideoCodecCapability.isSupportH265Decode)
			    {
					if (videoCodecWidth >= nvidiaHWVideoCodecCapability.minSupportedWidthForH265Decode && videoCodecHeight >= nvidiaHWVideoCodecCapability.minSupportedHeightForH265Decode)
					{
						if (nvidiaHWVideoCodecCapability.maxSupportedWidthForH265Decode <= 0 || videoCodecWidth <= nvidiaHWVideoCodecCapability.maxSupportedWidthForH265Decode)
						{
							if (nvidiaHWVideoCodecCapability.maxSupportedHeightForH265Decode <= 0 || videoCodecHeight <= nvidiaHWVideoCodecCapability.maxSupportedHeightForH265Decode)
							{
						        if (NvidiaVideoCodecCapability::GetInstance().refCodecInstance(isEncoder, isH264))
						        {
							        return HW_CODEC_NVIDIA_HEVC;
						        }
							}
						}
					}
			    }
			}
        }
    }

    for (GpuInfo gpuInfo : gpuInfos)
    {
        if (gpuInfo.provider == GPU_AMD)
        {
            HWVideoCodecCapability amdHWVideoCodecCapability = AMDVideoCodecCapability::GetInstance().GetHWVideoCodecCapability();
			if (isEncoder && isH264)
			{
                if (amdHWVideoCodecCapability.isSupportH264Encode)
			    {
					if (videoCodecWidth >= amdHWVideoCodecCapability.minSupportedWidthForH264Encode && videoCodecHeight >= amdHWVideoCodecCapability.minSupportedHeightForH264Encode)
					{
						if (amdHWVideoCodecCapability.maxSupportedWidthForH264Encode <= 0 || videoCodecWidth <= amdHWVideoCodecCapability.maxSupportedWidthForH264Encode)
						{
							if (amdHWVideoCodecCapability.maxSupportedHeightForH264Encode <= 0 || videoCodecHeight <= amdHWVideoCodecCapability.maxSupportedHeightForH264Encode)
							{
						        if (AMDVideoCodecCapability::GetInstance().refCodecInstance(isEncoder, isH264))
						        {
							        return HW_CODEC_AMD;
						        }
							}
						}
					}
			    }
			}else if(!isEncoder && isH264)
			{
				if (amdHWVideoCodecCapability.isSupportH264Decode)
				{
					if (videoCodecWidth >= amdHWVideoCodecCapability.minSupportedWidthForH264Decode && videoCodecHeight >= amdHWVideoCodecCapability.minSupportedHeightForH264Decode)
					{
						if (amdHWVideoCodecCapability.maxSupportedWidthForH264Decode <= 0 || videoCodecWidth <= amdHWVideoCodecCapability.maxSupportedWidthForH264Decode)
						{
							if (amdHWVideoCodecCapability.maxSupportedHeightForH264Decode <= 0 || videoCodecHeight <= amdHWVideoCodecCapability.maxSupportedHeightForH264Decode)
							{
						        if (AMDVideoCodecCapability::GetInstance().refCodecInstance(isEncoder, isH264))
						        {
							        return HW_CODEC_AMD;
						        }
							}
						}
					}
				}
			}else if(isEncoder && !isH264)
			{
				if (amdHWVideoCodecCapability.isSupportH265Encode)
			    {
					if (videoCodecWidth >= amdHWVideoCodecCapability.minSupportedWidthForH265Encode && videoCodecHeight >= amdHWVideoCodecCapability.minSupportedHeightForH265Encode)
					{
						if (amdHWVideoCodecCapability.maxSupportedWidthForH265Encode <= 0 || videoCodecWidth <= amdHWVideoCodecCapability.maxSupportedWidthForH265Encode)
						{
							if (amdHWVideoCodecCapability.maxSupportedHeightForH265Encode <= 0 || videoCodecHeight <= amdHWVideoCodecCapability.maxSupportedHeightForH265Encode)
							{
						        if (AMDVideoCodecCapability::GetInstance().refCodecInstance(isEncoder, isH264))
						        {
							        return HW_CODEC_AMD_HEVC;
						        }
							}
						}
					}
			    }
			}else if(!isEncoder && !isH264)
			{
				if (amdHWVideoCodecCapability.isSupportH265Decode)
			    {
					if (videoCodecWidth >= amdHWVideoCodecCapability.minSupportedWidthForH265Decode && videoCodecHeight >= amdHWVideoCodecCapability.minSupportedHeightForH265Decode)
					{
						if (amdHWVideoCodecCapability.maxSupportedWidthForH265Decode <= 0 || videoCodecWidth <= amdHWVideoCodecCapability.maxSupportedWidthForH265Decode)
						{
							if (amdHWVideoCodecCapability.maxSupportedHeightForH265Decode <= 0 || videoCodecHeight <= amdHWVideoCodecCapability.maxSupportedHeightForH265Decode)
							{
						        if (AMDVideoCodecCapability::GetInstance().refCodecInstance(isEncoder, isH264))
						        {
							        return HW_CODEC_AMD_HEVC;
						        }
							}
						}
					}
			    }
			}
        }
    }

	for (GpuInfo gpuInfo : gpuInfos)
    {
        if (gpuInfo.provider == GPU_INTEL)
        {
            HWVideoCodecCapability intelQsvHWVideoCodecCapability = IntelQsvVideoCodecCapability::GetInstance().GetHWVideoCodecCapability();
			if (isEncoder && isH264)
			{
                if (intelQsvHWVideoCodecCapability.isSupportH264Encode)
			    {
					if (videoCodecWidth >= intelQsvHWVideoCodecCapability.minSupportedWidthForH264Encode && videoCodecHeight >= intelQsvHWVideoCodecCapability.minSupportedHeightForH264Encode)
					{
						if (intelQsvHWVideoCodecCapability.maxSupportedWidthForH264Encode <= 0 || videoCodecWidth <= intelQsvHWVideoCodecCapability.maxSupportedWidthForH264Encode)
						{
							if (intelQsvHWVideoCodecCapability.maxSupportedHeightForH264Encode <= 0 || videoCodecHeight <= intelQsvHWVideoCodecCapability.maxSupportedHeightForH264Encode)
							{
								return HW_CODEC_INTEL_QUICKSYNC;
							}
						}
					}
			    }
			}else if(!isEncoder && isH264)
			{
				if (intelQsvHWVideoCodecCapability.isSupportH264Decode)
				{
					if (videoCodecWidth >= intelQsvHWVideoCodecCapability.minSupportedWidthForH264Decode && videoCodecHeight >= intelQsvHWVideoCodecCapability.minSupportedHeightForH264Decode)
					{
						if (intelQsvHWVideoCodecCapability.maxSupportedWidthForH264Decode <= 0 || videoCodecWidth <= intelQsvHWVideoCodecCapability.maxSupportedWidthForH264Decode)
						{
							if (intelQsvHWVideoCodecCapability.maxSupportedHeightForH264Decode <= 0 || videoCodecHeight <= intelQsvHWVideoCodecCapability.maxSupportedHeightForH264Decode)
							{
								return HW_CODEC_INTEL_QUICKSYNC;
							}
						}
					}
				}
			}else if(isEncoder && !isH264)
			{
				if (intelQsvHWVideoCodecCapability.isSupportH265Encode)
			    {
					if (videoCodecWidth >= intelQsvHWVideoCodecCapability.minSupportedWidthForH265Encode && videoCodecHeight >= intelQsvHWVideoCodecCapability.minSupportedHeightForH265Encode)
					{
						if (intelQsvHWVideoCodecCapability.maxSupportedWidthForH265Encode <= 0 || videoCodecWidth <= intelQsvHWVideoCodecCapability.maxSupportedWidthForH265Encode)
						{
							if (intelQsvHWVideoCodecCapability.maxSupportedHeightForH265Encode <= 0 || videoCodecHeight <= intelQsvHWVideoCodecCapability.maxSupportedHeightForH265Encode)
							{
								return HW_CODEC_INTEL_QUICKSYNC_HEVC;
							}
						}
					}
			    }
			}else if(!isEncoder && !isH264)
			{
				if (intelQsvHWVideoCodecCapability.isSupportH265Decode)
			    {
					if (videoCodecWidth >= intelQsvHWVideoCodecCapability.minSupportedWidthForH265Decode && videoCodecHeight >= intelQsvHWVideoCodecCapability.minSupportedHeightForH265Decode)
					{
						if (intelQsvHWVideoCodecCapability.maxSupportedWidthForH265Decode <= 0 || videoCodecWidth <= intelQsvHWVideoCodecCapability.maxSupportedWidthForH265Decode)
						{
							if (intelQsvHWVideoCodecCapability.maxSupportedHeightForH265Decode <= 0 || videoCodecHeight <= intelQsvHWVideoCodecCapability.maxSupportedHeightForH265Decode)
							{
								return HW_CODEC_INTEL_QUICKSYNC_HEVC;
							}
						}
					}
			    }
			}
        }
    }

	return HW_CODEC_UNSUPPORTED;
}

CODEC_RETCODE CreateHWVideoEncoder(
    VIDEO_CODEC_PROVIDER * pCodec, HWVideoEncoder ** ppVideoEncoder, int videoEncodeWidth, int videoEncodeHeight, int hwH26XEncListStrategy, std::string hwH26XEncAdaptationList, bool isEnableTemporalSVC = false, int numTemporalLayers = 2)
{
	CODEC_RETCODE ret = CODEC_ERR_UNSUPPORTED;
	VIDEO_CODEC_PROVIDER provider = HW_CODEC_UNSUPPORTED;

	switch(*pCodec)
	{
	case HW_CODEC_INTEL_QUICKSYNC:
	case HW_CODEC_INTEL_QUICKSYNC_HEVC:
		ret = CreateIntelQsvVideoEncoder(*pCodec, ppVideoEncoder);
		break;
	case HW_CODEC_NVIDIA:
	case HW_CODEC_NVIDIA_HEVC:
	    ret = CreateNvidiaVideoEncoder(*pCodec, ppVideoEncoder);
	    break;
	case HW_CODEC_AMD:
	case HW_CODEC_AMD_HEVC:
	    ret = CreateAMDVideoEncoder(*pCodec, ppVideoEncoder);
	    break;
	case HW_CODEC_AUTOSELECT_H264:
	case HW_CODEC_AUTOSELECT_HEVC:
	    provider = AutoSelectVideoCodec(true, *pCodec == HW_CODEC_AUTOSELECT_H264 ? true : false, videoEncodeWidth, videoEncodeHeight, hwH26XEncListStrategy, hwH26XEncAdaptationList, isEnableTemporalSVC, numTemporalLayers);
		if (provider == HW_CODEC_UNSUPPORTED)
		{
			ret = CODEC_ERR_UNSUPPORTED;
		}else {
			*pCodec = provider;
			if (*pCodec == HW_CODEC_INTEL_QUICKSYNC || *pCodec == HW_CODEC_INTEL_QUICKSYNC_HEVC)
			{
		        ret = CreateIntelQsvVideoEncoder(*pCodec, ppVideoEncoder);
			}else if (*pCodec == HW_CODEC_NVIDIA || *pCodec == HW_CODEC_NVIDIA_HEVC)
			{
			    ret = CreateNvidiaVideoEncoder(*pCodec, ppVideoEncoder);
				if (ret != CODEC_OK)
				{
					NvidiaVideoCodecCapability::GetInstance().unrefCodecInstance(true, *pCodec == HW_CODEC_NVIDIA ? true : false);
				}
			}else if (*pCodec == HW_CODEC_AMD || *pCodec == HW_CODEC_AMD_HEVC)
			{
	            ret = CreateAMDVideoEncoder(*pCodec, ppVideoEncoder);
				if (ret != CODEC_OK)
				{
					AMDVideoCodecCapability::GetInstance().unrefCodecInstance(true, *pCodec == HW_CODEC_AMD ? true : false);
				}
			}
		}
	    break;
	default:
		break;
	}

	return ret;
}


void ReleaseHWVideoEncoder(VIDEO_CODEC_PROVIDER codec, HWVideoEncoder * pVideoEncoder)
{
	if (codec == HW_CODEC_INTEL_QUICKSYNC || codec == HW_CODEC_INTEL_QUICKSYNC_HEVC) {
		IntelQsvVideoEncoder* pQsvVideoEncoder = (IntelQsvVideoEncoder*)pVideoEncoder;
		if (pQsvVideoEncoder) {
			delete pQsvVideoEncoder;
			pQsvVideoEncoder = NULL;
		}
		pVideoEncoder = NULL;
	}else if (codec == HW_CODEC_NVIDIA || codec == HW_CODEC_NVIDIA_HEVC) {
		NvidiaVideoEncoder* pNvidiaVideoEncoder = (NvidiaVideoEncoder*)pVideoEncoder;
		if (pNvidiaVideoEncoder)
		{
			delete pNvidiaVideoEncoder;
			pNvidiaVideoEncoder = NULL;
		}
		pVideoEncoder = NULL;
		NvidiaVideoCodecCapability::GetInstance().unrefCodecInstance(true, codec == HW_CODEC_NVIDIA ? true : false);
	}else if (codec == HW_CODEC_AMD || codec == HW_CODEC_AMD_HEVC) {
		AMDVideoEncoder* pAMDVideoEncoder = (AMDVideoEncoder*)pVideoEncoder;
		if (pAMDVideoEncoder)
		{
			delete pAMDVideoEncoder;
			pAMDVideoEncoder = NULL;
		}
		pVideoEncoder = NULL;
		AMDVideoCodecCapability::GetInstance().unrefCodecInstance(true, codec == HW_CODEC_AMD ? true : false);
	}
}

CODEC_RETCODE CreateHWVideoDecoder(
        VIDEO_CODEC_PROVIDER *pCodec,
        HWVideoDecoder ** ppVideoDecoder, int videoDecodeWidth, int videoDecodeHeight, int hwH26XDecListStrategy, std::string hwH26XDecAdaptationList)
{
	CODEC_RETCODE ret = CODEC_ERR_UNSUPPORTED;
	VIDEO_CODEC_PROVIDER provider = HW_CODEC_UNSUPPORTED;

	switch(*pCodec)
	{
	case HW_CODEC_INTEL_QUICKSYNC:
	case HW_CODEC_INTEL_QUICKSYNC_HEVC:
		ret = CreateIntelQsvVideoDecoder(*pCodec, ppVideoDecoder);
		break;
	case HW_CODEC_NVIDIA:
	case HW_CODEC_NVIDIA_HEVC:
	    ret = CreateNvidiaVideoDecoder(*pCodec, ppVideoDecoder);
	    break;
	case HW_CODEC_AMD:
	case HW_CODEC_AMD_HEVC:
	    ret = CreateAMDVideoDecoder(*pCodec, ppVideoDecoder);
		break;
	case HW_CODEC_AUTOSELECT_H264:
	case HW_CODEC_AUTOSELECT_HEVC:
	    provider = AutoSelectVideoCodec(false, *pCodec == HW_CODEC_AUTOSELECT_H264 ? true : false, videoDecodeWidth, videoDecodeHeight, hwH26XDecListStrategy, hwH26XDecAdaptationList);
		if (provider == HW_CODEC_UNSUPPORTED)
		{
			ret = CODEC_ERR_UNSUPPORTED;
		}else {
			*pCodec = provider;
			if (*pCodec == HW_CODEC_INTEL_QUICKSYNC || *pCodec == HW_CODEC_INTEL_QUICKSYNC_HEVC)
			{
		        ret = CreateIntelQsvVideoDecoder(*pCodec, ppVideoDecoder);
			}else if (*pCodec == HW_CODEC_NVIDIA || *pCodec == HW_CODEC_NVIDIA_HEVC)
			{
			    ret = CreateNvidiaVideoDecoder(*pCodec, ppVideoDecoder);
				if (ret != CODEC_OK)
				{
					NvidiaVideoCodecCapability::GetInstance().unrefCodecInstance(false, *pCodec == HW_CODEC_NVIDIA ? true : false);
				}
			}else if (*pCodec == HW_CODEC_AMD || *pCodec == HW_CODEC_AMD_HEVC)
			{
	            ret = CreateAMDVideoDecoder(*pCodec, ppVideoDecoder);
				if (ret != CODEC_OK)
				{
					AMDVideoCodecCapability::GetInstance().unrefCodecInstance(false, *pCodec == HW_CODEC_AMD ? true : false);
				}
			}
		}
	    break;
	default:
		break;
	}

	return ret;
}

void ReleaseHWVideoDecoder(VIDEO_CODEC_PROVIDER codec, HWVideoDecoder * pVideoDecoder)
{
	if (codec == HW_CODEC_INTEL_QUICKSYNC || codec == HW_CODEC_INTEL_QUICKSYNC_HEVC) {
		IntelQsvVideoDecoder *pQsvVideoDecoder = (IntelQsvVideoDecoder*)pVideoDecoder;
		if (pQsvVideoDecoder)
		{
			delete pQsvVideoDecoder;
			pQsvVideoDecoder = NULL;
		}
		pVideoDecoder = NULL;
	}else if (codec == HW_CODEC_NVIDIA || codec == HW_CODEC_NVIDIA_HEVC) {
		NvidiaVideoDecoder* pNvidiaVideoDecoder = (NvidiaVideoDecoder*)pVideoDecoder;
		if (pNvidiaVideoDecoder)
		{
			delete pNvidiaVideoDecoder;
			pNvidiaVideoDecoder = NULL;
		}
		pVideoDecoder = NULL;
		NvidiaVideoCodecCapability::GetInstance().unrefCodecInstance(false, codec == HW_CODEC_NVIDIA ? true : false);
	}else if (codec == HW_CODEC_AMD || codec == HW_CODEC_AMD_HEVC) {
		AMDVideoDecoder* pAMDVideoDecoder = (AMDVideoDecoder*)pVideoDecoder;
		if (pAMDVideoDecoder)
		{
			delete pAMDVideoDecoder;
			pAMDVideoDecoder = NULL;
		}
		pVideoDecoder = NULL;
		AMDVideoCodecCapability::GetInstance().unrefCodecInstance(false, codec == HW_CODEC_AMD ? true : false);
	}
}

} // namespace netease