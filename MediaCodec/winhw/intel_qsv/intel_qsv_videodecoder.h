#pragma once
#include <memory>
#include <iostream>
#include <deque>
#include <vector>

#include <windows.h>
#include <mmsystem.h>

#include "third_party/mfx/mfxvideo.h"
#include "third_party/mfx/mfxvideo++.h"
#include "third_party/mfx/mfxplugin.h"
#include "third_party/mfx/mfxplugin++.h"
#include "third_party/mfx/mfxadapter.h"

#include "modules/video_coding/codecs/winhw/libhwcodec.h"
#include "modules/video_coding/codecs/winhw/intel_qsv/intel_qsv_videoutils.h"
#include "modules/video_coding/codecs/winhw/intel_qsv/intel_qsv_video_general_allocator.h"
#include "modules/video_coding/codecs/winhw/intel_qsv/intel_qsv_video_mfx_buffering.h"
#include "common_video/include/i420_buffer_pool.h"

#define INTEL_QSV_VIDEO_DECODER_ASYNC_DEPTH 1

namespace netease {

class IntelQsvVideoDecoder: public netease::HWVideoDecoder, public CBuffering
{
public:
	IntelQsvVideoDecoder(mfxU32 codecId);
	~IntelQsvVideoDecoder();

	// implement VideoDecoder interface
	bool init();
	bool setConfig(const VideoDecoderConfig &config);
	bool beginDecode();
	bool addEncodedFrame(const VideoEncodedFrame &frame);
	bool getRawFrame(VideoRawFrame &frame);
	bool endDecode();
	VIDEO_STATNDARD getVideoStandard();
	VIDEO_CODEC_PROVIDER getCodecId();
	const char* getCodecFriendlyName();
	const std::string getCodecGPUName();
private:
	mfxU32 m_CodecId;
	VideoDecoderConfig m_config;
    int m_async_depth = INTEL_QSV_VIDEO_DECODER_ASYNC_DEPTH;

    MFXVideoSession         m_mfxSession;
    mfxIMPL                 m_impl;
    FPSLimiter              m_fpsLimiter;
    MFXVideoDECODE*         m_pmfxDEC;
    MfxVideoParamsWrapper   m_mfxVideoParams;

    std::unique_ptr<MFXVideoUSER>  m_pUserModule;
    std::unique_ptr<MFXPlugin> m_pPlugin;
	bool bErrorReport;
    GeneralAllocator*       m_pGeneralAllocator;
    mfxAllocatorParams*     m_pmfxAllocatorParams;
    bool                    m_bExternalAlloc; // use memory allocator as external for Media SDK
    bool                    m_bDecOutSysmem; // use system memory between Decoder and VPP, if false - video memory
    mfxFrameAllocResponse   m_mfxResponse; // memory allocation response for decoder

    msdkFrameSurface*       m_pCurrentFreeSurface; // surface detached from free surfaces array
    msdkOutputSurface*      m_pCurrentFreeOutputSurface; // surface detached from free output surfaces array
    msdkOutputSurface*      m_pCurrentOutputSurface; // surface detached from output surfaces array

    mfxBitstreamWrapper                    m_mfxBS; // contains encoded data

    VideoRawFrame m_OutputVideoRawFrame;
    webrtc::I420BufferPool m_I420BufferPool;
    bool isOutputVideoRawFrameReady;

	bool m_bDecoderInited;
	bool m_bDecodeHeader;
private:
	bool beginDecodePrivate(mfxBitstreamWrapper& bitstreamWrapper);

    mfxStatus GetImpl(mfxBitstreamWrapper& bitstreamWrapper, mfxIMPL & impl);
    mfxU32 GetPreferredAdapterNum(const mfxAdaptersInfo & adapters);
    mfxStatus InitMfxParams(mfxBitstreamWrapper& bitstreamWrapper);
#if (MFX_VERSION >= 1025)
    void PrintDecodeErrorReport(mfxExtDecodeErrorReport *pDecodeErrorReport);
#endif
    mfxStatus CreateAllocator();
    void DeleteAllocator();
    mfxStatus AllocFrames();
    void DeleteFrames();

    /** \brief Performs SyncOperation on the current output surface with the specified timeout.
     *
     * @return MFX_ERR_NONE Output surface was successfully synced and delivered.
     * @return MFX_ERR_MORE_DATA Array of output surfaces is empty, need to feed decoder.
     * @return MFX_WRN_IN_EXECUTION Specified timeout have elapsed.
     * @return MFX_ERR_UNKNOWN An error has occurred.
     */
    mfxStatus SyncOutputSurface(mfxU32 wait);
    mfxStatus ReallocCurrentSurface(const mfxFrameInfo & info);

    mfxStatus DeliverOutput(mfxFrameSurface1* frame);
    mfxStatus WriteNextFrameI420(mfxFrameSurface1* frame);
private:
	std::string m_HWGPUName;
	std::string m_CodecFriendlyName;
private:
    mfxI64 m_PresentationTimeStampReferencePoint = 0;
    mfxI64 m_DecodingTimeStampReferencePoint = 0;
};

CODEC_RETCODE CreateIntelQsvVideoDecoder(VIDEO_CODEC_PROVIDER codec, HWVideoDecoder** ppVideoDecoder);

} // namespace netease