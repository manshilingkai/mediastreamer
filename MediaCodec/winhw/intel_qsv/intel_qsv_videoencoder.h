#pragma once
#include <memory>
#include <iostream>
#include <deque>
#include <vector>

#include <windows.h>
#include <mmsystem.h>

#include "third_party/mfx/mfxvideo.h"
#include "third_party/mfx/mfxvideo++.h"
#include "third_party/mfx/mfxplugin.h"
#include "third_party/mfx/mfxplugin++.h"
#include "third_party/mfx/mfxadapter.h"

#include "modules/video_coding/codecs/winhw/libhwcodec.h"
#include "common_video/include/bitrate_adjuster.h"
#include "modules/video_coding/codecs/winhw/intel_qsv/intel_qsv_videoutils.h"
#include "modules/video_coding/codecs/winhw/intel_qsv/intel_qsv_video_sysmem_allocator.h"

namespace netease {

class IntelQsvVideoEncoder;

struct IntelQsvEncoderParam
{
	IntelQsvEncoderParam()
	{
		memset(this, 0, sizeof(*this));
	}

    mfxU16 nDstWidth;// destination picture width, specified if resizing required
    mfxU16 nDstHeight;// destination picture height, specified if resizing required
    mfxU16 nFps;
    mfxU16 nPicStruct;
    mfxU32 EncodeFourCC;
    mfxU16 nTargetUsage;
    mfxU32 CodecId;
    mfxU16 CodecLevel;
    mfxU16 CodecProfile;

    MfxPluginParameter pluginParam;
};

struct IntelQsvEncTask
{
    mfxBitstreamWrapper mfxBS;
    mfxEncodeCtrlWrap encCtrl;
    mfxSyncPoint EncSyncP;
    IntelQsvVideoEncoder *callback;

    IntelQsvEncTask();
    mfxStatus WriteBitstream();
    mfxStatus Reset();
    mfxStatus Init(mfxU32 nBufferSize, IntelQsvVideoEncoder *cb);
    mfxStatus Close();
};

class IntelQsvEncTaskPool
{
public:
    IntelQsvEncTaskPool();
    virtual ~IntelQsvEncTaskPool();

    virtual mfxStatus Init(MFXVideoSession* pmfxSession, mfxU32 nPoolSize, mfxU32 nBufferSize, IntelQsvVideoEncoder *callback);
    virtual mfxStatus GetFreeTask(IntelQsvEncTask **ppTask);
    virtual mfxStatus SynchronizeFirstTask();

    virtual void Close();
    virtual void SetGpuHangRecoveryFlag();
    virtual void ClearTasks();
protected:
    IntelQsvEncTask* m_pTasks;
    mfxU32 m_nPoolSize;
    mfxU32 m_nTaskBufferStart;

    bool m_bGpuHangRecovery;

    MFXVideoSession* m_pmfxSession;

    virtual mfxU32 GetFreeTaskIndex();
};

struct ExtendedSurface
{
    mfxFrameSurface1 *pSurface;
    mfxEncodeCtrl    *pCtrl;
    mfxSyncPoint      Syncp;
};

typedef struct IntelQsvEncodedBuffer {
	mfxBitstream bs;
    int qp;
} IntelQsvEncodedBuffer;

class IntelQsvVideoEncoder: public netease::HWVideoEncoder
{
public:
	IntelQsvVideoEncoder(mfxU32 codecId);
	~IntelQsvVideoEncoder();

	// implement VideoEncoder interface
	bool init();
	bool setConfig(const VideoEncoderConfig &config);
	bool beginEncode();
	bool flushEncode();
	bool addRawFrame(const VideoRawFrame &frame);
	bool getEncodedFrame(VideoEncodedFrame &frame);
	bool endEncode();
	bool reset();
	bool resetBitrate(uint32_t bitrateBps);
	bool resetFramerate(uint32_t fps);
	void forceIntraFrame();
	void resetTemporalSVC(bool isEnableTemporalSVC, uint32_t numTemporalLayers);
	TemporalSVCCap getTemporalSVCCap();
	HWQPThreshold getHWQPThreshold();
	VIDEO_STATNDARD getVideoStandard();
	VIDEO_CODEC_PROVIDER getCodecId();
	const char* getCodecFriendlyName();

    // implement callback
    void handleBitstream(mfxBitstreamWrapper &mfxBS);
private:
	mfxU32 m_CodecId;
	VideoEncoderConfig m_config;
    MFXVideoSession m_mfxSession;
    std::unique_ptr<MFXVideoUSER> m_pUserModule;
    std::unique_ptr<MFXPlugin> m_pPlugin;
    MFXVideoENCODE* m_pmfxENC;
    FPSLimiter m_fpsLimiter;

    MFXFrameAllocator* m_pMFXAllocator;
    mfxAllocatorParams* m_pmfxAllocatorParams;
    MfxVideoParamsWrapper m_mfxEncParams;

    mfxFrameSurface1* m_pEncSurfaces;       // frames array for encoder input (vpp output)
    mfxFrameAllocResponse m_EncResponse;    // memory allocation response for encoder
    IntelQsvEncTaskPool   m_TaskPool;

    mfxU16 m_nEncSurfIdx; // index of free surface for encoder input (vpp output)
    mfxU32 m_nFramesRead;

    bool   m_bInsertIDR;
	bool   m_bResetBitrate;
	bool   m_bResetFramerate;

	bool m_bResetTemporalSVC;
    TemporalSVCCap m_TemporalSVCCap;

    mfxU32 nEncodedDataBufferSize;

    bool m_bEncoderInited;
private:
    mfxStatus GetImpl(IntelQsvEncoderParam param, mfxIMPL& impl);
#if (defined(_WIN64) || defined(_WIN32)) && (MFX_VERSION >= 1031)
    mfxU32 GetPreferredAdapterNum(const mfxAdaptersInfo & adapters);
#endif
    mfxStatus CreateAllocator();
    void DeleteAllocator();
    mfxStatus InitMfxEncParams();
    mfxStatus ResetMFXComponents();
    mfxStatus AllocFrames();
    void DeleteFrames();
    mfxStatus GetFreeTask(IntelQsvEncTask **ppTask);
    mfxStatus LoadNextFrame(const VideoRawFrame &frame, mfxFrameSurface1* pSurf);
    mfxStatus EncodeOneFrame(const ExtendedSurface& In, IntelQsvEncTask*& pTask);
    void InsertIDR(mfxEncodeCtrl & ctrl, bool forceIDR);
    mfxStatus AllocateSufficientBuffer(mfxBitstreamWrapper& bs);
private:
    IntelQsvEncodedBuffer* getFreeEncodedBuffer(uint32_t bufsize);
    void putFilledEncodedBuffer(IntelQsvEncodedBuffer* filledEncodedBuffer);
	IntelQsvEncodedBuffer* getFilledEncodedBuffer();
    void putFreeEncodedBuffer(IntelQsvEncodedBuffer* freeEncodedBuffer);
	void releaseEncodedBuffer();
	std::deque<IntelQsvEncodedBuffer*> m_FreeEncodedBuffers;
	std::deque<IntelQsvEncodedBuffer*> m_FilledEncodedBuffers;
private:
	std::string m_HWGPUName;
	std::string m_CodecFriendlyName;
private:
	webrtc::BitrateAdjuster *m_pBitrateAdjuster;
	uint32_t m_encoderBitrateBps;
};

CODEC_RETCODE CreateIntelQsvVideoEncoder(VIDEO_CODEC_PROVIDER codec, HWVideoEncoder** ppVideoEncoder);

} // namespace netease