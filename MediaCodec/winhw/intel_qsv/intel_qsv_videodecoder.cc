#include "modules/video_coding/codecs/winhw/intel_qsv/intel_qsv_videodecoder.h"
#include <intrin.h>
#include <string>
#include "rtc_base/logging.h"
#include "modules/video_coding/codecs/winhw/win/gpu_info.h"
#include "third_party/libyuv/include/libyuv/planar_functions.h"

namespace netease {

IntelQsvVideoDecoder::IntelQsvVideoDecoder(mfxU32 codecId)
    : m_mfxBS(8 * 1024 * 1024),
      m_I420BufferPool(true, 20)
{
    m_CodecId = codecId;

    m_pmfxDEC = NULL;
    bErrorReport = true;

    m_pGeneralAllocator = NULL;
    m_pmfxAllocatorParams = NULL;

    m_bExternalAlloc = false;
    m_bDecOutSysmem = true;

    m_pCurrentFreeSurface = NULL;
    m_pCurrentFreeOutputSurface = NULL;
    m_pCurrentOutputSurface = NULL;

    isOutputVideoRawFrameReady = false;

    m_bDecoderInited = false;
	m_bDecodeHeader = false;

    if (m_CodecId == MFX_CODEC_HEVC) {
        m_async_depth = 4;
    }

	m_HWGPUName = "Intel";
    std::vector<GpuInfo>& available_gpuInfos = GPUInfoHandler::GetInstance().GetAvailableGpuInfos();
	for (GpuInfo gpuInfo : available_gpuInfos) {
        if (gpuInfo.provider == GPU_INTEL) {
            m_HWGPUName = gpuInfo.name;
        }
    }

    if (m_CodecId == MFX_CODEC_HEVC)
    {
        m_CodecFriendlyName = "H265 Decoder [" + m_HWGPUName + "]";
    }else {
        m_CodecFriendlyName = "H264 Decoder [" + m_HWGPUName + "]";
    }
}

IntelQsvVideoDecoder::~IntelQsvVideoDecoder()
{
    this->endDecode();
}

mfxStatus IntelQsvVideoDecoder::GetImpl(mfxBitstreamWrapper& bitstreamWrapper, mfxIMPL & impl)
{
#if (defined(_WIN64) || defined(_WIN32)) && (MFX_VERSION >= 1031)
    mfxU32 num_adapters_available;
    mfxStatus sts = MFXQueryAdaptersNumber(&num_adapters_available);
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "MFXQueryAdaptersNumber failed";
        return sts;
    }

    std::vector<mfxAdapterInfo> displays_data(num_adapters_available);
    mfxAdaptersInfo adapters = { displays_data.data(), mfxU32(displays_data.size()), 0u };
    sts = MFXQueryAdaptersDecode(&bitstreamWrapper, m_CodecId, &adapters);
    if (sts == MFX_ERR_NOT_FOUND)
    {
        RTC_LOG(LS_ERROR) << "ERROR: No suitable adapters found for this workload";
    }
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "MFXQueryAdapters failed";
        return sts;
    }

    mfxU32 idx = GetPreferredAdapterNum(adapters);
    switch (adapters.Adapters[idx].Number)
    {
    case 0:
        impl = MFX_IMPL_HARDWARE;
        break;
    case 1:
        impl = MFX_IMPL_HARDWARE2;
        break;
    case 2:
        impl = MFX_IMPL_HARDWARE3;
        break;
    case 3:
        impl = MFX_IMPL_HARDWARE4;
        break;

    default:
        // Try searching on all display adapters
        impl = MFX_IMPL_HARDWARE_ANY;
        break;
    }
#else
    // Library should pick first available compatible adapter during InitEx call with MFX_IMPL_HARDWARE_ANY
    impl = MFX_IMPL_HARDWARE_ANY;
#endif // (defined(_WIN64) || defined(_WIN32)) && (MFX_VERSION >= 1031)

    return MFX_ERR_NONE;
}

#if (defined(_WIN64) || defined(_WIN32)) && (MFX_VERSION >= 1031)
mfxU32 IntelQsvVideoDecoder::GetPreferredAdapterNum(const mfxAdaptersInfo & adapters)
{
    if (adapters.NumActual == 0 || !adapters.Adapters)
        return 0;

    bool bPrefferiGfx = true;
    bool bPrefferdGfx = false;
    if (bPrefferiGfx)
    {
        // Find iGfx adapter in list and return it's index

        auto idx = std::find_if(adapters.Adapters, adapters.Adapters + adapters.NumActual,
            [](const mfxAdapterInfo info)
        {
            return info.Platform.MediaAdapterType == mfxMediaAdapterType::MFX_MEDIA_INTEGRATED;
        });

        // No iGfx in list
        if (idx == adapters.Adapters + adapters.NumActual)
        {
            RTC_LOG(LS_WARNING) << "Warning: No iGfx detected on machine. Will pick another adapter\n";
            bPrefferdGfx = true;
        }else {
            return static_cast<mfxU32>(std::distance(adapters.Adapters, idx));
        }
    }

    if (bPrefferdGfx)
    {
        // Find dGfx adapter in list and return it's index

        auto idx = std::find_if(adapters.Adapters, adapters.Adapters + adapters.NumActual,
            [](const mfxAdapterInfo info)
            {
                return info.Platform.MediaAdapterType == mfxMediaAdapterType::MFX_MEDIA_DISCRETE;
            });

        // No dGfx in list
        if (idx == adapters.Adapters + adapters.NumActual)
        {
            RTC_LOG(LS_WARNING) << "Warning: No dGfx detected on machine. Will pick another adapter\n";
            return 0;
        }

        return static_cast<mfxU32>(std::distance(adapters.Adapters, idx));
    }

    // Other ways return 0, i.e. best suitable detected by dispatcher
    return 0;
}
#endif

bool IntelQsvVideoDecoder::init()
{
    return true;
}

bool IntelQsvVideoDecoder::setConfig(const VideoDecoderConfig &config)
{
    m_config = config;
    return true;
}

bool IntelQsvVideoDecoder::beginDecode()
{
    return true;
}

bool IntelQsvVideoDecoder::beginDecodePrivate(mfxBitstreamWrapper& bitstreamWrapper)
{
    // RTC_LOG(LS_INFO) << "in func: " << __FUNCTION__;

    if (m_bDecoderInited) return true;

    mfxStatus sts = MFX_ERR_NONE;

    mfxInitParamlWrap initPar;

    // we set version to 1.0 and later we will query actual version of the library which will got leaded
    initPar.Version.Major = 1;
    initPar.Version.Minor = 0;

    initPar.GPUCopy = false;

    // auto threadsPar = initPar.AddExtBuffer<mfxExtThreadsParam>();
    // threadsPar->NumThread = 2;

    sts = GetImpl(bitstreamWrapper, initPar.Implementation);
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "GetImpl failed. Error Code : " << sts;
        return false;
    }

    sts = m_mfxSession.InitEx(initPar);
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "m_mfxSession.InitEx failed. Error Code : " << sts;
        return false;
    }

    mfxVersion version;
    sts = m_mfxSession.QueryVersion(&version); // get real API version of the loaded library
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "m_mfxSession.QueryVersion failed. Error Code : " << sts;
        m_mfxSession.Close();
        return false;
    }

    sts = m_mfxSession.QueryIMPL(&m_impl); // get actual library implementation
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "m_mfxSession.QueryIMPL failed. Error Code : " << sts;
        m_mfxSession.Close();
        return false;
    }

    if (!CheckVersion(&version, MSDK_FEATURE_LOW_LATENCY)) {
        RTC_LOG(LS_WARNING) << "warning: Low Latency mode is not supported in the " << version.Major << "." << version.Minor << " API version";
    }

    m_fpsLimiter.Reset(m_config.fps);

    // create decoder
    m_pmfxDEC = new MFXVideoDECODE(m_mfxSession);
    if (m_pmfxDEC == NULL)
    {
        RTC_LOG(LS_ERROR) << "m_pmfxDEC == NULL. Error Code : " << MFX_ERR_MEMORY_ALLOC;
        m_mfxSession.Close();
        return false;
    }

    // set video type in parameters
    memset(&m_mfxVideoParams, 0, sizeof(mfxVideoParam));
    m_mfxVideoParams.mfx.CodecId = m_CodecId;
    
    if (CheckVersion(&version, MSDK_FEATURE_PLUGIN_API) && m_CodecId == MFX_CODEC_HEVC) {
        /* Here we actually define the following codec initialization scheme:
        *  1. If plugin path or guid is specified: we load user-defined plugin (example: VP8 sample decoder plugin)
        *  2. If plugin path not specified:
        *    2.a) we check if codec is distributed as a mediasdk plugin and load it if yes
        *    2.b) if codec is not in the list of mediasdk plugins, we assume, that it is supported inside mediasdk library
        */
        // Load user plug-in, should go after CreateAllocator function (when all callbacks were initialized)
        MfxPluginParameter pluginParam;
        pluginParam.type = MFX_PLUGINLOAD_TYPE_GUID;
        pluginParam.pluginGuid = MFX_PLUGINID_HEVCD_HW;
        pluginParam.strPluginPath[0] = '\0';
        if (pluginParam.type == MFX_PLUGINLOAD_TYPE_FILE && msdk_strnlen(pluginParam.strPluginPath,sizeof(pluginParam.strPluginPath)))
        {
            m_pUserModule.reset(new MFXVideoUSER(m_mfxSession));
            if (m_CodecId == MFX_CODEC_HEVC || m_CodecId == MFX_CODEC_VP9)
            {
                m_pPlugin.reset(LoadPlugin(MFX_PLUGINTYPE_VIDEO_DECODE, m_mfxSession, pluginParam.pluginGuid, 1, pluginParam.strPluginPath, (mfxU32)msdk_strnlen(pluginParam.strPluginPath,sizeof(pluginParam.strPluginPath))));
            }
            if (m_pPlugin.get() == NULL) sts = MFX_ERR_UNSUPPORTED;
        }
        else
        {
            bool isDefaultPlugin = false;
            if (AreGuidsEqual(pluginParam.pluginGuid, MSDK_PLUGINGUID_NULL))
            {
                mfxIMPL impl = MFX_IMPL_HARDWARE;
                pluginParam.pluginGuid = msdkGetPluginUID(impl, MSDK_VDECODE, m_CodecId);
                isDefaultPlugin = true;
            }
            if (!AreGuidsEqual(pluginParam.pluginGuid, MSDK_PLUGINGUID_NULL))
            {
                m_pPlugin.reset(LoadPlugin(MFX_PLUGINTYPE_VIDEO_DECODE, m_mfxSession, pluginParam.pluginGuid, 1));
                if (m_pPlugin.get() == NULL) sts = MFX_ERR_UNSUPPORTED;
            }
            if(sts==MFX_ERR_UNSUPPORTED)
            {
                RTC_LOG(LS_ERROR) << MSDK_STRING("%s"), isDefaultPlugin ?
                    MSDK_STRING("Default plugin cannot be loaded (possibly you have to define plugin explicitly)\n")
                    : MSDK_STRING("Explicitly specified plugin cannot be loaded.\n");
            }
        }
        if (sts < MFX_ERR_NONE) {
            RTC_LOG(LS_ERROR) << "Plugin load failed. Error Code : " << sts;
            MSDK_SAFE_DELETE(m_pmfxDEC);
            m_mfxSession.Close();
            return false;
        }
    }

// #if (MFX_VERSION >= 1034)
//     m_mfxVideoParams.mfx.IgnoreLevelConstrain = 1;
// #endif

    // Populate parameters. Involves DecodeHeader call
    sts = InitMfxParams(bitstreamWrapper);
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "InitMfxParams failed. Error Code : " << sts;
        m_pPlugin.reset();
        MSDK_SAFE_DELETE(m_pmfxDEC);
        m_mfxSession.Close();
        return false;
    }

    sts = CreateAllocator();
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "CreateAllocator failed. Error Code : " << sts;
        m_pPlugin.reset();
        MSDK_SAFE_DELETE(m_pmfxDEC);
        m_mfxSession.Close();
        return false;
    }

    // in case of HW accelerated decode frames must be allocated prior to decoder initialization
    sts = AllocFrames();
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "AllocFrames failed. Error Code : " << sts;
        DeleteAllocator();
        m_pPlugin.reset();
        MSDK_SAFE_DELETE(m_pmfxDEC);
        m_mfxSession.Close();
        return false;
    }

    sts = m_pmfxDEC->Init(&m_mfxVideoParams);
    if (MFX_WRN_PARTIAL_ACCELERATION == sts)
    {
        RTC_LOG(LS_WARNING) << "WARNING: partial acceleration";
        if (sts == MFX_WRN_PARTIAL_ACCELERATION)
        {
            sts = MFX_ERR_NONE;
        }
    }
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_pmfxDEC->Init failed. Error Code : " << sts;
        DeleteFrames();
        DeleteAllocator();
        m_pPlugin.reset();
        MSDK_SAFE_DELETE(m_pmfxDEC);
        m_mfxSession.Close();
        return false;
    }

    if (m_CodecId == MFX_CODEC_HEVC) {
        m_pmfxDEC->SetSkipMode(MFX_SKIPMODE_NOSKIP);
    }

    sts = m_pmfxDEC->GetVideoParam(&m_mfxVideoParams);
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_pmfxDEC->GetVideoParam failed. Error Code : " << sts;
        DeleteFrames();
        DeleteAllocator();
        m_pPlugin.reset();
        MSDK_SAFE_DELETE(m_pmfxDEC);
        m_mfxSession.Close();
        return false;
    }

    m_bDecoderInited = true;
    m_bDecodeHeader = false;

    // RTC_LOG(LS_INFO) << "out func: " << __FUNCTION__;

    return true;
}

mfxStatus IntelQsvVideoDecoder::AllocFrames()
{
    if (m_pmfxDEC == NULL)
    {
        return MFX_ERR_NULL_PTR;
    }
    
    mfxStatus sts = MFX_ERR_NONE;

    mfxFrameAllocRequest Request;

    mfxU16 nSurfNum = 0; // number of surfaces for decoder

    MSDK_ZERO_MEMORY(Request);

    sts = m_pmfxDEC->Query(&m_mfxVideoParams, &m_mfxVideoParams);
    if (sts == MFX_WRN_INCOMPATIBLE_VIDEO_PARAM)
    {
        sts = MFX_ERR_NONE;
    }

    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_pmfxDEC->Query failed. Error Code : " << sts;
        return sts;
    }
    
    // calculate number of surfaces required for decoder
    sts = m_pmfxDEC->QueryIOSurf(&m_mfxVideoParams, &Request);
    if (MFX_WRN_PARTIAL_ACCELERATION == sts)
    {
        RTC_LOG(LS_WARNING) << "WARNING: partial acceleration";
        if (sts == MFX_WRN_PARTIAL_ACCELERATION)
        {
            sts = MFX_ERR_NONE;
        }
        m_bDecOutSysmem = true;
    }
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_pmfxDEC->QueryIOSurf failed";
        return sts;
    }

    if ((Request.NumFrameSuggested < m_mfxVideoParams.AsyncDepth) &&
        (m_impl & MFX_IMPL_HARDWARE_ANY))
        return MFX_ERR_MEMORY_ALLOC;

    Request.Type |= (m_bDecOutSysmem) ?
        MFX_MEMTYPE_SYSTEM_MEMORY
        : MFX_MEMTYPE_VIDEO_MEMORY_DECODER_TARGET;

    // alloc frames for decoder
    sts = m_pGeneralAllocator->Alloc(m_pGeneralAllocator->pthis, &Request, &m_mfxResponse);
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_pGeneralAllocator->Alloc failed";
        return sts;
    }

    // prepare mfxFrameSurface1 array for decoder
    nSurfNum = m_mfxResponse.NumFrameActual;

    sts = AllocBuffers(nSurfNum);
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "AllocBuffers failed";
        return sts;
    }
    
    for (int i = 0; i < nSurfNum; i++)
    {
        // initating each frame:
        MSDK_MEMCPY_VAR(m_pSurfaces[i].frame.Info, &(Request.Info), sizeof(mfxFrameInfo));
        m_pSurfaces[i].frame.Data.MemType = Request.Type;
        if (m_bExternalAlloc) {
            m_pSurfaces[i].frame.Data.MemId = m_mfxResponse.mids[i];
        }
    }

    return MFX_ERR_NONE;
}

void IntelQsvVideoDecoder::DeleteFrames()
{
    FreeBuffers();

    m_pCurrentFreeSurface = NULL;
    MSDK_SAFE_FREE(m_pCurrentFreeOutputSurface);

    // delete frames
    if (m_pGeneralAllocator)
    {
        m_pGeneralAllocator->Free(m_pGeneralAllocator->pthis, &m_mfxResponse);
    }

    return;
}

mfxStatus IntelQsvVideoDecoder::CreateAllocator()
{
    mfxStatus sts = MFX_ERR_NONE;

    m_pGeneralAllocator = new GeneralAllocator();

    sts = m_mfxSession.SetFrameAllocator(m_pGeneralAllocator);
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_mfxSession.SetFrameAllocator failed. Error Code : " << sts;
        return sts;
    }
    m_bExternalAlloc = true;

    // initialize memory allocator
    sts = m_pGeneralAllocator->Init(m_pmfxAllocatorParams);
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_pGeneralAllocator->Init failed. Error Code : " << sts;
        return sts;
    }

    return MFX_ERR_NONE;
}

void IntelQsvVideoDecoder::DeleteAllocator()
{
    // delete allocator
    MSDK_SAFE_DELETE(m_pGeneralAllocator);
    MSDK_SAFE_DELETE(m_pmfxAllocatorParams);
}

#if (MFX_VERSION >= 1025)
void IntelQsvVideoDecoder::PrintDecodeErrorReport(mfxExtDecodeErrorReport *pDecodeErrorReport)
{
    if (pDecodeErrorReport)
    {
        if (pDecodeErrorReport->ErrorTypes & MFX_ERROR_SPS) {
            RTC_LOG(LS_ERROR) << "[Error] SPS Error detected!";
        }

        if (pDecodeErrorReport->ErrorTypes & MFX_ERROR_PPS) {
            RTC_LOG(LS_ERROR) << "[Error] PPS Error detected!";
        }

        if (pDecodeErrorReport->ErrorTypes & MFX_ERROR_SLICEHEADER) {
            RTC_LOG(LS_ERROR) << "[Error] SliceHeader Error detected!";
        }

        if (pDecodeErrorReport->ErrorTypes & MFX_ERROR_FRAME_GAP) {
            RTC_LOG(LS_ERROR) << "[Error] Frame Gap Error detected!";
        }
    }
}
#endif

mfxStatus IntelQsvVideoDecoder::InitMfxParams(mfxBitstreamWrapper& bitstreamWrapper)
{
    if (m_pmfxDEC == NULL)
    {
        return MFX_ERR_NULL_PTR;
    }
    
    mfxStatus sts = MFX_ERR_NONE;

#if (MFX_VERSION >= 1025)
    if (bErrorReport)
    {
        auto decErrorReport = bitstreamWrapper.AddExtBuffer<mfxExtDecodeErrorReport>();
        if (!decErrorReport) {
            return MFX_ERR_MEMORY_ALLOC;
        }
    }
#endif
    // try to find a sequence header in the stream
    // if header is not found this function exits with error (e.g. if device was lost and there's no header in the remaining stream)

#if (MFX_VERSION >= 1025)
    if (bErrorReport)
    {
        auto errorReport = bitstreamWrapper.GetExtBuffer<mfxExtDecodeErrorReport>();
        if (!errorReport)
        {
            return MFX_ERR_NOT_INITIALIZED;
        }
        
        errorReport->ErrorTypes = 0;

        // parse bit stream and fill mfx params
        sts = m_pmfxDEC->DecodeHeader(&bitstreamWrapper, &m_mfxVideoParams);

        PrintDecodeErrorReport(errorReport);
    }
    else
#endif
    {
        // parse bit stream and fill mfx params
        sts = m_pmfxDEC->DecodeHeader(&bitstreamWrapper, &m_mfxVideoParams);
    }

    // check DecodeHeader status
    if (MFX_WRN_PARTIAL_ACCELERATION == sts)
    {
        RTC_LOG(LS_WARNING) << "WARNING: partial acceleration";
        if (sts == MFX_WRN_PARTIAL_ACCELERATION) {sts = MFX_ERR_NONE;}
    }
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_pmfxDEC->DecodeHeader failed";
        return sts;
    }
    
    if (!m_mfxVideoParams.mfx.FrameInfo.FrameRateExtN || !m_mfxVideoParams.mfx.FrameInfo.FrameRateExtD) {
        RTC_LOG(LS_INFO) << "pretending that stream is 30fps one";
        m_mfxVideoParams.mfx.FrameInfo.FrameRateExtN = 30;
        m_mfxVideoParams.mfx.FrameInfo.FrameRateExtD = 1;
    }
    if (!m_mfxVideoParams.mfx.FrameInfo.AspectRatioW || !m_mfxVideoParams.mfx.FrameInfo.AspectRatioH) {
        RTC_LOG(LS_INFO) << "pretending that aspect ratio is 1:1";
        m_mfxVideoParams.mfx.FrameInfo.AspectRatioW = 1;
        m_mfxVideoParams.mfx.FrameInfo.AspectRatioH = 1;
    }

    m_mfxVideoParams.IOPattern = MFX_IOPATTERN_OUT_SYSTEM_MEMORY;

    m_mfxVideoParams.AsyncDepth = m_async_depth;

    return MFX_ERR_NONE;
}

mfxStatus IntelQsvVideoDecoder::WriteNextFrameI420(mfxFrameSurface1* frame)
{
    // RTC_LOG(LS_INFO) << "in func: " << __FUNCTION__;

	mfxFrameInfo& pInfo = frame->Info;
	mfxFrameData& pData = frame->Data;

	if (MFX_FOURCC_NV12 != pInfo.FourCC && MFX_FOURCC_YV12 != pInfo.FourCC)
	{
		return MFX_ERR_UNSUPPORTED;
	}

	mfxU16 w, h, pitch;
	mfxU8 *ptr;

	if (pInfo.CropH > 0 && pInfo.CropW > 0)
	{
		w = pInfo.CropW;
		h = pInfo.CropH;
	}
	else
	{
		w = pInfo.Width;
		h = pInfo.Height;
	}

    if (pInfo.FourCC == MFX_FOURCC_NV12) {
		m_OutputVideoRawFrame.format = FOURCC_NV12;

		ptr = pData.Y + pInfo.CropX + pInfo.CropY * pData.Pitch;
		m_OutputVideoRawFrame.planeptrs[0].ptr = ptr;

		pitch = pData.Pitch;
		m_OutputVideoRawFrame.strides[0] = pitch;

		ptr = pData.UV + pInfo.CropX + (pInfo.CropY / 2) * pitch;
		m_OutputVideoRawFrame.planeptrs[1].ptr = ptr;
		m_OutputVideoRawFrame.strides[1] = pitch;
	}else if (pInfo.FourCC == MFX_FOURCC_YV12) {
		m_OutputVideoRawFrame.format = FOURCC_I420;

		ptr = pData.Y + pInfo.CropX + pInfo.CropY * pData.Pitch;
		m_OutputVideoRawFrame.planeptrs[0].ptr = ptr;

		pitch = pData.Pitch;
		m_OutputVideoRawFrame.strides[0] = pitch;

        pitch /= 2;

		ptr  = pData.U + (pInfo.CropX / 2) + (pInfo.CropY / 2) * pitch;
		m_OutputVideoRawFrame.planeptrs[1].ptr = ptr;
		m_OutputVideoRawFrame.strides[1] = pitch;

		ptr = pData.V + (pInfo.CropX / 2) + (pInfo.CropY / 2) * pitch;
		m_OutputVideoRawFrame.planeptrs[2].ptr = ptr;
		m_OutputVideoRawFrame.strides[2] = pitch;
	}

	m_OutputVideoRawFrame.width = w;
	m_OutputVideoRawFrame.height = h;

	m_OutputVideoRawFrame.timestamp = pData.TimeStamp;

    m_OutputVideoRawFrame.frame_buffer = m_I420BufferPool.CreateBuffer(m_OutputVideoRawFrame.width, m_OutputVideoRawFrame.height);

  if (m_OutputVideoRawFrame.frame_buffer == nullptr)
  {
    RTC_LOG(LS_ERROR) << "CreateBuffer Fail With Width:" << m_OutputVideoRawFrame.width << " Height:" << m_OutputVideoRawFrame.height;
    return MFX_ERR_MEMORY_ALLOC;
  }
  
  if (m_OutputVideoRawFrame.format == netease::FOURCC_I420)
  {
    libyuv::I420Copy((const uint8_t*)m_OutputVideoRawFrame.planeptrs[0].ptr, m_OutputVideoRawFrame.strides[0],
                     (const uint8_t*)m_OutputVideoRawFrame.planeptrs[1].ptr, m_OutputVideoRawFrame.strides[1],
                     (const uint8_t*)m_OutputVideoRawFrame.planeptrs[2].ptr, m_OutputVideoRawFrame.strides[2],
                     m_OutputVideoRawFrame.frame_buffer->MutableDataY(), m_OutputVideoRawFrame.frame_buffer->StrideY(),
                     m_OutputVideoRawFrame.frame_buffer->MutableDataU(), m_OutputVideoRawFrame.frame_buffer->StrideU(),
                     m_OutputVideoRawFrame.frame_buffer->MutableDataV(), m_OutputVideoRawFrame.frame_buffer->StrideV(),
                     m_OutputVideoRawFrame.width, m_OutputVideoRawFrame.height);
  }else if(m_OutputVideoRawFrame.format == netease::FOURCC_NV12)
  {
    libyuv::NV12ToI420((const uint8_t*)m_OutputVideoRawFrame.planeptrs[0].ptr, m_OutputVideoRawFrame.strides[0],
                         (const uint8_t*)m_OutputVideoRawFrame.planeptrs[1].ptr, m_OutputVideoRawFrame.strides[1],
                         m_OutputVideoRawFrame.frame_buffer->MutableDataY(), m_OutputVideoRawFrame.frame_buffer->StrideY(),
                         m_OutputVideoRawFrame.frame_buffer->MutableDataU(), m_OutputVideoRawFrame.frame_buffer->StrideU(),
                         m_OutputVideoRawFrame.frame_buffer->MutableDataV(), m_OutputVideoRawFrame.frame_buffer->StrideV(),
                         m_OutputVideoRawFrame.width, m_OutputVideoRawFrame.height);
  }

    // RTC_LOG(LS_INFO) << "out func: " << __FUNCTION__;

    return MFX_ERR_NONE;
}

mfxStatus IntelQsvVideoDecoder::DeliverOutput(mfxFrameSurface1* frame)
{
    // RTC_LOG(LS_INFO) << "in func: " << __FUNCTION__;

    mfxStatus res = MFX_ERR_NONE, sts = MFX_ERR_NONE;

    if (!frame) {
        return MFX_ERR_NULL_PTR;
    }

    res = m_pGeneralAllocator->Lock(m_pGeneralAllocator->pthis, frame->Data.MemId, &(frame->Data));
    if (MFX_ERR_NONE == res) {
        if (!isOutputVideoRawFrameReady)
        {
            res = WriteNextFrameI420(frame);
            // RTC_LOG(LS_INFO) << "WriteNextFrameI420 ret: " << res;
            if (res == MFX_ERR_NONE)
            {
                isOutputVideoRawFrameReady = true;
            }
        }

        sts = m_pGeneralAllocator->Unlock(m_pGeneralAllocator->pthis, frame->Data.MemId, &(frame->Data));
    }
    if ((MFX_ERR_NONE == res) && (MFX_ERR_NONE != sts)) {
        res = sts;
    }

    // m_fpsLimiter.Work();

    // RTC_LOG(LS_INFO) << "out func: " << __FUNCTION__ << " ret : " << res;

    return res;
}

mfxStatus IntelQsvVideoDecoder::SyncOutputSurface(mfxU32 wait)
{
    // RTC_LOG(LS_INFO) << "in func: " << __FUNCTION__;

    if (!m_pCurrentOutputSurface) {
        m_pCurrentOutputSurface = m_OutputSurfacesPool.GetSurface();
    }
    if (!m_pCurrentOutputSurface) {
        return MFX_ERR_MORE_DATA;
    }

    mfxStatus sts = m_mfxSession.SyncOperation(m_pCurrentOutputSurface->syncp, wait);

    if (MFX_WRN_IN_EXECUTION == sts) {
        return sts;
    }
    if (MFX_ERR_NONE == sts) {
        // we got completely decoded frame - pushing it to the delivering thread...

        sts = DeliverOutput(&(m_pCurrentOutputSurface->surface->frame));
        if (MFX_ERR_NONE != sts) {
            sts = MFX_ERR_UNKNOWN;
        }

        // m_fpsLimiter.Work();
        ReturnSurfaceToBuffers(m_pCurrentOutputSurface);
        m_pCurrentOutputSurface = NULL;
    }

    // RTC_LOG(LS_INFO) << "out func: " << __FUNCTION__ " ret : " << sts;

    return sts;
}

mfxStatus IntelQsvVideoDecoder::ReallocCurrentSurface(const mfxFrameInfo & info)
{
    mfxStatus sts = MFX_ERR_NONE;
    mfxMemId inMid = nullptr;
    mfxMemId outMid = nullptr;

    if(!m_pGeneralAllocator)
        return MFX_ERR_MEMORY_ALLOC;

    m_pCurrentFreeSurface->frame.Info.CropW = info.CropW;
    m_pCurrentFreeSurface->frame.Info.CropH = info.CropH;
    m_mfxVideoParams.mfx.FrameInfo.Width = MSDK_ALIGN16(MSDK_MAX(info.Width, m_mfxVideoParams.mfx.FrameInfo.Width));
    m_mfxVideoParams.mfx.FrameInfo.Height = MSDK_ALIGN16(MSDK_MAX(info.Height, m_mfxVideoParams.mfx.FrameInfo.Height));
    m_pCurrentFreeSurface->frame.Info.Width = m_mfxVideoParams.mfx.FrameInfo.Width;
    m_pCurrentFreeSurface->frame.Info.Height = m_mfxVideoParams.mfx.FrameInfo.Height;

    inMid = m_pCurrentFreeSurface->frame.Data.MemId;

    sts = m_pGeneralAllocator->ReallocFrame(inMid, &m_pCurrentFreeSurface->frame.Info,
        m_pCurrentFreeSurface->frame.Data.MemType, &outMid);
    if (MFX_ERR_NONE == sts)
        m_pCurrentFreeSurface->frame.Data.MemId = outMid;

    return sts;
}

bool IntelQsvVideoDecoder::getRawFrame(VideoRawFrame &frame)
{
    if (isOutputVideoRawFrameReady)
    {
        isOutputVideoRawFrameReady = false;
        frame = m_OutputVideoRawFrame;
        return true;
    }else return false;
}

bool IntelQsvVideoDecoder::addEncodedFrame(const VideoEncodedFrame &frame)
{
    // RTC_LOG(LS_INFO) << "in func: " << __FUNCTION__;

    if (frame.frameSize<=0)
    {
        RTC_LOG(LS_WARNING) << "frame.frameSize<=0";
        return true;
    }
    
    mfxFrameSurface1*   pOutSurface = NULL;
    mfxBitstream*       pBitstream = &m_mfxBS;
    mfxStatus           sts = MFX_ERR_NONE;
    bool                bErrIncompatibleVideoParams = false;
    // time_t              start_time = time(0);

    if (frame.frameSize > pBitstream->MaxLength)
    {
        m_mfxBS.Extend(MSDK_MAX(pBitstream->MaxLength * 2, frame.frameSize));
    }

    if (frame.frameSize) {
        memcpy(pBitstream->Data, frame.frameData.ptr, frame.frameSize);
		pBitstream->DataOffset = 0;
        pBitstream->DataLength = frame.frameSize;
		pBitstream->DecodeTimeStamp = frame.dts;
        pBitstream->TimeStamp  = frame.pts;
		pBitstream->DataFlag = MFX_BITSTREAM_EOS;
    }

    if (!m_bDecoderInited)
    {
        if (frame.frameType!=IDR_Frame)
        {
            return true;
        }else {
            bool ret = this->beginDecodePrivate(m_mfxBS);
            if (!ret)
            {
                return false;
            }else {
                m_PresentationTimeStampReferencePoint = pBitstream->TimeStamp;
                m_DecodingTimeStampReferencePoint = pBitstream->DecodeTimeStamp;
            }
        }
    }

    pBitstream->DecodeTimeStamp = pBitstream->DecodeTimeStamp - m_DecodingTimeStampReferencePoint;
    pBitstream->TimeStamp  = pBitstream->TimeStamp - m_PresentationTimeStampReferencePoint;

    while ((sts == MFX_ERR_NONE) || (MFX_ERR_MORE_DATA == sts) || (MFX_ERR_MORE_SURFACE == sts))
    {
        if ((MFX_ERR_NONE == sts) || (MFX_ERR_MORE_DATA == sts) || (MFX_ERR_MORE_SURFACE == sts)) {
            SyncFrameSurfaces();
            if (!m_pCurrentFreeSurface) {
                m_pCurrentFreeSurface = m_FreeSurfacesPool.GetSurface();
            }
            if (!m_pCurrentFreeSurface || (m_OutputSurfacesPool.GetSurfaceCount() == m_mfxVideoParams.AsyncDepth)) {
                // we stuck with no free surface available, now we will sync...
                sts = SyncOutputSurface(MSDK_DEC_WAIT_INTERVAL);
                if (MFX_ERR_MORE_DATA == sts) {
                    sts = MFX_ERR_NOT_FOUND;
                    if (MFX_ERR_NOT_FOUND == sts) {
                        RTC_LOG(LS_ERROR) << "fatal: failed to find output surface, that's a bug!";
                        break;
                    }
                }
                // note: MFX_WRN_IN_EXECUTION will also be treated as an error at this point
                continue;
            }
            if (!m_pCurrentFreeOutputSurface)
            {
                m_pCurrentFreeOutputSurface = GetFreeOutputSurface();
            }
            if (!m_pCurrentFreeOutputSurface)
            {
                sts = MFX_ERR_NOT_FOUND;
                break;
            }
        }

        if ((MFX_ERR_NONE == sts) || (MFX_ERR_MORE_DATA == sts) || (MFX_ERR_MORE_SURFACE == sts)) {
            pOutSurface = NULL;
            do {
#if (MFX_VERSION >= 1025)
                mfxExtDecodeErrorReport *errorReport = nullptr;
                if (pBitstream)
                {
                    errorReport = m_mfxBS.GetExtBuffer<mfxExtDecodeErrorReport>();
                    // errorReport = (mfxExtDecodeErrorReport *)GetExtBuffer(pBitstream->ExtParam, pBitstream->NumExtParam, MFX_EXTBUFF_DECODE_ERROR_REPORT);
                }
#endif   
                if (m_bDecodeHeader)
                {
                    m_bDecodeHeader = false;
                    if (m_CodecId == MFX_CODEC_HEVC) {
                        sts = m_pmfxDEC->GetVideoParam(&m_mfxVideoParams);
                        if (sts < MFX_ERR_NONE)
                        {
                            RTC_LOG(LS_ERROR) << "m_pmfxDEC->GetVideoParam failed. Error Code : " << sts;

                            memset(&m_mfxVideoParams, 0, sizeof(mfxVideoParam));
                            m_mfxVideoParams.mfx.CodecId = m_CodecId;
                            sts = m_pmfxDEC->DecodeHeader(pBitstream, &m_mfxVideoParams);
                            if(sts<MFX_ERR_NONE) {
                                RTC_LOG(LS_ERROR) << "DecodeHeader Fail. Error Code: " << sts;
                                return false;
                            }
                        }
                    }else {
                        memset(&m_mfxVideoParams, 0, sizeof(mfxVideoParam));
                        m_mfxVideoParams.mfx.CodecId = m_CodecId;
                        sts = m_pmfxDEC->DecodeHeader(pBitstream, &m_mfxVideoParams);
                        if(sts<MFX_ERR_NONE) {
                            RTC_LOG(LS_ERROR) << "DecodeHeader Fail. Error Code: " << sts;
                            return false;
                        }
                    }

                    if (m_CodecId == MFX_CODEC_HEVC) {
                        m_pmfxDEC->SetSkipMode(MFX_SKIPMODE_NOSKIP);
                    }

                	m_mfxVideoParams.IOPattern = MFX_IOPATTERN_OUT_SYSTEM_MEMORY;
                    m_mfxVideoParams.AsyncDepth = m_async_depth;

				    sts = m_pmfxDEC->Reset(&m_mfxVideoParams);
				    if(sts<MFX_ERR_NONE) {
                        RTC_LOG(LS_ERROR) << "Reset Fail. Error Code: " << sts;
				        return false;
				    }
                }
                
                sts = m_pmfxDEC->DecodeFrameAsync(pBitstream, &(m_pCurrentFreeSurface->frame), &pOutSurface, &(m_pCurrentFreeOutputSurface->syncp));

#if (MFX_VERSION >= 1025)
                if (errorReport)
                {
                    PrintDecodeErrorReport(errorReport);
                }
#endif

                if (sts == MFX_WRN_VIDEO_PARAM_CHANGED)
                {
                    RTC_LOG(LS_WARNING) << "MFX_WRN_VIDEO_PARAM_CHANGED";
		            pBitstream->DataOffset = 0;
                    m_bDecodeHeader = true;
                    continue;
                }
                
                if (pBitstream && MFX_ERR_MORE_DATA == sts && pBitstream->MaxLength == pBitstream->DataLength)
                {
                    m_mfxBS.Extend(pBitstream->MaxLength * 2);
                }

                if (MFX_WRN_DEVICE_BUSY == sts) {
                    mfxStatus _sts = SyncOutputSurface(MSDK_DEC_WAIT_INTERVAL);
                    // note: everything except MFX_ERR_NONE are errors at this point
                    if (MFX_ERR_NONE == _sts) {
                        sts = MFX_WRN_DEVICE_BUSY;
                    } else {
                        sts = _sts;
                        if (MFX_ERR_MORE_DATA == sts) {
                            // we can't receive MFX_ERR_MORE_DATA and have no output - that's a bug
                            sts = MFX_WRN_DEVICE_BUSY;//MFX_ERR_NOT_FOUND;
                        }
                    }
                }
            } while (MFX_WRN_DEVICE_BUSY == sts || m_bDecodeHeader);

            if (sts > MFX_ERR_NONE) {
                // ignoring warnings...
                if (m_pCurrentFreeOutputSurface->syncp) {
                    MSDK_SELF_CHECK(pOutSurface);
                    // output is available
                    sts = MFX_ERR_NONE;
                } else {
                    // output is not available
                    sts = MFX_ERR_MORE_SURFACE;
                }
            } else if ((MFX_ERR_MORE_DATA == sts) && pBitstream) {
            } else if ((MFX_ERR_MORE_DATA == sts) && !pBitstream) {
                // that's it - we reached end of stream; now we need to render bufferred data...
                do {
                    sts = SyncOutputSurface(MSDK_DEC_WAIT_INTERVAL);
                } while (MFX_ERR_NONE == sts);

                if (sts == MFX_ERR_MORE_DATA)
                {
                    sts = MFX_ERR_NONE;
                }

                if (sts)
                {
                    RTC_LOG(LS_WARNING) << "SyncOutputSurface failed";
                }
                
                break;
            } else if (MFX_ERR_INCOMPATIBLE_VIDEO_PARAM == sts) {
                RTC_LOG(LS_ERROR) << "Got MFX_ERR_INCOMPATIBLE_VIDEO_PARAM";
                bErrIncompatibleVideoParams = true;
                // need to go to the buffering loop prior to reset procedure

                // pBitstream = NULL;
                // sts = MFX_ERR_NONE;
                // continue;

                break;
            } else if (MFX_ERR_REALLOC_SURFACE == sts) {
                RTC_LOG(LS_WARNING) << "MFX_ERR_REALLOC_SURFACE";
                mfxVideoParam param{};
                sts = m_pmfxDEC->GetVideoParam(&param);
                if (MFX_ERR_NONE != sts) {
                    // need to go to the buffering loop prior to reset procedure

                    // pBitstream = NULL;
                    // sts = MFX_ERR_NONE;
                    // continue;

                    break;
                }

                sts = ReallocCurrentSurface(param.mfx.FrameInfo);
                if (MFX_ERR_NONE != sts) {
                    // need to go to the buffering loop prior to reset procedure

                    // pBitstream = NULL;
                    // sts = MFX_ERR_NONE;
                    
                    break;
                }
                continue;
            }
        }

        if ((MFX_ERR_NONE == sts) || (MFX_ERR_MORE_DATA == sts) || (MFX_ERR_MORE_SURFACE == sts)) {
            // if current free surface is locked we are moving it to the used surfaces array
            /*if (m_pCurrentFreeSurface->frame.Data.Locked)*/ {
                m_UsedSurfacesPool.AddSurface(m_pCurrentFreeSurface);
                m_pCurrentFreeSurface = NULL;
            }
        }
        else
        {
            if (sts < MFX_ERR_NONE) {
                RTC_LOG(LS_ERROR) << "DecodeFrameAsync returned error status";
            }
        }

        if (MFX_ERR_NONE == sts)
        {
            msdkFrameSurface* surface = FindUsedSurface(pOutSurface);

            msdk_atomic_inc16(&(surface->render_lock));

            m_pCurrentFreeOutputSurface->surface = surface;
            m_OutputSurfacesPool.AddSurface(m_pCurrentFreeOutputSurface);
            m_pCurrentFreeOutputSurface = NULL;
        }

        break;
    }//while processing

    SyncOutputSurface(MSDK_DEC_WAIT_INTERVAL);

    if ((MFX_ERR_MORE_DATA == sts) || (MFX_ERR_MORE_SURFACE == sts))
    {
        sts = MFX_ERR_NONE;
    }

    // exit in case of other errors
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "Unexpected error!! Error Code: " << sts;
        return false;
    }

    // // if we exited main decoding loop with ERR_INCOMPATIBLE_PARAM we need to send this status to caller
    // if (bErrIncompatibleVideoParams) {
    //     sts = MFX_ERR_INCOMPATIBLE_VIDEO_PARAM;
    // }

    // RTC_LOG(LS_INFO) << "out func: " << __FUNCTION__;

    return true; // ERR_NONE or ERR_INCOMPATIBLE_VIDEO_PARAM
}

bool IntelQsvVideoDecoder::endDecode()
{
    if (!m_bDecoderInited) return true;

    MSDK_SAFE_DELETE(m_pmfxDEC);

    DeleteFrames();

    m_pPlugin.reset();
    m_mfxSession.Close();

    // allocator if used as external for MediaSDK must be deleted after decoder
    DeleteAllocator();

    m_bDecoderInited = false;

    m_pmfxDEC = NULL;
    bErrorReport = true;

    m_pGeneralAllocator = NULL;
    m_pmfxAllocatorParams = NULL;

    m_bExternalAlloc = false;
    m_bDecOutSysmem = true;

    m_pCurrentFreeSurface = NULL;
    m_pCurrentFreeOutputSurface = NULL;
    m_pCurrentOutputSurface = NULL;

    isOutputVideoRawFrameReady = false;

	m_bDecodeHeader = false;

    return true;
}

VIDEO_STATNDARD IntelQsvVideoDecoder::getVideoStandard()
{
	if (m_CodecId == MFX_CODEC_HEVC)
		return VideoStandard_ITU_H265;
	else
		return VideoStandard_ITU_H264;
}

VIDEO_CODEC_PROVIDER IntelQsvVideoDecoder::getCodecId()
{
	if (m_CodecId == MFX_CODEC_HEVC)
		return HW_CODEC_INTEL_QUICKSYNC_HEVC;
	else
		return HW_CODEC_INTEL_QUICKSYNC;
}

const char* IntelQsvVideoDecoder::getCodecFriendlyName()
{
	return m_CodecFriendlyName.c_str();
}

const std::string IntelQsvVideoDecoder::getCodecGPUName()
{
    return m_HWGPUName;
}

CODEC_RETCODE CreateIntelQsvVideoDecoder(VIDEO_CODEC_PROVIDER codec, HWVideoDecoder** ppVideoDecoder)
{
	RTC_LOG(LS_INFO) << "CODEC_RETCODE CreateIntelQsvVideoDecoder";

	mfxU32 codecId;
	if (codec == HW_CODEC_INTEL_QUICKSYNC){
		codecId = MFX_CODEC_AVC;
		RTC_LOG(LS_INFO) << "[IntelQsvVideoDecoder] H264";
	}
	else if (codec == HW_CODEC_INTEL_QUICKSYNC_HEVC){
		codecId = MFX_CODEC_HEVC;
		RTC_LOG(LS_INFO) << "[IntelQsvVideoDecoder] HEVC";
	}
	else
		return CODEC_ERR_UNSUPPORTED;

	IntelQsvVideoDecoder *decoder = new IntelQsvVideoDecoder(codecId);
	if (decoder && decoder->init()) {
		*ppVideoDecoder = decoder;
		return CODEC_OK; // init succeeded
	}

	delete decoder;
	return CODEC_ERR_UNKNOWN;
}

} // namespace netease