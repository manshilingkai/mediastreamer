#pragma once

#include "modules/video_coding/codecs/winhw/libhwcodec_capability.h"

namespace netease {

class IntelQsvVideoCodecCapability
{
public:
    static IntelQsvVideoCodecCapability &GetInstance();
    HWVideoCodecCapability GetHWVideoCodecCapability();
private:
    IntelQsvVideoCodecCapability();
    ~IntelQsvVideoCodecCapability();
    IntelQsvVideoCodecCapability(const IntelQsvVideoCodecCapability &capability);
    const IntelQsvVideoCodecCapability &operator=(const IntelQsvVideoCodecCapability &capability);
    
    HWVideoCodecCapability hwVideoCodecCapability;

    bool isEnableHWCodec;
};

} // namespace netease