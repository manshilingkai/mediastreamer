// https://www.intel.com/content/www/us/en/developer/articles/technical/video-conferencing-features-of-intel-media-software-development-kit.html

#include "modules/video_coding/codecs/winhw/intel_qsv/intel_qsv_videocodec_capability.h"
#include <windows.h>
#include <dxgi1_2.h>

#include "third_party/mfx/mfxvideo.h"
#include "third_party/mfx/mfxvideo++.h"
#include "third_party/mfx/mfxplugin.h"
#include "third_party/mfx/mfxplugin++.h"

#include "rtc_base/logging.h"

#define INTEL_QSV_MAX_SUPPORTED_WIDTH_FOR_HW_H264_ENCODE 3840
#define INTEL_QSV_MAX_SUPPORTED_HEIHGT_FOR_HW_H264_ENCODE 2160
#define INTEL_QSV_MAX_SUPPORTED_WIDTH_FOR_HW_H264_DECODE 3840
#define INTEL_QSV_MAX_SUPPORTED_HEIHGT_FOR_HW_H264_DECODE 2160

#define INTEL_QSV_MAX_SUPPORTED_WIDTH_FOR_HW_H265_ENCODE 7680
#define INTEL_QSV_MAX_SUPPORTED_HEIHGT_FOR_HW_H265_ENCODE 4320
#define INTEL_QSV_MAX_SUPPORTED_WIDTH_FOR_HW_H265_DECODE 7680
#define INTEL_QSV_MAX_SUPPORTED_HEIHGT_FOR_HW_H265_DECODE 4320

namespace netease {

bool prefer_igpu_enc(int *iGPUIndex)
{
	IDXGIAdapter *pAdapter;
	int adapterIndex = 0;
	bool hasIGPU = false;
	bool hasDGPU = false;

	HMODULE hDXGI = LoadLibrary(L"dxgi.dll");
	if (hDXGI == NULL) {
		return false;
	}

	typedef HRESULT(WINAPI * LPCREATEDXGIFACTORY)(REFIID riid,
						      void **ppFactory);

	LPCREATEDXGIFACTORY pCreateDXGIFactory =
		(LPCREATEDXGIFACTORY)GetProcAddress(hDXGI,
						    "CreateDXGIFactory1");
	if (pCreateDXGIFactory == NULL) {
		pCreateDXGIFactory = (LPCREATEDXGIFACTORY)GetProcAddress(
			hDXGI, "CreateDXGIFactory");

		if (pCreateDXGIFactory == NULL) {
			FreeLibrary(hDXGI);
			return false;
		}
	}

	IDXGIFactory *pFactory = NULL;
	if (FAILED((*pCreateDXGIFactory)(__uuidof(IDXGIFactory),
					 (void **)(&pFactory)))) {
		FreeLibrary(hDXGI);
		return false;
	}

	// Check for i+I cases (Intel discrete + Intel integrated graphics on the same system). Default will be integrated.
	while (SUCCEEDED(pFactory->EnumAdapters(adapterIndex, &pAdapter))) {
		DXGI_ADAPTER_DESC AdapterDesc = {};
		if (SUCCEEDED(pAdapter->GetDesc(&AdapterDesc))) {
			if (AdapterDesc.VendorId == 0x8086) {
				if (AdapterDesc.DedicatedVideoMemory <=
				    512 * 1024 * 1024) {
					hasIGPU = true;
					if (iGPUIndex != NULL) {
						*iGPUIndex = adapterIndex;
					}
				} else {
					hasDGPU = true;
				}
			}
		}
		adapterIndex++;
		pAdapter->Release();
	}

	pFactory->Release();
	FreeLibrary(hDXGI);

	return hasIGPU && hasDGPU;
}

bool get_mfx_platform(mfxPlatform &platform)
{
	mfxIMPL impl = MFX_IMPL_HARDWARE_ANY;
	mfxIMPL impl_list[4] = {MFX_IMPL_HARDWARE, MFX_IMPL_HARDWARE2,
				MFX_IMPL_HARDWARE3, MFX_IMPL_HARDWARE4};
	int igpu_index = -1;
	if (prefer_igpu_enc(&igpu_index)) {
		impl = impl_list[igpu_index];
	}

	mfxVersion version;
	version.Major = 1;
	version.Minor = 0;

	MFXVideoSession mfxSession;
	mfxStatus sts = mfxSession.Init(impl, &version);
	if (sts != MFX_ERR_NONE) {
		return false;
	}

	mfxSession.QueryVersion(&version);

	if(version.Major > 1 || (version.Major ==1 && version.Minor >= 19)) {
		mfxPlatform plat;
		sts = mfxSession.QueryPlatform(&plat);
		if (sts == MFX_ERR_NONE) {
			platform = plat;
			mfxSession.Close();
			return true;
		}else {
			RTC_LOG(LS_ERROR) << "QueryPlatform failed, ret code:" << (int)sts << ".";
		    mfxSession.Close();
		    return false;
		}
	}else {
		mfxSession.Close();
		return false;
	}
}

// refer : https://en.wikipedia.org/wiki/Intel_Quick_Sync_Video
HWVideoCodecCapability GetIntelQsvVideoCodecCapability(bool isEnableHWVideoCodec)
{
  HWVideoCodecCapability qsvVideoCodecCapability;

  if (isEnableHWVideoCodec) {
    mfxPlatform platform;
    bool ret = get_mfx_platform(platform);
    if (ret) {
      if (platform.CodeName >= MFX_PLATFORM_SANDYBRIDGE) {
        qsvVideoCodecCapability.isSupportH264Decode = true;
        qsvVideoCodecCapability.isSupportH264Encode = true;
      }

      if (platform.CodeName >= MFX_PLATFORM_CHERRYTRAIL) {
        qsvVideoCodecCapability.isSupportH265Decode = true;
      }

      if (platform.CodeName >= MFX_PLATFORM_SKYLAKE) {
        qsvVideoCodecCapability.isSupportH265Encode = true;
      }

	  qsvVideoCodecCapability.maxSupportedWidthForH264Encode = INTEL_QSV_MAX_SUPPORTED_WIDTH_FOR_HW_H264_ENCODE;
	  qsvVideoCodecCapability.maxSupportedHeightForH264Encode = INTEL_QSV_MAX_SUPPORTED_HEIHGT_FOR_HW_H264_ENCODE;
	  qsvVideoCodecCapability.maxSupportedWidthForH264Decode = INTEL_QSV_MAX_SUPPORTED_WIDTH_FOR_HW_H264_DECODE;
	  qsvVideoCodecCapability.maxSupportedHeightForH264Decode = INTEL_QSV_MAX_SUPPORTED_HEIHGT_FOR_HW_H264_DECODE;
	  qsvVideoCodecCapability.maxSupportedWidthForH265Encode = INTEL_QSV_MAX_SUPPORTED_WIDTH_FOR_HW_H265_ENCODE;
	  qsvVideoCodecCapability.maxSupportedHeightForH265Encode = INTEL_QSV_MAX_SUPPORTED_HEIHGT_FOR_HW_H265_ENCODE;
	  qsvVideoCodecCapability.maxSupportedWidthForH265Decode = INTEL_QSV_MAX_SUPPORTED_WIDTH_FOR_HW_H265_DECODE;
	  qsvVideoCodecCapability.maxSupportedHeightForH265Decode = INTEL_QSV_MAX_SUPPORTED_HEIHGT_FOR_HW_H265_DECODE;
    }
  } else {
    qsvVideoCodecCapability.isSupportH264Encode = false;
    qsvVideoCodecCapability.isSupportH264Decode = false;
    qsvVideoCodecCapability.isSupportH265Encode = false;
    qsvVideoCodecCapability.isSupportH265Decode = false;
  }

  return qsvVideoCodecCapability;
}

IntelQsvVideoCodecCapability &IntelQsvVideoCodecCapability::GetInstance()
{
    static IntelQsvVideoCodecCapability capability;
    return capability;
}

IntelQsvVideoCodecCapability::IntelQsvVideoCodecCapability()
{
	isEnableHWCodec = true;
    hwVideoCodecCapability = GetIntelQsvVideoCodecCapability(isEnableHWCodec);
}

IntelQsvVideoCodecCapability::~IntelQsvVideoCodecCapability()
{
}

HWVideoCodecCapability IntelQsvVideoCodecCapability::GetHWVideoCodecCapability()
{
    return hwVideoCodecCapability;
}

} // namespace netease