#include "modules/video_coding/codecs/winhw/intel_qsv/intel_qsv_videoutils.h"

#if (defined(_WIN64) || defined(_WIN32)) && (MFX_VERSION >= 1031)
mfxU16 FourCcBitDepth(mfxU32 fourCC)
{
    switch (fourCC)
    {
    case MFX_FOURCC_NV12:
    case MFX_FOURCC_NV16:
    case MFX_FOURCC_YUY2:
    case MFX_FOURCC_AYUV:
        return 8;
        break;

    case MFX_FOURCC_P010:
    case MFX_FOURCC_P210:
#if (MFX_VERSION >= 1027)
    case MFX_FOURCC_Y210:
    case MFX_FOURCC_Y410:
#endif
        return 10;
        break;

#if (MFX_VERSION >= 1031)
    case MFX_FOURCC_P016:
    case MFX_FOURCC_Y216:
    case MFX_FOURCC_Y416:
        return 12;
        break;
#endif
    default:
        return 0;
    }
}
#endif

mfxU16 FourCCToChroma(mfxU32 fourCC)
{
    switch(fourCC)
    {
    case MFX_FOURCC_NV12:
    case MFX_FOURCC_P010:
#if (MFX_VERSION >= 1031)
    case MFX_FOURCC_P016:
#endif
        return MFX_CHROMAFORMAT_YUV420;
    case MFX_FOURCC_NV16:
    case MFX_FOURCC_P210:
#if (MFX_VERSION >= 1027)
    case MFX_FOURCC_Y210:
#endif
#if (MFX_VERSION >= 1031)
    case MFX_FOURCC_Y216:
#endif
    case MFX_FOURCC_YUY2:
    case MFX_FOURCC_UYVY:
        return MFX_CHROMAFORMAT_YUV422;
#if (MFX_VERSION >= 1027)
    case MFX_FOURCC_Y410:
    case MFX_FOURCC_A2RGB10:
#endif
    case MFX_FOURCC_AYUV:
    case MFX_FOURCC_RGB4:
        return MFX_CHROMAFORMAT_YUV444;
    }

    return MFX_CHROMAFORMAT_YUV420;
}

bool CheckVersion(mfxVersion* version, msdkAPIFeature feature)
{
    if (!version) {
        return false;
    }

    mfxU32 ver = MakeVersion(version->Major, version->Minor);

    switch (feature) {
    case MSDK_FEATURE_NONE:
        return true;
    case MSDK_FEATURE_MVC:
        if (ver >= 1003) {
            return true;
        }
        break;
    case MSDK_FEATURE_JPEG_DECODE:
        if (ver >= 1003) {
            return true;
        }
        break;
   case MSDK_FEATURE_LOW_LATENCY:
        if (ver >= 1003) {
            return true;
        }
        break;
    case MSDK_FEATURE_MVC_VIEWOUTPUT:
        if (ver >= 1004) {
            return true;
        }
        break;
    case MSDK_FEATURE_JPEG_ENCODE:
        if (ver >= 1006) {
            return true;
        }
        break;
    case MSDK_FEATURE_LOOK_AHEAD:
        if (ver >= 1007) {
            return true;
        }
        break;
    case MSDK_FEATURE_PLUGIN_API:
        if (ver >= 1008) {
            return true;
        }
        break;
    default:
        return false;
    }
    return false;
}

bool AreGuidsEqual(const mfxPluginUID& guid1, const mfxPluginUID& guid2)
{
    for(size_t i = 0; i != sizeof(mfxPluginUID); i++)
    {
        if (guid1.Data[i] != guid2.Data[i])
            return false;
    }
    return true;
}

const mfxPluginUID & msdkGetPluginUID(mfxIMPL impl, msdkComponentType type, mfxU32 uCodecid)
{
    if (impl == MFX_IMPL_SOFTWARE)
    {
        switch(type)
        {
        case MSDK_VDECODE:
            switch(uCodecid)
            {
            case MFX_CODEC_HEVC:
                return MFX_PLUGINID_HEVCD_SW;
            }
            break;
        case MSDK_VENCODE:
            switch(uCodecid)
            {
            case MFX_CODEC_HEVC:
                return MFX_PLUGINID_HEVCE_SW;
            }
            break;
        }
    }
    else
    {
        switch(type)
        {
        case MSDK_VENCODE:
            break;
#if MFX_VERSION >= 1027
        case (MSDK_VENCODE | MSDK_FEI):
            switch (uCodecid)
            {
            case MFX_CODEC_HEVC:
                return MFX_PLUGINID_HEVC_FEI_ENCODE;
            }
            break;
#endif
        case MSDK_VENC:
            switch(uCodecid)
            {
            case MFX_CODEC_HEVC:
                return MFX_PLUGINID_HEVCE_FEI_HW;   // HEVC FEI uses ENC interface
            }
            break;
        }
    }

    return MSDK_PLUGINGUID_NULL;
}

void FreeSurfacePool(mfxFrameSurface1* pSurfacesPool, mfxU16 nPoolSize)
{
    if (pSurfacesPool)
    {
        for (mfxU16 i = 0; i < nPoolSize; i++)
        {
            pSurfacesPool[i].Data.Locked = 0;
        }
    }
}

mfxU16 GetFreeSurface(mfxFrameSurface1* pSurfacesPool, mfxU16 nPoolSize)
{
    mfxU32 SleepInterval = 10; // milliseconds

    mfxU16 idx = MSDK_INVALID_SURF_IDX;

    mfxU32 remainingWaitInterval = MSDK_SURFACE_WAIT_INTERVAL;
    //wait if there's no free surface
    do
    {
        idx = GetFreeSurfaceIndex(pSurfacesPool, nPoolSize);

        if (MSDK_INVALID_SURF_IDX != idx)
        {
            break;
        }
        else
        {
            MSDK_SLEEP(SleepInterval);
            remainingWaitInterval -= SleepInterval;
        }
        
    } while ( remainingWaitInterval > 0 );

    if(idx==MSDK_INVALID_SURF_IDX)
    {
        RTC_LOG(LS_ERROR) << MSDK_STRING("ERROR: No free surfaces in pool (during long period)\n");
    }

    return idx;
}

/* Thread-safe 16-bit variable decrementing */
mfxU16 msdk_atomic_dec16(volatile mfxU16 *pVariable)
{
    return _InterlockedDecrement16((volatile short*)pVariable);
}

mfxU16 msdk_atomic_inc16(volatile mfxU16 *pVariable)
{
    return _InterlockedIncrement16((volatile short*)pVariable);
}

msdk_tick msdk_time_get_tick(void)
{
    LARGE_INTEGER t1;

    QueryPerformanceCounter(&t1);
    return t1.QuadPart;
}

msdk_tick msdk_time_get_frequency(void)
{
    LARGE_INTEGER t1;

    QueryPerformanceFrequency(&t1);
    return t1.QuadPart;
}