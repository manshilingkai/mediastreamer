#pragma once
#include <iostream>
#include <iomanip> // for std::setfill, std::setw
#include <memory> // for std::unique_ptr
#include <tchar.h>
#include "third_party/mfx/mfxvideo.h"
#include "third_party/mfx/mfxvideo++.h"
#include "third_party/mfx/mfxplugin.h"
#include "third_party/mfx/mfxplugin++.h"
#include "rtc_base/logging.h"
#include <windows.h>

#ifndef MSDK_MAX
#define MSDK_MAX(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef MSDK_MIN
#define MSDK_MIN(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#define MSDK_WAIT_INTERVAL 500

#define MSDK_ALIGN16(value)                      (((value + 15) >> 4) << 4) // round up to a multiple of 16
#define MSDK_ALIGN32(value)                      (((value + 31) >> 5) << 5) // round up to a multiple of 32

#ifndef MSDK_SAFE_DELETE
#define MSDK_SAFE_DELETE(P)                      {if (P) {delete P; P = NULL;}}
#endif // MSDK_SAFE_DELETE
#define MSDK_SAFE_DELETE_ARRAY(P)                {if (P) {delete[] P; P = NULL;}}
#define MSDK_ZERO_MEMORY(VAR)                    {memset(&VAR, 0, sizeof(VAR));}
#define MSDK_MEMCPY_VAR(dstVarName, src, count) memcpy_s(&(dstVarName), sizeof(dstVarName), (src), (count))

#define MSDK_MAX_FILENAME_LEN 1024
#define msdk_strnlen(str,lenmax) strnlen_s(str,lenmax)

#if (defined(_WIN64) || defined(_WIN32)) && (MFX_VERSION >= 1031)
mfxU16 FourCcBitDepth(mfxU32 fourCC);
#endif

mfxU16 FourCCToChroma(mfxU32 fourCC);

inline
mfxU32 MakeVersion(mfxU16 major, mfxU16 minor)
{
    return major * 1000 + minor;
}

enum msdkAPIFeature {
    MSDK_FEATURE_NONE,
    MSDK_FEATURE_MVC,
    MSDK_FEATURE_JPEG_DECODE,
    MSDK_FEATURE_LOW_LATENCY,
    MSDK_FEATURE_MVC_VIEWOUTPUT,
    MSDK_FEATURE_JPEG_ENCODE,
    MSDK_FEATURE_LOOK_AHEAD,
    MSDK_FEATURE_PLUGIN_API
};

/* Returns true if feature is supported in the given API version */
bool CheckVersion(mfxVersion* version, msdkAPIFeature feature);

#define MSDK_SLEEP(msec) Sleep(msec)
typedef mfxI64 msdk_tick;
msdk_tick msdk_time_get_tick(void);
msdk_tick msdk_time_get_frequency(void);

/*
* Rationale: class to load+register any mediasdk plugin decoder/encoder/generic by given name
*/
#define MSDK_STRING(x) _T(x)
#define MSDK_CHAR(x) _T(x)
#define MSDK_MEMCPY(dst, src, count) memcpy_s(dst, (count), (src), (count))
typedef TCHAR msdk_char;
typedef std::basic_stringstream<msdk_char> msdk_stringstream;
static const mfxPluginUID MSDK_PLUGINGUID_NULL = {{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};

typedef mfxI32 msdkComponentType;
enum
{
    MSDK_VDECODE = 0x0001,
    MSDK_VENCODE = 0x0002,
    MSDK_VPP     = 0x0004,
    MSDK_VENC    = 0x0008,
#if (MFX_VERSION >= 1027)
    MSDK_FEI     = 0x1000,
#endif
};

bool AreGuidsEqual(const mfxPluginUID& guid1, const mfxPluginUID& guid2);
const mfxPluginUID & msdkGetPluginUID(mfxIMPL impl, msdkComponentType type, mfxU32 uCodecid);

class PluginLoader : public MFXPlugin
{
protected:
    mfxPluginType     ePluginType;

    mfxSession        m_session;
    mfxPluginUID      m_uid;

private:
    const msdk_char* msdkGetPluginName(const mfxPluginUID& guid)
    {
        if (AreGuidsEqual(guid, MFX_PLUGINID_HEVCD_SW))
            return MSDK_STRING("Intel (R) Media SDK plugin for HEVC DECODE");
        else if(AreGuidsEqual(guid, MFX_PLUGINID_HEVCD_HW))
            return MSDK_STRING("Intel (R) Media SDK HW plugin for HEVC DECODE");
        else if(AreGuidsEqual(guid, MFX_PLUGINID_HEVCE_SW))
            return MSDK_STRING("Intel (R) Media SDK plugin for HEVC ENCODE");
        else if(AreGuidsEqual(guid, MFX_PLUGINID_HEVCE_HW))
            return MSDK_STRING("Intel (R) Media SDK HW plugin for HEVC ENCODE");
        else if(AreGuidsEqual(guid, MFX_PLUGINID_VP8E_HW))
            return MSDK_STRING("Intel (R) Media SDK HW plugin for VP8 ENCODE");
        else if(AreGuidsEqual(guid, MFX_PLUGINID_VP8D_HW))
            return MSDK_STRING("Intel (R) Media SDK HW plugin for VP8 DECODE");
        else if(AreGuidsEqual(guid, MFX_PLUGINID_VP9E_HW))
            return MSDK_STRING("Intel (R) Media SDK HW plugin for VP9 ENCODE");
        else if(AreGuidsEqual(guid, MFX_PLUGINID_VP9D_HW))
            return MSDK_STRING("Intel (R) Media SDK HW plugin for VP9 DECODE");
        else if(AreGuidsEqual(guid, MFX_PLUGINID_H264LA_HW))
            return MSDK_STRING("Intel (R) Media SDK plugin for LA ENC");
        else if(AreGuidsEqual(guid, MFX_PLUGINID_ITELECINE_HW))
            return MSDK_STRING("Intel (R) Media SDK PTIR plugin (HW)");
        else if (AreGuidsEqual(guid, MFX_PLUGINID_HEVCE_GACC))
            return MSDK_STRING("Intel (R) Media SDK GPU-Accelerated plugin for HEVC ENCODE");
        else
#if (MFX_VERSION >= 1027) && !defined(_WIN32) && !defined(_WIN64)
        if (AreGuidsEqual(guid, MFX_PLUGINID_HEVC_FEI_ENCODE))
            return MSDK_STRING("Intel (R) Media SDK HW plugin for HEVC FEI ENCODE");
        else
#endif
            return MSDK_STRING("Unknown plugin");
    }

public:
    PluginLoader(mfxPluginType type, mfxSession session, const mfxPluginUID & uid, mfxU32 version, const mfxChar *pluginName, mfxU32 len)
        : ePluginType(type)
        , m_session()
        , m_uid()
    {
        mfxStatus sts = MFX_ERR_NONE;
        msdk_stringstream strStream;

        MSDK_MEMCPY(&m_uid, &uid, sizeof(mfxPluginUID));
        for (size_t i = 0; i != sizeof(mfxPluginUID); i++)
        {
            strStream << MSDK_STRING("0x") << std::setfill(MSDK_CHAR('0')) << std::setw(2) << std::hex << (int)m_uid.Data[i];
            if (i != (sizeof(mfxPluginUID)-1)) strStream << MSDK_STRING(", ");
        }

        if ((ePluginType == MFX_PLUGINTYPE_AUDIO_DECODE) ||
            (ePluginType == MFX_PLUGINTYPE_AUDIO_ENCODE))
        {
            // Audio plugins are not loaded by path
            sts = MFX_ERR_UNSUPPORTED;
        }
        else
        {
            sts = MFXVideoUSER_LoadByPath(session, &m_uid, version, pluginName, len);
        }

        if (MFX_ERR_NONE != sts)
        {
            RTC_LOG(LS_ERROR) << MSDK_STRING("Failed to load plugin from GUID, sts=") << sts << MSDK_STRING(": { ") << strStream.str().c_str() << MSDK_STRING(" } (") << msdkGetPluginName(m_uid) << MSDK_STRING(")");
        }
        else
        {
            RTC_LOG(LS_INFO) << MSDK_STRING("Plugin was loaded from GUID");
            m_session = session;
        }
    }

    PluginLoader(mfxPluginType type, mfxSession session, const mfxPluginUID & uid, mfxU32 version)
        : ePluginType(type)
        , m_session()
        , m_uid()
    {
        mfxStatus sts = MFX_ERR_NONE;
        msdk_stringstream strStream;

        MSDK_MEMCPY(&m_uid, &uid, sizeof(mfxPluginUID));
        for (size_t i = 0; i != sizeof(mfxPluginUID); i++)
        {
            strStream << MSDK_STRING("0x") << std::setfill(MSDK_CHAR('0')) << std::setw(2) << std::hex << (int)m_uid.Data[i];
            if (i != (sizeof(mfxPluginUID)-1)) strStream << MSDK_STRING(", ");
        }

        if ((ePluginType == MFX_PLUGINTYPE_AUDIO_DECODE) ||
            (ePluginType == MFX_PLUGINTYPE_AUDIO_ENCODE))
        {
            sts = MFXAudioUSER_Load(session, &m_uid, version);
        }
        else
        {
            sts = MFXVideoUSER_Load(session, &m_uid, version);
        }

        if (MFX_ERR_NONE != sts)
        {
            RTC_LOG(LS_ERROR) << MSDK_STRING("Failed to load plugin from GUID, sts=") << sts << MSDK_STRING(": { ") << strStream.str().c_str() << MSDK_STRING(" } (") << msdkGetPluginName(m_uid) << MSDK_STRING(")");
        }
        else
        {
            RTC_LOG(LS_INFO) << MSDK_STRING("Plugin was loaded from GUID")<< MSDK_STRING(": { ") << strStream.str().c_str() << MSDK_STRING(" } (") << msdkGetPluginName(m_uid) << MSDK_STRING(")");
            m_session = session;
        }
    }

    virtual ~PluginLoader()
    {
        mfxStatus sts = MFX_ERR_NONE;
        if (m_session)
        {
            if ((ePluginType == MFX_PLUGINTYPE_AUDIO_DECODE) ||
                (ePluginType == MFX_PLUGINTYPE_AUDIO_ENCODE))
            {
                sts = MFXAudioUSER_UnLoad(m_session, &m_uid);
            }
            else
            {
                sts = MFXVideoUSER_UnLoad(m_session, &m_uid);
            }

            if (sts != MFX_ERR_NONE)
            {
                RTC_LOG(LS_ERROR) << MSDK_STRING("Failed to unload plugin from GUID, sts=") << sts;
            }
            else
            {
                RTC_LOG(LS_INFO) << MSDK_STRING("MFXBaseUSER_UnLoad(session=0x") << m_session << MSDK_STRING("), sts=") << sts;
            }
        }
    }

    bool IsOk() {
        return m_session != 0;
    }
    virtual mfxStatus PluginInit( mfxCoreInterface * /*core*/ ) {
        return MFX_ERR_NULL_PTR;
    }
    virtual mfxStatus PluginClose() {
        return MFX_ERR_NULL_PTR;
    }
    virtual mfxStatus GetPluginParam( mfxPluginParam * /*par*/ ) {
        return MFX_ERR_NULL_PTR;
    }
    virtual mfxStatus Execute( mfxThreadTask /*task*/, mfxU32 /*uid_p*/, mfxU32 /*uid_a*/ ) {
        return MFX_ERR_NULL_PTR;
    }
    virtual mfxStatus FreeResources( mfxThreadTask /*task*/, mfxStatus /*sts*/ ) {
        return MFX_ERR_NULL_PTR;
    }
    virtual void Release() {
    }
    virtual mfxStatus Close() {
        return MFX_ERR_NULL_PTR;
    }
    virtual mfxStatus SetAuxParams( void* /*auxParam*/, int /*auxParamSize*/ ) {
        return MFX_ERR_NULL_PTR;
    }
};

inline MFXPlugin * LoadPluginByType(mfxPluginType type, mfxSession session, const mfxPluginUID & uid, mfxU32 version, const mfxChar *pluginName, mfxU32 len) {
    std::unique_ptr<PluginLoader> plg(new PluginLoader (type, session, uid, version, pluginName, len));
    return plg->IsOk() ? plg.release() : NULL;
}

inline MFXPlugin * LoadPluginByGUID(mfxPluginType type, mfxSession session, const mfxPluginUID & uid, mfxU32 version) {
    std::unique_ptr<PluginLoader> plg(new PluginLoader (type, session, uid, version));
    return plg->IsOk() ? plg.release() : NULL;
}

inline MFXPlugin * LoadPlugin(mfxPluginType type, mfxSession session, const mfxPluginUID & uid, mfxU32 version, const mfxChar *pluginName, mfxU32 len) {
    return LoadPluginByType(type, session, uid, version, pluginName, len);
}

inline MFXPlugin * LoadPlugin(mfxPluginType type, mfxSession session, const mfxPluginUID & uid, mfxU32 version) {
    return LoadPluginByGUID(type, session, uid, version);
}

// class is used as custom exception
class mfxError : public std::runtime_error
{
public:
    mfxError(mfxStatus status = MFX_ERR_UNKNOWN, std::string msg = "")
        : runtime_error(msg)
        , m_Status(status)
    {}

    mfxStatus GetStatus() const
    { return m_Status; }

private:
    mfxStatus m_Status;
};

//declare used extension buffers
template<class T>
struct mfx_ext_buffer_id{};

template<>struct mfx_ext_buffer_id<mfxExtCodingOption>{
    enum {id = MFX_EXTBUFF_CODING_OPTION};
};
template<>struct mfx_ext_buffer_id<mfxExtCodingOption2>{
    enum {id = MFX_EXTBUFF_CODING_OPTION2};
};
template<>struct mfx_ext_buffer_id<mfxExtCodingOption3>{
    enum {id = MFX_EXTBUFF_CODING_OPTION3};
};
template<>struct mfx_ext_buffer_id<mfxExtAvcTemporalLayers>{
    enum {id = MFX_EXTBUFF_AVC_TEMPORAL_LAYERS};
};
template<>struct mfx_ext_buffer_id<mfxExtAVCRefListCtrl>{
    enum {id = MFX_EXTBUFF_AVC_REFLIST_CTRL};
};
template<>struct mfx_ext_buffer_id<mfxExtThreadsParam>{
    enum {id = MFX_EXTBUFF_THREADS_PARAM};
};
template<>struct mfx_ext_buffer_id<mfxExtHEVCRefLists>{
    enum {id = MFX_EXTBUFF_HEVC_REFLISTS};
};
template<>struct mfx_ext_buffer_id<mfxExtHEVCParam> {
    enum {id = MFX_EXTBUFF_HEVC_PARAM};
};
template<>struct mfx_ext_buffer_id<mfxExtDecVideoProcessing> {
    enum {id = MFX_EXTBUFF_DEC_VIDEO_PROCESSING};
};
template<>struct mfx_ext_buffer_id<mfxExtDecodeErrorReport> {
    enum {id = MFX_EXTBUFF_DECODE_ERROR_REPORT};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPDoNotUse> {
    enum {id = MFX_EXTBUFF_VPP_DONOTUSE};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPDoUse> {
    enum {id = MFX_EXTBUFF_VPP_DOUSE};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPDeinterlacing> {
    enum {id = MFX_EXTBUFF_VPP_DEINTERLACING};
};
template<>struct mfx_ext_buffer_id<mfxExtCodingOptionSPSPPS> {
    enum {id = MFX_EXTBUFF_CODING_OPTION_SPSPPS};
};
template<>struct mfx_ext_buffer_id<mfxExtOpaqueSurfaceAlloc> {
    enum {id = MFX_EXTBUFF_OPAQUE_SURFACE_ALLOCATION};
};
template<>struct mfx_ext_buffer_id<mfxExtVppMctf> {
    enum {id = MFX_EXTBUFF_VPP_MCTF};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPComposite> {
    enum {id = MFX_EXTBUFF_VPP_COMPOSITE};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPFieldProcessing> {
    enum {id = MFX_EXTBUFF_VPP_FIELD_PROCESSING};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPDetail> {
    enum {id = MFX_EXTBUFF_VPP_DETAIL};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPFrameRateConversion> {
    enum {id = MFX_EXTBUFF_VPP_FRAME_RATE_CONVERSION};
};
template<>struct mfx_ext_buffer_id<mfxExtMultiFrameControl> {
    enum {id = MFX_EXTBUFF_MULTI_FRAME_CONTROL};
};
template<>struct mfx_ext_buffer_id<mfxExtMultiFrameParam> {
    enum {id = MFX_EXTBUFF_MULTI_FRAME_PARAM};
};
template<>struct mfx_ext_buffer_id<mfxExtHEVCTiles> {
    enum {id = MFX_EXTBUFF_HEVC_TILES};
};
template<>struct mfx_ext_buffer_id<mfxExtVP9Param> {
    enum {id = MFX_EXTBUFF_VP9_PARAM};
};
template<>struct mfx_ext_buffer_id<mfxExtVideoSignalInfo> {
    enum {id = MFX_EXTBUFF_VIDEO_SIGNAL_INFO};
};
template<>struct mfx_ext_buffer_id<mfxExtHEVCRegion> {
    enum {id = MFX_EXTBUFF_HEVC_REGION};
};
template<>struct mfx_ext_buffer_id<mfxExtAVCRoundingOffset> {
    enum {id = MFX_EXTBUFF_AVC_ROUNDING_OFFSET};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPDenoise> {
    enum {id = MFX_EXTBUFF_VPP_DENOISE};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPProcAmp> {
    enum {id = MFX_EXTBUFF_VPP_PROCAMP};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPImageStab> {
    enum {id = MFX_EXTBUFF_VPP_IMAGE_STABILIZATION};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPVideoSignalInfo> {
    enum {id = MFX_EXTBUFF_VPP_VIDEO_SIGNAL_INFO};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPMirroring> {
    enum {id = MFX_EXTBUFF_VPP_MIRRORING};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPColorFill> {
    enum {id = MFX_EXTBUFF_VPP_COLORFILL};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPRotation> {
    enum {id = MFX_EXTBUFF_VPP_ROTATION};
};
template<>struct mfx_ext_buffer_id<mfxExtVPPScaling> {
    enum {id = MFX_EXTBUFF_VPP_SCALING};
};
template<>struct mfx_ext_buffer_id<mfxExtColorConversion> {
    enum {id = MFX_EXTBUFF_VPP_COLOR_CONVERSION};
};
template<>struct mfx_ext_buffer_id<mfxExtPredWeightTable> {
    enum {id = MFX_EXTBUFF_PRED_WEIGHT_TABLE};
};
template<>struct mfx_ext_buffer_id<mfxExtAVCEncodedFrameInfo>{
    enum {id = MFX_EXTBUFF_ENCODED_FRAME_INFO};
};

constexpr uint16_t max_num_ext_buffers = 63 * 2; // '*2' is for max estimation if all extBuffer were 'paired'

//helper function to initialize mfx ext buffer structure
template <class T>
void init_ext_buffer(T & ext_buffer)
{
    memset(&ext_buffer, 0, sizeof(ext_buffer));
    reinterpret_cast<mfxExtBuffer*>(&ext_buffer)->BufferId = mfx_ext_buffer_id<T>::id;
    reinterpret_cast<mfxExtBuffer*>(&ext_buffer)->BufferSz = sizeof(ext_buffer);
}

template <typename T> struct IsPairedMfxExtBuffer                 : std::false_type {};
template <> struct IsPairedMfxExtBuffer<mfxExtAVCRefListCtrl>     : std::true_type {};
template <> struct IsPairedMfxExtBuffer<mfxExtAVCRoundingOffset>  : std::true_type {};
template <> struct IsPairedMfxExtBuffer<mfxExtPredWeightTable>    : std::true_type {};

template <typename R>
struct ExtParamAccessor
{
private:
    using mfxExtBufferDoublePtr = mfxExtBuffer**;
public:
    mfxU16& NumExtParam;
    mfxExtBufferDoublePtr& ExtParam;
    ExtParamAccessor(const R& r):
        NumExtParam(const_cast<mfxU16&>(r.NumExtParam)),
        ExtParam(const_cast<mfxExtBufferDoublePtr&>(r.ExtParam)) {}
};

template <>
struct ExtParamAccessor<mfxFrameSurface1>
{
private:
    using mfxExtBufferDoublePtr = mfxExtBuffer**;
public:
    mfxU16& NumExtParam;
    mfxExtBufferDoublePtr& ExtParam;
    ExtParamAccessor(const mfxFrameSurface1& r):
        NumExtParam(const_cast<mfxU16&>(r.Data.NumExtParam)),
        ExtParam(const_cast<mfxExtBufferDoublePtr&>(r.Data.ExtParam)) {}
};

/** ExtBufHolder is an utility class which
 *  provide interface for mfxExtBuffer objects management in any mfx structure (e.g. mfxVideoParam)
 */
template<typename T>
class ExtBufHolder : public T
{
public:
    ExtBufHolder() : T()
    {
        m_ext_buf.reserve(max_num_ext_buffers);
    }

    ~ExtBufHolder() // only buffers allocated by wrapper can be released
    {
        for (auto it = m_ext_buf.begin(); it != m_ext_buf.end(); it++ )
        {
            delete [] (mfxU8*)(*it);
        }
    }

    ExtBufHolder(const ExtBufHolder& ref)
    {
        m_ext_buf.reserve(max_num_ext_buffers);
        *this = ref; // call to operator=
    }

    ExtBufHolder& operator=(const ExtBufHolder& ref)
    {
        const T* src_base = &ref;
        return operator=(*src_base);
    }

    ExtBufHolder(const T& ref)
    {
        *this = ref; // call to operator=
    }

    ExtBufHolder& operator=(const T& ref)
    {
        // copy content of main structure type T
        T* dst_base = this;
        const T* src_base = &ref;
        *dst_base = *src_base;

        //remove all existing extension buffers
        ClearBuffers();

        const auto ref_ = ExtParamAccessor<T>(ref);

        //reproduce list of extension buffers and copy its content
        for (size_t i = 0; i < ref_.NumExtParam; ++i)
        {
            const auto src_buf = ref_.ExtParam[i];
            if (!src_buf) throw mfxError(MFX_ERR_NULL_PTR, "Null pointer attached to source ExtParam");
            if (!IsCopyAllowed(src_buf->BufferId))
            {
                auto msg = "Deep copy of '" + Fourcc2Str(src_buf->BufferId) + "' extBuffer is not allowed";
                throw mfxError(MFX_ERR_UNDEFINED_BEHAVIOR, msg);
            }

            // 'false' below is because here we just copy extBuffer's one by one
            auto dst_buf = AddExtBuffer(src_buf->BufferId, src_buf->BufferSz, false);
            // copy buffer content w/o restoring its type
            memcpy((void*)dst_buf, (void*)src_buf, src_buf->BufferSz);
        }

        return *this;
    }

    ExtBufHolder(ExtBufHolder &&)             = default;
    ExtBufHolder & operator= (ExtBufHolder&&) = default;

    // Always returns a valid pointer or throws an exception
    template<typename TB>
    TB* AddExtBuffer()
    {
        mfxExtBuffer* b = AddExtBuffer(mfx_ext_buffer_id<TB>::id, sizeof(TB), IsPairedMfxExtBuffer<TB>::value);
        return (TB*)b;
    }

    template<typename TB>
    void RemoveExtBuffer()
    {
        auto it = std::find_if(m_ext_buf.begin(), m_ext_buf.end(), CmpExtBufById(mfx_ext_buffer_id<TB>::id));
        if (it != m_ext_buf.end())
        {
            delete [] (mfxU8*)(*it);
            it = m_ext_buf.erase(it);

            if (IsPairedMfxExtBuffer<TB>::value)
            {
                if (it == m_ext_buf.end() || (*it)->BufferId != mfx_ext_buffer_id<TB>::id)
                    throw mfxError(MFX_ERR_NULL_PTR, "RemoveExtBuffer: ExtBuffer's parity has been broken");

                delete [] (mfxU8*)(*it);
                m_ext_buf.erase(it);
            }

            RefreshBuffers();
        }
    }

    template <typename TB>
    TB* GetExtBuffer(uint32_t fieldId = 0) const
    {
        return (TB*)FindExtBuffer(mfx_ext_buffer_id<TB>::id, fieldId);
    }

    template <typename TB>
    operator TB*()
    {
        return (TB*)FindExtBuffer(mfx_ext_buffer_id<TB>::id, 0);
    }

    template <typename TB>
    operator TB*() const
    {
        return (TB*)FindExtBuffer(mfx_ext_buffer_id<TB>::id, 0);
    }

private:

    mfxExtBuffer* AddExtBuffer(mfxU32 id, mfxU32 size, bool isPairedExtBuffer)
    {
        if (!size || !id)
            throw mfxError(MFX_ERR_NULL_PTR, "AddExtBuffer: wrong size or id!");

        auto it = std::find_if(m_ext_buf.begin(), m_ext_buf.end(), CmpExtBufById(id));
        if (it == m_ext_buf.end())
        {
            auto buf = (mfxExtBuffer*)new mfxU8[size];
            memset(buf, 0, size);
            m_ext_buf.push_back(buf);

            buf->BufferId = id;
            buf->BufferSz = size;

            if (isPairedExtBuffer)
            {
                // Allocate the other mfxExtBuffer _right_after_ the first one ...
                buf = (mfxExtBuffer*)new mfxU8[size];
                memset(buf, 0, size);
                m_ext_buf.push_back(buf);

                buf->BufferId = id;
                buf->BufferSz = size;

                RefreshBuffers();
                return m_ext_buf[m_ext_buf.size() - 2]; // ... and return a pointer to the first one
            }

            RefreshBuffers();
            return m_ext_buf.back();
        }

        return *it;
    }

    mfxExtBuffer* FindExtBuffer(mfxU32 id, uint32_t fieldId) const
    {
        auto it = std::find_if(m_ext_buf.begin(), m_ext_buf.end(), CmpExtBufById(id));
        if (fieldId && it != m_ext_buf.end())
        {
            ++it;
            return it != m_ext_buf.end() ? *it : nullptr;
        }
        return it != m_ext_buf.end() ? *it : nullptr;
    }

    void RefreshBuffers()
    {
        auto this_ = ExtParamAccessor<T>(*this);
        this_.NumExtParam = static_cast<mfxU16>(m_ext_buf.size());
        this_.ExtParam    = this_.NumExtParam ? m_ext_buf.data() : nullptr;
    }

    void ClearBuffers()
    {
        if (m_ext_buf.size())
        {
            for (auto it = m_ext_buf.begin(); it != m_ext_buf.end(); it++ )
            {
                delete [] (mfxU8*)(*it);
            }
            m_ext_buf.clear();
        }
        RefreshBuffers();
    }

    bool IsCopyAllowed(mfxU32 id)
    {
        static const mfxU32 allowed[] = {
            MFX_EXTBUFF_CODING_OPTION,
            MFX_EXTBUFF_CODING_OPTION2,
            MFX_EXTBUFF_CODING_OPTION3,
            // MFX_EXTBUFF_FEI_PARAM,
            // MFX_EXTBUFF_BRC,
            MFX_EXTBUFF_HEVC_PARAM,
            MFX_EXTBUFF_VP9_PARAM,
            MFX_EXTBUFF_OPAQUE_SURFACE_ALLOCATION,
            // MFX_EXTBUFF_FEI_PPS,
            // MFX_EXTBUFF_FEI_SPS,
            // MFX_EXTBUFF_LOOKAHEAD_CTRL,
            // MFX_EXTBUFF_LOOKAHEAD_STAT,
            MFX_EXTBUFF_DEC_VIDEO_PROCESSING
        };

        auto it = std::find_if(std::begin(allowed), std::end(allowed),
                               [&id](const mfxU32 allowed_id)
                               {
                                   return allowed_id == id;
                               });
        return it != std::end(allowed);
    }

    struct CmpExtBufById
    {
        mfxU32 id;

        CmpExtBufById(mfxU32 _id)
            : id(_id)
        { };

        bool operator () (mfxExtBuffer* b)
        {
            return  (b && b->BufferId == id);
        };
    };

    static std::string Fourcc2Str(mfxU32 fourcc)
    {
        std::string s;
        for (size_t i = 0; i < 4; i++)
        {
            s.push_back(*(i + (char*)&fourcc));
        }
        return s;
    }

    std::vector<mfxExtBuffer*> m_ext_buf;
};

using MfxVideoParamsWrapper = ExtBufHolder<mfxVideoParam>;
using mfxInitParamlWrap     = ExtBufHolder<mfxInitParam>;
using mfxEncodeCtrlWrap     = ExtBufHolder<mfxEncodeCtrl>;

class mfxBitstreamWrapper : public ExtBufHolder<mfxBitstream>
{
    typedef ExtBufHolder<mfxBitstream> base;
public:
    mfxBitstreamWrapper()
        : base()
    {}

    mfxBitstreamWrapper(mfxU32 n_bytes)
        : base()
    {
        Extend(n_bytes);
    }

    mfxBitstreamWrapper(const mfxBitstreamWrapper & bs_wrapper)
        : base(bs_wrapper)
        , m_data(bs_wrapper.m_data)
    {
        Data = m_data.data();
    }

    mfxBitstreamWrapper& operator=(mfxBitstreamWrapper const& bs_wrapper)
    {
        mfxBitstreamWrapper tmp(bs_wrapper);

        *this = std::move(tmp);

        return *this;
    }

    mfxBitstreamWrapper(mfxBitstreamWrapper && bs_wrapper)             = default;
    mfxBitstreamWrapper & operator= (mfxBitstreamWrapper&& bs_wrapper) = default;
    ~mfxBitstreamWrapper()                                             = default;

    void Extend(mfxU32 n_bytes)
    {
        if (MaxLength >= n_bytes)
            return;

        m_data.resize(n_bytes);

        Data      = m_data.data();
        MaxLength = n_bytes;
    }
private:
    std::vector<mfxU8> m_data;
};

#define MSDK_INVALID_SURF_IDX 0xFFFF
#define MSDK_SURFACE_WAIT_INTERVAL 500

template <class T>
mfxU16 GetFreeSurfaceIndex(T* pSurfacesPool, mfxU16 nPoolSize)
{
    // constexpr mfxU16 MSDK_INVALID_SURF_IDX = 0xffff;

    if (pSurfacesPool)
    {
        for (mfxU16 i = 0; i < nPoolSize; i++)
        {
            if (0 == pSurfacesPool[i].Data.Locked)
            {
                return i;
            }
        }
    }
    return MSDK_INVALID_SURF_IDX;
}

mfxU16 GetFreeSurface(mfxFrameSurface1* pSurfacesPool, mfxU16 nPoolSize);
void FreeSurfacePool(mfxFrameSurface1* pSurfacesPool, mfxU16 nPoolSize);

class FPSLimiter
{
public:
    FPSLimiter()  = default;
    ~FPSLimiter() = default;
    void Reset(mfxU32 fps)
    {
        m_delayTicks = fps ? msdk_time_get_frequency() / fps : 0;
    }
    void Work()
    {
        msdk_tick current_tick = msdk_time_get_tick();
        while( m_delayTicks && (m_startTick + m_delayTicks > current_tick) )
        {
            msdk_tick left_tick = m_startTick + m_delayTicks - current_tick;
            uint32_t sleepTime = (uint32_t)(left_tick * 1000 / msdk_time_get_frequency());
            MSDK_SLEEP(sleepTime);
            current_tick = msdk_time_get_tick();
        };
        m_startTick = msdk_time_get_tick();
    }

protected:
    msdk_tick               m_startTick  = 0;
    msdk_tick               m_delayTicks = 0;
};

typedef enum {
    MFX_PLUGINLOAD_TYPE_GUID = 1,
    MFX_PLUGINLOAD_TYPE_FILE = 2
} MfxPluginLoadType;

struct MfxPluginParameter
{
    MfxPluginParameter()
    {
		memset(this, 0, sizeof(*this));
    }

    mfxPluginUID      pluginGuid;
    mfxChar           strPluginPath[MSDK_MAX_FILENAME_LEN];
    MfxPluginLoadType type;
};

// A macro to disallow the copy constructor and operator= functions
// This should be used in the private: declarations for a class
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName&);               \
    void operator=(const TypeName&)


/* Thread-safe 16-bit variable decrementing */
mfxU16 msdk_atomic_dec16(volatile mfxU16 *pVariable);

#define MSDK_SAFE_FREE(X)                        {if (X) { free(X); X = NULL; }}

#define MSDK_DEC_WAIT_INTERVAL 500

mfxU16 msdk_atomic_inc16(volatile mfxU16 *pVariable);