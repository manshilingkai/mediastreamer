#include "modules/video_coding/codecs/winhw/intel_qsv/intel_qsv_videoencoder.h"
#include <intrin.h>
#include <string>
#include "rtc_base/logging.h"
#include "modules/video_coding/codecs/winhw/win/gpu_info.h"

#ifdef ENABLE_LIBYUV
#include "third_party/libyuv/include/libyuv/convert_from.h"
#endif

#include "rtc_base/timeutils.h"

// refer : https://www.intel.com/content/www/us/en/developer/articles/technical/video-conferencing-features-of-intel-media-software-development-kit.html

namespace netease {

IntelQsvEncTask::IntelQsvEncTask()
    : EncSyncP(0), callback(NULL)
{
    mfxBS.AddExtBuffer<mfxExtAVCEncodedFrameInfo>();
}

mfxStatus IntelQsvEncTask::Init(mfxU32 nBufferSize, IntelQsvVideoEncoder *cb)
{
    Close();

    mfxStatus sts = Reset();
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "Reset failed";
        return sts;
    }
    
    mfxBS.Extend(nBufferSize);
    callback = cb;

    return sts;
}

mfxStatus IntelQsvEncTask::Close()
{
    EncSyncP = 0;

    return MFX_ERR_NONE;
}

mfxStatus IntelQsvEncTask::WriteBitstream()
{
    if (callback)
    {
        callback->handleBitstream(mfxBS);
    }
    
    return MFX_ERR_NONE;
}

mfxStatus IntelQsvEncTask::Reset()
{
    // mark sync point as free
    EncSyncP = NULL;

    // prepare bit stream
    mfxBS.DataOffset = 0;
    mfxBS.DataLength = 0;

    return MFX_ERR_NONE;
}


IntelQsvEncTaskPool::IntelQsvEncTaskPool()
{
    m_pTasks  = NULL;
    m_pmfxSession       = NULL;
    m_nTaskBufferStart  = 0;
    m_nPoolSize         = 0;
    m_bGpuHangRecovery = false;
}

IntelQsvEncTaskPool::~IntelQsvEncTaskPool()
{
    Close();
}

mfxStatus IntelQsvEncTaskPool::Init(MFXVideoSession* pmfxSession, mfxU32 nPoolSize, mfxU32 nBufferSize, IntelQsvVideoEncoder *callback)
{
    if (pmfxSession == NULL)
    {
        return MFX_ERR_NULL_PTR;
    }
    
    if (nPoolSize == 0)
    {
        return MFX_ERR_UNDEFINED_BEHAVIOR;
    }
    
    if (nBufferSize == 0)
    {
        return MFX_ERR_UNDEFINED_BEHAVIOR;
    }

    m_pmfxSession = pmfxSession;
    m_nPoolSize = nPoolSize;

    m_pTasks = new IntelQsvEncTask [m_nPoolSize];
    if (m_pTasks == NULL)
    {
        return MFX_ERR_MEMORY_ALLOC;
    }

    mfxStatus sts = MFX_ERR_NONE;

    for (mfxU32 i = 0; i < m_nPoolSize; i++)
    {
        sts = m_pTasks[i].Init(nBufferSize, callback);
        if (sts < MFX_ERR_NONE) {
            RTC_LOG(LS_ERROR) << "m_pTasks[i].Init failed";
            return sts;
        }
    }

    return MFX_ERR_NONE;
}

mfxStatus IntelQsvEncTaskPool::SynchronizeFirstTask()
{
    if (m_pTasks == NULL)
    {
        return MFX_ERR_NOT_INITIALIZED;
    }

    if (m_pmfxSession == NULL)
    {
        return MFX_ERR_NOT_INITIALIZED;
    }

    mfxStatus sts  = MFX_ERR_NONE;
    bool bGpuHang = false;

    // non-null sync point indicates that task is in execution
    if (NULL != m_pTasks[m_nTaskBufferStart].EncSyncP)
    {
        sts = m_pmfxSession->SyncOperation(m_pTasks[m_nTaskBufferStart].EncSyncP, MSDK_WAIT_INTERVAL);
        if (sts == MFX_ERR_GPU_HANG && m_bGpuHangRecovery)
        {
            bGpuHang = true;
            {
                for (mfxU32 i = 0; i < m_nPoolSize; i++)
                {
                    if (m_pTasks[i].EncSyncP != NULL)
                    {
                        sts = m_pmfxSession->SyncOperation(m_pTasks[i].EncSyncP, 0);//MSDK_WAIT_INTERVAL
                    }
                }
            }
            ClearTasks();
            sts = MFX_ERR_NONE;
            RTC_LOG(LS_WARNING) << "GPU hang happened";
        }
        if (sts < MFX_ERR_NONE)
        {
            RTC_LOG(LS_ERROR) << "SyncOperation failed";
        }
        
        if (MFX_ERR_NONE == sts)
        {
            sts = m_pTasks[m_nTaskBufferStart].WriteBitstream();
            if (sts < MFX_ERR_NONE) {
                RTC_LOG(LS_ERROR) << "m_pTasks[m_nTaskBufferStart].WriteBitstream failed";
                return sts;
            }

            sts = m_pTasks[m_nTaskBufferStart].Reset();
            if (sts < MFX_ERR_NONE) {
                RTC_LOG(LS_ERROR) << "m_pTasks[m_nTaskBufferStart].Reset failed";
                return sts;
            }
            
            // move task buffer start to the next executing task
            // the first transform frame to the right with non zero sync point
            for (mfxU32 i = 0; i < m_nPoolSize; i++)
            {
                m_nTaskBufferStart = (m_nTaskBufferStart + 1) % m_nPoolSize;
                if (NULL != m_pTasks[m_nTaskBufferStart].EncSyncP)
                {
                    break;
                }
            }
        }else if (sts > MFX_ERR_NONE)
        {
            RTC_LOG(LS_WARNING) << "SyncOperation Return : " << sts;
        }
        else if (MFX_ERR_ABORTED == sts)
        {

        }
    }
    else
    {
        sts = MFX_ERR_NOT_FOUND; // no tasks left in task buffer
    }
    return bGpuHang ? MFX_ERR_GPU_HANG : sts;
}

mfxU32 IntelQsvEncTaskPool::GetFreeTaskIndex()
{
    mfxU32 off = 0;

    if (m_pTasks)
    {
        for (off = 0; off < m_nPoolSize; off++)
        {
            if (NULL == m_pTasks[(m_nTaskBufferStart + off) % m_nPoolSize].EncSyncP)
            {
                break;
            }
        }
    }

    if (off >= m_nPoolSize)
        return m_nPoolSize;

    return (m_nTaskBufferStart + off) % m_nPoolSize;
}

mfxStatus IntelQsvEncTaskPool::GetFreeTask(IntelQsvEncTask **ppTask)
{
    if (ppTask == NULL)
    {
        return MFX_ERR_NULL_PTR;
    }

    if (m_pTasks == NULL)
    {
        return MFX_ERR_NOT_INITIALIZED;
    }

    mfxU32 index = GetFreeTaskIndex();

    if (index >= m_nPoolSize)
    {
        return MFX_ERR_NOT_FOUND;
    }

    // return the address of the task
    *ppTask = &m_pTasks[index];

    return MFX_ERR_NONE;
}

void IntelQsvEncTaskPool::Close()
{
    if (m_pTasks)
    {
        for (mfxU32 i = 0; i < m_nPoolSize; i++)
        {
            m_pTasks[i].Close();
        }
    }

    MSDK_SAFE_DELETE_ARRAY(m_pTasks);

    m_pmfxSession = NULL;
    m_nTaskBufferStart = 0;
    m_nPoolSize = 0;
}

void IntelQsvEncTaskPool::SetGpuHangRecoveryFlag()
{
    m_bGpuHangRecovery = true;
}

void IntelQsvEncTaskPool::ClearTasks()
{
    for (size_t i = 0; i < m_nPoolSize; i++)
    {
        m_pTasks[i].Reset();
    }
    m_nTaskBufferStart = 0;
}

IntelQsvVideoEncoder::IntelQsvVideoEncoder(mfxU32 codecId)
{
    m_CodecId = codecId;

    m_pmfxENC = NULL;
    m_pMFXAllocator = NULL;
    m_pmfxAllocatorParams = NULL;
    m_pEncSurfaces = NULL;

    m_nEncSurfIdx = 0;
    m_nFramesRead = 0;

    m_bInsertIDR = false;
    m_bResetBitrate = false;
	m_bResetFramerate = false;

    nEncodedDataBufferSize = 0;

    m_bEncoderInited = false;

    m_bResetTemporalSVC = false;
    m_TemporalSVCCap.base_vs_enhance_bitrate_fraction_default = 0.7;

	m_HWGPUName = "Intel";
    std::vector<GpuInfo>& available_gpuInfos = GPUInfoHandler::GetInstance().GetAvailableGpuInfos();
	for (GpuInfo gpuInfo : available_gpuInfos) {
        if (gpuInfo.provider == GPU_INTEL) {
            m_HWGPUName = gpuInfo.name;
        }
    }

    if (m_CodecId == MFX_CODEC_HEVC)
    {
        m_CodecFriendlyName = "H265 Encoder [" + m_HWGPUName + "]";
    }else {
        m_CodecFriendlyName = "H264 Encoder [" + m_HWGPUName + "]";
    }

    m_pBitrateAdjuster = NULL;
    m_encoderBitrateBps = 0;
}

IntelQsvVideoEncoder::~IntelQsvVideoEncoder()
{
    this->endEncode();

    if (m_pBitrateAdjuster != NULL) {
        delete m_pBitrateAdjuster;
        m_pBitrateAdjuster = NULL;
    }
}

bool IntelQsvVideoEncoder::init()
{
    return true;
}

mfxStatus IntelQsvVideoEncoder::CreateAllocator()
{
    // create system memory allocator
    m_pMFXAllocator = new SysMemFrameAllocator;
    if (m_pMFXAllocator == NULL) {
        RTC_LOG(LS_ERROR) << "MFX_ERR_MEMORY_ALLOC";
        return MFX_ERR_MEMORY_ALLOC;
    }

    /* In case of system memory we demonstrate "no external allocator" usage model.
    We don't call SetAllocator, Media SDK uses internal allocator.
    We use system memory allocator simply as a memory manager for application*/
    
    // initialize memory allocator
    mfxStatus sts = m_pMFXAllocator->Init(m_pmfxAllocatorParams);
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "m_pMFXAllocator->Init failed";
    }

    return sts;
}

void IntelQsvVideoEncoder::DeleteAllocator()
{
    // delete allocator
    MSDK_SAFE_DELETE(m_pMFXAllocator);
    MSDK_SAFE_DELETE(m_pmfxAllocatorParams);
}

mfxStatus IntelQsvVideoEncoder::InitMfxEncParams()
{
    memset(&m_mfxEncParams, 0, sizeof(mfxVideoParam));

    m_mfxEncParams.mfx.CodecId                 = m_CodecId;
    m_mfxEncParams.mfx.TargetUsage             = MFX_TARGETUSAGE_BEST_SPEED;//MFX_TARGETUSAGE_BEST_SPEED; // trade-off between quality and speed
    m_mfxEncParams.mfx.RateControlMethod       = MFX_RATECONTROL_CBR;
    m_mfxEncParams.mfx.GopRefDist              = m_config.numBFrame + 1;
    m_mfxEncParams.mfx.GopPicSize              = m_config.gop;
    // m_mfxEncParams.mfx.NumRefFrame             = pInParams->nNumRefFrame;

    // set every i-frame to be IDR frame
    if (m_CodecId == MFX_CODEC_AVC) {
        m_mfxEncParams.mfx.IdrInterval = 0;
    }
	else if(m_CodecId == MFX_CODEC_HEVC) {
		m_mfxEncParams.mfx.IdrInterval = 1;
	}

    if (m_CodecId == MFX_CODEC_AVC)
    {
        m_mfxEncParams.mfx.CodecProfile            = MFX_PROFILE_AVC_HIGH;
    }else if(m_CodecId == MFX_CODEC_HEVC)
    {
        m_mfxEncParams.mfx.CodecProfile            = MFX_PROFILE_HEVC_MAIN;
    }else {
        m_mfxEncParams.mfx.CodecProfile            = MFX_PROFILE_UNKNOWN;
    }

    // m_mfxEncParams.mfx.CodecLevel              = pInParams->CodecLevel;
    // m_mfxEncParams.mfx.MaxKbps                 = pInParams->MaxKbps;
    // m_mfxEncParams.mfx.InitialDelayInKB        = pInParams->InitialDelayInKB;
    m_mfxEncParams.mfx.GopOptFlag              = MFX_GOP_STRICT;
    // m_mfxEncParams.mfx.BufferSizeInKB          = pInParams->BufferSizeInKB;
    m_mfxEncParams.mfx.TargetKbps = m_encoderBitrateBps / 1000; // in Kbps
    if (m_CodecId == MFX_CODEC_AVC){
        m_mfxEncParams.mfx.NumSlice = 1;
    }

	// calculate frameRate
	m_mfxEncParams.mfx.FrameInfo.FrameRateExtN = m_config.fps;
	m_mfxEncParams.mfx.FrameInfo.FrameRateExtD = 1;

    m_mfxEncParams.mfx.EncodedOrder = 0; // binary flag, 0 signals encoder to take frames in display order
    
    // specify memory type
    m_mfxEncParams.IOPattern = MFX_IOPATTERN_IN_SYSTEM_MEMORY;

    // frame info parameters
    m_mfxEncParams.mfx.FrameInfo.FourCC       = MFX_FOURCC_NV12;
    m_mfxEncParams.mfx.FrameInfo.ChromaFormat = FourCCToChroma(MFX_FOURCC_NV12);
    m_mfxEncParams.mfx.FrameInfo.PicStruct    = MFX_PICSTRUCT_PROGRESSIVE;
    m_mfxEncParams.mfx.FrameInfo.Shift        = 0;

    // width must be a multiple of 16
    // height must be a multiple of 16 in case of frame picture and a multiple of 32 in case of field picture
    m_mfxEncParams.mfx.FrameInfo.Width  = MSDK_ALIGN16(m_config.width);
    m_mfxEncParams.mfx.FrameInfo.Height = (MFX_PICSTRUCT_PROGRESSIVE == m_mfxEncParams.mfx.FrameInfo.PicStruct)?
        MSDK_ALIGN16(m_config.height) : MSDK_ALIGN32(m_config.height);

    m_mfxEncParams.mfx.FrameInfo.CropX = 0;
    m_mfxEncParams.mfx.FrameInfo.CropY = 0;
    m_mfxEncParams.mfx.FrameInfo.CropW = m_config.width;
    m_mfxEncParams.mfx.FrameInfo.CropH = m_config.height;

    if (m_CodecId == MFX_CODEC_AVC) {
        auto codingOption = m_mfxEncParams.AddExtBuffer<mfxExtCodingOption>();
        codingOption->PicTimingSEI        = MFX_CODINGOPTION_OFF;
        codingOption->NalHrdConformance   = MFX_CODINGOPTION_OFF;
        codingOption->VuiNalHrdParameters = MFX_CODINGOPTION_OFF;
        codingOption->AUDelimiter = MFX_CODINGOPTION_OFF;

        auto codingOption2 = m_mfxEncParams.AddExtBuffer<mfxExtCodingOption2>();
        codingOption2->RepeatPPS = MFX_CODINGOPTION_OFF;
    }

    // In case of HEVC when height and/or width divided with 8 but not divided with 16
    // add extended parameter to increase performance
    if ( ( !((m_mfxEncParams.mfx.FrameInfo.CropW & 15 ) ^ 8 ) ||
        !((m_mfxEncParams.mfx.FrameInfo.CropH & 15 ) ^ 8 ) ) &&
        (m_mfxEncParams.mfx.CodecId == MFX_CODEC_HEVC))
    {
        auto encHEVCParam = m_mfxEncParams.AddExtBuffer<mfxExtHEVCParam>();
        encHEVCParam->PicWidthInLumaSamples = m_mfxEncParams.mfx.FrameInfo.CropW;
        encHEVCParam->PicHeightInLumaSamples = m_mfxEncParams.mfx.FrameInfo.CropH;
    }

    m_mfxEncParams.AsyncDepth = 1;

    if (m_config.isEnableTemporalSVC) {
        m_mfxEncParams.mfx.EncodedOrder = 0;
        if (m_CodecId == MFX_CODEC_AVC) {
            auto avcTemporalLayers = m_mfxEncParams.AddExtBuffer<mfxExtAvcTemporalLayers>();
            avcTemporalLayers->BaseLayerPID = 0;
            for (size_t i = 0; i < 8; i++) {
                avcTemporalLayers->Layer[i].Scale = 0;
            }
            for (int i = 0; i < m_config.numTemporalLayers; i++) {
                avcTemporalLayers->Layer[i].Scale = i+1;
            }
        }else if(m_CodecId == MFX_CODEC_HEVC) {
            auto hevcTemporalLayers = m_mfxEncParams.AddExtBuffer<mfxExtHEVCTemporalLayers>();
            hevcTemporalLayers->BaseLayerPID = 0;
            for (size_t i = 0; i < 8; i++) {
                hevcTemporalLayers->Layer[i].Scale = 0;
            }
            for (int i = 0; i < m_config.numTemporalLayers; i++) {
                hevcTemporalLayers->Layer[i].Scale = i+1;
            }
        }

        m_TemporalSVCCap.is_support_temporal_svc = true;
        m_TemporalSVCCap.supported_max_temporal_layers = 2;
        m_TemporalSVCCap.is_support_dynamic_enable_temporal_svc = true;
        m_TemporalSVCCap.is_support_dynamic_set_bitrate_fraction = false;
    }else {
        m_TemporalSVCCap.is_support_temporal_svc = false;
    }

    return MFX_ERR_NONE;
}

mfxStatus IntelQsvVideoEncoder::ResetMFXComponents()
{
    if (m_pmfxENC == NULL)
    {
        RTC_LOG(LS_ERROR) << "m_pmfxENC is NULL";
        return MFX_ERR_NOT_INITIALIZED;
    }
    
    mfxStatus sts = MFX_ERR_NONE;
    sts = m_pmfxENC->Close();
    if (sts == MFX_ERR_NOT_INITIALIZED) {sts = MFX_ERR_NONE;}
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_pmfxENC->Close failed, Error Code : " << sts;
        return sts;
    }

    // free allocated frames
    DeleteFrames();

    m_TaskPool.Close();

    sts = AllocFrames();
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "AllocFrames failed";
        return sts;
    }

    m_mfxEncParams.mfx.FrameInfo.FourCC       = MFX_FOURCC_NV12;
    m_mfxEncParams.mfx.FrameInfo.ChromaFormat = FourCCToChroma(MFX_FOURCC_NV12);

    sts = m_pmfxENC->Init(&m_mfxEncParams);
    if (MFX_WRN_PARTIAL_ACCELERATION == sts)
    {
        RTC_LOG(LS_WARNING) << "WARNING: partial acceleration";
        if (sts == MFX_WRN_PARTIAL_ACCELERATION) {sts = MFX_ERR_NONE;}
    }
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_pmfxENC->Init failed, Error Code : " << sts;
        return sts;
    }

    sts = m_pmfxENC->GetVideoParam(&m_mfxEncParams);
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_pmfxENC->GetVideoParam failed, Error Code : " << sts;
        return sts;
    }

    nEncodedDataBufferSize = m_mfxEncParams.mfx.FrameInfo.Width * m_mfxEncParams.mfx.FrameInfo.Height * 3 / 2;
    sts = m_TaskPool.Init(&m_mfxSession, m_mfxEncParams.AsyncDepth, nEncodedDataBufferSize, this);
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_TaskPool.Init failed, Error Code : " << sts;
        return sts;
    }

    return MFX_ERR_NONE;
}

mfxStatus IntelQsvVideoEncoder::AllocFrames()
{
    if (m_pmfxENC == NULL)
    {
        return MFX_ERR_NOT_INITIALIZED;
    }
    
    mfxStatus sts = MFX_ERR_NONE;
    mfxFrameAllocRequest EncRequest;

    mfxU16 nEncSurfNum = 0; // number of surfaces for encoder

    MSDK_ZERO_MEMORY(EncRequest);

    mfxU16 initialTargetKbps = m_mfxEncParams.mfx.TargetKbps;
    auto co2 = m_mfxEncParams.GetExtBuffer<mfxExtCodingOption2>();

    // Querying encoder
    sts = m_pmfxENC->Query(&m_mfxEncParams, &m_mfxEncParams);
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "Query (for encoder) failed, Error Code : " << sts;
        return sts;
    }
    
    if (co2 && co2->BitrateLimit != MFX_CODINGOPTION_OFF && initialTargetKbps != m_mfxEncParams.mfx.TargetKbps)
    {
        RTC_LOG(LS_WARNING) << "WARNING: -BitrateLimit:on, target bitrate was changed from" << initialTargetKbps << " kbps to " << m_mfxEncParams.mfx.TargetKbps << " kbps.";
    }

    // Calculate the number of surfaces for components.
    // QueryIOSurf functions tell how many surfaces are required to produce at least 1 output.
    // To achieve better performance we provide extra surfaces.
    // 1 extra surface at input allows to get 1 extra output.
    sts = m_pmfxENC->QueryIOSurf(&m_mfxEncParams, &EncRequest);
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "QueryIOSurf (for encoder) failed, Error Code : " << sts;
        return sts;
    }

    EncRequest.NumFrameMin = EncRequest.NumFrameSuggested;

    if (EncRequest.NumFrameSuggested < m_mfxEncParams.AsyncDepth)
        return MFX_ERR_MEMORY_ALLOC;

    // The number of surfaces shared by vpp output and encode input.
    nEncSurfNum = EncRequest.NumFrameSuggested;

    // prepare allocation requests
    EncRequest.NumFrameSuggested = EncRequest.NumFrameMin = nEncSurfNum;
    MSDK_MEMCPY_VAR(EncRequest.Info, &(m_mfxEncParams.mfx.FrameInfo), sizeof(mfxFrameInfo));

    // alloc frames for encoder
    sts = m_pMFXAllocator->Alloc(m_pMFXAllocator->pthis, &EncRequest, &m_EncResponse);
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_pMFXAllocator->Alloc failed, Error Code : " << sts;
        return sts;
    }

    // prepare mfxFrameSurface1 array for encoder
    m_pEncSurfaces = new mfxFrameSurface1 [m_EncResponse.NumFrameActual];
    if (m_pEncSurfaces == NULL)
    {
        return MFX_ERR_MEMORY_ALLOC;
    }

    for (int i = 0; i < m_EncResponse.NumFrameActual; i++)
    {
        memset(&(m_pEncSurfaces[i]), 0, sizeof(mfxFrameSurface1));
        MSDK_MEMCPY_VAR(m_pEncSurfaces[i].Info, &(m_mfxEncParams.mfx.FrameInfo), sizeof(mfxFrameInfo));

        // get YUV pointers
        sts = m_pMFXAllocator->Lock(m_pMFXAllocator->pthis, m_EncResponse.mids[i], &(m_pEncSurfaces[i].Data));
        if (sts < MFX_ERR_NONE)
        {
            RTC_LOG(LS_ERROR) << "m_pMFXAllocator->Lock failed, Error Code : " << sts;
            return sts;
        }
    }

    return MFX_ERR_NONE;
}


void IntelQsvVideoEncoder::DeleteFrames()
{
    // delete surfaces array
    MSDK_SAFE_DELETE_ARRAY(m_pEncSurfaces);

    // delete frames
    if (m_pMFXAllocator)
    {
        m_pMFXAllocator->Free(m_pMFXAllocator->pthis, &m_EncResponse);
    }
}

bool IntelQsvVideoEncoder::setConfig(const VideoEncoderConfig &config)
{
	m_config = config;

    if (m_pBitrateAdjuster != NULL) {
        delete m_pBitrateAdjuster;
        m_pBitrateAdjuster = NULL;
    }

    HWQPThreshold hwQPThreshold = getHWQPThreshold();
    m_pBitrateAdjuster = new webrtc::BitrateAdjuster(config.minAdjustedBitratePct, config.maxAdjustedBitratePct, config.enableDynamicAdjustMaxBitratePct, 
                                                     hwQPThreshold.lowQpThreshold, hwQPThreshold.highQpThreshold, config.enableBitrateAdjuster);
    m_pBitrateAdjuster->SetTargetBitrateBps(m_config.avgBitrate);
    m_encoderBitrateBps = m_pBitrateAdjuster->GetAdjustedBitrateBps();

	return true;
}

bool IntelQsvVideoEncoder::beginEncode()
{
    // RTC_LOG(LS_INFO) << "in func: " << __FUNCTION__;

    if (m_bEncoderInited) return true;

    mfxInitParamlWrap initPar;

    // we set version to 1.0 and later we will query actual version of the library which will got leaded
    initPar.Version.Major = 1;
    initPar.Version.Minor = 0;

    initPar.GPUCopy = false;

    IntelQsvEncoderParam param;
    param.nDstWidth = m_config.width;
    param.nDstHeight = m_config.height;
    param.nFps = m_config.fps;
    param.nPicStruct = MFX_PICSTRUCT_PROGRESSIVE;
    param.EncodeFourCC =  MFX_FOURCC_NV12;
    param.nTargetUsage = MFX_TARGETUSAGE_BEST_SPEED;//MFX_TARGETUSAGE_BEST_SPEED;
    param.CodecId = m_CodecId;
    param.CodecLevel = MFX_LEVEL_UNKNOWN;
    param.CodecProfile = MFX_PROFILE_UNKNOWN;
    param.pluginParam.type = MFX_PLUGINLOAD_TYPE_GUID;
    param.pluginParam.pluginGuid = MFX_PLUGINID_HEVCE_HW;
    param.pluginParam.strPluginPath[0] = '\0';

    mfxStatus sts = GetImpl(param, initPar.Implementation);
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "GetImpl failed";
        return false;
    }

    sts = m_mfxSession.InitEx(initPar);
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "m_mfxSession.InitEx failed";
        return false;
    }

    mfxVersion version;
    sts = MFXQueryVersion(m_mfxSession, &version); // get real API version of the loaded library
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "MFXQueryVersion failed";
        m_mfxSession.Close();
        return false;
    }

    if (CheckVersion(&version, MSDK_FEATURE_PLUGIN_API) && m_CodecId == MFX_CODEC_HEVC) {
        /* Here we actually define the following codec initialization scheme:
        *  1. If plugin path or guid is specified: we load user-defined plugin (example: HEVC encoder plugin)
        *  2. If plugin path not specified:
        *    2.a) we check if codec is distributed as a mediasdk plugin and load it if yes
        *    2.b) if codec is not in the list of mediasdk plugins, we assume, that it is supported inside mediasdk library
        */
        if (param.pluginParam.type == MFX_PLUGINLOAD_TYPE_FILE && msdk_strnlen(param.pluginParam.strPluginPath, sizeof(param.pluginParam.strPluginPath)))
        {
            m_pUserModule.reset(new MFXVideoUSER(m_mfxSession));
            m_pPlugin.reset(LoadPlugin(MFX_PLUGINTYPE_VIDEO_ENCODE, m_mfxSession, param.pluginParam.pluginGuid, 1, param.pluginParam.strPluginPath, (mfxU32)msdk_strnlen(param.pluginParam.strPluginPath, sizeof(param.pluginParam.strPluginPath))));
            if (m_pPlugin.get() == NULL) sts = MFX_ERR_UNSUPPORTED;
        }
        else {
            bool isDefaultPlugin = false;
            if (AreGuidsEqual(param.pluginParam.pluginGuid, MSDK_PLUGINGUID_NULL))
            {
                mfxIMPL impl = MFX_IMPL_HARDWARE;
                param.pluginParam.pluginGuid = msdkGetPluginUID(impl, MSDK_VENCODE, param.CodecId);
                isDefaultPlugin = true;
            }
            if (!AreGuidsEqual(param.pluginParam.pluginGuid, MSDK_PLUGINGUID_NULL))
            {
                m_pPlugin.reset(LoadPlugin(MFX_PLUGINTYPE_VIDEO_ENCODE, m_mfxSession, param.pluginParam.pluginGuid, 1));
                if (m_pPlugin.get() == NULL) sts = MFX_ERR_UNSUPPORTED;
            }
            if (sts == MFX_ERR_UNSUPPORTED)
            {
                RTC_LOG(LS_ERROR) << MSDK_STRING("%s"), isDefaultPlugin ?
                    MSDK_STRING("Default plugin cannot be loaded (possibly you have to define plugin explicitly)\n")
                    : MSDK_STRING("Explicitly specified plugin cannot be loaded.\n");
            }
        }

        if (sts < MFX_ERR_NONE)
        {
            RTC_LOG(LS_ERROR) << "LoadPlugin failed";
            m_mfxSession.Close();
            return false;
        }
    }

    m_pmfxENC = new MFXVideoENCODE(m_mfxSession);
    if (m_pmfxENC == NULL)
    {
        RTC_LOG(LS_ERROR) << "MFX_ERR_MEMORY_ALLOC";
        m_pPlugin.reset();
        m_mfxSession.Close();
        return false;
    }
    
    m_fpsLimiter.Reset(param.nFps);

    // create and init frame allocator
    sts = CreateAllocator();
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "CreateAllocator failed";
        MSDK_SAFE_DELETE(m_pmfxENC);
        m_pPlugin.reset();
        m_mfxSession.Close();
        return false;
    }

    InitMfxEncParams();

    sts = ResetMFXComponents();
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "ResetMFXComponents failed";
        DeleteAllocator();
        MSDK_SAFE_DELETE(m_pmfxENC);
        m_pPlugin.reset();
        m_mfxSession.Close();
        return false;
    }

    m_bEncoderInited = true;


    // RTC_LOG(LS_INFO) << "out func: " << __FUNCTION__;
    return true;
}


#if (defined(_WIN64) || defined(_WIN32)) && (MFX_VERSION >= 1031)
mfxU32 IntelQsvVideoEncoder::GetPreferredAdapterNum(const mfxAdaptersInfo & adapters)
{
    if (adapters.NumActual == 0 || !adapters.Adapters)
        return 0;

    bool bPrefferiGfx = true;
    bool bPrefferdGfx = false;

    if (bPrefferiGfx)
    {
        // Find iGfx adapter in list and return it's index

        auto idx = std::find_if(adapters.Adapters, adapters.Adapters + adapters.NumActual,
            [](const mfxAdapterInfo info)
        {
            return info.Platform.MediaAdapterType == mfxMediaAdapterType::MFX_MEDIA_INTEGRATED;
        });

        // No iGfx in list
        if (idx == adapters.Adapters + adapters.NumActual)
        {
            RTC_LOG(LS_WARNING) << "Warning: No iGfx detected on machine. Will pick another adapter\n";
            bPrefferdGfx = true;
        }else {
            return static_cast<mfxU32>(std::distance(adapters.Adapters, idx));
        }
    }

    if (bPrefferdGfx)
    {
        // Find dGfx adapter in list and return it's index

        auto idx = std::find_if(adapters.Adapters, adapters.Adapters + adapters.NumActual,
            [](const mfxAdapterInfo info)
        {
            return info.Platform.MediaAdapterType == mfxMediaAdapterType::MFX_MEDIA_DISCRETE;
        });

        // No dGfx in list
        if (idx == adapters.Adapters + adapters.NumActual)
        {
            RTC_LOG(LS_WARNING) << "Warning: No dGfx detected on machine. Will pick another adapter\n";
            return 0;
        }

        return static_cast<mfxU32>(std::distance(adapters.Adapters, idx));
    }



    // Other ways return 0, i.e. best suitable detected by dispatcher
    return 0;
}
#endif // (defined(_WIN64) || defined(_WIN32)) && (MFX_VERSION >= 1031)


mfxStatus IntelQsvVideoEncoder::GetImpl(IntelQsvEncoderParam param, mfxIMPL& impl)
{
#if (defined(_WIN64) || defined(_WIN32)) && (MFX_VERSION >= 1031)
    mfxU32 num_adapters_available;
    mfxStatus sts = MFXQueryAdaptersNumber(&num_adapters_available);
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "MFXQueryAdaptersNumber failed";
        return sts;
    }
    mfxComponentInfo interface_request = { mfxComponentType::MFX_COMPONENT_ENCODE };
    mfxU16 Shift = false;
    mfxU16 Height   = (MFX_PICSTRUCT_PROGRESSIVE == param.nPicStruct) ? MSDK_ALIGN16(param.nDstHeight) : MSDK_ALIGN32(param.nDstHeight);
    mfxU16 LowPower = MFX_CODINGOPTION_UNKNOWN;
    mfxU16 BitDepth = FourCcBitDepth(param.EncodeFourCC);

    interface_request.Requirements                              = {};
    interface_request.Requirements.mfx.LowPower                 = LowPower;
    interface_request.Requirements.mfx.TargetUsage              = param.nTargetUsage;
    interface_request.Requirements.mfx.CodecId                  = param.CodecId;
    interface_request.Requirements.mfx.CodecProfile             = param.CodecProfile;
    interface_request.Requirements.mfx.CodecLevel               = param.CodecLevel;
    interface_request.Requirements.mfx.FrameInfo.BitDepthLuma   = BitDepth;
    interface_request.Requirements.mfx.FrameInfo.BitDepthChroma = BitDepth;
    interface_request.Requirements.mfx.FrameInfo.Shift          = Shift;
    interface_request.Requirements.mfx.FrameInfo.FourCC         = param.EncodeFourCC;
    interface_request.Requirements.mfx.FrameInfo.Width          = MSDK_ALIGN16(param.nDstWidth);
    interface_request.Requirements.mfx.FrameInfo.Height         = Height;
    interface_request.Requirements.mfx.FrameInfo.PicStruct      = param.nPicStruct;
    interface_request.Requirements.mfx.FrameInfo.ChromaFormat   = FourCCToChroma(param.EncodeFourCC);
    interface_request.Requirements.IOPattern = MFX_IOPATTERN_IN_SYSTEM_MEMORY;

    std::vector<mfxAdapterInfo> displays_data(num_adapters_available);
    mfxAdaptersInfo adapters = { displays_data.data(), mfxU32(displays_data.size()), 0u };
    sts = MFXQueryAdapters(&interface_request, &adapters);
    if (sts == MFX_ERR_NOT_FOUND)
    {
        RTC_LOG(LS_ERROR) << "ERROR: No suitable adapters found for this workload\n";
    }
    if (sts < MFX_ERR_NONE) {
        RTC_LOG(LS_ERROR) << "MFXQueryAdapters failed";
        return sts;
    }
    mfxU32 idx = GetPreferredAdapterNum(adapters);
    switch (adapters.Adapters[idx].Number)
    {
    case 0:
        impl = MFX_IMPL_HARDWARE;
        break;
    case 1:
        impl = MFX_IMPL_HARDWARE2;
        break;
    case 2:
        impl = MFX_IMPL_HARDWARE3;
        break;
    case 3:
        impl = MFX_IMPL_HARDWARE4;
        break;

    default:
        // try searching on all display adapters
        impl = MFX_IMPL_HARDWARE_ANY;
        break;
    }
#else
    // Library should pick first available compatible adapter during InitEx call with MFX_IMPL_HARDWARE_ANY
    impl = MFX_IMPL_HARDWARE_ANY;
#endif // (defined(_WIN64) || defined(_WIN32)) && (MFX_VERSION >= 1031)

    return MFX_ERR_NONE;
}

mfxStatus IntelQsvVideoEncoder::GetFreeTask(IntelQsvEncTask **ppTask)
{
    mfxStatus sts = MFX_ERR_NONE;

    sts = m_TaskPool.GetFreeTask(ppTask);
    if (MFX_ERR_NOT_FOUND == sts)
    {
        sts = m_TaskPool.SynchronizeFirstTask();
        if (MFX_ERR_NONE == sts)
        {
            // m_fpsLimiter.Work();
        }

        if (sts < MFX_ERR_NONE) {
            RTC_LOG(LS_ERROR) << "m_TaskPool.SynchronizeFirstTask failed";
            return sts;
        }

        // try again
        sts = m_TaskPool.GetFreeTask(ppTask);
    }

    return sts;
}

mfxStatus IntelQsvVideoEncoder::LoadNextFrame(const VideoRawFrame &frame, mfxFrameSurface1* pSurf)
{
	/*Fill raw Frame*/
    pSurf->Info.FrameId.ViewId = 0;

	mfxU8 *srcPtr, *dstPtr;
	mfxU16 pitch = pSurf->Data.Pitch;
	mfxU16 w = frame.width;
	mfxU16 h = frame.height;
	mfxU32 i = 0;
	if (frame.format == FOURCC_NV12) {
		// Y
		srcPtr = frame.planeptrs[0].ptr;
		dstPtr = pSurf->Data.Y; //pSurf->Data.Y + pSurf->Info.CropX + pSurf->Info.CropY * pSurf->Data.Pitch;
		for (i = 0; i < h; i++) {
			memcpy(dstPtr, srcPtr, w);
			srcPtr += frame.strides[0];
			dstPtr += pitch;
		}
		// UV
		srcPtr = frame.planeptrs[1].ptr;
		dstPtr = pSurf->Data.UV;
		h /= 2;
		for (i = 0; i < h; i++) {
			memcpy(dstPtr, srcPtr, w);
			srcPtr += frame.strides[1];
			dstPtr += pitch;
		}
	}
	else if (frame.format == FOURCC_I420) {
        /*I420 to NV12*/
#ifdef ENABLE_LIBYUV
        // int64_t encode_start_time_ms = rtc::TimeMillis();

        const uint8_t* src_y = frame.planeptrs[0].ptr;
        int src_stride_y = frame.strides[0];
        const uint8_t* src_u = frame.planeptrs[1].ptr;
        int src_stride_u = frame.strides[1];
        const uint8_t* src_v = frame.planeptrs[2].ptr;
        int src_stride_v = frame.strides[2];

        uint8_t* dst_y = (uint8_t *)(pSurf->Data.Y);
        int dst_stride_y = pSurf->Data.Pitch;
        uint8_t* dst_uv = (uint8_t *)(pSurf->Data.UV);
        int dst_stride_uv = pSurf->Data.Pitch;
        int width = frame.width;
        int height = frame.height;

        int libyuv_ret = libyuv::I420ToNV12(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, dst_y, dst_stride_y, dst_uv, dst_stride_uv, width, height);
        if (libyuv_ret !=0)
        {
		    RTC_LOG(LS_ERROR) << "libyuv::I420ToNV12 Fail";
		    return MFX_ERR_UNSUPPORTED;
        }

        // int64_t encode_stop_time_ms = rtc::TimeMillis();
        // RTC_LOG(LS_WARNING) << "winhw encode[libyuv::I420ToNV12] cost time : " << encode_stop_time_ms - encode_start_time_ms;
#else
		// load Y
		srcPtr = frame.planeptrs[0].ptr;
		dstPtr = pSurf->Data.Y; //pSurf->Data.Y + pSurf->Info.CropX + pSurf->Info.CropY * pSurf->Data.Pitch;
		for (i = 0; i < h; i++) {
			memcpy(dstPtr, srcPtr, w);
			srcPtr += frame.strides[0];
			dstPtr += pitch;
		}
		mfxU8 buf[2048]; // maximum supported chroma width for nv12
		mfxU32 j = 0;
		w /= 2;
		h /= 2;
		if (w > 2048) {
			RTC_LOG(LS_ERROR) << "[IntelQsvVideoEncoder] width=" << w << " is too big.";
			return false;
		}
		
		// load U
		srcPtr = frame.planeptrs[1].ptr;
		dstPtr = pSurf->Data.UV;
		for (i = 0; i < h; i++)
		{
			memcpy(buf, srcPtr, w);
			srcPtr += frame.strides[1];
			for (j = 0; j < w; j++)
			{
				dstPtr[i * pitch + j * 2] = buf[j];
			}
		}
		// load V
		srcPtr = frame.planeptrs[2].ptr;
		for (i = 0; i < h; i++)
		{
			memcpy(buf, srcPtr, w);
			srcPtr += frame.strides[2];
			for (j = 0; j < w; j++)
			{
				dstPtr[i * pitch + j * 2 + 1] = buf[j];
			}
		}
#endif
	}
	else{
		RTC_LOG(LS_ERROR) << "only support I420 Or NV12.";
		return MFX_ERR_UNSUPPORTED;
	}

    // frameorder required for reflist, dbp, and decrefpicmarking operations
    // if (pSurf)
    //     pSurf->Data.FrameOrder = m_nFramesRead;

    m_nFramesRead++;

    return MFX_ERR_NONE;
}

void IntelQsvVideoEncoder::InsertIDR(mfxEncodeCtrl & ctrl, bool forceIDR)
{
    ctrl.FrameType = forceIDR ? MFX_FRAMETYPE_I | MFX_FRAMETYPE_IDR | MFX_FRAMETYPE_REF : MFX_FRAMETYPE_UNKNOWN;
}

mfxStatus IntelQsvVideoEncoder::AllocateSufficientBuffer(mfxBitstreamWrapper& bs)
{
    if (m_pmfxENC == NULL)
    {
        return MFX_ERR_NOT_INITIALIZED;
    }
    
    mfxVideoParam par;
    MSDK_ZERO_MEMORY(par);

    // find out the required buffer size
    mfxStatus sts = m_pmfxENC->GetVideoParam(&par);
    if (sts < MFX_ERR_NONE) {
		RTC_LOG(LS_ERROR) << "GetFirstEncoder failed";

        return sts;
    }

    bs.Extend(par.mfx.BufferSizeInKB * 1000);

    return MFX_ERR_NONE;
}

mfxStatus IntelQsvVideoEncoder::EncodeOneFrame(const ExtendedSurface& In, IntelQsvEncTask*& pTask)
{
    mfxStatus sts = MFX_ERR_NONE;

    InsertIDR(pTask->encCtrl, m_bInsertIDR);

    for (;;)
    {
        // at this point surface for encoder contains either a frame from file or a frame processed by vpp/preenc
        sts = m_pmfxENC->EncodeFrameAsync(In.pCtrl, In.pSurface, &pTask->mfxBS, &pTask->EncSyncP);
        if (MFX_ERR_NONE < sts && !pTask->EncSyncP) // repeat the call if warning and no output
        {
            if (MFX_WRN_DEVICE_BUSY == sts)
                MSDK_SLEEP(1); // wait if device is busy
        }
        else if (MFX_ERR_NONE < sts && pTask->EncSyncP)
        {
            sts = MFX_ERR_NONE; // ignore warnings if output is available
            break;
        }
        else if (MFX_ERR_NOT_ENOUGH_BUFFER == sts)
        {
            sts = AllocateSufficientBuffer(pTask->mfxBS);
            if (sts < MFX_ERR_NONE)
            {
            	RTC_LOG(LS_ERROR) << "AllocateSufficientBuffer failed";
                return sts;
            }            
        }
        else
        {
            // get next surface and new task for 2nd bitstream in ViewOutput mode
            if (sts == MFX_ERR_MORE_BITSTREAM) {
                sts = MFX_ERR_NONE;
            }
            break;
        }
    }

    if (sts == MFX_ERR_NONE)
    {
        m_bInsertIDR = false;
    }

    return sts;
}

bool IntelQsvVideoEncoder::addRawFrame(const VideoRawFrame &frame)
{
    // RTC_LOG(LS_INFO) << "in func: " << __FUNCTION__;

    if (m_pmfxENC == NULL)
    {
        RTC_LOG(LS_ERROR) << "m_pmfxENC == NULL. Error Code: " << MFX_ERR_NOT_INITIALIZED;
        return false;
    }

    // reset bitrate or framerate
	if (m_bResetBitrate || m_bResetFramerate || m_bResetTemporalSVC)
	{

		if (m_bResetBitrate)
		{
		    m_mfxEncParams.mfx.TargetUsage = MFX_TARGETUSAGE_BEST_SPEED;//MFX_TARGETUSAGE_BEST_SPEED;
	        m_mfxEncParams.mfx.RateControlMethod = MFX_RATECONTROL_CBR;
            m_encoderBitrateBps = m_pBitrateAdjuster->GetAdjustedBitrateBps();
		    m_mfxEncParams.mfx.TargetKbps = m_encoderBitrateBps / 1000;
		}
		
		if (m_bResetFramerate)
		{
	        m_mfxEncParams.mfx.FrameInfo.FrameRateExtN = m_config.fps;
	        m_mfxEncParams.mfx.FrameInfo.FrameRateExtD = 1;
		}

        if (m_CodecId == MFX_CODEC_AVC) {
            auto codingOption = m_mfxEncParams.GetExtBuffer<mfxExtCodingOption>();
            codingOption->PicTimingSEI        = MFX_CODINGOPTION_OFF;
            codingOption->NalHrdConformance   = MFX_CODINGOPTION_OFF;
            codingOption->VuiNalHrdParameters = MFX_CODINGOPTION_OFF;
            codingOption->AUDelimiter = MFX_CODINGOPTION_OFF;

            auto codingOption2 = m_mfxEncParams.GetExtBuffer<mfxExtCodingOption2>();
            codingOption2->RepeatPPS = MFX_CODINGOPTION_OFF;
        }

		if (m_CodecId == MFX_CODEC_AVC){
            m_mfxEncParams.mfx.NumSlice = 1;
            m_mfxEncParams.mfx.CodecProfile = MFX_PROFILE_AVC_HIGH;
        }

        if (m_bResetTemporalSVC)
        {
            if (m_config.isEnableTemporalSVC) {
                m_mfxEncParams.mfx.EncodedOrder = 0;
                if (m_CodecId == MFX_CODEC_AVC) {
                    auto avcTemporalLayers = m_mfxEncParams.GetExtBuffer<mfxExtAvcTemporalLayers>();
                    avcTemporalLayers->BaseLayerPID = 0;
                    for (size_t i = 0; i < 8; i++) {
                        avcTemporalLayers->Layer[i].Scale = 0;
                    }
                    for (int i = 0; i < m_config.numTemporalLayers; i++) {
                        avcTemporalLayers->Layer[i].Scale = i+1;
                    }
                }else if(m_CodecId == MFX_CODEC_HEVC) {
                    auto hevcTemporalLayers = m_mfxEncParams.GetExtBuffer<mfxExtHEVCTemporalLayers>();
                    hevcTemporalLayers->BaseLayerPID = 0;
                    for (size_t i = 0; i < 8; i++) {
                        hevcTemporalLayers->Layer[i].Scale = 0;
                    }
                    for (int i = 0; i < m_config.numTemporalLayers; i++) {
                        hevcTemporalLayers->Layer[i].Scale = i+1;
                    }
                }

                // m_TemporalSVCCap.is_support_temporal_svc = true;
                // m_TemporalSVCCap.supported_max_temporal_layers = 2;
                // m_TemporalSVCCap.is_support_dynamic_enable_temporal_svc = true;
                // m_TemporalSVCCap.is_support_dynamic_set_bitrate_fraction = false;
            }else {
                m_mfxEncParams.mfx.EncodedOrder = 0;
                if (m_CodecId == MFX_CODEC_AVC) {
                    auto avcTemporalLayers = m_mfxEncParams.GetExtBuffer<mfxExtAvcTemporalLayers>();
                    avcTemporalLayers->BaseLayerPID = 0;
                    for (size_t i = 0; i < 8; i++) {
                        avcTemporalLayers->Layer[i].Scale = 0;
                    }
                }else if(m_CodecId == MFX_CODEC_HEVC) {
                    auto hevcTemporalLayers = m_mfxEncParams.GetExtBuffer<mfxExtHEVCTemporalLayers>();
                    hevcTemporalLayers->BaseLayerPID = 0;
                    for (size_t i = 0; i < 8; i++) {
                        hevcTemporalLayers->Layer[i].Scale = 0;
                    }
                }

                // m_TemporalSVCCap.is_support_temporal_svc = false;
            }
        }

/*
		if (m_bResetBitrate)
		{
            m_encoderBitrateBps = m_pBitrateAdjuster->GetAdjustedBitrateBps();
		}

        InitMfxEncParams();
*/
		mfxStatus reset_sts = m_pmfxENC->Reset(&m_mfxEncParams);
		if(reset_sts==MFX_ERR_NONE) {
			RTC_LOG(LS_INFO) << "reset mfxVideoParam success";
		}else if (reset_sts > MFX_ERR_NONE) {
			RTC_LOG(LS_WARNING) << "reset mfxVideoParam warning, warning code : " << (int)reset_sts;
		}else {
			RTC_LOG(LS_ERROR) << "reset mfxVideoParam fail, error code : " << (int)reset_sts;
		}

		m_bResetBitrate = false;
		m_bResetFramerate = false;
        m_bResetTemporalSVC = false;
	}
    
    mfxStatus sts = MFX_ERR_NONE;

    ExtendedSurface encSurface = {};

    IntelQsvEncTask *pCurrentTask = nullptr; // a pointer to the current task
    m_nEncSurfIdx = 0;

    while (MFX_ERR_NONE <= sts || MFX_ERR_MORE_DATA == sts)
    {
        // get a pointer to a free task (bit stream and sync point for encoder)
        sts = GetFreeTask(&pCurrentTask);
        if (MFX_ERR_NONE != sts) {
            if (MFX_ERR_NOT_FOUND == sts) {
                RTC_LOG(LS_WARNING) << "The previous asynchronous operation is in execution, No Free Task, ignore current raw frame and retry";
                sts = MFX_ERR_NONE;
            }
            break;
        }
        m_nEncSurfIdx = GetFreeSurface(m_pEncSurfaces, m_EncResponse.NumFrameActual);
        if (m_nEncSurfIdx == MSDK_INVALID_SURF_IDX)
        {
            return MFX_ERR_MEMORY_ALLOC;
        }
        mfxFrameSurface1 *pSurface = &m_pEncSurfaces[m_nEncSurfIdx];
        pSurface->Info.FrameId.ViewId = 0;
        pSurface->Data.TimeStamp = frame.timestamp;
        sts = LoadNextFrame(frame, pSurface);
        if (MFX_ERR_NONE != sts) break;
        encSurface.pCtrl = &pCurrentTask->encCtrl;
        encSurface.pSurface = pSurface;

        sts = EncodeOneFrame(encSurface, pCurrentTask);
        break;
    }

    // means that the input file has ended, need to go to buffering loops
    if (sts == MFX_ERR_MORE_DATA) {sts = MFX_ERR_NONE;}
    // exit in case of other errors
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_pmfxENC->EncodeFrameAsync failed. Error Code: " << sts;
        return false;
    }

    // RTC_LOG(LS_INFO) << "out func: " << __FUNCTION__;
    return true;
}

bool IntelQsvVideoEncoder::flushEncode()
{
    mfxStatus sts = MFX_ERR_NONE;
    IntelQsvEncTask *pCurrentTask = nullptr; // a pointer to the current task
    ExtendedSurface encSurface = {};

    // loop to get buffered frames from encoder
    while (MFX_ERR_NONE <= sts)
    {
        // get a free task (bit stream and sync point for encoder)
        sts = GetFreeTask(&pCurrentTask);
        if (MFX_ERR_NONE != sts) break;
        encSurface.pSurface = nullptr;
        sts = EncodeOneFrame(encSurface, pCurrentTask);
        if (MFX_ERR_NONE != sts) break;
    }

    // MFX_ERR_MORE_DATA is the correct status to exit buffering loop with
    // indicates that there are no more buffered frames
    if (sts == MFX_ERR_MORE_DATA) {sts = MFX_ERR_NONE;}
    // exit in case of other errors
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_pmfxENC->EncodeFrameAsync failed. Error Code: " << sts;
        return false;
    }

    // synchronize all tasks that are left in task pool
    while (MFX_ERR_NONE == sts)
    {
        sts = m_TaskPool.SynchronizeFirstTask();
        if (MFX_ERR_NONE == sts)
        {
            // m_fpsLimiter.Work();
        }
    }

    // MFX_ERR_NOT_FOUND is the correct status to exit the loop with
    // EncodeFrameAsync and SyncOperation don't return this status
    if (sts == MFX_ERR_NOT_FOUND) {sts = MFX_ERR_NONE;}
    // report any errors that occurred in asynchronous part
    if (sts < MFX_ERR_NONE)
    {
        RTC_LOG(LS_ERROR) << "m_TaskPool.SynchronizeFirstTask failed. Error Code: " << sts;
        return false;
    }

    return true;
}

bool IntelQsvVideoEncoder::endEncode()
{
    if (!m_bEncoderInited) return true;

    MSDK_SAFE_DELETE(m_pmfxENC);

    DeleteFrames();

    m_pPlugin.reset();

    m_TaskPool.Close();
    m_mfxSession.Close();

    // allocator if used as external for MediaSDK must be deleted after SDK components
    DeleteAllocator();

	releaseEncodedBuffer();

    m_bEncoderInited = false;
}

IntelQsvEncodedBuffer* IntelQsvVideoEncoder::getFreeEncodedBuffer(uint32_t bufsize)
{
    if (m_FreeEncodedBuffers.empty())
    {
	    IntelQsvEncodedBuffer *pOutBuffer = new IntelQsvEncodedBuffer;
	    if (!pOutBuffer) return NULL;
	    memset(pOutBuffer, 0, sizeof(IntelQsvEncodedBuffer));
        pOutBuffer->qp = 0;
	    pOutBuffer->bs.MaxLength = bufsize;
	    pOutBuffer->bs.DataOffset = 0;
	    pOutBuffer->bs.Data = new(std::nothrow) mfxU8[pOutBuffer->bs.MaxLength];
	    if (!pOutBuffer->bs.Data) {
		    delete pOutBuffer;
		    return NULL;
	    }
	    return pOutBuffer;
    }else {
        IntelQsvEncodedBuffer* front = m_FreeEncodedBuffers.front();
        m_FreeEncodedBuffers.pop_front();

        if (front->bs.MaxLength>=bufsize)
        {
            return front;
        }else {
		    if (front->bs.Data) {
			    delete[] front->bs.Data;
			    front->bs.Data = NULL;
	        }
	        front->bs.MaxLength = bufsize;
            front->bs.Data = new(std::nothrow) mfxU8[front->bs.MaxLength];
            if (!front->bs.Data) {
		        return NULL;
	        }else return front;
        }
    }
}

void IntelQsvVideoEncoder::putFilledEncodedBuffer(IntelQsvEncodedBuffer* filledEncodedBuffer)
{
    m_FilledEncodedBuffers.push_back(filledEncodedBuffer);
}

IntelQsvEncodedBuffer* IntelQsvVideoEncoder::getFilledEncodedBuffer()
{
    if (m_FilledEncodedBuffers.empty())
    {
        return NULL;
    }else {
        IntelQsvEncodedBuffer* front = m_FilledEncodedBuffers.front();
        m_FilledEncodedBuffers.pop_front();
        return front;
    }
}

void IntelQsvVideoEncoder::putFreeEncodedBuffer(IntelQsvEncodedBuffer* freeEncodedBuffer)
{
    m_FreeEncodedBuffers.push_back(freeEncodedBuffer);
}

void IntelQsvVideoEncoder::releaseEncodedBuffer()
{
    while (!m_FilledEncodedBuffers.empty())
    {
        IntelQsvEncodedBuffer* buf  = m_FilledEncodedBuffers.front();
        m_FilledEncodedBuffers.pop_front();

        if (buf->bs.Data) {
			delete[] buf->bs.Data;
			buf->bs.Data = NULL;
	    }

		delete buf;
    }
    
	m_FilledEncodedBuffers.clear();

    while (!m_FreeEncodedBuffers.empty())
    {
        IntelQsvEncodedBuffer* buf  = m_FreeEncodedBuffers.front();
        m_FreeEncodedBuffers.pop_front();

        if (buf->bs.Data) {
			delete[] buf->bs.Data;
			buf->bs.Data = NULL;
	    }

		delete buf;
    }

	m_FreeEncodedBuffers.clear();
}

void IntelQsvVideoEncoder::handleBitstream(mfxBitstreamWrapper &mfxBS)
{
    IntelQsvEncodedBuffer* pEncodedBuffer = getFreeEncodedBuffer(nEncodedDataBufferSize);
    if (pEncodedBuffer)
    {
        pEncodedBuffer->bs.DecodeTimeStamp = mfxBS.DecodeTimeStamp;
        pEncodedBuffer->bs.TimeStamp = mfxBS.TimeStamp;
        // pEncodedBuffer->bs.DataOffset = mfxbs->DataOffset;
        pEncodedBuffer->bs.DataLength = mfxBS.DataLength;
        // pEncodedBuffer->bs.MaxLength = mfxBS.MaxLength;
        pEncodedBuffer->bs.PicStruct = mfxBS.PicStruct;
        pEncodedBuffer->bs.FrameType = mfxBS.FrameType;
        pEncodedBuffer->bs.DataFlag = mfxBS.DataFlag;
        pEncodedBuffer->bs.DataOffset = 0;
        memcpy(pEncodedBuffer->bs.Data + pEncodedBuffer->bs.DataOffset, mfxBS.Data + mfxBS.DataOffset, mfxBS.DataLength);

        auto extAVCEncodedFrameInfo = mfxBS.GetExtBuffer<mfxExtAVCEncodedFrameInfo>();
        // RTC_LOG(LS_INFO) << "extAVCEncodedFrameInfo->QP : " << extAVCEncodedFrameInfo->QP;
        pEncodedBuffer->qp = extAVCEncodedFrameInfo->QP;

        putFilledEncodedBuffer(pEncodedBuffer);

        mfxBS.DataLength = 0;
    }
}

bool IntelQsvVideoEncoder::getEncodedFrame(VideoEncodedFrame &frame)
{
	IntelQsvEncodedBuffer* buf = getFilledEncodedBuffer();
	if(buf==NULL) {
		RTC_LOG(LS_WARNING) << "No Filled OutBuffer";
		return false;
	}else {
		ENCODED_FRAME_TYPE fType = ERR_Frame;
		mfxBitstream *bs = &buf->bs;

		if (bs->FrameType >= 256)
			fType = ERR_Frame;
		else {
			switch(bs->FrameType & 0x0f0f)
			{
				case MFX_FRAMETYPE_I:
				case MFX_FRAMETYPE_xI:
					switch(bs->FrameType & (MFX_FRAMETYPE_IDR|MFX_FRAMETYPE_xIDR))
					{
					case MFX_FRAMETYPE_IDR:
					case MFX_FRAMETYPE_xIDR:
						fType = IDR_Frame;
						break;
					default:
						fType = I_Frame;
						break;
					}
					break;
				case MFX_FRAMETYPE_P:
				case MFX_FRAMETYPE_xP:
					fType = P_Frame;
					break;
				case MFX_FRAMETYPE_B:
				case MFX_FRAMETYPE_xB:
					if (bs->FrameType & MFX_FRAMETYPE_REF)
					{
						fType = P_Frame;
					}
					else
					{
						fType = B_Frame;
					}
					break;
				case MFX_FRAMETYPE_S:
				case MFX_FRAMETYPE_xS:
					fType = B_Frame;
					break;
			}
		}

		frame.frameSize = bs->DataLength;
		frame.frameData.ptr = bs->Data + bs->DataOffset;

		frame.frameType = fType;
	    frame.standard = (m_CodecId == MFX_CODEC_HEVC) ? VideoStandard_ITU_H265 : VideoStandard_ITU_H264;

		frame.dts = (uint32_t)bs->DecodeTimeStamp;
		frame.pts = (uint32_t)bs->TimeStamp;

        frame.qp = (m_CodecId == MFX_CODEC_HEVC) ? (getHWQPThreshold().lowQpThreshold + getHWQPThreshold().highQpThreshold)/2 : buf->qp;

        frame.adjustedEncBitrateBps = m_pBitrateAdjuster->GetAdjustedBitrateBps();

        m_pBitrateAdjuster->ReportQP(frame.qp);
        m_pBitrateAdjuster->Update(frame.frameSize);
		
		//reuse
		bs->DataFlag = 0;
		bs->DataLength = 0;
		bs->DataOffset = 0;
		bs->FrameType = 0;
		bs->TimeStamp = 0;
		bs->DecodeTimeStamp = 0;
        bs->PicStruct = 0;

        buf->qp = 0;

        putFreeEncodedBuffer(buf);

		return true;
	}
}

bool IntelQsvVideoEncoder::resetBitrate(uint32_t bitrateBps)
{
    m_pBitrateAdjuster->SetTargetBitrateBps(bitrateBps);

    if (m_encoderBitrateBps != m_pBitrateAdjuster->GetAdjustedBitrateBps())
	{
		m_bResetBitrate = true;
	}

	return true;
}

bool IntelQsvVideoEncoder::resetFramerate(uint32_t fps)
{
    if (m_CodecId == MFX_CODEC_HEVC) {
    }else {
        if (m_config.fps != fps)
        {
            m_config.fps = fps;
            m_bResetFramerate = true;
        }
    }

	return true;
}

void IntelQsvVideoEncoder::forceIntraFrame()
{
    m_bInsertIDR = true;
}

void IntelQsvVideoEncoder::resetTemporalSVC(bool isEnableTemporalSVC, uint32_t numTemporalLayers)
{
    if (isEnableTemporalSVC != m_config.isEnableTemporalSVC || numTemporalLayers != m_config.numTemporalLayers)
    {
        m_config.isEnableTemporalSVC = isEnableTemporalSVC;
        m_config.numTemporalLayers = numTemporalLayers;

        m_bResetTemporalSVC = true;
    }
}

TemporalSVCCap IntelQsvVideoEncoder::getTemporalSVCCap()
{
    return m_TemporalSVCCap;
}

HWQPThreshold IntelQsvVideoEncoder::getHWQPThreshold()
{
	HWQPThreshold hwQPThreshold;

    if (m_CodecId == MFX_CODEC_HEVC) {
		// Intel Qsv Hevc Encoder Can't Get QP Value From Encoded Bitstream.
    }else {
        hwQPThreshold.lowQpThreshold = 28;
        hwQPThreshold.highQpThreshold = 39;
    }

	return hwQPThreshold;
}

VIDEO_STATNDARD IntelQsvVideoEncoder::getVideoStandard()
{
	if (m_CodecId == MFX_CODEC_HEVC)
		return VideoStandard_ITU_H265;
	else
		return VideoStandard_ITU_H264;
}

VIDEO_CODEC_PROVIDER IntelQsvVideoEncoder::getCodecId()
{
	if (m_CodecId == MFX_CODEC_HEVC)
		return HW_CODEC_INTEL_QUICKSYNC_HEVC;
	else
		return HW_CODEC_INTEL_QUICKSYNC;
}

const char* IntelQsvVideoEncoder::getCodecFriendlyName()
{
	return m_CodecFriendlyName.c_str();
}

bool IntelQsvVideoEncoder::reset()
{
	this->endEncode();
	return this->beginEncode();
}

CODEC_RETCODE CreateIntelQsvVideoEncoder(VIDEO_CODEC_PROVIDER codec, HWVideoEncoder ** ppVideoEncoder)
{
	RTC_LOG(LS_INFO) << "CODEC_RETCODE CreateIntelQsvVideoEncoder";

	mfxU32 codecId;
	if (codec == HW_CODEC_INTEL_QUICKSYNC){
		codecId = MFX_CODEC_AVC;
		RTC_LOG(LS_INFO) << "[IntelQsvVideoEncoder] H264";
	}
	else if (codec == HW_CODEC_INTEL_QUICKSYNC_HEVC){
		codecId = MFX_CODEC_HEVC;
		RTC_LOG(LS_INFO) << "[IntelQsvVideoEncoder] HEVC";
	}
	else
		return CODEC_ERR_UNSUPPORTED;

	IntelQsvVideoEncoder *encoder = new IntelQsvVideoEncoder(codecId);
	if (encoder && encoder->init()) {
		*ppVideoEncoder = encoder;
		return CODEC_OK; // init succeeded
	}

	delete encoder;
	return CODEC_ERR_UNKNOWN;
}

} // namespace netease