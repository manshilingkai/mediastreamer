#include "nv_perf.h"
#include "rtc_base/logging.h"

NvPerf::NvPerf()
{
    NvApiLibrary = NULL;
    NvAPI_QueryInterface = NULL;
    NvAPI_Initialize = NULL;
    NvAPI_Unload = NULL;
    NvAPI_EnumPhysicalGPUs = NULL;
    NvAPI_GPU_GetUsages = NULL;

    gpuCount = 0;
    memset(gpuHandles, 0, sizeof(gpuHandles));
}

NvPerf::~NvPerf()
{
    this->release();
}

bool NvPerf::init()
{    
#ifdef _WIN64
  NvApiLibrary = LoadLibrary(TEXT("nvapi64.dll"));
#else
  NvApiLibrary = LoadLibrary(TEXT("nvapi.dll"));
#endif

  if (NvApiLibrary == NULL)
  {
      RTC_LOG(LS_ERROR) << " LoadLibrary Fail.";
      return false;
  }

  NvAPI_QueryInterface = (_NvAPI_QueryInterface) GetProcAddress(NvApiLibrary, "nvapi_QueryInterface");
  if (NvAPI_QueryInterface == NULL)
  {
      RTC_LOG(LS_ERROR) << " GetProcAddress Fail.";
      this->release();
      return false;
  }
  
  NvAPI_Initialize = (_NvAPI_Initialize) NvAPI_QueryInterface(0x150E828UL);
  if (NvAPI_Initialize == NULL)
  {
      RTC_LOG(LS_ERROR) << " NvAPI_QueryInterface 0x150E828UL Fail.";
      this->release();
      return false;
  }

  NvAPI_Unload = (_NvAPI_Unload)NvAPI_QueryInterface(0xD22BDD7EUL);
  if (NvAPI_Unload == NULL)
  {
      RTC_LOG(LS_ERROR) << " NvAPI_QueryInterface 0xD22BDD7EUL Fail.";
      this->release();
      return false;
  }
  
  NvAPI_EnumPhysicalGPUs = (_NvAPI_EnumPhysicalGPUs) NvAPI_QueryInterface(0xE5AC921FUL);
  if (NvAPI_EnumPhysicalGPUs == NULL)
  {
      RTC_LOG(LS_ERROR) << " NvAPI_QueryInterface 0xE5AC921FUL Fail.";
      this->release();
      return false;
  }
  
  NvAPI_GPU_GetUsages = (_NvAPI_GPU_GetUsages) NvAPI_QueryInterface(0x189A1FDFUL);
  if (NvAPI_GPU_GetUsages == NULL)
  {
      RTC_LOG(LS_ERROR) << " NvAPI_QueryInterface 0x189A1FDFUL Fail.";
      this->release();
      return false;
  }

  if (NvAPI_Initialize() != NVAPI_OK)
  {
      RTC_LOG(LS_ERROR) << " NvAPI_Initialize() Fail.";
      this->release();
      return false;
  }

  if (NvAPI_EnumPhysicalGPUs(gpuHandles, &gpuCount) != NVAPI_OK || gpuCount<=0) {
      RTC_LOG(LS_ERROR) << " NvAPI_EnumPhysicalGPUs() Fail.";
      this->release();
      return false;
  }

  return true;
}

void NvPerf::release()
{
    if (NvAPI_Unload)
    {
        NvAPI_Unload();
        NvAPI_Unload = NULL;
    }

    if (NvApiLibrary)
    {
        FreeLibrary(NvApiLibrary);
        NvApiLibrary = NULL;
    }

    NvAPI_QueryInterface = NULL;
    NvAPI_Initialize = NULL;
    NvAPI_EnumPhysicalGPUs = NULL;
    NvAPI_GPU_GetUsages = NULL;
}

float NvPerf::getGpuUsage()
{
    FLOAT GpuCurrentGpuUsage = 0.0f;

    NV_USAGES_INFO usagesInfo = { NV_USAGES_INFO_VER };
    if (NvAPI_GPU_GetUsages && NvAPI_GPU_GetUsages(gpuHandles[0], &usagesInfo) == NVAPI_OK) {
        GpuCurrentGpuUsage = (FLOAT)usagesInfo.usages[2] / 100;
    }

    return GpuCurrentGpuUsage;
}