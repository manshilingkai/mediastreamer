#pragma once
#include <windows.h>
#include "modules/video_coding/codecs/winhw/perf/nv/nvapi/nvapi.h"

#define NVAPI_MAX_USAGES_PER_GPU    33

// rev - NvAPI_GPU_GetUsages
typedef struct _NV_USAGES_INFO
{
    NvU32 version;                              //!< Structure version
    NvU32 usages[NVAPI_MAX_USAGES_PER_GPU];
} NV_USAGES_INFO;
#define NV_USAGES_INFO_VER  MAKE_NVAPI_VERSION(NV_USAGES_INFO, 1)

typedef PVOID (WINAPIV *_NvAPI_QueryInterface)(NvU32 FunctionOffset);
typedef NvAPI_Status (__cdecl *_NvAPI_Initialize)(VOID);
typedef NvAPI_Status (__cdecl *_NvAPI_Unload)(VOID);
typedef NvAPI_Status (__cdecl *_NvAPI_EnumPhysicalGPUs)(NvPhysicalGpuHandle nvGPUHandle[NVAPI_MAX_PHYSICAL_GPUS], NvU32* pGpuCount);
typedef NvAPI_Status (WINAPIV *_NvAPI_GPU_GetUsages)(_In_ NvPhysicalGpuHandle hPhysicalGpu, _Inout_ NV_USAGES_INFO* pUsagesInfo);

class NvPerf
{
public:
    NvPerf();
    ~NvPerf();

    bool init();
    void release();

    float getGpuUsage();
private:
    HMODULE NvApiLibrary;
    _NvAPI_QueryInterface NvAPI_QueryInterface;
    _NvAPI_Initialize NvAPI_Initialize;
    _NvAPI_Unload NvAPI_Unload;
    _NvAPI_EnumPhysicalGPUs NvAPI_EnumPhysicalGPUs;
    _NvAPI_GPU_GetUsages NvAPI_GPU_GetUsages;

    NvU32 gpuCount;
    NvPhysicalGpuHandle gpuHandles[NVAPI_MAX_PHYSICAL_GPUS];
};

