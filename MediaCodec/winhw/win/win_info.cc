#include "modules/video_coding/codecs/winhw/win/win_info.h"
#include "rtc_base/logging.h"

#define _WIN32_DCOM
#include <iostream>
using namespace std;
#include <comdef.h>
#include <Wbemidl.h>

#pragma comment(lib, "wbemuuid.lib")

namespace netease {

bool getWinInfo(WinInfo &winInfo)
{
    HRESULT hres;

    // Initialize COM. ------------------------------------------
    hres =  CoInitializeEx(0, COINIT_MULTITHREADED); 
    if (FAILED(hres))
    {
        RTC_LOG(LS_ERROR) << "Failed to initialize COM library. Error code = " << hres;
        return false; // Program has failed.
    }

    // Set general COM security levels --------------------------
    hres =  CoInitializeSecurity(
        NULL, 
        -1,                          // COM authentication
        NULL,                        // Authentication services
        NULL,                        // Reserved
        RPC_C_AUTHN_LEVEL_DEFAULT,   // Default authentication 
        RPC_C_IMP_LEVEL_IMPERSONATE, // Default Impersonation  
        NULL,                        // Authentication info
        EOAC_NONE,                   // Additional capabilities 
        NULL                         // Reserved
        );
    if (FAILED(hres) && hres != RPC_E_TOO_LATE)
    {
        RTC_LOG(LS_ERROR) << "Failed to initialize security. Error code = " << hres;
        CoUninitialize();
        return false; // Program has failed.
    }

    if (hres == RPC_E_TOO_LATE)
    {
        RTC_LOG(LS_WARNING) << "CoInitializeSecurity has already been called.";
    }
    
    // Obtain the initial locator to WMI -------------------------
    IWbemLocator *pLoc = NULL;
    hres = CoCreateInstance(
        CLSID_WbemLocator,
        0, 
        CLSCTX_INPROC_SERVER, 
        IID_IWbemLocator, (LPVOID *) &pLoc);
    if (FAILED(hres))
    {
        RTC_LOG(LS_ERROR) << "Failed to create IWbemLocator object. Error code = " << hres;
        CoUninitialize();
        return false; // Program has failed.
    }

    // Connect to WMI through the IWbemLocator::ConnectServer method
    IWbemServices *pSvc = NULL;
    // Connect to the root\cimv2 namespace with
    // the current user and obtain pointer pSvc
    // to make IWbemServices calls.
    hres = pLoc->ConnectServer(
         _bstr_t(L"ROOT\\CIMV2"), // Object path of WMI namespace
         NULL,                    // User name. NULL = current user
         NULL,                    // User password. NULL = current
         0,                       // Locale. NULL indicates current
         NULL,                    // Security flags.
         0,                       // Authority (for example, Kerberos)
         0,                       // Context object 
         &pSvc                    // pointer to IWbemServices proxy
         );
    if (FAILED(hres))
    {
        RTC_LOG(LS_ERROR) << "Could not connect. Error code = " << hres;
        pLoc->Release();     
        CoUninitialize();
        return false; // Program has failed.
    }

//    RTC_LOG(LS_INFO) << "Connected to ROOT\\CIMV2 WMI namespace";

    // Set security levels on the proxy -------------------------
    hres = CoSetProxyBlanket(
       pSvc,                        // Indicates the proxy to set
       RPC_C_AUTHN_WINNT,           // RPC_C_AUTHN_xxx
       RPC_C_AUTHZ_NONE,            // RPC_C_AUTHZ_xxx
       NULL,                        // Server principal name 
       RPC_C_AUTHN_LEVEL_CALL,      // RPC_C_AUTHN_LEVEL_xxx 
       RPC_C_IMP_LEVEL_IMPERSONATE, // RPC_C_IMP_LEVEL_xxx
       NULL,                        // client identity
       EOAC_NONE                    // proxy capabilities 
    );
    if (FAILED(hres))
    {
        RTC_LOG(LS_ERROR) << "Could not set proxy blanket. Error code = " << hres;
        pSvc->Release();
        pLoc->Release();     
        CoUninitialize();
        return false; // Program has failed.
    }

    // Use the IWbemServices pointer to make requests of WMI ----
    IEnumWbemClassObject* pEnumerator = NULL;
    std::string select_sql = "SELECT * FROM Win32_";
    if (winInfo.provider == GPU_INFO)
    {
        select_sql = select_sql + "VideoController";
    }else if(winInfo.provider == CPU_INFO)
    {
        select_sql = select_sql + "Process";
    }else
    {
        select_sql = select_sql + "OperatingSystem";
    }
    hres = pSvc->ExecQuery(
        bstr_t("WQL"), 
        bstr_t(select_sql.c_str()),
        WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY, 
        NULL,
        &pEnumerator);
    if (FAILED(hres))
    {
        RTC_LOG(LS_ERROR) << "Query for operating system name failed. Error code = " << hres;
        pSvc->Release();
        pLoc->Release();
        CoUninitialize();
        return false; // Program has failed.
    }

    // Get the data from the query
    IWbemClassObject *pclsObj = NULL;
    ULONG uReturn = 0;
    while (pEnumerator)
    {
        HRESULT hr = pEnumerator->Next(WBEM_INFINITE, 1, 
            &pclsObj, &uReturn);

        if(0 == uReturn)
        {
            break;
        }

        VARIANT vtProp;

        // Get the value of the Name property
        hr = pclsObj->Get(L"Name", 0, &vtProp, 0, 0);
        wstring name_ws(vtProp.bstrVal);
        std::string name(name_ws.begin(), name_ws.end());
        winInfo.names.push_back(name);
//        RTC_LOG(LS_INFO) << " Name : " << name;
        VariantClear(&vtProp);

        pclsObj->Release();
    }

    // Cleanup
    pSvc->Release();
    pLoc->Release();
    pEnumerator->Release();
    CoUninitialize();

    return true; // Program successfully completed.
}

} // namespace netease