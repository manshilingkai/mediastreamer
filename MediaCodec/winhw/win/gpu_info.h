#pragma once

#include <vector>

namespace netease {

enum GPU_PROVIDER {
    GPU_INTEL = 1,
	GPU_NVIDIA = 2,
	GPU_AMD = 3,
};

struct GpuInfo
{
    GpuInfo()
	{
        provider = GPU_INTEL;
	}

    GPU_PROVIDER provider;
    std::string name;
};

class GPUInfoHandler
{
public:
    static GPUInfoHandler &GetInstance();
    std::vector<GpuInfo>& GetAvailableGpuInfos();
private:
    GPUInfoHandler();
    ~GPUInfoHandler();
    GPUInfoHandler(const GPUInfoHandler &handler);
    const GPUInfoHandler &operator=(const GPUInfoHandler &handler);

    std::vector<GpuInfo> gpuInfos;
};

} // namespace netease