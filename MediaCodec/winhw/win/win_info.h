#pragma once

#include <vector>

namespace netease {

enum WIN_INFO_PROVIDER {
	GPU_INFO = 1,
	CPU_INFO = 2,
	SYS_INFO = 3,
};

struct WinInfo
{
    WinInfo()
	{
        provider = GPU_INFO;
	}

    WIN_INFO_PROVIDER provider;
    std::vector<std::string> names;
};

bool getWinInfo(WinInfo &winInfo);

} // namespace netease