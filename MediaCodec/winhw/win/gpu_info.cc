#include "modules/video_coding/codecs/winhw/win/gpu_info.h"
#include "rtc_base/logging.h"
#include <string>

#ifdef GPU_INFO_FROM_D3D9
#include <d3d9.h>
#pragma comment(lib, "d3d9.lib")
#endif

#ifdef GPU_INFO_FROM_WMI
#include "modules/video_coding/codecs/winhw/win/win_info.h"
#endif

#include <DXGI.h>
#pragma comment(lib, "DXGI.lib")

namespace netease {

#ifdef GPU_INFO_FROM_D3D9
std::vector<GpuInfo> getActiveGpuInfos()
{
    std::vector<GpuInfo> gpuInfos;
    LPDIRECT3D9 d3d9_ = Direct3DCreate9(D3D_SDK_VERSION);
    if (!d3d9_)
        return gpuInfos;

    int count = d3d9_->GetAdapterCount();
    D3DADAPTER_IDENTIFIER9* adapters = (D3DADAPTER_IDENTIFIER9*)malloc(sizeof(D3DADAPTER_IDENTIFIER9) * count);

    for (int index = 0; index < count; ++index) {
        d3d9_->GetAdapterIdentifier(index, 0, &(adapters[index]));
        std::string gpu_name = adapters[index].Description;
        if (gpu_name.find("NVIDIA") != std::string::npos)
        {
            GpuInfo gpuInfo;
            gpuInfo.name = gpu_name;
            gpuInfo.provider = GPU_NVIDIA;
            gpuInfos.push_back(gpuInfo);
        }else if (gpu_name.find("Intel") != std::string::npos)
        {
            GpuInfo gpuInfo;
            gpuInfo.name = gpu_name;
            gpuInfo.provider = GPU_INTEL;
            gpuInfos.push_back(gpuInfo);
        }else if (gpu_name.find("AMD") != std::string::npos)
        {
            GpuInfo gpuInfo;
            gpuInfo.name = gpu_name;
            gpuInfo.provider = GPU_AMD;
            gpuInfos.push_back(gpuInfo);
        }
    }

    free(adapters);

    if (d3d9_) {
        d3d9_->Release();
        d3d9_ = NULL;
    }

    return gpuInfos;
}
#endif

#ifdef GPU_INFO_FROM_WMI
std::vector<GpuInfo> getGpuInfos()
{
    std::vector<GpuInfo> gpuInfos;

    WinInfo winInfo;
    winInfo.provider = GPU_INFO;
	bool i_ret = getWinInfo(winInfo);
    if (!i_ret) return gpuInfos;

    for (std::string gpu_name : winInfo.names)
	{
        if (gpu_name.find("NVIDIA") != std::string::npos)
        {
            GpuInfo gpuInfo;
            gpuInfo.name = gpu_name;
            gpuInfo.provider = GPU_NVIDIA;
            gpuInfos.push_back(gpuInfo);
        }else if (gpu_name.find("Intel") != std::string::npos)
        {
            GpuInfo gpuInfo;
            gpuInfo.name = gpu_name;
            gpuInfo.provider = GPU_INTEL;
            gpuInfos.push_back(gpuInfo);
        }else if (gpu_name.find("AMD") != std::string::npos)
        {
            GpuInfo gpuInfo;
            gpuInfo.name = gpu_name;
            gpuInfo.provider = GPU_AMD;
            gpuInfos.push_back(gpuInfo);
        }
	}

    return gpuInfos;
}
#endif

std::vector<GpuInfo> getGpuInfos()
{
    std::vector<GpuInfo> gpuInfos;

	IDXGIFactory* pFactory;
	HRESULT hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)(&pFactory));
	if (hr != S_OK)
		return gpuInfos;
    
    UINT i = 0; 
    IDXGIAdapter * pAdapter; 
    std::vector <IDXGIAdapter*> vAdapters; 
    while(pFactory->EnumAdapters(i, &pAdapter) != DXGI_ERROR_NOT_FOUND) 
    { 
	    vAdapters.push_back(pAdapter); 
	    ++i; 
    } 

    for (IDXGIAdapter* pIDXGIAdapter : vAdapters)
    {
		DXGI_ADAPTER_DESC adapterDesc;
		hr = pIDXGIAdapter->GetDesc(&adapterDesc);
        if (hr == S_OK)
        {
          std::wstring desc_ws(adapterDesc.Description);
          std::string gpu_name(desc_ws.begin(), desc_ws.end());
          if (gpu_name.find("NVIDIA") != std::string::npos)
          {
              GpuInfo gpuInfo;
              gpuInfo.name = gpu_name;
              gpuInfo.provider = GPU_NVIDIA;
              gpuInfos.push_back(gpuInfo);
          }else if (gpu_name.find("Intel") != std::string::npos)
          {
              GpuInfo gpuInfo;
              gpuInfo.name = gpu_name;
              gpuInfo.provider = GPU_INTEL;
              gpuInfos.push_back(gpuInfo);
          }else if (gpu_name.find("AMD") != std::string::npos)
          {
              GpuInfo gpuInfo;
              gpuInfo.name = gpu_name;
              gpuInfo.provider = GPU_AMD;
              gpuInfos.push_back(gpuInfo);
          }
        }
        pIDXGIAdapter->Release();
    }

	vAdapters.clear();
    pFactory->Release();

    return gpuInfos;
};

GPUInfoHandler &GPUInfoHandler::GetInstance()
{
    static GPUInfoHandler infoHandler;
    return infoHandler;
}

std::vector<GpuInfo>& GPUInfoHandler::GetAvailableGpuInfos()
{
    return gpuInfos;
}

GPUInfoHandler::GPUInfoHandler()
{
    gpuInfos = getGpuInfos();
}

GPUInfoHandler::~GPUInfoHandler()
{
}

} // namespace netease