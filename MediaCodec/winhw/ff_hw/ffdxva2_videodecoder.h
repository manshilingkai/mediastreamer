#pragma once

#include "modules/video_coding/codecs/winhw/libhwcodec.h"

extern "C" {
#include "third_party/ffmpeg/libavcodec/avcodec.h"
#include "third_party/ffmpeg/libavformat/avformat.h"
#include "third_party/ffmpeg/libavutil/pixdesc.h"
#include "third_party/ffmpeg/libavutil/hwcontext.h"
#include "third_party/ffmpeg/libavutil/opt.h"
#include "third_party/ffmpeg/libavutil/avassert.h"
#include "third_party/ffmpeg/libavutil/imgutils.h"
}  // extern "C"

#include "common_video/include/i420_buffer_pool.h"

namespace netease {

struct AVCodecContextFreer {
  void operator()(AVCodecContext* ptr) const { avcodec_free_context(&ptr); }
};
struct AVFrameFreer {
  void operator()(AVFrame* ptr) const { av_frame_free(&ptr); }
};

class FFDXVA2VideoDecoder: public netease::HWVideoDecoder
{
public:
	FFDXVA2VideoDecoder(AVCodecID codecId);
	~FFDXVA2VideoDecoder();

	// implement VideoDecoder interface
	bool init();
	bool setConfig(const VideoDecoderConfig &config);
	bool beginDecode();
	bool addEncodedFrame(const VideoEncodedFrame &frame);
	bool getRawFrame(VideoRawFrame &frame);
	bool endDecode();
	VIDEO_STATNDARD getVideoStandard();
	VIDEO_CODEC_PROVIDER getCodecId();
	const char* getCodecFriendlyName();
	const std::string getCodecGPUName();
private:
  static enum AVPixelFormat find_fmt_by_hw_type(const enum AVHWDeviceType type);
  static enum AVPixelFormat get_hw_format(AVCodecContext *ctx, const enum AVPixelFormat *pix_fmts);
  int hw_decoder_init(AVCodecContext *ctx, const enum AVHWDeviceType type);
  static void av_log_callback(void *ptr, int level, const char *fmt, va_list vl);
private:
  AVCodecID m_CodecId;
  bool m_bDecoderInited = false;
  VideoDecoderConfig m_config;

  webrtc::I420BufferPool pool_;
  std::unique_ptr<AVCodecContext, AVCodecContextFreer> av_context_;
  std::unique_ptr<AVFrame, AVFrameFreer> av_frame_;

	std::string m_CodecFriendlyName;
private:
  AVBufferRef *hw_device_ctx = nullptr;
  static enum AVPixelFormat hw_pix_fmt;
};

CODEC_RETCODE CreateFFDXVA2VideoDecoder(VIDEO_CODEC_PROVIDER codec, HWVideoDecoder** ppVideoDecoder);

}