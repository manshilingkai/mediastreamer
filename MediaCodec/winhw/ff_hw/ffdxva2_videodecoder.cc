#include "modules/video_coding/codecs/winhw/ff_hw/ffdxva2_videodecoder.h"

#include <algorithm>
#include <limits>

extern "C" {
#include "third_party/ffmpeg/libavcodec/avcodec.h"
#include "third_party/ffmpeg/libavformat/avformat.h"
#include "third_party/ffmpeg/libavutil/imgutils.h"
}  // extern "C"

#include "api/video/i420_buffer.h"
#include "common_video/include/video_frame_buffer.h"
#include "rtc_base/checks.h"
#include "rtc_base/criticalsection.h"
#include "rtc_base/keep_ref_until_done.h"
#include "rtc_base/logging.h"
#include "system_wrappers/include/metrics.h"
#include "third_party/libyuv/include/libyuv/planar_functions.h"
#include "third_party/libyuv/include/libyuv/scale.h"
#include "api/video/video_frame.h"

namespace netease {

enum AVPixelFormat FFDXVA2VideoDecoder::hw_pix_fmt = AV_PIX_FMT_NONE;

enum AVPixelFormat FFDXVA2VideoDecoder::find_fmt_by_hw_type(const enum AVHWDeviceType type)
{
    enum AVPixelFormat fmt;
    
    switch (type) {
        case AV_HWDEVICE_TYPE_VAAPI:
            fmt = AV_PIX_FMT_VAAPI;
            break;
        case AV_HWDEVICE_TYPE_DXVA2:
            fmt = AV_PIX_FMT_DXVA2_VLD;
            break;
        case AV_HWDEVICE_TYPE_D3D11VA:
            fmt = AV_PIX_FMT_D3D11;
            break;
        case AV_HWDEVICE_TYPE_VDPAU:
            fmt = AV_PIX_FMT_VDPAU;
            break;
        case AV_HWDEVICE_TYPE_VIDEOTOOLBOX:
            fmt = AV_PIX_FMT_VIDEOTOOLBOX;
            break;
        default:
            fmt = AV_PIX_FMT_NONE;
            break;
    }
    
    return fmt;
}

enum AVPixelFormat FFDXVA2VideoDecoder::get_hw_format(AVCodecContext *ctx,
                                        const enum AVPixelFormat *pix_fmts)
{
    const enum AVPixelFormat *p;
    
    for (p = pix_fmts; *p != -1; p++) {
        if (*p == hw_pix_fmt)
            return *p;
    }
    
    RTC_LOG(LS_ERROR) << "Failed to get HW surface format.";
    return AV_PIX_FMT_NONE;
}

int FFDXVA2VideoDecoder::hw_decoder_init(AVCodecContext *ctx, const enum AVHWDeviceType type)
{
    int err = 0;
    
    if ((err = av_hwdevice_ctx_create(&hw_device_ctx, type, NULL, NULL, 0)) < 0) {
        RTC_LOG(LS_ERROR) << "Failed to create specified HW device.";
        return err;
    }
    ctx->hw_device_ctx = av_buffer_ref(hw_device_ctx);
    
    return err;
}

void FFDXVA2VideoDecoder::av_log_callback(void *ptr, int level, const char *fmt, va_list vl)
{
    if (level>av_log_get_level()) return;
    
    va_list vl2;
    char line[1024];
    static int print_prefix = 1;
    
    va_copy(vl2, vl);
    av_log_format_line(ptr, level, fmt, vl2, line, sizeof(line), &print_prefix);
    va_end(vl2);

    line[1023] = '\0';
    std::string line_str = line;
    
    if (level<=AV_LOG_ERROR) {
      RTC_LOG(LS_ERROR) << line_str;
    }else if(level>AV_LOG_ERROR && level<=AV_LOG_WARNING) {
      RTC_LOG(LS_WARNING) << line_str;
    }else if(level>AV_LOG_WARNING && level<=AV_LOG_INFO ) {
      RTC_LOG(LS_INFO) << line_str;
    }else if(level>AV_LOG_INFO && level<=AV_LOG_VERBOSE) {
      RTC_LOG(LS_VERBOSE) << line_str;
    }else{
      RTC_LOG(LS_SENSITIVE) << line_str;
    }
}

FFDXVA2VideoDecoder::FFDXVA2VideoDecoder(AVCodecID codecId) : pool_(true, 30)
{
  m_CodecId = codecId;
  m_bDecoderInited = false;

  av_context_.reset();
  av_frame_.reset();

  if (m_CodecId == AV_CODEC_ID_HEVC) {
      m_CodecFriendlyName = "H265 Decoder [FF HW]";
  }else {
      m_CodecFriendlyName = "H264 Decoder [FF HW]";
  }

  av_log_set_level(AV_LOG_WARNING);
  av_log_set_callback(av_log_callback);
}

FFDXVA2VideoDecoder::~FFDXVA2VideoDecoder()
{
  this->endDecode();
}

bool FFDXVA2VideoDecoder::init()
{
  this->endDecode();
  RTC_DCHECK(!av_context_);

  RTC_LOG(LS_INFO) << "init success";
  
  return true;
}

bool FFDXVA2VideoDecoder::setConfig(const VideoDecoderConfig &config)
{
  m_config = config;
  return true;
}

bool FFDXVA2VideoDecoder::beginDecode()
{
  if(m_bDecoderInited) return true;

  // Initialize AVCodecContext.
  av_context_.reset(avcodec_alloc_context3(nullptr));

  av_context_->codec_type = AVMEDIA_TYPE_VIDEO;
  av_context_->codec_id = m_CodecId;
  av_context_->coded_width = m_config.width;
  av_context_->coded_height = m_config.height;

  AVCodec* codec = avcodec_find_decoder(av_context_->codec_id);
  if (!codec) {
    // This is an indication that FFmpeg has not been initialized or it has not
    // been compiled/initialized with the correct set of codecs.
    RTC_LOG(LS_ERROR) << "avcodec_find_decoder return null.";
    av_context_.reset();
    return false;
  }

  enum AVHWDeviceType type = AV_HWDEVICE_TYPE_D3D11VA;
  hw_pix_fmt = find_fmt_by_hw_type(type);

  av_context_->get_format  = get_hw_format;
  av_context_->refcounted_frames = 1;
    
  if (hw_decoder_init(av_context_.get(), type) < 0)
  {
    RTC_LOG(LS_ERROR) << "hw_decoder_init fail";
    av_context_.reset();
    return false;
  }

  int res = avcodec_open2(av_context_.get(), codec, nullptr);
  if (res < 0) {
    RTC_LOG(LS_ERROR) << "avcodec_open2 error: " << res;

    if (hw_device_ctx) {
        av_buffer_unref(&hw_device_ctx);
        hw_device_ctx = nullptr;
    }

    av_context_.reset();
    return false;
  }

  av_frame_.reset(av_frame_alloc());

  RTC_LOG(LS_INFO) << "beginDecode success";

  m_bDecoderInited = true;

  return true;
}

bool FFDXVA2VideoDecoder::addEncodedFrame(const VideoEncodedFrame &frame)
{
  if (!m_bDecoderInited) {
      RTC_LOG(LS_WARNING) << "Please Call beginDecode()";
      return true;
  }

  AVPacket packet;
  av_init_packet(&packet);
  packet.data = frame.frameData.ptr;
  packet.size = frame.frameSize;
  int64_t frame_timestamp_us = frame.pts;
  av_context_->reordered_opaque = frame_timestamp_us;

  int result = avcodec_send_packet(av_context_.get(), &packet);
  if (result < 0) {
    RTC_LOG(LS_ERROR) << "avcodec_send_packet error: " << result;
    return false;
  }

  return true;
}

bool FFDXVA2VideoDecoder::getRawFrame(VideoRawFrame &frame)
{
  if (!m_bDecoderInited) {
      RTC_LOG(LS_WARNING) << "Please Call beginDecode()";
      return true;
  }

  int result = avcodec_receive_frame(av_context_.get(), av_frame_.get());
  if (result < 0) {
    RTC_LOG(LS_ERROR) << "avcodec_receive_frame error: " << result;
    return false;
  }

  AVFrame *tmp_frame = nullptr;
  AVFrame *sw_frame = av_frame_alloc();
  if (av_frame_->format == hw_pix_fmt) {
    RTC_LOG(LS_INFO) << "retrieve data from GPU to CPU";
    if (av_hwframe_transfer_data(sw_frame, av_frame_.get(), 0) < 0) {
        av_frame_unref(av_frame_.get());
        av_frame_free(&sw_frame);
        RTC_LOG(LS_ERROR) << "Error transferring the data to system memory";
        return false;
    }
  	av_frame_copy_props(sw_frame, av_frame_.get());
    tmp_frame = sw_frame;
  }else {
    RTC_LOG(LS_INFO) << "retrieve data from CPU";
    tmp_frame = av_frame_.get();
  }

  rtc::scoped_refptr<webrtc::I420Buffer> frame_buffer = pool_.CreateBuffer(tmp_frame->width, tmp_frame->height);
  if (frame_buffer == nullptr)
  {
    av_frame_unref(av_frame_.get());
    av_frame_free(&sw_frame);
    RTC_LOG(LS_ERROR) << "CreateBuffer Fail With Width:" << av_frame_->width << " Height:" << av_frame_->height;
    return false;
  }

  libyuv::NV12ToI420(tmp_frame->data[0], tmp_frame->linesize[0],
                     tmp_frame->data[1], tmp_frame->linesize[1],
                     frame_buffer->MutableDataY(), frame_buffer->StrideY(),
                     frame_buffer->MutableDataU(), frame_buffer->StrideU(),
                     frame_buffer->MutableDataV(), frame_buffer->StrideV(),
                     tmp_frame->width, tmp_frame->height);

  frame.width = tmp_frame->width;
  frame.height = tmp_frame->height;
  frame.timestamp = tmp_frame->reordered_opaque;

  frame.format = FOURCC_I420;
  frame.frame_buffer = frame_buffer;

  // Stop referencing it, possibly freeing |input_frame|.
  av_frame_unref(av_frame_.get());
  av_frame_free(&sw_frame);

  return true;
}

bool FFDXVA2VideoDecoder::endDecode()
{
  if(!m_bDecoderInited) return true;

  av_context_.reset();
  av_frame_.reset();

  if (hw_device_ctx) {
    av_buffer_unref(&hw_device_ctx);
    hw_device_ctx = nullptr;
  }

  m_bDecoderInited = false;
  
  return true;
}

VIDEO_STATNDARD FFDXVA2VideoDecoder::getVideoStandard()
{
	if (m_CodecId == AV_CODEC_ID_HEVC)
		return VideoStandard_ITU_H265;
	else
		return VideoStandard_ITU_H264;
}

VIDEO_CODEC_PROVIDER FFDXVA2VideoDecoder::getCodecId()
{
	if (m_CodecId == AV_CODEC_ID_HEVC)
		return HW_CODEC_FF_DXVA2_HEVC;
	else
		return HW_CODEC_FF_DXVA2;
}

const char* FFDXVA2VideoDecoder::getCodecFriendlyName()
{
    return m_CodecFriendlyName.c_str();
}

const std::string FFDXVA2VideoDecoder::getCodecGPUName()
{
    return "";
}

CODEC_RETCODE CreateFFDXVA2VideoDecoder(VIDEO_CODEC_PROVIDER codec, HWVideoDecoder** ppVideoDecoder)
{
    RTC_LOG(LS_INFO) << "CODEC_RETCODE CreateFFDXVA2VideoDecoder";

	AVCodecID codecId;
	if (codec == HW_CODEC_FF_DXVA2){
		codecId = AV_CODEC_ID_H264;
		RTC_LOG(LS_INFO) << "[FFDXVA2VideoDecoder] H264";
	}
	else if (codec == HW_CODEC_FF_DXVA2_HEVC){
		codecId = AV_CODEC_ID_HEVC;
		RTC_LOG(LS_INFO) << "[FFDXVA2VideoDecoder] HEVC";
	}
	else
		return CODEC_ERR_UNSUPPORTED;

	FFDXVA2VideoDecoder *decoder = new FFDXVA2VideoDecoder(codecId);
	if (decoder && decoder->init()) {
		*ppVideoDecoder = decoder;
		return CODEC_OK; // init succeeded
	}

	delete decoder;
	return CODEC_ERR_UNKNOWN;
}

}