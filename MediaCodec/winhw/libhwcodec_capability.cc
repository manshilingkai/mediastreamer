#include "modules/video_coding/codecs/winhw/libhwcodec_capability.h"
#include "modules/video_coding/codecs/winhw/win/gpu_info.h"
#include "modules/video_coding/codecs/winhw/intel_qsv/intel_qsv_videocodec_capability.h"
#include "modules/video_coding/codecs/winhw/nvidia/nvidia_videocodec_capability.h"
#include "modules/video_coding/codecs/winhw/amd/amd_videocodec_capability.h"
#include "rtc_base/logging.h"

namespace netease {

HWVideoCodecCapability GetWinHWVideoCodecCapability()
{
    HWVideoCodecCapability ret;
    std::vector<GpuInfo>& gpuInfos = GPUInfoHandler::GetInstance().GetAvailableGpuInfos();
    for (GpuInfo gpuInfo : gpuInfos)
    {
        if (gpuInfo.provider == GPU_NVIDIA)
        {
            HWVideoCodecCapability nvidiaHWVideoCodecCapability = NvidiaVideoCodecCapability::GetInstance().GetHWVideoCodecCapability();
            ret.isSupportH264Decode = ret.isSupportH264Decode || nvidiaHWVideoCodecCapability.isSupportH264Decode;
            ret.isSupportH264Encode = ret.isSupportH264Encode || nvidiaHWVideoCodecCapability.isSupportH264Encode;
            ret.isSupportH265Decode = ret.isSupportH265Decode || nvidiaHWVideoCodecCapability.isSupportH265Decode;
            ret.isSupportH265Encode = ret.isSupportH265Encode || nvidiaHWVideoCodecCapability.isSupportH265Encode;
        }else if (gpuInfo.provider == GPU_AMD)
        {
            HWVideoCodecCapability amdHWVideoCodecCapability = AMDVideoCodecCapability::GetInstance().GetHWVideoCodecCapability();
            ret.isSupportH264Decode = ret.isSupportH264Decode || amdHWVideoCodecCapability.isSupportH264Decode;
            ret.isSupportH264Encode = ret.isSupportH264Encode || amdHWVideoCodecCapability.isSupportH264Encode;
            ret.isSupportH265Decode = ret.isSupportH265Decode || amdHWVideoCodecCapability.isSupportH265Decode;
            ret.isSupportH265Encode = ret.isSupportH265Encode || amdHWVideoCodecCapability.isSupportH265Encode;
        }else if (gpuInfo.provider == GPU_INTEL)
        {
            HWVideoCodecCapability intelqsvHWVideoCodecCapability = IntelQsvVideoCodecCapability::GetInstance().GetHWVideoCodecCapability();
            ret.isSupportH264Decode = ret.isSupportH264Decode || intelqsvHWVideoCodecCapability.isSupportH264Decode;
            ret.isSupportH264Encode = ret.isSupportH264Encode || intelqsvHWVideoCodecCapability.isSupportH264Encode;
            ret.isSupportH265Decode = ret.isSupportH265Decode || intelqsvHWVideoCodecCapability.isSupportH265Decode;
            ret.isSupportH265Encode = ret.isSupportH265Encode || intelqsvHWVideoCodecCapability.isSupportH265Encode;
        }
    }
    return ret;
}

} // namespace netease