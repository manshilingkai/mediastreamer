#include "modules/video_coding/codecs/winhw/h265/winhw_h265_encoder.h"
#include "rtc_base/logging.h"

#define WIN_HW_H265_ENCODE_MAX_NO_OUTPUT_RETRY_TIMES 20

namespace webrtc {

WindowsHwH265Encoder::WindowsHwH265Encoder() : isInitialized_(false),
  hevcSeqId_(0),
  requestingHevcKeyrame_(false) {
    pVideoEncoder = nullptr;
    mVideoCodecProvider = netease::HW_CODEC_AUTOSELECT_HEVC;
	  mNoOutputRetryTimes = 0;
}

WindowsHwH265Encoder::~WindowsHwH265Encoder() {
  Uninitialize();
}

int WindowsHwH265Encoder::Initialize(const WinHwH265EncoderParam &param) {
  if (isInitialized_)
    Uninitialize();

  if (!ConfigParam(param))
    return -1;

  CreateHevcEncoderParams();

  netease::CODEC_RETCODE enc_ret = netease::CreateHWVideoEncoder(&mVideoCodecProvider, &pVideoEncoder, videoEncoderParam.width, videoEncoderParam.height, param.hwH265EncListStrategy, param.hwH265EncAdaptationList);
  if (enc_ret != netease::CODEC_OK )
  {
    pVideoEncoder = nullptr;
    RTC_LOG(LS_ERROR) << __FUNCTION__ << " failed to create win hw h265 encoder";
    return -1;
  }

  pVideoEncoder->setConfig(videoEncoderParam);
  bool b_ret = pVideoEncoder->beginEncode();
  if (!b_ret)
  {
    netease::ReleaseHWVideoEncoder(mVideoCodecProvider, pVideoEncoder);
    pVideoEncoder = nullptr;
    RTC_LOG(LS_ERROR) << __FUNCTION__ << " failed to start win hw h265 encoder";
    return -1;
  }
  

  RTC_LOG(LS_INFO) << __FUNCTION__ << " win hw h265 encoder initialized. ";
  isInitialized_ = true;
  return 0;
}

void WindowsHwH265Encoder::Uninitialize() {
  if (isInitialized_) {
    if (pVideoEncoder) {
      netease::ReleaseHWVideoEncoder(mVideoCodecProvider, pVideoEncoder);
      pVideoEncoder = nullptr;
    }
  }

  hevcSeqId_ = 0;
  requestingHevcKeyrame_ = false;
  isInitialized_ = false;

  mNoOutputRetryTimes = 0;
}

int WindowsHwH265Encoder::EncodeFrame(const WinHwH265SourcePicture* pSrcPic, WinHwH265FrameBitStream* pBs) {
  if (!pSrcPic || !pBs || !isInitialized_)
    return -1;
  InitFrameBitStream(pBs);
  return EncodeFrameHevc(pSrcPic, pBs);
}

int WindowsHwH265Encoder::ForceIntraFrame() {
  if (!isInitialized_)
    return -1;

  requestingHevcKeyrame_ = true;
  return 0;
}

int WindowsHwH265Encoder::SetOption(WinHwH265ReconfigOption option, void* value) {
  if (!value || !isInitialized_)
    return -1;

  switch(option) {
  case WinHwH265ReconfigOption::kBitrate: {
      config_.iTargetBps = *((int32_t *)value);
      videoEncoderParam.avgBitrate = config_.iTargetBps;
      if (pVideoEncoder)
      {
        pVideoEncoder->resetBitrate(videoEncoderParam.avgBitrate);
      }
      
      RTC_LOG(LS_INFO) << "reconig win hw h265 encoder, bitrate: " << config_.iTargetBps;
      return 0;
  }
  case WinHwH265ReconfigOption::kFrameRate: {
      config_.fMaxFps = *((float *)value);
	    int fps = config_.fMaxFps;
      videoEncoderParam.fps = fps < netease::kDefaultHWCodecVideoMaxFramerate ? fps : netease::kDefaultHWCodecVideoMaxFramerate;
      if (pVideoEncoder)
      {
        pVideoEncoder->resetFramerate(videoEncoderParam.fps);
      }

      RTC_LOG(LS_INFO) << "reconig win hw h265 encoder, fps: " << config_.fMaxFps;
      return 0;
  }
  default:
    return -1;
  }
}

bool WindowsHwH265Encoder::ConfigParam(const WinHwH265EncoderParam &param) {
  if (param.iTemporalLayerNum < 1
      || param.iTemporalLayerNum > 4
      || param.fMaxFps > 120
      || param.iHeight * param.iWidth < 16 * 16
      || param.iHeight * param.iWidth > 7680 * 4320
      || param.iTargetBps > param.iMaxBps
      || param.iMaxSliceSize < 0) {
    RTC_LOG(LS_ERROR) << __FUNCTION__ << " invalid param";
    return false;
  }

  config_ = param;
  return true;
}

void WindowsHwH265Encoder::CreateHevcEncoderParams() {
  videoEncoderParam.width = config_.iWidth;
  videoEncoderParam.height = config_.iHeight;
  int fps = config_.fMaxFps;
  videoEncoderParam.fps = fps < netease::kDefaultHWCodecVideoMaxFramerate ? fps : netease::kDefaultHWCodecVideoMaxFramerate;
  videoEncoderParam.avgBitrate = config_.iTargetBps;
  videoEncoderParam.gop = config_.iGop;
  videoEncoderParam.numBFrame = 0;
  videoEncoderParam.rcMode = netease::VideoEncoderConfig::RC_CBR;

  if (config_.bitrateAdjusterType == netease::BASE_BR_ADJUSTER) {
    videoEncoderParam.enableBitrateAdjuster = false;
  }else if (config_.bitrateAdjusterType == netease::DYNAMIC_BR_ADJUSTER) {
    videoEncoderParam.enableBitrateAdjuster = true;
  }else {
    videoEncoderParam.enableBitrateAdjuster = false;
  }

  RTC_LOG(LS_INFO) << "isScreenShare : " << config_.isScreenShare << " enableBitrateAdjuster : " << videoEncoderParam.enableBitrateAdjuster;

  float minAdjustedBitratePct = 0.5f;
  float maxAdjustedBitratePct = 1.0f;
  bool enable_dynamic_adjust_max_bitrate_pct = true;

  if (config_.minAdjustedBitratePct > 0) {
    minAdjustedBitratePct = config_.minAdjustedBitratePct;
  }
  if (config_.maxAdjustedBitratePct > 0) {
    maxAdjustedBitratePct = config_.maxAdjustedBitratePct;
    enable_dynamic_adjust_max_bitrate_pct = false;
  }

  videoEncoderParam.minAdjustedBitratePct = minAdjustedBitratePct;
  videoEncoderParam.maxAdjustedBitratePct = maxAdjustedBitratePct;
  videoEncoderParam.enableDynamicAdjustMaxBitratePct = enable_dynamic_adjust_max_bitrate_pct;
}

int WindowsHwH265Encoder::EncodeFrameHevc(const WinHwH265SourcePicture* pSrcPic, WinHwH265FrameBitStream* pBs)
{
  netease::VideoRawFrame rawFrame;
  rawFrame.width = pSrcPic->iPicWidth;
  rawFrame.height = pSrcPic->iPicHeight;
  if (pSrcPic->eColorFormat == webrtc::WinHwH265ColorFormat::kI420)
  {
    rawFrame.format = netease::FOURCC_I420;
    rawFrame.planeptrs[0].ptr = pSrcPic->pData[0];
    rawFrame.planeptrs[1].ptr = pSrcPic->pData[1];
    rawFrame.planeptrs[2].ptr = pSrcPic->pData[2];
    rawFrame.strides[0] = pSrcPic->iStride[0];
    rawFrame.strides[1] = pSrcPic->iStride[1];
    rawFrame.strides[2] = pSrcPic->iStride[2];
  }else if (pSrcPic->eColorFormat == webrtc::WinHwH265ColorFormat::kNV12)
  {
    rawFrame.format = netease::FOURCC_NV12;
    rawFrame.planeptrs[0].ptr = pSrcPic->pData[0];
    rawFrame.planeptrs[1].ptr = pSrcPic->pData[1];
    rawFrame.strides[0] = pSrcPic->iStride[0];
    rawFrame.strides[1] = pSrcPic->iStride[1];
  }
  rawFrame.timestamp = pSrcPic->uiTimeStamp;

  if (requestingHevcKeyrame_)
  {
    requestingHevcKeyrame_ = false;
    pVideoEncoder->forceIntraFrame();
  }
  
  bool ret = pVideoEncoder->addRawFrame(rawFrame);
  if (!ret)
  {
    RTC_LOG(LS_ERROR) << __FUNCTION__ << " failed to add raw frame";
    if (rawFrame.width * rawFrame.height > 3840 * 2160)
    {
      pVideoEncoder->reset();
      return 0;
    }else {
      return -1;
    }
  }
  
  netease::VideoEncodedFrame encodedFrame;
  ret = pVideoEncoder->getEncodedFrame(encodedFrame);
  if (!ret)
  {
    mNoOutputRetryTimes++;
    if (mNoOutputRetryTimes > WIN_HW_H265_ENCODE_MAX_NO_OUTPUT_RETRY_TIMES)
    {
        RTC_LOG(LS_ERROR) << __FUNCTION__ << " no output encoded frame : exceeded maximum number of retries";
        if (rawFrame.width * rawFrame.height > 3840 * 2160)
        {
          pVideoEncoder->reset();
          return 0;
        }else {
          return -7;
        }
    }else {
        RTC_LOG(LS_WARNING) << __FUNCTION__ << " no output encoded frame";
        return 0;
    }
  }
  
  mNoOutputRetryTimes = 0;

  h265_bitstream_parser_.ParseBitstream(encodedFrame.frameData.ptr, encodedFrame.frameSize, true);
  if (encodedFrame.frameType == netease::IDR_Frame)
  {
    pBs->isKeyFrame = true;
  } else {
    pBs->isKeyFrame = false;
  }

  bool has_vps = false;
  bool has_sps = false;
  bool has_pps = false;
  bool is_idr = h265_bitstream_parser_.isLastIdr(has_vps, has_sps, has_pps);
  if (is_idr)
  {
    pBs->isKeyFrame = true;
  }

  if (has_vps != has_sps)
  {
    RTC_LOG(LS_ERROR) << __FUNCTION__ << "h265 bitstream has only vps but no sps or has only sps but on vps!!";
    return -8;
  }
  
  std::vector<H265::NaluIndex> nalu_indices = h265_bitstream_parser_.GetLastNaluIndices();
  uint32_t nals_count = nalu_indices.size();

  uint32_t FrameSizeInBytes = 0;

  // -3 : remove vps&sps&pps
  // -2 : remove pps
  // -1 : remove vps&sps
  //  0 : no
  //  1 : insert vps&sps
  //  2 : insert pps
  //  3 : insert vps&sps&pps
  int flag = 0;
  if (is_idr)
  {
    if (has_vps && has_sps && has_pps) {
      flag = 0;
    }else if (has_vps && has_sps && !has_pps) {
      flag = 2;
    }else if (!has_vps && !has_sps && has_pps) {
      flag = 1;
    }else if(!has_vps && !has_sps && !has_pps) {
      flag = 3;
    }
  }else {
    if (has_vps && has_sps && has_pps) {
      flag = -3;
    }else if (has_vps && has_sps && !has_pps) {
      flag = -1;
    }else if (!has_vps && !has_sps && has_pps) {
      flag = -2;
    }else if(!has_vps && !has_sps && !has_pps) {
      flag = 0;
    }
  }

  if (flag == 3) {
    pBs->iNalCount = nals_count+3;
  }else if (flag == 2) {
    pBs->iNalCount = nals_count+1;
  }else if (flag == 1) {
    pBs->iNalCount = nals_count+2;
  }else if (flag == 0) {
    pBs->iNalCount = nals_count;
  }else if (flag == -1) {
    pBs->iNalCount = nals_count-2;
  }else if (flag == -2) {
    pBs->iNalCount = nals_count-1;
  }else if (flag == -3) {
    pBs->iNalCount = nals_count-3;
  }
  
  if (pBs->iNalCount <= 0)
  {
      RTC_LOG(LS_WARNING) << __FUNCTION__ << "redundance bitstream, ignore it";
      return 0;
  }

  pBs->iTemporalLayerId = 0;
  pBs->iSubSeqId = hevcSeqId_++;
  pBs->uiTimeStamp = encodedFrame.dts;
    
  if (bsBuf_.size() < (size_t)pBs->iNalCount) {
    bsBuf_.resize((size_t)pBs->iNalCount);
    nalLens_.resize((size_t)pBs->iNalCount);
  }

  uint32_t read_start_idx = 0;
  uint32_t write_start_idx = 0;
  if (flag == 3) {
    size_t vps_size = 0;
	  bsBuf_[0] = h265_bitstream_parser_.GetLastVps(vps_size);
    nalLens_[0] = vps_size;
	  FrameSizeInBytes += nalLens_[0];
	
    size_t sps_size = 0;
    bsBuf_[1] = h265_bitstream_parser_.GetLastSps(sps_size);
    nalLens_[1] = sps_size;
    FrameSizeInBytes += nalLens_[1];

    size_t pps_size = 0;
    bsBuf_[2] = h265_bitstream_parser_.GetLastPps(pps_size);
    nalLens_[2] = pps_size;
    FrameSizeInBytes += nalLens_[2];

    RTC_LOG(LS_INFO) << "vps size : " << vps_size << " sps size : " << sps_size << " pps size : " << pps_size;

    if (bsBuf_[0] == NULL || nalLens_[0] == 0 || bsBuf_[1] == NULL || nalLens_[1] == 0 || bsBuf_[2] == NULL || nalLens_[2] == 0)
    {
        RTC_LOG(LS_ERROR) << __FUNCTION__ << " no backup vps sps and pps to put into idr which has no vps sps and pps";
        return -4;
    }

    read_start_idx = 0;
    write_start_idx = 3;
  }else if (flag == 1) {
    size_t vps_size = 0;
    bsBuf_[0] = h265_bitstream_parser_.GetLastVps(vps_size);
    nalLens_[0] = vps_size;
    FrameSizeInBytes += nalLens_[0];
	
    size_t sps_size = 0;
    bsBuf_[1] = h265_bitstream_parser_.GetLastSps(sps_size);
    nalLens_[1] = sps_size;
    FrameSizeInBytes += nalLens_[1];

    RTC_LOG(LS_INFO) << "vps size : " << vps_size << " sps size : " << sps_size;

    if (bsBuf_[0] == NULL || nalLens_[0] == 0 || bsBuf_[1] == NULL || nalLens_[1] == 0)
    {
        RTC_LOG(LS_ERROR) << __FUNCTION__ << " no backup vps and sps to put into idr which has no vps and sps";
        return -6;
    }
    read_start_idx = 0;
    write_start_idx = 2;
  }else if (flag == 0) {
    read_start_idx = 0;
    write_start_idx = 0;
  }else if (flag == -1) {
    read_start_idx = 2;
    write_start_idx = 0;
  }else if (flag == -2) {
    read_start_idx = 1;
    write_start_idx = 0;
  }else if (flag == -3) {
    read_start_idx = 3;
    write_start_idx = 0;
  }
  uint8_t* bitstream = encodedFrame.frameData.ptr;
  for (uint32_t i = read_start_idx; i < nals_count; ++i) {
    H265::NaluIndex index = nalu_indices[i];
    bsBuf_[write_start_idx+i] = &bitstream[index.start_offset];
    nalLens_[write_start_idx+i] = index.nalu_size;
    FrameSizeInBytes += nalLens_[write_start_idx+i];
  }

  pBs->iFrameSizeInBytes = FrameSizeInBytes;
  pBs->pNalLengthInByte = &nalLens_[0];
  pBs->ppBsBuf = &bsBuf_[0];

  int qp = 0;
  h265_bitstream_parser_.GetLastSliceQp(&qp);
  //RTC_LOG(LS_INFO) << __FUNCTION__ << " win hw h265 enc qp [from bitstream] : " << qp;

  if(encodedFrame.qp > 0) {
    qp = encodedFrame.qp;
  }

  pBs->iFrameQp = (int)qp;
  pBs->iTargetBit = config_.iTargetBps;
  pBs->iTotalBit = config_.iTargetBps;
  pBs->fQpAvgAq = (int)qp;
  pBs->iAvelineQp = (int)qp;
  pBs->frameCost = 0;
  pBs->iCntUnrefP = 0;

  pBs->adjustedEncBitrateBps = encodedFrame.adjustedEncBitrateBps;

  return encodedFrame.frameSize;
}

void WindowsHwH265Encoder::InitFrameBitStream(WinHwH265FrameBitStream* pBs) const {
  if (!pBs)
    return;
  pBs->isKeyFrame = false;
  pBs->iFrameSizeInBytes = 0;
  pBs->iNalCount = 0;
  pBs->iSubSeqId = 0;
  pBs->iTemporalLayerId = 0;
  pBs->pNalLengthInByte = nullptr;
  pBs->ppBsBuf = nullptr;
  pBs->uiTimeStamp = 0;
}

const char* WindowsHwH265Encoder::getEncoderFriendlyName()
{
  if (pVideoEncoder)
  {
    pVideoEncoder->getCodecFriendlyName();
  }else {
    return "unknown";
  }
}

WinHwH265QPThreshold WindowsHwH265Encoder::getWinHwH265QPThreshold()
{
  WinHwH265QPThreshold winHwH265QPThreshold;
  if (pVideoEncoder) {
    netease::HWQPThreshold hwH265QPThreshold = pVideoEncoder->getHWQPThreshold();
    winHwH265QPThreshold.lowQpThreshold = hwH265QPThreshold.lowQpThreshold;
    winHwH265QPThreshold.highQpThreshold = hwH265QPThreshold.highQpThreshold;
  }
  return winHwH265QPThreshold;
}

}
