/*
 *  Copyright (c) 2021 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 *
 */

#ifndef MODULES_VIDEO_CODING_CODECS_WINHW_H265_WINHW_H265_DECODER_IMPL_H_
#define MODULES_VIDEO_CODING_CODECS_WINHW_H265_WINHW_H265_DECODER_IMPL_H_

#include <memory>

#include "modules/video_coding/codecs/winhw/h265/include/winhwh265.h"
#include "common_video/h265/h265_bitstream_parser.h"
#include "common_video/include/i420_buffer_pool.h"

#include "modules/video_coding/codecs/winhw/libhwcodec.h"
#include "media/engine/engine_parameter_collection.h"

namespace webrtc {

class WinHwH265DecoderImpl : public WinHwH265Decoder
{
public:
    WinHwH265DecoderImpl();
    ~WinHwH265DecoderImpl() override;

    int32_t InitDecode(const VideoCodec* codec_settings, int32_t number_of_cores) override;
    int32_t Release() override;

    int32_t RegisterDecodeCompleteCallback(DecodedImageCallback* callback) override;

    int32_t Decode(const EncodedImage& input_image,
                bool /*missing_frames*/,
                const CodecSpecificInfo* codec_specific_info = nullptr,
                int64_t render_time_ms = -1) override;

    const char* ImplementationName() const override;

private:
    bool IsInitialized() const;

    // Reports statistics with histograms.
    void ReportInit();
    void ReportError();

    I420BufferPool pool_;

    DecodedImageCallback* decoded_image_callback_;

    bool has_reported_init_;
    bool has_reported_error_;

    webrtc::H265BitstreamParser h265_bitstream_parser_;
    int h265_bitstream_width = 0;
    int h265_bitstream_height = 0;

    bool is_initialized_;
private:
    netease::HWVideoDecoder *pVideoDecoder;
    netease::VIDEO_CODEC_PROVIDER mVideoCodecProvider;
    int mNoOutputRetryTimes = 0;
private:
    webrtc::EngineParameterCollection* ne_parameters_ = nullptr;
    void SetHWH265DecodeAdaptation(std::string hwH265DecAdaptation);
    int hwH265DecListStrategy = 0;
    std::string hwH265DecAdaptationList = "";
};

}  // namespace webrtc

#endif