/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 *
 */

#include "modules/video_coding/codecs/winhw/h265/include/winhwh265.h"

#include "api/video_codecs/sdp_video_format.h"
#include "media/base/h265_profile_level_id.h"
#include "media/base/h264_profile_level_id.h"

#include "modules/video_coding/codecs/winhw/h265/winhw_h265_decoder_impl.h"
#include "modules/video_coding/codecs/winhw/h265/winhw_h265_encoder_impl.h"
#include "modules/video_coding/codecs/winhw/libhwcodec_capability.h"

#include "absl/memory/memory.h"
#include "rtc_base/checks.h"
#include "rtc_base/logging.h"

namespace webrtc {

namespace {

SdpVideoFormat CreateWinHwH265Format(H264::Profile profile,
                                H264::Level level,
                                const std::string& packetization_mode) {
  const absl::optional<std::string> profile_string =
      H264::ProfileLevelIdToString(H264::ProfileLevelId(profile, level));
  RTC_CHECK(profile_string);
  return SdpVideoFormat(
      cricket::kH265CodecName,
      {{cricket::kH265FmtpProfileLevelId, *profile_string},
       {cricket::kH265FmtpLevelAsymmetryAllowed, "1"},
       {cricket::kH265FmtpPacketizationMode, packetization_mode}});
}

}  // namespace

std::vector<SdpVideoFormat> SupportedWinHwH265Codecs() {
  // We only support encoding Constrained Baseline Profile (CBP), but the
  // decoder supports more profiles. We can list all profiles here that are
  // supported by the decoder and that are also supersets of CBP, i.e. the
  // decoder for that profile is required to be able to decode CBP. This means
  // we can encode and send CBP even though we negotiated a potentially
  // higher profile. See the H265 spec for more information.
  //
  // We support both packetization modes 0 (mandatory) and 1 (optional,
  // preferred).
  return {CreateWinHwH265Format(H264::kProfileConstrainedBaseline, H264::kLevel3_1, "1")};
}

std::unique_ptr<WinHwH265Encoder> WinHwH265Encoder::Create(
    const cricket::VideoCodec& codec) {
  if(!WinHwH265Encoder::IsSupported()) {
    return nullptr;
  }
  RTC_LOG(LS_INFO) << "Creating WinHwH265EncoderImpl.";
  return absl::make_unique<WinHwH265EncoderImpl>(codec);
}

bool WinHwH265Encoder::IsSupported()
{
  // netease::HWVideoCodecCapability hwVideoCodecCapability = netease::GetWinHWVideoCodecCapability();
  // return hwVideoCodecCapability.isSupportH265Encode;
  return true;
}

std::unique_ptr<WinHwH265Decoder> WinHwH265Decoder::Create() {
  if(!WinHwH265Decoder::IsSupported()) {
    return nullptr;
  }
  RTC_LOG(LS_INFO) << "Creating WinHwH265DecoderImpl.";
  return absl::make_unique<WinHwH265DecoderImpl>();
}

bool WinHwH265Decoder::IsSupported()
{
  // netease::HWVideoCodecCapability hwVideoCodecCapability = netease::GetWinHWVideoCodecCapability();
  // return hwVideoCodecCapability.isSupportH265Decode;
  return true;
}

}  // namespace webrtc
