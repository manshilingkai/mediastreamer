#ifndef MODULES_VIDEO_CODING_CODECS_WINHW_H265_WINHW_H265_ENCODER_H_
#define MODULES_VIDEO_CODING_CODECS_WINHW_H265_WINHW_H265_ENCODER_H_

#include <memory>
#include <vector>

#include "api/video/i420_buffer.h"
#include "api/video_codecs/video_codec.h"
#include "modules/video_coding/codecs/winhw/h265/include/winhwh265.h"
#include "system_wrappers/include/clock.h"

#include "modules/video_coding/codecs/winhw/libhwcodec.h"
#include "common_video/h265/h265_bitstream_parser.h"

namespace webrtc {

enum class WinHwH265ReconfigOption {
  kBitrate,      // int32_t *value
  kFrameRate,    // float *value
};

enum class WinHwH265RcMode {
  kCBR,
  kABR,
};

enum class WinHwH265ColorFormat { kI420, kNV12 };


typedef struct WinHwH265EncoderParam {
  WinHwH265RcMode     eRCMode;            ///< ineffective when iTemporalLayerNum > 1
  int                 iWidth;
  int                 iHeight;
  int                 iTargetBps;
  int                 iMaxBps;
  int                 iGop;
  float               fMaxFps;
  size_t              iMaxSliceSize;      ///< 0 means no limit
  bool                bEnableFrameSkip;   ///< ineffective when iTemporalLayerNum == 1
  int                 iTemporalLayerNum;  ///< should be within [1,4]
  //int             iSpatialLayerNum; ///< WebRTC has its own solution for simulcast
  int                 hwH265EncListStrategy;
  std::string         hwH265EncAdaptationList;

  int                 bitrateAdjusterType;
  float               minAdjustedBitratePct;
  float               maxAdjustedBitratePct;
  bool                isScreenShare;
} WinHwH265EncoderParam;

typedef struct WinHwH265SourcePicture {
  WinHwH265ColorFormat eColorFormat;   ///< support yuv420 only currently
  int       iStride[4];            ///< stride for each plane pData
  unsigned char*  pData[4];        ///< plane pData
  int       iPicWidth;             ///< luma picture width in x coordinate
  int       iPicHeight;            ///< luma picture height in y coordinate
  long long uiTimeStamp;           ///< timestamp of the source picture, unit: millisecond
} WinHwH265SourcePicture;

typedef struct WinHwH265FrameBitStream {
  bool  isKeyFrame;
  int   iNalCount;              ///< number of NAL coded already
  int*  pNalLengthInByte;       ///< length of NAL size in byte
  unsigned char**  ppBsBuf;     ///< buffer of bitstream contained
  int iFrameSizeInBytes;
  long long uiTimeStamp;
  uint8_t iTemporalLayerId;     ///< layer index
  int iSubSeqId;                ///< frame index in temporal layer
  int             iFrameQp;
  int             iTargetBit;
  int             iTotalBit;
  float           fQpAvgAq;
  int             iCntUnrefP;
  double          frameCost;
  int             iAvelineQp;
	int32_t adjustedEncBitrateBps;
} WinHwH265FrameBitStream;

typedef struct WinHwH265QPThreshold {
	int lowQpThreshold;
	int highQpThreshold;

  WinHwH265QPThreshold()
  {
    lowQpThreshold = netease::kLowH26XQpThreshold;
    highQpThreshold = netease::kHighH26XQpThreshold;
  }
} WinHwH265QPThreshold;

class WindowsHwH265Encoder {
 public:
  WindowsHwH265Encoder();
  ~WindowsHwH265Encoder();

  /**
  * @brief  Initilaize encoder
  * @param  param: parameter for encoder
  * @return 0 - success; otherwise - failed;
  */
  int Initialize(const WinHwH265EncoderParam &param);

  /// uninitialize the encoder
  void Uninitialize();

  /**
  * @brief Encode one frame
  * @param pSrcPic: input source picture
  * @param pBs: output bit stream
  * @return  0 - success; otherwise -failed;
  */
  int EncodeFrame(const WinHwH265SourcePicture* pSrcPic, WinHwH265FrameBitStream* pBs);

  /**
  * @brief  Force encoder to encode frame as IDR frame
  * @return 0 - success; otherwise - failed;
  */
  int ForceIntraFrame();

  /**
  * @brief   Set option for encoder, detail option type, please refer to enumurate WinHwH265ReconfigOption.
  * @param   option: option for encoder such as Frame Rate, Bitrate
  * @param   value: address of option value
  * @return  0 - success; otherwise - failed;
  */
  int SetOption(WinHwH265ReconfigOption option, void* value);

	const char* getEncoderFriendlyName();

  WinHwH265QPThreshold getWinHwH265QPThreshold();

 private:
  bool ConfigParam(const WinHwH265EncoderParam &param);
  void CreateHevcEncoderParams();
  int EncodeFrameHevc(const WinHwH265SourcePicture* pSrcPic, WinHwH265FrameBitStream* pBs);
  void InitFrameBitStream(WinHwH265FrameBitStream* pBs) const;

 private:
  WinHwH265EncoderParam config_;
  bool isInitialized_;
  std::vector<unsigned char *> bsBuf_;
  std::vector<int> nalLens_;
  uint16_t hevcSeqId_;
  bool requestingHevcKeyrame_;
private:
  webrtc::H265BitstreamParser h265_bitstream_parser_;
  netease::VideoEncoderConfig videoEncoderParam;
  netease::HWVideoEncoder *pVideoEncoder;
  netease::VIDEO_CODEC_PROVIDER mVideoCodecProvider;
  int mNoOutputRetryTimes = 0;
};

} // namespace webrtc

#endif
