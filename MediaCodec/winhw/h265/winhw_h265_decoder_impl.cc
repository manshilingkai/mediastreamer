#include "modules/video_coding/codecs/winhw/h265/winhw_h265_decoder_impl.h"

#include <algorithm>
#include <limits>

#include "api/video/color_space.h"
#include "api/video/i420_buffer.h"
#include "common_video/include/video_frame_buffer.h"
#include "rtc_base/checks.h"
#include "rtc_base/criticalsection.h"
#include "rtc_base/keep_ref_until_done.h"
#include "rtc_base/logging.h"
#include "system_wrappers/include/metrics.h"
#include "third_party/libyuv/include/libyuv/planar_functions.h"
#include "third_party/libyuv/include/libyuv/scale.h"
#include "rtc_base/timeutils.h"

#define WIN_HW_H265_DECODE_MAX_NO_OUTPUT_RETRY_TIMES 20
namespace webrtc {

namespace {

// Used by histograms. Values of entries should not be changed.
enum WinHwH265DecoderImplEvent {
  kWinHwH265DecoderEventInit = 0,
  kWinHwH265DecoderEventError = 1,
  kWinHwH265DecoderEventMax = 16,
};

}  // namespace

WinHwH265DecoderImpl::WinHwH265DecoderImpl() : pool_(true, 12),
                                     decoded_image_callback_(nullptr),
                                     has_reported_init_(false),
                                     has_reported_error_(false),
                                     is_initialized_(false) {
  pVideoDecoder = nullptr;
  mVideoCodecProvider = netease::HW_CODEC_AUTOSELECT_HEVC;
  mNoOutputRetryTimes = 0;
}

WinHwH265DecoderImpl::~WinHwH265DecoderImpl() {
  Release();
}

int32_t WinHwH265DecoderImpl::InitDecode(const VideoCodec* codec_settings,
                                    int32_t number_of_cores) {
  ReportInit();
  if (codec_settings &&
      codec_settings->codecType != kVideoCodecH265) {
    ReportError();
    return WEBRTC_VIDEO_CODEC_ERR_PARAMETER;
  }

  int32_t ret = Release();
  if (ret != WEBRTC_VIDEO_CODEC_OK) {
    ReportError();
    return ret;
  }

  ne_parameters_ = (webrtc::EngineParameterCollection*)codec_settings->ne_parameters;
  if(ne_parameters_)
  {
    ne_parameters_->video.h265HWDecAdaptation.Connect(
      std::bind(&WinHwH265DecoderImpl::SetHWH265DecodeAdaptation,
      this, std::placeholders::_1), reinterpret_cast<size_t>(this), true);
  }

  RTC_LOG(LS_INFO) << "codec_settings->width : " << codec_settings->width;
  RTC_LOG(LS_INFO) << "codec_settings->height : " << codec_settings->height;
  h265_bitstream_width = codec_settings->width;
  h265_bitstream_height = codec_settings->height;

  netease::CODEC_RETCODE codec_ret = netease::CreateHWVideoDecoder(&mVideoCodecProvider, &pVideoDecoder, h265_bitstream_width, h265_bitstream_height, hwH265DecListStrategy, hwH265DecAdaptationList);
  if (codec_ret != netease::CODEC_OK) {
      pVideoDecoder = nullptr;
      ReportError();
      return WEBRTC_VIDEO_CODEC_ERROR;
  }

  netease::VideoDecoderConfig config;
  config.width = h265_bitstream_width;
  config.height = h265_bitstream_height;
  pVideoDecoder->setConfig(config);
  bool b_ret = pVideoDecoder->beginDecode();
  if(!b_ret) {
      netease::ReleaseHWVideoDecoder(mVideoCodecProvider, pVideoDecoder);
      pVideoDecoder = nullptr;
      ReportError();
      return WEBRTC_VIDEO_CODEC_ERROR;
  }
    
  is_initialized_ = true;

  return WEBRTC_VIDEO_CODEC_OK;
}

void WinHwH265DecoderImpl::SetHWH265DecodeAdaptation(std::string hwH265DecAdaptation)
{
    RTC_LOG(LS_INFO) << "SetHWH265DecodeAdaptation : " << hwH265DecAdaptation;
    if (!hwH265DecAdaptation.empty())
    {
      char first = hwH265DecAdaptation.at(0);
      if (first=='0' || first=='1') {
        hwH265DecListStrategy = first - '0';
        hwH265DecAdaptationList = hwH265DecAdaptation.substr(3);
      }else {
        hwH265DecListStrategy = 1;
        hwH265DecAdaptationList = "";
      }
    }
    RTC_LOG(LS_INFO) << "hwH265DecListStrategy : " << hwH265DecListStrategy
                     << " hwH265DecAdaptationList : " << hwH265DecAdaptationList;
}

int32_t WinHwH265DecoderImpl::Release() {
  if (is_initialized_) {
      if (pVideoDecoder) {
        netease::ReleaseHWVideoDecoder(mVideoCodecProvider, pVideoDecoder);
        pVideoDecoder = nullptr;
      }

      is_initialized_ = false;
  }

  if(ne_parameters_) {
    ne_parameters_->video.h265HWDecAdaptation.Disconnect(reinterpret_cast<size_t>(this));
  }

  ne_parameters_ = nullptr;
  hwH265DecListStrategy = 0;
  hwH265DecAdaptationList = "";

  mNoOutputRetryTimes = 0;

  return WEBRTC_VIDEO_CODEC_OK;
}

int32_t WinHwH265DecoderImpl::RegisterDecodeCompleteCallback(
    DecodedImageCallback* callback) {
  decoded_image_callback_ = callback;
  return WEBRTC_VIDEO_CODEC_OK;
}

int32_t WinHwH265DecoderImpl::Decode(const EncodedImage& input_image,
                                bool /*missing_frames*/,
                                const CodecSpecificInfo* codec_specific_info,
                                int64_t /*render_time_ms*/) {
  int64_t decode_start_time_ms = rtc::TimeMillis();

  if (!IsInitialized()) {
    ReportError();
    return WEBRTC_VIDEO_CODEC_UNINITIALIZED;
  }

  if (!decoded_image_callback_) {
    RTC_LOG(LS_WARNING)
        << "InitDecode() has been called, but a callback function "
           "has not been set with RegisterDecodeCompleteCallback()";
    ReportError();
    return WEBRTC_VIDEO_CODEC_UNINITIALIZED;
  }

  if (!input_image._buffer || !input_image._length) {
    ReportError();
    return WEBRTC_VIDEO_CODEC_ERR_PARAMETER;
  }

  if (codec_specific_info &&
      codec_specific_info->codecType != kVideoCodecH265) {
    ReportError();
    return WEBRTC_VIDEO_CODEC_ERR_PARAMETER;
  }

  // parse slice qp
  h265_bitstream_parser_.ParseBitstream(input_image._buffer,
                                        input_image._length);
  
  int bitstream_width = 0;
  int bitstream_height = 0;
  bool isResetVideoDecoder = false;
  if(h265_bitstream_parser_.GetLastResolution(&bitstream_width, &bitstream_height) && (bitstream_width!=h265_bitstream_width || bitstream_height!=h265_bitstream_height)) {
    h265_bitstream_width = bitstream_width;
    h265_bitstream_height = bitstream_height;
    isResetVideoDecoder = true;
  }
  bool hasVps = false;
  bool hasSps = false;
  bool hasPps = false;
  bool isIdr = false;
  if (input_image._frameType == kVideoFrameKey && h265_bitstream_parser_.isLastIdr(hasVps, hasSps, hasPps)) {
    isIdr = true;
    if (pVideoDecoder->getCodecGPUName().compare("Intel(R) Iris(R) Xe Graphics")==0) {
      isResetVideoDecoder = true;
    }
  }

  if (isResetVideoDecoder) {
    RTC_LOG(LS_INFO) << "Reset Win Hw H265 Decoder";
    pVideoDecoder->endDecode();
    netease::VideoDecoderConfig config;
    config.width = h265_bitstream_width;
    config.height = h265_bitstream_height;
    pVideoDecoder->setConfig(config);
    bool b_ret = pVideoDecoder->beginDecode();        
    if (!b_ret) {
      RTC_LOG(LS_ERROR) << "failed to reset Win Hw H265 Decoder";
      ReportError();
      return WEBRTC_VIDEO_CODEC_ERROR;
    }
  }

  netease::VideoEncodedFrame encodedFrame;
  encodedFrame.frameData.ptr = input_image._buffer;
  encodedFrame.frameSize = input_image._length;
  encodedFrame.dts = input_image.ntp_time_ms_ * 1000;
  encodedFrame.pts = input_image.ntp_time_ms_ * 1000;

  if (isIdr) {
    encodedFrame.frameType = netease::IDR_Frame;
  }else {
    encodedFrame.frameType = netease::P_Frame;
  }
  bool dec_ret = pVideoDecoder->addEncodedFrame(encodedFrame);
  if(!dec_ret) {
    RTC_LOG(LS_ERROR)
        << "failed to decode";
    ReportError();
    return WEBRTC_VIDEO_CODEC_ERROR;
  }

  netease::VideoRawFrame rawFrame;
  dec_ret = pVideoDecoder->getRawFrame(rawFrame);
  if (!dec_ret)
  {
    mNoOutputRetryTimes++;
    if (mNoOutputRetryTimes > WIN_HW_H265_DECODE_MAX_NO_OUTPUT_RETRY_TIMES)
    {
        RTC_LOG(LS_ERROR) << "no output VideoRawFrame : exceeded maximum number of retries";
        ReportError();
        return WEBRTC_VIDEO_CODEC_ERROR;
    }else if (mNoOutputRetryTimes > 1) {
        RTC_LOG(LS_WARNING) << "no output VideoRawFrame";
        return WEBRTC_VIDEO_CODEC_NO_OUTPUT;
    }else {
        RTC_LOG(LS_WARNING) << "no output VideoRawFrame";
        return WEBRTC_VIDEO_CODEC_OK;
    }
  }

  mNoOutputRetryTimes = 0;

  if (h265_bitstream_width != rawFrame.width || h265_bitstream_height != rawFrame.height)
  {
    RTC_LOG(LS_WARNING) << "Inconsistent width and height -->>" << " h265_bitstream_width : " << h265_bitstream_width << " h265_bitstream_height : " << h265_bitstream_height << " rawFrame.width : " << rawFrame.width << " rawFrame.height : " << rawFrame.height;
  }

  rtc::scoped_refptr<I420Buffer> frame_buffer;
  if (rawFrame.frame_buffer==nullptr)
  {
    frame_buffer = pool_.CreateBuffer(rawFrame.width, rawFrame.height);
    if (frame_buffer == nullptr)
    {
      RTC_LOG(LS_WARNING) << "Drop Frame -->>" << " CreateBuffer Fail With Width:" << rawFrame.width << " Height:" << rawFrame.height;
      // ReportError();
      // return WEBRTC_VIDEO_CODEC_FALLBACK_SOFTWARE;
        return WEBRTC_VIDEO_CODEC_NO_OUTPUT;
    }
  
    if (rawFrame.format == netease::FOURCC_I420)
    {
      libyuv::I420Copy((const uint8_t*)rawFrame.planeptrs[0].ptr, rawFrame.strides[0],
                     (const uint8_t*)rawFrame.planeptrs[1].ptr, rawFrame.strides[1],
                     (const uint8_t*)rawFrame.planeptrs[2].ptr, rawFrame.strides[2],
                     frame_buffer->MutableDataY(), frame_buffer->StrideY(),
                     frame_buffer->MutableDataU(), frame_buffer->StrideU(),
                     frame_buffer->MutableDataV(), frame_buffer->StrideV(),
                     rawFrame.width, rawFrame.height);
    }else if(rawFrame.format == netease::FOURCC_NV12)
    {
      libyuv::NV12ToI420((const uint8_t*)rawFrame.planeptrs[0].ptr, rawFrame.strides[0],
                         (const uint8_t*)rawFrame.planeptrs[1].ptr, rawFrame.strides[1],
                         frame_buffer->MutableDataY(), frame_buffer->StrideY(),
                         frame_buffer->MutableDataU(), frame_buffer->StrideU(),
                         frame_buffer->MutableDataV(), frame_buffer->StrideV(),
                         rawFrame.width, rawFrame.height);
    }
  }else {
    frame_buffer = rawFrame.frame_buffer;
  }

  
  VideoFrame decoded_frame =
      VideoFrame::Builder()
          .set_video_frame_buffer(frame_buffer)
          .set_timestamp_us(input_image.ntp_time_ms_ * 1000)
          .set_timestamp_rtp(input_image.Timestamp())
          .build();

  absl::optional<uint8_t> qp;
  int qp_int;
  if (h265_bitstream_parser_.GetLastSliceQp(&qp_int)) {
      qp.emplace(qp_int);
  }
  
  absl::optional<SeiParser::SeiState>& sei = h265_bitstream_parser_.ParsedSei();
  if (sei && !sei->internal_sei.empty()) {
    decoded_frame.set_sei_internal_data(sei->internal_sei);
  }
  decoded_image_callback_->Decoded(decoded_frame, absl::nullopt, qp);

  int64_t decode_stop_time_ms = rtc::TimeMillis();
  if (decode_stop_time_ms - decode_start_time_ms > 50)
  {
      RTC_LOG(LS_WARNING) << "winhw decode cost time : " << decode_stop_time_ms - decode_start_time_ms;
  }

  return WEBRTC_VIDEO_CODEC_OK;
}

const char* WinHwH265DecoderImpl::ImplementationName() const {
  if (pVideoDecoder) {
    return pVideoDecoder->getCodecFriendlyName();
  } else {
    return "unknown";
  }
}

bool WinHwH265DecoderImpl::IsInitialized() const {
  return is_initialized_;
}

void WinHwH265DecoderImpl::ReportInit() {
  if (has_reported_init_)
    return;
  RTC_HISTOGRAM_ENUMERATION("WebRTC.Video.WinHwH265DecoderImpl.Event",
                            kWinHwH265DecoderEventInit,
                            kWinHwH265DecoderEventMax);
  has_reported_init_ = true;
}

void WinHwH265DecoderImpl::ReportError() {
  if (has_reported_error_)
    return;
  RTC_HISTOGRAM_ENUMERATION("WebRTC.Video.WinHwH265DecoderImpl.Event",
                            kWinHwH265DecoderEventError,
                            kWinHwH265DecoderEventMax);
  has_reported_error_ = true;
}

}  // namespace webrtc
