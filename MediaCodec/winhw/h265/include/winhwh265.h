/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 *
 */

#ifndef MODULES_VIDEO_CODING_CODECS_WINHW_H265_INCLUDE_WINHWH265_H_
#define MODULES_VIDEO_CODING_CODECS_WINHW_H265_INCLUDE_WINHWH265_H_

#include <memory>
#include <vector>

#include "media/base/codec.h"
#include "modules/video_coding/include/video_codec_interface.h"
#include "rtc_base/system/rtc_export.h"

namespace webrtc {

struct SdpVideoFormat;

// Returns a vector with all supported internal H265 profiles that we can
// negotiate in SDP, in order of preference.
std::vector<SdpVideoFormat> SupportedWinHwH265Codecs();

class RTC_EXPORT WinHwH265Encoder : public VideoEncoder {
 public:
  static std::unique_ptr<WinHwH265Encoder> Create(const cricket::VideoCodec& codec);
  static bool IsSupported();
  ~WinHwH265Encoder() override {}
};

class RTC_EXPORT WinHwH265Decoder : public VideoDecoder {
 public:
  static std::unique_ptr<WinHwH265Decoder> Create();
  static bool IsSupported();
  ~WinHwH265Decoder() override {}
};

}  // namespace webrtc

#endif  // MODULES_VIDEO_CODING_CODECS_WINHW_H265_INCLUDE_WINHWH265_H_
