#include "modules/video_coding/codecs/hw/nvidia_jetson/nvidia_jetson_videoencoder.h"
#include "rtc_base/logging.h"

namespace netease {

NvidiaJetsonVideoEncoder::NvidiaJetsonVideoEncoder(int codecId)
{
    m_v4l2_dl = NULL;
    m_nvbufutils_dl = NULL;

    mCodecId = codecId;

    m_bEncoderInited = false;

    m_bForceIntraFrame = false;
	m_bResetBitrate = false;
    m_bResetFramerate = false;

    m_pHWBitrateAdjuster = new HWBitrateAdjuster(0.8, 1.2, false);
    m_encoderBitrateBps = 0;

    m_NvVideoEncoder = NULL;

    index = 0;

    m_device_id = 0;
}

NvidiaJetsonVideoEncoder::~NvidiaJetsonVideoEncoder()
{
    this->endEncode();

    v4l2_free_functions(&m_v4l2Functions, &m_v4l2_dl);
    nvbufutils_free_functions(&m_nvbufUtilsFunctions, &m_nvbufutils_dl);

    delete m_pHWBitrateAdjuster;
}

void NvidiaJetsonVideoEncoder::setEncodedFrameCallback(EncodedFrameCallback *callback)
{
    m_EncodedFrameCallback = callback;
}

// implement VideoEncoder interface
bool NvidiaJetsonVideoEncoder::init()
{
    bool ret = v4l2_load_functions(&m_v4l2Functions, &m_v4l2_dl);
    if (!ret)
    {
        RTC_LOG(LS_ERROR) << "v4l2_load_functions fail";
        return false;
    }

    ret = nvbufutils_load_functions(&m_nvbufUtilsFunctions, &m_nvbufutils_dl);
    if (!ret)
    {
        RTC_LOG(LS_ERROR) << "nvbufutils_load_functions fail";
        return false;
    }
    
    RTC_LOG(LS_INFO) << "init success";
    return true;
}

bool NvidiaJetsonVideoEncoder::setConfig(const VideoEncoderConfig &config)
{
    m_config = config;
    m_pHWBitrateAdjuster->SetTargetBitrateBps(m_config.avgBitrate);
    m_encoderBitrateBps = m_pHWBitrateAdjuster->GetAdjustedBitrateBps();
    return true;
}

bool NvidiaJetsonVideoEncoder::encoder_capture_plane_dq_callback(struct v4l2_buffer *v4l2_buf, NvBuffer * buffer, NvBuffer * shared_buffer, void *arg)
{
    NvidiaJetsonVideoEncoder *thiz = (NvidiaJetsonVideoEncoder *)arg;
    return thiz->do_encoder_capture_plane_dq(v4l2_buf, buffer, shared_buffer);
}
bool NvidiaJetsonVideoEncoder::do_encoder_capture_plane_dq(struct v4l2_buffer *v4l2_buf, NvBuffer * buffer, NvBuffer * shared_buffer)
{
	if (v4l2_buf == NULL)
	{
        RTC_LOG(LS_ERROR) << "Error while dequeing buffer from output plane";
		return false;
	}

	if (buffer->planes[0].bytesused == 0)
	{
        RTC_LOG(LS_ERROR) << "Got 0 size buffer in capture";
		return false;
	}

    VideoEncodedFrame encodedFrame;
    encodedFrame.frameData.ptr = buffer->planes[0].data;
    encodedFrame.frameSize = buffer->planes[0].bytesused;
	uint64_t ts = (v4l2_buf->timestamp.tv_usec % 1000000) + (v4l2_buf->timestamp.tv_sec * 1000000UL);
    encodedFrame.pts = ts;
    encodedFrame.dts = ts;
	v4l2_ctrl_videoenc_outputbuf_metadata enc_metadata;
	this->m_NvVideoEncoder->getMetadata(v4l2_buf->index, enc_metadata);
	if(enc_metadata.KeyFrame){
        encodedFrame.frameType = I_Frame;
	}else{
        encodedFrame.frameType = P_Frame;
	}
    encodedFrame.standard = (mCodecId == HW_CODEC_NVIDIA_JETSON_HEVC) ? VideoStandard_ITU_H265 : VideoStandard_ITU_H264;
    encodedFrame.qp = enc_metadata.AvgQP;

    m_pHWBitrateAdjuster->Update(encodedFrame.frameSize);
    encodedFrame.state = m_pHWBitrateAdjuster->GetBitrateState();

    if (m_EncodedFrameCallback) {
        m_EncodedFrameCallback->OnEncodedFrame(encodedFrame);
    }

    m_OutFrameId++;
	RTC_LOG(LS_INFO) << "Out frame Id : " << m_OutFrameId;

	if (this->m_NvVideoEncoder->capture_plane.qBuffer(*v4l2_buf, NULL) < 0)
	{
        RTC_LOG(LS_ERROR) << "Error while Qing buffer at capture plane";
		return false;
	}

	return true;
}

bool NvidiaJetsonVideoEncoder::beginEncode()
{
    if (m_bEncoderInited) return true;

    std::string enc_name = "enc" + std::to_string(m_device_id);

    //else O_NONBLOCK
    m_NvVideoEncoder = NvVideoEncoder::createVideoEncoder(&m_v4l2Functions, &m_nvbufUtilsFunctions, enc_name.c_str());
    // m_NvVideoEncoder = NvVideoEncoder::createVideoEncoder(&m_v4l2Functions, &m_nvbufUtilsFunctions, "enc0");
    if (m_NvVideoEncoder == NULL)
    {
        RTC_LOG(LS_ERROR) << "NvVideoEncoder::createVideoEncoder Fail";
        return false;
    }

	uint32_t encoder_pixfmt = V4L2_PIX_FMT_H264;
    if (mCodecId == HW_CODEC_NVIDIA_JETSON) {
        encoder_pixfmt=V4L2_PIX_FMT_H264;
    }else if (mCodecId == HW_CODEC_NVIDIA_JETSON_HEVC) {
        encoder_pixfmt=V4L2_PIX_FMT_H265;
    }

    int ret = m_NvVideoEncoder->setCapturePlaneFormat(encoder_pixfmt, m_config.width, m_config.height, NV_JETSON_ENC_CHUNK_SIZE);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setCapturePlaneFormat Fail, Error Code : " << ret;
        return false;
    }

    uint32_t raw_pixfmt = V4L2_PIX_FMT_YUV420M;
    if (m_config.pixFormat == FOURCC_I420)
    {
        raw_pixfmt = V4L2_PIX_FMT_YUV420M;
    }else if (m_config.pixFormat == FOURCC_NV12){
        raw_pixfmt = V4L2_PIX_FMT_NV12;
    }
    
	ret = m_NvVideoEncoder->setOutputPlaneFormat(raw_pixfmt, m_config.width, m_config.height);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setOutputPlaneFormat Fail, Error Code : " << ret;
        return false;
    }
    
    m_encoderBitrateBps = m_pHWBitrateAdjuster->GetAdjustedBitrateBps();
    ret = m_NvVideoEncoder->setBitrate(m_encoderBitrateBps);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setBitrate Fail, Error Code : " << ret;
        return false;
    }
    
	enum v4l2_enc_hw_preset_type hw_preset_type = V4L2_ENC_HW_PRESET_ULTRAFAST;
	ret = m_NvVideoEncoder->setHWPresetType(hw_preset_type);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setHWPresetType Fail, Error Code : " << ret;
    }

    ret = m_NvVideoEncoder->setNumBFrames(m_config.numBFrame);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setNumBFrames Fail, Error Code : " << ret;
        return false;
    }

    ret = m_NvVideoEncoder->setMaxPerfMode(1);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setMaxPerfMode Fail, Error Code : " << ret;
    }

    ret = m_NvVideoEncoder->setNumReferenceFrames(1);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setNumReferenceFrames Fail, Error Code : " << ret;
    }

    if (mCodecId == HW_CODEC_NVIDIA_JETSON) {
        ret = m_NvVideoEncoder->setProfile(V4L2_MPEG_VIDEO_H264_PROFILE_BASELINE);
    }else if (mCodecId == HW_CODEC_NVIDIA_JETSON_HEVC) {
        ret = m_NvVideoEncoder->setProfile(V4L2_MPEG_VIDEO_H265_PROFILE_MAIN);
    }
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setProfile Fail, Error Code : " << ret;
        return false;
    }

    if (mCodecId == HW_CODEC_NVIDIA_JETSON)
    {
		ret = m_NvVideoEncoder->setLevel(V4L2_MPEG_VIDEO_H264_LEVEL_5_1);
        if (ret < 0)
        {
            RTC_LOG(LS_ERROR) << "setLevel Fail, Error Code : " << ret;
        }
    }
    
    enum v4l2_mpeg_video_bitrate_mode ratecontrol = V4L2_MPEG_VIDEO_BITRATE_MODE_CBR;
    ret = m_NvVideoEncoder->setRateControlMode(ratecontrol);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setRateControlMode Fail, Error Code : " << ret;
        return false;
    }

    uint32_t idr_interval = m_config.gop;
    ret = m_NvVideoEncoder->setIDRInterval(idr_interval);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setIDRInterval Fail, Error Code : " << ret;
        return false;
    }
    
    // uint32_t iframe_interval = m_config.gop;
    // ret = m_NvVideoEncoder->setIFrameInterval(iframe_interval);
    // if (ret < 0)
    // {
    //     RTC_LOG(LS_ERROR) << "setIFrameInterval Fail, Error Code : " << ret;
    //     return false;
    // }
    
    ret = m_NvVideoEncoder->setInsertSpsPpsAtIdrEnabled(true);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setInsertSpsPpsAtIdrEnabled Fail, Error Code : " << ret;
        return false;
    }

    ret = m_NvVideoEncoder->setFrameRate(m_config.fps, 1);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setFrameRate Fail, Error Code : " << ret;
        return false;
    }

    ret = m_NvVideoEncoder->output_plane.setupPlane(V4L2_MEMORY_USERPTR, NV_JETSON_ENC_NUM_OUTPUT_BUFFERS, false, true);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setup output plane Fail, Error Code : " << ret;
        return false;
    }

    ret = m_NvVideoEncoder->capture_plane.setupPlane(V4L2_MEMORY_MMAP, NV_JETSON_ENC_NUM_CAPTURE_BUFFERS, true, false);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "setup capture plane Fail, Error Code : " << ret;
        return false;
    }


	ret = m_NvVideoEncoder->output_plane.setStreamStatus(true);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "Error in output plane streamon, Error Code : " << ret;
        return false;
    }

	ret = m_NvVideoEncoder->capture_plane.setStreamStatus(true);
    if (ret < 0)
    {
        RTC_LOG(LS_ERROR) << "Error in capture plane streamon, Error Code : " << ret;
        return false;
    }

	m_NvVideoEncoder->capture_plane.setDQThreadCallback(encoder_capture_plane_dq_callback);
	m_NvVideoEncoder->capture_plane.startDQThread(this);

	// Enqueue all the empty capture plane buffers
	for (uint32_t i = 0; i < m_NvVideoEncoder->capture_plane.getNumBuffers(); i++){
		struct v4l2_buffer v4l2_buf;
		struct v4l2_plane planes[MAX_PLANES];
		memset(&v4l2_buf, 0, sizeof(v4l2_buf));
		memset(planes, 0, MAX_PLANES * sizeof(struct v4l2_plane));

		v4l2_buf.index = i;
		v4l2_buf.m.planes = planes;

		ret = m_NvVideoEncoder->capture_plane.qBuffer(v4l2_buf, NULL);
        if (ret < 0)
        {
            RTC_LOG(LS_ERROR) << "Error while queueing buffer at capture plane, Error Code : " << ret;
            return false;
        }
	}

    m_InFrameId = 0;
    m_OutFrameId = 0;

	RTC_LOG(LS_INFO) << "beginEncode success.";

    m_bEncoderInited = true;

    return true;
}

bool NvidiaJetsonVideoEncoder::flushEncode()
{
    return true;
}

bool NvidiaJetsonVideoEncoder::addRawFrame(const VideoRawFrame &frame)
{
    if (m_NvVideoEncoder->isInError())
    {
        RTC_LOG(LS_ERROR) << "Internal Error Occurred For Nvidia Jetson HW Enc [Maybe Device or resource busy For HW Enc]";

        m_device_id++;
        if (m_device_id < NV_JETSON_ENC_MAX_INSTANCES)
        {
            RTC_LOG(LS_WARNING) << "Fallback To Next Hw Enc : " << m_device_id;
            this->reset();
        }else {
            RTC_LOG(LS_ERROR) << "No Awailable HW Enc Instances, Fallback To SW Enc";
            return false;
        }
    }
    
	int ret;
    if (m_bResetBitrate)
    {
        m_bResetBitrate = false;

        m_encoderBitrateBps = m_pHWBitrateAdjuster->GetAdjustedBitrateBps();
        ret = m_NvVideoEncoder->setBitrate(m_encoderBitrateBps);
        if (ret < 0) {
            RTC_LOG(LS_ERROR) << "setBitrate Fail, Error Code : " << ret;
		}
    }

    if (m_bForceIntraFrame)
    {
        m_bForceIntraFrame = false;

        ret = m_NvVideoEncoder->forceIDR();
        if (ret < 0) {
            RTC_LOG(LS_ERROR) << "forceIDR Fail, Error Code : " << ret;
		}
    }
    
    if (m_bResetFramerate)
    {
        m_bResetFramerate = false;

        ret = m_NvVideoEncoder->setFrameRate(m_config.fps, 1);
        if (ret < 0) {
            RTC_LOG(LS_ERROR) << "setFrameRate Fail, Error Code : " << ret;
        }
    }

	struct v4l2_buffer v4l2_buf;
	struct v4l2_plane planes[MAX_PLANES];
	NvBuffer *nvBuffer;

	memset(&v4l2_buf, 0, sizeof(v4l2_buf));
	memset(planes, 0, sizeof(planes));

	v4l2_buf.m.planes = planes;

	if(this->index < m_NvVideoEncoder->output_plane.getNumBuffers()) {
		nvBuffer=m_NvVideoEncoder->output_plane.getNthBuffer(this->index);
		v4l2_buf.index = this->index ;
		this->index++;
	}else {
		ret = m_NvVideoEncoder->output_plane.dqBuffer(v4l2_buf, &nvBuffer, NULL, -1);
		if (ret < 0) {
            RTC_LOG(LS_ERROR) << "Error DQing buffer at output plane, Error Code : " << ret;
			return false;
		}
	}

	memcpy(nvBuffer->planes[0].data,frame.planeptrs[0].ptr,frame.strides[0]*frame.height);
	memcpy(nvBuffer->planes[1].data,frame.planeptrs[1].ptr,frame.strides[1]*frame.height/2);
	memcpy(nvBuffer->planes[2].data,frame.planeptrs[2].ptr,frame.strides[2]*frame.height/2);
	nvBuffer->planes[0].bytesused=frame.strides[0]*frame.height;
	nvBuffer->planes[1].bytesused=frame.strides[1]*frame.height/2;
	nvBuffer->planes[2].bytesused=frame.strides[2]*frame.height/2;

	v4l2_buf.flags |= V4L2_BUF_FLAG_TIMESTAMP_COPY;
	v4l2_buf.timestamp.tv_usec = (frame.timestamp * 1000) % 1000000;
	v4l2_buf.timestamp.tv_sec = (frame.timestamp * 1000) / 1000000;

	ret = m_NvVideoEncoder->output_plane.qBuffer(v4l2_buf, NULL);
	if (ret < 0) {
        RTC_LOG(LS_ERROR) << "Error while queueing buffer at output plane, Error Code : " << ret;
		return false;
	}

    m_InFrameId++;
	RTC_LOG(LS_INFO) << "In frame Id : " << m_InFrameId;

    return true;
}

bool NvidiaJetsonVideoEncoder::getEncodedFrame(VideoEncodedFrame &frame)
{
    return false;
}

bool NvidiaJetsonVideoEncoder::endEncode()
{
    if (!m_bEncoderInited) return true;

    m_NvVideoEncoder->abort();
    m_NvVideoEncoder->capture_plane.stopDQThread();
	m_NvVideoEncoder->capture_plane.waitForDQThread(1000);
	delete m_NvVideoEncoder;

    m_bEncoderInited = false;

    RTC_LOG(LS_INFO) << "endEncode success.";
    return true;
}

bool NvidiaJetsonVideoEncoder::reset()
{
    this->endEncode();

    index = 0;

    return this->beginEncode();
}

bool NvidiaJetsonVideoEncoder::resetBitrate(uint32_t bitrateBps)
{
    m_pHWBitrateAdjuster->SetTargetBitrateBps(bitrateBps);

    if (m_encoderBitrateBps != m_pHWBitrateAdjuster->GetAdjustedBitrateBps())
	{
		m_bResetBitrate = true;
	}

	return true;
}

bool NvidiaJetsonVideoEncoder::resetFramerate(uint32_t fps)
{
    if (m_config.fps != fps)
    {
        m_config.fps = fps;
        m_bResetFramerate = true;
    }
    
    return true;
}

void NvidiaJetsonVideoEncoder::forceIntraFrame()
{
    m_bForceIntraFrame = true;
}

HWQPThreshold NvidiaJetsonVideoEncoder::getHWQPThreshold()
{
    HWQPThreshold hwQPThreshold;

    if (mCodecId == HW_CODEC_NVIDIA_JETSON_HEVC)
    {
        hwQPThreshold.lowQpThreshold = 24;
        hwQPThreshold.highQpThreshold = 37;
    }else {
        hwQPThreshold.lowQpThreshold = 24;
        hwQPThreshold.highQpThreshold = 37;
    }

	return hwQPThreshold;
}

VIDEO_STATNDARD NvidiaJetsonVideoEncoder::getVideoStandard()
{
    if (mCodecId == HW_CODEC_NVIDIA_JETSON_HEVC)
    {
        return VideoStandard_ITU_H265;
    }else {
        return VideoStandard_ITU_H264;
    }
}

VIDEO_CODEC_PROVIDER NvidiaJetsonVideoEncoder::getCodecId()
{
	return (VIDEO_CODEC_PROVIDER)mCodecId;
}

const char* NvidiaJetsonVideoEncoder::getCodecFriendlyName()
{
    if (mCodecId == HW_CODEC_NVIDIA_JETSON_HEVC)
    {
        return "Nvidia Jetson H265 Encoder";
    }else {
        return "Nvidia Jetson H264 Encoder";
    }
}

CODEC_RETCODE CreateNvidiaJetsonVideoEncoder(VIDEO_CODEC_PROVIDER codec, HWVideoEncoder** ppVideoEncoder)
{
    RTC_LOG(LS_INFO) << "CODEC_RETCODE CreateNvidiaJetsonVideoEncoder";

    NvidiaJetsonVideoEncoder* encoder = new NvidiaJetsonVideoEncoder(codec);
    if (encoder && encoder->init())
    {
        *ppVideoEncoder = encoder;
        return CODEC_OK;
    }
    
    delete encoder;
	return CODEC_ERR_UNKNOWN;
}

} // namespace netease