#ifndef MODULES_VIDEO_CODING_CODECS_HW_NVIDIA_JETSON_NVIDIA_JETSON_VIDEO_ENCODER_H_
#define MODULES_VIDEO_CODING_CODECS_HW_NVIDIA_JETSON_NVIDIA_JETSON_VIDEO_ENCODER_H_

#include "modules/video_coding/codecs/hw/libhwcodec.h"
#include "modules/video_coding/codecs/hw/nvidia_jetson/jetson_multimedia_api/include/NvVideoEncoder.h"
#include "modules/video_coding/codecs/hw/common/hw_bitrate_adjuster.h"
#include "modules/video_coding/codecs/hw/nvidia_jetson/jetson_multimedia_api/include/libv4l2.h"
#include "modules/video_coding/codecs/hw/nvidia_jetson/jetson_multimedia_api/include/nvbuf_utils.h"

#include <queue>

namespace netease {

#define NV_JETSON_ENC_NUM_CAPTURE_BUFFERS 10
#define NV_JETSON_ENC_NUM_OUTPUT_BUFFERS 10
#define NV_JETSON_ENC_CHUNK_SIZE 2*1024*1024
#define NV_JETSON_ENC_MAX_INSTANCES 4

class NvidiaJetsonVideoEncoder: public netease::HWVideoEncoder
{
public:
	NvidiaJetsonVideoEncoder(int codecId);
	~NvidiaJetsonVideoEncoder();

    void setEncodedFrameCallback(EncodedFrameCallback *callback);

	// implement VideoEncoder interface
	bool init();
	bool setConfig(const VideoEncoderConfig &config);
	bool beginEncode();
	bool flushEncode();
	bool addRawFrame(const VideoRawFrame &frame);
	bool getEncodedFrame(VideoEncodedFrame &frame);
	bool endEncode();
	bool reset();
	bool resetBitrate(uint32_t bitrateBps);
	bool resetFramerate(uint32_t fps);
	void forceIntraFrame();
	HWQPThreshold getHWQPThreshold();
	VIDEO_STATNDARD getVideoStandard();
	VIDEO_CODEC_PROVIDER getCodecId();
	const char* getCodecFriendlyName();
private:
    static bool encoder_capture_plane_dq_callback(struct v4l2_buffer *v4l2_buf, NvBuffer * buffer, NvBuffer * shared_buffer, void *arg);
	bool do_encoder_capture_plane_dq(struct v4l2_buffer *v4l2_buf, NvBuffer * buffer, NvBuffer * shared_buffer);
private:
    V4l2Functions m_v4l2Functions;
	void *m_v4l2_dl;
	NvbufUtilsFunctions m_nvbufUtilsFunctions;
	void *m_nvbufutils_dl;
private:
	NvVideoEncoder *m_NvVideoEncoder;
	int index;
private:
    int mCodecId;
	VideoEncoderConfig m_config;
	bool m_bEncoderInited;

	bool m_bForceIntraFrame;
	bool m_bResetBitrate;
	bool m_bResetFramerate;

	HWBitrateAdjuster *m_pHWBitrateAdjuster;
	uint32_t m_encoderBitrateBps;
private:
    int64_t m_InFrameId;
	int64_t m_OutFrameId;
private:
    int m_device_id;
private:
    EncodedFrameCallback *m_EncodedFrameCallback = nullptr;
};

CODEC_RETCODE CreateNvidiaJetsonVideoEncoder(VIDEO_CODEC_PROVIDER codec, HWVideoEncoder** ppVideoEncoder);


} // namespace netease

#endif