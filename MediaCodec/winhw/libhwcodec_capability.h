#pragma once
#include <stdint.h>

namespace netease {

struct HWVideoCodecCapability
{
    bool isSupportH264Encode;
    bool isSupportH264Decode;
    bool isSupportH265Encode;
    bool isSupportH265Decode;

    // if < 0 : unknown
    int supportedMaxInstanceForH264Encode;
    int supportedMaxInstanceForH264Decode;
    int supportedMaxInstanceForH265Encode;
    int supportedMaxInstanceForH265Decode;

    // if < 0 : unknown
    int minSupportedWidthForH264Encode;
    int minSupportedHeightForH264Encode;
    int maxSupportedWidthForH264Encode;
    int maxSupportedHeightForH264Encode;

    int minSupportedWidthForH264Decode;
    int minSupportedHeightForH264Decode;
    int maxSupportedWidthForH264Decode;
    int maxSupportedHeightForH264Decode;

    int minSupportedWidthForH265Encode;
    int minSupportedHeightForH265Encode;
    int maxSupportedWidthForH265Encode;
    int maxSupportedHeightForH265Encode;

    int minSupportedWidthForH265Decode;
    int minSupportedHeightForH265Decode;
    int maxSupportedWidthForH265Decode;
    int maxSupportedHeightForH265Decode;

    // if < 0 : unknown [for encode]
    int64_t maxEncodeBitrateBpsForH264;
    int64_t maxEncodeBitrateBpsForH265;

    // if < 0 : unknown [for encode]
    int supportedMaxLTRFramesForH264;
    int supportedMaxLTRFramesForH265;

    // if < 0 : unknown [for encode]
    int supportedMaxTemporalLayersForH264;
    int supportedMaxTemporalLayersForH265;

    HWVideoCodecCapability()
    {
        isSupportH264Encode = false;
        isSupportH264Decode = false;
        isSupportH265Encode = false;
        isSupportH265Decode = false;

        supportedMaxInstanceForH264Encode = -1;
        supportedMaxInstanceForH264Decode = -1;
        supportedMaxInstanceForH265Encode = -1;
        supportedMaxInstanceForH265Decode = -1;

        minSupportedWidthForH264Encode = -1;
        minSupportedHeightForH264Encode = -1;
        maxSupportedWidthForH264Encode = -1;
        maxSupportedHeightForH264Encode = -1;

        minSupportedWidthForH264Decode = -1;
        minSupportedHeightForH264Decode = -1;
        maxSupportedWidthForH264Decode = -1;
        maxSupportedHeightForH264Decode = -1;

        minSupportedWidthForH265Encode = -1;
        minSupportedHeightForH265Encode = -1;
        maxSupportedWidthForH265Encode = -1;
        maxSupportedHeightForH265Encode = -1;

        minSupportedWidthForH265Decode = -1;
        minSupportedHeightForH265Decode = -1;
        maxSupportedWidthForH265Decode = -1;
        maxSupportedHeightForH265Decode = -1;

        maxEncodeBitrateBpsForH264 = -1;
        maxEncodeBitrateBpsForH265 = -1;

        supportedMaxLTRFramesForH264 = -1;
        supportedMaxLTRFramesForH265 = -1;

        supportedMaxTemporalLayersForH264 = -1;
        supportedMaxTemporalLayersForH265 = -1;
    }
};

HWVideoCodecCapability GetWinHWVideoCodecCapability();

} // namespace netease