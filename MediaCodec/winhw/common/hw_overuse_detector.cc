#include "modules/video_coding/codecs/winhw/common/hw_overuse_detector.h"
#include "rtc_base/logging.h"

namespace netease {

const float HWOverUseDetector::kSensitivity = 0.99f;
const float HWOverUseDetector::kOverUseThreshold = 200.0f; 

HWOverUseDetector::HWOverUseDetector()
{
    averageOneFrameCostTimeMs = 0.0f;
}

void HWOverUseDetector::reportSample(float oneFrameCostTimeMs)
{
    averageOneFrameCostTimeMs = averageOneFrameCostTimeMs * kSensitivity + (1.0f - kSensitivity) * oneFrameCostTimeMs;
}

bool HWOverUseDetector::isHWOverUse()
{
    return averageOneFrameCostTimeMs > kOverUseThreshold;
}

} // namespace netease