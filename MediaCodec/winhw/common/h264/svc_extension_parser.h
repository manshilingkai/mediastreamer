/* 
 * Created by Shilingkai in 2023-10
 * Copyright (c) 2023 NETEASE RTC. All rights reserved.
 */

#ifndef COMMON_VIDEO_H264_SVC_EXTENSION_PARSER_H_
#define COMMON_VIDEO_H264_SVC_EXTENSION_PARSER_H_

#include "absl/types/optional.h"

namespace rtc {
class BitBuffer;
}

namespace webrtc {

// A class for parsing out svc extension data from an H264 NALU.
class SvcExtensionParser {
 public:
  // The parsed state of the svc extension. Only some select values are stored.
  // Add more as they are actually needed.
  struct SvcExtensionState {
    SvcExtensionState();
    SvcExtensionState(const SvcExtensionState&);
    ~SvcExtensionState();
    
    uint32_t svc_extension_flag = 0;
    
    uint32_t idr_flag = 0;
    uint32_t priority_id = 0;
    uint32_t no_inter_layer_pred_flag = 0;
    uint32_t dependency_id = 0;
    uint32_t quality_id = 0;
    uint32_t temporal_id = 0;
    uint32_t use_ref_base_pic_flag = 0;
    uint32_t discardable_flag = 0;
    uint32_t output_flag = 0;
    uint32_t reserved_three_2bits = 0;
  };

  // Unpack RBSP and parse svc extension state from the supplied buffer.
  static absl::optional<SvcExtensionState> ParseSvcExtension(const uint8_t* data, size_t length);

 protected:
  // Parse the svc extension state, for a bit buffer where RBSP decoding has already been
  // performed.
  static absl::optional<SvcExtensionState> ParseSvcExtensionInternal(rtc::BitBuffer* bit_buffer);
};

}  // namespace webrtc

#endif