/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */
#include "common_video/h264/h264_bitstream_parser.h"

#include <stdlib.h>
#include <cstdint>
#include <vector>

#include "rtc_base/bitbuffer.h"
#include "rtc_base/logging.h"

namespace {
const int kMaxAbsQpDeltaValue = 51;
const int kMinQpValue = 0;
const int kMaxQpValue = 51;
}  // namespace

namespace webrtc {

#define RETURN_ON_FAIL(x, res)            \
  if (!(x)) {                             \
    RTC_LOG_F(LS_ERROR) << "FAILED: " #x; \
    return res;                           \
  }

#define RETURN_INV_ON_FAIL(x) RETURN_ON_FAIL(x, kInvalidStream)

H264BitstreamParser::H264BitstreamParser() {}
H264BitstreamParser::~H264BitstreamParser() {
    if (last_sps_data)
    {
        delete [] last_sps_data;
        last_sps_data = NULL;
    }
    if (last_pps_data)
    {
      delete [] last_pps_data;
      last_pps_data = NULL;
    }
}


bool H264BitstreamParser::IsFirstSliceInFrame(
    const uint8_t* source,
    size_t source_length) {

  const std::vector<uint8_t> slice_rbsp =
      H264::ParseRbsp(source, source_length);
  if (slice_rbsp.size() < H264::kNaluTypeSize) {
    RTC_LOG(LS_ERROR) << "slice_rbsp size error  size="<<slice_rbsp.size();
    return false;
  }
    
  rtc::BitBuffer slice_reader(slice_rbsp.data() + H264::kNaluTypeSize,
                              slice_rbsp.size() - H264::kNaluTypeSize);

  uint32_t golomb_tmp = 0;
  if(!slice_reader.ReadExponentialGolomb(&golomb_tmp)) {
    RTC_LOG(LS_ERROR) << "ReadExponentialGolomb golomb error";
    return false;
  }

  //RTC_LOG(LS_INFO) << "IsFirstSliceInFrame golomb_tmp="<<golomb_tmp;
  return (golomb_tmp == 0);
}

H264BitstreamParser::Result H264BitstreamParser::h264_pred_weight_table(rtc::BitBuffer& slice_reader,uint32_t* ref_count, uint32_t slice_type, H264PredWeightTable *pwt)
{
    uint32_t i;
    int list;
    int luma_def, chroma_def;

    pwt->use_weight             = 0;
    pwt->use_weight_chroma      = 0;

    uint32_t luma_log2_weight_denom = 0;
    RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&luma_log2_weight_denom));
    pwt->luma_log2_weight_denom = luma_log2_weight_denom;
    if (pwt->luma_log2_weight_denom > 7U) {
        RTC_LOG(LS_ERROR) << "luma_log2_weight_denom " << pwt->luma_log2_weight_denom << " is out of range";
        pwt->luma_log2_weight_denom = 0;
    }
    luma_def = 1 << pwt->luma_log2_weight_denom;

    if (sps_->chroma_format_idc) {
        uint32_t chroma_log2_weight_denom = 0;
        RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&chroma_log2_weight_denom));
        pwt->chroma_log2_weight_denom = chroma_log2_weight_denom;
        if (pwt->chroma_log2_weight_denom > 7U) {
            RTC_LOG(LS_ERROR) << "chroma_log2_weight_denom " << pwt->chroma_log2_weight_denom << " is out of range";
            pwt->chroma_log2_weight_denom = 0;
        }
        chroma_def = 1 << pwt->chroma_log2_weight_denom;
    }

    for (list = 0; list < 2; list++) {
        pwt->luma_weight_flag[list]   = 0;
        pwt->chroma_weight_flag[list] = 0;
        for (i = 0; i < ref_count[list]; i++) {
            uint32_t luma_weight_flag, chroma_weight_flag;

            RETURN_INV_ON_FAIL(slice_reader.ReadBits(&luma_weight_flag, 1));
            if (luma_weight_flag) {
                int32_t golomb_tmp;
                RETURN_INV_ON_FAIL(slice_reader.ReadSignedExponentialGolomb(&golomb_tmp));
                pwt->luma_weight[i][list][0] = golomb_tmp;
                RETURN_INV_ON_FAIL(slice_reader.ReadSignedExponentialGolomb(&golomb_tmp));
                pwt->luma_weight[i][list][1] = golomb_tmp;
                if ((int8_t)pwt->luma_weight[i][list][0] != pwt->luma_weight[i][list][0] ||
                    (int8_t)pwt->luma_weight[i][list][1] != pwt->luma_weight[i][list][1]) {
                  RTC_LOG(LS_ERROR) << "Out of range weight";
                  return kUnsupportedStream;
                }
                if (pwt->luma_weight[i][list][0] != luma_def ||
                    pwt->luma_weight[i][list][1] != 0) {
                    pwt->use_weight             = 1;
                    pwt->luma_weight_flag[list] = 1;
                }
            } else {
                pwt->luma_weight[i][list][0] = luma_def;
                pwt->luma_weight[i][list][1] = 0;
            }

            if (sps_->chroma_format_idc) {
                RETURN_INV_ON_FAIL(slice_reader.ReadBits(&chroma_weight_flag, 1));
                if (chroma_weight_flag) {
                    int j;
                    for (j = 0; j < 2; j++) {
                        int32_t golomb_tmp;
                        RETURN_INV_ON_FAIL(slice_reader.ReadSignedExponentialGolomb(&golomb_tmp));
                        pwt->chroma_weight[i][list][j][0] = golomb_tmp;
                        RETURN_INV_ON_FAIL(slice_reader.ReadSignedExponentialGolomb(&golomb_tmp));
                        pwt->chroma_weight[i][list][j][1] = golomb_tmp;
                        if ((int8_t)pwt->chroma_weight[i][list][j][0] != pwt->chroma_weight[i][list][j][0] ||
                            (int8_t)pwt->chroma_weight[i][list][j][1] != pwt->chroma_weight[i][list][j][1]) {
                            pwt->chroma_weight[i][list][j][0] = chroma_def;
                            pwt->chroma_weight[i][list][j][1] = 0;
                            RTC_LOG(LS_ERROR) << "Out of range weight";
                            return kUnsupportedStream;
                        }
                        if (pwt->chroma_weight[i][list][j][0] != chroma_def ||
                            pwt->chroma_weight[i][list][j][1] != 0) {
                            pwt->use_weight_chroma        = 1;
                            pwt->chroma_weight_flag[list] = 1;
                        }
                    }
                } else {
                    int j;
                    for (j = 0; j < 2; j++) {
                        pwt->chroma_weight[i][list][j][0] = chroma_def;
                        pwt->chroma_weight[i][list][j][1] = 0;
                    }
                }
            }
        }
        if (slice_type != H264::SliceType::kB)
            break;
    }
    pwt->use_weight = pwt->use_weight || pwt->use_weight_chroma;
    return kOk;
}

H264BitstreamParser::Result H264BitstreamParser::ParseNonParameterSetNalu(
    const uint8_t* source,
    size_t source_length,
    uint8_t nalu_type) {
  if (!sps_ || !pps_)
    return kInvalidStream;

  if (nalu_type >=13 && nalu_type <= 23) {
    if (print) {
      RTC_LOG(LS_WARNING) << "Unsupported nal unit type : " << nalu_type;
    }
    return kOk;
  }

  last_slice_qp_delta_ = absl::nullopt;
  const std::vector<uint8_t> slice_rbsp =
      H264::ParseRbsp(source, source_length);
  if (slice_rbsp.size() < H264::kNaluTypeSize)
    return kInvalidStream;

  rtc::BitBuffer slice_reader(slice_rbsp.data() + H264::kNaluTypeSize,
                              slice_rbsp.size() - H264::kNaluTypeSize);
  // Check to see if this is an IDR slice, which has an extra field to parse
  // out.
  bool is_idr = (source[0] & 0x0F) == H264::NaluType::kIdr;
  uint8_t nal_ref_idc = (source[0] & 0x60) >> 5;
  uint32_t golomb_tmp;
  uint32_t bits_tmp;
  uint32_t ref_count[2] = {0,0};

  ref_count[0] = pps_->num_ref_idx_l0_active_minus1+1;
  ref_count[1] = pps_->num_ref_idx_l1_active_minus1+1;
  
  if (print) {
    RTC_LOG(LS_INFO) << "nal_ref_idc : " << nal_ref_idc;
  }

  // first_mb_in_slice: ue(v)
  RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
  if (print) {
    RTC_LOG(LS_INFO) << "first_mb_in_slice : " << golomb_tmp;
  }

  // slice_type: ue(v)
  uint32_t slice_type;
  RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&slice_type));
  if (print) {
    RTC_LOG(LS_INFO) << "slice_type : " << slice_type;
  }

  if (slice_type >= 10) {
    if (print) {
      RTC_LOG(LS_WARNING) << "unknown slice_type : " << slice_type;
    }
    return kOk;
  }
  
  // slice_type's 5..9 range is used to indicate that all slices of a picture
  // have the same value of slice_type % 5, we don't care about that, so we map
  // to the corresponding 0..4 range.
  slice_type %= 5;
  // pic_parameter_set_id: ue(v)
  RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
  if (print) {
    RTC_LOG(LS_INFO) << "pic_parameter_set_id : " << golomb_tmp;
  }
  if (sps_->separate_colour_plane_flag == 1) {
    // colour_plane_id
    RETURN_INV_ON_FAIL(slice_reader.ReadBits(&bits_tmp, 2));
    if (print) {
      RTC_LOG(LS_INFO) << "colour_plane_id : " << bits_tmp;
    }
  }
  // frame_num: u(v)
  // Represented by log2_max_frame_num bits.
  RETURN_INV_ON_FAIL(
      slice_reader.ReadBits(&bits_tmp, sps_->log2_max_frame_num));
  if (print) {
    RTC_LOG(LS_INFO) << "frame_num : " << bits_tmp
                     << " log2_max_frame_num : " << sps_->log2_max_frame_num;
  }
#if HAVE_NEVC
  if(sps_->enable_nevc)
  {
    uint32_t bChanged, bframeSr;
    RETURN_INV_ON_FAIL(
        slice_reader.ReadBits(&bChanged, 1));
    RETURN_INV_ON_FAIL(
        slice_reader.ReadBits(&bframeSr, 1));
    if(bChanged) {
        RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
        RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
        if(bframeSr) {
            RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
            RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
        }
    }
  }
#endif     
  uint32_t field_pic_flag = 0;
  if (sps_->frame_mbs_only_flag == 0) {
    // field_pic_flag: u(1)
    RETURN_INV_ON_FAIL(slice_reader.ReadBits(&field_pic_flag, 1));
    if (print) {
      RTC_LOG(LS_INFO) << "field_pic_flag : " << field_pic_flag;
    }
    if (field_pic_flag != 0) {
      // bottom_field_flag: u(1)
      RETURN_INV_ON_FAIL(slice_reader.ReadBits(&bits_tmp, 1));
      if (print) {
        RTC_LOG(LS_INFO) << "bottom_field_flag : " << bits_tmp;
      }
    }
  }
  if (is_idr) {
    // idr_pic_id: ue(v)
    RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
    if (print) {
      RTC_LOG(LS_INFO) << "idr_pic_id : " << golomb_tmp;
    }
  }
  // pic_order_cnt_lsb: u(v)
  // Represented by sps_.log2_max_pic_order_cnt_lsb bits.
  if (sps_->pic_order_cnt_type == 0) {
    RETURN_INV_ON_FAIL(
        slice_reader.ReadBits(&bits_tmp, sps_->log2_max_pic_order_cnt_lsb));
    if (print) {
      RTC_LOG(LS_INFO) << "pic_order_cnt_lsb : " << bits_tmp;
    }
    if (pps_->bottom_field_pic_order_in_frame_present_flag &&
        field_pic_flag == 0) {
      // delta_pic_order_cnt_bottom: se(v)
      RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
      if (print) {
        RTC_LOG(LS_INFO) << "delta_pic_order_cnt_bottom : " << golomb_tmp;
      }
    }
  }
  if (sps_->pic_order_cnt_type == 1 &&
      !sps_->delta_pic_order_always_zero_flag) {
    // delta_pic_order_cnt[0]: se(v)
    RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
    if (print) {
      RTC_LOG(LS_INFO) << "delta_pic_order_cnt[0] : " << golomb_tmp;
    }
    if (pps_->bottom_field_pic_order_in_frame_present_flag && !field_pic_flag) {
      // delta_pic_order_cnt[1]: se(v)
      RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
      if (print) {
        RTC_LOG(LS_INFO) << "delta_pic_order_cnt[1] : " << golomb_tmp;
      }
    }
  }
  if (pps_->redundant_pic_cnt_present_flag) {
    // redundant_pic_cnt: ue(v)
    RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
    if (print) {
      RTC_LOG(LS_INFO) << "redundant_pic_cnt : " << golomb_tmp;
    }
  }
  if (slice_type == H264::SliceType::kB) {
    // direct_spatial_mv_pred_flag: u(1)
    RETURN_INV_ON_FAIL(slice_reader.ReadBits(&bits_tmp, 1));
    if (print) {
      RTC_LOG(LS_INFO) << "direct_spatial_mv_pred_flag : " << bits_tmp;
    }
  }
  switch (slice_type) {
    case H264::SliceType::kP:
    case H264::SliceType::kB:
    case H264::SliceType::kSp:
      uint32_t num_ref_idx_active_override_flag;
      // num_ref_idx_active_override_flag: u(1)
      RETURN_INV_ON_FAIL(
          slice_reader.ReadBits(&num_ref_idx_active_override_flag, 1));
      if (print) {
        RTC_LOG(LS_INFO) << "num_ref_idx_active_override_flag : " << num_ref_idx_active_override_flag;
      }
      if (num_ref_idx_active_override_flag != 0) {
        // num_ref_idx_l0_active_minus1: ue(v)
        RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
        if (print) {
          RTC_LOG(LS_INFO) << "num_ref_idx_l0_active_minus1 : " << golomb_tmp;
        }
        ref_count[0] = golomb_tmp + 1;
        if (slice_type == H264::SliceType::kB) {
          // num_ref_idx_l1_active_minus1: ue(v)
          RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
          if (print) {
            RTC_LOG(LS_INFO) << "num_ref_idx_l1_active_minus1 : " << ref_count[1];
          }
          ref_count[1] = golomb_tmp + 1;
        }else {
          ref_count[1] = 1;
        }
      }
      break;
    default:
      ref_count[0] = ref_count[1] = 0;
      break;
  }
  // assume nal_unit_type != 20 && nal_unit_type != 21:
  // if (nalu_type == 20 || nalu_type == 21) {
  //   RTC_LOG(LS_ERROR) << "Unsupported nal unit type.";
  //   return kUnsupportedStream;
  // }
  // if (nal_unit_type == 20 || nal_unit_type == 21)
  //   ref_pic_list_mvc_modification()
  // else
  {
    // ref_pic_list_modification():
    // |slice_type| checks here don't use named constants as they aren't named
    // in the spec for this segment. Keeping them consistent makes it easier to
    // verify that they are both the same.
    if (slice_type % 5 != 2 && slice_type % 5 != 4) {
      // ref_pic_list_modification_flag_l0: u(1)
      uint32_t ref_pic_list_modification_flag_l0;
      RETURN_INV_ON_FAIL(
          slice_reader.ReadBits(&ref_pic_list_modification_flag_l0, 1));
      if (print) {
        RTC_LOG(LS_INFO) << "ref_pic_list_modification_flag_l0 : " << ref_pic_list_modification_flag_l0;
      }
      if (ref_pic_list_modification_flag_l0) {
        uint32_t modification_of_pic_nums_idc;
        do {
          // modification_of_pic_nums_idc: ue(v)
          RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(
              &modification_of_pic_nums_idc));
          if (print) {
            RTC_LOG(LS_INFO) << "modification_of_pic_nums_idc : " << modification_of_pic_nums_idc;
          }
          if (modification_of_pic_nums_idc == 0 ||
              modification_of_pic_nums_idc == 1) {
            // abs_diff_pic_num_minus1: ue(v)
            RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
            if (print) {
              RTC_LOG(LS_INFO) << "abs_diff_pic_num_minus1 : " << golomb_tmp;
            }
          } else if (modification_of_pic_nums_idc == 2) {
            // long_term_pic_num: ue(v)
            RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
            if (print) {
              RTC_LOG(LS_INFO) << "long_term_pic_num : " << golomb_tmp;
            }
          }
        } while (modification_of_pic_nums_idc != 3);
      }
    }
    if (slice_type % 5 == 1) {
      // ref_pic_list_modification_flag_l1: u(1)
      uint32_t ref_pic_list_modification_flag_l1;
      RETURN_INV_ON_FAIL(
          slice_reader.ReadBits(&ref_pic_list_modification_flag_l1, 1));
      if (print) {
        RTC_LOG(LS_INFO) << "ref_pic_list_modification_flag_l1 : " << ref_pic_list_modification_flag_l1;
      }
      if (ref_pic_list_modification_flag_l1) {
        uint32_t modification_of_pic_nums_idc;
        do {
          // modification_of_pic_nums_idc: ue(v)
          RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(
              &modification_of_pic_nums_idc));
          if (print) {
            RTC_LOG(LS_INFO) << "modification_of_pic_nums_idc : " << modification_of_pic_nums_idc;
          }
          if (modification_of_pic_nums_idc == 0 ||
              modification_of_pic_nums_idc == 1) {
            // abs_diff_pic_num_minus1: ue(v)
            RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
            if (print) {
              RTC_LOG(LS_INFO) << "abs_diff_pic_num_minus1 : " << golomb_tmp;
            }
          } else if (modification_of_pic_nums_idc == 2) {
            // long_term_pic_num: ue(v)
            RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
            if (print) {
              RTC_LOG(LS_INFO) << "long_term_pic_num : " << golomb_tmp;
            }
          }
        } while (modification_of_pic_nums_idc != 3);
      }
    }
  }
  // TODO(pbos): Do we need support for pred_weight_table()?
  if ((pps_->weighted_pred_flag && (slice_type == H264::SliceType::kP ||
                                    slice_type == H264::SliceType::kSp)) ||
      (pps_->weighted_bipred_idc == 1 && slice_type == H264::SliceType::kB)) {
        H264PredWeightTable pwt;
        if(h264_pred_weight_table(slice_reader,ref_count, slice_type, &pwt) != kOk)
        {
            RTC_LOG(LS_ERROR) << "Streams with pred_weight_table unsupported.";
            return kUnsupportedStream;
        }else {
            if (print) {
              RTC_LOG(LS_INFO) << "pred_weight_table pass";
            }
        }
  }
  // if ((weighted_pred_flag && (slice_type == P || slice_type == SP)) ||
  //    (weighted_bipred_idc == 1 && slice_type == B)) {
  //  pred_weight_table()
  // }
  if (nal_ref_idc != 0) {
    // dec_ref_pic_marking():
    if (is_idr) {
      // no_output_of_prior_pics_flag: u(1)
      // long_term_reference_flag: u(1)
      RETURN_INV_ON_FAIL(slice_reader.ReadBits(&bits_tmp, 2));
      if (print) {
        RTC_LOG(LS_INFO) << "no_output_of_prior_pics_flag & long_term_reference_flag : " << bits_tmp;
      }
    } else {
      // adaptive_ref_pic_marking_mode_flag: u(1)
      uint32_t adaptive_ref_pic_marking_mode_flag;
      RETURN_INV_ON_FAIL(
          slice_reader.ReadBits(&adaptive_ref_pic_marking_mode_flag, 1));
      if (print) {
        RTC_LOG(LS_INFO) << "adaptive_ref_pic_marking_mode_flag : " << adaptive_ref_pic_marking_mode_flag;
      }
      if (adaptive_ref_pic_marking_mode_flag) {
        uint32_t memory_management_control_operation;
        do {
          // memory_management_control_operation: ue(v)
          RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(
              &memory_management_control_operation));
          if (print) {
            RTC_LOG(LS_INFO) << "memory_management_control_operation : " << memory_management_control_operation;
          }
          if (memory_management_control_operation == 1 ||
              memory_management_control_operation == 3) {
            // difference_of_pic_nums_minus1: ue(v)
            RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
            if (print) {
              RTC_LOG(LS_INFO) << "difference_of_pic_nums_minus1 : " << golomb_tmp;
            }
          }
          if (memory_management_control_operation == 2) {
            // long_term_pic_num: ue(v)
            RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
            if (print) {
              RTC_LOG(LS_INFO) << "long_term_pic_num : " << golomb_tmp;
            }
          }
          if (memory_management_control_operation == 3 ||
              memory_management_control_operation == 6) {
            // long_term_frame_idx: ue(v)
            RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
            if (print) {
              RTC_LOG(LS_INFO) << "long_term_frame_idx : " << golomb_tmp;
            }
          }
          if (memory_management_control_operation == 4) {
            // max_long_term_frame_idx_plus1: ue(v)
            RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
            if (print) {
              RTC_LOG(LS_INFO) << "max_long_term_frame_idx_plus1 : " << golomb_tmp;
            }
          }
        } while (memory_management_control_operation != 0);
      }
    }
  }
  if (pps_->entropy_coding_mode_flag && slice_type != H264::SliceType::kI &&
      slice_type != H264::SliceType::kSi) {
    // cabac_init_idc: ue(v)
    RETURN_INV_ON_FAIL(slice_reader.ReadExponentialGolomb(&golomb_tmp));
    if (print) {
      RTC_LOG(LS_INFO) << "cabac_init_idc : " << golomb_tmp;
    }
  }

  int32_t last_slice_qp_delta;
  RETURN_INV_ON_FAIL(
      slice_reader.ReadSignedExponentialGolomb(&last_slice_qp_delta));
  if (print) {
    RTC_LOG(LS_INFO) << "last_slice_qp_delta : " << last_slice_qp_delta;
  }
  if (abs(last_slice_qp_delta) > kMaxAbsQpDeltaValue) {
    // Something has gone wrong, and the parsed value is invalid.
    RTC_LOG(LS_WARNING) << "Parsed QP value out of range.";
    return kInvalidStream;
  }

  last_slice_qp_delta_ = last_slice_qp_delta;
  return kOk;
}

// NOTE(Think): what if we have two sps or pps data ? 
// probably bug here especially for some hardware encoder
H264BitstreamParser::Result H264BitstreamParser::ParseSlice(
                                      const uint8_t* slice,
                                      size_t length,
                                      webrtc::SeiParser::SeiState &sei,
                                      const uint8_t* nal, 
                                      size_t nal_size, bool backup_spspps,
                                      bool *discardable_flag,
                                      H264::BufferCopy* buffer) {
  *discardable_flag = false;
  Result res = kOk;
  H264::NaluType nalu_type = H264::ParseNaluType(slice[0]);
  if (print) {
    RTC_LOG(LS_INFO) << "nalu_type : " << nalu_type;
  }
  switch (nalu_type) {
    case H264::NaluType::kSps: {
      sps_ = SpsParser::ParseAndRewriteSpsIfNeeded(slice + H264::kNaluTypeSize,
                                                   length - H264::kNaluTypeSize,
                                                   target_width_,
                                                   target_height_);
      if (!sps_)
        RTC_LOG(LS_WARNING) << "Unable to parse SPS from H264 bitstream.";
      else {
        isSps = true;
        if (backup_spspps)
        {
          last_sps_size = nal_size;
          if (last_sps_data)
          {
            delete [] last_sps_data;
            last_sps_data = NULL;
          }
          last_sps_data = new unsigned char[last_sps_size];
          memcpy(last_sps_data, nal, nal_size);
        }
        RTC_LOG(LS_INFO) << "parsed SPS from H264 bitstream.";
      }
      break;
    }
    case H264::NaluType::kPps: {
      pps_ = PpsParser::ParsePps(slice + H264::kNaluTypeSize,
                                 length - H264::kNaluTypeSize);
      if (!pps_)
        RTC_LOG(LS_WARNING) << "Unable to parse PPS from H264 bitstream.";
      else {
        isPps = true;
        if (backup_spspps) {
          last_pps_size = nal_size;
          if (last_pps_data)
          {
            delete [] last_pps_data;
            last_pps_data = NULL;
          }
          last_pps_data = new unsigned char[last_pps_size];
          memcpy(last_pps_data, nal, nal_size);
        }
        RTC_LOG(LS_INFO) << "parsed PPS from H264 bitstream.";
      }
      break;
    }
    case H264::NaluType::kSei: {
      bool valid_sei  = SeiParser::ParseSei(slice + H264::kNaluTypeSize,
                                      length - H264::kNaluTypeSize,
                                      sei);
      if (valid_sei) {
        res = kHasValidSei;
      }
      break;
    }
    case H264::NaluType::kAud:
      break;  // Ignore these nalus, as we don't care about their contents.
    case H264::NaluType::KSVCPrefix:
      svc_extension_ = SvcExtensionParser::ParseSvcExtension(slice + H264::kNaluTypeSize,
                                                             length - H264::kNaluTypeSize);
      if (svc_extension_ && svc_extension_->svc_extension_flag) {
        *discardable_flag = svc_extension_->discardable_flag;
      }
      
      if (print) {
        RTC_LOG(LS_INFO) << "svc_extension_flag : " << svc_extension_->svc_extension_flag;
        if (svc_extension_->svc_extension_flag) {
          RTC_LOG(LS_INFO) << "header_svc_extension - idr_flag : " << svc_extension_->idr_flag;
          RTC_LOG(LS_INFO) << "header_svc_extension - priority_id : " << svc_extension_->priority_id;
          RTC_LOG(LS_INFO) << "header_svc_extension - no_inter_layer_pred_flag : " << svc_extension_->no_inter_layer_pred_flag;
          RTC_LOG(LS_INFO) << "header_svc_extension - dependency_id : " << svc_extension_->dependency_id;
          RTC_LOG(LS_INFO) << "header_svc_extension - quality_id : " << svc_extension_->quality_id;
          RTC_LOG(LS_INFO) << "header_svc_extension - temporal_id : " << svc_extension_->temporal_id;
          RTC_LOG(LS_INFO) << "header_svc_extension - use_ref_base_pic_flag : " << svc_extension_->use_ref_base_pic_flag;
          RTC_LOG(LS_INFO) << "header_svc_extension - discardable_flag : " << svc_extension_->discardable_flag;
          RTC_LOG(LS_INFO) << "header_svc_extension - output_flag : " << svc_extension_->output_flag;
          RTC_LOG(LS_INFO) << "header_svc_extension - reserved_three_2bits : " << svc_extension_->reserved_three_2bits;
        }
      }
      
      break;
    default:
      if (nalu_type == H264::NaluType::kIdr) {
        isIdr = true;
        RTC_LOG(LS_INFO) << "parsed IDR from H264 bitstream.";
      }
      res = ParseNonParameterSetNalu(slice, length, nalu_type);
      if (res != kOk)
        RTC_LOG(LS_INFO) << "Failed to parse bitstream. Error: " << res;
      break;
  }

  if (buffer != nullptr && *discardable_flag == false) {
    buffer->WriteData(H264::kNaluShortStartCode, H264::kNaluShortStartSequenceSize);
    if (nalu_type == H264::NaluType::kSps && sps_ && sps_->output_buffer) {
      buffer->WriteData(slice, H264::kNaluTypeSize);
      buffer->WriteData(sps_->output_buffer->data(), sps_->output_buffer->size());
      res = kBufferChanged;
    } else {
      buffer->WriteData(slice, length);
    }
  } 

  if (*discardable_flag == false) {
    NAL_INFO info;
    info.nalu_type = nalu_type;
    info.nir = ((slice[0] & 0x60) >> 5);
    nalus_info_.emplace_back(info);
  }

  return res;
}

void H264BitstreamParser::ParseBitstream(const uint8_t* bitstream,
                                         size_t length,
                                         bool backup_spspps) {
  isIdr = false;
  isSps = false;
  isPps = false;
  sei_.reset();
  svc_extension_.reset();
  nalus_info_.clear();
  nalu_indices =
      H264::FindNaluIndices(bitstream, length);
  webrtc::SeiParser::SeiState sei;
  bool valid_sei = false;
  // for (const H264::NaluIndex& index : nalu_indices)
  //   valid_sei |= ParseSlice(&bitstream[index.payload_start_offset],
  //                           index.payload_size, sei,
  //                           &bitstream[index.start_offset],
  //                           index.nalu_size, backup_spspps);
  for(std::vector<H264::NaluIndex>::iterator it = nalu_indices.begin(); it != nalu_indices.end();) {
    H264::NaluIndex index = *it;
    bool discardable_flag = false;
    Result res = ParseSlice(&bitstream[index.payload_start_offset],
                            index.payload_size, sei,
                            &bitstream[index.start_offset],
                            index.nalu_size, backup_spspps, &discardable_flag);
    valid_sei |= (res == kHasValidSei);
    if (discardable_flag) {
      it = nalu_indices.erase(it);
    }else {
      ++it;
    }
  }
  
  if (valid_sei) {
    sei_ = sei;
  }
}

void H264BitstreamParser::ParseBitstreamAndRewriteSpsIfNeeded(
    std::vector<uint8_t>& buffer, bool backup_spspps) {
  isIdr = false;
  isSps = false;
  isPps = false;
  sei_.reset();
  svc_extension_.reset();
  nalus_info_.clear();
  const uint8_t* bitstream = buffer.data();
  size_t length = buffer.size();
  // NOTE (Think): now we only amend sps when change crop
  H264::BufferCopy buffer_new(length + H264::kMaxSpsIncreaseWhenChangeCrop);
  nalu_indices =
      H264::FindNaluIndices(bitstream, length);
  webrtc::SeiParser::SeiState sei;
  bool valid_sei = false;
  bool buffer_changed = false;
  for (std::vector<H264::NaluIndex>::iterator it = nalu_indices.begin(); it != nalu_indices.end();) {
    H264::NaluIndex index = *it;
    bool discardable_flag = false;
    Result res = ParseSlice(&bitstream[index.payload_start_offset],
                            index.payload_size, sei,
                            &bitstream[index.start_offset],
                            index.nalu_size, backup_spspps,
                            &discardable_flag,
                            &buffer_new);
    valid_sei |= (res == kHasValidSei);

    buffer_changed |= (res == kBufferChanged);

    if (discardable_flag) {
      it = nalu_indices.erase(it);
    } else {
      ++it;
    }
  }

  if (valid_sei) {
    sei_ = sei;
  }

  if (buffer_changed) {
    // RTC_LOG(LS_INFO) << "Think: ParseBistreamAndAmendSpsIfNeeded buffer changed";
    nalu_indices = H264::FindNaluIndices(buffer_new.buffer.data(), buffer_new.buffer.size());
    buffer.swap(buffer_new.buffer);
  }
}

bool H264BitstreamParser::GetLastSliceQp(int* qp) const {
  if (!last_slice_qp_delta_ || !pps_) {
    RTC_LOG(LS_ERROR) << "GetLastSliceQp - no last slice qp delta or pps";
    return false;
  }
    
  const int parsed_qp = 26 + pps_->pic_init_qp_minus26 + *last_slice_qp_delta_;
  if (parsed_qp < kMinQpValue || parsed_qp > kMaxQpValue) {
    RTC_LOG(LS_ERROR) << "Parsed invalid QP from bitstream.";
    return false;
  }
  *qp = parsed_qp;
  return true;
}

bool H264BitstreamParser::isLastIdr(bool &hasSps, bool &hasPps) const {
  hasSps = isSps;
  hasPps = isPps;
  return isIdr;
}

std::vector<H264::NaluIndex> H264BitstreamParser::GetLastNaluIndices() const {
  return nalu_indices;
}

bool H264BitstreamParser::AppendSei(const std::vector<uint8_t> &sei,
                                    uint32_t sei_type,
                                    uint8_t** bitstream,
                                    size_t* length,
                                    size_t* size) {
  std::vector<uint8_t> sei_nalu{0x00, 0x00, 0x00, 0x01, 0x06};
  SeiParser::GenerateSeiNalu(sei_nalu, sei, sei_type);

  //
  bool need_reset_buffer = false;
  size_t size_required = *length + sei_nalu.size();
  if (size_required > *size) {
    uint8_t* new_buffer = new uint8_t[size_required];
    memcpy(new_buffer, *bitstream, *length);
    *size = size_required;
    *bitstream = new_buffer;
    need_reset_buffer = true;
  }
//  std::vector<H264::NaluIndex> nalu_indices =
//      H264::FindNaluIndices(*bitstream, *length);
  //memcpy(*bitstream + *length, sei_nalu.data(), sei_nalu.size());
  memmove(*bitstream + sei_nalu.size(), *bitstream, *length);
  memcpy(*bitstream , sei_nalu.data(), sei_nalu.size());
  *length += sei_nalu.size();
//  nalu_indices = H264::FindNaluIndices(*bitstream, *length);
  return need_reset_buffer;
}

H264BitstreamParser::TemporalType H264BitstreamParser::GetTemporalType() {
  for(auto& it : nalus_info_) {
    if(it.nalu_type == H264::NaluType::kSlice) { 
      if(it.nir == kSVCBaseNRI) {
        return kSVCBase;
      } else if(it.nir == kSVCEnhance1NRI) {
        return kSVCEnhance1;
      }
    } else if(it.nalu_type == H264::NaluType::kIdr) {
      return kIdr;
    }
  }

  return kNormal;
}

bool H264BitstreamParser::GetLastResolution(int* w, int* h) const {
  if (!sps_)
    return false;

  *w = sps_->width;
  *h = sps_->height;
  return true;
}

unsigned char* H264BitstreamParser::GetLastSps(size_t &sps_size) {
  sps_size = last_sps_size;
  return last_sps_data;
}

unsigned char* H264BitstreamParser::GetLastPps(size_t &pps_size) {
  pps_size = last_pps_size;
  return last_pps_data;
}

}  // namespace webrtc
