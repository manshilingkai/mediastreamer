/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#ifndef COMMON_VIDEO_H264_H264_BITSTREAM_PARSER_H_
#define COMMON_VIDEO_H264_H264_BITSTREAM_PARSER_H_
#include <stddef.h>
#include <stdint.h>

#include "absl/types/optional.h"
#include "common_video/h264/pps_parser.h"
#include "common_video/h264/sps_parser.h"
#include "common_video/h264/sei_parser.h"
#include "common_video/h264/h264_common.h"
#include "common_video/h264/svc_extension_parser.h"

namespace webrtc {

typedef struct H264PredWeightTable {
    int use_weight;
    int use_weight_chroma;
    uint32_t luma_log2_weight_denom;
    uint32_t chroma_log2_weight_denom;
    int luma_weight_flag[2];    ///< 7.4.3.2 luma_weight_lX_flag
    int chroma_weight_flag[2];  ///< 7.4.3.2 chroma_weight_lX_flag
    // The following 2 can be changed to int8_t but that causes a 10 CPU cycles speed loss
    int luma_weight[48][2][2];
    int chroma_weight[48][2][2][2];
    int implicit_weight[48][48][2];
} H264PredWeightTable;

const int kSeiMaxPayloadSize = 4096; // reserve 4k size for sei use
const uint8_t kSVCBaseNRI = 0x2; 
const uint8_t kSVCEnhance1NRI = 0x0;

// Stateful H264 bitstream parser (due to SPS/PPS). Used to parse out QP values
// from the bitstream.
// TODO(pbos): Unify with RTP SPS parsing and only use one H264 parser.
// TODO(pbos): If/when this gets used on the receiver side CHECKs must be
// removed and gracefully abort as we have no control over receive-side
// bitstreams.
class H264BitstreamParser {
 public:
  enum Result {
    kOk,
    kInvalidStream,
    kUnsupportedStream,
    kHasValidSei,
    kBufferChanged,
  };

  enum TemporalType {
    kNormal,
    kIdr,
    kSVCBase,
    kSVCEnhance1,
  };

  H264BitstreamParser();
  virtual ~H264BitstreamParser();

  // Parse an additional chunk of H264 bitstream.
  void ParseBitstream(const uint8_t* bitstream, size_t length, bool backup_spspps = false);

  // NOTE(Think): here amend sps to align actual width & height in sps
  void ParseBitstreamAndRewriteSpsIfNeeded(std::vector<uint8_t>& buffer,  bool backup_spspps = false);
  void SetWantedResolution(uint32_t width, uint32_t height) {
    target_width_ = width;
    target_height_ = height;
  }

  // Get the last extracted QP value from the parsed bitstream.
  bool GetLastSliceQp(int* qp) const;

  absl::optional<SeiParser::SeiState>& ParsedSei() { return sei_; }
  absl::optional<SvcExtensionParser::SvcExtensionState>& ParsedSvcExtension() { return svc_extension_; }

  // return true: bitstream size is enough for sei
  // return false: bitstream size is not enough for sei, realloc actually inside
  bool AppendSei(const std::vector<uint8_t> &sei,
                 uint32_t sei_type,
                 uint8_t** bitstream,
                 size_t* length,
                 size_t* size);
  
  TemporalType GetTemporalType();

  static bool IsFirstSliceInFrame(const uint8_t* source,size_t source_length);

  // Get the last Resolution from the parsed bitstream.
  bool GetLastResolution(int* w, int* h) const;

  bool isLastIdr(bool &hasSps, bool &hasPps) const;

  std::vector<H264::NaluIndex> GetLastNaluIndices() const;

  unsigned char* GetLastSps(size_t &sps_size);
  unsigned char* GetLastPps(size_t &pps_size);

 protected:
  Result h264_pred_weight_table(rtc::BitBuffer& slice_reader,uint32_t* ref_count, uint32_t slice_type, H264PredWeightTable *pwt);
  Result ParseSlice(const uint8_t* slice, size_t length, SeiParser::SeiState &sei, const uint8_t* nal, size_t nal_size, bool backup_spspps, bool *discardable_flag, H264::BufferCopy* buffer = nullptr);
  Result ParseNonParameterSetNalu(const uint8_t* source,
                                  size_t source_length,
                                  uint8_t nalu_type);

  // SPS/PPS state, updated when parsing new SPS/PPS, used to parse slices.
  absl::optional<SpsParser::SpsState> sps_;
  absl::optional<PpsParser::PpsState> pps_;
  absl::optional<SeiParser::SeiState> sei_;
  absl::optional<SvcExtensionParser::SvcExtensionState> svc_extension_;

  // Last parsed slice QP.
  absl::optional<int32_t> last_slice_qp_delta_;

  struct NAL_INFO {
    H264::NaluType nalu_type;
    uint8_t nir;
  };
  std::vector<NAL_INFO> nalus_info_; 

  bool isIdr = false;
  bool isSps = false;
  bool isPps = false;

  std::vector<H264::NaluIndex> nalu_indices;

  unsigned char* last_sps_data = NULL;
  size_t last_sps_size = 0;
  unsigned char* last_pps_data = NULL;
  size_t last_pps_size = 0;

  // NOTE (Think): 0 means no need to amend sps;
  uint32_t target_width_ = 0;
  uint32_t target_height_ = 0;

  bool print = false;
};

}  // namespace webrtc

#endif  // COMMON_VIDEO_H264_H264_BITSTREAM_PARSER_H_
