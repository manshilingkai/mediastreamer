
#include "common_video/h264/svc_extension_parser.h"

#include <cstdint>
#include <vector>

#include "common_video/h264/h264_common.h"
#include "rtc_base/bitbuffer.h"
#include "rtc_base/checks.h"

#define RETURN_EMPTY_ON_FAIL(x) \
  if (!(x)) {                   \
    return absl::nullopt;       \
  }

namespace webrtc {

SvcExtensionParser::SvcExtensionState::SvcExtensionState() = default;
SvcExtensionParser::SvcExtensionState::SvcExtensionState(const SvcExtensionState&) = default;
SvcExtensionParser::SvcExtensionState::~SvcExtensionState() = default;

absl::optional<SvcExtensionParser::SvcExtensionState> SvcExtensionParser::ParseSvcExtension(const uint8_t* data, size_t length)
{
  std::vector<uint8_t> unpacked_buffer = H264::ParseRbsp(data, length);
  rtc::BitBuffer bit_buffer(unpacked_buffer.data(), unpacked_buffer.size());
  return SvcExtensionParser::ParseSvcExtensionInternal(&bit_buffer);
}

absl::optional<SvcExtensionParser::SvcExtensionState> SvcExtensionParser::ParseSvcExtensionInternal(rtc::BitBuffer* bit_buffer) {
  SvcExtensionParser::SvcExtensionState svcExtensionState;

  RETURN_EMPTY_ON_FAIL(bit_buffer->ReadBits(&svcExtensionState.svc_extension_flag, 1));
  if (svcExtensionState.svc_extension_flag) {
    RETURN_EMPTY_ON_FAIL(bit_buffer->ReadBits(&svcExtensionState.idr_flag, 1));
    RETURN_EMPTY_ON_FAIL(bit_buffer->ReadBits(&svcExtensionState.priority_id, 6));
    RETURN_EMPTY_ON_FAIL(bit_buffer->ReadBits(&svcExtensionState.no_inter_layer_pred_flag, 1));
    RETURN_EMPTY_ON_FAIL(bit_buffer->ReadBits(&svcExtensionState.dependency_id, 3));
    RETURN_EMPTY_ON_FAIL(bit_buffer->ReadBits(&svcExtensionState.quality_id, 4));
    RETURN_EMPTY_ON_FAIL(bit_buffer->ReadBits(&svcExtensionState.temporal_id, 3));
    RETURN_EMPTY_ON_FAIL(bit_buffer->ReadBits(&svcExtensionState.use_ref_base_pic_flag, 1));
    RETURN_EMPTY_ON_FAIL(bit_buffer->ReadBits(&svcExtensionState.discardable_flag, 1));
    RETURN_EMPTY_ON_FAIL(bit_buffer->ReadBits(&svcExtensionState.output_flag, 1));
    RETURN_EMPTY_ON_FAIL(bit_buffer->ReadBits(&svcExtensionState.reserved_three_2bits, 2));
  }

  return svcExtensionState;
}

}  // namespace webrtc