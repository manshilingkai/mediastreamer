/*
 *  Copyright (c) 2016 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#include "common_video/h264/sps_parser.h"

#include <cstdint>
#include <vector>

#include "common_video/h264/h264_common.h"
#include "rtc_base/bitbuffer.h"
#include "rtc_base/logging.h"

namespace {
typedef absl::optional<webrtc::SpsParser::SpsState> OptionalSps;

#define RETURN_EMPTY_ON_FAIL(x) \
  if (!(x)) {                   \
    return OptionalSps();       \
  }

#define RETURN_FALSE_ON_FAIL(x)                                      \
  if (!(x)) {                                                        \
    RTC_LOG_F(LS_ERROR) << " (line:" << __LINE__ << ") FAILED: " #x; \
    return false;                                                    \
  }

constexpr int kScalingDeltaMin = -128;
constexpr int kScaldingDeltaMax = 127;
}  // namespace

namespace webrtc {

SpsParser::SpsState::SpsState() = default;
SpsParser::SpsState::SpsState(const SpsState& sps)
    : width(sps.width)
    , height(sps.height)
    , delta_pic_order_always_zero_flag(sps.delta_pic_order_always_zero_flag)
    , separate_colour_plane_flag(sps.separate_colour_plane_flag)
    , frame_mbs_only_flag(sps.frame_mbs_only_flag)
    , log2_max_frame_num(sps.log2_max_frame_num)
    , log2_max_pic_order_cnt_lsb(sps.log2_max_pic_order_cnt_lsb)
    , pic_order_cnt_type(sps.pic_order_cnt_type)
    , max_num_ref_frames(sps.max_num_ref_frames)
    , vui_params_present(sps.vui_params_present)
    , id(sps.id)
    , chroma_format_idc(sps.chroma_format_idc)
#if HAVE_NEVC
    , enable_nevc(sps.enable_nevc)
#endif
    , frame_crop_left_offset(sps.frame_crop_left_offset)
    , frame_crop_right_offset(sps.frame_crop_right_offset)
    , frame_crop_top_offset(sps.frame_crop_top_offset)
    , frame_crop_bottom_offset(sps.frame_crop_bottom_offset)
    , byte_offset_till_crop(sps.byte_offset_till_crop)
    , bit_offset_till_crop(sps.bit_offset_till_crop)
    , output_buffer(sps.output_buffer) {
}
SpsParser::SpsState::~SpsState() = default;

// General note: this is based off the 02/2014 version of the H.264 standard.
// You can find it on this page:
// http://www.itu.int/rec/T-REC-H.264

// Unpack RBSP and parse SPS state from the supplied buffer.
absl::optional<SpsParser::SpsState> SpsParser::ParseSps(const uint8_t* data,
                                                        size_t length) {
  std::vector<uint8_t> unpacked_buffer = H264::ParseRbsp(data, length);
  rtc::BitBuffer bit_buffer(unpacked_buffer.data(), unpacked_buffer.size());
  return ParseSpsUpToVui(&bit_buffer);
}

absl::optional<SpsParser::SpsState> SpsParser::ParseAndRewriteSpsIfNeeded(
    const uint8_t* data, size_t length,
    uint32_t target_width, uint32_t target_height) {
  std::vector<uint8_t> unpacked_buffer = H264::ParseRbsp(data, length);
  rtc::BitBuffer bit_buffer(unpacked_buffer.data(), unpacked_buffer.size());
  absl::optional<SpsParser::SpsState> sps_state =  ParseSpsUpToVui(&bit_buffer);
  if (target_width > 0 && target_height > 0 && sps_state &&
     (sps_state->width != target_width || sps_state->height != target_height)) {
    // NOTE(Think): we need to rewrite sps to amend width or height;
    
    // 1. generate crop info to be writed
    // add all width offset on right, and height offset on bottom
    int32_t right_offset = sps_state->width - target_width;
    int32_t bottom_offset = sps_state->height - target_height;
    sps_state->frame_crop_right_offset += right_offset;
    sps_state->frame_crop_bottom_offset += bottom_offset;

    // amend crop info from spec
    if (sps_state->separate_colour_plane_flag || sps_state->chroma_format_idc == 0) {
      sps_state->frame_crop_bottom_offset /= (2 - sps_state->frame_mbs_only_flag);
      sps_state->frame_crop_top_offset /= (2 - sps_state->frame_mbs_only_flag);
    } else if (!sps_state->separate_colour_plane_flag && sps_state->chroma_format_idc > 0) {
      // Width multipliers for formats 1 (4:2:0) and 2 (4:2:2).
      if (sps_state->chroma_format_idc == 1 || sps_state->chroma_format_idc == 2) {
        sps_state->frame_crop_right_offset /= 2;
        sps_state->frame_crop_left_offset /= 2;
      }
      // Height multipliers for format 1 (4:2:0).
      if (sps_state->chroma_format_idc == 1) {
        sps_state->frame_crop_bottom_offset /= 2;
        sps_state->frame_crop_top_offset /= 2;
      }
    }

    RTC_LOG(LS_INFO) << "ParseAndRewriteSpsIfNeeded: target_width = " << target_width
                     << ", target_height = " << target_height
                     << ", sps_state->width = " << sps_state->width
                     << ", sps_state->height = " << sps_state->height
                     << ", right_offset = " << right_offset
                     << ", bottom_offset = " << bottom_offset
                     << ", frame_crop_right_offset = " << sps_state->frame_crop_right_offset
                     << ", frame_crop_bottom_offset = " << sps_state->frame_crop_bottom_offset;
    // 2. write new crop info into sps
    rtc::Buffer rbsp_buffer(length + H264::kMaxSpsIncreaseWhenChangeCrop);
    if (WriteCropToSps(&rbsp_buffer, &bit_buffer, unpacked_buffer, &(sps_state.value()))) {
      sps_state->output_buffer = std::make_shared<rtc::Buffer>(length + H264::kMaxSpsIncreaseWhenChangeCrop);
      sps_state->output_buffer->SetSize(0);
      H264::WriteRbsp(rbsp_buffer.data(), rbsp_buffer.size(), sps_state->output_buffer.get());
    }
  }
  return sps_state;
}

absl::optional<SpsParser::SpsState> SpsParser::ParseSpsUpToVui(
    rtc::BitBuffer* buffer) {
  // Now, we need to use a bit buffer to parse through the actual AVC SPS
  // format. See Section 7.3.2.1.1 ("Sequence parameter set data syntax") of the
  // H.264 standard for a complete description.
  // Since we only care about resolution, we ignore the majority of fields, but
  // we still have to actively parse through a lot of the data, since many of
  // the fields have variable size.
  // We're particularly interested in:
  // chroma_format_idc -> affects crop units
  // pic_{width,height}_* -> resolution of the frame in macroblocks (16x16).
  // frame_crop_*_offset -> crop information

  SpsState sps;

  // The golomb values we have to read, not just consume.
  uint32_t golomb_ignored;

  // chroma_format_idc will be ChromaArrayType if separate_colour_plane_flag is
  // 0. It defaults to 1, when not specified.  

  // profile_idc: u(8). We need it to determine if we need to read/skip chroma
  // formats.
  uint8_t profile_idc;
  RETURN_EMPTY_ON_FAIL(buffer->ReadUInt8(&profile_idc));
  // constraint_set0_flag through constraint_set5_flag + reserved_zero_2bits
  // 1 bit each for the flags + 2 bits = 8 bits = 1 byte.
#if HAVE_NEVC
  uint32_t constraint_set0_flag;  //0-5bit  6bit for all_nevc  7bit for nevc
  RETURN_EMPTY_ON_FAIL(buffer->ReadBits(&constraint_set0_flag, 6));
  RETURN_EMPTY_ON_FAIL(buffer->ReadBits(&sps.enable_all_nevc, 1));
  RETURN_EMPTY_ON_FAIL(buffer->ReadBits(&sps.enable_nevc, 1));
#else
  RETURN_EMPTY_ON_FAIL(buffer->ConsumeBytes(1));
#endif
  // level_idc: u(8)
  RETURN_EMPTY_ON_FAIL(buffer->ConsumeBytes(1));
  // seq_parameter_set_id: ue(v)
  RETURN_EMPTY_ON_FAIL(buffer->ReadExponentialGolomb(&sps.id));
  sps.separate_colour_plane_flag = 0;
  // See if profile_idc has chroma format information.
  if (profile_idc == 100 || profile_idc == 110 || profile_idc == 122 ||
      profile_idc == 244 || profile_idc == 44 || profile_idc == 83 ||
      profile_idc == 86 || profile_idc == 118 || profile_idc == 128 ||
      profile_idc == 138 || profile_idc == 139 || profile_idc == 134) {
    // chroma_format_idc: ue(v)
    RETURN_EMPTY_ON_FAIL(buffer->ReadExponentialGolomb(&sps.chroma_format_idc));
    if (sps.chroma_format_idc == 3) {
      // separate_colour_plane_flag: u(1)
      RETURN_EMPTY_ON_FAIL(
          buffer->ReadBits(&sps.separate_colour_plane_flag, 1));
    }
    // bit_depth_luma_minus8: ue(v)
    RETURN_EMPTY_ON_FAIL(buffer->ReadExponentialGolomb(&golomb_ignored));
    // bit_depth_chroma_minus8: ue(v)
    RETURN_EMPTY_ON_FAIL(buffer->ReadExponentialGolomb(&golomb_ignored));
    // qpprime_y_zero_transform_bypass_flag: u(1)
    RETURN_EMPTY_ON_FAIL(buffer->ConsumeBits(1));
    // seq_scaling_matrix_present_flag: u(1)
    uint32_t seq_scaling_matrix_present_flag;
    RETURN_EMPTY_ON_FAIL(buffer->ReadBits(&seq_scaling_matrix_present_flag, 1));
    if (seq_scaling_matrix_present_flag) {
      // Process the scaling lists just enough to be able to properly
      // skip over them, so we can still read the resolution on streams
      // where this is included.
      int scaling_list_count = (sps.chroma_format_idc == 3 ? 12 : 8);
      for (int i = 0; i < scaling_list_count; ++i) {
        // seq_scaling_list_present_flag[i]  : u(1)
        uint32_t seq_scaling_list_present_flags;
        RETURN_EMPTY_ON_FAIL(
            buffer->ReadBits(&seq_scaling_list_present_flags, 1));
        if (seq_scaling_list_present_flags != 0) {
          int last_scale = 8;
          int next_scale = 8;
          int size_of_scaling_list = i < 6 ? 16 : 64;
          for (int j = 0; j < size_of_scaling_list; j++) {
            if (next_scale != 0) {
              int32_t delta_scale;
              // delta_scale: se(v)
              RETURN_EMPTY_ON_FAIL(
                  buffer->ReadSignedExponentialGolomb(&delta_scale));
              RETURN_EMPTY_ON_FAIL(delta_scale >= kScalingDeltaMin &&
                                   delta_scale <= kScaldingDeltaMax);
              next_scale = (last_scale + delta_scale + 256) % 256;
            }
            if (next_scale != 0)
              last_scale = next_scale;
          }
        }
      }
    }
  }
  // log2_max_frame_num and log2_max_pic_order_cnt_lsb are used with
  // BitBuffer::ReadBits, which can read at most 32 bits at a time. We also have
  // to avoid overflow when adding 4 to the on-wire golomb value, e.g., for evil
  // input data, ReadExponentialGolomb might return 0xfffc.
  const uint32_t kMaxLog2Minus4 = 32 - 4;

  // log2_max_frame_num_minus4: ue(v)
  uint32_t log2_max_frame_num_minus4;
  if (!buffer->ReadExponentialGolomb(&log2_max_frame_num_minus4) ||
      log2_max_frame_num_minus4 > kMaxLog2Minus4) {
    return OptionalSps();
  }
  sps.log2_max_frame_num = log2_max_frame_num_minus4 + 4;

  // pic_order_cnt_type: ue(v)
  RETURN_EMPTY_ON_FAIL(buffer->ReadExponentialGolomb(&sps.pic_order_cnt_type));
  if (sps.pic_order_cnt_type == 0) {
    // log2_max_pic_order_cnt_lsb_minus4: ue(v)
    uint32_t log2_max_pic_order_cnt_lsb_minus4;
    if (!buffer->ReadExponentialGolomb(&log2_max_pic_order_cnt_lsb_minus4) ||
        log2_max_pic_order_cnt_lsb_minus4 > kMaxLog2Minus4) {
      return OptionalSps();
    }
    sps.log2_max_pic_order_cnt_lsb = log2_max_pic_order_cnt_lsb_minus4 + 4;
  } else if (sps.pic_order_cnt_type == 1) {
    // delta_pic_order_always_zero_flag: u(1)
    RETURN_EMPTY_ON_FAIL(
        buffer->ReadBits(&sps.delta_pic_order_always_zero_flag, 1));
    // offset_for_non_ref_pic: se(v)
    RETURN_EMPTY_ON_FAIL(buffer->ReadExponentialGolomb(&golomb_ignored));
    // offset_for_top_to_bottom_field: se(v)
    RETURN_EMPTY_ON_FAIL(buffer->ReadExponentialGolomb(&golomb_ignored));
    // num_ref_frames_in_pic_order_cnt_cycle: ue(v)
    uint32_t num_ref_frames_in_pic_order_cnt_cycle;
    RETURN_EMPTY_ON_FAIL(
        buffer->ReadExponentialGolomb(&num_ref_frames_in_pic_order_cnt_cycle));
    for (size_t i = 0; i < num_ref_frames_in_pic_order_cnt_cycle; ++i) {
      // offset_for_ref_frame[i]: se(v)
      RETURN_EMPTY_ON_FAIL(buffer->ReadExponentialGolomb(&golomb_ignored));
    }
  }
  // max_num_ref_frames: ue(v)
  RETURN_EMPTY_ON_FAIL(buffer->ReadExponentialGolomb(&sps.max_num_ref_frames));
  // gaps_in_frame_num_value_allowed_flag: u(1)
  RETURN_EMPTY_ON_FAIL(buffer->ConsumeBits(1));
  //
  // IMPORTANT ONES! Now we're getting to resolution. First we read the pic
  // width/height in macroblocks (16x16), which gives us the base resolution,
  // and then we continue on until we hit the frame crop offsets, which are used
  // to signify resolutions that aren't multiples of 16.
  //
  // pic_width_in_mbs_minus1: ue(v)
  uint32_t pic_width_in_mbs_minus1;
  RETURN_EMPTY_ON_FAIL(buffer->ReadExponentialGolomb(&pic_width_in_mbs_minus1));
  // pic_height_in_map_units_minus1: ue(v)
  uint32_t pic_height_in_map_units_minus1;
  RETURN_EMPTY_ON_FAIL(
      buffer->ReadExponentialGolomb(&pic_height_in_map_units_minus1));
  // frame_mbs_only_flag: u(1)
  RETURN_EMPTY_ON_FAIL(buffer->ReadBits(&sps.frame_mbs_only_flag, 1));
  if (!sps.frame_mbs_only_flag) {
    // mb_adaptive_frame_field_flag: u(1)
    RETURN_EMPTY_ON_FAIL(buffer->ConsumeBits(1));
  }
  // direct_8x8_inference_flag: u(1)
  RETURN_EMPTY_ON_FAIL(buffer->ConsumeBits(1));
  //
  // MORE IMPORTANT ONES! Now we're at the frame crop information.
  //

  // Check how far the sps parser has read
  buffer->GetCurrentOffset(&sps.byte_offset_till_crop,
                           &sps.bit_offset_till_crop);

  // frame_cropping_flag: u(1)
  uint32_t frame_cropping_flag;
  uint32_t frame_crop_left_offset = 0;
  uint32_t frame_crop_right_offset = 0;
  uint32_t frame_crop_top_offset = 0;
  uint32_t frame_crop_bottom_offset = 0;
  RETURN_EMPTY_ON_FAIL(buffer->ReadBits(&frame_cropping_flag, 1));
  if (frame_cropping_flag) {
    // frame_crop_{left, right, top, bottom}_offset: ue(v)
    RETURN_EMPTY_ON_FAIL(
        buffer->ReadExponentialGolomb(&frame_crop_left_offset));
    RETURN_EMPTY_ON_FAIL(
        buffer->ReadExponentialGolomb(&frame_crop_right_offset));
    RETURN_EMPTY_ON_FAIL(buffer->ReadExponentialGolomb(&frame_crop_top_offset));
    RETURN_EMPTY_ON_FAIL(
        buffer->ReadExponentialGolomb(&frame_crop_bottom_offset));
  }
  // vui_parameters_present_flag: u(1)
  RETURN_EMPTY_ON_FAIL(buffer->ReadBits(&sps.vui_params_present, 1));

  // Far enough! We don't use the rest of the SPS.

  // Start with the resolution determined by the pic_width/pic_height fields.
  sps.width = 16 * (pic_width_in_mbs_minus1 + 1);
  sps.height =
      16 * (2 - sps.frame_mbs_only_flag) * (pic_height_in_map_units_minus1 + 1);

  // Figure out the crop units in pixels. That's based on the chroma format's
  // sampling, which is indicated by chroma_format_idc.
  if (sps.separate_colour_plane_flag || sps.chroma_format_idc == 0) {
    frame_crop_bottom_offset *= (2 - sps.frame_mbs_only_flag);
    frame_crop_top_offset *= (2 - sps.frame_mbs_only_flag);
  } else if (!sps.separate_colour_plane_flag && sps.chroma_format_idc > 0) {
    // Width multipliers for formats 1 (4:2:0) and 2 (4:2:2).
    if (sps.chroma_format_idc == 1 || sps.chroma_format_idc == 2) {
      frame_crop_left_offset *= 2;
      frame_crop_right_offset *= 2;
    }
    // Height multipliers for format 1 (4:2:0).
    if (sps.chroma_format_idc == 1) {
      frame_crop_top_offset *= 2;
      frame_crop_bottom_offset *= 2;
    }
  }
  // Subtract the crop for each dimension.
  sps.width -= (frame_crop_left_offset + frame_crop_right_offset);
  sps.height -= (frame_crop_top_offset + frame_crop_bottom_offset);

  sps.frame_crop_left_offset = frame_crop_left_offset;
  sps.frame_crop_right_offset = frame_crop_right_offset;
  sps.frame_crop_top_offset = frame_crop_top_offset;
  sps.frame_crop_bottom_offset = frame_crop_bottom_offset;

  return OptionalSps(sps);
}

// assume that src_bit_buffer read up to vui_parameters_present_flag
bool SpsParser::WriteCropToSps(rtc::Buffer* dst_buffer,
                    rtc::BitBuffer* src_bit_buffer,
                    std::vector<uint8_t>& src_unpacked_buffer,
                    SpsParser::SpsState* sps) {
  // 1. copy till frame_cropping_flag, not include frame_cropping_flag
  size_t byte_offset = sps->byte_offset_till_crop +
                          (sps->bit_offset_till_crop > 0 ? 1 : 0);
  memcpy(dst_buffer->data(), src_unpacked_buffer.data(), byte_offset);

  // 2. write crop info into sps
  rtc::BitBufferWriter writer(dst_buffer->data(), dst_buffer->size());
  writer.Seek(sps->byte_offset_till_crop, sps->bit_offset_till_crop);
  RETURN_FALSE_ON_FAIL(writer.WriteBits(1/*frame_cropping_flag*/, 1));
  RETURN_FALSE_ON_FAIL(writer.WriteExponentialGolomb(sps->frame_crop_left_offset));
  RETURN_FALSE_ON_FAIL(writer.WriteExponentialGolomb(sps->frame_crop_right_offset));
  RETURN_FALSE_ON_FAIL(writer.WriteExponentialGolomb(sps->frame_crop_top_offset));
  RETURN_FALSE_ON_FAIL(writer.WriteExponentialGolomb(sps->frame_crop_bottom_offset));
  RETURN_FALSE_ON_FAIL(writer.WriteBits(sps->vui_params_present, 1));

  // 3. copy remaining data from src_unpacked_buffer to dst_buffer
  size_t bit_offset = 0;
  src_bit_buffer->GetCurrentOffset(&byte_offset, &bit_offset);
  // 3.1 copy remaining bits in bytes
  if (bit_offset > 0) {
    size_t bit_ramining_in_current_byte = 8 - bit_offset;
    uint32_t remaining_bits_value = 0;
    RETURN_FALSE_ON_FAIL(src_bit_buffer->ReadBits(&remaining_bits_value, bit_ramining_in_current_byte));
    RETURN_FALSE_ON_FAIL(writer.WriteBits(remaining_bits_value, bit_ramining_in_current_byte));
    ++byte_offset;
    // RTC_LOG(LS_INFO) << "Think debug: bit_offset > 0, bit_offset: " << bit_offset
    //                  << ", bit_ramining_in_current_byte: " << bit_ramining_in_current_byte
    //                  << ", remaining_bits_value: " << remaining_bits_value
    //                  << ", byte_offset: " << byte_offset;
  }
  // 3.2 copy remaining bytes
  bool optim = false;
  if (!optim) {
    for (size_t i = byte_offset; i < src_unpacked_buffer.size(); i++) {
      RETURN_FALSE_ON_FAIL(writer.WriteUInt8(src_unpacked_buffer[i]));
    }
  }else {
    for (size_t i = byte_offset; i < src_unpacked_buffer.size()-1; i++) {
      RETURN_FALSE_ON_FAIL(writer.WriteUInt8(src_unpacked_buffer[i]));
    }

    int bit_remaining_in_last_byte_include_rbsp_stop_one_bit = 0;
    uint8_t src_last_byte = src_unpacked_buffer[src_unpacked_buffer.size()-1];
    for(int i = 0; i < 8; i++) {
      if (src_last_byte == 0) break;
      src_last_byte = src_last_byte << 1;
      bit_remaining_in_last_byte_include_rbsp_stop_one_bit++;
    }
    if(bit_remaining_in_last_byte_include_rbsp_stop_one_bit > 0) {
      uint32_t remaining_bits_value = 0;
      src_bit_buffer->Seek(src_unpacked_buffer.size()-1, 0);
      RETURN_FALSE_ON_FAIL(src_bit_buffer->ReadBits(&remaining_bits_value, bit_remaining_in_last_byte_include_rbsp_stop_one_bit));
      RETURN_FALSE_ON_FAIL(writer.WriteBits(remaining_bits_value, bit_remaining_in_last_byte_include_rbsp_stop_one_bit));
    }
  }

  // 4. pad up to next byte with zero bits.
  writer.GetCurrentOffset(&byte_offset, &bit_offset);
  if (bit_offset > 0) {
    // RTC_LOG(LS_INFO) << "Think debug: bit_offset > 0, should not be here";
    RETURN_FALSE_ON_FAIL(writer.WriteBits(0, 8 - bit_offset));
    ++byte_offset;
    bit_offset = 0;
  }
  RTC_DCHECK(byte_offset <= dst_buffer->size());
  dst_buffer->SetSize(byte_offset);
  return true;
}

}  // namespace webrtc
