#pragma once

#include <stddef.h>
#include <stdint.h>

namespace netease {

class HWOverUseDetector {
 public:
  HWOverUseDetector();
  virtual ~HWOverUseDetector() {}

  static const float kSensitivity;
  static const float kOverUseThreshold;

  void reportSample(float oneFrameCostTimeMs);
  bool isHWOverUse();
private:
  float averageOneFrameCostTimeMs;
};

} // namespace netease