#ifndef HW_CODEC_PIPELINE_H_
#define HW_CODEC_PIPELINE_H_

#include <stdint.h>
#include <pthread.h>
#include <list>

struct HWCodecPipelineDataPacketReservedInfo {
  long long reserved_field_1;
  int reserved_field_2;

	HWCodecPipelineDataPacketReservedInfo() {
		reserved_field_1 = 0;
    reserved_field_2 = 0;
	}
};

struct HWCodecPipelineDataPacket {
  long long pts;
  long long inPipelineTimeUs;
  HWCodecPipelineDataPacketReservedInfo reservedInfo;

	HWCodecPipelineDataPacket() {
		pts = 0;
    inPipelineTimeUs = 0;
	}
};

class HWCodecPipeline {
public:
  HWCodecPipeline();
  ~HWCodecPipeline();

  void inDataPacketWhenInUnProcessedFrame(long long pts_from_unprocessed_frame, HWCodecPipelineDataPacketReservedInfo reservedInfo);
  HWCodecPipelineDataPacket outDataPacketWhenOutProcessedFrame(long long pts_from_processed_frame, long long *process_duration_us_for_one_frame);
private:
  pthread_mutex_t mLock;
  std::list<HWCodecPipelineDataPacket> mHWCodecPipelineDataPacketList;
  HWCodecPipelineDataPacket mInDataPacketLastBackup;
};

#endif