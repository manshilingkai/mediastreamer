#include "hw_codec_pipeline.h"
#include "MediaTime.h"

HWCodecPipeline::HWCodecPipeline()
{
  pthread_mutex_init(&mLock, NULL);
}

HWCodecPipeline::~HWCodecPipeline()
{
  while (!mHWCodecPipelineDataPacketList.empty()) {
    mHWCodecPipelineDataPacketList.pop_front();
  }
  mHWCodecPipelineDataPacketList.clear();

  pthread_mutex_destroy(&mLock);
}

void HWCodecPipeline::inDataPacketWhenInUnProcessedFrame(long long pts_from_unprocessed_frame, HWCodecPipelineDataPacketReservedInfo reservedInfo)
{
    HWCodecPipelineDataPacket dataPacket;
    dataPacket.pts = pts_from_unprocessed_frame;
    dataPacket.inPipelineTimeUs = GetNowUs();
    dataPacket.reservedInfo = reservedInfo;

    pthread_mutex_lock(&mLock);
    mHWCodecPipelineDataPacketList.push_back(dataPacket);
    mInDataPacketLastBackup = dataPacket;
    pthread_mutex_unlock(&mLock);
}

HWCodecPipelineDataPacket HWCodecPipeline::outDataPacketWhenOutProcessedFrame(long long pts_from_processed_frame, long long *process_duration_us_for_one_frame)
{
    pthread_mutex_lock(&mLock);

    bool isFoundTargetTimeStamp = false;
    HWCodecPipelineDataPacket ret;
    
    std::list<HWCodecPipelineDataPacket>::iterator iter;
	for(iter=mHWCodecPipelineDataPacketList.begin();iter!=mHWCodecPipelineDataPacketList.end();++iter) {
		HWCodecPipelineDataPacket dataPacket = *iter;
        if (dataPacket.pts == pts_from_processed_frame) {
            isFoundTargetTimeStamp = true;
            break;
        }
	}

    if (isFoundTargetTimeStamp) {
        while (!mHWCodecPipelineDataPacketList.empty()) {
            HWCodecPipelineDataPacket dataPacket = mHWCodecPipelineDataPacketList.front();
            mHWCodecPipelineDataPacketList.pop_front();
            if (dataPacket.pts == pts_from_processed_frame) {
                ret = dataPacket;
                break;
            }
        }
    }else {
        if (!mHWCodecPipelineDataPacketList.empty()) {
            HWCodecPipelineDataPacket dataPacket = mHWCodecPipelineDataPacketList.front();
            ret = dataPacket;
            mHWCodecPipelineDataPacketList.pop_front();
        }else {
            ret = mInDataPacketLastBackup;
        }
    }

    pthread_mutex_unlock(&mLock);

    *process_duration_us_for_one_frame = GetNowUs() - ret.inPipelineTimeUs;

    return ret;
}