#include "modules/video_coding/codecs/winhw/common/hw_bitrate_adjuster.h"

#include <algorithm>
#include <cmath>

#include "rtc_base/logging.h"
#include "rtc_base/timeutils.h"

namespace netease {

// ---------------          HWSmartBitrateHD      ----------------- //
const float HWSmartBitrateHD::kQPSensitivity = 0.9f;
const int HWSmartBitrateHD::kMaxQPThreshold = 51;
const float HWSmartBitrateHD::kBitrateTolerancePct = .1f;
const uint32_t HWSmartBitrateHD::kSaveRateUpdateIntervalMs = 2000;
const uint32_t HWSmartBitrateHD::kSaveRateUpdateFrameInterval = 60;
HWSmartBitrateHD::HWSmartBitrateHD(int low_qp_threshold, int high_qp_threshold, HWSmartBitrateHDCodecType codec_type, bool is_screen_share, float save_rate_per_qp, float max_save_rate)
    : codec_type_(codec_type),
      save_rate_per_qp_(save_rate_per_qp),
      max_save_rate_(max_save_rate)
{
  if (is_screen_share) {
    low_qp_threshold_ = low_qp_threshold - (int)(((float)high_qp_threshold-(float)low_qp_threshold)/3.0f + 0.5f);
    high_qp_threshold_ = high_qp_threshold - (int)(((float)high_qp_threshold-(float)low_qp_threshold)/3.0f + 0.5f);
  }else {
    low_qp_threshold_ = low_qp_threshold;
    high_qp_threshold_ = high_qp_threshold;
  }

  original_target_bitrate_bps_ = 0;
  
  avg_qp_ = low_qp_threshold_ + (high_qp_threshold_ - low_qp_threshold_)/2;
  target_bitrate_bps_ = 0;

  save_rate_ = 0.0f;
  last_save_rate_update_time_ms_ = 0;
  frames_since_last_update_ = 0;
}

HWSmartBitrateHD::~HWSmartBitrateHD()
{

}

bool HWSmartBitrateHD::isEnabled() {
  if (codec_type_ == kH264 || codec_type_ == kH265) return save_rate_per_qp_ > 0 && max_save_rate_ > 0;
  else return false;
}

void HWSmartBitrateHD::SetOriginalTargetBitrate(uint32_t bitrate_bps)
{
  original_target_bitrate_bps_ = bitrate_bps;

  bool isWithinTolerance = IsWithinTolerance(bitrate_bps, target_bitrate_bps_);

  uint32_t current_bitrate_bps = (1.0f - save_rate_) * target_bitrate_bps_;
  target_bitrate_bps_ = bitrate_bps;
  save_rate_ = 1.0f - static_cast<float>(current_bitrate_bps) / static_cast<float>(target_bitrate_bps_);
  save_rate_ = std::max(save_rate_, 0.0f);
  save_rate_ = std::min(save_rate_, max_save_rate_);

  if (!isWithinTolerance) {
    last_save_rate_update_time_ms_ = rtc::TimeMillis();
    frames_since_last_update_ = 0;
  }
}

void HWSmartBitrateHD::ReportQP(int qp)
{
  if (qp <= 0 || qp > kMaxQPThreshold) return;

  if (qp > high_qp_threshold_) {
    save_rate_ = 0.0f;
    last_save_rate_update_time_ms_ = rtc::TimeMillis();
    frames_since_last_update_ = 0;
    RTC_LOG(LS_INFO) << "current qp : " << qp
                     << " high qp threshold : " << high_qp_threshold_
                     << " the current qp is larger than high qp threshold, so reset";
  }
  
  avg_qp_ = avg_qp_ * kQPSensitivity + (1.0f - kQPSensitivity) * qp;

  if (isUpdateSavaRate()) {
    float save_rate = (low_qp_threshold_ - avg_qp_) * save_rate_per_qp_;
    save_rate = std::min(save_rate, max_save_rate_);
    save_rate_ = 1.0f - (1.0f - save_rate) * (1.0f - save_rate_);
    save_rate_ = std::max(save_rate_, 0.0f);
    save_rate_ = std::min(save_rate_, max_save_rate_);
    RTC_LOG(LS_INFO) << "low_qp_threshold_ : " << low_qp_threshold_ << " avg_qp_ : " << avg_qp_ << " save_rate : " << save_rate;
    RTC_LOG(LS_INFO) << "save_rate_ : " << save_rate_;
  }
}

uint32_t HWSmartBitrateHD::SmartBitrate() {
    return (1.0f - save_rate_) * target_bitrate_bps_;
}

uint32_t HWSmartBitrateHD::GetOriginalTargetBitrate() {
  return original_target_bitrate_bps_;
}

bool HWSmartBitrateHD::IsWithinTolerance(uint32_t bitrate_bps, uint32_t target_bitrate_bps) {
  if (target_bitrate_bps == 0) {
    return false;
  }
  float delta = std::abs(static_cast<float>(bitrate_bps) -
                         static_cast<float>(target_bitrate_bps));
  float delta_pct = delta / target_bitrate_bps;
  return delta_pct < kBitrateTolerancePct;
}

bool HWSmartBitrateHD::isUpdateSavaRate() {
  if (last_save_rate_update_time_ms_ == 0) {
    last_save_rate_update_time_ms_ = rtc::TimeMillis();
  }
  
  uint32_t current_time_ms = rtc::TimeMillis();
  uint32_t time_since_last_update_ms =
      current_time_ms - last_save_rate_update_time_ms_;
  // Don't attempt to update save rate unless enough time and frames have passed.
  ++frames_since_last_update_;
  if (time_since_last_update_ms < kSaveRateUpdateIntervalMs ||
      frames_since_last_update_ < kSaveRateUpdateFrameInterval) {
    return false;
  }

  last_save_rate_update_time_ms_ = current_time_ms;
  frames_since_last_update_ = 0;
  return true;
}

// ---------------          HWBitrateAdjuster      ----------------- //
const uint32_t HWBitrateAdjuster::kBitrateUpdateIntervalMs = 1000;

const uint32_t HWBitrateAdjuster::kBitrateUpdateFrameInterval = 30;

const float HWBitrateAdjuster::kBitrateTolerancePct = .1f;

const float HWBitrateAdjuster::kBytesPerMsToBitsPerSecond = 8 * 1000;

const float HWBitrateAdjuster::kBitrateUnderShotSensitivity = 0.99f;
const float HWBitrateAdjuster::kBitrateUnderShotThreshold = 0.3f;
const float HWBitrateAdjuster::kBitrateOverShotSensitivity = 0.99f;
const float HWBitrateAdjuster::kBitrateOverShotThreshold = 0.2f;

const float HWBitrateAdjuster::kQPSensitivity = 0.9f;
const uint32_t HWBitrateAdjuster::kMaxQPThreshold = 51;

HWBitrateAdjuster::HWBitrateAdjuster(uint32_t max_target_bitrate_bps,
                                     float min_adjusted_bitrate_pct,
                                     float max_adjusted_bitrate_pct,
                                     bool enable_dynamic_adjust_max_bitrate_pct,
                                     int low_qp_threshold,
                                     int high_qp_threshold,
                                     HWSmartBitrateHDCodecType codec_type,
                                     bool enableBitrateAdjuster,
                                     bool is_screen_share,
                                     float save_rate_per_qp,
                                     float max_save_rate,
                                     float fraction,
                                     bool allow_overshot_when_non_weak_network,
                                     float tolerant_bitrate_overshot_ratio)
    : max_target_bitrate_bps_(max_target_bitrate_bps),
      min_adjusted_bitrate_pct_(min_adjusted_bitrate_pct),
      init_max_adjusted_bitrate_pct_(max_adjusted_bitrate_pct),
      enable_dynamic_adjust_max_bitrate_pct_(enable_dynamic_adjust_max_bitrate_pct),
      low_qp_threshold_(low_qp_threshold),
      high_qp_threshold_(high_qp_threshold),
      codec_type_(codec_type),
      enableBitrateAdjuster_(enableBitrateAdjuster && !is_screen_share),
      is_screen_share_(is_screen_share),
      save_rate_per_qp_(save_rate_per_qp),
      max_save_rate_(max_save_rate),
      fraction_(fraction),
      allow_overshot_when_non_weak_network_(allow_overshot_when_non_weak_network),
      tolerant_bitrate_overshot_ratio_(tolerant_bitrate_overshot_ratio),
      bitrate_tracker_(1.5 * kBitrateUpdateIntervalMs,
                       kBytesPerMsToBitsPerSecond) {  
  Reset();
  RTC_LOG(LS_INFO) << "max_target_bitrate_bps_ : " << max_target_bitrate_bps_
                   << " min_adjusted_bitrate_pct_ : " << min_adjusted_bitrate_pct_ 
                   << " max_adjusted_bitrate_pct_ : " << max_adjusted_bitrate_pct_
                   << " enable_dynamic_adjust_max_bitrate_pct_ : " << enable_dynamic_adjust_max_bitrate_pct_
                   << " low_qp_threshold_ : " << low_qp_threshold_
                   << " high_qp_threshold_ : " << high_qp_threshold_
                   << " codec_type_ : " << codec_type_
                   << " enableBitrateAdjuster_ : " << enableBitrateAdjuster_
                   << " is_screen_share_ : " << is_screen_share_
                   << " save_rate_per_qp_ : " << save_rate_per_qp_
                   << " max_save_rate_ : " << max_save_rate_
                   << " fraction_ : " << fraction_
                   << " allow_overshot_when_non_weak_network_ : " << allow_overshot_when_non_weak_network_
                   << " tolerant_bitrate_overshot_ratio_ : " << tolerant_bitrate_overshot_ratio_;
}

BitrateAdjuster::~BitrateAdjuster() {
  smartBitrateHD_.reset();
}

void HWBitrateAdjuster::SetOriginalTargetBitrateBps(uint32_t bitrate_bps) {
  rtc::CritScope cs(&crit_);
  if (bitrate_bps < (1.0f - kBitrateTolerancePct) * max_target_bitrate_bps_) {
    if (is_non_weak_network_) {
      RTC_LOG(LS_INFO) << "network : strong -> weak";
    }
    is_non_weak_network_ = false;
  }else {
    if (!is_non_weak_network_) {
      RTC_LOG(LS_INFO) << "network : weak -> strong";
    }
    is_non_weak_network_ = true;
  }

  if (smartBitrateHD_->isEnabled()) {
    smartBitrateHD_->SetOriginalTargetBitrate(bitrate_bps);
    bitrate_bps = smartBitrateHD_->SmartBitrate();
  }
  if (!IsWithinTolerance(bitrate_bps, target_bitrate_bps_) ||
      !IsWithinTolerance(bitrate_bps, last_adjusted_target_bitrate_bps_)) {
    adjusted_bitrate_bps_ = bitrate_bps;
    last_adjusted_target_bitrate_bps_ = bitrate_bps;
  }
  target_bitrate_bps_ = bitrate_bps;
}

uint32_t HWBitrateAdjuster::GetOriginalTargetBitrateBps() const {
  rtc::CritScope cs(&crit_);
  if (smartBitrateHD_->isEnabled()) {
    return smartBitrateHD_->GetOriginalTargetBitrate();
  }else {
    return target_bitrate_bps_;
  }
}

uint32_t HWBitrateAdjuster::GetSavedTargetBitrateBps() const {
  rtc::CritScope cs(&crit_);
  return target_bitrate_bps_;
}

uint32_t HWBitrateAdjuster::GetAdjustedBitrateBps() const {
  rtc::CritScope cs(&crit_);
  if (enableBitrateAdjuster_) {
    return adjusted_bitrate_bps_;
  }else {
    return target_bitrate_bps_;
  }
}

bool HWBitrateAdjuster::isEnableHWSmartBitrateHD() const {
  rtc::CritScope cs(&crit_);
  return smartBitrateHD_->isEnabled();
}

absl::optional<uint32_t> HWBitrateAdjuster::GetEstimatedBitrateBps() {
  rtc::CritScope cs(&crit_);
  return bitrate_tracker_.Rate(rtc::TimeMillis());
}

void HWBitrateAdjuster::Update(size_t frame_size) {
  rtc::CritScope cs(&crit_);
  uint32_t current_time_ms = rtc::TimeMillis();
  bitrate_tracker_.Update(frame_size, current_time_ms);
  UpdateBitrate(current_time_ms);
}

HW_BITRATE_STATE HWBitrateAdjuster::GetBitrateState()
{
  rtc::CritScope cs(&crit_);

  if (undershot_bitrate_pct_ < kBitrateUnderShotThreshold) {
    // RTC_LOG(LS_INFO) << "current hw bitrate is under shot";
    return HW_BITRATE_UNDER_SHOT;
  } else if (overshot_bitrate_pct_ > kBitrateOverShotThreshold) {
    // RTC_LOG(LS_INFO) << "current hw bitrate is over shot";
    return HW_BITRATE_OVER_SHOT;
  }
  return HW_BITRATE_OK;
}

SCREEN_SHARE_CONTENT HWBitrateAdjuster::GetScreenShareContent()
{
  rtc::CritScope cs(&crit_);
  if (is_screen_share_) {
    HW_BITRATE_STATE bitrate_state = HW_BITRATE_OK;
    if (undershot_bitrate_pct_ < kBitrateUnderShotThreshold) {
      bitrate_state = HW_BITRATE_UNDER_SHOT;
    } else if (overshot_bitrate_pct_ > kBitrateOverShotThreshold) {
      bitrate_state = HW_BITRATE_OVER_SHOT;
    }
    if (bitrate_state == HW_BITRATE_UNDER_SHOT && avg_qp_<=low_qp_threshold_) {
      return STATIC_SCREEN_SHARE_CONTENT;
    }else if (bitrate_state == HW_BITRATE_OVER_SHOT && avg_qp_>=(low_qp_threshold_+high_qp_threshold_)/2) {
      return DYNAMIC_SCREEN_SHARE_CONTENT;
    }else return SMOOTH_SCREEN_SHARE_CONTENT;
  }else {
    return UNKNOWN_SCREEN_SHARE_CONTENT;
  }
}

void HWBitrateAdjuster::ReportQP(int qp) {
  if (qp <= 0) return;
  rtc::CritScope cs(&crit_);
  if (smartBitrateHD_->isEnabled()) {
    smartBitrateHD_->ReportQP(qp);
    uint32_t bitrate_bps = smartBitrateHD_->SmartBitrate();

    if (!IsWithinTolerance(bitrate_bps, target_bitrate_bps_) ||
        !IsWithinTolerance(bitrate_bps, last_adjusted_target_bitrate_bps_)) {
      adjusted_bitrate_bps_ = bitrate_bps;
      last_adjusted_target_bitrate_bps_ = bitrate_bps;
    }
    target_bitrate_bps_ = bitrate_bps;
  }

  if (avg_qp_ <= 0.0f) {
    avg_qp_ = low_qp_threshold_;
    return;
  }
  avg_qp_ = avg_qp_ * kQPSensitivity + (1.0 - kQPSensitivity) * qp;
}

bool HWBitrateAdjuster::IsWithinTolerance(uint32_t bitrate_bps,
                                        uint32_t target_bitrate_bps) {
  if (target_bitrate_bps == 0) {
    return false;
  }
  float delta = std::abs(static_cast<float>(bitrate_bps) -
                         static_cast<float>(target_bitrate_bps));
  float delta_pct = delta / target_bitrate_bps;
  return delta_pct < kBitrateTolerancePct;
}

uint32_t HWBitrateAdjuster::GetMinAdjustedBitrateBps() const {
  return min_adjusted_bitrate_pct_ * target_bitrate_bps_;
}

uint32_t HWBitrateAdjuster::GetMaxAdjustedBitrateBps() const {
  return max_adjusted_bitrate_pct_ * target_bitrate_bps_;
}

void HWBitrateAdjuster::Reset() {
  rtc::CritScope cs(&crit_);
  is_non_weak_network_ = true;
  smartBitrateHD_.reset(new HWSmartBitrateHD(low_qp_threshold_, high_qp_threshold_, codec_type_, is_screen_share_, save_rate_per_qp_, max_save_rate_));
  target_bitrate_bps_ = 0;
  adjusted_bitrate_bps_ = 0;
  last_adjusted_target_bitrate_bps_ = 0;
  last_bitrate_update_time_ms_ = 0;
  frames_since_last_update_ = 0;
  undershot_bitrate_pct_ = 0.0f;
  overshot_bitrate_pct_ = 0.0f;
  avg_qp_ = 0.0f;
  max_adjusted_bitrate_pct_ = init_max_adjusted_bitrate_pct_;
  adjusted_max_adjusted_bitrate_pct_ = 1.0f;
  bitrate_tracker_.Reset();
}

void HWBitrateAdjuster::UpdateBitrate(uint32_t current_time_ms) {
  uint32_t time_since_last_update_ms =
      current_time_ms - last_bitrate_update_time_ms_;
  ++frames_since_last_update_;
  if (time_since_last_update_ms < kBitrateUpdateIntervalMs ||
      frames_since_last_update_ < kBitrateUpdateFrameInterval) {
    return;
  }
  float target_bitrate_bps = target_bitrate_bps_;
  float estimated_bitrate_bps =
      bitrate_tracker_.Rate(current_time_ms).value_or(target_bitrate_bps);
  float error = target_bitrate_bps - estimated_bitrate_bps;

  if (estimated_bitrate_bps > target_bitrate_bps ||
      error > kBitrateTolerancePct * target_bitrate_bps) {

    if (enable_dynamic_adjust_max_bitrate_pct_) {
      if (error > kBitrateTolerancePct * target_bitrate_bps) {
        float now_undershot_bitrate_pct = error / target_bitrate_bps;
        RTC_LOG(LS_INFO) << "now_undershot_bitrate_pct : " << now_undershot_bitrate_pct;
        RTC_LOG(LS_INFO) << "avg_qp_ : " << avg_qp_;
        float adjustment_pct = now_undershot_bitrate_pct * fraction_;

        if (avg_qp_ >= (low_qp_threshold_ + high_qp_threshold_)/2 && avg_qp_ < high_qp_threshold_ && adjusted_max_adjusted_bitrate_pct_ <= 2.0) {
          adjusted_max_adjusted_bitrate_pct_ = std::min(adjusted_max_adjusted_bitrate_pct_ + adjustment_pct, 2.0f);
        }else if (avg_qp_ >= high_qp_threshold_ && avg_qp_ < (kMaxQPThreshold + high_qp_threshold_)/2 && adjusted_max_adjusted_bitrate_pct_ <= 3.0) {
          adjusted_max_adjusted_bitrate_pct_ = std::min(adjusted_max_adjusted_bitrate_pct_ + adjustment_pct, 3.0f);
        }else if (avg_qp_ >= (kMaxQPThreshold + high_qp_threshold_)/2 && avg_qp_ < kMaxQPThreshold && adjusted_max_adjusted_bitrate_pct_ <= 4.0) {
          adjusted_max_adjusted_bitrate_pct_ = std::min(adjusted_max_adjusted_bitrate_pct_ + adjustment_pct, 4.0f);
        }else if (avg_qp_ >= kMaxQPThreshold && adjusted_max_adjusted_bitrate_pct_ <= 5.0) {
          adjusted_max_adjusted_bitrate_pct_ = std::min(adjusted_max_adjusted_bitrate_pct_ + adjustment_pct, 5.0f);
        }

        RTC_LOG(LS_INFO) << "adjusted_max_adjusted_bitrate_pct_ : " << adjusted_max_adjusted_bitrate_pct_;
        max_adjusted_bitrate_pct_ = std::max((float)adjusted_max_adjusted_bitrate_pct_, init_max_adjusted_bitrate_pct_);
        RTC_LOG(LS_INFO) << "max_adjusted_bitrate_pct_ : " << max_adjusted_bitrate_pct_;
      }
    }

    float adjustment = fraction_ * error;
    // float adjusted_bitrate_bps = target_bitrate_bps + adjustment;
    float adjusted_bitrate_bps = adjusted_bitrate_bps_ + adjustment;

    if (allow_overshot_when_non_weak_network_) {
      if (estimated_bitrate_bps > target_bitrate_bps) {
        if (is_non_weak_network_) {
          if (estimated_bitrate_bps <= target_bitrate_bps * tolerant_bitrate_overshot_ratio_) {
            adjusted_bitrate_bps = adjusted_bitrate_bps_;
            RTC_LOG(LS_INFO) << "non_weak_network, overshot less than or equal to " << tolerant_bitrate_overshot_ratio_ << ", so keep last adjusted bitrate bps : " << adjusted_bitrate_bps;
          }else {
            adjusted_bitrate_bps = adjusted_bitrate_bps_ - fraction_ * (estimated_bitrate_bps - target_bitrate_bps * tolerant_bitrate_overshot_ratio_) / tolerant_bitrate_overshot_ratio_;
            RTC_LOG(LS_INFO) << "non_weak_network, overshot more than " << tolerant_bitrate_overshot_ratio_ << ", so adjusted bitrate bps from " << adjusted_bitrate_bps_ << " to " << adjusted_bitrate_bps;
          }
        }else {
          RTC_LOG(LS_INFO) << "weak_network, must limit overshot";
        }
      }else if(error > kBitrateTolerancePct * target_bitrate_bps) {
        if (!is_non_weak_network_) {
          max_adjusted_bitrate_pct_ = std::min((float)max_adjusted_bitrate_pct_, 1.0f);
          RTC_LOG(LS_INFO) << "weak_network, not allow upscale bitrate when undershot, so set max_adjusted_bitrate_pct to : " << max_adjusted_bitrate_pct_;
        }else {
          RTC_LOG(LS_INFO) << "non_weak_network, allow upscale bitrate when undershot";
        }
      }
    }

    float min_bitrate_bps = GetMinAdjustedBitrateBps();
    float max_bitrate_bps = GetMaxAdjustedBitrateBps();
    adjusted_bitrate_bps = std::max(adjusted_bitrate_bps, min_bitrate_bps);
    adjusted_bitrate_bps = std::min(adjusted_bitrate_bps, max_bitrate_bps);

    float last_adjusted_bitrate_bps = adjusted_bitrate_bps_;
    if (adjusted_bitrate_bps != last_adjusted_bitrate_bps) {
      RTC_LOG(LS_INFO) << "Adjusting encoder bitrate:"
                          << "\n  target_bitrate:"
                          << static_cast<uint32_t>(target_bitrate_bps)
                          << "\n  estimated_bitrate:"
                          << static_cast<uint32_t>(estimated_bitrate_bps)
                          << "\n  last_adjusted_bitrate:"
                          << static_cast<uint32_t>(last_adjusted_bitrate_bps)
                          << "\n  adjusted_bitrate:"
                          << static_cast<uint32_t>(adjusted_bitrate_bps);
      adjusted_bitrate_bps_ = adjusted_bitrate_bps;
    }
  }
  last_bitrate_update_time_ms_ = current_time_ms;
  frames_since_last_update_ = 0;
  last_adjusted_target_bitrate_bps_ = target_bitrate_bps_;

  undershot_bitrate_pct_ = kBitrateUnderShotSensitivity * undershot_bitrate_pct_ + (1.0f - kBitrateUnderShotSensitivity) * error / target_bitrate_bps;
  overshot_bitrate_pct_ = kBitrateOverShotSensitivity * overshot_bitrate_pct_ + (1.0f - kBitrateOverShotSensitivity) * (-error) / target_bitrate_bps;
  if (overshot_bitrate_pct_ >= kBitrateOverShotThreshold) {
    RTC_LOG(LS_WARNING) << "Got Bitrate OverShot -> overshot_bitrate_pct_ : " << overshot_bitrate_pct_;
  }
  if (undershot_bitrate_pct_ >= kBitrateUnderShotThreshold) {
    RTC_LOG(LS_WARNING) << "Got Bitrate UnderShot -> undershot_bitrate_pct_ : " << undershot_bitrate_pct_;
  }
}

} // namespace netease