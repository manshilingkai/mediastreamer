#pragma once

#include <stddef.h>
#include <stdint.h>
#include <memory>
#include "absl/types/optional.h"
#include "rtc_base/criticalsection.h"
#include "rtc_base/rate_statistics.h"
#include "rtc_base/thread_annotations.h"
#include "modules/video_coding/codecs/winhw/libhwcodec.h"

namespace netease {

enum HWSmartBitrateHDCodecType {
    kH264 = 0,
    kH265 = 1,
};

class HWSmartBitrateHD
{
public:
    static const float kQPSensitivity;
    static const int kMaxQPThreshold;
    static const float kBitrateTolerancePct;
    static const uint32_t kSaveRateUpdateIntervalMs;
    static const uint32_t kSaveRateUpdateFrameInterval;

    HWSmartBitrateHD(int low_qp_threshold, int high_qp_threshold, HWSmartBitrateHDCodecType codec_type = kH264, bool is_screen_share = false, float save_rate_per_qp = 0.05f, float max_save_rate = 0.0f);
    ~HWSmartBitrateHD();

    bool isEnabled();

    void SetOriginalTargetBitrate(uint32_t bitrate_bps);
    void ReportQP(int qp);
    uint32_t SmartBitrate();
    uint32_t GetOriginalTargetBitrate();
private:
    bool IsWithinTolerance(uint32_t bitrate_bps, uint32_t target_bitrate_bps);
    bool isUpdateSavaRate();

    int low_qp_threshold_;
    int high_qp_threshold_;
    const HWSmartBitrateHDCodecType codec_type_;
    const float save_rate_per_qp_;
    const float max_save_rate_;

    uint32_t original_target_bitrate_bps_;

    float avg_qp_;
    uint32_t target_bitrate_bps_;

    float save_rate_;
    uint32_t last_save_rate_update_time_ms_;
    uint32_t frames_since_last_update_;
};

enum SCREEN_SHARE_CONTENT
{
    UNKNOWN_SCREEN_SHARE_CONTENT = -1,
    STATIC_SCREEN_SHARE_CONTENT = 0,  // keep resolution, can drop frame
    SMOOTH_SCREEN_SHARE_CONTENT = 1,  // keep balance, can drop frame and scale down resolution together
    DYNAMIC_SCREEN_SHARE_CONTENT = 2, // keep framerate, can scale down resolution
};

class HWBitrateAdjuster {
 public:
  HWBitrateAdjuster(uint32_t max_target_bitrate_bps,
                    float min_adjusted_bitrate_pct,
                    float max_adjusted_bitrate_pct,
                    bool enable_dynamic_adjust_max_bitrate_pct,
                    int low_qp_threshold,
                    int high_qp_threshold,
                    HWSmartBitrateHDCodecType codec_type = kH264,
                    bool enableBitrateAdjuster = true,
                    bool is_screen_share = false,
                    float save_rate_per_qp = 0.03f,
                    float max_save_rate = 0.0f,
                    float fraction = 0.5f,
                    bool allow_overshot_when_non_weak_network = true,
                    float tolerant_bitrate_overshot_ratio = 2.0);
  virtual ~HWBitrateAdjuster();

  static const uint32_t kBitrateUpdateIntervalMs;
  static const uint32_t kBitrateUpdateFrameInterval;
  static const float kBitrateTolerancePct;
  static const float kBytesPerMsToBitsPerSecond;

  static const float kBitrateUnderShotSensitivity;
  static const float kBitrateUnderShotThreshold;
  static const float kBitrateOverShotSensitivity;
  static const float kBitrateOverShotThreshold;

  static const float kQPSensitivity;
  static const uint32_t kMaxQPThreshold;

  void SetOriginalTargetBitrateBps(uint32_t bitrate_bps);
  uint32_t GetOriginalTargetBitrateBps() const;

  uint32_t GetSavedTargetBitrateBps() const;

  uint32_t GetAdjustedBitrateBps() const;

  bool isEnableHWSmartBitrateHD() const;

  absl::optional<uint32_t> GetEstimatedBitrateBps();

  void Update(size_t frame_size);

  HW_BITRATE_STATE GetBitrateState();

  SCREEN_SHARE_CONTENT GetScreenShareContent();

  void ReportQP(int qp);

 private:
  bool IsWithinTolerance(uint32_t bitrate_bps, uint32_t target_bitrate_bps);

  uint32_t GetMinAdjustedBitrateBps() const RTC_EXCLUSIVE_LOCKS_REQUIRED(crit_);
  uint32_t GetMaxAdjustedBitrateBps() const RTC_EXCLUSIVE_LOCKS_REQUIRED(crit_);

  void Reset();
  void UpdateBitrate(uint32_t current_time_ms)
      RTC_EXCLUSIVE_LOCKS_REQUIRED(crit_);

  rtc::CriticalSection crit_;
  const uint32_t max_target_bitrate_bps_;
  const float min_adjusted_bitrate_pct_;
  const float init_max_adjusted_bitrate_pct_;
  const bool enable_dynamic_adjust_max_bitrate_pct_;
  const int low_qp_threshold_;
  const int high_qp_threshold_;
  const HWSmartBitrateHDCodecType codec_type_;
  const bool enableBitrateAdjuster_;
  const bool is_screen_share_;
  const float save_rate_per_qp_;
  const float max_save_rate_;
  const float fraction_;
  const bool allow_overshot_when_non_weak_network_;
  const float tolerant_bitrate_overshot_ratio_;
  bool is_non_weak_network_ RTC_GUARDED_BY(crit_);
  std::unique_ptr<HWSmartBitrateHD> smartBitrateHD_ RTC_GUARDED_BY(crit_);
  volatile uint32_t target_bitrate_bps_ RTC_GUARDED_BY(crit_);
  volatile uint32_t adjusted_bitrate_bps_ RTC_GUARDED_BY(crit_);
  volatile uint32_t last_adjusted_target_bitrate_bps_ RTC_GUARDED_BY(crit_);
  webrtc::RateStatistics bitrate_tracker_ RTC_GUARDED_BY(crit_);
  uint32_t last_bitrate_update_time_ms_ RTC_GUARDED_BY(crit_);
  uint32_t frames_since_last_update_ RTC_GUARDED_BY(crit_);

  volatile float undershot_bitrate_pct_ RTC_GUARDED_BY(crit_);
  volatile float overshot_bitrate_pct_ RTC_GUARDED_BY(crit_);

  volatile float avg_qp_ RTC_GUARDED_BY(crit_);
  volatile float max_adjusted_bitrate_pct_ RTC_GUARDED_BY(crit_);
  volatile float adjusted_max_adjusted_bitrate_pct_ RTC_GUARDED_BY(crit_);
};

} // namespace netease