/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#ifndef COMMON_VIDEO_H265_H265_BITSTREAM_PARSER_H_
#define COMMON_VIDEO_H265_H265_BITSTREAM_PARSER_H_
#include <stddef.h>
#include <stdint.h>

#include "absl/types/optional.h"
#include "common_video/h265/h265_common.h"
#include "common_video/h265/h265_pps_parser.h"
#include "common_video/h265/h265_sps_parser.h"
#include "common_video/h265/h265_vps_parser.h"
#include "common_video/h264/sei_parser.h"
#include "common_video/h264/h264_common.h"

namespace webrtc {

typedef struct H265PredWeightTable {
    uint8_t luma_log2_weight_denom;
    int16_t chroma_log2_weight_denom;

    int16_t luma_weight_l0[16];
    int16_t chroma_weight_l0[16][2];
    int16_t chroma_weight_l1[16][2];
    int16_t luma_weight_l1[16];

    int16_t luma_offset_l0[16];
    int16_t chroma_offset_l0[16][2];

    int16_t luma_offset_l1[16];
    int16_t chroma_offset_l1[16][2];
} H265PredWeightTable;

// Stateful H265 bitstream parser (due to SPS/PPS). Used to parse out QP values
// from the bitstream.
// TODO(pbos): Unify with RTP SPS parsing and only use one H265 parser.
// TODO(pbos): If/when this gets used on the receiver side CHECKs must be
// removed and gracefully abort as we have no control over receive-side
// bitstreams.
class H265BitstreamParser {
 public:
  H265BitstreamParser();
  ~H265BitstreamParser();

  // These are here for backwards-compatability for the time being.
  void ParseBitstream(const uint8_t* bitstream, size_t length, bool backup_vpsspspps = false);

  // NOTE(Think): here amend sps to align actual width & height in sps
  void ParseBitstreamAndRewriteSpsIfNeeded(std::vector<uint8_t>& buffer, bool backup_vpsspspps = false);
  void SetWantedResolution(uint32_t width, uint32_t height) {
    target_width_ = width;
    target_height_ = height;
  }
  
  bool GetLastSliceQp(int* qp) const;

  // Get the last Resolution from the parsed bitstream.
  bool GetLastResolution(int* w, int* h) const;

  bool isLastIdr(bool &hasVps, bool &hasSps, bool &hasPps) const;

  std::vector<H265::NaluIndex> GetLastNaluIndices() const;
  
  absl::optional<SeiParser::SeiState>& ParsedSei() { return sei_; }

  // return true: bitstream size is enough for sei
  // return false: bitstream size is not enough for sei, realloc actually inside
  bool AppendSei(const std::vector<uint8_t> &sei,
                 uint32_t sei_type,
                 uint8_t** bitstream,
                 size_t* length,
                 size_t* size);

  unsigned char* GetLastVps(size_t &vps_size);
  unsigned char* GetLastSps(size_t &sps_size);
  unsigned char* GetLastPps(size_t &pps_size);

 protected:
  enum Result {
    kOk,
    kInvalidStream,
    kUnsupportedStream,
    kHasValidSei,
    kBufferChanged,
  };

  Result h265_pred_weight_table(rtc::BitBuffer& slice_reader, uint32_t* ref_count, uint32_t slice_type, H265PredWeightTable *pwt);
  Result ParseSlice(const uint8_t* slice, size_t length, SeiParser::SeiState &sei, const uint8_t* nal, size_t nal_size, bool backup_vpsspspps, H264::BufferCopy* buffer = nullptr);
  Result ParseNonParameterSetNalu(const uint8_t* source,
                                  size_t source_length,
                                  uint8_t nalu_type);

  uint32_t CalcNumPocTotalCurr(uint32_t num_long_term_sps,
                               uint32_t num_long_term_pics,
                               const std::vector<uint32_t> lt_idx_sps,
                               const std::vector<uint32_t> used_by_curr_pic_lt_flag,
                               uint32_t short_term_ref_pic_set_sps_flag,
                               uint32_t short_term_ref_pic_set_idx,
                               const H265SpsParser::ShortTermRefPicSet& short_term_ref_pic_set);

  // SPS/PPS state, updated when parsing new SPS/PPS, used to parse slices.
  absl::optional<H265VpsParser::VpsState> vps_;
  absl::optional<H265SpsParser::SpsState> sps_;
  absl::optional<H265PpsParser::PpsState> pps_;
  absl::optional<SeiParser::SeiState> sei_;

  // Last parsed slice QP.
  absl::optional<int32_t> last_slice_qp_delta_;

  bool isIdr = false;
  bool isVps = false;
  bool isSps = false;
  bool isPps = false;
  
  std::vector<H265::NaluIndex> nalu_indices;

  unsigned char* last_vps_data = NULL;
  size_t last_vps_size = 0;  
  unsigned char* last_sps_data = NULL;
  size_t last_sps_size = 0;
  unsigned char* last_pps_data = NULL;
  size_t last_pps_size = 0;
  // NOTE (Think): 0 means no nedd to amend sps;
  uint32_t target_width_ = 0;
  uint32_t target_height_ = 0;
};

}  // namespace webrtc

#endif  // COMMON_VIDEO_H265_H265_BITSTREAM_PARSER_H_
