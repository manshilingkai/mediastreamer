/*
 *  Copyright 2017 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

package com.netease.lava.webrtc;

import android.annotation.TargetApi;
import android.graphics.Matrix;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.opengl.GLES20;
import android.os.Bundle;
import android.os.Build;
import android.view.Surface;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Deque;
import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import com.netease.lava.webrtc.ThreadUtils.ThreadChecker;
import com.netease.lava.webrtc.device.AndroidDeviceInfo;
import com.netease.lava.webrtc.device.HardwareLevel;
import android.os.SystemClock;
import com.netease.lava.webrtc.bitrate.BitrateAdjuster;
import com.netease.lava.webrtc.bitrate.BaseBitrateAdjuster;
import com.netease.lava.webrtc.bitrate.DynamicBitrateAdjuster;
import com.netease.lava.webrtc.bitrate.FramerateBitrateAdjuster;
import com.netease.lava.webrtc.bitrate.NewDynamicBitrateAdjuster;
import static com.netease.lava.webrtc.MediaCodecUtils.QCOM_PREFIX;
import static com.netease.lava.webrtc.MediaCodecUtils.MTK_PREFIX;
import static com.netease.lava.webrtc.MediaCodecUtils.EXYNOS_PREFIX;
import static com.netease.lava.webrtc.MediaCodecUtils.IMG_PREFIX;
import static com.netease.lava.webrtc.MediaCodecUtils.HISI_PREFIX;
import static com.netease.lava.webrtc.MediaCodecUtils.K3_PREFIX;
import static com.netease.lava.webrtc.MediaCodecUtils.AMLOGIC_PREFIX;
import static com.netease.lava.webrtc.MediaCodecUtils.RK_PREFIX;
import static com.netease.lava.webrtc.MediaCodecUtils.C2_QCOM_PREFIX;
import static com.netease.lava.webrtc.MediaCodecUtils.C2_HISI_PREFIX;
import static com.netease.lava.webrtc.MediaCodecUtils.C2_MTK_PREFIX;
import static com.netease.lava.webrtc.MediaCodecUtils.C2_EXYNOS_PREFIX;
import java.util.Arrays;
import java.util.List;

/**
 * Android hardware video encoder.
 *
 * @note This class is only supported on Android Kitkat and above.
 */
@TargetApi(19)
@SuppressWarnings("deprecation") // Cannot support API level 19 without using deprecated methods.
public class HardwareVideoEncoder implements VideoEncoder {
  private static final String TAG = "HardwareVideoEncoder";

  // Bitrate modes - should be in sync with OMX_VIDEO_CONTROLRATETYPE defined
  // in OMX_Video.h
  private static final int Video_ControlRateVariable = 1;
  private static final int VIDEO_ControlRateConstant = 2;
  // Key associated with the bitrate control mode value (above). Not present as a MediaFormat
  // constant until API level 21.
  private static final String KEY_BITRATE_MODE = "bitrate-mode";

  private static final String KEY_HISI_EXT_CODEC_NON_REF_P_FRAMES_SUPPORTED = "vendor.hisi.hisi-ext-codec-non-ref-p-frames-supported";
  private static final String KEY_HISI_EXT_CODEC_NON_REF_P_FRAMES = "vendor.hisi.hisi-ext-codec-non-ref-p-frames";

  private static final int VIDEO_AVC_PROFILE_HIGH = 8;
  private static final int VIDEO_AVC_LEVEL_3 = 0x100;

  private static final int MAX_VIDEO_FRAMERATE = 30;

  // See MAX_ENCODER_Q_SIZE in androidmediaencoder.cc.
  private static final int MAX_ENCODER_Q_SIZE = 2;

  private static final int MEDIA_CODEC_RELEASE_TIMEOUT_MS = 5000;
  private static final int DEQUEUE_OUTPUT_BUFFER_TIMEOUT_US = 100000;

  private static final int  AUTO_BR_ADJUSTER = -1;
  private static final int  BASE_BR_ADJUSTER = 0;
  private static final int  DYNAMIC_BR_ADJUSTER = 1;
  private static final int  FRAMERATE_BR_ADJUSTER = 2;

  private final int kLowVp8QpThreshold = 29;
  private final int kHighVp8QpThreshold = 95;

  private final int kLowH264QpThreshold = 30;
  private final int kHighH264QpThreshold = 37;

  private final int kLowH265QpThreshold = 30;
  private final int kHighH265QpThreshold = 37;

  private final int kLowH264QpThresholdForHWSmartBitrateHD = 27;
  private final int kHighH264QpThresholdForHWSmartBitrateHD = 37;

  private final int kLowH265QpThresholdForHWSmartBitrateHD = 27;
  private final int kHighH265QpThresholdForHWSmartBitrateHD = 37;

  private final int kOutputThreadExceptionThreshold = 5;

  private final int kMinCodecWidthAlign = 16;
  private final int kMinCodecHeightAlign = 4;

  // --- Initialized on construction.
  private final MediaCodecWrapperFactory mediaCodecWrapperFactory;
  @Nullable private final String codecName;
  private final VideoCodecType codecType;
  private final int surfaceColorFormat;
  private final int yuvColorFormat;
  private final YuvFormat yuvFormat;
  private final Map<String, String> params;
  private final int keyFrameIntervalSec; // Base interval for generating key frames.
  // Interval at which to force a key frame. Used to reduce color distortions caused by some
  // Qualcomm video encoders.
  private final long forcedKeyFrameNs;
  private BitrateAdjuster bitrateAdjuster;
  // EGL context shared with the application.  Used to access texture inputs.
  private final EglBase14.Context sharedContext;

  // Drawer used to draw input textures onto the codec's input surface.
  private final GlRectDrawer textureDrawer = new GlRectDrawer();
  private final VideoFrameDrawer videoFrameDrawer = new VideoFrameDrawer();
  // A queue of EncodedImage.Builders that correspond to frames in the codec.  These builders are
  // pre-populated with all the information that can't be sent through MediaCodec.
  private final BlockingDeque<EncodedImage.Builder> outputBuilders = new LinkedBlockingDeque<>();

  private final ThreadChecker encodeThreadChecker = new ThreadChecker();
  private final ThreadChecker outputThreadChecker = new ThreadChecker();

  // --- Set on initialize and immutable until release.
  private Callback callback;
  private boolean automaticResizeOn;
  private static boolean isScreenCast;

  // --- Valid and immutable while an encoding session is running.
  @Nullable private MediaCodecWrapper codec;
  // Thread that delivers encoded frames to the user callback.
  @Nullable private Thread outputThread;

  // EGL base wrapping the shared texture context.  Holds hooks to both the shared context and the
  // input surface.  Making this base current allows textures from the context to be drawn onto the
  // surface.
  @Nullable private EglBase14 textureEglBase;
  // Input surface for the codec.  The encoder will draw input textures onto this surface.
  @Nullable private Surface textureInputSurface;

  private int width;
  private int height;
  private int actualWidth;
  private int actualHeight;
  private boolean useSurfaceMode;

  // --- Only accessed from the encoding thread.
  // Presentation timestamp of the last requested (or forced) key frame.
  private long lastKeyFrameNs;

  // --- Only accessed on the output thread.
  // Contents of the last observed config frame output by the MediaCodec. Used by H.264.
  @Nullable private ByteBuffer configBuffer;
  private int adjustedBitrate;

  // Whether the encoder is running.  Volatile so that the output thread can watch this value and
  // exit when the encoder stops.
  private volatile boolean running;
  // Any exception thrown during shutdown.  The output thread releases the MediaCodec and uses this
  // value to send exceptions thrown during release back to the encoder thread.
  @Nullable private volatile Exception shutdownException;

  private int fallbackResolution;

  //Dynamic bitrate adjust
  private int targetQosBitrateBps ;
  private int targetFPS ; // 目前不参考qos 的fps调整;

  // NOTE (qijiyue): callback "onEncodedFrame" on encode thread; do not create outputThread;
  private boolean syncMode;
  private int dequeOutputTimeoutUs;

  // NOTE (shilingkai): svc
  private boolean isSupportSvc;
  private int bitrateMode; // 0:auto 1:VBR 2:CBR
  private boolean supportResetEncoderIfRequestForceKeyFrameNoRespond;
  private float maxSaveRate;
  private boolean requestingForceKeyFrame;
  private boolean gotKeyFrameInRequestingKeyFramePeriodTime;
  private boolean resetEncoderIfRequestForceKeyFrame;
  // NOTE (qijiyue): align width in encoder
  private boolean enableAlignment;
  private int codecWidthAlign;
  private int codecHeightAlign;
	  
  // NOTE (wangyu): Use surface mode for encoding even when the input is in YUV format.
  // -1: 未下发，0：下发关，1：下发开，2：本地策略开
  private int yuvSurfaceMode;

  private boolean forceHardwareEncode;

  private MediaCodecInfo.CodecCapabilities capabilities;

  private volatile boolean outputThreadError;

  private int outputThreadExceptionCount;

  private int hisiExtCodecNonRefPFrames = 1;

  /**
   * Creates a new HardwareVideoEncoder with the given codecName, codecType, colorFormat, key frame
   * intervals, and bitrateAdjuster.
   *
   * @param codecName the hardware codec implementation to use
   * @param codecType the type of the given video codec (eg. VP8, VP9, or H264)
   * @param surfaceColorFormat color format for surface mode or null if not available
   * @param yuvColorFormat color format for bytebuffer mode
   * @param keyFrameIntervalSec interval in seconds between key frames; used to initialize the codec
   * @param forceKeyFrameIntervalMs interval at which to force a key frame if one is not requested;
   *     used to reduce distortion caused by some codec implementations
   * @param bitrateAdjuster algorithm used to correct codec implementations that do not produce the
   *     desired bitrates
   * @throws IllegalArgumentException if colorFormat is unsupported
   */
  public HardwareVideoEncoder(MediaCodecWrapperFactory mediaCodecWrapperFactory,
                              VideoCodecType codecType, Map<String, String> params, EglBase14.Context sharedContext,
                              CompatVideoCodecInfo compatInfo, MediaCodecInfo.CodecCapabilities capabilities) {
    this.mediaCodecWrapperFactory = mediaCodecWrapperFactory;
    this.codecName = compatInfo.getCodecName();
    this.codecType = codecType;
    this.surfaceColorFormat = compatInfo.getEnSurfaceColorFormat();
    this.yuvColorFormat = compatInfo.getEnYUVColorFormat();
    this.yuvFormat = YuvFormat.valueOf(yuvColorFormat);
    this.params = params;
    this.keyFrameIntervalSec = compatInfo.getKeyFrameIntervalSec();
    this.forcedKeyFrameNs = TimeUnit.MILLISECONDS.toNanos(compatInfo.getForceKeyFrameIntervalMs());
    this.bitrateAdjuster = new BaseBitrateAdjuster();
    this.sharedContext = sharedContext;
    this.fallbackResolution = compatInfo.getEncFallbackResolution();
    this.capabilities = capabilities;
    this.yuvSurfaceMode = compatInfo.getEncYuvSurfaceMode();
    if(codecType == VideoCodecType.H264) {
      this.forceHardwareEncode = compatInfo.getForceHardwareEncodeingForH264() == 1;
    } else if(codecType == VideoCodecType.H265) {
      this.forceHardwareEncode = compatInfo.getForceHardwareEncodeingForH265() == 1;
    } else {
      this.forceHardwareEncode = false;
    }
    this.codecWidthAlign = 0;
    this.codecHeightAlign = 0;

    // Allow construction on a different thread.
    encodeThreadChecker.detachThread();

    Logging.d(TAG,"ctor");
  }

   /**
   * @param isScreenCast the isScreenCast to set
   */
  public static void setScreenCast(boolean isScreenCast) {
    HardwareVideoEncoder.isScreenCast = isScreenCast;
  }

  private int setBitrateAdjusterTypeForSomeModel(String codecName, int bitrateAdjusterType)
  {
    if (codecName.startsWith(MTK_PREFIX)) {
        // bitrateAdjusterType = BASE_BR_ADJUSTER;
    }else if (codecName.startsWith(EXYNOS_PREFIX)) {
      if (Build.MODEL.equalsIgnoreCase("MX4 Pro")) {
        // bitrateAdjusterType = BASE_BR_ADJUSTER;
      } else if (Build.MANUFACTURER.equalsIgnoreCase("vivo")
          && Build.MODEL.equalsIgnoreCase("V1938CT")) {
        // bitrateAdjusterType = BASE_BR_ADJUSTER;
      } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
        // bitrateAdjusterType = BASE_BR_ADJUSTER;
      } else {
        bitrateAdjusterType = FRAMERATE_BR_ADJUSTER;
      }
    }else if (codecName.startsWith(IMG_PREFIX)) {
      if (Build.HARDWARE.equalsIgnoreCase("hi6250")) {
        // bitrateAdjusterType = BASE_BR_ADJUSTER;
      } else {
        bitrateAdjusterType = FRAMERATE_BR_ADJUSTER;
      }
    }else if (codecName.startsWith(HISI_PREFIX)) {
      if (Build.HARDWARE.equalsIgnoreCase("kirin9000")) {
        bitrateAdjusterType = FRAMERATE_BR_ADJUSTER;
      }
        // bitrateAdjusterType = BASE_BR_ADJUSTER;
    }else if (codecName.startsWith(K3_PREFIX)) {
      bitrateAdjusterType = FRAMERATE_BR_ADJUSTER;
    }else if (codecName.startsWith(AMLOGIC_PREFIX)) {
      bitrateAdjusterType = FRAMERATE_BR_ADJUSTER;
    }else if (codecName.startsWith(RK_PREFIX)) {
      bitrateAdjusterType = FRAMERATE_BR_ADJUSTER;
    }

    return bitrateAdjusterType;
  }

  private BitrateAdjuster createBitrateAdjuster(int maxTargetBitrateBps, int width, int height, VideoCodecType type, String codecName,int bitrateAdjusterType, float minAdjustedBitratePct, float maxAdjustedBitratePct, boolean isScreenShare, float saveRatePerQp, float maxSaveRate, boolean allowOverShotWhenNonWeakNetwork) {

    Logging.d(TAG, "bitrate Adjuster type: " + bitrateAdjusterType);
    Logging.d(TAG, "screen share: " + isScreenShare);

    int workbitrateAdjusterType = bitrateAdjusterType;
    if (workbitrateAdjusterType != BASE_BR_ADJUSTER && workbitrateAdjusterType != DYNAMIC_BR_ADJUSTER && workbitrateAdjusterType != FRAMERATE_BR_ADJUSTER) {
      workbitrateAdjusterType = AUTO_BR_ADJUSTER;
    }
    
    if (workbitrateAdjusterType == AUTO_BR_ADJUSTER) {
      if (isScreenShare) {
        workbitrateAdjusterType = BASE_BR_ADJUSTER;
      }else {
        workbitrateAdjusterType = DYNAMIC_BR_ADJUSTER;
      }

      Logging.d(TAG, "before setBitrateAdjusterTypeForSomeModel : " + workbitrateAdjusterType);
      workbitrateAdjusterType = setBitrateAdjusterTypeForSomeModel(codecName, workbitrateAdjusterType);
      Logging.d(TAG, "after setBitrateAdjusterTypeForSomeModel : " + workbitrateAdjusterType);
    }

    Logging.d(TAG, "final work bitrate Adjuster type: " + workbitrateAdjusterType);

    int codec_type = 0;
    if (type == VideoCodecType.H264) {
      codec_type = 0;
    }else if (type == VideoCodecType.H265) {
      codec_type = 1;
    }

    BitrateAdjuster bitrateAdjuster = null;
    switch (workbitrateAdjusterType){
      case BASE_BR_ADJUSTER:
        bitrateAdjuster = new NewDynamicBitrateAdjuster(maxTargetBitrateBps, width, height, codec_type, minAdjustedBitratePct, maxAdjustedBitratePct, false, isScreenShare, saveRatePerQp, maxSaveRate, allowOverShotWhenNonWeakNetwork);
        break;
      case DYNAMIC_BR_ADJUSTER:
        bitrateAdjuster = new NewDynamicBitrateAdjuster(maxTargetBitrateBps, width, height, codec_type, minAdjustedBitratePct, maxAdjustedBitratePct, true, isScreenShare, saveRatePerQp, maxSaveRate, allowOverShotWhenNonWeakNetwork);
        break;
      case FRAMERATE_BR_ADJUSTER:
        bitrateAdjuster = new FramerateBitrateAdjuster(maxTargetBitrateBps, width, height, codec_type, minAdjustedBitratePct, 1.0f, true, isScreenShare, saveRatePerQp, maxSaveRate, allowOverShotWhenNonWeakNetwork);
        break;
      default:
        bitrateAdjuster = new NewDynamicBitrateAdjuster(maxTargetBitrateBps, width, height, codec_type, minAdjustedBitratePct, maxAdjustedBitratePct, false, isScreenShare, saveRatePerQp, maxSaveRate, allowOverShotWhenNonWeakNetwork);
        break;
    }

    return bitrateAdjuster;
  }

  @Override
  public VideoCodecStatus initEncode(Settings settings, Callback callback) {
    encodeThreadChecker.checkIsOnValidThread();

    this.callback = callback;
    automaticResizeOn = settings.automaticResizeOn;
    this.actualWidth = settings.width;
    this.actualHeight = settings.height;
    useSurfaceMode = canUseSurface();
    syncMode = settings.syncMode;
    dequeOutputTimeoutUs = settings.syncMode ? 0 : DEQUEUE_OUTPUT_BUFFER_TIMEOUT_US;
    yuvSurfaceMode = useSurfaceMode ? yuvSurfaceMode : 0;
    isSupportSvc = settings.enableSvc;
    bitrateMode = settings.bitrateMode;
    supportResetEncoderIfRequestForceKeyFrameNoRespond = settings.resetEncoderIfRequestForceKeyFrameNoRespond;
    if (isSupportSvc) {
      if (codecName.startsWith(QCOM_PREFIX) || codecName.startsWith(C2_QCOM_PREFIX)) {
        isSupportSvc = (settings.chipSelectionForSVC & 0x01)!=0 ? true : false;
      }else if (codecName.startsWith(HISI_PREFIX) || codecName.startsWith(C2_HISI_PREFIX)) {
        isSupportSvc = (settings.chipSelectionForSVC & 0x02)!=0 ? true : false;
      }else if (codecName.startsWith(MTK_PREFIX) || codecName.startsWith(C2_MTK_PREFIX)) {
        isSupportSvc = (settings.chipSelectionForSVC & 0x04)!=0 ? true : false;
      }else if (codecName.startsWith(EXYNOS_PREFIX) || codecName.startsWith(C2_EXYNOS_PREFIX)) {
        isSupportSvc = (settings.chipSelectionForSVC & 0x08)!=0 ? true : false;
      }else {
        isSupportSvc = (settings.chipSelectionForSVC & 0x10)!=0 ? true : false;
      }
      Logging.i(TAG, "codecName : " + codecName + " chipSelectionForSVC : " + settings.chipSelectionForSVC + " isSupportSvc : " + isSupportSvc);
    }
    enableAlignment = settings.enableAlignment;
    alignResolutionIfNeeded();

    boolean isSizeSupport = MediaCodecUtils.checkSize(this.capabilities, this.width, this.height,this.fallbackResolution);
    if (!isSizeSupport && !forceHardwareEncode) {
      Logging.w(TAG, " initEncode: " + width + " x " + height +" isSizeSupport: "+isSizeSupport + " FALLBACK_SOFTWARE");
      return VideoCodecStatus.FALLBACK_SOFTWARE;
    }

    // 硬件编码器 fallbackResolution 下发控制
    if((this.fallbackResolution > 0 && actualWidth * actualHeight < fallbackResolution)) {
      Logging.w(TAG, " initEncode: " + actualWidth + " x " + actualHeight + " fallbackResolution "+fallbackResolution
              + " SDK_INT: " + Build.VERSION.SDK_INT+" FALLBACK_SOFTWARE");
      return VideoCodecStatus.FALLBACK_SOFTWARE;
    }


    this.bitrateAdjuster = createBitrateAdjuster(settings.maxTargetBitrateBps, this.width, this.height, codecType, codecName, settings.bitrateAdjusterType, settings.minAdjustedBitratePct, settings.maxAdjustedBitratePct, settings.isScreenShare, settings.saveRatePerQp, settings.maxSaveRate, settings.allowOverShotWhenNonWeakNetwork);
    this.maxSaveRate = settings.maxSaveRate;

    if (settings.startBitrate != 0 && settings.maxFramerate != 0) {
      targetFPS = settings.maxFramerate;
      targetQosBitrateBps = settings.startBitrate * 1000;

      boolean isFrameRateSupport = MediaCodecUtils.checkFrameRate(this.capabilities,targetFPS);
      if(!isFrameRateSupport)
        Logging.w(TAG, "initEncode: target framerate: " + targetFPS + "is not supported by the encoder.");

      boolean isBitrateSupport = MediaCodecUtils.checkBitrate(this.capabilities,targetQosBitrateBps);
      if(!isBitrateSupport)
        Logging.w(TAG, "initEncode: target bps: "+ targetQosBitrateBps +" is not within the supported bitrate range.");

      bitrateAdjuster.setTargets(settings.startBitrate * 1000, settings.maxFramerate);
    }
    adjustedBitrate = bitrateAdjuster.getAdjustedBitrateBps();

    Logging.i(TAG,
        "initEncode, codecName:" + codecName + " isSizeSupport: " + isSizeSupport + " codecType: " + codecType
            + " surfaceColorFormat: " + surfaceColorFormat
            + " yuvColorFormat: " + yuvColorFormat + " params: " + params.toString() + " keyFrameIntervalSec: "
            + keyFrameIntervalSec + " forcedKeyFrameNs: " + forcedKeyFrameNs + " sharedContext: " + sharedContext
            + " " + width + " x " + height + ". @ " + settings.startBitrate
            + "kbps. Fps: " + settings.maxFramerate + " Use surface mode: " + useSurfaceMode
            + " maxSupportedInstances: " + MediaCodecUtils.getMaxSupportedInstances(this.capabilities)
            + " currentCodecInstances: " + MediaCodecUtils.getCodecInstances(true, this.codecName)
            + " syncMode: " + settings.syncMode
            + " yuvSurfaceMode: " + yuvSurfaceMode
            + " forceHardwareEncode: " + forceHardwareEncode
            + ", enableAlignment: " + settings.enableAlignment);
    return initEncodeInternal();
  }

  @Override
  public boolean isSupportSVC() {
    encodeThreadChecker.checkIsOnValidThread();
    return isSupportSvc;
  }

  @Override
  public boolean isEnableSVC() {
    encodeThreadChecker.checkIsOnValidThread();
    if (codecName.startsWith(HISI_PREFIX) || codecName.startsWith(C2_HISI_PREFIX)) {
      return isSupportSvc && hisiExtCodecNonRefPFrames>0;
    }else return isSupportSvc;
  }

  @Override
  public void setSvcParam(boolean enable_svc) {
    encodeThreadChecker.checkIsOnValidThread();
    if (isSupportSvc) {
      if (codecName.startsWith(HISI_PREFIX) || codecName.startsWith(C2_HISI_PREFIX)) {
        int hisi_ext_codec_non_ref_p_frames = enable_svc?1:0;
        if (hisi_ext_codec_non_ref_p_frames != hisiExtCodecNonRefPFrames) {
          hisiExtCodecNonRefPFrames = hisi_ext_codec_non_ref_p_frames;
          Bundle bundle = new Bundle();
          bundle.putInt(KEY_HISI_EXT_CODEC_NON_REF_P_FRAMES, hisiExtCodecNonRefPFrames);
          codec.setParameters(bundle);
        }
      }
    }
  }

  private VideoCodecStatus initEncodeInternal() {
    encodeThreadChecker.checkIsOnValidThread();

    lastKeyFrameNs = -1;

    // InstanceCount检测放在Internal中，防止无法捕捉内部reinit流程导致的实例数量变化
    boolean isWithinInstancesRange = MediaCodecUtils.checkCodecInstances(this.capabilities,true,this.codecName);
    if (!isWithinInstancesRange) {
      Logging.w(TAG, " initEncodeInternal: "
              +" currentCodecInstances: "+ MediaCodecUtils.getCodecInstances(true,this.codecName)
              +" maxSupportedInstances: "    + MediaCodecUtils.getMaxSupportedInstances(this.capabilities)
              +" isWithinInstancesRange: "    + isWithinInstancesRange);
      // FIXME: 若codec并发实例过多时遇到问题则将回退打开
      // return VideoCodecStatus.FALLBACK_SOFTWARE;
    }

    // 分辨率不对齐的情况下，MTK芯片本地开启yux-tex通道
    updateYuvSurfaceModeForMtk(width,height);

    try {
      codec = mediaCodecWrapperFactory.createByCodecName(codecName);
    } catch (Exception e) {
      Logging.e(TAG, "Cannot create media encoder " + codecName);
      return VideoCodecStatus.FALLBACK_SOFTWARE;
    }
    MediaCodecUtils.addCodecInstances(true,this.codecName);
    final int colorFormat = useSurfaceMode ? surfaceColorFormat : yuvColorFormat;
    try {
      MediaFormat format = MediaFormat.createVideoFormat(codecType.mimeType(), width, height);
      format.setInteger(MediaFormat.KEY_BIT_RATE, adjustedBitrate);
      format.setInteger(MediaFormat.KEY_COLOR_FORMAT, colorFormat);
      format.setInteger(MediaFormat.KEY_FRAME_RATE, bitrateAdjuster.getTargetsFrameRate());
      format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, keyFrameIntervalSec);

      if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
          // h264type.nBFrames = mLatency == 0 ? 1 : std::min(1U, mLatency - 1);
          format.setInteger(MediaFormat.KEY_LATENCY, 1);
      }

      if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          format.setInteger(MediaFormat.KEY_PRIORITY, 0);
      }

      if (codecType == VideoCodecType.H264) {
        if (isSupportSvc) {
          if (codecName.startsWith(HISI_PREFIX) || codecName.startsWith(C2_HISI_PREFIX)) {
            Logging.d(TAG, "hisi chip, try set private param to enable svc");
            format.setInteger(KEY_HISI_EXT_CODEC_NON_REF_P_FRAMES_SUPPORTED, 1);
            format.setInteger(KEY_HISI_EXT_CODEC_NON_REF_P_FRAMES, 1);
          }else {
            if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
              format.setString(MediaFormat.KEY_TEMPORAL_LAYERING, "android.generic.2+0");
            }else {
              isSupportSvc = false;
            }
          }
        }

        String compatProfile = params.get(VideoCodecInfo.H264_FMTP_PROFILE_ID);
        String compatLevel = params.get(VideoCodecInfo.H264_FMTP_LEVEL_ID);
        if(compatProfile != null 
                && compatLevel != null) {

          Logging.d(TAG, "using compat profile: " + compatProfile + " level: "+ compatLevel);   
          format.setInteger("profile", Integer.parseInt(compatProfile));
          format.setInteger("level", Integer.parseInt(compatLevel));
        }else {
            String profileLevelId = params.get(VideoCodecInfo.H264_FMTP_PROFILE_LEVEL_ID);
            if (profileLevelId == null) {
              profileLevelId = VideoCodecInfo.H264_CONSTRAINED_BASELINE_3_1;
            }
            switch (profileLevelId) {
              case VideoCodecInfo.H264_CONSTRAINED_HIGH_3_1:
                format.setInteger("profile", VIDEO_AVC_PROFILE_HIGH);
                format.setInteger("level", VIDEO_AVC_LEVEL_3);
                break;
              case VideoCodecInfo.H264_CONSTRAINED_BASELINE_3_1:
                break;
              default:
                Logging.w(TAG, "Unknown profile level id: " + profileLevelId);
            }
        }
      }else {
        isSupportSvc = false;
      }

      Logging.d(TAG, "bitrateMode from config engine: " + bitrateMode);
      if (bitrateMode == 1) {
          format.setInteger(KEY_BITRATE_MODE, Video_ControlRateVariable);
          this.maxSaveRate = 0.0f;
      }else if (bitrateMode == 2) {
          format.setInteger(KEY_BITRATE_MODE, VIDEO_ControlRateConstant);
      }else {
        if (isSupportSvc && codecName.startsWith(QCOM_PREFIX)) {
          Logging.d(TAG, "isSupportSvc : " + isSupportSvc + " codecName : " + codecName + " ignore MediaCodec Bitrate Mode Setting");
          this.maxSaveRate = 0.0f;
        }else {
          format.setInteger(KEY_BITRATE_MODE, VIDEO_ControlRateConstant);
        }
      }
      Logging.d(TAG, "Format(before configure): " + format);
      codec.configure(
          format, null /* surface */, null /* crypto */, MediaCodec.CONFIGURE_FLAG_ENCODE);
      MediaFormat output_format = codec.getOutputFormat();
      Logging.d(TAG, "Format(after configure): " + output_format.toString());

      if (isSupportSvc) {
        if (codecName.startsWith(HISI_PREFIX) || codecName.startsWith(C2_HISI_PREFIX)) {
          if (output_format.containsKey(KEY_HISI_EXT_CODEC_NON_REF_P_FRAMES_SUPPORTED)) {
            Logging.d(TAG, "Contain Key:" + KEY_HISI_EXT_CODEC_NON_REF_P_FRAMES_SUPPORTED);
            isSupportSvc = output_format.getInteger(KEY_HISI_EXT_CODEC_NON_REF_P_FRAMES_SUPPORTED) == 1;
            Logging.d(TAG, "KEY_HISI_EXT_CODEC_NON_REF_P_FRAMES_SUPPORTED : " + isSupportSvc);
          }else {
            isSupportSvc = false;
            Logging.d(TAG, "Not Contain Key:" + KEY_HISI_EXT_CODEC_NON_REF_P_FRAMES_SUPPORTED);
          }
        }else {
          if (output_format.containsKey("ts-schema")) {
            Logging.d(TAG, "Contain Key:ts-schema Value:" + output_format.getString("ts-schema"));
            isSupportSvc = true;
          }else {
            Logging.d(TAG, "Not Contain Key:ts-schema");
            isSupportSvc = false;
          }
        }
      }

      if (useSurfaceMode) {
        try {
          textureEglBase = EglBase.createEgl14(sharedContext, EglBase.CONFIG_RECORDABLE_3);
        } catch (Exception e) {
          textureEglBase = EglBase.createEgl14(sharedContext, EglBase.CONFIG_RECORDABLE);
        }
        textureInputSurface = codec.createInputSurface();
        textureEglBase.createSurface(textureInputSurface);
        textureEglBase.makeCurrent();
        if (yuvSurfaceMode >= 1) {
          GLES20.glPixelStorei(GLES20.GL_UNPACK_ALIGNMENT, 1);
        }
      }
      
      codec.start();
      callback.onUpdateEncoderFormat(useSurfaceMode);
    } catch (Exception e) {
      Logging.e(TAG, "initEncodeInternal failed", e);
      release();
      return VideoCodecStatus.FALLBACK_SOFTWARE;
    }
    
    int lowQpThreshold = kLowH264QpThresholdForHWSmartBitrateHD;
    int highQpThreshold = kHighH264QpThresholdForHWSmartBitrateHD;
    if (codecType == VideoCodecType.VP8) {
      lowQpThreshold = kLowVp8QpThreshold;
      highQpThreshold = kHighVp8QpThreshold;
    } else if (codecType == VideoCodecType.H264) {
      lowQpThreshold = kLowH264QpThresholdForHWSmartBitrateHD;
      highQpThreshold = kHighH264QpThresholdForHWSmartBitrateHD;
    }else if (codecType == VideoCodecType.H265) {
      lowQpThreshold = kLowH265QpThresholdForHWSmartBitrateHD;
      highQpThreshold = kHighH265QpThresholdForHWSmartBitrateHD;
    }

    bitrateAdjuster.init(lowQpThreshold, highQpThreshold, this.maxSaveRate);

    running = true;
    outputThreadExceptionCount = 0;
    outputThreadError = false;
    outputThreadChecker.detachThread();
    if (!syncMode) {
      outputThread = createOutputThread();
      outputThread.start();
    }

    if(isSupportSvc) {
      if (codecName.startsWith(HISI_PREFIX) || codecName.startsWith(C2_HISI_PREFIX)) {
        hisiExtCodecNonRefPFrames = 1;
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_HISI_EXT_CODEC_NON_REF_P_FRAMES, hisiExtCodecNonRefPFrames);
        codec.setParameters(bundle);
      }
    }

    requestingForceKeyFrame = false;
    gotKeyFrameInRequestingKeyFramePeriodTime = true;
    resetEncoderIfRequestForceKeyFrame = false;

    return VideoCodecStatus.OK;
  }

  @Override
  public VideoCodecStatus release() {
    Logging.i(TAG, "release start.");
    encodeThreadChecker.checkIsOnValidThread();
    final VideoCodecStatus returnValue;
    if (syncMode) {
      // NOTE(qijiyue): in sync mode, outputThread is null, so we should put releaseCodecOnOutputThread here;
      // since Mediacodec stop() and release() may hang, we put it on separate thread;
      if (codec != null) {
        outputThreadChecker.detachThread();
        // PUT CODE BELOW ON A SEPARATE THREAD
        final CountDownLatch releaseDone = new CountDownLatch(1);
        Runnable runMediaCodecRelease = new Runnable() {
          @Override
          public void run() {
            releaseCodecOnOutputThread();
            releaseDone.countDown();
          }
        };
        new Thread(runMediaCodecRelease).start();
        if (!ThreadUtils.awaitUninterruptibly(releaseDone, MEDIA_CODEC_RELEASE_TIMEOUT_MS)) {
          Logging.e(TAG, "sync releaseCodecOnOutputThread timeout");
          returnValue = VideoCodecStatus.TIMEOUT;
        } else if (shutdownException != null) {
          Logging.e(TAG, "sync releaseCodecOnOutputThread exception", shutdownException);
          returnValue = VideoCodecStatus.ERROR;
        } else {
          returnValue = VideoCodecStatus.OK;
        }
      } else {
        Logging.w(TAG, "sync mode release, codec be null!!!");
        returnValue = VideoCodecStatus.OK;
      }
    } else {
      // The outputThread actually stops and releases the codec once running is false.
      running = false;
      if (outputThread != null &&!ThreadUtils.joinUninterruptibly(outputThread, MEDIA_CODEC_RELEASE_TIMEOUT_MS)) {
        Logging.e(TAG, "Media encoder release timeout");
        returnValue = VideoCodecStatus.TIMEOUT;
      }else if (shutdownException != null) {
        // Log the exception and turn it into an error.
        Logging.e(TAG, "Media encoder release exception", shutdownException);
        returnValue = VideoCodecStatus.ERROR;
      } else {
        returnValue = VideoCodecStatus.OK;
      }
    }

    textureDrawer.release();
    videoFrameDrawer.release();
    if (textureEglBase != null) {
      textureEglBase.release();
      textureEglBase = null;
    }
    if (textureInputSurface != null) {
      textureInputSurface.release();
      textureInputSurface = null;
    }
    outputBuilders.clear();

    codec = null;
    outputThread = null;

    bitrateAdjuster.release();

    // Allow changing thread after release.
    encodeThreadChecker.detachThread();
    Logging.i(TAG, "release end.");
    return returnValue;
  }

  @Override
  public VideoCodecStatus encode(VideoFrame videoFrame, EncodeInfo encodeInfo) {
    encodeThreadChecker.checkIsOnValidThread();
    // Logging.i(TAG, "qijiyue: debug, step in encode()");
    if (codec == null) {
      Logging.e(TAG, "codec null, UNINITIALIZED");
      return VideoCodecStatus.UNINITIALIZED;
    }

    if (outputThreadError) {
      Logging.w(TAG, "Too many exceptions in output thread, fallback to software encoder.");
      return VideoCodecStatus.FALLBACK_SOFTWARE;
    }

    // Logging.d("LavaNativeCapturerObserver", "output enc 5 " + videoFrame + " count " + videoFrame.getRefCount());

    final VideoFrame.Buffer videoFrameBuffer = videoFrame.getBuffer();
    final boolean isTextureBuffer = videoFrameBuffer instanceof VideoFrame.TextureBuffer;
    final boolean isWrapTextureBuffer = videoFrameBuffer instanceof VideoFrame.WrapTextureBuffer;
    final boolean isTexture = isTextureBuffer || isWrapTextureBuffer;

    // If input resolution changed, restart the codec with the new resolution.
    final int frameWidth = videoFrame.getBuffer().getWidth();
    final int frameHeight = videoFrame.getBuffer().getHeight();
    final boolean shouldUseSurfaceMode = (canUseSurface() && (isTexture || this.yuvSurfaceMode >= 1));
    // FIXME (qijiyue): always reset due to useSurfaceMode changed from true to false, fix asap !!!
    if (frameWidth != actualWidth || frameHeight != actualHeight || shouldUseSurfaceMode != useSurfaceMode) {
      Logging.i(TAG, " origin size is: "+actualWidth+"x"+actualHeight+" useSurfaceMode = "+useSurfaceMode
                  +", new size is: "+frameWidth+"x"+frameHeight+" shouldUseSurfaceMode = "+shouldUseSurfaceMode);
      actualWidth = frameWidth;
      actualHeight = frameHeight;
      useSurfaceMode = shouldUseSurfaceMode;
      alignResolutionIfNeeded();
      // checkout size before resetCodec.
      boolean isSizeSupport = MediaCodecUtils.checkSize(this.capabilities, width, height,this.fallbackResolution);
      if(!isSizeSupport && !forceHardwareEncode) {
        Logging.w(TAG, " resetCodec: " + width + " x " + height +" isSizeSupport: "+isSizeSupport + " FALLBACK_SOFTWARE");
        return VideoCodecStatus.FALLBACK_SOFTWARE;
      }

      // 硬件编码器 fallbackResolution 下发控制
      if((this.fallbackResolution > 0 && frameWidth * frameHeight < this.fallbackResolution)) {
        Logging.w(TAG, " resetCodec: " + frameWidth + " x " + frameHeight + " fallbackResolution "+fallbackResolution
                + " SDK_INT: " + Build.VERSION.SDK_INT+" FALLBACK_SOFTWARE");
        return VideoCodecStatus.FALLBACK_SOFTWARE;
      }

      VideoCodecStatus status = resetCodec();
      if (status != VideoCodecStatus.OK) {
        Logging.e(TAG, "resetCodec faile, status: " + status);
        return status;
      }
    }

    if (syncMode && outputBuilders.size() > 0) {
      // Logging.i(TAG, "qijiyue: debug, before encode, loopForDeliverEncodedImage, outputBuilders.size(): " + outputBuilders.size());
      loopForDeliverEncodedImage();
    }

    if (outputBuilders.size() > MAX_ENCODER_Q_SIZE) {
      // Too many frames in the encoder.  Drop this frame.
      Logging.e(TAG, "Dropped frame, encoder queue full");
      return VideoCodecStatus.NO_OUTPUT; // See webrtc bug 2887.
    }

    boolean requestedKeyFrame = false;
    for (EncodedImage.FrameType frameType : encodeInfo.frameTypes) {
      if (frameType == EncodedImage.FrameType.VideoFrameKey) {
        requestedKeyFrame = true;
      }
    }

    boolean offerRequestingForceKeyFrame = false;
    if (resetEncoderIfRequestForceKeyFrame) {
      resetEncoderIfRequestForceKeyFrame = false;
      Logging.i(TAG, "resetEncoderIfRequestForceKeyFrame");
      VideoCodecStatus status = resetCodec();
      if (status != VideoCodecStatus.OK) {
        Logging.e(TAG, "resetCodec faile, status: " + status);
        return status;
      }
      lastKeyFrameNs = videoFrame.getTimestampNs();
    }else {
      if (requestedKeyFrame || shouldForceKeyFrame(videoFrame.getTimestampNs())) {
        if (!requestingForceKeyFrame) {
          requestingForceKeyFrame = true;
          offerRequestingForceKeyFrame = true;
          gotKeyFrameInRequestingKeyFramePeriodTime = false;
        }
        requestKeyFrame(videoFrame.getTimestampNs());
      }
    }

    // Number of bytes in the video buffer. Y channel is sampled at one byte per pixel; U and V are
    // subsampled at one byte per four pixels.
    // NOTE (qijiyue): use aligned width x height instead of actual width x height;
    // int bufferSize = videoFrameBuffer.getHeight() * videoFrameBuffer.getWidth() * 3 / 2;
    int bufferSize = width * height * 3 / 2;
    EncodedImage.Builder builder = EncodedImage.builder()
                                       .setCaptureTimeNs(videoFrame.getTimestampNs())
                                       .setCompleteFrame(true)
                                       .setEncodedWidth(width)
                                       .setEncodedHeight(height)
                                       .setActualWidth(actualWidth)
                                       .setActualHeight(actualHeight)
                                       .setRotation(videoFrame.getRotation())
                                       .setRequestingForceKeyFrame(offerRequestingForceKeyFrame);
    try{
      outputBuilders.offer(builder);
    }catch (Exception e){
      e.printStackTrace();
      return VideoCodecStatus.ERROR;
    }

    final VideoCodecStatus returnValue;
    if (useSurfaceMode) {
      returnValue = encodeTextureBuffer(videoFrame);
    } else {
      returnValue = encodeByteBuffer(videoFrame, videoFrameBuffer, bufferSize);
    }

    // Check if the queue was successful.
    if (returnValue != VideoCodecStatus.OK) {
      // Keep the output builders in sync with buffers in the codec.
      outputBuilders.pollLast();
      Logging.e(TAG, "encode error: "+returnValue+", outputBuilders.size(): " + outputBuilders.size());
    }

    // NOTE (qijiyue): loop again if possible;
    if (syncMode && outputBuilders.size() > 0) {
      // Logging.i(TAG, "qijiyue: debug, after encode, loopForDeliverEncodedImage, outputBuilders.size(): " + outputBuilders.size());
      loopForDeliverEncodedImage();
    }

    // Logging.d("LavaNativeCapturerObserver", "output enc 6 " + videoFrame + " count " + videoFrame.getRefCount());


    return returnValue;
  }

  private VideoCodecStatus encodeTextureBuffer(VideoFrame videoFrame) {
    encodeThreadChecker.checkIsOnValidThread();
    try {
      // TODO(perkj): glClear() shouldn't be necessary since every pixel is covered anyway,
      // but it's a workaround for bug webrtc:5147.
      GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
      // It is not necessary to release this frame because it doesn't own the buffer.
      VideoFrame derotatedFrame =
          new VideoFrame(videoFrame.getBuffer(), 0 /* rotation */, videoFrame.getTimestampNs());
      // Logging.e(TAG, "qijiyue: encodeTextureBuffer, origin width: " + videoFrame.getBuffer().getWidth() + ", rotated width: " + videoFrame.getRotatedWidth() + ", aligned width:" + width);
      videoFrameDrawer.drawFrame(derotatedFrame, textureDrawer, null /* additionalRenderMatrix */,
                        0 /* viewportX */, 0 /* viewportY */, 
                        width /* viewportWidth */, height /* viewportHeight */,
                        width /* dstWidth */, height /* dstHeight */);
      textureEglBase.swapBuffers(videoFrame.getTimestampNs());
      // Logging.d("LavaNativeCapturerObserver", "output enc 7 " + derotatedFrame + " count " + derotatedFrame.getRefCount());
    } catch (RuntimeException e) {
      Logging.e(TAG, "encodeTexture failed", e);
      return VideoCodecStatus.ERROR;
    }
    return VideoCodecStatus.OK;
  }

  private VideoCodecStatus encodeByteBuffer(
      VideoFrame videoFrame, VideoFrame.Buffer videoFrameBuffer, int bufferSize) {
    encodeThreadChecker.checkIsOnValidThread();
    // Frame timestamp rounded to the nearest microsecond.
    long presentationTimestampUs = (videoFrame.getTimestampNs() + 500) / 1000;

    // No timeout.  Don't block for an input buffer, drop frames if the encoder falls behind.
    int index;
    try {
      index = codec.dequeueInputBuffer(0 /* timeout */);
    } catch (Exception e) {
      Logging.e(TAG, "dequeueInputBuffer failed", e);
      return VideoCodecStatus.ERROR;
    }

    if (index == -1) {
      // Encoder is falling behind.  No input buffers available.  Drop the frame.
      Logging.d(TAG, "Dropped frame, no input buffers available");
      return VideoCodecStatus.NO_OUTPUT; // See webrtc bug 2887.
    }

    ByteBuffer buffer;
    try {
      buffer = codec.getInputBuffers()[index];

      fillInputBuffer(buffer, videoFrameBuffer);
    } catch (Exception e) {
      Logging.e(TAG, "getInputBuffers failed", e);
      return VideoCodecStatus.ERROR;
    }

    try {
      codec.queueInputBuffer(
          index, 0 /* offset */, bufferSize, presentationTimestampUs, 0 /* flags */);
    } catch (Exception e) {
      Logging.e(TAG, "queueInputBuffer failed", e);
      // IllegalStateException thrown when the codec is in the wrong state.
      return VideoCodecStatus.ERROR;
    }
    // Logging.i(TAG, "qijiyue: debug, encodeByteBuffer done");
    return VideoCodecStatus.OK;
  }

  @Override
  public VideoCodecStatus setRateAllocation(BitrateAllocation bitrateAllocation, int framerate) {
    encodeThreadChecker.checkIsOnValidThread();
    if (framerate > MAX_VIDEO_FRAMERATE) {
      framerate = MAX_VIDEO_FRAMERATE;
    }

    int bitrateBps = bitrateAllocation.getSum();

    if(bitrateBps < 0){
      return VideoCodecStatus.ERROR;
    }

    // FIXME (qijiyue): what if only framerate changed ? remove asap !!!
    if(targetQosBitrateBps != bitrateBps){
      bitrateAdjuster.setTargets(bitrateBps, targetFPS);
      targetQosBitrateBps = bitrateBps;
    }
    return VideoCodecStatus.OK;
  }

  @Override
  public ScalingSettings getScalingSettings() {
    encodeThreadChecker.checkIsOnValidThread();
    if (automaticResizeOn) {
      if (codecType == VideoCodecType.VP8) {
        return new ScalingSettings(kLowVp8QpThreshold, kHighVp8QpThreshold);
      } else if (codecType == VideoCodecType.H264) {
        return new ScalingSettings(kLowH264QpThreshold, kHighH264QpThreshold);
      }else if (codecType == VideoCodecType.H265) {
        return new ScalingSettings(kLowH265QpThreshold, kHighH265QpThreshold);
      }
    }
    return ScalingSettings.OFF;
  }

  @Override
  public void reportQP(int qp) {
    bitrateAdjuster.reportQP(qp);
  }

  @Override
  @Nullable
  public String getImplementationName() {
    return codecName;
  }

  @Override
  public boolean isSupportHardwareTextureEncoder(){
    return canUseSurface();
  }

  @Override
  public boolean isDimensityCpu() {
     return AndroidDeviceInfo.isHardWareVendorDimensity();
  }

  private VideoCodecStatus resetCodec() {
    encodeThreadChecker.checkIsOnValidThread();
    Logging.d(TAG, " resetCodec");
    VideoCodecStatus status = release();
    if (status != VideoCodecStatus.OK) {
      return status;
    }
    return initEncodeInternal();
  }

  private boolean shouldForceKeyFrame(long presentationTimestampNs) {
    encodeThreadChecker.checkIsOnValidThread();
    return forcedKeyFrameNs > 0 && presentationTimestampNs > lastKeyFrameNs + forcedKeyFrameNs;
  }

  private void requestKeyFrame(long presentationTimestampNs) {
    encodeThreadChecker.checkIsOnValidThread();
    // Ideally MediaCodec would honor BUFFER_FLAG_SYNC_FRAME so we could
    // indicate this in queueInputBuffer() below and guarantee _this_ frame
    // be encoded as a key frame, but sadly that flag is ignored.  Instead,
    // we request a key frame "soon".
    try {
      Bundle b = new Bundle();
      b.putInt(MediaCodec.PARAMETER_KEY_REQUEST_SYNC_FRAME, 0);
      codec.setParameters(b);
    } catch (Exception e) {
      Logging.e(TAG, "requestKeyFrame failed", e);
      return;
    }
    lastKeyFrameNs = presentationTimestampNs;
  }

  private Thread createOutputThread() {
    return new Thread() {
      @Override
      public void run() {
        while (running) {
          deliverEncodedImage();
        }
        releaseCodecOnOutputThread();
      }
    };
  }

  // NOTE (qijiyue): this method is only for syncMode
  private void loopForDeliverEncodedImage() {
    while (syncMode && deliverEncodedImage()) {
      // NOP
    }
  }

  // Visible for testing.
  // NOTE (qijiyue): return value used by return value
  // true: try again; false: try next time
  protected boolean deliverEncodedImage() {
    outputThreadChecker.checkIsOnValidThread();
    try {
      MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
      int index = codec.dequeueOutputBuffer(info, dequeOutputTimeoutUs);
      // Logging.i(TAG, "qijiyue: debug, dequeueOutputBuffer, result: " + index);
      if (index < 0) {
        //-3: MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED
        //-2: MediaCodec.INFO_OUTPUT_FORMAT_CHANGED
        //-1: MediaCodec.INFO_TRY_AGAIN_LATER
        if (index == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED
            || index == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
          // NOTE (qijiyue): for INFO_OUTPUT_BUFFERS_CHANGED, we will getOutputBuffers next cycle
          return true;
        }
        return false;
      }

      ByteBuffer codecOutputBuffer = codec.getOutputBuffers()[index];

      codecOutputBuffer.position(info.offset);
      codecOutputBuffer.limit(info.offset + info.size);

      boolean result = false;
      if ((info.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
        Logging.d(TAG, "Config frame generated. Offset: " + info.offset + ". Size: " + info.size);
        configBuffer = ByteBuffer.allocateDirect(info.size);
        configBuffer.put(codecOutputBuffer);
      } else {
        bitrateAdjuster.reportEncodedFrame(info.size);
        if (adjustedBitrate != bitrateAdjuster.getAdjustedBitrateBps()) {
          updateBitrate();
        }

        final boolean isKeyFrame = (info.flags & MediaCodec.BUFFER_FLAG_SYNC_FRAME) != 0;
        if (isKeyFrame) {
          gotKeyFrameInRequestingKeyFramePeriodTime = true;
          Logging.d(TAG, "Sync frame generated");
        }

        final ByteBuffer frameBuffer;
        if (isKeyFrame && (codecType == VideoCodecType.H264 || codecType == VideoCodecType.H265)) {
          Logging.d(TAG,
              "Prepending config frame of size " + configBuffer.capacity()
                  + " to output buffer with offset " + info.offset + ", size " + info.size);
          // For H.264 key frame prepend SPS and PPS NALs at the start.
          frameBuffer = ByteBuffer.allocateDirect(info.size + configBuffer.capacity());
          configBuffer.rewind();
          frameBuffer.put(configBuffer);
          frameBuffer.put(codecOutputBuffer);
          frameBuffer.rewind();
        } else {
          frameBuffer = codecOutputBuffer.slice();
        }

        final EncodedImage.FrameType frameType = isKeyFrame
            ? EncodedImage.FrameType.VideoFrameKey
            : EncodedImage.FrameType.VideoFrameDelta;

        EncodedImage.Builder builder = outputBuilders.poll();
        if (builder.isRequestingForceKeyFrame()) {
            requestingForceKeyFrame = false;
            if (!gotKeyFrameInRequestingKeyFramePeriodTime) {
              if (supportResetEncoderIfRequestForceKeyFrameNoRespond) {
                resetEncoderIfRequestForceKeyFrame = true;
              }
            }
        }
        builder.setBuffer(frameBuffer).setFrameType(frameType).setTargetEncBps(bitrateAdjuster.getTargetsBitrateBps())
                                                              .setSavedTargetEncBps(bitrateAdjuster.getSavedTargetBitrateBps())
                                                              .setAdjustedEncBps(adjustedBitrate)
                                                              .setIsSmartBitrateHD(bitrateAdjuster.isSmartBitrateHD());
        // TODO(mellem):  Set codec-specific info.
        callback.onEncodedFrame(builder.createEncodedImage(), new CodecSpecificInfo());
      }
      codec.releaseOutputBuffer(index, false);
      outputThreadExceptionCount = 0;
      return true;
    } catch (Exception e) {
      
      outputThreadExceptionCount++;
      if (outputThreadExceptionCount > kOutputThreadExceptionThreshold) {
        outputThreadError = true;
      }
      Logging.e(TAG, "deliverOutput failed", e);
      return false;
    }
  }

  private void releaseCodecOnOutputThread() {
    outputThreadChecker.checkIsOnValidThread();
    Logging.i(TAG, "Releasing MediaCodec on output thread");
    try {
      codec.stop();
    } catch (Exception e) {
      Logging.e(TAG, "Media encoder stop failed", e);
    }
    try {
      codec.release();
    } catch (Exception e) {
      Logging.e(TAG, "Media encoder release failed", e);
      // Propagate exceptions caught during release back to the main thread.
      shutdownException = e;
    }
    MediaCodecUtils.removeCodecInstances(true,this.codecName);
    configBuffer = null;
    Logging.i(TAG, "Release on output thread done");
  }

  private VideoCodecStatus updateBitrate() {
    outputThreadChecker.checkIsOnValidThread();
    int preBitrate =  adjustedBitrate;
    adjustedBitrate = bitrateAdjuster.getAdjustedBitrateBps();
    try {
      Bundle params = new Bundle();

      //Logging.d(TAG, "updateBitrate from "+ preBitrate + "  to: " + adjustedBitrate);
      params.putInt(MediaCodec.PARAMETER_KEY_VIDEO_BITRATE, adjustedBitrate);
      codec.setParameters(params);
      return VideoCodecStatus.OK;
    } catch (Exception e) {
      Logging.e(TAG, "updateBitrate failed", e);
      return VideoCodecStatus.ERROR;
    }
  }

  private boolean canUseSurface() {
    return sharedContext != null && surfaceColorFormat >0;
  }

  // yuvSurfaceMode未配置下发的情况下，非16位对齐的分辨率，默认使用yuv-tex通道。
  private void updateYuvSurfaceModeForMtk(int width,int height) {
    if ((width % 16 != 0 || height % 16 != 0)
        && AndroidDeviceInfo.isHardWareVendorMediaTek()
        && this.yuvSurfaceMode == -1) {
        yuvSurfaceMode = canUseSurface() ? 2 : yuvSurfaceMode; //本地策略开启
    } else if ((width % 16 == 0 && height % 16 == 0)
                && AndroidDeviceInfo.isHardWareVendorMediaTek()
                && this.yuvSurfaceMode == 2) {
        yuvSurfaceMode = -1;
    }
    Logging.i(TAG, "updateYuvSurfaceModeForMtk, yuvSurfaceMode: " + yuvSurfaceMode + " width: " + width + " height: " + height);
  }

  // Visible for testing.
  protected void fillInputBuffer(ByteBuffer buffer, VideoFrame.Buffer videoFrameBuffer) {
    // NOTE (qijiyue): use aligned width x height instead of actual width x height
    // to generate new dst buffer with green stripes in its image, and then we will
    // crop it in sps crop offset field
    yuvFormat.fillBuffer(buffer, videoFrameBuffer, width, height);
  }

  /**
   * Enumeration of supported YUV color formats used for MediaCodec's input.
   */
  private enum YuvFormat {
    I420 {
      @Override
      void fillBuffer(ByteBuffer dstBuffer, VideoFrame.Buffer srcBuffer, int dstWidth, int dstHeight) {
        VideoFrame.I420Buffer i420 = srcBuffer.toI420();
        if(i420 == null){
           return ;
        }
        // Logging.i(TAG, "qijiyue: dstBuffer: " + dstBuffer + ", dstWidth: " + dstWidth + ", dstHeight: " + dstHeight
        //                 + ", src i420, srcWidth: " + i420.getWidth() + ", srcHeight: " + i420.getHeight()
        //                 + ", dataY: " + i420.getDataY() + ", strideY: " + i420.getStrideY()
        //                 + ", dataU: " + i420.getDataU() + ", strideU: " + i420.getStrideU()
        //                 + ", dataV: " + i420.getDataV() + ", strideV: " + i420.getStrideV());
        YuvHelper.I420Copy(i420.getDataY(), i420.getStrideY(), i420.getDataU(), i420.getStrideU(),
            i420.getDataV(), i420.getStrideV(), i420.getWidth(), i420.getHeight(), dstBuffer, dstWidth, dstHeight);
        i420.release();
      }
    },
    NV12 {
      @Override
      void fillBuffer(ByteBuffer dstBuffer, VideoFrame.Buffer srcBuffer, int dstWidth, int dstHeight) {
        VideoFrame.I420Buffer i420 = srcBuffer.toI420();
        if(i420 == null){
          return ;
        }
        YuvHelper.I420ToNV12(i420.getDataY(), i420.getStrideY(), i420.getDataU(), i420.getStrideU(),
            i420.getDataV(), i420.getStrideV(), i420.getWidth(), i420.getHeight(), dstBuffer, dstWidth, dstHeight);
        i420.release();
      }
    };

    abstract void fillBuffer(ByteBuffer dstBuffer, VideoFrame.Buffer srcBuffer, int dstWidth, int dstHeight);

    static YuvFormat valueOf(int colorFormat) {
      switch (colorFormat) {
        case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar:
          return I420;
        case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar:
        case MediaCodecInfo.CodecCapabilities.COLOR_QCOM_FormatYUV420SemiPlanar:
        case MediaCodecUtils.COLOR_QCOM_FORMATYUV420PackedSemiPlanar32m:
          return NV12;
        default:
          Logging.w(TAG, "Unsupported colorFormat: " + colorFormat + " falling back to I420");
          return I420;
      }
    }
  }

  private void alignResolutionIfNeeded() {
    if (enableAlignment) {
      if (codecWidthAlign == 0) {
        int capWidthAlign = 0;
        int capHeightAlign = 0;
        if (android.os.Build.VERSION.SDK_INT>=android.os.Build.VERSION_CODES.LOLLIPOP) {
          MediaCodecInfo.VideoCapabilities videoCapabilities = capabilities.getVideoCapabilities();
          capWidthAlign = videoCapabilities.getWidthAlignment();
          capHeightAlign = videoCapabilities.getHeightAlignment();
        }
        codecWidthAlign = capWidthAlign > kMinCodecWidthAlign ? capWidthAlign : kMinCodecWidthAlign;
        codecHeightAlign = capHeightAlign > kMinCodecHeightAlign ? capHeightAlign : kMinCodecHeightAlign;
        Logging.i(TAG, "qijiyue: alignResolutionIfNeeded, capabilities align is: "+capWidthAlign+" x "+capHeightAlign
                      +", final codec align is:"+codecWidthAlign+" x "+codecHeightAlign);
      }
      width = (actualWidth + codecWidthAlign - 1) & ~(codecWidthAlign - 1);
      height = (actualHeight + codecHeightAlign - 1) & ~(codecHeightAlign - 1);
    } else {
      width = actualWidth;
      height = actualHeight;
    }
    // only for test;
    // width = width + 160;
    // height = height + 160;
    Logging.i(TAG, "qijiyue: alignResolutionIfNeeded, actual: "+actualWidth+"x"+actualHeight+", align to: "+width+"x"+height);
  }
}
