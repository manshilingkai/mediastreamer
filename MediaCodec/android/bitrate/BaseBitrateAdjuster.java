/*
 *  Copyright 2017 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

package com.netease.lava.webrtc.bitrate;

/** BitrateAdjuster that tracks bitrate and framerate but does not adjust them. */
public class BaseBitrateAdjuster implements BitrateAdjuster {
  private static final int DEFAULT_FPS = 30;
  protected int targetBitrateBps;
  protected int targetFps;

  public BaseBitrateAdjuster() {
    this.targetBitrateBps = 0;
    this.targetFps = 0;
  }

  @Override
  public void init(int low_qp_threshold, int high_qp_threshold, float maxSaveRate) {
    // No op.
  }

  @Override
  public void setTargets(int targetBitrateBps, int targetFps) {
    this.targetBitrateBps = targetBitrateBps;

    if (targetFps<=0 || targetFps>60 ) return;
    this.targetFps = targetFps;
  }

  @Override
  public void reportEncodedFrame(int size) {
    // No op.
  }

  @Override
  public void reportQP(int qp) {
    // No op.
  }

  @Override
  public int getSavedTargetBitrateBps() {
    //default use targetBps
    return targetBitrateBps;
  }

  @Override
  public int getAdjustedBitrateBps() {
    //default use targetBps
    return targetBitrateBps;
  }

  @Override
  public int getTargetsBitrateBps() {
    return targetBitrateBps;
  }

  @Override
  public int getTargetsFrameRate() {
    if (this.targetFps==0) return DEFAULT_FPS;
    else return this.targetFps;
  }

  @Override
  public boolean isSmartBitrateHD() {
    return false;
  }

  @Override
  public void release() {
    // No op.
  }
}
