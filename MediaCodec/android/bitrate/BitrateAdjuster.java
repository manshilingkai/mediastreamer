/*
 *  Copyright 2017 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

package com.netease.lava.webrtc.bitrate;

/** Object that adjusts the bitrate of a hardware codec. */
public interface BitrateAdjuster {

  void init(int low_qp_threshold, int high_qp_threshold, float maxSaveRate);

  /**
   * Sets the target bitrate in bits per second and framerate in frames per second.
   */
  void setTargets(int targetBitrateBps, int targetFps);

  /**
   * Gets the target bitrate in bits per second
   */
  int getTargetsBitrateBps();

  /**
   * Gets the target framerate in frames per second.
   */
  int getTargetsFrameRate();

  /**
   * Reports that a frame of the given size has been encoded.  Returns true if the bitrate should
   * be adjusted.
   */
  void reportEncodedFrame(int size);

  void reportQP(int qp);

  int getSavedTargetBitrateBps();

  /** Gets the current bitrate. */
  int getAdjustedBitrateBps();

  boolean isSmartBitrateHD();

  void release();
}
