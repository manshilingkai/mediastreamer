package com.netease.lava.webrtc.bitrate;
import com.netease.lava.webrtc.Logging;

public class NewDynamicBitrateAdjuster extends BaseBitrateAdjuster {

  private static final String TAG = "NewDynamicBitrateAdjuster";

  private long nativeBitrateAdjusterInstance;

  private int max_target_bitrate_bps;
  private int width;
  private int height;
  private int codec_type;
  private float min_adjusted_bitrate_pct;
  private float max_adjusted_bitrate_pct;
  private boolean enable_dynamic_adjust_max_bitrate_pct;
  private boolean enable_bitrate_adjuster;
  private boolean is_screen_share;
  private float save_rate_per_qp;
  private float max_save_rate;
  private boolean allow_overshot_when_non_weak_network;

  private final Object dynamicBitrateAdjusterLock = new Object();

  public NewDynamicBitrateAdjuster(int maxTargetBitrateBps, int w, int h, int type, float minAdjustedBitratePct, float maxAdjustedBitratePct, boolean enableBitrateAdjuster, boolean isScreenShare, float saveRatePerQp, float maxSaveRate, boolean allowOverShotWhenNonWeakNetwork) {
     super();
     max_target_bitrate_bps = maxTargetBitrateBps;
     width = w;
     height = h;
     codec_type = type;
     min_adjusted_bitrate_pct = 0.5f;
     max_adjusted_bitrate_pct = 1.0f;
     enable_dynamic_adjust_max_bitrate_pct = true;
     enable_bitrate_adjuster = false;
     is_screen_share = false;
     save_rate_per_qp = 0.0f;
     max_save_rate = 0.0f;
     allow_overshot_when_non_weak_network = allowOverShotWhenNonWeakNetwork;

    if (minAdjustedBitratePct > 0) {
      min_adjusted_bitrate_pct = minAdjustedBitratePct;
    }
    if (maxAdjustedBitratePct > 0) {
      max_adjusted_bitrate_pct = maxAdjustedBitratePct;
      enable_dynamic_adjust_max_bitrate_pct = false;
    }
    enable_bitrate_adjuster = enableBitrateAdjuster;
    is_screen_share = isScreenShare;
    if (saveRatePerQp > 0 && saveRatePerQp < 1.0f) {
      save_rate_per_qp = saveRatePerQp;
    }
    if (maxSaveRate >= 0 && maxSaveRate < 1.0f) {
      max_save_rate = maxSaveRate;
    }

    nativeBitrateAdjusterInstance = 0;
  }

  @Override
  public void init(int low_qp_threshold, int high_qp_threshold, float maxSaveRate) {
    Logging.d(TAG, "init()");
    synchronized (dynamicBitrateAdjusterLock) {
      if(nativeBitrateAdjusterInstance!=0) {
        nativeRelease(nativeBitrateAdjusterInstance);
        nativeBitrateAdjusterInstance = 0;
      }
      if (maxSaveRate >= 0 && maxSaveRate < 1.0f) {
        max_save_rate = maxSaveRate;
      }
      nativeBitrateAdjusterInstance = nativeInit(max_target_bitrate_bps, width, height, min_adjusted_bitrate_pct, max_adjusted_bitrate_pct, enable_dynamic_adjust_max_bitrate_pct, low_qp_threshold, high_qp_threshold, codec_type, enable_bitrate_adjuster, is_screen_share, save_rate_per_qp, max_save_rate, allow_overshot_when_non_weak_network);
    }
  }

  @Override
  public void setTargets(int targetBitrateBps, int targetFps) {
    synchronized (dynamicBitrateAdjusterLock) {
      if(nativeBitrateAdjusterInstance!=0) {
        nativeSetTargetBitrateBps(nativeBitrateAdjusterInstance, targetBitrateBps);
      }
      super.setTargets(targetBitrateBps, targetFps);
    }
  }

  @Override
  public void reportEncodedFrame(int size) {
    synchronized (dynamicBitrateAdjusterLock) {
      if(nativeBitrateAdjusterInstance!=0) {
        nativeReportEncodedFrame(nativeBitrateAdjusterInstance, size);
      }
    }
  }

  @Override
  public void reportQP(int qp) {
    synchronized (dynamicBitrateAdjusterLock) {
      if(nativeBitrateAdjusterInstance!=0) {
        nativeReportQP(nativeBitrateAdjusterInstance, qp);
      }
    }
  }

  @Override
  public int getSavedTargetBitrateBps() {
    synchronized (dynamicBitrateAdjusterLock) {
      if(nativeBitrateAdjusterInstance!=0) {
        int ret = nativeGetSavedTargetBitrateBps(nativeBitrateAdjusterInstance);
        if (ret <= 0) return super.getSavedTargetBitrateBps();
        else return ret;
      }else {
        return super.getSavedTargetBitrateBps();
      }
    }
  }

  @Override
  public int getAdjustedBitrateBps() {
    synchronized (dynamicBitrateAdjusterLock) {
      if(nativeBitrateAdjusterInstance!=0) {
        int ret = nativeGetAdjustedBitrateBps(nativeBitrateAdjusterInstance);
        if (ret <= 0) return super.getAdjustedBitrateBps();
        else return ret;
      }else {
        return super.getAdjustedBitrateBps();
      }
    }
  }

  @Override
  public boolean isSmartBitrateHD() {
    synchronized (dynamicBitrateAdjusterLock) {
      if(nativeBitrateAdjusterInstance!=0) {
        return nativeIsSmartBitrateHD(nativeBitrateAdjusterInstance);
      }else {
        return super.isSmartBitrateHD();
      }
    }
  }

  @Override
  public void release() {
    Logging.d(TAG, "release()");
    synchronized (dynamicBitrateAdjusterLock) {
      if(nativeBitrateAdjusterInstance!=0) {
        nativeRelease(nativeBitrateAdjusterInstance);
        nativeBitrateAdjusterInstance = 0;
      }
    }
  }

  private static native long nativeInit(int maxTargetBitrateBps, int width, int height, float minAdjustedBitratePct, float maxAdjustedBitratePct, boolean enable_dynamic_adjust_max_bitrate_pct, int low_qp_threshold, int high_qp_threshold, int codec_type, boolean enableBitrateAdjuster, boolean isScreenShare, float saveRatePerQp, float maxSaveRate, boolean allowOverShotWhenNonWeakNetwork);
  private static native void nativeSetTargetBitrateBps(long nativeNewDynamicBitrateAdjuster, int targetBitrateBps);
  private static native void nativeReportEncodedFrame(long nativeNewDynamicBitrateAdjuster, int size);
  private static native void nativeReportQP(long nativeNewDynamicBitrateAdjuster, int qp);
  private static native int nativeGetAdjustedBitrateBps(long nativeNewDynamicBitrateAdjuster);
  private static native int nativeGetSavedTargetBitrateBps(long nativeNewDynamicBitrateAdjuster);
  private static native boolean nativeIsSmartBitrateHD(long nativeNewDynamicBitrateAdjuster);
  private static native void nativeRelease(long instance);
}