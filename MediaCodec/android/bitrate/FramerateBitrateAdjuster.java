/*
 *  Copyright 2017 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

package com.netease.lava.webrtc.bitrate;

import com.netease.lava.webrtc.Logging;

/**
 * BitrateAdjuster that adjusts the bitrate to compensate for changes in the framerate.  Used with
 * hardware codecs that assume the framerate never changes.
 */

public class FramerateBitrateAdjuster extends NewDynamicBitrateAdjuster {
  private static final String TAG = "FramerateBitrateAdjuster";
  private int initTargetFps;
  private int currentTargetFps;

  public FramerateBitrateAdjuster(int maxTargetBitrateBps, int w, int h, int type, float minAdjustedBitratePct,
      float maxAdjustedBitratePct, boolean enableBitrateAdjuster, boolean isScreenShare, float saveRatePerQp,
      float maxSaveRate, boolean allowOverShotWhenNonWeakNetwork) {
    super(maxTargetBitrateBps, w, h, type, minAdjustedBitratePct, maxAdjustedBitratePct, enableBitrateAdjuster,
        isScreenShare, saveRatePerQp, maxSaveRate, allowOverShotWhenNonWeakNetwork);
    initTargetFps = 0;
    currentTargetFps = 0;
  }

  @Override
  public void setTargets(int targetBitrateBps, int targetFps) {
    super.setTargets(targetBitrateBps, targetFps);

    if (targetFps<=0 || targetFps>60 ) return;
    currentTargetFps = targetFps;
    if (initTargetFps == 0) {
      initTargetFps = currentTargetFps;
      Logging.d(TAG, "initTargetFps : "+ initTargetFps);
    }
  }

  @Override
  public int getAdjustedBitrateBps() {
    int adjustedBitrateBps = super.getAdjustedBitrateBps();

    if (initTargetFps > 0 && currentTargetFps > 0) {
      adjustedBitrateBps = adjustedBitrateBps * initTargetFps / currentTargetFps;
    }
    return adjustedBitrateBps;
  }
}