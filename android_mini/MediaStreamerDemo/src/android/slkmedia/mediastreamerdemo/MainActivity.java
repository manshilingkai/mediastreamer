package android.slkmedia.mediastreamerdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.slkmedia.mediastreamer.ScreenPublisherActivity;
import android.slkmedia.mediastreamer.ScreenStreamerActivity;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity{
	private static final String TAG = "MainActivity";
	
	private String mPublishUrl = "rtmp://pili-publish.live.fmeryu.com/xxq-live/shilingkai-test";
//	private String mPublishUrl = "/sdcard/mediacodec.mp4";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_main);

        Button remuxer = (Button)findViewById(R.id.MediaRemuxer);
        remuxer.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent in = new Intent();
				in.setClass(MainActivity.this, MediaRemuxerActivity.class);
				startActivity(in);
			}
		});
        
        Button audioRecorder = (Button)findViewById(R.id.AudioRecorder);
        audioRecorder.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent in = new Intent();
				in.setClass(MainActivity.this, AudioRecorderActivity.class);
				startActivity(in);
			}
        });
        
        Button mediaEditEngine = (Button)findViewById(R.id.MediaEditEngine);
        mediaEditEngine.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent in = new Intent();
				in.setClass(MainActivity.this, MediaEditEngineActivity.class);
				startActivity(in);
			}
		});
        
        Button camera = (Button)findViewById(R.id.Camera);
        camera.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent in = new Intent();
				in.putExtra("PublishUrl", mPublishUrl);
				in.setClass(MainActivity.this, CameraActivity.class);
				startActivity(in);
			}
		});
        
        Button screen = (Button)findViewById(R.id.Screen);
        screen.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				Intent in = new Intent();
				in.putExtra(ScreenStreamerActivity.PUBLISH_URL_KEY, mPublishUrl);
				in.setClass(MainActivity.this, ScreenStreamerActivity.class);
				startActivity(in);
			}
		});
    }
    
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    
	@Override
	protected void onStart() {
		super.onStart();
	}

    @Override
    protected void onStop() {
        super.onStop();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
