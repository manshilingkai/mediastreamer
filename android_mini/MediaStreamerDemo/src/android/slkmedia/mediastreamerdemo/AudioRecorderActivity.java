package android.slkmedia.mediastreamerdemo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.slkmedia.mediaeditengine.AudioEditEngineEnv;
import android.slkmedia.mediaeditengine.AudioPlayer;
import android.slkmedia.mediaeditengine.AudioPlayerListener;
import android.slkmedia.mediastreamer.AudioOptions;
import android.slkmedia.mediastreamer.MediaStreamer;
import android.slkmedia.mediastreamer.MediaStreamerListener;
import android.slkmedia.mediastreamer.VideoOptions;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class AudioRecorderActivity extends Activity implements MediaStreamerListener, AudioPlayerListener{
	private static final String TAG = "AudioRecorderActivity";
	
	private MediaStreamer mAudioRecorder = null;
//	private AudioPlayer mAudioPlayer = null;
	
	private String mPublishUrl = "rtmp://pili-publish.fmeryu.com/test-xxq-live/shilingkai-test";
	private boolean isEnableAudio = true;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_audiorecorder);

		AudioEditEngineEnv.setupAudioManager(this);
		
		mAudioRecorder = new MediaStreamer(this);
		mAudioRecorder.setMediaStreamerListener(this);
		
//		mAudioPlayer = new AudioPlayer();
//		mAudioPlayer.setListener(this);
		
		Button startRecord = (Button) findViewById(R.id.StartRecord);
		startRecord.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				VideoOptions videoOptions = new VideoOptions();
				videoOptions.hasVideo = false;
				AudioOptions audioOptions = new AudioOptions();
				audioOptions.hasAudio = true;
				audioOptions.audioBitRate = 128;
				
				mAudioRecorder.Start(videoOptions, audioOptions, mPublishUrl);
			}
		});

		Button stopRecord = (Button) findViewById(R.id.StopRecord);
		stopRecord.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mAudioRecorder.Stop();
			}
		});
		

		Button startPlay = (Button) findViewById(R.id.StartPlay);
		startPlay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				mAudioPlayer.setDataSource(mPublishUrl);
//				mAudioPlayer.prepareAsyncToPlay();
			}
		});
		
		Button stopPlay = (Button) findViewById(R.id.StopPlay);
		stopPlay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				mAudioPlayer.stop();
			}
		});
		
		Button pushSEM = (Button) findViewById(R.id.PushSEM);
		pushSEM.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mAudioRecorder.MixSEM("/sdcard/SEM.mp3", 0.5f, 0);
			}
		});
		
		Button enableAudio = (Button) findViewById(R.id.EnableAudio);
		enableAudio.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				isEnableAudio = !isEnableAudio;
				mAudioRecorder.EnableAudio(isEnableAudio);
			}
		});
	}
	
	// AudioRecorder
	@Override
	public void onMediaStreamerConnecting() {
		Log.d(TAG, "onMediaStreamerConnecting");
	}

	@Override
	public void onMediaStreamerConnected() {
		Log.d(TAG, "onMediaStreamerConnected");
	}

	@Override
	public void onMediaStreamerStreaming() {
		Log.d(TAG, "onMediaStreamerStreaming");
		
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            	mAudioRecorder.MixBGM("/sdcard/zmdqd.mp3", 0.2f, -1);
            }
        });	
	}

	@Override
	public void onMediaStreamerError(int errorType) {
		Log.d(TAG, "onMediaStreamerError " + errorType);
	}

	@Override
	public void onMediaStreamerInfo(int infoType, int infoValue) {
		Log.d(TAG, "onMediaStreamerInfo " + infoType + infoValue);

		if(infoType==MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_TIME)
		{
			Log.d(TAG, "Audio Record Time : " + String.valueOf(infoValue/10));
		}
	}

	@Override
	public void onMediaStreamerEnd() {
		Log.d(TAG, "onMediaStreamerEnd");
	}

	// AudioPlayer
	@Override
	public void onPrepared() {
	}

	@Override
	public void onError(int what, int extra) {
	}

	@Override
	public void onInfo(int what, int extra) {
	}

	@Override
	public void onCompletion() {
	}

	@Override
	public void OnSeekComplete() {
	}
	
	// Activity
	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		mAudioRecorder.Release();
		mAudioRecorder = null;
		
//		mAudioPlayer.release();
//		mAudioPlayer = null;
	}
}
