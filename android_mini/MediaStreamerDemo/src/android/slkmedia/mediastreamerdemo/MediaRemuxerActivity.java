package android.slkmedia.mediastreamerdemo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.os.Bundle;
import android.slkmedia.mediaprocesser.MediaRemuxer;
import android.slkmedia.mediaprocesser.MediaRemuxerListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MediaRemuxerActivity extends Activity implements
		MediaRemuxerListener {
	private static final String TAG = "MediaRemuxerActivity";

	private MediaRemuxer mMediaRemuxer = null;
	ExecutorService executorService = Executors.newSingleThreadExecutor();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_mediaremuxer);

		mMediaRemuxer = new MediaRemuxer("/sdcard/wx.mp4", "/sdcard/slktest.mp4");
		mMediaRemuxer.setMediaRemuxerListener(this);

		Button start = (Button) findViewById(R.id.Start);
		start.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executorService.execute(mMediaRemuxer);
			}
		});

		Button stop = (Button) findViewById(R.id.Stop);
		stop.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mMediaRemuxer.interrupt();
				executorService.shutdown();
			}
		});
	}

	@Override
	public void onMediaRemuxerError(int errorType) {
		Log.e(TAG, "Error Type : " + String.valueOf(errorType));
	}

	@Override
	public void onMediaRemuxerInfo(int infoType, int infoValue) {
		if (infoType == MediaRemuxer.CALLBACK_MEDIA_REMUXER_INFO_PUBLISH_TIME) {
			Log.d(TAG, "Publish Time : " + String.valueOf(infoValue));
		}
	}

	@Override
	public void onMediaRemuxerEnd() {
		Log.i(TAG, "onMediaRemuxerEnd");
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		executorService.shutdownNow();
	}
}
