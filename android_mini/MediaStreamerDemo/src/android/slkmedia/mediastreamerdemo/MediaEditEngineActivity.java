package android.slkmedia.mediastreamerdemo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.slkmedia.mediaeditengine.AudioEditEngineEnv;
import android.slkmedia.mediaeditengine.AudioPlayer;
import android.slkmedia.mediaeditengine.AudioPlayerListener;
import android.slkmedia.mediaeditengine.AudioRemuxer;
import android.slkmedia.mediaeditengine.AudioRemuxerListener;
import android.slkmedia.mediaeditengine.AudioRemuxerOptions;
import android.slkmedia.mediaeditengine.DubbingScore;
import android.slkmedia.mediaeditengine.DubbingScoreListener;
import android.slkmedia.mediaeditengine.MediaDubbingProducer;
import android.slkmedia.mediaeditengine.MediaDubbingProducerListener;
import android.slkmedia.mediaeditengine.MediaDubbingProducerOptions;
import android.slkmedia.mediaeditengine.MediaTranscoder;
import android.slkmedia.mediaeditengine.MediaTranscoderListener;
import android.slkmedia.mediaeditengine.MediaTranscoderOptions;
import android.slkmedia.mediaeditengine.MediaTranscoderGL;
import android.slkmedia.mediaeditengine.MicrophoneAudioRecorder;
import android.slkmedia.mediaeditengine.MicrophoneAudioRecorderOptions;
import android.slkmedia.mediaeditengine.VideoFileUtils;
import android.slkmedia.mediaeditengine.VideoThumbnailTask;
import android.slkmedia.mediaeditengine.VideoThumbnailTaskGL;
import android.slkmedia.mediaeditengine.VideoThumbnailTaskPlus;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MediaEditEngineActivity extends Activity implements MediaDubbingProducerListener, AudioPlayerListener, DubbingScoreListener, MediaTranscoderListener, AudioRemuxerListener{
	private static final String TAG = "MediaEditEngineActivity";

	private MicrophoneAudioRecorder mMicrophoneAudioRecorder = null;
	private Handler workHandler=new Handler();
	
	private MediaDubbingProducer mMediaDubbingProducer = null;
	
	private AudioPlayer mAudioPlayer = null;

	private DubbingScore mDubbingScore = null;
	
	private MediaTranscoderGL mMediaTranscoder = null;
	
	private AudioRemuxer mAudioRemuxer = null;
	
	private boolean isUseAudioPlayer = true;
	
	private VideoThumbnailTaskGL mVideoThumbnailTask = null;
	private ExecutorService executorService = Executors.newSingleThreadExecutor();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_mediaeditengine);

		AudioEditEngineEnv.setupAudioManager(this);
		
		mMicrophoneAudioRecorder = new MicrophoneAudioRecorder(this);
		MicrophoneAudioRecorderOptions options = new MicrophoneAudioRecorderOptions();
		options.isControlAudioManger = false;
		mMicrophoneAudioRecorder.initialize(options);
		mMicrophoneAudioRecorder.enableEarReturn(false);
		mMicrophoneAudioRecorder.enableNoiseSuppression(true);
		
		mMediaDubbingProducer = new MediaDubbingProducer(0);
		mMediaDubbingProducer.setMediaDubbingProducerListener(this);
		
		mAudioPlayer = new AudioPlayer();
		mAudioPlayer.setListener(this);
		
		mDubbingScore = new DubbingScore();
		mDubbingScore.setDubbingScoreListener(this);
		
		mAudioRemuxer = new AudioRemuxer();
		mAudioRemuxer.setAudioRemuxerListener(this);
		
		Button startMicRecord = (Button) findViewById(R.id.StartMicRecord);
		startMicRecord.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				workHandler.post(recordinfoRunnable);
				mMicrophoneAudioRecorder.startRecord();
			}
		});

		Button stopMicRecord = (Button) findViewById(R.id.StopMicRecord);
		stopMicRecord.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mMicrophoneAudioRecorder.stopRecord();
				workHandler.removeCallbacks(recordinfoRunnable); 
			}
		});
		
		Button openAudioPlayer = (Button) findViewById(R.id.OpenAudioPlayer);
		openAudioPlayer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(isUseAudioPlayer)
				{
					try{
						mAudioPlayer.stop();
					}catch(Exception e)
					{
						
					}
					
					mAudioPlayer.setDataSource("/sdcard/3.mp3");
					mAudioPlayer.prepare();
				}else{
					mMicrophoneAudioRecorder.openAudioPlayer();
				}
			}
		});
		
		
		Button startAudioPlay = (Button) findViewById(R.id.StartAudioPlay);
		startAudioPlay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(isUseAudioPlayer)
				{
					mAudioPlayer.play();
				}else{
					mMicrophoneAudioRecorder.startAudioPlay();
				}
			}
		});
		
		Button seekAudioPlay = (Button) findViewById(R.id.SeekAudioPlay);
		seekAudioPlay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(isUseAudioPlayer)
				{
					mAudioPlayer.seekTo(2000);
				}else{
					mMicrophoneAudioRecorder.seekAudioPlay(2000);
				}
			}
		});
		
		Button pauseAudioPlay = (Button) findViewById(R.id.PauseAudioPlay);
		pauseAudioPlay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(isUseAudioPlayer)
				{
					mAudioPlayer.pause();
				}else{
					mMicrophoneAudioRecorder.pauseAudioPlay();
				}
			}
		});
		
		Button closeAudioPlayer = (Button) findViewById(R.id.CloseAudioPlayer);
		closeAudioPlayer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(isUseAudioPlayer)
				{
					mAudioPlayer.stop();
				}else{
					mMicrophoneAudioRecorder.closeAudioPlayer();
				}
			}
		});
		
		Button covertToWav = (Button) findViewById(R.id.ConvertToWAV);
		covertToWav.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mMicrophoneAudioRecorder.convertToWav("/sdcard/123.wav");
				MicrophoneAudioRecorder.NoiseSuppressionWav("/sdcard/123.wav", "/sdcard/123_NS.wav");
				MicrophoneAudioRecorder.OffsetWav("/sdcard/123.wav", "/sdcard/123_Offset.wav", -2000);
			}
		});
		
		Button startProduce = (Button) findViewById(R.id.StartProduce);
		startProduce.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				MediaDubbingProducerOptions options = new MediaDubbingProducerOptions();
				options.videoUrl = "/sdcard/no.mp4";
				options.bgmUrl = "/sdcard/no.m4a";
				options.bgmVolume = 0.1f;
				options.dubUrl = "/sdcard/no.wav";
				options.dubVolume = 2.0f;
				options.productUrl = "/sdcard/product.mp4";
				mMediaDubbingProducer.start(options);
			}
		});
		
		Button stopProduce = (Button) findViewById(R.id.StopProduce);
		stopProduce.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mMediaDubbingProducer.Stop(false);
			}
		});
		
//		Button startScore = (Button) findViewById(R.id.StartScore);
//		startScore.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				DubbingScoreOptions options = new DubbingScoreOptions();
//				options.originUrl = "/sdcard/fnby.mp4";
//				options.bgmUrl = "/sdcard/fnby.mp3";
//				options.dubUrl = "/sdcard/123.wav";
//				mDubbingScore.start(options);
//			}
//		});
//		
//		Button stopScore = (Button) findViewById(R.id.StopScore);
//		stopScore.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				mDubbingScore.stop();
//			}
//		});
		
		Button thumbnail = (Button) findViewById(R.id.Thumbnail);
		thumbnail.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				long durationMs = VideoFileUtils.getVideoFileDurationMs("/sdcard/lv_0_20191031110931.mp4");
				Log.d(TAG, "DurationMs : " + String.valueOf(durationMs));
				
				VideoFileUtils.getScaledThumbnailImage("/sdcard/lv_0_20191031110931.mp4", 3000, true, "/sdcard/error_slk.png", 200, 200);
			}
		});
		
		Button startTranscode = (Button) findViewById(R.id.StartTranscode);
		startTranscode.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				MediaTranscoderOptions options = new MediaTranscoderOptions();
//				options.inputUrl = "/sdcard/fnby.mp4";
//				options.startPosMs = 2000;
//				options.endPosMs = 8000;
				
//				options.video_rotation_degree = 90;
//				options.video_flip = MediaTranscoder.VFLIP;
				
//				options.overlayUrl = "/sdcard/fcy.mp4";
//				options.has_alpha_for_overlay_video = true;
//				options.overlay_x = 0;
//				options.overlay_y = -300;
//				options.overlay_scale = 0.5f;
//				options.overlay_effect_in_ms = 1000;
//				options.overlay_effect_out_ms = 7000;
				
//				options.outputUrl = "/sdcard/fnby_fcy.mp4";
//				options.hasVideo = true;
//				options.outputVideoWidth = 0;
//				options.outputVideoHeight = 0;
//				options.outputVideoBitrateKbps = 2000;
//				options.hasAudio = true;
//				options.outputAudioSampleRate = 44100;
//				options.outputAudioBitrateKbps = 128;
//				
//				mMediaTranscoder.start(options);
				
//				if(MediaTranscoder.isNeedMediaTranscoder("/sdcard/fnby.mp4"))
//				{
//					Log.d(TAG, "Need MediaTranscoder");
//				}else{
//					Log.d(TAG, "Not Need MediaTranscoder");
//				}
				
//				mVideoThumbnailTask = new VideoThumbnailTaskGL("/sdcard/RPReplay_Final1586573103.MP4", "/sdcard/backup", 0);
//				mVideoThumbnailTask.setVideoThumbnailTaskListenr(new VideoThumbnailTaskGL.VideoThumbnailTaskListener() {
//					@Override
//					public void onVideoThumbnail(int index, int all,
//							String thumbnailPng) {
//						Log.d(TAG, "onVideoThumbnail " + index + " "+ all + " " + thumbnailPng);
//					}
//				});
//				
//				mVideoThumbnailTask.startWork();
				
				MediaTranscoderOptions options = new MediaTranscoderOptions();
				options.inputUrl = "/sdcard/3840x2160.mp4";
				options.startPosMs = 1000;
				options.endPosMs = 26000;
				
				options.outputUrl = "/sdcard/fnby_fcy.mp4";
				options.hasVideo = true;
				
				mMediaTranscoder = new MediaTranscoderGL(MediaEditEngineActivity.this);
				mMediaTranscoder.setMediaTranscoderListener(MediaEditEngineActivity.this);
				mMediaTranscoder.startTranscode(options);
			}
		});
		
		Button stopTranscode = (Button) findViewById(R.id.StopTranscode);
		stopTranscode.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				mMediaTranscoder.stop(false);
//				mVideoThumbnailTask.release();
				if(mMediaTranscoder!=null)
				{
					mMediaTranscoder.release();
					mMediaTranscoder = null;
				}
			}
		});
		
		Button startAudioRemuxer = (Button) findViewById(R.id.StartAudioRemuxer);
		startAudioRemuxer.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				AudioRemuxerOptions options = new AudioRemuxerOptions();
				options.inputUrl = "/sdcard/fnby.mp4";
				options.startPosMs = 0;
				options.endPosMs = 0;
				options.isRemuxAll = true;
				options.outputUrl = "/sdcard/fnby_audio.m4a";
				
				mAudioRemuxer.start(options);
			}
		});
		
		Button stopAudioRemuxer = (Button) findViewById(R.id.StopAudioRemuxer);
		stopAudioRemuxer.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mAudioRemuxer.stop(false);
			}
		});
	}
	
	private Runnable recordinfoRunnable = new Runnable()
	{
		@Override
		public void run() {
			int recordTimeMs = mMicrophoneAudioRecorder.getRecordTimeMs();
			int recordPcmDB = mMicrophoneAudioRecorder.getRecordPcmDB();
			Log.d(TAG, "recordTimeMs : "+String.valueOf(recordTimeMs));
			Log.d(TAG, "recordPcmDB : "+String.valueOf(recordPcmDB));
			workHandler.postDelayed(this, 50); 
		}
	};

	//MediaDubbingProducer
	@Override
	public void onDubbingStart(int tag) {
		Log.d(TAG, "onDubbingStart Tag : " + String.valueOf(tag));
	}

	@Override
	public void onDubbingError(int errorType, int tag) {
		Log.d(TAG, "onDubbingError ErrorType : " + String.valueOf(errorType) + " Tag : " + String.valueOf(tag));
	}

	@Override
	public void onDubbingInfo(int infoType, int infoValue, int tag) {
		if(infoType == MediaDubbingProducer.CALLBACK_MEDIA_DUBBING_INFO_WRITE_TIMESTAMP)
		{
			Log.d(TAG, "Dubbing Write TimeStamp : " + String.valueOf(infoValue) + " Tag : " + String.valueOf(tag));
		}
	}

	@Override
	public void onDubbingEnd(int tag) {
		Log.d(TAG, "onDubbingEnd Tag : " + String.valueOf(tag));
	}

	// AudioPlayer
	@Override
	public void onPrepared() {
		Log.d(TAG, "AudioPlayer onPrepared");
	}

	@Override
	public void onError(int what, int extra) {
		Log.d(TAG, "AudioPlayer onError what:" + String.valueOf(what) + " extra:"+String.valueOf(extra));
	}

	@Override
	public void onInfo(int what, int extra) {
		Log.d(TAG, "AudioPlayer onInfo what:" + String.valueOf(what) + " extra:"+String.valueOf(extra));
	}

	@Override
	public void onCompletion() {
		Log.d(TAG, "AudioPlayer onCompletion");
	}

	@Override
	public void OnSeekComplete() {
		Log.d(TAG, "AudioPlayer OnSeekComplete");
	}
	
	//sys
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    
	@Override
	protected void onStart() {
		super.onStart();
	}

    @Override
    protected void onStop() {
        super.onStop();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        
		workHandler.removeCallbacks(recordinfoRunnable); 

        if(mMicrophoneAudioRecorder!=null)
        {
        	mMicrophoneAudioRecorder.terminate();
        	mMicrophoneAudioRecorder = null;
        }
        
        if(mMediaDubbingProducer!=null)
        {
        	mMediaDubbingProducer.Release();
        	mMediaDubbingProducer = null;
        }
        
        if(mAudioPlayer!=null)
        {
        	mAudioPlayer.release();
        	mAudioPlayer = null;
        }
        
        if(mDubbingScore!=null)
        {
        	mDubbingScore.release();
        	mDubbingScore = null;
        }
        
        if(mMediaTranscoder!=null)
        {
        	mMediaTranscoder.release();
        	mMediaTranscoder = null;
        }
        
        if(mAudioRemuxer!=null)
        {
        	mAudioRemuxer.release();
        	mAudioRemuxer = null;
        }
    }

    //DubbingScore
	@Override
	public void onDubbingScoreStart() {
		Log.d(TAG, "onDubbingScoreStart");
	}

	@Override
	public void onDubbingScoreError(int errorType) {
		Log.d(TAG, "onDubbingScoreError : ErrorType " + String.valueOf(errorType));
	}

	@Override
	public void onDubbingScoreInfo(int infoType, int infoValue) {
		Log.d(TAG, "onDubbingScoreInfo : InfoType " + String.valueOf(infoType));
	}

	@Override
	public void onDubbingScoreEnd(float scoreValue) {
		Log.d(TAG, "onDubbingScoreEnd : ScoreValue " + String.valueOf(scoreValue));
	}
	
	///////////////////////////////////////////////////////////////////////////
	//MediaTranscoder
	@Override
	public void onMediaTranscoderConnecting() {
		Log.d(TAG, "onMediaTranscoderConnecting");
	}
	
	@Override
	public void onMediaTranscoderConnected() {
		Log.d(TAG, "onMediaTranscoderConnected");
	}
	
	@Override
	public void onMediaTranscoderTranscoding() {
		Log.d(TAG, "onMediaTranscoderTranscoding");
	}
	
	@Override
	public void onMediaTranscoderError(int errorType)
	{
		Log.d(TAG, "onMediaTranscoderError : ErrorType " + String.valueOf(errorType));
	}
	
	private int mMediaTranscoderDuration = 0;
	private int mMediaTranscoderPublishTime = 0;
	private float mMediaTranscoderProgress = 0.0f;
	@Override
	public void onMediaTranscoderInfo(int infoType, int infoValue)
	{
		Log.d(TAG, "onMediaTranscoderInfo : InfoType " + String.valueOf(infoType) + " InfoValue " + String.valueOf(infoValue));
		if(infoType==MediaTranscoder.CALLBACK_MEDIA_TRANSCODER_INFO_PUBLISH_DURATION)
		{
			mMediaTranscoderDuration = infoValue;
		}
		
		if(infoType==MediaTranscoder.CALLBACK_MEDIA_TRANSCODER_INFO_PUBLISH_TIME)
		{
			mMediaTranscoderPublishTime = infoValue;
			
			if(mMediaTranscoderDuration>0)
			{
				mMediaTranscoderProgress = (float)mMediaTranscoderPublishTime/(float)mMediaTranscoderDuration;
			}
			
			Log.d(TAG, "MediaTranscoder Progress : " + String.valueOf(mMediaTranscoderProgress));
		}
	}
	
	@Override
	public void onMediaTranscoderEnd()
	{
		Log.d(TAG, "onMediaTranscoderEnd");
	}
	
	///////////////////////////////////////////////////////////////////////////
	// AudioRemuxer
	@Override
	public void onAudioRemuxerStart() {
		Log.d(TAG, "onAudioRemuxerStart");
	}

	@Override
	public void onAudioRemuxerError(int errorType) {
		Log.d(TAG, "onAudioRemuxerError errorType : " + String.valueOf(errorType));
	}

	@Override
	public void onAudioRemuxerInfo(int infoType, int infoValue) {
		Log.d(TAG, "onAudioRemuxerInfo infoType : " + String.valueOf(infoType) + " infoValue : " + String.valueOf(infoValue));
	}

	@Override
	public void onAudioRemuxerEnd() {
		Log.d(TAG, "onAudioRemuxerEnd");
	}
	
	///////////////////////////////////////////////////////////////////////////
}
