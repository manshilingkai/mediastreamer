package android.slkmedia.mediastreamerdemo;

public interface SLKMediaRemuxerListener {
	public abstract void onMediaRemuxerError(int errorType);
	public abstract void onMediaRemuxerInfo(int infoType, int infoValue);
	public abstract void onMediaRemuxerEnd();
}
