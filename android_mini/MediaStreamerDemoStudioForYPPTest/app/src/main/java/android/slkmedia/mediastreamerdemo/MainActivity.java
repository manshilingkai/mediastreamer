package android.slkmedia.mediastreamerdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button remuxer = (Button)findViewById(R.id.MediaRemuxer);
        remuxer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent in = new Intent();
                in.setClass(MainActivity.this, MediaRemuxerActivity.class);
                startActivity(in);
            }
        });
    }
}
