package android.slkmedia.mediastreamer;

public class VideoOptions {
    public boolean hasVideo = false;

    public int videoEncodeType = MediaStreamer.VIDEO_SOFT_ENCODE;

    public int videoWidth = 0;
    public int videoHeight = 0;
    public int videoFps = 0;
    public int videoRawType = MediaStreamer.VIDEOFRAME_RAWTYPE_I420;

    public int videoProfile = 0; //0:base_line 1:main_profile 2:high_profile
    public int videoBitRate = 0;
    public int encodeMode = 0; //0:VBR or 1:CBR
    public int maxKeyFrameIntervalMs = 0;

    public int quality = 0; //[-5, 5]:CRF
    public boolean bStrictCBR = false;
    public int deblockingFilterFactor = 0; //[-6, 6] -6 light filter, 6 strong
    
    public boolean isEnableNetworkAdaptive = false;
    public int networkAdaptiveMinFps = 12;
    public int networkAdaptiveMinVideoBitrate = 200;
}
