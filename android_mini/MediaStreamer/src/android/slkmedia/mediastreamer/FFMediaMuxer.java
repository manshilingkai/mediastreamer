package android.slkmedia.mediastreamer;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.annotation.TargetApi;
import android.media.AudioFormat;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

public class FFMediaMuxer implements MediaMuxerInterface {
	private static final String TAG = "FFMediaMuxer";

	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}
	
	//Native Context
	private long mNativeContext;
	private static native final void Native_Init();
	
	/**
	 * Called from native code when an interesting event happens. This method
	 * just uses the EventHandler system to post the event back to the main app
	 * thread. We use a weak reference to the original MediaPlayer object so
	 * that the native code is safe from the object disappearing from underneath
	 * it. (This is the cookie passed to native_setup().)
	 */
	private static void postEventFromNative(Object ffmediamuxer_ref, int what,
			int arg1, int arg2, Object obj) {
		FFMediaMuxer ffmm = (FFMediaMuxer) ((WeakReference<?>) ffmediamuxer_ref).get();
		if (ffmm == null) {
			return;
		}

		if (ffmm.ffMediaMuxerCallbackHandler != null) {
			Message msg = ffmm.ffMediaMuxerCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}
	
	private Lock mMediaMuxerLock = null;
	private Condition mMediaMuxerCondition = null;
	private HandlerThread mHandlerThread = null;
	private Handler ffMediaMuxerCallbackHandler = null;

	private String mPublishUrl = null;
	private int mFormat = SLKMediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4;
	private static final int MAX_SEND_BYTE_BUFFER_SIZE = 1024 * 1024;
	private ByteBuffer mSendByteBuffer = null;
	public FFMediaMuxer(String path, int format)
	{
		mMediaMuxerLock = new ReentrantLock(); 
		mMediaMuxerCondition = mMediaMuxerLock.newCondition();
		
		mHandlerThread = new HandlerThread("FFMediaMuxerHandlerThread");
		mHandlerThread.start();
		
		ffMediaMuxerCallbackHandler = new Handler(mHandlerThread.getLooper()) {
			@Override
			public void handleMessage(Message msg) {
				
				switch (msg.what) {
				case SLKMediaMuxer.CALLBACK_MEDIA_MUXER_CONNECTING:
					Log.d(TAG, "CALLBACK_MEDIA_MUXER_CONNECTING");
					if(mediaMuxerListener!=null)
					{
						mediaMuxerListener.onMediaMuxerConnecting();
					}
					break;
				case SLKMediaMuxer.CALLBACK_MEDIA_MUXER_CONNECTED:
					Log.d(TAG, "CALLBACK_MEDIA_MUXER_CONNECTED");
					mMediaMuxerLock.lock();
					isConnected = true;
					mMediaMuxerLock.unlock();
					
					if(mediaMuxerListener!=null)
					{
						mediaMuxerListener.onMediaMuxerConnected();
					}
					break;
				case SLKMediaMuxer.CALLBACK_MEDIA_MUXER_STREAMING:
					Log.d(TAG, "CALLBACK_MEDIA_MUXER_STREAMING");
					if(mediaMuxerListener!=null)
					{
						mediaMuxerListener.onMediaMuxerStreaming();
					}
					break;
				case SLKMediaMuxer.CALLBACK_MEDIA_MUXER_ERROR:
					Log.d(TAG, "CALLBACK_MEDIA_MUXER_ERROR");
					mMediaMuxerLock.lock();
					isConnected = false;
					mMediaMuxerLock.unlock();
					
					if(mediaMuxerListener!=null)
					{
						mediaMuxerListener.onMediaMuxerError(msg.arg1);
					}
					break;
				case SLKMediaMuxer.CALLBACK_MEDIA_MUXER_INFO:
					if(mediaMuxerListener!=null)
					{
						mediaMuxerListener.onMediaMuxerInfo(msg.arg1, msg.arg2);
					}
					break;
				case SLKMediaMuxer.CALLBACK_MEDIA_MUXER_END:
					Log.d(TAG, "CALLBACK_MEDIA_MUXER_END");
					mMediaMuxerLock.lock();
					isConnected = false;
					mMediaMuxerLock.unlock();
					
					if(mediaMuxerListener!=null)
					{
						mediaMuxerListener.onMediaMuxerEnd();
					}
					break;
				default:
					break;
				}
			}
		};
		
		mPublishUrl = path;
		mFormat = format;
		
		mSendByteBuffer = ByteBuffer.allocateDirect(MAX_SEND_BYTE_BUFFER_SIZE);
	}
	
	private MediaMuxerListener mediaMuxerListener = null;
	@Override
	public void setMediaMuxerListener(MediaMuxerListener listener)
	{
		mediaMuxerListener = listener;
	}
	
	public final static int AV_SAMPLE_FMT_NONE = -1;
	public final static int AV_SAMPLE_FMT_U8 = 0;          ///< unsigned 8 bits
	public final static int AV_SAMPLE_FMT_S16 = 1;         ///< signed 16 bits
	public final static int AV_SAMPLE_FMT_S32 = 2;         ///< signed 32 bits
	public final static int AV_SAMPLE_FMT_FLT = 3;         ///< float
	public final static int AV_SAMPLE_FMT_DBL = 4;         ///< double
	
	private int mVideoWidth = 0;
	private int mVideoHeight = 0;
	private int mVideoFps = 0;
	private int mVideoRotation = 0;
	private int mVideoBitrate = 0;
	private ByteBuffer mSps = null;
	private ByteBuffer mPps = null;
	
	private int mAudioSamplerate = 0;
	private int mAudioChannels = 0;
	private int mAudioSampleFormat = AV_SAMPLE_FMT_S16;
	private int mAudioBitrate = 0;
	private ByteBuffer mAsc = null;
	
	private int mTrackIndex = -1;
	private int mVideoTrackIndex = -1;
	private int mAudioTrackIndex = -1;
	@Override
	public int addTrack(MediaFormat format) {
		int ret = -1;
		mMediaMuxerLock.lock();
		if(isReleased)
		{
			mMediaMuxerLock.unlock();
			return -1;
		}
		if(isStarted)
		{
			mMediaMuxerLock.unlock();
			return -1;
		}
		
		if(format.getString(MediaFormat.KEY_MIME).startsWith(MediaFormat.MIMETYPE_VIDEO_AVC))
		{
			if(mVideoTrackIndex==-1)
			{
				mTrackIndex++;
				mVideoTrackIndex = mTrackIndex;
			}
			
			mVideoWidth = format.getInteger(MediaFormat.KEY_WIDTH);
			mVideoHeight = format.getInteger(MediaFormat.KEY_HEIGHT);
			
			try{
				mVideoFps = format.getInteger(MediaFormat.KEY_FRAME_RATE);
			}catch(NullPointerException e)
			{
				mVideoFps = 25;
			}
			
//			mVideoBitrate = format.getInteger(MediaFormat.KEY_BIT_RATE)/1024; //bits/sec/1024
			mVideoBitrate = 0;
			
			if(Build.VERSION.SDK_INT>=21)
			{
				try{
					mVideoRotation = format.getInteger(MediaFormat.KEY_ROTATION);
				}catch(NullPointerException e)
				{
					mVideoRotation = 0;
				}	
			}else{
				mVideoRotation = 0;
			}
			
			mSps = format.getByteBuffer("csd-0");
			mPps = format.getByteBuffer("csd-1");
			
			ret = mVideoTrackIndex;
			mMediaMuxerLock.unlock();
			return ret;
		}else if(format.getString(MediaFormat.KEY_MIME).startsWith(MediaFormat.MIMETYPE_AUDIO_AAC))
		{
			if(mAudioTrackIndex==-1)
			{
				mTrackIndex++;
				mAudioTrackIndex = mTrackIndex;
			}
			
			mAudioSamplerate = format.getInteger(MediaFormat.KEY_SAMPLE_RATE);
			mAudioChannels = format.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
//			mAudioBitrate = format.getInteger(MediaFormat.KEY_BIT_RATE)/1024; //bits/sec
			mAudioBitrate = 0;
			
			if(Build.VERSION.SDK_INT>=24)
			{
				int pcm_encoding = AudioFormat.ENCODING_PCM_16BIT;
				try{
					pcm_encoding = format.getInteger("pcm-encoding");
				}catch(NullPointerException e)
				{
					pcm_encoding = AudioFormat.ENCODING_PCM_16BIT;
				}
				
				if(pcm_encoding==AudioFormat.ENCODING_PCM_8BIT)
				{
					mAudioSampleFormat = AV_SAMPLE_FMT_U8;
				}else if(pcm_encoding==AudioFormat.ENCODING_PCM_FLOAT)
				{
					mAudioSampleFormat = AV_SAMPLE_FMT_FLT;
				}else{
					mAudioSampleFormat = AV_SAMPLE_FMT_S16;
				}
			}
			
			mAsc = format.getByteBuffer("csd-0");
			ret = mAudioTrackIndex;
			mMediaMuxerLock.unlock();
			return ret;
		}
		
		mMediaMuxerLock.unlock();
		return -1;
	}
	
	private boolean isStarted = false;
	private boolean isConnected = false;
	@Override
	public void start() {
		mMediaMuxerLock.lock();
		if(isReleased)
		{
			mMediaMuxerLock.unlock();
			return;
		}
		if(isStarted)
		{
			mMediaMuxerLock.unlock();
			return;
		}
		
		boolean hasVideo = false;
		if(mVideoTrackIndex!=-1)
		{
			hasVideo = true;
		}
		boolean hasAudio = false;
		if(mAudioTrackIndex!=-1)
		{
			hasAudio = true;
		}
		
		byte[] sps = mSps.array();
		byte[] pps = mPps.array();
		byte[] asc = mAsc.array();
		Native_Start(mPublishUrl, mFormat, hasVideo, mVideoWidth, mVideoHeight, mVideoFps, mVideoRotation, mVideoBitrate, sps, sps.length, pps, pps.length,
				hasAudio, mAudioSamplerate, mAudioChannels, mAudioSampleFormat, mAudioBitrate, asc, asc.length, new WeakReference<FFMediaMuxer>(this));
		
		isStarted = true;
		isConnected  = false;
		mMediaMuxerLock.unlock();
		return;
	}
	private native void Native_Start(String publishUrl, int format, boolean hasVideo, int videoWidth, int videoHeight, int videoFps, int videoRotation, int videoBitrate, byte[] sps, int sps_len, byte[] pps, int pps_len,
			boolean hasAudio, int audioSamplerate, int audioChannels, int audioSampleFormat, int audioBitrate, byte[] asc, int asc_len, Object ffmediamuxer_this);

	@Override
	public void stop() {
		mMediaMuxerLock.lock();
		if(isReleased)
		{
			mMediaMuxerLock.unlock();
			return;
		}
		if(!isStarted)
		{
			mMediaMuxerLock.unlock();
			return;
		}
		
		Native_Stop();
		
		isStarted = false;
		isConnected = false;
		mMediaMuxerLock.unlock();
		return;
	}
	private native void Native_Stop();
	
	public final static int VIDEO_PACKET_H264_SPS_PPS = 0;
	public final static int VIDEO_PACKET_H264_KEY_FRAME = 1;
	public final static int VIDEO_PACKET_H264_P_OR_B_FRAME = 2;
	public final static int AUDIO_PACKET_AAC_HEADER = 3;
	public final static int AUDIO_PACKET_AAC_BODY = 4;
    
	@Override
	public void writeSampleData(int trackIndex, ByteBuffer byteBuf,
			BufferInfo bufferInfo) {
		mMediaMuxerLock.lock();
		if(isReleased)
		{
			mMediaMuxerLock.unlock();
			return;
		}
		if(!isStarted)
		{
			Log.w(TAG, "FFMediaMuxer have not Started, So MediaSample May be droped");
			mMediaMuxerLock.unlock();
			return;
		}
		
		if(!isConnected)
		{
			Log.w(TAG, "FFMediaMuxer have not Connected, So MediaSample May be droped");
			mMediaMuxerLock.unlock();
			return;
		}
		if(trackIndex==mVideoTrackIndex)
		{
			mSendByteBuffer.clear();
			mSendByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
			mSendByteBuffer.rewind();
			mSendByteBuffer.put(byteBuf);
			byte[] data = mSendByteBuffer.array();
			int offset = mSendByteBuffer.arrayOffset();
			int size = bufferInfo.size;
			long pts = bufferInfo.presentationTimeUs/1000;
			long dts = pts;
			int packetType = VIDEO_PACKET_H264_P_OR_B_FRAME;
			if((bufferInfo.flags & MediaCodec.BUFFER_FLAG_KEY_FRAME) != 0)
			{
				packetType = VIDEO_PACKET_H264_KEY_FRAME;
			}
			
			Native_WriteAVPacket(data, offset, size, pts, dts, packetType);
		}else if(trackIndex==mAudioTrackIndex)
		{
			mSendByteBuffer.clear();
			mSendByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
			mSendByteBuffer.rewind();
			mSendByteBuffer.put(byteBuf);
			byte[] data = mSendByteBuffer.array();
			int offset = mSendByteBuffer.arrayOffset();
			int size = bufferInfo.size;
			long pts = bufferInfo.presentationTimeUs/1000;
			long dts = pts;
			int packetType = AUDIO_PACKET_AAC_BODY;
			Native_WriteAVPacket(data, offset, size, pts, dts, packetType);
		}
		mMediaMuxerLock.unlock();
	}
	private native void Native_WriteAVPacket(byte[] data, int offset, int size, long pts, long dts, int packetType);
	
	@Override
	public long getPublishDelayTimeMs()
	{
		long ret = 0;
		mMediaMuxerLock.lock();
		if(isReleased)
		{
			mMediaMuxerLock.unlock();
			return ret;
		}
		if(!isStarted)
		{
			mMediaMuxerLock.unlock();
			return ret;
		}
		if(!isConnected)
		{
			mMediaMuxerLock.unlock();
			return ret;
		}
		ret = Native_GetPublishDelayTimeMs();
		mMediaMuxerLock.unlock();
		
		return ret;
	}
	private native long Native_GetPublishDelayTimeMs();
	
	private boolean isReleased = false;
	private boolean isFinishAllCallbacksAndMessages = false;
	@Override
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2) public void release()
	{
		mMediaMuxerLock.lock();
		if(isReleased)
		{
			mMediaMuxerLock.unlock();
			return;
		}
		
		if(isStarted)
		{
			Native_Stop();
			isStarted = false;
			isConnected = false;
		}
		
		ffMediaMuxerCallbackHandler.post(new Runnable() {
			@Override
			public void run() {
				ffMediaMuxerCallbackHandler.removeCallbacksAndMessages(null);
				
				mMediaMuxerLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mMediaMuxerCondition.signalAll();
				mMediaMuxerLock.unlock();
			}
		});
		
		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mMediaMuxerCondition.await(1, TimeUnit.SECONDS);
			}
			
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
		}

		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR2)
		{
			mHandlerThread.quitSafely();
		}else{
			mHandlerThread.quit();
		}
		
		isReleased = true;
		
		if(mSendByteBuffer!=null)
		{
			mSendByteBuffer.clear();
			mSendByteBuffer = null;
		}

		mMediaMuxerLock.unlock();
	}
}
