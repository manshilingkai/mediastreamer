package android.slkmedia.mediastreamer.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import android.util.Log;

public class FileUtils {
	private static final String TAG = "FileUtils";

	public static void deleteFile(String path)
	{
		if(path == null || path.isEmpty()) return;
		
		File file = new File(path);
		if(file!=null)
		{
			try{
				if(file.isFile() && file.exists())
				{
					file.delete();
				}
			}catch (SecurityException e){
				
			}
		}
	}
	
	public static boolean copyNio(String from, String to) {
	    FileChannel input = null;
	    FileChannel output = null;
	    boolean ret = true;
	    try {
	        input = new FileInputStream(new File(from)).getChannel();
	        output = new FileOutputStream(new File(to)).getChannel();
	        output.transferFrom(input, 0, input.size());
	    } catch (Exception e) {
	        Log.e(TAG, "copyNio : error occur while copy");
	        ret = false;
	    } finally {
	    	try {
				input.close();
				output.close();
			}catch (java.lang.NullPointerException e) {
				
			}
	    	catch (IOException e) {
			}
	    }
	    
	    return ret;
	}
}
