package android.slkmedia.mediastreamer;

public class AudioOptions {
	public boolean hasAudio = false;
	
	public int audioSampleRate = 44100;
	public int audioNumChannels = 2;
	public int audioBitRate = 128;
	
	public boolean isExternalAudioInput = false;
}
