package android.slkmedia.mediastreamer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import android.os.Handler;
import android.os.Looper;
import android.slkmedia.mediaeditengine.AudioPlayer;
import android.slkmedia.mediaeditengine.AudioPlayerListener;

public class MixAudioFilePlugin implements AudioPlayerListener{
	private static final String TAG = "MixAudioFilePlugin";

	private AudioPlayer mAudioPlayer = null;
	private Handler mhandler = null;
	private ByteBuffer mMixByteBuffer = null;
	public MixAudioFilePlugin(int sampleRate, int channelCount)
	{
		Looper workLooper = Looper.myLooper();
		if(workLooper==null)
		{
			workLooper = Looper.getMainLooper();
		}
		
		mhandler = new Handler(workLooper);
		
		mAudioPlayer = new AudioPlayer(true, sampleRate, channelCount);
		mAudioPlayer.setListener(this);
		
		mMixByteBuffer = ByteBuffer.allocateDirect(sampleRate*channelCount*2);
	}
	
	private MixAudioFilePluginListener mMixAudioFilePluginListener = null;
	public void setMixAudioFilePluginListener(MixAudioFilePluginListener listener)
	{
		mMixAudioFilePluginListener = listener;
	}
	
	public void playAudioFile(String audioFilePath)
	{
		if(mAudioPlayer==null) return;
		
		try{
			mAudioPlayer.stop();
		}catch(IllegalStateException e)
		{}
		
		mhandler.removeCallbacksAndMessages(null);
		
		mAudioPlayer.setDataSource(audioFilePath);
		mAudioPlayer.prepareAsyncToPlay();
	}
	
	public void playAudioFile(String audioFilePath, float volume)
	{
		if(mAudioPlayer==null) return;

		try{
			mAudioPlayer.stop();
		}catch(IllegalStateException e)
		{}
		
		mhandler.removeCallbacksAndMessages(null);
		
		mAudioPlayer.setDataSource(audioFilePath);
		mAudioPlayer.setVolume(volume);
		mAudioPlayer.prepareAsyncToPlay();
	}
	
	public void resume()
	{
		try{
			if(mAudioPlayer!=null)
			{
				mAudioPlayer.play();
			}
		}catch(IllegalStateException e)
		{
			
		}
	}
	
	public void pause()
	{
		try{
			if(mAudioPlayer!=null)
			{
				mAudioPlayer.pause();
			}
		}catch(IllegalStateException e)
		{
			
		}
	}
	
	public void stop()
	{
		try{
			if(mAudioPlayer!=null)
			{
				mAudioPlayer.stop();
			}
		}catch(IllegalStateException e)
		{
			
		}
	}
	
	public void setVolume(float volume)
	{
		try{
			if(mAudioPlayer!=null)
			{
				mAudioPlayer.setVolume(volume);
			}
		}catch(IllegalStateException e)
		{
			
		}
	}
	
	public boolean isPlaying()
	{
		boolean ret = false;
		
		try{
			if(mAudioPlayer!=null)
			{
				ret = mAudioPlayer.isPlaying();
			}
		}catch(IllegalStateException e)
		{
			
		}
		
		return ret;
	}
	
	public ByteBuffer mixWithExternal(ByteBuffer byteBuffer, int byteBufferSize)
	{
		if(mAudioPlayer==null) return byteBuffer;
		
		mAudioPlayer.MixWithExternal(byteBuffer, byteBufferSize);
		return byteBuffer;
	}
	
	public ByteBuffer mixWithExternalByteBuffer(ByteBuffer byteBuffer, int byteBufferSize)
	{
		if(mAudioPlayer==null) return byteBuffer;
		
		byteBuffer.position(0);
		byteBuffer.limit(byteBufferSize);
		
		mMixByteBuffer.clear();
		mMixByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		mMixByteBuffer.rewind();
		mMixByteBuffer.put(byteBuffer);
		
		mAudioPlayer.MixWithExternal(mMixByteBuffer, byteBufferSize);
		
		mMixByteBuffer.position(0);
		mMixByteBuffer.limit(byteBufferSize);
		
		byteBuffer.clear();
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		byteBuffer.rewind();
		byteBuffer.put(mMixByteBuffer);
		
		return byteBuffer;
	}
	
	public void mixWithExternalData(byte[] data, int offset, int size)
	{
		if(mAudioPlayer==null) return;
		
		mAudioPlayer.MixWithExternalData(data, offset, size);
	}
	
	public void release()
	{
		if(mAudioPlayer!=null)
		{
			mAudioPlayer.release();
			mAudioPlayer = null;
		}
		
		if(mhandler!=null)
		{
			mhandler.removeCallbacksAndMessages(null);
		}
		
		if(mMixByteBuffer!=null)
		{
			mMixByteBuffer.clear();
			mMixByteBuffer = null;
		}
	}

	@Override
	public void onPrepared() {
		mhandler.post(new Runnable(){

			@Override
			public void run() {				
				if(mMixAudioFilePluginListener!=null)
				{
					mMixAudioFilePluginListener.onMixAudioFilePluginStart();
				}
			}
		});
	}

	@Override
	public void onError(int what, int extra) {
		mhandler.post(new Runnable(){

			@Override
			public void run() {
				if(mAudioPlayer!=null)
				{
					mAudioPlayer.stop();
				}
				
				if(mMixAudioFilePluginListener!=null)
				{
					mMixAudioFilePluginListener.onMixAudioFilePluginEnd();
				}
			}
		});
	}

	@Override
	public void onInfo(int what, int extra) {
	}

	@Override
	public void onCompletion() {
		mhandler.post(new Runnable(){

			@Override
			public void run() {
				if(mAudioPlayer!=null)
				{
					mAudioPlayer.stop();
				}
				
				if(mMixAudioFilePluginListener!=null)
				{
					mMixAudioFilePluginListener.onMixAudioFilePluginEnd();
				}
			}
		});
	}

	@Override
	public void OnSeekComplete() {
	}
	
	public interface MixAudioFilePluginListener
	{
		public abstract void onMixAudioFilePluginStart();
		public abstract void onMixAudioFilePluginEnd();
	}
}
