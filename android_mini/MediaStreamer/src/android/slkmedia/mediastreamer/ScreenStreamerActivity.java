package android.slkmedia.mediastreamer;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Process;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;

public class ScreenStreamerActivity extends Activity{
	private static final String TAG = "ScreenStreamerActivity";
	
	public static final String PUBLISH_URL_KEY = "PublishUrl";

	private static final int SCREEN_STREAMER_REQUEST_CODE  = 101;

	private MediaProjectionManager mProjectionManager = null;
	private MediaProjection mMediaProjection = null;

	private FrameLayout mFrameLayout = null;
	private Button mStartOrStopButton = null;
	
	private Intent mIntentService = null;
	private ScreenStreamerService mScreenStreamerService = null;

	private Handler mMainHander = null;
	
	private PublishStreamOptions mPublishStreamOptions = new PublishStreamOptions();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        mProjectionManager = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        mFrameLayout = new FrameLayout(this);
        setContentView(mFrameLayout);
        
        Intent intent = getIntent();
        mPublishStreamOptions.publishUrl = intent.getStringExtra(PUBLISH_URL_KEY);
        
        mStartOrStopButton = new Button(this);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,FrameLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;
        mStartOrStopButton.setLayoutParams(layoutParams);
        mStartOrStopButton.setText("Start");
        mFrameLayout.addView(mStartOrStopButton);
                
        mStartOrStopButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(mScreenStreamerService.isRunning())
				{
					mScreenStreamerService.stopStreaming();
					mMainHander.removeCallbacksAndMessages(null);

					mStartOrStopButton.setText("Start");
				}else{
					Intent screenCaptureIntent = mProjectionManager.createScreenCaptureIntent();
					startActivityForResult(screenCaptureIntent, SCREEN_STREAMER_REQUEST_CODE);
				}
			}
		});
        
        mIntentService = new Intent(this, ScreenStreamerService.class);
        Log.d(TAG, "start ScreenStreamerService");
        startService(mIntentService);
//        startForegroundService(mIntentService);
        Log.d(TAG, "bind ScreenStreamerService");
        bindService(mIntentService, screenStreamerServiceConnection, BIND_AUTO_CREATE);
        
        mMainHander = new Handler();
    }
    
    @Override
    protected void onDestroy() {
      super.onDestroy();
      
      unbindService(screenStreamerServiceConnection);
      Log.d(TAG, "unbind ScreenStreamerService");
      stopService(mIntentService);
      Log.d(TAG, "stop ScreenStreamerService");
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	if (requestCode == SCREEN_STREAMER_REQUEST_CODE && resultCode == RESULT_OK) {
		mMediaProjection = mProjectionManager.getMediaProjection(resultCode, data);
		mScreenStreamerService.setMediaProjection(mMediaProjection);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		makePublishStreamOptions(mPublishStreamOptions, metrics.widthPixels, metrics.heightPixels); 
		mScreenStreamerService.setPublishStreamOptions(mPublishStreamOptions, metrics.densityDpi);			
		mScreenStreamerService.setScreenStreamerServiceListener(new ScreenStreamerServiceListener(){
			@Override
			public void onScreenStreamerConnecting() {
				Log.d(TAG, "ScreenStreamerService - ScreenStreamerConnecting");
			}

			@Override
			public void onScreenStreamerConnected() {
				Log.d(TAG, "ScreenStreamerService - ScreenStreamerConnected");
			}

			@Override
			public void onScreenStreamerStreaming() {
				Log.d(TAG, "ScreenStreamerService - ScreenStreamerStreaming");
			}

			@Override
			public void onScreenStreamerError(int errorType) {
				Log.d(TAG, "ScreenStreamerService - ScreenStreamerError ErrorType :" + String.valueOf(errorType));
				
				mMainHander.post(new Runnable(){
					@Override
					public void run() {
						mStartOrStopButton.setText("Start");
					}
				});
			}

			@Override
			public void onScreenStreamerInfo(int infoType, int infoValue) {
				Log.d(TAG, "ScreenStreamerService - ScreenStreamerInfo InfoType : " + String.valueOf(infoType) + " InfoValue : " + String.valueOf(infoValue));
			}

			@Override
			public void onScreenStreamerEnd() {
				Log.d(TAG, "ScreenStreamerService - ScreenStreamerEnd");					
			}
		});
		
		mScreenStreamerService.startStreaming();
		mStartOrStopButton.setText("Stop");
      }
    }
    
    private ServiceConnection screenStreamerServiceConnection = new ServiceConnection(){
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			ScreenStreamerService.ScreenStreamerBinder binder = (ScreenStreamerService.ScreenStreamerBinder)service;
			mScreenStreamerService = binder.getScreenStreamerService();
			mStartOrStopButton.setText(mScreenStreamerService.isRunning()? "Stop":"Start");
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
		}

		@Override
		public void onBindingDied(ComponentName name) {
		}
	};
	
 	public static final int MAX_VIDEO_SIDE_SIZE = 1280;
 	private void makePublishStreamOptions(PublishStreamOptions publishStreamOptions, int displayWidth, int displayHeight)
 	{
		int publishVideoWidth = 0;
		int publishVideoHeight = 0;
		if(displayWidth >= displayHeight)
		{
			publishVideoWidth = MAX_VIDEO_SIDE_SIZE;
			publishVideoHeight = publishVideoWidth * displayHeight / displayWidth;
			
			if(publishVideoHeight%2!=0)
			{
				publishVideoHeight = publishVideoHeight + 1;
			}
		}else {
			publishVideoHeight = MAX_VIDEO_SIDE_SIZE;
			publishVideoWidth = displayWidth * publishVideoHeight / displayHeight;
			
			if(publishVideoWidth%2!=0)
			{
				publishVideoWidth = publishVideoWidth + 1;
			}
		}
		
		publishStreamOptions.videoWidth = publishVideoWidth;
		publishStreamOptions.videoHeight = publishVideoHeight;
 	}
}
