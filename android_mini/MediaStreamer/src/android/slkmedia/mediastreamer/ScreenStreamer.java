package android.slkmedia.mediastreamer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.projection.MediaProjection;
import android.opengl.EGL14;
import android.opengl.EGLContext;
import android.opengl.GLES20;
import android.os.Handler;
import android.slkmedia.mediastreamer.gpuimage.GPUImageAmaroFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageAntiqueFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBeautyFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBlackCatFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBrannanFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBrooklynFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageCoolFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageCrayonFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageN1977Filter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageRGBFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageRotationMode;
import android.slkmedia.mediastreamer.gpuimage.GPUImageSketchFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageUtils;
import android.slkmedia.mediastreamer.gpuimage.OpenGLUtils;
import android.slkmedia.mediastreamer.gpuimage.TextureRotationUtil;
import android.slkmedia.mediastreamer.gpuimage.GPUImageFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageInputFilter;
import android.slkmedia.mediastreamer.SLKMediaMuxer.OutputFormat;
import android.slkmedia.mediastreamer.egl.EGL;
import android.util.Log;
import android.view.Surface;

public class ScreenStreamer {
	private static final String TAG = "ScreenStreamer";
	
	private Handler mMainHander = null;
    public ScreenStreamer()
    {
        mMainHander = new Handler();
    }
	
	///////////////////////////////////////////////////////////// GPUImage[GL] //////////////////////////////////////////////////////////////

	private FloatBuffer mGLCubeBuffer;
	private FloatBuffer mGLTextureBuffer;
    private float[] rotatedTex;
	
	private GPUImageInputFilter mInputFilter = null;
	private GPUImageFilter mTargetInputFilter = null;
	private GPUImageFilter mWorkFilter = null;
	
    private void initGPUImageParam()
    {
		mGLCubeBuffer = ByteBuffer.allocateDirect(TextureRotationUtil.CUBE.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLCubeBuffer.put(TextureRotationUtil.CUBE).position(0);

        rotatedTex = new float[8];
        TextureRotationUtil.calculateCropTextureCoordinates(GPUImageRotationMode.kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        
        mGLTextureBuffer = ByteBuffer.allocateDirect(8 * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLTextureBuffer.put(rotatedTex).position(0);
    	
        mInputFilter = new GPUImageInputFilter();
        mTargetInputFilter = new GPUImageRGBFilter();
        mWorkFilter = new GPUImageRGBFilter();
    }
    
	private boolean isGPUImageWorkerOpened = false;
	private void openGPUImageWorker()
	{
    	if(mInputTextureId == OpenGLUtils.NO_TEXTURE) {
    		mInputTextureId = OpenGLUtils.getExternalOESTextureID();
    		mInputSurfaceTexture = new SurfaceTexture(mInputTextureId);
    		mInputSurfaceTexture.setDefaultBufferSize(mVideoWidth, mVideoHeight);
    		mInputSurfaceTexture.setOnFrameAvailableListener(new OnFrameAvailableListener(){
				@Override
				public void onFrameAvailable(SurfaceTexture surfaceTexture) {
					Log.d(TAG, "On Handle Screen Frame");
				}
			});
    		mInputSurface = new Surface(mInputSurfaceTexture); //mInputSurface.release();
    	}
		
    	mInputFilter.init();
    	
    	mTargetInputFilter.init();
        isTargetInputFilterOutputSizeUpdated = true;

        mWorkFilter.init();
        isOutputSizeUpdated = true;
	}
	
	private void closeGPUImageWorker()
	{
	    if(mInputTextureId != OpenGLUtils.NO_TEXTURE)
	    {
	    	GLES20.glDeleteTextures(1, new int[]{mInputTextureId}, 0);
	    	mInputTextureId = OpenGLUtils.NO_TEXTURE;
	    }
	    
	    if(mInputSurfaceTexture!=null)
	    {
	    	mInputSurfaceTexture.release();
	    	mInputSurfaceTexture = null;
	    }
	    
	    if(mInputSurface!=null)
	    {
	    	mInputSurface.release();
	    	mInputSurface = null;
	    }
		
    	mInputFilter.destroy();
    	
    	mTargetInputFilter.destroy();
    	isTargetInputFilterOutputSizeUpdated = false;
    	
    	mWorkFilter.destroy();
        isOutputSizeUpdated = false;
        
        destroyFrameBufferObject();
	}
	
	private VirtualDisplay mVirtualDisplay = null;
	private void createVirtualDisplay() {
		mVirtualDisplay = mMediaProjection.createVirtualDisplay("MainScreen", mVideoWidth, mVideoHeight, mVirtualDisplayDpi,
				DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC, mInputSurface, null, null);
	}
	private void releaseVirtualDisplay()
	{
	    mVirtualDisplay.release();
	    mMediaProjection.stop();
	}
	
	private float[] mTransformMatrix = new float[16];
	private boolean onDrawFrame() {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
		if(mInputTextureId == OpenGLUtils.NO_TEXTURE || mInputSurfaceTexture==null) return false;
		if(mVideoWidth<=0 || mVideoHeight<=0) return false;
		if(mTargetVideoWidth<=0 || mTargetVideoHeight<=0) return false;
		if(mSurfaceWidth<=0 || mSurfaceHeight<=0) return false;
		
		mInputSurfaceTexture.updateTexImage();
		mInputSurfaceTexture.getTransformMatrix(mTransformMatrix);
		mInputFilter.setTextureTransformMatrix(mTransformMatrix);
        int inputTextureID = mInputFilter.onDrawToTexture(mInputTextureId, mVideoWidth, mVideoHeight);
        if(inputTextureID == OpenGLUtils.NO_TEXTURE) return false;
        
        /*
        // draw to target input
        initFrameBufferObject(mTargetVideoWidth, mTargetVideoHeight);
        
    	//bind
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
		GLES20.glViewport(0, 0, mFrameBufferWidth, mFrameBufferHeight);
    	GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
    	
    	ScaleAspectFillForTargetInputFilter(mRotationModeForTargetInputFilter, mTargetVideoWidth, mTargetVideoHeight, mVideoWidth, mVideoHeight);
    	mTargetInputFilter.onDrawFrame(inputTextureID, mGLCubeBuffer, mGLTextureBuffer);
    	
        //unbind
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
		int outputTextureID = mFrameBufferTextures[0];
        if(outputTextureID == OpenGLUtils.NO_TEXTURE) return false;
		
		// draw to surface
        int displayWidth = mSurfaceWidth;
        int displayHeight = mSurfaceHeight;
        int displayX = 0;
        int displayY = 0;
        if(mVideoScalingModeForWorkFilter == GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT)
        {
        	ScaleAspectFit(mRotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mFrameBufferWidth, mFrameBufferHeight);
        }else if(mVideoScalingModeForWorkFilter == GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING){
        	ScaleAspectFill(mRotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mFrameBufferWidth, mFrameBufferHeight);
        }else{
        	ScaleToFill(mRotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mFrameBufferWidth, mFrameBufferHeight);
        }
		mWorkFilter.onDrawFrame(outputTextureID, mGLCubeBuffer, mGLTextureBuffer);
		return true;
		*/
        
        int displayWidth = mSurfaceWidth;
        int displayHeight = mSurfaceHeight;
        int displayX = 0;
        int displayY = 0;
        int sourceWidth = mVideoWidth;
        int sourceHeight = mVideoHeight;
        if(mVideoScalingModeForWorkFilter == GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT)
        {
        	ScaleAspectFit(mRotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, sourceWidth, sourceHeight);
        }else if(mVideoScalingModeForWorkFilter == GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)
        {
        	ScaleAspectFill(mRotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, sourceWidth, sourceHeight);
        }else{
        	ScaleToFill(mRotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, sourceWidth, sourceHeight);
        }
		mWorkFilter.onDrawFrame(inputTextureID, mGLCubeBuffer, mGLTextureBuffer);
		return true;
	}
	
	private void switchFilter(int type, String filterDir)
	{
    	if(mWorkFilter!=null)
    	{
    		mWorkFilter.destroy();
    		mWorkFilter = null;
    	}
    	
        switch (type) {
        case GPUImageUtils.FILTER_SKETCH:
        	mWorkFilter = new GPUImageSketchFilter();
        	mWorkFilter.init();
            break;
        case GPUImageUtils.FILTER_AMARO:
        	mWorkFilter = new GPUImageAmaroFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_ANTIQUE:
        	mWorkFilter = new GPUImageAntiqueFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BEAUTY:
        	mWorkFilter = new GPUImageBeautyFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BLACKCAT:
        	mWorkFilter = new GPUImageBlackCatFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BRANNAN:
        	mWorkFilter = new GPUImageBrannanFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BROOKLYN:
        	mWorkFilter = new GPUImageBrooklynFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_COOL:
        	mWorkFilter = new GPUImageCoolFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_CRAYON:
        	mWorkFilter = new GPUImageCrayonFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_N1977:
        	mWorkFilter = new GPUImageN1977Filter(filterDir);
        	mWorkFilter.init();
        	break;
        default:
            mWorkFilter = new GPUImageRGBFilter();
            mWorkFilter.init();
            break;
        }    
        isOutputSizeUpdated = true;
	}
	
	private int outputWidth = -1;
	private int outputHeight = -1;
	private boolean isOutputSizeUpdated = false;
	private void ScaleToFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
	{
	    int src_width;
	    int src_height;
	    
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
	    {
	        src_width = videoHeight;
	        src_height = videoWidth;
	    }else{
	        src_width = videoWidth;
	        src_height = videoHeight;
	    }
	    
	    videoWidth = src_width;
	    videoHeight = src_height;
	    
	    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
	        outputWidth = videoWidth;
	        outputHeight = videoHeight;
	        
	        isOutputSizeUpdated = true;
	    }
	    
	    if (isOutputSizeUpdated) {
	        isOutputSizeUpdated = false;
	        mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
	    }
	    	    
	    GLES20.glClearColor(0.f, 0.f, 0.f, 0.f);
	    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	    
	    GLES20.glViewport(displayX, displayY, displayWidth, displayHeight);
	    
	    TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
	    mGLTextureBuffer.put(rotatedTex).position(0);
	}
	
    private void ScaleAspectFit(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
        int x = 0;
        int y = 0;
        int w = 0;
        int h = 0;
        
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        videoWidth = src_width;
        videoHeight = src_height;
        
        if(displayWidth*videoHeight>videoWidth*displayHeight)
        {
            int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
            int viewPortVideoHeight = displayHeight;
            
            x = displayX+(displayWidth - viewPortVideoWidth)/2;
            y = displayY;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }else
        {
            int viewPortVideoWidth = displayWidth;
            int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
            
            x = displayX;
            y = displayY+(displayHeight - viewPortVideoHeight)/2;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }
        
        if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
            outputWidth = videoWidth;
            outputHeight = videoHeight;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
                
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
                
        GLES20.glViewport(x, y, w, h);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
    
    //LayoutModeScaleAspectFill
    private void ScaleAspectFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        int dst_width = displayWidth;
        int dst_height = displayHeight;
        
        int crop_x;
        int crop_y;
        
        int crop_width;
        int crop_height;
        
        if(src_width*dst_height>dst_width*src_height)
        {
            crop_width = dst_width*src_height/dst_height;
            crop_height = src_height;
            
            crop_x = (src_width - crop_width)/2;
            crop_y = 0;

        }else if(src_width*dst_height<dst_width*src_height)
        {
            crop_width = src_width;
            crop_height = dst_height*src_width/dst_width;
            
            crop_x = 0;
            crop_y = (src_height - crop_height)/2;
        }else {
            crop_width = src_width;
            crop_height = src_height;
            crop_x = 0;
            crop_y = 0;
        }
        
        float minX = (float)crop_x/(float)src_width;
        float minY = (float)crop_y/(float)src_height;
        float maxX = 1.0f - minX;
        float maxY = 1.0f - minY;
        
        if (outputWidth!=crop_width || outputHeight!=crop_height) {
            outputWidth = crop_width;
            outputHeight = crop_height;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(displayX, displayY, displayWidth, displayHeight);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
    
	private int mTargetInputFilterOutputWidth = -1;
	private int mTargetInputFilterOutputHeight = -1;
	private boolean isTargetInputFilterOutputSizeUpdated = false;
    private void ScaleAspectFillForTargetInputFilter(GPUImageRotationMode rotationMode, int targetWidth, int targetHeight, int sourceWidth, int sourceHeight)
    {
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = sourceHeight;
            src_height = sourceWidth;
        }else{
            src_width = sourceWidth;
            src_height = sourceHeight;
        }
        
        int dst_width = targetWidth;
        int dst_height = targetHeight;
        
        int crop_x;
        int crop_y;
        
        int crop_width;
        int crop_height;
        
        if(src_width*dst_height>dst_width*src_height)
        {
            crop_width = dst_width*src_height/dst_height;
            crop_height = src_height;
            
            crop_x = (src_width - crop_width)/2;
            crop_y = 0;
            
        }else if(src_width*dst_height<dst_width*src_height)
        {
            crop_width = src_width;
            crop_height = dst_height*src_width/dst_width;
            
            crop_x = 0;
            crop_y = (src_height - crop_height)/2;
        }else {
            crop_width = src_width;
            crop_height = src_height;
            crop_x = 0;
            crop_y = 0;
        }
        
        float minX = (float)crop_x/(float)src_width;
        float minY = (float)crop_y/(float)src_height;
        float maxX = 1.0f - minX;
        float maxY = 1.0f - minY;
        
        if (mTargetInputFilterOutputWidth!=crop_width || mTargetInputFilterOutputHeight!=crop_height) {
        	mTargetInputFilterOutputWidth = crop_width;
        	mTargetInputFilterOutputHeight = crop_height;
            
        	isTargetInputFilterOutputSizeUpdated = true;
        }
        
        if (isTargetInputFilterOutputSizeUpdated) {
        	isTargetInputFilterOutputSizeUpdated = false;
            mTargetInputFilter.onOutputSizeChanged(mTargetInputFilterOutputWidth, mTargetInputFilterOutputHeight);
        }
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
    
    private static int[] mFrameBuffers = null;
    private static int[] mFrameBufferTextures = null;
    private int mFrameBufferWidth = -1;
    private int mFrameBufferHeight = -1;
	private void initFrameBufferObject(int frameBufferWidth, int frameBufferHeight) {
		if(mFrameBuffers != null && (mFrameBufferWidth != frameBufferWidth || mFrameBufferHeight != frameBufferHeight))
			destroyFrameBufferObject();
        if (mFrameBuffers == null) {
        	mFrameBufferWidth = frameBufferWidth;
        	mFrameBufferHeight = frameBufferHeight;
        	mFrameBuffers = new int[1];
            mFrameBufferTextures = new int[1];

            GLES20.glGenFramebuffers(1, mFrameBuffers, 0);
            
            GLES20.glGenTextures(1, mFrameBufferTextures, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mFrameBufferTextures[0]);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, frameBufferWidth, frameBufferHeight, 0,
                    GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
            GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                    GLES20.GL_TEXTURE_2D, mFrameBufferTextures[0], 0);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
        }
	}
	
	private void destroyFrameBufferObject() {
        if (mFrameBufferTextures != null) {
            GLES20.glDeleteTextures(1, mFrameBufferTextures, 0);
            mFrameBufferTextures = null;
        }
        if (mFrameBuffers != null) {
            GLES20.glDeleteFramebuffers(1, mFrameBuffers, 0);
            mFrameBuffers = null;
        }
        mFrameBufferWidth = -1;
        mFrameBufferHeight = -1;
    }
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
    private boolean isRendering = false;
    
	private Surface mInputSurface = null;
	private SurfaceTexture mInputSurfaceTexture = null;
	private int mInputTextureId = OpenGLUtils.NO_TEXTURE;
	private int mVideoWidth = 0, mVideoHeight = 0;
	private int mTargetVideoWidth = 0, mTargetVideoHeight = 0;
	private int mSurfaceWidth = 0, mSurfaceHeight = 0;
	private VideoOptions mVideoOptions = null;
	private AudioOptions mAudioOptions = null;
	private String mPublishUrl = null;
	private MediaProjection mMediaProjection = null;
	private int mVirtualDisplayDpi = 0;
	private ScreenStreamerListener mScreenStreamerListener = null;
	public void startRender(MediaProjection mediaProjection, PublishStreamOptions options, int dpi, ScreenStreamerListener screenStreamerListener)
	{
		if(isRendering) return;
		
		mMediaProjection = mediaProjection;
		
		mVideoOptions = new VideoOptions();
		mAudioOptions = new AudioOptions();
		
		mVideoOptions.hasVideo = true;
		
		if(mVideoOptions.hasVideo)
		{
			mVideoOptions.videoEncodeType = MediaStreamer.VIDEO_HARD_ENCODE;

			mVideoOptions.videoWidth = options.videoWidth;
			mVideoOptions.videoHeight = options.videoHeight;

			mVideoOptions.videoFps = options.videoFps;
			mVideoOptions.videoProfile = 0; //0:base_line 1:main_profile 2:high_profile
			mVideoOptions.videoBitRate = options.bitRate-CameraTextureView.DEFAULT_AUDIO_BITRATE;
			
			mVideoOptions.videoRawType = MediaStreamer.VIDEOFRAME_RAWTYPE_NV21;
			
			mVideoOptions.encodeMode = 1; //0:VBR or 1:CBR
			mVideoOptions.quality = 0;
			mVideoOptions.maxKeyFrameIntervalMs = 5000;
			mVideoOptions.bStrictCBR = true;
			mVideoOptions.deblockingFilterFactor = 0;
		}

		mAudioOptions.hasAudio = options.hasAudio;
		
		if(mAudioOptions.hasAudio)
		{
			mAudioOptions.audioSampleRate = 16000;
			mAudioOptions.audioNumChannels = 1;
			mAudioOptions.audioBitRate = 24;
		}
		
		mPublishUrl = options.publishUrl;
		
		mVideoWidth = mVideoOptions.videoWidth;
		mVideoHeight = mVideoOptions.videoHeight;
		mTargetVideoWidth = mVideoOptions.videoWidth;
		mTargetVideoHeight = mVideoOptions.videoHeight;

		mSurfaceWidth = mVideoOptions.videoWidth;
		mSurfaceHeight = mVideoOptions.videoHeight;
		
		mVirtualDisplayDpi = dpi;
		
		mScreenStreamerListener = screenStreamerListener;
		
		initGPUImageParam();
		createRenderThread();
		
		isRendering = true;
	}
	
	private Lock mLock = null;
	private Condition mCondition = null;
	private Thread mRenderThread = null;
	private boolean isBreakRenderThread = false;
	private void createRenderThread()
	{
		mLock = new ReentrantLock(); 
		mCondition = mLock.newCondition();
		
		isBreakRenderThread = false;
		mRenderThread = new Thread(mRenderRunnable);
		mRenderThread.start();
	}
	
	private EGL mEGL = null;
	private SLKMediaMuxer mSLKMediaMuxer = null;
	private AudioEncoderCore mAudioEncoderCore = null;
	private EncodeAudioCore mEncodeAudioCore = null;
    private VideoEncoderCore mVideoEncoderCore = null;
    private Surface mEncodeSurface = null;
    private long mBaseLinePresentationTimestampNs = 0;
    private boolean mGotBaseLinePresentationTimestamp = false;
	private Runnable mRenderRunnable = new Runnable() {
		@Override
		public void run() {
			// Start MediaMuxer
			int type = -1;
			int format = -1;
			int track = -1;
			if(mPublishUrl.startsWith("/"))
			{
				type = SLKMediaMuxer.SYSTEM_MEDIAMUXER_TYPE;
				format = OutputFormat.MUXER_OUTPUT_MPEG_4;
			}else{
				type = SLKMediaMuxer.FFMPEG_MEDIAMUXER_TYPE;
				format = OutputFormat.MUXER_OUTPUT_FLV;
			}
			
			if(mAudioOptions.hasAudio)
			{
				track = SLKMediaMuxer.AUDIO_VIDEO_TRACK;
			}else
			{
				track = SLKMediaMuxer.ONLY_VIDEO_TRACK;
			}
			
			mSLKMediaMuxer = new SLKMediaMuxer(type, mPublishUrl, format, track);
			mSLKMediaMuxer.setMediaMuxerListener(mMediaMuxerListener);
			
			if(mAudioOptions.hasAudio)
			{
				// Start Audio Encoder and Capturer
				mAudioEncoderCore = new AudioEncoderCore(mAudioOptions, mSLKMediaMuxer);
				mEncodeAudioCore = new EncodeAudioCore(mAudioEncoderCore);
				mEncodeAudioCore.initRecording(mAudioOptions.audioSampleRate, mAudioOptions.audioNumChannels);
				mEncodeAudioCore.startRecording();
			}
			
			// Start VideoEncoder
			mVideoEncoderCore = new VideoEncoderCore(mVideoOptions, mSLKMediaMuxer);
	    	mEncodeSurface = mVideoEncoderCore.getInputSurface();
	    	
	    	//Create EGL
			if(mEGL==null)
			{
				mEGL = new EGL();
				mEGL.Egl_Initialize(null, true);
				
		        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
			}
			mEGL.Egl_AttachToSurface(mEncodeSurface);
			mGotBaseLinePresentationTimestamp = false;
			
	    	mTargetVideoBitrate = mVideoOptions.videoBitRate;
	    	mTargetFps = mVideoOptions.videoFps;
			
	        long videoEventTimer_StartTime = 0;
	        long videoEventTimer_EndTime = 0;
	        long videoEventTime = 0;
	        long videoDelayTime = 0;
			while(true)
			{
				mLock.lock();
				if(isBreakRenderThread)
				{
					mLock.unlock();
					break;
				}
				
				videoEventTimer_StartTime = System.nanoTime();
				
				controlVideoBitrate(mVideoOptions, mVideoEncoderCore, mSLKMediaMuxer);
				
				if(!isGPUImageWorkerOpened)
				{
					openGPUImageWorker();
					createVirtualDisplay();
					isGPUImageWorkerOpened = true;
				}
				
				if(isSwitchFilter)
				{
					isSwitchFilter = false;
					switchFilter(mFilterType, mFilterDir);
				}
				
				mVideoEncoderCore.drainEncoder(false);
				boolean ret = onDrawFrame();
				if(ret) {
					if(!mGotBaseLinePresentationTimestamp)
					{
						mBaseLinePresentationTimestampNs = System.nanoTime();//mInputSurfaceTexture.getTimestamp();
						mGotBaseLinePresentationTimestamp = true;
					}
					mEGL.Egl_SetPresentationTime(/*mInputSurfaceTexture.getTimestamp()*/System.nanoTime()-mBaseLinePresentationTimestampNs);
				}
				mEGL.Egl_SwapBuffers();
				
				videoEventTimer_EndTime = System.nanoTime();
			    videoEventTime = (videoEventTimer_EndTime-videoEventTimer_StartTime)/1000000;

			    long delayMs = 1000/mTargetFps-videoEventTime-videoDelayTime;
		        if (delayMs>=0) {
		        	videoDelayTime = 0;
		        }else {
		        	videoDelayTime = -delayMs;
		        }
		        
		        Log.i(TAG, "delayMs : " + String.valueOf(delayMs));
		        
				try {
					mCondition.await(delayMs, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
								
				mLock.unlock();
				continue;
			}
						
			if(isGPUImageWorkerOpened)
			{
				releaseVirtualDisplay();
				closeGPUImageWorker();
				isGPUImageWorkerOpened = false;
			}
			
			// Release EGL
			mEGL.Egl_DetachFromSurfaceTexture();
			mEGL.Egl_Terminate();
			mEGL = null;
			
			// Release VideoEncoder
			mVideoEncoderCore.release();
			mVideoEncoderCore = null;
			
			//Release Audio Capturer and Encoder
			if(mEncodeAudioCore!=null)
			{
				mEncodeAudioCore.stopRecording();
				mEncodeAudioCore.terminateRecording();
				mEncodeAudioCore = null;
			}
			if(mAudioEncoderCore!=null)
			{
				mAudioEncoderCore.release();
				mAudioEncoderCore = null;
			}
			
			// Release MediaMuxer
			mSLKMediaMuxer.stop();
			mSLKMediaMuxer.release();
			mSLKMediaMuxer = null;
			
		    mBaseLinePresentationTimestampNs = 0;
		    mGotBaseLinePresentationTimestamp = false;
		}
	};

	private void deleteRenderThread()
	{
		mLock.lock();
		isBreakRenderThread = true;
		mCondition.signal();
		mLock.unlock();
		
		try {
			mRenderThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private int mVideoScalingModeForWorkFilter = GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT;
	public void requestScalingMode (int mode)
	{
		if(isRendering)
		{
			mLock.lock();
			mVideoScalingModeForWorkFilter = mode;
			mLock.unlock();	
		}
	}
	
	private GPUImageRotationMode mRotationModeForTargetInputFilter = GPUImageRotationMode.kGPUImageRotateRightFlipVertical;
    private GPUImageRotationMode mRotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation; //GPUImageRotationMode.kGPUImageFlipVertical;
	public void requestGPUImageRotationMode(GPUImageRotationMode rotationModeForTargetInputFilter, GPUImageRotationMode rotationModeForWorkFilter)
	{
		if(isRendering)
		{
			mLock.lock();
			mRotationModeForTargetInputFilter = rotationModeForTargetInputFilter;
			mRotationModeForWorkFilter = rotationModeForWorkFilter;
			mLock.unlock();
		}
	}
	
	private int mFilterType = GPUImageUtils.FILTER_RGB;
	private String mFilterDir = null;
	private boolean isSwitchFilter = false;
	public void requestFilter(int type, String filterDir)
	{
		if(isRendering)
		{
			mLock.lock();
			mFilterType = type;
			mFilterDir = new String(filterDir);
			isSwitchFilter = true;
			mLock.unlock();
		}
	}
	
	public void requestTargetVideoSize(int targetVideoWidth, int targetVideoHeight)
	{
		if(isRendering)
		{
			mLock.lock();
			mTargetVideoWidth = targetVideoWidth;
			mTargetVideoHeight = targetVideoHeight;
			mLock.unlock();
		}
	}
	
	private void stopRender_l()
	{
		if(isRendering)
		{
			deleteRenderThread();
			isRendering = false;
		}
	}
	public void stopRender()
	{
		stopRender_l();
		mMainHander.removeCallbacksAndMessages(null);
	}
	
	public void Finalize() {
		stopRender();
	}
	
	private int mTargetVideoBitrate = 0;
	private int mTargetFps = 0;
	private void controlVideoBitrate(VideoOptions videoOptions, VideoEncoderCore videoEncoderCore, SLKMediaMuxer slkMediaMuxer)
	{		
		if(videoOptions.isEnableNetworkAdaptive)
		{
			long publishDelayTimeMs = slkMediaMuxer.getPublishDelayTimeMs();
			if(publishDelayTimeMs>=0)
			{
	            int oldTargetVideoBitrate = mTargetVideoBitrate;

	            if (publishDelayTimeMs>=200 && publishDelayTimeMs<1000) {
	                return;
	            }
	            
	            if (publishDelayTimeMs>=1000) {
	                mTargetVideoBitrate -= publishDelayTimeMs / 10;
	            }
	            
	            if (publishDelayTimeMs<200) {
	                mTargetVideoBitrate += 100;//+100k
	            }
	            
	            if (mTargetVideoBitrate>=videoOptions.videoBitRate) {
	                mTargetVideoBitrate = videoOptions.videoBitRate;
	            }
	            
	            if (mTargetVideoBitrate<=videoOptions.networkAdaptiveMinVideoBitrate) {
	                mTargetVideoBitrate = videoOptions.networkAdaptiveMinVideoBitrate;
	            }
	            
	            int upOrDownBitrate = mTargetVideoBitrate - oldTargetVideoBitrate;
	            if (upOrDownBitrate>=100 || upOrDownBitrate<=-100)
	            {
	            	videoEncoderCore.adjustVideoBitrate(mTargetVideoBitrate);
	            }
	            
	            if (upOrDownBitrate>=100)
	            {
	                int upFrames = upOrDownBitrate / 100;
	                mTargetFps += upFrames;
	            }
	            
	            if (upOrDownBitrate<=-100)
	            {
	                int downFrames = upOrDownBitrate / -100;
	                mTargetFps -= downFrames;
	            }
	            
	            if (mTargetFps>=videoOptions.videoFps) {
	                mTargetFps = videoOptions.videoFps;
	            }
	            
	            if (mTargetFps<=videoOptions.networkAdaptiveMinFps) {
	                mTargetFps = videoOptions.networkAdaptiveMinFps;
	            }
	            
	            if(publishDelayTimeMs>=30000)
	            {
	            	if(mScreenStreamerListener!=null)
	            	{
	            		mScreenStreamerListener.onScreenStreamerError(MediaStreamer.CALLBACK_MEDIA_STREAMER_ERROR_POOR_NETWORK);
	            	}
	            }
			}
		}
	}
	
	private MediaMuxerListener mMediaMuxerListener = new MediaMuxerListener()
	{
		@Override
		public void onMediaMuxerConnecting() {
			if(mScreenStreamerListener!=null)
			{
				mScreenStreamerListener.onScreenStreamerConnecting();
			}
		}

		@Override
		public void onMediaMuxerConnected() {
			if(mScreenStreamerListener!=null)
			{
				mScreenStreamerListener.onScreenStreamerConnected();
			}
		}

		@Override
		public void onMediaMuxerStreaming() {
			if(mScreenStreamerListener!=null)
			{
				mScreenStreamerListener.onScreenStreamerStreaming();
			}
		}

		@Override
		public void onMediaMuxerError(int errorType) {
			mMainHander.post(new MediaMuxerErrorRunnable(errorType));
		}

		@Override
		public void onMediaMuxerInfo(int infoType, int infoValue) {
			if(mScreenStreamerListener!=null)
			{
				if(infoType == SLKMediaMuxer.CALLBACK_MEDIA_MUXER_INFO_PUBLISH_DELAY_TIME)
				{
					mScreenStreamerListener.onScreenStreamerInfo(MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_DELAY_TIME, infoValue);
				}else if(infoType == SLKMediaMuxer.CALLBACK_MEDIA_MUXER_INFO_PUBLISH_REAL_BITRATE)
				{
					mScreenStreamerListener.onScreenStreamerInfo(MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_REAL_BITRATE, infoValue);
				}else if(infoType == SLKMediaMuxer.CALLBACK_MEDIA_MUXER_INFO_PUBLISH_REAL_FPS)
				{
					mScreenStreamerListener.onScreenStreamerInfo(MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS, infoValue);
				}else if(infoType == SLKMediaMuxer.CALLBACK_MEDIA_MUXER_INFO_PUBLISH_TIME)
				{
					mScreenStreamerListener.onScreenStreamerInfo(MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_TIME, infoValue);
				}
			}
		}

		@Override
		public void onMediaMuxerEnd() {
			if(mScreenStreamerListener!=null)
			{
				mScreenStreamerListener.onScreenStreamerEnd();
			}
		}
	};
	
	class MediaMuxerErrorRunnable implements Runnable {

		private int mErrorType = 0;
		public MediaMuxerErrorRunnable(int errorType)
		{
			mErrorType = errorType;
		}
		
		@Override
		public void run() {
			stopRender_l();
			
			if(mScreenStreamerListener!=null)
			{
				if(mErrorType == SLKMediaMuxer.CALLBACK_MEDIA_MUXER_ERROR_UNKNOWN)
				{
					mScreenStreamerListener.onScreenStreamerError(MediaStreamer.CALLBACK_MEDIA_STREAMER_ERROR_UNKNOWN);
				}else if(mErrorType == SLKMediaMuxer.CALLBACK_MEDIA_MUXER_ERROR_CONNECT_FAIL)
				{
					mScreenStreamerListener.onScreenStreamerError(MediaStreamer.CALLBACK_MEDIA_STREAMER_ERROR_CONNECT_FAIL);
				}else if(mErrorType == SLKMediaMuxer.CALLBACK_MEDIA_MUXER_ERROR_MUX_FAIL)
				{
					mScreenStreamerListener.onScreenStreamerError(MediaStreamer.CALLBACK_MEDIA_STREAMER_ERROR_MUX_FAIL);
				}else if(mErrorType == SLKMediaMuxer.CALLBACK_MEDIA_MUXER_ERROR_POOR_NETWORK)
				{
					mScreenStreamerListener.onScreenStreamerError(MediaStreamer.CALLBACK_MEDIA_STREAMER_ERROR_POOR_NETWORK);
				}
			}
			
			mMainHander.removeCallbacksAndMessages(null);
		}
	}
}