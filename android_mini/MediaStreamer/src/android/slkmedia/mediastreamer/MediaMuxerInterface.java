package android.slkmedia.mediastreamer;

import java.nio.ByteBuffer;

import android.media.MediaCodec;
import android.media.MediaFormat;

public interface MediaMuxerInterface {
	public abstract void setMediaMuxerListener(MediaMuxerListener listener);
	public abstract int  addTrack(MediaFormat format);
	public abstract void start();
	public abstract void stop();
	public abstract void release();
	public abstract void writeSampleData(int trackIndex, ByteBuffer byteBuf, MediaCodec.BufferInfo bufferInfo);
	public abstract long getPublishDelayTimeMs();
}
