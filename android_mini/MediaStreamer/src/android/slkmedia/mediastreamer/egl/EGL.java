package android.slkmedia.mediastreamer.egl;

import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.opengl.EGLConfig;

import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.opengl.EGLExt;
import android.util.Log;
import android.view.Surface;

public class EGL {
	private static final String TAG = "EGL";

    // Android-specific extension.
	// https://www.khronos.org/registry/EGL/extensions/ANDROID/EGL_ANDROID_recordable.txt
    private static final int EGL_RECORDABLE_ANDROID = 0x3142;
	
    private EGLContext eglContext = EGL14.EGL_NO_CONTEXT;
    private EGLDisplay eglDisplay = EGL14.EGL_NO_DISPLAY;
    private EGLSurface eglSurface = EGL14.EGL_NO_SURFACE;
    private EGLConfig eglConfig = null;
	
    public void Egl_Initialize(SurfaceTexture surfaceTexture)
    {
    	Egl_Initialize();
    	Egl_AttachToSurfaceTexture(surfaceTexture);
    }
    
	private boolean isInitialized = false;
	public void Egl_Initialize()
	{
		if(isInitialized) return;
		
        eglDisplay = EGL14.eglGetDisplay(EGL14.EGL_DEFAULT_DISPLAY);
        int version[] = new int[2];
        EGL14.eglInitialize(eglDisplay, version, 0, version, 1);
        
        eglConfig = chooseEglConfig(false);
        
        int[] attrs = {
                EGL14.EGL_CONTEXT_CLIENT_VERSION, 2,
                EGL14.EGL_NONE
        };
        eglContext = EGL14.eglCreateContext(eglDisplay, eglConfig, EGL14.EGL_NO_CONTEXT, attrs, 0);
        
        isInitialized = true;
        
        Log.d(TAG, "Egl_Initialize");
	}
	
	public EGLContext getEGLContext()
	{
		return eglContext;
	}
	
	public void Egl_Initialize(EGLContext sharedContext, boolean isRecordable)
	{
		if(isInitialized) return;
		
        if (sharedContext == null) {
            sharedContext = EGL14.EGL_NO_CONTEXT;
        }
		
        eglDisplay = EGL14.eglGetDisplay(EGL14.EGL_DEFAULT_DISPLAY);
        int version[] = new int[2];
        EGL14.eglInitialize(eglDisplay, version, 0, version, 1);
        
        eglConfig = chooseEglConfig(isRecordable);
        
        int[] attrs = {
                EGL14.EGL_CONTEXT_CLIENT_VERSION, 2,
                EGL14.EGL_NONE
        };
        eglContext = EGL14.eglCreateContext(eglDisplay, eglConfig, sharedContext, attrs, 0);
        
        isInitialized = true;
        
        Log.d(TAG, "Egl_Initialize");
	}
	
	private boolean isAttached = false;
	public void Egl_AttachToSurfaceTexture(SurfaceTexture surfaceTexture)
	{
		if(!isInitialized) return;
		
		if(isAttached) return;
		
        int[] surfaceAttribs = {
                EGL14.EGL_NONE
        };
        eglSurface = EGL14.eglCreateWindowSurface(eglDisplay, eglConfig, surfaceTexture, surfaceAttribs, 0);
        
        try {
            if (eglSurface == null || eglSurface == EGL14.EGL_NO_SURFACE) {
                throw new RuntimeException("GL error:" + getEGLErrorString(EGL14.eglGetError()));
            }
            if (!EGL14.eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext)) {
                throw new RuntimeException("GL Make current Error"+ getEGLErrorString(EGL14.eglGetError()));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        
        isAttached = true;
        
        Log.d(TAG, "Egl_AttachToSurfaceTexture");
	}
	
	public void Egl_AttachToSurface(Surface surface)
	{
		if(!isInitialized) return;
		
		if(isAttached) return;
		
        int[] surfaceAttribs = {
                EGL14.EGL_NONE
        };
        
        eglSurface = EGL14.eglCreateWindowSurface(eglDisplay, eglConfig, surface, surfaceAttribs, 0);
        
        try {
            if (eglSurface == null || eglSurface == EGL14.EGL_NO_SURFACE) {
                throw new RuntimeException("GL error:" + getEGLErrorString(EGL14.eglGetError()));
            }
            if (!EGL14.eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext)) {
                throw new RuntimeException("GL Make current Error"+ getEGLErrorString(EGL14.eglGetError()));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        
        isAttached = true;
        
        Log.d(TAG, "Egl_AttachToSurface");
	}
	
	public void Egl_AttachToOffScreenSurface(int width, int height)
	{
		if(!isInitialized) return;
		
		if(isAttached) return;
		
        int[] surfaceAttribs = {
                EGL14.EGL_WIDTH, width,
                EGL14.EGL_HEIGHT, height,
                EGL14.EGL_NONE
        };
        
        eglSurface = EGL14.eglCreatePbufferSurface(eglDisplay, eglConfig, surfaceAttribs, 0);
        
        try {
            if (eglSurface == null || eglSurface == EGL14.EGL_NO_SURFACE) {
                throw new RuntimeException("GL error:" + getEGLErrorString(EGL14.eglGetError()));
            }
            if (!EGL14.eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext)) {
                throw new RuntimeException("GL Make current Error"+ getEGLErrorString(EGL14.eglGetError()));
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        
        isAttached = true;
        
        Log.d(TAG, "Egl_AttachToOffScreenSurface");
	}
	
	public void Egl_DetachFromSurfaceTexture()
	{
		if(!isInitialized) return;
		
		if(!isAttached) return;
		
//        egl.eglMakeCurrent(eglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
		EGL14.eglDestroySurface(eglDisplay, eglSurface);
        eglSurface = EGL14.EGL_NO_SURFACE;
        
        isAttached = false;
        
        Log.d(TAG, "Egl_DetachFromSurfaceTexture");
	}
	
	public void Egl_SetPresentationTime(long time)
	{
        EGLExt.eglPresentationTimeANDROID(eglDisplay, eglSurface, time);
	}
	
	public void Egl_SwapBuffers()
	{
		if(isInitialized && isAttached)
		{
			EGL14.eglSwapBuffers(eglDisplay, eglSurface);
		}
	}
	
	public void Egl_Terminate()
	{
		if(!isInitialized) return;
		
		EGL14.eglMakeCurrent(eglDisplay, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT);
		
		Egl_DetachFromSurfaceTexture();
        
		EGL14.eglDestroyContext(eglDisplay, eglContext);
		EGL14.eglTerminate(eglDisplay);
        
		eglContext = EGL14.EGL_NO_CONTEXT;
		eglDisplay = EGL14.EGL_NO_DISPLAY;
        eglConfig = null;
		
        isInitialized = false;
        
        Log.d(TAG, "Egl_Terminate");
	}
	
    private EGLConfig chooseEglConfig(boolean isRecordable) {
        int[] configsCount = new int[1];
        EGLConfig[] configs = new EGLConfig[1];
        int[] attributes = getAttributes(isRecordable);
        int confSize = 1;

        if (!EGL14.eglChooseConfig(eglDisplay, attributes, 0, configs, 0, confSize, configsCount, 0)) {
            throw new IllegalArgumentException("Failed to choose config:"+ getEGLErrorString(EGL14.eglGetError()));
        }else if (configsCount[0] > 0) {
            return configs[0];
        }

        return null;
    }
    
    private int[] getAttributes(boolean isRecordable)
    {
    	if(isRecordable)
    	{
            return new int[] {
            		EGL14.EGL_RENDERABLE_TYPE, EGL14.EGL_OPENGL_ES2_BIT,
            		EGL14.EGL_RED_SIZE, 8,
            		EGL14.EGL_GREEN_SIZE, 8,
            		EGL14.EGL_BLUE_SIZE, 8,
            		EGL14.EGL_ALPHA_SIZE, 8,
            		EGL14.EGL_DEPTH_SIZE, 0,
            		EGL14.EGL_STENCIL_SIZE, 0,
            		EGL_RECORDABLE_ANDROID, 1,
            		EGL14.EGL_NONE
            };
    	}else{
            return new int[] {
            		EGL14.EGL_RENDERABLE_TYPE, EGL14.EGL_OPENGL_ES2_BIT,
            		EGL14.EGL_RED_SIZE, 8,
            		EGL14.EGL_GREEN_SIZE, 8,
            		EGL14.EGL_BLUE_SIZE, 8,
            		EGL14.EGL_ALPHA_SIZE, 8,
            		EGL14.EGL_DEPTH_SIZE, 0,
            		EGL14.EGL_STENCIL_SIZE, 0,
            		EGL14.EGL_NONE
            };
    	}
    }
    
    private String getEGLErrorString(int error) {
        switch (error) {
            case EGL14.EGL_SUCCESS:
                return "EGL_SUCCESS";
            case EGL14.EGL_NOT_INITIALIZED:
                return "EGL_NOT_INITIALIZED";
            case EGL14.EGL_BAD_ACCESS:
                return "EGL_BAD_ACCESS";
            case EGL14.EGL_BAD_ALLOC:
                return "EGL_BAD_ALLOC";
            case EGL14.EGL_BAD_ATTRIBUTE:
                return "EGL_BAD_ATTRIBUTE";
            case EGL14.EGL_BAD_CONFIG:
                return "EGL_BAD_CONFIG";
            case EGL14.EGL_BAD_CONTEXT:
                return "EGL_BAD_CONTEXT";
            case EGL14.EGL_BAD_CURRENT_SURFACE:
                return "EGL_BAD_CURRENT_SURFACE";
            case EGL14.EGL_BAD_DISPLAY:
                return "EGL_BAD_DISPLAY";
            case EGL14.EGL_BAD_MATCH:
                return "EGL_BAD_MATCH";
            case EGL14.EGL_BAD_NATIVE_PIXMAP:
                return "EGL_BAD_NATIVE_PIXMAP";
            case EGL14.EGL_BAD_NATIVE_WINDOW:
                return "EGL_BAD_NATIVE_WINDOW";
            case EGL14.EGL_BAD_PARAMETER:
                return "EGL_BAD_PARAMETER";
            case EGL14.EGL_BAD_SURFACE:
                return "EGL_BAD_SURFACE";
            case EGL14.EGL_CONTEXT_LOST:
                return "EGL_CONTEXT_LOST";
            default:
                return "0x" + Integer.toHexString(error);
        }
    }
}
