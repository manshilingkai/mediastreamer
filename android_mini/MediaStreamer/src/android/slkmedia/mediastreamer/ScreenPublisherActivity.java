package android.slkmedia.mediastreamer;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;

public class ScreenPublisherActivity extends Activity implements ScreenStreamerListener{
	private static final String TAG = "ScreenPublisherActivity";

	public static final String PUBLISH_URL_KEY = "PublishUrl";

	private static final int SCREEN_STREAMER_REQUEST_CODE  = 101;

	private MediaProjectionManager mProjectionManager = null;
	private MediaProjection mMediaProjection = null;

	private FrameLayout mFrameLayout = null;
	private Button mStartOrStopButton = null;
	
	private Handler mMainHander = null;
	
	private PublishStreamOptions mPublishStreamOptions = new PublishStreamOptions();
	private int mVirtualDisplayDpi = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        mProjectionManager = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        mFrameLayout = new FrameLayout(this);
        setContentView(mFrameLayout);
        
        Intent intent = getIntent();
        mPublishStreamOptions.publishUrl = intent.getStringExtra(PUBLISH_URL_KEY);
        
        mStartOrStopButton = new Button(this);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,FrameLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;
        mStartOrStopButton.setLayoutParams(layoutParams);
        mStartOrStopButton.setText("Start");
        mFrameLayout.addView(mStartOrStopButton);
        
        mStartOrStopButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if(isRunning())
				{
					stopStreaming();
					mMainHander.removeCallbacksAndMessages(null);
					
					mStartOrStopButton.setText("Start");
				}else{
					Intent screenCaptureIntent = mProjectionManager.createScreenCaptureIntent();
					startActivityForResult(screenCaptureIntent, SCREEN_STREAMER_REQUEST_CODE);
				}
			}
		});
        
        mMainHander = new Handler();
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	if (requestCode == SCREEN_STREAMER_REQUEST_CODE && resultCode == RESULT_OK) {
		mMediaProjection = mProjectionManager.getMediaProjection(resultCode, data);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		makePublishStreamOptions(mPublishStreamOptions, metrics.widthPixels, metrics.heightPixels); 
		mVirtualDisplayDpi = metrics.densityDpi;
		
		startStreaming();
		mStartOrStopButton.setText("Stop");
      }
    }
    
    @Override
    protected void onDestroy() {
      super.onDestroy();
      
      stopStreaming();
    }

	private boolean running = false;
	public boolean isRunning() { return running; }
	
	private ScreenStreamer mScreenStreamer = null;
	public boolean startStreaming()
	{
		if (running) {
			return false;
		}
		
		mScreenStreamer = new ScreenStreamer();
		mScreenStreamer.startRender(mMediaProjection, mPublishStreamOptions, mVirtualDisplayDpi, this);
		
	    running = true;
	    return true;
	}
	
	public boolean stopStreaming()
	{
		if (!running) {
			return false;
		}
		
		if(mScreenStreamer!=null)
		{
			mScreenStreamer.stopRender();
			mScreenStreamer.Finalize();
			mScreenStreamer = null;
		}
		
		running = false;
		return true;
	}
	
 	public static final int MAX_VIDEO_SIDE_SIZE = 1280;
 	private void makePublishStreamOptions(PublishStreamOptions publishStreamOptions, int displayWidth, int displayHeight)
 	{
		int publishVideoWidth = 0;
		int publishVideoHeight = 0;
		if(displayWidth >= displayHeight)
		{
			publishVideoWidth = MAX_VIDEO_SIDE_SIZE;
			publishVideoHeight = publishVideoWidth * displayHeight / displayWidth;
			
			if(publishVideoHeight%2!=0)
			{
				publishVideoHeight = publishVideoHeight + 1;
			}
		}else {
			publishVideoHeight = MAX_VIDEO_SIDE_SIZE;
			publishVideoWidth = displayWidth * publishVideoHeight / displayHeight;
			
			if(publishVideoWidth%2!=0)
			{
				publishVideoWidth = publishVideoWidth + 1;
			}
		}
		
		publishStreamOptions.videoWidth = publishVideoWidth;
		publishStreamOptions.videoHeight = publishVideoHeight;
 	}

	@Override
	public void onScreenStreamerConnecting() {
		Log.d(TAG, "ScreenPublisherActivity - ScreenStreamerConnecting");		
	}

	@Override
	public void onScreenStreamerConnected() {
		Log.d(TAG, "ScreenPublisherActivity - ScreenStreamerConnected");
	}

	@Override
	public void onScreenStreamerStreaming() {
		Log.d(TAG, "ScreenPublisherActivity - ScreenStreamerStreaming");		
	}

	@Override
	public void onScreenStreamerError(int errorType) {
		Log.d(TAG, "ScreenPublisherActivity - ScreenStreamerError ErrorType :" + String.valueOf(errorType));

		mMainHander.post(new Runnable(){
			@Override
			public void run() {
				mStartOrStopButton.setText("Start");
			}
		});
	}

	@Override
	public void onScreenStreamerInfo(int infoType, int infoValue) {
		Log.d(TAG, "ScreenPublisherActivity - ScreenStreamerInfo InfoType : " + String.valueOf(infoType) + " InfoValue : " + String.valueOf(infoValue));
	}

	@Override
	public void onScreenStreamerEnd() {
		Log.d(TAG, "ScreenPublisherActivity - ScreenStreamerEnd");							
	}
}
