package android.slkmedia.mediastreamer;

public interface ScreenStreamerListener {
	public abstract void onScreenStreamerConnecting();
	public abstract void onScreenStreamerConnected();
	public abstract void onScreenStreamerStreaming();
	public abstract void onScreenStreamerError(int errorType);
	public abstract void onScreenStreamerInfo(int infoType, int infoValue);
	public abstract void onScreenStreamerEnd();
}
