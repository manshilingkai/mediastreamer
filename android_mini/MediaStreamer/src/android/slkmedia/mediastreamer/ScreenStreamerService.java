package android.slkmedia.mediastreamer;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.projection.MediaProjection;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

public class ScreenStreamerService extends Service implements ScreenStreamerListener{
	private static final String TAG = "ScreenStreamerService";

	private Handler mMainHander = null;

	@Override
	public IBinder onBind(Intent intent) {
		return new ScreenStreamerBinder();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Notification.Builder builder = new Notification.Builder(this.getApplicationContext());
		Intent nfIntent = new Intent(this, ScreenStreamerActivity.class);
		builder.setContentIntent(PendingIntent.getActivity(this, 0, nfIntent, 0)).setContentTitle("Service").setContentText("ScreenStreamerService").setWhen(System.currentTimeMillis());
		Notification notification = builder.build();
		notification.defaults = Notification.DEFAULT_SOUND;
		
		startForeground(110, notification);
		
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onCreate() {
		super.onCreate();
        mMainHander = new Handler();
	}
	
	@Override
	public void onDestroy() {
		stopForeground(true);
		super.onDestroy();
	}
	
	private MediaProjection mMediaProjection = null;
	public void setMediaProjection(MediaProjection projection) {
		mMediaProjection = projection;
	}
	
	private PublishStreamOptions mPublishStreamOptions = null;
	private int mVirtualDisplayDpi = 0;
	public void setPublishStreamOptions(PublishStreamOptions options, int dpi) {
		mPublishStreamOptions = options;
		mVirtualDisplayDpi = dpi;
	}
	
	private ScreenStreamerServiceListener mScreenStreamerServiceListener = null;
	public void setScreenStreamerServiceListener(ScreenStreamerServiceListener listener)
	{
		mScreenStreamerServiceListener = listener;
	}
	
	private boolean running = false;
	public boolean isRunning() { return running; }
	
	private ScreenStreamer mScreenStreamer = null;
	public boolean startStreaming()
	{
		if (running) {
			return false;
		}
		
		mScreenStreamer = new ScreenStreamer();
		mScreenStreamer.startRender(mMediaProjection, mPublishStreamOptions, mVirtualDisplayDpi, this);
		
	    running = true;
	    return true;
	}
	
	private boolean stopStreaming_l()
	{
		if (!running) {
			return false;
		}
		
		if(mScreenStreamer!=null)
		{
			mScreenStreamer.stopRender();
			mScreenStreamer.Finalize();
			mScreenStreamer = null;
		}
		
		running = false;
		
		return true;
	}
	
	public boolean stopStreaming()
	{
		boolean ret = stopStreaming_l();
		
		mMainHander.removeCallbacksAndMessages(null);
		
		return ret;
	}
	
	public class ScreenStreamerBinder extends Binder{
		public ScreenStreamerService getScreenStreamerService()
		{
			return ScreenStreamerService.this;
		}
	}

	@Override
	public void onScreenStreamerConnecting() {
		if(mScreenStreamerServiceListener!=null)
		{
			mScreenStreamerServiceListener.onScreenStreamerConnecting();
		}
	}

	@Override
	public void onScreenStreamerConnected() {
		if(mScreenStreamerServiceListener!=null)
		{
			mScreenStreamerServiceListener.onScreenStreamerConnected();
		}
	}

	@Override
	public void onScreenStreamerStreaming() {
		if(mScreenStreamerServiceListener!=null)
		{
			mScreenStreamerServiceListener.onScreenStreamerStreaming();
		}
	}

	@Override
	public void onScreenStreamerError(int errorType) {
		mMainHander.post(new ScreenStreamerErrorRunnable(errorType));
	}

	@Override
	public void onScreenStreamerInfo(int infoType, int infoValue) {
		if(mScreenStreamerServiceListener!=null) {
			mScreenStreamerServiceListener.onScreenStreamerInfo(infoType, infoValue);
		}
	}

	@Override
	public void onScreenStreamerEnd() {
		if(mScreenStreamerServiceListener!=null)
		{
			mScreenStreamerServiceListener.onScreenStreamerEnd();
		}
	}
	
	class ScreenStreamerErrorRunnable implements Runnable
	{
		private int mErrorType = 0;
		public ScreenStreamerErrorRunnable(int errorType)
		{
			mErrorType = errorType;
		}
		
		@Override
		public void run() {
			stopStreaming_l();
			
			if(mScreenStreamerServiceListener!=null)
			{
				mScreenStreamerServiceListener.onScreenStreamerError(mErrorType);
			}
			
			mMainHander.removeCallbacksAndMessages(null);
		}
	}
}
