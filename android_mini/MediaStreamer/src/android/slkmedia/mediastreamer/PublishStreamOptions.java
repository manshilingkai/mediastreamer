package android.slkmedia.mediastreamer;

public class PublishStreamOptions {
	public boolean hasVideo = true;
	public boolean hasAudio = true;
	public boolean enableMediaCodecEncoder = false;
	public int videoWidth = 0;
	public int videoHeight = 0;
	public int videoFps = 25;
	public int bitRate = 1600;
	public String publishUrl = null;
	public int reConnectTimes = MediaStreamer.DEFAULT_RECONNECT_TIMES;
}
