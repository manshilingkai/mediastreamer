package android.slkmedia.mediastreamer;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.slkmedia.mediastreamer.AudioOptions;
import android.slkmedia.mediastreamer.VideoOptions;
import android.util.AttributeSet;
import android.util.Log;
import android.view.TextureView;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;

public class CameraTextureView extends TextureView implements CameraTextureCoreListener, MediaStreamerListener{
	private static final String TAG = "CameraTextureView";

	public CameraTextureView(Context context) {
		super(context);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public CameraTextureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public CameraTextureView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public CameraTextureView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	private int mOutputSurfaceTextureWidth = 0, mOutputSurfaceTextureHeight = 0;
	private SurfaceTexture mOutputSurfaceTexture = null;
	
	private SurfaceTextureListener mSurfaceTextureListener = new SurfaceTextureListener()
	{
		@Override
		public void onSurfaceTextureAvailable(SurfaceTexture surface,
				int width, int height) {
			mOutputSurfaceTexture = surface;
			mOutputSurfaceTextureWidth = width;
			mOutputSurfaceTextureHeight = height;
			
			if(mCameraTextureCore!=null)
			{
				mCameraTextureCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
			}
			
			Log.d(TAG, "onSurfaceTextureAvailable");
		}

		@Override
		public void onSurfaceTextureSizeChanged(SurfaceTexture surface,
				int width, int height) {
			mOutputSurfaceTextureWidth = width;
			mOutputSurfaceTextureHeight = height;
			
			if(mCameraTextureCore!=null)
			{
				mCameraTextureCore.resizeSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
			}
			
			Log.d(TAG, "onSurfaceTextureSizeChanged");
		}

		@Override
		public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
			mOutputSurfaceTexture = null;
			mOutputSurfaceTextureWidth = 0;
			mOutputSurfaceTextureHeight = 0;
			
			if(mCameraTextureCore!=null)
			{
				mCameraTextureCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
			}
			
			Log.d(TAG, "onSurfaceTextureDestroyed");
			
			return true;
		}

		@Override
		public void onSurfaceTextureUpdated(SurfaceTexture surface) {
//			Log.d(TAG, "onSurfaceTextureUpdated");
		}
	};
	
	private Handler cameraPublishCallbackHandler = null;
	public void initialize(CameraOptions options)
	{
		Looper workLooper = Looper.myLooper();
		if(workLooper==null)
		{
			workLooper = Looper.getMainLooper();
		}
		cameraPublishCallbackHandler = new Handler(workLooper){
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case MediaStreamer.CALLBACK_MEDIA_STREAMER_CONNECTING:
					Log.d(TAG, "onMediaStreamerConnecting");
					if(mCameraTextureViewListener!=null)
					{
						mCameraTextureViewListener.onMediaStreamerConnecting();
					}
					break;
				case MediaStreamer.CALLBACK_MEDIA_STREAMER_CONNECTED:
					Log.d(TAG, "onMediaStreamerConnected");
					if(mCameraTextureViewListener!=null)
					{
						mCameraTextureViewListener.onMediaStreamerConnected();
					}
					break;
				case MediaStreamer.CALLBACK_MEDIA_STREAMER_STREAMING:
					Log.d(TAG, "onMediaStreamerStreaming");
					if(mCameraTextureViewListener!=null)
					{
						mCameraTextureViewListener.onMediaStreamerStreaming();
					}
					break;
				case MediaStreamer.CALLBACK_MEDIA_STREAMER_ERROR:
					if(mCameraTextureCore!=null)
					{
						mCameraTextureCore.setListener(null);
					}
					
					if(mMediaStreamer!=null)
					{
						mMediaStreamer.Stop();
						mMediaStreamer.Release();
						mMediaStreamer = null;
					}
					
					if(mCameraTextureCore!=null)
					{
						mCameraTextureCore.stopEncodeSurfaceCore();
					}
					
					videoOptions = null;
					audioOptions = null;
					
					if(cameraPublishCallbackHandler!=null)
					{
						cameraPublishCallbackHandler.removeCallbacksAndMessages(null);
					}
					
					Log.d(TAG, "onMediaStreamerError [ErrorType : " + String.valueOf(msg.arg1) + "]");
					if(mCameraTextureViewListener!=null)
					{
						mCameraTextureViewListener.onMediaStreamerError(msg.arg1);
					}
					break;
				case MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO:
//					Log.d(TAG, "onMediaStreamerInfo [InfoType : " + String.valueOf(msg.arg1) + "]" + "[infoValue : " + String.valueOf(msg.arg2) + "]");
					if(mCameraTextureViewListener!=null)
					{
						mCameraTextureViewListener.onMediaStreamerInfo(msg.arg1, msg.arg2);
					}
					break;
				case MediaStreamer.CALLBACK_MEDIA_STREAMER_END:
					if(mCameraTextureCore!=null)
					{
						mCameraTextureCore.setListener(null);
					}
					
					if(cameraPublishCallbackHandler!=null)
					{
						cameraPublishCallbackHandler.removeCallbacksAndMessages(null);
					}
					
					Log.d(TAG, "onMediaStreamerEnd");
					
					if(mCameraTextureViewListener!=null)
					{
						mCameraTextureViewListener.onMediaStreamerEnd();
					}
					break;
				default:
					break;
				}
			}
		};
		
		startCameraPreview(options);
	}
	
    private CameraTextureCore mCameraTextureCore = null;
	private void startCameraPreview(CameraOptions options)
	{
	    if (!hasPermission(this.getContext(), android.Manifest.permission.CAMERA)) {
	        Log.e(TAG, "CAMERA permission is missing");
	        options.hasFlashLight = false;
	    }else{
//		    if (!hasPermission(this.getContext(), android.Manifest.permission.FLASHLIGHT)) {
//		        Log.e(TAG, "FLASHLIGHT permission is missing");
//		        options.hasFlashLight = false;
//		    }
	        options.hasFlashLight = true;

			mCameraTextureCore = new CameraTextureCore(options);
			mCameraTextureCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
	    }
	}
	
	private void stopCameraPreview()
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.Finalize();
			mCameraTextureCore = null;
		}
	}
	
	public void setFilter(int type, String filterDir)
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.setFilter(type, filterDir);
		}
	}
	
	public void setDisplayScalingMode(int mode)
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.setDisplayScalingMode(mode);
		}
	}
	
	public void switchCameraFlashLight()
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.switchCameraFlashLight();
		}
	}

	public void switchCamera()
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.switchCamera();
		}
	}
	
	public void setCameraZoomFactor(float factor)
	{
		//todo
	}
	
	public Bitmap takePhoto()
	{
		return this.getBitmap();
	}
	
	private CameraTextureViewListener mCameraTextureViewListener = null;
	public void setListener(CameraTextureViewListener listener)
	{
		mCameraTextureViewListener = listener;
	}
	
	private MediaStreamer mMediaStreamer = null;
	private VideoOptions videoOptions = null;
	private AudioOptions audioOptions = null;
	public static final int DEFAULT_AUDIO_BITRATE = 64;//64k
	public boolean startPublish(PublishStreamOptions options)
	{
		videoOptions = new VideoOptions();
		audioOptions = new AudioOptions();
		
		if(options.hasVideo)
		{
			if(mCameraTextureCore!=null)
			{
				videoOptions.hasVideo = true;
			}else{
				videoOptions.hasVideo = false;
			}
		}else{
			videoOptions.hasVideo = false;
		}
		
		if(videoOptions.hasVideo)
		{
			if(options.enableMediaCodecEncoder)
			{
				videoOptions.videoEncodeType = MediaStreamer.VIDEO_HARD_ENCODE;
			}else{
				videoOptions.videoEncodeType = MediaStreamer.VIDEO_SOFT_ENCODE;
			}
			videoOptions.videoWidth = options.videoWidth;
			videoOptions.videoHeight = options.videoHeight;

			videoOptions.videoFps = options.videoFps;
			videoOptions.videoProfile = 0; //0:base_line 1:main_profile 2:high_profile
			videoOptions.videoBitRate = options.bitRate-DEFAULT_AUDIO_BITRATE;
			
			if(videoOptions.videoEncodeType == MediaStreamer.VIDEO_SOFT_ENCODE)
			{
				videoOptions.videoRawType = MediaStreamer.VIDEOFRAME_RAWTYPE_I420;
			}else{
				videoOptions.videoRawType = MediaStreamer.VIDEOFRAME_RAWTYPE_NV21;
			}
			
			videoOptions.encodeMode = 1; //0:VBR or 1:CBR
			videoOptions.quality = 0;
			videoOptions.maxKeyFrameIntervalMs = 5000;
			videoOptions.bStrictCBR = true;
			videoOptions.deblockingFilterFactor = 0;
		}

		audioOptions.hasAudio = options.hasAudio;
		
		if(audioOptions.hasAudio)
		{
			audioOptions.audioSampleRate = 44100;
			audioOptions.audioNumChannels = 1;
			audioOptions.audioBitRate = DEFAULT_AUDIO_BITRATE;
		}
		
		if(!videoOptions.hasVideo && !audioOptions.hasAudio) return false;
		
		if(videoOptions.hasVideo && videoOptions.videoEncodeType == MediaStreamer.VIDEO_HARD_ENCODE)
		{
			mCameraTextureCore.startEncodeSurfaceCore(videoOptions, audioOptions, options.publishUrl, this);
		}else{
			mMediaStreamer = new MediaStreamer(this.getContext());
			mMediaStreamer.setMediaStreamerListener(this);
			mMediaStreamer.Start(mCameraTextureCore.makeVideoEncodeSize(videoOptions, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight), audioOptions, options.publishUrl, options.reConnectTimes);
		}

		if(videoOptions.hasVideo && videoOptions.videoEncodeType == MediaStreamer.VIDEO_SOFT_ENCODE)
		{
			if(mCameraTextureCore!=null)
			{
				mCameraTextureCore.setListener(this);
			}
		}
		
		return true;
	}
	
	public void pausePublish()
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.setListener(null);
		}
		
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.Pause();
		}
	}
	
	public void resumePublish()
	{
		if(videoOptions.hasVideo && videoOptions.videoEncodeType == MediaStreamer.VIDEO_SOFT_ENCODE)
		{
			if(mCameraTextureCore!=null)
			{
				mCameraTextureCore.setListener(this);
			}
		}
		
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.Resume();
		}
	}
	
	public void stopPublish()
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.setListener(null);
		}
		
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.Stop();
			mMediaStreamer.Release();
			mMediaStreamer = null;
		}
		
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.stopEncodeSurfaceCore();
		}
		
		videoOptions = null;
		audioOptions = null;
		
		if(cameraPublishCallbackHandler!=null)
		{
			cameraPublishCallbackHandler.removeCallbacksAndMessages(null);
		}
	}
	
	public void enableAudio(boolean isEnable)
	{
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.EnableAudio(isEnable);
		}
		
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.enableAudio(isEnable);
		}
	}
	
	public void mixBGM(String bgmUrl, float volume, int numberOfLoops)
	{
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.MixBGM(bgmUrl, volume, numberOfLoops);
		}
		
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.mixBGM(bgmUrl, volume, numberOfLoops);
		}
	}
	
	public void playBGM()
	{
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.PlayBGM();
		}
		
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.playBGM();
		}
	}
	
	public void pauseBGM()
	{
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.PauseBGM();
		}
		
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.pauseBGM();
		}
	}
	
	public void stopBGM()
	{
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.StopBGM();
		}
		
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.stopBGM();
		}
	}
	
	public void setBGMVolume(float volume)
	{
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.SetBGMVolume(volume);
		}
		
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.setBGMVolume(volume);
		}
	}
	
	public void mixSEM(String semUrl, float volume, int numberOfLoops)
	{
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.MixSEM(semUrl, volume, numberOfLoops);
		}
		
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.mixSEM(semUrl, volume, numberOfLoops);
		}
	}
		
	public void release()
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.setListener(null);
		}
		
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.Stop();
			mMediaStreamer.Release();
			mMediaStreamer = null;
		}
		
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.stopEncodeSurfaceCore();
		}
		
		videoOptions = null;
		audioOptions = null;
		
		stopCameraPreview();
		
		if(cameraPublishCallbackHandler!=null)
		{
			cameraPublishCallbackHandler.removeCallbacksAndMessages(null);
			cameraPublishCallbackHandler = null;
		}
	}
	
    @Override
    protected void finalize() throws Throwable {
        try {
        	release();
        } finally {
            super.finalize();
        }
    }
	
	@Override
	public void onHandleCameraFrame(byte[] videoFrameData, int videoFrameWidth,
			int videoFrameHeight) {
    	if(mMediaStreamer!=null)
    	{
    		mMediaStreamer.InputPreviewFrame(videoFrameData, videoFrameWidth, videoFrameHeight, 0, MediaStreamer.VIDEOFRAME_RAWTYPE_RGBA);
    	}
	}

	@Override
	public void onMediaStreamerConnecting() {
		if(cameraPublishCallbackHandler!=null)
		{
			Message msg = cameraPublishCallbackHandler.obtainMessage(MediaStreamer.CALLBACK_MEDIA_STREAMER_CONNECTING, 0, 0);
			msg.sendToTarget();
		}
	}

	@Override
	public void onMediaStreamerConnected() {
		if(cameraPublishCallbackHandler!=null)
		{
			Message msg = cameraPublishCallbackHandler.obtainMessage(MediaStreamer.CALLBACK_MEDIA_STREAMER_CONNECTED, 0, 0);
			msg.sendToTarget();
		}
	}

	@Override
	public void onMediaStreamerStreaming() {
		if(cameraPublishCallbackHandler!=null)
		{
			Message msg = cameraPublishCallbackHandler.obtainMessage(MediaStreamer.CALLBACK_MEDIA_STREAMER_STREAMING, 0, 0);
			msg.sendToTarget();
		}
	}

	@Override
	public void onMediaStreamerError(int errorType) {
		if(cameraPublishCallbackHandler!=null)
		{
			Message msg = cameraPublishCallbackHandler.obtainMessage(MediaStreamer.CALLBACK_MEDIA_STREAMER_ERROR, errorType, 0);
			msg.sendToTarget();
		}
	}

	@Override
	public void onMediaStreamerInfo(int infoType, int infoValue) {
		if(cameraPublishCallbackHandler!=null)
		{
			Message msg = cameraPublishCallbackHandler.obtainMessage(MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO, infoType, infoValue);
			msg.sendToTarget();
		}
	}

	@Override
	public void onMediaStreamerEnd() {
		
		if(cameraPublishCallbackHandler!=null)
		{
			Message msg = cameraPublishCallbackHandler.obtainMessage(MediaStreamer.CALLBACK_MEDIA_STREAMER_END, 0, 0);
			msg.sendToTarget();
		}
	}
	
	// Checks if the process has as specified permission or not.
	private static boolean hasPermission(Context context, String permission) {
	    return context.checkPermission(permission, Process.myPid(), Process.myUid())
	        == PackageManager.PERMISSION_GRANTED;
	}
}
