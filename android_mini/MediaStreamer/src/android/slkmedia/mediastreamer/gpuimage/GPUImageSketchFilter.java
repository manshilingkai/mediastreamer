package android.slkmedia.mediastreamer.gpuimage;

import android.opengl.GLES20;

public class GPUImageSketchFilter extends GPUImageFilter{
	
	private static final String SKETCH_FRAGMENT_SHADER = ""+
			"varying highp vec2 textureCoordinate;\n" +
		    "precision highp float;\n" +
		    "uniform sampler2D inputImageTexture;\n" +
		    "uniform vec2 singleStepOffset;\n" +
		    "uniform float strength;\n" +
		    "const highp vec3 W = vec3(0.299,0.587,0.114);\n" +
		    "void main()\n" +
		    "{\n" +
		    "float threshold = 0.0;\n" +
		    "vec4 oralColor = texture2D(inputImageTexture, textureCoordinate);\n" +
		    "vec3 maxValue = vec3(0.,0.,0.);\n" +
		    "for(int i = -2; i<=2; i++)\n" +
		    "{\n" +
		    "for(int j = -2; j<=2; j++)\n" +
		    "{\n" + 
		    "vec4 tempColor = texture2D(inputImageTexture, textureCoordinate+singleStepOffset*vec2(i,j));\n" + 
		    "maxValue.r = max(maxValue.r,tempColor.r);\n" + 
		    "maxValue.g = max(maxValue.g,tempColor.g);\n" + 
		    "maxValue.b = max(maxValue.b,tempColor.b);\n" +
		    "threshold += dot(tempColor.rgb, W);\n" +
		    "}\n" +
		    "}\n" +
		    "float gray1 = dot(oralColor.rgb, W);\n" +
		    "float gray2 = dot(maxValue, W);\n" +
		    "float contour = gray1 / gray2;\n" +
		    "threshold = threshold / 25.;\n" +
		    "float alpha = max(strength,gray1>threshold?1.0:(gray1/threshold));\n" +
		    "float result = contour * alpha + (1.0-alpha)*gray1;\n" +
		    "gl_FragColor = vec4(vec3(result,result,result), oralColor.w);\n" +
		    "}\n";
	
	private int mSingleStepOffsetLocation;
	//0.0 - 1.0
	private int mStrength;
	
	public GPUImageSketchFilter(){
		super(NO_FILTER_VERTEX_SHADER, SKETCH_FRAGMENT_SHADER);
	}
	
	protected void onInit() {
        super.onInit();
        mSingleStepOffsetLocation = GLES20.glGetUniformLocation(mGLProgId, "singleStepOffset");
        mStrength = GLES20.glGetUniformLocation(mGLProgId, "strength");
    }
    
    protected void onInitialized() {
    	super.onInitialized();
        setFloat(mStrength, 0.5f);
    }
	
    private void setTexelSize(final float w, final float h) {
		setFloatVec2(mSingleStepOffsetLocation, new float[] {1.0f / w, 1.0f / h});
	}
	
	@Override
    public void onOutputSizeChanged(final int width, final int height) {
        super.onOutputSizeChanged(width, height);
        setTexelSize(width, height);
    }
}
