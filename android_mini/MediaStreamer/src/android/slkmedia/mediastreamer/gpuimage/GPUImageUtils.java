package android.slkmedia.mediastreamer.gpuimage;

public class GPUImageUtils {
	public static final int FILTER_RGB = 0;
	public static final int FILTER_SKETCH = 1;
	public static final int FILTER_AMARO = 2;
	public static final int FILTER_ANTIQUE = 3;
	public static final int FILTER_BLACKCAT = 4;
	public static final int FILTER_BEAUTY = 5;
	public static final int FILTER_BRANNAN = 6;
	public static final int FILTER_N1977 = 7;
	public static final int FILTER_BROOKLYN = 8;
	public static final int FILTER_COOL = 9;
	public static final int FILTER_CRAYON = 10;
	public static final int FILTER_NUM = 11;
	
	public static final int DISPLAY_SCALING_MODE_SCALE_TO_FILL = 0;
	public static final int DISPLAY_SCALING_MODE_SCALE_TO_FIT = 1;
	public static final int DISPLAY_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING = 2;
	
	public static final int NO_ROTATION = 0;
	public static final int ROTATION_LEFT = 1;
	public static final int ROTATION_RIGHT = 2;
	public static final int ROTATION_180 = 3;
	
}
