package android.slkmedia.mediastreamer.gpuimage;

import android.opengl.GLES20;

public class GPUImageBrannanFilter extends GPUImageFilter{
	
	private static final String BRANNAN_FRAGMENT_SHADER = ""+
		    "precision mediump float;\n"+
		    "varying mediump vec2 textureCoordinate;\n"+
		    "uniform sampler2D inputImageTexture;\n"+
		    "uniform sampler2D inputImageTexture2;\n"+
		    "uniform sampler2D inputImageTexture3;\n"+
		    "uniform sampler2D inputImageTexture4;\n"+
		    "uniform sampler2D inputImageTexture5;\n"+
		    "uniform sampler2D inputImageTexture6;\n"+
		    "mat3 saturateMatrix = mat3(1.105150,-0.044850,-0.046000,-0.088050,1.061950,-0.089200,-0.017100,-0.017100,1.132900);\n"+
		    "vec3 luma = vec3(.3, .59, .11);\n"+
		    "uniform float strength;\n"+
		    "void main()\n"+
		    "{\n"+
		    "vec4 originColor = texture2D(inputImageTexture, textureCoordinate);\n"+
		    "vec3 texel = texture2D(inputImageTexture, textureCoordinate).rgb;\n"+
		    "vec2 lookup;\n"+
		    "lookup.y = 0.5;\n"+
		    "lookup.x = texel.r;\n"+
		    "texel.r = texture2D(inputImageTexture2, lookup).r;\n"+
		    "lookup.x = texel.g;\n"+
		    "texel.g = texture2D(inputImageTexture2, lookup).g;\n"+
		    "lookup.x = texel.b;\n"+
		    "texel.b = texture2D(inputImageTexture2, lookup).b;\n"+
		    "texel = saturateMatrix * texel;\n"+
		    "vec2 tc = (2.0 * textureCoordinate) - 1.0;\n"+
		    "float d = dot(tc, tc);\n"+
		    "vec3 sampled;\n"+
		    "lookup.y = 0.5;\n"+
		    "lookup.x = texel.r;\n"+
		    "sampled.r = texture2D(inputImageTexture3, lookup).r;\n"+
		    "lookup.x = texel.g;\n"+
		    "sampled.g = texture2D(inputImageTexture3, lookup).g;\n"+
		    "lookup.x = texel.b;\n"+
		    "sampled.b = texture2D(inputImageTexture3, lookup).b;\n"+
		    "float value = smoothstep(0.0, 1.0, d);\n"+
		    "texel = mix(sampled, texel, value);\n"+
		    "lookup.x = texel.r;\n"+
		    "texel.r = texture2D(inputImageTexture4, lookup).r;\n"+
		    "lookup.x = texel.g;\n"+
		    "texel.g = texture2D(inputImageTexture4, lookup).g;\n"+
		    "lookup.x = texel.b;\n"+
		    "texel.b = texture2D(inputImageTexture4, lookup).b;\n"+
		    "lookup.x = dot(texel, luma);\n"+
		    "texel = mix(texture2D(inputImageTexture5, lookup).rgb, texel, .5);\n"+
		    "lookup.x = texel.r;\n"+
		    "texel.r = texture2D(inputImageTexture6, lookup).r;\n"+
		    "lookup.x = texel.g;\n"+
		    "texel.g = texture2D(inputImageTexture6, lookup).g;\n"+
		    "lookup.x = texel.b;\n"+
		    "texel.b = texture2D(inputImageTexture6, lookup).b;\n"+
		    "texel = mix(originColor.rgb, texel.rgb, strength);\n"+
		    "gl_FragColor = vec4(texel, 1.0);\n"+
		    "}\n";
	
	private int[] inputTextureHandles = {-1,-1,-1,-1,-1};
	private int[] inputTextureUniformLocations = {-1,-1,-1,-1,-1};
	private String mFilterDir = null;
	public GPUImageBrannanFilter(String filterDir){
		super(NO_FILTER_VERTEX_SHADER,BRANNAN_FRAGMENT_SHADER);
		mFilterDir = new String(filterDir);
	}
	
	protected void onDestroy() {
        super.onDestroy();
        GLES20.glDeleteTextures(inputTextureHandles.length, inputTextureHandles, 0);
        for(int i = 0; i < inputTextureHandles.length; i++)
        	inputTextureHandles[i] = -1;
    }
	
	protected void onDrawArraysAfter(){
		for(int i = 0; i < inputTextureHandles.length
				&& inputTextureHandles[i] != OpenGLUtils.NO_TEXTURE; i++){
			GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + (i+1));
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
//			GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		}
	}
	  
	protected void onDrawArraysPre(){
		for(int i = 0; i < inputTextureHandles.length 
				&& inputTextureHandles[i] != OpenGLUtils.NO_TEXTURE; i++){
			GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + (i+1) );
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, inputTextureHandles[i]);
			GLES20.glUniform1i(inputTextureUniformLocations[i], (i+1));
		}
	}
	
	protected void onInit(){
		super.onInit();
		for(int i=0; i < inputTextureUniformLocations.length; i++){
			inputTextureUniformLocations[i] = GLES20.glGetUniformLocation(mGLProgId, "inputImageTexture"+(2+i));
		}
	}
	
	protected void onInitialized(){
		super.onInitialized();
	    runOnDraw(new Runnable(){
		    public void run(){
		    	inputTextureHandles[0] = OpenGLUtils.loadTexture(mFilterDir+"/brannan_process.png");
				inputTextureHandles[1] = OpenGLUtils.loadTexture(mFilterDir+"/brannan_blowout.png");
				inputTextureHandles[2] = OpenGLUtils.loadTexture(mFilterDir+"/brannan_contrast.png");
				inputTextureHandles[3] = OpenGLUtils.loadTexture(mFilterDir+"/brannan_luma.png");
				inputTextureHandles[4] = OpenGLUtils.loadTexture(mFilterDir+"/brannan_screen.png");
		    }
	    });
	}
}
