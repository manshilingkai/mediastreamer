package android.slkmedia.mediastreamer.gpuimage;

public class TextureRotationUtil {
	
    public static final float CUBE[] = {
        -1.0f, -1.0f,
        1.0f, -1.0f,
        -1.0f, 1.0f,
        1.0f, 1.0f,
	};
    
    public static void calculateCropTextureCoordinates(GPUImageRotationMode rotationMode, float minX, float minY, float maxX, float maxY, float[] rotatedTex)
    {
//        float[] rotatedTex = new float[8];

        switch (rotationMode) {
        case kGPUImageNoRotation:
        	rotatedTex[0] = minX; // 0,0
        	rotatedTex[1] = minY;
            
        	rotatedTex[2] = maxX; // 1,0
        	rotatedTex[3] = minY;
            
        	rotatedTex[4] = minX; // 0,1
        	rotatedTex[5] = maxY;
            
        	rotatedTex[6] = maxX; // 1,1
        	rotatedTex[7] = maxY;
            
            break;
        case kGPUImageRotateLeft:
        	rotatedTex[0] = maxY; // 1,0
        	rotatedTex[1] = 1.0f - maxX;
            
        	rotatedTex[2] = maxY; // 1,1
        	rotatedTex[3] = 1.0f - minX;
            
        	rotatedTex[4] = minY; // 0,0
        	rotatedTex[5] = 1.0f - maxX;
            
        	rotatedTex[6] = minY; // 0,1
        	rotatedTex[7] = 1.0f - minX;
            
            break;
        case kGPUImageRotateRight:
        	rotatedTex[0] = minY; // 0,1
        	rotatedTex[1] = 1.0f - minX;
            
        	rotatedTex[2] = minY; // 0,0
        	rotatedTex[3] = 1.0f - maxX;
            
        	rotatedTex[4] = maxY; // 1,1
        	rotatedTex[5] = 1.0f - minX;
            
        	rotatedTex[6] = maxY; // 1,0
        	rotatedTex[7] = 1.0f - maxX;
            
            break;
        case kGPUImageFlipVertical:
        	rotatedTex[0] = minX; // 0,1
        	rotatedTex[1] = maxY;
            
        	rotatedTex[2] = maxX; // 1,1
        	rotatedTex[3] = maxY;
            
        	rotatedTex[4] = minX; // 0,0
        	rotatedTex[5] = minY;
            
        	rotatedTex[6] = maxX; // 1,0
        	rotatedTex[7] = minY;
        	
        	break;
        case kGPUImageFlipHorizonal:
        	rotatedTex[0] = maxX; // 1,0
        	rotatedTex[1] = minY;
            
        	rotatedTex[2] = minX; // 0,0
        	rotatedTex[3] = minY;
            
        	rotatedTex[4] = maxX; // 1,1
        	rotatedTex[5] = maxY;
            
        	rotatedTex[6] = minX; // 0,1
        	rotatedTex[7] = maxY;
        	
        	break;
        case kGPUImageRotate180: // Fixed
        	rotatedTex[0] = maxX; // 1,1
        	rotatedTex[1] = maxY;
            
        	rotatedTex[2] = minX; // 0,1
            rotatedTex[3] = maxY;
            
            rotatedTex[4] = maxX; // 1,0
            rotatedTex[5] = minY;
            
            rotatedTex[6] = minX; // 0,0
            rotatedTex[7] = minY;
            
            break;
        case kGPUImageRotateRightFlipVertical: // Fixed
        	rotatedTex[0] = minY; // 0,0
        	rotatedTex[1] = 1.0f - maxX;
            
        	rotatedTex[2] = minY; // 0,1
        	rotatedTex[3] = 1.0f - minX;
            
        	rotatedTex[4] = maxY; // 1,0
        	rotatedTex[5] = 1.0f - maxX;
            
        	rotatedTex[6] = maxY; // 1,1
        	rotatedTex[7] = 1.0f - minX;
        	
        	break;
        case kGPUImageRotateRightFlipHorizontal: // Fixed
        	rotatedTex[0] = maxY; // 1,1
        	rotatedTex[1] = 1.0f - minX;
            
        	rotatedTex[2] = maxY; // 1,0
        	rotatedTex[3] = 1.0f - maxX;
            
        	rotatedTex[4] = minY; // 0,1
        	rotatedTex[5] = 1.0f - minX;
            
        	rotatedTex[6] = minY; // 0,0
        	rotatedTex[7] = 1.0f - maxX;
        	
        	break;
        default:
        	rotatedTex[0] = minX; // 0,0
        	rotatedTex[1] = minY;
            
        	rotatedTex[2] = maxX; // 1,0
        	rotatedTex[3] = minY;
            
        	rotatedTex[4] = minX; // 0,1
        	rotatedTex[5] = maxY;
            
        	rotatedTex[6] = maxX; // 1,1
        	rotatedTex[7] = maxY;
            
            break;
        }
        
//        return rotatedTex;
    }
}
