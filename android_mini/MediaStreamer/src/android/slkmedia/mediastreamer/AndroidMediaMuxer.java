package android.slkmedia.mediastreamer;

import java.io.IOException;
import java.nio.ByteBuffer;

import android.media.MediaCodec.BufferInfo;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.util.Log;

public class AndroidMediaMuxer implements MediaMuxerInterface{
	private static final String TAG = "AndroidMediaMuxer";

	//It also supports muxing B-frames in MP4 since Android Nougat
	private MediaMuxer mSystemMediaMuxer = null;
	public AndroidMediaMuxer(String path, int format)
	{
		try {
			mSystemMediaMuxer = new MediaMuxer(path, format);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private MediaMuxerListener mediaMuxerListener = null;
	@Override
	public void setMediaMuxerListener(MediaMuxerListener listener)
	{
		mediaMuxerListener = listener;
	}
	
	private int mVideoTrackIndex = -1;
	private int mAudioTrackIndex = -1;
	@Override
	public int addTrack(MediaFormat format) {
		if(format.getString(MediaFormat.KEY_MIME).startsWith("video/"))
		{
			mVideoTrackIndex = mSystemMediaMuxer.addTrack(format);
			return mVideoTrackIndex;
		}else if(format.getString(MediaFormat.KEY_MIME).startsWith("audio/"))
		{
			mAudioTrackIndex = mSystemMediaMuxer.addTrack(format);
			return mAudioTrackIndex;
		}
		
		return -1;
	}

	@Override
	public void start() {
		if(mediaMuxerListener!=null)
		{
			mediaMuxerListener.onMediaMuxerConnecting();
		}
		
		mSystemMediaMuxer.start();
		
		if(mediaMuxerListener!=null)
		{
			mediaMuxerListener.onMediaMuxerConnected();
		}
	}

	@Override
	public void stop() {
		
		if(haveWrittenMediaData)
		{
			mSystemMediaMuxer.stop();
			haveWrittenMediaData = false;
		}
		
		mGotFirstVideoSample = false;
		mVideoSamplePresentationTimeUsBaseLine = 0;
		mVideoSamplePresentationTimeUs = 0;
		mGotFirstAudioSample = false;
		mAudioSamplePresentationTimeUsBaseLine = 0;
		mAudioSamplePresentationTimeUs = 0;
		mLastSendPresentationTimeUs = 0;
		
		if(mediaMuxerListener!=null)
		{
			mediaMuxerListener.onMediaMuxerEnd();
		}
	}

	@Override
	public void release() {
		if(mSystemMediaMuxer!=null)
		{
			try{
				mSystemMediaMuxer.release();
			}catch (java.lang.IllegalStateException e){
				
			}
			
			mSystemMediaMuxer = null;
		}
		
		haveWrittenMediaData = false;

		mGotFirstVideoSample = false;
		mVideoSamplePresentationTimeUsBaseLine = 0;
		mVideoSamplePresentationTimeUs = 0;
		mGotFirstAudioSample = false;
		mAudioSamplePresentationTimeUsBaseLine = 0;
		mAudioSamplePresentationTimeUs = 0;
		mLastSendPresentationTimeUs = 0;
	}

	private boolean haveWrittenMediaData = false;
	
	private boolean mGotFirstVideoSample = false;
	private long mVideoSamplePresentationTimeUsBaseLine = 0;
	private long mVideoSamplePresentationTimeUs = 0;
	private boolean mGotFirstAudioSample = false;
	private long mAudioSamplePresentationTimeUsBaseLine = 0;
	private long mAudioSamplePresentationTimeUs = 0;
	
	private long mLastSendPresentationTimeUs = 0;
	@Override
	public void writeSampleData(int trackIndex, ByteBuffer byteBuf,
			BufferInfo bufferInfo) {
//		Log.d(TAG, "SystemMediaMuxer.writeSampleData");
		mSystemMediaMuxer.writeSampleData(trackIndex, byteBuf, bufferInfo);
		
		if(!haveWrittenMediaData)
		{
			haveWrittenMediaData = true;
			if(mediaMuxerListener!=null)
			{
				mediaMuxerListener.onMediaMuxerStreaming();
			}
		}
		
		if(trackIndex==mVideoTrackIndex && mVideoTrackIndex!=-1)
		{
			if(!mGotFirstVideoSample)
			{
				mGotFirstVideoSample = true;
				mVideoSamplePresentationTimeUsBaseLine = bufferInfo.presentationTimeUs;
			}
			
			mVideoSamplePresentationTimeUs = bufferInfo.presentationTimeUs - mVideoSamplePresentationTimeUsBaseLine;
		}else if(trackIndex==mAudioTrackIndex && mAudioTrackIndex!=-1)
		{
			if(!mGotFirstAudioSample)
			{
				mGotFirstAudioSample = true;
				mAudioSamplePresentationTimeUsBaseLine = bufferInfo.presentationTimeUs;
			}
			
			mAudioSamplePresentationTimeUs = bufferInfo.presentationTimeUs - mAudioSamplePresentationTimeUsBaseLine;
		}
		
		if(mVideoSamplePresentationTimeUs-mLastSendPresentationTimeUs>=100*1000)
		{
			mLastSendPresentationTimeUs = mLastSendPresentationTimeUs+100*1000;
			
			if(mediaMuxerListener!=null)
			{
				mediaMuxerListener.onMediaMuxerInfo(SLKMediaMuxer.CALLBACK_MEDIA_MUXER_INFO_PUBLISH_TIME, (int)(mLastSendPresentationTimeUs/100000));
			}
		}
		if(mAudioSamplePresentationTimeUs-mLastSendPresentationTimeUs>=100*1000)
		{
			mLastSendPresentationTimeUs = mLastSendPresentationTimeUs+100*1000;
			
			if(mediaMuxerListener!=null)
			{
				mediaMuxerListener.onMediaMuxerInfo(SLKMediaMuxer.CALLBACK_MEDIA_MUXER_INFO_PUBLISH_TIME, (int)(mLastSendPresentationTimeUs/100000));
			}
		}
	}
	
	@Override
	public long getPublishDelayTimeMs()
	{
		return 0;
	}
}
