package android.slkmedia.mediaprocesser;

import java.nio.ByteBuffer;

import android.media.MediaFormat;

public interface MediaExtractorInterface {
	public abstract void setDataSource(String path);
	public abstract int getTrackCount();
	public abstract MediaFormat getTrackFormat(int index); 
	public abstract void selectTrack(int index);
	public abstract void unselectTrack(int index);
	public abstract int readSampleData(ByteBuffer byteBuf, int offset);
	public abstract int getSampleTrackIndex();
	public abstract long getSampleTime();
	public abstract int getSampleFlags();
	public abstract void seekTo(long timeUs, int mode);
	public abstract long getCachedDuration();
	public abstract boolean hasCacheReachedEndOfStream();
	public abstract boolean advance();
	public abstract void release();
}
