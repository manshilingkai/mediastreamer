package android.slkmedia.mediaprocesser;

import java.nio.ByteBuffer;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.media.MediaCodec.BufferInfo;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Build;
import android.util.Log;

public class MediaWriter implements MediaWriterInterface{
	private static final String TAG = "MediaWriter";
	
    public static final class OutputFormat {
        public static final int MUXER_OUTPUT_MPEG_4 = 0;
        public static final int MUXER_OUTPUT_FLV   = 1;
    };
    
    public static final int SYSTEM_MEDIA_WRITER_TYPE = 0;
    public static final int FFMPEG_MEDIA_WRITER_TYPE = 1;
	
	private Lock mMediaWriterLock = null;
	private MediaWriterInterface mMediaWriterInterface = null;
	public MediaWriter(String path, int format)
	{
		mMediaWriterLock = new ReentrantLock();
		
		if(Build.VERSION.SDK_INT>=24 && format==OutputFormat.MUXER_OUTPUT_MPEG_4)
		{
			mMediaWriterInterface = new AndroidMediaWriter(path, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
		}else{
			mMediaWriterInterface = new FFMediaWriter(path, format);
		}
	}
	
	public MediaWriter(String path)
	{
		mMediaWriterLock = new ReentrantLock();

		mMediaWriterInterface = new FFMediaWriter(path, OutputFormat.MUXER_OUTPUT_MPEG_4);
	}
	
	public MediaWriter(int type, String path, int format)
	{
		mMediaWriterLock = new ReentrantLock();

		if(type == SYSTEM_MEDIA_WRITER_TYPE)
		{
			if(Build.VERSION.SDK_INT>=24 && format==OutputFormat.MUXER_OUTPUT_MPEG_4)
			{
				mMediaWriterInterface = new AndroidMediaWriter(path, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
			}else{
				mMediaWriterInterface = new FFMediaWriter(path, format);
			}
		}else{
			mMediaWriterInterface = new FFMediaWriter(path, format);
		}
	}
	
	private int mVideoTrackIndex = -1;
	private int mAudioTrackIndex = -1;
	@Override
	public int addTrack(MediaFormat format) {
		int ret = -1;

		mMediaWriterLock.lock();
		if(isStarted)
		{
			mMediaWriterLock.unlock();
			return -1;
		}
		
		if(mMediaWriterInterface!=null)
		{
			if(format.getString(MediaFormat.KEY_MIME).startsWith(MediaFormat.MIMETYPE_VIDEO_AVC))
			{
				mVideoTrackIndex = mMediaWriterInterface.addTrack(format);
				ret = mVideoTrackIndex;
			}else if(format.getString(MediaFormat.KEY_MIME).startsWith(MediaFormat.MIMETYPE_AUDIO_AAC))
			{
				mAudioTrackIndex = mMediaWriterInterface.addTrack(format);
				ret = mAudioTrackIndex;
			}
			 
		}
		mMediaWriterLock.unlock();

		return ret;
	}

	private boolean isStarted = false;
	@Override
	public boolean start() {
		boolean ret = false;

		mMediaWriterLock.lock();
		if(isStarted)
		{
			mMediaWriterLock.unlock();
			return true;
		}

		if(mMediaWriterInterface!=null)
		{
			ret = mMediaWriterInterface.start();
			if(ret)
			{
				isStarted = true;
			}
		}
		
		mMediaWriterLock.unlock();
		
		return ret;
	}

	@Override
	public void stop() {
		
		mMediaWriterLock.lock();
		if(!isStarted)
		{
			mMediaWriterLock.unlock();
			return;
		}
		
		if(mMediaWriterInterface!=null)
		{
			mMediaWriterInterface.stop();
		}
		
		isStarted = false;
		
		mMediaWriterLock.unlock();
	}

	@Override
	public void release() {
		
		mMediaWriterLock.lock();
		
		if(isStarted)
		{		
			if(mMediaWriterInterface!=null)
			{
				mMediaWriterInterface.stop();
			}
			
			isStarted = false;
		}
				
		if(mMediaWriterInterface!=null)
		{
			mMediaWriterInterface.release();
			mMediaWriterInterface = null;
		}
		
		mMediaWriterLock.unlock();
	}
	
	private boolean mGotFirstVideoSample = false;
	private long mVideoSamplePresentationTimeUsBaseLine = 0;
	private long mVideoSamplePresentationTimeUs = 0;
	private boolean mGotFirstAudioSample = false;
	private long mAudioSamplePresentationTimeUsBaseLine = 0;
	private long mAudioSamplePresentationTimeUs = 0;
	
	private long mLastPresentationTimeUs = 0;

	@Override
	public boolean writeSampleData(int trackIndex, ByteBuffer byteBuf,
			BufferInfo bufferInfo) {
		
		boolean ret = false;

		mMediaWriterLock.lock();
		if(!isStarted)
		{
			mMediaWriterLock.unlock();
			return false;
		}
		
		if(mMediaWriterInterface!=null)
		{
			if(trackIndex==mVideoTrackIndex && mVideoTrackIndex!=-1)
			{
				if(!mGotFirstVideoSample)
				{
					mGotFirstVideoSample = true;
					if(bufferInfo.presentationTimeUs<0 || bufferInfo.presentationTimeUs>1000*1000) bufferInfo.presentationTimeUs = 0;
					mVideoSamplePresentationTimeUsBaseLine = bufferInfo.presentationTimeUs;
				}
				
				mVideoSamplePresentationTimeUs = bufferInfo.presentationTimeUs - mVideoSamplePresentationTimeUsBaseLine;
				bufferInfo.presentationTimeUs = mVideoSamplePresentationTimeUs;
				
				if(mVideoSamplePresentationTimeUs>mLastPresentationTimeUs)
				{
					mLastPresentationTimeUs = mVideoSamplePresentationTimeUs;
				}

				ret = mMediaWriterInterface.writeSampleData(trackIndex, byteBuf, bufferInfo);

			}else if(trackIndex==mAudioTrackIndex && mAudioTrackIndex!=-1)
			{
				if(!mGotFirstAudioSample)
				{
					mGotFirstAudioSample = true;
					if(bufferInfo.presentationTimeUs<0 || bufferInfo.presentationTimeUs>1000*1000) bufferInfo.presentationTimeUs = 0;
					mAudioSamplePresentationTimeUsBaseLine = bufferInfo.presentationTimeUs;
				}
				
				mAudioSamplePresentationTimeUs = bufferInfo.presentationTimeUs - mAudioSamplePresentationTimeUsBaseLine;
				bufferInfo.presentationTimeUs = mAudioSamplePresentationTimeUs;
				
				if(mAudioSamplePresentationTimeUs>mLastPresentationTimeUs)
				{
					mLastPresentationTimeUs = mAudioSamplePresentationTimeUs;
				}
				
				ret = mMediaWriterInterface.writeSampleData(trackIndex, byteBuf, bufferInfo);
			}
		}
		
		mMediaWriterLock.unlock();

		return ret;
	}
	
	public long getWriteTimeUs()
	{
		long ret = 0;

		mMediaWriterLock.lock();
		if(!isStarted)
		{
			mMediaWriterLock.unlock();
			return 0;
		}
		
		ret = mLastPresentationTimeUs;
		
		mMediaWriterLock.unlock();

		return ret;
	}
}
