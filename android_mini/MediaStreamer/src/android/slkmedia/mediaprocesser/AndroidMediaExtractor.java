package android.slkmedia.mediaprocesser;

import java.io.IOException;
import java.nio.ByteBuffer;

import android.media.MediaExtractor;
import android.media.MediaFormat;

public class AndroidMediaExtractor implements MediaExtractorInterface{

	private MediaExtractor mMediaExtractor = null;
	public AndroidMediaExtractor()
	{
		mMediaExtractor = new MediaExtractor();
	}
	
	@Override
	public void setDataSource(String path) {
		try {
			mMediaExtractor.setDataSource(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getTrackCount() {
		return mMediaExtractor.getTrackCount();
	}

	@Override
	public MediaFormat getTrackFormat(int index) {
		return mMediaExtractor.getTrackFormat(index);
	}

	@Override
	public void selectTrack(int index) {
		mMediaExtractor.selectTrack(index);
	}

	@Override
	public void unselectTrack(int index) {
		mMediaExtractor.unselectTrack(index);
	}

	@Override
	public int readSampleData(ByteBuffer byteBuf, int offset) {
		return mMediaExtractor.readSampleData(byteBuf, offset);
	}

	@Override
	public int getSampleTrackIndex() {
		return mMediaExtractor.getSampleTrackIndex();
	}

	@Override
	public long getSampleTime() {
		return mMediaExtractor.getSampleTime();
	}

	@Override
	public int getSampleFlags() {
		return mMediaExtractor.getSampleFlags();
	}

	@Override
	public void seekTo(long timeUs, int mode) {
		mMediaExtractor.seekTo(timeUs, mode);
	}

	@Override
	public long getCachedDuration() {
		return mMediaExtractor.getCachedDuration();
	}

	@Override
	public boolean hasCacheReachedEndOfStream() {
		return mMediaExtractor.hasCacheReachedEndOfStream();
	}

	@Override
	public boolean advance() {
		return mMediaExtractor.advance();
	}

	@Override
	public void release() {
		mMediaExtractor.release();
	}
}
