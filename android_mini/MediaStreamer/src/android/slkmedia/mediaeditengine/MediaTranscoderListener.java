package android.slkmedia.mediaeditengine;

public interface MediaTranscoderListener {
	public abstract void onMediaTranscoderConnecting();
	public abstract void onMediaTranscoderConnected();
	public abstract void onMediaTranscoderTranscoding();
	public abstract void onMediaTranscoderError(int errorType);
	public abstract void onMediaTranscoderInfo(int infoType, int infoValue);
	public abstract void onMediaTranscoderEnd();
}
