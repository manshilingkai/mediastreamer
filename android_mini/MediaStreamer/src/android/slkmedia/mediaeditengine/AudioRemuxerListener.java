package android.slkmedia.mediaeditengine;

public interface AudioRemuxerListener {
	public abstract void onAudioRemuxerStart();
	public abstract void onAudioRemuxerError(int errorType);
	public abstract void onAudioRemuxerInfo(int infoType, int infoValue);
	public abstract void onAudioRemuxerEnd();
}
