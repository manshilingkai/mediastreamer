package android.slkmedia.mediaeditengine;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

public class AudioRemuxer {
	private static final String TAG = "AudioRemuxer";

	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}

	//Native Context
	private long mNativeContext = 0;
	
	private static native final void Native_Init();
	private native void Native_Start(String inputUrl, int startPosMs, int endPosMs, boolean isRemuxAll, String outputUrl, Object audioremuxer_this);
	private native void Native_Pause();
	private native void Native_Resume();
	private native void Native_Stop(boolean isCancel);
	
	//////////////////////////////////////////////////////////////////////////
	/**
	 * Called from native code when an interesting event happens. This method
	 * just uses the EventHandler system to post the event back to the main app
	 * thread. We use a weak reference to the original MediaPlayer object so
	 * that the native code is safe from the object disappearing from underneath
	 * it. (This is the cookie passed to native_setup().)
	 */
	private static void postEventFromNative(Object audioremuxer_ref, int what,
			int arg1, int arg2, Object obj) {
		AudioRemuxer ar = (AudioRemuxer) ((WeakReference<?>) audioremuxer_ref).get();
		if (ar == null) {
			return;
		}

		if (ar.audioRemuxerCallbackHandler != null) {
			Message msg = ar.audioRemuxerCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}
	//////////////////////////////////////////////////////////////////////////

	private Lock mAudioRemuxerLock = null;
	private Condition mAudioRemuxerCondition = null;
	private HandlerThread mHandlerThread = null;
	private Handler audioRemuxerCallbackHandler = null;

	public AudioRemuxer()
	{
		mAudioRemuxerLock = new ReentrantLock(); 
		mAudioRemuxerCondition = mAudioRemuxerLock.newCondition();
		
		mHandlerThread = new HandlerThread("AudioRemuxerHandlerThread");
		mHandlerThread.start();
		
		audioRemuxerCallbackHandler = new Handler(mHandlerThread.getLooper()) {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case CALLBACK_AUDIO_REMUXER_STREAMING:
					if(mAudioRemuxerListener!=null)
					{
						mAudioRemuxerListener.onAudioRemuxerStart();
					}
					break;
				case CALLBACK_AUDIO_REMUXER_ERROR:
					if(mAudioRemuxerListener!=null)
					{
						mAudioRemuxerListener.onAudioRemuxerError(msg.arg1);
					}
					
					break;
				case CALLBACK_AUDIO_REMUXER_INFO:
					if(mAudioRemuxerListener!=null)
					{
						mAudioRemuxerListener.onAudioRemuxerInfo(msg.arg1, msg.arg2);
					}
					break;
				case CALLBACK_AUDIO_REMUXER_END:
					if(mAudioRemuxerListener!=null)
					{
						mAudioRemuxerListener.onAudioRemuxerEnd();
					}
					
					break;
				default:
					break;
				}
			}
		};
	}
	
	private AudioRemuxerListener mAudioRemuxerListener = null;
	public void setAudioRemuxerListener(AudioRemuxerListener listener)
	{
		mAudioRemuxerListener = listener;
	}
	
	private boolean isStarted = false;
	public void start(AudioRemuxerOptions options)
	{
		mAudioRemuxerLock.lock();

		if(isReleased)
		{
			Log.w(TAG, "AudioRemuxer has released!!");
			mAudioRemuxerLock.unlock();
			return;
		}
		
		if(isStarted)
		{
			Log.w(TAG, "AudioRemuxer has started!!");
			mAudioRemuxerLock.unlock();
			return;
		}
		
		Native_Start(options.inputUrl, options.startPosMs, options.endPosMs, options.isRemuxAll, options.outputUrl, new WeakReference<AudioRemuxer>(this));
		
		isStarted = true;
		isPaused = false;
		mAudioRemuxerLock.unlock();
	}
	
	private boolean isPaused = false;
	public void pause()
	{
		mAudioRemuxerLock.lock();
		
		if(isReleased)
		{
			Log.w(TAG, "AudioRemuxer has released!!");
			mAudioRemuxerLock.unlock();
			return;
		}
		
		if(!isStarted)
		{
			Log.w(TAG, "AudioRemuxer has not started!!");
			mAudioRemuxerLock.unlock();
			return;
		}
		
		if(!isPaused)
		{
			Native_Pause();
			isPaused = true;
		}
		mAudioRemuxerLock.unlock();
	}
	
	public void resume()
	{
		mAudioRemuxerLock.lock();
		
		if(isReleased)
		{
			Log.w(TAG, "AudioRemuxer has released!!");
			mAudioRemuxerLock.unlock();
			return;
		}
		
		if(!isStarted)
		{
			Log.w(TAG, "AudioRemuxer has not started!!");
			mAudioRemuxerLock.unlock();
			return;
		}
		
		if(isPaused)
		{
			Native_Resume();
			isPaused = false;
		}
		
		mAudioRemuxerLock.unlock();
	}
	
	public void stop(boolean isCancle)
	{
		mAudioRemuxerLock.lock();
		
		if(isReleased)
		{
			Log.w(TAG, "AudioRemuxer has released!!");
			mAudioRemuxerLock.unlock();
			return;
		}

		if(!isStarted)
		{
			Log.w(TAG, "AudioRemuxer has stopped");
			mAudioRemuxerLock.unlock();
			return;
		}
		
		Native_Stop(isCancle);
	
		isStarted = false;
		isPaused = false;
		
		mAudioRemuxerLock.unlock();
	}
	
	private boolean isReleased = false;
	private boolean isFinishAllCallbacksAndMessages = false;
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2) public void release()
	{
		stop(false);
		
		mAudioRemuxerLock.lock();
		if(isReleased)
		{
			Log.w(TAG, "AudioRemuxer has released!!");
			mAudioRemuxerLock.unlock();
			return;
		}
		
		audioRemuxerCallbackHandler.post(new Runnable() {
			@Override
			public void run() {
				audioRemuxerCallbackHandler.removeCallbacksAndMessages(null);
				
				mAudioRemuxerLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mAudioRemuxerCondition.signalAll();
				mAudioRemuxerLock.unlock();
			}
		});
		
		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mAudioRemuxerCondition.await(10, TimeUnit.MILLISECONDS);
			}
			
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
		}
		
		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR2)
		{
			mHandlerThread.quitSafely();
		}else{
			mHandlerThread.quit();
		}
		
		isReleased = true;
		
		mAudioRemuxerLock.unlock();
		
		Log.d(TAG, "Finish AudioRemuxer Release");
	}
	
	//audio_remuxer_event_type
	public final static int CALLBACK_AUDIO_REMUXER_STREAMING = 0;
	public final static int CALLBACK_AUDIO_REMUXER_ERROR = 1;
	public final static int CALLBACK_AUDIO_REMUXER_INFO = 2;
	public final static int CALLBACK_AUDIO_REMUXER_END = 3;

	//audio_remuxer_error_type
	public final static int CALLBACK_AUDIO_REMUXER_ERROR_UNKNOWN = -1;
	public final static int CALLBACK_AUDIO_REMUXER_ERROR_NO_INPUT_MATERIAL = 1;
	public final static int CALLBACK_AUDIO_REMUXER_ERROR_OPEN_INPUT_MATERIAL_FAIL = 2;
	public final static int CALLBACK_AUDIO_REMUXER_ERROR_DEMUX_FAIL = 3;
	public final static int CALLBACK_AUDIO_REMUXER_ERROR_MUX_FAIL = 4;
}
