package android.slkmedia.mediaeditengine;

public interface DubbingScoreListener {
	public abstract void onDubbingScoreStart();
	public abstract void onDubbingScoreError(int errorType);
	public abstract void onDubbingScoreInfo(int infoType, int infoValue);
	public abstract void onDubbingScoreEnd(float scoreValue);
}
