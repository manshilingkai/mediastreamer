package android.slkmedia.mediaeditengine;

public class AudioRemuxerOptions {
	public String inputUrl = null;
	public int startPosMs = 0;
	public int endPosMs = 0;
	public boolean isRemuxAll = true;
	public String outputUrl = null;
}
