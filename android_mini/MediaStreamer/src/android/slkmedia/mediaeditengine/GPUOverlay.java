package android.slkmedia.mediaeditengine;

public class GPUOverlay {
	private static final String TAG = "GPUOverlay";
	
	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}

	//Native Context
	private long mNativeContext = 0;
	
	private static native final void Native_Init();
	private native final void Native_Setup();
	private native final void Native_Finalize();
	
	public GPUOverlay()
	{
		isInited = false;
		mImageUrlForInit = null;
	}
	
	private boolean isInited = false;
	public void init() {
		if(isInited) return;
		
		Native_Setup();
		isInited = true;
		
		if(mImageUrlForInit!=null)
		{
			setOverlay(mImageUrlForInit);
		}
	}

    public void destroy() {
    	if(!isInited) return;
    	
    	Native_Finalize();
    	isInited = false;
    }
    
    private String mImageUrlForInit = null;
	public void setOverlay(String imageUrl)
	{
		if(isInited)
		{
			Native_SetOverlay(imageUrl);
		}else{
			mImageUrlForInit = imageUrl;
		}
	}
	private native void Native_SetOverlay(String imageUrl);
	
	public int blendTo(int mainTextureId, int mainWidth, int mainHeight, int x, int y)
	{
		if(isInited)
		{
			return Native_BlendTo(mainTextureId, mainWidth, mainHeight, x, y, 0, 1.0f, false, false);
		}else return mainTextureId;
	}
	
	public int blendTo(int mainTextureId, int mainWidth, int mainHeight, int x, int y, int overlayRotation, float overlayScale, boolean overlayFlipHorizontal, boolean overlayFlipVertical)
	{
		if(isInited)
		{
			return Native_BlendTo(mainTextureId, mainWidth, mainHeight, x, y, overlayRotation, overlayScale, overlayFlipHorizontal, overlayFlipVertical);
		}else return mainTextureId;
	}
	private native int Native_BlendTo(int mainTextureId, int mainWidth, int mainHeight, int x, int y, int overlayRotation, float overlayScale, boolean overlayFlipHorizontal, boolean overlayFlipVertical);
}
