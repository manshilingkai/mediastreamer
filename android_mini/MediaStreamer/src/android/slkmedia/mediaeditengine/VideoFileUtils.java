package android.slkmedia.mediaeditengine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import android.graphics.Bitmap;
import android.media.Image;
import android.media.MediaMetadataRetriever;
import android.util.Log;

public class VideoFileUtils {
	private static final String TAG = "VideoFileUtils";

	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");
	}
	
	static public class MediaInfo {
		public int videoWidth = 0;
		public int videoHeight = 0;
		public int videoRotation = 0;
	}
	
	public static MediaInfo getMediaInfo(String videoFilePath)
	{
		String widthStr = null;
		String heightStr = null;
		String rotationStr = null;
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		try{
			mmr.setDataSource(videoFilePath);
			widthStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
			heightStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
			rotationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
		}catch (Exception ex) {
			
		}finally {
			mmr.release();
		}
		
		if(widthStr==null) return null;
		int width = Integer.parseInt(widthStr);
		if(width<=0) return null;
		
		if(heightStr==null) return null;
		int height = Integer.parseInt(heightStr);
		if(height<=0) return null;
		
		int rotation = 0;
		if(rotationStr==null)
		{
			rotation = 0;
		}else{
			rotation = Integer.parseInt(rotationStr);
			if(rotation!=0 && rotation!=90 && rotation!=180 && rotation!=270)
			{
				rotation = 0;
			}
		}
		
		MediaInfo mediaInfo = new MediaInfo();
		mediaInfo.videoWidth = width;
		mediaInfo.videoHeight = height;
		mediaInfo.videoRotation = rotation;
		
		return mediaInfo;
	}
	
	public static long getVideoFileDurationMs(String videoFilePath)
	{
		String duration=null;
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		try{
			mmr.setDataSource(videoFilePath);
			duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
		}catch (Exception ex) {
			
		}finally {
			mmr.release();
		}
		
		if(duration!=null)
		{
			Log.d(TAG, videoFilePath + " [ " + duration + " ] ");
			return Long.parseLong(duration);
		}else{
			return 0;
		}
	}
	
	public static long getVideoFileSizeKB(String videoFilePath)
	{
		File f = new File(videoFilePath);
		if (f.exists() && f.isFile()) {  
		    return f.length()/1024;
		} else {
			Log.e(TAG, "vide file does not exist or is not a file");
		    return 0;
		}
	}
	
	public static boolean getThumbnailImage(String videoFilePath, int videoTimeMs, boolean isAccurate, String outputPngFilePath)
	{
		boolean ret = false;
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();

		try{
			mmr.setDataSource(videoFilePath);
			int option = MediaMetadataRetriever.OPTION_CLOSEST;
			if(isAccurate)
			{
				option = MediaMetadataRetriever.OPTION_CLOSEST;
			}else{
				option = MediaMetadataRetriever.OPTION_CLOSEST_SYNC;
			}
			Bitmap thumbnailBitmap = mmr.getFrameAtTime(videoTimeMs*1000, option);
			if(thumbnailBitmap!=null)
			{
		        try {
		            FileOutputStream out = new FileOutputStream(outputPngFilePath);
		            thumbnailBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
		            out.flush();
		            out.close();
		            ret = true;
		        } catch (FileNotFoundException e) {
		        } catch (IOException e) {
		        }
			}
		}catch (Exception ex) {
			
		}finally {
			mmr.release();
		}
		
		return ret;
	}
	
	public static boolean getScaledThumbnailImage(String videoFilePath, int videoTimeMs, boolean isAccurate, String outputPngFilePath, int dstWidth, int dstHeight)
	{
		boolean ret = false;
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();

		try{
			mmr.setDataSource(videoFilePath);
			int option = MediaMetadataRetriever.OPTION_CLOSEST;
			if(isAccurate)
			{
				option = MediaMetadataRetriever.OPTION_CLOSEST;
			}else{
				option = MediaMetadataRetriever.OPTION_CLOSEST_SYNC;
			}
			
//			if(Build.VERSION.SDK_INT>=27)
//			{
//				Bitmap thumbnailBitmap = mmr.getScaledFrameAtTime(videoTimeMs*1000, option, dstWidth, dstHeight);
//				if(thumbnailBitmap!=null)
//				{
//			        try {
//			            FileOutputStream out = new FileOutputStream(outputPngFilePath);
//			            thumbnailBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
//			            out.flush();
//			            out.close();
//			            ret = true;
//			        } catch (FileNotFoundException e) {
//			        } catch (IOException e) {
//			        }
//				}
//			}else{
//				Bitmap thumbnailBitmap = mmr.getFrameAtTime(videoTimeMs*1000, option);
//				ret = BitmapToPng(thumbnailBitmap, outputPngFilePath, dstWidth, dstHeight);
//			}
			
			Bitmap thumbnailBitmap = mmr.getFrameAtTime(videoTimeMs*1000, option);
			if(thumbnailBitmap!=null)
			{
				ret = BitmapToPng(thumbnailBitmap, outputPngFilePath, dstWidth, dstHeight);
			}
		}catch (Exception ex) {
			
		}finally {
			mmr.release();
		}
		
		return ret;
	}
	
	public static native boolean BitmapToPng(Bitmap inBitmap, String outputPngFilePath, int dstWidth, int dstHeight);
	public static native boolean WriteI420ImageToDisk(ByteBuffer y_buffer, int y_rowStride, ByteBuffer u_buffer, int u_rowStride, ByteBuffer v_buffer, int v_rowStride, int width, int height, String outputPngFilePath, int dstWidth, int dstHeight, int rotation);
	public static native boolean WriteNV12ImageToDisk(ByteBuffer y_buffer, int y_rowStride, ByteBuffer uv_buffer, int uv_rowStride, int width, int height, String outputPngFilePath, int dstWidth, int dstHeight, int rotation);
	public static native boolean WriteYUV420PImageToDisk(byte[] data, int width, int height, String outputPngFilePath, int dstWidth, int dstHeight, int rotation);
	
	//-1 : UnKnown Media File
	// 0 : Audio File
	// 1 : Video File
	public static native int GetMediaFileType(String mediaFilePath);
}
