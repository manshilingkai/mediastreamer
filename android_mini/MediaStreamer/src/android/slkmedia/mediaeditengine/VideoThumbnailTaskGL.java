package android.slkmedia.mediaeditengine;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.opengl.GLES20;
import android.os.Build;
import android.slkmedia.mediastreamer.egl.EGL;
import android.slkmedia.mediastreamer.gpuimage.GPUImageAmaroFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageAntiqueFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBeautyFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBlackCatFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBrannanFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBrooklynFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageCoolFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageCrayonFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageInputFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageN1977Filter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageRGBFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageRotationMode;
import android.slkmedia.mediastreamer.gpuimage.GPUImageSketchFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageUtils;
import android.slkmedia.mediastreamer.gpuimage.OpenGLUtils;
import android.slkmedia.mediastreamer.gpuimage.TextureRotationUtil;
import android.slkmedia.mediastreamer.utils.FileUtils;
import android.slkmedia.mediastreamer.utils.FolderUtils;
import android.util.Log;
import android.view.Surface;

public class VideoThumbnailTaskGL {
	private static final String TAG = "VideoThumbnailTaskGL";

	private String mVideoFilePath = null;
	private int mPieces = 0;
	private String mOutputPngFileDir = null;
	private int mDstWidth = 0;
	private int mDstHeight = 0;
	public VideoThumbnailTaskGL(String videoFilePath, int pieces, String outputPngFileDir, int dstWidth, int dstHeight)
	{
		mVideoFilePath = videoFilePath;
		mPieces = pieces;
		mOutputPngFileDir = outputPngFileDir;
		mDstWidth = dstWidth;
		mDstHeight = dstHeight;
	}
	
	public VideoThumbnailTaskGL(String videoFilePath, int pieces, String outputPngFileDir)
	{
		mVideoFilePath = videoFilePath;
		mPieces = pieces;
		mOutputPngFileDir = outputPngFileDir;
		mDstWidth = 0;
		mDstHeight = 0;
	}
	
	private long mTimeMsForSinglePiece = 0;
	public VideoThumbnailTaskGL(String videoFilePath, String outputPngFileDir, long timeMs, int dstWidth, int dstHeight)
	{
		mVideoFilePath = videoFilePath;
		mPieces = 1;
		mTimeMsForSinglePiece = timeMs;
		mOutputPngFileDir = outputPngFileDir;
		mDstWidth = dstWidth;
		mDstHeight = dstHeight;
	}
	
	public VideoThumbnailTaskGL(String videoFilePath, String outputPngFileDir, long timeMs)
	{
		mVideoFilePath = videoFilePath;
		mPieces = 1;
		mTimeMsForSinglePiece = timeMs;
		mOutputPngFileDir = outputPngFileDir;
		mDstWidth = 0;
		mDstHeight = 0;
	}
	
	private VideoThumbnailTaskListener mListener = null;
	public void setVideoThumbnailTaskListenr(VideoThumbnailTaskListener listener)
	{
		mListener = listener;
	}
	
	private boolean isWorking = false;
	public void startWork()
	{
		if(isWorking) return;
		createWorkThread();
		isWorking = true;
	}
	
	public void release()
	{
		if(!isWorking) return;
		deleteWorkThread();
		isWorking = false;
	}
	
	private Lock mLock = null;
	private Condition mCondition = null;
	private Thread mWorkThread = null;
	private boolean isBreakWorkThread = false;
	private boolean isRenderReady = false;
	private void createWorkThread()
	{
		mLock = new ReentrantLock(); 
		mCondition = mLock.newCondition();
		
		mWorkThread = new Thread(mWorkRunnable);
		mWorkThread.start();
	}
	
	private Runnable mWorkRunnable = new Runnable()
	{
		@Override
		public void run() {
			if(!FolderUtils.isFolderExists(mOutputPngFileDir))
			{
				Log.e(TAG, "Output Png File Dir is Not Exist");
				return;
			}
			int ret = privateRun();
			if(ret>=0 || ret==-100) return;
			else{
				systemRun();
			}
		}
	};
	
	private void deleteWorkThread()
	{
		mLock.lock();
		isBreakWorkThread = true;
		mCondition.signalAll();
		mLock.unlock();
		
		try {
			mWorkThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		Log.d(TAG, "Finish deleteWorkThread");
	}
	
	///////////////////////////////////////////////////////////// GPUImage[OpenGLES] //////////////////////////////////////////////////////////////
	private FloatBuffer mGLCubeBuffer;
	private FloatBuffer mGLTextureBuffer;
    private float[] rotatedTex;
	
	private GPUImageInputFilter mInputFilter = null;
	private GPUImageFilter mWorkFilter = null;
	
    private void initGPUImageParam()
    {
		mGLCubeBuffer = ByteBuffer.allocateDirect(TextureRotationUtil.CUBE.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLCubeBuffer.put(TextureRotationUtil.CUBE).position(0);

        rotatedTex = new float[8];
        TextureRotationUtil.calculateCropTextureCoordinates(GPUImageRotationMode.kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        
        mGLTextureBuffer = ByteBuffer.allocateDirect(8 * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLTextureBuffer.put(rotatedTex).position(0);
    	
        mInputFilter = new GPUImageInputFilter();
        mWorkFilter = new GPUImageRGBFilter();
    }
    
	private Surface mInputSurface = null;
	private SurfaceTexture mInputSurfaceTexture = null;
	private int mInputTextureId = OpenGLUtils.NO_TEXTURE;
	private void openGPUImageWorker(int inputVideoWidth, int inputVideoHeight)
	{
    	if(mInputTextureId == OpenGLUtils.NO_TEXTURE) {
    		mInputTextureId = OpenGLUtils.getExternalOESTextureID();
    		mInputSurfaceTexture = new SurfaceTexture(mInputTextureId);
    		mInputSurfaceTexture.setDefaultBufferSize(inputVideoWidth, inputVideoHeight);
    		mInputSurfaceTexture.setOnFrameAvailableListener(new OnFrameAvailableListener(){
				@Override
				public void onFrameAvailable(SurfaceTexture surfaceTexture) {
//					Log.d(TAG, "On Handle Input Video Frame");
					
					mRenderLock.lock();
					Msg msg = new Msg();
					mMsgQueue.add(msg);
					mRenderCondition.signal();
					mRenderLock.unlock();
				}
			});
    		mInputSurface = new Surface(mInputSurfaceTexture); //mInputSurface.release();
    	}
		
    	mInputFilter.init();
        mWorkFilter.init();
        isOutputSizeUpdated = true;
	}
	
	private boolean isGPUImageWorkerOpened = false;
	private void closeGPUImageWorker()
	{
	    if(mInputTextureId != OpenGLUtils.NO_TEXTURE)
	    {
	    	GLES20.glDeleteTextures(1, new int[]{mInputTextureId}, 0);
	    	mInputTextureId = OpenGLUtils.NO_TEXTURE;
	    }
	    
	    if(mInputSurfaceTexture!=null)
	    {
	    	mInputSurfaceTexture.release();
	    	mInputSurfaceTexture = null;
	    }
	    
	    if(mInputSurface!=null)
	    {
	    	mInputSurface.release();
	    	mInputSurface = null;
	    }
		
    	mInputFilter.destroy();
    	
    	mWorkFilter.destroy();
        isOutputSizeUpdated = false;
        
        destroyFrameBufferObject();
	}
	
	private void switchFilter(int type, String filterDir)
	{
    	if(mWorkFilter!=null)
    	{
    		mWorkFilter.destroy();
    		mWorkFilter = null;
    	}
    	
        switch (type) {
        case GPUImageUtils.FILTER_SKETCH:
        	mWorkFilter = new GPUImageSketchFilter();
        	mWorkFilter.init();
            break;
        case GPUImageUtils.FILTER_AMARO:
        	mWorkFilter = new GPUImageAmaroFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_ANTIQUE:
        	mWorkFilter = new GPUImageAntiqueFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BEAUTY:
        	mWorkFilter = new GPUImageBeautyFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BLACKCAT:
        	mWorkFilter = new GPUImageBlackCatFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BRANNAN:
        	mWorkFilter = new GPUImageBrannanFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BROOKLYN:
        	mWorkFilter = new GPUImageBrooklynFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_COOL:
        	mWorkFilter = new GPUImageCoolFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_CRAYON:
        	mWorkFilter = new GPUImageCrayonFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_N1977:
        	mWorkFilter = new GPUImageN1977Filter(filterDir);
        	mWorkFilter.init();
        	break;
        default:
            mWorkFilter = new GPUImageRGBFilter();
            mWorkFilter.init();
            break;
        }
        isOutputSizeUpdated = true;
	}
	
	private int outputWidth = -1;
	private int outputHeight = -1;
	private boolean isOutputSizeUpdated = false;
	private void ScaleToFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
	{
	    int src_width;
	    int src_height;
	    
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
	    {
	        src_width = videoHeight;
	        src_height = videoWidth;
	    }else{
	        src_width = videoWidth;
	        src_height = videoHeight;
	    }
	    
	    videoWidth = src_width;
	    videoHeight = src_height;
	    
	    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
	        outputWidth = videoWidth;
	        outputHeight = videoHeight;
	        
	        isOutputSizeUpdated = true;
	    }
	    
	    if (isOutputSizeUpdated) {
	        isOutputSizeUpdated = false;
	        mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
	    }
	    	    
	    GLES20.glClearColor(0.f, 0.f, 0.f, 0.f);
	    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	    
	    GLES20.glViewport(displayX, displayY, displayWidth, displayHeight);
	    
	    TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
	    mGLTextureBuffer.put(rotatedTex).position(0);
	}
	
    private void ScaleAspectFit(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
        int x = 0;
        int y = 0;
        int w = 0;
        int h = 0;
        
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        videoWidth = src_width;
        videoHeight = src_height;
        
        if(displayWidth*videoHeight>videoWidth*displayHeight)
        {
            int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
            int viewPortVideoHeight = displayHeight;
            
            x = displayX+(displayWidth - viewPortVideoWidth)/2;
            y = displayY;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }else
        {
            int viewPortVideoWidth = displayWidth;
            int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
            
            x = displayX;
            y = displayY+(displayHeight - viewPortVideoHeight)/2;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }
        
        if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
            outputWidth = videoWidth;
            outputHeight = videoHeight;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
                
        GLES20.glViewport(x, y, w, h);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
    
    private void ScaleAspectFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        int dst_width = displayWidth;
        int dst_height = displayHeight;
        
        int crop_x;
        int crop_y;
        
        int crop_width;
        int crop_height;
        
        if(src_width*dst_height>dst_width*src_height)
        {
            crop_width = dst_width*src_height/dst_height;
            crop_height = src_height;
            
            crop_x = (src_width - crop_width)/2;
            crop_y = 0;

        }else if(src_width*dst_height<dst_width*src_height)
        {
            crop_width = src_width;
            crop_height = dst_height*src_width/dst_width;
            
            crop_x = 0;
            crop_y = (src_height - crop_height)/2;
        }else {
            crop_width = src_width;
            crop_height = src_height;
            crop_x = 0;
            crop_y = 0;
        }
        
        float minX = (float)crop_x/(float)src_width;
        float minY = (float)crop_y/(float)src_height;
        float maxX = 1.0f - minX;
        float maxY = 1.0f - minY;
        
        if (outputWidth!=crop_width || outputHeight!=crop_height) {
            outputWidth = crop_width;
            outputHeight = crop_height;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(displayX, displayY, displayWidth, displayHeight);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
    
	private float[] mTransformMatrix = new float[16];
	private int pieceIndex = 0;
	private boolean onDrawFrame(int inputVideoWidth, int inputVideoHeight, int displayX, int displayY, int displayWidth, int displayHeight, int videoScalingModeForWorkFilter, GPUImageRotationMode rotationModeForWorkFilter) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
		if(mInputTextureId == OpenGLUtils.NO_TEXTURE || mInputSurfaceTexture==null) return false;
		if(inputVideoWidth<=0 || inputVideoHeight<=0) return false;
		if(displayWidth<=0 || displayHeight<=0) return false;
		
		mInputSurfaceTexture.updateTexImage();
		mInputSurfaceTexture.getTransformMatrix(mTransformMatrix);
		mInputFilter.setTextureTransformMatrix(mTransformMatrix);
        int inputTextureID = mInputFilter.onDrawToTexture(mInputTextureId, inputVideoWidth, inputVideoHeight);
        if(inputTextureID == OpenGLUtils.NO_TEXTURE) return false;
        
        initFrameBufferObject(displayWidth, displayHeight);
        
        if(videoScalingModeForWorkFilter == GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT)
        {
        	ScaleAspectFit(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, inputVideoWidth, inputVideoHeight);
        }else if(videoScalingModeForWorkFilter == GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)
        {
        	ScaleAspectFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, inputVideoWidth, inputVideoHeight);
        }else{
        	ScaleToFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, inputVideoWidth, inputVideoHeight);
        }
		
    	GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
    	
		mWorkFilter.onDrawFrame(inputTextureID, mGLCubeBuffer, mGLTextureBuffer);

		if(pieceIndex<mPieces)
		{
            String writePath = mOutputPngFileDir+"/"+pieceIndex+".png";
	    	boolean ret = outputPixelBufferFromFBOToPngFile(writePath);
	    	if(ret)
	    	{
            	if(mListener!=null)
            	{
            		mListener.onVideoThumbnail(pieceIndex, mPieces, writePath);
            	}
            	
	    		pieceIndex++;
	    	}
		}
		
		if(pieceIndex>=mPieces)
		{
			mLock.lock();
			isBreakWorkThread = true;
			mCondition.signalAll();
			mLock.unlock();
		}
    	
    	GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
		
		return true;
	}
	
	////////////////////////////////////////////-- FBO --///////////////////////////////////////////////////////////////
    private static int[] mFrameBuffers = null;
    private static int[] mFrameBufferTextures = null;
    private int mFrameBufferWidth = -1;
    private int mFrameBufferHeight = -1;
    private ByteBuffer mPixelBuffer = null;
	private void initFrameBufferObject(int frameBufferWidth, int frameBufferHeight) {
		if(mFrameBuffers != null && (mFrameBufferWidth != frameBufferWidth || mFrameBufferHeight != frameBufferHeight))
			destroyFrameBufferObject();
        if (mFrameBuffers == null) {
        	mFrameBufferWidth = frameBufferWidth;
        	mFrameBufferHeight = frameBufferHeight;
        	mFrameBuffers = new int[1];
            mFrameBufferTextures = new int[1];

            GLES20.glGenFramebuffers(1, mFrameBuffers, 0);
            
            GLES20.glGenTextures(1, mFrameBufferTextures, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mFrameBufferTextures[0]);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, frameBufferWidth, frameBufferHeight, 0,
                    GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
            GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                    GLES20.GL_TEXTURE_2D, mFrameBufferTextures[0], 0);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
            
            mPixelBuffer = ByteBuffer.allocate(mFrameBufferWidth * mFrameBufferHeight * 4);
        }
	}
	
	private void destroyFrameBufferObject() {
        if (mFrameBufferTextures != null) {
            GLES20.glDeleteTextures(1, mFrameBufferTextures, 0);
            mFrameBufferTextures = null;
        }
        if (mFrameBuffers != null) {
            GLES20.glDeleteFramebuffers(1, mFrameBuffers, 0);
            mFrameBuffers = null;
        }
        mFrameBufferWidth = -1;
        mFrameBufferHeight = -1;
        
        if(mPixelBuffer!=null)
        {
            mPixelBuffer.clear();
            mPixelBuffer = null;
        }
    }
	
	private boolean outputPixelBufferFromFBOToPngFile(String shotPath)
	{
		if(mPixelBuffer!=null)
		{
			mPixelBuffer.clear();
			mPixelBuffer.order(ByteOrder.LITTLE_ENDIAN);
			mPixelBuffer.rewind();

	        GLES20.glReadPixels(0, 0, mFrameBufferWidth, mFrameBufferHeight, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, mPixelBuffer);
	        
			Bitmap bitmap = Bitmap.createBitmap(mFrameBufferWidth, mFrameBufferHeight, Bitmap.Config.ARGB_8888);
			bitmap.copyPixelsFromBuffer(mPixelBuffer);
			boolean ret = saveBitmapToDisk(bitmap, shotPath);
			return ret;
		}else {
			return false;
		}
	}
	
	private boolean saveBitmapToDisk(Bitmap inBitmap, String outputFile)
	{
        try {
            FileOutputStream fout = new FileOutputStream(outputFile);
            inBitmap.compress(Bitmap.CompressFormat.PNG, 100, fout);
            fout.flush();
            fout.close();

            inBitmap.recycle();
            
            return true;

        } catch (IOException e) {
            return false;
        }
	}
	
	//////////////////////////////////////////////////////////////// Video Render /////////////////////////////////////
	private Lock mRenderLock = null;
	private Condition mRenderCondition = null;
	private Thread mRenderThread = null;
	private boolean isBreakRenderThread = false;
	private int mInputVideoWidth = 0;
	private int mInputVideoHeight = 0;
	private void createRenderThread(int inputVideoWidth, int inputVideoHeight)
	{
		mInputVideoWidth = inputVideoWidth;
		mInputVideoHeight = inputVideoHeight;
		
		mRenderLock = new ReentrantLock(); 
		mRenderCondition = mRenderLock.newCondition();
		
		isBreakRenderThread = false;
		mRenderThread = new Thread(mRenderRunnable);
		mRenderThread.start();
	}
	
	private EGL mEGL = null;
	private Runnable mRenderRunnable = new Runnable() {
		@Override
		public void run() {
	    	//Create EGL
			if(mEGL==null)
			{
				mEGL = new EGL();
				mEGL.Egl_Initialize(null, true);
				
		        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
			}
			mEGL.Egl_AttachToOffScreenSurface(mDstWidth, mDstHeight);
			
			initGPUImageParam();
			
			if(!isGPUImageWorkerOpened)
			{
				openGPUImageWorker(mInputVideoWidth,mInputVideoHeight);
				isGPUImageWorkerOpened = true;
			}
			
			mLock.lock();
			isRenderReady = true;
			mCondition.signalAll();
			mLock.unlock();
			
			while(true)
			{
				mRenderLock.lock();
				if(isBreakRenderThread)
				{
					mRenderLock.unlock();
					break;
				}
				
				while(!mMsgQueue.isEmpty())
				{
					mMsgQueue.removeFirst();
					
					onDrawFrame(mInputVideoWidth, mInputVideoHeight, 0, 0, mDstWidth, mDstHeight, GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING, GPUImageRotationMode.kGPUImageFlipVertical);
					mEGL.Egl_SwapBuffers();
				}
				
				try {
					mRenderCondition.await(50, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
								
				mRenderLock.unlock();
				continue;
			}
						
			if(isGPUImageWorkerOpened)
			{
				closeGPUImageWorker();
				isGPUImageWorkerOpened = false;
			}
			
			// Release EGL
			mEGL.Egl_DetachFromSurfaceTexture();
			mEGL.Egl_Terminate();
			mEGL = null;
		}
	};

	private void deleteRenderThread()
	{
		mRenderLock.lock();
		isBreakRenderThread = true;
		mRenderCondition.signal();
		mRenderLock.unlock();
		
		try {
			mRenderThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	static public class Msg
	{
		public int arg1 = 0;
		public int arg2 = 0;
	}
	private LinkedList<Msg> mMsgQueue = new LinkedList<Msg>();
	
	//////////////////////////////////////////////////////////////// Main /////////////////////////////////////////////
	
	static public class Piece
	{
		public long timeMs = 0;
		public boolean isNeedSeek = false;
	}
	// -1 : Fail
	// -100 : Exit
	private int privateRun()
	{
		if(Build.VERSION.SDK_INT<19)
		{
			return -1;
		}
		
		boolean isExit = false;
		
		LinkedList<Piece> pieceList = new LinkedList<Piece>();
		
		MediaExtractor extractor = new MediaExtractor();
        try {
			extractor.setDataSource(mVideoFilePath);
		} catch (IOException e) {
			extractor.release();
			extractor = null;
			return -1;
		}
        
        int trackCount = extractor.getTrackCount();
        MediaFormat videoFormat = null;
        for (int i = 0; i < trackCount; i++) {
            MediaFormat trackFormat = extractor.getTrackFormat(i);
            if (trackFormat.getString(MediaFormat.KEY_MIME).contains("video")) {
                videoFormat = trackFormat;
                extractor.selectTrack(i);
                break;
            }
        }
        
        if(videoFormat==null)
        {
        	extractor.release();
        	extractor = null;
        	return -1;
        }
        
        long durationMs = 0;
        if(videoFormat.containsKey(MediaFormat.KEY_DURATION))
        {
        	try {
        		durationMs = videoFormat.getLong(MediaFormat.KEY_DURATION)/1000;
        	}catch (java.lang.NullPointerException e){
        		
        	}
        }
        
        int rotation = 0;
        if(videoFormat.containsKey(MediaFormat.KEY_ROTATION)){
             rotation = videoFormat.getInteger(MediaFormat.KEY_ROTATION);
        }
        if (rotation<0) {
        	rotation = 360 + rotation;
        }
        
        if(mPieces>1 && durationMs>0)
        {
        	long intervalMs = durationMs/mPieces;
        	boolean isInSameGopWithLastSample = false;
        	int pieceIndex = 0;
        	long sampleTimeBaseLine = extractor.getSampleTime();
        	while(pieceIndex<mPieces)
        	{
        		mLock.lock();
        		if(isBreakWorkThread)
        		{
        			isExit = true;
        			mLock.unlock();
        			break;
        		}
        		mLock.unlock();
        		
        		long sampleTime = extractor.getSampleTime();
        		boolean isKeyFrame = false;
        		try {
        			isKeyFrame = (extractor.getSampleFlags() & MediaExtractor.SAMPLE_FLAG_SYNC) != 0;
        		}catch(java.lang.IllegalArgumentException e) {
        			isKeyFrame = false;
        			isInSameGopWithLastSample = false;
        		}
        		if(isKeyFrame)
        		{
//        			Log.d(TAG, "Got KeyFrame");
        			isInSameGopWithLastSample = false;
        		}
        		if(sampleTime==-1)
        		{
        			//eof
        	    	break;
        		}else{
        			if((sampleTime-sampleTimeBaseLine)/1000>=pieceIndex*intervalMs)
        			{
            	    	Piece piece = new Piece();
            	    	piece.timeMs = pieceIndex*intervalMs;
//            			Log.d(TAG, "isInSameGopWithLastSample : " + String.valueOf(isInSameGopWithLastSample));
            	    	piece.isNeedSeek = !isInSameGopWithLastSample;
            	    	pieceList.add(piece);
            	    	
            	    	isInSameGopWithLastSample = true;
            	    	pieceIndex++;
        			}
        			
        			extractor.advance();
        		}
        	}
        }else{
        	if(mPieces==1 && mTimeMsForSinglePiece>0 && durationMs>0)
        	{
    	    	Piece piece = new Piece();
    	    	piece.timeMs = mTimeMsForSinglePiece;
    	    	piece.isNeedSeek = true;
    	    	pieceList.add(piece);
        	}else{
    	    	Piece piece = new Piece();
    	    	piece.timeMs = 0;
    	    	piece.isNeedSeek = false;
    	    	pieceList.add(piece);
        	}
        }
        
        if(isExit)
        {
        	extractor.release();
        	extractor = null;
        	pieceList.clear();
        	
        	return -100;
        }
        
        if(Build.VERSION.SDK_INT>=21)
        {
            int colorFormat = MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Flexible;
            videoFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, colorFormat);
        }

        videoFormat.setInteger(MediaFormat.KEY_WIDTH, videoFormat.getInteger(MediaFormat.KEY_WIDTH));
        videoFormat.setInteger(MediaFormat.KEY_HEIGHT, videoFormat.getInteger(MediaFormat.KEY_HEIGHT));
        MediaCodec mediaCodec = null;
        try {
			mediaCodec = MediaCodec.createDecoderByType(videoFormat.getString(MediaFormat.KEY_MIME));
		} catch (IOException e) {
        	extractor.release();
        	extractor = null;
        	pieceList.clear();
        	return -1;
		}
        
        int inputVideoWidth = videoFormat.getInteger(MediaFormat.KEY_WIDTH);
        int inputVideoHeight = videoFormat.getInteger(MediaFormat.KEY_HEIGHT);
        if(rotation == 90 || rotation == 270)
        {
        	inputVideoWidth = videoFormat.getInteger(MediaFormat.KEY_HEIGHT);
        	inputVideoHeight = videoFormat.getInteger(MediaFormat.KEY_WIDTH);
        }
        
        if(mDstWidth==0 || mDstHeight==0)
        {
        	mDstWidth = inputVideoWidth;
        	mDstHeight = inputVideoHeight;
        }
        
        createRenderThread(inputVideoWidth, inputVideoHeight);

    	mLock.lock();
		try {
			while(!isBreakWorkThread && !isRenderReady)
			{
				mCondition.await(10, TimeUnit.MILLISECONDS);
			}
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
		}
		
		if(isBreakWorkThread)
		{
			isExit = true;
		}
		mLock.unlock();
		
		if(isExit)
		{
            if (mediaCodec != null) {
            	mediaCodec.release();
            	mediaCodec = null;
            }
          
            if (extractor != null) {
            	extractor.release();
            	extractor = null;
            }
            
            deleteRenderThread();
            
            pieceList.clear();
        	
        	return -100;
		}
        
        try {
        	mediaCodec.configure(videoFormat, mInputSurface, null, 0);
        	mediaCodec.start();
        }catch (android.media.MediaCodec.CodecException e) {
            if (mediaCodec != null) {
            	mediaCodec.release();
            	mediaCodec = null;
            }
          
            if (extractor != null) {
            	extractor.release();
            	extractor = null;
            }
            
            deleteRenderThread();
            
            pieceList.clear();
        	
        	return -1;
        }
        
        
        MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
        long timeOut = 5 * 1000; //5ms
        boolean inputDone = false;
        boolean outputDone = false;
        ByteBuffer[] inputBuffers = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            inputBuffers = mediaCodec.getInputBuffers();
        }
        
        Piece currentPiece = pieceList.removeFirst();
        if(currentPiece.isNeedSeek)
        {
        	extractor.seekTo(currentPiece.timeMs*1000, MediaExtractor.SEEK_TO_PREVIOUS_SYNC);
        }
        boolean gotPresentationTimeUsBaseLine = false;
        long presentationTimeUsBaseLine = 0;
        while(!outputDone)
        {
    		mLock.lock();
    		if(isBreakWorkThread)
    		{
    			isExit = true;
    			mLock.unlock();
    			break;
    		}
    		mLock.unlock();
        	
        	if(!inputDone)
        	{
        		//feed data
                int inputBufferIndex = mediaCodec.dequeueInputBuffer(timeOut);
                if (inputBufferIndex >= 0) {
                    ByteBuffer inputBuffer;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        inputBuffer = mediaCodec.getInputBuffer(inputBufferIndex);
                    } else {
                        inputBuffer = inputBuffers[inputBufferIndex];
                    }
                    int sampleSize = extractor.readSampleData(inputBuffer, 0);
                    if(sampleSize>=0)
                    {
                        long sampleTime = extractor.getSampleTime();
                        mediaCodec.queueInputBuffer(inputBufferIndex, 0, sampleSize, sampleTime, 0);
                        extractor.advance();
                    }else{
                    	//eof
                        mediaCodec.queueInputBuffer(inputBufferIndex, 0, 0, 0L, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                        inputDone = true;
                        Log.d(TAG, "input EOS");
                    }
                }
        	}
        	if(!outputDone)
        	{
                //drain data
                int status = mediaCodec.dequeueOutputBuffer(bufferInfo, timeOut);
                if (status == MediaCodec.INFO_TRY_AGAIN_LATER) {
                } else if (status == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                } else if (status == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                } else {
                    if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                        Log.d(TAG, "output EOS");
                        outputDone = true;
                    }
                    boolean doRender = (bufferInfo.size != 0);
                    long presentationTimeUs = bufferInfo.presentationTimeUs;
                    if(!gotPresentationTimeUsBaseLine)
                    {
                    	gotPresentationTimeUsBaseLine = true;
                    	presentationTimeUsBaseLine = presentationTimeUs;
                    }

                	if((presentationTimeUs - presentationTimeUsBaseLine)/1000 >= currentPiece.timeMs)
                	{
                		doRender = true;
                	}else {
                		doRender = false;
                	}
                    
                    mediaCodec.releaseOutputBuffer(status, doRender);
                    
                    if(doRender)
                    {
                		if(pieceList.isEmpty())
                		{
                			outputDone = true;
                			break;
                		}else{
                    		currentPiece = pieceList.removeFirst();
                            if(currentPiece.isNeedSeek)
                            {
                            	extractor.seekTo(currentPiece.timeMs*1000, MediaExtractor.SEEK_TO_PREVIOUS_SYNC);
                            	mediaCodec.flush();
//                            	Log.d(TAG, "currentPiece NeedSeek -> SeekTimeUs : " + String.valueOf(currentPiece.timeMs*1000));
                            }else{
//                            	Log.d(TAG, "currentPiece Just Need Continue Decode");
                            }
                		}
                    }
                }
        	}else break;
        }
        
        if(isExit)
        {
            if (mediaCodec != null) {
            	mediaCodec.stop();
            	mediaCodec.release();
            }
          
            if (extractor != null) {
            	extractor.release();
            }
            
            deleteRenderThread();
            
            pieceList.clear();
        	
        	return -100;
        }
        
    	mLock.lock();
		try {
			while(!isBreakWorkThread)
			{
				mCondition.await(10, TimeUnit.MILLISECONDS);
			}
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
		}
		mLock.unlock();
                
        if (mediaCodec != null) {
        	mediaCodec.stop();
        	mediaCodec.release();
        }
      
        if (extractor != null) {
        	extractor.release();
        }
        
        deleteRenderThread();
        
        pieceList.clear();
        
    	mLock.lock();
    	if(!isBreakWorkThread)
    	{
    		while(pieceIndex<mPieces)
    		{
    			boolean ret = FileUtils.copyNio(mOutputPngFileDir+"/"+(pieceIndex-1)+".png", mOutputPngFileDir+"/"+pieceIndex+".png");
                if(ret)
                {
                	if(mListener!=null)
                	{
                		mListener.onVideoThumbnail(pieceIndex, mPieces, mOutputPngFileDir+"/"+pieceIndex+".png");
                	}
                	
                	pieceIndex++;
                }else break;
    		}
    	}
		mLock.unlock();
	  
        return 0;
	}

	public interface VideoThumbnailTaskListener {
		public abstract void onVideoThumbnail(int index, int all, String thumbnailPng);
	}
	
	public void systemRun() {
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		try{
			mmr.setDataSource(mVideoFilePath);
			long videoDurationMs = 0;
			String duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
			if(duration!=null)
			{
				Log.d(TAG, mVideoFilePath + " [ " + duration + " ] ");
				videoDurationMs = Long.parseLong(duration);
			}
			
			int option = MediaMetadataRetriever.OPTION_CLOSEST;
			for(int i = 0; i<mPieces; i++)
			{
				mLock.lock();
				if(isBreakWorkThread)
				{
					mLock.unlock();
					break;
				}
				mLock.unlock();
				
				long videoTimeMs = (videoDurationMs/mPieces)*i;
				if(mPieces==1 && mTimeMsForSinglePiece>0)
				{
					videoTimeMs = mTimeMsForSinglePiece;
				}
				Bitmap thumbnailBitmap = mmr.getFrameAtTime(videoTimeMs*1000, option);
				if(thumbnailBitmap!=null)
				{
					boolean ret = VideoFileUtils.BitmapToPng(thumbnailBitmap, mOutputPngFileDir+"/"+i+".png", mDstWidth, mDstHeight);
					if(ret)
					{
						if(mListener!=null)
						{
							mListener.onVideoThumbnail(i, mPieces, mOutputPngFileDir+"/"+i+".png");
						}
					}else{
						mmr.release();
						return;
					}
				}else{
					mmr.release();
					return;
				}
			}
			
		}catch (Exception ex) {
		}finally {
			mmr.release();
		}
	}
}
