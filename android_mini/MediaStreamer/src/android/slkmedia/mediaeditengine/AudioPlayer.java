package android.slkmedia.mediaeditengine;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public class AudioPlayer {
	private static final String TAG = "AudioPlayer";
	
	private static final int MIN_DB = 40;
	private static final int MAX_DB = 80;

	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}

	//Native Context
	private long mNativeContext = 0;
	
	private static native final void Native_Init();
	private native final void Native_Setup(Object audioplayer_this);
	private native final void Native_SetupWithMixOptions(Object audioplayer_this, int sampleRate, int channelCount);
	private native final void Native_Finalize();
	
	// PlayState
	public static final int UNKNOWN = -1;
	public static final int IDLE = 0;
	public static final int INITIALIZED = 1;
	public static final int PREPARING = 2;
	public static final int PREPARED = 3;
	public static final int STARTED = 4;
	public static final int STOPPED = 5;
	public static final int PAUSED = 6;
//	public static final int PLAYBACK_COMPLETED = 7;
	public static final int END = 8;
	public static final int ERROR = 9;
	private int currentPlayState = UNKNOWN;
	
	// ////////////////////////////////////////////////////////////////////////////////
	// audio_player_event_type
	private final static int CALLBACK_AUDIO_PLAYER_PREPARED = 0;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR = 1;
	private final static int CALLBACK_AUDIO_PLAYER_INFO = 2;
	private final static int CALLBACK_AUDIO_PLAYER_PLAYBACK_COMPLETE = 3;
	private final static int CALLBACK_AUDIO_PLAYER_SEEK_COMPLETE = 4;
	// audio_player_error_type
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_UNKNOWN = -1;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_NO_MEMORY = 0;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_OPEN_PLAY_URL_FAIL = 1;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_FIND_STREAM_INFO_FAIL = 2;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_NO_AUDIO_STREAM = 3;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_AUDIO_DECODER_NOT_FOUND = 4;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_ALLOCATE_AUDIO_DECODER_CONTEXT_FAIL = 5;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_COPY_AUDIO_STREAM_CODEC_PARAM_TO_AUDIO_DECODER_CONTEXT_FAIL = 6;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_OPEN_AUDIO_DECODER_FAIL = 7;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_OPEN_AUDIO_FILTER_FAIL = 8;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_CREATE_AUDIO_RENDER_FAIL = 9;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_INIT_AUDIO_RENDER_FAIL = 10;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_DEMUXER_READ_FAIL = 11;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_AUDIO_FILTER_INPUT_FAIL = 12;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_AUDIO_FILTER_OUTPUT_FAIL = 13;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR_UPDATE_AUDIO_FILTER_FAIL = 14;
	// audio_player_info_type
	
	/**
	 * Called from native code when an interesting event happens. This method
	 * just uses the EventHandler system to post the event back to the main app
	 * thread. We use a weak reference to the original MediaPlayer object so
	 * that the native code is safe from the object disappearing from underneath
	 * it. (This is the cookie passed to native_setup().)
	 */
	private static void postEventFromNative(Object audioplayer_ref, int what,
			int arg1, int arg2, Object obj) {
		AudioPlayer ap = (AudioPlayer) ((WeakReference<?>) audioplayer_ref).get();
		if (ap == null) {
			return;
		}

		if (ap.audioPlayerCallbackHandler != null) {
			Message msg = ap.audioPlayerCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}
	
	private Handler audioPlayerCallbackHandler = null;

	public AudioPlayer()
	{
		construct(false, 0, 0);
	}
	
	private Context mContext = null;
	private int oldAudioMode = AudioManager.MODE_CURRENT;
	private int oldAudioVolume = 0;
	private AudioPlayerOptions mAudioPlayerOptions = null;
	private HeadSetReceiver mHeadSetReceiver = null;
	public AudioPlayer(Context context, AudioPlayerOptions options)
	{
		mContext = context;
		mAudioPlayerOptions = options;
		
		if(mAudioPlayerOptions!=null && mAudioPlayerOptions.isControlAudioManger && mContext!=null)
		{
		    AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		    oldAudioMode = audioManager.getMode();
		    audioManager.setMode(AudioManager.MODE_NORMAL);
		    oldAudioVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), AudioManager.FLAG_PLAY_SOUND);
		}
		
		construct(false, 0, 0);
		
		if(mAudioPlayerOptions!=null && mAudioPlayerOptions.isDubbingMode && mContext!=null)
		{
		    AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		    if(!audioManager.isWiredHeadsetOn())
		    {
		    	this.setVolume(0.0f);
		    }
		    
	    	mHeadSetReceiver = new HeadSetReceiver(this);
	    	IntentFilter intentFilter = new IntentFilter();
	    	intentFilter.addAction(Intent.ACTION_HEADSET_PLUG);
	    	intentFilter.addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED);
	    	mContext.registerReceiver(mHeadSetReceiver, intentFilter);
		}
	}
	
	public AudioPlayer(boolean isMixPlugin, int sampleRate, int channelCount)
	{
		construct(isMixPlugin, sampleRate, channelCount);
	}
	
	private void construct(boolean isMixPlugin, int sampleRate, int channelCount)
	{
		Looper workLooper = Looper.myLooper();
		if(workLooper==null)
		{
			workLooper = Looper.getMainLooper();
		}
		
		audioPlayerCallbackHandler = new Handler(workLooper) {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case CALLBACK_AUDIO_PLAYER_PREPARED:
					if(msg.arg1==1)
					{
						currentPlayState = STARTED;
					}else{
						currentPlayState = PREPARED;
					}
					if(audioPlayerListener != null)
					{
						audioPlayerListener.onPrepared();
					}
					break;
				case CALLBACK_AUDIO_PLAYER_ERROR:
					currentPlayState = ERROR;
					if(audioPlayerListener != null)
					{
						audioPlayerListener.onError(msg.arg1, msg.arg2);
					}
					break;
				case CALLBACK_AUDIO_PLAYER_INFO:
					if(audioPlayerListener != null)
					{
						audioPlayerListener.onInfo(msg.arg1, msg.arg2);
					}
					break;
				case CALLBACK_AUDIO_PLAYER_PLAYBACK_COMPLETE:
					if(audioPlayerListener !=null)
					{
						audioPlayerListener.onCompletion();
					}
					break;
				case CALLBACK_AUDIO_PLAYER_SEEK_COMPLETE:
					if(audioPlayerListener!=null)
					{
						audioPlayerListener.OnSeekComplete();
					}
					break;
				default:
					break;
				}
			}
		};
		
		if(isMixPlugin)
		{
			Native_SetupWithMixOptions(new WeakReference<AudioPlayer>(this), sampleRate, channelCount);
		}else{
			Native_Setup(new WeakReference<AudioPlayer>(this));
		}
		
		currentPlayState = IDLE;
	}
	
	public int getPlayState()
	{
		return currentPlayState;
	}
	
	private AudioPlayerListener audioPlayerListener = null;
	public void setListener(AudioPlayerListener listener)
	{
		audioPlayerListener = listener;
	}
	
	public void setDataSource(String path) throws IllegalStateException
	{
		if (currentPlayState != IDLE && currentPlayState != STOPPED && currentPlayState != ERROR) {
			
//			throw new IllegalStateException("Error State: " + currentPlayState);
			Log.e(TAG, "Error State: " + currentPlayState);
			return;
		}
		
		Native_SetDataSource(path);
		currentPlayState = INITIALIZED;
	}
	private native void Native_SetDataSource(String path);
	
	public void prepare() throws IllegalStateException
	{
		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
//			throw new IllegalStateException("Error State: " + currentPlayState);
			Log.e(TAG, "Error State: " + currentPlayState);
			return;
		}
		
		currentPlayState = PREPARING;
		Native_Prepare();
		currentPlayState = PREPARED;
	}
	private native void Native_Prepare();
	
	public void prepareAsync() throws IllegalStateException {
		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
//			throw new IllegalStateException("Error State: " + currentPlayState);
			Log.e(TAG, "Error State: " + currentPlayState);
			return;
		}
		
		currentPlayState = PREPARING;
		
		Native_PrepareAsync();
	}
	private native void Native_PrepareAsync();
	
	public void prepareAsyncToPlay() throws IllegalStateException {
		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
//			throw new IllegalStateException("Error State: " + currentPlayState);
			Log.e(TAG, "Error State: " + currentPlayState);
			return;
		}
		
		currentPlayState = PREPARING;
		
		Native_PrepareAsyncToPlay();
	}
	private native void Native_PrepareAsyncToPlay();
	
	public void play() throws IllegalStateException
	{
		if (currentPlayState==STARTED) {
			return;
		} else if (currentPlayState != PREPARED && currentPlayState != PAUSED) {
//			throw new IllegalStateException("Error State: " + currentPlayState);
			Log.e(TAG, "Error State: " + currentPlayState);
			return;
		}
		
		Native_Play();
		
		currentPlayState = STARTED;
	}
	private native void Native_Play();
	
	public boolean isPlaying() throws IllegalStateException {

		boolean bIsPlaying = false;

		if(currentPlayState == STARTED)
		{
			bIsPlaying = Native_IsPlaying();
		}

		return bIsPlaying;
	}
	private native boolean Native_IsPlaying();
	
	public void pause() throws IllegalStateException {
		if (currentPlayState == PAUSED) {
			return;
		}

		if (currentPlayState != STARTED) {
//			throw new IllegalStateException("Error State: " + currentPlayState);
			Log.e(TAG, "Error State: " + currentPlayState);
			return;
		}

		Native_Pause();
		
		currentPlayState = PAUSED;
	}
	private native void Native_Pause();
	
	public void stop() throws IllegalStateException {
		if (currentPlayState==STOPPED) {
			return;
		} else if (currentPlayState != PREPARING && currentPlayState != PREPARED && currentPlayState != STARTED
				&& currentPlayState != PAUSED && currentPlayState != ERROR) {
//			throw new IllegalStateException("Error State: " + currentPlayState);
			Log.e(TAG, "Error State: " + currentPlayState);
			return;
		}

		Native_Stop();
		
		currentPlayState = STOPPED;
		
		Log.d(TAG, "Finish AudioPlayer Stop");
	}
	private native void Native_Stop();
	
	public void seekTo(int msec) throws IllegalStateException
	{
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
//			throw new IllegalStateException("Error State: " + currentPlayState);
			Log.e(TAG, "Error State: " + currentPlayState);
			return;
		}
		
		Native_SeekTo(msec);		
	}
	
	private native void Native_SeekTo(int msec);
	
	public int getCurrentPositionMs() {
		
		int currentPositionMs = 0;
		
		if (currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED) {
			return currentPositionMs;
		}
		currentPositionMs = Native_GetPlayTimeMs();
		
		return currentPositionMs;
	}
	private native int Native_GetPlayTimeMs();
	
	public int getDurationMs() {
		
		int durationMs = 0;
				
		if (currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED) {
			return durationMs;
		}
		
		durationMs = Native_GetDurationMs();
		
		return durationMs;
	}
	private native int Native_GetDurationMs();
	
	//40DB-80DB
	public int getPcmDB() {
		
		int pcmDB = 0;
				
		if (currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED) {
			
			if(pcmDB<MIN_DB)
			{
				pcmDB = MIN_DB;
			}
			
			if(pcmDB>MAX_DB)
			{
				pcmDB = MAX_DB;
			}
			
			return pcmDB;
		}
		
		pcmDB = Native_GetPcmDB();
		
		if(pcmDB<MIN_DB)
		{
			pcmDB = MIN_DB;
		}
		
		if(pcmDB>MAX_DB)
		{
			pcmDB = MAX_DB;
		}
		
		return pcmDB;
	}
	private native int Native_GetPcmDB();
	
	public void setVolume(float volume) {		
		if(currentPlayState==END)
		{
			return;
		}
		
		Native_SetVolume(volume);
	}
	private native void Native_SetVolume(float volume);
	
	public void setPlayRate(float playrate) {		
		if(currentPlayState==END)
		{
			return;
		}
		
		Native_SetPlayRate(playrate);
	}
	private native void Native_SetPlayRate(float playrate);

	public void setLooping(boolean isLooping) {
		if(currentPlayState==END)
		{
			return;
		}
		
		Native_SetLooping(isLooping);
	}
	private native void Native_SetLooping(boolean isLooping);
	
	public ByteBuffer MixWithExternal(ByteBuffer byteBuffer, int byteBufferSize)
	{
		byte[] data = byteBuffer.array();
		int offset = byteBuffer.arrayOffset();
		int size = 0;
		if(byteBufferSize>0)
		{
			size = byteBufferSize;
		}else{
			size = data.length;
		}
		
		Native_MixWithExternal(data, offset, size);
		return byteBuffer;
	}
	
	public void MixWithExternalData(byte[] data, int offset, int size)
	{
		Native_MixWithExternal(data, offset, size);
	}
	
	private native void Native_MixWithExternal(byte[] data, int offset, int size);
	
	public void release() {
		if (currentPlayState == END) {
			return;
		}

		if (currentPlayState == PREPARING || currentPlayState == PREPARED || currentPlayState == STARTED
				|| currentPlayState == PAUSED || currentPlayState == ERROR) {
			Native_Stop();
		}
		
		Native_Finalize();
		
		audioPlayerCallbackHandler.removeCallbacksAndMessages(null);
		
		if(mAudioPlayerOptions!=null && mAudioPlayerOptions.isControlAudioManger && mContext!=null)
		{
		    AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		    audioManager.setMode(oldAudioMode);
		    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, oldAudioVolume, AudioManager.FLAG_PLAY_SOUND);
		}
		
		if(mAudioPlayerOptions!=null && mAudioPlayerOptions.isDubbingMode && mContext!=null)
		{
			if(mHeadSetReceiver!=null)
			{
				mContext.unregisterReceiver(mHeadSetReceiver);
				mHeadSetReceiver = null;
			}
		}
		
		currentPlayState = END;
		
		Log.d(TAG, "Finish AudioPlayer Release");
	}
	
    @Override
    protected void finalize() throws Throwable {
        try {
        	release();
        } finally {
            super.finalize();
        }
    }
    
    public class HeadSetReceiver extends BroadcastReceiver
    {
    	private AudioPlayer mAudioPlayer = null;
    	public HeadSetReceiver(AudioPlayer audioPlayer)
    	{
    		mAudioPlayer = audioPlayer;
    	}
    	
		@Override
		public void onReceive(Context context, Intent intent) {
	        String action = intent.getAction();
	        if (BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
	            BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
	            if (BluetoothProfile.STATE_DISCONNECTED == adapter.getProfileConnectionState(BluetoothProfile.HEADSET)) {
	            	Log.d(TAG, "Bluetooth headset is now disconnected");
	            	if(mAudioPlayer!=null)
	            	{
	            		mAudioPlayer.setVolume(0.0f);
	            	}
	            }else if(BluetoothProfile.STATE_CONNECTED == adapter.getProfileConnectionState(BluetoothProfile.HEADSET)) {
	            	Log.d(TAG, "Bluetooth headset is now connected");
	            	if(mAudioPlayer!=null)
	            	{
	            		mAudioPlayer.setVolume(1.0f);
	            	}
	            }
	        } else if (Intent.ACTION_HEADSET_PLUG.equals(action)) {
	            if (intent.hasExtra("state")){
	                if (intent.getIntExtra("state", 0) == 0){
	                	Log.d(TAG, "headset not connected");
		            	if(mAudioPlayer!=null)
		            	{
		            		mAudioPlayer.setVolume(0.0f);
		            	}
	                }
	                else if (intent.getIntExtra("state", 0) == 1){
	                	Log.d(TAG, "headset connected");
		            	if(mAudioPlayer!=null)
		            	{
		            		mAudioPlayer.setVolume(1.0f);
		            	}
	                }
	            }
	        }
		}
    };
}
