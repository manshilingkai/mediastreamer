package android.slkmedia.mediaeditengine;

public class TwoToOneRemuxerOptions {
	public String inputVideoUrl = null;
	public String inputAudioUrl = null;
	public long startPosMsForInputAudioUrl = -1;
	public long endPosMsForInputAudioUrl = -1;
	public String outputUrl = null;
}
