package android.slkmedia.mediaeditengine;

public interface TwoToOneRemuxerListener {
	public abstract void onTwoToOneRemuxerStart();
	public abstract void onTwoToOneRemuxerError(int errorType);
	public abstract void onTwoToOneRemuxerInfo(int infoType, int infoValue);
	public abstract void onTwoToOneRemuxerEnd();
}
