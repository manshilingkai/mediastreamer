package android.slkmedia.mediaeditengine;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.slkmedia.mediastreamer.utils.FileUtils;
import android.slkmedia.mediastreamer.utils.FolderUtils;
import android.util.Log;

public class VideoThumbnailTaskPlus {
	private static final String TAG = "VideoThumbnailTaskPlus";

	private String mVideoFilePath = null;
	private int mPieces = 0;
	private String mOutputPngFileDir = null;
	private int mDstWidth = 0;
	private int mDstHeight = 0;
	public VideoThumbnailTaskPlus(String videoFilePath, int pieces, String outputPngFileDir, int dstWidth, int dstHeight)
	{
		mVideoFilePath = videoFilePath;
		mPieces = pieces;
		mOutputPngFileDir = outputPngFileDir;
		mDstWidth = dstWidth;
		mDstHeight = dstHeight;
	}
	
	private VideoThumbnailTaskListener mListener = null;
	public void setVideoThumbnailTaskListenr(VideoThumbnailTaskListener listener)
	{
		mListener = listener;
	}
	
	private boolean isWorking = false;
	public void startWork()
	{
		if(isWorking) return;
		createWorkThread();
		isWorking = true;
	}
	
	public void release()
	{
		if(!isWorking) return;
		deleteWorkThread();
		isWorking = false;
	}
	
	private Lock mLock = null;
	private Condition mCondition = null;
	private Thread mWorkThread = null;
	private boolean isBreakWorkThread = false;
	private void createWorkThread()
	{
		mLock = new ReentrantLock(); 
		mCondition = mLock.newCondition();
		
		mWorkThread = new Thread(mWorkRunnable);
		mWorkThread.start();
	}
	
	private Runnable mWorkRunnable = new Runnable()
	{
		@Override
		public void run() {
			if(!FolderUtils.isFolderExists(mOutputPngFileDir))
			{
				Log.e(TAG, "Output Png File Dir is Not Exist");
				return;
			}
			int ret = privateRun();
			if(ret>=0 || ret==-100) return;
			else{
				systemRun();
			}
		}
	};
	
	private void deleteWorkThread()
	{
		mLock.lock();
		isBreakWorkThread = true;
		mCondition.signalAll();
		mLock.unlock();
		
		try {
			mWorkThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		Log.d(TAG, "Finish deleteWorkThread");
	}
	
	static public class Piece
	{
		public long timeMs = 0;
		public boolean isNeedSeek = false;
	}
	// -1 : Fail
	// -100 : Exit
	private int privateRun()
	{
		if(Build.VERSION.SDK_INT<21)
		{
			return -1;
		}
		
		boolean isExit = false;
		
		LinkedList<Piece> pieceList = new LinkedList<Piece>();
		
		MediaExtractor extractor = new MediaExtractor();
        try {
			extractor.setDataSource(mVideoFilePath);
		} catch (IOException e) {
			extractor.release();
			extractor = null;
			return -1;
		}
        
        int trackCount = extractor.getTrackCount();
        MediaFormat videoFormat = null;
        for (int i = 0; i < trackCount; i++) {
            MediaFormat trackFormat = extractor.getTrackFormat(i);
            if (trackFormat.getString(MediaFormat.KEY_MIME).contains("video")) {
                videoFormat = trackFormat;
                extractor.selectTrack(i);
                break;
            }
        }
        
        if(videoFormat==null)
        {
        	extractor.release();
        	extractor = null;
        	return -1;
        }
        
        long durationMs = videoFormat.getLong(MediaFormat.KEY_DURATION)/1000;
        int rotation = 0;
        if(videoFormat.containsKey(MediaFormat.KEY_ROTATION)){
             rotation = videoFormat.getInteger(MediaFormat.KEY_ROTATION);
        }
        
        if(mPieces>1 && durationMs>0)
        {
        	long intervalMs = durationMs/mPieces;
        	boolean isInSameGopWithLastSample = false;
        	int pieceIndex = 0;
        	long sampleTimeBaseLine = extractor.getSampleTime();
        	while(pieceIndex<mPieces)
        	{
        		mLock.lock();
        		if(isBreakWorkThread)
        		{
        			isExit = true;
        			mLock.unlock();
        			break;
        		}
        		mLock.unlock();
        		
        		long sampleTime = extractor.getSampleTime();
        		boolean isKeyFrame = (extractor.getSampleFlags() & MediaExtractor.SAMPLE_FLAG_SYNC) != 0;
        		if(isKeyFrame)
        		{
//        			Log.d(TAG, "Got KeyFrame");
        			isInSameGopWithLastSample = false;
        		}
        		if(sampleTime==-1)
        		{
        			//eof
        	    	break;
        		}else{
        			if((sampleTime-sampleTimeBaseLine)/1000>=pieceIndex*intervalMs)
        			{
            	    	Piece piece = new Piece();
            	    	piece.timeMs = pieceIndex*intervalMs;
//            			Log.d(TAG, "isInSameGopWithLastSample : " + String.valueOf(isInSameGopWithLastSample));
            	    	piece.isNeedSeek = !isInSameGopWithLastSample;
            	    	pieceList.add(piece);
            	    	
            	    	isInSameGopWithLastSample = true;
            	    	pieceIndex++;
        			}
        			
        			extractor.advance();
        		}
        	}
        }else{
	    	Piece piece = new Piece();
	    	piece.timeMs = 0;
	    	piece.isNeedSeek = false;
	    	pieceList.add(piece);
        }
        
        if(isExit)
        {
        	extractor.release();
        	extractor = null;
        	pieceList.clear();
        	
        	return -100;
        }
        
        int colorFormat = MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Flexible;
        videoFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, colorFormat);
        videoFormat.setInteger(MediaFormat.KEY_WIDTH, videoFormat.getInteger(MediaFormat.KEY_WIDTH));
        videoFormat.setInteger(MediaFormat.KEY_HEIGHT, videoFormat.getInteger(MediaFormat.KEY_HEIGHT));
        MediaCodec mediaCodec = null;
        try {
			mediaCodec = MediaCodec.createDecoderByType(videoFormat.getString(MediaFormat.KEY_MIME));
		} catch (IOException e) {
        	extractor.release();
        	extractor = null;
        	pieceList.clear();
        	return -1;
		}
		int imageFormat = ImageFormat.YUV_420_888;
		ImageReader imageReader = ImageReader
                .newInstance(
                        videoFormat.getInteger(MediaFormat.KEY_WIDTH),
                        videoFormat.getInteger(MediaFormat.KEY_HEIGHT),
                        imageFormat,
                        2);
		ImageReaderHandlerThread imageReaderHandlerThread = new ImageReaderHandlerThread();
		ImageProducer imageProducer = new ImageProducer(rotation, mDstWidth, mDstHeight, mListener, mOutputPngFileDir, mPieces);
        imageReader.setOnImageAvailableListener(imageProducer, imageReaderHandlerThread.getHandler());

        mediaCodec.configure(videoFormat, imageReader.getSurface(), null, 0);
        mediaCodec.start();
        
        MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
        long timeOut = 5 * 1000; //5ms
        boolean inputDone = false;
        boolean outputDone = false;
        ByteBuffer[] inputBuffers = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            inputBuffers = mediaCodec.getInputBuffers();
        }
        
        Piece currentPiece = pieceList.removeFirst();
        if(currentPiece.isNeedSeek)
        {
        	extractor.seekTo(currentPiece.timeMs*1000, MediaExtractor.SEEK_TO_PREVIOUS_SYNC);
        }
        boolean gotPresentationTimeUsBaseLine = false;
        long presentationTimeUsBaseLine = 0;
        while(!outputDone)
        {
    		mLock.lock();
    		if(isBreakWorkThread)
    		{
    			isExit = true;
    			mLock.unlock();
    			break;
    		}
    		mLock.unlock();
        	
        	if(!inputDone)
        	{
        		//feed data
                int inputBufferIndex = mediaCodec.dequeueInputBuffer(timeOut);
                if (inputBufferIndex >= 0) {
                    ByteBuffer inputBuffer;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        inputBuffer = mediaCodec.getInputBuffer(inputBufferIndex);
                    } else {
                        inputBuffer = inputBuffers[inputBufferIndex];
                    }
                    int sampleSize = extractor.readSampleData(inputBuffer, 0);
                    if(sampleSize>=0)
                    {
                        long sampleTime = extractor.getSampleTime();
                        mediaCodec.queueInputBuffer(inputBufferIndex, 0, sampleSize, sampleTime, 0);
                        extractor.advance();
                    }else{
                    	//eof
                        mediaCodec.queueInputBuffer(inputBufferIndex, 0, 0, 0L, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                        inputDone = true;
                        Log.d(TAG, "input EOS");
                    }
                }
        	}
        	if(!outputDone)
        	{
                //drain data
                int status = mediaCodec.dequeueOutputBuffer(bufferInfo, timeOut);
                if (status == MediaCodec.INFO_TRY_AGAIN_LATER) {
                } else if (status == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                } else if (status == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                } else {
                    if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                        Log.d(TAG, "output EOS");
                        outputDone = true;
                    }
                    boolean doRender = (bufferInfo.size != 0);
                    long presentationTimeUs = bufferInfo.presentationTimeUs;
                    if(!gotPresentationTimeUsBaseLine)
                    {
                    	gotPresentationTimeUsBaseLine = true;
                    	presentationTimeUsBaseLine = presentationTimeUs;
                    }
                    
                	if((presentationTimeUs - presentationTimeUsBaseLine)/1000 >= currentPiece.timeMs)
                	{
                		doRender = true;
                	}else {
                		doRender = false;
                	}
                    
                    mediaCodec.releaseOutputBuffer(status, doRender);
                    
                    if(doRender)
                    {
                		if(pieceList.isEmpty())
                		{
                			outputDone = true;
                			break;
                		}else{
                    		currentPiece = pieceList.removeFirst();
                            if(currentPiece.isNeedSeek)
                            {
                            	extractor.seekTo(currentPiece.timeMs*1000, MediaExtractor.SEEK_TO_PREVIOUS_SYNC);
                            	mediaCodec.flush();
//                            	Log.d(TAG, "currentPiece NeedSeek -> SeekTimeUs : " + String.valueOf(currentPiece.timeMs*1000));
                            }else{
//                            	Log.d(TAG, "currentPiece Just Need Continue Decode");
                            }
                		}
                    }
                }
        	}else break;
        }
        
        if(isExit)
        {
        	imageReaderHandlerThread.release();
        	
            if (mediaCodec != null) {
            	mediaCodec.stop();
            	mediaCodec.release();
            }
          
            if (extractor != null) {
            	extractor.release();
            }
            
            if (imageReader !=null) {
            	imageReader.close();
            }
            
            pieceList.clear();
        	
        	return -100;
        }
        
    	mLock.lock();
		try {
			while(!isBreakWorkThread)
			{
				mCondition.await(10, TimeUnit.MILLISECONDS);
			}
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
		}
		mLock.unlock();
        
        imageReaderHandlerThread.release();
        
        if (mediaCodec != null) {
        	mediaCodec.stop();
        	mediaCodec.release();
        }
      
        if (extractor != null) {
        	extractor.release();
        }
        
        if (imageReader !=null) {
        	imageReader.close();
        }
        
        pieceList.clear();
        
    	mLock.lock();
    	if(!isBreakWorkThread)
    	{
    		imageProducer.flush();
    	}
		mLock.unlock();
	  
        return 0;
	}
	
    private class ImageReaderHandlerThread extends HandlerThread {

        private final Handler handler;

    	private Lock mLock;
    	private Condition mCondition;
    	private boolean isFinishAllCallbacksAndMessages = false;
    	
        public ImageReaderHandlerThread() {
            super("ImageReader");
            start();
            handler = new Handler(getLooper());
            
            mLock = new ReentrantLock();
            mCondition = mLock.newCondition();
        }

        public Handler getHandler() {
            return handler;
        }

        public void release()
        {
        	mLock.lock();
        	handler.post(new Runnable() {
    			@Override
    			public void run() {
    				handler.removeCallbacksAndMessages(null);
    				
    				mLock.lock();
    				isFinishAllCallbacksAndMessages = true;
    				mCondition.signalAll();
    				mLock.unlock();
    			}
    		});

    		try {
    			while(!isFinishAllCallbacksAndMessages)
    			{
    				mCondition.await(10, TimeUnit.MILLISECONDS);
    			}
    		} catch (InterruptedException e) {
    			Log.e(TAG, e.getLocalizedMessage());
    		}finally {
    		}
        	
			handler.removeCallbacksAndMessages(null);
    		
    		if(Build.VERSION.SDK_INT>=/*Build.VERSION_CODES.JELLY_BEAN_MR2*/18)
    		{
    			super.quitSafely();
    		}else{
    			super.quit();
    		}
    		
    		mLock.unlock();
    	}
    }
    
    private class ImageProducer implements ImageReader.OnImageAvailableListener
    {
    	private int mRotation;
    	private int mDstWidth;
    	private int mDstHeight;
    	private VideoThumbnailTaskListener mVideoThumbnailTaskListener;
    	private String mOutputPngFileDir;
    	private int mPieces;
    	public ImageProducer(int rotation, int dstWidth, int dstHeight, VideoThumbnailTaskListener callback, String outputPngFileDir, int pieces)
    	{
    		mRotation = rotation;
    		mDstWidth = dstWidth;
    		mDstHeight = dstHeight;
    		mVideoThumbnailTaskListener = callback;
    		mOutputPngFileDir = outputPngFileDir;
    		mPieces = pieces;
    	}
    	
    	private int mPieceIndex = 0;
		@Override
		public void onImageAvailable(ImageReader reader) {
			if(mPieceIndex>=mPieces) return;
			Image img = null;
            try {
                img = reader.acquireLatestImage();
                if (img != null) {
                    Image.Plane[] planes = img.getPlanes();
                    if(planes == null || planes[0].getBuffer() == null)
                    {
                    	img.close();
                    	return;
                    }
                    
                    String writePath = mOutputPngFileDir+"/"+mPieceIndex+".png";
                    boolean ret = writeImageToDisk(img, writePath, mDstWidth, mDstHeight, mRotation);
                    if(ret)
                    {
                    	if(mVideoThumbnailTaskListener!=null)
                    	{
                    		mVideoThumbnailTaskListener.onVideoThumbnail(mPieceIndex, mPieces, writePath);
                    	}
                    	
                    	mPieceIndex++;
                    	
                    	if(mPieceIndex>=mPieces)
                    	{
                    		mLock.lock();
                    		isBreakWorkThread = true;
                    		mCondition.signalAll();
                    		mLock.unlock();
                    	}
                    }
                }
            } catch (Exception e) {
            } finally {
                if (img != null) {
                    img.close();
                }
            }
		}
		
		public void flush()
		{
			while(mPieceIndex<mPieces)
			{
				boolean ret = FileUtils.copyNio(mOutputPngFileDir+"/"+(mPieceIndex-1)+".png", mOutputPngFileDir+"/"+mPieceIndex+".png");
                if(ret)
                {
                	if(mVideoThumbnailTaskListener!=null)
                	{
                		mVideoThumbnailTaskListener.onVideoThumbnail(mPieceIndex, mPieces, mOutputPngFileDir+"/"+mPieceIndex+".png");
                	}
                	
                	mPieceIndex++;
                }else break;
			}
		}
		
		private boolean writeImageToDisk(Image image, String outputPngFilePath, int dstWidth, int dstHeight, int rotation)
		{
            if(image.getFormat()==android.graphics.ImageFormat.YUV_420_888)
            {
                int width = image.getWidth();
                int height = image.getHeight();
                // Read image data
                Image.Plane[] planes = image.getPlanes();
                
//                Log.d(TAG, "width : " + image.getWidth());
//                Log.d(TAG, "height : " + image.getHeight());
//                
//                Log.d(TAG, "planes[0].getRowStride : " + planes[0].getRowStride());
//                Log.d(TAG, "planes[0].getPixelStride : " + planes[0].getPixelStride());
//                
//                Log.d(TAG, "planes[1].getRowStride : " + planes[1].getRowStride());
//                Log.d(TAG, "planes[1].getPixelStride : " + planes[1].getPixelStride());
//                
//                Log.d(TAG, "planes[2].getRowStride : " + planes[2].getRowStride());
//                Log.d(TAG, "planes[2].getPixelStride : " + planes[2].getPixelStride());

                
                if(planes[1].getPixelStride()==1)
                {
                	return VideoFileUtils.WriteI420ImageToDisk(planes[0].getBuffer(), planes[0].getRowStride(),
                            planes[1].getBuffer(), planes[1].getRowStride(),
                            planes[2].getBuffer(), planes[2].getRowStride(),
                            width, height, outputPngFilePath, dstWidth, dstHeight, rotation);
                }else if(planes[1].getPixelStride()==2){
                	return VideoFileUtils.WriteNV12ImageToDisk(planes[0].getBuffer(), planes[0].getRowStride(),
                            planes[1].getBuffer(), planes[1].getRowStride(), width, height, outputPngFilePath, dstWidth, dstHeight, rotation);
                }else{
                	Log.d(TAG, "Unknown PixelStride For Image Format YUV_420_888");
                	return false;
                }
            }else{
            	Log.d(TAG, "Unknown Image Format");
            	return false;
            }
		}
    }

	public interface VideoThumbnailTaskListener {
		public abstract void onVideoThumbnail(int index, int all, String thumbnailPng);
	}
	
	public void systemRun() {
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		try{
			mmr.setDataSource(mVideoFilePath);
			long videoDurationMs = 0;
			String duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
			if(duration!=null)
			{
				Log.d(TAG, mVideoFilePath + " [ " + duration + " ] ");
				videoDurationMs = Long.parseLong(duration);
			}
			
			int option = MediaMetadataRetriever.OPTION_CLOSEST;
			for(int i = 0; i<mPieces; i++)
			{
				mLock.lock();
				if(isBreakWorkThread)
				{
					mLock.unlock();
					break;
				}
				mLock.unlock();
				
				long videoTimeMs = (videoDurationMs/mPieces)*i;
				Bitmap thumbnailBitmap = mmr.getFrameAtTime(videoTimeMs*1000, option);
				if(thumbnailBitmap!=null)
				{
					boolean ret = VideoFileUtils.BitmapToPng(thumbnailBitmap, mOutputPngFileDir+"/"+i+".png", mDstWidth, mDstHeight);
					if(ret)
					{
						if(mListener!=null)
						{
							mListener.onVideoThumbnail(i, mPieces, mOutputPngFileDir+"/"+i+".png");
						}
					}else{
						mmr.release();
						return;
					}
				}else{
					mmr.release();
					return;
				}
			}
			
		}catch (Exception ex) {
		}finally {
			mmr.release();
		}
	}
}
