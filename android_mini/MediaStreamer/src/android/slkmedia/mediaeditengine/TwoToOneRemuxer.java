package android.slkmedia.mediaeditengine;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

public class TwoToOneRemuxer {
	private static final String TAG = "TwoToOneRemuxer";

	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}

	//Native Context
	private long mNativeContext = 0;
	
	private static native final void Native_Init();
	private native void Native_Start(String inputVideoUrl, String inputAudioUrl, long startPosMsForInputAudioUrl, long endPosMsForInputAudioUrl, String outputUrl, Object twotooneremuxer_this);
	private native void Native_Pause();
	private native void Native_Resume();
	private native void Native_Stop(boolean isCancel);
	
	//////////////////////////////////////////////////////////////////////////
	/**
	 * Called from native code when an interesting event happens. This method
	 * just uses the EventHandler system to post the event back to the main app
	 * thread. We use a weak reference to the original MediaPlayer object so
	 * that the native code is safe from the object disappearing from underneath
	 * it. (This is the cookie passed to native_setup().)
	 */
	private static void postEventFromNative(Object twotooneremuxer_ref, int what,
			int arg1, int arg2, Object obj) {
		TwoToOneRemuxer ttor = (TwoToOneRemuxer) ((WeakReference<?>) twotooneremuxer_ref).get();
		if (ttor == null) {
			return;
		}

		if (ttor.twoToOneRemuxerCallbackHandler != null) {
			Message msg = ttor.twoToOneRemuxerCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}
	//////////////////////////////////////////////////////////////////////////

	private Lock mTwoToOneRemuxerLock = null;
	private Condition mTwoToOneRemuxerCondition = null;
	private HandlerThread mHandlerThread = null;
	private Handler twoToOneRemuxerCallbackHandler = null;

	public TwoToOneRemuxer()
	{
		mTwoToOneRemuxerLock = new ReentrantLock(); 
		mTwoToOneRemuxerCondition = mTwoToOneRemuxerLock.newCondition();
		
		mHandlerThread = new HandlerThread("TwoToOneRemuxerHandlerThread");
		mHandlerThread.start();
		
		twoToOneRemuxerCallbackHandler = new Handler(mHandlerThread.getLooper()) {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case CALLBACK_TWO_TO_ONE_REMUXER_STREAMING:
					if(mTwoToOneRemuxerListener!=null)
					{
						mTwoToOneRemuxerListener.onTwoToOneRemuxerStart();
					}
					break;
				case CALLBACK_TWO_TO_ONE_REMUXER_ERROR:
					if(mTwoToOneRemuxerListener!=null)
					{
						mTwoToOneRemuxerListener.onTwoToOneRemuxerError(msg.arg1);
					}
					
					break;
				case CALLBACK_TWO_TO_ONE_REMUXER_INFO:
					if(mTwoToOneRemuxerListener!=null)
					{
						mTwoToOneRemuxerListener.onTwoToOneRemuxerInfo(msg.arg1, msg.arg2);
					}
					break;
				case CALLBACK_TWO_TO_ONE_REMUXER_END:
					if(mTwoToOneRemuxerListener!=null)
					{
						mTwoToOneRemuxerListener.onTwoToOneRemuxerEnd();
					}
					
					break;
				default:
					break;
				}
			}
		};
	}
	
	private TwoToOneRemuxerListener mTwoToOneRemuxerListener = null;
	public void setTwoToOneRemuxerListener(TwoToOneRemuxerListener listener)
	{
		mTwoToOneRemuxerListener = listener;
	}
	
	private boolean isStarted = false;
	public void start(TwoToOneRemuxerOptions options)
	{
		mTwoToOneRemuxerLock.lock();

		if(isReleased)
		{
			Log.w(TAG, "TwoToOneRemuxer has released!!");
			mTwoToOneRemuxerLock.unlock();
			return;
		}
		
		if(isStarted)
		{
			Log.w(TAG, "TwoToOneRemuxer has started!!");
			mTwoToOneRemuxerLock.unlock();
			return;
		}
		
		Native_Start(options.inputVideoUrl, options.inputAudioUrl, options.startPosMsForInputAudioUrl, options.endPosMsForInputAudioUrl, options.outputUrl, new WeakReference<TwoToOneRemuxer>(this));
		
		isStarted = true;
		isPaused = false;
		mTwoToOneRemuxerLock.unlock();
	}
	
	private boolean isPaused = false;
	public void pause()
	{
		mTwoToOneRemuxerLock.lock();
		
		if(isReleased)
		{
			Log.w(TAG, "TwoToOneRemuxer has released!!");
			mTwoToOneRemuxerLock.unlock();
			return;
		}
		
		if(!isStarted)
		{
			Log.w(TAG, "TwoToOneRemuxer has not started!!");
			mTwoToOneRemuxerLock.unlock();
			return;
		}
		
		if(!isPaused)
		{
			Native_Pause();
			isPaused = true;
		}
		mTwoToOneRemuxerLock.unlock();
	}
	
	public void resume()
	{
		mTwoToOneRemuxerLock.lock();
		
		if(isReleased)
		{
			Log.w(TAG, "TwoToOneRemuxer has released!!");
			mTwoToOneRemuxerLock.unlock();
			return;
		}
		
		if(!isStarted)
		{
			Log.w(TAG, "TwoToOneRemuxer has not started!!");
			mTwoToOneRemuxerLock.unlock();
			return;
		}
		
		if(isPaused)
		{
			Native_Resume();
			isPaused = false;
		}
		
		mTwoToOneRemuxerLock.unlock();
	}
	
	public void stop(boolean isCancle)
	{
		mTwoToOneRemuxerLock.lock();
		
		if(isReleased)
		{
			Log.w(TAG, "TwoToOneRemuxer has released!!");
			mTwoToOneRemuxerLock.unlock();
			return;
		}

		if(!isStarted)
		{
			Log.w(TAG, "TwoToOneRemuxer has stopped");
			mTwoToOneRemuxerLock.unlock();
			return;
		}
		
		Native_Stop(isCancle);
	
		isStarted = false;
		isPaused = false;
		
		mTwoToOneRemuxerLock.unlock();
	}
	
	private boolean isReleased = false;
	private boolean isFinishAllCallbacksAndMessages = false;
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2) public void release()
	{
		stop(false);
		
		mTwoToOneRemuxerLock.lock();
		if(isReleased)
		{
			Log.w(TAG, "TwoToOneRemuxer has released!!");
			mTwoToOneRemuxerLock.unlock();
			return;
		}
		
		twoToOneRemuxerCallbackHandler.post(new Runnable() {
			@Override
			public void run() {
				twoToOneRemuxerCallbackHandler.removeCallbacksAndMessages(null);
				
				mTwoToOneRemuxerLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mTwoToOneRemuxerCondition.signalAll();
				mTwoToOneRemuxerLock.unlock();
			}
		});
		
		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mTwoToOneRemuxerCondition.await(10, TimeUnit.MILLISECONDS);
			}
			
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
		}
		
		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR2)
		{
			mHandlerThread.quitSafely();
		}else{
			mHandlerThread.quit();
		}
		
		isReleased = true;
		
		mTwoToOneRemuxerLock.unlock();
		
		Log.d(TAG, "Finish TwoToOneRemuxer Release");
	}
	
	//two_to_one_remuxer_event_type
	public final static int CALLBACK_TWO_TO_ONE_REMUXER_STREAMING = 0;
	public final static int CALLBACK_TWO_TO_ONE_REMUXER_ERROR = 1;
	public final static int CALLBACK_TWO_TO_ONE_REMUXER_INFO = 2;
	public final static int CALLBACK_TWO_TO_ONE_REMUXER_END = 3;

	//two_to_one_remuxer_error_type
	public final static int CALLBACK_TWO_TO_ONE_REMUXER_ERROR_UNKNOWN = -1;
	public final static int CALLBACK_TWO_TO_ONE_REMUXER_ERROR_OPEN_INPUT_VIDEO_MATERIAL_FAIL = 0;
	public final static int CALLBACK_TWO_TO_ONE_REMUXER_ERROR_OPEN_INPUT_AUDIO_MATERIAL_FAIL = 1;
	public final static int CALLBACK_TWO_TO_ONE_REMUXER_ERROR_OPEN_OUTPUT_PRODUCT_FAIL = 2;
	public final static int CALLBACK_TWO_TO_ONE_REMUXER_ERROR_VIDEO_DEMUX_FAIL = 3;
	public final static int CALLBACK_TWO_TO_ONE_REMUXER_ERROR_VIDEO_MUX_FAIL = 4;
	public final static int CALLBACK_TWO_TO_ONE_REMUXER_ERROR_AUDIO_DEMUX_FAIL = 5;
	public final static int CALLBACK_TWO_TO_ONE_REMUXER_ERROR_AUDIO_MUX_FAIL = 6;


}
