package android.slkmedia.mediaeditengine;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Process;
import android.util.Log;

public class AudioEditEngineEnv {
	private static final String TAG = "AudioEditEngineEnv";

	private static int oldAudioMode = AudioManager.MODE_CURRENT;
//	private static int oldAudioVolume = 0;
	
	private static boolean isSetupSuccess = false;
	public static boolean setupAudioManager(Context context)
	{
		if(isSetupSuccess) return true;
		
	    if (!hasPermission(context, android.Manifest.permission.RECORD_AUDIO)) {
	        Log.e(TAG, "RECORD_AUDIO permission is missing");
	        
	        isSetupSuccess = false;
	        return false;
	    }
	    
	    AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
	    oldAudioMode = audioManager.getMode();
	    audioManager.setMode(AudioManager.MODE_NORMAL);
//	    oldAudioVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//	    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), AudioManager.FLAG_PLAY_SOUND);
	    isSetupSuccess = true;
	    return true;
	}
	
	public static void unsetupAudioManager(Context context)
	{
		if(!isSetupSuccess) return;
		
	    AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
	    audioManager.setMode(oldAudioMode);
//	    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, oldAudioVolume, AudioManager.FLAG_PLAY_SOUND);
	    
	    isSetupSuccess = false;
	}
	
	private static boolean hasPermission(Context context, String permission) {
	    return context.checkPermission(permission, Process.myPid(), Process.myUid())
	        == PackageManager.PERMISSION_GRANTED;
	}
}
