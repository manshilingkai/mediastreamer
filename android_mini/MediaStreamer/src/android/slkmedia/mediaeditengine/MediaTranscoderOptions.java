package android.slkmedia.mediaeditengine;

public class MediaTranscoderOptions {
	public String inputUrl = null;
	public long startPosMs = -1;
	public long endPosMs = -1;
	
	public int video_rotation_degree = 0; //0, 90, 180, 270
	public int video_flip = MediaTranscoder.NO_FLIP;
	
	public String overlayUrl = null;
	public boolean has_alpha_for_overlay_video = false;
	public int overlay_x = 0;
	public int overlay_y = 0;
	public float overlay_scale = 1.0f;
	public long overlay_effect_in_ms = -1;
	public long overlay_effect_out_ms = -1;
	
	public String outputUrl = null;
	public boolean hasVideo = true;
	public int outputVideoWidth = 0;
	public int outputVideoHeight = 0;
	public int outputVideoBitrateKbps = MediaTranscoder.MAX_VIDEO_BITRATE;
	public boolean hasAudio = true;
	public int outputAudioSampleRate = 44100;
	public int outputAudioBitrateKbps = MediaTranscoder.MAX_AUDIO_BITRATE;
}
