package android.slkmedia.mediaeditengine;

public interface MediaDubbingProducerListener {
	public abstract void onDubbingStart(int tag);
	public abstract void onDubbingError(int errorType, int tag);
	public abstract void onDubbingInfo(int infoType, int infoValue, int tag);
	public abstract void onDubbingEnd(int tag);
}
