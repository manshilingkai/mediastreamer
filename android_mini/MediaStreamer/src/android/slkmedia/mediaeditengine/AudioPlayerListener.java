package android.slkmedia.mediaeditengine;

public interface AudioPlayerListener {
	public abstract void onPrepared();
	public abstract void onError(int what, int extra);
	public abstract void onInfo(int what, int extra);
	public abstract void onCompletion();
	public abstract void OnSeekComplete();
}