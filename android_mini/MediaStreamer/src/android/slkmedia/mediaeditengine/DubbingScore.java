package android.slkmedia.mediaeditengine;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.slkmedia.mediastreamer.utils.FolderUtils;
import android.util.Log;

public class DubbingScore {

	private static final String TAG = "DubbingScore";

	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}

	//Native Context
	private long mNativeContext = 0;
	
	private static native final void Native_Init();
	private native void Native_Start(String originUrl, String bgmUrl, String dubUrl, String workDir, Object dubbingscore_this);
	private native void Native_Pause();
	private native void Native_Resume();
	private native void Native_Stop();
	
	//////////////////////////////////////////////////////////////////////////
	/**
	 * Called from native code when an interesting event happens. This method
	 * just uses the EventHandler system to post the event back to the main app
	 * thread. We use a weak reference to the original MediaPlayer object so
	 * that the native code is safe from the object disappearing from underneath
	 * it. (This is the cookie passed to native_setup().)
	 */
	private static void postEventFromNative(Object dubbingscore_ref, int what,
			int arg1, int arg2, Object obj) {
		DubbingScore ds = (DubbingScore) ((WeakReference<?>) dubbingscore_ref).get();
		if (ds == null) {
			return;
		}

		if (ds.dubbingScoreCallbackHandler != null) {
			Message msg = ds.dubbingScoreCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}
	//////////////////////////////////////////////////////////////////////////

	private Lock mDubbingScoreLock = null;
	private Condition mDubbingScoreCondition = null;
	private HandlerThread mHandlerThread = null;
	private Handler dubbingScoreCallbackHandler = null;
	
	public DubbingScore()
	{
		mDubbingScoreLock = new ReentrantLock(); 
		mDubbingScoreCondition = mDubbingScoreLock.newCondition();
		
		mHandlerThread = new HandlerThread("DubbingScoreHandlerThread");
		mHandlerThread.start();
		
		dubbingScoreCallbackHandler = new Handler(mHandlerThread.getLooper()) {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case CALLBACK_DUBBING_SCORE_PROCESSING:
					if(mDubbingScoreListener!=null)
					{
						mDubbingScoreListener.onDubbingScoreStart();
					}
					break;
				case CALLBACK_DUBBING_SCORE_ERROR:
					if(mDubbingScoreListener!=null)
					{
						mDubbingScoreListener.onDubbingScoreError(msg.arg1);
					}
					
					break;
				case CALLBACK_DUBBING_SCORE_INFO:
					if(mDubbingScoreListener!=null)
					{
						mDubbingScoreListener.onDubbingScoreInfo(msg.arg1, msg.arg2);
					}
					break;
				case CALLBACK_DUBBING_SCORE_END:
					if(mDubbingScoreListener!=null)
					{
						mDubbingScoreListener.onDubbingScoreEnd((float)(msg.arg1)/10.0f);
					}
					
					break;
				default:
					break;
				}
			}
		};
	}
	
	private DubbingScoreListener mDubbingScoreListener = null;
	public void setDubbingScoreListener(DubbingScoreListener listener)
	{
		mDubbingScoreListener = listener;
	}
	
	private boolean isStarted = false;
	public void start(DubbingScoreOptions options)
	{
		mDubbingScoreLock.lock();

		if(isReleased)
		{
			Log.w(TAG, "DubbingScore has released!!");
			mDubbingScoreLock.unlock();
			return;
		}
		
		if(isStarted)
		{
			Log.w(TAG, "DubbingScore has started!!");
			mDubbingScoreLock.unlock();
			return;
		}
		
		String workDir = Environment.getExternalStorageDirectory().getPath()+"/YPPMediaStreamer";
		boolean ret = FolderUtils.isFolderExists(workDir);
		if(!ret)
		{
			Log.e(TAG, "WorkDir is not exist");
			mDubbingScoreLock.unlock();
			return;
		}
		
		Native_Start(options.originUrl, options.bgmUrl, options.dubUrl, workDir, new WeakReference<DubbingScore>(this));
		
		isStarted = true;
		isPaused = false;
		mDubbingScoreLock.unlock();
	}
	
	private boolean isPaused = false;
	public void pause()
	{
		mDubbingScoreLock.lock();
		
		if(isReleased)
		{
			Log.w(TAG, "DubbingScore has released!!");
			mDubbingScoreLock.unlock();
			return;
		}
		
		if(!isStarted)
		{
			Log.w(TAG, "DubbingScore has not started!!");
			mDubbingScoreLock.unlock();
			return;
		}
		
		if(!isPaused)
		{
			Native_Pause();
			isPaused = true;
		}
		mDubbingScoreLock.unlock();
	}
	
	public void resume()
	{
		mDubbingScoreLock.lock();
		
		if(isReleased)
		{
			Log.w(TAG, "DubbingScore has released!!");
			mDubbingScoreLock.unlock();
			return;
		}
		
		if(!isStarted)
		{
			Log.w(TAG, "DubbingScore has not started!!");
			mDubbingScoreLock.unlock();
			return;
		}
		
		if(isPaused)
		{
			Native_Resume();
			isPaused = false;
		}
		
		mDubbingScoreLock.unlock();
	}
	
	public void stop()
	{
		mDubbingScoreLock.lock();
		
		if(isReleased)
		{
			Log.w(TAG, "DubbingScore has released!!");
			mDubbingScoreLock.unlock();
			return;
		}

		if(!isStarted)
		{
			Log.w(TAG, "DubbingScore has stopped");
			mDubbingScoreLock.unlock();
			return;
		}
		
		Native_Stop();
	
		isStarted = false;
		isPaused = false;
		
		mDubbingScoreLock.unlock();
	}
	
	private boolean isReleased = false;
	private boolean isFinishAllCallbacksAndMessages = false;
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2) public void release()
	{
		stop();
		
		mDubbingScoreLock.lock();
		if(isReleased)
		{
			Log.w(TAG, "DubbingScore has released!!");
			mDubbingScoreLock.unlock();
			return;
		}
		
		dubbingScoreCallbackHandler.post(new Runnable() {
			@Override
			public void run() {
				dubbingScoreCallbackHandler.removeCallbacksAndMessages(null);
				
				mDubbingScoreLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mDubbingScoreCondition.signalAll();
				mDubbingScoreLock.unlock();
			}
		});
		
		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mDubbingScoreCondition.await(10, TimeUnit.MILLISECONDS);
			}
			
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
		}
		
		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR2)
		{
			mHandlerThread.quitSafely();
		}else{
			mHandlerThread.quit();
		}
		
		isReleased = true;
		
		Log.d(TAG, "Finish DubbingScore Release");

		mDubbingScoreLock.unlock();
	}
	
	//dubbing_score_event_type
	public final static int CALLBACK_DUBBING_SCORE_PROCESSING = 0;
	public final static int CALLBACK_DUBBING_SCORE_ERROR = 1;
	public final static int CALLBACK_DUBBING_SCORE_INFO = 2;
	public final static int CALLBACK_DUBBING_SCORE_END = 3;
	
	//dubbing_score_info_type
	public final static int CALLBACK_DUBBING_SCORE_INFO_PROCESS_TIMESTAMP = 1;
	public final static int CALLBACK_DUBBING_SCORE_INFO_PROCESS_PERCENT = 2;
	
	//dubbing_score_error_type
	public final static int CALLBACK_DUBBING_SCORE_ERROR_UNKNOWN = -1;
	public final static int CALLBACK_DUBBING_SCORE_ERROR_ORIGIN_ERROR = 0;
	public final static int CALLBACK_DUBBING_SCORE_ERROR_BGM_ERROR = 1;
	public final static int CALLBACK_DUBBING_SCORE_ERROR_OPEN_DUB_FAIL = 2;
	public final static int CALLBACK_DUBBING_SCORE_ERROR_OPEN_OUTPUT_PRODUCT_FAIL = 3;
}
