package android.slkmedia.mediaeditengine;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.slkmedia.mediaeditengine.VideoFileUtils.MediaInfo;
import android.util.Log;

public class MediaTranscoder {
	private static final String TAG = "MediaTranscoder";

	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}

	//Native Context
	private long mNativeContext = 0;
	
	private static native final void Native_Init();
	private native void Native_Start(String inputUrl, long startPosMs, long endPosMs, int video_rotation_degree, int video_flip, String overlayUrl, boolean has_alpha_for_overlay_video, int overlay_x, int overlay_y, float overlay_scale, long overlay_effect_in_ms, long overlay_effect_out_ms, String outputUrl, boolean hasVideo, int outputVideoWidth, int outputVideoHeight, int outputVideoBitrateKbps, boolean hasAudio, int outputAudioSampleRate, int outputAudioBitrateKbps, Object mediatranscoder_this);
	private native void Native_Pause();
	private native void Native_Resume();
	private native void Native_Stop(boolean isCancel);
	
	//////////////////////////////////////////////////////////////////////////
	/**
	 * Called from native code when an interesting event happens. This method
	 * just uses the EventHandler system to post the event back to the main app
	 * thread. We use a weak reference to the original MediaPlayer object so
	 * that the native code is safe from the object disappearing from underneath
	 * it. (This is the cookie passed to native_setup().)
	 */
	private static void postEventFromNative(Object mediatranscoder_ref, int what,
			int arg1, int arg2, Object obj) {
		MediaTranscoder mt = (MediaTranscoder) ((WeakReference<?>) mediatranscoder_ref).get();
		if (mt == null) {
			return;
		}

		if (mt.mediaTranscoderCallbackHandler != null) {
			Message msg = mt.mediaTranscoderCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}
	//////////////////////////////////////////////////////////////////////////

	private Lock mMediaTranscoderLock = null;
	private Condition mMediaTranscoderCondition = null;
	private HandlerThread mHandlerThread = null;
	private Handler mediaTranscoderCallbackHandler = null;
	
	public MediaTranscoder()
	{
		mMediaTranscoderLock = new ReentrantLock(); 
		mMediaTranscoderCondition = mMediaTranscoderLock.newCondition();
		
		mHandlerThread = new HandlerThread("MediaTranscoderHandlerThread");
		mHandlerThread.start();
		
		mediaTranscoderCallbackHandler = new Handler(mHandlerThread.getLooper()) {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case CALLBACK_MEDIA_TRANSCODER_CONNECTING:
					if(mMediaTranscoderListener!=null)
					{
						mMediaTranscoderListener.onMediaTranscoderConnecting();
					}
					break;
				case CALLBACK_MEDIA_TRANSCODER_CONNECTED:
					if(mMediaTranscoderListener!=null)
					{
						mMediaTranscoderListener.onMediaTranscoderConnected();
					}
					break;
				case CALLBACK_MEDIA_TRANSCODER_STREAMING:
					if(mMediaTranscoderListener!=null)
					{
						mMediaTranscoderListener.onMediaTranscoderTranscoding();
					}
					break;
				case CALLBACK_MEDIA_TRANSCODER_INFO:
					if(mMediaTranscoderListener!=null)
					{
						mMediaTranscoderListener.onMediaTranscoderInfo(msg.arg1, msg.arg2);
					}
					break;
				case CALLBACK_MEDIA_TRANSCODER_ERROR:
					if(mMediaTranscoderListener!=null)
					{
						mMediaTranscoderListener.onMediaTranscoderError(msg.arg1);
					}
					break;
				case CALLBACK_MEDIA_TRANSCODER_END:
					if(mMediaTranscoderListener!=null)
					{
						mMediaTranscoderListener.onMediaTranscoderEnd();
					}
					
					break;
				default:
					break;
				}
			}
		};
	}
	
	private MediaTranscoderListener mMediaTranscoderListener = null;
	public void setMediaTranscoderListener(MediaTranscoderListener listener)
	{
		mMediaTranscoderListener = listener;
	}
	
	private boolean isStarted = false;
	public void start(MediaTranscoderOptions options)
	{
		mMediaTranscoderLock.lock();

		if(isReleased)
		{
			Log.w(TAG, "MediaTranscoder has released!!");
			mMediaTranscoderLock.unlock();
			return;
		}
		
		if(isStarted)
		{
			Log.w(TAG, "MediaTranscoder has started!!");
			mMediaTranscoderLock.unlock();
			return;
		}
		
		makeVideoEncodeSize(options, null);
		makeVideoEncodeBitrate(options);
		Native_Start(options.inputUrl, options.startPosMs, options.endPosMs, options.video_rotation_degree, options.video_flip, options.overlayUrl, options.has_alpha_for_overlay_video, options.overlay_x, options.overlay_y, options.overlay_scale, options.overlay_effect_in_ms, options.overlay_effect_out_ms, options.outputUrl, options.hasVideo, options.outputVideoWidth, options.outputVideoHeight, options.outputVideoBitrateKbps, options.hasAudio, options.outputAudioSampleRate, options.outputAudioBitrateKbps, new WeakReference<MediaTranscoder>(this));
		
		isStarted = true;
		isPaused = false;
		mMediaTranscoderLock.unlock();
	}
	
	private boolean isPaused = false;
	public void pause()
	{
		mMediaTranscoderLock.lock();
		
		if(isReleased)
		{
			Log.w(TAG, "MediaTranscoder has released!!");
			mMediaTranscoderLock.unlock();
			return;
		}
		
		if(!isStarted)
		{
			Log.w(TAG, "MediaTranscoder has not started!!");
			mMediaTranscoderLock.unlock();
			return;
		}
		
		if(!isPaused)
		{
			Native_Pause();
			isPaused = true;
		}
		mMediaTranscoderLock.unlock();
	}
	
	public void resume()
	{
		mMediaTranscoderLock.lock();
		
		if(isReleased)
		{
			Log.w(TAG, "MediaTranscoder has released!!");
			mMediaTranscoderLock.unlock();
			return;
		}
		
		if(!isStarted)
		{
			Log.w(TAG, "MediaTranscoder has not started!!");
			mMediaTranscoderLock.unlock();
			return;
		}
		
		if(isPaused)
		{
			Native_Resume();
			isPaused = false;
		}
		
		mMediaTranscoderLock.unlock();
	}
	
	public void stop(boolean isCancel)
	{
		mMediaTranscoderLock.lock();
		
		if(isReleased)
		{
			Log.w(TAG, "MediaTranscoder has released!!");
			mMediaTranscoderLock.unlock();
			return;
		}

		if(!isStarted)
		{
			Log.w(TAG, "MediaTranscoder has stopped");
			mMediaTranscoderLock.unlock();
			return;
		}
		
		Native_Stop(isCancel);
	
		isStarted = false;
		isPaused = false;
		
		mMediaTranscoderLock.unlock();
	}
	
	private boolean isReleased = false;
	private boolean isFinishAllCallbacksAndMessages = false;
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2) public void release()
	{
		stop(false);
		
		mMediaTranscoderLock.lock();
		if(isReleased)
		{
			Log.w(TAG, "MediaTranscoder has released!!");
			mMediaTranscoderLock.unlock();
			return;
		}
		
		mediaTranscoderCallbackHandler.post(new Runnable() {
			@Override
			public void run() {
				mediaTranscoderCallbackHandler.removeCallbacksAndMessages(null);
				
				mMediaTranscoderLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mMediaTranscoderCondition.signalAll();
				mMediaTranscoderLock.unlock();
			}
		});
		
		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mMediaTranscoderCondition.await(10, TimeUnit.MILLISECONDS);
			}
			
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
		}
		
		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR2)
		{
			mHandlerThread.quitSafely();
		}else{
			mHandlerThread.quit();
		}
		
		isReleased = true;
		
		Log.d(TAG, "Finish MediaTranscoder Release");

		mMediaTranscoderLock.unlock();
	}
	
	public static final int MAX_VIDEO_BITRATE = 8*1024; //Kbps
	public static final int MAX_AUDIO_BITRATE = 128; //Kbps
 	public static boolean isNeedMediaTranscoder(String videoFilePath)
 	{
 		long videoFileSizeKB = VideoFileUtils.getVideoFileSizeKB(videoFilePath);
 		long videoFileDurationMs = VideoFileUtils.getVideoFileDurationMs(videoFilePath);
 		Log.d(TAG, "videoFileSizeKB : " + String.valueOf(videoFileSizeKB));
 		Log.d(TAG, "videoFileDurationMs : " + String.valueOf(videoFileDurationMs));
 		
 		if(videoFileDurationMs!=0)
 		{
 	 		long bitrateKbps = videoFileSizeKB * 8  * 1000 / videoFileDurationMs;
 	 		if(bitrateKbps<=MAX_VIDEO_BITRATE+MAX_AUDIO_BITRATE) return false;
 		}

 		return true;
 	}
 	
 	public static final int MAX_VIDEO_SIDE_SIZE = 1920;
 	public static void makeVideoEncodeSize(MediaTranscoderOptions options, MediaInfo mediaInfo)
 	{
 		if(options.hasVideo)
 		{
			if(mediaInfo==null)
			{
				mediaInfo = VideoFileUtils.getMediaInfo(options.inputUrl);
			}
			
			if(mediaInfo.videoRotation==90 || mediaInfo.videoRotation==270)
			{
				int tmp = mediaInfo.videoWidth;
				mediaInfo.videoWidth = mediaInfo.videoHeight;
				mediaInfo.videoHeight = tmp;
			}
			
			int publishVideoWidth = 0;
			int publishVideoHeight = 0;
			if(mediaInfo.videoWidth >= mediaInfo.videoHeight)
			{
				publishVideoWidth = MAX_VIDEO_SIDE_SIZE;
				publishVideoHeight = publishVideoWidth * mediaInfo.videoHeight / mediaInfo.videoWidth;
				
				if(publishVideoHeight%2!=0)
				{
					publishVideoHeight = publishVideoHeight + 1;
				}
			}else {
				publishVideoHeight = MAX_VIDEO_SIDE_SIZE;
				publishVideoWidth = mediaInfo.videoWidth * publishVideoHeight / mediaInfo.videoHeight;
				
				if(publishVideoWidth%2!=0)
				{
					publishVideoWidth = publishVideoWidth + 1;
				}
			}
			
			options.outputVideoWidth = publishVideoWidth;
			options.outputVideoHeight = publishVideoHeight;
 		}
 	}
 	
 	public static void makeVideoEncodeBitrate(MediaTranscoderOptions options)
 	{
 	    if (options.outputVideoWidth<=640 && options.outputVideoHeight<=640) {
 	    	options.outputVideoBitrateKbps = 1024;
 	    }else if (options.outputVideoWidth<=960 && options.outputVideoHeight<=960) {
 	    	options.outputVideoBitrateKbps = 2*1024;
 	    }else if (options.outputVideoWidth<=1280 && options.outputVideoHeight<=1280) {
 	    	options.outputVideoBitrateKbps = 4*1024;
 	    }else if (options.outputVideoWidth<=1600 && options.outputVideoHeight<=1600) {
 	    	options.outputVideoBitrateKbps = 6*1024;
 	    }else if (options.outputVideoWidth<=1920 && options.outputVideoHeight<=1920) {
 	    	options.outputVideoBitrateKbps = 8*1024;
 	    }else {
 	    	options.outputVideoBitrateKbps = MAX_VIDEO_BITRATE;
 	    }
 	}
 	
 	public static final int DEFAULT_VIDEO_FPS = 24;
 	public static final int MAX_VIDEO_FPS = 30;
	
	//media_transcoder_event_type
	public final static int CALLBACK_MEDIA_TRANSCODER_CONNECTING = 0;
	public final static int CALLBACK_MEDIA_TRANSCODER_CONNECTED = 1;
	public final static int CALLBACK_MEDIA_TRANSCODER_STREAMING = 2;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR = 3;
	public final static int CALLBACK_MEDIA_TRANSCODER_INFO = 4;
	public final static int CALLBACK_MEDIA_TRANSCODER_END = 5;
	public final static int CALLBACK_MEDIA_TRANSCODER_PAUSED = 6;
	
	//media_transcoder_info_type 
	public final static int CALLBACK_MEDIA_TRANSCODER_INFO_PUBLISH_DELAY_TIME = 1;
	public final static int CALLBACK_MEDIA_TRANSCODER_INFO_PUBLISH_REAL_BITRATE = 2;
	public final static int CALLBACK_MEDIA_TRANSCODER_INFO_PUBLISH_REAL_FPS = 3;
	public final static int CALLBACK_MEDIA_TRANSCODER_INFO_PUBLISH_DOWN_BITRATE = 4;
	public final static int CALLBACK_MEDIA_TRANSCODER_INFO_PUBLISH_UP_BITRATE = 5;
	public final static int CALLBACK_MEDIA_TRANSCODER_INFO_PUBLISH_TIME = 6;
	public final static int CALLBACK_MEDIA_TRANSCODER_INFO_PUBLISH_DURATION = 7;

	public final static int CALLBACK_MEDIA_TRANSCODER_WARN_AUDIO_DECODE_FAIL = 100;
	public final static int CALLBACK_MEDIA_TRANSCODER_WARN_VIDEO_DECODE_FAIL = 101;
	
	//media_transcoder_error_type
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_UNKNOWN = -1;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_CONNECT_FAIL = 0;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_OPEN_VIDEO_ENCODER_FAIL = 1;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_OPEN_AUDIO_ENCODER_FAIL = 2;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_VIDEO_FILTER_FAIL = 3;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_AUDIO_FILTER_FAIL = 4;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_VIDEO_ENCODE_FAIL = 5;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_AUDIO_ENCODE_FAIL = 6;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_AUDIO_MUX_FAIL = 7;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_VIDEO_MUX_FAIL = 8;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_AUDIO_FIFO_READ_FAIL = 9;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_POOR_NETWORK = 10;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_DEMUXER_READ_FAIL = 100;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_NO_MEMORY = 200;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_HW_OPEN_VIDEO_DECODER_FAIL = 1000;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_HW_FIND_START_POS_FAIL = 1001;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_TWO_TO_ONE_REMUXER_FAIL = 1002;
	public final static int CALLBACK_MEDIA_TRANSCODER_ERROR_HW_OPEN_VIDEO_ENCODER_FAIL = 1003;
	
	//flip
	public final static int NO_FLIP = 0;
	public final static int VFLIP = 1;
	public final static int HFLIP = 2;
}
