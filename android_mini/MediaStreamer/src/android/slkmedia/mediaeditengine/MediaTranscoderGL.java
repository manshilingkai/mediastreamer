package android.slkmedia.mediaeditengine;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.opengl.GLES20;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.slkmedia.mediaeditengine.VideoFileUtils.MediaInfo;
import android.slkmedia.mediastreamer.MediaMuxerListener;
import android.slkmedia.mediastreamer.MediaStreamer;
import android.slkmedia.mediastreamer.SLKMediaMuxer;
import android.slkmedia.mediastreamer.VideoEncoderCore;
import android.slkmedia.mediastreamer.VideoOptions;
import android.slkmedia.mediastreamer.SLKMediaMuxer.OutputFormat;
import android.slkmedia.mediastreamer.egl.EGL;
import android.slkmedia.mediastreamer.gpuimage.GPUImageAmaroFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageAntiqueFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBeautyFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBlackCatFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBrannanFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBrooklynFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageCoolFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageCrayonFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageInputFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageN1977Filter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageRGBFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageRotationMode;
import android.slkmedia.mediastreamer.gpuimage.GPUImageSketchFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageUtils;
import android.slkmedia.mediastreamer.gpuimage.OpenGLUtils;
import android.slkmedia.mediastreamer.gpuimage.TextureRotationUtil;
import android.slkmedia.mediastreamer.utils.FileUtils;
import android.util.Log;
import android.view.Surface;

public class MediaTranscoderGL {
	private static final String TAG = "MediaTranscoderGL";

	private Context mContext = null;
	public MediaTranscoderGL(Context context)
	{
		mContext = context;
	}
	
	private MediaTranscoderListener mMediaTranscoderListener = null;
	public void setMediaTranscoderListener(MediaTranscoderListener listener)
	{
		mMediaTranscoderListener = listener;
	}
	
	private MediaTranscoderOptions mMediaTranscoderOptions = null;
	private boolean isWorking = false;
	public void startTranscode(MediaTranscoderOptions options)
	{
		mMediaTranscoderOptions = options;
		
		if(isWorking) return;
		createWorkThread();
		isWorking = true;
	}
	
	public void release()
	{
		if(!isWorking) return;
		deleteWorkThread();
		isWorking = false;
	}
	
	///////////////////////////////////////////////////////////// GPUImage[OpenGLES] //////////////////////////////////////////////////////////////
	private FloatBuffer mGLCubeBuffer;
	private FloatBuffer mGLTextureBuffer;
    private float[] rotatedTex;
	
	private GPUImageInputFilter mInputFilter = null;
	private GPUImageFilter mWorkFilter = null;
	
    private void initGPUImageParam()
    {
		mGLCubeBuffer = ByteBuffer.allocateDirect(TextureRotationUtil.CUBE.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLCubeBuffer.put(TextureRotationUtil.CUBE).position(0);

        rotatedTex = new float[8];
        TextureRotationUtil.calculateCropTextureCoordinates(GPUImageRotationMode.kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        
        mGLTextureBuffer = ByteBuffer.allocateDirect(8 * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLTextureBuffer.put(rotatedTex).position(0);
    	
        mInputFilter = new GPUImageInputFilter();
        mWorkFilter = new GPUImageRGBFilter();
    }
    
	private Surface mInputSurface = null;
	private SurfaceTexture mInputSurfaceTexture = null;
	private int mInputTextureId = OpenGLUtils.NO_TEXTURE;
	private void openGPUImageWorker(int inputVideoWidth, int inputVideoHeight)
	{
    	if(mInputTextureId == OpenGLUtils.NO_TEXTURE) {
    		mInputTextureId = OpenGLUtils.getExternalOESTextureID();
    		mInputSurfaceTexture = new SurfaceTexture(mInputTextureId);
    		mInputSurfaceTexture.setDefaultBufferSize(inputVideoWidth, inputVideoHeight);
    		mInputSurfaceTexture.setOnFrameAvailableListener(new OnFrameAvailableListener(){
				@Override
				public void onFrameAvailable(SurfaceTexture surfaceTexture) {
//					Log.d(TAG, "On Handle Input Video Frame");
					if(mWorkHandler!=null)
					{
						mWorkHandler.post(mWriteRunnable);
					}
				}
			});
    		mInputSurface = new Surface(mInputSurfaceTexture); //mInputSurface.release();
    	}
		
    	mInputFilter.init();
        mWorkFilter.init();
        
        isOutputSizeUpdated = true;
	}
	
	private boolean isGPUImageWorkerOpened = false;
	private void closeGPUImageWorker()
	{
	    if(mInputTextureId != OpenGLUtils.NO_TEXTURE)
	    {
	    	GLES20.glDeleteTextures(1, new int[]{mInputTextureId}, 0);
	    	mInputTextureId = OpenGLUtils.NO_TEXTURE;
	    }
	    
	    if(mInputSurfaceTexture!=null)
	    {
	    	mInputSurfaceTexture.release();
	    	mInputSurfaceTexture = null;
	    }
	    
	    if(mInputSurface!=null)
	    {
	    	mInputSurface.release();
	    	mInputSurface = null;
	    }
		
    	mInputFilter.destroy();
    	mWorkFilter.destroy();
    	
        isOutputSizeUpdated = false;
        outputWidth = -1;
        outputHeight = -1;
	}
	
	private void switchFilter(int type, String filterDir)
	{
    	if(mWorkFilter!=null)
    	{
    		mWorkFilter.destroy();
    		mWorkFilter = null;
    	}
    	
        switch (type) {
        case GPUImageUtils.FILTER_SKETCH:
        	mWorkFilter = new GPUImageSketchFilter();
        	mWorkFilter.init();
            break;
        case GPUImageUtils.FILTER_AMARO:
        	mWorkFilter = new GPUImageAmaroFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_ANTIQUE:
        	mWorkFilter = new GPUImageAntiqueFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BEAUTY:
        	mWorkFilter = new GPUImageBeautyFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BLACKCAT:
        	mWorkFilter = new GPUImageBlackCatFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BRANNAN:
        	mWorkFilter = new GPUImageBrannanFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BROOKLYN:
        	mWorkFilter = new GPUImageBrooklynFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_COOL:
        	mWorkFilter = new GPUImageCoolFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_CRAYON:
        	mWorkFilter = new GPUImageCrayonFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_N1977:
        	mWorkFilter = new GPUImageN1977Filter(filterDir);
        	mWorkFilter.init();
        	break;
        default:
            mWorkFilter = new GPUImageRGBFilter();
            mWorkFilter.init();
            break;
        }
        isOutputSizeUpdated = true;
	}
	
	private int outputWidth = -1;
	private int outputHeight = -1;
	private boolean isOutputSizeUpdated = false;
	private void ScaleToFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
	{
	    int src_width;
	    int src_height;
	    
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
	    {
	        src_width = videoHeight;
	        src_height = videoWidth;
	    }else{
	        src_width = videoWidth;
	        src_height = videoHeight;
	    }
	    
	    videoWidth = src_width;
	    videoHeight = src_height;
	    
	    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
	        outputWidth = videoWidth;
	        outputHeight = videoHeight;
	        
	        isOutputSizeUpdated = true;
	    }
	    
	    if (isOutputSizeUpdated) {
	        isOutputSizeUpdated = false;
	        mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
	    }
	    	    
	    GLES20.glClearColor(0.f, 0.f, 0.f, 0.f);
	    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	    
	    GLES20.glViewport(displayX, displayY, displayWidth, displayHeight);
	    
	    TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
	    mGLTextureBuffer.put(rotatedTex).position(0);
	}
	
    private void ScaleAspectFit(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
        int x = 0;
        int y = 0;
        int w = 0;
        int h = 0;
        
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        videoWidth = src_width;
        videoHeight = src_height;
        
        if(displayWidth*videoHeight>videoWidth*displayHeight)
        {
            int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
            int viewPortVideoHeight = displayHeight;
            
            x = displayX+(displayWidth - viewPortVideoWidth)/2;
            y = displayY;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }else
        {
            int viewPortVideoWidth = displayWidth;
            int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
            
            x = displayX;
            y = displayY+(displayHeight - viewPortVideoHeight)/2;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }
        
        if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
            outputWidth = videoWidth;
            outputHeight = videoHeight;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
                
        GLES20.glViewport(x, y, w, h);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
    
    private void ScaleAspectFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        int dst_width = displayWidth;
        int dst_height = displayHeight;
        
        int crop_x;
        int crop_y;
        
        int crop_width;
        int crop_height;
        
        if(src_width*dst_height>dst_width*src_height)
        {
            crop_width = dst_width*src_height/dst_height;
            crop_height = src_height;
            
            crop_x = (src_width - crop_width)/2;
            crop_y = 0;

        }else if(src_width*dst_height<dst_width*src_height)
        {
            crop_width = src_width;
            crop_height = dst_height*src_width/dst_width;
            
            crop_x = 0;
            crop_y = (src_height - crop_height)/2;
        }else {
            crop_width = src_width;
            crop_height = src_height;
            crop_x = 0;
            crop_y = 0;
        }
        
        float minX = (float)crop_x/(float)src_width;
        float minY = (float)crop_y/(float)src_height;
        float maxX = 1.0f - minX;
        float maxY = 1.0f - minY;
        
        if (outputWidth!=crop_width || outputHeight!=crop_height) {
            outputWidth = crop_width;
            outputHeight = crop_height;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(displayX, displayY, displayWidth, displayHeight);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
    
	private float[] mTransformMatrix = new float[16];
	private boolean onDrawFrame(int inputVideoWidth, int inputVideoHeight, int displayX, int displayY, int displayWidth, int displayHeight, int videoScalingModeForWorkFilter, GPUImageRotationMode rotationModeForWorkFilter) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
		if(mInputTextureId == OpenGLUtils.NO_TEXTURE || mInputSurfaceTexture==null) return false;
		if(inputVideoWidth<=0 || inputVideoHeight<=0) return false;
		if(displayWidth<=0 || displayHeight<=0) return false;
		
		mInputSurfaceTexture.updateTexImage();
		mInputSurfaceTexture.getTransformMatrix(mTransformMatrix);
		mInputFilter.setTextureTransformMatrix(mTransformMatrix);
		int inputTextureID = OpenGLUtils.NO_TEXTURE;
		try {
	        inputTextureID = mInputFilter.onDrawToTexture(mInputTextureId, inputVideoWidth, inputVideoHeight);
		}catch(java.lang.NullPointerException e){
			inputTextureID = OpenGLUtils.NO_TEXTURE;
		}

        if(inputTextureID == OpenGLUtils.NO_TEXTURE) return false;
        
        if(videoScalingModeForWorkFilter == GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT)
        {
        	ScaleAspectFit(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, inputVideoWidth, inputVideoHeight);
        }else if(videoScalingModeForWorkFilter == GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)
        {
        	ScaleAspectFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, inputVideoWidth, inputVideoHeight);
        }else{
        	ScaleToFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, inputVideoWidth, inputVideoHeight);
        }
		mWorkFilter.onDrawFrame(inputTextureID, mGLCubeBuffer, mGLTextureBuffer);
		return true;
	}
	
	////////////////////////////////////////////////////////////////////////// Video Frame Reader ////////////////////////////////////////////
	private MediaExtractor mMediaExtractor = null;
	private int mInputVideoWidth = 0, mInputVideoHeight = 0;
	private int mInputVideoFps = MediaTranscoder.DEFAULT_VIDEO_FPS;
	private long mInputDurationMs = 0;
	private MediaFormat mVideoFormat = null;
	private boolean hasAudioTrack = false;
	private boolean createVideoExtractor(String inputVideoFilePath)
	{
		mMediaExtractor = null;
		mInputVideoWidth = 0;
		mInputVideoHeight = 0;
		mInputVideoFps = MediaTranscoder.DEFAULT_VIDEO_FPS;
		mInputDurationMs = 0;
		mVideoFormat = null;
		
		mMediaExtractor = new MediaExtractor();
        try {
        	mMediaExtractor.setDataSource(inputVideoFilePath);
		} catch (IOException e) {
			mMediaExtractor.release();
			mMediaExtractor = null;
			return false;
		}
        
        int trackCount = mMediaExtractor.getTrackCount();
        for (int i = 0; i < trackCount; i++) {
            MediaFormat trackFormat = mMediaExtractor.getTrackFormat(i);
            if (trackFormat.getString(MediaFormat.KEY_MIME).contains("audio")) {
            	hasAudioTrack = true;
                break;
            }
        }
        for (int i = 0; i < trackCount; i++) {
            MediaFormat trackFormat = mMediaExtractor.getTrackFormat(i);
            if (trackFormat.getString(MediaFormat.KEY_MIME).contains("video")) {
            	mVideoFormat = trackFormat;
                mMediaExtractor.selectTrack(i);
                break;
            }
        }
        
        if(mVideoFormat==null)
        {
        	mMediaExtractor.release();
        	mMediaExtractor = null;
        	return false;
        }
        
        mInputVideoWidth = mVideoFormat.getInteger(MediaFormat.KEY_WIDTH);
        mInputVideoHeight = mVideoFormat.getInteger(MediaFormat.KEY_HEIGHT);
        
        int rotation = 0;
        if(mVideoFormat.containsKey(MediaFormat.KEY_ROTATION)){
             rotation = mVideoFormat.getInteger(MediaFormat.KEY_ROTATION);
        }
        if (rotation<0) {
        	rotation = 360 + rotation;
        }
        if(rotation == 90 || rotation == 270)
        {
        	mInputVideoWidth = mVideoFormat.getInteger(MediaFormat.KEY_HEIGHT);
        	mInputVideoHeight = mVideoFormat.getInteger(MediaFormat.KEY_WIDTH);
        }
        
        MediaInfo mediaInfo = new MediaInfo();
        mediaInfo.videoWidth = mInputVideoWidth;
        mediaInfo.videoHeight = mInputVideoHeight;
		MediaTranscoder.makeVideoEncodeSize(mMediaTranscoderOptions, mediaInfo);
		MediaTranscoder.makeVideoEncodeBitrate(mMediaTranscoderOptions);
        
		mInputVideoFps = MediaTranscoder.DEFAULT_VIDEO_FPS;
		if(mVideoFormat.containsKey(MediaFormat.KEY_FRAME_RATE))
		{
			mInputVideoFps = mVideoFormat.getInteger(MediaFormat.KEY_FRAME_RATE);
		}
        
        if(mInputVideoFps<=0)
        {
        	mOutputVideoFps = MediaTranscoder.DEFAULT_VIDEO_FPS;
        }else{
        	mOutputVideoFps = mInputVideoFps;
        }
        
        if(mOutputVideoFps>MediaTranscoder.MAX_VIDEO_FPS)
        {
        	mOutputVideoFps = MediaTranscoder.MAX_VIDEO_FPS;
        	
            isNeedDropVideoFrame = true;
            targetVideoTimeStampMs = 0;
        }else{
            isNeedDropVideoFrame = false;
        }
        
        mInputDurationMs = 0;
        if(mVideoFormat.containsKey(MediaFormat.KEY_DURATION))
        {
        	try {
        		mInputDurationMs = mVideoFormat.getLong(MediaFormat.KEY_DURATION)/1000;
        	}catch (java.lang.NullPointerException e){
        		
        	}
        }
 
        return true;
	}
	
	private MediaCodec mMediaCodec = null;
	private boolean createVideoDecoder(MediaFormat videoFormat, Surface inputSurface)
	{
        if(Build.VERSION.SDK_INT>=21)
        {
            int colorFormat = MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Flexible;
            videoFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, colorFormat);
        }
		
        try {
        	mMediaCodec = MediaCodec.createDecoderByType(videoFormat.getString(MediaFormat.KEY_MIME));
		} catch (IOException e) {
        	return false;
		}
        
        mMediaCodec.configure(videoFormat, inputSurface, null, 0);
        mMediaCodec.start();
        
        return true;
	}
	
    private MediaCodec.BufferInfo mBufferInfo = new MediaCodec.BufferInfo();
    private long mTimeOut = 5 * 1000; //5ms
    private ByteBuffer[] mInputBuffers = null;
    private boolean mGotPresentationTimeUsBaseLineForInputVideo = false;
    private long mPresentationTimeUsBaseLineForInputVideo = 0;
    boolean mInputDone = false;
    boolean mOutputDone = false;
    // 1 : find & no render
    // 0 : find & render
	// -2 : EOS
    // -3 : Exit
	private int findStartPosForInputVideo(long startPosMs, long endPosMs)
	{
		mInputBuffers = null;
		mGotPresentationTimeUsBaseLineForInputVideo = false;
		mPresentationTimeUsBaseLineForInputVideo = 0;
		mInputDone = false;
		mOutputDone = false;
		
        long baselineTime = mMediaExtractor.getSampleTime();
        if(baselineTime==-1) {
            mInputDone = true;
            mOutputDone = true;
            Log.d(TAG, "input video file has no any video data");
            return -2;
        }
        
        if(!mGotPresentationTimeUsBaseLineForInputVideo)
        {
        	mGotPresentationTimeUsBaseLineForInputVideo = true;
        	mPresentationTimeUsBaseLineForInputVideo = baselineTime;
        }
		
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
        	mInputBuffers = mMediaCodec.getInputBuffers();
        }
        
        if(startPosMs>0 && endPosMs>0 && endPosMs - startPosMs>0){
        	mMediaExtractor.seekTo(startPosMs*1000, MediaExtractor.SEEK_TO_PREVIOUS_SYNC);
        }else return 1;

        int ret = -1;
        while(!mOutputDone)
        {
    		mWorkLock.lock();
    		if(isBreakWorkThread)
    		{
    			ret = -3;
        		mWorkLock.unlock();
    			break;
    		}
    		mWorkLock.unlock();
        	
        	if(!mInputDone)
        	{
        		//feed data
                int inputBufferIndex = mMediaCodec.dequeueInputBuffer(mTimeOut);
                if (inputBufferIndex >= 0) {
                    ByteBuffer inputBuffer;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        inputBuffer = mMediaCodec.getInputBuffer(inputBufferIndex);
                    } else {
                        inputBuffer = mInputBuffers[inputBufferIndex];
                    }
                    int sampleSize = mMediaExtractor.readSampleData(inputBuffer, 0);
                    if(sampleSize>=0)
                    {
                        long sampleTime = mMediaExtractor.getSampleTime();
//                        Log.d(TAG, "Sample Time : " + String.valueOf(sampleTime));
                        mMediaCodec.queueInputBuffer(inputBufferIndex, 0, sampleSize, sampleTime, 0);
                        mMediaExtractor.advance();
                    }else{
                    	//eof
                    	mMediaCodec.queueInputBuffer(inputBufferIndex, 0, 0, 0L, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                        mInputDone = true;
                        Log.d(TAG, "input EOS");
                    }
                }
        	}
        	if(!mOutputDone)
        	{
                //drain data
                int status = mMediaCodec.dequeueOutputBuffer(mBufferInfo, mTimeOut);
                if (status == MediaCodec.INFO_TRY_AGAIN_LATER) {
                } else if (status == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                } else if (status == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                } else {
                    if ((mBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                        Log.d(TAG, "output EOS");
                        mOutputDone = true;
                    }
                    boolean doRender = (mBufferInfo.size != 0);
                    long presentationTimeUs = mBufferInfo.presentationTimeUs;
                    
//                    Log.d(TAG, "presentationTimeUs : " + String.valueOf(presentationTimeUs));
                    
                    if((presentationTimeUs - mPresentationTimeUsBaseLineForInputVideo)/1000 < startPosMs)
                    {
                    	doRender = false;
                        mMediaCodec.releaseOutputBuffer(status, doRender);
                        continue;
                    }else if((presentationTimeUs - mPresentationTimeUsBaseLineForInputVideo)/1000 >= startPosMs && (presentationTimeUs - mPresentationTimeUsBaseLineForInputVideo)/1000 <= endPosMs)
                    {
                    	doRender = true;
//                        mMediaCodec.releaseOutputBuffer(status, doRender);
                    	mMediaCodec.releaseOutputBuffer(status, presentationTimeUs - mPresentationTimeUsBaseLineForInputVideo - startPosMs*1000);
                    	ret = 0;
                    	break;
                    }else{
                    	doRender = false;
                        mMediaCodec.releaseOutputBuffer(status, doRender);
                        mOutputDone = true;
                        continue;
                    }
                }
        	}else{
        		ret = -2;
        		break;
        	}
        }
        
        return ret;
	}
	
	// 0 : read
	// -2 : EOS
	private int mOutputVideoFps = MediaTranscoder.DEFAULT_VIDEO_FPS;
	private boolean isNeedDropVideoFrame = false;
	private int targetVideoTimeStampMs = 0;
	private int readVideoFrameForInputVideo(long startPosMs, long endPosMs)
	{
//		Log.d(TAG, "readVideoFrameForInputVideo");
        int ret = -1;
        while(!mOutputDone)
        {
        	if(!mInputDone)
        	{
        		//feed data
                int inputBufferIndex = mMediaCodec.dequeueInputBuffer(mTimeOut);
                if (inputBufferIndex >= 0) {
                    ByteBuffer inputBuffer;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        inputBuffer = mMediaCodec.getInputBuffer(inputBufferIndex);
                    } else {
                        inputBuffer = mInputBuffers[inputBufferIndex];
                    }
                    int sampleSize = mMediaExtractor.readSampleData(inputBuffer, 0);
                    if(sampleSize>=0)
                    {
                        long sampleTime = mMediaExtractor.getSampleTime();
//                        Log.d(TAG, "Sample Time : " + String.valueOf(sampleTime));
                        mMediaCodec.queueInputBuffer(inputBufferIndex, 0, sampleSize, sampleTime, 0);
                        mMediaExtractor.advance();
                    }else{
                    	//eof
                    	mMediaCodec.queueInputBuffer(inputBufferIndex, 0, 0, 0L, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                        mInputDone = true;
                        Log.d(TAG, "input EOS");
                    }
                }
        	}
        	if(!mOutputDone)
        	{
                //drain data
                int status = mMediaCodec.dequeueOutputBuffer(mBufferInfo, mTimeOut);
                if (status == MediaCodec.INFO_TRY_AGAIN_LATER) {
                } else if (status == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                } else if (status == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                } else {
                    if ((mBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                        Log.d(TAG, "output EOS");
                        mOutputDone = true;
                    }
                    boolean doRender = (mBufferInfo.size != 0);
                    long presentationTimeUs = mBufferInfo.presentationTimeUs;
                    
//                    Log.d(TAG, "presentationTimeUs : " + String.valueOf(presentationTimeUs));
                    
                    if(startPosMs>=0 && endPosMs>0 && endPosMs - startPosMs>0)
                    {
                        if((presentationTimeUs - mPresentationTimeUsBaseLineForInputVideo)/1000 < startPosMs)
                        {
                        	doRender = false;
                            mMediaCodec.releaseOutputBuffer(status, doRender);
                            continue;
                        }else if((presentationTimeUs - mPresentationTimeUsBaseLineForInputVideo)/1000 >= startPosMs && (presentationTimeUs - mPresentationTimeUsBaseLineForInputVideo)/1000 <= endPosMs)
                        {
                        	boolean isDrop = false;
                        	if(isNeedDropVideoFrame) {
                        		if(presentationTimeUs - mPresentationTimeUsBaseLineForInputVideo - startPosMs*1000 >= targetVideoTimeStampMs*1000)
                        		{
                        			isDrop = false;
                        			targetVideoTimeStampMs += 1000/mOutputVideoFps;
                        		}else {
                                    isDrop = true;
                        		}
                        	}
                        	
                        	if(isDrop)
                        	{
                            	doRender = false;
                                mMediaCodec.releaseOutputBuffer(status, doRender);
                                continue;
                        	}
                        	
                        	doRender = true;
//                            mMediaCodec.releaseOutputBuffer(status, doRender);
                        	mMediaCodec.releaseOutputBuffer(status, presentationTimeUs - mPresentationTimeUsBaseLineForInputVideo - startPosMs*1000);
                        	ret = 0;
                        	break;
                        }else{
                        	doRender = false;
                            mMediaCodec.releaseOutputBuffer(status, doRender);
                            mOutputDone = true;
                            continue;
                        }
                    }else{
                    	boolean isDrop = false;
                    	if(isNeedDropVideoFrame) {
                    		if(presentationTimeUs - mPresentationTimeUsBaseLineForInputVideo >= targetVideoTimeStampMs*1000)
                    		{
                    			isDrop = false;
                    			targetVideoTimeStampMs += 1000/mOutputVideoFps;
                    		}else {
                                isDrop = true;
                    		}
                    	}
                    	
                    	if(isDrop)
                    	{
                        	doRender = false;
                            mMediaCodec.releaseOutputBuffer(status, doRender);
                            continue;
                    	}
                    	
                    	doRender = true;
//                        mMediaCodec.releaseOutputBuffer(status, doRender);
                    	mMediaCodec.releaseOutputBuffer(status, presentationTimeUs - mPresentationTimeUsBaseLineForInputVideo - startPosMs*1000);
                    	ret = 0;
                    	break;
                    }
                }
        	}else{
        		ret = -2;
        		break;
        	}
        }
        
        if(mOutputDone)
        {
        	ret = -2;
        }
        
        return ret;
	}
	
	private void deleteVideoDecoder()
	{
        if (mMediaCodec != null) {
        	mMediaCodec.stop();
        	mMediaCodec.release();
        	mMediaCodec = null;
        }
	}
	
	private void deleteVideoExtractor()
	{
        if (mMediaExtractor != null) {
        	mMediaExtractor.release();
        	mMediaExtractor = null;
        }
	}
	
	//////////////////////////////////////////////////////////////////////- Video Frame Writer -///////////////////////////////////////////////////////
	private SLKMediaMuxer mSLKMediaMuxer = null;
	private void createVideoMuxer(String publishUrl)
	{
		// Start MediaMuxer
		int type = SLKMediaMuxer.SYSTEM_MEDIAMUXER_TYPE;
		int format = OutputFormat.MUXER_OUTPUT_MPEG_4;
		int track = SLKMediaMuxer.ONLY_VIDEO_TRACK;
		
		mSLKMediaMuxer = new SLKMediaMuxer(type, publishUrl, format, track, mVideoProfile);
		mSLKMediaMuxer.setMediaMuxerListener(mMediaMuxerListener);
	}
	
	private void deleteVideoMuxer()
	{
		// Release MediaMuxer
		if(mSLKMediaMuxer!=null)
		{
			mSLKMediaMuxer.stop();
			mSLKMediaMuxer.release();
			mSLKMediaMuxer = null;
		}
	}
	
	private int mSurfaceWidth = 0, mSurfaceHeight = 0;
	private VideoOptions mVideoOptions = null;
    private VideoEncoderCore mVideoEncoderCore = null;
    private Surface mEncodeSurface = null;
    private int mVideoProfile = 0; //0:base_line 1:main_profile 2:high_profile
	private void createVideoEncoder(int outputVideoWidth, int outputVideoHeight, int outputVideoFps, int outputVideoBitrate)
	{
		mVideoOptions = new VideoOptions();
		mVideoOptions.hasVideo = true;
		mVideoOptions.videoEncodeType = MediaStreamer.VIDEO_HARD_ENCODE;

		mVideoOptions.videoWidth = outputVideoWidth;
		mVideoOptions.videoHeight = outputVideoHeight;
		mVideoOptions.videoFps = outputVideoFps;
		mVideoOptions.videoRawType = MediaStreamer.VIDEOFRAME_RAWTYPE_NV21;

		mVideoOptions.videoProfile = mVideoProfile; 
		mVideoOptions.videoBitRate = outputVideoBitrate;
		mVideoOptions.encodeMode = 0; //0:VBR or 1:CBR
		mVideoOptions.maxKeyFrameIntervalMs = 5000;

		mVideoOptions.quality = 0;
		mVideoOptions.bStrictCBR = false;
		mVideoOptions.deblockingFilterFactor = 0;
		
		mSurfaceWidth = mVideoOptions.videoWidth;
		mSurfaceHeight = mVideoOptions.videoHeight;
		
		// Start VideoEncoder
		mVideoEncoderCore = new VideoEncoderCore(mVideoOptions, mSLKMediaMuxer);
    	mEncodeSurface = mVideoEncoderCore.getInputSurface();
	}
	
	private void deleteVideoEncoder()
	{
		// Release VideoEncoder
		if(mVideoEncoderCore!=null)
		{
			mVideoEncoderCore.drainEncoder(true);
			mVideoEncoderCore.release();
			mVideoEncoderCore = null;
		}
	}
	
	private EGL mEGL = null;
	private void createRenderEnv()
	{
		initGPUImageParam();

    	//Create EGL
		if(mEGL==null)
		{
			mEGL = new EGL();
			mEGL.Egl_Initialize(null, true);
			
	        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
		}
		mEGL.Egl_AttachToSurface(mEncodeSurface);
		
		if(!isGPUImageWorkerOpened)
		{
			openGPUImageWorker(mInputVideoWidth, mInputVideoHeight);
			isGPUImageWorkerOpened = true;
		}
	}
	
	private void deleteRenderEnv()
	{
		if(isGPUImageWorkerOpened)
		{
			closeGPUImageWorker();
			isGPUImageWorkerOpened = false;
		}
		
		// Release EGL
		if(mEGL!=null)
		{
			mEGL.Egl_DetachFromSurfaceTexture();
			mEGL.Egl_Terminate();
			mEGL = null;
		}
	}
	
	private void writeVideoFrame()
	{
//		Log.d(TAG, "writeVideoFrame");
		mVideoEncoderCore.drainEncoder(false);
		boolean ret = onDrawFrame(mInputVideoWidth, mInputVideoHeight, 0 , 0, mSurfaceWidth, mSurfaceHeight, GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT, GPUImageRotationMode.kGPUImageNoRotation);
		if(ret) {
//			Log.d(TAG, "InputSurfaceTexture Timestamp : " + String.valueOf(mInputSurfaceTexture.getTimestamp()));
			mEGL.Egl_SetPresentationTime(mInputSurfaceTexture.getTimestamp()*1000);
			mEGL.Egl_SwapBuffers();
		}
	}
	
	///////////////////////////////////////////////////////////////// - Work Thread - //////////////////////////////////////////////////////////
	
	private HandlerThread mWorkHandlerThread = null;
	private Handler mWorkHandler = null;
	private Lock mWorkLock = null;
	private Condition mWorkCondition = null;
	private boolean isBreakWorkThread = false;
	private void createWorkThread()
	{
		mWorkHandlerThread = new HandlerThread("MediaTranscoderGL");
		mWorkHandlerThread.start();
		
		mWorkHandler = new Handler(mWorkHandlerThread.getLooper());
		
		mWorkLock = new ReentrantLock();
		mWorkCondition = mWorkLock.newCondition();
		
		isBreakWorkThread = false;
		isFinishAllCallbacksAndMessages = false;
		
		startWorkRunnable();
	}
	
	private boolean isFinishAllCallbacksAndMessages = false;
	private void deleteWorkThread()
	{
		mWorkLock.lock();
		isBreakWorkThread = true;
		mWorkHandler.postAtFrontOfQueue(new Runnable() {
			@Override
			public void run() {
				deleteVideoDecoder();
				deleteRenderEnv();
				deleteVideoEncoder();
				deleteVideoMuxer();
				deleteVideoExtractor();
				
				mWorkHandler.removeCallbacksAndMessages(null);
				
				if(mTwoToOneRemuxer!=null)
				{
					mTwoToOneRemuxer.release();
					mTwoToOneRemuxer = null;
				}
				
				FileUtils.deleteFile(tempFilePath);
//				FileUtils.deleteFile(mMediaTranscoderOptions.outputUrl);
				
				mWorkLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mWorkCondition.signalAll();
				mWorkLock.unlock();
			}
		});

		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mWorkCondition.await(10, TimeUnit.MILLISECONDS);
			}
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
		}
    	
		mWorkHandler.removeCallbacksAndMessages(null);
		
		if(Build.VERSION.SDK_INT>=/*Build.VERSION_CODES.JELLY_BEAN_MR2*/18)
		{
			mWorkHandlerThread.quitSafely();
		}else{
			mWorkHandlerThread.quit();
		}
		
		mWorkLock.unlock();
	}
	
	private void startWorkRunnable()
	{
		mWorkHandler.post(mInitRunnable);
	}
	
	private String tempFilePath = null;
	private Runnable mInitRunnable = new Runnable() {

		@Override
		public void run() {
			boolean ret = createVideoExtractor(mMediaTranscoderOptions.inputUrl);
			if(!ret)
			{
				Log.e(TAG, "createVideoExtractor fail");
				
				if(mMediaTranscoderListener!=null)
				{
					mMediaTranscoderListener.onMediaTranscoderError(MediaTranscoder.CALLBACK_MEDIA_TRANSCODER_ERROR_CONNECT_FAIL);
				}
				return;
			}
			
//			if(mMediaTranscoderOptions.outputUrl.endsWith(".mp4") || mMediaTranscoderOptions.outputUrl.endsWith(".MP4") || mMediaTranscoderOptions.outputUrl.endsWith(".mov") || mMediaTranscoderOptions.outputUrl.endsWith(".MOV"))
//			{
//				if(mMediaTranscoderOptions.outputUrl.endsWith(".mp4"))
//				{
//					tempFilePath = new String(mMediaTranscoderOptions.outputUrl).replace(".mp4", "_tmp.mp4");
//				}else if(mMediaTranscoderOptions.outputUrl.endsWith(".MP4"))
//				{
//					tempFilePath = new String(mMediaTranscoderOptions.outputUrl).replace(".MP4", "_tmp.MP4");
//				}else if(mMediaTranscoderOptions.outputUrl.endsWith(".mov"))
//				{
//					tempFilePath = new String(mMediaTranscoderOptions.outputUrl).replace(".mov", "_tmp.mov");
//				}else if(mMediaTranscoderOptions.outputUrl.endsWith(".MOV"))
//				{
//					tempFilePath = new String(mMediaTranscoderOptions.outputUrl).replace(".MOV", "_tmp.MOV");
//				}
//			}else{
//				tempFilePath = mContext.getCacheDir().getAbsolutePath() + "/MediaTranscoderGL_TMP.mp4";
//			}
			
			tempFilePath = mContext.getCacheDir().getAbsolutePath() + "/MediaTranscoderGL_TMP.mp4";
			FileUtils.deleteFile(tempFilePath);
			
			createVideoMuxer(tempFilePath);
			
			try {
				createVideoEncoder(mMediaTranscoderOptions.outputVideoWidth, mMediaTranscoderOptions.outputVideoHeight, mOutputVideoFps, mMediaTranscoderOptions.outputVideoBitrateKbps);
			}catch (android.media.MediaCodec.CodecException e) {
				Log.e(TAG, "createVideoEncoder fail");
				
				deleteVideoEncoder();
				
				deleteVideoMuxer();
				
				deleteVideoExtractor();
				
				FileUtils.deleteFile(tempFilePath);
				
				if(mMediaTranscoderListener!=null)
				{
					mMediaTranscoderListener.onMediaTranscoderError(MediaTranscoder.CALLBACK_MEDIA_TRANSCODER_ERROR_HW_OPEN_VIDEO_ENCODER_FAIL);
				}
				return;
			}

			createRenderEnv();
			
			ret = createVideoDecoder(mVideoFormat, mInputSurface);
			if(!ret)
			{
				Log.e(TAG, "createVideoDecoder fail");
				
				deleteRenderEnv();
				
				deleteVideoEncoder();
				
				deleteVideoMuxer();
				
				deleteVideoExtractor();
				
				mWorkHandler.removeCallbacks(mWriteRunnable);
				
				FileUtils.deleteFile(tempFilePath);
				
				if(mMediaTranscoderListener!=null)
				{
					mMediaTranscoderListener.onMediaTranscoderError(MediaTranscoder.CALLBACK_MEDIA_TRANSCODER_ERROR_HW_OPEN_VIDEO_DECODER_FAIL);
				}
				return;
			}
			
			if(mMediaTranscoderListener!=null)
			{
				if(mMediaTranscoderOptions.startPosMs>=0 && mMediaTranscoderOptions.endPosMs>0 && mMediaTranscoderOptions.endPosMs - mMediaTranscoderOptions.startPosMs>0)
				{
					mMediaTranscoderListener.onMediaTranscoderInfo(MediaTranscoder.CALLBACK_MEDIA_TRANSCODER_INFO_PUBLISH_DURATION, (int)(mMediaTranscoderOptions.endPosMs - mMediaTranscoderOptions.startPosMs));
				}else{
					mMediaTranscoderListener.onMediaTranscoderInfo(MediaTranscoder.CALLBACK_MEDIA_TRANSCODER_INFO_PUBLISH_DURATION, (int)(mInputDurationMs));
				}
			}
			
			int i_ret = findStartPosForInputVideo(mMediaTranscoderOptions.startPosMs, mMediaTranscoderOptions.endPosMs);
			if(i_ret==-2 || i_ret==-3)
			{
				deleteVideoDecoder();
				
				deleteRenderEnv();
				
				deleteVideoEncoder();
				
				deleteVideoMuxer();
				
				deleteVideoExtractor();
				
				mWorkHandler.removeCallbacks(mWriteRunnable);
				
				FileUtils.deleteFile(tempFilePath);
				
				if(i_ret==-3)
				{
					Log.w(TAG, "Exit MediaTranscoder");
				}else if(i_ret==-2)
				{
					Log.e(TAG, "EOS, FindStartPosForInputVideo Fail");

					if(mMediaTranscoderListener!=null)
					{
						mMediaTranscoderListener.onMediaTranscoderError(MediaTranscoder.CALLBACK_MEDIA_TRANSCODER_ERROR_HW_FIND_START_POS_FAIL);
					}
				}
								
				return;
			}else if(i_ret==1){
				mWorkHandler.post(mReadRunnable);
			}
		}
	};
	
	private Runnable mReadRunnable = new Runnable(){
		@Override
		public void run() {
			int ret = readVideoFrameForInputVideo(mMediaTranscoderOptions.startPosMs, mMediaTranscoderOptions.endPosMs);
			if(ret==-2)
			{
				deleteVideoDecoder();
				
				deleteRenderEnv();
				
				deleteVideoEncoder();
				
				deleteVideoMuxer();
				
				deleteVideoExtractor();
				
				mWorkHandler.removeCallbacks(mWriteRunnable);
				
				Log.d(TAG, "Video Transcode Finish");
				
				if(hasAudioTrack)
				{
					mWorkHandler.post(mTwoToOneRemuxerRunnable);
				}else{
					mWorkHandler.post(finishRunnableWithoutAudioTrack);
				}
				
				return;
			}
		}
	};
	
	private Runnable mWriteRunnable = new Runnable(){

		@Override
		public void run() {
			writeVideoFrame();
			mWorkHandler.post(mReadRunnable);
		}
	};
	
	private TwoToOneRemuxer mTwoToOneRemuxer = null;
	private Runnable mTwoToOneRemuxerRunnable = new Runnable(){
		@Override
		public void run() {
			TwoToOneRemuxerOptions options = new TwoToOneRemuxerOptions();
			options.inputVideoUrl = tempFilePath;
			options.inputAudioUrl = mMediaTranscoderOptions.inputUrl;
			options.startPosMsForInputAudioUrl = mMediaTranscoderOptions.startPosMs;
			options.endPosMsForInputAudioUrl = mMediaTranscoderOptions.endPosMs;
			options.outputUrl = mMediaTranscoderOptions.outputUrl;
			
			mTwoToOneRemuxer = new TwoToOneRemuxer();
			mTwoToOneRemuxer.setTwoToOneRemuxerListener(new TwoToOneRemuxerListener() {

				@Override
				public void onTwoToOneRemuxerStart() {
					Log.d(TAG, "onTwoToOneRemuxerStart");
				}

				@Override
				public void onTwoToOneRemuxerError(int errorType) {
					Log.d(TAG, "onTwoToOneRemuxerError ErrorType : " + errorType);
					mWorkHandler.post(onTwoToOneRemuxerErrorRunnable);
				}

				@Override
				public void onTwoToOneRemuxerInfo(int infoType, int infoValue) {
				}

				@Override
				public void onTwoToOneRemuxerEnd() {
					Log.d(TAG, "onTwoToOneRemuxerEnd");
					mWorkHandler.post(finishRunnable);
				}
				
			});
			mTwoToOneRemuxer.start(options);
		}
	};
	
	private Runnable onTwoToOneRemuxerErrorRunnable = new Runnable(){

		@Override
		public void run() {
			if(mTwoToOneRemuxer!=null)
			{
				mTwoToOneRemuxer.release();
				mTwoToOneRemuxer = null;
			}
			
			FileUtils.deleteFile(tempFilePath);
			
			if(mMediaTranscoderListener!=null)
			{
				mMediaTranscoderListener.onMediaTranscoderError(MediaTranscoder.CALLBACK_MEDIA_TRANSCODER_ERROR_TWO_TO_ONE_REMUXER_FAIL);
			}
		}
	};
	
	private Runnable finishRunnable = new Runnable(){

		@Override
		public void run() {
			if(mTwoToOneRemuxer!=null)
			{
				mTwoToOneRemuxer.release();
				mTwoToOneRemuxer = null;
			}
			
			FileUtils.deleteFile(tempFilePath);
			
			if(mMediaTranscoderListener!=null)
			{
				mMediaTranscoderListener.onMediaTranscoderEnd();
			}			
		}
	};
	
	private Runnable finishRunnableWithoutAudioTrack = new Runnable(){

		@Override
		public void run() {
			FileUtils.copyNio(tempFilePath, mMediaTranscoderOptions.outputUrl);
			
			FileUtils.deleteFile(tempFilePath);
			
			if(mMediaTranscoderListener!=null)
			{
				mMediaTranscoderListener.onMediaTranscoderEnd();
			}			
		}
	};
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	private MediaMuxerListener mMediaMuxerListener = new MediaMuxerListener()
	{
		@Override
		public void onMediaMuxerConnecting() {
			if(mMediaTranscoderListener!=null)
			{
				mMediaTranscoderListener.onMediaTranscoderConnecting();
			}
		}

		@Override
		public void onMediaMuxerConnected() {
			if(mMediaTranscoderListener!=null)
			{
				mMediaTranscoderListener.onMediaTranscoderConnected();
			}
		}

		@Override
		public void onMediaMuxerStreaming() {
			if(mMediaTranscoderListener!=null)
			{
				mMediaTranscoderListener.onMediaTranscoderTranscoding();
			}
		}

		@Override
		public void onMediaMuxerError(int errorType) {
			if(mMediaTranscoderListener!=null)
			{
				if(errorType == SLKMediaMuxer.CALLBACK_MEDIA_MUXER_ERROR_UNKNOWN)
				{
					mMediaTranscoderListener.onMediaTranscoderError(MediaTranscoder.CALLBACK_MEDIA_TRANSCODER_ERROR_UNKNOWN);
				}
				
				if(errorType == SLKMediaMuxer.CALLBACK_MEDIA_MUXER_ERROR_CONNECT_FAIL)
				{
					mMediaTranscoderListener.onMediaTranscoderError(MediaTranscoder.CALLBACK_MEDIA_TRANSCODER_ERROR_CONNECT_FAIL);
				}
				
				if(errorType == SLKMediaMuxer.CALLBACK_MEDIA_MUXER_ERROR_MUX_FAIL)
				{
					mMediaTranscoderListener.onMediaTranscoderError(MediaTranscoder.CALLBACK_MEDIA_TRANSCODER_ERROR_VIDEO_MUX_FAIL);
				}
			}
		}

		@Override
		public void onMediaMuxerInfo(int infoType, int infoValue) {
			if(mMediaTranscoderListener!=null)
			{
				if(infoType == SLKMediaMuxer.CALLBACK_MEDIA_MUXER_INFO_PUBLISH_TIME)
				{
					mMediaTranscoderListener.onMediaTranscoderInfo(MediaTranscoder.CALLBACK_MEDIA_TRANSCODER_INFO_PUBLISH_TIME, infoValue * 100);
				}
			}
		}

		@Override
		public void onMediaMuxerEnd() {
		}
	};
}
