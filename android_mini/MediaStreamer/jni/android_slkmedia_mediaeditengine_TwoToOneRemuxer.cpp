// Created by Think on 2019/7/16.
// Copyright © 2019年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaeditengine_TwoToOneRemuxer
#define _Included_android_slkmedia_mediaeditengine_TwoToOneRemuxer

#include <assert.h>

#include "JNIHelp.h"
#include "TwoToOneRemuxer.h"
#include "MediaLog.h"

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;
static jmethodID post_event = NULL;

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_TwoToOneRemuxer_Native_1Init
  (JNIEnv *env, jclass thiz)
{
	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/mediaeditengine/TwoToOneRemuxer");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/mediaeditengine/TwoToOneRemuxer");
        return;
    }
    
    context = env->GetFieldID(clazz, "mNativeContext", "J");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find TwoToOneRemuxer.mNativeContext");
        return;
    }

	post_event = env->GetStaticMethodID(clazz, "postEventFromNative",
			"(Ljava/lang/Object;IIILjava/lang/Object;)V");
	if (post_event == NULL) {
		jniThrowRuntimeException(env, "Can't find TwoToOneRemuxer.postEventFromNative");
		return;
	}

	env->DeleteLocalRef(clazz);
}


JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_TwoToOneRemuxer_Native_1Start
  (JNIEnv *env, jobject thiz, jstring jInputVideoUrl, jstring jInputAudioUrl, jlong jStartPosMsForInputAudioUrl, jlong jEndPosMsForInutAudioUrl, jstring jOutputUrl, jobject weak_this)
{
	TwoToOneRemuxer* twoToOneRemuxer = (TwoToOneRemuxer*)env->GetLongField(thiz, context);
    if (twoToOneRemuxer!=NULL) return;

    const char *inputVideoUrl;
    inputVideoUrl = env->GetStringUTFChars(jInputVideoUrl, NULL);

    const char *inputAudioUrl;
    inputAudioUrl = env->GetStringUTFChars(jInputAudioUrl, NULL);

    const char *outputUrl;
    outputUrl = env->GetStringUTFChars(jOutputUrl, NULL);

    TTORVideoMaterial videoMaterial;
    TTORAudioMaterial audioMaterial;
    TTORProduct product;

    videoMaterial.url = inputVideoUrl;
    audioMaterial.url = inputAudioUrl;
    audioMaterial.startPosMs = jStartPosMsForInputAudioUrl;
    audioMaterial.endPosMs = jEndPosMsForInutAudioUrl;
    product.url = outputUrl;

    twoToOneRemuxer = new TwoToOneRemuxer(jvm, videoMaterial, audioMaterial, product);
    twoToOneRemuxer->setListener(thiz, weak_this, post_event);
    twoToOneRemuxer->start();

    env->SetLongField(thiz, context, (int64_t)twoToOneRemuxer);

    env->ReleaseStringUTFChars(jInputVideoUrl, inputVideoUrl);
    env->ReleaseStringUTFChars(jInputAudioUrl, inputAudioUrl);
    env->ReleaseStringUTFChars(jOutputUrl, outputUrl);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_TwoToOneRemuxer_Native_1Pause
  (JNIEnv *env, jobject thiz)
{
	TwoToOneRemuxer* twoToOneRemuxer = (TwoToOneRemuxer*)env->GetLongField(thiz, context);
    if (twoToOneRemuxer==NULL) return;
    
    twoToOneRemuxer->pause();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_TwoToOneRemuxer_Native_1Resume
  (JNIEnv *env, jobject thiz)
{
	TwoToOneRemuxer* twoToOneRemuxer = (TwoToOneRemuxer*)env->GetLongField(thiz, context);
    if (twoToOneRemuxer==NULL) return;

    twoToOneRemuxer->resume();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_TwoToOneRemuxer_Native_1Stop
  (JNIEnv *env, jobject thiz, jboolean jIsCancel)
{
	TwoToOneRemuxer* twoToOneRemuxer = (TwoToOneRemuxer*)env->GetLongField(thiz, context);
    if (twoToOneRemuxer==NULL) return;

    if(jIsCancel==JNI_TRUE)
    {
    	twoToOneRemuxer->stop(true);
    }else{
    	twoToOneRemuxer->stop(false);
    }

    delete twoToOneRemuxer;
    twoToOneRemuxer = NULL;

    env->SetLongField(thiz, context, (int64_t)0);
}

#ifdef __cplusplus
}
#endif

#endif
