#  Created by Think on 16/2/25.
#  Copyright © 2016年 Cell. All rights reserved.

LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
    X264_BASE := $(LOCAL_PATH)/../../../ThirdParty/x264/android/build/x264-armv7/output
    FFMPEG_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg/android/build/ffmpeg-armv7a/output
    LIBYUV_BASE := $(LOCAL_PATH)/../../../ThirdParty/libyuv/libyuv/android/armeabi-v7a
    LIBFAAC_BASE := $(LOCAL_PATH)/../../../ThirdParty/faac/android/build/faac-armv7/output
    LIBFDKAAC_BASE := $(LOCAL_PATH)/../../../ThirdParty/fdk-aac/android/fdk-aac-0.1.5/output/armeabi-v7a
    LIBPNG_BASE := $(LOCAL_PATH)/../../../ThirdParty/libpng-android/android/armeabi-v7a
    GIFH_BASE := $(LOCAL_PATH)/../../../ThirdParty/gif-h
    GIFFLEN_BASE := $(LOCAL_PATH)/../../../ThirdParty/gifflen
    FREETYPE_BASE := $(LOCAL_PATH)/../../../ThirdParty/freetype2-android/output/armeabi-v7a
endif

ifeq ($(TARGET_ARCH_ABI),x86)
    X264_BASE := $(LOCAL_PATH)/../../../ThirdParty/x264/android/build/x264-x86/output
    FFMPEG_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg/android/build/ffmpeg-x86/output
    LIBYUV_BASE := $(LOCAL_PATH)/../../../ThirdParty/libyuv//libyuv/android/x86
    LIBFAAC_BASE := $(LOCAL_PATH)/../../../ThirdParty/faac/android/build/faac-x86/output
    LIBFDKAAC_BASE := $(LOCAL_PATH)/../../../ThirdParty/fdk-aac/android/fdk-aac-0.1.5/output/x86
    LIBPNG_BASE := $(LOCAL_PATH)/../../../ThirdParty/libpng-android/android/x86
    GIFH_BASE := $(LOCAL_PATH)/../../../ThirdParty/gif-h
    GIFFLEN_BASE := $(LOCAL_PATH)/../../../ThirdParty/gifflen
    FREETYPE_BASE := $(LOCAL_PATH)/../../../ThirdParty/freetype2-android/output/x86
endif

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
    X264_BASE := $(LOCAL_PATH)/../../../ThirdParty/x264/android/build/x264-arm64/output
    FFMPEG_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg/android/build/ffmpeg-arm64-v8a/output
    LIBYUV_BASE := $(LOCAL_PATH)/../../../ThirdParty/libyuv/libyuv/android/arm64-v8a
    LIBFAAC_BASE := $(LOCAL_PATH)/../../../ThirdParty/faac/android/build/faac-arm64/output
    LIBFDKAAC_BASE := $(LOCAL_PATH)/../../../ThirdParty/fdk-aac/android/fdk-aac-0.1.5/output/arm64-v8a
    LIBPNG_BASE := $(LOCAL_PATH)/../../../ThirdParty/libpng-android/android/arm64-v8a
    GIFH_BASE := $(LOCAL_PATH)/../../../ThirdParty/gif-h
    GIFFLEN_BASE := $(LOCAL_PATH)/../../../ThirdParty/gifflen
    FREETYPE_BASE := $(LOCAL_PATH)/../../../ThirdParty/freetype2-android/output/arm64-v8a
endif

EIGEN_BASE := $(LOCAL_PATH)/../../../ThirdParty/eigen

############################### X264 #############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(X264_BASE)/lib/libx264.a
LOCAL_MODULE := x264
include $(PREBUILT_STATIC_LIBRARY)

############################### FFMPEG ############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(FFMPEG_BASE)/libffmpeg_ypp.so
LOCAL_MODULE := ffmpeg_ypp
include $(PREBUILT_SHARED_LIBRARY)

############################### LIBYUV ############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBYUV_BASE)/lib/libyuv_static.a
LOCAL_MODULE := yuv_static
include $(PREBUILT_STATIC_LIBRARY)

############################### FAAC ##############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBFAAC_BASE)/lib/libfaac.a
LOCAL_MODULE := faac
include $(PREBUILT_STATIC_LIBRARY)

############################### FDK-AAC ###########################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBFDKAAC_BASE)/lib/libFraunhoferAAC.a
LOCAL_MODULE := FraunhoferAAC
include $(PREBUILT_STATIC_LIBRARY)

############################### LIBPNG ############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBPNG_BASE)/lib/libpng.a
LOCAL_MODULE := png
include $(PREBUILT_STATIC_LIBRARY)

############################### FREETYPE ############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(FREETYPE_BASE)/lib/libfreetype2-static.a
LOCAL_MODULE := freetype
include $(PREBUILT_STATIC_LIBRARY)

###################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(X264_BASE)/include \
	$(FFMPEG_BASE)/include \
	$(LIBYUV_BASE)/include \
    $(LIBFAAC_BASE)/include \
    $(LIBFDKAAC_BASE)/include \
    $(LIBPNG_BASE)/include \
    $(GIFH_BASE) \
    $(GIFFLEN_BASE) \
   	$(FREETYPE_BASE)/include \
    $(EIGEN_BASE) \
    $(LOCAL_PATH)/../../../ThirdParty/musly/include \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/libresample \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/methods \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/kissfft \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/decoders \
    $(LOCAL_PATH)/../../../ThirdParty/dubbing_score \
	$(LOCAL_PATH)/../../../MediaBase \
	$(LOCAL_PATH)/../../../MediaFile \
	$(LOCAL_PATH)/../../../MediaUtils \
	$(LOCAL_PATH)/../../../MediaUtils/android \
	$(LOCAL_PATH)/../../../MediaDebug \
    $(LOCAL_PATH)/../../../MediaDebug/android \
    $(LOCAL_PATH)/../../../MediaListener \
    $(LOCAL_PATH)/../../../MediaListener/android \
    $(LOCAL_PATH)/../../../MediaDataPool \
    $(LOCAL_PATH)/../../../MediaPreprocess \
    $(LOCAL_PATH)/../../../MediaPreprocess/NoiseSuppression \
    $(LOCAL_PATH)/../../../MediaCapture \
    $(LOCAL_PATH)/../../../MediaCapture/android \
    $(LOCAL_PATH)/../../../MediaRender \
    $(LOCAL_PATH)/../../../MediaRender/android \
    $(LOCAL_PATH)/../../../MediaEncoder \
	$(LOCAL_PATH)/../../../MediaEncoder/android \
    $(LOCAL_PATH)/../../../MediaMuxer \
    $(LOCAL_PATH)/../../../Gif \
    $(LOCAL_PATH)/../../../MediaEditEngine \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode \
	$(LOCAL_PATH)/../../../MediaStreamer \
	$(LOCAL_PATH)/../../../MediaProcesser \
	$(LOCAL_PATH)/../../../GPUImageFilter \
    $(LOCAL_PATH)/ \

LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/libresample/resamplesubs.c \
	$(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/libresample/resample.c \
	$(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/libresample/filterkit.c \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/methods/timbre.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/methods/mandelellis.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/kissfft/kiss_fftr.c \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/kissfft/kiss_fft.c \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/decoders/libav.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/windowfunction.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/resampler.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/powerspectrum.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/plugins.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/mutualproximity.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/mfcc.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/method.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/melspectrum.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/lib.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/gaussianstatistics.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/discretecosinetransform.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/musly/libmusly/decoder.cpp \
    $(LOCAL_PATH)/../../../ThirdParty/dubbing_score/dubbing_score.cpp \
	$(LOCAL_PATH)/../../../MediaBase/TimedEventQueue.cpp \
	$(LOCAL_PATH)/../../../MediaBase/MediaTime.cpp \
	$(LOCAL_PATH)/../../../MediaBase/MediaTimer.cpp \
	$(LOCAL_PATH)/../../../MediaBase/MediaMath.cpp \
	$(LOCAL_PATH)/../../../MediaFile/MediaFile.cpp \
    $(LOCAL_PATH)/../../../MediaFile/MediaDir.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/AVCUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/FileUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/StringUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/android/AndroidUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/android/DeviceInfo.cpp \
    $(LOCAL_PATH)/../../../MediaDebug/MediaLog.cpp \
    $(LOCAL_PATH)/../../../MediaDebug/FFLog.cpp \
    $(LOCAL_PATH)/../../../MediaDebug/android/JNIHelp.cpp \
    $(LOCAL_PATH)/../../../MediaListener/IMediaListener.cpp \
    $(LOCAL_PATH)/../../../MediaListener/NormalMediaListener.cpp \
    $(LOCAL_PATH)/../../../MediaListener/NotificationQueue.cpp \
    $(LOCAL_PATH)/../../../MediaListener/android/JniMediaListener.cpp \
    $(LOCAL_PATH)/../../../MediaDataPool/AudioFrameBufferPool.cpp \
    $(LOCAL_PATH)/../../../MediaDataPool/VideoFrameBufferPool.cpp \
    $(LOCAL_PATH)/../../../MediaDataPool/MediaPacketQueue.cpp \
    $(LOCAL_PATH)/../../../MediaPreprocess/NoiseSuppression/lpcx.cpp \
    $(LOCAL_PATH)/../../../MediaPreprocess/NoiseSuppression/click_suppression.cpp \
    $(LOCAL_PATH)/../../../MediaPreprocess/NoiseSuppression/noise_suppression.cpp \
    $(LOCAL_PATH)/../../../MediaPreprocess/ColorSpaceConvert.cpp \
    $(LOCAL_PATH)/../../../MediaPreprocess/LibyuvColorSpaceConvert.cpp \
    $(LOCAL_PATH)/../../../MediaPreprocess/AudioNoiseSuppression.cpp \
    $(LOCAL_PATH)/../../../MediaPreprocess/Overlay.cpp \
    $(LOCAL_PATH)/../../../MediaCapture/AudioCapture.cpp \
    $(LOCAL_PATH)/../../../MediaCapture/ExternalAudioInputPlus.cpp \
    $(LOCAL_PATH)/../../../MediaCapture/DiscreteExternalAudioInput.cpp \
    $(LOCAL_PATH)/../../../MediaCapture/android/OpenSlesInput.cpp \
    $(LOCAL_PATH)/../../../MediaCapture/android/JniAudioCapturer.cpp \
    $(LOCAL_PATH)/../../../MediaRender/AudioRender.cpp \
    $(LOCAL_PATH)/../../../MediaRender/android/OpenSLESRender.cpp \
    $(LOCAL_PATH)/../../../MediaRender/android/JniAudioTrackRender.cpp \
    $(LOCAL_PATH)/../../../MediaEncoder/AudioEncoder.cpp \
    $(LOCAL_PATH)/../../../MediaEncoder/FDKAACEncoder.cpp \
    $(LOCAL_PATH)/../../../MediaEncoder/FAACEncoder.cpp \
    $(LOCAL_PATH)/../../../MediaEncoder/VideoEncoder.cpp \
    $(LOCAL_PATH)/../../../MediaEncoder/X264Encoder.cpp \
    $(LOCAL_PATH)/../../../MediaEncoder/android/MediaCodecEncoder.cpp \
    $(LOCAL_PATH)/../../../MediaMuxer/FFMediaWriter.cpp \
    $(LOCAL_PATH)/../../../MediaMuxer/FFMediaMuxer.cpp \
    $(LOCAL_PATH)/../../../MediaMuxer/MediaMuxer.cpp \
    $(LOCAL_PATH)/../../../MediaMuxer/FFmpegWriter.cpp \
    $(LOCAL_PATH)/../../../MediaMuxer/FFmpegMuxer.cpp \
    $(GIFFLEN_BASE)/dib.cpp \
    $(GIFFLEN_BASE)/gifflen.cpp \
    $(LOCAL_PATH)/../../../Gif/AnimatedGifCreater.cpp \
    $(LOCAL_PATH)/../../../Gif/GifHAnimatedGifCreater.cpp \
    $(LOCAL_PATH)/../../../Gif/GifflenAnimatedGifCreater.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode/MediaDataStruct.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode/MediaFrameQueue.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode/MediaFrameOutputer.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode/DefaultMediaFrameOutputer.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode/MediaTranscoder.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode/DefaultMediaTranscoder.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode/CPUOverlayCreater.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode/MTWordToBitmap.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/PCMUtils.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/WAVFile.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/IPCMReader.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/FFAudioFileReader.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/PCMPlayer.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MicrophonePCMRecorder.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MicrophoneAudioRecorder.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/AudioPlayer.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaDubbingProducer.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/AudioSimilarity.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MusicSimilarity.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/DubbingScore.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/AudioRemuxer.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/TwoToOneRemuxer.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaFileUtils.cpp \
    $(LOCAL_PATH)/../../../MediaStreamer/MediaStreamer.cpp \
    $(LOCAL_PATH)/../../../MediaStreamer/AudioStreamer.cpp \
    $(LOCAL_PATH)/../../../MediaStreamer/SLKAudioStreamer.cpp \
    $(LOCAL_PATH)/../../../MediaStreamer/SLKMediaStreamer.cpp \
    $(LOCAL_PATH)/../../../MediaStreamer/AnimatedImageMediaStreamer.cpp \
    $(LOCAL_PATH)/../../../MediaProcesser/ImageProcesserUtils.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageTwoInputFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageNormalBlendFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageTransformFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/Matrix.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/LinkedList.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/OpenGLUtils.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/Runnable.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/TextureRotationUtil.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUOverlay.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediastreamer_audiocapture_AudioCapturer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaeditengine_audiorender_AudioTrackRender.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaeditengine_AudioPlayer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaeditengine_MicrophoneAudioRecorder.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaeditengine_MediaDubbingProducer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaeditengine_DubbingScore.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaeditengine_MediaTranscoder.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaeditengine_AudioRemuxer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaeditengine_TwoToOneRemuxer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaeditengine_GPUOverlay.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaprocesser_FFMediaWriter.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediastreamer_FFMediaMuxer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediastreamer_MediaStreamer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaeditengine_VideoFileUtils.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediastreamer.cpp \

LOCAL_CFLAGS += -DANDROID -D__STDC_FORMAT_MACROS -DBUILD_STATIC

ifeq ($(TARGET_ARCH_ABI),x86)
LOCAL_CFLAGS += -DANDROID_X86
endif

LOCAL_LDLIBS := -llog -lOpenSLES -lz -lEGL -lGLESv2 -ljnigraphics

LOCAL_STATIC_LIBRARIES := libx264 libyuv_static libfaac libFraunhoferAAC libpng libfreetype cpufeatures

LOCAL_SHARED_LIBRARIES := ffmpeg_ypp

LOCAL_MODULE := MediaStreamer

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/cpufeatures)