// Created by Think on 16/2/25.
// Copyright © 2016年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediastreamer_MediaStreamer
#define _Included_android_slkmedia_mediastreamer_MediaStreamer

#include <assert.h>

#include "JNIHelp.h"
#include "SLKMediaStreamer.h"
#include "MediaLog.h"

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;
static jmethodID post_event = NULL;

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1Init
  (JNIEnv *env, jclass thiz)
{
	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/mediastreamer/MediaStreamer");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/mediastreamer/MediaStreamer");
        return;
    }
    
    context = env->GetFieldID(clazz, "mNativeContext", "J");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find MediaStreamer.mNativeContext");
        return;
    }
    
	post_event = env->GetStaticMethodID(clazz, "postEventFromNative",
			"(Ljava/lang/Object;IIILjava/lang/Object;)V");
	if (post_event == NULL) {
		jniThrowRuntimeException(env, "Can't find MediaStreamer.postEventFromNative");
		return;
	}

	env->DeleteLocalRef(clazz);
}


JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1Start
  (JNIEnv *env, jobject thiz, jobject jVideoOptions, jobject jAudioOptions, jstring jPublishUrl, jint jReConnectTimes, jobject weak_this)
{
    VideoOptions videoOptions;
    AudioOptions audioOptions;
    const char *publishUrl;
    
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer!=NULL) return;

    // get the java object fields
    jclass ClassVideoOptions = env->GetObjectClass(jVideoOptions);
    
    jfieldID jhasVideoId = env->GetFieldID(ClassVideoOptions, "hasVideo", "Z");
    jfieldID jVideoEncodeTypeId = env->GetFieldID(ClassVideoOptions, "videoEncodeType", "I");
    jfieldID jVideoHeightId = env->GetFieldID(ClassVideoOptions, "videoHeight", "I");
    jfieldID jVideoWidthId = env->GetFieldID(ClassVideoOptions, "videoWidth", "I");
    jfieldID jVideoFpsId = env->GetFieldID(ClassVideoOptions, "videoFps", "I");
    jfieldID jVideoRawTypeId = env->GetFieldID(ClassVideoOptions, "videoRawType", "I");
    jfieldID jVideoProfileId = env->GetFieldID(ClassVideoOptions, "videoProfile", "I");
    jfieldID jVideoBitRateId = env->GetFieldID(ClassVideoOptions, "videoBitRate", "I");
    jfieldID jEncodeModeId = env->GetFieldID(ClassVideoOptions, "encodeMode", "I");
    jfieldID jQualityId = env->GetFieldID(ClassVideoOptions, "quality", "I");
    jfieldID jMaxKeyFrameIntervalMsId = env->GetFieldID(ClassVideoOptions, "maxKeyFrameIntervalMs", "I");
    jfieldID jBStrictCBRId = env->GetFieldID(ClassVideoOptions, "bStrictCBR", "Z");
    jfieldID jDeblockingFilterFactorId = env->GetFieldID(ClassVideoOptions, "deblockingFilterFactor", "I");
    
    videoOptions.hasVideo = env->GetBooleanField(jVideoOptions, jhasVideoId);
    videoOptions.videoEncodeType = env->GetIntField(jVideoOptions, jVideoEncodeTypeId);
    videoOptions.videoHeight = env->GetIntField(jVideoOptions, jVideoHeightId);
    videoOptions.videoWidth = env->GetIntField(jVideoOptions, jVideoWidthId);
    videoOptions.videoFps = env->GetIntField(jVideoOptions, jVideoFpsId);
    videoOptions.videoRawType = env->GetIntField(jVideoOptions, jVideoRawTypeId);
    videoOptions.videoProfile = env->GetIntField(jVideoOptions, jVideoProfileId);
    videoOptions.videoBitRate = env->GetIntField(jVideoOptions, jVideoBitRateId);
    videoOptions.encodeMode = env->GetIntField(jVideoOptions, jEncodeModeId);
    videoOptions.quality = env->GetIntField(jVideoOptions, jQualityId);
    videoOptions.maxKeyFrameIntervalMs = env->GetIntField(jVideoOptions, jMaxKeyFrameIntervalMsId);
    videoOptions.bStrictCBR = env->GetBooleanField(jVideoOptions, jBStrictCBRId);
    videoOptions.deblockingFilterFactor = env->GetIntField(jVideoOptions, jDeblockingFilterFactorId);

    jclass ClassAudioOptions = env->GetObjectClass(jAudioOptions);
    
    jfieldID jhasAudioId = env->GetFieldID(ClassAudioOptions, "hasAudio", "Z");
    jfieldID jAudioSampleRateId = env->GetFieldID(ClassAudioOptions, "audioSampleRate", "I");
    jfieldID jAudioNumChannelsId = env->GetFieldID(ClassAudioOptions, "audioNumChannels", "I");
    jfieldID jAudioBitRateId = env->GetFieldID(ClassAudioOptions, "audioBitRate", "I");
    jfieldID jIsExternalAudioInput = env->GetFieldID(ClassAudioOptions, "isExternalAudioInput", "Z");
    
    audioOptions.hasAudio = env->GetBooleanField(jAudioOptions, jhasAudioId);
    audioOptions.audioSampleRate = env->GetIntField(jAudioOptions, jAudioSampleRateId);
    audioOptions.audioNumChannels = env->GetIntField(jAudioOptions, jAudioNumChannelsId);
    audioOptions.audioBitRate = env->GetIntField(jAudioOptions, jAudioBitRateId);
    audioOptions.isExternalAudioInput = env->GetBooleanField(jAudioOptions, jIsExternalAudioInput);
    
    publishUrl = env->GetStringUTFChars(jPublishUrl, NULL);

    SLKMediaStreamer *androidMediaStreamer = new SLKMediaStreamer(jvm, publishUrl, NULL, videoOptions, audioOptions, jReConnectTimes);
    androidMediaStreamer->setListener(thiz, weak_this, post_event);
    androidMediaStreamer->start();

    env->SetLongField(thiz, context, (int64_t)androidMediaStreamer);

    env->ReleaseStringUTFChars(jPublishUrl, publishUrl);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1StartWithEncodeSurface
  (JNIEnv *env, jobject thiz, jobject jVideoOptions, jobject jAudioOptions, jstring jPublishUrl, jint jReConnectTimes, jobject weak_this, jobject surfaceObj, jobject jEncodeSurfaceCore)
{
    VideoOptions videoOptions;
    AudioOptions audioOptions;
    const char *publishUrl;

    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer!=NULL) return;

    // get the java object fields
    jclass ClassVideoOptions = env->GetObjectClass(jVideoOptions);

    jfieldID jhasVideoId = env->GetFieldID(ClassVideoOptions, "hasVideo", "Z");
    jfieldID jVideoEncodeTypeId = env->GetFieldID(ClassVideoOptions, "videoEncodeType", "I");
    jfieldID jVideoHeightId = env->GetFieldID(ClassVideoOptions, "videoHeight", "I");
    jfieldID jVideoWidthId = env->GetFieldID(ClassVideoOptions, "videoWidth", "I");
    jfieldID jVideoFpsId = env->GetFieldID(ClassVideoOptions, "videoFps", "I");
    jfieldID jVideoRawTypeId = env->GetFieldID(ClassVideoOptions, "videoRawType", "I");
    jfieldID jVideoProfileId = env->GetFieldID(ClassVideoOptions, "videoProfile", "I");
    jfieldID jVideoBitRateId = env->GetFieldID(ClassVideoOptions, "videoBitRate", "I");
    jfieldID jEncodeModeId = env->GetFieldID(ClassVideoOptions, "encodeMode", "I");
    jfieldID jQualityId = env->GetFieldID(ClassVideoOptions, "quality", "I");
    jfieldID jMaxKeyFrameIntervalMsId = env->GetFieldID(ClassVideoOptions, "maxKeyFrameIntervalMs", "I");
    jfieldID jBStrictCBRId = env->GetFieldID(ClassVideoOptions, "bStrictCBR", "Z");
    jfieldID jDeblockingFilterFactorId = env->GetFieldID(ClassVideoOptions, "deblockingFilterFactor", "I");

    videoOptions.hasVideo = env->GetBooleanField(jVideoOptions, jhasVideoId);
    videoOptions.videoEncodeType = env->GetIntField(jVideoOptions, jVideoEncodeTypeId);
    videoOptions.videoHeight = env->GetIntField(jVideoOptions, jVideoHeightId);
    videoOptions.videoWidth = env->GetIntField(jVideoOptions, jVideoWidthId);
    videoOptions.videoFps = env->GetIntField(jVideoOptions, jVideoFpsId);
    videoOptions.videoRawType = env->GetIntField(jVideoOptions, jVideoRawTypeId);
    videoOptions.videoProfile = env->GetIntField(jVideoOptions, jVideoProfileId);
    videoOptions.videoBitRate = env->GetIntField(jVideoOptions, jVideoBitRateId);
    videoOptions.encodeMode = env->GetIntField(jVideoOptions, jEncodeModeId);
    videoOptions.quality = env->GetIntField(jVideoOptions, jQualityId);
    videoOptions.maxKeyFrameIntervalMs = env->GetIntField(jVideoOptions, jMaxKeyFrameIntervalMsId);
    videoOptions.bStrictCBR = env->GetBooleanField(jVideoOptions, jBStrictCBRId);
    videoOptions.deblockingFilterFactor = env->GetIntField(jVideoOptions, jDeblockingFilterFactorId);

    jclass ClassAudioOptions = env->GetObjectClass(jAudioOptions);

    jfieldID jhasAudioId = env->GetFieldID(ClassAudioOptions, "hasAudio", "Z");
    jfieldID jAudioSampleRateId = env->GetFieldID(ClassAudioOptions, "audioSampleRate", "I");
    jfieldID jAudioNumChannelsId = env->GetFieldID(ClassAudioOptions, "audioNumChannels", "I");
    jfieldID jAudioBitRateId = env->GetFieldID(ClassAudioOptions, "audioBitRate", "I");
    jfieldID jIsExternalAudioInput = env->GetFieldID(ClassAudioOptions, "isExternalAudioInput", "Z");

    audioOptions.hasAudio = env->GetBooleanField(jAudioOptions, jhasAudioId);
    audioOptions.audioSampleRate = env->GetIntField(jAudioOptions, jAudioSampleRateId);
    audioOptions.audioNumChannels = env->GetIntField(jAudioOptions, jAudioNumChannelsId);
    audioOptions.audioBitRate = env->GetIntField(jAudioOptions, jAudioBitRateId);
    audioOptions.isExternalAudioInput = env->GetBooleanField(jAudioOptions, jIsExternalAudioInput);

    publishUrl = env->GetStringUTFChars(jPublishUrl, NULL);

    SLKMediaStreamer *androidMediaStreamer = new SLKMediaStreamer(jvm, publishUrl, NULL, videoOptions, audioOptions, jReConnectTimes);
    androidMediaStreamer->setEncodeSurfaceCore((void*)jEncodeSurfaceCore);
    androidMediaStreamer->setEncodeSurface((void*)surfaceObj);
    androidMediaStreamer->setListener(thiz, weak_this, post_event);
    androidMediaStreamer->start();

    env->SetLongField(thiz, context, (int64_t)androidMediaStreamer);

    env->ReleaseStringUTFChars(jPublishUrl, publishUrl);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1Pause
  (JNIEnv *env, jobject thiz)
{
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;

    mediaStreamer->pause();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1Resume
  (JNIEnv *env, jobject thiz)
{
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;

    mediaStreamer->resume();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1Stop
  (JNIEnv *env, jobject thiz)
{
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;
    
    mediaStreamer->stop();
    
    delete mediaStreamer;
    mediaStreamer = NULL;
    
    env->SetLongField(thiz, context, (int64_t)0);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1EnableAudio
(JNIEnv *env, jobject thiz, jint jIsEnable)
{
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;

    if(jIsEnable)
    {
    	mediaStreamer->enableAudio(true);
    }else{
    	mediaStreamer->enableAudio(false);
    }
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1InputPreviewFrame
  (JNIEnv *env, jobject thiz, jbyteArray jData, jint jSize, jint jWidth, jint jHeight,jlong jPts, jint jRotation, jint jVideoRawType)
{
    VideoFrame videoFrame;
    
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;

    jbyte* raw_bytes = env->GetByteArrayElements(jData, NULL);
    videoFrame.data = (uint8_t*)raw_bytes;
    videoFrame.frameSize = (int)jSize;
    videoFrame.pts = (uint64_t)jPts;
    videoFrame.width = jWidth;
    videoFrame.height = jHeight;
    videoFrame.rotation = jRotation;
    videoFrame.videoRawType = jVideoRawType;

    mediaStreamer->inputVideoFrame(&videoFrame);

    env->ReleaseByteArrayElements(jData, raw_bytes, 0);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1NetWorkReachabilityNotify
(JNIEnv *env, jobject thiz, jint jNetworkStatus)
{
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;

	mediaStreamer->netWorkReachabilityNotify(jNetworkStatus);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1InputAudioFrame
  (JNIEnv *env, jobject thiz, jbyteArray jData, jint jSize, jlong jPts, jint jSampleRate, jint jChannels, jint jBitsPerChannel)
{
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;

    AudioFrame audioFrame;
    jbyte* raw_bytes = env->GetByteArrayElements(jData, NULL);
    audioFrame.data = (uint8_t*)raw_bytes;
    audioFrame.frameSize = (int)jSize;
    audioFrame.pts = (uint64_t)jPts;
    audioFrame.sampleRate = jSampleRate;
    audioFrame.channels = jChannels;
    audioFrame.bitsPerChannel = jBitsPerChannel;

    mediaStreamer->inputAudioFrame(&audioFrame);

    env->ReleaseByteArrayElements(jData, raw_bytes, 0);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1InputAudioFrameWithSourceType
  (JNIEnv *env, jobject thiz, jbyteArray jData, jint jSize, jlong jPts, jint jSampleRate, jint jChannels, jint jBitsPerChannel, jint jAudioSourceType)
{
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;

    AudioFrame audioFrame;
    jbyte* raw_bytes = env->GetByteArrayElements(jData, NULL);
    audioFrame.data = (uint8_t*)raw_bytes;
    audioFrame.frameSize = (int)jSize;
    audioFrame.pts = (uint64_t)jPts;
    audioFrame.sampleRate = jSampleRate;
    audioFrame.channels = jChannels;
    audioFrame.bitsPerChannel = jBitsPerChannel;

    mediaStreamer->inputAudioFrame(&audioFrame, jAudioSourceType);

    env->ReleaseByteArrayElements(jData, raw_bytes, 0);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1MixBGM
(JNIEnv *env, jobject thiz, jint jBgmId, jstring jBgmUrl, jfloat jVolume, jint jNumberOfLoops)
{
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;

    const char *bgmUrl = env->GetStringUTFChars(jBgmUrl, NULL);

    mediaStreamer->mixBGM(jBgmId, (char*)bgmUrl, jVolume, jNumberOfLoops);

    env->ReleaseStringUTFChars(jBgmUrl, bgmUrl);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1PlayBGM
(JNIEnv *env, jobject thiz)
{
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;

    mediaStreamer->playBGM();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1PauseBGM
(JNIEnv *env, jobject thiz)
{
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;

    mediaStreamer->pauseBGM();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1StopBGM
(JNIEnv *env, jobject thiz)
{
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;

    mediaStreamer->stopBGM();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1SetBGMVolume
(JNIEnv *env, jobject thiz, jfloat jVolume)
{
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;

    mediaStreamer->setBGMVolume(jVolume);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_MediaStreamer_Native_1MixSEM
(JNIEnv *env, jobject thiz, jint jSemId, jstring jSemUrl, jfloat jVolume, jint jNumberOfLoops)
{
    SLKMediaStreamer* mediaStreamer = (SLKMediaStreamer*)env->GetLongField(thiz, context);
    if (mediaStreamer==NULL) return;

    const char *semUrl = env->GetStringUTFChars(jSemUrl, NULL);

    mediaStreamer->mixSEM(jSemId, (char*)semUrl, jVolume, jNumberOfLoops);

    env->ReleaseStringUTFChars(jSemUrl, semUrl);
}

#ifdef __cplusplus
}
#endif

#endif
