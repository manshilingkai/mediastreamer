// Created by Think on 2019/7/16.
// Copyright © 2019年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaeditengine_AudioRemuxer
#define _Included_android_slkmedia_mediaeditengine_AudioRemuxer

#include <assert.h>

#include "JNIHelp.h"
#include "AudioRemuxer.h"
#include "MediaLog.h"

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;
static jmethodID post_event = NULL;

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioRemuxer_Native_1Init
  (JNIEnv *env, jclass thiz)
{
	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/mediaeditengine/AudioRemuxer");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/mediaeditengine/AudioRemuxer");
        return;
    }
    
    context = env->GetFieldID(clazz, "mNativeContext", "J");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find AudioRemuxer.mNativeContext");
        return;
    }

	post_event = env->GetStaticMethodID(clazz, "postEventFromNative",
			"(Ljava/lang/Object;IIILjava/lang/Object;)V");
	if (post_event == NULL) {
		jniThrowRuntimeException(env, "Can't find AudioRemuxer.postEventFromNative");
		return;
	}

	env->DeleteLocalRef(clazz);
}


JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioRemuxer_Native_1Start
  (JNIEnv *env, jobject thiz, jstring jInputUrl, jint jStartPosMs, jint jEndPosMs, jboolean jIsRemuxAll, jstring jOutputUrl, jobject weak_this)
{
	AudioRemuxer* audioRemuxer = (AudioRemuxer*)env->GetLongField(thiz, context);
    if (audioRemuxer!=NULL) return;

    const char *inputUrl;
    inputUrl = env->GetStringUTFChars(jInputUrl, NULL);

    const char *outputUrl;
    outputUrl = env->GetStringUTFChars(jOutputUrl, NULL);

    AudioRemuxerMaterialGroup inputMaterialGroup;
    AudioRemuxerProduct outputProduct;
    inputMaterialGroup.audioRemuxerMaterialNum = 1;
    inputMaterialGroup.audioRemuxerMaterials[0] = new AudioRemuxerMaterial;
    inputMaterialGroup.audioRemuxerMaterials[0]->url = strdup(inputUrl);
    inputMaterialGroup.audioRemuxerMaterials[0]->startPosMs = jStartPosMs;
    inputMaterialGroup.audioRemuxerMaterials[0]->endPosMs = jEndPosMs;
    if(jIsRemuxAll==JNI_TRUE)
    {
        inputMaterialGroup.audioRemuxerMaterials[0]->isRemuxAll = true;
    }else{
        inputMaterialGroup.audioRemuxerMaterials[0]->isRemuxAll = false;
    }
    outputProduct.url = outputUrl;

    audioRemuxer = new AudioRemuxer(jvm, inputMaterialGroup, outputProduct);
    audioRemuxer->setListener(thiz, weak_this, post_event);
    audioRemuxer->start();

    env->SetLongField(thiz, context, (int64_t)audioRemuxer);

    inputMaterialGroup.Free();

    env->ReleaseStringUTFChars(jInputUrl, inputUrl);
    env->ReleaseStringUTFChars(jOutputUrl, outputUrl);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioRemuxer_Native_1Pause
  (JNIEnv *env, jobject thiz)
{
	AudioRemuxer* audioRemuxer = (AudioRemuxer*)env->GetLongField(thiz, context);
    if (audioRemuxer==NULL) return;
    
    audioRemuxer->pause();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioRemuxer_Native_1Resume
  (JNIEnv *env, jobject thiz)
{
	AudioRemuxer* audioRemuxer = (AudioRemuxer*)env->GetLongField(thiz, context);
    if (audioRemuxer==NULL) return;

    audioRemuxer->resume();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioRemuxer_Native_1Stop
  (JNIEnv *env, jobject thiz, jboolean jIsCancel)
{
	AudioRemuxer* audioRemuxer = (AudioRemuxer*)env->GetLongField(thiz, context);
    if (audioRemuxer==NULL) return;

    if(jIsCancel==JNI_TRUE)
    {
    	audioRemuxer->stop(true);
    }else{
    	audioRemuxer->stop(false);
    }

    delete audioRemuxer;
    audioRemuxer = NULL;

    env->SetLongField(thiz, context, (int64_t)0);
}

#ifdef __cplusplus
}
#endif

#endif
