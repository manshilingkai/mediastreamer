// Created by Think on 2019/10/19.
// Copyright © 2019年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaeditengine_MediaTranscoder
#define _Included_android_slkmedia_mediaeditengine_MediaTranscoder

#include <assert.h>

#include "JNIHelp.h"
#include "MediaLog.h"
#include "MediaTranscoder.h"

using namespace MT;

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;
static jmethodID post_event = NULL;

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MediaTranscoder_Native_1Init
  (JNIEnv *env, jclass thiz)
{
	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/mediaeditengine/MediaTranscoder");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/mediaeditengine/MediaTranscoder");
        return;
    }
    
    context = env->GetFieldID(clazz, "mNativeContext", "J");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find MediaTranscoder.mNativeContext");
        return;
    }

	post_event = env->GetStaticMethodID(clazz, "postEventFromNative",
			"(Ljava/lang/Object;IIILjava/lang/Object;)V");
	if (post_event == NULL) {
		jniThrowRuntimeException(env, "Can't find MediaTranscoder.postEventFromNative");
		return;
	}

	env->DeleteLocalRef(clazz);
}


JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MediaTranscoder_Native_1Start
  (JNIEnv *env, jobject thiz, jstring jInputUrl, jlong jStartPosMs, jlong jEndPosMs, jint jvideoRotationDegree, jint jvideoFlip, jstring jOverlayUrl, jboolean jHas_alpha_for_overlay_video, jint jOverlayX, jint jOverlayY, jfloat jOverlayScale, jlong jOverlayEffectInMs, jlong jOverlayEffectOutMs, jstring jOutputUrl, jboolean jHasVideo, jint jOutputVideoWidth, jint jOutputVideoHeight, jint jOutputVideoBitrateKbps, jboolean jHasAudio, jint jOutputAudioSampleRate, jint jOutputAudioBitrateKbps, jobject weak_this)
{
	MediaTranscoder* mediaTranscoder = (MediaTranscoder*)env->GetLongField(thiz, context);
    if (mediaTranscoder!=NULL) return;

    const char *inputUrl;
    inputUrl = env->GetStringUTFChars(jInputUrl, NULL);

    const char *overlayUrl;
    if(jOverlayUrl!=NULL)
    {
    	overlayUrl = env->GetStringUTFChars(jOverlayUrl, NULL);
    }else{
    	overlayUrl = NULL;
    }

    bool has_alpha_for_overlay_video = false;
    if(jHas_alpha_for_overlay_video==JNI_TRUE)
    {
    	has_alpha_for_overlay_video = true;
    }

    const char* outputUrl;
    outputUrl = env->GetStringUTFChars(jOutputUrl, NULL);

    MediaMaterial mediaMaterial;
    MediaEffectGroup *pMediaEffectGroup;
    MediaProduct mediaProduct;

    mediaMaterial.url = (char*)inputUrl;
    mediaMaterial.startPosMs = jStartPosMs;
    mediaMaterial.endPosMs = jEndPosMs;

    pMediaEffectGroup = new MediaEffectGroup;
    if (jvideoRotationDegree !=0 ) {
        pMediaEffectGroup->mediaEffectCount++;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1] = new MediaEffect;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->type =  MEDIA_EFFECT_TYPE_VIDEO_ROTATE;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->video_rotation_degree = jvideoRotationDegree;
    }
    if (jvideoFlip !=0 ) {
        pMediaEffectGroup->mediaEffectCount++;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1] = new MediaEffect;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->type =  MEDIA_EFFECT_TYPE_VIDEO_FLIP;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->video_flip = jvideoFlip;
    }
    if (overlayUrl != NULL) {
        pMediaEffectGroup->mediaEffectCount++;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1] = new MediaEffect;
        if(strstr(overlayUrl,".mp4") || strstr(overlayUrl,".MP4") || strstr(overlayUrl,".MOV"))
        {
        	if(has_alpha_for_overlay_video)
        	{
                pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->type =  MEDIA_EFFECT_TYPE_MP4_ALPHA;
        	}else{
                pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->type =  MEDIA_EFFECT_TYPE_MP4;
        	}
        }else{
            pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->type =  MEDIA_EFFECT_TYPE_PNG;
        }
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->resourceUrl = strdup(overlayUrl);
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->x = jOverlayX;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->y = jOverlayY;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->scale = jOverlayScale;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->effect_in_ms = jOverlayEffectInMs;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->effect_out_ms = jOverlayEffectOutMs;
    }

    mediaProduct.type = MEDIA_PRODUCT_TYPE_MP4;
    if(jHasVideo==JNI_TRUE)
    {
        mediaProduct.type = MEDIA_PRODUCT_TYPE_MP4;
    }else{
        mediaProduct.type = MEDIA_PRODUCT_TYPE_M4A;
    }
    mediaProduct.url = (char*)outputUrl;
    if(jHasVideo==JNI_TRUE)
    {
        mediaProduct.videoOptions.hasVideoTrack = true;
    }else{
        mediaProduct.videoOptions.hasVideoTrack = false;
    }
    mediaProduct.videoOptions.videoWidth = jOutputVideoWidth;
    mediaProduct.videoOptions.videoHeight = jOutputVideoHeight;
    mediaProduct.videoOptions.videoBitrateKbps = jOutputVideoBitrateKbps;
    if(jHasAudio==JNI_TRUE)
    {
        mediaProduct.audioOptions.hasAudioTrack = true;
    }else{
        mediaProduct.audioOptions.hasAudioTrack = false;
    }
    mediaProduct.audioOptions.audioSampleRate = jOutputAudioSampleRate;
    mediaProduct.audioOptions.audioBitrateKbps = jOutputAudioBitrateKbps;

    mediaTranscoder = MediaTranscoder::CreateMediaTranscoder(jvm, MEDIA_TRANSCODER_TYPE_DEFAULT, NULL, mediaMaterial, pMediaEffectGroup, mediaProduct);
    mediaTranscoder->setListener(thiz, weak_this, post_event);
    mediaTranscoder->start();

    env->SetLongField(thiz, context, (int64_t)mediaTranscoder);

    if (pMediaEffectGroup) {
        pMediaEffectGroup->Free();
        delete pMediaEffectGroup;
        pMediaEffectGroup = NULL;
    }

    env->ReleaseStringUTFChars(jInputUrl, inputUrl);
    if(jOverlayUrl!=NULL)
    {
    	env->ReleaseStringUTFChars(jOverlayUrl, overlayUrl);
    }
    env->ReleaseStringUTFChars(jOutputUrl, outputUrl);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MediaTranscoder_Native_1Pause
  (JNIEnv *env, jobject thiz)
{
	MediaTranscoder* mediaTranscoder = (MediaTranscoder*)env->GetLongField(thiz, context);
    if (mediaTranscoder==NULL) return;
    
    mediaTranscoder->pause();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MediaTranscoder_Native_1Resume
  (JNIEnv *env, jobject thiz)
{
	MediaTranscoder* mediaTranscoder = (MediaTranscoder*)env->GetLongField(thiz, context);
    if (mediaTranscoder==NULL) return;

    mediaTranscoder->resume();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MediaTranscoder_Native_1Stop
  (JNIEnv *env, jobject thiz, jboolean jIscancel)
{
	MediaTranscoder* mediaTranscoder = (MediaTranscoder*)env->GetLongField(thiz, context);
    if (mediaTranscoder==NULL) return;

    if(jIscancel==JNI_TRUE)
    {
        mediaTranscoder->stop(true);
    }else{
        mediaTranscoder->stop(false);
    }
    MediaTranscoder::DeleteMediaTranscoder(MEDIA_TRANSCODER_TYPE_DEFAULT, mediaTranscoder);
    mediaTranscoder = NULL;

    env->SetLongField(thiz, context, (int64_t)0);
}

#ifdef __cplusplus
}
#endif

#endif
