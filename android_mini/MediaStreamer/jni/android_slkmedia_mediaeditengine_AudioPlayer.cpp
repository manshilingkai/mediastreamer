// Created by Think on 16/2/25.
// Copyright © 2016年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaeditengine_AudioPlayer
#define _Included_android_slkmedia_mediaeditengine_AudioPlayer

#include <assert.h>

#include "JNIHelp.h"
#include "AudioPlayer.h"
#include "MediaLog.h"

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;
static jmethodID post_event = NULL;

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_Init
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1Init
  (JNIEnv *env, jclass thiz)
{
	LOGD("Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1Init");

	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/mediaeditengine/AudioPlayer");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/mediaeditengine/AudioPlayer");
        return;
    }

    context = env->GetFieldID(clazz, "mNativeContext", "J");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find AudioPlayer.mNativeContext");
        return;
    }

	post_event = env->GetStaticMethodID(clazz, "postEventFromNative",
			"(Ljava/lang/Object;IIILjava/lang/Object;)V");
	if (post_event == NULL) {
		jniThrowRuntimeException(env, "Can't find AudioPlayer.postEventFromNative");
		return;
	}

	env->DeleteLocalRef(clazz);
}

// ----------------------------------------------------------------------------

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_Setup
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1Setup
  (JNIEnv *env, jobject thiz, jobject weak_thiz)
{
	LOGD("Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1Setup");

	AudioRenderConfigure audioRenderConfigure;
	AudioPlayer *audioPlayer = new AudioPlayer(jvm, AUDIO_PLAYER_INTERNAL_RENDER, audioRenderConfigure);
	audioPlayer->setListener(thiz, weak_thiz, post_event);

    env->SetLongField(thiz, context, (int64_t)audioPlayer);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1SetupWithMixOptions
  (JNIEnv *env, jobject thiz, jobject weak_thiz, jint jSampleRate, jint jChannelCount)
{
	LOGD("Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1SetupWithMixOptions");

    AudioRenderConfigure audioRenderConfigure;
    audioRenderConfigure.channelCount = jChannelCount;
    audioRenderConfigure.sampleFormat = AV_SAMPLE_FMT_S16;
    audioRenderConfigure.sampleRate = jSampleRate;
    if (audioRenderConfigure.channelCount<=1) {
        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_MONO;
    }else{
        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_STEREO;
    }

	AudioPlayer *audioPlayer = new AudioPlayer(jvm, AUDIO_PLAYER_INTERNAL_RENDER_EXTERNAL_MIX, audioRenderConfigure);
	audioPlayer->setListener(thiz, weak_thiz, post_event);

    env->SetLongField(thiz, context, (int64_t)audioPlayer);
}

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_Finalize
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1Finalize
  (JNIEnv *env, jobject thiz)
{
	LOGD("Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1Finalize");

	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		delete audioPlayer;
		audioPlayer = NULL;
	}

    env->SetLongField(thiz, context, (int64_t)0);
}

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_SetDataSource
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1SetDataSource
  (JNIEnv *env, jobject thiz, jstring url)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		const char *curl = env->GetStringUTFChars(url,NULL);

		audioPlayer->setDataSource(curl);

		env->ReleaseStringUTFChars(url,curl);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1Prepare
  (JNIEnv *env, jobject thiz)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		audioPlayer->prepare();
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_PrepareAsync
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1PrepareAsync
  (JNIEnv *env, jobject thiz)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		audioPlayer->prepareAsync();
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_PrepareAsyncToPlay
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1PrepareAsyncToPlay
  (JNIEnv *env, jobject thiz)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		audioPlayer->prepareAsyncToPlay();
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_Play
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1Play
(JNIEnv *env, jobject thiz)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		audioPlayer->play();
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_IsPlaying
 * Signature: ()V
 */
JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1IsPlaying
(JNIEnv *env, jobject thiz)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		if(audioPlayer->isPlaying())
		{
			return JNI_TRUE;
		}else{
			return JNI_FALSE;
		}
	}else{
		jniThrowNullPointerException(env,NULL);
		return JNI_FALSE;
	}
}

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_Pause
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1Pause
(JNIEnv *env, jobject thiz)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		audioPlayer->pause();
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_Stop
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1Stop
(JNIEnv *env, jobject thiz)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		audioPlayer->stop();
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_SeekTo
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1SeekTo
(JNIEnv *env, jobject thiz, jint msec)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		audioPlayer->seekTo(msec);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_GetPlayTimeMs
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1GetPlayTimeMs
(JNIEnv *env, jobject thiz)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		return audioPlayer->getPlayTimeMs();
	}else{
		jniThrowNullPointerException(env,NULL);

		return -1;
	}
}

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_GetDurationMs
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1GetDurationMs
  (JNIEnv *env, jobject thiz)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		return audioPlayer->getDurationMs();
	}else{
		jniThrowNullPointerException(env,NULL);

		return -1;
	}
}

JNIEXPORT jint JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1GetPcmDB
  (JNIEnv *env, jobject thiz)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		return audioPlayer->getPcmDB();
	}else{
		jniThrowNullPointerException(env,NULL);

		return -1;
	}
}

/*
 * Class:     android_slkmedia_mediaeditengine_AudioPlayer
 * Method:    Native_SetVolume
 * Signature: (F)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1SetVolume
(JNIEnv *env, jobject thiz, jfloat jvolume)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		audioPlayer->setVolume(jvolume);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1SetPlayRate
(JNIEnv *env, jobject thiz, jfloat jplayrate)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		audioPlayer->setPlayRate(jplayrate);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1SetLooping
(JNIEnv *env, jobject thiz, jboolean jisLooping)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);

	if(audioPlayer!=NULL)
	{
		if(jisLooping==JNI_TRUE)
		{
			audioPlayer->setLooping(true);
		}else{
			audioPlayer->setLooping(false);
		}
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_AudioPlayer_Native_1MixWithExternal
  (JNIEnv *env, jobject thiz, jbyteArray jData, jint jOffset, jint jSize)
{
	AudioPlayer *audioPlayer = (AudioPlayer*)env->GetLongField(thiz, context);
    if (audioPlayer==NULL) return;

    jbyte* raw_bytes = env->GetByteArrayElements(jData, NULL);
    uint8_t *data = (uint8_t*)raw_bytes;
    int size = jSize;

    audioPlayer->mixWithExternal(data+jOffset, size);

    env->ReleaseByteArrayElements(jData, raw_bytes, 0);

    return;
}

#ifdef __cplusplus
}
#endif

#endif
