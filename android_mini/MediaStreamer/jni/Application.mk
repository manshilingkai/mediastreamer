#  Created by Think on 16/2/25.
#  Copyright © 2016年 Cell. All rights reserved.

APP_OPTIM := release
APP_PLATFORM := android-21
APP_ABI := armeabi-v7a arm64-v8a
#APP_ABI := armeabi-v7a arm64-v8a x86
NDK_TOOLCHAIN_VERSION=4.9
APP_PIE := false
#APP_STL := stlport_static
APP_STL := gnustl_static
#APP_CPPFLAGS := -fno-rtti -fpermissive -fPIC
APP_CPPFLAGS := -fno-rtti -fpermissive -fPIC -fexceptions -std=c++11
