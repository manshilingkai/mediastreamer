// Created by Think on 16/2/25.
// Copyright © 2016年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaeditengine_GPUOverlay
#define _Included_android_slkmedia_mediaeditengine_GPUOverlay

#include <assert.h>

#include "JNIHelp.h"
#include "GPUOverlay.h"
#include "MediaLog.h"

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;

/*
 * Class:     android_slkmedia_mediaeditengine_GPUOverlay
 * Method:    Native_Init
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_GPUOverlay_Native_1Init
  (JNIEnv *env, jclass thiz)
{
	LOGD("Java_android_slkmedia_mediaeditengine_GPUOverlay_Native_1Init");

	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/mediaeditengine/GPUOverlay");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/mediaeditengine/GPUOverlay");
        return;
    }

    context = env->GetFieldID(clazz, "mNativeContext", "J");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find GPUOverlay.mNativeContext");
        return;
    }

	env->DeleteLocalRef(clazz);
}

// ----------------------------------------------------------------------------

/*
 * Class:     android_slkmedia_mediaeditengine_GPUOverlay
 * Method:    Native_Setup
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_GPUOverlay_Native_1Setup
  (JNIEnv *env, jobject thiz)
{
	LOGD("Java_android_slkmedia_mediaeditengine_GPUOverlay_Native_1Setup");

	GPUOverlay *gpuOverlay = new GPUOverlay();
	gpuOverlay->initialize();

    env->SetLongField(thiz, context, (int64_t)gpuOverlay);
}

/*
 * Class:     android_slkmedia_mediaeditengine_GPUOverlay
 * Method:    Native_Finalize
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_GPUOverlay_Native_1Finalize
  (JNIEnv *env, jobject thiz)
{
	LOGD("Java_android_slkmedia_mediaeditengine_GPUOverlay_Native_1Finalize");

	GPUOverlay *gpuOverlay = (GPUOverlay*)env->GetLongField(thiz, context);

	if(gpuOverlay!=NULL)
	{
		delete gpuOverlay;
		gpuOverlay = NULL;
	}

    env->SetLongField(thiz, context, (int64_t)0);
}

/*
 * Class:     android_slkmedia_mediaeditengine_GPUOverlay
 * Method:    Native_SetOverlay
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_GPUOverlay_Native_1SetOverlay
  (JNIEnv *env, jobject thiz, jstring url)
{
	GPUOverlay *gpuOverlay = (GPUOverlay*)env->GetLongField(thiz, context);

	if(gpuOverlay!=NULL)
	{
		const char *curl = env->GetStringUTFChars(url,NULL);

		gpuOverlay->setOverlay(curl);

		env->ReleaseStringUTFChars(url,curl);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

JNIEXPORT jint JNICALL Java_android_slkmedia_mediaeditengine_GPUOverlay_Native_1BlendTo
  (JNIEnv *env, jobject thiz, jint jMainTextureId, jint jMainWidth, jint jMainHeight, jint jX, jint jY, jint jOverlayRotation, jfloat jOverlayScale, jboolean jOverlayFlipHorizontal, jboolean jOverlayFlipVertical)
{
	GPUOverlay *gpuOverlay = (GPUOverlay*)env->GetLongField(thiz, context);

	if(gpuOverlay!=NULL)
	{
		bool overlayFlipHorizontal = false;
		bool overlayFlipVertical = false;
		if(jOverlayFlipHorizontal == JNI_TRUE)
		{
			overlayFlipHorizontal = true;
		}
		if(jOverlayFlipVertical == JNI_TRUE)
		{
			overlayFlipVertical = true;
		}
		return gpuOverlay->blendTo(jMainTextureId, jMainWidth, jMainHeight, jX, jY, jOverlayRotation, jOverlayScale, overlayFlipHorizontal, overlayFlipVertical);
	}else{
		jniThrowNullPointerException(env,NULL);
	}
}

#ifdef __cplusplus
}
#endif

#endif
