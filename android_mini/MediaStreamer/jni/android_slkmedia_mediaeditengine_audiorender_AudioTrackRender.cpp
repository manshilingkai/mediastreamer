// Created by Think on 18/12/10.
// Copyright © 2018年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaeditengine_audiorender_AudioTrackRender
#define _Included_android_slkmedia_mediaeditengine_audiorender_AudioTrackRender

#include <assert.h>

#include "JNIHelp.h"
#include "MediaLog.h"

#include "JniAudioTrackRender.h"

#ifdef __cplusplus
extern "C" {
#endif


JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_audiorender_AudioTrackRender_nativeCacheDirectBufferAddress
	(JNIEnv* env, jobject thiz, jobject byte_buffer, jlong nativeAudioRender)
{
	if(nativeAudioRender==0)
	{
		LOGE("input param nativeAudioRender is 0");
		return;
	}

	JniAudioTrackRender *jniAudioTrackRender = (JniAudioTrackRender*)nativeAudioRender;
	if(jniAudioTrackRender==NULL)
	{
		LOGE("jniAudioTrackRender is NULL");
		return;
	}

	jniAudioTrackRender->OnCacheDirectBufferAddress(byte_buffer);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_audiorender_AudioTrackRender_nativeGetPlayoutData
	(JNIEnv* env, jobject thiz, jint bytes, jlong nativeAudioRender)
{
	if(nativeAudioRender==0)
	{
		LOGE("input param nativeAudioRender is 0");
		return;
	}

	JniAudioTrackRender *jniAudioTrackRender = (JniAudioTrackRender*)nativeAudioRender;
	if(jniAudioTrackRender==NULL)
	{
		LOGE("jniAudioTrackRender is NULL");
		return;
	}

	jniAudioTrackRender->OnGetPlayoutData(bytes);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_audiorender_AudioTrackRender_nativeSetAudioTrackLatency
	(JNIEnv* env, jobject thiz, jlong latency, jlong nativeAudioRender)
{
	if(nativeAudioRender==0)
	{
		LOGE("input param nativeAudioRender is 0");
		return;
	}

	JniAudioTrackRender *jniAudioTrackRender = (JniAudioTrackRender*)nativeAudioRender;
	if(jniAudioTrackRender==NULL)
	{
		LOGE("jniAudioTrackRender is NULL");
		return;
	}

	jniAudioTrackRender->setAudioTrackLatency(latency);
}

#ifdef __cplusplus
}
#endif

#endif
