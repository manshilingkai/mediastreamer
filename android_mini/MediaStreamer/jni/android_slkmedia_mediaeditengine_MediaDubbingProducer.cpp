// Created by Think on 2019/7/16.
// Copyright © 2019年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaeditengine_MediaDubbingProducer
#define _Included_android_slkmedia_mediaeditengine_MediaDubbingProducer

#include <assert.h>

#include "JNIHelp.h"
#include "MediaDubbingProducer.h"
#include "MediaLog.h"

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;
static jmethodID post_event = NULL;

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MediaDubbingProducer_Native_1Init
  (JNIEnv *env, jclass thiz)
{
	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/mediaeditengine/MediaDubbingProducer");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/mediaeditengine/MediaDubbingProducer");
        return;
    }
    
    context = env->GetFieldID(clazz, "mNativeContext", "J");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find MediaDubbingProducer.mNativeContext");
        return;
    }

	post_event = env->GetStaticMethodID(clazz, "postEventFromNative",
			"(Ljava/lang/Object;IIILjava/lang/Object;)V");
	if (post_event == NULL) {
		jniThrowRuntimeException(env, "Can't find MediaDubbingProducer.postEventFromNative");
		return;
	}

	env->DeleteLocalRef(clazz);
}


JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MediaDubbingProducer_Native_1Start
  (JNIEnv *env, jobject thiz, jstring jVideoUrl, jstring jBgmUrl, jfloat jBgmVolume, jstring jDubUrl, jfloat jDubVolume, jstring jProductUrl, jobject weak_this)
{
	MediaDubbingProducer* mediaDubbingProducer = (MediaDubbingProducer*)env->GetLongField(thiz, context);
    if (mediaDubbingProducer!=NULL) return;

    const char *videoUrl;
    if(jVideoUrl!=NULL)
    {
        videoUrl = env->GetStringUTFChars(jVideoUrl, NULL);
    }else{
    	videoUrl = NULL;
    }

    const char *bgmUrl;
    bgmUrl = env->GetStringUTFChars(jBgmUrl, NULL);
    float bgmVolume = jBgmVolume;

    const char* dubUrl;
    if(jDubUrl!=NULL)
    {
        dubUrl = env->GetStringUTFChars(jDubUrl, NULL);
    }else{
    	dubUrl = NULL;
    }
    float dubVolume = jDubVolume;

    const char* productUrl;
    productUrl = env->GetStringUTFChars(jProductUrl, NULL);

    MediaDubbingVideoOptions videoOptions;
    videoOptions.videoUrl = videoUrl;

    MediaDubbingBGMOptions bgmOptions;
    bgmOptions.bgmUrl = bgmUrl;
    bgmOptions.bgmVolume = bgmVolume;

    MediaDubbingDubOptions dubOptions;
    dubOptions.dubUrl = dubUrl;
    dubOptions.dubVolume = dubVolume;

    MediaDubbingProductOptions productOptions;
    productOptions.productUrl = productUrl;

    mediaDubbingProducer = new MediaDubbingProducer(jvm, videoOptions, bgmOptions, dubOptions, productOptions);
    mediaDubbingProducer->setListener(thiz, weak_this, post_event);
    mediaDubbingProducer->start();

    env->SetLongField(thiz, context, (int64_t)mediaDubbingProducer);

    if(jVideoUrl!=NULL)
    {
        env->ReleaseStringUTFChars(jVideoUrl, videoUrl);
    }
    env->ReleaseStringUTFChars(jBgmUrl, bgmUrl);
    if(jDubUrl!=NULL)
    {
        env->ReleaseStringUTFChars(jDubUrl, dubUrl);
    }
    env->ReleaseStringUTFChars(jProductUrl, productUrl);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MediaDubbingProducer_Native_1Pause
  (JNIEnv *env, jobject thiz)
{
	MediaDubbingProducer* mediaDubbingProducer = (MediaDubbingProducer*)env->GetLongField(thiz, context);
    if (mediaDubbingProducer==NULL) return;
    
    mediaDubbingProducer->pause();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MediaDubbingProducer_Native_1Resume
  (JNIEnv *env, jobject thiz)
{
	MediaDubbingProducer* mediaDubbingProducer = (MediaDubbingProducer*)env->GetLongField(thiz, context);
    if (mediaDubbingProducer==NULL) return;

    mediaDubbingProducer->resume();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MediaDubbingProducer_Native_1Stop
  (JNIEnv *env, jobject thiz, jboolean jIsCancle)
{
	MediaDubbingProducer* mediaDubbingProducer = (MediaDubbingProducer*)env->GetLongField(thiz, context);
    if (mediaDubbingProducer==NULL) return;

    if(jIsCancle==JNI_TRUE)
    {
        mediaDubbingProducer->stop(true);
    }else{
        mediaDubbingProducer->stop(false);
    }

    delete mediaDubbingProducer;
    mediaDubbingProducer = NULL;

    env->SetLongField(thiz, context, (int64_t)0);
}

#ifdef __cplusplus
}
#endif

#endif
