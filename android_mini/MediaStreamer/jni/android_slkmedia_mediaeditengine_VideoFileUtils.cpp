// Created by Think on 16/2/25.
// Copyright © 2016年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaeditengine_VideoFileUtils
#define _Included_android_slkmedia_mediaeditengine_VideoFileUtils

#include <android/bitmap.h>

#include "MediaLog.h"
#include "LibyuvColorSpaceConvert.h"
#include "ImageProcesserUtils.h"

#include "MediaFileUtils.h"

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaeditengine_VideoFileUtils_BitmapToPng
	(JNIEnv* env, jobject thiz, jobject jInBitmap, jstring jOutputPngFilePath, jint jDstWidth, jint jDstHeight)
{
    AndroidBitmapInfo info;
    int ret = AndroidBitmap_getInfo(env, jInBitmap, &info);
    if(ANDROID_BITMAP_RESULT_SUCCESS != ret)
    {
        LOGE("AndroidBitmap_getInfo() failed ! error=%d", ret);
        return JNI_FALSE;
    }

    LOGD("width : %d", info.width);
    LOGD("height : %d", info.height);
    LOGD("stride : %d", info.stride);
    LOGD("format : %d", info.format);
    LOGD("flags : %d", info.flags);

    if(info.format!=ANDROID_BITMAP_FORMAT_RGBA_8888 && info.format!=ANDROID_BITMAP_FORMAT_RGB_565)
    {
        LOGE("unknown bitmap format");
        return JNI_FALSE;
    }

    void* pixels;
    ret = AndroidBitmap_lockPixels(env, jInBitmap, &pixels);
    if(ANDROID_BITMAP_RESULT_SUCCESS != ret)
    {
        LOGE("AndroidBitmap_lockPixels() failed ! error=%d", ret);
        return JNI_FALSE;
    }

    VideoFrame in_rgbx;
    in_rgbx.data = pixels;
    in_rgbx.frameSize = info.stride * info.height;
    in_rgbx.width = info.width;
    in_rgbx.height = info.height;

    VideoFrame out_rgba;
    out_rgba.data = (uint8_t*)malloc(jDstWidth*jDstHeight*4);
    out_rgba.frameSize = jDstWidth*jDstHeight*4;
    out_rgba.width = jDstWidth;
    out_rgba.height = jDstHeight;

    bool b_ret = false;
    if(info.format==ANDROID_BITMAP_FORMAT_RGBA_8888)
    {
    	b_ret = LibyuvColorSpaceConvertUtils::RGBAtoRGBA_Crop_Scale(&in_rgbx, &out_rgba);
    }else if(info.format==ANDROID_BITMAP_FORMAT_RGB_565){
    	b_ret = LibyuvColorSpaceConvertUtils::RGBPtoRGBA_Crop_Scale(&in_rgbx, &out_rgba);
    }

    if(!b_ret)
    {
    	if(out_rgba.data)
    	{
    		free(out_rgba.data);
    		out_rgba.data = NULL;
    	}

        AndroidBitmap_unlockPixels(env, jInBitmap);
        return JNI_FALSE;
    }

	const char* outputPngFilePath = env->GetStringUTFChars(jOutputPngFilePath,NULL);
    ret = RGBAToPNGFile(out_rgba.data, jDstWidth, jDstHeight, outputPngFilePath);
	env->ReleaseStringUTFChars(jOutputPngFilePath,outputPngFilePath);

	if(out_rgba.data)
	{
		free(out_rgba.data);
		out_rgba.data = NULL;
	}

    AndroidBitmap_unlockPixels(env, jInBitmap);

    if(ret!=0)
    {
    	return JNI_FALSE;
    }else{
    	return JNI_TRUE;
    }
}

JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaeditengine_VideoFileUtils_WriteI420ImageToDisk
	(JNIEnv* env, jobject thiz,
			jobject j_y_buffer, jint j_y_rowStride,
            jobject j_u_buffer, jint j_u_rowStride,
            jobject j_v_buffer, jint j_v_rowStride,
            jint j_width, jint j_height, jstring jOutputPngFilePath, jint jDstWidth, jint jDstHeight, jint j_rotation)
{
    uint8_t *srcYPtr = reinterpret_cast<uint8_t *>(env->GetDirectBufferAddress(j_y_buffer));
    uint8_t *srcUPtr = reinterpret_cast<uint8_t *>(env->GetDirectBufferAddress(j_u_buffer));
    uint8_t *srcVPtr = reinterpret_cast<uint8_t *>(env->GetDirectBufferAddress(j_v_buffer));

    YUVVideoFrame in_yuv420p;
    in_yuv420p.data[0] = srcYPtr;
    in_yuv420p.linesize[0] = j_y_rowStride;
    in_yuv420p.data[1] = srcUPtr;
    in_yuv420p.linesize[1] = j_u_rowStride;
    in_yuv420p.data[2] = srcVPtr;
    in_yuv420p.linesize[2] = j_v_rowStride;
    in_yuv420p.width = j_width;
    in_yuv420p.height = j_height;

    VideoFrame out_rgba;
    out_rgba.data = (uint8_t*)malloc(jDstWidth*jDstHeight*4);
    out_rgba.frameSize = jDstWidth*jDstHeight*4;
    out_rgba.width = jDstWidth;
    out_rgba.height = jDstHeight;

    bool ret = LibyuvColorSpaceConvertUtils::I420ToRGBA_Crop_Scale_Rotation(&in_yuv420p, &out_rgba, j_rotation);
    if(!ret)
    {
    	if(out_rgba.data)
    	{
    		free(out_rgba.data);
    		out_rgba.data = NULL;
    	}

        return JNI_FALSE;
    }

	const char* outputPngFilePath = env->GetStringUTFChars(jOutputPngFilePath,NULL);
    int i_ret = RGBAToPNGFile(out_rgba.data, jDstWidth, jDstHeight, outputPngFilePath);
	env->ReleaseStringUTFChars(jOutputPngFilePath,outputPngFilePath);

	if(out_rgba.data)
	{
		free(out_rgba.data);
		out_rgba.data = NULL;
	}

    if(i_ret!=0)
    {
    	return JNI_FALSE;
    }else{
    	return JNI_TRUE;
    }
}

JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaeditengine_VideoFileUtils_WriteNV12ImageToDisk
	(JNIEnv* env, jobject thiz,
			jobject j_y_buffer, jint j_y_rowStride,
            jobject j_uv_buffer, jint j_uv_rowStride,
            jint j_width, jint j_height, jstring jOutputPngFilePath, jint jDstWidth, jint jDstHeight, jint j_rotation)
{
    uint8_t *srcYPtr = reinterpret_cast<uint8_t *>(env->GetDirectBufferAddress(j_y_buffer));
    uint8_t *srcUVPtr = reinterpret_cast<uint8_t *>(env->GetDirectBufferAddress(j_uv_buffer));

    YUVVideoFrame in_yuv420sp;
    in_yuv420sp.data[0] = srcYPtr;
    in_yuv420sp.linesize[0] = j_y_rowStride;
    in_yuv420sp.data[1] = srcUVPtr;
    in_yuv420sp.linesize[1] = j_uv_rowStride;
    in_yuv420sp.width = j_width;
    in_yuv420sp.height = j_height;

    VideoFrame out_rgba;
    out_rgba.data = (uint8_t*)malloc(jDstWidth*jDstHeight*4);
    out_rgba.frameSize = jDstWidth*jDstHeight*4;
    out_rgba.width = jDstWidth;
    out_rgba.height = jDstHeight;

    bool ret = LibyuvColorSpaceConvertUtils::NV12ToRGBA_Crop_Scale_Rotation(&in_yuv420sp, &out_rgba, j_rotation);
    if(!ret)
    {
    	if(out_rgba.data)
    	{
    		free(out_rgba.data);
    		out_rgba.data = NULL;
    	}

        return JNI_FALSE;
    }

	const char* outputPngFilePath = env->GetStringUTFChars(jOutputPngFilePath,NULL);
    int i_ret = RGBAToPNGFile(out_rgba.data, jDstWidth, jDstHeight, outputPngFilePath);
	env->ReleaseStringUTFChars(jOutputPngFilePath,outputPngFilePath);

	if(out_rgba.data)
	{
		free(out_rgba.data);
		out_rgba.data = NULL;
	}

    if(i_ret!=0)
    {
    	return JNI_FALSE;
    }else{
    	return JNI_TRUE;
    }
}

JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaeditengine_VideoFileUtils_WriteYUV420PImageToDisk
	(JNIEnv* env, jobject thiz, jbyteArray jData, jint j_width, jint j_height, jstring jOutputPngFilePath, jint jDstWidth, jint jDstHeight, jint j_rotation)
{
    jbyte* raw_bytes = env->GetByteArrayElements(jData, NULL);

    YUVVideoFrame in_yuv420p;
    in_yuv420p.data[0] = (uint8_t*)raw_bytes;
    in_yuv420p.linesize[0] = j_width;
    in_yuv420p.data[1] = (uint8_t*)(raw_bytes + in_yuv420p.linesize[0] * j_height);
    in_yuv420p.linesize[1] = j_width/2;
    in_yuv420p.data[2] = (uint8_t*)(raw_bytes + in_yuv420p.linesize[0] * j_height + in_yuv420p.linesize[1] * j_height / 2);
    in_yuv420p.linesize[2] = j_width/2;
    in_yuv420p.width = j_width;
    in_yuv420p.height = j_height;

    VideoFrame out_rgba;
    out_rgba.data = (uint8_t*)malloc(jDstWidth*jDstHeight*4);
    out_rgba.frameSize = jDstWidth*jDstHeight*4;
    out_rgba.width = jDstWidth;
    out_rgba.height = jDstHeight;

    bool ret = LibyuvColorSpaceConvertUtils::I420ToRGBA_Crop_Scale_Rotation(&in_yuv420p, &out_rgba, j_rotation);

    env->ReleaseByteArrayElements(jData, raw_bytes, 0);

    if(!ret)
    {
    	if(out_rgba.data)
    	{
    		free(out_rgba.data);
    		out_rgba.data = NULL;
    	}

        return JNI_FALSE;
    }

	const char* outputPngFilePath = env->GetStringUTFChars(jOutputPngFilePath,NULL);
    int i_ret = RGBAToPNGFile(out_rgba.data, jDstWidth, jDstHeight, outputPngFilePath);
	env->ReleaseStringUTFChars(jOutputPngFilePath,outputPngFilePath);

	if(out_rgba.data)
	{
		free(out_rgba.data);
		out_rgba.data = NULL;
	}

    if(i_ret!=0)
    {
    	return JNI_FALSE;
    }else{
    	return JNI_TRUE;
    }
}

JNIEXPORT jint JNICALL Java_android_slkmedia_mediaeditengine_VideoFileUtils_GetMediaFileType
	(JNIEnv* env, jobject thiz, jstring jMediaFilePath)
{
	jint ret = -1;

	if(jMediaFilePath==NULL) return -1;

	const char* mediaFilePath = env->GetStringUTFChars(jMediaFilePath,NULL);
	ret = getMediaFileType(mediaFilePath);
	env->ReleaseStringUTFChars(jMediaFilePath,mediaFilePath);
	return ret;
}

#ifdef __cplusplus
}
#endif

#endif
