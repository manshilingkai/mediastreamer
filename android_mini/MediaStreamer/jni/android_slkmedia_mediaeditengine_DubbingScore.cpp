// Created by Think on 2019/7/16.
// Copyright © 2019年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaeditengine_DubbingScore
#define _Included_android_slkmedia_mediaeditengine_DubbingScore

#include <assert.h>

#include "JNIHelp.h"
#include "MediaLog.h"
#include "AudioSimilarity.h"

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;
static jmethodID post_event = NULL;

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_DubbingScore_Native_1Init
  (JNIEnv *env, jclass thiz)
{
	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/mediaeditengine/DubbingScore");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/mediaeditengine/DubbingScore");
        return;
    }
    
    context = env->GetFieldID(clazz, "mNativeContext", "J");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find DubbingScore.mNativeContext");
        return;
    }

	post_event = env->GetStaticMethodID(clazz, "postEventFromNative",
			"(Ljava/lang/Object;IIILjava/lang/Object;)V");
	if (post_event == NULL) {
		jniThrowRuntimeException(env, "Can't find DubbingScore.postEventFromNative");
		return;
	}

	env->DeleteLocalRef(clazz);
}


JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_DubbingScore_Native_1Start
  (JNIEnv *env, jobject thiz, jstring jOriginUrl, jstring jBgmUrl, jstring jDubUrl, jstring jWorkDir, jobject weak_this)
{
	AudioSimilarity* audioSimilarity = (AudioSimilarity*)env->GetLongField(thiz, context);
    if (audioSimilarity!=NULL) return;

    const char *originUrl;
    originUrl = env->GetStringUTFChars(jOriginUrl, NULL);

    const char *bgmUrl;
    bgmUrl = env->GetStringUTFChars(jBgmUrl, NULL);

    const char* dubUrl;
    dubUrl = env->GetStringUTFChars(jDubUrl, NULL);

    const char* workUrl;
    workUrl = env->GetStringUTFChars(jWorkDir, NULL);

    audioSimilarity = AudioSimilarity::CreateAudioSimilarity(jvm, CORRELATION_COEFFICIENT, originUrl, bgmUrl, dubUrl, workUrl);
    audioSimilarity->setListener(thiz, weak_this, post_event);
    audioSimilarity->start();

    env->SetLongField(thiz, context, (int64_t)audioSimilarity);

    env->ReleaseStringUTFChars(jOriginUrl, originUrl);
    env->ReleaseStringUTFChars(jBgmUrl, bgmUrl);
    env->ReleaseStringUTFChars(jDubUrl, dubUrl);
    env->ReleaseStringUTFChars(jWorkDir, workUrl);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_DubbingScore_Native_1Pause
  (JNIEnv *env, jobject thiz)
{
	AudioSimilarity* audioSimilarity = (AudioSimilarity*)env->GetLongField(thiz, context);
    if (audioSimilarity==NULL) return;
    
    audioSimilarity->pause();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_DubbingScore_Native_1Resume
  (JNIEnv *env, jobject thiz)
{
	AudioSimilarity* audioSimilarity = (AudioSimilarity*)env->GetLongField(thiz, context);
    if (audioSimilarity==NULL) return;

    audioSimilarity->resume();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_DubbingScore_Native_1Stop
  (JNIEnv *env, jobject thiz)
{
	AudioSimilarity* audioSimilarity = (AudioSimilarity*)env->GetLongField(thiz, context);
    if (audioSimilarity==NULL) return;

    audioSimilarity->stop();
    AudioSimilarity::DeleteAudioSimilarity(CORRELATION_COEFFICIENT, audioSimilarity);
    audioSimilarity = NULL;

    env->SetLongField(thiz, context, (int64_t)0);
}

#ifdef __cplusplus
}
#endif

#endif
