//
//  OpenCVUtils.cpp
//  MediaStreamer
//
//  Created by Think on 2017/3/7.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "OpenCVUtils.h"

#include "cvAdd4cMat.h"

bool OpenCVUtils::normalBlend(char* src1_path, int min_x, int min_y, char* src2_path, char* dst_path)
{
    cv::Mat img1 = cv::imread(src1_path), img2 = cv::imread(src2_path, -1);
    cv::Mat img1_t1(img1, cvRect(min_x, min_y, img2.cols, img2.rows));
    int ret = cvAdd4cMat(img1_t1, img2, 1.0);
    
    if (ret) return imwrite(dst_path, img1);
    else return false;
}

bool OpenCVUtils::normalBlend(VideoFrame* src1, int min_x, int min_y, VideoFrame* src2)
{
    cv::Mat img1(src1->width, src1->height, CV_8UC3, src1->data);
    cv::Mat img2(src2->width, src2->height, CV_8UC4, src2->data);
    cv::Mat img1_t1(img1, cvRect(min_x, min_y, img2.cols, img2.rows));
    cvAdd4cMat(img1_t1, img2, 1.0);
    
    return true;
}
