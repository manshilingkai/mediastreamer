//
//  OpenCVUtils.h
//  MediaStreamer
//
//  Created by Think on 2017/3/7.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef OpenCVUtils_h
#define OpenCVUtils_h

#include <stdio.h>

#include "MediaDataType.h"

class OpenCVUtils {
public:
    static bool normalBlend(char* src1_path, int min_x, int min_y, char* src2_path, char* dst_path);
    static bool normalBlend(VideoFrame* src1, int min_x, int min_y, VideoFrame* src2);
};

#endif /* OpenCVUtils_h */
