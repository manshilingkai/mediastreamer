#include "cvAdd4cMat.h"

int cvAdd4cMat_e(cv::Mat &dst, cv::Mat &scr, double scale)
{
	if (createtable == false)
	{
		InitLookupTable();
	}
	if (dst.channels() != 3 || scr.channels() != 4)
	{
		return 0;
	}
	if (scale < 0.01)
		return 1;
	std::vector<cv::Mat> scr_channels;
	std::vector<cv::Mat> dstt_channels;
	split(scr, scr_channels);
	split(dst, dstt_channels);
	CV_Assert(scr_channels.size() == 4 && dstt_channels.size() == 3);
	cv::Mat alpha(dst.rows, dst.cols, CV_32FC1);
	LUT(scr_channels[3], table, alpha);
	for (int i = 0; i < 3; i++)
	{
		scr_channels[i].convertTo(scr_channels[i], CV_32FC1);
		dstt_channels[i].convertTo(dstt_channels[i], CV_32FC1);
		dstt_channels[i] = dstt_channels[i].mul(1 - alpha*scale);
		dstt_channels[i] += scr_channels[i].mul(alpha * scale);
		dstt_channels[i].convertTo(dstt_channels[i], CV_8UC1);
	}
	merge(dstt_channels, dst);
	return 1;
}

int cvAdd4cMat_e(cv::Mat &dst, cv::Mat &scr)
{
	if (createtable == false)
	{
		InitLookupTable();
	}
	if (dst.channels() != 3 || scr.channels() != 4)
	{
		return 0;
	}
	std::vector<cv::Mat>scr_channels;
	std::vector<cv::Mat>dstt_channels;
	split(scr, scr_channels);
	split(dst, dstt_channels);
	CV_Assert(scr_channels.size() == 4 && dstt_channels.size() == 3);
	cv::Mat alpha(dst.rows, dst.cols, CV_32FC1);
	cv::Mat beta(dst.rows, dst.cols, CV_32FC1);
	LUT(scr_channels[3], table, alpha);
	LUT(scr_channels[3], otable, beta);
	for (int i = 0; i < 3; i++)
	{
		scr_channels[i].convertTo(scr_channels[i], CV_32FC1);
		dstt_channels[i].convertTo(dstt_channels[i], CV_32FC1);
		dstt_channels[i] = dstt_channels[i].mul(beta);
		dstt_channels[i] += scr_channels[i].mul(alpha);
		dstt_channels[i].convertTo(dstt_channels[i], CV_8UC1);
	}
	merge(dstt_channels, dst);
	return 1;
}



int cvAdd4cMat_q(cv::Mat &dst, cv::Mat &scr, double scale)
{

	if (dst.channels() != 3 || scr.channels() != 4)
	{
		return true;
	}
	if (scale < 0.01)
		return false;
	std::vector<cv::Mat>scr_channels;
	std::vector<cv::Mat>dstt_channels;
	split(scr, scr_channels);
	split(dst, dstt_channels);
	CV_Assert(scr_channels.size() == 4 && dstt_channels.size() == 3);

	if (scale < 1)
	{
		scr_channels[3] *= scale;
		scale = 1;
	}
	for (int i = 0; i < 3; i++)
	{
		//newalpha equal scale * alpha
		dstt_channels[i] = dstt_channels[i].mul(255.0 / scale - scr_channels[3], scale / 255.0);
		dstt_channels[i] += scr_channels[i].mul(scr_channels[3], scale / 255.0);
	}
	merge(dstt_channels, dst);
	return true;
}

void InitLookupTable(void)
{
	for (int i = 0; i < 256; i++)
	{
		table.at<float>(i) = i / 255.0f;
		otable.at<float>(i) = 1 - table.at<float>(i);
	}
	createtable = true;
}