#ifndef cvAdd4cMat_h
#define cvAdd4cMat_h

#include <vector>
#include <opencv2/opencv.hpp>

static int createtable = false;//init lookup-table;
static cv::Mat table(1, 256, CV_32FC1);
static cv::Mat otable(1, 256, CV_32FC1);
int cvAdd4cMat_e(cv::Mat &dst, cv::Mat &scr, double scale);
int cvAdd4cMat_e(cv::Mat &dst, cv::Mat &scr);
int cvAdd4cMat_q(cv::Mat &dst, cv::Mat &scr, double scale = 1.0);
void InitLookupTable(void);

#ifdef CA4M_EXCAT
#define cvAdd4cMat cvAdd4cMat_e
#else
#define cvAdd4cMat cvAdd4cMat_q
#endif

#endif

// From : http://blog.csdn.net/u013097499/article/details/30017739
