//
//  AudioFrameBufferPool.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaStreamer__AudioFrameBufferPool__
#define __MediaStreamer__AudioFrameBufferPool__

#include <vector>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include "MediaDataType.h"

#define MAX_AUDIOFRAME_BUFFER_NUM 4
#define MAX_AUDIOFRAME_BUFFER_SIZE 44100*2*2

using namespace std;

class AudioFrameBufferPool
{
public:
    AudioFrameBufferPool();
    AudioFrameBufferPool(int audioFrameSize, int num);
    AudioFrameBufferPool(int sampleRate, int channelCount, int bitsPerSample, int duration, int num);
    ~AudioFrameBufferPool();
    
    bool push(AudioFrame *audioFrame);
    AudioFrame* front();
    void pop();
    
    void flush();
private:
    pthread_mutex_t mLock;
    vector<AudioFrame*> mAudioFrameBuffers;
    
    int capacity;
    
    int write_pos;
    int read_pos;
    int buffer_num;
};

#endif /* defined(__MediaStreamer__AudioFrameBufferPool__) */
