//
//  AVPacketQueue.cpp
//  MediaStreamer
//
//  Created by slklovewyy on 2018/11/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "AVPacketQueue.h"

#include "MediaPacketQueue.h"
#include "MediaLog.h"

AVPacketQueue::AVPacketQueue()
{
    mCacheDurationUs = 0ll;
    pthread_mutex_init(&mLock, NULL);
    
    mMediaHeaderPts = AV_NOPTS_VALUE;
    mMediaTailerPts = AV_NOPTS_VALUE;
    
    mCacheDataSize = 0ll;
}

AVPacketQueue::~AVPacketQueue()
{
    pthread_mutex_destroy(&mLock);
}

void AVPacketQueue::push(AVPacket* pkt)
{
    if(pkt==NULL) return;
    
    pthread_mutex_lock(&mLock);
    mPacketQueue.push(pkt);
    
    if (pkt->pts==AV_NOPTS_VALUE || pkt->flags < 0) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    if (mCacheDataSize<0) {
        mCacheDataSize = 0ll;
    }
    mCacheDataSize += pkt->size;
    if (mCacheDataSize<0) {
        mCacheDataSize = 0ll;
    }
    
    if (pkt->duration>=0) {
        mMediaTailerPts = pkt->pts + pkt->duration;
    }else{
        mMediaTailerPts = pkt->pts;
    }
    
    if (mMediaHeaderPts==AV_NOPTS_VALUE) {
        mMediaHeaderPts = pkt->pts;
    }
    
    if (pkt->duration>=0) {
        mCacheDurationUs += pkt->duration;
    }
    
    pthread_mutex_unlock(&mLock);
}

AVPacket* AVPacketQueue::front()
{
    AVPacket *pkt = NULL;
    pthread_mutex_lock(&mLock);
    if(!mPacketQueue.empty())
    {
        pkt = mPacketQueue.front();
    }
    pthread_mutex_unlock(&mLock);
    
    return pkt;
}

AVPacket* AVPacketQueue::pop()
{
    AVPacket *pkt = NULL;
    
    pthread_mutex_lock(&mLock);
    if(!mPacketQueue.empty())
    {
        pkt = mPacketQueue.front();
        mPacketQueue.pop();
        
        if (pkt->pts==AV_NOPTS_VALUE || pkt->flags < 0) {
            pthread_mutex_unlock(&mLock);
            return pkt;
        }
        
        if (mCacheDataSize<0) {
            mCacheDataSize = 0ll;
        }
        mCacheDataSize -= pkt->size;
        if (mCacheDataSize<0) {
            mCacheDataSize = 0ll;
        }
        
        if (pkt->duration>=0) {
            mMediaHeaderPts = pkt->pts+pkt->duration;
        }else{
            mMediaHeaderPts = pkt->pts;
        }
        
        if (pkt->duration>=0) {
            mCacheDurationUs -= pkt->duration;
        }
        
        if (mCacheDurationUs<0) {
            mCacheDurationUs = 0ll;
        }
    }
    pthread_mutex_unlock(&mLock);
    
    return pkt;
}

void AVPacketQueue::flush()
{
    pthread_mutex_lock(&mLock);
    while(!mPacketQueue.empty())
    {
        AVPacket* packet = mPacketQueue.front();
        mPacketQueue.pop();
        av_packet_unref(packet);//av_packet_unref
        av_freep(&packet);
    }
    mCacheDurationUs = 0ll;
    
    mMediaHeaderPts = AV_NOPTS_VALUE;
    mMediaTailerPts = AV_NOPTS_VALUE;
    
    mCacheDataSize = 0ll;
    
    pthread_mutex_unlock(&mLock);
}

int64_t AVPacketQueue::duration(int method)
{
    int64_t mCacheDuration = 0;
    
    pthread_mutex_lock(&mLock);
    
    if (method==0) {
        if (mMediaHeaderPts==AV_NOPTS_VALUE || mMediaTailerPts == AV_NOPTS_VALUE) {
            mCacheDuration = 0;
        }else{
            mCacheDuration = mMediaTailerPts - mMediaHeaderPts;
        }
        
        if (mCacheDuration < mCacheDurationUs) {
            mCacheDuration = mCacheDurationUs;
        }
    }else if(method==1) {
        if (mMediaHeaderPts==AV_NOPTS_VALUE || mMediaTailerPts == AV_NOPTS_VALUE) {
            mCacheDuration = 0;
        }else{
            mCacheDuration = mMediaTailerPts - mMediaHeaderPts;
        }
    }else if(method==2) {
        mCacheDuration = mCacheDurationUs;
    }
    
    pthread_mutex_unlock(&mLock);
    
    return mCacheDuration;
}

int64_t AVPacketQueue::size()
{
    int64_t cacheDataSize = 0ll;
    
    pthread_mutex_lock(&mLock);
    
    if (mCacheDataSize<0) {
        mCacheDataSize = 0ll;
    }
    
    cacheDataSize = mCacheDataSize;
    
    pthread_mutex_unlock(&mLock);
    
    return cacheDataSize;
}
