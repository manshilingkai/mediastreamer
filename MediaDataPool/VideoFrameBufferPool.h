//
//  VideoFrameBufferPool.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaStreamer__VideoFrameBufferPool__
#define __MediaStreamer__VideoFrameBufferPool__

#include <vector>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include "MediaDataType.h"

#if defined(IOS)
#include <QuartzCore/QuartzCore.h>
#include <CoreVideo/CoreVideo.h>
#endif

#define VIDEOFRAME_BUFFER_NUM 4

using namespace std;

class VideoFrameBufferPool
{
public:
    VideoFrameBufferPool();
    VideoFrameBufferPool(int capacity);
    VideoFrameBufferPool(int videoWidth, int videoHeight, int videoRawType);
    VideoFrameBufferPool(int capacity, int videoRawType);
    ~VideoFrameBufferPool();

    bool push(VideoFrame *videoFrame);
    bool push(YUVVideoFrame* videoFrame);
    VideoFrame* front();
    void pop();

    void flush();
private:
    int videoFrameBufferPoolCapacity;
    pthread_mutex_t mLock;
    vector<VideoFrame*> mVideoFrameBuffers;
    
    int write_pos;
    int read_pos;
    int buffer_num;
};

#endif /* defined(__MediaStreamer__VideoFrameBufferPool__) */
