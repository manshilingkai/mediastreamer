//
//  AVPacketQueue.h
//  MediaStreamer
//
//  Created by slklovewyy on 2018/11/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef AVPacketQueue_h
#define AVPacketQueue_h

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include <queue>

extern "C" {
#include "libavformat/avformat.h"
}

using namespace std;

class AVPacketQueue {
public:
    AVPacketQueue();
    ~AVPacketQueue();
    
    void push(AVPacket* pkt);
    AVPacket* front();
    AVPacket* pop();
    
    void flush();
    
    //0:AUTO 1:PTS 2:DURATION
    int64_t duration(int method = 0);
    
    int64_t size();
private:
    pthread_mutex_t mLock;
    queue<AVPacket*> mPacketQueue;
    
    int64_t mCacheDurationUs;
    
    int64_t mMediaHeaderPts;
    int64_t mMediaTailerPts;
    
    // size
    int64_t mCacheDataSize;
};

#endif /* AVPacketQueue_h */
