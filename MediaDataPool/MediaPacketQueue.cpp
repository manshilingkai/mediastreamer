//
//  MediaPacketQueue.cpp
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaPacketQueue.h"

MediaPacketQueue::MediaPacketQueue()
{
    pthread_mutex_init(&mLock, NULL);
    
    mVideoHeaderPts = 0;
    mVideoTailerPts = 0;
    
    mAudioHeaderPts = 0;
    mAudioTailerPts = 0;
}

MediaPacketQueue::~MediaPacketQueue()
{
    pthread_mutex_destroy(&mLock);
}

void MediaPacketQueue::push(MediaPacket* mediaPacket)
{
    pthread_mutex_lock(&mLock);
    
    mPacketQueue.push(mediaPacket);
    
    if (mediaPacket->packetType==VIDEO_H264_KEY_FRAME || mediaPacket->packetType==VIDEO_H264_P_OR_B_FRAME || mediaPacket->packetType==VIDEO_VP8_KEY_FRAME || mediaPacket->packetType==VIDEO_VP8_NON_KEY_FRAME) {
        mVideoTailerPts = mediaPacket->pts;
    }
    
    if (mediaPacket->packetType==AUDIO_AAC_BODY || mediaPacket->packetType==AUDIO_AMR_BODY) {
        mAudioTailerPts = mediaPacket->pts;
    }
    
    pthread_mutex_unlock(&mLock);
}

MediaPacket * MediaPacketQueue::pop()
{
    MediaPacket *packet = NULL;

    pthread_mutex_lock(&mLock);

    if (!mPacketQueue.empty()) {
        packet = mPacketQueue.front();
        mPacketQueue.pop();
    }
    
    if (packet!=NULL) {
        if (packet->packetType==VIDEO_H264_KEY_FRAME || packet->packetType==VIDEO_H264_P_OR_B_FRAME || packet->packetType==VIDEO_VP8_KEY_FRAME || packet->packetType==VIDEO_VP8_NON_KEY_FRAME) {
            mVideoHeaderPts = packet->pts;
        }
        
        if (packet->packetType==AUDIO_AAC_BODY || packet->packetType==AUDIO_AMR_BODY) {
            mAudioHeaderPts = packet->pts;
        }
    }

    pthread_mutex_unlock(&mLock);

    return packet;
}

void MediaPacketQueue::flush()
{
    pthread_mutex_lock(&mLock);
    MediaPacket *packet = NULL;
    while (!mPacketQueue.empty()) {
        packet = mPacketQueue.front();
        if (packet!=NULL) {
            if (packet->data!=NULL) {
                free(packet->data);
            }
            
            delete packet;
        }
        mPacketQueue.pop();
    }
    
    mVideoHeaderPts = mVideoTailerPts;
    mAudioHeaderPts = mAudioTailerPts;
    
    pthread_mutex_unlock(&mLock);
}

int MediaPacketQueue::size()
{
    int size = 0;

    pthread_mutex_lock(&mLock);
    size = (int)mPacketQueue.size();
    pthread_mutex_unlock(&mLock);
    
    return size;
}

int64_t MediaPacketQueue::duration()
{
    int64_t durVideo = 0;
    int64_t durAudio = 0;
    
    pthread_mutex_lock(&mLock);
    
    durVideo = mVideoTailerPts - mVideoHeaderPts;
    durAudio = mAudioTailerPts - mAudioHeaderPts;
    
    pthread_mutex_unlock(&mLock);
    
    return durVideo>durAudio? durVideo : durAudio;
}
