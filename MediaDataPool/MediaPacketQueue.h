//
//  MediaPacketQueue.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaStreamer__MediaPacketQueue__
#define __MediaStreamer__MediaPacketQueue__

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include <queue>

#include "MediaDataType.h"

using namespace std;

class MediaPacketQueue
{
public:
    MediaPacketQueue();
    ~MediaPacketQueue();

    void push(MediaPacket* mediaPacket);

    MediaPacket* pop();

    void flush();
    
    int size();
    int64_t duration();

private:
    pthread_mutex_t mLock;
    queue<MediaPacket*> mPacketQueue;
    
    int64_t mVideoHeaderPts;
    int64_t mVideoTailerPts;
    
    int64_t mAudioHeaderPts;
    int64_t mAudioTailerPts;
};

#endif /* defined(__MediaStreamer__MediaPacketQueue__) */
