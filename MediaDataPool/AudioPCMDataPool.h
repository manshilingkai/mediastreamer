//
//  AudioPCMDataPool.h
//  MediaStreamer
//
//  Created by Think on 2016/11/24.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef AudioPCMDataPool_h
#define AudioPCMDataPool_h

#include <stdio.h>
#include <stdlib.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include "MediaDataType.h"

class AudioPCMDataPool {

public:
    AudioPCMDataPool(int sampleRate, int bytesPerSample, int durationMS);
    ~AudioPCMDataPool();
    
    void push(uint8_t* in_data, int in_size, int64_t in_pts);
    
    AudioFrame* pop(int out_size);
    
    void flush();
    
private:
    int mSampleRate;
    int mBytesPerSample;
    
    pthread_mutex_t mLock;
    
    uint8_t *fifo;
    int fifoSize;
    
    int write_pos;
    int read_pos;
    int cache_len;
    
    int64_t currentPtsUS;
    
    AudioFrame mAudioFrame;
};

#endif /* AudioPCMDataPool_h */
