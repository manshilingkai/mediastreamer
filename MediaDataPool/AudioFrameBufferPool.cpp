//
//  AudioFrameBufferPool.cpp
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include <stdlib.h>
#include "AudioFrameBufferPool.h"

AudioFrameBufferPool::AudioFrameBufferPool()
{
    for(int i=0; i<MAX_AUDIOFRAME_BUFFER_NUM;i++)
    {
        AudioFrame *audioFrame = new AudioFrame;
        audioFrame->data = (uint8_t*)malloc(MAX_AUDIOFRAME_BUFFER_SIZE);
        audioFrame->frameSize = MAX_AUDIOFRAME_BUFFER_SIZE;
        mAudioFrameBuffers.push_back(audioFrame);
    }
    
    capacity = MAX_AUDIOFRAME_BUFFER_NUM;
    
    pthread_mutex_init(&mLock, NULL);
    
    write_pos = 0;
    read_pos = 0;
    buffer_num = 0;
}

AudioFrameBufferPool::AudioFrameBufferPool(int audioFrameSize, int num)
{
    for(int i=0; i<num;i++)
    {
        AudioFrame *audioFrame = new AudioFrame;
        audioFrame->data = (uint8_t*)malloc(audioFrameSize);
        audioFrame->frameSize = audioFrameSize;
        mAudioFrameBuffers.push_back(audioFrame);
    }
    
    capacity = num;
    
    pthread_mutex_init(&mLock, NULL);
    
    write_pos = 0;
    read_pos = 0;
    buffer_num = 0;
}

AudioFrameBufferPool::AudioFrameBufferPool(int sampleRate, int channelCount, int bitsPerSample, int duration, int num)
{
    int audioFrameSize = sampleRate*channelCount*bitsPerSample/8*duration/1000;
    
    for(int i=0; i<num;i++)
    {
        AudioFrame *audioFrame = new AudioFrame;
        audioFrame->data = (uint8_t*)malloc(audioFrameSize);
        audioFrame->frameSize = audioFrameSize;
        mAudioFrameBuffers.push_back(audioFrame);
    }
    
    capacity = num;
    
    pthread_mutex_init(&mLock, NULL);
    
    write_pos = 0;
    read_pos = 0;
    buffer_num = 0;
}

AudioFrameBufferPool::~AudioFrameBufferPool()
{
    flush();
    
    for(vector<AudioFrame*>::iterator it = mAudioFrameBuffers.begin(); it != mAudioFrameBuffers.end(); ++it)
    {
        AudioFrame* audioFrame = *it;
        
        if (audioFrame!=NULL) {
            if (audioFrame->data!=NULL) {
                free(audioFrame->data);
                audioFrame->data = NULL;
            }
            
            delete audioFrame;
            audioFrame = NULL;
        }
    }
    
    mAudioFrameBuffers.clear();
    
    pthread_mutex_destroy(&mLock);
}

bool AudioFrameBufferPool::push(AudioFrame* audioFrame)
{
    if (audioFrame==NULL) return false;
    if (audioFrame->data==NULL) return false;
    
    pthread_mutex_lock(&mLock);
    
    if (buffer_num>=capacity) {
        // is full
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (write_pos>=capacity) {
        write_pos = 0;
    }
    
    memcpy(mAudioFrameBuffers[write_pos]->data, audioFrame->data, audioFrame->frameSize);
    mAudioFrameBuffers[write_pos]->frameSize = audioFrame->frameSize;
    mAudioFrameBuffers[write_pos]->duration = audioFrame->duration;
    mAudioFrameBuffers[write_pos]->pts = audioFrame->pts;
    
    write_pos++;
    buffer_num++;
    
    pthread_mutex_unlock(&mLock);

    return true;
}

AudioFrame* AudioFrameBufferPool::front()
{
    pthread_mutex_lock(&mLock);
    
    if (buffer_num<=0) {
        // is empty
        pthread_mutex_unlock(&mLock);
        return NULL;
    }else {
        if (read_pos>=capacity) {
            read_pos = 0;
        }
        int readPos = read_pos;
        pthread_mutex_unlock(&mLock);
        
        return mAudioFrameBuffers[readPos];
    }
}

void AudioFrameBufferPool::pop()
{
    pthread_mutex_lock(&mLock);
    
    read_pos++;
    buffer_num--;
    
    pthread_mutex_unlock(&mLock);
}

void AudioFrameBufferPool::flush()
{
    pthread_mutex_lock(&mLock);
    write_pos = 0;
    read_pos = 0;
    buffer_num = 0;
    pthread_mutex_unlock(&mLock);
}