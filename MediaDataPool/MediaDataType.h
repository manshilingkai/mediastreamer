//
//  MediaDataType.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaStreamer_MediaDataType_h
#define MediaStreamer_MediaDataType_h

#include <vector>
#include <stdlib.h>
#include <stdint.h>

//#include "Mediatimer.h"

using namespace std;

//#define MAX_VIDEO_FRAME_SIZE 1920*1080*4
#define MAX_VIDEO_FRAME_SIZE 4096*2160*4

enum {
    VIDEOFRAME_RAWTYPE_I420 = 0x0001,
    VIDEOFRAME_RAWTYPE_NV12 = 0x0002,
    VIDEOFRAME_RAWTYPE_NV21 = 0x0003,
    VIDEOFRAME_RAWTYPE_BGRA = 0x0004,
    VIDEOFRAME_RAWTYPE_RGBA = 0x0005,
    VIDEOFRAME_RAWTYPE_ARGB = 0x0006,
    VIDEOFRAME_RAWTYPE_ABGR = 0x0007,
    
    VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF = 0x0008,
};

enum {
    VIDEO_HARD_ENCODE = 0,
    VIDEO_SOFT_ENCODE = 1
};

struct VideoOptions
{
    bool hasVideo;
    
    int videoEncodeType;
    
    int videoWidth;
    int videoHeight;
    int videoFps;
    int videoRawType;

    int videoProfile;
    int videoBitRate; //k
    int encodeMode; //0:VBR or 1:CBR
    int maxKeyFrameIntervalMs;
    
    int networkAdaptiveMinFps;
    int networkAdaptiveMinVideoBitrate;
    bool isEnableNetworkAdaptive;

    int quality; //[-5, 5]:CRF
    bool bStrictCBR;
    int deblockingFilterFactor; //[-6, 6] -6 light filter, 6 strong
    
    int rotation;
    bool isAspectFit;
    
    VideoOptions()
    {
        hasVideo = true;
        
        videoEncodeType = VIDEO_SOFT_ENCODE;
        
        videoWidth = 480;
        videoHeight = 640;
        videoFps = 20;
        videoRawType = VIDEOFRAME_RAWTYPE_I420;
        
        videoProfile = 0;
        videoBitRate = 500;
        encodeMode = 0;
        maxKeyFrameIntervalMs = 3000;
        
        networkAdaptiveMinFps = 12;
        networkAdaptiveMinVideoBitrate = 200;
        isEnableNetworkAdaptive = true;
        
        quality = 0;
        bStrictCBR = false;
        deblockingFilterFactor = 0;
        
        rotation = 0;
        isAspectFit = false;
    }
};

struct AudioOptions
{
    bool hasAudio;
    
    int audioSampleRate;
    int audioNumChannels;
    int audioBitRate;//k
    
    bool isRealTime;
    bool isExternalAudioInput;
    
    AudioOptions()
    {
        hasAudio = true;
        
        audioSampleRate = 44100;
        audioNumChannels = 1;
        audioBitRate = 32; //high:64
        
        isRealTime = false;
        isExternalAudioInput = false;
    }
};

//------------------------------------------------------------

struct YUVVideoFrame
{
#define YUV_NUM_DATA_POINTERS 3
    uint8_t *data[YUV_NUM_DATA_POINTERS];
    int linesize[YUV_NUM_DATA_POINTERS];
    
    int planes;
    
    int width;
    int height;
    
    uint64_t pts; //ms
    uint64_t duration; //ms
    
    int rotation;
    
    int videoRawType;
    
    YUVVideoFrame()
    {
        for(int i = 0; i<YUV_NUM_DATA_POINTERS; i++)
        {
            data[i] = NULL;
            linesize[i] = 0;
        }
        
        planes = 0;
        
        width = 0;
        height = 0;
        
        pts = 0;
        duration = 0ll;
        
        rotation = 0;
        videoRawType = VIDEOFRAME_RAWTYPE_I420;
    }
};

struct VideoFrame
{
    uint8_t *data;
    int frameSize;
    
    int width;
    int height;
    
    uint64_t pts; //ms
    uint64_t duration; //ms
    
    int rotation;
    
    int videoRawType;
   
    bool isLastVideoFrame;
    
    bool isKeepFullContent;
    /**
     * for some private data of the user
     */
    void *opaque;
    
    VideoFrame()
    {
        data = NULL;
        frameSize = 0;
        
        width = 0;
        height = 0;
        
        pts = 0;
        duration = 0ll;
        
        rotation = 0;
        videoRawType = VIDEOFRAME_RAWTYPE_NV21;
        
        isKeepFullContent = false;
        
        isLastVideoFrame = false;
        
        opaque = NULL;
    }
    
    inline void Free()
    {
        if (data) {
            free(data);
            data = NULL;
        }
    }
};

struct AudioFrameOptions
{
    uint64_t channelLayout;
    int channels;
    int sampleRate;
    int sampleFormat;
    
    AudioFrameOptions()
    {
        channelLayout = 0;
        channels = 0;
        sampleRate = 0;
        sampleFormat = -1;
    }
};

struct AudioFrame
{
    uint8_t *data;
    int frameSize;
    
    float duration; //ms
    
    uint64_t pts; //ms
    
    int sampleRate;
    int channels;
    int bitsPerChannel;
    
    bool isBigEndian;
    
    AudioFrame()
    {
        data = NULL;
        frameSize  = 0;
        duration = 0.0f;
        pts = 0;
        
        sampleRate = 44100;
        channels = 1;
        bitsPerChannel = 16;
        
        isBigEndian = false;
    }
};

//------------------------------------------------------------
enum {
    VC_TYPE_UNKNOWN = 0x0000,
    H264_TYPE_I = 0x0001,
    H264_TYPE_P = 0x0002,
    H264_TYPE_B = 0x0003,
    H264_TYPE_SPS_PPS = 0x0004,
    
    VP8_TYPE_HEADER = 0x0011,
    VP8_TYPE_KEYFRAME = 0x0012,
    VP8_TYPE_NONKEYFRAME = 0x0013,
};


struct Nal
{
    uint8_t *data;
    int size;
    
    Nal()
    {
        data = NULL;
        size = 0;
    }
};

struct VideoPacket
{
    vector<Nal*> nals;
    int nal_Num;
    
    uint64_t pts;
    uint64_t dts;
    
    int type;

    VideoPacket()
    {
        nal_Num = 0;
        
        pts = 0;
        dts = 0;
        
        type = VC_TYPE_UNKNOWN;
    }
    
    inline void Clear()
    {
        for(vector<Nal*>::iterator it = nals.begin(); it != nals.end(); ++it)
        {
            Nal* nal = *it;
            
            if(nal!=NULL)
            {
                delete nal;
                nal = NULL;
            }
        }
        
        nals.clear();
        
        nal_Num = 0;
        
        pts = 0;
        dts = 0;
        
        type = VC_TYPE_UNKNOWN;

    }
    
    inline void Free()
    {
        for(vector<Nal*>::iterator it = nals.begin(); it != nals.end(); ++it)
        {
            Nal* nal = *it;
            
            if(nal!=NULL)
            {
                if(nal->data!=NULL)
                {
                    free(nal->data);
                    nal->data = NULL;
                }
                
                delete nal;
                nal = NULL;
            }
        }
        
        nals.clear();
        
        nal_Num = 0;
        
        pts = 0;
        dts = 0;
        
        type = VC_TYPE_UNKNOWN;

    }
};

struct AudioPacket
{
    uint8_t *data;
    int size;

    uint64_t pts;
    uint64_t dts;
    
    float duration; //ms

    AudioPacket()
    {
        data = NULL;
        size = 0;
        
        pts = 0;
        dts = 0;
        
        duration = 0.0f;
    }
};

struct TextPacket
{
    uint8_t *data;
    int size;
    
    uint64_t pts;
    
    TextPacket()
    {
        data = NULL;
        size = 0;
        pts = 0;
    }
};

enum Media_Packet_Type
{
    MEDIA_PACKET_UNKNOWN = -1,
    
    VIDEO_H264_SPS_PPS = 0,
    VIDEO_H264_KEY_FRAME = 1,
    VIDEO_H264_P_OR_B_FRAME = 2,

    AUDIO_AAC_HEADER = 3,
    AUDIO_AAC_BODY = 4,
    
    TEXT = 5,
    
    AUDIO_AMR_HEADER = 6,
    AUDIO_AMR_BODY = 7,
    
    VIDEO_VP8_HEADER = 10,
    VIDEO_VP8_KEY_FRAME = 11,
    VIDEO_VP8_NON_KEY_FRAME = 12,
};

struct MediaPacket
{
    Media_Packet_Type packetType;

    uint8_t *data;
    int size;
    
    uint64_t pts;
    uint64_t dts;
    
    float duration; //ms
    
    MediaPacket()
    {
        packetType = MEDIA_PACKET_UNKNOWN;
        data = NULL;
        size = 0;
        
        pts = 0;
        dts = 0;
        
        duration = 0.0f;
    }
};

const uint8_t kAnnexBHeaderBytes[4] = {0, 0, 0, 1};

struct VideoSourceLayout {
    int Zorder;
    
    int x;
    int y;
    
    VideoSourceLayout()
    {
        Zorder = -1;        
        x = y = 0;
    }
};

enum AudioFrameSourceType
{
    AudioFrameSourceUnknown = 0,
    AudioFrameSourceMic = 1,
    AudioFrameSourceApp = 2,
    AudioFrameSourceRemote = 3,
};

#endif
