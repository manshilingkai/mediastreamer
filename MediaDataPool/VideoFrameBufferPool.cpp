//
//  VideoFrameBufferPool.cpp
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include <stdlib.h>
#include "VideoFrameBufferPool.h"
#include "MediaLog.h"

VideoFrameBufferPool::VideoFrameBufferPool()
{
    videoFrameBufferPoolCapacity = VIDEOFRAME_BUFFER_NUM;
    
    int videoFrameSize = MAX_VIDEO_FRAME_SIZE;
    
    for(int i=0; i<videoFrameBufferPoolCapacity;i++)
    {
        VideoFrame *videoFrame = new VideoFrame;
        videoFrame->data = (uint8_t*)malloc(videoFrameSize);
        videoFrame->frameSize = videoFrameSize;
        mVideoFrameBuffers.push_back(videoFrame);
    }
    pthread_mutex_init(&mLock, NULL);
    
    write_pos = 0;
    read_pos = 0;
    buffer_num = 0;
}

VideoFrameBufferPool::VideoFrameBufferPool(int capacity)
{
    videoFrameBufferPoolCapacity = capacity;
    
    int videoFrameSize = MAX_VIDEO_FRAME_SIZE;
    
    for(int i=0; i<videoFrameBufferPoolCapacity;i++)
    {
        VideoFrame *videoFrame = new VideoFrame;
        videoFrame->data = (uint8_t*)malloc(videoFrameSize);
        videoFrame->frameSize = videoFrameSize;
        mVideoFrameBuffers.push_back(videoFrame);
    }
    pthread_mutex_init(&mLock, NULL);
    
    write_pos = 0;
    read_pos = 0;
    buffer_num = 0;
}

VideoFrameBufferPool::VideoFrameBufferPool(int videoWidth, int videoHeight, int videoRawType)
{
    videoFrameBufferPoolCapacity = VIDEOFRAME_BUFFER_NUM;
    
    int videoFrameSize = 0;
    if (videoRawType==VIDEOFRAME_RAWTYPE_BGRA) {
        videoFrameSize = videoWidth*videoHeight*4;
    }else{
        videoFrameSize = videoWidth*videoHeight*3/2;
    }
    
    for(int i=0; i<videoFrameBufferPoolCapacity;i++)
    {
        VideoFrame *videoFrame = new VideoFrame;
        videoFrame->data = (uint8_t*)malloc(videoFrameSize);
        videoFrame->frameSize = videoFrameSize;
        mVideoFrameBuffers.push_back(videoFrame);
    }
    pthread_mutex_init(&mLock, NULL);
    
    write_pos = 0;
    read_pos = 0;
    buffer_num = 0;
}

VideoFrameBufferPool::VideoFrameBufferPool(int capacity, int videoRawType)
{
    videoFrameBufferPoolCapacity = capacity;
    
    for(int i=0; i<videoFrameBufferPoolCapacity;i++)
    {
        VideoFrame *videoFrame = new VideoFrame;
        videoFrame->videoRawType = videoRawType;
        mVideoFrameBuffers.push_back(videoFrame);
    }
    pthread_mutex_init(&mLock, NULL);
    
    write_pos = 0;
    read_pos = 0;
    buffer_num = 0;
}

VideoFrameBufferPool::~VideoFrameBufferPool()
{
    flush();
    
    for(vector<VideoFrame*>::iterator it = mVideoFrameBuffers.begin(); it != mVideoFrameBuffers.end(); ++it)
    {
        VideoFrame* videoFrame = *it;
        
        if (videoFrame!=NULL) {
            if (videoFrame->data!=NULL) {
                free(videoFrame->data);
                videoFrame->data = NULL;
            }
            
            delete videoFrame;
            videoFrame = NULL;
        }
    }
    
    mVideoFrameBuffers.clear();
    
    pthread_mutex_destroy(&mLock);
}

bool VideoFrameBufferPool::push(VideoFrame *videoFrame)
{
    if (videoFrame==NULL) return false;
    if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF) {
        if (videoFrame->opaque==NULL) return false;
    }else{
        if (videoFrame->data==NULL) return false;
    }
    
    pthread_mutex_lock(&mLock);
    
    if (buffer_num>=videoFrameBufferPoolCapacity) {
        // is full
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (write_pos>=videoFrameBufferPoolCapacity) {
        write_pos = 0;
    }
    
    if (videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF) {
        mVideoFrameBuffers[write_pos]->opaque = videoFrame->opaque;
    }else{
        memcpy(mVideoFrameBuffers[write_pos]->data, videoFrame->data, videoFrame->frameSize);
    }
    mVideoFrameBuffers[write_pos]->frameSize = videoFrame->frameSize;
    mVideoFrameBuffers[write_pos]->pts = videoFrame->pts;
    mVideoFrameBuffers[write_pos]->width = videoFrame->width;
    mVideoFrameBuffers[write_pos]->height = videoFrame->height;
    mVideoFrameBuffers[write_pos]->rotation = videoFrame->rotation;
    mVideoFrameBuffers[write_pos]->videoRawType = videoFrame->videoRawType;
	mVideoFrameBuffers[write_pos]->isKeepFullContent = videoFrame->isKeepFullContent;
    
    write_pos++;
    buffer_num++;
    
    pthread_mutex_unlock(&mLock);
    
    return true;
}

bool VideoFrameBufferPool::push(YUVVideoFrame* videoFrame)
{
    if (videoFrame==NULL) return false;
    if (videoFrame->planes<=0) return false;
    
    pthread_mutex_lock(&mLock);
    
    if (buffer_num>=videoFrameBufferPoolCapacity) {
        // is full
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (write_pos>=videoFrameBufferPoolCapacity) {
        write_pos = 0;
    }
    
    if (videoFrame->planes==1) {
        memcpy(mVideoFrameBuffers[write_pos]->data, videoFrame->data[0], videoFrame->linesize[0]*videoFrame->height);
        mVideoFrameBuffers[write_pos]->frameSize = videoFrame->linesize[0]*videoFrame->height;
    }else if(videoFrame->planes==2) {
        if (videoFrame->linesize[0]!=videoFrame->width || videoFrame->linesize[1]!=videoFrame->width) {
            //Y
            for (int i = 0; i<videoFrame->height; i++) {
                memcpy(mVideoFrameBuffers[write_pos]->data+i*videoFrame->width, videoFrame->data[0]+i*videoFrame->linesize[0], videoFrame->width);
            }
            //UV
            for (int i = 0; i<videoFrame->height/2; i++) {
                memcpy(mVideoFrameBuffers[write_pos]->data+videoFrame->width*videoFrame->height+i*videoFrame->width, videoFrame->data[1]+i*videoFrame->linesize[1], videoFrame->width);
            }
        }else{
            //Y
            memcpy(mVideoFrameBuffers[write_pos]->data, videoFrame->data[0], videoFrame->linesize[0]*videoFrame->height);
            //UV
            memcpy(mVideoFrameBuffers[write_pos]->data + videoFrame->linesize[0]*videoFrame->height, videoFrame->data[1], videoFrame->linesize[1]*videoFrame->height/2);
        }
        
        mVideoFrameBuffers[write_pos]->frameSize = videoFrame->linesize[0]*videoFrame->height + videoFrame->linesize[1]*videoFrame->height/2;
        
    }else if(videoFrame->planes==3) {
        memcpy(mVideoFrameBuffers[write_pos]->data, videoFrame->data[0], videoFrame->linesize[0]*videoFrame->height);
        memcpy(mVideoFrameBuffers[write_pos]->data + videoFrame->linesize[0]*videoFrame->height, videoFrame->data[1], videoFrame->linesize[1]*videoFrame->height/2);
        memcpy(mVideoFrameBuffers[write_pos]->data + videoFrame->linesize[0]*videoFrame->height + videoFrame->linesize[1]*videoFrame->height/2, videoFrame->data[2], videoFrame->linesize[2]*videoFrame->height/2);
        
        mVideoFrameBuffers[write_pos]->frameSize = videoFrame->linesize[0]*videoFrame->height + videoFrame->linesize[1]*videoFrame->height/2 + videoFrame->linesize[2]*videoFrame->height/2;
    }
    mVideoFrameBuffers[write_pos]->pts = videoFrame->pts;
    mVideoFrameBuffers[write_pos]->width = videoFrame->width;
    mVideoFrameBuffers[write_pos]->height = videoFrame->height;
    mVideoFrameBuffers[write_pos]->rotation = videoFrame->rotation;
    mVideoFrameBuffers[write_pos]->videoRawType = videoFrame->videoRawType;
    
    write_pos++;
    buffer_num++;
    
    pthread_mutex_unlock(&mLock);
    
    return true;
}

VideoFrame* VideoFrameBufferPool::front()
{
    pthread_mutex_lock(&mLock);
    
    if (buffer_num<=0) {
        // is empty
        pthread_mutex_unlock(&mLock);
        return NULL;
    }else {
        if (read_pos>=videoFrameBufferPoolCapacity) {
            read_pos = 0;
        }
        if (buffer_num==1) {
            mVideoFrameBuffers[read_pos]->isLastVideoFrame = true;
        }else {
            mVideoFrameBuffers[read_pos]->isLastVideoFrame = false;
        }
        int readPos = read_pos;
        pthread_mutex_unlock(&mLock);
        
        return mVideoFrameBuffers[readPos];
    }
}

void VideoFrameBufferPool::pop()
{
    pthread_mutex_lock(&mLock);
    
    VideoFrame *videoFrame = mVideoFrameBuffers[read_pos];
    if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF)
    {
#if defined(IOS)
        if (videoFrame->opaque) {
            CVBufferRelease((CVPixelBufferRef)videoFrame->opaque);
            videoFrame->opaque = NULL;
        }
#endif
    }
    
    read_pos++;
    buffer_num--;
    
    pthread_mutex_unlock(&mLock);
}

void VideoFrameBufferPool::flush()
{
    pthread_mutex_lock(&mLock);
    
    while (buffer_num>0) {
        if (read_pos>=videoFrameBufferPoolCapacity) {
            read_pos = 0;
        }
            
        VideoFrame *videoFrame = mVideoFrameBuffers[read_pos];
        if(videoFrame->videoRawType==VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF)
        {
#if defined(IOS)
            if (videoFrame->opaque) {
                CVBufferRelease((CVPixelBufferRef)videoFrame->opaque);
                videoFrame->opaque = NULL;
            }
#endif
        }
        read_pos++;
        buffer_num--;
    }
    
    write_pos = 0;
    read_pos = 0;
    buffer_num = 0;
    pthread_mutex_unlock(&mLock);
}
