//
//  AudioPCMDataPool.cpp
//  MediaStreamer
//
//  Created by Think on 2016/11/24.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "AudioPCMDataPool.h"
#include "MediaLog.h"

AudioPCMDataPool::AudioPCMDataPool(int sampleRate, int bytesPerSample, int durationMS)
{
    mSampleRate = sampleRate;
    mBytesPerSample = bytesPerSample;
    
    fifoSize = sampleRate*bytesPerSample*durationMS/1000;
    fifo = (uint8_t*)malloc(fifoSize);
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    currentPtsUS = 0;
    
    pthread_mutex_init(&mLock, NULL);
    
    mAudioFrame.data = (uint8_t*)malloc(sampleRate*bytesPerSample);
}

AudioPCMDataPool::~AudioPCMDataPool()
{
    pthread_mutex_destroy(&mLock);
    
    if (fifo!=NULL) {
        free(fifo);
        fifo = NULL;
    }
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    currentPtsUS = 0;
    
    if (mAudioFrame.data!=NULL) {
        free(mAudioFrame.data);
        mAudioFrame.data = NULL;
    }
}

void AudioPCMDataPool::push(uint8_t* in_data, int in_size, int64_t in_pts_us)
{
    pthread_mutex_lock(&mLock);
    
    if(cache_len + in_size>fifoSize)
    {
        // is full
        LOGW("is full");
    }else
    {
        if(in_size>fifoSize-write_pos)
        {
            memcpy(fifo+write_pos,in_data,fifoSize-write_pos);
            memcpy(fifo,in_data+fifoSize-write_pos,in_size-(fifoSize-write_pos));
            write_pos = in_size-(fifoSize-write_pos);
        }else
        {
            memcpy(fifo+write_pos,in_data,in_size);
            write_pos = write_pos+in_size;
        }
        
        currentPtsUS = in_pts_us - cache_len*1000*1000/(mSampleRate*mBytesPerSample);
        
        cache_len = cache_len+in_size;
    }
    
    pthread_mutex_unlock(&mLock);
}

AudioFrame* AudioPCMDataPool::pop(int out_size)
{
    AudioFrame* audioFrame = NULL;
    
    pthread_mutex_lock(&mLock);
    
    if (cache_len >= out_size) {
        
        if(out_size>fifoSize-read_pos)
        {
            memcpy(mAudioFrame.data,fifo+read_pos,fifoSize-read_pos);
            memcpy(mAudioFrame.data+fifoSize-read_pos,fifo,out_size-(fifoSize-read_pos));
            read_pos = out_size-(fifoSize-read_pos);
        }else
        {
            memcpy(mAudioFrame.data,fifo+read_pos,out_size);
            read_pos = read_pos + out_size;
        }
        cache_len = cache_len - out_size;
        
        mAudioFrame.frameSize = out_size;
        mAudioFrame.duration = float(out_size*1000)/float(mSampleRate*mBytesPerSample);
        mAudioFrame.pts = currentPtsUS/1000;
        
        currentPtsUS += out_size*1000*1000/(mSampleRate*mBytesPerSample);
        
        audioFrame = &mAudioFrame;
    }
    
    pthread_mutex_unlock(&mLock);
    
    return audioFrame;
}

void AudioPCMDataPool::flush()
{
    pthread_mutex_lock(&mLock);
    cache_len = 0;
    write_pos = 0;
    read_pos = 0;
    currentPtsUS = 0;
    pthread_mutex_unlock(&mLock);
}
