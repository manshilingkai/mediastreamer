//
//  BitstreamParser.h
//  MediaStreamer
//
//  Created by Think on 2020/12/24.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef BitstreamParser_h
#define BitstreamParser_h

#include <stdio.h>

// This class is an interface for bitstream parsers.
class BitstreamParser {
 public:
  virtual ~BitstreamParser() = default;

  // Parse an additional chunk of the bitstream.
  virtual void ParseBitstream(const uint8_t* bitstream, size_t length) = 0;

  // Get the last extracted QP value from the parsed bitstream. If no QP
  // value could be parsed, returns absl::nullopt.
  virtual bool GetLastSliceQp(int* qp) const = 0;
};

#endif /* BitstreamParser_h */
