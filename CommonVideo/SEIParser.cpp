//
//  SEIParser.cpp
//  MediaStreamer
//
//  Created by Think on 2020/12/23.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "SEIParser.h"

// EBSP to RBSP
std::vector<uint8_t> SEIParser::ParseRbsp(const uint8_t* data, size_t length)
{
    std::vector<uint8_t> out;
    out.reserve(length);

    for (size_t i = 0; i < length;) {
      // Be careful about over/underflow here. byte_length_ - 3 can underflow, and
      // i + 3 can overflow, but byte_length_ - i can't, because i < byte_length_
      // above, and that expression will produce the number of bytes left in
      // the stream including the byte at i.
      if (length - i >= 3 && !data[i] && !data[i + 1] && data[i + 2] == 3) {
        // Two rbsp bytes.
        out.push_back(data[i++]);
        out.push_back(data[i++]);
        // Skip the emulation byte.
        i++;
      } else {
        // Single rbsp byte.
        out.push_back(data[i++]);
      }
    }
    return out;
}

// RBSP to SODB [still keep trailing bits]
void SEIParser::RbspToSodb(std::vector<uint8_t> &buffer)
{
    int bitoffset = 0;
    // find trailing 1 bit
    int ctr_bit = (buffer.back() & (0x01 << bitoffset));

    while (ctr_bit == 0) {
      ++bitoffset;

      if (bitoffset == 8) {
        buffer.pop_back();
        bitoffset = 0;
      }

      ctr_bit = buffer.back() & (0x01 << bitoffset);
    }
}

SEI SEIParser::InterpretSei(std::vector<uint8_t> &buffer)
{
    SEI sei;
    const uint8_t* buf = buffer.data();
    const uint32_t sei_size = buffer.size();
    uint32_t payload_type = 0;
    uint32_t payload_size = 0;
    uint32_t offset = 0;
    uint8_t tmp_byte;

    do {
      tmp_byte = buf[offset++];
      payload_type = 0;

      while (offset < sei_size && tmp_byte == 0xFF) {
        payload_type += 255;
        tmp_byte = buf[offset++];
      }

      payload_type += tmp_byte;

      payload_size = 0;
      if (offset >= sei_size) return sei;
      tmp_byte = buf[offset++];

      while (offset < sei_size && tmp_byte == 0xFF) {
        payload_size += 255;
        tmp_byte = buf[offset++];
      }

      payload_size += tmp_byte;

      switch (payload_type) {
        case SEI_PAYLOAD_TYPE_EXTERNAL:
          sei.external_sei.assign(buf + offset, buf + offset + payload_size);
          break;
        case SEI_PAYLOAD_TYPE_INTERNAL:
          sei.internal_sei.assign(buf + offset, buf + offset + payload_size);
          break;
      }
      offset += payload_size;
    } while (buf[offset] != 0x80);
    return sei;
}

SEI SEIParser::ParseSei(const uint8_t* data, size_t length)
{
    std::vector<uint8_t> unpacked_buffer = ParseRbsp(data, length);
    RbspToSodb(unpacked_buffer);
    return InterpretSei(unpacked_buffer);
}

void SEIParser::GenerateSeiEBSP(std::vector<uint8_t> &sei_ebsp,
                            const std::vector<uint8_t> &sei_data,
                            uint32_t sei_payload_type)
{
    std::vector<uint8_t> raw_sei_info;

    // add sei payload type
    while (sei_payload_type >= 255) {
      raw_sei_info.push_back(0xFF);
        sei_payload_type -= 255;
    }

    raw_sei_info.push_back(static_cast<uint8_t>(sei_payload_type));

    // add sei payload size
    uint32_t len_tmp = static_cast<uint32_t>(sei_data.size());
    while (len_tmp >= 255) {
      raw_sei_info.push_back(0xFF);
      len_tmp -= 255;
    }
    raw_sei_info.push_back(static_cast<uint8_t>(len_tmp));

    // add sei data
    raw_sei_info.insert(raw_sei_info.end(), sei_data.begin(), sei_data.end());

    uint32_t count = 0;
    for (size_t i = 0; i < raw_sei_info.size(); i++) {
      if (count == 2) {
        sei_ebsp.push_back(0x03);
        count = 0;
      }

      if (raw_sei_info[i] == 0) {
        ++count;
      } else {
        count = 0;
      }

      sei_ebsp.push_back(raw_sei_info[i]);
    }
    sei_ebsp.push_back(0x80);
}
