//
//  H264BitstreamParser.h
//  MediaStreamer
//
//  Created by Think on 2020/12/24.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef H264BitstreamParser_h
#define H264BitstreamParser_h

#include <stdio.h>
#include <stdint.h>
#include "BitstreamParser.h"

// TODO
class H264BitstreamParser : public BitstreamParser {
public:
    H264BitstreamParser();
    ~H264BitstreamParser() override;
    
    void ParseBitstream(const uint8_t* bitstream, size_t length) override;
    bool GetLastSliceQp(int* qp) const override;
    
protected:
 enum Result {
   kOk,
   kInvalidStream,
   kUnsupportedStream,
 };
 void ParseSlice(const uint8_t* slice, size_t length);
 Result ParseNonParameterSetNalu(const uint8_t* source,
                                 size_t source_length,
                                 uint8_t nalu_type);
    
 // Last parsed slice QP.
 int32_t last_slice_qp_delta_;
};
#endif /* H264BitstreamParser_h */
