//
//  H265BitstreamCommon.cpp
//  MediaStreamer
//
//  Created by Think on 2020/12/24.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "H265BitstreamCommon.h"
#include "H264BitstreamCommon.h"

namespace H265 {

const uint8_t kNaluTypeMask = 0x7E;

std::vector<NaluIndex> FindNaluIndices(const uint8_t* buffer,
                                       size_t buffer_size) {
  std::vector<H264::NaluIndex> indices = H264::FindNaluIndices(buffer, buffer_size);
  std::vector<NaluIndex> results;
  for (auto& index : indices) {
    results.push_back({index.start_offset, index.payload_start_offset, index.payload_size});
  }
  return results;
}

NaluType ParseNaluType(uint8_t data) {
  return static_cast<NaluType>((data & kNaluTypeMask) >> 1);
}

std::vector<uint8_t> ParseRbsp(const uint8_t* data, size_t length) {
  return H264::ParseRbsp(data, length);
}

}  // namespace H265
