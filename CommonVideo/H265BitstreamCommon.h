//
//  H265BitstreamCommon.h
//  MediaStreamer
//
//  Created by Think on 2020/12/24.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef H265BitstreamCommon_h
#define H265BitstreamCommon_h

#include <stdio.h>
#include <stdint.h>
#include <vector>

namespace H265 {
// The size of a full NALU start sequence {0 0 0 1}, used for the first NALU
// of an access unit, and for SPS and PPS blocks.
const size_t kNaluLongStartSequenceSize = 4;

// The size of a shortened NALU start sequence {0 0 1}, that may be used if
// not the first NALU of an access unit or an SPS or PPS block.
const size_t kNaluShortStartSequenceSize = 3;

// The size of the NALU type byte (2).
const size_t kNaluTypeSize = 2;

enum NaluType : uint8_t {
  kTrailN = 0,
  kTrailR = 1,
  kTsaN = 2,
  kTsaR = 3,
  kStsaN = 4,
  kStsaR = 5,
  kRadlN = 6,
  kRadlR = 7,
  kRaslN = 8,
  kRaslR = 9,
  kRsvVclN10 = 10,
  kRsvVclN11 = 11,
  kRsvVclN12 = 12,
  kRsvVclN13 = 13,
  kRsvVclN14 = 14,
  kRsvVclN15 = 15,
  kBlaWLp = 16,
  kBlaWRadl = 17,
  kBlaNLp = 18,
  kIdrWRadl = 19, // IDR
  kIdrNLp = 20, // IDR
  kCra = 21, // I
  kRsvIrapVcl22 = 22,
  kRsvIrapVcl23 = 23,
  kRsvVcl24 = 24,
  kRsvVcl25 = 25,
  kRsvVcl26 = 26,
  kRsvVcl27 = 27,
  kRsvVcl28 = 28,
  kRsvVcl29 = 29,
  kRsvVcl30 = 30,
  kRsvVcl31 = 31,
  kVps = 32,
  kSps = 33,
  kPps = 34,
  kAud = 35,
  kEos = 36,
  kEob = 37,
  kFilterData = 38,
  kPrefixSei = 39,
  kSuffixSei = 40,
  kRsvNvcl41 = 41,
  kRsvNvcl42 = 42,
  kRsvNvcl43 = 43,
  kRsvNvcl44 = 44,
  kRsvNvcl45 = 45,
  kRsvNvcl46 = 46,
  kRsvNvcl47 = 47,
  kAP = 48,
  kFU = 49,
  kUnSpec50 = 50,
  kUnSpec51 = 51,
  kUnSpec52 = 52,
  kUnSpec53 = 53,
  kUnSpec54 = 54,
  kUnSpec55 = 55,
  kUnSpec56 = 56,
  kUnSpec57 = 57,
  kUnSpec58 = 58,
  kUnSpec59 = 59,
  kUnSpec60 = 60,
  kUnSpec61 = 61,
  kUnSpec62 = 62,
  kUnSpec63 = 63,
  kInvalid = 64
};

enum SliceType : uint8_t { kB = 0, kP = 1, kI = 2 };

struct NaluIndex {
  // Start index of NALU, including start sequence.
  size_t start_offset;
  // Start index of NALU payload, typically type header.
  size_t payload_start_offset;
  // Length of NALU payload, in bytes, counting from payload_start_offset.
  size_t payload_size;
};

// Returns a vector of the NALU indices in the given buffer.
std::vector<NaluIndex> FindNaluIndices(const uint8_t* buffer,
                                       size_t buffer_size);

// Get the NAL type from the header byte immediately following start sequence.
NaluType ParseNaluType(uint8_t data);

// Methods for parsing and writing RBSP. See section 7.4.1 of the H265 spec.
//
// The following sequences are illegal, and need to be escaped when encoding:
// 00 00 00 -> 00 00 03 00
// 00 00 01 -> 00 00 03 01
// 00 00 02 -> 00 00 03 02
// And things in the source that look like the emulation byte pattern (00 00 03)
// need to have an extra emulation byte added, so it's removed when decoding:
// 00 00 03 -> 00 00 03 03
//
// Decoding is simply a matter of finding any 00 00 03 sequence and removing
// the 03 emulation byte.

// Parse the given data and remove any emulation byte escaping.
std::vector<uint8_t> ParseRbsp(const uint8_t* data, size_t length);

}  // namespace H265

#endif /* H265BitstreamCommon_h */
