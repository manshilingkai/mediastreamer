//
//  SEIParser.h
//  MediaStreamer
//
//  Created by Think on 2020/12/23.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef SEIParser_h
#define SEIParser_h

#include <stdio.h>
#include <stdint.h>
#include <vector>

// http://lazybing.github.io/blog/2017/06/22/sodb-rbsp-ebsp/
// https://www.jianshu.com/p/7c6861f0d7fd

enum {
  SEI_PAYLOAD_TYPE_EXTERNAL = 100,
  SEI_PAYLOAD_TYPE_INTERNAL = 101,
};

struct SEI {
  std::vector<uint8_t> external_sei;
  std::vector<uint8_t> internal_sei;
};

class SEIParser {
public:
    // EBSP to RBSP
    static std::vector<uint8_t> ParseRbsp(const uint8_t* data, size_t length);
    // RBSP to SODB
    static void RbspToSodb(std::vector<uint8_t> &buffer);
    static SEI InterpretSei(std::vector<uint8_t> &buffer);
    static SEI ParseSei(const uint8_t* data, size_t length);
    static void GenerateSeiEBSP(std::vector<uint8_t> &sei_nalu,
                                const std::vector<uint8_t> &sei_data,
                                uint32_t sei_type);
};

#endif /* SEIParser_h */
