#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

cd iOS/MediaStreamerFramework-
./compile-framework.sh
cd ../../
