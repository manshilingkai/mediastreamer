//
//  FDKAACEncoder.h
//  MediaStreamer
//
//  Created by Think on 2019/9/17.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef FDKAACEncoder_h
#define FDKAACEncoder_h

#include <stdio.h>
#include "AudioEncoder.h"

#include <fdk-aac/aacenc_lib.h>
#include <fdk-aac/FDK_audio.h>

#define PROFILE_AAC_LC      2               // AOT_AAC_LC
#define PROFILE_AAC_HE      5               // AOT_SBR
#define PROFILE_AAC_HE_v2   29              // AOT_PS PS, Parametric Stereo (includes SBR)
#define PROFILE_AAC_LD      23              // AOT_ER_AAC_LD Error Resilient(ER) AAC LowDelay object
#define PROFILE_AAC_ELD     39              // AOT_ER_AAC_ELD AAC Enhanced Low Delay

class FDKAACEncoder : public AudioEncoder{
public:
    FDKAACEncoder(unsigned int sampleRate, unsigned int numChannels, unsigned int bitRate);
    ~FDKAACEncoder();
    
    bool Open();
    void Close();
    
    bool Encode(AudioFrame &audioFrame, AudioPacket &audioPacket);
    
    //async encode
    void SetOutputHandler(MediaMuxer *mediaMuxer);
    bool Encode(AudioFrame &audioFrame);
    
    AudioPacket* GetHeaders();
    
    void setBitrate(int bitrate);
    
    int GetBitRate();
    
    int GetFixedInputFrameSize();
private:
    static const char *fdkaac_error(AACENC_ERROR erraac);
private:
    unsigned int curSampleRate;
    unsigned int curNumChannels;
    unsigned int curBitRate;
private:
    HANDLE_AACENCODER handle;
    int channel_mode;
    int profile_aac;
    AACENC_InfoStruct info;
    int pcm_frame_len;
    
    uint8_t *output;
    int output_len;
    
    AudioPacket header;
private:
    MediaMuxer *mMediaMuxer;
    bool bOutPutHeader;
};

#endif /* FDKAACEncoder_h */
