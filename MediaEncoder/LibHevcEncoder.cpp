//
//  LibHevcEncoder.cpp
//  MediaStreamer
//
//  Created by Think on 2021/2/8.
//  Copyright © 2021 Cell. All rights reserved.
//

#include "LibHevcEncoder.h"
#include "MediaLog.h"

LibHevcEncoder::LibHevcEncoder()
{
    
}

LibHevcEncoder::~LibHevcEncoder()
{
    
}

IHEVCE_PLUGIN_STATUS_T LibHevcEncoder::openLibhevcEncoder(int w, int h, int fps, int bitrate, int gop, int cores)
{
    libhevcEncoder = NULL;
    
    if(IHEVCE_EFAIL == ihevce_set_def_params(&param))
    {
        LOGE("Unable to set default parameters");
        return IHEVCE_EFAIL;
    }
    
    param.s_config_prms.i4_num_frms_to_encode = INT32_MAX - 1;;
    param.i4_log_dump_level = 0;
    param.s_src_prms.i4_width = w;
    param.s_src_prms.i4_height = h;
    param.s_src_prms.i4_frm_rate_num = 1000 * fps;
    param.s_src_prms.i4_frm_rate_denom = 1000;
    param.s_src_prms.inp_chr_format = IV_YUV_420P;
    param.s_tgt_lyr_prms.as_tgt_params[0].i4_codec_level = 156;
    param.s_tgt_lyr_prms.as_tgt_params[0].ai4_tgt_bitrate[0] = bitrate;//in bps
//    param.s_tgt_lyr_prms.as_tgt_params[0].ai4_frame_qp[0] = 32;
    param.s_coding_tools_prms.i4_max_closed_gop_period = 0;
    param.s_coding_tools_prms.i4_min_closed_gop_period = 0;
    param.s_coding_tools_prms.i4_max_cra_open_gop_period = gop;
    param.s_coding_tools_prms.i4_max_i_open_gop_period = gop;
    param.s_coding_tools_prms.i4_max_temporal_layers = 0;
    param.s_coding_tools_prms.i4_slice_type = 0; // 0 - no slices; 1 - packet based; 2 - CU based
    param.s_tgt_lyr_prms.as_tgt_params[0].i4_quality_preset = IHEVCE_QUALITY_P6;
    param.s_coding_tools_prms.i4_deblocking_type = 0;
    param.s_coding_tools_prms.i4_use_default_sc_mtx = 0;
    param.s_coding_tools_prms.i4_enable_entropy_sync = 0;
    param.s_config_prms.i4_max_tr_tree_depth_I = 1;
    param.s_config_prms.i4_max_tr_tree_depth_nI = 3;
    param.s_config_prms.i4_max_search_range_horz = 512;
    param.s_config_prms.i4_max_search_range_vert = 256;
    param.e_arch_type = ARCH_ARM_A9Q;
    if (cores > MAX_NUM_CORES || cores <1) {
        cores = 4;
    }
    param.s_multi_thrd_prms.i4_max_num_cores = cores;
    param.s_config_prms.i4_rate_control_mode = 5; /* 2- VBR 3- CQP, 5- CBR {2} */
    param.s_config_prms.i4_cu_level_rc = 1;
    param.s_config_prms.i4_max_frame_qp = 51;
    param.s_config_prms.i4_min_frame_qp = 15;
    param.s_lap_prms.i4_rc_look_ahead_pics = 0;
    param.s_out_strm_prms.i4_codec_type = 0; /* 0- HEVC {0} */
    param.s_out_strm_prms.i4_codec_profile = 1; /* 1- Main */
    param.s_out_strm_prms.i4_codec_tier = 0; /* 0- Main 1- High {1} */
    param.s_out_strm_prms.i4_sps_at_cdr_enable = 0;
    param.s_out_strm_prms.i4_vui_enable = 0;
    param.s_out_strm_prms.i4_sei_enable_flag = 0;
    param.s_slice_params.i4_slice_segment_mode = 0;
    
    if(IHEVCE_EFAIL == ihevce_init(&param, &libhevcEncoder))
    {
        libhevcEncoder = NULL;
        LOGE("Unable to initialise libihevce encoder");
        return IHEVCE_EFAIL;
    }
    
    memset(&in_pic, 0, sizeof(in_pic));
    memset(&out_pic, 0, sizeof(out_pic));
    
    PROFILE_INIT(&profile_data);
    
    return IHEVCE_EOK;
}

IHEVCE_PLUGIN_STATUS_T LibHevcEncoder::encode_frame(void* y_data, void* u_data, void* v_data, int y_stride, int u_stride, int v_stride, bool is_force_idr, void** out_buf, long * out_bytes)
{
    memset(&in_pic, 0, sizeof(in_pic));
    memset(&out_pic, 0, sizeof(out_pic));
    
    in_pic.apv_inp_planes[0] = y_data;
    in_pic.apv_inp_planes[1] = u_data;
    in_pic.apv_inp_planes[2] = v_data;

    in_pic.ai4_inp_strd[0] = y_stride;
    in_pic.ai4_inp_strd[1] = u_stride;
    in_pic.ai4_inp_strd[2] = v_stride;

    in_pic.ai4_inp_size[0] = in_pic.ai4_inp_strd[0] * param.s_src_prms.i4_height;
    in_pic.ai4_inp_size[1] = in_pic.ai4_inp_strd[1] * param.s_src_prms.i4_height / 2;
    in_pic.ai4_inp_size[2] = in_pic.ai4_inp_strd[2] * param.s_src_prms.i4_height / 2;
    
    in_pic.i4_curr_bitrate = param.s_tgt_lyr_prms.as_tgt_params[0].ai4_tgt_bitrate[0];
    in_pic.i4_curr_peak_bitrate = param.s_tgt_lyr_prms.as_tgt_params[0].ai4_peak_bitrate[0];
    in_pic.u8_pts = 0;
    in_pic.i4_force_idr_flag = is_force_idr;
    
    PROFILE_START(&profile_data);
    IHEVCE_PLUGIN_STATUS_T status = ihevce_encode(libhevcEncoder, &in_pic, &out_pic);
    PROFILE_STOP(&profile_data, NULL);
    
    if(status != IHEVCE_EOK)
    {
        LOGE("Unable to process encode");
        return IHEVCE_EFAIL;
    }
    
    if(out_pic.i4_bytes_generated)
    {
        *out_buf = out_pic.pu1_output_buf;
        *out_bytes = out_pic.i4_bytes_generated;
    }else {
        *out_buf = NULL;
        *out_bytes = 0;
    }
    
    return IHEVCE_EOK;
}

IHEVCE_PLUGIN_STATUS_T LibHevcEncoder::closeLibhevcEncoder()
{
    PROFILE_END(&profile_data, "encode API call");
    
    if (libhevcEncoder) {
        ihevce_close(libhevcEncoder);
        libhevcEncoder = NULL;
    }
    
    return IHEVCE_EOK;
}

IHEVCE_PLUGIN_STATUS_T LibHevcEncoder::setBitrate(int bitrate)
{
    param.s_tgt_lyr_prms.as_tgt_params[0].ai4_tgt_bitrate[0] = bitrate;
    param.s_tgt_lyr_prms.as_tgt_params[0].ai4_peak_bitrate[0] = bitrate;
    
    return IHEVCE_EOK;
}

IHEVCE_PLUGIN_STATUS_T LibHevcEncoder::getEncoderHdr()
{
    memset(&out_pic_hdr, 0, sizeof(out_pic_hdr));
    return ihevce_encode_header(libhevcEncoder, &out_pic_hdr);
}
