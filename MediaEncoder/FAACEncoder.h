//
//  FAACEncoder.h
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaStreamer__FAACEncoder__
#define __MediaStreamer__FAACEncoder__

#include "AudioEncoder.h"
#include "faac.h"

class FAACEncoder : public AudioEncoder
{
public:
    FAACEncoder(unsigned int sampleRate, unsigned int numChannels, unsigned int bitRate);
    ~FAACEncoder();

    bool Open();
    void Close();
    
    bool Encode(AudioFrame &audioFrame, AudioPacket &audioPacket);
    
    //async encode
    void SetOutputHandler(MediaMuxer *mediaMuxer);
    bool Encode(AudioFrame &audioFrame);
    
    AudioPacket* GetHeaders();
    
    void setBitrate(int bitrate);
    
    int GetBitRate();
    
    int GetFixedInputFrameSize();
private:
    unsigned int curSampleRate;
    unsigned int curNumChannels;
    unsigned int curBitRate;
    faacEncHandle faac;
    
    unsigned long inputSamples;
    unsigned long maxOutputBytes;
    
    uint8_t *aacBuffer;
    AudioPacket header;
    
    MediaMuxer *mMediaMuxer;
    bool bOutPutHeader;

};

#endif /* defined(__MediaStreamer__FAACEncoder__) */
