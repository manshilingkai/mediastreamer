//
//  AudioEncoder.cpp
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "AudioEncoder.h"

#ifdef IOS
#include "ATBEncoder.h"
#endif

#include "FAACEncoder.h"
#include "FDKAACEncoder.h"

//#include "OpenCoreAMRNBEncoder.h"

AudioEncoder* AudioEncoder::CreateAudioEncoder(AUDIO_ENCODER_TYPE type, unsigned int sampleRate, unsigned int numChannels, unsigned int bitRate)
{
#ifdef IOS
    if (type==AUDIO_TOOL_BOX) {
        return new ATBEncoder(sampleRate, numChannels, bitRate);
    }
#endif
    
    if (type==FAAC) {
        return new FAACEncoder(sampleRate, numChannels, bitRate);
    }
    
    if (type==OPENCOREAMRNB) {
//        return new OpenCoreAMRNBEncoder(sampleRate, numChannels, bitRate);
    }
    
    if (type==FDK_AAC) {
        return new FDKAACEncoder(sampleRate, numChannels, bitRate);
    }
    
    return NULL;
}

void AudioEncoder::DeleteAudioEncoder(AudioEncoder* audioEncoder, AUDIO_ENCODER_TYPE type)
{
#ifdef IOS
    if(type==AUDIO_TOOL_BOX)
    {
        ATBEncoder *atbEncoder = (ATBEncoder *)audioEncoder;
        if (atbEncoder) {
            delete atbEncoder;
            atbEncoder = NULL;
        }
    }
#endif
    
    if (type==FAAC) {
        FAACEncoder *faacEncoder = (FAACEncoder*)audioEncoder;
        if (faacEncoder) {
            delete faacEncoder;
            faacEncoder = NULL;
        }
    }
    
    if (type==OPENCOREAMRNB) {
//        OpenCoreAMRNBEncoder *amrNBEncoder = (OpenCoreAMRNBEncoder*)audioEncoder;
//        delete amrNBEncoder;
//        amrNBEncoder = NULL;
    }
    
    if (type==FDK_AAC) {
        FDKAACEncoder* fdkAACEncoder = (FDKAACEncoder*)audioEncoder;
        if (fdkAACEncoder) {
            delete fdkAACEncoder;
            fdkAACEncoder = NULL;
        }
    }
}
