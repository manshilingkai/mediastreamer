//
//  X265Encoder.h
//  MediaStreamer
//
//  Created by slklovewyy on 2020/12/2.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef X265Encoder_h
#define X265Encoder_h

#include <stdio.h>

#include "VideoEncoder.h"

extern "C"
{
    #include "x265.h"
    #include "libavutil/pixdesc.h"
}

class X265Encoder : public VideoEncoder {
public:
    X265Encoder(int fps, int width, int height, VIDEO_ENCODE_PROFILE profile, VIDEO_ENCODE_MODE encodeMode, int bitrate, int quality, int maxKeyFrameIntervalMs, VIDEO_ENCODE_PRESET preset, int vbvBufferSize, bool enableStrictCBR, int deblockingFilterFactor);
    ~X265Encoder();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
    void setEncodeSurface(void *surface) {};
#endif
    
    bool Open();
    void Close();
    
    int Encode(VideoFrame &videoFrame, VideoPacket &videoPacket);
    
#ifdef IOS
    int Encode(void *cvPixelBufferRef, uint64_t pts) {return -1;}
#endif
#ifdef ANDROID
    bool Encode() {return false;}
#endif
    
    void SetOutputHandler(MediaMuxer *mediaMuxer);
    int Encode(VideoFrame &videoFrame);
    
    void SetEncodeMode(VIDEO_ENCODE_MODE encodeMode);
    VIDEO_ENCODE_MODE GetEncodeMode();
    
    void SetBitRate(int bitrate); //kbit
    int GetBitRate();
    
    void SetQuality(int quality);
    int GetQuality();
    
    void SetMaxKeyFrameInterval(int maxKeyFrameIntervalMs);
    int GetMaxKeyFrameInterval();
    
    void SetEncoderPreset(VIDEO_ENCODE_PRESET preset);
    VIDEO_ENCODE_PRESET GetEncoderPreset();
    
    void SetVBVBufferSize(int vbvBufferSize); //kbit
    int GetVBVBufferSize();
    
    void EnableStrictCBR(bool enable);

    //[-6, 6] -6 light filter, 6 strong, 0 default
    void SetDeblockingFilterFactor(int factor) {};
    int GetDeblockingFilterFactor() {return 0;}
    
    bool ReconfigEncodeParam();
    
    VideoPacket * GetHeaders();
    
    void RequestKeyframe();
    int GetBufferedFrames();
private:
    bool is_keyframe(NalUnitType naltype);
    bool libx265_param_parse_float(const char *key, float value);
    bool libx265_param_parse_int(const char *key, int value);
private:
    x265_encoder *encoder;
    x265_param   *params;
    const x265_api *api;
    
    int mFps;
    int mWidth, mHeight;
    VIDEO_ENCODE_PROFILE mProfile;
    VIDEO_ENCODE_MODE mVideoEncodeMode;
    int mBitrate;
    int mQuality;
    int mMaxKeyFrameInterval;
    VIDEO_ENCODE_PRESET mPreset;
    int mVBVBufferSize;
    bool bStrictCBR;
    int mDeblockingFilterFactor;
    
    bool bRequestKeyframe;

    bool isDoReconfig;

    VideoPacket HeaderPacket;
    
    MediaMuxer *mMediaMuxer;
    bool bOutPutHeader;
};

#endif /* X265Encoder_h */
