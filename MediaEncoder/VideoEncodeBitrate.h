//
//  VideoEncodeBitrate.h
//  MediaStreamer
//
//  Created by slklovewyy on 2022/6/10.
//  Copyright © 2022 Cell. All rights reserved.
//

#ifndef VideoEncodeBitrate_h
#define VideoEncodeBitrate_h

#include <stdio.h>

enum ChannelProfile {
  kRTCMode = 0,
  kLiveMode = 1,
  kVODMode = 2,
};

class VideoEncodeBitrate {
public:
    static int CalculateVideoEncodeBitrateKbpsWithFormula(int width, int height, int fps, ChannelProfile channelProfile);
    static int CalculateVideoEncodeBitrateKbpsWithFormulaForH265(int width, int height, int fps, ChannelProfile channelProfile);
};

#endif /* VideoEncodeBitrate_h */
