//
//  VP8Encoder.cpp
//  MediaStreamer
//
//  Created by Think on 2017/2/4.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "VP8Encoder.h"
#include "MediaLog.h"

VP8Encoder::VP8Encoder(int fps, int width, int height, int bitrate, int maxKeyFrameIntervalMs)
{
    mFps = fps;
    mWidth = width;
    mHeight = height;
    
    mBitrate = bitrate;
    mMaxKeyFrameInterval = maxKeyFrameIntervalMs*mFps/1000;
}

VP8Encoder::~VP8Encoder()
{
    HeaderPacket.Free();
}

bool VP8Encoder::Open()
{
    if(!vpx_img_alloc(&raw, VPX_IMG_FMT_I420, mWidth, mHeight, 1)){
        LOGE("Fail to allocate image\n");
        return false;
    }
    
    LOGD("Using %s\n",vpx_codec_iface_name(interface));
    
    vpx_codec_enc_cfg_t cfg;
    vpx_codec_err_t res = vpx_codec_enc_config_default(interface, &cfg, 0);

    if(res) {
        LOGE("Failed to get config: %s\n", vpx_codec_err_to_string(res));
        
        vpx_img_free(&raw);
        
        return false;
    }
    
    cfg.rc_target_bitrate = mBitrate;
    cfg.g_w = mWidth;
    cfg.g_h = mHeight;
    cfg.g_timebase.num = 1;
    cfg.g_timebase.den = 1000;
    cfg.g_lag_in_frames = 0;
    cfg.rc_end_usage = VPX_CBR;

    if(vpx_codec_enc_init(&codec, interface, &cfg, 0)){
        LOGE("Failed to initialize encoder\n");
        
        vpx_img_free(&raw);
        
        return false;
    }
    
    frame_count = 0;
    bOutPutHeader = true;
    isDoReconfig = false;
    bRequestKeyframe = false;

    return true;
}

void VP8Encoder::Close()
{
    vpx_img_free(&raw);
    if (vpx_codec_destroy(&codec)) LOGE("Failed to destroy codec.");
}

int VP8Encoder::Encode(VideoFrame &videoFrame, VideoPacket &videoPacket)
{
    //Y
    raw.stride[0] = videoFrame.width;
    raw.planes[0] = videoFrame.data;
    
    //U
    raw.stride[1] = videoFrame.width/2;
    raw.planes[1] = raw.planes[0] + raw.stride[0] * videoFrame.height;
    
    //V
    raw.stride[2] = videoFrame.width/2;
    raw.planes[2] = raw.planes[1] + raw.stride[1] * videoFrame.height/2;
    
    int flags = 0;
    if (mMaxKeyFrameInterval>0 && frame_count % mMaxKeyFrameInterval == 0) {
        flags |= VPX_EFLAG_FORCE_KF;
    }
    
    if (bRequestKeyframe) {
        flags |= VPX_EFLAG_FORCE_KF;
        
        bRequestKeyframe = false;
    }
    
    const vpx_codec_err_t res = vpx_codec_encode(&codec, &raw, videoFrame.pts, 1000/mFps, flags, VPX_DL_REALTIME);
    if (res != VPX_CODEC_OK)
    {
        LOGE("Failed to encode frame");
        return -1;
    }
    frame_count++;
    
    vpx_codec_iter_t iter = NULL;
    const vpx_codec_cx_pkt_t *pkt = NULL;

    pkt = vpx_codec_get_cx_data(&codec, &iter);
    
    if (pkt!=NULL && pkt->kind == VPX_CODEC_CX_FRAME_PKT) {
        const int keyframe = (pkt->data.frame.flags & VPX_FRAME_IS_KEY) != 0;
        
        //clear videoPacket
        videoPacket.Clear();
        
        Nal *pNal = new Nal;
        pNal->data = (uint8_t *)pkt->data.frame.buf;
        pNal->size = pkt->data.frame.sz;
        
        videoPacket.nals.push_back(pNal);
        
        videoPacket.nal_Num = 1;
        videoPacket.pts = pkt->data.frame.pts;
        videoPacket.dts = videoPacket.pts;
        
        if (keyframe) {
            videoPacket.type = VP8_TYPE_KEYFRAME;
        }else{
            videoPacket.type = VP8_TYPE_NONKEYFRAME;
        }
        
        return 0;

    }else {
        LOGE("Failed to get encoded packet");
        return -1;
    }
}

void VP8Encoder::SetOutputHandler(MediaMuxer *mediaMuxer)
{
    mMediaMuxer = mediaMuxer;
}

int VP8Encoder::Encode(VideoFrame &videoFrame)
{
    if (bOutPutHeader) {
        //Push VP8 Headers to MediaMuxer
        LOGD("Push VP8 Header to MediaMuxer");
        if (mMediaMuxer!=NULL) {
            mMediaMuxer->pushVP8Header(GetHeaders());
        }
        
        bOutPutHeader = false;
    }
    
    //Y
    raw.stride[0] = videoFrame.width;
    raw.planes[0] = videoFrame.data;
    
    //U
    raw.stride[1] = videoFrame.width/2;
    raw.planes[1] = raw.planes[0] + raw.stride[0] * videoFrame.height;
    
    //V
    raw.stride[2] = videoFrame.width/2;
    raw.planes[2] = raw.planes[1] + raw.stride[1] * videoFrame.height/2;
    
    int flags = 0;
    if (mMaxKeyFrameInterval>0 && frame_count % mMaxKeyFrameInterval == 0) {
        flags |= VPX_EFLAG_FORCE_KF;
    }
    
    if (bRequestKeyframe) {
        flags |= VPX_EFLAG_FORCE_KF;

        bRequestKeyframe = false;
    }
    
    const vpx_codec_err_t res = vpx_codec_encode(&codec, &raw, videoFrame.pts, 1000/mFps, flags, VPX_DL_REALTIME);
    if (res != VPX_CODEC_OK)
    {
        LOGE("Failed to encode frame");
        return -1;
    }
    frame_count++;
    
    vpx_codec_iter_t iter = NULL;
    const vpx_codec_cx_pkt_t *pkt = NULL;

    while ((pkt = vpx_codec_get_cx_data(&codec, &iter)) != NULL) {
        if (pkt!=NULL && pkt->kind == VPX_CODEC_CX_FRAME_PKT) {
            const int keyframe = (pkt->data.frame.flags & VPX_FRAME_IS_KEY) != 0;
            
            VideoPacket videoPacket;
            
            Nal *pNal = new Nal;
            pNal->data = (uint8_t *)pkt->data.frame.buf;
            pNal->size = pkt->data.frame.sz;
            
            videoPacket.nals.push_back(pNal);
            
            videoPacket.nal_Num = 1;
            videoPacket.pts = pkt->data.frame.pts;
            
            if (keyframe) {
                videoPacket.type = VP8_TYPE_KEYFRAME;
            }else{
                videoPacket.type = VP8_TYPE_NONKEYFRAME;
            }
            
            if (mMediaMuxer!=NULL) {
                mMediaMuxer->pushVP8Body(&videoPacket);
            }
            
            //clear videoPacket
            videoPacket.Clear();
        }
    }
    
    return 0;
}

VideoPacket * VP8Encoder::GetHeaders()
{
    vpx_fixed_buf_t * headerBuf = vpx_codec_get_global_headers(&codec);
    
    if (headerBuf==NULL) {
        
        LOGE("Failed to get global header");
        
        HeaderPacket.Free();
        
        Nal *pNal = new Nal;
        pNal->size = 1;
        pNal->data = (uint8_t*)malloc(pNal->size);
        memset(pNal->data, 0, pNal->size);
        HeaderPacket.nals.push_back(pNal);
        HeaderPacket.nal_Num = 1;
        HeaderPacket.type = VP8_TYPE_HEADER;
        
        return &HeaderPacket;
    }else{
        HeaderPacket.Free();
        
        Nal *pNal = new Nal;
        pNal->size = headerBuf->sz;
        pNal->data = (uint8_t*)malloc(pNal->size);
        memcpy(pNal->data, headerBuf->buf, headerBuf->sz);
        HeaderPacket.nals.push_back(pNal);
        HeaderPacket.nal_Num = 1;
        HeaderPacket.type = VP8_TYPE_HEADER;
        
        return &HeaderPacket;
    }
}

void VP8Encoder::SetBitRate(int bitrate)
{
    if (this->mBitrate!=bitrate) {
        this->mBitrate = bitrate;
        
        this->isDoReconfig = true;
    }
}

int VP8Encoder::GetBitRate()
{
    return this->mBitrate;
}

void VP8Encoder::SetMaxKeyFrameInterval(int maxKeyFrameIntervalMs)
{
    mMaxKeyFrameInterval = maxKeyFrameIntervalMs*mFps/1000;
}

int VP8Encoder::GetMaxKeyFrameInterval()
{
    return mMaxKeyFrameInterval;
}

bool VP8Encoder::ReconfigEncodeParam()
{
    if (this->isDoReconfig) {
        
        this->isDoReconfig = false;
        
        vpx_codec_enc_cfg_t cfg;
        vpx_codec_err_t res = vpx_codec_enc_config_default(interface, &cfg, 0);
        
        if(res) {
            LOGE("Failed to get config: %s\n", vpx_codec_err_to_string(res));
            
            return false;
        }
        
        cfg.rc_target_bitrate = mBitrate;
        cfg.g_w = mWidth;
        cfg.g_h = mHeight;
        cfg.g_timebase.num = 1;
        cfg.g_timebase.den = 1000;
        cfg.rc_end_usage = VPX_CBR;
        
        res = vpx_codec_enc_config_set(&codec,&cfg);
        
        if(res) {
            LOGE("Failed to change config: %s\n", vpx_codec_err_to_string(res));
            
            return false;
        }
        
    }
    
    return true;
}

void VP8Encoder::RequestKeyframe()
{

}
