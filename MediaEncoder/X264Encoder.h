//
//  x264_encoder.h
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaStreamer__x264_encoder__
#define __MediaStreamer__x264_encoder__

#include "VideoEncoder.h"

extern "C"
{
    #include "x264.h"
}

class X264Encoder : public VideoEncoder
{
private:
    x264_param_t paramData;
    x264_t *x264;
    x264_picture_t picIn;
    x264_picture_t picOut;
    
    int mFps;
    int mWidth, mHeight;
    VIDEO_ENCODE_PROFILE mProfile;
    VIDEO_ENCODE_MODE mVideoEncodeMode;
    int mBitrate;
    int mQuality;
    int mMaxKeyFrameInterval;
    VIDEO_ENCODE_PRESET mPreset;
    int mVBVBufferSize;
    bool bStrictCBR;
    int mDeblockingFilterFactor;
    
    bool bRequestKeyframe;
    
    bool isDoReconfig;
    
    VideoPacket HeaderPacket;
    
    MediaMuxer *mMediaMuxer;
    bool bOutPutHeader;
    
public:
    X264Encoder(int fps, int width, int height, VIDEO_ENCODE_PROFILE profile, VIDEO_ENCODE_MODE encodeMode, int bitrate, int quality, int maxKeyFrameIntervalMs, VIDEO_ENCODE_PRESET preset, int vbvBufferSize, bool enableStrictCBR, int deblockingFilterFactor);
    ~X264Encoder();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
    void setEncodeSurface(void *surface) {};
#endif
    
    bool Open();
    void Close();
    
    int Encode(VideoFrame &videoFrame, VideoPacket &videoPacket);
    
#ifdef IOS
    int Encode(void *cvPixelBufferRef, uint64_t pts) {return -1;}
#endif
#ifdef ANDROID
    bool Encode() {return false;}
#endif
    
    void SetOutputHandler(MediaMuxer *mediaMuxer);
    int Encode(VideoFrame &videoFrame);
    
    void SetEncodeMode(VIDEO_ENCODE_MODE encodeMode);
    VIDEO_ENCODE_MODE GetEncodeMode();
    
    void SetBitRate(int bitrate); //kbit
    int GetBitRate();
    
    void SetQuality(int quality);
    int GetQuality();
    
    void SetMaxKeyFrameInterval(int maxKeyFrameIntervalMs);
    int GetMaxKeyFrameInterval();
    
    void SetEncoderPreset(VIDEO_ENCODE_PRESET preset);
    VIDEO_ENCODE_PRESET GetEncoderPreset();
    
    void SetVBVBufferSize(int vbvBufferSize); //kbit
    int GetVBVBufferSize();
    
    void EnableStrictCBR(bool enable);

    //[-6, 6] -6 light filter, 6 strong, 0 default
    void SetDeblockingFilterFactor(int factor);
    int GetDeblockingFilterFactor();
    
    bool ReconfigEncodeParam();
    
    VideoPacket * GetHeaders();
    
    void RequestKeyframe();
    int GetBufferedFrames();
};



#endif /* defined(__MediaStreamer__x264_encoder__) */
