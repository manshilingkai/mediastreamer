//
//  OpenCoreAMRNBEncoder.cpp
//  MediaStreamer
//
//  Created by Think on 2017/1/17.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "OpenCoreAMRNBEncoder.h"
#include "MediaLog.h"

OpenCoreAMRNBEncoder::OpenCoreAMRNBEncoder(unsigned int sampleRate, unsigned int numChannels, unsigned int bitRate)
{
    if (sampleRate!=8000) {
        LOGE("Only 8000Hz sample rate supported");
    }
    
    if (numChannels != 1) {
        LOGE("Only mono supported");
    }
    
    if (bitRate<=4) {
        enc_mode = MR475;
    }else if(bitRate==5) {
        enc_mode = MR515;
    }else if(bitRate==6) {
        enc_mode = MR59;
    }else if(bitRate==7) {
        enc_mode = MR67;
    }else if(bitRate==8) {
        enc_mode = MR795;
    }else if(bitRate==9) {
        enc_mode = MR102;
    }else if(bitRate==10) {
        enc_mode = MR102;
    }else if(bitRate==11) {
        enc_mode = MR122;
    }else if(bitRate>=12) {
        enc_mode = MR122;
    }
    
    opencore_amrnb_enc_handle = NULL;
    
    memcpy(amrNBFileHeader, "#!AMR\n", 6);
    
    amrNBBuffer = (uint8_t*)malloc(500);
    
    mMediaMuxer = NULL;
    bOutPutHeader = true;
    
}

OpenCoreAMRNBEncoder::~OpenCoreAMRNBEncoder()
{
    free(amrNBBuffer);
}

bool OpenCoreAMRNBEncoder::Open()
{
    opencore_amrnb_enc_handle = Encoder_Interface_init(0);
    
    if (opencore_amrnb_enc_handle!=NULL) {
        return true;
    }else return false;
}

void OpenCoreAMRNBEncoder::Close()
{
    if (opencore_amrnb_enc_handle!=NULL) {
        Encoder_Interface_exit(opencore_amrnb_enc_handle);
        opencore_amrnb_enc_handle = NULL;
    }
}

bool OpenCoreAMRNBEncoder::Encode(AudioFrame &audioFrame, AudioPacket &audioPacket)
{
    memset(&audioPacket, 0, sizeof(AudioPacket));

    int written = Encoder_Interface_Encode(opencore_amrnb_enc_handle, enc_mode, (short*)audioFrame.data, amrNBBuffer, 0);
    
    audioPacket.data = amrNBBuffer;
    audioPacket.size = written;
    audioPacket.pts = audioFrame.pts;
    audioPacket.dts = audioFrame.pts;
    audioPacket.duration = audioFrame.duration;
    
    return true;
}

void OpenCoreAMRNBEncoder::SetOutputHandler(MediaMuxer *mediaMuxer)
{
    mMediaMuxer = mediaMuxer;
    
    if (bOutPutHeader) {
        LOGD("Push AMRNB Header to MediaMuxer");
        if (mMediaMuxer!=NULL) {
            mMediaMuxer->pushAMRHeader(this->GetHeaders());
        }
        
        bOutPutHeader = false;
    }
}

bool OpenCoreAMRNBEncoder::Encode(AudioFrame &audioFrame)
{
    AudioPacket outAudioPacket;
    this->Encode(audioFrame, outAudioPacket);
    
    if (mMediaMuxer!=NULL) {
        mMediaMuxer->pushAMRBody(&outAudioPacket);
    }
    
    return true;
}

AudioPacket* OpenCoreAMRNBEncoder::GetHeaders()
{
    header.data = amrNBFileHeader;
    header.size = 6;
    
    return &header;
}


void OpenCoreAMRNBEncoder::setBitrate(int bitrate)
{
    enum Mode encMode;
    
    if (bitrate<=4) {
        encMode = MR475;
    }else if(bitrate==5) {
        encMode = MR515;
    }else if(bitrate==6) {
        encMode = MR59;
    }else if(bitrate==7) {
        encMode = MR67;
    }else if(bitrate==8) {
        encMode = MR795;
    }else if(bitrate==9) {
        encMode = MR102;
    }else if(bitrate==10) {
        encMode = MR102;
    }else if(bitrate==11) {
        encMode = MR122;
    }else if(bitrate>=12) {
        encMode = MR122;
    }
    
    if (encMode!=this->enc_mode) {
        this->enc_mode = encMode;
    }
}

int OpenCoreAMRNBEncoder::GetBitRate()
{
    if (enc_mode==MR475) {
        return 4;
    }else if(enc_mode==MR515) {
        return 5;
    }else if(enc_mode==MR59) {
        return 6;
    }else if(enc_mode==MR67) {
        return 7;
    }else if(enc_mode==MR795) {
        return 8;
    }else if(enc_mode==MR102) {
        return 10;
    }else if(enc_mode==MR122) {
        return 12;
    }else return 0;
}

int OpenCoreAMRNBEncoder::GetFixedInputFrameSize()
{
    return 160*sizeof(short);
}
