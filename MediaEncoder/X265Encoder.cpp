//
//  X265Encoder.cpp
//  MediaStreamer
//
//  Created by slklovewyy on 2020/12/2.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "X265Encoder.h"
#include "MediaLog.h"

static const float baseCRF = 28.0f;

X265Encoder::X265Encoder(int fps, int width, int height, VIDEO_ENCODE_PROFILE profile, VIDEO_ENCODE_MODE encodeMode, int bitrate, int quality, int maxKeyFrameIntervalMs, VIDEO_ENCODE_PRESET preset, int vbvBufferSize, bool enableStrictCBR, int deblockingFilterFactor)
{
    this->mFps = fps;
    this->mWidth = width;
    this->mHeight = height;
    this->mProfile = profile;
    
    this->mVideoEncodeMode = encodeMode;
    this->mBitrate = bitrate;
    this->mQuality = quality;
    this->mMaxKeyFrameInterval = maxKeyFrameIntervalMs;
    this->mPreset = preset;
    this->mVBVBufferSize = vbvBufferSize;
    this->bStrictCBR = enableStrictCBR;
    this->mDeblockingFilterFactor = deblockingFilterFactor;
    
    this->bRequestKeyframe = false;
    this->isDoReconfig = false;
    
    encoder = NULL;
    params = NULL;
    api = NULL;
    
    this->mMediaMuxer = NULL;
    this->bOutPutHeader = true;
}

X265Encoder::~X265Encoder()
{
    this->Close();
    HeaderPacket.Free();
}

bool X265Encoder::is_keyframe(NalUnitType naltype)
{
    switch (naltype) {
    case NAL_UNIT_CODED_SLICE_BLA_W_LP:
    case NAL_UNIT_CODED_SLICE_BLA_W_RADL:
    case NAL_UNIT_CODED_SLICE_BLA_N_LP:
    case NAL_UNIT_CODED_SLICE_IDR_W_RADL:
    case NAL_UNIT_CODED_SLICE_IDR_N_LP:
    case NAL_UNIT_CODED_SLICE_CRA:
        return true;
    default:
        return false;
    }
}

bool X265Encoder::libx265_param_parse_float(const char *key, float value)
{
    char buf[256];

    snprintf(buf, sizeof(buf), "%2.2f", value);
    if (api->param_parse(params, key, buf) == X265_PARAM_BAD_VALUE) {
        LOGE("Invalid value %2.2f for param %s", value, key);
        return false;
    }
    
    return true;
}

bool X265Encoder::libx265_param_parse_int(const char *key, int value)
{
    char buf[256];

    snprintf(buf, sizeof(buf), "%d", value);
    if (api->param_parse(params, key, buf) == X265_PARAM_BAD_VALUE) {
        LOGE("Invalid value %d for param %s", value, key);
        return false;
    }
    
    return true;
}

#ifdef ANDROID
void X265Encoder::registerJavaVMEnv(JavaVM *jvm)
{
}
#endif

bool X265Encoder::Open()
{
    api = x265_api_get(av_pix_fmt_desc_get(AV_PIX_FMT_YUV420P)->comp[0].depth);
    if (!api) {
        api = x265_api_get(0);
    }
    
    params = api->param_alloc();
    if (!params) {
        LOGE("Could not allocate x265 param structure");
        
        api = NULL;
        return false;
    }
    
    if (api->param_default_preset(params, x265_preset_names[this->mPreset], x265_tune_names[3]) < 0) {
        LOGE("Error setting preset/tune %s/%s.\n", x265_preset_names[this->mPreset], x265_tune_names[3]);
        
        for (int i = 0; x265_preset_names[i]; i++)
            LOGI(" %s", x265_preset_names[i]);

        LOGI("\n");
        LOGI("Possible tunes:");
        for (int i = 0; x265_tune_names[i]; i++)
            LOGI(" %s", x265_tune_names[i]);

        LOGI("\n");
        
        if (params) {
            api->param_free(params);
            params = NULL;
        }
        api = NULL;
        return false;
    }
    
    params->frameNumThreads = 0; //for auto-detection
    params->fpsNum = this->mFps * 1000;
    params->fpsDenom = 1000;
    
    params->sourceWidth = this->mWidth / 2 * 2; // hevc encoder do not support odd resolution
    params->sourceHeight = this->mHeight / 2 * 2; // hevc encoder do not support odd resolution
    params->bEnablePsnr = 0;
    params->bOpenGOP = 1;
    
    /* Tune the CTU size based on input resolution. */
    if (params->sourceWidth < 64 || params->sourceHeight < 64)
        params->maxCUSize = 32;
    if (params->sourceWidth < 32 || params->sourceHeight < 32)
        params->maxCUSize = 16;
    if (params->sourceWidth < 16 || params->sourceHeight < 16) {
        LOGE("Image size is too small (%dx%d).\n",params->sourceWidth, params->sourceHeight);
        
        if (params) {
            api->param_free(params);
            params = NULL;
        }
        api = NULL;
        return false;
    }
    
    params->internalCsp = X265_CSP_I420;
    
    if (this->mVideoEncodeMode==VBR)
    {
        char crf[6];

        snprintf(crf, sizeof(crf), "%2.2f", baseCRF+this->mQuality);
        if (api->param_parse(params, "crf", crf) == X265_PARAM_BAD_VALUE) {
            LOGE("Invalid crf: %2.2f.\n", baseCRF+this->mQuality);
            
            if (params) {
                api->param_free(params);
                params = NULL;
            }
            api = NULL;
            return false;
        }
        
        params->rc.rateControlMode = X265_RC_CRF;
        params->rc.rfConstantMin = 23;
        params->rc.rfConstantMax = 33;
    }else {
        params->rc.bitrate         = this->mBitrate;
        params->rc.rateControlMode = X265_RC_ABR;
        
        if (bStrictCBR) {
            params->rc.bStrictCbr = 1;
        }
    }
    
    bool ret = libx265_param_parse_int("qpmin", 10);
    if (!ret) {
        if (params) {
            api->param_free(params);
            params = NULL;
        }
        api = NULL;
        return false;
    }
    
    ret = libx265_param_parse_int("qpmax", 40);
    if (!ret) {
        if (params) {
            api->param_free(params);
            params = NULL;
        }
        api = NULL;
        return false;
    }
    
    params->rc.vbvMaxBitrate = params->rc.bitrate * 2;
    params->rc.vbvBufferSize = params->rc.vbvMaxBitrate;
    
    params->bRepeatHeaders = 0; //maybe webrtc need repeat
    
    params->keyframeMax = this->mFps*mMaxKeyFrameInterval/1000;
    params->keyframeMin = params->keyframeMax < this->mFps ? params->keyframeMax : this->mFps;
    
    if (api->param_apply_profile(params, x265_profile_names[this->mProfile - 10]) < 0) {
        LOGE("Invalid or incompatible profile set: %s.\n", x265_profile_names[this->mProfile -10]);
        LOGI("Possible profiles:");
        for (int i = 0; x265_profile_names[i]; i++)
            LOGI(" %s", x265_profile_names[i]);
        LOGI("\n");
        
        if (params) {
            api->param_free(params);
            params = NULL;
        }
        api = NULL;
        return false;
    }
    
    encoder = api->encoder_open(params);
    if (!encoder) {
        LOGE("Cannot open libx265 encoder.\n");
        Close();
        return false;
    }
    
    LOGD("%s","Finish Open X265Encoder");
    
    return true;
}

void X265Encoder::Close()
{
    if (api) {
        if (params) {
            api->param_free(params);
            params = NULL;
        }
        
        if (encoder) {
            api->encoder_close(encoder);
            encoder = NULL;
        }
        
        api = NULL;
    }
    
    LOGD("%s","Finish Close X265Encoder");
}

int X265Encoder::Encode(VideoFrame &videoFrame, VideoPacket &videoPacket)
{
    x265_picture x265pic;
    x265_picture x265pic_out = { 0 };
    x265_nal *nalOut;
    uint32_t nalNum = 0;
    
    api->picture_init(params, &x265pic);
    x265pic.pts = videoFrame.pts;
    x265pic.bitDepth = av_pix_fmt_desc_get(AV_PIX_FMT_YUV420P)->comp[0].depth;
    x265pic.sliceType = X265_TYPE_AUTO;
    if(bRequestKeyframe) {
        x265pic.sliceType = X265_TYPE_IDR;
    }
    
    x265pic.colorSpace = X265_CSP_I420;
    x265pic.stride[0] = this->mWidth;
    x265pic.planes[0] = videoFrame.data;
    x265pic.stride[1] = this->mWidth / 2;
    x265pic.planes[1] = (uint8_t*)x265pic.planes[0] + x265pic.stride[0]*this->mHeight;
    x265pic.stride[2] = this->mWidth / 2;
    x265pic.planes[2] = (uint8_t*)x265pic.planes[1] + x265pic.stride[1]*this->mHeight/2;
    
    //you can set roi here
    //you can set userdata here
    
    int ret = api->encoder_encode(encoder, &nalOut, &nalNum, &x265pic, &x265pic_out);
    
    if (ret < 0) {
        LOGE("%s","x265 encode failed");
        return -1;
    }
    
    if(bRequestKeyframe)
    {
        bRequestKeyframe = false;
    }
    
    if (!nalNum) return 0;
    
    //clear videoPacket
    videoPacket.Clear();
    
    for(int i=0; i<nalNum; i++)
    {
        x265_nal &nal = nalOut[i];
        
        Nal *pNal = new Nal;
        pNal->data = nal.payload;
        pNal->size = nal.sizeBytes;
        
        videoPacket.nals.push_back(pNal);
        
        if (is_keyframe((NalUnitType)nal.type))
        {
            videoPacket.type = H265_TYPE_I;
        }
    }
    
    videoPacket.nal_Num = nalNum;
    
    videoPacket.pts = x265pic_out.pts;
    videoPacket.dts = x265pic_out.dts;

    switch (x265pic_out.sliceType) {
    case X265_TYPE_IDR:
    case X265_TYPE_I:
        videoPacket.type = H265_TYPE_I;
        break;
    case X265_TYPE_P:
        videoPacket.type = H265_TYPE_P;
        break;
    case X265_TYPE_B:
    case X265_TYPE_BREF:
        videoPacket.type = H265_TYPE_B;
        break;
    default:
        videoPacket.type = VC_TYPE_UNKNOWN;
    }
    
    return 1;
}

void X265Encoder::SetOutputHandler(MediaMuxer *mediaMuxer)
{
    mMediaMuxer = mediaMuxer;
}

int X265Encoder::Encode(VideoFrame &videoFrame)
{
    if (bOutPutHeader) {
        //Push H265 Headers to MediaMuxer
        LOGD("Push H265 Header(VPS, SPS and PPS) to MediaMuxer");
        if (mMediaMuxer!=NULL) {
//            mMediaMuxer->pushH265Header(GetHeaders());
        }
        
        bOutPutHeader = false;
    }
    
    VideoPacket videoPacket;
    
    int ret = this->Encode(videoFrame, videoPacket);
    
    if (ret<0)
    {
        videoPacket.Clear();
        return ret;
    }
    
    if (mMediaMuxer!=NULL) {
//        mMediaMuxer->pushH265Body(&videoPacket);
    }
    
    videoPacket.Clear();
    
    return 0;
}

void X265Encoder::SetEncodeMode(VIDEO_ENCODE_MODE encodeMode)
{
    if (encodeMode!=this->mVideoEncodeMode) {
        this->mVideoEncodeMode = encodeMode;
        
        this->isDoReconfig = true;
    }
}

VIDEO_ENCODE_MODE X265Encoder::GetEncodeMode()
{
    return this->mVideoEncodeMode;
}

void X265Encoder::SetBitRate(int bitrate)
{
    if (this->mBitrate!=bitrate) {
        this->mBitrate = bitrate;
        
        this->isDoReconfig = true;
    }
}

int X265Encoder::GetBitRate()
{
    return this->mBitrate;
}

void X265Encoder::SetQuality(int quality)
{
    if (this->mQuality!=quality) {
        this->mQuality=quality;
        
        this->isDoReconfig = true;
    }
}

int X265Encoder::GetQuality()
{
    return this->mQuality;
}

void X265Encoder::SetMaxKeyFrameInterval(int maxKeyFrameIntervalMs)
{
    if (this->mMaxKeyFrameInterval!=maxKeyFrameIntervalMs) {
        this->mMaxKeyFrameInterval=maxKeyFrameIntervalMs;
        
        this->isDoReconfig = true;
    }
}

int X265Encoder::GetMaxKeyFrameInterval()
{
    return this->mMaxKeyFrameInterval;
}

void X265Encoder::SetEncoderPreset(VIDEO_ENCODE_PRESET preset)
{
    if (this->mPreset!=preset) {
        this->mPreset=preset;
        
        this->isDoReconfig = true;
    }
}

VIDEO_ENCODE_PRESET X265Encoder::GetEncoderPreset()
{
    return this->mPreset;
}

void X265Encoder::SetVBVBufferSize(int vbvBufferSize)
{
    if (this->mVBVBufferSize!=vbvBufferSize) {
        this->mVBVBufferSize=vbvBufferSize;
        
        this->isDoReconfig = true;
    }
}

int X265Encoder::GetVBVBufferSize()
{
    return this->mVBVBufferSize;
}

void X265Encoder::EnableStrictCBR(bool enable)
{
    if (this->bStrictCBR!=enable) {
        this->bStrictCBR = enable;
        
        this->isDoReconfig = true;
    }
}

bool X265Encoder::ReconfigEncodeParam()
{
    if (this->isDoReconfig && encoder!=NULL)
    {
        if (api->param_default_preset(params, x265_preset_names[this->mPreset], x265_tune_names[3]) < 0) {
            LOGE("Error setting preset/tune %s/%s.\n", x265_preset_names[this->mPreset], x265_tune_names[3]);
            
            for (int i = 0; x265_preset_names[i]; i++)
                LOGI(" %s", x265_preset_names[i]);

            LOGI("\n");
            LOGI("Possible tunes:");
            for (int i = 0; x265_tune_names[i]; i++)
                LOGI(" %s", x265_tune_names[i]);

            LOGI("\n");
            
            return false;
        }
        
        params->frameNumThreads = 0; //for auto-detection
        params->fpsNum = this->mFps * 1000;
        params->fpsDenom = 1000;
        
        params->sourceWidth = this->mWidth / 2 * 2; // hevc encoder do not support odd resolution
        params->sourceHeight = this->mHeight / 2 * 2; // hevc encoder do not support odd resolution
        params->bEnablePsnr = 0;
        params->bOpenGOP = 1;
        
        /* Tune the CTU size based on input resolution. */
        if (params->sourceWidth < 64 || params->sourceHeight < 64)
            params->maxCUSize = 32;
        if (params->sourceWidth < 32 || params->sourceHeight < 32)
            params->maxCUSize = 16;
        if (params->sourceWidth < 16 || params->sourceHeight < 16) {
            LOGE("Image size is too small (%dx%d).\n",params->sourceWidth, params->sourceHeight);
            
            return false;
        }
        
        params->internalCsp = X265_CSP_I420;
        
        if (this->mVideoEncodeMode==VBR)
        {
            char crf[6];

            snprintf(crf, sizeof(crf), "%2.2f", baseCRF+this->mQuality);
            if (api->param_parse(params, "crf", crf) == X265_PARAM_BAD_VALUE) {
                LOGE("Invalid crf: %2.2f.\n", baseCRF+this->mQuality);
                
                return false;
            }
            
            params->rc.rateControlMode = X265_RC_CRF;
            params->rc.rfConstantMin = 23;
            params->rc.rfConstantMax = 33;
        }else {
            params->rc.bitrate         = this->mBitrate;
            params->rc.rateControlMode = X265_RC_ABR;
            
            if (bStrictCBR) {
                params->rc.bStrictCbr = 1;
            }
        }
        
        bool ret = libx265_param_parse_int("qpmin", 10);
        if (!ret) {
            return false;
        }
        
        ret = libx265_param_parse_int("qpmax", 40);
        if (!ret) {
            return false;
        }
        
        params->rc.vbvMaxBitrate = params->rc.bitrate * 2;
        params->rc.vbvBufferSize = params->rc.vbvMaxBitrate;
        
        params->bRepeatHeaders = 0; //maybe webrtc need repeat
        
        params->keyframeMax = this->mFps*mMaxKeyFrameInterval/1000;
        params->keyframeMin = params->keyframeMax < this->mFps ? params->keyframeMax : this->mFps;
        
        if (api->param_apply_profile(params, x265_profile_names[this->mProfile - 10]) < 0) {
            LOGE("Invalid or incompatible profile set: %s.\n", x265_profile_names[this->mProfile -10]);
            LOGI("Possible profiles:");
            for (int i = 0; x265_profile_names[i]; i++)
                LOGI(" %s", x265_profile_names[i]);
            LOGI("\n");
            
            return false;
        }

        
        int i_ret = api->encoder_reconfig(encoder, params);
        
        if (i_ret) {
            LOGD("Reconfig Encode Param Fail");
            return false;
        }else{
            LOGD("Reconfig Encode Param Success");
            return true;
        }
    }
    
    return false;
}


VideoPacket * X265Encoder::GetHeaders()
{
    HeaderPacket.Free();
    
    x265_nal *nalOut;
    uint32_t nalNum = 0;
    
    api->encoder_headers(encoder, &nalOut, &nalNum);
    
    if(nalNum>0)
    {
        Nal *pNal = new Nal;
        pNal->size = MAX_H265_HEADER_SIZE;
        pNal->data = (uint8_t*)malloc(pNal->size);
        HeaderPacket.nals.push_back(pNal);
        
        pNal = new Nal;
        pNal->size = MAX_H265_HEADER_SIZE;
        pNal->data = (uint8_t*)malloc(pNal->size);
        HeaderPacket.nals.push_back(pNal);
        
        pNal = new Nal;
        pNal->size = MAX_H265_HEADER_SIZE;
        pNal->data = (uint8_t*)malloc(pNal->size);
        HeaderPacket.nals.push_back(pNal);
        
        HeaderPacket.nal_Num = 3;
    }
    
    for(int i=0; i<nalNum; i++)
    {
        x265_nal &nal = nalOut[i];
        
        if(nal.type == NAL_UNIT_VPS)
        {
            memcpy(HeaderPacket.nals[0]->data, nal.payload, nal.sizeBytes);
            HeaderPacket.nals[0]->size = nal.sizeBytes;
        }else if(nal.type == NAL_UNIT_SPS) {
            memcpy(HeaderPacket.nals[1]->data, nal.payload, nal.sizeBytes);
            HeaderPacket.nals[1]->size = nal.sizeBytes;
        }else if(nal.type == NAL_UNIT_PPS) {
            memcpy(HeaderPacket.nals[2]->data, nal.payload, nal.sizeBytes);
            HeaderPacket.nals[2]->size = nal.sizeBytes;
        }
    }
    
    HeaderPacket.type = H265_TYPE_VPS_SPS_PPS;
    
    return &HeaderPacket;
}

void X265Encoder::RequestKeyframe()
{
    bRequestKeyframe = true;
}

int X265Encoder::GetBufferedFrames()
{
    return 0;
}
