//
//  VP8Encoder.h
//  MediaStreamer
//
//  Created by Think on 2017/2/4.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef VP8Encoder_h
#define VP8Encoder_h

#include "VideoEncoder.h"

extern "C"
{
#include "vpx_encoder.h"
#include "vp8cx.h"
}

#define interface (&vpx_codec_vp8_cx_algo)

class VP8Encoder : public VideoEncoder{
public:
    VP8Encoder(int fps, int width, int height, int bitrate, int maxKeyFrameIntervalMs);
    ~VP8Encoder();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm) {};
#endif
    
    bool Open();
    void Close();
    
#ifdef IOS
    int Encode(void *cvPixelBufferRef, uint64_t pts) {return -1;}
#endif
    
    int Encode(VideoFrame &videoFrame, VideoPacket &videoPacket);
    
    void SetOutputHandler(MediaMuxer *mediaMuxer);
    int Encode(VideoFrame &videoFrame);
    
    void SetEncodeMode(VIDEO_ENCODE_MODE encodeMode) {}
    VIDEO_ENCODE_MODE GetEncodeMode() { return CBR; }
    
    void SetBitRate(int bitrate); //kbit
    int GetBitRate();
    
    void SetQuality(int quality) {}
    int GetQuality() {return 0;}
    
    void SetMaxKeyFrameInterval(int maxKeyFrameIntervalMs);
    int GetMaxKeyFrameInterval();
    
    void SetEncoderPreset(VIDEO_ENCODE_PRESET preset) {}
    VIDEO_ENCODE_PRESET GetEncoderPreset() {return ULTRAFAST;}
    
    void SetVBVBufferSize(int vbvBufferSize) {} //kbit
    int GetVBVBufferSize() {return 0;}
    
    void EnableStrictCBR(bool enable) {}
    
    //[-6, 6] -6 light filter, 6 strong, 0 default
    void SetDeblockingFilterFactor(int factor) {}
    int GetDeblockingFilterFactor() {return 0;}
    
    bool ReconfigEncodeParam();
    
    VideoPacket * GetHeaders();
    
    void RequestKeyframe();
    int GetBufferedFrames() {return 0;}
private:
    int mFps;
    int mWidth, mHeight;
    int mBitrate;
    int mMaxKeyFrameInterval;
    
    MediaMuxer *mMediaMuxer;
    
    VideoPacket HeaderPacket;
    bool bOutPutHeader;
    
    int64_t frame_count;
    
    bool isDoReconfig;
    bool bRequestKeyframe;

    vpx_codec_ctx_t codec;
    vpx_image_t raw;
};

#endif /* VP8Encoder_h */
