//
//  VideoEncoder.cpp
//  MediaStreamer
//
//  Created by 施灵凯 on 15/6/5.
//  Copyright (c) 2015年 Cell. All rights reserved.
//

#include "VideoEncoder.h"
#include "MediaLog.h"

#include "X264Encoder.h"

#ifdef IOS
#include "VTBEncoder.h"
#endif

#ifdef ANDROID
#include "MediaCodecEncoder.h"
#endif

#ifndef ANDROID_X86
//#include "VP8Encoder.h"
#endif

int VideoEncoder::CalculateEncodeBitrateWithFormula(VIDEO_ENCODER_TYPE type, ENCODE_SCENE scene, int width, int height, int fps)
{
    float weight = 1.0f;
    if (scene == kRTCScene) {
        weight = 1.0f;
    }else if(scene == kLiveScene) {
        weight = 1.5f;
    }
    return weight * 400 * pow(fps * 1.0 / 15, 0.6) * pow(width * height * 1.0 / 640 / 360, 0.75);
}

int VideoEncoder::CalculateEncodeBitrate(VIDEO_ENCODER_TYPE type, ENCODE_SCENE scene, int width, int height, int fps, int usersettingBitrate)
{
    int formulaBitrate = VideoEncoder::CalculateEncodeBitrateWithFormula(type, scene, width, height, fps);
    
    if (usersettingBitrate <=0) {
        return formulaBitrate;
    }
    
    int upboundBitrate = formulaBitrate * 2;
    int lowboundBitrate = formulaBitrate / 2;
    
    if (usersettingBitrate < lowboundBitrate || usersettingBitrate > upboundBitrate) {
        return formulaBitrate;
    }else return usersettingBitrate;
}

VideoEncoder *VideoEncoder::CreateVideoEncoder(VIDEO_ENCODER_TYPE type, int fps, int width, int height, int videoRawType, VIDEO_ENCODE_PROFILE profile, VIDEO_ENCODE_MODE encodeMode, int bitrate, int quality, int maxKeyFrameIntervalMs, VIDEO_ENCODE_PRESET preset, int vbvBufferSize, bool enableStrictCBR, int deblockingFilterFactor)
{
    if (type==X264) {
        return new X264Encoder(fps, width, height, profile, encodeMode, bitrate, quality, maxKeyFrameIntervalMs, preset, vbvBufferSize, enableStrictCBR, deblockingFilterFactor);
    }
    
#ifdef ANDROID
    if(type==MEDIACODEC) {
        return new MediaCodecEncoder(fps, width, height, profile, encodeMode, bitrate, maxKeyFrameIntervalMs);
    }
#endif
    
#ifdef IOS
    if (type==VIDEO_TOOL_BOX) {
        return new VTBEncoder(fps, width, height, videoRawType, profile, bitrate, maxKeyFrameIntervalMs);
    }
#endif
    
#ifndef ANDROID_X86
    if (type==VP8) {
//        return new VP8Encoder(fps, width, height, bitrate, maxKeyFrameIntervalMs);
    }
#endif
    return NULL;
}
void VideoEncoder::DeleteVideoEncoder(VideoEncoder *videoEncoder, VIDEO_ENCODER_TYPE type)
{
    if (type==X264) {
        X264Encoder *x264Encoder = (X264Encoder *)videoEncoder;
        delete x264Encoder;
        x264Encoder = NULL;
    }
    
#ifdef ANDROID
    if(type==MEDIACODEC) {
        MediaCodecEncoder *mediaCodecEncoder = (MediaCodecEncoder *)videoEncoder;
        delete mediaCodecEncoder;
        mediaCodecEncoder = NULL;
    }
#endif
    
#ifdef IOS
    if (type==VIDEO_TOOL_BOX) {
        VTBEncoder *vtbEncoder = (VTBEncoder*)videoEncoder;
        delete vtbEncoder;
        vtbEncoder = NULL;
    }
#endif
    
#ifndef ANDROID_X86
    if (type==VP8) {
//        VP8Encoder* vp8Encoder = (VP8Encoder*)videoEncoder;
//        delete vp8Encoder;
//        vp8Encoder = NULL;
    }
#endif
}
