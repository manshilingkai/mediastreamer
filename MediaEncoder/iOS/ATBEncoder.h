//
//  ATBEncoder.h
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef ATBEncoder_h
#define ATBEncoder_h

#include <stdio.h>
#include <AudioToolbox/AudioToolbox.h>

#include "AudioEncoder.h"

struct ATBUserData {
    uint8_t* data;
    int size;
    int sampleSize;
} ;

class ATBEncoder : public AudioEncoder
{
public:
    ATBEncoder(unsigned int sampleRate, unsigned int numChannels, unsigned int bitRate);
    ~ATBEncoder();
    
    bool Open();
    void Close();
    
    bool Encode(AudioFrame &audioFrame, AudioPacket &audioPacket);
    
    void SetOutputHandler(MediaMuxer *mediaMuxer);
    bool Encode(AudioFrame &audioFrame);
    
    AudioPacket* GetHeaders();
    
    void setBitrate(int bitrate);
    
    int GetBitRate();
    
    int GetFixedInputFrameSize();
    
private:
    static OSStatus ioProc(AudioConverterRef audioConverter, UInt32 *ioNumDataPackets, AudioBufferList* ioData, AudioStreamPacketDescription** ioPacketDesc, void* inUserData );
    void makeAsc(uint8_t sampleRateIndex, uint8_t channelCount);
private:
    int m_sampleRate;
    int m_numChannels;
    int m_bitrate;
    
    AudioStreamBasicDescription m_in, m_out;
    AudioConverterRef       m_audioConverter;
    
    size_t                  m_bytesPerSample;
    uint32_t                m_outputPacketMaxSize;

    uint8_t m_asc[2];
    
    AudioPacket header;
    
    ATBUserData mUserData;
    
    uint8_t* m_outputBuffer;
    
    MediaMuxer *mMediaMuxer;
    bool bOutPutHeader;
};

#endif /* ATBEncoder_h */
