//
//  VTBEncoder.h
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef VTBEncoder_h
#define VTBEncoder_h

#include <stdio.h>
#include <VideoToolbox/VideoToolbox.h>
#include <CoreFoundation/CoreFoundation.h>
#include "VideoEncoder.h"

class VTBEncoder : public VideoEncoder
{
public:
    VTBEncoder(int fps, int width, int height, int videoRawType, VIDEO_ENCODE_PROFILE profile, int bitrate, int maxKeyFrameIntervalMs);
    ~VTBEncoder();
    
    bool Open();
    void Close();
    
    int Encode(VideoFrame &videoFrame, VideoPacket &videoPacket);
    
    void SetOutputHandler(MediaMuxer *mediaMuxer);
    int Encode(VideoFrame &videoFrame);
    int Encode(void *cvPixelBufferRef, uint64_t pts);
    
    void SetEncodeMode(VIDEO_ENCODE_MODE encodeMode);
    VIDEO_ENCODE_MODE GetEncodeMode();
    
    void SetBitRate(int bitrate);
    int GetBitRate();
    
    //[-5 5] default 0
    void SetQuality(int quality);
    int GetQuality();
    
    void SetMaxKeyFrameInterval(int maxKeyFrameIntervalMs);
    int GetMaxKeyFrameInterval();
    
    void SetEncoderPreset(VIDEO_ENCODE_PRESET preset);
    VIDEO_ENCODE_PRESET GetEncoderPreset();
    
    void SetVBVBufferSize(int vbvBufferSize);
    int GetVBVBufferSize();
    
    void EnableStrictCBR(bool enable);
    
    //[-6, 6] -6 light filter, 6 strong, 0 default
    void SetDeblockingFilterFactor(int factor);
    int GetDeblockingFilterFactor();
    
    bool ReconfigEncodeParam();
    
    VideoPacket * GetHeaders(); //SPS and PPS
    
    void RequestKeyframe();
    int GetBufferedFrames();

private:
    bool CreateCompressionSession();
    void ConfigureCompressionSession();
    void DestroyCompressionSession();
    
    static void vtbCallback(void *outputCallbackRefCon,
                           void *sourceFrameRefCon,
                           OSStatus status,
                           VTEncodeInfoFlags infoFlags,
                           CMSampleBufferRef sampleBuffer);
    
    void compressionSessionOutput(CMSampleBufferRef sampleBuffer);

private:
    VTCompressionSessionRef mCompressionSession;
    
    int mFps;
    int mEncodeWidth;
    int mEncodeHeight;
    int mVideoRawType;
    VIDEO_ENCODE_PROFILE mProfile;
    
    int mBitrate;
    int mMaxKeyFrameIntervalMs;
    
    bool mForceKeyframe;

    MediaMuxer *mMediaMuxer;

    bool bOutPutHeader;
    VideoPacket HeaderPacket;
    int nalu_header_size;
    
    VideoPacket videoPacket;
    
    bool isDoReconfig;

};


#endif /* VTBEncoder_h */
