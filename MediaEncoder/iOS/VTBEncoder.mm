//
//  VTBEncoder.cpp
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "VTBEncoder.h"
#include "MediaLog.h"
#include "iOSUtils.h"

VTBEncoder::VTBEncoder(int fps, int width, int height, int videoRawType, VIDEO_ENCODE_PROFILE profile, int bitrate, int maxKeyFrameIntervalMs)
{
    mCompressionSession = NULL;

    mFps = fps;
    mEncodeWidth = width;
    mEncodeHeight = height;
    mVideoRawType = videoRawType;
    mProfile = profile;
    
    mBitrate = bitrate;
    mMaxKeyFrameIntervalMs = maxKeyFrameIntervalMs;
    
    mForceKeyframe = false;
    
    mMediaMuxer = NULL;
    bOutPutHeader = true;
    
    nalu_header_size = 0;
}

VTBEncoder::~VTBEncoder()
{
    DestroyCompressionSession();
    
    HeaderPacket.Free();
    videoPacket.Clear();
}

bool VTBEncoder::Open()
{
    return CreateCompressionSession();
}

void VTBEncoder::Close()
{
    DestroyCompressionSession();
}

void VTBEncoder::SetEncodeMode(VIDEO_ENCODE_MODE encodeMode)
{

}

VIDEO_ENCODE_MODE VTBEncoder::GetEncodeMode()
{
    return CBR;
}


//[-5 5] default 0
void VTBEncoder::SetQuality(int quality)
{

}

int VTBEncoder::GetQuality()
{
    return 0;
}

void VTBEncoder::SetMaxKeyFrameInterval(int maxKeyFrameIntervalMs)
{
    if (mMaxKeyFrameIntervalMs == maxKeyFrameIntervalMs) return;
    
    mMaxKeyFrameIntervalMs = maxKeyFrameIntervalMs;
    
    OSStatus err;
    if (mCompressionSession!=NULL) {
        const int32_t v = mFps * (mMaxKeyFrameIntervalMs/1000); // 2-second kfi
        
        CFNumberRef ref = CFNumberCreate(NULL, kCFNumberSInt32Type, &v);
        err = VTSessionSetProperty(mCompressionSession, kVTCompressionPropertyKey_MaxKeyFrameInterval, ref);
        CFRelease(ref);
    }
}

int VTBEncoder::GetMaxKeyFrameInterval()
{
    return mMaxKeyFrameIntervalMs;
}

void VTBEncoder::SetEncoderPreset(VIDEO_ENCODE_PRESET preset)
{
}

VIDEO_ENCODE_PRESET VTBEncoder::GetEncoderPreset()
{
    return ULTRAFAST;
}

void VTBEncoder::SetVBVBufferSize(int vbvBufferSize)
{

}

int VTBEncoder::GetVBVBufferSize()
{
    return 0;
}

void VTBEncoder::EnableStrictCBR(bool enable)
{

}

//[-6, 6] -6 light filter, 6 strong, 0 default
void VTBEncoder::SetDeblockingFilterFactor(int factor)
{

}

int VTBEncoder::GetDeblockingFilterFactor()
{
    return 0;
}

bool VTBEncoder::ReconfigEncodeParam()
{
    return true;
}


VideoPacket * VTBEncoder::GetHeaders()
{
    return &HeaderPacket;
}

void VTBEncoder::RequestKeyframe()
{
    mForceKeyframe = true;
}

int VTBEncoder::GetBufferedFrames()
{
    return 0;
}

void VTBEncoder::vtbCallback(void *outputCallbackRefCon,
                        void *sourceFrameRefCon,
                        OSStatus status,
                        VTEncodeInfoFlags infoFlags,
                        CMSampleBufferRef sampleBuffer )
{
    ((VTBEncoder*)outputCallbackRefCon)->compressionSessionOutput(sampleBuffer);
}

void VTBEncoder::compressionSessionOutput(CMSampleBufferRef sampleBuffer)
{
    CFArrayRef attachments = CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, false);
    
    bool isKeyframe = false;
    if(attachments != NULL) {
        CFDictionaryRef attachment;
        CFBooleanRef dependsOnOthers;
        attachment = (CFDictionaryRef)CFArrayGetValueAtIndex(attachments, 0);
        dependsOnOthers = (CFBooleanRef)CFDictionaryGetValue(attachment, kCMSampleAttachmentKey_DependsOnOthers);
        isKeyframe = (dependsOnOthers == kCFBooleanFalse);
    }
    
    if (bOutPutHeader) {
        if (!isKeyframe) return;
        
        // Send the SPS and PPS.
        CMFormatDescriptionRef format = CMSampleBufferGetFormatDescription(sampleBuffer);
        size_t spsSize, ppsSize;
        size_t parmCount;
        const uint8_t* sps, *pps;
        
        CMVideoFormatDescriptionGetH264ParameterSetAtIndex(format, 0, nullptr, nullptr, &parmCount, &nalu_header_size);
        CMVideoFormatDescriptionGetH264ParameterSetAtIndex(format, 0, &sps, &spsSize, &parmCount, nullptr );
        CMVideoFormatDescriptionGetH264ParameterSetAtIndex(format, 1, &pps, &ppsSize, &parmCount, nullptr );
        
        HeaderPacket.Free();

        HeaderPacket.nal_Num = 2;
        Nal *pNal = new Nal;
        pNal->size = MAX_H264_HEADER_SIZE;
        pNal->data = (uint8_t*)malloc(pNal->size);
        HeaderPacket.nals.push_back(pNal);
        
        pNal = new Nal;
        pNal->size = MAX_H264_HEADER_SIZE;
        pNal->data = (uint8_t*)malloc(pNal->size);
        HeaderPacket.nals.push_back(pNal);
        
        memcpy(HeaderPacket.nals[0]->data, kAnnexBHeaderBytes, sizeof(kAnnexBHeaderBytes));
        memcpy(HeaderPacket.nals[0]->data+sizeof(kAnnexBHeaderBytes), sps, spsSize);
        HeaderPacket.nals[0]->size = (int)spsSize+sizeof(kAnnexBHeaderBytes);
        
        memcpy(HeaderPacket.nals[1]->data, kAnnexBHeaderBytes, sizeof(kAnnexBHeaderBytes));
        memcpy(HeaderPacket.nals[1]->data+sizeof(kAnnexBHeaderBytes), pps, ppsSize);
        HeaderPacket.nals[1]->size = (int)ppsSize+sizeof(kAnnexBHeaderBytes);
        
        HeaderPacket.type = H264_TYPE_SPS_PPS;
        
        //Push H264 Headers to MediaMuxer
        LOGD("Push H264 Header(SPS and PPS) to MediaMuxer");
        if (mMediaMuxer!=NULL) {
            mMediaMuxer->pushH264Header(&HeaderPacket);
        }
        
        bOutPutHeader = false;
    }
    
    CMBlockBufferRef block = CMSampleBufferGetDataBuffer(sampleBuffer);
    CMTime pts = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
    CMTime dts = CMSampleBufferGetDecodeTimeStamp(sampleBuffer);
    
    uint8_t* bufferData = NULL;
    size_t size = 0;
    CMBlockBufferGetDataPointer(block, 0, NULL, &size, (char**)&bufferData);
    
//    for (int i=0; i<size; i++) {
//        printf("%x\n",bufferData[i]);
//    }
    
    size_t pos = 0;
    while (pos < size) {
        size_t sliceSize = bufferData[pos] * 256 * 256 * 256 + bufferData[pos+1] * 256 * 256 + bufferData[pos+2] * 256 + bufferData[pos+3];
        
        memcpy(bufferData+pos, (uint8_t *)kAnnexBHeaderBytes, sizeof(kAnnexBHeaderBytes));
        
        Nal *pNal = new Nal;
        pNal->data = (uint8_t*)bufferData+pos;
        pNal->size = sliceSize + 4;
        videoPacket.nals.push_back(pNal);
        videoPacket.nal_Num++;
        
        pos = pos + 4 + sliceSize;
    }
    
    if (mProfile==BASE_LINE)
    {
        videoPacket.pts = pts.value;
        videoPacket.dts = pts.value;
    }else{
        videoPacket.pts = pts.value;
        videoPacket.dts = dts.value;
    }
    
    if (videoPacket.dts==0) {
        videoPacket.dts = videoPacket.pts;
    }
    
    if (isKeyframe) {
        videoPacket.type = H264_TYPE_I;
    }else {
        videoPacket.type = H264_TYPE_P;
    }
    
    if (mMediaMuxer!=NULL) {
        mMediaMuxer->pushH264Body(&videoPacket);
    }
    
    videoPacket.Clear();
    
    // for debug
    if (sizeof(kAnnexBHeaderBytes)!=nalu_header_size){
        LOGE("nalu header size != 4");
    }
}


int VTBEncoder::Encode(VideoFrame &videoFrame, VideoPacket &videoPacket)
{
    return this->Encode(videoFrame);
}

void VTBEncoder::SetOutputHandler(MediaMuxer *mediaMuxer)
{
    mMediaMuxer = mediaMuxer;
}

int VTBEncoder::Encode(void *cvPixelBufferRef, uint64_t pts)
{
    if (mCompressionSession==NULL) return -1;
    
    CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)cvPixelBufferRef;
    
    VTEncodeInfoFlags flags;
    CFMutableDictionaryRef frameProps = NULL;
    
    if(mForceKeyframe) {
        frameProps = CFDictionaryCreateMutable(kCFAllocatorDefault, 1,&kCFTypeDictionaryKeyCallBacks,                                                            &kCFTypeDictionaryValueCallBacks);
        
        CFDictionaryAddValue(frameProps, kVTEncodeFrameOptionKey_ForceKeyFrame, kCFBooleanTrue);
    }
    
    CMTime cmTimePts = CMTimeMake(pts, 1000); // timestamp is in ms.
    
    OSStatus encodeStatus = VTCompressionSessionEncodeFrame(mCompressionSession, pixelBuffer, cmTimePts, kCMTimeInvalid, frameProps, NULL, &flags);
    
    if(mForceKeyframe) {
        CFRelease(frameProps);
        mForceKeyframe = false;
    }
    
    if (encodeStatus!=noErr) {
        return encodeStatus;
    }
    
    return noErr;
}


int VTBEncoder::Encode(VideoFrame &videoFrame)
{
    if (mCompressionSession==NULL) return -1;
    
    if (videoFrame.videoRawType==VIDEOFRAME_RAWTYPE_BGRA_IOS_CVPIXELBUFFERREF) {
        return this->Encode(videoFrame.opaque, videoFrame.pts);
    }
    
    if (videoFrame.width!=mEncodeWidth || videoFrame.height!=mEncodeHeight) {
        return -1;
    }
    
    CVPixelBufferRef pixelBuffer = NULL;
    SInt32 cvPixelFormatTypeValue = kCVPixelFormatType_32BGRA;
    size_t bytesPerRow = mEncodeWidth * 4;
    if (mVideoRawType==VIDEOFRAME_RAWTYPE_NV12) {
        cvPixelFormatTypeValue = kCVPixelFormatType_420YpCbCr8BiPlanarFullRange;
        bytesPerRow = mEncodeWidth * 3/2;
    }
    CVPixelBufferCreateWithBytes(kCFAllocatorDefault, mEncodeWidth, mEncodeHeight, cvPixelFormatTypeValue, videoFrame.data, bytesPerRow, nil, nil, nil, &pixelBuffer);
    
    VTEncodeInfoFlags flags;
    CFMutableDictionaryRef frameProps = NULL;
    
    if(mForceKeyframe) {
        frameProps = CFDictionaryCreateMutable(kCFAllocatorDefault, 1,&kCFTypeDictionaryKeyCallBacks,                                                            &kCFTypeDictionaryValueCallBacks);
        
        CFDictionaryAddValue(frameProps, kVTEncodeFrameOptionKey_ForceKeyFrame, kCFBooleanTrue);
    }
    
    CMTime pts = CMTimeMake(videoFrame.pts, 1000); // timestamp is in ms.
    
    OSStatus encodeStatus = VTCompressionSessionEncodeFrame(mCompressionSession, pixelBuffer, pts, kCMTimeInvalid, frameProps, NULL, &flags);
    
    if(mForceKeyframe) {
        CFRelease(frameProps);
        mForceKeyframe = false;
    }
    
    if (pixelBuffer) {
        CVPixelBufferRelease(pixelBuffer);
    }
    
    if (encodeStatus!=noErr) {
        LOGE("VTCompressionSessionEncodeFrame Error Code : %d", encodeStatus);
        return encodeStatus;
    }
    
    return noErr;
}

bool VTBEncoder::CreateCompressionSession()
{
    CFMutableDictionaryRef encoderSpecifications = nullptr;
    
    SInt32 cvPixelFormatTypeValue = kCVPixelFormatType_32BGRA;
    if (mVideoRawType==VIDEOFRAME_RAWTYPE_NV12) {
        cvPixelFormatTypeValue = kCVPixelFormatType_420YpCbCr8BiPlanarFullRange;
    }
    SInt8  boolYESValue = 0xFF;
    
    CFDictionaryRef emptyDict = CFDictionaryCreate(kCFAllocatorDefault, nil, nil, 0, nil, nil);
    CFNumberRef cvPixelFormatType = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, (const void*)(&(cvPixelFormatTypeValue)));
    CFNumberRef encodeWdith = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, (const void*)(&(mEncodeWidth)));
    CFNumberRef encodeHeight = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, (const void*)(&(mEncodeHeight)));
    CFNumberRef boolYES = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt8Type, (const void*)(&(boolYESValue)));
    
    const void *pixelBufferOptionsDictKeys[] = { kCVPixelBufferPixelFormatTypeKey, kCVPixelBufferWidthKey,  kCVPixelBufferHeightKey, kCVPixelBufferOpenGLESCompatibilityKey, kCVPixelBufferIOSurfacePropertiesKey };
    const void *pixelBufferOptionsDictValues[] = { cvPixelFormatType,  encodeWdith, encodeHeight, boolYES, emptyDict};
    CFDictionaryRef pixelBufferOptions = CFDictionaryCreate(kCFAllocatorDefault, pixelBufferOptionsDictKeys, pixelBufferOptionsDictValues, 5, nil, nil);
    
    OSStatus err = VTCompressionSessionCreate(
                                     kCFAllocatorDefault,
                                     mEncodeWidth,
                                     mEncodeHeight,
                                     kCMVideoCodecType_H264,
                                     encoderSpecifications,
                                     pixelBufferOptions,
                                     NULL,
                                     &vtbCallback,
                                     this,
                                     &mCompressionSession);
    
    CFRelease(emptyDict);
    CFRelease(cvPixelFormatType);
    CFRelease(encodeWdith);
    CFRelease(encodeHeight);
    CFRelease(boolYES);
    CFRelease(pixelBufferOptions);
    
    if(err != noErr)
    {
        return false;
    }
    
    ConfigureCompressionSession();
    
    return true;
}

void VTBEncoder::ConfigureCompressionSession()
{
    OSStatus err = noErr;
    
    if (mCompressionSession!=NULL) {
        if(err == noErr) {
            const int32_t v = mFps * (mMaxKeyFrameIntervalMs/1000); // 2-second kfi
            
            CFNumberRef ref = CFNumberCreate(NULL, kCFNumberSInt32Type, &v);
            err = VTSessionSetProperty(mCompressionSession, kVTCompressionPropertyKey_MaxKeyFrameInterval, ref);
            CFRelease(ref);
        }
        
        if(err == noErr) {
            const int v = mFps;
            CFNumberRef ref = CFNumberCreate(NULL, kCFNumberSInt32Type, &v);
            err = VTSessionSetProperty(mCompressionSession, kVTCompressionPropertyKey_ExpectedFrameRate, ref);
            CFRelease(ref);
        }

        // if [SVC], Check VTSessionCopySupportedPropertyDictionary()
        // if [SVC], set [kVTCompressionPropertyKey_ExpectedFrameRate = mFps] && [kVTCompressionPropertyKey_BaseLayerFrameRate = mFps/4]
        
        if(err == noErr) {
            if (mProfile==BASE_LINE) {
                err = VTSessionSetProperty(mCompressionSession , kVTCompressionPropertyKey_AllowFrameReordering, kCFBooleanFalse);
            }else {
                err = VTSessionSetProperty(mCompressionSession , kVTCompressionPropertyKey_AllowFrameReordering, kCFBooleanTrue);
            }
        }
        
        if(err == noErr) {
            const int v = mBitrate*1024;
            CFNumberRef ref = CFNumberCreate(NULL, kCFNumberSInt32Type, &v);
            err = VTSessionSetProperty(mCompressionSession, kVTCompressionPropertyKey_AverageBitRate, ref);
            CFRelease(ref);
        }

        if(err == noErr) {
            if (iOSUtils::getIosPlatform()==iPhone7 || iOSUtils::getIosPlatform()==iPhone7Plus || iOSUtils::getIosPlatform()==iPhoneX) {
                //ignore
            }else{
//                err = VTSessionSetProperty(mCompressionSession, kVTCompressionPropertyKey_RealTime, kCFBooleanTrue);
            }
        }

        if(err == noErr) {
            if (mProfile==BASE_LINE) {
                err = VTSessionSetProperty(mCompressionSession, kVTCompressionPropertyKey_ProfileLevel, kVTProfileLevel_H264_Baseline_AutoLevel);
            }else if(mProfile==MAIN_PROFILE) {
                err = VTSessionSetProperty(mCompressionSession, kVTCompressionPropertyKey_ProfileLevel, kVTProfileLevel_H264_Main_AutoLevel);
            }else if (mProfile==HIGH_PROFILE) {
                err = VTSessionSetProperty(mCompressionSession, kVTCompressionPropertyKey_ProfileLevel, kVTProfileLevel_H264_High_AutoLevel);
            }

        }

        if(err == noErr) {
            if (mProfile==MAIN_PROFILE || mProfile==HIGH_PROFILE)
            {
                err = VTSessionSetProperty(mCompressionSession, kVTCompressionPropertyKey_H264EntropyMode, kVTH264EntropyMode_CABAC);
            }
        }

        if(err == noErr) {
            err = VTCompressionSessionPrepareToEncodeFrames(mCompressionSession);
        }
    }
}

void VTBEncoder::DestroyCompressionSession()
{
    if(mCompressionSession!=NULL) {
        VTCompressionSessionInvalidate((VTCompressionSessionRef)mCompressionSession);
        CFRelease((VTCompressionSessionRef)mCompressionSession);
        mCompressionSession = NULL;
    }
}

void VTBEncoder::SetBitRate(int bitrate)
{
    if (mBitrate == bitrate) return;
    
    mBitrate = bitrate;
    
    if (mCompressionSession==NULL) return;

    int v = mBitrate*1024 ;
    CFNumberRef ref = CFNumberCreate(NULL, kCFNumberSInt32Type, &v);
    OSStatus ret = VTSessionSetProperty(mCompressionSession, kVTCompressionPropertyKey_AverageBitRate, ref);
    if(ret != noErr) {
        LOGD("VTBEncoder::SetBitRate : Error setting bitrate! %d", (int) ret);
    }
    CFRelease(ref);
    
    ret = VTSessionCopyProperty(mCompressionSession, kVTCompressionPropertyKey_AverageBitRate, kCFAllocatorDefault, &ref);
    
    if(ret == noErr && ref) {
        SInt32 br = 0;
        
        CFNumberGetValue(ref, kCFNumberSInt32Type, &br);
        
        mBitrate = br/1024;
        CFRelease(ref);
    } else {
        mBitrate = v/1024;
    }
    v = bitrate * 1024 / 8;
    CFNumberRef bytes = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &v);
    v = 1;
    CFNumberRef duration = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &v);
    
    CFMutableArrayRef limit = CFArrayCreateMutable(kCFAllocatorDefault, 2, &kCFTypeArrayCallBacks);
    
    CFArrayAppendValue(limit, bytes);
    CFArrayAppendValue(limit, duration);
    
    VTSessionSetProperty(mCompressionSession, kVTCompressionPropertyKey_DataRateLimits, limit);
    CFRelease(bytes);
    CFRelease(duration);
    CFRelease(limit);
}

int VTBEncoder::GetBitRate()
{
    return mBitrate;
}

