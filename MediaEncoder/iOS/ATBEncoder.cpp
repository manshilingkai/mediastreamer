//
//  ATBEncoder.cpp
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "ATBEncoder.h"
#include "MediaLog.h"

static const int kSamplesPerFrame = 1024;

ATBEncoder::ATBEncoder(unsigned int sampleRate, unsigned int numChannels, unsigned int bitRate)
    :m_sampleRate(sampleRate), m_numChannels(numChannels), m_bitrate(bitRate)

{
    m_outputBuffer = NULL;
    
    mMediaMuxer = NULL;
    bOutPutHeader = true;
}

ATBEncoder::~ATBEncoder()
{

}

bool ATBEncoder::Open()
{
    OSStatus result = 0;
    
    m_in = {0};
    m_out = {0};
    
    // passing anything except 48000, 44100, and 22050 for mSampleRate results in "!dat"
    // OSStatus when querying for kAudioConverterPropertyMaximumOutputPacketSize property
    // below
    m_in.mSampleRate = m_sampleRate;
    // passing anything except 2 for mChannelsPerFrame results in "!dat" OSStatus when
    // querying for kAudioConverterPropertyMaximumOutputPacketSize property below
    m_in.mChannelsPerFrame = m_numChannels;
    m_in.mBitsPerChannel = 16;
    m_in.mFormatFlags =  kAudioFormatFlagIsSignedInteger | kAudioFormatFlagsNativeEndian | kAudioFormatFlagIsPacked;
    m_in.mFormatID = kAudioFormatLinearPCM;
    m_in.mFramesPerPacket = 1;
    m_in.mBytesPerFrame = m_in.mBitsPerChannel * m_in.mChannelsPerFrame / 8;
    m_in.mBytesPerPacket = m_in.mFramesPerPacket*m_in.mBytesPerFrame;
    
    m_out.mFormatID = kAudioFormatMPEG4AAC;
    m_out.mFormatFlags = 0;
    m_out.mFramesPerPacket = kSamplesPerFrame;
    m_out.mSampleRate = m_sampleRate;
    m_out.mChannelsPerFrame = m_numChannels;
    m_out.mFormatFlags = kMPEG4Object_AAC_LC;
    
    UInt32 outputBitrate = m_bitrate * 1024;
    UInt32 propSize = sizeof(outputBitrate);
    UInt32 maximumOutputPacketSize = 0;
    
    const OSType subtype = kAudioFormatMPEG4AAC;
    AudioClassDescription requestedCodecs[2] = {
        {
            kAudioEncoderComponentType,
            subtype,
            kAppleSoftwareAudioCodecManufacturer
        },
        {
            kAudioEncoderComponentType,
            subtype,
            kAppleHardwareAudioCodecManufacturer
        }
    };
    
    result = AudioConverterNewSpecific(&m_in, &m_out, 2, requestedCodecs, &m_audioConverter);
    
    if(result == noErr) {
        result = AudioConverterSetProperty(m_audioConverter, kAudioConverterEncodeBitRate, propSize, &outputBitrate);
    }
    if(result == noErr) {
        result = AudioConverterGetProperty(m_audioConverter, kAudioConverterPropertyMaximumOutputPacketSize, &propSize, &maximumOutputPacketSize);
    }
    
    if(result == noErr) {
        m_outputPacketMaxSize = maximumOutputPacketSize;
        
        m_bytesPerSample = 2 * m_numChannels;
        
        uint8_t sampleRateIndex = 0;
        switch(m_sampleRate) {
            case 96000:
                sampleRateIndex = 0;
                break;
            case 88200:
                sampleRateIndex = 1;
                break;
            case 64000:
                sampleRateIndex = 2;
                break;
            case 48000:
                sampleRateIndex = 3;
                break;
            case 44100:
                sampleRateIndex = 4;
                break;
            case 32000:
                sampleRateIndex = 5;
                break;
            case 24000:
                sampleRateIndex = 6;
                break;
            case 22050:
                sampleRateIndex = 7;
                break;
            case 16000:
                sampleRateIndex = 8;
                break;
            case 12000:
                sampleRateIndex = 9;
                break;
            case 11025:
                sampleRateIndex = 10;
                break;
            case 8000:
                sampleRateIndex = 11;
                break;
            case 7350:
                sampleRateIndex = 12;
                break;
            default:
                sampleRateIndex = 15;
        }
        makeAsc(sampleRateIndex, uint8_t(m_numChannels));
        
        m_outputBuffer = (uint8_t*)malloc(m_outputPacketMaxSize);
        
        return true;
        
    } else {
        LOGE("Error setting up audio encoder %x", (int)result);
        
        return false;
    }
}

void ATBEncoder::Close()
{
    AudioConverterDispose(m_audioConverter);
    
    if (m_outputBuffer!=NULL) {
        free(m_outputBuffer);
        m_outputBuffer = NULL;
    }
}

int ATBEncoder::GetFixedInputFrameSize()
{
    return kSamplesPerFrame * m_bytesPerSample;
}

OSStatus ATBEncoder::ioProc(AudioConverterRef audioConverter, UInt32 *ioNumDataPackets, AudioBufferList* ioData, AudioStreamPacketDescription** ioPacketDesc, void* inUserData )
{
    ATBUserData* ud = static_cast<ATBUserData*>(inUserData);
    
    UInt32 maxPackets = ud->size / ud->sampleSize;
    
    *ioNumDataPackets = std::min(maxPackets, *ioNumDataPackets);
    
    ioData->mBuffers[0].mData = ud->data;
    ioData->mBuffers[0].mDataByteSize = ud->size;
    ioData->mBuffers[0].mNumberChannels = 1; //1
    
    return noErr;
}

bool ATBEncoder::Encode(AudioFrame &audioFrame, AudioPacket &audioPacket)
{
    UInt32 num_packets = 1;
    AudioStreamPacketDescription output_packet_desc[num_packets];
    
    mUserData.data = audioFrame.data;
    mUserData.size = audioFrame.frameSize;
    mUserData.sampleSize = (int)m_bytesPerSample;
    
    AudioBufferList l;
    l.mNumberBuffers=1;
    l.mBuffers[0].mDataByteSize = m_outputPacketMaxSize * num_packets;
    l.mBuffers[0].mData = m_outputBuffer;

    AudioConverterFillComplexBuffer(m_audioConverter, ATBEncoder::ioProc, &mUserData, &num_packets, &l, output_packet_desc);
    
    audioPacket.data = m_outputBuffer;
    audioPacket.size = output_packet_desc[0].mDataByteSize;
    audioPacket.pts = audioFrame.pts;
    audioPacket.dts = audioFrame.pts;
    audioPacket.duration = audioFrame.duration;

    return true;
}

void ATBEncoder::SetOutputHandler(MediaMuxer *mediaMuxer)
{
    mMediaMuxer = mediaMuxer;
    
    if (bOutPutHeader) {
        
        LOGD("Push AAC Header to MediaMuxer");
        if (mMediaMuxer!=NULL) {
            mMediaMuxer->pushAACHeader(this->GetHeaders());
        }
        
        bOutPutHeader = false;
    }
}

bool ATBEncoder::Encode(AudioFrame &audioFrame)
{
    AudioPacket outAudioPacket;
    this->Encode(audioFrame, outAudioPacket);
    
    if (mMediaMuxer!=NULL) {
        mMediaMuxer->pushAACBody(&outAudioPacket);
    }

    return true;
}


AudioPacket* ATBEncoder::GetHeaders()
{
    header.data = m_asc;
    header.size = 2;
    
    return &header;
}

void ATBEncoder::setBitrate(int bitrate)
{
    m_bitrate = bitrate;
    
    Close();
    
    Open();
}

int ATBEncoder::GetBitRate()
{
    return m_bitrate;
}


void ATBEncoder::makeAsc(uint8_t sampleRateIndex, uint8_t channelCount)
{
    // http://wiki.multimedia.cx/index.php?title=MPEG-4_Audio#Audio_Specific_Config
    m_asc[0] = 0x10 | ((sampleRateIndex>>1) & 0x3);
    m_asc[1] = ((sampleRateIndex & 0x1)<<7) | ((channelCount & 0xF) << 3);
}

