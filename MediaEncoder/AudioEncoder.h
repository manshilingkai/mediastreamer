//
//  AudioEncoder.h
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaStreamer_AudioEncoder_h
#define MediaStreamer_AudioEncoder_h

#include "MediaDataType.h"
#include "MediaMuxer.h"

enum AUDIO_ENCODER_TYPE
{
    FAAC = 0,
    AUDIO_TOOL_BOX = 1, // for iOS
    MEDIA_CODEC = 2, // for android
    OPENCOREAMRNB = 3,
//    OPENCOREAMRWB = 4,
    FDK_AAC = 4,
};

class AudioEncoder
{
public:
    virtual ~AudioEncoder() {}

    static AudioEncoder* CreateAudioEncoder(AUDIO_ENCODER_TYPE type, unsigned int sampleRate, unsigned int numChannels, unsigned int bitRate);
    static void DeleteAudioEncoder(AudioEncoder* audioEncoder, AUDIO_ENCODER_TYPE type);

    virtual bool Open() = 0;
    virtual void Close() = 0;

    virtual bool Encode(AudioFrame &audioFrame, AudioPacket &audioPacket) = 0;

    //async encode
    virtual void SetOutputHandler(MediaMuxer *mediaMuxer) = 0;
    virtual bool Encode(AudioFrame &audioFrame) = 0;
    
    virtual AudioPacket* GetHeaders() = 0;

    virtual void setBitrate(int bitrate) = 0;

    virtual int GetBitRate() = 0;

    virtual int GetFixedInputFrameSize() = 0;
};

#endif
