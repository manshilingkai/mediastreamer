//
//  VideoEncoder.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaStreamer__VideoEncoder__
#define __MediaStreamer__VideoEncoder__

#include "MediaDataType.h"
#include "MediaMuxer.h"

#ifdef ANDROID
#include "jni.h"
#endif

#define MAX_H264_HEADER_SIZE 128 //sps and pps

enum VIDEO_ENCODE_PROFILE
{
    BASE_LINE = 0,
    MAIN_PROFILE = 1,
    HIGH_PROFILE = 2,
};

enum VIDEO_ENCODE_MODE
{
    VBR = 0,
    CBR = 1,
};

enum VIDEO_ENCODE_PRESET
{
    ULTRAFAST = 0,
    SUPERFAST = 1,
    VERYFAST = 2,
    FASTER = 3,
    FAST = 4,
    MEDIUM = 5,
    SLOW = 6,
    SLOWER = 7,
    VERYSLOW = 8
};

enum VIDEO_ENCODER_TYPE
{
    X264 = 0,
    VIDEO_TOOL_BOX = 1, // for iOS
    MEDIACODEC = 2, // for android
    VP8 = 3,
};

enum ENCODE_SCENE {
  kRTCScene = 0,
  kLiveScene = 1,
};

class VideoEncoder
{
public:
    virtual ~VideoEncoder() {}
    
    static int CalculateEncodeBitrateWithFormula(VIDEO_ENCODER_TYPE type, ENCODE_SCENE scene, int width, int height, int fps);//kbps
    static int CalculateEncodeBitrate(VIDEO_ENCODER_TYPE type, ENCODE_SCENE scene, int width, int height, int fps, int usersettingBitrate);//kbps
    
    static VideoEncoder *CreateVideoEncoder(VIDEO_ENCODER_TYPE type, int fps, int width, int height, int videoRawType, VIDEO_ENCODE_PROFILE profile, VIDEO_ENCODE_MODE encodeMode, int bitrate, int quality, int maxKeyFrameIntervalMs, VIDEO_ENCODE_PRESET preset, int vbvBufferSize, bool enableStrictCBR, int deblockingFilterFactor);
    static void DeleteVideoEncoder(VideoEncoder *videoEncoder, VIDEO_ENCODER_TYPE type);
    
#ifdef ANDROID
    virtual void registerJavaVMEnv(JavaVM *jvm) = 0;
    virtual void setEncodeSurface(void *surface) = 0;
#endif
    
    virtual bool Open() = 0;
    virtual void Close() = 0;
    
    //sync encode
    virtual int Encode(VideoFrame &videoFrame, VideoPacket &videoPacket) = 0; //X264
    
    //async encode
    virtual void SetOutputHandler(MediaMuxer *mediaMuxer) = 0;
    virtual int Encode(VideoFrame &videoFrame) = 0; // X264+VTB
#ifdef IOS
    virtual int Encode(void *cvPixelBufferRef, uint64_t pts) = 0; //VTB
#endif
#ifdef ANDROID
    virtual bool Encode() = 0; //EncodeSurface
#endif
    
    virtual void SetEncodeMode(VIDEO_ENCODE_MODE encodeMode) = 0;
    virtual VIDEO_ENCODE_MODE GetEncodeMode() = 0;
    
    virtual void SetBitRate(int bitrate) = 0; //kbit
    virtual int GetBitRate() = 0;

    //[-5 5] default 0
    virtual void SetQuality(int quality) = 0;
    virtual int GetQuality() = 0;
    
    virtual void SetMaxKeyFrameInterval(int maxKeyFrameIntervalMs) = 0;
    virtual int GetMaxKeyFrameInterval() = 0;
    
    virtual void SetEncoderPreset(VIDEO_ENCODE_PRESET preset) = 0;
    virtual VIDEO_ENCODE_PRESET GetEncoderPreset() = 0;
    
    virtual void SetVBVBufferSize(int vbvBufferSize) = 0;
    virtual int GetVBVBufferSize() = 0;
    
    virtual void EnableStrictCBR(bool enable) = 0;
    
    //[-6, 6] -6 light filter, 6 strong, 0 default
    virtual void SetDeblockingFilterFactor(int factor) = 0;
    virtual int GetDeblockingFilterFactor() = 0;
    
    virtual bool ReconfigEncodeParam() = 0;
    
    virtual VideoPacket * GetHeaders() = 0; //SPS and PPS
    
    virtual void RequestKeyframe() = 0;
    virtual int GetBufferedFrames() = 0;
};

#endif /* defined(__MediaStreamer__VideoEncoder__) */
