//
//  OpenCoreAMRNBEncoder.h
//  MediaStreamer
//
//  Created by Think on 2017/1/17.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef OpenCoreAMRNBEncoder_h
#define OpenCoreAMRNBEncoder_h

#include <stdio.h>
#include <stdint.h>

#include "opencore-amrnb/interf_enc.h"
#include "AudioEncoder.h"

class OpenCoreAMRNBEncoder : public AudioEncoder {
    
public:
    OpenCoreAMRNBEncoder(unsigned int sampleRate, unsigned int numChannels, unsigned int bitRate);
    ~OpenCoreAMRNBEncoder();
    
    bool Open();
    void Close();
    
    bool Encode(AudioFrame &audioFrame, AudioPacket &audioPacket);
    
    void SetOutputHandler(MediaMuxer *mediaMuxer);
    bool Encode(AudioFrame &audioFrame);
    
    AudioPacket* GetHeaders();
    
    void setBitrate(int bitrate);
    
    int GetBitRate();
    
    int GetFixedInputFrameSize();

private:
    enum Mode enc_mode;
    
    void* opencore_amrnb_enc_handle;
    
    uint8_t amrNBFileHeader[6];
    AudioPacket header;

    uint8_t *amrNBBuffer;
    
    MediaMuxer *mMediaMuxer;
    bool bOutPutHeader;
};

#endif /* OpenCoreAMRNBEncoder_h */
