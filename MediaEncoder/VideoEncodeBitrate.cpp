//
//  VideoEncodeBitrate.cpp
//  MediaStreamer
//
//  Created by slklovewyy on 2022/6/10.
//  Copyright © 2022 Cell. All rights reserved.
//

#include "VideoEncodeBitrate.h"
#include <algorithm>
#include <math.h>

int VideoEncodeBitrate::CalculateVideoEncodeBitrateKbpsWithFormula(int width, int height, int fps, ChannelProfile channelProfile)
{
#if 0
    float scalingFactor = 0.0f;
    if (channelProfile == kRTCMode) {
        scalingFactor = 1.0f;
    }else if (channelProfile == kLiveMode) {
        scalingFactor = 1.5f;
    }else if (channelProfile == kVODMode) {
        scalingFactor = 2.0f;
    }
    
    int video_encode_bitrate = scalingFactor * 400 * pow(fps * 1.0 / 15, 0.6) * pow(width * height * 1.0 / 640 / 360, 0.75);
    return video_encode_bitrate;
#else
    if (channelProfile == kRTCMode) {
        return 400 * pow(fps * 1.0 / 15, 0.6) * pow(width * height * 1.0 / 640 / 360, 0.75);
    }else if (channelProfile == kLiveMode) {
        return 2000 * pow(fps * 1.0 / 15, 0.6) * pow(width * height * 1.0 / 1280 / 720, 0.65);
    }else if (channelProfile == kVODMode) {
        return 1.5 * 2000 * pow(fps * 1.0 / 15, 0.6) * pow(width * height * 1.0 / 1280 / 720, 0.65);
    }
#endif
}

int VideoEncodeBitrate::CalculateVideoEncodeBitrateKbpsWithFormulaForH265(int width, int height, int fps, ChannelProfile channelProfile)
{
    static float h265BitrateSaveRateWithFormula = 0.04f * pow(fps * 1.0 / 15, 0.4) * pow(width * height * 1.0 / 640 / 360, 0.5);
    return CalculateVideoEncodeBitrateKbpsWithFormula(width, height, fps, channelProfile) * (1.0f - h265BitrateSaveRateWithFormula);
}