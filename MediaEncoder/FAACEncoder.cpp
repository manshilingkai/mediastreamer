//
//  FAACEncoder.cpp
//  MediaStreamer
//
//  Created by Think on 16/3/6.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "FAACEncoder.h"
#include "MediaLog.h"

FAACEncoder::FAACEncoder(unsigned int sampleRate, unsigned int numChannels, unsigned int bitRate)
{
    curSampleRate = sampleRate;
    curNumChannels = numChannels;
    curBitRate = bitRate;
    
    mMediaMuxer = NULL;
    bOutPutHeader = true;

}

FAACEncoder::~FAACEncoder()
{

}

bool FAACEncoder::Open()
{
    // open aac handle
    faac = faacEncOpen(curSampleRate, curNumChannels, &inputSamples, &maxOutputBytes);
    
    if (!faac) {
        LOGE("error in faacEncOpen()\n");
        return false;
    }
    
    // check faac version
    faacEncConfigurationPtr config = faacEncGetCurrentConfiguration(faac);
    if (config->version != FAAC_CFG_VERSION) {
        LOGE("wrong libfaac version (compiled for: %d, using %d)\n", FAAC_CFG_VERSION, config->version);
        return false;
    }
    
    // put the options in the configuration struct
    config->aacObjectType = LOW;
    config->mpegVersion = MPEG4;
    config->useTns = 1;
    config->allowMidside = 1;
    config->bitRate = curBitRate*1024/curNumChannels;
    //    config->bandWidth = 0;
    //    config->quantqual = 100;
    config->outputFormat = 1;
    config->inputFormat = FAAC_INPUT_16BIT;
    
    // Set decoder specific info
    unsigned char *buffer = NULL;
    unsigned long decoder_specific_info_size=0;
    if (!faacEncGetDecoderSpecificInfo(faac, &buffer,
                                       &decoder_specific_info_size))
    {
        header.data = (uint8_t*)malloc(decoder_specific_info_size);
        header.size = (int)decoder_specific_info_size;
        memcpy(header.data, buffer, decoder_specific_info_size);
        
        config->outputFormat = 0;
        
        if (buffer!=NULL) {
            free(buffer);
        }
    }
    
    int ret = faacEncSetConfiguration(faac, config);
    if(!ret)
    {
        int i;
        for (i = curBitRate; i ; i--) {
            config->bitRate = i*1024 / curNumChannels;
            if (faacEncSetConfiguration(faac, config))
                break;
        }
        if (!i) {
            LOGE("libfaac doesn't support this output format!\n");
            return false;
        } else {
            curBitRate = i;
            LOGW("libfaac doesn't support the specified bitrate, using %dkbit/s instead\n", i);
        }
    }
    
    aacBuffer = (uint8_t*)malloc(maxOutputBytes);
    
    return true;
}

void FAACEncoder::Close()
{
    faacEncClose(faac);
    
    free(aacBuffer);
    free(header.data);
}

bool FAACEncoder::Encode(AudioFrame &audioFrame, AudioPacket &audioPacket)
{
    int bytes_written = 0;
    memset(&audioPacket, 0, sizeof(AudioPacket));
    bytes_written = faacEncEncode(faac, (int32_t *)audioFrame.data, inputSamples, aacBuffer, maxOutputBytes);
    
    if (bytes_written < 0) {
        LOGE("faacEncEncode() error\n");
        return false;
    }
    
    audioPacket.data = aacBuffer;
    audioPacket.size = bytes_written;
    audioPacket.pts = audioFrame.pts;
    audioPacket.dts = audioFrame.pts;
    
    audioPacket.duration = audioFrame.duration;
    
    return true;
}

//async encode
void FAACEncoder::SetOutputHandler(MediaMuxer *mediaMuxer)
{
    mMediaMuxer = mediaMuxer;
    
    if (bOutPutHeader) {
        
        LOGD("Push AAC Header to MediaMuxer");
        if (mMediaMuxer!=NULL) {
            mMediaMuxer->pushAACHeader(this->GetHeaders());
        }
        
        bOutPutHeader = false;
    }
}

bool FAACEncoder::Encode(AudioFrame &audioFrame)
{
    AudioPacket outAudioPacket;
    bool ret = this->Encode(audioFrame, outAudioPacket);
    if (ret) {
        if (mMediaMuxer!=NULL) {
            mMediaMuxer->pushAACBody(&outAudioPacket);
        }
    }
    
    return true;
}

AudioPacket* FAACEncoder::GetHeaders()
{
    return &header;
}

void FAACEncoder::setBitrate(int bitrate)
{
    curBitRate = bitrate;
    
    Close();
    
    Open();
}


int FAACEncoder::GetBitRate()
{
    return curBitRate;
}

int FAACEncoder::GetFixedInputFrameSize()
{
    return inputSamples*2;
}
