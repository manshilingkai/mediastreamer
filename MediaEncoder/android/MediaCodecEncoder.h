//
//  MediaCodecEncoder.h
//  AndroidMediaStreamer
//
//  Created by PPTV on 16/9/1.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaCodecEncoder_h
#define MediaCodecEncoder_h

#include <stdio.h>

#include "VideoEncoder.h"
#include "jni.h"

#define CONFIGURE_FLAG_ENCODE 1

#define INFO_OUTPUT_BUFFERS_CHANGED -3
#define INFO_OUTPUT_FORMAT_CHANGED  -2
#define INFO_TRY_AGAIN_LATER        -1

#define COLOR_FormatYUV420Planar 19
#define COLOR_FormatYUV420PackedPlanar 20
#define COLOR_FormatYUV420SemiPlanar 21
#define COLOR_FormatSurface 0x7f000789

#define BUFFER_FLAG_KEY_FRAME 1
#define BUFFER_FLAG_CODEC_CONFIG 2
#define BUFFER_FLAG_END_OF_STREAM 4

#define BITRATE_MODE_CQ 0
#define BITRATE_MODE_VBR 1
#define BITRATE_MODE_CBR 2

#define AVCProfileBaseline 1
#define AVCProfileMain 2
#define AVCProfileHigh 8

#define AVCLevel3 256
#define AVCLevel31 512
#define AVCLevel4 2048

#define KITKAT 19

class MediaCodecEncoder : public VideoEncoder
{
public:
    MediaCodecEncoder(int fps, int width, int height, VIDEO_ENCODE_PROFILE profile, VIDEO_ENCODE_MODE encodeMode, int bitrate, int maxKeyFrameIntervalMs);
    ~MediaCodecEncoder();
    
    void registerJavaVMEnv(JavaVM *jvm);
    void setEncodeSurface(void *surface);
    
    bool Open();
    void Close();
    
    //sync encode
    int Encode(VideoFrame &videoFrame, VideoPacket &videoPacket) {return -1;}
    
    //async encode
    void SetOutputHandler(MediaMuxer *mediaMuxer);
    int Encode(VideoFrame &videoFrame);
    bool Encode();
    
    void SetEncodeMode(VIDEO_ENCODE_MODE encodeMode);
    VIDEO_ENCODE_MODE GetEncodeMode();
    
    void SetBitRate(int bitrate); //kbit
    int GetBitRate();
    
    //[-5 5] default 0
    void SetQuality(int quality) {}
    int GetQuality() {return 0;}
    
    void SetMaxKeyFrameInterval(int maxKeyFrameIntervalMs);
    int GetMaxKeyFrameInterval();
    
    void SetEncoderPreset(VIDEO_ENCODE_PRESET preset) {}
    VIDEO_ENCODE_PRESET GetEncoderPreset() {return 0;}
    
    void SetVBVBufferSize(int vbvBufferSize) {}
    int GetVBVBufferSize() {return 0;}
    
    void EnableStrictCBR(bool enable) {}
    
    //[-6, 6] -6 light filter, 6 strong, 0 default
    void SetDeblockingFilterFactor(int factor) {}
    int GetDeblockingFilterFactor() {return 0;}
    
    bool ReconfigEncodeParam();
    
    VideoPacket * GetHeaders(); //SPS and PPS
    
    void RequestKeyframe();
    int GetBufferedFrames();
    
private:
    JavaVM *mJvm;
    JNIEnv *mEnv;
    
    void* mEncodeSurface;
    
    int mFps;
    int mWidth;
    int mHeight;
    VIDEO_ENCODE_PROFILE mProfile;
    VIDEO_ENCODE_MODE mEncodeMode;
    int mBitrate;
    int mMaxKeyFrameIntervalMs;
    
    jmethodID tostring;
    jmethodID get_codec_count, get_codec_info_at, is_encoder, get_capabilities_for_type;
    jfieldID profile_levels_field, profile_field, level_field;
    jmethodID get_supported_types, get_name;
    jmethodID create_by_codec_name, configure, start, stop, jflush, release;
    jmethodID get_output_format, get_input_buffers, get_output_buffers;
    jmethodID dequeue_input_buffer, dequeue_output_buffer, queue_input_buffer;
    jmethodID release_output_buffer;
    jmethodID create_video_format, set_integer, set_bytebuffer, get_integer;
    jmethodID buffer_info_ctor;
    jmethodID allocate_direct, limit;
    jfieldID size_field, offset_field, pts_field, flags_field;
    
    jmethodID bundle_ctor;
    jmethodID put_int;
    
    jmethodID set_input_surface;
    jmethodID set_parameters;
    
    char mime[64];
    char *codecName;
    
    jobject codec;
    jobject input_buffers, output_buffers;
    jobject buffer_info;

    MediaMuxer *mMediaMuxer;
    
    VideoPacket HeaderPacket;
    
    bool isDoReconfig;
};

#endif /* MediaCodecEncoder_h */
