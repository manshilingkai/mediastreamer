//
//  MediaCodecEncoder.cpp
//  AndroidMediaStreamer
//
//  Created by PPTV on 16/9/1.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaCodecEncoder.h"
#include <sys/system_properties.h>
#include "MediaLog.h"
#include "MediaMath.h"
#include "AndroidUtils.h"
#include "DeviceInfo.h"

enum MEMBERTYPE
{
    UNKNOWNMEMBER, METHOD, STATIC_METHOD, FIELD
};

struct JavaClassMember
{
    const char *memberName;
    const char *sig;
    const char *className;
    void* jniMemberID;
    MEMBERTYPE type;
};

static struct JavaClassMember members[] = {
    { "toString", "()Ljava/lang/String;", "java/lang/Object", NULL, METHOD },
    
    { "getCodecCount", "()I", "android/media/MediaCodecList", NULL, STATIC_METHOD },
    { "getCodecInfoAt", "(I)Landroid/media/MediaCodecInfo;", "android/media/MediaCodecList", NULL, STATIC_METHOD },
    
    { "isEncoder", "()Z", "android/media/MediaCodecInfo", NULL, METHOD },
    { "getSupportedTypes", "()[Ljava/lang/String;", "android/media/MediaCodecInfo", NULL, METHOD },
    { "getName", "()Ljava/lang/String;", "android/media/MediaCodecInfo", NULL, METHOD },
    { "getCapabilitiesForType", "(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;", "android/media/MediaCodecInfo", NULL, METHOD },
    
    { "profileLevels", "[Landroid/media/MediaCodecInfo$CodecProfileLevel;", "android/media/MediaCodecInfo$CodecCapabilities", NULL, FIELD },
    { "profile", "I", "android/media/MediaCodecInfo$CodecProfileLevel", NULL, FIELD },
    { "level", "I", "android/media/MediaCodecInfo$CodecProfileLevel", NULL, FIELD },
    
    { "createByCodecName", "(Ljava/lang/String;)Landroid/media/MediaCodec;", "android/media/MediaCodec", NULL, STATIC_METHOD },
    { "configure", "(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V", "android/media/MediaCodec", NULL, METHOD },
    { "start", "()V", "android/media/MediaCodec", NULL, METHOD },
    { "stop", "()V", "android/media/MediaCodec", NULL, METHOD },
    { "flush", "()V", "android/media/MediaCodec", NULL, METHOD },
    { "release", "()V", "android/media/MediaCodec", NULL, METHOD },
    { "getOutputFormat", "()Landroid/media/MediaFormat;", "android/media/MediaCodec", NULL, METHOD },
    { "getInputBuffers", "()[Ljava/nio/ByteBuffer;", "android/media/MediaCodec", NULL, METHOD },
    { "getOutputBuffers", "()[Ljava/nio/ByteBuffer;", "android/media/MediaCodec", NULL, METHOD },
    { "dequeueInputBuffer", "(J)I", "android/media/MediaCodec", NULL, METHOD },
    { "dequeueOutputBuffer", "(Landroid/media/MediaCodec$BufferInfo;J)I", "android/media/MediaCodec", NULL, METHOD },
    { "queueInputBuffer", "(IIIJI)V", "android/media/MediaCodec", NULL, METHOD },
    { "releaseOutputBuffer", "(IZ)V", "android/media/MediaCodec", NULL, METHOD },
    
    { "createVideoFormat", "(Ljava/lang/String;II)Landroid/media/MediaFormat;", "android/media/MediaFormat", NULL, STATIC_METHOD },
    { "setInteger", "(Ljava/lang/String;I)V", "android/media/MediaFormat", NULL, METHOD },
    { "getInteger", "(Ljava/lang/String;)I", "android/media/MediaFormat", NULL, METHOD },
    { "setByteBuffer", "(Ljava/lang/String;Ljava/nio/ByteBuffer;)V", "android/media/MediaFormat", NULL, METHOD },
    
    { "<init>", "()V", "android/media/MediaCodec$BufferInfo", NULL, METHOD },
    { "size", "I", "android/media/MediaCodec$BufferInfo", NULL, FIELD },
    { "offset", "I", "android/media/MediaCodec$BufferInfo", NULL, FIELD },
    { "presentationTimeUs", "J", "android/media/MediaCodec$BufferInfo", NULL, FIELD },
    { "flags", "I", "android/media/MediaCodec$BufferInfo", NULL, FIELD },
    
    { "allocateDirect", "(I)Ljava/nio/ByteBuffer;", "java/nio/ByteBuffer", NULL, STATIC_METHOD },
    { "limit", "(I)Ljava/nio/Buffer;", "java/nio/ByteBuffer", NULL, METHOD },
    
    { "<init>", "()V", "android/os/Bundle", NULL, METHOD },
    { "putInt", "(Ljava/lang/String;I)V", "android/os/Bundle", NULL, METHOD },
    
    { NULL, NULL, NULL, NULL, UNKNOWNMEMBER },
};

static int jstrcmp(JNIEnv* env, jobject str, const char* str2)
{
    jsize len = env->GetStringUTFLength(str);
    if (len != (jsize) strlen(str2))
        return -1;
    const char *ptr = env->GetStringUTFChars(str, NULL);
    int ret = memcmp(ptr, str2, len);
    env->ReleaseStringUTFChars(str, ptr);
    return ret;
}

MediaCodecEncoder::MediaCodecEncoder(int fps, int width, int height, VIDEO_ENCODE_PROFILE profile, VIDEO_ENCODE_MODE encodeMode, int bitrate, int maxKeyFrameIntervalMs)
{
    mJvm = NULL;
    mEnv = NULL;
    
    mEncodeSurface = NULL;
    
    mFps = fps;
    mWidth = width;
    mHeight = height;
    mProfile = profile;
    mEncodeMode = encodeMode;
    mBitrate = bitrate;
    mMaxKeyFrameIntervalMs = maxKeyFrameIntervalMs;
    
    memset(mime, 0, 64);
    codecName = NULL;
    
    mMediaMuxer = NULL;
    
    codec = NULL;
    input_buffers = NULL;
    output_buffers = NULL;
    buffer_info = NULL;
    
    isDoReconfig = false;
}

MediaCodecEncoder::~MediaCodecEncoder()
{
    //todo
}

void MediaCodecEncoder::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
    mEnv = AndroidUtils::getJNIEnv(mJvm);
}

void MediaCodecEncoder::setEncodeSurface(void *surface)
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    if (mEncodeSurface!=NULL) {
        mEnv->DeleteGlobalRef(static_cast<jobject>(mEncodeSurface));
        mEncodeSurface = NULL;
    }
    
    if (surface!=NULL) {
        mEncodeSurface = (void*)mEnv->NewGlobalRef(static_cast<jobject>(surface));
    }
}

bool MediaCodecEncoder::Open()
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    //for H265:"video/hevc"
    //for H264:"video/avc"
    //for VP8:"video/x-vnd.on2.vp8"
    strcpy(mime, "video/avc");
    
    LOGI("members");
    
    jclass last_class = NULL;
    for (int i = 0; members[i].memberName; i++) {
        if (i == 0 || strcmp(members[i].className, members[i - 1].className))
        {
            //release last_class
            if (last_class!=NULL) {
                mEnv->DeleteLocalRef(last_class);
                last_class = NULL;
            }
            
            last_class = mEnv->FindClass(members[i].className);
        }
        
        if (mEnv->ExceptionOccurred()) {
            LOGE("Unable to find class %s", members[i].className);
            mEnv->ExceptionClear();
            
            return false;
        }
        
        switch (members[i].type) {
            case METHOD:
                members[i].jniMemberID = (void*)
                mEnv->GetMethodID(last_class, members[i].memberName, members[i].sig);
                break;
            case STATIC_METHOD:
                members[i].jniMemberID = (void*)
                mEnv->GetStaticMethodID(last_class, members[i].memberName, members[i].sig);
                break;
            case FIELD:
                members[i].jniMemberID = (void*)
                mEnv->GetFieldID(last_class, members[i].memberName, members[i].sig);
                break;
        }
        
        if (mEnv->ExceptionOccurred()) {
            LOGE("Unable to find the member %s in %s",
                 members[i].memberName, members[i].className);
            mEnv->ExceptionClear();
            
            return false;
        }
    }
    
    //release last_class
    if (last_class!=NULL) {
        mEnv->DeleteLocalRef(last_class);
        last_class = NULL;
    }

    int i = -1;
    this->tostring = (jmethodID)members[++i].jniMemberID;
    this->get_codec_count = (jmethodID)members[++i].jniMemberID;
    this->get_codec_info_at = (jmethodID)members[++i].jniMemberID;
    this->is_encoder = (jmethodID)members[++i].jniMemberID;
    this->get_supported_types = (jmethodID)members[++i].jniMemberID;
    this->get_name = (jmethodID)members[++i].jniMemberID;
    this->get_capabilities_for_type = (jmethodID)members[++i].jniMemberID;
    this->profile_levels_field = (jfieldID)members[++i].jniMemberID;
    this->profile_field = (jfieldID)members[++i].jniMemberID;
    this->level_field = (jfieldID)members[++i].jniMemberID;
    this->create_by_codec_name = (jmethodID)members[++i].jniMemberID;
    this->configure = (jmethodID)members[++i].jniMemberID;
    this->start = (jmethodID)members[++i].jniMemberID;
    this->stop = (jmethodID)members[++i].jniMemberID;
    this->jflush = (jmethodID)members[++i].jniMemberID;
    this->release = (jmethodID)members[++i].jniMemberID;
    this->get_output_format = (jmethodID)members[++i].jniMemberID;
    this->get_input_buffers = (jmethodID)members[++i].jniMemberID;
    this->get_output_buffers = (jmethodID)members[++i].jniMemberID;
    this->dequeue_input_buffer = (jmethodID)members[++i].jniMemberID;
    this->dequeue_output_buffer = (jmethodID)members[++i].jniMemberID;
    this->queue_input_buffer = (jmethodID)members[++i].jniMemberID;
    this->release_output_buffer = (jmethodID)members[++i].jniMemberID;
    this->create_video_format = (jmethodID)members[++i].jniMemberID;
    this->set_integer = (jmethodID)members[++i].jniMemberID;
    this->get_integer = (jmethodID)members[++i].jniMemberID;
    this->set_bytebuffer = (jmethodID)members[++i].jniMemberID;
    this->buffer_info_ctor = (jmethodID)members[++i].jniMemberID;
    this->size_field = (jfieldID)members[++i].jniMemberID;
    this->offset_field = (jfieldID)members[++i].jniMemberID;
    this->pts_field = (jfieldID)members[++i].jniMemberID;
    this->flags_field = (jfieldID)members[++i].jniMemberID;
    this->allocate_direct = (jmethodID)members[++i].jniMemberID;
    this->limit = (jmethodID)members[++i].jniMemberID;
    this->bundle_ctor = (jmethodID)members[++i].jniMemberID;
    this->put_int = (jmethodID)members[++i].jniMemberID;
    
    char* OSVersionStr = DeviceInfo::GetInstance()->get_Version_Sdk();
    int OSVersion = atoi(OSVersionStr);
    if(OSVersion>=23)
    {
        jclass media_codec_class = mEnv->FindClass("android/media/MediaCodec");
        this->set_input_surface = mEnv->GetMethodID(media_codec_class, "setInputSurface", "(Landroid/view/Surface;)V");
        mEnv->DeleteLocalRef(media_codec_class);

        if (mEnv->ExceptionOccurred()) {
            LOGE("Unable to find the member %s in %s",
                 "setInputSurface", "android/media/MediaCodec");
            mEnv->ExceptionClear();
        }
    }
    
    if (OSVersion>=19) {
        jclass media_codec_class = mEnv->FindClass("android/media/MediaCodec");
        this->set_parameters = mEnv->GetMethodID(media_codec_class, "setParameters", "(Landroid/os/Bundle;)V");
        mEnv->DeleteLocalRef(media_codec_class);

        if (mEnv->ExceptionOccurred()) {
            LOGE("Unable to find the member %s in %s",
                 "setParameters", "android/media/MediaCodec");
            mEnv->ExceptionClear();
        }
    }
    
    //get codec name
    jclass media_codec_list_class = mEnv->FindClass("android/media/MediaCodecList");
    int num_codecs = mEnv->CallStaticIntMethod(media_codec_list_class,this->get_codec_count);
    mEnv->DeleteLocalRef(media_codec_list_class);
    
    for (int i = 0; i < num_codecs; i++) {
        jobject info = NULL;
        jobject name = NULL;
        jobject types = NULL;
        jsize name_len = 0;
        int num_types = 0;
        const char *name_ptr = NULL;
        bool found = false;
        
        jclass media_codec_list_class = mEnv->FindClass("android/media/MediaCodecList");
        info = mEnv->CallStaticObjectMethod(media_codec_list_class,this->get_codec_info_at, i);
        mEnv->DeleteLocalRef(media_codec_list_class);
        
        if (!mEnv->CallBooleanMethod(info, this->is_encoder))
        {
            if (info)
                mEnv->DeleteLocalRef(info);
            continue;
        }
        
        types = mEnv->CallObjectMethod(info, this->get_supported_types);
        num_types = mEnv->GetArrayLength((jarray)types);
        name = mEnv->CallObjectMethod(info, this->get_name);
        name_len = mEnv->GetStringUTFLength((jstring)name);
        name_ptr = mEnv->GetStringUTFChars((jstring)name, NULL);
        found = false;
        
        if (!strncmp(name_ptr, "OMX.google.", __MIN(11, name_len)))
        {
            if (name)
            {
                mEnv->ReleaseStringUTFChars((jstring)name, name_ptr);
                mEnv->DeleteLocalRef(name);
            }
            if (types)
                mEnv->DeleteLocalRef(types);
            
            if (info)
                mEnv->DeleteLocalRef(info);
            
            continue;
        }
        
        for (int j = 0; j < num_types && !found; j++) {
            jobject type = mEnv->GetObjectArrayElement(types, j);
            if (!jstrcmp(mEnv, type, mime)) {
                found = true;
            }
            mEnv->DeleteLocalRef(type);
        }

        if (found) {
            LOGI("using %.*s", name_len, name_ptr);
            this->codecName = (char*)malloc(name_len + 1);
            memcpy(this->codecName, name_ptr, name_len);
            this->codecName[name_len] = '\0';
        }
        
        if (name)
        {
            mEnv->ReleaseStringUTFChars((jstring)name, name_ptr);
            mEnv->DeleteLocalRef(name);
        }
        if (types)
            mEnv->DeleteLocalRef(types);
        
        if (info)
            mEnv->DeleteLocalRef(info);
        
        if (found)
            break;
    }
    
    if (!this->codecName) {
        LOGD("No suitable codec matching %s was found", mime);
        return false;
    }
    
    LOGI("alloc codec");
    //alloc codec
    jstring codec_name = NULL;
    codec_name = mEnv->NewStringUTF(this->codecName);
    jclass media_codec_class = mEnv->FindClass("android/media/MediaCodec");
    jobject encoder = mEnv->CallStaticObjectMethod(media_codec_class, this->create_by_codec_name,codec_name);
    mEnv->DeleteLocalRef(media_codec_class);
    mEnv->DeleteLocalRef(codec_name);
    
    if (mEnv->ExceptionOccurred()) {
        LOGE("Exception occurred in MediaCodec.createByCodecName.");
        mEnv->ExceptionClear();
        
        return false;
    }
    
    this->codec = mEnv->NewGlobalRef(encoder);
    mEnv->DeleteLocalRef(encoder);
    
    LOGI("create format");
    //create format
    jstring jstrMime = NULL;
    jstrMime = mEnv->NewStringUTF(mime);
    jclass media_format_class = mEnv->FindClass("android/media/MediaFormat");
    jobject format = mEnv->CallStaticObjectMethod(media_format_class, this->create_video_format, jstrMime, mWidth, mHeight);
    mEnv->DeleteLocalRef(media_format_class);
    mEnv->DeleteLocalRef(jstrMime);
    
    LOGI("set color format");
    //set color format
    char key[64];
    strcpy(key, "color-format");
    jstring jstr_color_format_key = mEnv->NewStringUTF(key);
    jint jint_color_format_value = 0;
    if (mEncodeSurface) {
        jint_color_format_value = COLOR_FormatSurface;
    }else{
        jint_color_format_value = COLOR_FormatYUV420SemiPlanar;
    }
    mEnv->CallVoidMethod(format, this->set_integer, jstr_color_format_key, jint_color_format_value);
    mEnv->DeleteLocalRef(jstr_color_format_key);

    LOGI("set profile");
    // set profile
    strcpy(key, "profile");
    jstring jstr_profile_key = mEnv->NewStringUTF(key);
    if (mProfile==BASE_LINE) {
        mEnv->CallVoidMethod(format, this->set_integer, jstr_profile_key, AVCProfileBaseline);
    }else if (mProfile==MAIN_PROFILE) {
        mEnv->CallVoidMethod(format, this->set_integer, jstr_profile_key, AVCProfileMain);
    }else if (mProfile==HIGH_PROFILE) {
        mEnv->CallVoidMethod(format, this->set_integer, jstr_profile_key, AVCProfileHigh);
    }else {
        mEnv->CallVoidMethod(format, this->set_integer, jstr_profile_key, AVCProfileBaseline);
    }
    mEnv->DeleteLocalRef(jstr_profile_key);
    
    LOGI("set level");
    // set profile
    strcpy(key, "level");
    jstring jstr_level_key = mEnv->NewStringUTF(key);
    if (mProfile==BASE_LINE) {
        mEnv->CallVoidMethod(format, this->set_integer, jstr_level_key, AVCLevel3);
    }else if (mProfile==MAIN_PROFILE) {
        mEnv->CallVoidMethod(format, this->set_integer, jstr_level_key, AVCLevel31);
    }else if (mProfile==HIGH_PROFILE) {
        mEnv->CallVoidMethod(format, this->set_integer, jstr_level_key, AVCLevel4);
    }else {
        mEnv->CallVoidMethod(format, this->set_integer, jstr_level_key, AVCLevel3);
    }
    mEnv->DeleteLocalRef(jstr_level_key);
    
    LOGI("set bitrate mode");
    // set bitrate mode
    strcpy(key, "bitrate-mode");
    jstring jstr_bitrate_mode_key = mEnv->NewStringUTF(key);
    if (mEncodeMode==CBR) {
        mEnv->CallVoidMethod(format, this->set_integer, jstr_bitrate_mode_key, BITRATE_MODE_CBR);
    }else if (mEncodeMode==VBR) {
        mEnv->CallVoidMethod(format, this->set_integer, jstr_bitrate_mode_key, BITRATE_MODE_VBR);
    }else {
        mEnv->CallVoidMethod(format, this->set_integer, jstr_bitrate_mode_key, BITRATE_MODE_CQ);
    }
    mEnv->DeleteLocalRef(jstr_bitrate_mode_key);

    LOGI("set bitrate : %d",mBitrate);
    //set bitrate
    strcpy(key, "bitrate");
    jstring jstr_bitrate_key = mEnv->NewStringUTF(key);
    mEnv->CallVoidMethod(format, this->set_integer, jstr_bitrate_key, mBitrate*1024);
    mEnv->DeleteLocalRef(jstr_bitrate_key);
    
    LOGI("set frame rate : %d", mFps);
    //set frame rate
    strcpy(key, "frame-rate");
    jstring jstr_frame_rate_key = mEnv->NewStringUTF(key);
    mEnv->CallVoidMethod(format, this->set_integer, jstr_frame_rate_key, mFps);
    mEnv->DeleteLocalRef(jstr_frame_rate_key);
    
    LOGI("set i frame interval : %d", mMaxKeyFrameIntervalMs);
    //set i frame interval
    strcpy(key, "i-frame-interval");
    jstring jstr_i_frame_interval_key = mEnv->NewStringUTF(key);
    mEnv->CallVoidMethod(format, this->set_integer, jstr_i_frame_interval_key, mMaxKeyFrameIntervalMs/1000);
    mEnv->DeleteLocalRef(jstr_i_frame_interval_key);
    
    LOGI("configure");
    //configure
    mEnv->CallVoidMethod(this->codec, this->configure, format, NULL, NULL, CONFIGURE_FLAG_ENCODE);
    
    if (mEnv->ExceptionOccurred()) {
        LOGE("Exception occurred in MediaCodec.configure");
        mEnv->ExceptionClear();
        
        return false;
    }
    
    mEnv->DeleteLocalRef(format);
    
    if (OSVersion>=23 && mEncodeSurface) {
        LOGI("setInputSurface");
        
        mEnv->CallVoidMethod(this->codec, this->set_input_surface, mEncodeSurface);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception occurred in MediaCodec.setInputSurface");
            mEnv->ExceptionClear();
        }
    }
    
    LOGI("start");
    //start
    mEnv->CallVoidMethod(this->codec, this->start);
    if (mEnv->ExceptionOccurred()) {
        LOGE("Exception occurred in MediaCodec.start");
        mEnv->ExceptionClear();
        
        return false;
    }
    
    LOGI("Encoder IO");
    // Encoder IO
    jobject jinput_buffers = mEnv->CallObjectMethod(this->codec, this->get_input_buffers);
    this->input_buffers = mEnv->NewGlobalRef(jinput_buffers);
    mEnv->DeleteLocalRef(jinput_buffers);

    jobject joutput_buffers = mEnv->CallObjectMethod(this->codec, this->get_output_buffers);
    this->output_buffers = mEnv->NewGlobalRef(joutput_buffers);
    mEnv->DeleteLocalRef(joutput_buffers);

    jclass buffer_info_class = mEnv->FindClass("android/media/MediaCodec$BufferInfo");
    jobject jbuffer_info = mEnv->NewObject(buffer_info_class, this->buffer_info_ctor);
    mEnv->DeleteLocalRef(buffer_info_class);
    this->buffer_info = mEnv->NewGlobalRef(jbuffer_info);
    mEnv->DeleteLocalRef(jbuffer_info);

    LOGI("MediaCodecEncoder::Open Success");
    
    return true;
}

void MediaCodecEncoder::Close()
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    
    if (this->codec) {
//        LOGI("MediaCodec.flush");
//        mEnv->CallVoidMethod(this->codec, this->jflush);
//        if (mEnv->ExceptionOccurred()) {
//            LOGE("Exception in MediaCodec.flush");
//            mEnv->ExceptionClear();
//        }
        
        LOGI("MediaCodec.stop");
        mEnv->CallVoidMethod(this->codec, this->stop);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.stop");
            mEnv->ExceptionClear();
        }
        
        if (this->input_buffers)
            mEnv->DeleteGlobalRef(this->input_buffers);
        if (this->output_buffers) {
            mEnv->DeleteGlobalRef(this->output_buffers);
        }
        if (this->buffer_info)
            mEnv->DeleteGlobalRef(this->buffer_info);
        
        LOGI("MediaCodec.release");
        mEnv->CallVoidMethod(this->codec, this->release);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.release");
            mEnv->ExceptionClear();
        }
        
        mEnv->DeleteGlobalRef(this->codec);
    }
    
    if (codecName) {
        free(codecName);
        codecName = NULL;
    }
    
    HeaderPacket.Free();
    
    LOGI("MediaCodecEncoder::Close Success");
}

//async encode
void MediaCodecEncoder::SetOutputHandler(MediaMuxer *mediaMuxer)
{
    mMediaMuxer = mediaMuxer;
}

int MediaCodecEncoder::Encode(VideoFrame &videoFrame)
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);

    jlong timeoutUs = 1000;
    
    //first send an encoded packet
    while (true) {
        int index = mEnv->CallIntMethod(this->codec, this->dequeue_output_buffer,this->buffer_info, timeoutUs);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.dequeueOutputBuffer");
            mEnv->ExceptionClear();
            return -1;
        }
        
        if (index>=0) {
            jobject buf = mEnv->GetObjectArrayElement((jobjectArray)this->output_buffers, index);
            void *bufptr = mEnv->GetDirectBufferAddress(buf);
            
            jint size = mEnv->GetIntField(this->buffer_info, this->size_field);
            jlong pts = mEnv->GetLongField(this->buffer_info, this->pts_field);
            jint flags = mEnv->GetIntField(this->buffer_info, this->flags_field);
            
            if (flags==BUFFER_FLAG_CODEC_CONFIG) {
                HeaderPacket.Free();
                
                HeaderPacket.nal_Num = 1;
                Nal *pNal = new Nal;
                pNal->size = size;
                pNal->data = (uint8_t*)malloc(pNal->size);
                HeaderPacket.nals.push_back(pNal);
                
                memcpy(HeaderPacket.nals[0]->data, bufptr, size);
                HeaderPacket.nals[0]->size = size;
                
                HeaderPacket.type = H264_TYPE_SPS_PPS;
                
                //Push H264 Headers to MediaMuxer
                LOGD("Push H264 Header(SPS and PPS) to MediaMuxer");
                if (mMediaMuxer!=NULL) {
                    mMediaMuxer->pushH264Header(&HeaderPacket);
                }
            }else {
                VideoPacket videoPacket;
                Nal *pNal = new Nal;
                pNal->data = (uint8_t *)bufptr;
                pNal->size = size;
                
                videoPacket.nals.push_back(pNal);
                videoPacket.nal_Num = 1;
                videoPacket.pts = pts/1000;
                
                if (flags==BUFFER_FLAG_KEY_FRAME) {
                    videoPacket.type = H264_TYPE_I;
                }else {
                    videoPacket.type = H264_TYPE_P;
                }
                
                if (mMediaMuxer!=NULL) {
                    mMediaMuxer->pushH264Body(&videoPacket);
                }
                
                videoPacket.Clear();
            }
            
            mEnv->DeleteLocalRef(buf);
            
            mEnv->CallVoidMethod(this->codec, this->release_output_buffer, index, false);
            if (mEnv->ExceptionOccurred()) {
                LOGE("Exception in MediaCodec.releaseOutputBuffer");
                mEnv->ExceptionClear();
                return -1;
            }
        }else if(index==INFO_OUTPUT_BUFFERS_CHANGED)
        {
            LOGD("output buffers changed");
            
            if (this->output_buffers) {
                mEnv->DeleteGlobalRef(this->output_buffers);
            }
            
            jobject joutput_buffers = mEnv->CallObjectMethod(this->codec, this->get_output_buffers);
            this->output_buffers = mEnv->NewGlobalRef(joutput_buffers);
            mEnv->DeleteLocalRef(joutput_buffers);

            continue;
        }else if(index==INFO_OUTPUT_FORMAT_CHANGED)
        {
            LOGD("output format changed");
            continue;
        }else
        {
//            LOGD("no output buffer, try again later");
            break;
        }
    }
    
    if (mEncodeSurface==NULL) {
        int index = mEnv->CallIntMethod(this->codec, this->dequeue_input_buffer, timeoutUs);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception occurred in MediaCodec.dequeueInputBuffer");
            mEnv->ExceptionClear();
            
            return -1;
        }
        
        if (index<0) {
            LOGD("input buffers is full");
            return 0;
        }
        
        jobject buf = mEnv->GetObjectArrayElement((jobjectArray)this->input_buffers, index);
        jlong size = mEnv->GetDirectBufferCapacity(buf);
        
        if (size < videoFrame.frameSize) {
            mEnv->DeleteLocalRef(buf);
            return -1;
        }
        
        void *bufptr = mEnv->GetDirectBufferAddress(buf);
        
        memcpy(bufptr, videoFrame.data, videoFrame.frameSize);
        mEnv->CallVoidMethod(this->codec, this->queue_input_buffer, index, 0, videoFrame.frameSize, videoFrame.pts*1000, 0);
        
        mEnv->DeleteLocalRef(buf);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.queueInputBuffer");
            mEnv->ExceptionClear();
            
            return -1;
        }
    }
    
    return 0;
}

bool MediaCodecEncoder::Encode()
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);

    jlong timeoutUs = 1000;
    
    //first send an encoded packet
    while (true) {
        int index = mEnv->CallIntMethod(this->codec, this->dequeue_output_buffer,this->buffer_info, timeoutUs);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception in MediaCodec.dequeueOutputBuffer");
            mEnv->ExceptionClear();
            return false;
        }
        
        if (index>=0) {
            jobject buf = mEnv->GetObjectArrayElement((jobjectArray)this->output_buffers, index);
            void *bufptr = mEnv->GetDirectBufferAddress(buf);
            
            jint size = mEnv->GetIntField(this->buffer_info, this->size_field);
            jlong pts = mEnv->GetLongField(this->buffer_info, this->pts_field);
            jint flags = mEnv->GetIntField(this->buffer_info, this->flags_field);
            jint offset = mEnv->GetIntField(this->buffer_info, this->offset_field);
            
            if (flags==BUFFER_FLAG_CODEC_CONFIG) {
                HeaderPacket.Free();
                
                HeaderPacket.nal_Num = 1;
                Nal *pNal = new Nal;
                pNal->size = size;
                pNal->data = (uint8_t*)malloc(pNal->size);
                HeaderPacket.nals.push_back(pNal);
                
                memcpy(HeaderPacket.nals[0]->data, bufptr+offset, size);
                HeaderPacket.nals[0]->size = size;
                
                HeaderPacket.type = H264_TYPE_SPS_PPS;
                
                //Push H264 Headers to MediaMuxer
                LOGD("Push H264 Header(SPS and PPS) to MediaMuxer");
                if (mMediaMuxer!=NULL) {
                    mMediaMuxer->pushH264Header(&HeaderPacket);
                }
            }else {
                VideoPacket videoPacket;
                Nal *pNal = new Nal;
                pNal->data = (uint8_t *)bufptr+offset;
                pNal->size = size;
                
                videoPacket.nals.push_back(pNal);
                videoPacket.nal_Num = 1;
                videoPacket.pts = pts/1000;
                
                LOGD("pts:%d",videoPacket.pts);
                
                if (flags==BUFFER_FLAG_KEY_FRAME) {
                    videoPacket.type = H264_TYPE_I;
                }else {
                    videoPacket.type = H264_TYPE_P;
                }
                
                if (mMediaMuxer!=NULL) {
                    mMediaMuxer->pushH264Body(&videoPacket);
                }
                
                videoPacket.Clear();
            }
            
            mEnv->DeleteLocalRef(buf);
            
            mEnv->CallVoidMethod(this->codec, this->release_output_buffer, index, false);
            if (mEnv->ExceptionOccurred()) {
                LOGE("Exception in MediaCodec.releaseOutputBuffer");
                mEnv->ExceptionClear();
                return false;
            }
        }else if(index==INFO_OUTPUT_BUFFERS_CHANGED)
        {
            LOGD("output buffers changed");
            
            if (this->output_buffers) {
                mEnv->DeleteGlobalRef(this->output_buffers);
            }
            
            jobject joutput_buffers = mEnv->CallObjectMethod(this->codec, this->get_output_buffers);
            this->output_buffers = mEnv->NewGlobalRef(joutput_buffers);
            mEnv->DeleteLocalRef(joutput_buffers);
            
            continue;
        }else if(index==INFO_OUTPUT_FORMAT_CHANGED)
        {
            LOGD("output format changed");
            continue;
        }else
        {
            //            LOGD("no output buffer, try again later");
            break;
        }
    }
    
    return true;
}

void MediaCodecEncoder::SetEncodeMode(VIDEO_ENCODE_MODE encodeMode)
{
    if (encodeMode!=mEncodeMode) {
        mEncodeMode = encodeMode;
        
        isDoReconfig = true;
    }
}

VIDEO_ENCODE_MODE MediaCodecEncoder::GetEncodeMode()
{
    return mEncodeMode;
}

void MediaCodecEncoder::SetBitRate(int bitrate) //kbit
{
    if (mBitrate!=bitrate) {
        mBitrate = bitrate;
        
        char* OSVersionStr = DeviceInfo::GetInstance()->get_Version_Sdk();
        int OSVersion = atoi(OSVersionStr);
        if(OSVersion>=KITKAT)
        {
            mEnv = AndroidUtils::getJNIEnv(mJvm);
            
            jclass bundle_class = mEnv->FindClass("aandroid/os/Bundle");
            jobject jbundle = mEnv->NewObject(bundle_class, this->bundle_ctor);
            mEnv->DeleteLocalRef(bundle_class);
            
            char key[64];
            strcpy(key, "video-bitrate");
            jstring jstr_video_bitrate_key = mEnv->NewStringUTF(key);
            mEnv->CallObjectMethod(jbundle, this->put_int, jstr_video_bitrate_key, mBitrate*1024);
            mEnv->DeleteLocalRef(jstr_video_bitrate_key);
            
            mEnv->CallVoidMethod(this->codec, this->set_parameters, jbundle);
            if (mEnv->ExceptionOccurred()) {
                LOGE("Exception occurred in MediaCodec.setParameters");
                mEnv->ExceptionClear();
            }
            
            mEnv->DeleteLocalRef(jbundle);
        }
    }
}
int MediaCodecEncoder::GetBitRate()
{
    return mBitrate;
}

void MediaCodecEncoder::SetMaxKeyFrameInterval(int maxKeyFrameIntervalMs)
{
    if (mMaxKeyFrameIntervalMs!=maxKeyFrameIntervalMs) {
        mMaxKeyFrameIntervalMs = maxKeyFrameIntervalMs;
        
        isDoReconfig = true;
    }
}

int MediaCodecEncoder::GetMaxKeyFrameInterval()
{
    return mMaxKeyFrameIntervalMs;
}

bool MediaCodecEncoder::ReconfigEncodeParam()
{
    if (isDoReconfig) {
        isDoReconfig = false;
        
        this->Close();
        this->Open();
    }
}

VideoPacket * MediaCodecEncoder::GetHeaders() //SPS and PPS
{
    return &HeaderPacket;
}

void MediaCodecEncoder::RequestKeyframe()
{
    char* OSVersionStr = DeviceInfo::GetInstance()->get_Version_Sdk();
    int OSVersion = atoi(OSVersionStr);
    if(OSVersion>=KITKAT)
    {
        mEnv = AndroidUtils::getJNIEnv(mJvm);
        
        jclass bundle_class = mEnv->FindClass("aandroid/os/Bundle");
        jobject jbundle = mEnv->NewObject(bundle_class, this->bundle_ctor);
        mEnv->DeleteLocalRef(bundle_class);
        
        char key[64];
        strcpy(key, "request-sync");
        jstring jstr_request_sync_key = mEnv->NewStringUTF(key);
        mEnv->CallObjectMethod(jbundle, this->put_int, jstr_request_sync_key, 0);
        mEnv->DeleteLocalRef(jstr_request_sync_key);
        
        mEnv->CallVoidMethod(this->codec, this->set_parameters, jbundle);
        if (mEnv->ExceptionOccurred()) {
            LOGE("Exception occurred in MediaCodec.setParameters");
            mEnv->ExceptionClear();
        }
        
        mEnv->DeleteLocalRef(jbundle);
    }
}

int MediaCodecEncoder::GetBufferedFrames()
{
    return 0;
}
