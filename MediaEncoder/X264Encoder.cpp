//
//  x264_encoder.cpp
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "X264Encoder.h"
#include "MediaLog.h"

#ifdef WIN32
#define bzero(a, b) memset(a, 0, b)
#endif

static const float baseCRF = 23.0f;

X264Encoder::X264Encoder(int fps, int width, int height, VIDEO_ENCODE_PROFILE profile, VIDEO_ENCODE_MODE encodeMode, int bitrate, int quality, int maxKeyFrameIntervalMs, VIDEO_ENCODE_PRESET preset, int vbvBufferSize, bool enableStrictCBR, int deblockingFilterFactor)
{
    this->mFps = fps;
    this->mWidth = width;
    this->mHeight = height;
    this->mProfile = profile;
    
    this->mVideoEncodeMode = encodeMode;
    this->mBitrate = bitrate;
    this->mQuality = quality;
    this->mMaxKeyFrameInterval = maxKeyFrameIntervalMs;
    this->mPreset = preset;
    this->mVBVBufferSize = vbvBufferSize;
    this->bStrictCBR = enableStrictCBR;
    this->mDeblockingFilterFactor = deblockingFilterFactor;
    
    this->bRequestKeyframe = false;
    this->isDoReconfig = false;
    this->x264 = NULL;

    this->mMediaMuxer = NULL;
    this->bOutPutHeader = true;
}

X264Encoder::~X264Encoder()
{
    if (x264) {
        x264_encoder_close(x264);
        x264 = NULL;
    }
    
    HeaderPacket.Free();
}

#ifdef ANDROID
void X264Encoder::registerJavaVMEnv(JavaVM *jvm)
{
}
#endif

bool X264Encoder::Open()
{
    bzero(&paramData, sizeof(paramData));
    
    if (x264_param_default_preset(&paramData, x264_preset_names[this->mPreset], x264_tune_names[7])) {
        LOGE("Failed to set x264 defaults: %s/%s",x264_preset_names[this->mPreset],x264_tune_names[7]);
        return false;
    }
    
    paramData.b_deterministic = false;
    paramData.i_threads = X264_THREADS_AUTO;
    paramData.i_level_idc = 40;
    
    if (this->mProfile==BASE_LINE) {
        // no B frame
        paramData.i_bframe = 0;
        paramData.i_bframe_adaptive = X264_B_ADAPT_NONE;
        
        paramData.b_cabac = 0;
    }else if(this->mProfile==MAIN_PROFILE) {
        // set B frame
        paramData.i_bframe = this->mFps*(this->mMaxKeyFrameInterval/1000)/10;
//        paramData.i_bframe_adaptive = X264_B_ADAPT_NONE;
        paramData.i_bframe_pyramid = 0;
        
        paramData.b_cabac = 1;
    }else if (this->mProfile==HIGH_PROFILE) {
        // set B frame
        paramData.i_bframe = this->mFps*(this->mMaxKeyFrameInterval/1000)/5;
//        paramData.i_bframe_adaptive = X264_B_ADAPT_NONE;
        paramData.i_bframe_pyramid = 0;
        
        paramData.b_cabac = 1;
    }else {
        // no B frame
        paramData.i_bframe = 0;
        paramData.i_bframe_adaptive = X264_B_ADAPT_NONE;
        
        paramData.b_cabac = 0;
    }

    paramData.rc.i_vbv_max_bitrate  = this->mBitrate;//kbps
    paramData.rc.i_vbv_buffer_size  = this->mVBVBufferSize;
    
    if (this->mVideoEncodeMode==VBR) {
        //CRF:23 is default, a subjectively sane range is 18-28
        paramData.rc.i_rc_method    = X264_RC_CRF;
        paramData.rc.f_rf_constant  = baseCRF+this->mQuality;
        paramData.rc.f_rf_constant_max = 28;
    }else{
        paramData.rc.i_rc_method    = X264_RC_ABR;
        paramData.rc.f_rf_constant  = 0.0f;
        paramData.rc.f_rf_constant_max = 0.0f;
        
        // VFR input.  If 1, use timebase and timestamps for ratecontrol purposes. If 0, use fps only.
        paramData.b_vfr_input = 1;
        
        paramData.rc.i_bitrate = this->mBitrate;//kbps
        
        if (bStrictCBR) {
            paramData.rc.b_filler = 1;
            paramData.i_nal_hrd = X264_NAL_HRD_CBR;
        }
    }
    
    paramData.b_deblocking_filter = 1;
    paramData.i_deblocking_filter_alphac0 = this->mDeblockingFilterFactor;
    paramData.i_deblocking_filter_beta = this->mDeblockingFilterFactor;
    
    paramData.i_csp = X264_CSP_I420;
    
    paramData.i_width               = this->mWidth;
    paramData.i_height              = this->mHeight;
    
    paramData.vui.b_fullrange       = false;
    paramData.vui.i_colorprim       = 1%sizeof(x264_colorprim_names);
    paramData.vui.i_transfer        = 14%sizeof(x264_transfer_names);
    paramData.vui.i_colmatrix       = this->mWidth>=1280 || this->mHeight>576 ? 1%sizeof(x264_colmatrix_names) : 6%sizeof(x264_colmatrix_names);
    
    paramData.i_keyint_max      = this->mFps*mMaxKeyFrameInterval/1000;
    
    paramData.i_fps_num             = this->mFps;
    paramData.i_fps_den             = 1;
    
    paramData.i_timebase_num        = 1;
    paramData.i_timebase_den        = 1000;
    
    //    paramData.i_log_level           = X264_LOG_WARNING;
    
    // default baseline profile
    if (x264_param_apply_profile (&paramData, x264_profile_names[this->mProfile]))
    {
        LOGE("Failed to set x264 profile: %s",x264_profile_names[this->mProfile]);
        return false;
    }
    
    x264 = x264_encoder_open(&paramData);
    if(!x264)
    {
        LOGE("%s","Could not open x264");
        return false;
    }
    
    LOGD("%s","Finish Open X264Encoder");
    
    return true;
}

void X264Encoder::Close()
{
    if (x264) {
        /*
        //Flush delayed frames
        x264_nal_t *nalOut;
        int nalNum;
        while(x264_encoder_delayed_frames(x264))
        {
            x264_encoder_encode(x264, &nalOut, &nalNum, NULL, &picOut);
        }*/
        
        x264_encoder_close(x264);
        x264 = NULL;
    }
    
    LOGD("%s","Finish Close X264Encoder");

}

int X264Encoder::Encode(VideoFrame &videoFrame, VideoPacket &videoPacket)
{
    x264_picture_init(&picIn);
    picIn.i_pts = videoFrame.pts;
    picIn.i_type = X264_TYPE_AUTO;
    picIn.i_qpplus1 = X264_QP_AUTO;
    picIn.img.i_csp = paramData.i_csp;
    picIn.img.i_plane = 3;
    
    //Y
    picIn.img.i_stride[0] = this->mWidth;
    picIn.img.plane[0] = videoFrame.data;
    
    //U
    picIn.img.i_stride[1] = this->mWidth/2;
    picIn.img.plane[1] = picIn.img.plane[0] + picIn.img.i_stride[0]*this->mHeight;
    
    //V
    picIn.img.i_stride[2] = this->mWidth/2;
    picIn.img.plane[2] = picIn.img.plane[1] + picIn.img.i_stride[1]*this->mHeight/2;
    
    if(bRequestKeyframe)
        picIn.i_type = X264_TYPE_IDR;
    
    // picIn.prop.quant_offsets [ROI]
    
    x264_nal_t *nalOut;
    int nalNum;
    
    if(x264_encoder_encode(x264, &nalOut, &nalNum, &picIn, &picOut) < 0)
    {
        LOGE("%s","x264 encode failed");
        return -1;
    }
    
    if(bRequestKeyframe)
    {
        bRequestKeyframe = false;
    }
    
    //clear videoPacket
    videoPacket.Clear();
    
    for(int i=0; i<nalNum; i++)
    {
        x264_nal_t &nal = nalOut[i];
        
        Nal *pNal = new Nal;
        pNal->data = nal.p_payload;
        pNal->size = nal.i_payload;
        
        videoPacket.nals.push_back(pNal);
    }
    
    videoPacket.nal_Num = nalNum;
    
//    videoPacket.pts = picIn.i_pts;
//    videoPacket.dts = videoPacket.pts;
    
    videoPacket.pts = picOut.i_pts;
    videoPacket.dts = picOut.i_dts;
    
    if (IS_X264_TYPE_I(picOut.i_type)) {
        videoPacket.type = H264_TYPE_I;
    }else if(IS_X264_TYPE_B(picOut.i_type)) {
        videoPacket.type = H264_TYPE_B;
    }else if(X264_TYPE_P==picOut.i_type) {
        videoPacket.type = H264_TYPE_P;
    }else {
        videoPacket.type = VC_TYPE_UNKNOWN;
    }
    
    return 0;
}

void X264Encoder::SetOutputHandler(MediaMuxer *mediaMuxer)
{
    mMediaMuxer = mediaMuxer;
}

int X264Encoder::Encode(VideoFrame &videoFrame)
{
    if (bOutPutHeader) {
        //Push H264 Headers to MediaMuxer
        LOGD("Push H264 Header(SPS and PPS) to MediaMuxer");
        if (mMediaMuxer!=NULL) {
            mMediaMuxer->pushH264Header(GetHeaders());
        }
        
        bOutPutHeader = false;
    }
    
    VideoPacket videoPacket;
    
    int ret = this->Encode(videoFrame, videoPacket);
    
    if (ret<0)
    {
        videoPacket.Clear();
        return ret;
    }
    
    if (mMediaMuxer!=NULL) {
        mMediaMuxer->pushH264Body(&videoPacket);
    }
    
    videoPacket.Clear();
    
    return 0;
}

void X264Encoder::SetEncodeMode(VIDEO_ENCODE_MODE encodeMode)
{
    if (encodeMode!=this->mVideoEncodeMode) {
        this->mVideoEncodeMode = encodeMode;
        
        this->isDoReconfig = true;
    }
}

VIDEO_ENCODE_MODE X264Encoder::GetEncodeMode()
{
    return this->mVideoEncodeMode;
}

void X264Encoder::SetBitRate(int bitrate)
{
    if (this->mBitrate!=bitrate) {
        this->mBitrate = bitrate;
        
        this->isDoReconfig = true;
    }
}

int X264Encoder::GetBitRate()
{
    return this->mBitrate;
}

void X264Encoder::SetQuality(int quality)
{
    if (this->mQuality!=quality) {
        this->mQuality=quality;
        
        this->isDoReconfig = true;
    }
}

int X264Encoder::GetQuality()
{
    return this->mQuality;
}

void X264Encoder::SetMaxKeyFrameInterval(int maxKeyFrameIntervalMs)
{
    if (this->mMaxKeyFrameInterval!=maxKeyFrameIntervalMs) {
        this->mMaxKeyFrameInterval=maxKeyFrameIntervalMs;
        
        this->isDoReconfig = true;
    }
}

int X264Encoder::GetMaxKeyFrameInterval()
{
    return this->mMaxKeyFrameInterval;
}

void X264Encoder::SetEncoderPreset(VIDEO_ENCODE_PRESET preset)
{
    if (this->mPreset!=preset) {
        this->mPreset=preset;
        
        this->isDoReconfig = true;
    }
}

VIDEO_ENCODE_PRESET X264Encoder::GetEncoderPreset()
{
    return this->mPreset;
}

void X264Encoder::SetVBVBufferSize(int vbvBufferSize)
{
    if (this->mVBVBufferSize!=vbvBufferSize) {
        this->mVBVBufferSize=vbvBufferSize;
        
        this->isDoReconfig = true;
    }
}

int X264Encoder::GetVBVBufferSize()
{
    return this->mVBVBufferSize;
}

void X264Encoder::EnableStrictCBR(bool enable)
{
    if (this->bStrictCBR!=enable) {
        this->bStrictCBR = enable;
        
        this->isDoReconfig = true;
    }
}

//[-6, 6] -6 light filter, 6 strong, 0 default
void X264Encoder::SetDeblockingFilterFactor(int factor)
{
    if (this->mDeblockingFilterFactor!=factor) {
        this->mDeblockingFilterFactor = factor;
        
        this->isDoReconfig = true;
    }
}

int X264Encoder::GetDeblockingFilterFactor()
{
    return this->mDeblockingFilterFactor;
}

bool X264Encoder::ReconfigEncodeParam()
{
    if (this->isDoReconfig && x264!=NULL)
    {
        // old reconfig method
//        Close();
//        return Open();
        
        // new reconfig method
        
        bzero(&paramData, sizeof(paramData));
        
        if (x264_param_default_preset(&paramData, x264_preset_names[this->mPreset], x264_tune_names[7])) {
            LOGE("Failed to set x264 defaults: %s/%s",x264_preset_names[this->mPreset],x264_tune_names[7]);
            return false;
        }
        
        paramData.b_deterministic = false;
        paramData.i_threads = X264_THREADS_AUTO;
        paramData.i_level_idc = 40;
        
        if (this->mProfile==BASE_LINE) {
            // no B frame
            paramData.i_bframe = 0;
            paramData.i_bframe_adaptive = X264_B_ADAPT_NONE;
            
            paramData.b_cabac = 0;
        }else if(this->mProfile==MAIN_PROFILE) {
            // set B frame
            paramData.i_bframe = this->mFps*(this->mMaxKeyFrameInterval/1000)/10;
            //        paramData.i_bframe_adaptive = X264_B_ADAPT_NONE;
            paramData.i_bframe_pyramid = 0;
            
            paramData.b_cabac = 1;
        }else if (this->mProfile==HIGH_PROFILE) {
            // set B frame
            paramData.i_bframe = this->mFps*(this->mMaxKeyFrameInterval/1000)/5;
            //        paramData.i_bframe_adaptive = X264_B_ADAPT_NONE;
            paramData.i_bframe_pyramid = 0;
            
            paramData.b_cabac = 1;
        }else {
            // no B frame
            paramData.i_bframe = 0;
            paramData.i_bframe_adaptive = X264_B_ADAPT_NONE;
            
            paramData.b_cabac = 0;
        }
        
        paramData.rc.i_vbv_max_bitrate  = this->mBitrate;//kbps
        paramData.rc.i_vbv_buffer_size  = this->mVBVBufferSize;
        
        if (this->mVideoEncodeMode==VBR) {
            //CRF:23 is default, a subjectively sane range is 18-48
            paramData.rc.i_rc_method    = X264_RC_CRF;
            paramData.rc.f_rf_constant  = baseCRF+this->mQuality;
            paramData.rc.f_rf_constant_max = 48;
        }else{
            paramData.rc.i_rc_method    = X264_RC_ABR;
            paramData.rc.f_rf_constant  = 0.0f;
            paramData.rc.f_rf_constant_max = 0.0f;
            
            // VFR input.  If 1, use timebase and timestamps for ratecontrol purposes. If 0, use fps only.
            paramData.b_vfr_input = 1;
            
            paramData.rc.i_bitrate = this->mBitrate;//kbps
            
            if (bStrictCBR) {
                paramData.rc.b_filler = 1;
                paramData.i_nal_hrd = X264_NAL_HRD_CBR;
            }
        }
        
        paramData.b_deblocking_filter = 1;
        paramData.i_deblocking_filter_alphac0 = this->mDeblockingFilterFactor;
        paramData.i_deblocking_filter_beta = this->mDeblockingFilterFactor;
        
        paramData.i_csp = X264_CSP_I420;
        
        paramData.i_width               = this->mWidth;
        paramData.i_height              = this->mHeight;
        
        paramData.vui.b_fullrange       = false;
        paramData.vui.i_colorprim       = 1%sizeof(x264_colorprim_names);
        paramData.vui.i_transfer        = 14%sizeof(x264_transfer_names);
        paramData.vui.i_colmatrix       = this->mWidth>=1280 || this->mHeight>576 ? 1%sizeof(x264_colmatrix_names) : 6%sizeof(x264_colmatrix_names);
        
        paramData.i_keyint_max      = this->mFps*mMaxKeyFrameInterval/1000;
        
        paramData.i_fps_num             = this->mFps;
        paramData.i_fps_den             = 1;
        
        paramData.i_timebase_num        = 1;
        paramData.i_timebase_den        = 1000;
        
        //    paramData.i_log_level           = X264_LOG_WARNING;
        
        // default baseline profile
        if (x264_param_apply_profile (&paramData, x264_profile_names[this->mProfile]))
        {
            LOGE("Failed to set x264 profile: %s",x264_profile_names[this->mProfile]);
            return false;
        }
        
        int ret = x264_encoder_reconfig(x264, &paramData);
        
        if (ret) {
            LOGD("Reconfig Encode Param Fail");
            return false;
        }else{
            LOGD("Reconfig Encode Param Success");
            return true;
        }
    }
    
    return false;
}

VideoPacket *X264Encoder::GetHeaders()
{
    HeaderPacket.Free();
    
    x264_nal_t *nalOut;
    int nalNum;
    
    x264_encoder_headers(x264, &nalOut, &nalNum);
    
    if(nalNum>0)
    {
        Nal *pNal = new Nal;
        pNal->size = MAX_H264_HEADER_SIZE;
        pNal->data = (uint8_t*)malloc(pNal->size);
        HeaderPacket.nals.push_back(pNal);
        
        pNal = new Nal;
        pNal->size = MAX_H264_HEADER_SIZE;
        pNal->data = (uint8_t*)malloc(pNal->size);
        HeaderPacket.nals.push_back(pNal);
        
        HeaderPacket.nal_Num = 2;
    }
    
    for(int i=0; i<nalNum; i++)
    {
        x264_nal_t &nal = nalOut[i];
        
        if(nal.i_type == NAL_SPS)
        {
            memcpy(HeaderPacket.nals[0]->data, nal.p_payload, nal.i_payload);
            HeaderPacket.nals[0]->size = nal.i_payload;
        }else if(nal.i_type == NAL_PPS) {
            memcpy(HeaderPacket.nals[1]->data, nal.p_payload, nal.i_payload);
            HeaderPacket.nals[1]->size = nal.i_payload;
        }
    }
    
    HeaderPacket.type = H264_TYPE_SPS_PPS;
    
    return &HeaderPacket;
}

void X264Encoder::RequestKeyframe()
{
    bRequestKeyframe = true;
}

int X264Encoder::GetBufferedFrames()
{
    return x264_encoder_delayed_frames(x264);
}
