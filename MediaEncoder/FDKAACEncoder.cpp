//
//  FDKAACEncoder.cpp
//  MediaStreamer
//
//  Created by Think on 2019/9/17.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "FDKAACEncoder.h"
#include "MediaLog.h"

FDKAACEncoder::FDKAACEncoder(unsigned int sampleRate, unsigned int numChannels, unsigned int bitRate)
{
    curSampleRate = sampleRate;
    curNumChannels = numChannels;
    curBitRate = bitRate;
    
    handle = NULL;
    if (curNumChannels==1) {
        channel_mode = MODE_1;
        profile_aac = PROFILE_AAC_HE;
    }else if(curNumChannels==2) {
        channel_mode = MODE_2;
        profile_aac = PROFILE_AAC_HE_v2;
    }else {
        channel_mode = MODE_1;
        profile_aac = PROFILE_AAC_HE;
    }
    pcm_frame_len = 0;
    
    output = NULL;
    output_len = 0;
    
    memset(&header, 0, sizeof(AudioPacket));
    
    mMediaMuxer = NULL;
    bOutPutHeader = false;
}

FDKAACEncoder::~FDKAACEncoder()
{
    Close();
}

const char *FDKAACEncoder::fdkaac_error(AACENC_ERROR erraac)
{
    switch (erraac)
    {
        case AACENC_OK: return "No error";
        case AACENC_INVALID_HANDLE: return "Invalid handle";
        case AACENC_MEMORY_ERROR: return "Memory allocation error";
        case AACENC_UNSUPPORTED_PARAMETER: return "Unsupported parameter";
        case AACENC_INVALID_CONFIG: return "Invalid config";
        case AACENC_INIT_ERROR: return "Initialization error";
        case AACENC_INIT_AAC_ERROR: return "AAC library initialization error";
        case AACENC_INIT_SBR_ERROR: return "SBR library initialization error";
        case AACENC_INIT_TP_ERROR: return "Transport library initialization error";
        case AACENC_INIT_META_ERROR: return "Metadata library initialization error";
        case AACENC_ENCODE_ERROR: return "Encoding error";
        case AACENC_ENCODE_EOF: return "End of file";
        default: return "Unknown error";
    }
}

bool FDKAACEncoder::Open()
{
    if (handle) {
        if (aacEncClose(&handle) != AACENC_OK)
        {
            LOGE("aacEncClose failed");
        }
        
        handle = NULL;
    }
    
    AACENC_ERROR ret;
    
    if ((ret = aacEncOpen(&handle, 0x0, curNumChannels)) != AACENC_OK)
    {
        LOGE("Unable to open fdkaac encoder, ret = 0x%x, error is %s", ret, fdkaac_error(ret));
        
        handle = NULL;

        return false;
    }
    
    if ((ret = aacEncoder_SetParam(handle, AACENC_AOT, profile_aac)) != AACENC_OK)
    {
        LOGE("Unable to set the AACENC_AOT, ret = 0x%x, error is %s", ret, fdkaac_error(ret));

        if (aacEncClose(&handle) != AACENC_OK)
        {
            LOGE("aacEncClose failed");
        }
        
        handle = NULL;
        
        return false;
    }
    
    if ((ret = aacEncoder_SetParam(handle, AACENC_SAMPLERATE, curSampleRate)) != AACENC_OK)
    {
        LOGE("Unable to set the SAMPLERATE, ret = 0x%x, error is %s", ret, fdkaac_error(ret));
        
        if (aacEncClose(&handle) != AACENC_OK)
        {
            LOGE("aacEncClose failed");
        }
        
        handle = NULL;
        
        return false;
    }
    
    if ((ret = aacEncoder_SetParam(handle, AACENC_CHANNELMODE, channel_mode)) != AACENC_OK)
    {
        LOGE("Unable to set the AACENC_CHANNELMODE, ret = 0x%x, error is %s", ret, fdkaac_error(ret));
        
        if (aacEncClose(&handle) != AACENC_OK)
        {
            LOGE("aacEncClose failed");
        }
        
        handle = NULL;
        
        return false;
    }
    
    if ((ret = aacEncoder_SetParam(handle, AACENC_BITRATE, curBitRate*1024)) != AACENC_OK)
    {
        LOGE("Unable to set the AACENC_BITRATE, ret = 0x%x, error is %s", ret, fdkaac_error(ret));
        
        if (aacEncClose(&handle) != AACENC_OK)
        {
            LOGE("aacEncClose failed");
        }
        
        handle = NULL;
        
        return false;
    }
    
    if ((ret = aacEncoder_SetParam(handle, AACENC_TRANSMUX, TT_MP4_RAW)) != AACENC_OK) // 0-raw 2-adts
    {
        LOGE("Unable to set the ADTS AACENC_TRANSMUX, ret = 0x%x, error is %s", ret, fdkaac_error(ret));
        
        if (aacEncClose(&handle) != AACENC_OK)
        {
            LOGE("aacEncClose failed");
        }
        
        handle = NULL;
        
        return false;
    }
    
    if ((ret = aacEncEncode(handle, NULL, NULL, NULL, NULL)) != AACENC_OK)
    {
        LOGE("Unable to initialize the aacEncEncode, ret = 0x%x, error is %s", ret, fdkaac_error(ret));
        
        if (aacEncClose(&handle) != AACENC_OK)
        {
            LOGE("aacEncClose failed");
        }
        
        handle = NULL;
        
        return false;
    }
    
    if ((ret = aacEncInfo(handle, &info)) != AACENC_OK)
    {
        LOGE("Unable to get the aacEncInfo info, ret = 0x%x, error is %s", ret, fdkaac_error(ret));
        
        if (aacEncClose(&handle) != AACENC_OK)
        {
            LOGE("aacEncClose failed");
        }
        
        handle = NULL;
        
        return false;
    }
    
    pcm_frame_len = info.inputChannels * info.frameLength * 2;

    LOGD("AACEncoderInit channels = %d, pcm_frame_len = %d", info.inputChannels, pcm_frame_len);
    
    if (output) {
        free(output);
        output = NULL;
    }
    
    output_len = pcm_frame_len;
    output = (uint8_t*)malloc(output_len);
    
    header.data = info.confBuf;
    header.size = info.confSize;
    
    return true;
}

void FDKAACEncoder::Close()
{
    if (handle) {
        if (aacEncClose(&handle) != AACENC_OK)
        {
            LOGE("aacEncClose failed");
        }
        
        handle = NULL;
    }
    
    if (output) {
        free(output);
        output = NULL;
    }
    
    output_len = 0;
}

bool FDKAACEncoder::Encode(AudioFrame &audioFrame, AudioPacket &audioPacket)
{
    AACENC_ERROR ret;
    
    if (handle==NULL) {
        LOGE("AACENC_INVALID_HANDLE");
        return false;
    }
    
    if (audioFrame.frameSize != pcm_frame_len) {
        LOGE("AACENC_UNSUPPORTED_PARAMETER input len = %d no equal to need length = %d\n", audioFrame.frameSize, pcm_frame_len);
        return false;
    }
    
    memset(&audioPacket, 0, sizeof(AudioPacket));
    
    AACENC_BufDesc  in_buf = { 0 };
    AACENC_BufDesc  out_buf = { 0 };
    AACENC_InArgs   in_args = { 0 };
    AACENC_OutArgs  out_args = { 0 };

    in_args.numInSamples = audioFrame.frameSize/2;
    
    int     in_identifier = IN_AUDIO_DATA;
    int     in_elem_size = 2;
    void    *in_ptr = audioFrame.data;
    int     in_buffer_size = audioFrame.frameSize;
    in_buf.numBufs = 1;
    in_buf.bufs = &in_ptr;
    in_buf.bufferIdentifiers = &in_identifier;
    in_buf.bufSizes = &in_buffer_size;
    in_buf.bufElSizes = &in_elem_size;
    
    int out_identifier = OUT_BITSTREAM_DATA;
    int out_elem_size = 1;
    void *out_ptr = output;
    int out_buffer_size = output_len;
    out_buf.numBufs = 1;
    out_buf.bufs = &out_ptr;
    out_buf.bufferIdentifiers = &out_identifier;
    out_buf.bufSizes = &out_buffer_size;
    out_buf.bufElSizes = &out_elem_size;
    
    if ((ret = aacEncEncode(handle, &in_buf, &out_buf, &in_args, &out_args)) != AACENC_OK)
    {
        LOGE("aacEncEncode ret = 0x%x, error is %s", ret, fdkaac_error(ret));
        
        return false;
    }
    
    audioPacket.data = output;
    audioPacket.size = out_args.numOutBytes;
    audioPacket.pts = audioFrame.pts;
    audioPacket.dts = audioFrame.pts;
    audioPacket.duration = audioFrame.duration;
    
    return true;
}

void FDKAACEncoder::SetOutputHandler(MediaMuxer *mediaMuxer)
{
    mMediaMuxer = mediaMuxer;
    
    if (bOutPutHeader) {
        
        LOGD("Push AAC Header to MediaMuxer");
        if (mMediaMuxer!=NULL) {
            mMediaMuxer->pushAACHeader(this->GetHeaders());
        }
        
        bOutPutHeader = false;
    }
}

bool FDKAACEncoder::Encode(AudioFrame &audioFrame)
{
    AudioPacket outAudioPacket;
    bool ret = this->Encode(audioFrame, outAudioPacket);
    if (ret) {
        if (mMediaMuxer!=NULL) {
            mMediaMuxer->pushAACBody(&outAudioPacket);
        }
    }

    return true;
}

AudioPacket* FDKAACEncoder::GetHeaders()
{
    return &header;
}

void FDKAACEncoder::setBitrate(int bitrate)
{
    curBitRate = bitrate;
    
    Close();
    
    Open();
}

int FDKAACEncoder::GetBitRate()
{
    return curBitRate;
}

int FDKAACEncoder::GetFixedInputFrameSize()
{
    return pcm_frame_len;
}
