//
//  LibHevcEncoder.h
//  MediaStreamer
//
//  Created by Think on 2021/2/8.
//  Copyright © 2021 Cell. All rights reserved.
//

#ifndef LibHevcEncoder_h
#define LibHevcEncoder_h

#include <stdio.h>
#include "VideoEncoder.h"

extern "C" {
#include "ihevc_typedefs.h"
#include "itt_video_api.h"
#include "ihevce_api.h"
#include "ihevce_plugin.h"
#include "ihevce_profile.h"
}

class LibHevcEncoder : public VideoEncoder {
public:
    LibHevcEncoder();
    ~LibHevcEncoder();
private:
    IHEVCE_PLUGIN_STATUS_T openLibhevcEncoder(int w, int h, int fps, int bitrate, int gop, int cores);
    IHEVCE_PLUGIN_STATUS_T encode_frame(void* y_data, void* u_data, void* v_data, int y_stride, int u_stride, int v_stride, bool is_force_idr, void** out_buf, long * out_bytes);
    IHEVCE_PLUGIN_STATUS_T closeLibhevcEncoder();
    IHEVCE_PLUGIN_STATUS_T setBitrate(int bitrate);
    IHEVCE_PLUGIN_STATUS_T getEncoderHdr();
private:
    ihevce_static_cfg_params_t param;
    void *libhevcEncoder;
    ihevce_inp_buf_t in_pic;
    ihevce_out_buf_t out_pic;
    profile_database_t profile_data;
    ihevce_out_buf_t out_pic_hdr;
};

#endif /* LibHevcEncoder_h */
