#!/bin/sh

if [ -z "$ANDROID_NDK_HOME" ];then
    echo "ERROR: Please add ANDROID_NDK_HOME into your enviroment. The required version is ndk-r16."
    exit -1
fi

ndk-build NDK_PROJECT_PATH=. APP_BUILD_SCRIPT=./Android.mk NDK_APPLICATION_MK=./Application.mk

