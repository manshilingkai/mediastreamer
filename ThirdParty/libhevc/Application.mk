APP_OPTIM := release
APP_PLATFORM := android-16
APP_ABI := arm64-v8a
NDK_TOOLCHAIN_VERSION := 4.9
APP_PIE := false
APP_CPPFLAGS := -fPIC -O3 -Wall -Werror -Wno-error=constant-conversion -Wno-unused-variable -Wno-unused-parameter -Wno-switch
