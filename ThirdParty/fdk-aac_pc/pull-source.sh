#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

echo "== clean source =="
if [ -d "fdk-aac" ]; then
rm -rf fdk-aac
fi

echo "== pull faac base =="
git clone https://github.com/ShiftMediaProject/fdk-aac.git fdk-aac
