#! /usr/bin/env bash
#
# Copyright (C) 2013-2014 William Shi <manshilingkai@gmail.com>
#
#

#--------------------
echo "===================="
echo "[*] check env $1"
echo "===================="
set -e

if [ -z "$ANDROID_NDK" ]; then
    echo "You must define ANDROID_NDK before starting."
    echo "They must point to your NDK directories.\n"
    exit 1
fi

LIBFDKAAC=`pwd`/../../fdk-aac/android/fdk-aac-0.1.5
if [ -z "$LIBFDKAAC" ]; then
echo "No define LIBFDKAAC before starting"
exit 1
fi

LIBX264=`pwd`/../../x264/android
if [ -z "$LIBX264" ]; then
echo "No define LIBX264 before starting"
exit 1
fi

SSL=`pwd`/../../openssl/android/OpenSSL-Vitamio
if [ -z "$SSL" ]; then
echo "No define SSL before starting"
exit 1
fi

#--------------------
# common defines
FF_ARCH=$1
if [ -z "$FF_ARCH" ]; then
    echo "You must specific an architecture 'armv7a, x86, ...'.\n"
    exit 1
fi

FF_BUILD_ROOT=`pwd`
FF_ANDROID_PLATFORM=android-9
FF_GCC_VER=4.9
FF_GCC_64_VER=4.9



FF_BUILD_NAME=
FF_SOURCE=
FF_CROSS_PREFIX=

FF_CFG_FLAGS=

FF_EXTRA_CFLAGS=
FF_EXTRA_LDFLAGS=
FF_ASM_OBJ_DIR=

FF_DEP_LIBS=

#----- armv7a begin -----
if [ "$FF_ARCH" == "armv7a" ]; then
    FF_BUILD_NAME=ffmpeg-armv7a
    FF_SOURCE=$FF_BUILD_ROOT/$FF_BUILD_NAME

    FF_CROSS_PREFIX=arm-linux-androideabi
    FF_TOOLCHAIN_NAME=${FF_CROSS_PREFIX}-${FF_GCC_VER}

    FF_CFG_FLAGS="$FF_CFG_FLAGS --arch=arm --cpu=cortex-a8"
    FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-neon"
    FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-thumb"

    FF_EXTRA_CFLAGS="$FF_EXTRA_CFLAGS -march=armv7-a -mcpu=cortex-a8 -mfpu=vfpv3-d16 -mfloat-abi=softfp -mthumb -I$LIBFDKAAC/include -I$LIBX264/build/x264-armv7/output/include -I$SSL/include"
    FF_EXTRA_LDFLAGS="$FF_EXTRA_LDFLAGS -Wl,--fix-cortex-a8 -L$LIBFDKAAC/libs/armeabi-v7a -L$LIBX264/build/x264-armv7/output/lib -L$SSL/libs/armeabi-v7a"

    FF_ASM_OBJ_DIR="libavutil/arm/*.o libavcodec/arm/*.o libswresample/arm/*.o libswscale/arm/*.o"

    LIBFDKAAC_OBJS=`find $LIBFDKAAC/obj/local/armeabi-v7a/objs/FraunhoferAAC -type f -name "*.o"`
    LIBX264_OBJS=`find $LIBX264/x264-armv7 -type f -name "*.o"`
    SSL_OBJS=`find $SSL/obj/local/armeabi-v7a/objs/ssl $SSL/obj/local/armeabi-v7a/objs/crypto -type f -name "*.o"`
elif [ "$FF_ARCH" == "armv5" ]; then
    FF_BUILD_NAME=ffmpeg-armv5
    FF_SOURCE=$FF_BUILD_ROOT/$FF_BUILD_NAME

    FF_CROSS_PREFIX=arm-linux-androideabi
    FF_TOOLCHAIN_NAME=${FF_CROSS_PREFIX}-${FF_GCC_VER}

    FF_CFG_FLAGS="$FF_CFG_FLAGS --arch=arm"

    FF_EXTRA_CFLAGS="$FF_EXTRA_CFLAGS -march=armv5te -mtune=arm9tdmi -msoft-float"
    FF_EXTRA_LDFLAGS="$FF_EXTRA_LDFLAGS"

    FF_ASM_OBJ_DIR="libavutil/arm/*.o libavcodec/arm/*.o libswresample/arm/*.o libswscale/arm/*.o"

elif [ "$FF_ARCH" == "x86" ]; then
    FF_BUILD_NAME=ffmpeg-x86
    FF_SOURCE=$FF_BUILD_ROOT/$FF_BUILD_NAME

    FF_CROSS_PREFIX=i686-linux-android
    FF_TOOLCHAIN_NAME=x86-${FF_GCC_VER}

    FF_CFG_FLAGS="$FF_CFG_FLAGS --arch=x86 --cpu=i686 --enable-yasm"

    FF_EXTRA_CFLAGS="$FF_EXTRA_CFLAGS -march=atom -msse3 -ffast-math -mfpmath=sse -I$LIBFDKAAC/include -I$LIBX264/build/x264-x86/output/include -I$SSL/include"
    FF_EXTRA_LDFLAGS="$FF_EXTRA_LDFLAGS -L$LIBFDKAAC/libs/x86 -L$LIBX264/build/x264-x86/output/lib -L$SSL/libs/x86"

    FF_ASM_OBJ_DIR="libavutil/x86/*.o libavcodec/x86/*.o libswresample/x86/*.o libavfilter/x86/*.o libswscale/x86/*.o"

    LIBFDKAAC_OBJS=`find $LIBFDKAAC/obj/local/x86/objs/FraunhoferAAC -type f -name "*.o"`
    LIBX264_OBJS=`find $LIBX264/x264-x86 -type f -name "*.o"`
    SSL_OBJS=`find $SSL/obj/local/x86/objs/ssl $SSL/obj/local/x86/objs/crypto -type f -name "*.o"`
elif [ "$FF_ARCH" == "arm64-v8a" ]; then
    FF_ANDROID_PLATFORM=android-21

    FF_BUILD_NAME=ffmpeg-arm64-v8a
    FF_SOURCE=$FF_BUILD_ROOT/$FF_BUILD_NAME

    FF_CROSS_PREFIX=aarch64-linux-android
    FF_TOOLCHAIN_NAME=${FF_CROSS_PREFIX}-${FF_GCC_64_VER}

    FF_CFG_FLAGS="$FF_CFG_FLAGS --arch=aarch64 --enable-yasm"

    FF_EXTRA_CFLAGS="$FF_EXTRA_CFLAGS -I$LIBFDKAAC/include -I$LIBX264/build/x264-arm64/output/include -I$SSL/include"
    FF_EXTRA_LDFLAGS="$FF_EXTRA_LDFLAGS -L$LIBFDKAAC/libs/arm64-v8a -L$LIBX264/build/x264-arm64/output/lib -L$SSL/libs/arm64-v8a"

    FF_ASM_OBJ_DIR="libavutil/aarch64/*.o libavcodec/aarch64/*.o libavcodec/neon/*.o libswresample/aarch64/*.o libswscale/aarch64/*.o"
    LIBFDKAAC_OBJS=`find $LIBFDKAAC/obj/local/arm64-v8a/objs/FraunhoferAAC -type f -name "*.o"`
    LIBX264_OBJS=`find $LIBX264/x264-arm64 -type f -name "*.o"`
    SSL_OBJS=`find $SSL/obj/local/arm64-v8a/objs/ssl $SSL/obj/local/arm64-v8a/objs/crypto -type f -name "*.o"`
else
    echo "unknown architecture $FF_ARCH";
    exit 1
fi

FF_TOOLCHAIN_PATH=$FF_BUILD_ROOT/build/$FF_BUILD_NAME/toolchain

FF_SYSROOT=$FF_TOOLCHAIN_PATH/sysroot
FF_PREFIX=$FF_BUILD_ROOT/build/$FF_BUILD_NAME/output

mkdir -p $FF_PREFIX
mkdir -p $FF_SYSROOT

#--------------------
echo "\n--------------------"
echo "[*] make NDK standalone toolchain"
echo "--------------------"
UNAMES=$(uname -s)
UNAMESM=$(uname -sm)
echo "build on $UNAMESM"
FF_MAKE_TOOLCHAIN_FLAGS="--install-dir=$FF_TOOLCHAIN_PATH"
if [ "$UNAMES" == "Darwin" ]; then
    FF_MAKE_TOOLCHAIN_FLAGS="$FF_MAKE_TOOLCHAIN_FLAGS --system=darwin-x86_64"
fi

FF_MAKE_FLAGS=
if which nproc >/dev/null
then
    FF_MAKE_FLAGS=-j`nproc`
elif [ "$UNAMES" == "Darwin" ] && which sysctl >/dev/null
then
    FF_MAKE_FLAGS=-j`sysctl -n machdep.cpu.thread_count`
fi

FF_TOOLCHAIN_TOUCH="$FF_TOOLCHAIN_PATH/touch"
if [ ! -f "$FF_TOOLCHAIN_TOUCH" ]; then
    $ANDROID_NDK/build/tools/make-standalone-toolchain.sh \
        $FF_MAKE_TOOLCHAIN_FLAGS \
        --platform=$FF_ANDROID_PLATFORM \
        --toolchain=$FF_TOOLCHAIN_NAME
    touch $FF_TOOLCHAIN_TOUCH;
fi


#--------------------
echo "\n--------------------"
echo "[*] check ffmpeg env"
echo "--------------------"
export PATH=$FF_TOOLCHAIN_PATH/bin:$PATH
export CC="${FF_CROSS_PREFIX}-gcc"
export LD=${FF_CROSS_PREFIX}-ld
export AR=${FF_CROSS_PREFIX}-ar
export STRIP=${FF_CROSS_PREFIX}-strip

FF_CFLAGS="-O3 -Wall -pipe \
    -std=c99 \
    -ffast-math \
    -fstrict-aliasing -Werror=strict-aliasing \
    -Wno-psabi -Wa,--noexecstack \
    -DANDROID -DNDEBUG"

# cause av_strlcpy crash with gcc4.7, gcc4.8
# -fmodulo-sched -fmodulo-sched-allow-regmoves

# --enable-thumb is OK
#FF_CFLAGS="$FF_CFLAGS -mthumb"

# not necessary
#FF_CFLAGS="$FF_CFLAGS -finline-limit=300"

export COMMON_FF_CFG_FLAGS=
source $FF_BUILD_ROOT/../config/module.sh


FF_CFG_FLAGS="$FF_CFG_FLAGS $COMMON_FF_CFG_FLAGS"

#--------------------
# Standard options:
FF_CFG_FLAGS="$FF_CFG_FLAGS --prefix=$FF_PREFIX"

# Advanced options (experts only):
FF_CFG_FLAGS="$FF_CFG_FLAGS --cross-prefix=${FF_CROSS_PREFIX}-"
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-cross-compile"
FF_CFG_FLAGS="$FF_CFG_FLAGS --target-os=linux"
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-pic"
# FF_CFG_FLAGS="$FF_CFG_FLAGS --disable-symver"

# Optimization options (experts only):
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-asm"
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-inline-asm"

# Support MediaCodec
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-hwaccel=h264_mediacodec"
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-hwaccel=hevc_mediacodec"
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-hwaccel=mpeg4_mediacodec"
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-hwaccel=mpeg2_mediacodec"
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-decoder=h264_mediacodec"
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-decoder=hevc_mediacodec"
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-decoder=mpeg4_mediacodec"
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-decoder=mpeg2_mediacodec"
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-jni"
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-mediacodec"

# Support x264 Encoder
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-libx264"
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-encoder=libx264"

# Support fdk-aac Encoder
FF_CFG_FLAGS="$FF_CFG_FLAGS --enable-encoder=libfdk_aac"

#--------------------
echo "\n--------------------"
echo "[*] configurate ffmpeg"
echo "--------------------"
cd $FF_SOURCE
if [ -f "./config.h" ]; then
    echo 'reuse configure'
else
    ./configure $FF_CFG_FLAGS \
        --extra-cflags="$FF_CFLAGS $FF_EXTRA_CFLAGS" \
        --extra-ldflags="$FF_DEP_LIBS $FF_EXTRA_LDFLAGS"
    make clean
fi

#--------------------
echo "\n--------------------"
echo "[*] compile ffmpeg"
echo "--------------------"
cp config.* $FF_PREFIX
make $FF_MAKE_FLAGS
make install

#--------------------
echo "\n--------------------"
echo "[*] link ffmpeg"
echo "--------------------"
echo $FF_EXTRA_LDFLAGS
$CC -lm -lz -shared --sysroot=$FF_SYSROOT -Wl,--no-undefined -Wl,-z,noexecstack $FF_EXTRA_LDFLAGS \
    compat/*.o \
    libavutil/*.o \
    libavcodec/*.o \
    libavfilter/*.o \
    libavformat/*.o \
    libswresample/*.o \
    libswscale/*.o \
    $FF_ASM_OBJ_DIR \
    $FF_DEP_LIBS \
    $LIBFDKAAC_OBJS \
    $SSL_OBJS \
    $LIBX264_OBJS \
    -Wl,-soname,libffmpeg_ypp.so -o $FF_PREFIX/libffmpeg_ypp.so

# $LIBX264_OBJS \

make clean
cd $FF_BUILD_ROOT
echo "\n--------------------\n"
