#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

echo "== clean source =="
if [ -d "OpenSSL-Vitamio" ]; then
rm -rf OpenSSL-Vitamio
fi

echo "== pull openssl base =="
git clone http://gitlab/lingkaishi/OpenSSL-Vitamio.git OpenSSL-Vitamio
