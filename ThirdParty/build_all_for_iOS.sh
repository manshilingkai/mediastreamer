#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

cd x264/
./pull-source.sh
./init-ios.sh
cd ios/
./compile-x264.sh all
cd ../../

cd ffmpeg/
./pull-source.sh
./init-ios.sh
cd ios/
./compile-ffmpeg.sh all
cd ../../

cd faac/
./pull-source.sh
./init-ios.sh
cd ios/
./compile-faac.sh all
cd ../../
