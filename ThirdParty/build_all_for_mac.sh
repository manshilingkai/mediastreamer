#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

cd ffmpeg_pc/
./pull-source.sh
./build-for-mac.sh
cd ..
