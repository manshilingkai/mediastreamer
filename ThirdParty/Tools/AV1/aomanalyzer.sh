#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

# 1.Download aomanalyzer
git clone https://github.com/xiph/aomanalyzer

# 2.install npm
if ! [ -x "$(command -v npm)" ]; then
    brew install npm
fi

cd aomanalyzer

npm install && npm run build-release
# 3.Install electron 
npm install electron --save-dev

# 4.Download the AV1 decoder https://github.com/xiph/aomanalyzer/issues/80
wget https://github.com/xiph/aomanalyzer/files/4685593/inspect.wasm.gz
wget https://github.com/xiph/aomanalyzer/files/4685594/inspect.js.gz

# unzip
gunzip inspect.wasm.gz
gunzip inspect.js.gz
