//
//  GPUImageMirrorFilter.h
//  GPUImage
//
//  Created by slklovewyy on 2020/2/27.
//  Copyright © 2020 Brad Larson. All rights reserved.
//

#import "GPUImageFilter.h"

@interface GPUImageMirrorFilter : GPUImageFilter {
}

@property (nonatomic) BOOL verticalMirror;
@property (nonatomic) BOOL horizontalMirror;

@end
