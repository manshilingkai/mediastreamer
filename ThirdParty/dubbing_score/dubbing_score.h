#pragma once
#include <iostream>
#include <vector>
#include "kiss_fft.h"

enum ErrorCode
{
	NoError,
	NotSupportedSampleRate, // 不支持的采样率
	NoEnoughMem,  // 内存不足
};

class dubbing_score {
public:
	dubbing_score(int channelCnt, int sampleRate);
	~dubbing_score();
	bool init(int& errorCode);
	int getFrameSize();
	void prepareFrameScore(char* src, char* ref, int sampleCnt);
	int getDubbingScore();

private:
	void getFramePitch(char* buff, int sampleCnt, std::vector<int>& pitchVector);

	int mChannelCnt;
	int mSampleRate;
	int mFFTSize = 0;
	kiss_fft_cfg cfg_fft = nullptr;
	kiss_fft_cpx* buff_fft = nullptr;
	std::vector<int> srcFramePitch;
	std::vector<int> refFramePitch;
};
