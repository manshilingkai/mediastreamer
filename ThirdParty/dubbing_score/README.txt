
step 1: 构造实例
DubbingScore(int channelCnt, int sampleRate)

参数：
声道数
采样率


step 2: 相似度计算初始化
bool init(int& errorCode)

参数：
错误码变量引用

返回值：
成功 返回true;
失败 返回false, 并通过errorCode引用返回errorCode


step 3: 获取每帧需要的数据大小（sample个数，与声道数无关）
int getFrameSize()

返回值：
每帧sample个数


step 4: 传递数据帧（每次调用传递一帧数据）
void prepareFrameScore(char* src, char* ref, int sampleCnt)

参数：
src  配音合成后buffer数据
ref  原视频中音轨buffer数据
sampleCnt  sample个数


step 5: 获取配音分数（注：考虑娱乐交互性，分数被限制在[60，100]区间）
int getDubbingScore()

返回值：
配音分数