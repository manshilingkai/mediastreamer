#include <iostream>
#include "dubbing_score.h"

int main(int argc, char**argv)
{
	DubbingScore* scObj = new DubbingScore(1, 48000);
	int error_code;
	scObj->init(error_code);

	int frameSize = 0;
	if (error_code == NoError)
		frameSize = scObj->getFrameSize();
	else
		return -1;

	short* srcBuff = new short[frameSize];
	short* refBuff = new short[frameSize];

	for (int i = 0; i < frameSize; i++)
	{
		srcBuff[i] = 100 + i * 2;
		refBuff[i] = 300 + i * 4;
	}
	scObj->prepareFrameScore((char*)srcBuff, (char*)refBuff, frameSize);

	for (int i = 0; i < frameSize; i++)
	{
		srcBuff[i] = 100 + i * 2;
		refBuff[i] = 300 + i * 4;
	}
	scObj->prepareFrameScore((char*)srcBuff, (char*)refBuff, frameSize);

	for (int i = 0; i < frameSize; i++)
	{
		srcBuff[i] = 100 + i * 2;
		refBuff[i] = 300 + i * 4;
	}
	scObj->prepareFrameScore((char*)srcBuff, (char*)refBuff, frameSize);

	for (int i = 0; i < frameSize; i++)
	{
		srcBuff[i] = 100 + i * 2;
		refBuff[i] = 300 + i * 4;
	}
	scObj->prepareFrameScore((char*)srcBuff, (char*)refBuff, frameSize);

	int score = scObj->getDubbingScore();
	
	delete scObj;
	return 0;
}