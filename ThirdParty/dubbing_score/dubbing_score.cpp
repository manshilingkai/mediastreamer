/*
Copyright(C) Qiu Mingjian
All rights reserved.

Audio Similarity based on cosine of base frequency element.
*/

#include "dubbing_score.h"
#include <math.h>

#define MIN(a,b)  (a)>(b)?(b):(a)
#define CPX_POW(a)  (a.r*a.r + a.i*a.i)
#define MAX_PITCH_SIZE  10000
#define MIN_SAMPLE_LIMIT  40

dubbing_score::dubbing_score(int channelCnt, int sampleRate)
{
	mChannelCnt = channelCnt;
	mSampleRate = sampleRate;
}

dubbing_score::~dubbing_score()
{
	if (cfg_fft != nullptr)
		free(cfg_fft);
	if (buff_fft != nullptr)
		delete[] buff_fft;
}

bool dubbing_score::init(int& errorCode)
{
	switch (mSampleRate)
	{
	case 48000:
	case 44100:
	{
		errorCode = NoError;
		mFFTSize = 4096;
		break;
	}
	case 32000:
	{
		errorCode = NoError;
		mFFTSize = 2048;
		break;
	}
	case 16000:
	case 8000:
	{
		errorCode = NoError;
		mFFTSize = 1024;
		break;
	}
	default:
	{
		errorCode = NotSupportedSampleRate;
		mFFTSize = 0;
		return false;
	}
	}
	if (mFFTSize != 0)
	{
		cfg_fft = kiss_fft_alloc(mFFTSize, 0, NULL, NULL);
		if (cfg_fft == nullptr)
		{
			errorCode = NoEnoughMem;
			return false;
		}

		buff_fft = new kiss_fft_cpx[mFFTSize];
		if (buff_fft == nullptr)
		{
			errorCode = NoEnoughMem;
			return false;
		}
		memset(buff_fft, 0, sizeof(kiss_fft_cpx)*mFFTSize);
	}
	return true;
}

int dubbing_score::getFrameSize()
{
	return mFFTSize;
}

void dubbing_score::getFramePitch(char* buff, int sampleCnt, std::vector<int>& pitchVector)
{
	int copySize = MIN(mFFTSize, sampleCnt);
	int zeroSamplCnt = 0;

	for (int index = 0; index < copySize; index++)
	{
		short sample = *((short*)buff + index * mChannelCnt);
		buff_fft[index].r = sample / 32768.0;
		buff_fft[index].i = 0;
		if ((sample <= MIN_SAMPLE_LIMIT) && (sample >= -MIN_SAMPLE_LIMIT))
			zeroSamplCnt++;
	}
	if (zeroSamplCnt >= mFFTSize / 4)
	{
		memset(buff_fft, 0, sizeof(kiss_fft_cpx)*mFFTSize);
		return;
	}

	kiss_fft(cfg_fft, buff_fft, buff_fft);
//	for (int i = 0; i < mFFTSize; i++)
//	{
//		std::cout << buff_fft[i].r << " + " << buff_fft[i].i << std::endl;
//	}
	int maxIndex = 1;
	float freqMaxPower = CPX_POW(buff_fft[1]);
	for (int index = 2; index < mFFTSize / 2; index++)
	{
		float freqPower = CPX_POW(buff_fft[index]);
		if (freqPower > freqMaxPower)
		{
			freqMaxPower = freqPower;
			maxIndex = index;
		}
	}
	pitchVector.push_back(maxIndex);
	memset(buff_fft, 0, sizeof(kiss_fft_cpx)*mFFTSize);

	return;
}

void dubbing_score::prepareFrameScore(char* src, char* ref, int sampleCnt)
{
	getFramePitch(src, sampleCnt, srcFramePitch);
	getFramePitch(ref, sampleCnt, refFramePitch);
}

int dubbing_score::getDubbingScore()
{
	int pitchSize = MIN(srcFramePitch.size(), refFramePitch.size());
	pitchSize = MIN(pitchSize, MAX_PITCH_SIZE);
	if (pitchSize == 0)
		return 0;

	double srcPower = 0.0;
	double refPower = 0.0;
	double inner_prod = 0.0;
	double cos_similarity = 0.0;
	for (int index = 0; index < pitchSize; index++)
	{
		srcPower += srcFramePitch[index] * srcFramePitch[index];
		refPower += refFramePitch[index] * refFramePitch[index];
		inner_prod += srcFramePitch[index] * refFramePitch[index];
	}
	cos_similarity = inner_prod / sqrt(srcPower) / sqrt(refPower);
	if (cos_similarity > 0.7)
	{
		return cos_similarity * 1000;
	}
	else
	{
		return 600 + cos_similarity / 0.7*(700 - 600);
	}
}
