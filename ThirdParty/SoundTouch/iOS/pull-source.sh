#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

echo "== clean source =="
if [ -d "soundtouch" ]; then
rm -rf soundtouch
fi

echo "== pull soundtouch base =="
git clone http://gitlab/lingkaishi/SoundTouch_iOS.git soundtouch
