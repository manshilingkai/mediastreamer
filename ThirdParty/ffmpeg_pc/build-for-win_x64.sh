#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

UNI_BUILD_ROOT=`pwd`

if [ -d "win_x64" ]; then
rm -rf win_x64
fi
mkdir win_x64
cp -rf ffmpeg win_x64/ffmpeg
cd win_x64/ffmpeg
./configure --prefix=$UNI_BUILD_ROOT/win_x64/output --toolchain=msvc --enable-shared --enable-gpl --enable-nonfree --disable-programs --disable-ffmpeg --disable-ffplay --disable-ffprobe --disable-doc --disable-htmlpages --disable-manpages --disable-podpages --disable-txtpages
make
make install
cd ../../
