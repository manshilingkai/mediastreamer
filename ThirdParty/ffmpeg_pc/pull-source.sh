#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

echo "== clean source =="
if [ -d "ffmpeg" ]; then
rm -rf ffmpeg
fi

echo "== pull ffmpeg base =="
git clone http://git.yupaopao.com/mobile_platform_common/ffmpeg.git ffmpeg
