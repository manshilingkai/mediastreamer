#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

UNI_BUILD_ROOT=`pwd`

if [ -d "win_x86" ]; then
rm -rf win_x86
fi
mkdir win_x86
cp -rf ffmpeg win_x86/ffmpeg
cd win_x86/ffmpeg
./configure --prefix=$UNI_BUILD_ROOT/win_x86/output --toolchain=msvc --enable-shared --enable-gpl --enable-nonfree --disable-programs --disable-ffmpeg --disable-ffplay --disable-ffprobe --disable-ffserver --disable-doc --disable-htmlpages --disable-manpages --disable-podpages --disable-txtpages --disable-avdevice --enable-avcodec --enable-avformat --enable-avutil --enable-swresample --enable-swscale --enable-postproc --enable-avfilter --disable-avresample --enable-network --disable-everything --disable-hwaccels --disable-encoders --disable-decoders --enable-decoder=aac --enable-decoder=aac_latm --enable-decoder=flv --enable-decoder=h264 --enable-decoder=mp3* --enable-decoder=mp2* --enable-decoder=mp1* --enable-decoder=mpeg4 --enable-decoder=wavpack --enable-decoder=text --enable-decoder=pcm_alaw --enable-decoder=hevc --enable-decoder=pcm_s16le --enable-decoder=flac --enable-decoder=ac3 --enable-decoder=ac3_at --enable-decoder=ac3_fixed --enable-decoder=eac3 --enable-decoder=eac3_at --disable-muxers --enable-muxer=mp4 --enable-muxer=mov --enable-muxer=flv --enable-muxer=h264 --enable-muxer=mp3 --disable-demuxers --enable-demuxer=aac --enable-demuxer=concat --enable-demuxer=data --enable-demuxer=flv --enable-demuxer=hls --enable-demuxer=live_flv --enable-demuxer=m4v --enable-demuxer=mov --enable-demuxer=mp3 --enable-demuxer=mpegps --enable-demuxer=mpegts --enable-demuxer=mpegvideo --enable-demuxer=wav --enable-demuxer=hevc --enable-demuxer=flac --enable-demuxer=image2 --disable-parsers --enable-parser=aac --enable-parser=aac_latm --enable-parser=h264 --enable-parser=mpeg4video --enable-parser=mpegvideo --enable-parser=mpegaudio --enable-parser=hevc --enable-parser=flac --disable-bsfs --enable-bsf=aac_adtstoasc --enable-bsf=h264_mp4toannexb --enable-bsf=hevc_mp4toannexb --enable-bsf=extract_extradata --enable-bsf=remove_extradata --enable-bsf=null --enable-protocols --disable-protocol=async --disable-protocol=bluray --enable-protocol=concat --disable-protocol=ffrtmpcrypt --enable-protocol=ffrtmphttp --enable-protocol=file --disable-protocol=ftp --disable-protocol=gopher --enable-protocol=hls --enable-protocol=http --disable-protocol=icecast --disable-protocol=librtmp* --disable-protocol=libsmbclient --disable-protocol=libssh --disable-protocol=mmsh --disable-protocol=mmst --disable-protocol=rtmp* --enable-protocol=rtmp --disable-protocol=rtp --disable-protocol=sctp --disable-protocol=srtp --enable-protocol=tcp --disable-protocol=udp --disable-protocol=udplite --disable-protocol=unix --disable-devices --disable-filters --disable-iconv --enable-pic --enable-rdft --enable-fft --enable-filter=pan --enable-filter=atempo --enable-filter=anull --enable-filter=aresample --enable-filter=asetrate --enable-filter=setpts --enable-filter=asetpts --enable-filter=volume --enable-filter=volumedetect --enable-filter=amix --enable-filter=null --enable-filter=rotate --enable-filter=vflip --enable-filter=hflip --enable-filter=transpose --enable-filter=crop --enable-filter=scale --enable-filter=movie --enable-filter=overlay --disable-debug
make -j4
make install
cd ../../
