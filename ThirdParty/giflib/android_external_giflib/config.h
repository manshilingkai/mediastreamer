
// giflib config.h

#ifndef GIF_CONFIG_H_DEFINED
#define GIF_CONFIG_H_DEFINED

#include <sys/types.h>
#include <stdint.h>
#include <fcntl.h>

#ifdef WIN32
#include "w32unistd.h"
#else
#include <unistd.h>
#endif

typedef uint32_t UINT32;

#endif
