#! /usr/bin/env bash
#
# Copyright (C) 2014-2015 William Shi <manshilingkai@gmail.com>
#
#

#----------
BUILD_ROOT=`pwd`
TARGET=$1
set -e
set +x

#----------
ALL_ARCHS_ANDROID_NDK="armv5 armv7 x86 mips arm64 x86_64 mips64"
ACT_ARCHS_ANDROID_NDK="armv7 x86 arm64"

echo_archs() {
    echo "===================="
    echo "[*] check archs"
    echo "===================="
    echo "ALL_ARCHS_ANDROID_NDK = $ALL_ARCHS_ANDROID_NDK"
    echo "ACT_ARCHS_ANDROID_NDK = $ACT_ARCHS_ANDROID_NDK"
    echo ""
}

#----------
case "$TARGET" in
    "")
        echo_archs
        sh tools/do-compile-faac.sh armv7
    ;;
    armv5|armv7|x86|mips|arm64|x86_64|mips64)
        echo_archs
        sh tools/do-compile-faac.sh $TARGET
    ;;
    all)
        echo_archs
        for ARCH in $ACT_ARCHS_ANDROID_NDK
        do
        sh tools/do-compile-faac.sh $ARCH
        done
        ;;
    clean)
        echo_archs
        for ARCH in $ACT_ARCHS_ANDROID_NDK
        do
            cd faac-$ARCH && git clean -xdf && cd -
        done
        rm -rf ./build
    ;;
    check)
        echo_archs
    ;;
    *)
        echo "Usage:"
        echo "  compile-faac.sh armv5|armv7|x86|mips|arm64|x86_64|mips64"
        echo "  compile-faac.sh all"
        echo "  compile-faac.sh clean"
        echo "  compile-faac.sh check"
        exit 1
    ;;
esac

#----------
echo "--------------------"
echo "[*] Finished"
echo "--------------------"
