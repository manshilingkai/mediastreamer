#! /usr/bin/env bash
#
# Copyright (C) 2014-2015 William Shi <manshilingkai@gmail.com>
#
#

set -e

#--------------------
ARCH=$1
FAAC_BUILD_ROOT=`pwd`
FAAC_SOURCE="faac-$ARCH"

if [ -z "$ANDROID_NDK" ]; then
echo "You must define ANDROID_NDK before starting."
echo "They must point to your NDK directories.\n"
exit 1
fi

# Detect OS
OS=`uname`
HOST_ARCH=`uname -m`
export CCACHE=; type ccache >/dev/null 2>&1 && export CCACHE=ccache
if [ $OS == 'Linux' ]; then
export HOST_SYSTEM=linux-$HOST_ARCH
elif [ $OS == 'Darwin' ]; then
export HOST_SYSTEM=darwin-$HOST_ARCH
fi

case $1 in
armv7)

$ANDROID_NDK/build/tools/make-standalone-toolchain.sh --install-dir=$FAAC_BUILD_ROOT/build/faac-$ARCH/toolchain --system=$HOST_SYSTEM --platform=android-16 --arch=arm --toolchain=arm-linux-androideabi-4.9

export PATH=$PATH:$FAAC_BUILD_ROOT/build/faac-$ARCH/toolchain/bin

cd $FAAC_BUILD_ROOT/$FAAC_SOURCE

PREFIX="$FAAC_BUILD_ROOT/build/faac-$ARCH/output"

CROSS_PREFIX=arm-linux-androideabi-

ANDROID_CFLAGS="-DANDROID -D__ARM_ARCH_7__ -D__ARM_ARCH_7A__ -ffunction-sections -funwind-tables -Wno-psabi -march=armv7-a -mfloat-abi=softfp -mfpu=vfpv3-d16 -fomit-frame-pointer -fstrict-aliasing -funswitch-loops -Wa,--noexecstack -Os "
ANDROID_CXXFLAGS="-DANDROID -D__ARM_ARCH_7__ -D__ARM_ARCH_7A__ -ffunction-sections -funwind-tables -Wno-psabi -march=armv7-a -mfloat-abi=softfp -mfpu=vfpv3-d16 -fno-exceptions -fno-rtti -mthumb -fomit-frame-pointer -fno-strict-aliasing -finline-limit=64 -Wa,--noexecstack -Os "

export AR=${CROSS_PREFIX}ar
export AS=${CROSS_PREFIX}gcc
export CC=${CROSS_PREFIX}gcc
export CXX=${CROSS_PREFIX}g++
export LD=${CROSS_PREFIX}ld
export NM=${CROSS_PREFIX}nm
export RANLIB=${CROSS_PREFIX}ranlib
export STRIP=${CROSS_PREFIX}strip
export CFLAGS=${ANDROID_CFLAGS}
export CXXFLAGS=${ANDROID_CXXFLAGS}
export CPPFLAGS=${ANDROID_CPPFLAGS}

./configure --host=arm-linux \
--prefix=$PREFIX \
--disable-dependency-tracking \
--disable-shared \
--enable-static \
--with-pic \
--without-mp4v2

make clean
make install

cd $FAAC_BUILD_ROOT

;;

arm64)

$ANDROID_NDK/build/tools/make-standalone-toolchain.sh --install-dir=$FAAC_BUILD_ROOT/build/faac-$ARCH/toolchain --system=$HOST_SYSTEM --platform=android-21 --arch=arm64 --toolchain=aarch64-linux-android-4.9

export PATH=$PATH:$FAAC_BUILD_ROOT/build/faac-$ARCH/toolchain/bin

cd $FAAC_BUILD_ROOT/$FAAC_SOURCE

PREFIX="$FAAC_BUILD_ROOT/build/faac-$ARCH/output"

CROSS_PREFIX=aarch64-linux-android-

ANDROID_CFLAGS=""
ANDROID_CXXFLAGS=""

export AR=${CROSS_PREFIX}ar
export AS=${CROSS_PREFIX}gcc
export CC=${CROSS_PREFIX}gcc
export CXX=${CROSS_PREFIX}g++
export LD=${CROSS_PREFIX}ld
export NM=${CROSS_PREFIX}nm
export RANLIB=${CROSS_PREFIX}ranlib
export STRIP=${CROSS_PREFIX}strip
export CFLAGS=${ANDROID_CFLAGS}
export CXXFLAGS=${ANDROID_CXXFLAGS}
export CPPFLAGS=${ANDROID_CPPFLAGS}

./configure --host=arm-linux \
--prefix=$PREFIX \
--disable-dependency-tracking \
--disable-shared \
--enable-static \
--with-pic \
--without-mp4v2

make clean
make install

cd $FAAC_BUILD_ROOT


;;

x86)

$ANDROID_NDK/build/tools/make-standalone-toolchain.sh --install-dir=$FAAC_BUILD_ROOT/build/faac-$ARCH/toolchain --system=$HOST_SYSTEM --platform=android-16 --arch=x86 --toolchain=x86-4.9

export PATH=$PATH:$FAAC_BUILD_ROOT/build/faac-$ARCH/toolchain/bin

cd $FAAC_BUILD_ROOT/$FAAC_SOURCE

PREFIX="$FAAC_BUILD_ROOT/build/faac-$ARCH/output"

CROSS_PREFIX=i686-linux-android-

ANDROID_CFLAGS=""
ANDROID_CXXFLAGS=""

export AR=${CROSS_PREFIX}ar
export AS=${CROSS_PREFIX}gcc
export CC=${CROSS_PREFIX}gcc
export CXX=${CROSS_PREFIX}g++
export LD=${CROSS_PREFIX}ld
export NM=${CROSS_PREFIX}nm
export RANLIB=${CROSS_PREFIX}ranlib
export STRIP=${CROSS_PREFIX}strip
export CFLAGS=${ANDROID_CFLAGS}
export CXXFLAGS=${ANDROID_CXXFLAGS}
export CPPFLAGS=${ANDROID_CPPFLAGS}

./configure --host=i686-linux \
--prefix=$PREFIX \
--disable-dependency-tracking \
--disable-shared \
--enable-static \
--with-pic \
--without-mp4v2

make clean
make install

cd $FAAC_BUILD_ROOT

;;
esac

