#! /usr/bin/env bash
#
# Copyright (C) 2014-2015 William Shi <manshilingkai@gmail.com>
#
#

set -e

echo "== clean source =="
if [ -d "extra" ]; then
rm -rf extra
fi

echo "== pull faac base =="
git clone git@gitlab.com:manshilingkai/faac.git extra/faac

echo "== pull gas-preprocessor base =="
git clone git@gitlab.com:manshilingkai/gas-preprocessor.git extra/gas-preprocessor
