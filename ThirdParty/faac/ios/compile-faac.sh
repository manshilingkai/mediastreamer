#! /usr/bin/env bash
#
# Copyright (C) 2014-2015 William Shi <manshilingkai@gmail.com>
#
#

set -e

#----------
ALL_ARCHS_IOS6_SDK="armv7 armv7s i386"
ALL_ARCHS_IOS7_SDK="armv7 armv7s arm64 x86_64"

ALL_ARCHS=$ALL_ARCHS_IOS7_SDK

#----------
BUILD_ROOT=`pwd`
TARGET=$1

#----------
LIBS="libfaac"

#----------
echo_archs() {
    echo "===================="
    echo "[*] check xcode version"
    echo "===================="
    echo "ALL_ARCHS = $ALL_ARCHS"
}

do_lipo () {
    LIB_FILE=$1
    LIPO_FLAGS=
    for ARCH in $ALL_ARCHS
    do
        LIPO_FLAGS="$LIPO_FLAGS $BUILD_ROOT/build/faac-$ARCH/output/lib/$LIB_FILE"
    done

    xcrun lipo -create $LIPO_FLAGS -output $BUILD_ROOT/build/universal/lib/$LIB_FILE
    xcrun lipo -info $BUILD_ROOT/build/universal/lib/$LIB_FILE
}

do_lipo_all () {
    mkdir -p $BUILD_ROOT/build/universal/lib
    echo "lipo archs: $ALL_ARCHS"
    for LIB in $LIBS
    do
        do_lipo "$LIB.a";
    done

    cp -R $BUILD_ROOT/build/faac-armv7/output/include $BUILD_ROOT/build/universal/
}

#----------
if [ "$TARGET" = "armv7" -o "$TARGET" = "armv7s" -o "$TARGET" = "arm64" ]; then
    echo_archs
    sh tools/do-compile-faac.sh $TARGET
elif [ "$TARGET" = "i386" -o "$TARGET" = "x86_64" ]; then
    echo_archs
    sh tools/do-compile-faac.sh $TARGET
elif [ "$TARGET" = "lipo" ]; then
    echo_archs
    do_lipo_all
elif [ "$TARGET" = "all" ]; then
    echo_archs
    for ARCH in $ALL_ARCHS
    do
        sh tools/do-compile-faac.sh $ARCH
    done

    do_lipo_all
elif [ "$TARGET" = "check" ]; then
    echo_archs
elif [ "$TARGET" = "clean" ]; then
    echo_archs
    for ARCH in $ALL_ARCHS
    do
        cd faac-$ARCH && git clean -xdf && cd -
    done
else
    echo "Usage:"
    echo "  compile-faac.sh armv7|armv7s|arm64|i386|x86_64"
    echo "  compile-faac.sh lipo"
    echo "  compile-faac.sh all"
    echo "  compile-faac.sh clean"
    echo "  compile-faac.sh check"
    exit 1
fi
