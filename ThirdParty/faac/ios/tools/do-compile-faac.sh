#! /usr/bin/env bash
#
# Copyright (C) 2014-2015 William Shi <manshilingkai@gmail.com>
#
#

set -e

#--------------------
ARCH=$1
FAAC_BUILD_ROOT=`pwd`
FAAC_SOURCE="faac-$ARCH"

#--------------------
SDKVERSION=`xcrun -sdk iphoneos --show-sdk-version`
DEVELOPER=`xcode-select -print-path`

CONFIGURE_FLAGS="--enable-static --with-pic"

# the one included in faac does not work; specify full path to working one
GAS_PREPROCESSOR=$FAAC_BUILD_ROOT/../extra/gas-preprocessor/gas-preprocessor.pl

#--------------------
cd $FAAC_BUILD_ROOT/$FAAC_SOURCE
if [ "$ARCH" = "i386" -o "$ARCH" = "x86_64" ]
then
    PLATFORM="iPhoneSimulator"
    CPU=
    if [ "$ARCH" = "x86_64" ]
    then
        SIMULATOR="-mios-simulator-version-min=7.0"
        HOST="--host=x86_64-apple-darwin"
    else
        SIMULATOR="-mios-simulator-version-min=5.0"
        HOST="--host=i386-apple-darwin"
    fi
else
    PLATFORM="iPhoneOS"
    if [ $ARCH = "armv7s" ]
    then
        CPU="--cpu=swift"
    else
        CPU=
    fi
    SIMULATOR=
    HOST="--host=arm-apple-darwin"
fi

XCRUN_SDK=`echo $PLATFORM | tr '[:upper:]' '[:lower:]'`
XCRUN_CC=`xcrun -sdk $XCRUN_SDK -find clang`
CC="$XCRUN_CC -Wno-error=unused-command-line-argument-hard-error-in-future -arch $ARCH"
AS="$GAS_PREPROCESSOR $CC"
EXTRA_CFLAGS="-isysroot ${DEVELOPER}/Platforms/$PLATFORM.platform/Developer/SDKs/$PLATFORM${SDKVERSION}.sdk"
CFLAGS="-arch $ARCH $SIMULATOR"
CFLAGS="$CFLAGS $EXTRA_CFLAGS -fembed-bitcode"
CXXFLAGS="$CFLAGS"
LDFLAGS="$CFLAGS"

CC=$CC CFLAGS=$CXXFLAGS LDFLAGS=$LDFLAGS CPPFLAGS=$CXXFLAGS CXX=$CC CXXFLAGS=$CXXFLAGS $FAAC_BUILD_ROOT/$FAAC_SOURCE/configure \
$CONFIGURE_FLAGS \
$HOST \
--prefix="$FAAC_BUILD_ROOT/build/faac-$ARCH/output" \
--disable-shared \
--without-mp4v2

make clean && make && make install-strip

cd $FAAC_BUILD_ROOT


