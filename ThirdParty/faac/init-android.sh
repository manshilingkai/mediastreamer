#! /usr/bin/env bash
#
# Copyright (C) 2014-2015 William Shi <manshilingkai@gmail.com>
#
#

set -e

function pull_fork()
{
    echo "== pull faac fork $1 for android =="
    rm -rf android/faac-$1
    cp -rf extra/faac android/faac-$1
}

pull_fork "armv7"
pull_fork "x86"
pull_fork "arm64"
#pull_fork "mips"
#pull_fork "mips64"
#pull_fork "x86_64"




