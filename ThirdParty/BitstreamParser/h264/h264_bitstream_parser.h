/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#ifndef COMMON_VIDEO_H264_H264_BITSTREAM_PARSER_H_
#define COMMON_VIDEO_H264_H264_BITSTREAM_PARSER_H_
#include <stddef.h>
#include <stdint.h>

#include "absl/types/optional.h"
#include "common_video/h264/pps_parser.h"
#include "common_video/h264/sps_parser.h"
#include "common_video/h264/sei_parser.h"
#include "common_video/h264/h264_common.h"

namespace webrtc {

const int kSeiMaxPayloadSize = 4096; // reserve 4k size for sei use
const int kSVCBaseNRI = 0x10; 
const int kSVCEnhance1NRI = 0x00;

// Stateful H264 bitstream parser (due to SPS/PPS). Used to parse out QP values
// from the bitstream.
// TODO(pbos): Unify with RTP SPS parsing and only use one H264 parser.
// TODO(pbos): If/when this gets used on the receiver side CHECKs must be
// removed and gracefully abort as we have no control over receive-side
// bitstreams.
class H264BitstreamParser {
 public:
  enum Result {
    kOk,
    kInvalidStream,
    kUnsupportedStream,
  };

  enum TemporalType {
    kNormal,
    kIdr,
    kSVCBase,
    kSVCEnhance1,
  };

  H264BitstreamParser();
  virtual ~H264BitstreamParser();

  // Parse an additional chunk of H264 bitstream.
  void ParseBitstream(const uint8_t* bitstream, size_t length, bool backup_spspps = false);

  // Get the last extracted QP value from the parsed bitstream.
  bool GetLastSliceQp(int* qp) const;

  absl::optional<SeiParser::SeiState>& ParsedSei() { return sei_; }

  // return true: bitstream size is enough for sei
  // return false: bitstream size is not enough for sei, realloc actually inside
  bool AppendSei(const std::vector<uint8_t> &sei,
                 uint32_t sei_type,
                 uint8_t** bitstream,
                 size_t* length,
                 size_t* size);
  
  TemporalType GetTemporalType();

  // Get the last Resolution from the parsed bitstream.
  bool GetLastResolution(int* w, int* h) const;

  bool isLastIdr(bool &hasSps, bool &hasPps) const;

  std::vector<H264::NaluIndex> GetLastNaluIndices() const;

  unsigned char* GetLastSps(int &sps_size);
  unsigned char* GetLastPps(int &pps_size);

 protected:
  bool ParseSlice(const uint8_t* slice, size_t length, SeiParser::SeiState &sei, const uint8_t* nal, size_t nal_size, bool backup_spspps);
  Result ParseNonParameterSetNalu(const uint8_t* source,
                                  size_t source_length,
                                  uint8_t nalu_type);

  // SPS/PPS state, updated when parsing new SPS/PPS, used to parse slices.
  absl::optional<SpsParser::SpsState> sps_;
  absl::optional<PpsParser::PpsState> pps_;
  absl::optional<SeiParser::SeiState> sei_;

  // Last parsed slice QP.
  absl::optional<int32_t> last_slice_qp_delta_;

  struct NAL_INFO {
    H264::NaluType nalu_type;
    uint8_t nir;
  };
  std::vector<NAL_INFO> nalus_info_; 

  bool isIdr = false;
  bool isSps = false;
  bool isPps = false;

  std::vector<H264::NaluIndex> nalu_indices;

  unsigned char* last_sps_data = NULL;
  int last_sps_size = 0;
  unsigned char* last_pps_data = NULL;
  int last_pps_size = 0;
};

}  // namespace webrtc

#endif  // COMMON_VIDEO_H264_H264_BITSTREAM_PARSER_H_
