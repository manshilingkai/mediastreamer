/* 
 * Created by Qijiyue in 2020-11
 * Copyright (c) 2020 NETEASE RTC. All rights reserved.
 */

#include "common_video/h264/sei_parser.h"

#include "common_video/h264/h264_common.h"


namespace webrtc {

SeiParser::SeiState::SeiState() = default;
SeiParser::SeiState::SeiState(const SeiState&) = default;
SeiParser::SeiState::~SeiState() = default;

bool SeiParser::ParseSei(const uint8_t* data, size_t length, SeiState &sei) {
  // EBSP to RBSP
  std::vector<uint8_t> unpacked_buffer = H264::ParseRbsp(data, length);
  RbspToSodb(unpacked_buffer);
  return InterpretSei(unpacked_buffer, sei);
}

void SeiParser::RbspToSodb(std::vector<uint8_t> &buffer) {
  int bitoffset = 0;
  // find trailing 1 bit
  int ctr_bit = (buffer.back() & (0x01 << bitoffset));

  while (ctr_bit == 0) {
    ++bitoffset;

    if (bitoffset == 8) {
      buffer.pop_back();
      bitoffset = 0;
    }

    ctr_bit = buffer.back() & (0x01 << bitoffset);
  }
}

bool SeiParser::InterpretSei(std::vector<uint8_t> &buffer, SeiState &sei) {
  const uint8_t* buf = buffer.data();
  const uint32_t sei_size = buffer.size();
  uint32_t payload_type = 0;
  uint32_t payload_size = 0;
  uint32_t offset = 0;
  uint8_t tmp_byte;
  bool valid_sei = false;
  do {
    tmp_byte = buf[offset++];
    payload_type = 0;

    while (offset < sei_size && tmp_byte == 0xFF) {
      payload_type += 255;
      tmp_byte = buf[offset++];
    }

    payload_type += tmp_byte;

    payload_size = 0;
    if (offset >= sei_size) return false;
    tmp_byte = buf[offset++];

    while (offset < sei_size && tmp_byte == 0xFF) {
      payload_size += 255;
      tmp_byte = buf[offset++];
    }

    payload_size += tmp_byte;

    switch (payload_type) {
      case NERTC_SEI_TYPE_EXTERNAL:
        sei.external_sei.assign(buf + offset, buf + offset + payload_size);
        valid_sei = true;
        break;
      case NERTC_SEI_TYPE_INTERNAL:
        sei.internal_sei.assign(buf + offset, buf + offset + payload_size);
        valid_sei = true;
        break;
    }
    offset += payload_size;
  } while (buf[offset] != 0x80);
  return valid_sei;
}

void SeiParser::GenerateSeiNalu(std::vector<uint8_t> &sei_nalu,
                                const std::vector<uint8_t> &sei_data,
                                uint32_t sei_type) {
  std::vector<uint8_t> raw_sei_info;

  // add sei type
  while (sei_type >= 255) {
    raw_sei_info.push_back(0xFF);
    sei_type -= 255;
  }

  raw_sei_info.push_back(static_cast<uint8_t>(sei_type));

  // add sei size
  uint32_t len_tmp = static_cast<uint32_t>(sei_data.size());
  while (len_tmp >= 255) {
    raw_sei_info.push_back(0xFF);
    len_tmp -= 255;
  }
  raw_sei_info.push_back(static_cast<uint8_t>(len_tmp));

  // add sei data
  raw_sei_info.insert(raw_sei_info.end(), sei_data.begin(), sei_data.end());

  // generate sei nalu
  uint32_t count = 0;
  for (size_t i = 0; i < raw_sei_info.size(); i++) {
    if (count == 2 && (raw_sei_info[i] < 0x04)) {
      sei_nalu.push_back(0x03);
      count = 0;
    }

    if (raw_sei_info[i] == 0) {
      ++count;
    } else {
      count = 0;
    }

    sei_nalu.push_back(raw_sei_info[i]);
  }
  sei_nalu.push_back(0x80);
}

}  // namespace webrtc