/* 
 * Created by Qijiyue in 2020-11
 * Copyright (c) 2020 NETEASE RTC. All rights reserved.
 */

#ifndef COMMON_VIDEO_H264_SEI_PARSER_H_
#define COMMON_VIDEO_H264_SEI_PARSER_H_

#include <vector>
#include "absl/types/optional.h"

namespace webrtc {

class SeiParser {
 public:
  enum {
    NERTC_SEI_TYPE_EXTERNAL = 110,
    NERTC_SEI_TYPE_INTERNAL = 111,
  };
  // The parsed state of the PPS.
  // Only some select user defined sei value are stored.
  // Add more as they are actually needed.
  struct SeiState {
   public:
    SeiState();
    ~SeiState();
    SeiState(const SeiState&);
    // //

    std::vector<uint8_t> external_sei;
    std::vector<uint8_t> internal_sei;
  };

  static bool ParseSei(const uint8_t* data, size_t length, SeiState &sei);
  static void RbspToSodb(std::vector<uint8_t> &buffer);
  static bool InterpretSei(std::vector<uint8_t> &buffer, SeiState &sei);
  static void GenerateSeiNalu(std::vector<uint8_t> &sei_nalu,
                              const std::vector<uint8_t> &sei_data,
                              uint32_t sei_type);
};

}  // namespace webrtc
#endif  // COMMON_VIDEO_H264_SEI_PARSER_H_