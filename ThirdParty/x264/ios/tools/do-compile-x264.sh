#! /usr/bin/env bash
#
# Copyright (C) 2014-2015 William Shi <manshilingkai@gmail.com>
#
#

set -e

#--------------------
ARCH=$1
X264_BUILD_ROOT=`pwd`
X264_SOURCE="x264-$ARCH"

#--------------------
SDKVERSION=`xcrun -sdk iphoneos --show-sdk-version`
DEVELOPER=`xcode-select -print-path`

CONFIGURE_FLAGS="--enable-static --enable-pic --disable-cli"

# the one included in x264 does not work; specify full path to working one
GAS_PREPROCESSOR=$X264_BUILD_ROOT/$X264_SOURCE/tools/gas-preprocessor.pl

#--------------------
cd $X264_BUILD_ROOT/$X264_SOURCE
if [ "$ARCH" = "i386" -o "$ARCH" = "x86_64" ]
then
    BITCODE="-fembed-bitcode"
    PLATFORM="iPhoneSimulator"
    CPU=
    if [ "$ARCH" = "x86_64" ]
    then
        SIMULATOR="-mios-simulator-version-min=7.0"
        HOST=
    else
        SIMULATOR="-mios-simulator-version-min=5.0"
        HOST="--host=i386-apple-darwin"
    fi
else
    BITCODE="-fembed-bitcode"
    PLATFORM="iPhoneOS"
    if [ $ARCH = "armv7s" ]
    then
        CPU="--cpu=swift"
    else
        CPU=
    fi
    SIMULATOR=
    if [ $ARCH = "arm64" ]
    then
        HOST="--host=aarch64-apple-darwin"
    else
        HOST="--host=arm-apple-darwin"
    fi
fi

XCRUN_SDK=`echo $PLATFORM | tr '[:upper:]' '[:lower:]'`
XCRUN_CC=`xcrun -sdk $XCRUN_SDK -find clang`
CC="$XCRUN_CC -Wno-error=unused-command-line-argument-hard-error-in-future -arch $ARCH"
EXTRA_CFLAGS="-isysroot ${DEVELOPER}/Platforms/$PLATFORM.platform/Developer/SDKs/$PLATFORM${SDKVERSION}.sdk"
CFLAGS="-arch $ARCH $SIMULATOR"
CFLAGS="$CFLAGS $EXTRA_CFLAGS $BITCODE"
CXXFLAGS="$CFLAGS"
LDFLAGS="$CFLAGS"

CC=$CC $X264_BUILD_ROOT/$X264_SOURCE/configure \
$CONFIGURE_FLAGS \
$HOST \
$CPU \
--extra-cflags="$CFLAGS" \
--extra-ldflags="$LDFLAGS" \
--prefix="$X264_BUILD_ROOT/build/x264-$ARCH/output" \
--disable-asm

make -j3 install
#make clean
cd $X264_BUILD_ROOT


