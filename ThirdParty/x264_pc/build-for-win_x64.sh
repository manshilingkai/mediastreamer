#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

UNI_BUILD_ROOT=`pwd`

if [ -d "win_x64" ]; then
rm -rf win_x64
fi
mkdir win_x64
cp -rf x264 win_x64/x264
cd win_x64/x264
CC=cl ./configure --enable-static --enable-strip --host=mingw64 --extra-cflags="-DNO_PREFIX" --prefix=$UNI_BUILD_ROOT/win_x64/output --disable-cli
make
make install
cd ../../
