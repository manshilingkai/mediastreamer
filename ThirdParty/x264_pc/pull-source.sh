#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

echo "== clean source =="
if [ -d "x264" ]; then
rm -rf x264
fi

echo "== pull x264 base =="
git clone http://git.yupaopao.com/mobile_platform_common/x264.git x264
