#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#
#

set -e

UNI_BUILD_ROOT=`pwd`

if [ -d "win_x86" ]; then
rm -rf win_x86
fi
mkdir win_x86
cp -rf x264 win_x86/x264
cd win_x86/x264
CC=cl ./configure --enable-static --enable-strip --host=mingw32 --extra-cflags="-DNO_PREFIX" --prefix=$UNI_BUILD_ROOT/win_x86/output --disable-cli
make
make install

cd ../../
