#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

UNI_BUILD_ROOT=`pwd`

if [ -d "mac" ]; then
rm -rf mac
fi
mkdir mac
cp -rf x264 mac/x264
cd mac/x264
./configure --prefix=$UNI_BUILD_ROOT/mac/output --enable-static --enable-pic --disable-cli
make
make install
cd ../../
