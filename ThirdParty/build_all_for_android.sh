#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

cd x264/
./pull-source.sh
./init-android.sh
cd android/
./compile-x264.sh all
cd ../../

cd ffmpeg/
./pull-source.sh
./init-android.sh
cd android/
./compile-ffmpeg.sh all
cd ../../

cd faac/
./pull-source.sh
./init-android.sh
cd android/
./compile-faac.sh all
cd ../../

cd ../android_mini
cd MediaStreamer
cd jni
ndk-build
cd ../../
