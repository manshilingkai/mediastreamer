#! /usr/bin/env bash
#
# Copyright (C) 2014-2015 William Shi <manshilingkai@gmail.com>
#
#

set -e

#--------------------
ARCH=$1
RTMP_BUILD_ROOT=`pwd`
RTMP_SOURCE="rtmp-$ARCH"

SSL=${RTMP_BUILD_ROOT}/../../../openssl/iOS/OpenSSL-for-iPhone
SSLINCLUDE=${SSL}/include
SSLLIBS=${SSL}/lib

#--------------------
SDKVERSION=`xcrun -sdk iphoneos --show-sdk-version`
DEVELOPER=`xcode-select -print-path`

#--------------------
if [ "$ARCH" = "i386" -o "$ARCH" = "x86_64" ]
then
    PLATFORM="iPhoneSimulator"
else
    PLATFORM="iPhoneOS"
fi

#--------------------
cd $RTMP_BUILD_ROOT/$RTMP_SOURCE/librtmp

export CROSS_COMPILE=/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/
export XCFLAGS="-isysroot ${DEVELOPER}/Platforms/$PLATFORM.platform/Developer/SDKs/$PLATFORM${SDKVERSION}.sdk -I${SSLINCLUDE} -arch $ARCH -fembed-bitcode"
export XLDFLAGS="-isysroot ${DEVELOPER}/Platforms/$PLATFORM.platform/Developer/SDKs/$PLATFORM${SDKVERSION}.sdk -L${SSLLIBS} -arch $ARCH -fembed-bitcode"

DIST=$RTMP_BUILD_ROOT/build/rtmp-$ARCH/output && mkdir -p ${DIST}
make SYS=darwin clean && make SYS=darwin librtmp.a && make SYS=darwin prefix=${DIST} install_base

cd $RTMP_BUILD_ROOT
