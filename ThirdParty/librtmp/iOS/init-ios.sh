#! /usr/bin/env bash
#
# Copyright (C) 2014-2015 William Shi <manshilingkai@gmail.com>
#
#

set -e

function pull_fork()
{
    echo "== pull rtmp fork $1 =="
    rm -rf workspace/rtmp-$1
    cp -rf extra/librtmp workspace/rtmp-$1
}

pull_fork "armv7"
pull_fork "armv7s"
pull_fork "arm64"
pull_fork "i386"
pull_fork "x86_64"
