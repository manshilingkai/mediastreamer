#! /usr/bin/env bash
#
# Copyright (C) 2014-2015 William Shi <manshilingkai@gmail.com>
#
#

set -e

echo "== clean source =="
if [ -d "extra" ]; then
rm -rf extra
fi

echo "== pull librtmp base =="
git clone http://gitlab/lingkaishi/librtmp_iOS.git extra/librtmp
