#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

echo "== clean source =="
if [ -d "librtmp" ]; then
rm -rf librtmp
fi

echo "== pull librtmp base =="
git clone http://gitlab/lingkaishi/librtmp_android.git librtmp
