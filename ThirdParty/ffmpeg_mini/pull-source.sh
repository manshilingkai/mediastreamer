#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

echo "== clean source =="
if [ -d "extra" ]; then
rm -rf extra
fi

echo "== pull ffmpeg base =="
git clone git@git.yupaopao.com:shilingkai/ffmpeg.git extra/ffmpeg

echo "== pull gas-preprocessor base =="
git clone git@git.yupaopao.com:shilingkai/gas-preprocessor.git extra/gas-preprocessor
