//
//  gifflen.h
//  MediaStreamer
//
//  Created by Think on 2018/9/5.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef gifflen_h
#define gifflen_h

#include <stdint.h>

extern "C"
{
    int giffle_Giffle_Init(const char* gifName, int w, int h, int numColors, int quality, int frameDelay);
    void giffle_Giffle_Close();
    int giffle_Giffle_AddFrame(const uint8_t * image);
};

#endif /* gifflen_h */
