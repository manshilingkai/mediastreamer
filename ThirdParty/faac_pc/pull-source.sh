#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

echo "== clean source =="
if [ -d "faac" ]; then
rm -rf faac
fi

echo "== pull faac base =="
git clone http://git.yupaopao.com/mobile_platform_common/faac.git faac
