#! /usr/bin/env bash
#
# Copyright (C) 2014-2016 William Shi <manshilingkai@gmail.com>
#
#

set -e

UNI_BUILD_ROOT=`pwd`

if [ -d "mac" ]; then
rm -rf mac
fi
mkdir mac
cp -rf faac mac/faac
cd mac/faac
./configure --prefix=$UNI_BUILD_ROOT/mac/output --enable-static --with-pic --disable-shared --without-mp4v2
make
make install
cd ../../
