#! /usr/bin/env bash
#
# Copyright (C) 2020-2021 William Shi <manshilingkai@gmail.com>
#
#

set -e

cd build/linux
cmake -G "Unix Makefiles" ../../source
make -j4
cd ../..

