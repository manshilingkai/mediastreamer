//
//  PerformanceMonitor.m
//  MediaStreamer
//
//  Created by Think on 2019/9/21.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "PerformanceMonitor.h"
#include <mach/mach.h>

@interface PerformanceMonitor () {}
@end

@implementation PerformanceMonitor
{
    dispatch_source_t timer;
    bool isTiming;
}

+ (CGFloat)cpuUsageForApp {
    kern_return_t           kr;
    thread_array_t          thread_list;
    mach_msg_type_number_t  thread_count;
    thread_info_data_t      thinfo;
    mach_msg_type_number_t  thread_info_count;
    thread_basic_info_t     basic_info_th;

    // 根据当前 task 获取所有线程
    kr = task_threads(mach_task_self(), &thread_list, &thread_count);
    if (kr != KERN_SUCCESS)
        return -1;

    float total_cpu_usage = 0;
    // 遍历所有线程
    for (int i = 0; i < thread_count; i++) {
        thread_info_count = THREAD_INFO_MAX;
        // 获取每一个线程信息
        kr = thread_info(thread_list[i], THREAD_BASIC_INFO, (thread_info_t)thinfo, &thread_info_count);
        if (kr != KERN_SUCCESS)
            return -1;

        basic_info_th = (thread_basic_info_t)thinfo;
        if (!(basic_info_th->flags & TH_FLAGS_IDLE)) {
            // cpu_usage : Scaled cpu usage percentage. The scale factor is TH_USAGE_SCALE.
            // 宏定义 TH_USAGE_SCALE 返回 CPU 处理总频率：
            total_cpu_usage += basic_info_th->cpu_usage / (float)TH_USAGE_SCALE;
        }
    }

    // 注意方法最后要调用 vm_deallocate，防止出现内存泄漏
    kr = vm_deallocate(mach_task_self(), (vm_offset_t)thread_list, thread_count * sizeof(thread_t));
    assert(kr == KERN_SUCCESS);

    return total_cpu_usage;
}

// 当前 app 内存使用量
+ (NSInteger)useMemoryForApp {
    task_vm_info_data_t vmInfo;
    mach_msg_type_number_t count = TASK_VM_INFO_COUNT;
    kern_return_t kernelReturn = task_info(mach_task_self(), TASK_VM_INFO, (task_info_t) &vmInfo, &count);
    if (kernelReturn == KERN_SUCCESS) {
        int64_t memoryUsageInByte = (int64_t) vmInfo.phys_footprint;
        return memoryUsageInByte / 1024 / 1024;
    } else {
        return -1;
    }
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        timer = nil;
        isTiming = false;
    }
    
    return self;
}

- (void)start
{
    if (isTiming) {
        NSLog(@"PerformanceMonitor has Started");
        return;
    }
    
    NSTimeInterval period = 0.5;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, period * NSEC_PER_SEC, 0.0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(timer, ^{
        
        CGFloat cpuUsage = [PerformanceMonitor cpuUsageForApp];
        NSInteger memoryUsage = [PerformanceMonitor useMemoryForApp];
        
        if (self.delegate!=nil) {
            if (([self.delegate respondsToSelector:@selector(onCpuUsage:)])) {
                [self.delegate onCpuUsage:cpuUsage];
            }
            if (([self.delegate respondsToSelector:@selector(onMemoryUsage:)])) {
                [self.delegate onMemoryUsage:memoryUsage];
            }
        }
    });
    dispatch_resume(timer);
    
    isTiming = true;
}

- (void)stop
{
    if (isTiming) {
        dispatch_source_cancel(timer);
        isTiming  = false;
    }
}

- (void)dealloc
{
    [self stop];
    
    NSLog(@"PerformanceMonitor dealloc");
}

@end
