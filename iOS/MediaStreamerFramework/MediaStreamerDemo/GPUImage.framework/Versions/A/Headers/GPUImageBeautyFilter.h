//
//  GPUImageBeautyFilter.h
//  GPUImage
//
//  Created by shilingkai on 16/8/19.
//  Copyright © 2016年 Brad Larson. All rights reserved.
//

#import "GPUImageFilter.h"

@interface GPUImageBeautyFilter : GPUImageFilter
{
    GLint mParamsLocation;
}

@property (readwrite, nonatomic) GPUVector4 beautyParam;

@end
