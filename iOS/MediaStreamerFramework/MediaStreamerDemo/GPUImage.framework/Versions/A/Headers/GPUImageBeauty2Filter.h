//
//  GPUImageBeauty2Filter.h
//  GPUImage
//
//  Created by PPTV on 16/8/31.
//  Copyright © 2016年 Brad Larson. All rights reserved.
//

#import "GPUImageFilter.h"

@interface GPUImageBeauty2Filter : GPUImageFilter
{
}

@property (nonatomic, assign) CGFloat beautyLevel;
@property (nonatomic, assign) CGFloat brightLevel;
@property (nonatomic, assign) CGFloat toneLevel;

@end