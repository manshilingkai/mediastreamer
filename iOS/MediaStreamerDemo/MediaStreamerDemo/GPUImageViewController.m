//
//  GPUImageViewController.m
//  MediaStreamerDemo
//
//  Created by shilingkai on 16/6/20.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "GPUImageViewController.h"

#import "GPUImage.h"
#import "SLKStreamer.h"

//#import "MainViewController.h"

@interface GPUImageViewController () <SLKStreamerDelegate> {
    BOOL audioEnabled;
    
    BOOL isPaused;
}

@property (nonatomic, strong) GPUImageVideoCamera   *videoCamera;
@property (nonatomic, strong) SLKStreamer           *slkStreamer;
@property (nonatomic, strong) SLKStreamerOptions    *slkStreamerOptions;
@property (nonatomic, strong) GPUImageView          *filteredVideoView;
@property (nonatomic, strong) GPUImageBeauty2Filter    *beautyFilter;
@property (nonatomic, strong) GPUImageBrightnessFilter *brightnessFilter;
@property (nonatomic, strong) GPUImageContrastFilter   *contrastFilter;
@property (nonatomic, strong) GPUImageSaturationFilter *saturationFilter;
@property (nonatomic, strong) GPUImageSharpenFilter    *sharpenFilter;

@property (nonatomic, strong) GPUImageFilterGroup   *filterGroup;

//Configuration
@property (nonatomic, assign) CGSize  sourceVideoSize;
@property (nonatomic, assign) CGSize  targetVideoSize;
@property (nonatomic, assign) NSUInteger expectedSourceVideoFrameRate;
@property (nonatomic, assign) NSUInteger averageBitRate;
@property (nonatomic, assign) NSUInteger maxKeyFrameIntervalMs;

@end

@implementation GPUImageViewController

+ (void)presentFromViewController:(UIViewController *)viewController URL:(NSURL *)url
{
    [viewController presentViewController:[[GPUImageViewController alloc] initWithURL:url] animated:YES completion:nil];
}

- (instancetype)initWithURL:(NSURL *)url {
    self = [self initWithNibName:@"GPUImageViewController" bundle:nil];
    if (self) {
        self.publishUrl = url;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"GPUImageViewController dealloc");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Init Configuration
    self.sourceVideoSize = CGSizeMake(720, 1280/*480,640*/);
    self.targetVideoSize = CGSizeMake(720, 1280/*480,640*/);
    self.expectedSourceVideoFrameRate = 20;
    self.averageBitRate = 1200;
    self.maxKeyFrameIntervalMs = 3000;
    
    audioEnabled = YES;
    
    isPaused = NO;
    
    [self setupSLKStreamer];
    [self setupGPUImage];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.filterGroup removeAllTargets];
    
    [self.videoCamera stopCameraCapture];
    [self.videoCamera removeInputsAndOutputs];
    [self.videoCamera removeAllTargets];

    [self.beautyFilter removeAllTargets];
    [self.saturationFilter removeAllTargets];
    [self.sharpenFilter removeAllTargets];
    
    [self.filteredVideoView removeFromSuperview];
    
    [self.slkStreamer stop];
    [self.slkStreamer terminate];
    
    NSLog(@"viewWillDisappear");
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    NSLog(@"viewDidDisappear");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)enableAudioAction:(id)sender
{
    if (audioEnabled) {
        audioEnabled = NO;
        [self.enableAudioButton setTitle:@"Audio Disabled" forState:UIControlStateNormal];
        [self.slkStreamer enableAudio:audioEnabled];
    }else{
        audioEnabled = YES;
        [self.enableAudioButton setTitle:@"Audio Enabled" forState:UIControlStateNormal];
        [self.slkStreamer enableAudio:audioEnabled];
    }
}

- (IBAction)switchCamera:(id)sender
{
    if (self.videoCamera!=nil) {
        [self.videoCamera rotateCamera];
    }
}

- (IBAction)pauseOrResume:(id)sender
{
    if (!isPaused) {
        [self.slkStreamer pause];
        isPaused = !isPaused;
        
        [self.pauseOrResumeButton setTitle:@"Resume" forState:UIControlStateNormal];
    }else{
        [self.slkStreamer resume];
        isPaused = !isPaused;
        
        [self.pauseOrResumeButton setTitle:@"Paused" forState:UIControlStateNormal];
    }
}

-(IBAction)sendAction:(id)sender
{
    [self.slkStreamer pushText:self.signalTextField.text];
    
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"ppy" ofType:@"MOV"];
//    NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
//
//    [MainViewController presentFromViewController:self URL:url];
}

- (IBAction)startAction:(id)sender
{
    if (self.slkStreamer) {
        [self.slkStreamer start];
    }
}

- (IBAction)stopAction:(id)sender
{
    if (self.slkStreamer) {
        [self.slkStreamer stop];
    }
}

- (void)setupGPUImage {
    //AVCaptureSessionPresetiFrame960x540
    self.videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset1280x720
                                                           cameraPosition:AVCaptureDevicePositionFront];
    self.videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    self.videoCamera.frameRate = (int32_t)[self expectedSourceVideoFrameRate];
//    self.videoCamera.horizontallyMirrorFrontFacingCamera = YES;
//    self.videoCamera.horizontallyMirrorRearFacingCamera = YES;
    
    self.filterGroup = [[GPUImageFilterGroup alloc] init];
    [self.videoCamera addTarget:self.filterGroup];

    self.saturationFilter = [[GPUImageSaturationFilter alloc] init];
    self.saturationFilter.saturation = 1.5f;
    self.sharpenFilter = [[GPUImageSharpenFilter alloc] init];
    self.sharpenFilter.sharpness = 0.5f;
    self.beautyFilter = [[GPUImageBeauty2Filter alloc] init];
//    [self.filterGroup addFilter:self.saturationFilter];
//    [self.filterGroup addFilter:self.sharpenFilter];
    [self addGPUImageFilter:self.beautyFilter];
    [self addGPUImageFilter:self.saturationFilter];
    [self addGPUImageFilter:self.sharpenFilter];
    
    CGRect bounds = [UIScreen mainScreen].bounds;
    self.filteredVideoView = [[GPUImageView alloc] initWithFrame:bounds];
    
    // Add the view somewhere so it's visible
//    [self.view addSubview:filteredVideoView];
    [self.view insertSubview:self.filteredVideoView atIndex:0];
    
    [self.filterGroup addTarget:self.filteredVideoView];
    
    GPUImageRawDataOutput *rawDataOutput = [[GPUImageRawDataOutput alloc] initWithImageSize:[self sourceVideoSize] resultsInBGRAFormat:YES];
    [self.filterGroup addTarget:rawDataOutput];
    __weak GPUImageRawDataOutput *weakOutput = rawDataOutput;
    __weak typeof(self) wself = self;
    [rawDataOutput setNewFrameAvailableBlock:^{
        __strong GPUImageRawDataOutput *strongOutput = weakOutput;
        __strong typeof(wself) strongSelf = wself;
        [strongOutput lockFramebufferForReading];
        GLubyte *outputBytes = [strongOutput rawBytesForImage];
        NSInteger bytesPerRow = [strongOutput bytesPerRowInOutput];
        CVPixelBufferRef pixelBuffer = NULL;
        CVPixelBufferCreateWithBytes(kCFAllocatorDefault, [self sourceVideoSize].width, [self sourceVideoSize].height, kCVPixelFormatType_32BGRA, outputBytes, bytesPerRow, nil, nil, nil, &pixelBuffer);
        [strongOutput unlockFramebufferAfterReading];
        if(pixelBuffer == NULL) {
            return ;
        }
        [strongSelf pushPixelBuffer:pixelBuffer Rotation:0];
        CVPixelBufferRelease(pixelBuffer);
    }];

    [self.videoCamera startCameraCapture];
}

- (void)addGPUImageFilter:(GPUImageOutput<GPUImageInput> *)filter
{
    [_filterGroup addFilter:filter];
    
    GPUImageOutput<GPUImageInput> *newTerminalFilter = filter;
    
    NSInteger count = _filterGroup.filterCount;
    
    if (count == 1)
    {
        _filterGroup.initialFilters = @[newTerminalFilter];
        _filterGroup.terminalFilter = newTerminalFilter;
        
    } else
    {
        GPUImageOutput<GPUImageInput> *terminalFilter    = _filterGroup.terminalFilter;
        NSArray *initialFilters                          = _filterGroup.initialFilters;
        
        [terminalFilter addTarget:newTerminalFilter];
        
        _filterGroup.initialFilters = @[initialFilters[0]];
        _filterGroup.terminalFilter = newTerminalFilter;
    }
}

- (void)setupSLKStreamer {
    NSString *urlString = [self.publishUrl isFileURL] ? [self.publishUrl path] : [self.publishUrl absoluteString];
//    self.slkStreamer = [[SLKStreamer alloc] initWithVideoSize:[self targetVideoSize] FrameRate:(int)[self expectedSourceVideoFrameRate] Bitrate:(int)[self averageBitRate] PublishUrl:urlString];
    
//    urlString = [urlString stringByAppendingString:@" proxy?0?10.200.10.238:10808"];
    
    self.slkStreamer = [[SLKStreamer alloc] init];
    self.slkStreamer.delegate = self;
    
    self.slkStreamerOptions = [[SLKStreamerOptions alloc] init];
    self.slkStreamerOptions.hasVideo = true;
    self.slkStreamerOptions.videoEncodeType = VIDEO_HARDWARE_ENCODE;
    self.slkStreamerOptions.videoSize = [self targetVideoSize];
    self.slkStreamerOptions.fps = [self expectedSourceVideoFrameRate];
    self.slkStreamerOptions.videoBitrate = [self averageBitRate] - 64;
    self.slkStreamerOptions.maxKeyFrameIntervalMs = [self maxKeyFrameIntervalMs];
    
    self.slkStreamerOptions.hasAudio = true;
    self.slkStreamerOptions.audioBitrate = 64;
    
    self.slkStreamerOptions.publishUrl = urlString;
    
    [self.slkStreamer initializeWithOptions:self.slkStreamerOptions];
//    [self.slkStreamer start];
}

- (void)pushPixelBuffer:(CVPixelBufferRef)pixelBuffer
               Rotation:(int)rotation
{
    [self.slkStreamer pushPixelBuffer:pixelBuffer Rotation:0];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)gotConnecting
{
    NSLog(@"GPUImageViewController gotConnecting");
}

- (void)didConnected
{
    NSLog(@"GPUImageViewController didConnected");

}

- (void)gotStreaming
{
    NSLog(@"GPUImageViewController gotStreaming");

}

- (void)didPaused
{
    NSLog(@"GPUImageViewController didPaused");

}

- (void)gotStreamerErrorWithErrorType:(int)errorType
{
    NSLog(@"GPUImageViewController gotErrorWithErrorType");
    
    if (errorType == SLK_MEDIA_STREAMER_ERROR_POOR_NETWORK) {
        NSLog(@"Network is too poor");
    }
}

- (void)gotStreamerInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    NSLog(@"GPUImageViewController gotInfoWithInfoType");
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_REAL_BITRATE) {
        NSLog(@"Real Bitrate:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS) {
        NSLog(@"Real Fps:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_DELAY_TIME) {
        NSLog(@"buffer cache duration:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_DOWN_BITRATE) {
        NSLog(@"down target bitrate to:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_UP_BITRATE) {
        NSLog(@"up target bitrate to:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_TIME) {
        NSLog(@"Record Time:%f S",(float)(infoValue)/10.0f);
    }
}

- (void)didEndStreaming
{
    NSLog(@"GPUImageViewController didEndStreaming");
}

@end
