//
//  AudioRecorderViewController.m
//  MediaStreamerDemo
//
//  Created by slklovewyy on 2019/12/17.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "AudioRecorderViewController.h"
#import "AudioEditEngineEnv.h"
#import "SLKStreamer.h"
#import "YPPAudioPlayer.h"

@interface AudioRecorderViewController () <SLKStreamerDelegate, AudioPlayerDelegate>
@property (nonatomic, strong) NSURL *publishUrl;
@property (nonatomic, strong) SLKStreamer *mAudioRecorder;
@property (nonatomic, strong) YPPAudioPlayer *mAudioPlayer;
@end

@implementation AudioRecorderViewController

+ (void)presentFromViewController:(UIViewController *)viewController URL:(NSURL *)url
{
    [viewController presentViewController:[[AudioRecorderViewController alloc] initWithURL:url] animated:YES completion:nil];
}

- (instancetype)initWithURL:(NSURL *)url {
    self = [self initWithNibName:@"AudioRecorderViewController" bundle:nil];
    if (self) {
        self.publishUrl = url;
        self.mAudioRecorder = nil;
        self.mAudioPlayer = nil;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    if (self.mAudioRecorder) {
        [self.mAudioRecorder terminate];
        self.mAudioRecorder = nil;
    }
    
    if (self.mAudioPlayer) {
        [self.mAudioPlayer terminate];
        self.mAudioPlayer = nil;
    }
    
    NSLog(@"AudioRecorderViewController dealloc");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AudioEditEngineEnv setupAudioSession];
    
    NSString *urlString = [self.publishUrl isFileURL] ? [self.publishUrl path] : [self.publishUrl absoluteString];

    SLKStreamerOptions *options = [[SLKStreamerOptions alloc] init];
    options.hasVideo = NO;
    options.hasAudio = YES;
    options.audioCaptureSource = INTERNAL_MIC_SOURCE;
    options.isControlAudioSession = NO;
    options.audioBitrate = 128;
    options.publishUrl = urlString;
    
    self.mAudioRecorder = [[SLKStreamer alloc] init];
    [self.mAudioRecorder initializeWithOptions:options];
    self.mAudioRecorder.delegate = self;
    
    YPPAudioPlayerOptions* yppAudioPlayerOptions = [[YPPAudioPlayerOptions alloc] init];
    yppAudioPlayerOptions.audioPlayerType = PRIVATE_AUDIO_PLAYER_TYPE;
    self.mAudioPlayer = [[YPPAudioPlayer alloc] init];
    [self.mAudioPlayer initialize:yppAudioPlayerOptions];
    self.mAudioPlayer.delegate = self;
}

- (IBAction)BackAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)StartRecordAction:(id)sender
{
    [self.mAudioRecorder start];
}

- (IBAction)StopRecordAction:(id)sender
{
    [self.mAudioRecorder stop];
}

- (IBAction)StartPlayAction:(id)sender
{
    NSString *urlString = [self.publishUrl isFileURL] ? [self.publishUrl path] : [self.publishUrl absoluteString];
    [self.mAudioPlayer setDataSourceWithUrl:urlString];
    [self.mAudioPlayer prepareAsyncToPlay];
}

- (IBAction)StopPlayAction:(id)sender
{
    [self.mAudioPlayer stop];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - AudioRecorder

- (void)gotConnecting
{
    
}

- (void)didConnected
{
    
}

- (void)gotStreaming
{
    
}

- (void)didPaused
{
    
}

- (void)gotStreamerErrorWithErrorType:(int)errorType
{
    
}

- (void)gotStreamerInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_TIME) {
        NSLog(@"Record Time:%f S",(float)(infoValue)/10.0f);
    }
}

- (void)didEndStreaming
{
    
}

#pragma mark - AudioPlayer

- (void)onPrepared
{
    
}

- (void)onErrorWithErrorType:(int)errorType
{
    
}

- (void)onInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    
}

- (void)onCompletion
{
    
}

- (void)onSeekComplete
{
    
}

@end
