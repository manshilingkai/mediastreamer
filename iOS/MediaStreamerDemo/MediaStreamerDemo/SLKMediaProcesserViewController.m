//
//  SLKMediaProcesserViewController.m
//  MediaStreamerDemo
//
//  Created by Think on 2016/11/22.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "SLKMediaProcesserViewController.h"
#import "SLKMediaMerger.h"
#import "SLKMediaInfo.h"

@interface SLKMediaProcesserViewController () <SLKMediaMergerDelegate>

@property (nonatomic, strong) SLKMediaMerger *slkMediaMerger;
@property (nonatomic, strong) SLKMediaMaterialGroup *slkMediaMaterialGroup;
@property (nonatomic, strong) SLKMediaEffectGroup *slkMediaEffectGroup;
@property (nonatomic, strong) SLKMediaProduct* slkMediaProduct;

@end

@implementation SLKMediaProcesserViewController

+ (void)presentFromViewController:(UIViewController *)viewController
{
    [viewController presentViewController:[[SLKMediaProcesserViewController alloc] init] animated:YES completion:nil];
}

- (instancetype)init {
    self = [self initWithNibName:@"SLKMediaProcesserViewController" bundle:nil];
    if (self) {
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"SLKMediaProcesserViewController dealloc");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.slkMediaMerger = [[SLKMediaMerger alloc] init];
    self.slkMediaMerger.delegate = self;

    
    self.slkMediaMaterialGroup = [[SLKMediaMaterialGroup alloc] init];
    self.slkMediaEffectGroup = [[SLKMediaEffectGroup alloc] init];
    self.slkMediaProduct = [[SLKMediaProduct alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.slkMediaMerger stop];
    [self.slkMediaMerger terminate];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
- (IBAction)startAction:(id)sender
{
    [self copyResourceToAppDocDir];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    NSString* resource0= [[NSBundle mainBundle] pathForResource:@"slk" ofType:@"mp4"];
    NSString* resource1 = [[NSBundle mainBundle] pathForResource:@"ppy" ofType:@"mp3"];
    NSString* resource2= [[NSBundle mainBundle] pathForResource:@"overlay" ofType:@"png"];
    NSString* resource3= [[NSBundle mainBundle] pathForResource:@"tt" ofType:@"mov"];
    NSString* ttf_resource= [[NSBundle mainBundle] pathForResource:@"han_yi_you_rang_ti" ofType:@"ttf"];
    NSString* webp_resource= [[NSBundle mainBundle] pathForResource:@"test" ofType:@"webp"];
    NSString* gif_resource= [[NSBundle mainBundle] pathForResource:@"test" ofType:@"gif"];
    NSString* resource4 = [[NSBundle mainBundle] pathForResource:@"ppy2" ofType:@"mp3"];

    SLKMediaMaterial *slkMediaMaterial = [[SLKMediaMaterial alloc] init];
    slkMediaMaterial.url = resource3;
    slkMediaMaterial.slk_media_material_type = SLK_MEDIA_MATERIAL_TYPE_VIDEO_AUDIO;
    slkMediaMaterial.startPos = 0*1000;
    slkMediaMaterial.endPos = 10*1000;
    slkMediaMaterial.volume = 1.0f;
    slkMediaMaterial.speed = 2.0f;
    
    SLKMediaMaterial *slkMediaMaterial_1 = [[SLKMediaMaterial alloc] init];
    slkMediaMaterial_1.url = resource3;
    slkMediaMaterial_1.slk_media_material_type = SLK_MEDIA_MATERIAL_TYPE_VIDEO_AUDIO;
    slkMediaMaterial_1.startPos = 0*1000;
    slkMediaMaterial_1.endPos = 7500;
    slkMediaMaterial_1.volume = 1.0f;
    slkMediaMaterial_1.speed = 0.5f;
    
    SLKMediaEffect *slkMediaEffect = [[SLKMediaEffect alloc] init];
    slkMediaEffect.slk_media_effect_type = SLK_MEDIA_EFFECT_TYPE_FILTER;
    slkMediaEffect.url = docDir;
    slkMediaEffect.effect_in_pos = 0*1000;
    slkMediaEffect.effect_out_pos = 6*1000;
    slkMediaEffect.slk_gpu_image_filter_type = SLK_GPU_IMAGE_FILTER_CRAYON;
    
    SLKMediaEffect *slkMediaEffect_1 = [[SLKMediaEffect alloc] init];
    slkMediaEffect_1.url = resource1;
    slkMediaEffect_1.slk_media_effect_type = SLK_MEDIA_EFFECT_TYPE_AUDIO;
    slkMediaEffect_1.effect_in_pos = 4*1000;
    slkMediaEffect_1.effect_out_pos = 12*1000;
    slkMediaEffect_1.startPos = 4*1000;
    slkMediaEffect_1.endPos = 12*1000;
    slkMediaEffect_1.volume = 0.3f;
    slkMediaEffect_1.speed = 1.0f;
    
    SLKMediaEffect *slkMediaEffect_2 = [[SLKMediaEffect alloc] init];
    slkMediaEffect_2.url = gif_resource;
    slkMediaEffect_2.slk_media_effect_type = SLK_MEDIA_EFFECT_TYPE_GIF;
    slkMediaEffect_2.effect_in_pos = 0*1000;
    slkMediaEffect_2.effect_out_pos = 8*1000;
    slkMediaEffect_2.x = 240;
    slkMediaEffect_2.y = 320;
    slkMediaEffect_2.rotation = 0;
    slkMediaEffect_2.scale = 1.0f;
    slkMediaEffect_2.flipHorizontal = false;
    slkMediaEffect_2.flipVertical = false;
    
    SLKMediaEffect *slkMediaEffect_3 = [[SLKMediaEffect alloc] init];
    slkMediaEffect_3.url = nil;
    slkMediaEffect_3.slk_media_effect_type = SLK_MEDIA_EFFECT_TYPE_TEXT;
    slkMediaEffect_3.effect_in_pos = 4*1000;
    slkMediaEffect_3.effect_out_pos = 10*1000;
    slkMediaEffect_3.x = 80;
    slkMediaEffect_3.y = 240;
    slkMediaEffect_3.rotation = 60;
    slkMediaEffect_3.scale = 1.2f;
    slkMediaEffect_3.flipHorizontal = false;
    slkMediaEffect_3.flipVertical = false;
    slkMediaEffect_3.text = @"施 你好LLll123";
    slkMediaEffect_3.fontLibPath = ttf_resource;
    slkMediaEffect_3.fontSize = 32;
    slkMediaEffect_3.fontColor = [UIColor colorWithRed:255.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
    slkMediaEffect_3.leftMargin = 4;
    slkMediaEffect_3.rightMargin = 4;
    slkMediaEffect_3.topMargin = 4;
    slkMediaEffect_3.bottomMargin = 4;
    slkMediaEffect_3.lineSpace = 2;
    slkMediaEffect_3.wordSpace = 2;
    slkMediaEffect_3.slk_text_animation_type = SLK_TEXT_ANIMATION_NONE;
    
    SLKMediaEffect *slkMediaEffect_4 = [[SLKMediaEffect alloc] init];
    slkMediaEffect_4.url = resource4;
    slkMediaEffect_4.slk_media_effect_type = SLK_MEDIA_EFFECT_TYPE_AUDIO;
    slkMediaEffect_4.effect_in_pos = 2*1000;
    slkMediaEffect_4.effect_out_pos = 18*1000;
    slkMediaEffect_4.startPos = 10*1000;
    slkMediaEffect_4.endPos = 26*1000;
    slkMediaEffect_4.volume = 0.8f;
    slkMediaEffect_4.speed = 1.0f;

    [self.slkMediaMaterialGroup addMediaMaterial:slkMediaMaterial];
    [self.slkMediaMaterialGroup addMediaMaterial:slkMediaMaterial_1];
    
    [self.slkMediaEffectGroup addMediaEffect:slkMediaEffect];
    [self.slkMediaEffectGroup addMediaEffect:slkMediaEffect_1];
    [self.slkMediaEffectGroup addMediaEffect:slkMediaEffect_2];
    [self.slkMediaEffectGroup addMediaEffect:slkMediaEffect_3];
    [self.slkMediaEffectGroup addMediaEffect:slkMediaEffect_4];
    
    self.slkMediaProduct.url = [docDir stringByAppendingString:@"/product.mp4"];
    self.slkMediaProduct.slk_media_product_type = SLK_MEDIA_PRODUCT_TYPE_MP4;
    self.slkMediaProduct.hasVideo = YES;
    self.slkMediaProduct.videoSize = CGSizeMake(480,640);
    self.slkMediaProduct.hasAudio = YES;
    self.slkMediaProduct.bps = 2000;
    
    [self.slkMediaMerger initializeWithInputMediaMaterialGroup:self.slkMediaMaterialGroup WithInputMediaEffectGroup:self.slkMediaEffectGroup WithMediaMergeAlgorithm:SLK_MEDIA_MERGE_ALGORITHM_TIMELINE WithOutputMediaProduct:self.slkMediaProduct];

    [self.slkMediaMaterialGroup removeMediaMaterial:slkMediaMaterial];
    [self.slkMediaMaterialGroup removeMediaMaterial:slkMediaMaterial_1];

    [self.slkMediaEffectGroup removeMediaEffect:slkMediaEffect];
    [self.slkMediaEffectGroup removeMediaEffect:slkMediaEffect_1];
    [self.slkMediaEffectGroup removeMediaEffect:slkMediaEffect_2];
    [self.slkMediaEffectGroup removeMediaEffect:slkMediaEffect_3];
    [self.slkMediaEffectGroup removeMediaEffect:slkMediaEffect_4];
    
    [self.slkMediaMerger start];
}
*/

/*
- (IBAction)startAction:(id)sender
{
    [self copyResourceToAppDocDir];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    NSString* resource0= [[NSBundle mainBundle] pathForResource:@"slk" ofType:@"mp4"];
    
    SLKMediaMaterial *slkMediaMaterial = [[SLKMediaMaterial alloc] init];
    slkMediaMaterial.url = resource0;
    slkMediaMaterial.slk_media_material_type = SLK_MEDIA_MATERIAL_TYPE_VIDEO_AUDIO;
    slkMediaMaterial.startPos = 4*1000;
    slkMediaMaterial.endPos = 12*1000;
    slkMediaMaterial.volume = 1.0f;
    slkMediaMaterial.speed = 1.0f;
    
    [self.slkMediaMaterialGroup addMediaMaterial:slkMediaMaterial];
    
    self.slkMediaProduct.url = [docDir stringByAppendingString:@"/product.gif"];
    self.slkMediaProduct.slk_media_product_type = SLK_MEDIA_PRODUCT_TYPE_GIF;
    self.slkMediaProduct.hasVideo = YES;
    self.slkMediaProduct.videoSize = CGSizeMake(90,160);
    self.slkMediaProduct.hasAudio = NO;
    self.slkMediaProduct.bps = 2000;
    
    [self.slkMediaMerger initializeWithInputMediaMaterialGroup:self.slkMediaMaterialGroup WithInputMediaEffectGroup:nil WithMediaMergeAlgorithm:SLK_MEDIA_MERGE_ALGORITHM_TIMELINE WithOutputMediaProduct:self.slkMediaProduct];
    
    [self.slkMediaMaterialGroup removeMediaMaterial:slkMediaMaterial];
    
    [self.slkMediaMerger start];
}
*/

- (IBAction)startAction:(id)sender
{
    [self copyResourceToAppDocDir];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    NSString* resource0= [[NSBundle mainBundle] pathForResource:@"IMG_0297" ofType:@"MP4"];
    
    SLKMediaMaterial *slkMediaMaterial = [[SLKMediaMaterial alloc] init];
    slkMediaMaterial.url = resource0;
    slkMediaMaterial.slk_media_material_type = SLK_MEDIA_MATERIAL_TYPE_VIDEO_AUDIO;
    slkMediaMaterial.startPos = 0*1000;
    slkMediaMaterial.endPos = 16*1000;
    slkMediaMaterial.volume = 1.0f;
    slkMediaMaterial.speed = 1.0f;
    
    [self.slkMediaMaterialGroup addMediaMaterial:slkMediaMaterial];
    
    
//    NSString* resource1 = [[NSBundle mainBundle] pathForResource:@"ppy" ofType:@"MOV"];
//
//    SLKMediaMaterial *slkMediaMaterial_1 = [[SLKMediaMaterial alloc] init];
//    slkMediaMaterial_1.url = resource1;
//    slkMediaMaterial_1.slk_media_material_type = SLK_MEDIA_MATERIAL_TYPE_VIDEO_AUDIO;
//    slkMediaMaterial_1.startPos = 0*1000;
//    slkMediaMaterial_1.endPos = 10*1000;
//    slkMediaMaterial_1.volume = 1.0f;
//    slkMediaMaterial_1.speed = 1.0f;
//
//    [self.slkMediaMaterialGroup addMediaMaterial:slkMediaMaterial_1];

    self.slkMediaProduct.url = [docDir stringByAppendingString:@"/product.mp4"];
    self.slkMediaProduct.slk_media_product_type = SLK_MEDIA_PRODUCT_TYPE_MP4;
    self.slkMediaProduct.hasVideo = YES;
    self.slkMediaProduct.videoSize = CGSizeMake(720,1280);
    self.slkMediaProduct.isAspectFit = YES;
    self.slkMediaProduct.hasAudio = YES;
    self.slkMediaProduct.bps = 2000;
    
    [self.slkMediaMerger initializeWithInputMediaMaterialGroup:self.slkMediaMaterialGroup WithInputMediaEffectGroup:nil WithMediaMergeAlgorithm:SLK_MEDIA_MERGE_ALGORITHM_TIMELINE WithOutputMediaProduct:self.slkMediaProduct];
    
    [self.slkMediaMaterialGroup removeMediaMaterial:slkMediaMaterial];
//    [self.slkMediaMaterialGroup removeMediaMaterial:slkMediaMaterial_1];
    
    [self.slkMediaMerger start];
}

/*
- (IBAction)startAction:(id)sender
{
    [self copyResourceToAppDocDir];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    NSString* resource0= [[NSBundle mainBundle] pathForResource:@"IMG_0549" ofType:@"MOV"];
    
    SLKMediaDetailInfo* mediaDetailInfo = [SLKMediaInfo syncGetMediaDetailInfoWithInputFile:resource0];
    
    if (mediaDetailInfo!=nil) {
        if (mediaDetailInfo.hasVideo) {
            NSLog(@"SLKMediaDetailInfo hasVideo:YES");
        }else{
            NSLog(@"SLKMediaDetailInfo hasVideo:NO");
        }
        
        if (mediaDetailInfo.hasAudio) {
            NSLog(@"SLKMediaDetailInfo hasAudio:YES");
        }else{
            NSLog(@"SLKMediaDetailInfo hasAudio:NO");
        }
        
        NSLog(@"SLKMediaDetailInfo Width:%ld",(long)mediaDetailInfo.width);
        NSLog(@"SLKMediaDetailInfo Height:%ld",mediaDetailInfo.height);
        NSLog(@"SLKMediaDetailInfo Fps:%ld",mediaDetailInfo.fps);
        NSLog(@"SLKMediaDetailInfo Kbps:%ld",mediaDetailInfo.kbps);
        NSLog(@"SLKMediaDetailInfo DurationMs:%f",mediaDetailInfo.durationMs);
        NSLog(@"SLKMediaDetailInfo Rotate:%ld",mediaDetailInfo.rotate);
        
        if (mediaDetailInfo.isH264Codec) {
            NSLog(@"SLKMediaDetailInfo isH264Codec:YES");
        }else{
            NSLog(@"SLKMediaDetailInfo isH264Codec:NO");
        }
        if (mediaDetailInfo.isAACCodec) {
            NSLog(@"SLKMediaDetailInfo isAACCodec:YES");
        }else{
            NSLog(@"SLKMediaDetailInfo isAACCodec:NO");
        }

        if (mediaDetailInfo.hasVideo || mediaDetailInfo.hasAudio) {
            if (mediaDetailInfo.isH264Codec && mediaDetailInfo.isAACCodec) {
                SLKMediaMaterial *slkMediaMaterial = [[SLKMediaMaterial alloc] init];
                slkMediaMaterial.url = resource0;
                
                [self.slkMediaMaterialGroup addMediaMaterial:slkMediaMaterial];
                
                self.slkMediaProduct.url = [docDir stringByAppendingString:@"/product_remux.mp4"];
                self.slkMediaProduct.slk_media_product_type = SLK_MEDIA_PRODUCT_TYPE_MP4;
                
                [self.slkMediaMerger initializeWithInputMediaMaterialGroup:self.slkMediaMaterialGroup WithInputMediaEffectGroup:nil WithMediaMergeAlgorithm:SLK_MEDIA_MERGE_ALGORITHM_REMUX WithOutputMediaProduct:self.slkMediaProduct];
                
                [self.slkMediaMaterialGroup removeMediaMaterial:slkMediaMaterial];
                
                [self.slkMediaMerger start];
            }else{
                SLKMediaMaterial *slkMediaMaterial = [[SLKMediaMaterial alloc] init];
                slkMediaMaterial.url = resource0;
                if (!mediaDetailInfo.hasVideo && mediaDetailInfo.hasAudio) {
                    slkMediaMaterial.slk_media_material_type = SLK_MEDIA_MATERIAL_TYPE_AUDIO;
                }else if(mediaDetailInfo.hasVideo && !mediaDetailInfo.hasAudio){
                    slkMediaMaterial.slk_media_material_type = SLK_MEDIA_MATERIAL_TYPE_VIDEO;
                }else{
                    slkMediaMaterial.slk_media_material_type = SLK_MEDIA_MATERIAL_TYPE_VIDEO_AUDIO;
                }
                slkMediaMaterial.startPos = 0*1000;
                slkMediaMaterial.endPos = mediaDetailInfo.durationMs;
                slkMediaMaterial.volume = 1.0f;
                slkMediaMaterial.speed = 1.0f;
                [self.slkMediaMaterialGroup addMediaMaterial:slkMediaMaterial];
                
                self.slkMediaProduct.url = [docDir stringByAppendingString:@"/product.mp4"];
                self.slkMediaProduct.slk_media_product_type = SLK_MEDIA_PRODUCT_TYPE_MP4;
                self.slkMediaProduct.hasVideo = mediaDetailInfo.hasVideo;
                if (mediaDetailInfo.rotate==90 || mediaDetailInfo.rotate==270) {
                    self.slkMediaProduct.videoSize = CGSizeMake(mediaDetailInfo.height,mediaDetailInfo.width);
                }else{
                    self.slkMediaProduct.videoSize = CGSizeMake(mediaDetailInfo.width,mediaDetailInfo.height);
                }
                self.slkMediaProduct.hasAudio = mediaDetailInfo.hasAudio;
                self.slkMediaProduct.bps = mediaDetailInfo.kbps;
                
                [self.slkMediaMerger initializeWithInputMediaMaterialGroup:self.slkMediaMaterialGroup WithInputMediaEffectGroup:nil WithMediaMergeAlgorithm:SLK_MEDIA_MERGE_ALGORITHM_TIMELINE WithOutputMediaProduct:self.slkMediaProduct];
                
                [self.slkMediaMaterialGroup removeMediaMaterial:slkMediaMaterial];
                
                [self.slkMediaMerger start];
            }
        }else{
            NSLog(@"SLKMediaDetailInfo No Video and No Audio");
        }
    }else{
        NSLog(@"syncGetMediaDetailInfoWithInputFile Fail");
    }
}
*/

/*
- (IBAction)startAction:(id)sender
{
    [self copyResourceToAppDocDir];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    NSString* resource0= [[NSBundle mainBundle] pathForResource:@"20181105084100103" ofType:@"mp4"];

    SLKMediaMaterial *slkMediaMaterial = [[SLKMediaMaterial alloc] init];
    slkMediaMaterial.url = resource0;
    
    [self.slkMediaMaterialGroup addMediaMaterial:slkMediaMaterial];
    
    self.slkMediaProduct.url = [docDir stringByAppendingString:@"/product_deep_remux.mp4"];
    self.slkMediaProduct.slk_media_product_type = SLK_MEDIA_PRODUCT_TYPE_MP4;
    
    [self.slkMediaMerger initializeWithInputMediaMaterialGroup:self.slkMediaMaterialGroup WithInputMediaEffectGroup:nil WithMediaMergeAlgorithm:SLK_MEDIA_MERGE_ALGORITHM_DEEP_REMUX WithOutputMediaProduct:self.slkMediaProduct];
    
    [self.slkMediaMaterialGroup removeMediaMaterial:slkMediaMaterial];
    
    [self.slkMediaMerger start];
}
 */

- (IBAction)stopAction:(id)sender
{
    [self.slkMediaMerger stop];
    [self.slkMediaMerger terminate];
}

- (void)gotErrorWithErrorType:(int)errorType
{
    NSLog(@"gotErrorWithErrorType errorType:%d",errorType);
}

- (void)gotInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    if (infoType==SLK_MEDIA_PROCESSER_INFO_WRITE_TIMESTAMP) {
        NSLog(@"Write TimeStamp:%d",infoValue);
    }
}

- (void)didEnd
{
    NSLog(@"didEnd");
}

- (void)copyResourceToAppDocDir
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    NSString *path;
    path = [[NSBundle mainBundle] pathForResource:@"amaromap" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_blowout" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"overlaymap" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"n1977map" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"filter_map_first" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brooklynCurves2" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brooklynCurves1" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_screen" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_process" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_luma" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_contrast" ofType:@"png"];
    [self copyFile:path toDir:docDir];
}

- (BOOL)copyFile:(NSString *)sourcePath toDir:(NSString *)toDir
{
    BOOL retVal = YES; // If the file already exists, we'll return success…
    NSString * finalLocation = [toDir stringByAppendingPathComponent:[sourcePath lastPathComponent]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:finalLocation])
    {
        retVal = [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:finalLocation error:NULL];
    }
    return retVal;
}

@end
