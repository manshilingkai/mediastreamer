//
//  CameraViewController.h
//  MediaStreamerDemo
//
//  Created by slklovewyy on 2019/9/6.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CameraViewController : UIViewController

@property (nonatomic, strong) NSURL *publishUrl;
@property (nonatomic, strong) IBOutlet UITextField *signalTextField;

- (IBAction)backAction:(id)sender;

- (IBAction)switchCamera:(id)sender;
- (IBAction)switchTorch:(id)sender;

- (IBAction)zoomIn:(id)sender;
- (IBAction)zoomOut:(id)sender;

- (IBAction)takePhoto:(id)sender;

- (IBAction)switchFilter:(id)sender;
- (IBAction)mirrorAction:(id)sender;

- (IBAction)MirrorPublish:(id)sender;

- (IBAction)startAction:(id)sender;
- (IBAction)stopAction:(id)sender;

- (IBAction)sendAction:(id)sender;

- (IBAction)pushBGMAction:(id)sender;
- (IBAction)pushSEMAction:(id)sender;
- (IBAction)enableAudioAction:(id)sender;

+ (void)presentFromViewController:(UIViewController *)viewController URL:(NSURL *)url;

@end

NS_ASSUME_NONNULL_END
