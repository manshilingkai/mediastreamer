//
//  AudioRecorderViewController.h
//  MediaStreamerDemo
//
//  Created by slklovewyy on 2019/12/17.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AudioRecorderViewController : UIViewController

+ (void)presentFromViewController:(UIViewController *)viewController URL:(NSURL *)url;

- (IBAction)BackAction:(id)sender;

- (IBAction)StartRecordAction:(id)sender;
- (IBAction)StopRecordAction:(id)sender;

- (IBAction)StartPlayAction:(id)sender;
- (IBAction)StopPlayAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
