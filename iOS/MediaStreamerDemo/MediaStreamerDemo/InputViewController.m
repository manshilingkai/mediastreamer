//
//  InputViewController.m
//  MediaStreamerDemo
//
//  Created by Think on 16/8/24.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "InputViewController.h"
#import "GPUImageViewController.h"
#import "SLKMediaProcesserUtils.h"
#import "SLKMediaProcesserViewController.h"

#import "SLKMediaInfoViewController.h"

#import "SLKMediaInfo.h"

#import "MicrophoneAudioEditorViewController.h"

#import "CameraViewController.h"
#import "ScreenViewController.h"

#import "AudioRecorderViewController.h"

@interface InputViewController ()
{
}

@end

@implementation InputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)publishLiveAction:(id)sender
{
    NSURL *url = [NSURL URLWithString:self.liveUrlTextField.text];
    NSString *scheme = [[url scheme] lowercaseString];
    
    if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"] || [scheme isEqualToString:@"rtmp"] || [scheme isEqualToString:@"rtsp"]) {
        [GPUImageViewController presentFromViewController:self URL:url];
    }
}

-(IBAction)recordLocalAction:(id)sender
{
//    NSString *tmpDir = NSTemporaryDirectory();
//    NSString* tmpPath= [tmpDir stringByAppendingString:@"ppy.mp4"];
//    
//    NSLog(@"tmp Path:%@",tmpPath);
//    
//    NSURL *url = [NSURL URLWithString:tmpPath];
//    [GPUImageViewController presentFromViewController:self URL:url];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString* docPath= [docDir stringByAppendingString:@"/ypp.mp4"];
    
    NSLog(@"doc Path:%@",docPath);

    NSURL *url = [NSURL URLWithString:docPath];
    [GPUImageViewController presentFromViewController:self URL:url];
}

-(IBAction)getCoverImage:(id)sender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
//    NSString* inPath= [docDir stringByAppendingString:@"/product.mp4"];
//    NSString* outPath= [docDir stringByAppendingString:@"/ppy.png"];

    NSString* inPath = [[NSBundle mainBundle] pathForResource:@"no_thumb_image" ofType:@"mp4"];
    NSString* outPath= [docDir stringByAppendingString:@"/no_thumb_image.png"];
    
    NSLog(@"in Path:%@",inPath);
    NSLog(@"out Path:%@",outPath);
    
//    [SLKMediaProcesserUtils getCoverImageFileWithInputFile:inPath OutputWidth:480 OutputHeight:480 OutputPNGFile:outPath];
    [SLKMediaInfo syncGetCoverImageFileWithInputFile:inPath Position:10000.0f OutputWidth:480 OutputHeight:480 OutputPNGFile:outPath];
}

-(IBAction)mergeAction:(id)sender
{
    [SLKMediaProcesserViewController presentFromViewController:self];
    
//    NSString* ttf_resource= [[NSBundle mainBundle] pathForResource:@"han_yi_you_rang_ti" ofType:@"ttf"];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *docDir = [paths objectAtIndex:0];
//    
//    [SLKMediaProcesserUtils wordToBitmapWithInputWord:@"凯" InputFontSize:32 InputFontColor:[UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0f] InputFontLibPath:ttf_resource OutputPNGFile:[docDir stringByAppendingString:@"/ppy.png"]];
}

-(IBAction)mediaInfoAction:(id)sender
{
    [SLKMediaInfoViewController presentFromViewController:self];
/*    
    NSString* resource0= [[NSBundle mainBundle] pathForResource:@"slk" ofType:@"mp4"];

    SLKMediaDetailInfo *slkMediaDetailInfo = [SLKMediaInfo syncGetMediaDetailInfoWithInputFile:resource0];
    
    NSLog(@"Width:%ld",(long)slkMediaDetailInfo.width);
    NSLog(@"Height:%ld",(long)slkMediaDetailInfo.height);
    NSLog(@"Fps:%ld",(long)slkMediaDetailInfo.fps);
    NSLog(@"Kbps:%ld",(long)slkMediaDetailInfo.kbps);
    NSLog(@"durationMs:%f",slkMediaDetailInfo.durationMs);
*/
}

-(IBAction)overlayAction:(id)sender
{

//    [self.overlayTextField setBackgroundColor:[UIColor clearColor]];
    self.overlayTextField.backgroundColor = [UIColor colorWithRed:0.1f green:0.8f blue:0.1f alpha:0.1f];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    NSString* outPath= [docDir stringByAppendingString:@"/overlay1.png"];
    
    UIImage *uiViewImage = [InputViewController imageWithView:self.overlayTextField];
    NSData* imageData = UIImagePNGRepresentation(uiViewImage);
    [imageData writeToFile:outPath atomically:NO];
}

+ (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

-(IBAction)microphoneAudioEditor:(id)sender
{
    [MicrophoneAudioEditorViewController presentFromViewController:self];
}

-(IBAction)camera:(id)sender
{
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *docDir = [paths objectAtIndex:0];
//    NSString* docPath= [docDir stringByAppendingString:@"/ypp.mp4"];
//
//    NSLog(@"doc Path:%@",docPath);
//
//    NSURL *url = [NSURL URLWithString:docPath];
    
    NSURL *url = [NSURL URLWithString:self.liveUrlTextField.text];
    NSString *scheme = [[url scheme] lowercaseString];
    
    if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"] || [scheme isEqualToString:@"rtmp"] || [scheme isEqualToString:@"rtsp"]) {
        [CameraViewController presentFromViewController:self URL:url];
    }
}

-(IBAction)screen:(id)sender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString* docPath= [docDir stringByAppendingString:@"/ypp.mp4"];
    
    NSLog(@"doc Path:%@",docPath);
    
    NSURL *url = [NSURL URLWithString:docPath];
    
    [ScreenViewController presentFromViewController:self URL:url];
}

-(IBAction)AudioRecorder:(id)sender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString* docPath= [docDir stringByAppendingString:@"/ypp.m4a"];
    
    NSLog(@"doc Path:%@",docPath);
    
    NSURL *url = [NSURL URLWithString:docPath];

    [AudioRecorderViewController presentFromViewController:self URL:url];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
