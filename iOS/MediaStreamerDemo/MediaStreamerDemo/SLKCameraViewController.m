//
//  SLKCameraViewController.m
//  MediaStreamerDemo
//
//  Created by Think on 2018/7/25.
//  Copyright © 2018年 Cell. All rights reserved.
//

#import "SLKCameraViewController.h"
#import "SLKCameraView.h"

@interface SLKCameraViewController () <SLKStreamerDelegate>
{
    
}

@property (nonatomic, strong) SLKCameraView *slkCameraView;

@end

@implementation SLKCameraViewController

+ (void)presentFromViewController:(UIViewController *)viewController URL:(NSURL *)url
{
    [viewController presentViewController:[[SLKCameraViewController alloc] initWithURL:url] animated:YES completion:nil];
}

- (instancetype)initWithURL:(NSURL *)url {
    self = [self initWithNibName:@"SLKCameraViewController" bundle:nil];
    if (self) {
        self.publishUrl = url;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"SLKCameraViewController dealloc");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CameraOptions *cameraOptions = [[CameraOptions alloc] init];
    CGRect bounds = [UIScreen mainScreen].bounds;
    self.slkCameraView = [[SLKCameraView alloc] initWithFrame:bounds];
    [self.slkCameraView initialize:cameraOptions];
    self.slkCameraView.delegate = self;
    
    [self.view insertSubview:self.slkCameraView atIndex:0];
    
    NSString *urlString = [self.publishUrl isFileURL] ? [self.publishUrl path] : [self.publishUrl absoluteString];
    SLKStreamerOptions* streamerOptions = [[SLKStreamerOptions alloc] init];
    streamerOptions.publishUrl = urlString;
    [self.slkCameraView startPublish:streamerOptions];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.slkCameraView terminate];
    
    [self.slkCameraView removeFromSuperview];
    
    NSLog(@"viewWillDisappear");
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    NSLog(@"viewDidDisappear");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)gotConnecting
{
    NSLog(@"SLKCameraViewController gotConnecting");
}

- (void)didConnected
{
    NSLog(@"SLKCameraViewController didConnected");
    
}

- (void)gotStreaming
{
    NSLog(@"SLKCameraViewController gotStreaming");
    
}

- (void)didPaused
{
    NSLog(@"SLKCameraViewController didPaused");
    
}

- (void)gotStreamerErrorWithErrorType:(int)errorType
{
    NSLog(@"SLKCameraViewController gotErrorWithErrorType");
    
    if (errorType == SLK_MEDIA_STREAMER_ERROR_POOR_NETWORK) {
        NSLog(@"Network is too poor");
    }
}

- (void)gotStreamerInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    NSLog(@"SLKCameraViewController gotInfoWithInfoType");
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_REAL_BITRATE) {
        NSLog(@"Real Bitrate:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS) {
        NSLog(@"Real Fps:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_DELAY_TIME) {
        NSLog(@"buffer cache duration:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_DOWN_BITRATE) {
        NSLog(@"down target bitrate to:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_UP_BITRATE) {
        NSLog(@"up target bitrate to:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_TIME) {
        NSLog(@"Record Time:%f S",(float)(infoValue)/10.0f);
    }
}

- (void)didEndStreaming
{
    NSLog(@"SLKCameraViewController didEndStreaming");
}

@end
