//
//  ScreenViewController.h
//  MediaStreamerDemo
//
//  Created by slklovewyy on 2019/9/20.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ScreenViewController : UIViewController

- (IBAction)backAction:(id)sender;

+ (void)presentFromViewController:(UIViewController *)viewController URL:(NSURL *)url;

@end

NS_ASSUME_NONNULL_END
