//
//  MicrophoneAudioEditorViewController.h
//  MediaStreamerDemo
//
//  Created by slklovewyy on 2019/7/15.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MicrophoneAudioEditorViewController : UIViewController

+ (void)presentFromViewController:(UIViewController *)viewController;

-(IBAction)backAction:(id)sender;

-(IBAction)startRecordAction:(id)sender;
-(IBAction)startRecordWithStartPosAction:(id)sender;
-(IBAction)stopRecordAction:(id)sender;
-(IBAction)EarReturn:(id)sender;

-(IBAction)openAudioPlayerAction:(id)sender;
-(IBAction)startAudioPlayAction:(id)sender;
-(IBAction)seekAudioPlayAction:(id)sender;
-(IBAction)pauseAudioPlayAction:(id)sender;
-(IBAction)closeAudioPlayerAction:(id)sender;

-(IBAction)ReverseOrderGenerationAction:(id)sender;

-(IBAction)convertToWav:(id)sender;

-(IBAction)startProduce:(id)sender;
-(IBAction)stopProduce:(id)sender;

-(IBAction)Thumbnail:(id)sender;

-(IBAction)startTranscode:(id)sender;
-(IBAction)stopTranscode:(id)sender;

-(IBAction)startScore:(id)sender;
-(IBAction)endScore:(id)sender;

-(IBAction)speechRecognition:(id)sender;

-(IBAction)startAudioRemuxer:(id)sender;
-(IBAction)endAudioRemuxer:(id)sender;

-(IBAction)AudioAssetTranscode:(id)sender;

-(IBAction)StartMixAudioFilePlugin:(id)sender;
-(IBAction)StopMixAudioFilePlugin:(id)sender;

-(IBAction)convertToWavWithNS:(id)sender;
-(IBAction)convertToWavWithOffset:(id)sender;

@end

NS_ASSUME_NONNULL_END
