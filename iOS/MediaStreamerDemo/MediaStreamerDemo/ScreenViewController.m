//
//  ScreenViewController.m
//  MediaStreamerDemo
//
//  Created by slklovewyy on 2019/9/20.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "ScreenViewController.h"
#import <ReplayKit/ReplayKit.h>

@interface ScreenViewController ()

@end

@implementation ScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 12.0, *)) {
        if (UIDevice.currentDevice.systemVersion.floatValue != 13.0) {
            RPSystemBroadcastPickerView *picker = [[RPSystemBroadcastPickerView alloc] initWithFrame:CGRectMake(0, 0, 100, 200)];
            picker.showsMicrophoneButton = YES;
            picker.preferredExtension = @"com.pptv.MediaStreamerDemo.ScreenStreamer";
            [self.view addSubview:picker];
            picker.center = self.view.center;
        }
    } else {
        // Fallback on earlier versions
    }
}

+ (void)presentFromViewController:(UIViewController *)viewController URL:(NSURL *)url
{
    UIViewController *vc = [[ScreenViewController alloc] initWithURL:url];
    vc.navigationController.modalPresentationStyle = UIModalPresentationFullScreen;
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    [viewController presentViewController:vc animated:YES completion:nil];
}

- (instancetype)initWithURL:(NSURL *)url {
    self = [self initWithNibName:@"ScreenViewController" bundle:nil];
    if (self) {

    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSLog(@"viewWillAppear");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSLog(@"viewDidAppear");
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    NSLog(@"viewWillDisappear");
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    NSLog(@"viewDidDisappear");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
