//
//  MicrophoneAudioEditorViewController.m
//  MediaStreamerDemo
//
//  Created by slklovewyy on 2019/7/15.
//  Copyright © 2019年 Cell. All rights reserved.
//
#import "MicrophoneAudioEditorViewController.h"
#import "YPPMicrophoneAudioRecorder.h"
#import "YPPDubbingProducer.h"

#import "YPPAudioPlayer.h"

#import "AudioEditEngineEnv.h"
#import "VideoFileUtils.h"

#import "YPPDubbingScore.h"

#import "YPPMediaTranscoder.h"

#import "SysSpeechRecognizer.h"

#import "YPPAudioRemuxer.h"
#import "YPPTwoToOneRemuxer.h"

#import <MediaPlayer/MediaPlayer.h>
#import "AudioFileUtils.h"

#import "SysMediaTranscoder.h"

#import "YPPMixAudioFilePlugin.h"

@interface MicrophoneAudioEditorViewController () <YPPDubbingProducerDelegate, AudioPlayerDelegate, YPPDubbingScoreDelegate, YPPMediaTranscoderDelegate, SysSpeechRecognizerDelegate, YPPAudioRemuxerDelegate, YPPTwoToOneRemuxerDelegate, MPMediaPickerControllerDelegate, SysMediaTranscoderDelegate, YPPMixAudioFilePluginDelegate>

@property (nonatomic, strong) YPPMicrophoneAudioRecorder *microphoneAudioRecorder;
@property (nonatomic, strong) YPPDubbingProducer *yppDubbingProducer;
@property (nonatomic, strong) YPPAudioPlayer *yppAudioPlayer;
@property (nonatomic, strong) YPPDubbingScore* yppDubbingScore;
@property (nonatomic, strong) YPPMediaTranscoder* yppMediaTranscoder;
@property (nonatomic) BOOL isUseYPPAudioPlayer;

@property (nonatomic, strong) SysSpeechRecognizer* sysSpeechRecognizer;
@property (nonatomic, strong) YPPAudioRemuxer* yppAudioRemuxer;
@property (nonatomic, strong) YPPTwoToOneRemuxer* yppTwoToOneRemuxer;

@property (nonatomic, strong) MPMediaPickerController *pickerController;

@property (nonatomic, strong) SysMediaTranscoder* sysMediaTranscoder;
@property (nonatomic) BOOL isUseSysMediaTranscoder;

@property (nonatomic, strong) YPPMixAudioFilePlugin* mixSEMPlugin;
@property (nonatomic, strong) YPPMixAudioFilePlugin* mixBGMPlugin;

@end

@implementation MicrophoneAudioEditorViewController
{
    MPMediaItem *song;

    int publishDurationForMediaTranscoder;
    
    BOOL isEarReturn;
}

+ (void)presentFromViewController:(UIViewController *)viewController
{
    [viewController presentViewController:[[MicrophoneAudioEditorViewController alloc] init] animated:YES completion:nil];
}

- (instancetype)init {
    self = [self initWithNibName:@"MicrophoneAudioEditorViewController" bundle:nil];
    if (self) {
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        publishDurationForMediaTranscoder = 0;
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"MicrophoneAudioEditorViewController dealloc");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AudioEditEngineEnv setupAudioSession];
    
    MicrophoneAudioRecorderOptions* options = [[MicrophoneAudioRecorderOptions alloc] init];
    options.sampleRate = 44100;
    options.numChannels = 2;
    options.workDir = NSTemporaryDirectory();
    
    self.microphoneAudioRecorder = [[YPPMicrophoneAudioRecorder alloc] init];
    [self.microphoneAudioRecorder initialize:options];
    isEarReturn = NO;
    [self.microphoneAudioRecorder enableEarReturn:isEarReturn];

    YPPAudioPlayerOptions* yppAudioPlayerOptions = [[YPPAudioPlayerOptions alloc] init];
    yppAudioPlayerOptions.audioPlayerType = PRIVATE_AUDIO_PLAYER_TYPE;

    self.yppAudioPlayer = [[YPPAudioPlayer alloc] init];
    [self.yppAudioPlayer initialize:yppAudioPlayerOptions];
    self.yppAudioPlayer.delegate = self;

    self.isUseYPPAudioPlayer = NO;

    self.sysSpeechRecognizer = [[SysSpeechRecognizer alloc] init];
    self.sysSpeechRecognizer.delegate = self;

    self.pickerController = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeMusic];
    self.pickerController.prompt = @"Choose song to export";
    self.pickerController.allowsPickingMultipleItems = NO;
    self.pickerController.delegate = self;
    [self presentViewController:self.pickerController animated:YES completion:nil];
//    [self presentModalViewController:pickerController animated:YES];

    self.isUseSysMediaTranscoder = YES;
}

-(IBAction)backAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)startRecordAction:(id)sender
{
    [self.microphoneAudioRecorder startRecord];
}

-(IBAction)startRecordWithStartPosAction:(id)sender
{
    [self.microphoneAudioRecorder startRecordWithStartPos:2000];
}

-(IBAction)stopRecordAction:(id)sender
{
    [self.microphoneAudioRecorder stopRecord];
}

-(IBAction)EarReturn:(id)sender
{
    isEarReturn = !isEarReturn;
    [self.microphoneAudioRecorder enableEarReturn:isEarReturn];
}

-(IBAction)openAudioPlayerAction:(id)sender
{
    if (self.isUseYPPAudioPlayer) {
        NSString* bgmUrl= [[NSBundle mainBundle] pathForResource:@"zhw" ofType:@"mp3"];
        [self.yppAudioPlayer setVolume:1.1f];
        [self.yppAudioPlayer setDataSourceWithUrl:bgmUrl];
        [self.yppAudioPlayer prepareAsyncToPlay];
        
//        NSURL *assetURL = [song valueForProperty:MPMediaItemPropertyAssetURL];
//        [self.yppAudioPlayer setDataSource:assetURL];
//        [self.yppAudioPlayer prepare];
//        [self.yppAudioPlayer seekTo:5000];
        
//        [self.yppAudioPlayer setDataSourceWithUrl:@"http://p5.hibixin.com/bx-content/0a89cedea96b47feb6e33b9cc0cf1e99.m4a"];
//        [self.yppAudioPlayer prepareAsyncToPlay];
    }else{
        [self.microphoneAudioRecorder openAudioPlayer];
    }
}

-(IBAction)startAudioPlayAction:(id)sender
{
    if (self.isUseYPPAudioPlayer) {
        [self.yppAudioPlayer play];
    }else{
        [self.microphoneAudioRecorder startAudioPlay];
    }
}

-(IBAction)seekAudioPlayAction:(id)sender
{
    static int i = 0;
    if (self.isUseYPPAudioPlayer) {
//        [self.yppAudioPlayer seekTo:4000];
        i++;
        [self.yppAudioPlayer setVolume:0.05f*i];
    }else{
        [self.microphoneAudioRecorder seekAudioPlay:2000];
    }
}

-(IBAction)pauseAudioPlayAction:(id)sender
{
    if (self.isUseYPPAudioPlayer) {
        [self.yppAudioPlayer pause];
    }else{
        [self.microphoneAudioRecorder pauseAudioPlay];
    }
}

-(IBAction)closeAudioPlayerAction:(id)sender
{
    if (self.isUseYPPAudioPlayer) {
        [self.yppAudioPlayer stop];
    }else{
        [self.microphoneAudioRecorder closeAudioPlayer];
    }
}

-(IBAction)ReverseOrderGenerationAction:(id)sender
{
    [self.microphoneAudioRecorder reverseOrderGeneration];
}

-(IBAction)convertToWav:(id)sender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString * wavPath = [docDir stringByAppendingPathComponent:@"123.wav"];
    
    [self.microphoneAudioRecorder convertToWav:wavPath];
}

-(IBAction)startProduce:(id)sender
{
    NSString* videoUrl= [[NSBundle mainBundle] pathForResource:@"fnby" ofType:@"mp4"];
    NSString* bgmUrl= [[NSBundle mainBundle] pathForResource:@"15s" ofType:@"mp3"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
//    NSString *dubUrl = [docDir stringByAppendingPathComponent:@"123.wav"];
    NSString* dubUrl= [[NSBundle mainBundle] pathForResource:@"123" ofType:@"wav"];
    
    NSString *productUrl = [docDir stringByAppendingPathComponent:@"dub.mp4"];
    NSLog(@"productUrl: %@",productUrl);
    
    YPPDubbingProducerOptions *options = [[YPPDubbingProducerOptions alloc] init];
    options.videoUrl = videoUrl;
    options.bgmUrl = bgmUrl;
    options.bgmVolume = 1.0f;
    options.dubUrl = dubUrl;
    options.dubVolume = 1.0f;
    options.productUrl = productUrl;
    
    self.yppDubbingProducer = [[YPPDubbingProducer alloc] initWithTag:0];
    self.yppDubbingProducer.delegate = self;
    [self.yppDubbingProducer initialize:options];
    [self.yppDubbingProducer start];
}

-(IBAction)stopProduce:(id)sender
{
    if (self.yppDubbingProducer) {
        [self.yppDubbingProducer stop:NO];
        [self.yppDubbingProducer terminate];
        self.yppDubbingProducer = nil;
    }
}

-(IBAction)Thumbnail:(id)sender
{
    dispatch_queue_t thumbnailWorkQueue = dispatch_queue_create("ThumbnailWorkQueue", 0);
    dispatch_async(thumbnailWorkQueue, ^{

            NSString* videoUrl= [[NSBundle mainBundle] pathForResource:@"e0cbcff86c51b988a0effb3b9d4fb21e" ofType:@"mp4"];

            NSLog(@"Video File Duration: %d",[VideoFileUtils getVideoFileDuration:videoUrl]);
            NSLog(@"Video File Size: %lld",[VideoFileUtils getVideoFileSize:videoUrl]);
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
            NSString *docDir = [paths objectAtIndex:0];
            NSString* outPath= [docDir stringByAppendingString:@"/IMG_3324.png"];

            NSLog(@"%@",docDir);
            
            UIImage* thumbnailImage = [VideoFileUtils getThumbnailImageFromVideoFilePath:videoUrl Time:0 Accurate:YES];
        //    UIImage* thumbnailImage = [VideoFileUtils getThumbnailImageFromVideoFilePath:videoUrl Time:3 Accurate:YES];
            NSData* imageData = UIImagePNGRepresentation(thumbnailImage);
            [imageData writeToFile:outPath atomically:NO];
        //    thumbnailImage = nil;
    });
}

-(IBAction)startTranscode:(id)sender
{
    if (self.isUseSysMediaTranscoder) {
        NSString* videoUrl= [[NSBundle mainBundle] pathForResource:@"3840x2160" ofType:@"mp4"];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
        NSString *docDir = [paths objectAtIndex:0];
        NSString* outPath= [docDir stringByAppendingString:@"/fnby_video.mp4"];
        NSLog(@"%@", outPath);

        SysMediaTranscoderOptions *options = [[SysMediaTranscoderOptions alloc] init];
        options.inputUrl = [NSURL fileURLWithPath:videoUrl];
        options.startPos = 0;
        options.endPos = 5000;
        options.outputPath = outPath;
        
        self.sysMediaTranscoder = [[SysMediaTranscoder alloc] initWithOptions:options];
        self.sysMediaTranscoder.delegate = self;
        [self.sysMediaTranscoder startTranscoding];
    }else{
        NSString* videoUrl= [[NSBundle mainBundle] pathForResource:@"2020test" ofType:@"mov"];
        NSString* overlayUrl= [[NSBundle mainBundle] pathForResource:@"fcy" ofType:@"mp4"];
//        NSString* pngUrl = [[NSBundle mainBundle] pathForResource:@"overlay1" ofType:@"png"];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
        NSString *docDir = [paths objectAtIndex:0];
        NSString* outPath= [docDir stringByAppendingString:@"/123.mp4"];
        NSLog(@"%@",outPath);
        
        NSString* liveUrl = @"https://pili-vod-live.xxqapp.cn/recordings/z1.xxq-live.SLQ30Q1806734557760928256Q193360861504957144Q2895396380539485440/1592649473_1592649600.m3u8";
//        NSString* liveUrl = @"https://pili-vod.fmeryu.com/playback/6c0fcf999e1540be97368c162e4bb2e1.m3u8";
        NSString* iconPath = [[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"];
        NSString* fontLibPath = [[NSBundle mainBundle] pathForResource:@"DIN_Alternate" ofType:@"ttf"];
        NSString* mask_horizontal = [[NSBundle mainBundle] pathForResource:@"mask_horizontal" ofType:@"png"];
        NSString* mask_vertical = [[NSBundle mainBundle] pathForResource:@"mask_vertical" ofType:@"png"];
        
        YPPMediaTranscoderOptions *options = [[YPPMediaTranscoderOptions alloc] init];
        options.inputUrl = liveUrl;
        options.startPosMs = 0;
        options.endPosMs = 3000;
        
//        options.video_rotation_degree = 90;
//        options.video_flip = VFLIP;
//
//        options.overlayUrl = overlayUrl;
//        options.has_alpha_for_overlay_video = YES;
//        options.overlay_x = 0;
//        options.overlay_y = -64;
//        options.overlay_scale = 0.5524738;
//        options.overlay_effect_in_ms = 1000;
//        options.overlay_effect_out_ms = 11000;
        
        options.privateStrategy = YES;
        options.privateLogoIcon = iconPath;
        options.privateLogoId = @"82013753";
        options.privateLogoFontLibPath = fontLibPath;
        options.privateMaskVertical = mask_vertical;
        options.privateMaskHorizontal = mask_horizontal;
        
        options.outputUrl = outPath;
        options.hasVideo = YES;
//        options.outputVideoWidth = 720;
//        options.outputVideoHeight = 1280;
        options.outputVideoBitrateKbps = 8000;
        options.hasAudio = YES;
        options.outputAudioSampleRate = 44100;
        options.outputAudioBitrateKbps = 128;
        
        NSNumber *startPosMsNumber = [NSNumber numberWithLong:options.startPosMs];
        NSNumber *endPosMsNumber = [NSNumber numberWithLong:options.endPosMs];

        NSDictionary *mediaMaterialDict = [NSDictionary dictionaryWithObjectsAndKeys:options.inputUrl,@"Url",
                                  startPosMsNumber,@"StartPosMs",
                                  endPosMsNumber,@"EndPosMs", nil];
        NSDictionary *privateDict = [NSDictionary dictionaryWithObjectsAndKeys:options.privateLogoIcon,@"PrivateLogoIcon",
                                  options.privateLogoId,@"PrivateLogoId",
                                  options.privateLogoFontLibPath,@"PrivateLogoFontLibPath",
                                  options.privateMaskVertical,@"PrivateMaskHorizontal",
                                  options.privateMaskHorizontal,@"PrivateMaskVertical", nil];
        NSDictionary *mediaEffectDict = [NSDictionary dictionaryWithObjectsAndKeys:privateDict,@"Private", nil];
        NSDictionary *mediaProductDict = [NSDictionary dictionaryWithObjectsAndKeys:options.outputUrl,@"Url", nil];
        
        NSDictionary *rootDict = [NSDictionary dictionaryWithObjectsAndKeys:mediaMaterialDict,@"MediaMaterial", mediaEffectDict,@"MediaEffect", mediaProductDict,@"MediaProduct", nil];
        
        if (rootDict!=nil && [NSJSONSerialization isValidJSONObject:rootDict])
        {
            NSError *error;
            
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:rootDict options:0 error:&error];
            
            if (jsonData!=nil) {
                NSString *jsonStr =[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                
                if (jsonStr!=nil) {
                    NSLog(@"Configure Json : %@", jsonStr);
                    self.yppMediaTranscoder = [[YPPMediaTranscoder alloc] init];
                    self.yppMediaTranscoder.delegate = self;
                    [self.yppMediaTranscoder initializeWithConfigure:jsonStr];
                    [self.yppMediaTranscoder start];
                }
            }
        }else{
            NSLog(@"a given object can't be converted to JSON data\n");
        }
    }
}

-(IBAction)stopTranscode:(id)sender
{
    if (self.isUseSysMediaTranscoder)
    {
        if (self.sysMediaTranscoder) {
            [self.sysMediaTranscoder cancelTranscoding];
        }
    }else{
        if (self.yppMediaTranscoder) {
            [self.yppMediaTranscoder stop:NO];
            [self.yppMediaTranscoder terminate];
            self.yppMediaTranscoder = nil;
        }
    }
}

-(IBAction)startScore:(id)sender
{
    NSString* videoUrl= [[NSBundle mainBundle] pathForResource:@"fnby" ofType:@"mp4"];
    NSString* mp3Url= [[NSBundle mainBundle] pathForResource:@"fnby" ofType:@"mp3"];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString * wavPath = [docDir stringByAppendingPathComponent:@"123.wav"];
        
    YPPDubbingScoreOptions* options = [[YPPDubbingScoreOptions alloc] init];
    options.originUrl = videoUrl;
    options.bgmUrl = mp3Url;
    options.dubUrl = wavPath;
    
    self.yppDubbingScore = [[YPPDubbingScore alloc] init];
    self.yppDubbingScore.delegate = self;
    [self.yppDubbingScore initialize:options];
    [self.yppDubbingScore start];
}

-(IBAction)endScore:(id)sender
{
    if (self.yppDubbingScore) {
        [self.yppDubbingScore terminate];
        self.yppDubbingScore = nil;
    }
}

-(IBAction)speechRecognition:(id)sender
{
    if (self.sysSpeechRecognizer) {
        NSString* audioUrl= [[NSBundle mainBundle] pathForResource:@"fnby" ofType:@"mp3"];
        [self.sysSpeechRecognizer requestRecognitionWithLocalAudioFile:audioUrl];
    }
}

-(IBAction)startAudioRemuxer:(id)sender
{
//    NSString* videoUrl= [[NSBundle mainBundle] pathForResource:@"fnby" ofType:@"mp3"];
//
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *docDir = [paths objectAtIndex:0];
//    NSString * audioPath = [docDir stringByAppendingPathComponent:@"fnby_audio.mp3"];
//
//    YPPAudioRemuxerOptions* options = [[YPPAudioRemuxerOptions alloc] init];
//    options.inputUrl = videoUrl;
//    options.startPosMs = 3000;
//    options.endPosMs = 9000;
//    options.isRemuxAll = NO;
//    options.outputUrl = audioPath;
//
//    self.yppAudioRemuxer = [[YPPAudioRemuxer alloc] init];
//    self.yppAudioRemuxer.delegate = self;
//    [self.yppAudioRemuxer initialize:options];
//    [self.yppAudioRemuxer start];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString * outPath = [docDir stringByAppendingPathComponent:@"fnby_av.mp4"];
    
    NSLog(@"%@",outPath);
    
    YPPTwoToOneRemuxerOptions* options = [[YPPTwoToOneRemuxerOptions alloc] init];
    options.inputVideoUrl = [[NSBundle mainBundle] pathForResource:@"fnby_video" ofType:@"mp4"];
    options.inputAudioUrl = [[NSBundle mainBundle] pathForResource:@"fnby" ofType:@"mp4"];
    options.startPosMsForInputAudioUrl = 2000;
    options.endPosMsForInputAudioUrl = 8000;
    options.outputUrl = outPath;
    
    self.yppTwoToOneRemuxer = [[YPPTwoToOneRemuxer alloc] init];
    self.yppTwoToOneRemuxer.delegate = self;
    [self.yppTwoToOneRemuxer initialize:options];
    [self.yppTwoToOneRemuxer start];
}

-(IBAction)endAudioRemuxer:(id)sender
{
//    if (self.yppAudioRemuxer) {
//        [self.yppAudioRemuxer terminate];
//        self.yppAudioRemuxer = nil;
//    }
    
    if (self.yppTwoToOneRemuxer) {
        [self.yppTwoToOneRemuxer terminate];
        self.yppTwoToOneRemuxer = nil;
    }
}

-(IBAction)AudioAssetTranscode:(id)sender
{
//    NSString* audioString= [[NSBundle mainBundle] pathForResource:@"fnby" ofType:@"mp3"];
//    NSURL* assetURL = [NSURL fileURLWithPath:audioString];
    
    NSURL *assetURL = [song valueForProperty:MPMediaItemPropertyAssetURL];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString * audioOutputPath = [docDir stringByAppendingPathComponent:@"fnby_audio.m4a"];
    
    [AudioFileUtils transcodeWithInput:assetURL WithOutput:audioOutputPath WithHandler:^(AudioFileTrancodeResult result) {
        if (result==AudioFileTrancodeFail) {
            NSLog(@"AudioFileTrancodeFail");
        }else{
            NSLog(@"AudioFileTrancodeSuccess");
        }
    }];
}

-(IBAction)StartMixAudioFilePlugin:(id)sender
{
    NSString* semUrl= [[NSBundle mainBundle] pathForResource:@"fnby" ofType:@"mp3"];
    NSString* bgmUrl= [[NSBundle mainBundle] pathForResource:@"zhw" ofType:@"mp3"];

    YPPMixAudioFilePluginOptions* bgmOptions = [[YPPMixAudioFilePluginOptions alloc] init];
    bgmOptions.pluginType = YPPMixAudioFilePlugin_BGM;
    self.mixBGMPlugin = [[YPPMixAudioFilePlugin alloc] initWithOptions:bgmOptions];
    self.mixBGMPlugin.delegate = self;
//    [self.mixBGMPlugin setVolume:0.2f];
    [self.mixBGMPlugin prepareToPlay:bgmUrl Volume:1.0f];
    
//    YPPMixAudioFilePluginOptions* semOptions = [[YPPMixAudioFilePluginOptions alloc] init];
//    semOptions.pluginType = YPPMixAudioFilePlugin_SEM;
//    self.mixSEMPlugin = [[YPPMixAudioFilePlugin alloc] initWithOptions:semOptions];
//    self.mixBGMPlugin.delegate = self;
//    [self.mixSEMPlugin setVolume:2.0f];
//    [self.mixSEMPlugin prepareToPlay:semUrl];
}

-(IBAction)StopMixAudioFilePlugin:(id)sender
{
    if (self.mixSEMPlugin) {
        [self.mixSEMPlugin stop];
        self.mixSEMPlugin = nil;
    }
    
    if (self.mixBGMPlugin) {
        [self.mixBGMPlugin stop];
        self.mixBGMPlugin = nil;
    }
}

-(IBAction)convertToWavWithNS:(id)sender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString * inputWavPath = [docDir stringByAppendingPathComponent:@"123.wav"];
    
    NSString * outputWavPath = [docDir stringByAppendingPathComponent:@"123_AE.wav"];

//    [YPPMicrophoneAudioRecorder NoiseSuppressionWavWithInput:inputWavPath WithOutput:outputWavPath];
    [YPPMicrophoneAudioRecorder EffectWavWithInput:inputWavPath WithOutput:outputWavPath WithEffectType:MAR_Reverb_Style WithEffect:MAR_KTV];
}

-(IBAction)convertToWavWithOffset:(id)sender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString * inputWavPath = [docDir stringByAppendingPathComponent:@"123_NS.wav"];
    
    NSString * outputWavPath = [docDir stringByAppendingPathComponent:@"123_OffSet.wav"];
    
    [YPPMicrophoneAudioRecorder OffsetWavWithInput:inputWavPath WithOutput:outputWavPath WithOffset:-2000];
}

//-------------------//
- (void)onDubbingStart:(int)tag
{
    NSLog(@"onDubbingStart [Tag:%d]",tag);
}

- (void)onDubbingErrorWithErrorType:(int)errorType Tag:(int)tag
{
    NSLog(@"ErrorType : %d [Tag:%d]",errorType, tag);
}

- (void)onDubbingInfoWithInfoType:(int)infoType InfoValue:(int)infoValue Tag:(int)tag
{
    if (infoType==MEDIA_DUBBING_INFO_WRITE_TIMESTAMP) {
        NSLog(@"Write Time : %d [Tag:%d]",infoValue, tag);
    }
}

- (void)onDubbingEnd:(int)tag
{
    NSLog(@"onDubbingEnd [Tag:%d]", tag);
}
//-------------------//

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//------------------//
- (void)onPrepared
{
    NSLog(@"YPPAudioPlayer onPrepared");
}

- (void)onErrorWithErrorType:(int)errorType
{
    NSLog(@"YPPAudioPlayer onErrorWithErrorType [Error:%d]", errorType);
}

- (void)onInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    NSLog(@"YPPAudioPlayer onInfoWithInfoType [Type:%d Value:%d]",infoType, infoValue);
}

- (void)onCompletion
{
    NSLog(@"YPPAudioPlayer onCompletion");
}

- (void)onSeekComplete
{
    NSLog(@"YPPAudioPlayer onSeekComplete");
}

//-------------------//
- (void)onDubbingScoreStart
{
    NSLog(@"YPPDubbingScore onDubbingScoreStart");
}

- (void)onDubbingScoreErrorWithErrorType:(int)errorType
{
    NSLog(@"YPPDubbingScore onDubbingScoreErrorWithErrorType [Error:%d]",errorType);
}

- (void)onDubbingScoreInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    NSLog(@"YPPDubbingScore onDubbingScoreInfoWithInfoType [Type:%d Value:%d]",infoType, infoValue);
}

- (void)onDubbingScoreEnd:(float)scoreValue
{
    NSLog(@"YPPDubbingScore onDubbingScoreEnd [Score:%f]",scoreValue);
}

//-------------------//
//YPPMediaTranscoderDelegate
- (void)gotConnecting
{
    
}

- (void)didConnected
{
    
}

- (void)gotTranscoding
{
    
}

- (void)didPaused
{
    
}

- (void)gotErrorWithErrorType:(int)errorType
{
    
}

- (void)gotInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    if (infoType==MEDIA_TRANSCODER_INFO_PUBLISH_DURATION) {
        publishDurationForMediaTranscoder = infoValue;
    }
    
    if (infoType==MEDIA_TRANSCODER_INFO_PUBLISH_TIME) {
        NSLog(@"MediaTranscoder Publish Time(S) : %d",infoValue);
        NSLog(@"MediaTranscoder Publish Percent : %d%%",infoValue*100/publishDurationForMediaTranscoder);
    }
}

- (void)didEnd
{
    
}

//-------------------//

- (void)onSpeechRecognizerAuthorizationStatus:(int)status
{
    
}

- (void)onSpeechRecognizerRecognitionResult:(NSString*)result
{

}

//-------------------//

- (void)onAudioRemuxerStart
{
    NSLog(@"onAudioRemuxerStart");
}

- (void)onAudioRemuxerErrorWithErrorType:(int)errorType
{
    NSLog(@"onAudioRemuxerErrorWithErrorType : %d", errorType);
}

- (void)onAudioRemuxerInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    NSLog(@"onAudioRemuxerInfoWithInfoType infoType : %d infoValue %d", infoType, infoValue);
}

- (void)onAudioRemuxerEnd
{
    NSLog(@"onAudioRemuxerEnd");
}
//--
- (void)onTwoToOneRemuxerStart
{
    NSLog(@"onTwoToOneRemuxerStart");
}

- (void)onTwoToOneRemuxerErrorWithErrorType:(int)errorType
{
    NSLog(@"onTwoToOneRemuxerErrorWithErrorType : %d", errorType);
}

- (void)onTwoToOneRemuxerInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    NSLog(@"onTwoToOneRemuxerInfoWithInfoType infoType : %d infoValue %d", infoType, infoValue);
}

- (void)onTwoToOneRemuxerEnd
{
    NSLog(@"onTwoToOneRemuxerEnd");
}

//---------------------//

#pragma mark MPMediaPickerControllerDelegate
- (void)mediaPicker: (MPMediaPickerController *)mediaPicker
  didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection {
    [self.pickerController dismissViewControllerAnimated:YES completion:nil];
    
    if ([mediaItemCollection count] < 1) {
        return;
    }
    song = [[mediaItemCollection items] objectAtIndex:0];
}

- (void)mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker {
    [self.pickerController dismissViewControllerAnimated:YES completion:nil];
}

//-------------------//
- (void)onTranscodingFinished:(NSString *_Nullable)outputPath
{
    NSLog(@"onTranscodingFinished %@",outputPath);
}

- (void)onTranscodingFailed:(NSError * _Nullable)error
{
    NSLog(@"onTranscodingFailed %@", [error localizedDescription]);
}
//-------------------//
- (void)onMixAudioFileError:(int)pluginType ErrorType:(int)errorType
{
    if (pluginType==YPPMixAudioFilePlugin_BGM) {
        NSLog(@"YPPMixAudioFilePlugin-BGM Got Error");
    }else if(pluginType==YPPMixAudioFilePlugin_SEM) {
        NSLog(@"YPPMixAudioFilePlugin-SEM Got Error");
    }
}

- (void)onMixAudioFileEnd:(int)pluginType
{
    if (pluginType==YPPMixAudioFilePlugin_BGM) {
        NSLog(@"YPPMixAudioFilePlugin-BGM Got End");
    }else if(pluginType==YPPMixAudioFilePlugin_SEM) {
        NSLog(@"YPPMixAudioFilePlugin-SEM Got End");
    }
}
//-------------------//

@end
