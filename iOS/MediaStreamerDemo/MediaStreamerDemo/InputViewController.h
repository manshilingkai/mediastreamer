//
//  InputViewController.h
//  MediaStreamerDemo
//
//  Created by shilingkai on 16/8/24.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITextField *liveUrlTextField;
@property (nonatomic, strong) IBOutlet UITextField *overlayTextField;

-(IBAction)publishLiveAction:(id)sender;

-(IBAction)recordLocalAction:(id)sender;

-(IBAction)getCoverImage:(id)sender;

-(IBAction)mergeAction:(id)sender;

-(IBAction)mediaInfoAction:(id)sender;

-(IBAction)overlayAction:(id)sender;

-(IBAction)microphoneAudioEditor:(id)sender;

-(IBAction)camera:(id)sender;

-(IBAction)screen:(id)sender;

-(IBAction)AudioRecorder:(id)sender;

@end
