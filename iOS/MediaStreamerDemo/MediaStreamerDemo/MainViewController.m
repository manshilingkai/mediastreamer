//
//  MainViewController.m
//  MediaPlayerDemo
//
//  Created by shilingkai on 16/3/25.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "MainViewController.h"
//#import "MediaPlayerFrameworkNB/ICVideoView.h"
#import "MediaPlayerFramework/SLKVideoView.h"
#import "MediaPlayerFramework/MediaSourceGroup.h"

//#import "P2PEngine.h"

@interface MainViewController () <MediaPlayerDelegate> {
}

@property (nonatomic, strong) SLKVideoView* videoView;
@property (nonatomic) BOOL isStarted;
@property (nonatomic) BOOL isPaused;
@property (nonatomic, strong) MediaSourceGroup *mediaSourceGroup;


@end

@implementation MainViewController

- (void)dealloc
{
//    [self.videoView stop:YES];
//    [self.videoView terminate];
    
    NSLog(@"MainViewController dealloc");
}

+ (void)presentFromViewController:(UIViewController *)viewController URL:(NSURL *)url
{
    [viewController presentViewController:[[MainViewController alloc] initWithURL:url] animated:YES completion:nil];
}

- (instancetype)initWithURL:(NSURL *)url {
    self = [self initWithNibName:@"MainViewController" bundle:nil];
    if (self) {
        self.url = url;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadMultiDataSource {
    [self copyResourceToAppDocDir];
    
    self.mediaSourceGroup = [[MediaSourceGroup alloc] init];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"slk" ofType:@"mp4"]; // 0 ~ 14*1000
    
    MediaSource *mediaSource1 = [[MediaSource alloc] init];
    mediaSource1.url = path;
    mediaSource1.startPos = 0*1000;
    mediaSource1.endPos = 14*1000;
    
    MediaSource *mediaSource2 = [[MediaSource alloc] init];
    mediaSource2.url = path;
    mediaSource2.startPos = 4*1000;
    mediaSource2.endPos = 10*1000;
    
    MediaSource *mediaSource3 = [[MediaSource alloc] init];
    mediaSource3.url = path;
    mediaSource3.startPos = 2*1000;
    mediaSource3.endPos = 6*1000;
    
    [self.mediaSourceGroup addMediaSource:mediaSource1];
    [self.mediaSourceGroup addMediaSource:mediaSource2];
    [self.mediaSourceGroup addMediaSource:mediaSource3];
    
    CGRect rect = [[UIScreen mainScreen] bounds];
    
    self.videoView = [[SLKVideoView alloc] initWithFrame:rect];
    self.videoView.delegate = self;
    [self.videoView initialize];
    [self.videoView setMultiDataSourceWithMediaSourceGroup:self.mediaSourceGroup DataSourceType:VOD_QUEUE_HIGH_CACHE];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    [self.videoView setFilterWithType:FILTER_SKETCH WithDir:docDir];

    [self.mediaSourceGroup removeAllMediaSources];
    
    [self.view insertSubview:self.videoView atIndex:0];
    
    [self.videoView prepareAsync];
}

- (void)loadSingleDataSource {
    [self copyResourceToAppDocDir];
    
    NSString *urlString = [self.url isFileURL] ? [self.url path] : [self.url absoluteString];
    
//    urlString = [urlString stringByAppendingString:@" proxy?0?10.200.10.238:10808"];

    NSString* filePath=[[NSBundle mainBundle] pathForResource:@"play"ofType:@"txt"];
    NSString *playInfo = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
//    [[P2PEngine SharedInstance] setPlayInfo:playInfo :urlString :YES];
    
    CGRect rect = [[UIScreen mainScreen] bounds];
    
    MediaPlayerOptions *options = [[MediaPlayerOptions alloc] init];
    options.media_player_mode = PRIVATE_MEDIA_PLAYER_MODE;
    options.video_decode_mode = HARDWARE_DECODE_MODE;
    options.pause_in_background = YES;
    options.record_mode = NO_RECORD_MODE;
    options.backupDir = NSTemporaryDirectory();
    
    self.videoView = [[SLKVideoView alloc] initWithFrame:rect];
    self.videoView.delegate = self;
    [self.videoView initializeWithOptions:options];
    [self.videoView setVideoScalingMode:VIDEO_SCALING_MODE_SCALE_TO_FIT];
//    [self.videoView setVideoScaleRate:0.5f];
    //PPBOX_DATA_SOURCE
    //VOD_HIGH_CACHE
    [self.videoView setDataSourceWithUrl:urlString DataSourceType:VOD_HIGH_CACHE DataCacheTimeMs:3000];
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *docDir = [paths objectAtIndex:0];

//    NSString *path = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"m4a"];
//    [self.videoView setDataSourceWithUrl:path DataSourceType:LIVE_HIGH_DELAY/*LIVE_HIGH_DELAY*/];
//    [self.videoView setDataSourceWithUrl:urlString DataSourceType:LIVE_HIGH_DELAY/*LIVE_HIGH_DELAY*/ InfoCollectorDir:docDir];
//    [self.videoView setPlayRate:0.5f];

//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *docDir = [paths objectAtIndex:0];
//    [self.videoView setFilterWithType:FILTER_SKETCH WithDir:docDir];
    
    [self.view insertSubview:self.videoView atIndex:0];
    
    [self.videoView prepareAsyncWithStartPos:0];
    
    self.isStarted = YES;
}

- (void)copyResourceToAppDocDir
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    NSString *path;
    path = [[NSBundle mainBundle] pathForResource:@"amaromap" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_blowout" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"overlaymap" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"n1977map" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"filter_map_first" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brooklynCurves2" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brooklynCurves1" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_screen" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_process" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_luma" ofType:@"png"];
    [self copyFile:path toDir:docDir];
    
    path = [[NSBundle mainBundle] pathForResource:@"brannan_contrast" ofType:@"png"];
    [self copyFile:path toDir:docDir];
}

- (BOOL)copyFile:(NSString *)sourcePath toDir:(NSString *)toDir
{
    BOOL retVal = YES; // If the file already exists, we'll return success…
    NSString * finalLocation = [toDir stringByAppendingPathComponent:[sourcePath lastPathComponent]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:finalLocation])
    {
        retVal = [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:finalLocation error:NULL];
    }
    return retVal;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self loadMultiDataSource];
    [self loadSingleDataSource];
    
    NSLog(@"viewDidLoad");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.view insertSubview:self.videoView atIndex:0];

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
//    [self.videoView stop:YES];
//    [self.videoView terminate];
    
    [self.videoView removeFromSuperview];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)startOrStopAction:(id)sender
{
    if (self.isStarted) {
        [self.StartOrStopButtton setTitle:@"Start" forState:UIControlStateNormal];

        [self.videoView stop:NO];
        self.isStarted = NO;
        
        [self.playOrPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
        self.isPaused = NO;

    }else{
        [self.StartOrStopButtton setTitle:@"Stop" forState:UIControlStateNormal];
        
        [self.videoView prepareAsync];
        
        self.isStarted = YES;
    }
}

- (IBAction)playOrPauseAction:(id)sender
{
    if (self.isStarted)
    {
        if (self.isPaused) {
            [self.playOrPauseButton setTitle:@"Pause" forState:UIControlStateNormal];

            [self.videoView start];
            self.isPaused = NO;
        }else {
            [self.playOrPauseButton setTitle:@"Play" forState:UIControlStateNormal];

            [self.videoView pause];
            self.isPaused = YES;
        }
    }
}

- (IBAction)seekForwardAction:(id)sender
{
    if (self.isStarted) {
//        [self.videoView seekTo:[self.videoView currentPlaybackTime]+/*10*60*1000*/30*1000];
        [self.videoView seekTo:10000];
//        [self.videoView seekToSource:2];
//        [self.videoView setVolume:4.0f];
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//        NSString *docDir = [paths objectAtIndex:0];
        
//        [self.videoView setFilterWithType:FILTER_CRAYON WithDir:docDir];
//        [self.videoView setPlayRate:2.0];
        
//        [self.videoView setVideoScalingMode:VIDEO_SCALING_MODE_SCALE_TO_FIT];
    }
}

- (IBAction)seekBackwardAction:(id)sender
{
    if (self.isStarted) {
//        [self.videoView seekTo:[self.videoView currentPlaybackTime]-/*10*60*1000*/30*1000];
//        [self.videoView seekTo:0];

        [self.videoView seekTo:20000];

//        [self.videoView seekToSource:1];
//        [self.videoView setVolume:0.25f];
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//        NSString *docDir = [paths objectAtIndex:0];
        
//        [self.videoView setFilterWithType:FILTER_COOL WithDir:docDir];
        
//        [self.videoView setVideoScalingMode:VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING];

    }
}

- (IBAction)removeVideoViewAction:(id)sender
{
    [self.videoView removeFromSuperview];
}

- (IBAction)addVideoViewAction:(id)sender
{
    [self.view insertSubview:self.videoView atIndex:0];
}

- (IBAction)nextViewAction:(id)sender;
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"slk" ofType:@"mp4"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
    
    [MainViewController presentFromViewController:self URL:url];
}

- (IBAction)startRecordAction:(id)sender
{
//    [self.videoView backWardForWardRecordStart];
    
    /*
    NSString* urlString = @"ppvod2:///24984043?ft=3&bwtype=3&platform=android3&type=phone.android.vip&sv=2.0.20170914.0&video=true&p2p.advtime=0&k=65646ad6222a663db4485b2dfb03111a-dab1-1506667701&bppcataid=9&vvid=b6a6f0af-dd9c-4675-8b2b-051333158cf0";
    
    [self.videoView stop:YES];
    [self.videoView setDataSourceWithUrl:urlString DataSourceType:PPBOX_DATA_SOURCE];
    [self.videoView prepareAsync];
     */
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    AccurateRecorderOptions *options = [[AccurateRecorderOptions alloc] init];
    options.publishUrl = [docDir stringByAppendingString:@"/record.mp4"];
    
    [self.videoView accurateRecordStart:options];
}

- (IBAction)endRecordAction:(id)sender
{
    /*
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    [self deleteFile];
    
    [self.videoView backWardRecordAsync:[docDir stringByAppendingString:@"/record.mp4"]];
     */
    
//    [self.videoView accurateRecordStop];
    
    /*
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    [self.videoView grabDisplayShot:[docDir stringByAppendingString:@"/grab.png"]];
     */
}

-(void)deleteFile {
    NSFileManager* fileManager=[NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    
    //文件名
    NSString *uniquePath=[[paths objectAtIndex:0] stringByAppendingPathComponent:@"record.mp4"];
    BOOL blHave=[[NSFileManager defaultManager] fileExistsAtPath:uniquePath];
    if (!blHave) {
        NSLog(@"no  have");
        return ;
    }else {
        NSLog(@" have");
        BOOL blDele= [fileManager removeItemAtPath:uniquePath error:nil];
        if (blDele) {
            NSLog(@"dele success");
        }else {
            NSLog(@"dele fail");
        }
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)onPrepared
{
    NSLog(@"VideoView didPrepare");
    [self.videoView start];
    
    self.isStarted = YES;
    self.isPaused = NO;
    
//    [self.videoView seekTo:0];
//    [self.videoView seekToSource:1];
    
//    __block MainViewController* bSelf = self;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [bSelf.playOrPauseButton setTitle:@"Play" forState:UIControlStateNormal];
//    });
//    
//    self.isPaused = YES;

//    [self.videoView setVolume:4.0f];
    
    NSLog(@"VideoView Duration:%f", [self.videoView duration]);

}

- (void)onErrorWithErrorType:(int)errorType
{
    NSLog(@"VideoView gotError With ErrorType:%d",errorType);
}

- (void)onInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    NSLog(@"VideoView gotInfoWithInfoType");
        
    if (infoType==MEDIA_PLAYER_INFO_BUFFERING_START) {
        NSLog(@"VideoView buffering start");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_BUFFERING_END) {
        NSLog(@"VideoView buffering end");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BITRATE) {
//        NSLog(@"Real Bitrate:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_FPS) {
//        NSLog(@"Real Fps:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BUFFER_DURATION) {
        NSLog(@"buffer cache duration:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_REAL_BUFFER_SIZE) {
//        NSLog(@"buffer cache size:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_CONNECTED_SERVER) {
        NSLog(@"connected to server");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_DOWNLOAD_STARTED) {
        NSLog(@"started download stream");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_GOT_FIRST_KEY_FRAME) {
        NSLog(@"got first key frame [DataSize : %d kbit]", infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_VIDEO_RENDERING_START) {
        NSLog(@"start video rendering [Time:%d Ms]",infoValue);
        
//        __block MainViewController* bSelf = self;
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [bSelf.playOrPauseButton setTitle:@"Play" forState:UIControlStateNormal];
//        });
//        
//        [self.videoView pause];
//        self.isPaused = YES;
        
//        __block MainViewController* bSelf = self;
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [bSelf.playOrPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
//        });
//        
//        [self.videoView start];
//        self.isPaused = NO;
    }
    
    if (infoType==MEDIA_PLAYER_INFO_CURRENT_SOURCE_ID) {
        NSLog(@"current source id:%d",infoValue);
    }
    
    if (infoType==MEDIA_PLAYER_INFO_RECORD_FILE_FAIL) {
        NSLog(@"record file fail");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_RECORD_FILE_SUCCESS) {
        NSLog(@"record file success");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_SUCCESS) {
        NSLog(@"grab display shot success");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_GRAB_DISPLAY_SHOT_FAIL) {
        NSLog(@"grab display shot fail");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_START) {
        NSLog(@"ios vtb resurrecting start");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_IOS_VTB_RESURRECTION_END) {
        NSLog(@"ios vtb resurrecting end");
    }
    
    if (infoType==MEDIA_PLAYER_INFO_PLAYBACK_STATE) {
        if (infoValue & INITIALIZED) {
            NSLog(@"INITIALIZED");
        }
        if(infoValue & PREPARING) {
            NSLog(@"PREPARING");
        }
        if(infoValue & PREPARED) {
            NSLog(@"PREPARED");
        }
        
        if(infoValue & STARTED) {
            NSLog(@"STARTED");
        }
        
        if(infoValue & PAUSED) {
            NSLog(@"PAUSED");
        }
        
        if(infoValue & STOPPED) {
            NSLog(@"STOPPED");
        }
        
        if(infoValue & COMPLETED) {
            NSLog(@"COMPLETED");
        }
        
        if(infoValue & ERROR) {
            NSLog(@"ERROR");
        }
        
        if(infoValue & SEEKING) {
            NSLog(@"SEEKING");
        }
    }
}

- (void)onVideoSizeChangedWithVideoWidth:(int)width VideoHeight:(int)height
{
    NSLog(@"VideoView gotVideoSizeChangedWithVideoWidth:%d VideoHeight:%d",width,height);
}

- (void)onCompletion
{
    NSLog(@"gotComplete");
}

- (void)onBufferingUpdateWithPercent:(int)percent
{
    NSLog(@"got Buffering Update With Percent : %d",percent);
}

- (void)onSeekComplete
{
    NSLog(@"gotSeekComplete");
}

@end
