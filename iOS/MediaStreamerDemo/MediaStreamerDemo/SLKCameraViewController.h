//
//  SLKCameraViewController.h
//  MediaStreamerDemo
//
//  Created by Think on 2018/7/25.
//  Copyright © 2018年 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLKCameraViewController : UIViewController

@property (nonatomic, strong) NSURL *publishUrl;

- (IBAction)backAction:(id)sender;

+ (void)presentFromViewController:(UIViewController *)viewController URL:(NSURL *)url;

@end
