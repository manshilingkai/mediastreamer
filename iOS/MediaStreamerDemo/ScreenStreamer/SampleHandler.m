//
//  SampleHandler.m
//  ScreenStreamer
//
//  Created by slklovewyy on 2019/9/20.
//  Copyright © 2019 Cell. All rights reserved.
//


#import "SampleHandler.h"
#import "SLKStreamer.h"
#import "PerformanceMonitor.h"

@interface SampleHandler () <SLKStreamerDelegate, PerformanceMonitorDelegate> {
}

@property (nonatomic, strong) SLKStreamer *slkStreamer;
@property (nonatomic, strong) PerformanceMonitor* performanceMonitor;

@end

@implementation SampleHandler

- (void)broadcastStartedWithSetupInfo:(NSDictionary<NSString *,NSObject *> *)setupInfo {
    // User has requested to start the broadcast. Setup info from the UI extension can be supplied but optional.
    NSLog(@"broadcastStartedWithSetupInfo");
    
    self.performanceMonitor = [[PerformanceMonitor alloc] init];
    self.performanceMonitor.delegate = self;
    [self.performanceMonitor start];
    
    [self requireNetwork];
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *docDir = [paths objectAtIndex:0];
//    NSString* recordPath= [docDir stringByAppendingString:@"/ypp.mp4"];
    
    NSString* recordPath = @"rtmp://pili-publish.live.fmeryu.com/xxq-live/shilingkai-test";
    
    [self stopMediaStreamer];
    [self startMediaStreamer:recordPath];
}

- (void)broadcastPaused {
    // User has requested to pause the broadcast. Samples will stop being delivered.
    NSLog(@"broadcastPaused");
}

- (void)broadcastResumed {
    // User has requested to resume the broadcast. Samples delivery will resume.
    NSLog(@"broadcastResumed");
}

- (void)broadcastFinished {
    // User has requested to finish the broadcast.
    NSLog(@"broadcastFinished");
    
    [self stopMediaStreamer];
    
    if (self.performanceMonitor) {
        [self.performanceMonitor stop];
        self.performanceMonitor = nil;
    }
}

- (void)processSampleBuffer:(CMSampleBufferRef)sampleBuffer withType:(RPSampleBufferType)sampleBufferType {
    switch (sampleBufferType) {
        case RPSampleBufferTypeVideo:
            // Handle video sample buffer
            NSLog(@"RPSampleBufferTypeVideo");

            if (self.slkStreamer!=nil) {
                [self.slkStreamer pushVideoSampleBuffer:sampleBuffer];
            }
            break;
        case RPSampleBufferTypeAudioApp:
            NSLog(@"RPSampleBufferTypeAudioApp");

            // Handle audio sample buffer for app audio
            if (self.slkStreamer!=nil) {
                [self.slkStreamer pushAudioSampleBuffer:sampleBuffer WithType:RPSampleBufferTypeAudioApp];
            }
            break;
        case RPSampleBufferTypeAudioMic:
            NSLog(@"RPSampleBufferTypeAudioMic");

            // Handle audio sample buffer for mic audio
//            if (self.slkStreamer!=nil) {
//                [self.slkStreamer pushAudioSampleBuffer:sampleBuffer];
//            }
            if (self.slkStreamer!=nil) {
                [self.slkStreamer pushAudioSampleBuffer:sampleBuffer WithType:RPSampleBufferTypeAudioMic];
            }
            break;
        default:
            break;
    }
}

- (void)requireNetwork {
    [[[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:@"http://www.baidu.com"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
    }] resume];
}

- (void)startMediaStreamer:(NSString*)publishUrl
{
    CGSize screenSize = [[UIScreen mainScreen] currentMode].size;
    CGFloat width = 360;
    CGFloat height = screenSize.height/(screenSize.width/width);
    int i_height = height;
    if (i_height/2!=0) {
        i_height += 1;
    }
    height = i_height;
    
    SLKStreamerOptions *options = [[SLKStreamerOptions alloc] init];
    options.hasVideo = YES;
    options.videoCaptureSource = SCREEN_CAPTURE_SOURCE;
    options.videoEncodeType = VIDEO_HARDWARE_ENCODE;
    options.videoSize = CGSizeMake(width, height);
    options.fps = 25;
    options.videoBitrate = 1000;
    options.maxKeyFrameIntervalMs = 5000;
    options.isEnableNetworkAdaptive = NO;
    
    options.hasAudio = YES;
    options.audioCaptureSource = EXTERNAL_AUDIO_SOURCE;
    options.audioBitrate = 64;

    options.publishUrl = publishUrl;
    
    self.slkStreamer = [[SLKStreamer alloc] init];
    self.slkStreamer.delegate = self;
    
    [self.slkStreamer initializeWithOptions:options];
    [self.slkStreamer start];
}

- (void)stopMediaStreamer
{
    if (self.slkStreamer) {
        [self.slkStreamer stop];
        [self.slkStreamer terminate];
        self.slkStreamer = nil;
    }
}

// SLKStreamerDelegate

- (void)gotConnecting
{
    
}

- (void)didConnected
{
    
}

- (void)gotStreaming
{
    
}

- (void)didPaused
{
    
}

- (void)gotStreamerErrorWithErrorType:(int)errorType
{
    
}

- (void)gotStreamerInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_TIME) {
        NSLog(@"Publish Time: %d Ms",infoValue*100);
    }
}

- (void)didEndStreaming
{
    
}

- (void)onCpuUsage:(CGFloat)usage
{
    NSLog(@"Cpu Usage: %f", usage);
}

- (void)onMemoryUsage:(NSInteger)usage
{
    NSLog(@"Memory Usage: %ld", usage);
    
    if (usage>=40) {
        NSString* recordPath = @"rtmp://pili-publish.live.fmeryu.com/xxq-live/shilingkai-test";
        
        [self stopMediaStreamer];
        [self startMediaStreamer:recordPath];
    }
}

@end
