#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

IOSSDK_VER=`xcrun -sdk iphoneos --show-sdk-version`

cd ../../../

if [ -d "iOS_release_for_MediaStreamerFramework_XXQ" ]; then
rm -rf iOS_release_for_MediaStreamerFramework_XXQ
fi

mkdir iOS_release_for_MediaStreamerFramework_XXQ
cd iOS_release_for_MediaStreamerFramework_XXQ
mkdir iOS_MediaStreamer
cd ..
cd MediaStreamer/iOS/MediaStreamerFramework_XXQ

rm -rf build

INFOPLIST_FILE=./MediaStreamerFramework/Info.plist
buildNumber=$(date +%Y%m%d%H%M)
echo "buildNumber=${buildNumber}"
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $buildNumber" "$INFOPLIST_FILE"

xcodebuild -project MediaStreamerFramework.xcodeproj -target MediaStreamerFramework -configuration Release -sdk iphoneos${IOSSDK_VER}
xcodebuild -project MediaStreamerFramework.xcodeproj -target MediaStreamerFramework -configuration Release -sdk iphonesimulator${IOSSDK_VER}  -arch x86_64
cd build
cp -r Release-iphoneos Release-iphone-all
lipo -create Release-iphoneos/MediaStreamerFramework.framework/MediaStreamerFramework Release-iphonesimulator/MediaStreamerFramework.framework/MediaStreamerFramework -output Release-iphone-all/MediaStreamerFramework.framework/MediaStreamerFramework
cp -r Release-iphoneos ../../../../iOS_release_for_MediaStreamerFramework_XXQ/
cp -r Release-iphone-all/MediaStreamerFramework.framework ../../../../iOS_release_for_MediaStreamerFramework_XXQ/iOS_MediaStreamer/
cp -r Release-iphonesimulator ../../../../iOS_release_for_MediaStreamerFramework_XXQ/
cd ..
rm -rf build
