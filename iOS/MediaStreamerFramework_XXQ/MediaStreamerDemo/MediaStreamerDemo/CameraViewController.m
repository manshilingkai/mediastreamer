//
//  CameraViewController.m
//  MediaStreamerDemo
//
//  Created by slklovewyy on 2019/9/6.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "CameraViewController.h"
#import <MediaStreamerFramework/SLKCameraView.h>

@interface CameraViewController () <SLKCameraViewDelegate> {
    
}

@property (nonatomic, strong) SLKCameraView  *cameraView;

@end

@implementation CameraViewController
{
    CGFloat mZoomFactor;
}

+ (void)presentFromViewController:(UIViewController *)viewController URL:(NSURL *)url
{
    [viewController presentViewController:[[CameraViewController alloc] initWithURL:url] animated:YES completion:nil];
}

- (instancetype)initWithURL:(NSURL *)url {
    self = [self initWithNibName:@"CameraViewController" bundle:nil];
    if (self) {
        self.publishUrl = url;
        mZoomFactor = 0.0f;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    if (self.cameraView) {
        [self.cameraView terminate];
    }
    NSLog(@"CameraViewController dealloc");
}

- (void)viewDidLoad {
    [super viewDidLoad];

    CGRect rect = [[UIScreen mainScreen] bounds];
    self.cameraView = [[SLKCameraView alloc] initWithFrame:rect];
    self.cameraView.delegate = self;
    
    CameraOptions *options = [[CameraOptions alloc] init];
    options.cameraSessionPreset = AVCaptureSessionPreset1280x720;
    options.cameraPosition = AVCaptureDevicePositionFront;
    options.torchMode = AVCaptureTorchModeAuto;
    options.cameraZoomFactor = mZoomFactor;
    options.cameraFrameRate = 30;
    [self.cameraView initialize:options];
    
    [self.cameraView setDisplayScalingMode:DISPLAY_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING];
    [SLKCameraView setScreenOn:YES];
    
    [self.view insertSubview:self.cameraView atIndex:0];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSLog(@"viewWillAppear");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSLog(@"viewDidAppear");
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    NSLog(@"viewWillDisappear");
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    NSLog(@"viewDidDisappear");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)switchCamera:(id)sender
{
    if (self.cameraView!=nil) {
        [self.cameraView switchCamera];
    }
}

- (IBAction)switchTorch:(id)sender
{
    if (self.cameraView!=nil) {
        [self.cameraView switchTorchMode];
    }
}

- (IBAction)zoomIn:(id)sender
{
    mZoomFactor -= 0.05f;
    if (mZoomFactor<0.0f) {
        mZoomFactor = 0.0f;
    }
    
    if (self.cameraView!=nil) {
        [self.cameraView setCameraZoomFactor:mZoomFactor];
    }
}

- (IBAction)zoomOut:(id)sender
{
    mZoomFactor += 0.05;
    if (mZoomFactor>1.0f) {
        mZoomFactor = 1.0f;
    }
    
    if (self.cameraView!=nil) {
        [self.cameraView setCameraZoomFactor:mZoomFactor];
    }
}

- (IBAction)takePhoto:(id)sender
{
    if (self.cameraView!=nil) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
        NSString *docDir = [paths objectAtIndex:0];
        
        NSString* outPath= [docDir stringByAppendingString:@"/photo.png"];
        
        UIImage *photoImage = [self.cameraView takePhoto];
        NSData* photoData = UIImagePNGRepresentation(photoImage);
        [photoData writeToFile:outPath atomically:NO];
    }
}

- (IBAction)startAction:(id)sender
{
    NSString *urlString = [self.publishUrl isFileURL] ? [self.publishUrl path] : [self.publishUrl absoluteString];

    SLKStreamerOptions *options = [[SLKStreamerOptions alloc] init];
    options.hasVideo = true;
    options.videoCaptureSource = CAMERA_CAPTURE_SOURCE;
    options.videoEncodeType = VIDEO_HARDWARE_ENCODE;
    options.videoSize = CGSizeMake(720, 1280);
    options.fps = 25;
    options.videoBitrate = 1600;
    options.maxKeyFrameIntervalMs = 5000;
    options.isEnableNetworkAdaptive = NO;
    
    options.hasAudio = true;
    options.audioCaptureSource = INTERNAL_MIC_SOURCE;
    options.isControlAudioSession = YES;
    options.audioBitrate = 64;
    
    options.publishUrl = urlString;
    
    if (self.cameraView) {
        [self.cameraView startPublish:options];
    }
}

- (IBAction)stopAction:(id)sender
{
    if (self.cameraView) {
        [self.cameraView stopPublish];
    }
}

- (void)onConnecting
{
    NSLog(@"CameraViewController onConnecting");
}

- (void)onConnected
{
    NSLog(@"CameraViewController onConnected");
    
}

- (void)onStreaming
{
    NSLog(@"CameraViewController onStreaming");
    
}

- (void)onPaused
{
    NSLog(@"CameraViewController onPaused");
    
}

- (void)onError:(int)errorType
{
    NSLog(@"CameraViewController onError [ErrorType : %d]", errorType);
    
    if (errorType == SLK_MEDIA_STREAMER_ERROR_POOR_NETWORK) {
        NSLog(@"Network is too poor");
    }
}

- (void)onInfo:(int)infoType InfoValue:(int)infoValue
{
    NSLog(@"CameraViewController onInfo [InfoType : %d] [InfoValue : %d]", infoType, infoValue);
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_REAL_BITRATE) {
        NSLog(@"Real Bitrate:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS) {
        NSLog(@"Real Fps:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_DELAY_TIME) {
        NSLog(@"buffer cache duration:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_DOWN_BITRATE) {
        NSLog(@"down target bitrate to:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_UP_BITRATE) {
        NSLog(@"up target bitrate to:%d",infoValue);
    }
    
    if (infoType==SLK_MEDIA_STREAMER_INFO_PUBLISH_TIME) {
        NSLog(@"Record Time:%f S",(float)(infoValue)/10.0f);
    }
}

- (void)onEnd
{
    NSLog(@"CameraViewController onEnd");
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
