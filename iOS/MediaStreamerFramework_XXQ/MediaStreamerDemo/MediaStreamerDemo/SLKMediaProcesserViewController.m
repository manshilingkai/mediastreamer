//
//  SLKMediaProcesserViewController.m
//  MediaStreamerDemo
//
//  Created by Think on 2016/11/22.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "SLKMediaProcesserViewController.h"
#import "MediaStreamerFramework/SLKMediaMerger.h"

@interface SLKMediaProcesserViewController () <SLKMediaMergerDelegate>

@property (nonatomic, strong) SLKMediaMerger *slkMediaMerger;
@property (nonatomic, strong) SLKMediaMaterialGroup *slkMediaMaterialGroup;
@property (nonatomic, strong) SLKMediaEffectGroup *slkMediaEffectGroup;
@property (nonatomic, strong) SLKMediaProduct* slkMediaProduct;

@end

@implementation SLKMediaProcesserViewController

+ (void)presentFromViewController:(UIViewController *)viewController
{
    [viewController presentViewController:[[SLKMediaProcesserViewController alloc] init] animated:YES completion:nil];
}

- (instancetype)init {
    self = [self initWithNibName:@"SLKMediaProcesserViewController" bundle:nil];
    if (self) {
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"SLKMediaProcesserViewController dealloc");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.slkMediaMerger = [[SLKMediaMerger alloc] init];
    self.slkMediaMerger.delegate = self;

    
    self.slkMediaMaterialGroup = [[SLKMediaMaterialGroup alloc] init];
    self.slkMediaEffectGroup = [[SLKMediaEffectGroup alloc] init];
    self.slkMediaProduct = [[SLKMediaProduct alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.slkMediaMerger stop];
    [self.slkMediaMerger terminate];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)startAction:(id)sender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    NSString* resource1 = [[NSBundle mainBundle] pathForResource:@"ppy" ofType:@"mp3"];
    NSString* resource2 = [[NSBundle mainBundle] pathForResource:@"slk" ofType:@"mp4"];

    SLKMediaMaterial *slkMediaMaterial1 = [[SLKMediaMaterial alloc] init];
    slkMediaMaterial1.url = resource2;
    slkMediaMaterial1.slk_media_material_type = SLK_MEDIA_MATERIAL_TYPE_VIDEO_AUDIO;
    slkMediaMaterial1.startPos = 0;
    slkMediaMaterial1.endPos = 10*1000;
    slkMediaMaterial1.volume = 1.0f;
    slkMediaMaterial1.speed = 2.0f;
    
    SLKMediaMaterial *slkMediaMaterial2 = [[SLKMediaMaterial alloc] init];
    slkMediaMaterial2.url = resource2;
    slkMediaMaterial2.slk_media_material_type = SLK_MEDIA_MATERIAL_TYPE_VIDEO_AUDIO;
    slkMediaMaterial2.startPos = 0;
    slkMediaMaterial2.endPos = 7500;
    slkMediaMaterial2.volume = 1.0f;
    slkMediaMaterial2.speed = 0.5f;
    
    SLKMediaEffect *slkMediaEffect_1 = [[SLKMediaEffect alloc] init];
    slkMediaEffect_1.url = resource1;
    slkMediaEffect_1.slk_media_effect_type = SLK_MEDIA_EFFECT_TYPE_AUDIO;
    slkMediaEffect_1.effect_in_pos = 4*1000;
    slkMediaEffect_1.effect_out_pos = 12*1000;
    slkMediaEffect_1.startPos = 4*1000;
    slkMediaEffect_1.endPos = 12*1000;
    slkMediaEffect_1.volume = 0.3f;
    slkMediaEffect_1.speed = 1.0f;
    
    [self.slkMediaMaterialGroup addMediaMaterial:slkMediaMaterial1];
    [self.slkMediaMaterialGroup addMediaMaterial:slkMediaMaterial2];

    [self.slkMediaEffectGroup addMediaEffect:slkMediaEffect_1];
    
    self.slkMediaProduct.url = [docDir stringByAppendingString:@"/product.mp4"];
    self.slkMediaProduct.slk_media_product_type = SLK_MEDIA_PRODUCT_TYPE_MP4;
    self.slkMediaProduct.hasVideo = YES;
    self.slkMediaProduct.videoSize = CGSizeMake(480,640);
    self.slkMediaProduct.hasAudio = YES;
    self.slkMediaProduct.bps = 2000;
    
    [self.slkMediaMerger initializeWithInputMediaMaterialGroup:self.slkMediaMaterialGroup WithInputMediaEffectGroup:self.slkMediaEffectGroup WithMediaMergeAlgorithm:SLK_MEDIA_MERGE_ALGORITHM_TIMELINE WithOutputMediaProduct:self.slkMediaProduct];
    
    [self.slkMediaMaterialGroup removeMediaMaterial:slkMediaMaterial1];
    [self.slkMediaMaterialGroup removeMediaMaterial:slkMediaMaterial2];
    
    [self.slkMediaEffectGroup removeMediaEffect:slkMediaEffect_1];

    [self.slkMediaMerger start];
}

- (IBAction)stopAction:(id)sender
{
    [self.slkMediaMerger stop];
    [self.slkMediaMerger terminate];
}

- (void)gotErrorWithErrorType:(int)errorType
{
    NSLog(@"gotErrorWithErrorType errorType:%d",errorType);
}

- (void)gotInfoWithInfoType:(int)infoType InfoValue:(int)infoValue
{
    if (infoType==SLK_MEDIA_PROCESSER_INFO_WRITE_TIMESTAMP) {
        NSLog(@"Write TimeStamp:%d",infoValue);
    }
}

- (void)didEnd
{
    NSLog(@"didEnd");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}

@end
