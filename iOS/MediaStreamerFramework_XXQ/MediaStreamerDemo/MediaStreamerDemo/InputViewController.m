//
//  InputViewController.m
//  MediaStreamerDemo
//
//  Created by Think on 16/8/24.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "InputViewController.h"
#import "CameraViewController.h"

@interface InputViewController ()
{
}

@end

@implementation InputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)publishLiveAction:(id)sender
{
    NSURL *url = [NSURL URLWithString:self.liveUrlTextField.text];
    NSString *scheme = [[url scheme] lowercaseString];
    
    if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"] || [scheme isEqualToString:@"rtmp"] || [scheme isEqualToString:@"rtsp"]) {
        [CameraViewController presentFromViewController:self URL:url];
    }
}

-(IBAction)recordLocalAction:(id)sender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString* docPath= [docDir stringByAppendingString:@"/ppy.mp4"];
    
    NSLog(@"doc Path:%@",docPath);

    NSURL *url = [NSURL URLWithString:docPath];
    [CameraViewController presentFromViewController:self URL:url];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
