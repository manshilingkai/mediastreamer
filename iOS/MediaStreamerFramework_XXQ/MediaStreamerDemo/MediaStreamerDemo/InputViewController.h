//
//  InputViewController.h
//  MediaStreamerDemo
//
//  Created by shilingkai on 16/8/24.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITextField *liveUrlTextField;

-(IBAction)publishLiveAction:(id)sender;

-(IBAction)recordLocalAction:(id)sender;

@end
