//
//  YPPMixAudioFilePlugin.h
//  MediaStreamer
//
//  Created by Think on 2019/11/27.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreAudioKit/CoreAudioKit.h>

NS_ASSUME_NONNULL_BEGIN

enum YPPMixAudioFilePluginType{
    YPPMixAudioFilePlugin_SEM = 0,
    YPPMixAudioFilePlugin_BGM = 1,
};

@interface YPPMixAudioFilePluginOptions : NSObject
@property (nonatomic) int pluginType;
@property (nonatomic) int sampleRate;
@property (nonatomic) int channelCount;
@end

@protocol YPPMixAudioFilePluginDelegate <NSObject>
@required
- (void)onMixAudioFileError:(int)pluginType ErrorType:(int)errorType;
- (void)onMixAudioFileEnd:(int)pluginType;
@end

@interface YPPMixAudioFilePlugin : NSObject

- (instancetype) initWithOptions:(YPPMixAudioFilePluginOptions*)options;

// Deprecated API
- (void)playAudioFile:(NSString*)audioFilePath;
- (void)playAudioFile:(NSString*)audioFilePath Volume:(float)volume;

- (void)prepareToPlay:(NSString*)audioFilePath;
- (void)prepareToPlay:(NSString*)audioFilePath Volume:(float)volume;

- (void)setVolume:(float)volume;

- (void)play;
- (void)pause;

- (void)stop;

- (AudioBuffer*)mixWithExternal:(AudioBuffer*)audioBuffer;

@property (nonatomic, weak) id<YPPMixAudioFilePluginDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
