//
//  SLKStreamer.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

#import "SLKStreamerOptions.h"

enum slk_media_streamer_error_type {
    SLK_MEDIA_STREAMER_ERROR_UNKNOWN = -1,
    SLK_MEDIA_STREAMER_ERROR_CONNECT_FAIL = 0,
    SLK_MEDIA_STREAMER_ERROR_MUX_FAIL = 1,
    SLK_MEDIA_STREAMER_ERROR_COLORSPACECONVERT_FAIL = 2,
    SLK_MEDIA_STREAMER_ERROR_VIDEO_ENCODE_FAIL = 3,
    SLK_MEDIA_STREAMER_ERROR_AUDIO_CAPTURE_START_FAIL = 4,
    SLK_MEDIA_STREAMER_ERROR_AUDIO_ENCODE_FAIL = 5,
    SLK_MEDIA_STREAMER_ERROR_AUDIO_CAPTURE_STOP_FAIL = 6,
    SLK_MEDIA_STREAMER_ERROR_POOR_NETWORK = 7,
    SLK_MEDIA_STREAMER_ERROR_AUDIO_FILTER_FAIL = 8,
    SLK_MEDIA_STREAMER_ERROR_OPEN_VIDEO_ENCODER_FAIL = 9,
};

enum slk_media_streamer_warn_type {
    SLK_MEDIA_STREAMER_WARN_UNKNOWN = -1,
    SLK_MEDIA_STREAMER_WARN_AUDIO_CAPTURE_START_FAIL = 0,
    SLK_MEDIA_STREAMER_WARN_RECONNECTING = 1,
};

enum slk_media_streamer_info_type {
    SLK_MEDIA_STREAMER_INFO_ALREADY_CONNECTING = 1,
    SLK_MEDIA_STREAMER_INFO_PUBLISH_DELAY_TIME = 2,
    SLK_MEDIA_STREAMER_INFO_ALREADY_ENDING     = 3,
    SLK_MEDIA_STREAMER_INFO_PUBLISH_REAL_BITRATE = 4,
    SLK_MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS = 5,
    SLK_MEDIA_STREAMER_INFO_PUBLISH_DOWN_BITRATE = 6,
    SLK_MEDIA_STREAMER_INFO_PUBLISH_UP_BITRATE = 7,
    SLK_MEDIA_STREAMER_INFO_PUBLISH_TIME = 8,
    
    SLK_MEDIA_STREAMER_INFO_AUDIO_INPUT_DATA_LOST = 100,
    
    SLK_MEDIA_STREAMER_INFO_BGM_EOF = 200,
    SLK_MEDIA_STREAMER_INFO_SEM_EOF = 201,
    
    SLK_MEDIA_STREAMER_INFO_NETWORK_REACHABLE = 300,
    SLK_MEDIA_STREAMER_INFO_NETWORK_UNREACHABLE = 301,
};

enum SLKNetworkStatus{
    SLKNetworkNotReachable = 0,
    SLKNetworkReachableViaWWAN = 1,
    SLKNetworkReachableViaWiFi = 2,
};

enum SLKAudioSourceType
{
    SLKAudioSourceLocalMic = 1,
    SLKAudioSourceRemote = 2,
};

@protocol SLKStreamerDelegate <NSObject>
@required
- (void)gotConnecting;
- (void)didConnected;
- (void)gotStreaming;
- (void)didPaused;
- (void)gotStreamerErrorWithErrorType:(int)errorType;
- (void)gotStreamerWarnWithWarnType:(int)warnType WarnValue:(int)warnValue;
- (void)gotStreamerInfoWithInfoType:(int)infoType InfoValue:(int)infoValue;
- (void)didEndStreaming;
@optional
@end

@interface SLKStreamer : NSObject

- (instancetype) init;

- (void)initializeWithOptions:(SLKStreamerOptions*)options;

- (void)pushPixelBuffer:(CVPixelBufferRef)pixelBuffer
                        Rotation:(int)rotation;
- (void)pushAudioSourceBuffer:(CMSampleBufferRef)sampleBuffer WithAudioSourceType:(int)audioSourceType;

- (void)pushVideoSampleBuffer:(CMSampleBufferRef)sampleBuffer;
- (void)pushAudioSampleBuffer:(CMSampleBufferRef)sampleBuffer;
- (void)pushAudioSampleBuffer:(CMSampleBufferRef)sampleBuffer WithType:(int)sampleBufferType;

- (void)pushText:(NSString*)text;

- (void)start;
- (void)resume;
- (void)pause;
- (void)stop;

- (void)enableAudio:(BOOL)isEnable;

// -1 : Loop Forever
//  0 : No Loop
//  1 : Loop One Time
- (void)mixBGM:(NSString*)bgmUrl Volume:(float)volume Loop:(int)numberOfLoops;
- (void)stopBGM;
- (void)mixSEM:(NSString*)semUrl Volume:(float)volume Loop:(int)numberOfLoops;

- (void)iOSDidEnterBackground;
- (void)iOSDidBecomeActive;

- (void)terminate;

@property (nonatomic, weak) id<SLKStreamerDelegate> delegate;

@end
