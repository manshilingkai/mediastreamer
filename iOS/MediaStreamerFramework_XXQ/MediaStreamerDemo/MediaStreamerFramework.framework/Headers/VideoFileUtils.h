//
//  VideoFileUtils.h
//  MediaStreamer
//
//  Created by Think on 2019/9/26.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoFileUtils : NSObject

+ (int)getVideoFileDuration:(NSString*)videoFilePath; //return second
+ (long long)getVideoFileSize:(NSString*)videoFilePath;

+ (UIImage *)getThumbnailImageFromVideoFilePath:(NSString *)videoFilePath Time:(NSTimeInterval)videoTime Accurate:(BOOL)isAccurate;
+ (UIImage *)getThumbnailImageFromVideoFilePath:(NSString *)videoFilePath Time:(NSTimeInterval)videoTime Accurate:(BOOL)isAccurate ToSize:(CGSize)reSize;

+ (CVPixelBufferRef)getVideoImageWithInputFile:(NSString*)inputFile Position:(NSTimeInterval)position OutputWidth:(int)width OutputHeight:(int)height;
@end

NS_ASSUME_NONNULL_END
