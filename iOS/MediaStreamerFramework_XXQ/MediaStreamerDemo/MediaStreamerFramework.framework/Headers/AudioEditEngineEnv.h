//
//  AudioEditEngineEnv.h
//  MediaStreamer
//
//  Created by Think on 2019/8/1.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AudioEditEngineEnv : NSObject

+ (void)setupAudioSession;

@end

NS_ASSUME_NONNULL_END
