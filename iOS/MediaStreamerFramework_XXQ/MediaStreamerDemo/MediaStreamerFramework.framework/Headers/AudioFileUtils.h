//
//  AudioFileUtils.h
//  MediaStreamer
//
//  Created by Think on 2019/11/14.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    AudioFileTrancodeFail,
    AudioFileTrancodeSuccess,
} AudioFileTrancodeResult;

@interface AudioFileUtils : NSObject

+ (void)transcodeWithInput:(NSURL*)assetURL WithOutput:(NSString*)exportPath WithHandler:(void (^)(AudioFileTrancodeResult result))handler;

@end
