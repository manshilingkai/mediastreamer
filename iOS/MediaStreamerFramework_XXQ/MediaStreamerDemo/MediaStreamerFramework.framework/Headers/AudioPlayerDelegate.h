//
//  AudioPlayerDelegate.h
//  MediaStreamer
//
//  Created by Think on 2019/11/15.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AudioPlayerDelegate <NSObject>
@required
- (void)onPrepared;
- (void)onErrorWithErrorType:(int)errorType;
- (void)onInfoWithInfoType:(int)infoType InfoValue:(int)infoValue;
- (void)onCompletion;
- (void)onSeekComplete;
@optional
@end
