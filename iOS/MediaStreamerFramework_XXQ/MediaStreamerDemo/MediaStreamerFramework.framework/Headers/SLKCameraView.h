//
//  SLKCameraView.h
//  MediaStreamer
//
//  Created by Think on 2018/7/20.
//  Copyright © 2018年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GPUImageView.h"
#import "SLKStreamer.h"

enum DisplayScalingMode
{
    DISPLAY_SCALING_MODE_SCALE_TO_FILL = 0,
    DISPLAY_SCALING_MODE_SCALE_TO_FIT = 1,
    DISPLAY_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING = 2,
};

enum FilterType
{
    FILTER_RGB = 0,
    FILTER_SKETCH = 1,
    FILTER_COLORINVERT = 2,
    FILTER_GRAYSCALE = 3,
    FILTER_PIXELLATE = 4,
    FILTER_EMBOSS = 5,
    FILTER_BEAUTY = 6,
    FILTER_NUM = 7,
};

enum FaceDetectionType
{
    PRIVATE_FACE_DETECTION = 0,
    SYSTEM_FACE_DETECTION = 1,
    THIRDPARTY_FACE_DETECTION = 2,
};

@protocol SLKCameraViewDelegate <NSObject>
@required
- (void)onConnecting;
- (void)onConnected;
- (void)onStreaming;
- (void)onPaused;
- (void)onError:(int)errorType;
- (void)onWarn:(int)warnType WarnValue:(int)warnValue;
- (void)onInfo:(int)infoType InfoValue:(int)infoValue;
- (void)onEnd;
@optional
- (CVPixelBufferRef)onProcessPixelBuffer:(CVPixelBufferRef)pixelBuffer;
@end

@interface CameraOptions : NSObject
@property (nonatomic, strong) AVCaptureSessionPreset cameraSessionPreset;
@property (nonatomic) AVCaptureDevicePosition cameraPosition;
@property (nonatomic) AVCaptureTorchMode torchMode;
@property (nonatomic) CGFloat cameraZoomFactor;//0.0f ~ 1.0f
@property (nonatomic) int cameraFrameRate;
//@property (nonatomic) BOOL isUseOwnBeautyFilter;
@property (nonatomic) int faceDetectionType;
@end

@interface SLKCameraView : GPUImageView

- (instancetype)init;
- (instancetype)initWithFrame:(CGRect)frame;

- (void)initialize:(CameraOptions*)options;

- (void)setDisplayScalingMode:(int)mode;
- (void)switchCamera;
- (void)mirror;
- (void)switchTorchMode;
- (void)setCameraZoomFactor:(CGFloat)factor; //0.0f ~ 1.0f
- (void)setFilter:(int)filterType;

- (void)startPublish:(SLKStreamerOptions*)options;
- (void)pausePublish;
- (void)resumePublish;
- (void)stopPublish;

- (void)mirrorPublish;

- (void)enableAudio:(BOOL)isEnable;

// -1 : Loop Forever
//  0 : No Loop
//  1 : Loop One Time
- (void)mixBGM:(NSString*)bgmUrl Volume:(float)volume Loop:(int)numberOfLoops;
- (void)stopBGM;
- (void)mixSEM:(NSString*)semUrl Volume:(float)volume Loop:(int)numberOfLoops;

- (void)pushText:(NSString*)text;

- (UIImage *)takePhoto;

- (void)terminate;

+ (void)setScreenOn:(BOOL)on;

@property (nonatomic, weak) id<SLKCameraViewDelegate> delegate;

@end
