#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

IOSSDK_VER="12.1"

cd ../../../

if [ -d "iOS_release_for_MediaStreamerFrameworkNative" ]; then
rm -rf iOS_release_for_MediaStreamerFrameworkNative
fi

mkdir iOS_release_for_MediaStreamerFrameworkNative
cd iOS_release_for_MediaStreamerFrameworkNative
mkdir iOS_MediaStreamer
cd ..
cd MediaStreamer/iOS/MediaStreamerFrameworkNative

rm -rf build
xcodebuild -project MediaStreamerFramework.xcodeproj -target MediaStreamerFramework -configuration Release -sdk iphoneos${IOSSDK_VER} OTHER_CFLAGS="-fembed-bitcode"
xcodebuild -project MediaStreamerFramework.xcodeproj -target MediaStreamerFramework -configuration Release -sdk iphonesimulator${IOSSDK_VER} -arch x86_64 OTHER_CFLAGS="-fembed-bitcode"
cd build
cp -r Release-iphoneos Release-iphone-all
lipo -create Release-iphoneos/MediaStreamerFramework.framework/MediaStreamerFramework Release-iphonesimulator/MediaStreamerFramework.framework/MediaStreamerFramework -output Release-iphone-all/MediaStreamerFramework.framework/MediaStreamerFramework
cp -r Release-iphoneos ../../../../iOS_release_for_MediaStreamerFrameworkNative/
cp -r Release-iphone-all/MediaStreamerFramework.framework ../../../../iOS_release_for_MediaStreamerFrameworkNative/iOS_MediaStreamer/
cp -r Release-iphonesimulator ../../../../iOS_release_for_MediaStreamerFrameworkNative/
cd ..
rm -rf build
