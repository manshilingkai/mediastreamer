//
//  GPUImageBeauty3Filter.h
//  GPUImage
//
//  Created by PPTV on 16/9/23.
//  Copyright © 2016年 Brad Larson. All rights reserved.
//


#import "GPUImage.h"

@class GPUImageCombinationFilter;

@interface GPUImageBeauty3Filter : GPUImageFilterGroup {
    GPUImageBilateralFilter *bilateralFilter;
    GPUImageCannyEdgeDetectionFilter *cannyEdgeFilter;
    GPUImageCombinationFilter *combinationFilter;
    GPUImageHSBFilter *hsbFilter;
}

@end