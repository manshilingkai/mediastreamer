//
//  GPUImageViewController.h
//  MediaStreamerDemo
//
//  Created by shilingkai on 16/6/20.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GPUImageViewController : UIViewController

@property (nonatomic, strong) NSURL   *publishUrl;
@property (nonatomic, strong) IBOutlet UIButton *enableAudioButton;
@property (nonatomic, strong) IBOutlet UIButton *pauseOrResumeButton;

- (IBAction)backAction:(id)sender;
- (IBAction)enableAudioAction:(id)sender;

- (IBAction)switchCamera:(id)sender;
- (IBAction)pauseOrResume:(id)sender;

+ (void)presentFromViewController:(UIViewController *)viewController URL:(NSURL *)url;

@end
