//
//  YPPAudioPlayer.h
//  MediaStreamer
//
//  Created by Think on 2019/8/1.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

enum audio_player_event_type {
    AUDIO_PLAYER_PREPARED = 0,
    AUDIO_PLAYER_ERROR = 1,
    AUDIO_PLAYER_INFO = 2,
    AUDIO_PLAYER_PLAYBACK_COMPLETE = 3,
    AUDIO_PLAYER_SEEK_COMPLETE = 4,
};

enum audio_player_error_type {
    AUDIO_PLAYER_ERROR_UNKNOWN = -1,
    AUDIO_PLAYER_ERROR_NO_MEMORY = 0,
    AUDIO_PLAYER_ERROR_OPEN_PLAY_URL_FAIL = 1,
    AUDIO_PLAYER_ERROR_FIND_STREAM_INFO_FAIL = 2,
    AUDIO_PLAYER_ERROR_NO_AUDIO_STREAM = 3,
    AUDIO_PLAYER_ERROR_AUDIO_DECODER_NOT_FOUND = 4,
    AUDIO_PLAYER_ERROR_ALLOCATE_AUDIO_DECODER_CONTEXT_FAIL = 5,
    AUDIO_PLAYER_ERROR_COPY_AUDIO_STREAM_CODEC_PARAM_TO_AUDIO_DECODER_CONTEXT_FAIL = 6,
    AUDIO_PLAYER_ERROR_OPEN_AUDIO_DECODER_FAIL = 7,
    AUDIO_PLAYER_ERROR_OPEN_AUDIO_FILTER_FAIL = 8,
    AUDIO_PLAYER_ERROR_CREATE_AUDIO_RENDER_FAIL = 9,
    AUDIO_PLAYER_ERROR_INIT_AUDIO_RENDER_FAIL = 10,
    AUDIO_PLAYER_ERROR_DEMUXER_READ_FAIL = 11,
    AUDIO_PLAYER_ERROR_AUDIO_FILTER_INPUT_FAIL = 12,
    AUDIO_PLAYER_ERROR_AUDIO_FILTER_OUTPUT_FAIL = 13,
    AUDIO_PLAYER_ERROR_UPDATE_AUDIO_FILTER_FAIL = 14,
};

@protocol AudioPlayerDelegate <NSObject>
@required
- (void)onPrepared;
- (void)onErrorWithErrorType:(int)errorType;
- (void)onInfoWithInfoType:(int)infoType InfoValue:(int)infoValue;
- (void)onCompletion;
- (void)onSeekComplete;
@optional
@end

@interface YPPAudioPlayerOptions : NSObject
@property (nonatomic) BOOL isControlAudioSession;
@end

@interface YPPAudioPlayer : NSObject

- (void)initialize:(YPPAudioPlayerOptions*)options;
- (void)initialize;

- (void)setDataSourceWithUrl:(NSString*)url;

- (void)prepare;
- (void)prepareAsync;
- (void)prepareAsyncToPlay;
- (void)play;
- (BOOL)isPlaying;
- (void)pause;
- (void)stop;
- (void)seekTo:(NSTimeInterval)seekPosMs;

- (void)setVolume:(NSTimeInterval)volume;
- (void)setPlayRate:(NSTimeInterval)playrate;
- (void)setLooping:(BOOL)isLooping;

- (void)terminate;

@property (nonatomic, weak) id<AudioPlayerDelegate> delegate;

@property (nonatomic, readonly) NSTimeInterval durationMs;
@property (nonatomic, readonly) NSTimeInterval currentPlaybackTimeMs;
@property (nonatomic, readonly) NSInteger pcmDB;

@end

NS_ASSUME_NONNULL_END
