//
//  YPPMicrophoneAudioRecorder.h
//  MediaStreamer
//
//  Created by Think on 2019/7/13.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MicrophoneAudioRecorderOptions : NSObject

@property (nonatomic) BOOL isControlAudioSession;
@property (nonatomic) NSInteger sampleRate;
@property (nonatomic) NSInteger numChannels;
@property (nonatomic, strong) NSString *workDir;

@end

@interface YPPMicrophoneAudioRecorder : NSObject

- (instancetype) init;

- (BOOL)initialize:(MicrophoneAudioRecorderOptions*)options;

- (BOOL)startRecord;
- (BOOL)startRecordWithStartPos:(NSTimeInterval)startPositionMs;
- (void)stopRecord;

- (BOOL)openAudioPlayer;
- (BOOL)startAudioPlay;
- (BOOL)seekAudioPlay:(NSTimeInterval)seekTimeMs;
- (void)pauseAudioPlay;
- (void)closeAudioPlayer;

- (BOOL)backDelete:(NSInteger)keepTimeMs;
- (BOOL)convertToWav:(NSString*)wavFilePath;
- (BOOL)convertToWavWithOffset:(NSString*)wavFilePath Offset:(NSInteger)offsetMs;

- (void)terminate;

@property (nonatomic, readonly) NSTimeInterval recordTimeMs;
@property (nonatomic, readonly) NSInteger recordPcmDB;

@property (nonatomic, readonly) NSTimeInterval playTimeMs;
@property (nonatomic, readonly) NSTimeInterval playDurationMs;
@property (nonatomic, readonly) NSInteger playPcmDB;

@end

NS_ASSUME_NONNULL_END
