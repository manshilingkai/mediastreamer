#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

IOSSDK_VER="12.1"

cd ../../../

if [ -d "iOS_release_for_FFMediaFramework" ]; then
rm -rf iOS_release_for_FFMediaFramework
fi

mkdir iOS_release_for_FFMediaFramework
cd iOS_release_for_FFMediaFramework
mkdir iOS_FFMedia
cd ..
cd MediaStreamer/iOS/FFMediaFramework

rm -rf build

INFOPLIST_FILE=./FFMediaFramework/Info.plist
buildNumber=$(date +%Y%m%d%H%M)
echo "buildNumber=${buildNumber}"
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $buildNumber" "$INFOPLIST_FILE"

xcodebuild -project FFMediaFramework.xcodeproj -target FFMediaFramework -configuration Release -sdk iphoneos${IOSSDK_VER}
xcodebuild -project FFMediaFramework.xcodeproj -target FFMediaFramework -configuration Release -sdk iphonesimulator${IOSSDK_VER} -arch x86_64
cd build
cp -r Release-iphoneos Release-iphone-all
lipo -create Release-iphoneos/FFMediaFramework.framework/FFMediaFramework Release-iphonesimulator/FFMediaFramework.framework/FFMediaFramework -output Release-iphone-all/FFMediaFramework.framework/FFMediaFramework
cp -r Release-iphoneos ../../../../iOS_release_for_FFMediaFramework/
cp -r Release-iphone-all/FFMediaFramework.framework ../../../../iOS_release_for_FFMediaFramework/iOS_FFMedia/
cp -r Release-iphonesimulator ../../../../iOS_release_for_FFMediaFramework/
cd ..
rm -rf build
