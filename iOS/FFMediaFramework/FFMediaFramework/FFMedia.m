//
//  FFMedia.m
//  FFMediaFramework
//
//  Created by slklovewyy on 2019/3/4.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import "FFMedia.h"
#include "libavformat/avformat.h"
#include "libavfilter/avfilter.h"

@implementation FFMedia

+ (void)registerAll
{
    av_register_all();
    avfilter_register_all();
    avfilter_inout_alloc();
    avfilter_inout_free(NULL);
}

@end
