#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

IOSSDK_VER="12.1"

cd ../../../

if [ -d "iOS_release_for_MediaStreamerFramework-" ]; then
rm -rf iOS_release_for_MediaStreamerFramework-
fi

mkdir iOS_release_for_MediaStreamerFramework-
cd iOS_release_for_MediaStreamerFramework-
mkdir iOS_MediaStreamer
cd ..
cd MediaStreamer/iOS/MediaStreamerFramework-

rm -rf build

INFOPLIST_FILE=./MediaStreamerFramework/Info.plist
buildNumber=$(date +%Y%m%d%H%M)
echo "buildNumber=${buildNumber}"
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $buildNumber" "$INFOPLIST_FILE"

xcodebuild -project MediaStreamerFramework.xcodeproj -target MediaStreamerFramework -configuration Release -sdk iphoneos${IOSSDK_VER}
xcodebuild -project MediaStreamerFramework.xcodeproj -target MediaStreamerFramework -configuration Release -sdk iphonesimulator${IOSSDK_VER}  -arch x86_64
cd build
cp -r Release-iphoneos Release-iphone-all
lipo -create Release-iphoneos/MediaStreamerFramework.framework/MediaStreamerFramework Release-iphonesimulator/MediaStreamerFramework.framework/MediaStreamerFramework -output Release-iphone-all/MediaStreamerFramework.framework/MediaStreamerFramework
cp -r Release-iphoneos ../../../../iOS_release_for_MediaStreamerFramework-/
cp -r Release-iphone-all/MediaStreamerFramework.framework ../../../../iOS_release_for_MediaStreamerFramework-/iOS_MediaStreamer/
cp -r Release-iphonesimulator ../../../../iOS_release_for_MediaStreamerFramework-/
cd ..
rm -rf build
