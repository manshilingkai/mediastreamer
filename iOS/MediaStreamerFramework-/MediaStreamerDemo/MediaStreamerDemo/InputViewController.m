//
//  InputViewController.m
//  MediaStreamerDemo
//
//  Created by Think on 16/8/24.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import "InputViewController.h"
#import "GPUImageViewController.h"
#import "MediaStreamerFramework/SLKMediaProcesserUtils.h"
#import "SLKMediaProcesserViewController.h"

#import "SLKMediaInfoViewController.h"

@interface InputViewController ()
{
}

@end

@implementation InputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)publishLiveAction:(id)sender
{
    NSURL *url = [NSURL URLWithString:self.liveUrlTextField.text];
    NSString *scheme = [[url scheme] lowercaseString];
    
    if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"] || [scheme isEqualToString:@"rtmp"] || [scheme isEqualToString:@"rtsp"]) {
        [GPUImageViewController presentFromViewController:self URL:url];
    }
}

-(IBAction)recordLocalAction:(id)sender
{
//    NSString *tmpDir = NSTemporaryDirectory();
//    NSString* tmpPath= [tmpDir stringByAppendingString:@"ppy.mp4"];
//    
//    NSLog(@"tmp Path:%@",tmpPath);
//    
//    NSURL *url = [NSURL URLWithString:tmpPath];
//    [GPUImageViewController presentFromViewController:self URL:url];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString* docPath= [docDir stringByAppendingString:@"/ppy.mp4"];
    
    NSLog(@"doc Path:%@",docPath);

    NSURL *url = [NSURL URLWithString:docPath];
    [GPUImageViewController presentFromViewController:self URL:url];
}

-(IBAction)getCoverImage:(id)sender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString* inPath= [docDir stringByAppendingString:@"/ppy.mp4"];
    NSString* outPath= [docDir stringByAppendingString:@"/ppy.png"];

    NSLog(@"in Path:%@",inPath);
    NSLog(@"out Path:%@",outPath);
    
    [SLKMediaProcesserUtils getCoverImageFileWithInputFile:inPath OutputWidth:480 OutputHeight:480 OutputPNGFile:outPath];
}

-(IBAction)mergeAction:(id)sender
{
    [SLKMediaProcesserViewController presentFromViewController:self];
}

-(IBAction)mediaInfoAction:(id)sender
{
    [SLKMediaInfoViewController presentFromViewController:self];
}

-(IBAction)overlayAction:(id)sender
{
//    [self.overlayTextField setBackgroundColor:[UIColor clearColor]];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    NSString* outPath= [docDir stringByAppendingString:@"/overlay1.png"];
    
    UIImage *uiViewImage = [InputViewController imageWithView:self.overlayTextField];
    NSData* imageData = UIImagePNGRepresentation(uiViewImage);
    [imageData writeToFile:outPath atomically:NO];
}

+ (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
