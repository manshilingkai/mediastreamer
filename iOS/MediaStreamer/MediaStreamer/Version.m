//
//  Version.m
//  MediaStreamer
//
//  Created by Think on 2017/6/2.
//  Copyright © 2017年 Cell. All rights reserved.
//

#import "Version.h"

@implementation Version

+(NSString*)getVersionCode
{
    return @"0.0.5";
}

@end
