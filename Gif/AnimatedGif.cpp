//
//  AnimatedGif.cpp
//  MediaStreamer
//
//  Created by Think on 2017/4/6.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "AnimatedGif.h"
#include "MediaLog.h"

AnimatedGif::AnimatedGif(char* filePath)
{
    mFilePath = strdup(filePath);
    
    delay = 0;
    trans_color = -1;
    
    mFramesNum = 0;
    
    mAnimatedGifDuration = 0ll;
}

AnimatedGif::~AnimatedGif()
{
    free(mFilePath);
    
    for(vector<VideoFrame*>::iterator it = mAnimatedFrames.begin(); it != mAnimatedFrames.end(); ++it)
    {
        VideoFrame* videoFrame = *it;
        
        if(videoFrame!=NULL)
        {
            if (videoFrame->data) {
                free(videoFrame->data);
                videoFrame->data = NULL;
            }
            
            delete videoFrame;
            videoFrame = NULL;
        }
    }
    
    mAnimatedFrames.clear();
    
    mFramesNum = 0;
}

bool AnimatedGif::loadAnimation()
{
    GifFile = DGifOpenFileName(mFilePath);
    
    if (GifFile==NULL) {
        LOGE("open gif fail!");
        return false;
    }
    
    if (GifFile->SHeight == 0 || GifFile->SWidth == 0) {
        
        DGifCloseFile(GifFile);
        GifFile = NULL;
        
        LOGE("Image of width or height 0");
        return false;
    }
    
    /*
     * Allocate the screen as vector of column of rows. Note this
     * screen is device independent - it's the screen defined by the
     * GIF file parameters.
     */
    ScreenBuffer = (GifRowType *)malloc(GifFile->SHeight * sizeof(GifRowType));
    int Size = GifFile->SWidth * sizeof(GifPixelType);/* Size in bytes one row.*/
    ScreenBuffer[0] = (GifRowType) malloc(Size);/* First row. */
    for (int i = 0; i < GifFile->SWidth; i++)  /* Set its color to BackGround. */
        ScreenBuffer[0][i] = GifFile->SBackGroundColor;
    for (int i = 1; i < GifFile->SHeight; i++) {
        /* Allocate the other rows, and set their color to background too: */
        ScreenBuffer[i] = (GifRowType) malloc(Size);
        memcpy(ScreenBuffer[i], ScreenBuffer[0], Size);
    }
    
    /* Scan the content of the GIF file and load the image(s) in: */
    GifRecordType RecordType = UNDEFINED_RECORD_TYPE;
    do{
        if (DGifGetRecordType(GifFile, &RecordType) == GIF_ERROR) {
            
            freeAnimation();
            
            LOGE("DGifGetRecordType Fail");
            
            return false;
        }
        
        if (RecordType==IMAGE_DESC_RECORD_TYPE) {
            if (DGifGetImageDesc(GifFile) == GIF_ERROR) {

                freeAnimation();
                
                LOGE("DGifGetImageDesc Fail");
                
                return false;
            }
            
            int Row = GifFile->Image.Top; /* Image Position relative to Screen. */
            int Col = GifFile->Image.Left;
            int Width = GifFile->Image.Width;
            int Height = GifFile->Image.Height;
            
            if (GifFile->Image.Left + GifFile->Image.Width > GifFile->SWidth ||
                GifFile->Image.Top + GifFile->Image.Height > GifFile->SHeight) {
                
                freeAnimation();
                
                LOGE("Image is not confined to screen dimension");
                
                return false;
            }
            
            int
            InterlacedOffset[] = { 0, 4, 2, 1 }, /* The way Interlaced image should. */
            InterlacedJumps[] = { 8, 8, 4, 2 };    /* be read - offsets and jumps... */
            
            if (GifFile->Image.Interlace) {
                /* Need to perform 4 passes on the images: */
                for (int i = 0; i < 4; i++)
                    for (int j = Row + InterlacedOffset[i]; j < Row + Height;
                         j += InterlacedJumps[i]) {
                        if (DGifGetLine(GifFile, &ScreenBuffer[j][Col],
                                        Width) == GIF_ERROR) {

                            freeAnimation();
                            
                            LOGE("DGifGetLine Fail");
                            
                            return false;
                        }
                    }
            }
            else {
                for (int i = 0; i < Height; i++) {
                    if (DGifGetLine(GifFile, &ScreenBuffer[Row++][Col],
                                    Width) == GIF_ERROR) {

                        freeAnimation();
                        
                        LOGE("DGifGetLine Fail");
                        
                        return false;
                    }
                }
            }
            
            /* Lets dump it - set the global variables required and do it: */
            ColorMap = (GifFile->Image.ColorMap
                        ? GifFile->Image.ColorMap
                        : GifFile->SColorMap);
            if (ColorMap == NULL) {

                freeAnimation();
                
                LOGE("Gif Image does not have a colormap\n");
                return false;
            }
            
            unsigned char *Buffer, *BufferP;
            int ScreenWidth = GifFile->SWidth;
            int ScreenHeight = GifFile->SHeight;
            GifRowType GifRow;
            GifColorType *ColorMapEntry;
            Buffer = (unsigned char *) malloc(ScreenWidth * ScreenHeight * 4);
            for (int i = 0; i < ScreenHeight; i++) {
                GifRow = ScreenBuffer[i];
                BufferP = Buffer + i * (ScreenWidth * 4);
                for (int j = 0; j < ScreenWidth; j++) {
                    if( trans_color != -1 && trans_color == GifRow[j] )
                    {
                        BufferP += 4;
                        continue;
                    }
                    
                    ColorMapEntry = &ColorMap->Colors[GifRow[j]];
                    *BufferP++ = ColorMapEntry->Red;
                    *BufferP++ = ColorMapEntry->Green;
                    *BufferP++ = ColorMapEntry->Blue;
                    *BufferP++ = 0xff;
                }
            }
            
            VideoFrame* gifFrame = new VideoFrame;
            gifFrame->width = ScreenWidth;
            gifFrame->height = ScreenHeight;
            gifFrame->duration = delay;
            gifFrame->data = Buffer;
            gifFrame->frameSize = ScreenWidth * ScreenHeight * 4;
            gifFrame->videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
            mAnimatedFrames.push_back(gifFrame);
            mFramesNum++;
            mAnimatedGifDuration += delay;

        }else if (RecordType == EXTENSION_RECORD_TYPE)
        {
            /* Skip any extension blocks in file: */
            int ExtCode;
            if (DGifGetExtension(GifFile, &ExtCode, &Extension) == GIF_ERROR) {
                
                freeAnimation();
                
                LOGE("DGifGetExtension Fail");
                
                return false;
            }
            
            if (Extension!=NULL && ExtCode==GRAPHICS_EXT_FUNC_CODE && Extension[0] == 4) {
                delay = (Extension[3] << 8 | Extension[2]) * 10;
                
                if(Extension[1] & 0x01)
                {
                    trans_color = Extension[4];
                }else{
                    trans_color = -1;
                }
            }
            
            while (Extension != NULL) {
                if (DGifGetExtensionNext(GifFile, &Extension) == GIF_ERROR) {

                    freeAnimation();
                    
                    LOGE("DGifGetExtensionNext Fail");
                    
                    return false;
                }
            }
            

        }
    }while (RecordType != TERMINATE_RECORD_TYPE);
    
    return true;
}

void AnimatedGif::freeAnimation()
{
    for (int i = 0; i < GifFile->SHeight; i++) {
        free(ScreenBuffer[i]);
    }
    free(ScreenBuffer);
    ScreenBuffer = NULL;
    
    DGifCloseFile(GifFile);
    GifFile = NULL;
}

VideoFrame* AnimatedGif::getAnimatedFrame(int64_t pts)
{
    int64_t animatedGifPts = pts%mAnimatedGifDuration;

    int foundIndex = 0;
    for (int i = 0; i<mFramesNum; i++) {
        animatedGifPts -= mAnimatedFrames[i]->duration;
        
        if (animatedGifPts<=0) {
            foundIndex = i;
            break;
        }
    }
    
    return mAnimatedFrames[foundIndex];
}
