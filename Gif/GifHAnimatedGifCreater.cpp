//
//  GifHAnimatedGifCreater.cpp
//  MediaStreamer
//
//  Created by Think on 2018/9/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "GifHAnimatedGifCreater.h"

#if defined(_MSC_VER) && _MSC_VER >= 1800
#define strdup _strdup
#endif

GifHAnimatedGifCreater::GifHAnimatedGifCreater(char* filePath)
{
    mFilePath = strdup(filePath);
    
    mWidth = 0;
    mHeight = 0;
    mDelay = 0;
}

GifHAnimatedGifCreater::~GifHAnimatedGifCreater()
{
    free(mFilePath);
}

bool GifHAnimatedGifCreater::open(int width, int height, int delay)
{
    mWidth = width;
    mHeight = height;
    mDelay = delay;
    
    return GifBegin(&gw, mFilePath, mWidth, mHeight, mDelay);
}

bool GifHAnimatedGifCreater::close()
{
    return GifEnd(&gw);
}

bool GifHAnimatedGifCreater::writeFrame(VideoFrame* rgba)
{
    if (rgba->width!=mWidth || rgba->height!=mHeight) {
        return false;
    }
    // delay : 单位是1/100秒钟 即100表示一秒
    return GifWriteFrame(&gw, rgba->data, mWidth, mHeight, rgba->duration/10);
}
