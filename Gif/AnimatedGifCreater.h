//
//  AnimatedGifCreater.h
//  MediaStreamer
//
//  Created by Think on 2017/6/20.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef AnimatedGifCreater_h
#define AnimatedGifCreater_h

#include <stdio.h>

#include "MediaDataType.h"

enum AnimatedGifCreaterType
{
    GIF_H = 0,
    GIFFLEN = 1,
    IOS_SYS = 2,
};

class AnimatedGifCreater {
public:
    virtual ~AnimatedGifCreater() {}

    static AnimatedGifCreater* CreateAnimatedGifCreater(AnimatedGifCreaterType type, char* filePath);
    static void DeleteAnimatedGifCreater(AnimatedGifCreater* gifCreater, AnimatedGifCreaterType type);
    
    virtual bool open(int width, int height, int delay) = 0;
    virtual bool close() = 0;
    virtual bool writeFrame(VideoFrame* rgba) = 0;
};

#endif /* AnimatedGifCreater_h */
