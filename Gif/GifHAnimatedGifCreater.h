//
//  GifHAnimatedGifCreater.hpp
//  MediaStreamer
//
//  Created by Think on 2018/9/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef GifHAnimatedGifCreater_h
#define GifHAnimatedGifCreater_h

#include <stdio.h>
#include "AnimatedGifCreater.h"

extern "C"
{
#include "gif.h"
}

class GifHAnimatedGifCreater : public AnimatedGifCreater{
public:
    GifHAnimatedGifCreater(char* filePath);
    ~GifHAnimatedGifCreater();
    
    bool open(int width, int height, int delay);
    bool close();
    bool writeFrame(VideoFrame* rgba);
private:
    char* mFilePath;
    
    int mWidth;
    int mHeight;
    int mDelay;
    
    GifWriter gw;
};

#endif /* GifHAnimatedGifCreater_h */
