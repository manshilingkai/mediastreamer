//
//  AnimatedGif.h
//  MediaStreamer
//
//  Created by Think on 2017/4/6.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef AnimatedGif_h
#define AnimatedGif_h

#include <stdio.h>

extern "C"
{
#include "gif_lib.h"
}

#include "MediaDataType.h"

class AnimatedGif {
public:
    AnimatedGif(char* filePath);
    ~AnimatedGif();
    
    bool loadAnimation();
    void freeAnimation();

    VideoFrame* getAnimatedFrame(int64_t pts);//ms
private:
    char* mFilePath;
    GifFileType *GifFile;
    GifRowType *ScreenBuffer;
    GifByteType *Extension;
    ColorMapObject *ColorMap;
    
    int delay;
    int trans_color;
    
    vector<VideoFrame*> mAnimatedFrames;
    int mFramesNum;
    
    int64_t mAnimatedGifDuration;
};

#endif /* AnimatedGif_h */
