//
//  AnimatedGifCreater.cpp
//  MediaStreamer
//
//  Created by Think on 2017/6/20.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "AnimatedGifCreater.h"
#include "MediaLog.h"

#include "GifHAnimatedGifCreater.h"
#include "GifflenAnimatedGifCreater.h"

AnimatedGifCreater* AnimatedGifCreater::CreateAnimatedGifCreater(AnimatedGifCreaterType type, char* filePath)
{
    if (type == GIF_H) {
        return new GifHAnimatedGifCreater(filePath);
    }
    
    if (type == GIFFLEN) {
        return new GifflenAnimatedGifCreater(filePath);
    }
    
    return NULL;
}

void AnimatedGifCreater::DeleteAnimatedGifCreater(AnimatedGifCreater* gifCreater, AnimatedGifCreaterType type)
{
    if (type == GIF_H) {
        GifHAnimatedGifCreater* gifHAnimatedGifCreater = (GifHAnimatedGifCreater*)gifCreater;
        if (gifHAnimatedGifCreater) {
            delete gifHAnimatedGifCreater;
            gifHAnimatedGifCreater = NULL;
        }
    }
    
    if (type == GIFFLEN) {
        GifflenAnimatedGifCreater* gifflenAnimatedGifCreater = (GifflenAnimatedGifCreater*)gifCreater;
        if (gifflenAnimatedGifCreater) {
            delete gifflenAnimatedGifCreater;
            gifflenAnimatedGifCreater = NULL;
        }
    }
}
