//
//  GifflenAnimatedGifCreater.cpp
//  MediaStreamer
//
//  Created by Think on 2018/9/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "GifflenAnimatedGifCreater.h"

#if defined(_MSC_VER) && _MSC_VER >= 1800
#define strdup _strdup
#endif

GifflenAnimatedGifCreater::GifflenAnimatedGifCreater(char* filePath)
{
    mFilePath = strdup(filePath);
    
    mWidth = 0;
    mHeight = 0;
    mDelay = 0;
}

GifflenAnimatedGifCreater::~GifflenAnimatedGifCreater()
{
    free(mFilePath);
}

bool GifflenAnimatedGifCreater::open(int width, int height, int delay)
{
    mWidth = width;
    mHeight = height;
    mDelay = delay;
    
    int ret = giffle_Giffle_Init(mFilePath, mWidth, mHeight, 256, 100, mDelay/10);
    if (ret!=0) {
        return false;
    }else return true;
}

bool GifflenAnimatedGifCreater::close()
{
    giffle_Giffle_Close();
    return true;
}

bool GifflenAnimatedGifCreater::writeFrame(VideoFrame* rgba)
{
    if (rgba->width!=mWidth || rgba->height!=mHeight) {
        return false;
    }
    
    int ret = giffle_Giffle_AddFrame(rgba->data);
    if (ret!=0) {
        return false;
    }else return true;
}
