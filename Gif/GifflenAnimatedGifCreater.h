//
//  GifflenAnimatedGifCreater.hpp
//  MediaStreamer
//
//  Created by Think on 2018/9/6.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef GifflenAnimatedGifCreater_hpp
#define GifflenAnimatedGifCreater_hpp

#include <stdio.h>

#include "AnimatedGifCreater.h"

#include "gifflen.h"

class GifflenAnimatedGifCreater : public AnimatedGifCreater{
public:
    GifflenAnimatedGifCreater(char* filePath);
    ~GifflenAnimatedGifCreater();
    
    bool open(int width, int height, int delay);
    bool close();
    bool writeFrame(VideoFrame* rgba);
private:
    char* mFilePath;
    
    int mWidth;
    int mHeight;
    int mDelay;
};

#endif /* GifflenAnimatedGifCreater_hpp */
