//
//  FFLog.cpp
//  MediaPlayer
//
//  Created by Think on 2017/9/1.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "FFLog.h"

void FFLog::setLogLevel(int level)
{
    av_log_set_level(level);
    
//#ifdef ANDROID
    av_log_set_callback(av_log_callback);
//#endif
}

void FFLog::av_log_callback(void *ptr, int level, const char *fmt, va_list vl)
{
    if (level>av_log_get_level()) return;
    
    va_list vl2;
    char line[1024];
    static int print_prefix = 1;
    
    va_copy(vl2, vl);
    av_log_format_line(ptr, level, fmt, vl2, line, sizeof(line), &print_prefix);
    va_end(vl2);
    
    if (level<=AV_LOG_ERROR) {
        LOGE("%s", line);
    }else if(level>AV_LOG_ERROR && level<=AV_LOG_WARNING) {
        LOGW("%s", line);
    }else if(level>AV_LOG_WARNING && level<=AV_LOG_INFO ) {
        LOGI("%s", line);
    }else if(level>AV_LOG_INFO && level<=AV_LOG_VERBOSE) {
        LOGV("%s", line);
    }else{
        LOGD("%s", line);
    }
}
