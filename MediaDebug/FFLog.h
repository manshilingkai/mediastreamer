//
//  FFLog.h
//  MediaPlayer
//
//  Created by Think on 2017/9/1.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef FFLog_h
#define FFLog_h

#include <stdio.h>
#include "MediaLog.h"

extern "C" {
#include "libavformat/avformat.h"
}

class FFLog {
public:
    static void setLogLevel(int level);
private:
    static void av_log_callback(void *ptr, int level, const char *fmt, va_list vl);
};

#endif /* FFLog_h */
