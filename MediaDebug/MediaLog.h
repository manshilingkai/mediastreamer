//
//  MediaLog.h
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MEDIA_LOG_H
#define MEDIA_LOG_H

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

// enable log or disable log
#define MEDIA_LOG_VERBOSE 1
#define MEDIA_LOG_DEBUG 1
#define MEDIA_LOG_INFO 1
#define MEDIA_LOG_WARN 1
#define MEDIA_LOG_ERROR 1

#ifdef ANDROID

#include <jni.h>
#include <android/log.h>

#ifndef LOG_TAG
#define LOG_TAG "MEDIA_LOG"
#endif

#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#else

#if MEDIA_LOG_VERBOSE
//#define LOGV(fmt, ...) printf(fmt "\n", ##__VA_ARGS__)
#define LOGV(fmt, ...) printf("%s(%d)-<%s>: " fmt "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__)
#else
#define LOGV(fmt, ...)
#endif

#if MEDIA_LOG_DEBUG
#define LOGD(fmt, ...) printf("%s(%d)-<%s>: " fmt "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__)
#else
#define LOGD(fmt, ...)
#endif

#if MEDIA_LOG_INFO
//#define LOGI(fmt, ...) printf(fmt "\n", ##__VA_ARGS__)
#define LOGI(fmt, ...) printf("%s(%d)-<%s>: " fmt "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__)
#else
#define LOGI(fmt, ...)
#endif

#if MEDIA_LOG_WARN
//#define LOGW(fmt, ...) printf(fmt "\n", ##__VA_ARGS__)
#define LOGW(fmt, ...) printf("%s(%d)-<%s>: " fmt "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__)
#else
#define LOGW(fmt, ...)
#endif

#if MEDIA_LOG_ERROR
//#define LOGE(fmt, ...) printf(fmt "\n", ##__VA_ARGS__)
#define LOGE(fmt, ...) printf("%s(%d)-<%s>: " fmt "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__)
#else
#define LOGE(fmt, ...)
#endif

#endif

// LOG_MULTIPLE_FILE
// LOG_SINGLE_FILE

#ifndef MINI_VERSION

#include "MediaFile.h"
#define MAX_MEDIA_LOG_LEN 1024

#ifdef LOG_MULTIPLE_FILE

class MediaLog {
public:
    MediaLog(char *backupDir);
    ~MediaLog();
    
    void writeLog(char* logStr);
private:
    pthread_mutex_t mLock;
    MediaFile* mMediaLogFile;
};

#else

class MediaLogLocker {
public:
    MediaLogLocker();
    ~MediaLogLocker();
    
    void Lock();
    void UnLock();
    
private:
    pthread_mutex_t mLock;
};

class MediaLog {
private:
    static MediaLogLocker mLocker;
    static MediaLog* mMediaLogInstance;
    MediaLog(char *backupDir);
public:
    static MediaLog* getInstance(char *backupDir);
    
    void writeLog(char* logStr);
    
    void flush();
    
    void checkSize();
private:
    pthread_mutex_t mWriteLock;
    FILE *mMediaLogFILE;
    
    char* mLogPath;
};

#endif

#endif

#endif //MEDIA_LOG_H
