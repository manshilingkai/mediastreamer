//
//  ColorSpaceConvert.cpp
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//
#include "ColorSpaceConvert.h"
#include "LibyuvColorSpaceConvert.h"

ColorSpaceConvert* ColorSpaceConvert::CreateColorSpaceConvert(ALGORITHM_SOURCE algorithmSource)
{
    if (algorithmSource==LIBYUV) {
        return new LibyuvColorSpaceConvert();
    }
    
    return NULL;
}

ColorSpaceConvert* ColorSpaceConvert::CreateColorSpaceConvert(ALGORITHM_SOURCE algorithmSource, int max_video_frame_size)
{
    if (algorithmSource==LIBYUV) {
        return new LibyuvColorSpaceConvert(max_video_frame_size);
    }
    
    return NULL;
}

void ColorSpaceConvert::DeleteColorSpaceConvert(ColorSpaceConvert* colorSpaceConvert, ALGORITHM_SOURCE algorithmSource)
{
    if (algorithmSource==LIBYUV) {
        LibyuvColorSpaceConvert* libyuvColorSpaceConvert = (LibyuvColorSpaceConvert*)colorSpaceConvert;
        delete libyuvColorSpaceConvert;
        libyuvColorSpaceConvert = NULL;
    }
}
