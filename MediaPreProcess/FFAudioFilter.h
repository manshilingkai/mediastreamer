//
//  FFAudioFilter.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef FFAudioFilter_h
#define FFAudioFilter_h

extern "C" {
#include "libavfilter/avfilter.h"
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"
#include "libavfilter/avfiltergraph.h"
    
#include "libavformat/avformat.h"
#include "libavutil/avstring.h"
#include "libavutil/opt.h"
}

#include <stdio.h>

#include "AudioFilter.h"

class FFAudioFilter : public AudioFilter {
public:
    FFAudioFilter();
    ~FFAudioFilter();

    bool open(char* afilters, uint64_t inChannelLayout, int inChannels, int inSampleRate, AVSampleFormat inSampleFormat, uint64_t outChannelLayout, int outChannels, int outSampleRate, AVSampleFormat outSampleFormat);
    void dispose();

    bool filterIn(AVFrame *inFrame);
    AVFrame * filterOut();
    
    AudioFrame* filterIO(AudioFrame* inFrame) {return NULL;}
    
private:
    uint64_t mInChannelLayout;
    int mInChannels;
    int mInSampleRate;
    AVSampleFormat mInSampleFormat;
    
    uint64_t mOutChannelLayout;
    int mOutChannels;
    int mOutSampleRate;
    AVSampleFormat mOutSampleFormat;
    
    AVFilterGraph *mGraph;
    
    AVFilterContext *in_audio_filter;   // the first filter in the audio chain
    AVFilterContext *out_audio_filter;  // the last filter in the audio chain
    
    bool configure_audio_filters(AVFilterGraph *graph);
    
    int configure_filtergraph(AVFilterGraph *graph, const char *filtergraph,
                              AVFilterContext *source_ctx, AVFilterContext *sink_ctx);
    
    char *mAfiltersDescripe;
    
    AVFrame *mOutFrame;
};

#endif /* FFAudioFilter_h */
