//
//  VoiceActivityDetection.h
//  MediaStreamer
//
//  Created by Think on 2019/12/10.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef VoiceActivityDetection_h
#define VoiceActivityDetection_h

#include <stdio.h>
#include "vad.h"
#include "resampler.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
#include <libavutil/avstring.h>

#include "libavutil/avhook.h"
}

class VoiceActivityDetection {
public:
    VoiceActivityDetection();
    ~VoiceActivityDetection();
    
    bool open();
    
    // returns              : 1 - (Active Voice),
    //                        0 - (Non-active Voice),
    //                       -1 - (Non Result)
    int process(int sampleRate, int channles, char* pcm_data, int pcm_size);
    
    void close();
private:
    VadInst *vadInst;
private:
    float* float_pcm_data;
    int float_pcm_data_size;
    int16_t* int16_pcm_data;
    int int16_pcm_data_size;
    
    musly::resampler* mResampler;
    int input_rate;
    int output_rate;
private:
    AVAudioFifo* mAudioProcessFifo;
    char* mOutputContainer;
    int mOutputContainerSize;
};

#endif /* VoiceActivityDetection_h */
