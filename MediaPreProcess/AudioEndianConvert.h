//
//  AudioEndianConvert.h
//  MediaStreamer
//
//  Created by Think on 2019/11/4.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef AudioEndianConvert_h
#define AudioEndianConvert_h

#ifdef WIN32
#pragma comment( lib, "Ws2_32.lib" )
#include <winsock.h>
#else
#include <netinet/in.h>
#endif

inline void big_endian_to_little_endian(int16_t* sample_data, int sample_num)
{
    for (int i = 0; i<sample_num; i++) {
        sample_data[i] = ntohs(sample_data[i]);
    }
}

#endif /* AudioEndianConvert_h */
