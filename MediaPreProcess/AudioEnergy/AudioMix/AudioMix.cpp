﻿#include "AudioMix.h"

namespace
{
	int32_t VolumeValueFix(int32_t iVol)
	{
		static const int32_t min_short = -32768;
		static const int32_t max_short = 32767;
		if (iVol > max_short)
		{
			iVol = max_short;
		}
		else if (iVol < min_short)
		{
			iVol = min_short;
		}
		return iVol;
	}
	int32_t Mix(short src1, short src2, short src3)
	{
		return VolumeValueFix(src1 + src2 + src3);
	}

	int32_t Mix(short src1, short src2)
	{
		return VolumeValueFix(src1 + src2);
	}
}

AudioMix::AudioMix(int micChannel, int bgmChannel, int accomChannel, int sampleRate, int samples, int bytesPerSample)
: m_micChannel(micChannel)
, m_bgmChannel(bgmChannel)
, m_accomChannel(accomChannel)
, m_sampleRate(sampleRate)
, m_samples(samples)
, m_bytesPerSample(bytesPerSample)
{
	m_mixOutBuffer = new short[2*samples];
	m_mixBgmAccomOutBuffer = new short[2 * samples];
	m_micRenderBuffer = new short[2 * samples];
	if (2 != m_micChannel)
	{
		m_micBuffer = new short[2 * samples];
	}
	if (2 != m_bgmChannel)
	{
		m_bgmBuffer = new short[2 * samples];
	}
	if (2 != m_accomChannel)
	{
		m_accomBuffer = new short[2 * samples];
	}
	m_render = new AudioRender(2, sampleRate, samples);

	audio_level = 0.0;
}

AudioMix::~AudioMix()
{
	delete[] m_mixOutBuffer;
	m_mixOutBuffer = nullptr;
	delete[] m_mixBgmAccomOutBuffer;
	m_mixBgmAccomOutBuffer = nullptr;
	delete[] m_micBuffer;
	m_micBuffer = nullptr;
	delete[] m_bgmBuffer;
	m_bgmBuffer = nullptr;
	delete[] m_accomBuffer;
	m_accomBuffer = nullptr;
	delete[] m_micRenderBuffer;
	m_micRenderBuffer = nullptr;
	delete m_render;
	m_render = nullptr;
}

void AudioMix::Process(char* micBuffer, char* bgmBuffer, char* accomBuffer, char** mixOutBuffer, char** mixBgmAccomOutBuffer)
{
	Process((short*)micBuffer, (short*)bgmBuffer, (short*)accomBuffer, (short**)mixOutBuffer, (short**)mixBgmAccomOutBuffer);
}

void AudioMix::Process(short* micBuffer, short* bgmBuffer, short* accomBuffer, short** mixOutBuffer, short** mixBgmAccomOutBuffer)
{
	short* micBuf = micBuffer;
	short* bgmBuf = bgmBuffer;
	short* accomBuf = accomBuffer;
	if (nullptr != micBuf && 2 != m_micChannel)
	{
		SingleChannelToDouble(micBuf, m_micBuffer);
		micBuf = m_micBuffer;
	}
	if (nullptr != bgmBuf && 2 != m_bgmChannel)
	{
		SingleChannelToDouble(bgmBuf, m_bgmBuffer);
		bgmBuf = m_bgmBuffer;
	}
	if (nullptr != accomBuf && 2 != m_accomChannel)
	{
		SingleChannelToDouble(accomBuf, m_accomBuffer);
		accomBuf = m_accomBuffer;
	}
	if (nullptr != micBuf)
	{
		micBuf = RenderMicAudio(micBuf);
	}

	int bufferLen = 2 * m_samples;
	for (int i = 0; i < bufferLen; i++) {
		short mic = VolumeValueFix(micBuf ? micBuf[i]*(m_micVolume/100.0) : 0);
		short bgm = VolumeValueFix(bgmBuf ? bgmBuf[i]*(m_bgmVolume/100.0) : 0);
		short accom = VolumeValueFix(accomBuf ? accomBuf[i]*(m_accomVolume/100.0) : 0);
		m_mixOutBuffer[i] = Mix(mic, bgm, accom);
		m_mixBgmAccomOutBuffer[i] = Mix(bgm, accom);
	}

	if (nullptr != mixOutBuffer)
	{
		*mixOutBuffer = m_mixOutBuffer;
	}
	if (nullptr != mixBgmAccomOutBuffer)
	{
		*mixBgmAccomOutBuffer = m_mixBgmAccomOutBuffer;
	}

	const double power_max = std::pow(10.0, -10 / 20.0);
	const double power_min = std::pow(10.0, -65 / 20.0);
	double level_frame = 0.0;
	for (int i = 0; i < bufferLen; ++i)
	{
		level_frame += std::pow((double)m_mixOutBuffer[i] / 32768.0, 2);
	}
	level_frame /= bufferLen;

	level_frame = (level_frame < power_max) ? level_frame : power_max;
	level_frame = (level_frame > power_min) ? level_frame : power_min;

	level_frame = (level_frame - power_min) / (power_max - power_min);
	level_frame = std::sqrt(level_frame);

	audio_level = (audio_level > level_frame) ? audio_level : level_frame;
}

void AudioMix::SetEffect(UserDefinedEffect userEffect)
{
	if (nullptr != m_render)
	{
		m_render->SetUserDefineEffect(userEffect);
	}
}

void AudioMix::SetEffect(EqualizerStyle eqStyle)
{
	if (nullptr != m_render)
	{
		m_render->SetEqualizerStyle(eqStyle);
	}
}

void AudioMix::SetEffect(ReverbStyle rebStyle)
{
	if (nullptr != m_render)
	{
		m_render->SetReverbStyle(rebStyle);
	}
}

void AudioMix::SetSoundTouch(int value)
{
	if (nullptr != m_render)
	{
		m_render->SetSoundTouch(value);
	}
}

void AudioMix::EnableAudioEffect(bool bEnable)
{
	if (nullptr != m_render)
	{
		m_render->EnableAudioEffect(bEnable);
	}
}

void AudioMix::EnableSoundTouch(bool bEnable)
{
	if (nullptr != m_render)
	{
		m_render->EnableSoundTouch(bEnable);
	}
}

double AudioMix::GetAverageData()
{
	double audio_level_temp = audio_level;
	audio_level = 0.0;

	return audio_level_temp;
}

void AudioMix::SetMicVolume(int iVol)
{
	m_micVolume = iVol;
}

void AudioMix::SetBgmVolume(int iVol)
{
	m_bgmVolume = iVol;
}

void AudioMix::SetAccomVolume(int iVol)
{
	m_accomVolume = iVol;
}

short* AudioMix::RenderMicAudio(short* micBuffer)
{
	memcpy(m_micRenderBuffer, micBuffer, 2 * m_samples * sizeof(short));
	//render
	if (nullptr != m_render)
	{
		m_render->RenderAudio((unsigned char*)m_micRenderBuffer);
	}
	return m_micRenderBuffer;
}

void AudioMix::SingleChannelToDouble(short* singleChannelBuf, short* doubleChannelBuf)
{
	for (int i = 0; i < m_samples; ++i)
	{
		doubleChannelBuf[i*2] = singleChannelBuf[i];
		doubleChannelBuf[i*2 + 1] = singleChannelBuf[i];
	}
}


