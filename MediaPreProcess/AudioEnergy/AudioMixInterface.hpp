//
//  AudioMixInterface.hpp
//  YppLife
//
//  Created by 胡润辰 on 2018/10/15.
//  Copyright © 2018 shanghai shanghai. All rights reserved.
//

#ifndef AudioMixInterface_hpp
#define AudioMixInterface_hpp

#include <stdio.h>

class AudioMix;

class AudioMixInterface {
public:
    AudioMixInterface(int channel=2, int sampleRate=44100, int samples=1024, int bytesPerSample=2);
    ~AudioMixInterface();

    void Process(char* micBuffer, char* bgmBuffer, char* accomBuffer, char** mixOutBuffer, char** mixBgmAccomOutBuffer);

    void setMicVolume(int volume);
    
    void setBgmVolume(int volume);
    
    void setEnviEffect(int effectIndex);
    
    void setEqualizerEffect(int effectIndex);
    
    void setReverbEffect(int effectIndex);
    
    double getSoundValue();
private:
    AudioMix *m_mix;
};

#endif /* AudioMixInterface_hpp */
