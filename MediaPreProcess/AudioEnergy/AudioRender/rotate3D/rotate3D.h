﻿/*
 *  Copyright (c) 2018 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

// Implement SRS effect

#ifndef ROTATE3D_H_
#define ROTATE3D_H_

#include "../fft/kiss_fft.h"
#include "hrtflib_rotate3D.h"

#define ANGLE_PER_CIRCLE (360.0)
#define GAIN_ROTATE3D (1.0)

static const float Time_RotatePerCircle = 20.0; // location of the sound source rotate 1 circle per 20 seconds.
static const float Interval_Partitions_Angle = ANGLE_PER_CIRCLE / NUM_PARTITIONS_ANGLE;
static const double Min_Distance_Partitions = ANGLE_PER_CIRCLE / NUM_PARTITIONS_ANGLE + 1e-6;

typedef struct {
    int num_channels;
    int sample_rate;
    int frame_length;

    double angle_interval;
    float angle_partitions[NUM_PARTITIONS_ANGLE][2];

    double angle_location;

    kiss_fft_cfg cfg_fft;
    kiss_fft_cfg cfg_ifft;

    kiss_fft_cpx fftbuffer_l[HRTF_LEN_ROTATE3D];
    kiss_fft_cpx fftbuffer_r[HRTF_LEN_ROTATE3D];

    float hrtf_l[HRTF_LEN_ROTATE3D /2+1][2];
    float hrtf_r[HRTF_LEN_ROTATE3D /2+1][2];

    float** buffer_input_block;
    float** buffer_output_block;

    float** output_previous;
}Rotate3DCore;

Rotate3DCore* InitRotate3D(int num_channels, int sample_rate, int frame_length);
int ProcessRotate3D(Rotate3DCore* rotate3D_state, float** input, float** output);
void DestoryRotate3D(Rotate3DCore** rotate3D_state);

#endif // ROTATE3D_H_