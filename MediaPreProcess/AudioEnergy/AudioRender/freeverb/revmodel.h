﻿
/*
 * Copyright (c) 2018 Qiu Mingjian. All Rights Reserved.
 * Implement Schroeder reverb algorithm
 * Reference: https://ccrma.stanford.edu/~jos/pasp/Freeverb.html.
 * Contact: qiumingjian@yupaopao.cn
 */

#ifndef __REVMODEL__
#define __REVMODEL__

#include "comb.h"
#include "allpass.h"

#define COMB_COUNT 8
#define ALLPASS_COUNT 4
#define STEREO_SPREAD 23
#define COMB_TUNNING_L1 1116
#define COMB_TUNNING_R1 (COMB_TUNNING_L1+STEREO_SPREAD)
#define COMB_TUNNING_L2 1188
#define COMB_TUNNING_R2 (COMB_TUNNING_L2+STEREO_SPREAD)
#define COMB_TUNNING_L3 1277
#define COMB_TUNNING_R3 (COMB_TUNNING_L3+STEREO_SPREAD)
#define COMB_TUNNING_L4 1356
#define COMB_TUNNING_R4 (COMB_TUNNING_L4+STEREO_SPREAD)
#define COMB_TUNNING_L5 1422
#define COMB_TUNNING_R5 (COMB_TUNNING_L5+STEREO_SPREAD)
#define COMB_TUNNING_L6 1491
#define COMB_TUNNING_R6 (COMB_TUNNING_L6+STEREO_SPREAD)
#define COMB_TUNNING_L7 1557
#define COMB_TUNNING_R7 (COMB_TUNNING_L7+STEREO_SPREAD)
#define COMB_TUNNING_L8 1617
#define COMB_TUNNING_R8 (COMB_TUNNING_L8+STEREO_SPREAD)
#define ALLPASS_TUNNING_L1 556
#define ALLPASS_TUNNING_R1 (ALLPASS_TUNNING_L1+STEREO_SPREAD)
#define ALLPASS_TUNNING_L2 441
#define ALLPASS_TUNNING_R2 (ALLPASS_TUNNING_L2+STEREO_SPREAD)
#define ALLPASS_TUNNING_L3 341
#define ALLPASS_TUNNING_R3 (ALLPASS_TUNNING_L3+STEREO_SPREAD)
#define ALLPASS_TUNNING_L4 225
#define ALLPASS_TUNNING_R4 (ALLPASS_TUNNING_L4+STEREO_SPREAD)

typedef struct 
{
    float gain;
    float roomsize,roomsize1;
    float damp,damp1;
    float wet,wet1,wet2;
    float dry;
    float width;
    float mode;

    // Comb filters
    CombState combL[COMB_COUNT];
    CombState combR[COMB_COUNT];

    // Allpass filters
    AllpassState allpassL[ALLPASS_COUNT];
    AllpassState allpassR[ALLPASS_COUNT];

    // Buffers for the combs
    float comb_buff_L1[COMB_TUNNING_L1];
    float comb_buff_R1[COMB_TUNNING_R1];
    float comb_buff_L2[COMB_TUNNING_L2];
    float comb_buff_R2[COMB_TUNNING_R2];
    float comb_buff_L3[COMB_TUNNING_L3];
    float comb_buff_R3[COMB_TUNNING_R3];
    float comb_buff_L4[COMB_TUNNING_L4];
    float comb_buff_R4[COMB_TUNNING_R4];
    float comb_buff_L5[COMB_TUNNING_L5];
    float comb_buff_R5[COMB_TUNNING_R5];
    float comb_buff_L6[COMB_TUNNING_L6];
    float comb_buff_R6[COMB_TUNNING_R6];
    float comb_buff_L7[COMB_TUNNING_L7];
    float comb_buff_R7[COMB_TUNNING_R7];
    float comb_buff_L8[COMB_TUNNING_L8];
    float comb_buff_R8[COMB_TUNNING_R8];

    // Buffers for the allpasses
    float allpass_buff_L1[ALLPASS_TUNNING_L1];
    float allpass_buff_R1[ALLPASS_TUNNING_R1];
    float allpass_buff_L2[ALLPASS_TUNNING_L2];
    float allpass_buff_R2[ALLPASS_TUNNING_R2];
    float allpass_buff_L3[ALLPASS_TUNNING_L3];
    float allpass_buff_R3[ALLPASS_TUNNING_R3];
    float allpass_buff_L4[ALLPASS_TUNNING_L4];
    float allpass_buff_R4[ALLPASS_TUNNING_R4];
}RevModel;

void InitReverbModel(RevModel* model);
void SetWetWeight(float value, RevModel* model);
void SetRoomSize(float value, RevModel* model);
void SetDryWeight(float value, RevModel* model);
void SetDampLevel(float value, RevModel* model);
void SetSpreadWidth(float value, RevModel* model);
void SetReverbMode(float value, RevModel* model);
void MuteReverb(RevModel* model);
void ProcessMix(float* inputL, float* inputR, float* outputL, float* outputR, int sample_cnt, int skip, RevModel* model);
void ProcessReplace(float* inputL, float* inputR, float* outputL, float* outputR, int sample_cnt, int skip, RevModel* model);
float GetRoomSize(RevModel* model);
float GetDampLevel(RevModel* model);
float GetWetWeight(RevModel* model);
float GetDryWeight(RevModel* model);
float GetSpreadWidth(RevModel* model);
float GetReverbMode(RevModel* model);
void UpdateReverb(RevModel* model);

#endif //__REVMODEL__
