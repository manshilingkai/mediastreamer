﻿
/*
 * Copyright (c) 2018 Qiu Mingjian. All Rights Reserved.
 * Implement Schroeder reverb algorithm
 * Reference: https://ccrma.stanford.edu/~jos/pasp/Freeverb.html.
 * Contact: qiumingjian@yupaopao.cn
 */

#include "comb.h"


void InitBaseComb(CombState* combfilter)
{
    combfilter->filterstore = 0;
    combfilter->bufidx = 0;
}

void SetCombBuffer(float* buf, int size, CombState* combfilter) 
{
    combfilter->buffer = buf; 
    combfilter->bufsize = size;
}

void MuteComb(CombState* combfilter)
{
    int idx=0;
    for (idx=0; idx<combfilter->bufsize; idx++)
        combfilter->buffer[idx]=0;
}

void SetCombDamp(float val, CombState* combfilter) 
{
    combfilter->damp1 = val; 
    combfilter->damp2 = 1-val;
}

float GetCombDamp(CombState* combfilter) 
{
    return combfilter->damp1;
}

void SetCombFeedback(float val, CombState* combfilter) 
{
    combfilter->feedback = val;
}

float GetCombFeedback(CombState* combfilter) 
{
    return combfilter->feedback;
}

float ProcessComb(float input, CombState* combfilter)
{
    float output;

    output = combfilter->buffer[combfilter->bufidx];
    UNDENORMALIZE(output);

    combfilter->filterstore = (output*combfilter->damp2) + (combfilter->filterstore*combfilter->damp1);
    UNDENORMALIZE(combfilter->filterstore);

    combfilter->buffer[combfilter->bufidx] = input + (combfilter->filterstore*combfilter->feedback);

    if(++combfilter->bufidx>=combfilter->bufsize) combfilter->bufidx = 0;

    return output;
}

