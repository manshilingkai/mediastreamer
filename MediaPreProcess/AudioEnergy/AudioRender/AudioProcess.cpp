#include "AudioProcess.h"

AudioProcess::AudioProcess(int channel, int rate, int samples)
{
#ifdef ANDROID
    pthread_mutex_init(&mut_audioEffect, NULL);
    pthread_mutex_init(&mut_soundtouch, NULL);

#endif
    
	_pAudio_effect_state = InitAudioEffects(channel, rate, samples);
	AudioProcess::AudioFrameInfo frameInfo;
	frameInfo.channel = channel;
	frameInfo.sample_rate = rate;
	frameInfo.samples = samples;
	InternalInit(frameInfo);
}

AudioProcess::~AudioProcess()
{
	InternalUnInit();
	m_soundTouchVal = 0;
	m_bEffect = false;
	m_bSoundTouch = false;
	m_useDefinedEffect = NOEFFECT;
	m_equelizerStyle = NOEQUALIZER;
	m_reverbStyle = NOREVERB;
    
#ifdef ANDROID
    pthread_mutex_destroy(&mut_audioEffect);
    pthread_mutex_destroy(&mut_soundtouch);
#endif
}

AudioProcess::AudioFrameInfo AudioProcess::getAudioFrameInfo()
{
    return _frameInfo;
}

bool AudioProcess::EnableAudioEffect(bool bEnable)
{
	m_bEffect = bEnable;
	return true;
}

bool AudioProcess::EnableSoundTouch(bool bEnable)
{
	m_bSoundTouch = bEnable;
	return true;
}

bool AudioProcess::RenderAudio(unsigned char* pFrameBuffer)
{
	if (NULL != _pAudio_effect_state)
	{
		if (m_bSoundTouch)
		{
			SoundTouchRender(pFrameBuffer);
		}
		if (m_bEffect)
		{
			EffectRender(pFrameBuffer);
		}
	}
	return true;
}

bool AudioProcess::SetUserDefineEffect(UserDefinedEffect userEffect)
{
#ifdef ANDROID
    AutoLock autoLock(&mut_audioEffect);
#else
    std::lock_guard<std::mutex> lk(mut_audioEffect);
#endif
	if (NULL == _pAudio_effect_state)
	{
		return false;
	}
	m_useDefinedEffect = userEffect;
	if (NOEFFECT != userEffect)
	{
		m_equelizerStyle = NOEQUALIZER;
		m_reverbStyle = NOREVERB;
	}
	
	SetUserDefinedEffects(userEffect, _pAudio_effect_state);
	return true;
}

bool AudioProcess::SetEqualizerStyle(EqualizerStyle userEffect)
{
#ifdef ANDROID
    AutoLock autoLock(&mut_audioEffect);
#else
    std::lock_guard<std::mutex> lk(mut_audioEffect);
#endif
    if (NULL == _pAudio_effect_state)
	{
		return false;
	}
	m_equelizerStyle = userEffect;
	if (NOEQUALIZER != userEffect)
	{
		m_useDefinedEffect = NOEFFECT;
	}

	SetEqualizer(userEffect, _pAudio_effect_state);
	return true;
}

bool AudioProcess::SetReverbStyle(ReverbStyle userEffect)
{
#ifdef ANDROID
    AutoLock autoLock(&mut_audioEffect);
#else
    std::lock_guard<std::mutex> lk(mut_audioEffect);
#endif
    
	if (NULL == _pAudio_effect_state)
	{
		return false;
	}
	m_reverbStyle = userEffect;
	if (NOREVERB != userEffect)
	{
		m_useDefinedEffect = NOEFFECT;
	}
	
	SetReverb(userEffect, _pAudio_effect_state);
	return true;
}


UserDefinedEffect AudioProcess::GetUserDefineEffect()
{
	return m_useDefinedEffect;
}


EqualizerStyle AudioProcess::GetEqualizerStyle()
{
	return m_equelizerStyle;
}


ReverbStyle AudioProcess::GetReverbStyle()
{
	return m_reverbStyle;
}

bool AudioProcess::SetSoundTouch(int value)
{
	if (value > 12)
	{
		value = 12;
	}
	if (value < -12)
	{
		value = -12;
	}
	m_soundTouchVal = value;
#ifdef ANDROID
    AutoLock autoLock(&mut_soundtouch);
#else
    std::lock_guard<std::mutex> lk(mut_soundtouch);
#endif
	m_soundTouch.setPitchSemiTones(value);
	return true;
}

int AudioProcess::GetSoundTouch()
{
	return m_soundTouchVal;
}

bool AudioProcess::InternalInit(const AudioProcess::AudioFrameInfo& frameInfo)
{
	_frameInfo = frameInfo;
	bool bRet = true;
	do
	{
		m_pEffectInputFrame = (float**)new char[sizeof(float*) * frameInfo.channel];
		if (NULL == m_pEffectInputFrame)
		{
			bRet = false;
			break;
		}
		m_pEffectOutFrame = (float**)new char[sizeof(float*) * frameInfo.channel];
		if (NULL == m_pEffectOutFrame)
		{
			bRet = false;
			break;
		}

		for (int i = 0; i < frameInfo.channel; ++i)
		{
			m_pEffectInputFrame[i] = new float[frameInfo.samples];
			m_pEffectOutFrame[i] = new float[frameInfo.samples];
		}

		m_pSoundTouchFrame = new float[frameInfo.samples*frameInfo.channel];
		if (NULL == m_pSoundTouchFrame)
		{
			bRet = false;
			break;
		}
		m_soundTouch.setChannels(frameInfo.channel);
		m_soundTouch.setSampleRate(frameInfo.sample_rate);
		m_soundTouch.setPitchSemiTones(0);
	} while (0);

	if (!bRet)
	{
		InternalUnInit();
	}
	return bRet;
}

bool AudioProcess::InternalUnInit()
{
	{
#ifdef ANDROID
        AutoLock autoLock(&mut_audioEffect);
#else
        std::lock_guard<std::mutex> lk(mut_audioEffect);
#endif
		if (NULL != _pAudio_effect_state)
		{
			DestoryAudioEffect(&_pAudio_effect_state);
			_pAudio_effect_state = NULL;
		}
		if (NULL != m_pEffectInputFrame)
		{
			for (int i = 0; i < _frameInfo.channel; ++i)
			{
				delete[] m_pEffectInputFrame[i];
			}
			delete[] m_pEffectInputFrame;
			m_pEffectInputFrame = NULL;
		}

		if (NULL != m_pEffectOutFrame)
		{
			for (int i = 0; i < _frameInfo.channel; ++i)
			{
				delete[] m_pEffectOutFrame[i];
			}
			delete[] m_pEffectOutFrame;
			m_pEffectOutFrame = NULL;
		}
	}

	{
#ifdef ANDROID
        AutoLock autoLock(&mut_soundtouch);
#else
        std::lock_guard<std::mutex> lk(mut_soundtouch);
#endif
		if (NULL != m_pSoundTouchFrame)
		{
			delete[] m_pSoundTouchFrame;
			m_pSoundTouchFrame = NULL;
		}
	}
	return true;
}

bool AudioProcess::EffectRender(unsigned char* pFrameBuffer)
{
#ifdef ANDROID
    AutoLock autoLock(&mut_audioEffect);
#else
    std::lock_guard<std::mutex> lk(mut_audioEffect);
#endif
	if (NULL == _pAudio_effect_state)
	{
		return false;
	}
	if (NULL == pFrameBuffer)
	{
		return false;
	}
	short* pFrameBuf = (short*)pFrameBuffer;
	for (int i = 0; i < _frameInfo.samples; ++i) {
		for (int j = 0; j < _frameInfo.channel; ++j){
			m_pEffectInputFrame[j][i] = (float)pFrameBuf[_frameInfo.channel * i + j];
		}
	}
	
	if (0 != ProcessAudioEffects(_pAudio_effect_state, m_pEffectInputFrame, m_pEffectOutFrame, _frameInfo.samples))
	{
		return false;
	}
	
	for (int i = 0; i < _frameInfo.samples; ++i)
		for (int j = 0; j < _frameInfo.channel; ++j) {
			pFrameBuf[_frameInfo.channel * i + j] = (short)m_pEffectOutFrame[j][i];
		}
	return true;
}

bool AudioProcess::SoundTouchRender(unsigned char* pFrameBuffer)
{
#ifdef ANDROID
    AutoLock autoLock(&mut_soundtouch);
#else
    std::lock_guard<std::mutex> lk(mut_soundtouch);
#endif
	if (NULL == m_pSoundTouchFrame)
	{
		return false;
	}
	short* pFrameBuf = (short*)pFrameBuffer;
	for (int i = 0; i < _frameInfo.samples*_frameInfo.channel; ++i)
	{
		m_pSoundTouchFrame[i] = pFrameBuf[i];
	}

	m_soundTouch.putSamples((const soundtouch::SAMPLETYPE *)m_pSoundTouchFrame, _frameInfo.samples);
	m_soundTouch.receiveSamples((soundtouch::SAMPLETYPE *)m_pSoundTouchFrame, _frameInfo.samples);

	for (int i = 0; i < _frameInfo.samples*_frameInfo.channel; ++i)
	{
		pFrameBuf[i] = m_pSoundTouchFrame[i];
	}
	return true;
}
