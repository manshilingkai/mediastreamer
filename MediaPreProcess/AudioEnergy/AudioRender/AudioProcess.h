#pragma once
#include "audio_effects.h"
#include "SoundTouch/SoundTouch.h"

#ifdef ANDROID
#include "AutoLock.h"
#else
#include <mutex>
#endif

#define STEREO (2)
#define SAMPLE_RATE (44100)
#define FRAME_LENGTH (882)
#define REVERB_STYLE (CONCERT)

class AudioProcess
{
public:
	typedef struct AudioFrameInfo
	{
		int channel;
		int samples;
		int sample_rate;
	};
	AudioProcess(int channel, int rate, int samples);
	~AudioProcess();
    AudioProcess::AudioFrameInfo getAudioFrameInfo();
	bool EnableAudioEffect(bool bEnable);
	bool EnableSoundTouch(bool bEnable);
	bool RenderAudio(unsigned char* pFrameBuffer);
	bool SetUserDefineEffect(UserDefinedEffect userEffect);
	bool SetEqualizerStyle(EqualizerStyle userEffect);
	bool SetReverbStyle(ReverbStyle userEffect);
	UserDefinedEffect GetUserDefineEffect();
	EqualizerStyle    GetEqualizerStyle();
	ReverbStyle       GetReverbStyle();
	//-12~+12
	bool SetSoundTouch(int value);
	int  GetSoundTouch();

private:
	bool InternalInit(const AudioProcess::AudioFrameInfo& frameInfo);
	bool InternalUnInit();
	bool EffectRender(unsigned char* pFrameBuffer);
	bool SoundTouchRender(unsigned char* pFrameBuffer);
private:

	AudioEffectState* _pAudio_effect_state = NULL;
	AudioProcess::AudioFrameInfo _frameInfo;
    
#ifdef ANDROID
    pthread_mutex_t mut_audioEffect;
#else
    std::mutex mut_audioEffect;
#endif
    
	float** m_pEffectInputFrame = NULL;
	float** m_pEffectOutFrame = NULL;
	bool m_bEffect = false;

	soundtouch::SoundTouch m_soundTouch;
    
#ifdef ANDROID
    pthread_mutex_t mut_soundtouch;
#else
    std::mutex mut_soundtouch;
#endif
    
	bool m_bSoundTouch = false;
	float* m_pSoundTouchFrame = NULL;

	int m_soundTouchVal = 0;
	UserDefinedEffect m_useDefinedEffect = NOEFFECT;
	EqualizerStyle    m_equelizerStyle = NOEQUALIZER;
	ReverbStyle       m_reverbStyle = NOREVERB;
};
