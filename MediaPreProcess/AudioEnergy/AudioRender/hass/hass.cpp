﻿/*
 *  Copyright (c) 2018 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "hass.h"

/*
 * Init a hass effect instance
 */
HassCore* InitHassCore(int num_channels, int sample_rate)
{
    if (num_channels != 2) {
        printf("invalid parameter for Hass: num_channels must be 2(stereo).\n");
        return NULL;
    }
    if ((sample_rate != 44100) && (sample_rate != 48000)) {
        printf("invalid parameter for Hass: sample_rate must be either 44.1kHz or 48kHz.\n");
        return NULL;
    }

    int i;
    HassCore* hass_state = NULL;
    hass_state = (HassCore*)malloc(sizeof(HassCore));
    if (hass_state == NULL) {
        printf("creat hass state failed.\n");
        goto fail;
    }
    hass_state->buffer_right_channel = NULL;

    hass_state->num_channels = num_channels;
    hass_state->sample_rate = sample_rate;
    hass_state->buffer_size = (long)floor(sample_rate * DELAY_TIME);

    hass_state->buffer_right_channel = CreatQueue();
    if (hass_state->buffer_right_channel == NULL) {
        printf("creat queue failed in InitHassCore().\n");
        goto fail;
    }

    for (i = 0; i < hass_state->buffer_size; ++i) {
        PushQueue(hass_state->buffer_right_channel, .0f);
    }

    return hass_state;

fail:
    DestoryHass(&hass_state);
    return NULL;
}

/*
 *  Release memory allocated by InitHassCore()
 */
void DestoryHass(HassCore** hass_state)
{
    HassCore* p_hass_state = *hass_state;
    if (p_hass_state == NULL) {
        return;
    }

    if(p_hass_state->buffer_right_channel != NULL)
        DestoryQueue(p_hass_state->buffer_right_channel);
  
    free(p_hass_state);
    *hass_state = NULL;
}

/*
 * Run Hass effect
 */
int ProcessHass(HassCore* hass_state, float** input, float** output, int num_samples)
{
    long buffer_size = hass_state->buffer_size;
    int idx;
    float data;

    for (idx = 0; idx < num_samples; ++idx) {
        data = PopQueue(hass_state->buffer_right_channel);

        output[0][idx] = input[0][idx];
        output[1][idx] = data;

        // update buffer_right_channel
        PushQueue(hass_state->buffer_right_channel, input[1][idx]);
    }

    return 0;
}

