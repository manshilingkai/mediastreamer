﻿/*
 *  Copyright (c) 2018 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

#include <stdlib.h>
#include <stdio.h>
#include "queue.h"

/*
 * Creat an empty queue
 */
BufferQueue* CreatQueue()
{
    BufferQueue* pbuffer;
    pbuffer = (BufferQueue*)malloc(sizeof(BufferQueue));
    if (pbuffer == NULL) return NULL;

    pbuffer->front = NULL;
    pbuffer->rear = NULL;
    pbuffer->size = 0;

    return pbuffer;
}

/*
 * Check whether the queue is empty
 */
int IsEmptyQueue(BufferQueue* p)        
{
    if (p->front == NULL && p->rear == NULL)
        return 1;
    return 0;
}

/*
 * Push data to queue
 */
void PushQueue(BufferQueue* buffer_queue, float data)           
{
    BufferNode* node = (BufferNode*)malloc(sizeof(BufferNode));
    node->data = data;
    node->next = NULL;
    if (IsEmptyQueue(buffer_queue))
        buffer_queue->front = buffer_queue->rear = node;
    else
    {
        buffer_queue->rear->next = node;
        buffer_queue->rear = node;
    }
    buffer_queue->size++;
}

/*
 * Pop data from queue
 */
float PopQueue(BufferQueue* buffer_queue)           
{
    float data;
    if (buffer_queue->front->next == NULL) {
        printf("buffer_queue seems not be created.\n");
        return .0f;
    }

    BufferNode* node;
    node = buffer_queue->front;
    data = node->data;
    buffer_queue->front = node->next;
    if (buffer_queue->rear == node)
        buffer_queue->rear = NULL;

    buffer_queue->size--;
    free(node);
    return data;
}

/*
 * Release memory allocated by CreatQueue()
 */
void DestoryQueue(BufferQueue* buffer_queue)     
{
    while (buffer_queue->front){
        buffer_queue->rear = buffer_queue->front->next;
        free(buffer_queue->front);
        buffer_queue->front = buffer_queue->rear;
    }
}
