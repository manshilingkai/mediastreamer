﻿/*
 *  Copyright (c) 2018 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

// Define buffer queue for Hass.

#ifndef QUEUE_H_
#define QUEUE_H_

typedef struct BUFFERNODE {
    float data;
    struct BUFFERNODE* next;
}BufferNode;

typedef struct {
    BufferNode* front;
    BufferNode* rear;
    int size;
}BufferQueue;

BufferQueue* CreatQueue();
int IsEmptyQueue(BufferQueue* p);
void PushQueue(BufferQueue* buffer_queue, float data);
float PopQueue(BufferQueue* buffer_queue);
void DestoryQueue(BufferQueue* buffer_queue);

#endif // QUEUE_H_