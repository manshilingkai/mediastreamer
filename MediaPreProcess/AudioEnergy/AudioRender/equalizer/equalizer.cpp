﻿/*
 *  Copyright (c) 2018 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "../common_audio_effects.h"
#include "equalizer.h"

double Filter(EqualizerCore* equalizer_state, int index_channel, int index_band);
void UpdateOutBuffer(EqualizerCore* equalizer_state, double new_out, int index_channel, int index_band);

/*
 * Initialize an Equalizer instance
 */
EqualizerCore* InitEqualizer(int num_channels, int sample_rate)
{
    if ((sample_rate != 44100) && (sample_rate != 48000)) {
        printf("invalid parameter for Equalizer: sample_rate must be either 44.1kHz or 48kHz.\n");
        return NULL;
    }
    if (num_channels != 2) {
        printf("invalid parameter for Equalizer: num_channels must be 2(stereo).\n");
        return NULL;
    }

    EqualizerCore* equalizer_state = NULL;
    equalizer_state = (EqualizerCore*)malloc(sizeof(EqualizerCore));
    if (equalizer_state == NULL) {
        return NULL;
    }

    equalizer_state->num_channels = num_channels;
    equalizer_state->sample_rate = sample_rate;
    memset(&equalizer_state->buffer_in[0], 0, sizeof(equalizer_state->buffer_in));
    memset(&equalizer_state->buffer_out[0], 0, sizeof(equalizer_state->buffer_out));

    int i, j;
    // init equalizer coefficients
    if (sample_rate == 44100) {
        for (i = 0; i < NUM_BANDS_EQ; ++i) {
            for (j = 0; j < 7; j++) {
                equalizer_state->equalizer_coeffs[i][0][j] = Equalizer_Coeffs_Base_44100[i][0][j];
                equalizer_state->equalizer_coeffs[i][1][j] = Equalizer_Coeffs_Base_44100[i][1][j];
            }
        }
    }
    else { 
        for (i = 0; i < NUM_BANDS_EQ; ++i) {
            for (j = 0; j < 7; j++) {
                equalizer_state->equalizer_coeffs[i][0][j] = Equalizer_Coeffs_Base_48000[i][0][j];
                equalizer_state->equalizer_coeffs[i][1][j] = Equalizer_Coeffs_Base_48000[i][1][j];
            }
        }
    } 

    return equalizer_state;
}

/*
 * Run ButterWorth filter
 */
double Filter(EqualizerCore* equalizer_state, int index_channel, int index_band)
{
    double r = 0;

    // lowpass filter order = 3
    if (index_band == 0) {
        r = equalizer_state->equalizer_coeffs[index_band][0][0] * equalizer_state->buffer_in[index_channel][0] + equalizer_state->equalizer_coeffs[index_band][0][1] * equalizer_state->buffer_in[index_channel][1] + equalizer_state->equalizer_coeffs[index_band][0][2] * equalizer_state->buffer_in[index_channel][2] + equalizer_state->equalizer_coeffs[index_band][0][3] * equalizer_state->buffer_in[index_channel][3]
            - equalizer_state->equalizer_coeffs[index_band][1][1] * equalizer_state->buffer_out[index_channel][index_band][0] - equalizer_state->equalizer_coeffs[index_band][1][2] * equalizer_state->buffer_out[index_channel][index_band][1] - equalizer_state->equalizer_coeffs[index_band][1][3] * equalizer_state->buffer_out[index_channel][index_band][2];
    }
    // highpass filter order = 4
    else if (index_band == 9) {
        r = equalizer_state->equalizer_coeffs[index_band][0][0] * equalizer_state->buffer_in[index_channel][0] + equalizer_state->equalizer_coeffs[index_band][0][1] * equalizer_state->buffer_in[index_channel][1] + equalizer_state->equalizer_coeffs[index_band][0][2] * equalizer_state->buffer_in[index_channel][2] + equalizer_state->equalizer_coeffs[index_band][0][3] * equalizer_state->buffer_in[index_channel][3] + equalizer_state->equalizer_coeffs[index_band][0][4] * equalizer_state->buffer_in[index_channel][4]
            - equalizer_state->equalizer_coeffs[index_band][1][1] * equalizer_state->buffer_out[index_channel][index_band][0] - equalizer_state->equalizer_coeffs[index_band][1][2] * equalizer_state->buffer_out[index_channel][index_band][1] - equalizer_state->equalizer_coeffs[index_band][1][3] * equalizer_state->buffer_out[index_channel][index_band][2] - equalizer_state->equalizer_coeffs[index_band][1][4] * equalizer_state->buffer_out[index_channel][index_band][3];
    }
    // bandpass filter order = 3
    else {
        r = equalizer_state->equalizer_coeffs[index_band][0][0] * equalizer_state->buffer_in[index_channel][0] + equalizer_state->equalizer_coeffs[index_band][0][1] * equalizer_state->buffer_in[index_channel][1] + equalizer_state->equalizer_coeffs[index_band][0][2] * equalizer_state->buffer_in[index_channel][2] + equalizer_state->equalizer_coeffs[index_band][0][3] * equalizer_state->buffer_in[index_channel][3] + equalizer_state->equalizer_coeffs[index_band][0][4] * equalizer_state->buffer_in[index_channel][4] + equalizer_state->equalizer_coeffs[index_band][0][5] * equalizer_state->buffer_in[index_channel][5] + equalizer_state->equalizer_coeffs[index_band][0][6] * equalizer_state->buffer_in[index_channel][6]
            - equalizer_state->equalizer_coeffs[index_band][1][1] * equalizer_state->buffer_out[index_channel][index_band][0] - equalizer_state->equalizer_coeffs[index_band][1][2] * equalizer_state->buffer_out[index_channel][index_band][1] - equalizer_state->equalizer_coeffs[index_band][1][3] * equalizer_state->buffer_out[index_channel][index_band][2] - equalizer_state->equalizer_coeffs[index_band][1][4] * equalizer_state->buffer_out[index_channel][index_band][3] - equalizer_state->equalizer_coeffs[index_band][1][5] * equalizer_state->buffer_out[index_channel][index_band][4] - equalizer_state->equalizer_coeffs[index_band][1][6] * equalizer_state->buffer_out[index_channel][index_band][5];
    }

    return r;
}

/*
 * Update buffer_out for each filter
 */
void UpdateOutBuffer(EqualizerCore* equalizer_state, double new_out, int index_channel, int index_band)
{
    int i;
    if (index_band == 0) {
        for (i = 2; i > 0; --i) {
            equalizer_state->buffer_out[index_channel][index_band][i] = equalizer_state->buffer_out[index_channel][index_band][i-1];
        }
        equalizer_state->buffer_out[index_channel][index_band][0] = new_out;
    }
    else if (index_band == 9) {
        for (i = 3; i > 0; --i) {
            equalizer_state->buffer_out[index_channel][index_band][i] = equalizer_state->buffer_out[index_channel][index_band][i - 1];
        }
        equalizer_state->buffer_out[index_channel][index_band][0] = new_out;
    }
    else {
        for (i = 5; i > 0; --i) {
            equalizer_state->buffer_out[index_channel][index_band][i] = equalizer_state->buffer_out[index_channel][index_band][i - 1];
        }
        equalizer_state->buffer_out[index_channel][index_band][0] = new_out;
    }
}

/*
 * Run equalizer module
 */
int ProcessEqualizer(EqualizerCore* equalizer_state, float** input_block, float** output_block, int num_samples)
{
    int idx_channel, idx_band;
    int i, j;
    double y_band, y;

    for (idx_channel = 0; idx_channel < equalizer_state->num_channels; ++idx_channel)
    {
        for (i = 0; i < num_samples; ++i){
            // update buffer_in
            for (j = BUFFER_IN_LEN - 1; j > 0; --j) {
                equalizer_state->buffer_in[idx_channel][j] = equalizer_state->buffer_in[idx_channel][j - 1];
            }
            equalizer_state->buffer_in[idx_channel][0] = (float)input_block[idx_channel][i];

            y = 0;
            for (idx_band = 0; idx_band < NUM_BANDS_EQ; ++idx_band) {
                // filtering
                y_band = Filter(equalizer_state, idx_channel, idx_band);

                // update buffer_out
                UpdateOutBuffer(equalizer_state, y_band, idx_channel, idx_band);

                y += y_band;
            }

            y = AMPLITUDE_SATURATE(y*GAIN_EQ_FULLBAND);
            output_block[idx_channel][i] = (float)y;
        }
    }
   
    return 0;
}

/*
 * Release memory allocated by InitEqualizer()
 */
void DestoryEqualizer(EqualizerCore** equalizer_state)
{
    EqualizerCore* equalizer_state_p = *equalizer_state;
    if (equalizer_state_p == NULL)
        return;

    free(equalizer_state_p);
    *equalizer_state = NULL;
}


void SetEqualizerParameters(float* gain_array, EqualizerCore* equalizer_state)
{
    //for (int i = 0; i < NUM_BANDS_EQ; ++i) {
    //    for (int j = 0; j < 7; j++) {
    //        equalizer_state->equalizer_coeffs[i][0][j] *= gain_array[i];
    //    }
    //}

    int i, j;

    if (equalizer_state->sample_rate == 44100) {
        for (i = 0; i < NUM_BANDS_EQ; ++i) {
            for (j = 0; j < 7; j++) {
                equalizer_state->equalizer_coeffs[i][0][j] = Equalizer_Coeffs_Base_44100[i][0][j] * gain_array[i];
                equalizer_state->equalizer_coeffs[i][1][j] = Equalizer_Coeffs_Base_44100[i][1][j];
            }
        }
    }
    else {
        for (i = 0; i < NUM_BANDS_EQ; ++i) {
            for (j = 0; j < 7; j++) {
                equalizer_state->equalizer_coeffs[i][0][j] = Equalizer_Coeffs_Base_48000[i][0][j] * gain_array[i];
                equalizer_state->equalizer_coeffs[i][1][j] = Equalizer_Coeffs_Base_48000[i][1][j];
            }
        }
    }

}