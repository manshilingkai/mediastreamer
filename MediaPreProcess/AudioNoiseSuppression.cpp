//
//  AudioNoiseSuppression.cpp
//  MediaStreamer
//
//  Created by Think on 2019/12/9.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "AudioNoiseSuppression.h"

AudioNoiseSuppression::AudioNoiseSuppression(int num_channels, int sample_rate)
{
    mNumChannels = num_channels;
    mSampleRate = sample_rate;
    
    mNoiseSuppression = new NoiseSuppression(num_channels, sample_rate);
    mAudioProcessFifo = av_audio_fifo_alloc(AV_SAMPLE_FMT_S16, num_channels, 1);
    mOutputContainerSize = sample_rate*num_channels*2;
    mOutputContainer = (char*)malloc(mOutputContainerSize);
}

AudioNoiseSuppression::~AudioNoiseSuppression()
{
    if (mNoiseSuppression) {
        delete mNoiseSuppression;
        mNoiseSuppression = NULL;
    }
    
    if (mAudioProcessFifo) {
        av_audio_fifo_free(mAudioProcessFifo);
        mAudioProcessFifo = NULL;
    }
    
    if (mOutputContainer) {
        free(mOutputContainer);
        mOutputContainer = NULL;
    }
}

int AudioNoiseSuppression::process(char* in_pcm_data, int in_pcm_size, char** out_pcm_data)
{
    if (in_pcm_data==NULL || in_pcm_size<=0) return 0;
    
    av_audio_fifo_write(mAudioProcessFifo, (void**)&in_pcm_data, in_pcm_size/mNumChannels/2);
    int available_samples = av_audio_fifo_size(mAudioProcessFifo);
    if (available_samples<mNoiseSuppression->GetFrameSize()) {
        return 0;
    }
    
    for (int i = 0; i<available_samples/mNoiseSuppression->GetFrameSize(); i++) {
        char* pOutputContainer = mOutputContainer+i*mNoiseSuppression->GetFrameSize()*mNumChannels*2;
        av_audio_fifo_read(mAudioProcessFifo, (void**)&pOutputContainer, mNoiseSuppression->GetFrameSize());
        mNoiseSuppression->ImplementNoiseSuppression((short *)pOutputContainer, mNoiseSuppression->GetFrameSize());
    }
    
    *out_pcm_data = mOutputContainer;
    
    return (available_samples/mNoiseSuppression->GetFrameSize())*mNoiseSuppression->GetFrameSize() * mNumChannels * 2;
}
