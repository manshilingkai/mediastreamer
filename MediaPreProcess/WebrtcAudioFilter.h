//
//  WebrtcAudioFilter.h
//  MediaStreamer
//
//  Created by Think on 2017/2/9.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef WebrtcAudioFilter_h
#define WebrtcAudioFilter_h

#include <stdio.h>

#include "AudioFilter.h"

#include "noise_suppression_x.h"

class WebrtcAudioFilter : public AudioFilter {
public:
    WebrtcAudioFilter();
    ~WebrtcAudioFilter();
    
    //"NS,AEC,AGC,"
    bool open(char* afilters, uint64_t inChannelLayout, int inChannels, int inSampleRate, AVSampleFormat inSampleFormat, uint64_t outChannelLayout, int outChannels, int outSampleRate, AVSampleFormat outSampleFormat);
    void dispose();
    
    bool filterIn(AVFrame *inFrame) {return false;}
    AVFrame * filterOut() {return NULL;}
    
    AudioFrame* filterIO(AudioFrame* inFrame);
private:
    NsxHandle* nsxhandle;
    
    AudioFrame* mOutputAudioFrame;
};

#endif /* WebrtcAudioFilter_h */
