//
//  LibyuvColorSpaceConvert.cpp
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "LibyuvColorSpaceConvert.h"
#include "libyuv.h"
#include <stddef.h>
#include "MediaLog.h"
#include "Overlay.h"

LibyuvColorSpaceConvert::LibyuvColorSpaceConvert(int max_video_frame_size)
{
    mVideoFrame1 = new VideoFrame;
    mVideoFrame1->data = (uint8_t*)malloc(max_video_frame_size);
    mVideoFrame1->frameSize = max_video_frame_size;
    
    mVideoFrame2 = new VideoFrame;
    mVideoFrame2->data = (uint8_t*)malloc(max_video_frame_size);
    mVideoFrame2->frameSize = max_video_frame_size;
}

LibyuvColorSpaceConvert::LibyuvColorSpaceConvert()
{
    int max_video_frame_size = MAX_VIDEO_FRAME_SIZE;
    mVideoFrame1 = new VideoFrame;
    mVideoFrame1->data = (uint8_t*)malloc(max_video_frame_size);
    mVideoFrame1->frameSize = max_video_frame_size;
    
    mVideoFrame2 = new VideoFrame;
    mVideoFrame2->data = (uint8_t*)malloc(max_video_frame_size);
    mVideoFrame2->frameSize = max_video_frame_size;
}

LibyuvColorSpaceConvert::~LibyuvColorSpaceConvert()
{
    if (mVideoFrame1!=NULL) {
        if (mVideoFrame1->data!=NULL) {
            free(mVideoFrame1->data);
            mVideoFrame1->data = NULL;
        }
        
        delete mVideoFrame1;
        mVideoFrame1 = NULL;
    }
    
    if (mVideoFrame2!=NULL) {
        if (mVideoFrame2->data!=NULL) {
            free(mVideoFrame2->data);
            mVideoFrame2->data = NULL;
        }
        
        delete mVideoFrame2;
        mVideoFrame2 = NULL;
    }
}

bool LibyuvColorSpaceConvert::NV21toI420(VideoFrame* nv21, VideoFrame* i420)
{
    i420->frameSize = nv21->frameSize;
    i420->pts = nv21->pts;
    
    uint8* src_y = nv21->data;
    int src_stride_y = nv21->width;
    uint8* src_vu = src_y + src_stride_y * nv21->height;
    int src_stride_vu = nv21->width;
    
    uint8* dst_y = i420->data;
    int dst_stride_y = i420->width;
    uint8* dst_u = dst_y + dst_stride_y * i420->height;
    int dst_stride_u = (i420->width+1)/2;
    uint8* dst_v = dst_u + dst_stride_u * i420->height/2;
    int dst_stride_v = (i420->width+1)/2;
    
    int ret = libyuv::NV21ToI420(src_y, src_stride_y,
               src_vu, src_stride_vu,
               dst_y, dst_stride_y,
               dst_u, dst_stride_u,
               dst_v, dst_stride_v,
               i420->width, i420->height);
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::NV21toI420_Rotation(VideoFrame* nv21, VideoFrame* i420, int rotation)
{
    mVideoFrame1->frameSize = nv21->frameSize;
    mVideoFrame1->pts = nv21->pts;
    mVideoFrame1->width = nv21->width;
    mVideoFrame1->height = nv21->height;
    
    this->NV21toI420(nv21, mVideoFrame1);
    
    uint8 *src_y = mVideoFrame1->data;
    int src_stride_y = mVideoFrame1->width;
    uint8 *src_u = src_y + src_stride_y * mVideoFrame1->height;
    int src_stride_u = (mVideoFrame1->width+1)/2;
    uint8 *src_v = src_u + src_stride_u * mVideoFrame1->height/2;
    int src_stride_v = (mVideoFrame1->width+1)/2;
    uint8* dst_y = i420->data;
    int dst_stride_y = i420->width;
    uint8* dst_u = dst_y + dst_stride_y * i420->height;
    int dst_stride_u = (i420->width+1)/2;
    uint8* dst_v = dst_u + dst_stride_u * i420->height/2;
    int dst_stride_v = (i420->width+1)/2;
    int src_width = mVideoFrame1->width;
    int src_height = mVideoFrame1->height;
    
    i420->frameSize = mVideoFrame1->frameSize;
    i420->pts = mVideoFrame1->pts;
    
    int ret = libyuv::I420Rotate(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, dst_y, dst_stride_y, dst_u, dst_stride_u, dst_v, dst_stride_v, src_width, src_height, libyuv::RotationMode(rotation));
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::NV21toI420_Crop_Rotation(VideoFrame* nv21, VideoFrame* i420, int rotation)
{
    i420->pts = nv21->pts;
    
    int i420_width_pre_rotation= 0;
    int i420_height_pre_rotation = 0;
    
    if (rotation==0 || rotation==180) {
        i420_width_pre_rotation = i420->width;
        i420_height_pre_rotation = i420->height;
    }else if(rotation==90 || rotation==270) {
        i420_width_pre_rotation = i420->height;
        i420_height_pre_rotation = i420->width;
    }
    
    if (nv21->width<i420_width_pre_rotation || nv21->height<i420_height_pre_rotation) {
        return false;
    }

    uint8* src_frame = nv21->data;
    size_t src_size = nv21->frameSize;
    uint8* dst_y = i420->data;
    int dst_stride_y = i420->width;
    uint8* dst_u = dst_y + dst_stride_y * i420->height;
    int dst_stride_u = (i420->width+1)/2;
    uint8* dst_v = dst_u + dst_stride_u * i420->height/2;
    int dst_stride_v = (i420->width+1)/2;
    int crop_x;
    int crop_y;
    int src_width = nv21->width;
    int src_height = nv21->height;
    int crop_width;
    int crop_height;
    uint32 format = libyuv::FOURCC_NV21;
    
    if (rotation==0 || rotation==180) {
        crop_x = (nv21->width - i420->width)/2;
        crop_y = (nv21->height - i420->height)/2;
        
        crop_width = i420->width;
        crop_height = i420->height;
    }else if(rotation==90 || rotation==270) {
        crop_x = (nv21->width - i420->height)/2;
        crop_y = (nv21->height - i420->width)/2;
        
        crop_width = i420->height;
        crop_height = i420->width;
    }else {
        crop_x = 0;
        crop_y = 0;
        
        crop_width = src_width;
        crop_height = src_height;
    }
    
    int ret = libyuv::ConvertToI420(src_frame, src_size,
                      dst_y, dst_stride_y,
                      dst_u, dst_stride_u,
                      dst_v, dst_stride_v,
                      crop_x, crop_y,
                      src_width, src_height,
                      crop_width, crop_height,
                      libyuv::RotationMode(rotation),
                      format);
    
    if (ret) return false;
   
    return true;
}

bool LibyuvColorSpaceConvert::NV21toI420_Crop_Rotation_Scale(VideoFrame* nv21, VideoFrame* i420, int rotation)
{
    i420->pts = nv21->pts;
    
    int i420_width_pre_rotation= 0;
    int i420_height_pre_rotation = 0;
    
    if (rotation==0 || rotation==180) {
        i420_width_pre_rotation = i420->width;
        i420_height_pre_rotation = i420->height;
    }else if(rotation==90 || rotation==270) {
        i420_width_pre_rotation = i420->height;
        i420_height_pre_rotation = i420->width;
    }
    
//    if (nv21->width>=i420_width_pre_rotation && nv21->height>=i420_height_pre_rotation) {
//        return NV21toI420_Crop_Rotation(nv21, i420, rotation);
//    }
    
    if(nv21->width*i420_height_pre_rotation>=i420_width_pre_rotation*nv21->height)
    {
        if (rotation==0 || rotation==180) {
            mVideoFrame1->width = i420_width_pre_rotation*nv21->height/i420_height_pre_rotation;
            mVideoFrame1->height = nv21->height;
        }else if(rotation==90 || rotation==270) {
            mVideoFrame1->width = nv21->height;
            mVideoFrame1->height = i420_width_pre_rotation*nv21->height/i420_height_pre_rotation;
        }

    }else if(nv21->width*i420_height_pre_rotation<i420_width_pre_rotation*nv21->height)
    {
        if (rotation==0 || rotation==180) {
            mVideoFrame1->width = nv21->width;
            mVideoFrame1->height = i420_height_pre_rotation*nv21->width/i420_width_pre_rotation;
        }else if(rotation==90 || rotation==270) {
            mVideoFrame1->width = i420_height_pre_rotation*nv21->width/i420_width_pre_rotation;
            mVideoFrame1->height = nv21->width;
        }
    }
    mVideoFrame1->width = mVideoFrame1->width%2>0?mVideoFrame1->width+1:mVideoFrame1->width;
    mVideoFrame1->height = mVideoFrame1->height%2>0?mVideoFrame1->height+1:mVideoFrame1->height;
    
    mVideoFrame1->frameSize = mVideoFrame1->width*mVideoFrame1->height*3/2;
    mVideoFrame1->pts = nv21->pts;
    
    bool bRet = NV21toI420_Crop_Rotation(nv21, mVideoFrame1, rotation);
    
    if (!bRet) return false;
    
    //do scale
    uint8 *src_y = mVideoFrame1->data;
    int src_stride_y = mVideoFrame1->width;
    uint8 *src_u = src_y + src_stride_y * mVideoFrame1->height;
    int src_stride_u = (mVideoFrame1->width+1)/2;
    uint8 *src_v = src_u + src_stride_u * mVideoFrame1->height/2;
    int src_stride_v = (mVideoFrame1->width+1)/2;
    int src_width = mVideoFrame1->width;
    int src_height = mVideoFrame1->height;
    uint8* dst_y = i420->data;
    int dst_stride_y = i420->width;
    uint8* dst_u = dst_y + dst_stride_y * i420->height;
    int dst_stride_u = (i420->width+1)/2;
    uint8* dst_v = dst_u + dst_stride_u * i420->height/2;
    int dst_stride_v = (i420->width+1)/2;
    int dst_width = i420->width;
    int dst_height = i420->height;
    
    int ret = libyuv::I420Scale(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, src_width, src_height, dst_y, dst_stride_y, dst_u, dst_stride_u, dst_v, dst_stride_v, dst_width, dst_height, libyuv::kFilterBox);
    
    if (ret) return false;
    
    return true;

}

bool LibyuvColorSpaceConvert::NV21toI420_Crop_Rotation_Scale_Mirror(VideoFrame* nv21, VideoFrame* i420, int rotation)
{
    i420->pts = nv21->pts;
    
    mVideoFrame2->width = i420->width;
    mVideoFrame2->height = i420->height;
    mVideoFrame2->frameSize = mVideoFrame2->width*mVideoFrame2->height*3/2;
    
    bool bRet = NV21toI420_Crop_Rotation_Scale(nv21, mVideoFrame2, rotation);
    if (!bRet) return false;
    
    uint8 *src_y = mVideoFrame2->data;
    int src_stride_y = mVideoFrame2->width;
    uint8 *src_u = src_y + src_stride_y * mVideoFrame2->height;
    int src_stride_u = (mVideoFrame2->width+1)/2;
    uint8 *src_v = src_u + src_stride_u * mVideoFrame2->height/2;
    int src_stride_v = (mVideoFrame2->width+1)/2;
    uint8* dst_y = i420->data;
    int dst_stride_y = i420->width;
    uint8* dst_u = dst_y + dst_stride_y * i420->height;
    int dst_stride_u = (i420->width+1)/2;
    uint8* dst_v = dst_u + dst_stride_u * i420->height/2;
    int dst_stride_v = (i420->width+1)/2;
    int width = i420->width;
    int height = i420->height;
    
    int ret = libyuv::I420Mirror(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, dst_y, dst_stride_y, dst_u, dst_stride_u, dst_v, dst_stride_v, width, height);
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::NV21toNV12_Crop_Rotation_Scale(VideoFrame* nv21, VideoFrame* nv12, int rotation)
{
    nv12->pts = nv21->pts;
    
    mVideoFrame2->width = nv12->width;
    mVideoFrame2->height = nv12->height;
    mVideoFrame2->frameSize = mVideoFrame2->width*mVideoFrame2->height*3/2;
    
    bool bRet = NV21toI420_Crop_Rotation_Scale(nv21, mVideoFrame2, rotation);
    if (!bRet) return false;
    
    uint8 *src_y = mVideoFrame2->data;
    int src_stride_y = mVideoFrame2->width;
    uint8 *src_u = src_y + src_stride_y * mVideoFrame2->height;
    int src_stride_u = (mVideoFrame2->width+1)/2;
    uint8 *src_v = src_u + src_stride_u * mVideoFrame2->height/2;
    int src_stride_v = (mVideoFrame2->width+1)/2;
    
    uint8* dst_y = nv12->data;
    int dst_stride_y = nv12->width;
    uint8* dst_uv = dst_y + dst_stride_y * nv12->height;
    int dst_stride_uv = nv12->width;
    int width = nv12->width;
    int height = nv12->height;

    int ret = libyuv::I420ToNV12(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, dst_y, dst_stride_y, dst_uv, dst_stride_uv, width, height);
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::NV21toNV21_Crop_Rotation_Scale(VideoFrame* nv21, VideoFrame* outNV21, int rotation)
{
    outNV21->pts = nv21->pts;
    
    mVideoFrame2->width = outNV21->width;
    mVideoFrame2->height = outNV21->height;
    mVideoFrame2->frameSize = mVideoFrame2->width*mVideoFrame2->height*3/2;
    
    bool bRet = NV21toI420_Crop_Rotation_Scale(nv21, mVideoFrame2, rotation);
    if (!bRet) return false;
    
    uint8 *src_y = mVideoFrame2->data;
    int src_stride_y = mVideoFrame2->width;
    uint8 *src_u = src_y + src_stride_y * mVideoFrame2->height;
    int src_stride_u = (mVideoFrame2->width+1)/2;
    uint8 *src_v = src_u + src_stride_u * mVideoFrame2->height/2;
    int src_stride_v = (mVideoFrame2->width+1)/2;
    
    uint8* dst_y = outNV21->data;
    int dst_stride_y = outNV21->width;
    uint8* dst_uv = dst_y + dst_stride_y * outNV21->height;
    int dst_stride_uv = outNV21->width;
    int width = outNV21->width;
    int height = outNV21->height;
    
    int ret = libyuv::I420ToNV21(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, dst_y, dst_stride_y, dst_uv, dst_stride_uv, width, height);
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::ABGRtoI420_Crop(VideoFrame* abgr, VideoFrame* i420)
{
    i420->pts = abgr->pts;
    
    const uint8 *src_frame = abgr->data;
    size_t src_size = abgr->frameSize;
    uint8 *dst_y = i420->data;
    int dst_stride_y = i420->width;
    uint8 *dst_u = dst_y + dst_stride_y * i420->height;
    int dst_stride_u = (i420->width+1)/2;
    uint8 *dst_v = dst_u + dst_stride_u * i420->height/2;
    int dst_stride_v = (i420->width+1)/2;
    int crop_x = 0;
    int crop_y = 0;
    int src_width = abgr->width;
    int src_height = abgr->height;
    int crop_width = i420->width;
    int crop_height = i420->height;
    uint32 format = libyuv::FOURCC_ABGR;
    
    crop_x = (abgr->width - i420->width)/2;
    crop_y = (abgr->height - i420->height)/2;
    
    crop_width = i420->width;
    crop_height = i420->height;
    
    int ret = libyuv::ConvertToI420(src_frame, src_size, dst_y, dst_stride_y, dst_u, dst_stride_u, dst_v, dst_stride_v, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::kRotate0, format);
    
    if (ret) return false;
    
    return true;
}

/*
bool LibyuvColorSpaceConvert::BGRAtoBGRA_Crop_Scale(VideoFrame* in_bgra, VideoFrame* out_bgra)
{
    out_bgra->pts = in_bgra->pts;
    
    mVideoFrame1->frameSize = in_bgra->frameSize;
    mVideoFrame1->pts = in_bgra->pts;
    mVideoFrame1->width = in_bgra->width;
    mVideoFrame1->height = in_bgra->height;
    
    const uint8 *src_frame = in_bgra->data;
    int src_stride_frame = in_bgra->width*4;
    uint8 *dst_argb = mVideoFrame1->data;
    int dst_stride_argb = mVideoFrame1->width*4;
    int width = mVideoFrame1->width;
    int height = mVideoFrame1->height;
    
    int ret = libyuv::BGRAToARGB(src_frame, src_stride_frame, dst_argb, dst_stride_argb, width, height);
    
    if (ret) return false;
    
    mVideoFrame2->frameSize = out_bgra->frameSize;
    mVideoFrame2->width = out_bgra->width;
    mVideoFrame2->height = out_bgra->height;
    
    const uint8 *src_argb = mVideoFrame1->data;
    int src_stride_argb = mVideoFrame1->width*4;
    int src_width = mVideoFrame1->width;
    int src_height = mVideoFrame1->height;
    uint8 *dst_argb_2 = mVideoFrame2->data;
    int dst_stride_argb_2 = mVideoFrame2->width*4;
    int dst_width = mVideoFrame2->width;
    int dst_height = mVideoFrame2->height;
    int clip_x;
    int clip_y;
    int clip_width;
    int clip_height;
    if(src_width*dst_height>dst_width*src_height)
    {
        clip_width = dst_width*src_height/dst_height;
        clip_height = src_height;
        
        clip_x = (src_width - clip_width)/2;
        clip_y = 0;
        
    }else if(src_width*dst_height<dst_width*src_height)
    {
        clip_width = src_width;
        clip_height = dst_height*src_width/dst_width;
        
        clip_x = 0;
        clip_y = (src_height - clip_height)/2;
    }else {
        clip_width = src_width;
        clip_height = src_height;
        clip_x = 0;
        clip_y = 0;
    }
    clip_width = clip_width%2>0?clip_width+1:clip_width;
    clip_height = clip_height%2>0?clip_height+1:clip_height;
    
    ret = libyuv::ARGBScaleClip(src_argb, src_stride_argb, src_width, src_height, dst_argb_2, dst_stride_argb_2, dst_width, dst_height, clip_x, clip_y, clip_width, clip_height, libyuv::kFilterBox);
    
    if (ret) return false;
    
    const uint8 *src_argb_3 = mVideoFrame2->data;
    int src_stride_argb_3 = mVideoFrame2->width*4;
    uint8 *dst_bgra_3 = out_bgra->data;
    int dst_stride_bgra_3 = out_bgra->width*4;
    int width_3 = out_bgra->width;
    int height_3 = out_bgra->height;
    
    ret = libyuv::ARGBToBGRA(src_argb_3, src_stride_argb_3, dst_bgra_3, dst_stride_bgra_3, width_3, height_3);
    
    if (ret) return false;
    
    return true;
}
*/

bool LibyuvColorSpaceConvert::BGRAtoARGB_Crop_Scale(VideoFrame* in_bgra, VideoFrame* out_argb)
{
    out_argb->pts = in_bgra->pts;
    
    const uint8 *src_frame = in_bgra->data;
    size_t src_size = in_bgra->frameSize;
    
    int src_width = in_bgra->width;
    int src_height = in_bgra->height;
    
    int dst_width = out_argb->width;
    int dst_height = out_argb->height;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    if(src_width*dst_height>dst_width*src_height)
    {
        crop_width = dst_width*src_height/dst_height;
        crop_height = src_height;
        
        crop_x = (src_width - crop_width)/2;
        crop_y = 0;
        
    }else if(src_width*dst_height<dst_width*src_height)
    {
        crop_width = src_width;
        crop_height = dst_height*src_width/dst_width;
        
        crop_x = 0;
        crop_y = (src_height - crop_height)/2;
    }else {
        crop_width = src_width;
        crop_height = src_height;
        crop_x = 0;
        crop_y = 0;
    }
    
    mVideoFrame1->frameSize = crop_width*crop_height*4;
    mVideoFrame1->width = crop_width;
    mVideoFrame1->height = crop_height;
    
    uint8 *dst_argb = mVideoFrame1->data;
    int dst_stride_argb = mVideoFrame1->width*4;
    
    int ret = libyuv::ConvertToARGB(src_frame, src_size, dst_argb, dst_stride_argb, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::RotationMode(0), libyuv::FOURCC_BGRA);
    
    if (ret) return false;
    
    const uint8 *src_argb = mVideoFrame1->data;
    int src_stride_argb = mVideoFrame1->width*4;
    int src_width_1 = mVideoFrame1->width;
    int src_height_1 = mVideoFrame1->height;
    uint8 *dst_argb_1 = out_argb->data;
    int dst_stride_argb_1 = out_argb->width*4;
    int dst_width_1 = out_argb->width;
    int dst_height_1 = out_argb->height;
    ret = libyuv::ARGBScale(src_argb, src_stride_argb, src_width_1, src_height_1, dst_argb_1, dst_stride_argb_1, dst_width_1, dst_height_1, libyuv::kFilterBox);
    
    if (ret) return false;
    
    return true;

}

bool LibyuvColorSpaceConvert::BGRAtoBGRA_Crop_Scale(VideoFrame* in_bgra, VideoFrame* out_bgra)
{
    out_bgra->pts = in_bgra->pts;
    
    mVideoFrame2->frameSize = out_bgra->frameSize;
    mVideoFrame2->width = out_bgra->width;
    mVideoFrame2->height = out_bgra->height;
    
    bool bret = BGRAtoARGB_Crop_Scale(in_bgra, mVideoFrame2);
    
    if(!bret) return false;
    
    const uint8 *src_argb = mVideoFrame2->data;
    int src_stride_argb = mVideoFrame2->width*4;
    uint8 *dst_bgra = out_bgra->data;
    int dst_stride_bgra = out_bgra->width*4;
    int width = out_bgra->width;
    int height = out_bgra->height;
    int iret = libyuv::ARGBToBGRA(src_argb, src_stride_argb, dst_bgra, dst_stride_bgra, width, height);
    
    if (iret) return false;
    
    return true;
}


bool LibyuvColorSpaceConvert::I420toARGB_Crop_Scale(VideoFrame* in_i420, VideoFrame* out_argb)
{
    out_argb->pts = in_i420->pts;
    
    const uint8 *src_frame = in_i420->data;
    size_t src_size = in_i420->frameSize;
    
    int src_width = in_i420->width;
    int src_height = in_i420->height;
    
    int dst_width = out_argb->width;
    int dst_height = out_argb->height;
    
    int crop_x;
    int crop_y;

    int crop_width;
    int crop_height;
    
    if(src_width*dst_height>dst_width*src_height)
    {
        crop_width = dst_width*src_height/dst_height;
        crop_height = src_height;
        
        crop_x = (src_width - crop_width)/2;
        crop_y = 0;
        
    }else if(src_width*dst_height<dst_width*src_height)
    {
        crop_width = src_width;
        crop_height = dst_height*src_width/dst_width;
        
        crop_x = 0;
        crop_y = (src_height - crop_height)/2;
    }else {
        crop_width = src_width;
        crop_height = src_height;
        crop_x = 0;
        crop_y = 0;
    }
    
    mVideoFrame1->frameSize = crop_width*crop_height*4;
    mVideoFrame1->width = crop_width;
    mVideoFrame1->height = crop_height;
    
    uint8 *dst_argb = mVideoFrame1->data;
    int dst_stride_argb = mVideoFrame1->width*4;
    
    int ret = libyuv::ConvertToARGB(src_frame, src_size, dst_argb, dst_stride_argb, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::RotationMode(in_i420->rotation), libyuv::FOURCC_I420);
    
    if (ret) return false;
    
    const uint8 *src_argb = mVideoFrame1->data;
    int src_stride_argb = mVideoFrame1->width*4;
    int src_width_1 = mVideoFrame1->width;
    int src_height_1 = mVideoFrame1->height;
    uint8 *dst_argb_1 = out_argb->data;
    int dst_stride_argb_1 = out_argb->width*4;
    int dst_width_1 = out_argb->width;
    int dst_height_1 = out_argb->height;
    ret = libyuv::ARGBScale(src_argb, src_stride_argb, src_width_1, src_height_1, dst_argb_1, dst_stride_argb_1, dst_width_1, dst_height_1, libyuv::kFilterBox);
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::I420toARGB_Crop_Rotation_Scale(VideoFrame* in_i420, VideoFrame* out_argb, int rotation)
{
    out_argb->pts = in_i420->pts;
    
    const uint8 *src_frame = in_i420->data;
    size_t src_size = in_i420->frameSize;
    
    int src_width = in_i420->width;
    int src_height = in_i420->height;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    int dst_width_pre_rotation= 0;
    int dst_height_pre_rotation = 0;
    
    if (rotation==90 || rotation==270) {
        dst_width_pre_rotation = out_argb->height;
        dst_height_pre_rotation = out_argb->width;
    }else{
        dst_width_pre_rotation = out_argb->width;
        dst_height_pre_rotation = out_argb->height;
    }
    
    if(src_width*dst_height_pre_rotation>dst_width_pre_rotation*src_height)
    {
        crop_width = dst_width_pre_rotation*src_height/dst_height_pre_rotation;
        crop_height = src_height;
        
        crop_x = (src_width - crop_width)/2;
        crop_y = 0;
    }else if(src_width*dst_height_pre_rotation<dst_width_pre_rotation*src_height)
    {
        crop_width = src_width;
        crop_height = dst_height_pre_rotation*src_width/dst_width_pre_rotation;
        
        crop_x = 0;
        crop_y = (src_height - crop_height)/2;
    }else {
        crop_width = src_width;
        crop_height = src_height;
        crop_x = 0;
        crop_y = 0;
    }
    
    mVideoFrame1->frameSize = crop_width*crop_height*4;
    
    if (rotation==90 || rotation==270) {
        mVideoFrame1->width = crop_height;
        mVideoFrame1->height = crop_width;
    }else{
        mVideoFrame1->width = crop_width;
        mVideoFrame1->height = crop_height;
    }
    
    uint8 *dst_argb = mVideoFrame1->data;
    int dst_stride_argb = mVideoFrame1->width*4;
    
    int ret = libyuv::ConvertToARGB(src_frame, src_size, dst_argb, dst_stride_argb, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::RotationMode(rotation), libyuv::FOURCC_I420);
    
    if (ret) return false;
    
    const uint8 *src_argb = mVideoFrame1->data;
    int src_stride_argb = mVideoFrame1->width*4;
    int src_width_1 = mVideoFrame1->width;
    int src_height_1 = mVideoFrame1->height;
    uint8 *dst_argb_1 = out_argb->data;
    int dst_stride_argb_1 = out_argb->width*4;
    int dst_width_1 = out_argb->width;
    int dst_height_1 = out_argb->height;
    ret = libyuv::ARGBScale(src_argb, src_stride_argb, src_width_1, src_height_1, dst_argb_1, dst_stride_argb_1, dst_width_1, dst_height_1, libyuv::kFilterBox);
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::I420toBGRA_Crop_Scale(VideoFrame* in_i420, VideoFrame* out_bgra)
{
    out_bgra->pts = in_i420->pts;
    
    mVideoFrame2->frameSize = out_bgra->frameSize;
    mVideoFrame2->width = out_bgra->width;
    mVideoFrame2->height = out_bgra->height;
    
    bool bret = I420toARGB_Crop_Scale(in_i420, mVideoFrame2);
    
    if(!bret) return false;
    
    const uint8 *src_argb = mVideoFrame2->data;
    int src_stride_argb = mVideoFrame2->width*4;
    uint8 *dst_bgra = out_bgra->data;
    int dst_stride_bgra = out_bgra->width*4;
    int width = out_bgra->width;
    int height = out_bgra->height;
    int iret = libyuv::ARGBToBGRA(src_argb, src_stride_argb, dst_bgra, dst_stride_bgra, width, height);
    
    if (iret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::I420toRGBA_Crop_Scale(VideoFrame* in_i420, VideoFrame* out_rgba)
{
    out_rgba->pts = in_i420->pts;
    
    mVideoFrame2->frameSize = out_rgba->frameSize;
    mVideoFrame2->width = out_rgba->width;
    mVideoFrame2->height = out_rgba->height;
    
    bool bret = I420toARGB_Crop_Scale(in_i420, mVideoFrame2);
    
    if(!bret) return false;
    
    const uint8 *src_argb = mVideoFrame2->data;
    int src_stride_argb = mVideoFrame2->width*4;
    uint8 *dst_rgba = out_rgba->data;
    int dst_stride_rgba = out_rgba->width*4;
    int width = out_rgba->width;
    int height = out_rgba->height;
    int iret = libyuv::ARGBToRGBA(src_argb, src_stride_argb, dst_rgba, dst_stride_rgba, width, height);
    
    if (iret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::I420toABGR_Crop_Rotation_Scale(VideoFrame* in_i420, VideoFrame* out_abgr, int rotation)
{
    out_abgr->pts = in_i420->pts;
    
    mVideoFrame2->frameSize = out_abgr->frameSize;
    mVideoFrame2->width = out_abgr->width;
    mVideoFrame2->height = out_abgr->height;
    
    bool bret = I420toARGB_Crop_Rotation_Scale(in_i420, mVideoFrame2, rotation);
    
    if(!bret) return false;
    
    const uint8 *src_argb = mVideoFrame2->data;
    int src_stride_argb = mVideoFrame2->width*4;
    uint8 *dst_abgr = out_abgr->data;
    int dst_stride_abgr = out_abgr->width*4;
    int width = out_abgr->width;
    int height = out_abgr->height;
    int iret = libyuv::ARGBToABGR(src_argb, src_stride_argb, dst_abgr, dst_stride_abgr, width, height);
    
    if (iret) return false;
    
    return true;

}

bool LibyuvColorSpaceConvert::I420toI420_Crop_Scale(VideoFrame* in_i420, VideoFrame* out_i420)
{
    out_i420->pts = in_i420->pts;
    
    if(in_i420->width*out_i420->height>out_i420->width*in_i420->height)
    {
        mVideoFrame1->width = out_i420->width*in_i420->height/out_i420->height;
        mVideoFrame1->height = in_i420->height;
        
    }else if(in_i420->width*out_i420->height<out_i420->width*in_i420->height)
    {
        mVideoFrame1->width = in_i420->width;
        mVideoFrame1->height = out_i420->height*in_i420->width/out_i420->width;
    }
    else {
        mVideoFrame1->width = in_i420->width;
        mVideoFrame1->height = in_i420->height;
    }
    
    mVideoFrame1->width = mVideoFrame1->width%2>0?mVideoFrame1->width+1:mVideoFrame1->width;
    mVideoFrame1->height = mVideoFrame1->height%2>0?mVideoFrame1->height+1:mVideoFrame1->height;
    
    mVideoFrame1->frameSize = mVideoFrame1->width*mVideoFrame1->height*3/2;
    
    const uint8 *src_frame = in_i420->data;
    size_t src_size = in_i420->frameSize;
    uint8 *dst_y = mVideoFrame1->data;
    int dst_stride_y = mVideoFrame1->width;
    uint8 *dst_u = dst_y + dst_stride_y * mVideoFrame1->height;
    int dst_stride_u = mVideoFrame1->width/2;
    uint8 *dst_v = dst_u + dst_stride_u * mVideoFrame1->height/2;
    int dst_stride_v = mVideoFrame1->width/2;
    int crop_x = (in_i420->width - mVideoFrame1->width)/2;
    int crop_y = (in_i420->height - mVideoFrame1->height)/2;
    int src_width = in_i420->width;
    int src_height = in_i420->height;
    int crop_width = mVideoFrame1->width;
    int crop_height = mVideoFrame1->height;
    uint32 format = libyuv::FOURCC_I420;
    
    int ret = libyuv::ConvertToI420(src_frame, src_size, dst_y, dst_stride_y, dst_u, dst_stride_u, dst_v, dst_stride_v, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::kRotate0, format);
    
    if (ret) return false;
    
    const uint8 *src_y = mVideoFrame1->data;
    int src_stride_y = mVideoFrame1->width;
    const uint8 *src_u = src_y + src_stride_y * mVideoFrame1->height;
    int src_stride_u = mVideoFrame1->width/2;
    const uint8 *src_v = src_u + src_stride_u * mVideoFrame1->height/2;
    int src_stride_v = mVideoFrame1->width/2;
    int src_width_1 = mVideoFrame1->width;
    int src_height_1 = mVideoFrame1->height;
    uint8 *dst_y_2 = out_i420->data;
    int dst_stride_y_2 = out_i420->width;
    uint8 *dst_u_2 = dst_y_2 + dst_stride_y_2 * out_i420->height;
    int dst_stride_u_2 = out_i420->width/2;
    uint8 *dst_v_2 = dst_u_2 + dst_stride_u_2 * out_i420->height/2;
    int dst_stride_v_2 = out_i420->width/2;
    int dst_width = out_i420->width;
    int dst_height = out_i420->height;
    
    ret = libyuv::I420Scale(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, src_width_1, src_height_1, dst_y_2, dst_stride_y_2, dst_u_2, dst_stride_u_2, dst_v_2, dst_stride_v_2, dst_width, dst_height, libyuv::kFilterBox);
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::RGBAtoI420_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_i420)
{
    out_i420->pts = in_rgba->pts;
    
    if(in_rgba->width*out_i420->height>out_i420->width*in_rgba->height)
    {
        mVideoFrame1->width = out_i420->width*in_rgba->height/out_i420->height;
        mVideoFrame1->height = in_rgba->height;
        
    }else if(in_rgba->width*out_i420->height<out_i420->width*in_rgba->height)
    {
        mVideoFrame1->width = in_rgba->width;
        mVideoFrame1->height = out_i420->height*in_rgba->width/out_i420->width;
    }
    else {
        mVideoFrame1->width = in_rgba->width;
        mVideoFrame1->height = in_rgba->height;
    }
    
    mVideoFrame1->width = mVideoFrame1->width%2>0?mVideoFrame1->width+1:mVideoFrame1->width;
    mVideoFrame1->height = mVideoFrame1->height%2>0?mVideoFrame1->height+1:mVideoFrame1->height;
    
    mVideoFrame1->frameSize = mVideoFrame1->width*mVideoFrame1->height*3/2;
    
    const uint8 *src_frame = in_rgba->data;
    size_t src_size = in_rgba->frameSize;
    uint8 *dst_y = mVideoFrame1->data;
    int dst_stride_y = mVideoFrame1->width;
    uint8 *dst_u = dst_y + dst_stride_y * mVideoFrame1->height;
    int dst_stride_u = mVideoFrame1->width/2;
    uint8 *dst_v = dst_u + dst_stride_u * mVideoFrame1->height/2;
    int dst_stride_v = mVideoFrame1->width/2;
    int crop_x = (in_rgba->width - mVideoFrame1->width)/2;
    int crop_y = (in_rgba->height - mVideoFrame1->height)/2;
    int src_width = in_rgba->width;
    int src_height = in_rgba->height;
    int crop_width = mVideoFrame1->width;
    int crop_height = mVideoFrame1->height;
    uint32 format = libyuv::FOURCC_RGBA;
    
    int ret = libyuv::ConvertToI420(src_frame, src_size, dst_y, dst_stride_y, dst_u, dst_stride_u, dst_v, dst_stride_v, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::kRotate0, format);
    
    if (ret) return false;
    
    const uint8 *src_y = mVideoFrame1->data;
    int src_stride_y = mVideoFrame1->width;
    const uint8 *src_u = src_y + src_stride_y * mVideoFrame1->height;
    int src_stride_u = mVideoFrame1->width/2;
    const uint8 *src_v = src_u + src_stride_u * mVideoFrame1->height/2;
    int src_stride_v = mVideoFrame1->width/2;
    int src_width_1 = mVideoFrame1->width;
    int src_height_1 = mVideoFrame1->height;
    uint8 *dst_y_2 = out_i420->data;
    int dst_stride_y_2 = out_i420->width;
    uint8 *dst_u_2 = dst_y_2 + dst_stride_y_2 * out_i420->height;
    int dst_stride_u_2 = out_i420->width/2;
    uint8 *dst_v_2 = dst_u_2 + dst_stride_u_2 * out_i420->height/2;
    int dst_stride_v_2 = out_i420->width/2;
    int dst_width = out_i420->width;
    int dst_height = out_i420->height;
    
    ret = libyuv::I420Scale(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, src_width_1, src_height_1, dst_y_2, dst_stride_y_2, dst_u_2, dst_stride_u_2, dst_v_2, dst_stride_v_2, dst_width, dst_height, libyuv::kFilterBox);
    
    if (ret) return false;
    
    return true;

}

bool LibyuvColorSpaceConvert::ARGBtoI420_Crop_Scale(VideoFrame* in_argb, VideoFrame* out_i420)
{
    out_i420->pts = in_argb->pts;
    
    if(in_argb->width*out_i420->height>out_i420->width*in_argb->height)
    {
        mVideoFrame1->width = out_i420->width*in_argb->height/out_i420->height;
        mVideoFrame1->height = in_argb->height;
        
    }else if(in_argb->width*out_i420->height<out_i420->width*in_argb->height)
    {
        mVideoFrame1->width = in_argb->width;
        mVideoFrame1->height = out_i420->height*in_argb->width/out_i420->width;
    }
    else {
        mVideoFrame1->width = in_argb->width;
        mVideoFrame1->height = in_argb->height;
    }
    
    mVideoFrame1->width = mVideoFrame1->width%2>0?mVideoFrame1->width+1:mVideoFrame1->width;
    mVideoFrame1->height = mVideoFrame1->height%2>0?mVideoFrame1->height+1:mVideoFrame1->height;
    
    mVideoFrame1->frameSize = mVideoFrame1->width*mVideoFrame1->height*3/2;
    
    const uint8 *src_frame = in_argb->data;
    size_t src_size = in_argb->frameSize;
    uint8 *dst_y = mVideoFrame1->data;
    int dst_stride_y = mVideoFrame1->width;
    uint8 *dst_u = dst_y + dst_stride_y * mVideoFrame1->height;
    int dst_stride_u = mVideoFrame1->width/2;
    uint8 *dst_v = dst_u + dst_stride_u * mVideoFrame1->height/2;
    int dst_stride_v = mVideoFrame1->width/2;
    int crop_x = (in_argb->width - mVideoFrame1->width)/2;
    int crop_y = (in_argb->height - mVideoFrame1->height)/2;
    int src_width = in_argb->width;
    int src_height = in_argb->height;
    int crop_width = mVideoFrame1->width;
    int crop_height = mVideoFrame1->height;
    uint32 format = libyuv::FOURCC_ARGB;
    
    int ret = libyuv::ConvertToI420(src_frame, src_size, dst_y, dst_stride_y, dst_u, dst_stride_u, dst_v, dst_stride_v, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::kRotate0, format);
    
    if (ret) return false;
    
    const uint8 *src_y = mVideoFrame1->data;
    int src_stride_y = mVideoFrame1->width;
    const uint8 *src_u = src_y + src_stride_y * mVideoFrame1->height;
    int src_stride_u = mVideoFrame1->width/2;
    const uint8 *src_v = src_u + src_stride_u * mVideoFrame1->height/2;
    int src_stride_v = mVideoFrame1->width/2;
    int src_width_1 = mVideoFrame1->width;
    int src_height_1 = mVideoFrame1->height;
    uint8 *dst_y_2 = out_i420->data;
    int dst_stride_y_2 = out_i420->width;
    uint8 *dst_u_2 = dst_y_2 + dst_stride_y_2 * out_i420->height;
    int dst_stride_u_2 = out_i420->width/2;
    uint8 *dst_v_2 = dst_u_2 + dst_stride_u_2 * out_i420->height/2;
    int dst_stride_v_2 = out_i420->width/2;
    int dst_width = out_i420->width;
    int dst_height = out_i420->height;
    
    ret = libyuv::I420Scale(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, src_width_1, src_height_1, dst_y_2, dst_stride_y_2, dst_u_2, dst_stride_u_2, dst_v_2, dst_stride_v_2, dst_width, dst_height, libyuv::kFilterBox);
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::ABGRtoI420_Crop_Scale(VideoFrame* in_abgr, VideoFrame* out_i420)
{
    out_i420->pts = in_abgr->pts;
    
    if(in_abgr->width*out_i420->height>out_i420->width*in_abgr->height)
    {
        mVideoFrame1->width = out_i420->width*in_abgr->height/out_i420->height;
        mVideoFrame1->height = in_abgr->height;
        
    }else if(in_abgr->width*out_i420->height<out_i420->width*in_abgr->height)
    {
        mVideoFrame1->width = in_abgr->width;
        mVideoFrame1->height = out_i420->height*in_abgr->width/out_i420->width;
    }
    else {
        mVideoFrame1->width = in_abgr->width;
        mVideoFrame1->height = in_abgr->height;
    }
    
    mVideoFrame1->width = mVideoFrame1->width%2>0?mVideoFrame1->width+1:mVideoFrame1->width;
    mVideoFrame1->height = mVideoFrame1->height%2>0?mVideoFrame1->height+1:mVideoFrame1->height;
    
    mVideoFrame1->frameSize = mVideoFrame1->width*mVideoFrame1->height*3/2;
    
    const uint8 *src_frame = in_abgr->data;
    size_t src_size = in_abgr->frameSize;
    uint8 *dst_y = mVideoFrame1->data;
    int dst_stride_y = mVideoFrame1->width;
    uint8 *dst_u = dst_y + dst_stride_y * mVideoFrame1->height;
    int dst_stride_u = mVideoFrame1->width/2;
    uint8 *dst_v = dst_u + dst_stride_u * mVideoFrame1->height/2;
    int dst_stride_v = mVideoFrame1->width/2;
    int crop_x = (in_abgr->width - mVideoFrame1->width)/2;
    int crop_y = (in_abgr->height - mVideoFrame1->height)/2;
    int src_width = in_abgr->width;
    int src_height = in_abgr->height;
    int crop_width = mVideoFrame1->width;
    int crop_height = mVideoFrame1->height;
    uint32 format = libyuv::FOURCC_ABGR;
    
    int ret = libyuv::ConvertToI420(src_frame, src_size, dst_y, dst_stride_y, dst_u, dst_stride_u, dst_v, dst_stride_v, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::kRotate0, format);
    
    if (ret) return false;
    
    const uint8 *src_y = mVideoFrame1->data;
    int src_stride_y = mVideoFrame1->width;
    const uint8 *src_u = src_y + src_stride_y * mVideoFrame1->height;
    int src_stride_u = mVideoFrame1->width/2;
    const uint8 *src_v = src_u + src_stride_u * mVideoFrame1->height/2;
    int src_stride_v = mVideoFrame1->width/2;
    int src_width_1 = mVideoFrame1->width;
    int src_height_1 = mVideoFrame1->height;
    uint8 *dst_y_2 = out_i420->data;
    int dst_stride_y_2 = out_i420->width;
    uint8 *dst_u_2 = dst_y_2 + dst_stride_y_2 * out_i420->height;
    int dst_stride_u_2 = out_i420->width/2;
    uint8 *dst_v_2 = dst_u_2 + dst_stride_u_2 * out_i420->height/2;
    int dst_stride_v_2 = out_i420->width/2;
    int dst_width = out_i420->width;
    int dst_height = out_i420->height;
    
    ret = libyuv::I420Scale(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, src_width_1, src_height_1, dst_y_2, dst_stride_y_2, dst_u_2, dst_stride_u_2, dst_v_2, dst_stride_v_2, dst_width, dst_height, libyuv::kFilterBox);
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::ABGRtoI420_Scale(VideoFrame* in_abgr, VideoFrame* out_i420)
{
    int in_old_videorawtype = in_abgr->videoRawType;
    
    in_abgr->videoRawType = VIDEOFRAME_RAWTYPE_ABGR;

    VideoFrame* in = new VideoFrame;
    
    if(in_abgr->width*out_i420->height>out_i420->width*in_abgr->height)
    {
        in->width = in_abgr->width;
        in->height = out_i420->height * in_abgr->width / out_i420->width;
    }else if(in_abgr->width*out_i420->height<out_i420->width*in_abgr->height)
    {
        in->height = in_abgr->height;
        in->width = out_i420->width * in_abgr->height / out_i420->height;
    }
    
    in->frameSize = in->width * in->height * 4;
    in->data = (uint8_t*)malloc(in->frameSize);
    in->pts = in_abgr->pts;
    in->videoRawType = VIDEOFRAME_RAWTYPE_ABGR;
    memset(in->data, 0, in->frameSize);
        
    Overlay::blend_image_rgba(in, in_abgr, (in->width - in_abgr->width)/2, (in->height - in_abgr->height)/2);
    
    in_abgr->videoRawType = in_old_videorawtype;

    bool ret = this->ABGRtoI420_Crop_Scale(in, out_i420);
    
    in->Free();
    delete in;
    
    return ret;
}

bool LibyuvColorSpaceConvert::ABGRtoARGB_Crop_Scale(VideoFrame* in_abgr, VideoFrame* out_argb)
{
    out_argb->pts = in_abgr->pts;
    
    const uint8 *src_frame = in_abgr->data;
    size_t src_size = in_abgr->frameSize;
    
    int src_width = in_abgr->width;
    int src_height = in_abgr->height;
    
    int dst_width = out_argb->width;
    int dst_height = out_argb->height;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    if(src_width*dst_height>dst_width*src_height)
    {
        crop_width = dst_width*src_height/dst_height;
        crop_height = src_height;
        
        crop_x = (src_width - crop_width)/2;
        crop_y = 0;
        
    }else if(src_width*dst_height<dst_width*src_height)
    {
        crop_width = src_width;
        crop_height = dst_height*src_width/dst_width;
        
        crop_x = 0;
        crop_y = (src_height - crop_height)/2;
    }else {
        crop_width = src_width;
        crop_height = src_height;
        crop_x = 0;
        crop_y = 0;
    }
    
    mVideoFrame1->frameSize = crop_width*crop_height*4;
    mVideoFrame1->width = crop_width;
    mVideoFrame1->height = crop_height;
    
    uint8 *dst_argb = mVideoFrame1->data;
    int dst_stride_argb = mVideoFrame1->width*4;
    
    int ret = libyuv::ConvertToARGB(src_frame, src_size, dst_argb, dst_stride_argb, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::RotationMode(0), libyuv::FOURCC_ABGR);
    
    if (ret) return false;
    
    const uint8 *src_argb = mVideoFrame1->data;
    int src_stride_argb = mVideoFrame1->width*4;
    int src_width_1 = mVideoFrame1->width;
    int src_height_1 = mVideoFrame1->height;
    uint8 *dst_argb_1 = out_argb->data;
    int dst_stride_argb_1 = out_argb->width*4;
    int dst_width_1 = out_argb->width;
    int dst_height_1 = out_argb->height;
    ret = libyuv::ARGBScale(src_argb, src_stride_argb, src_width_1, src_height_1, dst_argb_1, dst_stride_argb_1, dst_width_1, dst_height_1, libyuv::kFilterBox);
    
    if (ret) return false;
    
    return true;

}

bool LibyuvColorSpaceConvert::NV12toI420_Crop_Rotation(VideoFrame* nv12, VideoFrame* i420, int rotation)
{
    i420->pts = nv12->pts;
    
    int i420_width_pre_rotation= 0;
    int i420_height_pre_rotation = 0;
    
    if (rotation==0 || rotation==180) {
        i420_width_pre_rotation = i420->width;
        i420_height_pre_rotation = i420->height;
    }else if(rotation==90 || rotation==270) {
        i420_width_pre_rotation = i420->height;
        i420_height_pre_rotation = i420->width;
    }
    
    if (nv12->width<i420_width_pre_rotation || nv12->height<i420_height_pre_rotation) {
        return false;
    }
    
    uint8* src_frame = nv12->data;
    size_t src_size = nv12->frameSize;
    uint8* dst_y = i420->data;
    int dst_stride_y = i420->width;
    uint8* dst_u = dst_y + dst_stride_y * i420->height;
    int dst_stride_u = (i420->width+1)/2;
    uint8* dst_v = dst_u + dst_stride_u * i420->height/2;
    int dst_stride_v = (i420->width+1)/2;
    int crop_x;
    int crop_y;
    int src_width = nv12->width;
    int src_height = nv12->height;
    int crop_width;
    int crop_height;
    uint32 format = libyuv::FOURCC_NV12;
    
    if (rotation==0 || rotation==180) {
        crop_x = (nv12->width - i420->width)/2;
        crop_y = (nv12->height - i420->height)/2;
        
        crop_width = i420->width;
        crop_height = i420->height;
    }else if(rotation==90 || rotation==270) {
        crop_x = (nv12->width - i420->height)/2;
        crop_y = (nv12->height - i420->width)/2;
        
        crop_width = i420->height;
        crop_height = i420->width;
    }else {
        crop_x = 0;
        crop_y = 0;
        
        crop_width = src_width;
        crop_height = src_height;
    }
    
    int ret = libyuv::ConvertToI420(src_frame, src_size,
                                    dst_y, dst_stride_y,
                                    dst_u, dst_stride_u,
                                    dst_v, dst_stride_v,
                                    crop_x, crop_y,
                                    src_width, src_height,
                                    crop_width, crop_height,
                                    libyuv::RotationMode(rotation),
                                    format);
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::NV12toI420_Crop_Rotation_Scale(VideoFrame* nv12, VideoFrame* i420, int rotation)
{
    i420->pts = nv12->pts;
    
    int i420_width_pre_rotation= 0;
    int i420_height_pre_rotation = 0;
    
    if (rotation==0 || rotation==180) {
        i420_width_pre_rotation = i420->width;
        i420_height_pre_rotation = i420->height;
    }else if(rotation==90 || rotation==270) {
        i420_width_pre_rotation = i420->height;
        i420_height_pre_rotation = i420->width;
    }
    
    //    if (nv21->width>=i420_width_pre_rotation && nv21->height>=i420_height_pre_rotation) {
    //        return NV21toI420_Crop_Rotation(nv21, i420, rotation);
    //    }
    
    if(nv12->width*i420_height_pre_rotation>=i420_width_pre_rotation*nv12->height)
    {
        if (rotation==0 || rotation==180) {
            mVideoFrame1->width = i420_width_pre_rotation*nv12->height/i420_height_pre_rotation;
            mVideoFrame1->height = nv12->height;
        }else if(rotation==90 || rotation==270) {
            mVideoFrame1->width = nv12->height;
            mVideoFrame1->height = i420_width_pre_rotation*nv12->height/i420_height_pre_rotation;
        }
        
    }else if(nv12->width*i420_height_pre_rotation<i420_width_pre_rotation*nv12->height)
    {
        if (rotation==0 || rotation==180) {
            mVideoFrame1->width = nv12->width;
            mVideoFrame1->height = i420_height_pre_rotation*nv12->width/i420_width_pre_rotation;
        }else if(rotation==90 || rotation==270) {
            mVideoFrame1->width = i420_height_pre_rotation*nv12->width/i420_width_pre_rotation;
            mVideoFrame1->height = nv12->width;
        }
    }
    mVideoFrame1->width = mVideoFrame1->width%2>0?mVideoFrame1->width+1:mVideoFrame1->width;
    mVideoFrame1->height = mVideoFrame1->height%2>0?mVideoFrame1->height+1:mVideoFrame1->height;
    
    mVideoFrame1->frameSize = mVideoFrame1->width*mVideoFrame1->height*3/2;
    mVideoFrame1->pts = nv12->pts;
    
    bool bRet = NV12toI420_Crop_Rotation(nv12, mVideoFrame1, rotation);
    
    if (!bRet) return false;
    
    //do scale
    uint8 *src_y = mVideoFrame1->data;
    int src_stride_y = mVideoFrame1->width;
    uint8 *src_u = src_y + src_stride_y * mVideoFrame1->height;
    int src_stride_u = (mVideoFrame1->width+1)/2;
    uint8 *src_v = src_u + src_stride_u * mVideoFrame1->height/2;
    int src_stride_v = (mVideoFrame1->width+1)/2;
    int src_width = mVideoFrame1->width;
    int src_height = mVideoFrame1->height;
    uint8* dst_y = i420->data;
    int dst_stride_y = i420->width;
    uint8* dst_u = dst_y + dst_stride_y * i420->height;
    int dst_stride_u = (i420->width+1)/2;
    uint8* dst_v = dst_u + dst_stride_u * i420->height/2;
    int dst_stride_v = (i420->width+1)/2;
    int dst_width = i420->width;
    int dst_height = i420->height;
    
    int ret = libyuv::I420Scale(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, src_width, src_height, dst_y, dst_stride_y, dst_u, dst_stride_u, dst_v, dst_stride_v, dst_width, dst_height, libyuv::kFilterBox);
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::NV12toI420_Crop_Rotation_Scale_Mirror(VideoFrame* nv12, VideoFrame* i420, int rotation)
{
    i420->pts = nv12->pts;
    
    mVideoFrame2->width = i420->width;
    mVideoFrame2->height = i420->height;
    mVideoFrame2->frameSize = mVideoFrame2->width*mVideoFrame2->height*3/2;
    
    bool bRet = NV12toI420_Crop_Rotation_Scale(nv12, mVideoFrame2, rotation);
    if (!bRet) return false;
    
    uint8 *src_y = mVideoFrame2->data;
    int src_stride_y = mVideoFrame2->width;
    uint8 *src_u = src_y + src_stride_y * mVideoFrame2->height;
    int src_stride_u = (mVideoFrame2->width+1)/2;
    uint8 *src_v = src_u + src_stride_u * mVideoFrame2->height/2;
    int src_stride_v = (mVideoFrame2->width+1)/2;
    uint8* dst_y = i420->data;
    int dst_stride_y = i420->width;
    uint8* dst_u = dst_y + dst_stride_y * i420->height;
    int dst_stride_u = (i420->width+1)/2;
    uint8* dst_v = dst_u + dst_stride_u * i420->height/2;
    int dst_stride_v = (i420->width+1)/2;
    int width = i420->width;
    int height = i420->height;
    
    int ret = libyuv::I420Mirror(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, dst_y, dst_stride_y, dst_u, dst_stride_u, dst_v, dst_stride_v, width, height);
    
    if (ret) return false;
    
    return true;

}

bool LibyuvColorSpaceConvert::NV12toARGB_Crop_Scale(VideoFrame* in_nv12, VideoFrame* out_argb)
{
    out_argb->pts = in_nv12->pts;
    
    const uint8 *src_frame = in_nv12->data;
    size_t src_size = in_nv12->frameSize;
    
    int src_width = in_nv12->width;
    int src_height = in_nv12->height;
    
    int dst_width = out_argb->width;
    int dst_height = out_argb->height;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    if(src_width*dst_height>dst_width*src_height)
    {
        crop_width = dst_width*src_height/dst_height;
        crop_height = src_height;
        
        crop_x = (src_width - crop_width)/2;
        crop_y = 0;
        
    }else if(src_width*dst_height<dst_width*src_height)
    {
        crop_width = src_width;
        crop_height = dst_height*src_width/dst_width;
        
        crop_x = 0;
        crop_y = (src_height - crop_height)/2;
    }else {
        crop_width = src_width;
        crop_height = src_height;
        crop_x = 0;
        crop_y = 0;
    }
    
    mVideoFrame1->frameSize = crop_width*crop_height*4;
    mVideoFrame1->width = crop_width;
    mVideoFrame1->height = crop_height;
    
    uint8 *dst_argb = mVideoFrame1->data;
    int dst_stride_argb = mVideoFrame1->width*4;
    
    int ret = libyuv::ConvertToARGB(src_frame, src_size, dst_argb, dst_stride_argb, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::RotationMode(in_nv12->rotation), libyuv::FOURCC_NV12);
    
    if (ret) return false;
    
    const uint8 *src_argb = mVideoFrame1->data;
    int src_stride_argb = mVideoFrame1->width*4;
    int src_width_1 = mVideoFrame1->width;
    int src_height_1 = mVideoFrame1->height;
    uint8 *dst_argb_1 = out_argb->data;
    int dst_stride_argb_1 = out_argb->width*4;
    int dst_width_1 = out_argb->width;
    int dst_height_1 = out_argb->height;
    ret = libyuv::ARGBScale(src_argb, src_stride_argb, src_width_1, src_height_1, dst_argb_1, dst_stride_argb_1, dst_width_1, dst_height_1, libyuv::kFilterBox);
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::NV12toRGBA_Crop_Scale(VideoFrame* in_nv12, VideoFrame* out_rgba)
{
    out_rgba->pts = in_nv12->pts;
    
    mVideoFrame2->frameSize = out_rgba->frameSize;
    mVideoFrame2->width = out_rgba->width;
    mVideoFrame2->height = out_rgba->height;
    
    bool bret = NV12toARGB_Crop_Scale(in_nv12, mVideoFrame2);
    
    if(!bret) return false;
    
    const uint8 *src_argb = mVideoFrame2->data;
    int src_stride_argb = mVideoFrame2->width*4;
    uint8 *dst_rgba = out_rgba->data;
    int dst_stride_rgba = out_rgba->width*4;
    int width = out_rgba->width;
    int height = out_rgba->height;
    int iret = libyuv::ARGBToRGBA(src_argb, src_stride_argb, dst_rgba, dst_stride_rgba, width, height);
    
    if (iret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::NV12toBGRA_Crop_Scale(VideoFrame* in_nv12, VideoFrame* out_bgra)
{
    out_bgra->pts = in_nv12->pts;
    
    mVideoFrame2->frameSize = out_bgra->frameSize;
    mVideoFrame2->width = out_bgra->width;
    mVideoFrame2->height = out_bgra->height;
    
    bool bret = NV12toARGB_Crop_Scale(in_nv12, mVideoFrame2);
    
    if(!bret) return false;
    
    const uint8 *src_argb = mVideoFrame2->data;
    int src_stride_argb = mVideoFrame2->width*4;
    uint8 *dst_bgra = out_bgra->data;
    int dst_stride_bgra = out_bgra->width*4;
    int width = out_bgra->width;
    int height = out_bgra->height;
    int iret = libyuv::ARGBToBGRA(src_argb, src_stride_argb, dst_bgra, dst_stride_bgra, width, height);
    
    if (iret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::NV12toABGR_Crop_Scale(VideoFrame* in_nv12, VideoFrame* out_abgr)
{
    out_abgr->pts = in_nv12->pts;
    
    mVideoFrame2->frameSize = out_abgr->frameSize;
    mVideoFrame2->width = out_abgr->width;
    mVideoFrame2->height = out_abgr->height;
    
    bool bret = NV12toARGB_Crop_Scale(in_nv12, mVideoFrame2);
    
    if(!bret) return false;
    
    const uint8 *src_argb = mVideoFrame2->data;
    int src_stride_argb = mVideoFrame2->width*4;
    uint8 *dst_abgr = out_abgr->data;
    int dst_stride_abgr = out_abgr->width*4;
    int width = out_abgr->width;
    int height = out_abgr->height;
    int iret = libyuv::ARGBToABGR(src_argb, src_stride_argb, dst_abgr, dst_stride_abgr, width, height);
    
    if (iret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::ARGBtoNV21_Crop_Scale(VideoFrame* in_argb, VideoFrame* out_nv21)
{
    out_nv21->pts = in_argb->pts;
    
    const uint8 *src_frame = in_argb->data;
    size_t src_size = in_argb->frameSize;
    
    int src_width = in_argb->width;
    int src_height = in_argb->height;
    
    int dst_width = out_nv21->width;
    int dst_height = out_nv21->height;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    if(src_width*dst_height>dst_width*src_height)
    {
        crop_width = dst_width*src_height/dst_height;
        crop_height = src_height;
        
        crop_x = (src_width - crop_width)/2;
        crop_y = 0;
    }else if(src_width*dst_height<dst_width*src_height)
    {
        crop_width = src_width;
        crop_height = dst_height*src_width/dst_width;
        
        crop_x = 0;
        crop_y = (src_height - crop_height)/2;
    }else {
        crop_width = src_width;
        crop_height = src_height;
        crop_x = 0;
        crop_y = 0;
    }
    
    mVideoFrame1->frameSize = crop_width*crop_height*4;
    mVideoFrame1->width = crop_width;
    mVideoFrame1->height = crop_height;
    
    uint8 *dst_argb = mVideoFrame1->data;
    int dst_stride_argb = mVideoFrame1->width*4;
    
    int ret = libyuv::ConvertToARGB(src_frame, src_size, dst_argb, dst_stride_argb, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::RotationMode(0), libyuv::FOURCC_ARGB);
    if (ret) return false;
    
    mVideoFrame2->width = out_nv21->width;
    mVideoFrame2->height = out_nv21->height;
    mVideoFrame2->frameSize = mVideoFrame2->width * mVideoFrame2->height *3/2;
    
    const uint8 *src_argb_1 = mVideoFrame1->data;
    int src_stride_argb_1 = mVideoFrame1->width*4;
    int src_width_1 = mVideoFrame1->width;
    int src_height_1 = mVideoFrame1->height;
    uint8 *dst_argb_1 = mVideoFrame2->data;
    int dst_stride_argb_1 = mVideoFrame2->width*4;
    int dst_width_1 = mVideoFrame2->width;
    int dst_height_1 = mVideoFrame2->height;
    ret = libyuv::ARGBScale(src_argb_1, src_stride_argb_1, src_width_1, src_height_1, dst_argb_1, dst_stride_argb_1, dst_width_1, dst_height_1, libyuv::kFilterBox);
    if (ret) return false;
    
    const uint8 *src_argb = mVideoFrame2->data;
    int src_stride_argb = mVideoFrame2->width*4;
    uint8 *dst_y = out_nv21->data;
    int dst_stride_y = out_nv21->width;
    uint8 *dst_vu = out_nv21->data + out_nv21->width*out_nv21->height;
    int dst_stride_vu = out_nv21->width;
    int width = out_nv21->width;
    int height = out_nv21->height;
    ret = libyuv::ARGBToNV21(src_argb, src_stride_argb, dst_y, dst_stride_y, dst_vu, dst_stride_vu, width, height);
    
    if (ret) return false;

    return true;
}

bool LibyuvColorSpaceConvert::NV12toNV12_Crop_Rotation_Scale(VideoFrame* in_nv12, VideoFrame* out_nv12, int rotation)
{
    out_nv12->pts = in_nv12->pts;
    
    mVideoFrame2->width = out_nv12->width;
    mVideoFrame2->height = out_nv12->height;
    mVideoFrame2->frameSize = mVideoFrame2->width*mVideoFrame2->height*3/2;
    
    bool bRet = NV12toI420_Crop_Rotation_Scale(in_nv12, mVideoFrame2, rotation);
    if (!bRet) return false;
    
    uint8 *src_y = mVideoFrame2->data;
    int src_stride_y = mVideoFrame2->width;
    uint8 *src_u = src_y + src_stride_y * mVideoFrame2->height;
    int src_stride_u = (mVideoFrame2->width+1)/2;
    uint8 *src_v = src_u + src_stride_u * mVideoFrame2->height/2;
    int src_stride_v = (mVideoFrame2->width+1)/2;
    
    uint8* dst_y = out_nv12->data;
    int dst_stride_y = out_nv12->width;
    uint8* dst_uv = dst_y + dst_stride_y * out_nv12->height;
    int dst_stride_uv = out_nv12->width;
    int width = out_nv12->width;
    int height = out_nv12->height;
    
    int ret = libyuv::I420ToNV12(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, dst_y, dst_stride_y, dst_uv, dst_stride_uv, width, height);

    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvert::RGBAtoARGB_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_argb)
{
    out_argb->pts = in_rgba->pts;
    
    const uint8 *src_frame = in_rgba->data;
    size_t src_size = in_rgba->frameSize;
    
    int src_width = in_rgba->width;
    int src_height = in_rgba->height;
    
    int dst_width = out_argb->width;
    int dst_height = out_argb->height;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    if(src_width*dst_height>dst_width*src_height)
    {
        crop_width = dst_width*src_height/dst_height;
        crop_height = src_height;
        
        crop_x = (src_width - crop_width)/2;
        crop_y = 0;
        
    }else if(src_width*dst_height<dst_width*src_height)
    {
        crop_width = src_width;
        crop_height = dst_height*src_width/dst_width;
        
        crop_x = 0;
        crop_y = (src_height - crop_height)/2;
    }else {
        crop_width = src_width;
        crop_height = src_height;
        crop_x = 0;
        crop_y = 0;
    }
    
    mVideoFrame1->frameSize = crop_width*crop_height*4;
    mVideoFrame1->width = crop_width;
    mVideoFrame1->height = crop_height;
    
    uint8 *dst_argb = mVideoFrame1->data;
    int dst_stride_argb = mVideoFrame1->width*4;
    
    int ret = libyuv::ConvertToARGB(src_frame, src_size, dst_argb, dst_stride_argb, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::RotationMode(0), libyuv::FOURCC_BGRA);
    
    if (ret) return false;
    
    const uint8 *src_argb = mVideoFrame1->data;
    int src_stride_argb = mVideoFrame1->width*4;
    int src_width_1 = mVideoFrame1->width;
    int src_height_1 = mVideoFrame1->height;
    uint8 *dst_argb_1 = out_argb->data;
    int dst_stride_argb_1 = out_argb->width*4;
    int dst_width_1 = out_argb->width;
    int dst_height_1 = out_argb->height;
    ret = libyuv::ARGBScale(src_argb, src_stride_argb, src_width_1, src_height_1, dst_argb_1, dst_stride_argb_1, dst_width_1, dst_height_1, libyuv::kFilterBox);
    
    if (ret) return false;
    
    return true;

}

bool LibyuvColorSpaceConvert::RGBAtoBGRA_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_bgra)
{
    out_bgra->pts = in_rgba->pts;
    
    mVideoFrame2->frameSize = out_bgra->frameSize;
    mVideoFrame2->width = out_bgra->width;
    mVideoFrame2->height = out_bgra->height;
    
    bool bret = BGRAtoARGB_Crop_Scale(in_rgba, mVideoFrame2);
    
    if(!bret) return false;
    
    const uint8 *src_argb = mVideoFrame2->data;
    int src_stride_argb = mVideoFrame2->width*4;
    uint8 *dst_bgra = out_bgra->data;
    int dst_stride_bgra = out_bgra->width*4;
    int width = out_bgra->width;
    int height = out_bgra->height;
    int iret = libyuv::ARGBToBGRA(src_argb, src_stride_argb, dst_bgra, dst_stride_bgra, width, height);
    
    if (iret) return false;
    
    return true;
}

//------------------------------------------------------------------------------------------------------------------------------//

bool LibyuvColorSpaceConvertUtils::RGBAtoARGB_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_argb)
{
    out_argb->pts = in_rgba->pts;
    
    const uint8 *src_frame = in_rgba->data;
    size_t src_size = in_rgba->frameSize;
    
    int src_width = in_rgba->width;
    int src_height = in_rgba->height;
    
    int dst_width = out_argb->width;
    int dst_height = out_argb->height;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    if(src_width*dst_height>dst_width*src_height)
    {
        crop_width = dst_width*src_height/dst_height;
        crop_height = src_height;
        
        crop_x = (src_width - crop_width)/2;
        crop_y = 0;
        
    }else if(src_width*dst_height<dst_width*src_height)
    {
        crop_width = src_width;
        crop_height = dst_height*src_width/dst_width;
        
        crop_x = 0;
        crop_y = (src_height - crop_height)/2;
    }else {
        crop_width = src_width;
        crop_height = src_height;
        crop_x = 0;
        crop_y = 0;
    }
    
    VideoFrame *videoFrame = new VideoFrame;
    videoFrame->frameSize = crop_width*crop_height*4;
    videoFrame->data = (uint8_t*)malloc(videoFrame->frameSize);
    videoFrame->width = crop_width;
    videoFrame->height = crop_height;
    
    uint8 *dst_argb = videoFrame->data;
    int dst_stride_argb = videoFrame->width*4;
    
    int ret = libyuv::ConvertToARGB(src_frame, src_size, dst_argb, dst_stride_argb, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::RotationMode(0), libyuv::FOURCC_RGBA);
    
    if (ret)
    {
        if (videoFrame) {
            if (videoFrame->data) {
                free(videoFrame->data);
                videoFrame->data = NULL;
            }
            
            delete videoFrame;
            videoFrame = NULL;
        }
        return false;
    }
    
    const uint8 *src_argb = videoFrame->data;
    int src_stride_argb = videoFrame->width*4;
    int src_width_1 = videoFrame->width;
    int src_height_1 = videoFrame->height;
    uint8 *dst_argb_1 = out_argb->data;
    int dst_stride_argb_1 = out_argb->width*4;
    int dst_width_1 = out_argb->width;
    int dst_height_1 = out_argb->height;
    ret = libyuv::ARGBScale(src_argb, src_stride_argb, src_width_1, src_height_1, dst_argb_1, dst_stride_argb_1, dst_width_1, dst_height_1, libyuv::kFilterBox);
    
    if (videoFrame) {
        if (videoFrame->data) {
            free(videoFrame->data);
            videoFrame->data = NULL;
        }
        
        delete videoFrame;
        videoFrame = NULL;
    }
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvertUtils::RGBAtoRGBA_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_rgba)
{
    out_rgba->pts = in_rgba->pts;
    
    VideoFrame *videoFrame = new VideoFrame;
    videoFrame->frameSize = out_rgba->frameSize;
    videoFrame->data = (uint8_t*)malloc(videoFrame->frameSize);
    videoFrame->width = out_rgba->width;
    videoFrame->height = out_rgba->height;
    
    bool bret = LibyuvColorSpaceConvertUtils::RGBAtoARGB_Crop_Scale(in_rgba, videoFrame);
    
    if(!bret)
    {
        if (videoFrame) {
            if (videoFrame->data) {
                free(videoFrame->data);
                videoFrame->data = NULL;
            }
            
            delete videoFrame;
            videoFrame = NULL;
        }
        
        return false;
    }
    
    const uint8 *src_argb = videoFrame->data;
    int src_stride_argb = videoFrame->width*4;
    uint8 *dst_bgra = out_rgba->data;
    int dst_stride_bgra = out_rgba->width*4;
    int width = out_rgba->width;
    int height = out_rgba->height;
    int iret = libyuv::ARGBToRGBA(src_argb, src_stride_argb, dst_bgra, dst_stride_bgra, width, height);
    
    if (videoFrame) {
        if (videoFrame->data) {
            free(videoFrame->data);
            videoFrame->data = NULL;
        }
        
        delete videoFrame;
        videoFrame = NULL;
    }
    
    if (iret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvertUtils::RGBPtoARGB_Crop_Scale(VideoFrame* in_rgbp, VideoFrame* out_argb)
{
    out_argb->pts = in_rgbp->pts;
    
    const uint8 *src_frame = in_rgbp->data;
    size_t src_size = in_rgbp->frameSize;
    
    int src_width = in_rgbp->width;
    int src_height = in_rgbp->height;
    
    int dst_width = out_argb->width;
    int dst_height = out_argb->height;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    if(src_width*dst_height>dst_width*src_height)
    {
        crop_width = dst_width*src_height/dst_height;
        crop_height = src_height;
        
        crop_x = (src_width - crop_width)/2;
        crop_y = 0;
        
    }else if(src_width*dst_height<dst_width*src_height)
    {
        crop_width = src_width;
        crop_height = dst_height*src_width/dst_width;
        
        crop_x = 0;
        crop_y = (src_height - crop_height)/2;
    }else {
        crop_width = src_width;
        crop_height = src_height;
        crop_x = 0;
        crop_y = 0;
    }
    
    VideoFrame *videoFrame = new VideoFrame;
    videoFrame->frameSize = crop_width*crop_height*4;
    videoFrame->data = (uint8_t*)malloc(videoFrame->frameSize);
    videoFrame->width = crop_width;
    videoFrame->height = crop_height;
    
    uint8 *dst_argb = videoFrame->data;
    int dst_stride_argb = videoFrame->width*4;
    
    int ret = libyuv::ConvertToARGB(src_frame, src_size, dst_argb, dst_stride_argb, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::RotationMode(0), libyuv::FOURCC_RGBP);
    
    if (ret)
    {
        if (videoFrame) {
            if (videoFrame->data) {
                free(videoFrame->data);
                videoFrame->data = NULL;
            }
            
            delete videoFrame;
            videoFrame = NULL;
        }
        return false;
    }
    
    const uint8 *src_argb = videoFrame->data;
    int src_stride_argb = videoFrame->width*4;
    int src_width_1 = videoFrame->width;
    int src_height_1 = videoFrame->height;
    uint8 *dst_argb_1 = out_argb->data;
    int dst_stride_argb_1 = out_argb->width*4;
    int dst_width_1 = out_argb->width;
    int dst_height_1 = out_argb->height;
    ret = libyuv::ARGBScale(src_argb, src_stride_argb, src_width_1, src_height_1, dst_argb_1, dst_stride_argb_1, dst_width_1, dst_height_1, libyuv::kFilterBox);
    
    if (videoFrame) {
        if (videoFrame->data) {
            free(videoFrame->data);
            videoFrame->data = NULL;
        }
        
        delete videoFrame;
        videoFrame = NULL;
    }
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvertUtils::RGBPtoRGBA_Crop_Scale(VideoFrame* in_rgbp, VideoFrame* out_rgba)
{
    out_rgba->pts = in_rgbp->pts;
    
    VideoFrame *videoFrame = new VideoFrame;
    videoFrame->frameSize = out_rgba->frameSize;
    videoFrame->data = (uint8_t*)malloc(videoFrame->frameSize);
    videoFrame->width = out_rgba->width;
    videoFrame->height = out_rgba->height;
    
    bool bret = LibyuvColorSpaceConvertUtils::RGBPtoARGB_Crop_Scale(in_rgbp, videoFrame);
    
    if(!bret)
    {
        if (videoFrame) {
            if (videoFrame->data) {
                free(videoFrame->data);
                videoFrame->data = NULL;
            }
            
            delete videoFrame;
            videoFrame = NULL;
        }
        
        return false;
    }
    
    const uint8 *src_argb = videoFrame->data;
    int src_stride_argb = videoFrame->width*4;
    uint8 *dst_bgra = out_rgba->data;
    int dst_stride_bgra = out_rgba->width*4;
    int width = out_rgba->width;
    int height = out_rgba->height;
    int iret = libyuv::ARGBToABGR(src_argb, src_stride_argb, dst_bgra, dst_stride_bgra, width, height);
    
    if (videoFrame) {
        if (videoFrame->data) {
            free(videoFrame->data);
            videoFrame->data = NULL;
        }
        
        delete videoFrame;
        videoFrame = NULL;
    }
    
    if (iret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvertUtils::I420ToRGBA_Crop_Scale_Rotation(YUVVideoFrame* in_yuv420p, VideoFrame* out_rgba, int rotation)
{
    out_rgba->pts = in_yuv420p->pts;
    
    VideoFrame* in_i420 = new VideoFrame;
    if (rotation == 90 || rotation==270) {
        float scale_w = in_yuv420p->width / out_rgba->height;
        float scale_y = in_yuv420p->height / out_rgba->width;
        if (scale_w < scale_y) {
            in_i420->width = out_rgba->height;
            in_i420->height = (int)((float)in_yuv420p->height / scale_w);
            if (in_i420->height%2!=0) {
                in_i420->height += 1;
            }
        }else {
            in_i420->height = out_rgba->width;
            in_i420->width = (int)((float)in_yuv420p->width / scale_y);
            if (in_i420->width%2!=0) {
                in_i420->width += 1;
            }
        }
    }else{
        float scale_w = in_yuv420p->width / out_rgba->width;
        float scale_y = in_yuv420p->height / out_rgba->height;
        if (scale_w < scale_y) {
            in_i420->width = out_rgba->width;
            in_i420->height = (int)((float)in_yuv420p->height / scale_w);
            if (in_i420->height%2!=0) {
                in_i420->height += 1;
            }
        }else {
            in_i420->height = out_rgba->height;
            in_i420->width = (int)((float)in_yuv420p->width / scale_y);
            if (in_i420->width%2!=0) {
                in_i420->width += 1;
            }
        }
    }
    
    in_i420->frameSize = in_i420->width * in_i420->height * 3 / 2;
    in_i420->data = (uint8_t*)malloc(in_i420->frameSize);
    
    //do scale
    uint8 *src_y = in_yuv420p->data[0];
    int src_stride_y = in_yuv420p->linesize[0];
    uint8 *src_u = in_yuv420p->data[1];
    int src_stride_u = in_yuv420p->linesize[1];
    uint8 *src_v = in_yuv420p->data[2];
    int src_stride_v = in_yuv420p->linesize[2];
    int src_width = in_yuv420p->width;
    int src_height = in_yuv420p->height;
    uint8* dst_y = in_i420->data;
    int dst_stride_y = in_i420->width;
    uint8* dst_u = dst_y + dst_stride_y * in_i420->height;
    int dst_stride_u = in_i420->width/2;
    uint8* dst_v = dst_u + dst_stride_u * in_i420->height/2;
    int dst_stride_v = in_i420->width/2;
    int dst_width = in_i420->width;
    int dst_height = in_i420->height;
    
    int ret = libyuv::I420Scale(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, src_width, src_height, dst_y, dst_stride_y, dst_u, dst_stride_u, dst_v, dst_stride_v, dst_width, dst_height, libyuv::kFilterNone);
    if (ret) {
        if (in_i420) {
            if (in_i420->data) {
                free(in_i420->data);
                in_i420->data = NULL;
            }
            delete in_i420;
            in_i420 = NULL;
        }
        
        return false;
    }
    
    VideoFrame *out_argb = new VideoFrame;
    out_argb->width = out_rgba->width;
    out_argb->height = out_rgba->height;
    out_argb->frameSize = out_argb->width * out_argb->height * 4;
    out_argb->data = (uint8_t*)malloc(out_argb->frameSize);

    const uint8 *src_frame = in_i420->data;
    size_t src_size = in_i420->frameSize;
    
    src_width = in_i420->width;
    src_height = in_i420->height;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    int dst_width_pre_rotation= 0;
    int dst_height_pre_rotation = 0;
    
    if (rotation==90 || rotation==270) {
        dst_width_pre_rotation = out_argb->height;
        dst_height_pre_rotation = out_argb->width;
    }else{
        dst_width_pre_rotation = out_argb->width;
        dst_height_pre_rotation = out_argb->height;
    }
    
    crop_width = dst_width_pre_rotation;
    crop_height = dst_height_pre_rotation;
    crop_x = (src_width - crop_width)/2;
    crop_y = (src_height - crop_height)/2;
    
    uint8 *dst_argb = out_argb->data;
    int dst_stride_argb = out_argb->width*4;
    
    ret = libyuv::ConvertToARGB(src_frame, src_size, dst_argb, dst_stride_argb, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::RotationMode(rotation), libyuv::FOURCC_I420);
    
    if (in_i420) {
        if (in_i420->data) {
            free(in_i420->data);
            in_i420->data = NULL;
        }
        delete in_i420;
        in_i420 = NULL;
    }
    
    if (ret)
    {
        if (out_argb) {
            if (out_argb->data) {
                free(out_argb->data);
                out_argb->data = NULL;
            }
            delete out_argb;
            out_argb = NULL;
        }
        return false;
    }
    
    const uint8 *src_argb = out_argb->data;
    int src_stride_argb = out_argb->width*4;
    uint8 *dst_bgra = out_rgba->data;
    int dst_stride_bgra = out_rgba->width*4;
    int width = out_rgba->width;
    int height = out_rgba->height;
    
    ret = libyuv::ARGBToABGR(src_argb, src_stride_argb, dst_bgra, dst_stride_bgra, width, height);
    
    if (out_argb) {
        if (out_argb->data) {
            free(out_argb->data);
            out_argb->data = NULL;
        }
        delete out_argb;
        out_argb = NULL;
    }
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvertUtils::NV12ToRGBA_Crop_Scale_Rotation(YUVVideoFrame* in_yuv420sp, VideoFrame* out_rgba, int rotation)
{
    YUVVideoFrame* in_yuv420p = new YUVVideoFrame;
    in_yuv420p->width = in_yuv420sp->width;
    in_yuv420p->height = in_yuv420sp->height;
    in_yuv420p->linesize[0] = in_yuv420p->width;
    in_yuv420p->linesize[1] = in_yuv420p->width / 2;
    in_yuv420p->linesize[2] = in_yuv420p->width / 2;
    in_yuv420p->data[0] = (uint8_t*)malloc(in_yuv420p->linesize[0] * in_yuv420p->height);
    in_yuv420p->data[1] = (uint8_t*)malloc(in_yuv420p->linesize[1] * in_yuv420p->height/2);
    in_yuv420p->data[2] = (uint8_t*)malloc(in_yuv420p->linesize[2] * in_yuv420p->height/2);

    uint8* src_y = in_yuv420sp->data[0];
    int src_stride_y = in_yuv420sp->linesize[0];
    uint8* src_uv = in_yuv420sp->data[1];
    int src_stride_uv = in_yuv420sp->linesize[1];
    
    uint8* dst_y = in_yuv420p->data[0];
    int dst_stride_y = in_yuv420p->width;
    uint8* dst_u = in_yuv420p->data[1];
    int dst_stride_u = dst_stride_y / 2;
    uint8* dst_v = in_yuv420p->data[2];
    int dst_stride_v = dst_stride_u;
    
    int ret = libyuv::NV12ToI420(src_y, src_stride_y,
               src_uv, src_stride_uv,
               dst_y, dst_stride_y,
               dst_u, dst_stride_u,
               dst_v, dst_stride_v,
               in_yuv420p->width, in_yuv420p->height);
    if (ret)
    {
        if (in_yuv420p) {
            for (int i = 0; i<3; i++) {
                if (in_yuv420p->data[i]!=NULL) {
                    free(in_yuv420p->data[i]);
                    in_yuv420p->data[i] = NULL;
                }
            }
            delete in_yuv420p;
            in_yuv420p = NULL;
        }
        return false;
    }
    
    VideoFrame* in_i420 = new VideoFrame;
    if (rotation == 90 || rotation==270) {
        float scale_w = in_yuv420p->width / out_rgba->height;
        float scale_y = in_yuv420p->height / out_rgba->width;
        if (scale_w < scale_y) {
            in_i420->width = out_rgba->height;
            in_i420->height = (int)((float)in_yuv420p->height / scale_w);
            if (in_i420->height%2!=0) {
                in_i420->height += 1;
            }
        }else {
            in_i420->height = out_rgba->width;
            in_i420->width = (int)((float)in_yuv420p->width / scale_y);
            if (in_i420->width%2!=0) {
                in_i420->width += 1;
            }
        }
    }else{
        float scale_w = in_yuv420p->width / out_rgba->width;
        float scale_y = in_yuv420p->height / out_rgba->height;
        if (scale_w < scale_y) {
            in_i420->width = out_rgba->width;
            in_i420->height = (int)((float)in_yuv420p->height / scale_w);
            if (in_i420->height%2!=0) {
                in_i420->height += 1;
            }
        }else {
            in_i420->height = out_rgba->height;
            in_i420->width = (int)((float)in_yuv420p->width / scale_y);
            if (in_i420->width%2!=0) {
                in_i420->width += 1;
            }
        }
    }
    
    in_i420->frameSize = in_i420->width * in_i420->height * 3 / 2;
    in_i420->data = (uint8_t*)malloc(in_i420->frameSize);
    
    //do scale
    src_y = in_yuv420p->data[0];
    src_stride_y = in_yuv420p->linesize[0];
    uint8 *src_u = in_yuv420p->data[1];
    int src_stride_u = in_yuv420p->linesize[1];
    uint8 *src_v = in_yuv420p->data[2];
    int src_stride_v = in_yuv420p->linesize[2];
    int src_width = in_yuv420p->width;
    int src_height = in_yuv420p->height;
    dst_y = in_i420->data;
    dst_stride_y = in_i420->width;
    dst_u = dst_y + dst_stride_y * in_i420->height;
    dst_stride_u = in_i420->width/2;
    dst_v = dst_u + dst_stride_u * in_i420->height/2;
    dst_stride_v = in_i420->width/2;
    int dst_width = in_i420->width;
    int dst_height = in_i420->height;
    
    ret = libyuv::I420Scale(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, src_width, src_height, dst_y, dst_stride_y, dst_u, dst_stride_u, dst_v, dst_stride_v, dst_width, dst_height, libyuv::kFilterNone);
    if (in_yuv420p) {
        for (int i = 0; i<3; i++) {
            if (in_yuv420p->data[i]!=NULL) {
                free(in_yuv420p->data[i]);
                in_yuv420p->data[i] = NULL;
            }
        }
        delete in_yuv420p;
        in_yuv420p = NULL;
    }
    if (ret) {
        if (in_i420) {
            if (in_i420->data) {
                free(in_i420->data);
                in_i420->data = NULL;
            }
            delete in_i420;
            in_i420 = NULL;
        }
        
        return false;
    }
    
    VideoFrame *out_argb = new VideoFrame;
    out_argb->width = out_rgba->width;
    out_argb->height = out_rgba->height;
    out_argb->frameSize = out_argb->width * out_argb->height * 4;
    out_argb->data = (uint8_t*)malloc(out_argb->frameSize);

    const uint8 *src_frame = in_i420->data;
    size_t src_size = in_i420->frameSize;
    
    src_width = in_i420->width;
    src_height = in_i420->height;
    
    int crop_x;
    int crop_y;
    
    int crop_width;
    int crop_height;
    
    int dst_width_pre_rotation= 0;
    int dst_height_pre_rotation = 0;
    
    if (rotation==90 || rotation==270) {
        dst_width_pre_rotation = out_argb->height;
        dst_height_pre_rotation = out_argb->width;
    }else{
        dst_width_pre_rotation = out_argb->width;
        dst_height_pre_rotation = out_argb->height;
    }
    
    crop_width = dst_width_pre_rotation;
    crop_height = dst_height_pre_rotation;
    crop_x = (src_width - crop_width)/2;
    crop_y = (src_height - crop_height)/2;
    
    uint8 *dst_argb = out_argb->data;
    int dst_stride_argb = out_argb->width*4;
    
    ret = libyuv::ConvertToARGB(src_frame, src_size, dst_argb, dst_stride_argb, crop_x, crop_y, src_width, src_height, crop_width, crop_height, libyuv::RotationMode(rotation), libyuv::FOURCC_I420);
    
    if (in_i420) {
        if (in_i420->data) {
            free(in_i420->data);
            in_i420->data = NULL;
        }
        delete in_i420;
        in_i420 = NULL;
    }
    
    if (ret)
    {
        if (out_argb) {
            if (out_argb->data) {
                free(out_argb->data);
                out_argb->data = NULL;
            }
            delete out_argb;
            out_argb = NULL;
        }
        return false;
    }
    
    const uint8 *src_argb = out_argb->data;
    int src_stride_argb = out_argb->width*4;
    uint8 *dst_bgra = out_rgba->data;
    int dst_stride_bgra = out_rgba->width*4;
    int width = out_rgba->width;
    int height = out_rgba->height;
    
    ret = libyuv::ARGBToABGR(src_argb, src_stride_argb, dst_bgra, dst_stride_bgra, width, height);
    
    if (out_argb) {
        if (out_argb->data) {
            free(out_argb->data);
            out_argb->data = NULL;
        }
        delete out_argb;
        out_argb = NULL;
    }
    
    if (ret) return false;
    
    return true;
}

bool LibyuvColorSpaceConvertUtils::NV12ToRGBA_Crop_Scale_Rotation_Plus(YUVVideoFrame* in_yuv420sp, VideoFrame* out_rgba, int rotation)
{
    return false;
}
