//
//  ColorSpaceConvert.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaStreamer__ColorSpaceConvert__
#define __MediaStreamer__ColorSpaceConvert__

#include "MediaDataType.h"

enum ALGORITHM_SOURCE
{
    LIBYUV = 0,
    FFMPEG_SWSCALE = 1,
};

class ColorSpaceConvert
{
public:
    virtual ~ColorSpaceConvert() {}
    
    static ColorSpaceConvert* CreateColorSpaceConvert(ALGORITHM_SOURCE algorithmSource);
    static ColorSpaceConvert* CreateColorSpaceConvert(ALGORITHM_SOURCE algorithmSource, int max_video_frame_size);
    static void DeleteColorSpaceConvert(ColorSpaceConvert* colorSpaceConvert, ALGORITHM_SOURCE algorithmSource);
    
    virtual bool NV21toI420(VideoFrame* nv21, VideoFrame* i420) = 0;
    virtual bool NV21toI420_Rotation(VideoFrame* nv21, VideoFrame* i420, int rotation) = 0;
    
    virtual bool NV21toI420_Crop_Rotation(VideoFrame* nv21, VideoFrame* i420, int rotation) = 0;
    virtual bool NV21toI420_Crop_Rotation_Scale(VideoFrame* nv21, VideoFrame* i420, int rotation) = 0;
    virtual bool NV21toI420_Crop_Rotation_Scale_Mirror(VideoFrame* nv21, VideoFrame* i420, int rotation) = 0;
    
    virtual bool NV21toNV12_Crop_Rotation_Scale(VideoFrame* nv21, VideoFrame* nv12, int rotation) = 0;
    virtual bool NV21toNV21_Crop_Rotation_Scale(VideoFrame* nv21, VideoFrame* outNV21, int rotation) = 0;
    
    virtual bool ABGRtoI420_Crop(VideoFrame* abgr, VideoFrame* i420) = 0;
    
    virtual bool I420toARGB_Crop_Scale(VideoFrame* in_i420, VideoFrame* out_argb) = 0;
    virtual bool I420toARGB_Crop_Rotation_Scale(VideoFrame* in_i420, VideoFrame* out_argb, int rotation) = 0;

    virtual bool I420toBGRA_Crop_Scale(VideoFrame* in_i420, VideoFrame* out_bgra) = 0;
    virtual bool I420toRGBA_Crop_Scale(VideoFrame* in_i420, VideoFrame* out_rgba) = 0;
    virtual bool I420toABGR_Crop_Rotation_Scale(VideoFrame* in_i420, VideoFrame* out_abgr, int rotation) = 0;

    virtual bool NV12toARGB_Crop_Scale(VideoFrame* in_nv12, VideoFrame* out_argb) = 0;
    virtual bool NV12toRGBA_Crop_Scale(VideoFrame* in_nv12, VideoFrame* out_rgba) = 0;
    virtual bool NV12toBGRA_Crop_Scale(VideoFrame* in_nv12, VideoFrame* out_bgra) = 0;
    virtual bool NV12toABGR_Crop_Scale(VideoFrame* in_nv12, VideoFrame* out_abgr) = 0;
    
    virtual bool BGRAtoARGB_Crop_Scale(VideoFrame* in_bgra, VideoFrame* out_argb) = 0;
    virtual bool BGRAtoBGRA_Crop_Scale(VideoFrame* in_bgra, VideoFrame* out_bgra) = 0;
        
    virtual bool I420toI420_Crop_Scale(VideoFrame* in_i420, VideoFrame* out_i420) = 0;
    
    virtual bool RGBAtoI420_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_i420) = 0;
    virtual bool ARGBtoI420_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_i420) = 0;
    virtual bool ABGRtoI420_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_i420) = 0;
    virtual bool ABGRtoI420_Scale(VideoFrame* in_abgr, VideoFrame* out_i420) = 0;

    virtual bool ABGRtoARGB_Crop_Scale(VideoFrame* in_abgr, VideoFrame* out_argb) = 0;
    
    virtual bool NV12toI420_Crop_Rotation(VideoFrame* nv12, VideoFrame* i420, int rotation) = 0;
    virtual bool NV12toI420_Crop_Rotation_Scale(VideoFrame* nv12, VideoFrame* i420, int rotation) = 0;
    virtual bool NV12toI420_Crop_Rotation_Scale_Mirror(VideoFrame* nv12, VideoFrame* i420, int rotation) = 0;
    
    virtual bool ARGBtoNV21_Crop_Scale(VideoFrame* in_argb, VideoFrame* out_nv21) = 0;
    
    virtual bool NV12toNV12_Crop_Rotation_Scale(VideoFrame* in_nv12, VideoFrame* out_nv12, int rotation) = 0;
    
    virtual bool RGBAtoARGB_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_argb) = 0;
    virtual bool RGBAtoBGRA_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_bgra) = 0;
};

#endif /* defined(__MediaStreamer__ColorSpaceConvert__) */
