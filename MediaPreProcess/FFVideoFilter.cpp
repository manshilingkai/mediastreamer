//
//  FFVideoFilter.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "FFVideoFilter.h"
#include "MediaLog.h"

FFVideoFilter::FFVideoFilter()
{
    avfilter_register_all();

    mVideoStreamContext = NULL;
    
    mGraph = NULL;
    
    in_video_filter = NULL;
    out_video_filter = NULL;
    
    mVfiltersDescripe = NULL;
    
    mOutFrame = av_frame_alloc();
}

FFVideoFilter::~FFVideoFilter()
{
    if (mOutFrame) {
        av_frame_free(&mOutFrame);
        mOutFrame = NULL;
    }
    
    if (mVfiltersDescripe) {
        av_free(mVfiltersDescripe);
        mVfiltersDescripe = NULL;
    }
}

bool FFVideoFilter::open(char* vfilters, AVStream* videoStreamContext, int rotation)
{
    mVfiltersDescripe = av_strdup(vfilters);
        
    mVideoStreamContext = videoStreamContext;
    
    mGraph = avfilter_graph_alloc();
    
    return configure_video_filters(mGraph);
}

void FFVideoFilter::dispose()
{
    if (mGraph!=NULL) {
        avfilter_graph_free(&mGraph);
        mGraph = NULL;
    }
    
    if (mVfiltersDescripe) {
        av_free(mVfiltersDescripe);
        mVfiltersDescripe = NULL;
    }
}

bool FFVideoFilter::videoIn(AVFrame *inVideoFrame)
{
    int ret = av_buffersrc_add_frame_flags(in_video_filter, inVideoFrame, 0);
    if (ret < 0)
    {
        LOGW("av_buffersrc_add_frame_flags failure");
        return false;
    }
    
    return true;
}

AVFrame * FFVideoFilter::videoOut()
{
    av_frame_unref(mOutFrame);
    
    int ret = av_buffersink_get_frame(out_video_filter, mOutFrame);
    if (ret < 0) {
        if (ret==AVERROR(EAGAIN)) {
            LOGW("Need More inFrame to filterIn");
        }else{
            LOGW("av_buffersink_get_frame failure");
        }
        
        return NULL;
    }
    
    return mOutFrame;
}

bool FFVideoFilter::configure_video_filters(AVFilterGraph *graph)
{
    char buffersrc_args[256];

    AVRational time_base = mVideoStreamContext->time_base;
    enum AVPixelFormat pix_fmts[] = { AV_PIX_FMT_YUV420P, AV_PIX_FMT_NONE };
    AVCodecParameters *codecpar = mVideoStreamContext->codecpar;
    
    AVFilterContext *filt_src = NULL, *filt_out = NULL, *last_filter = NULL;
    
    snprintf(buffersrc_args, sizeof(buffersrc_args),
             "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
             mVideoStreamContext->codec->width, mVideoStreamContext->codec->height, mVideoStreamContext->codec->pix_fmt,
             time_base.num, time_base.den,
             codecpar->sample_aspect_ratio.num, FFMAX(codecpar->sample_aspect_ratio.den, 1));
    
    int ret = avfilter_graph_create_filter(&filt_src,
                                           avfilter_get_by_name("buffer"),
                                           "in", buffersrc_args, NULL,
                                           graph);
    if (ret<0) {
        return false;
    }
    
    ret = avfilter_graph_create_filter(&filt_out,
                                       avfilter_get_by_name("buffersink"),
                                       "out", NULL, NULL, graph);
    if (ret < 0)
    {
        return false;
    }
    
    if ((ret = av_opt_set_int_list(filt_out, "pix_fmts", pix_fmts,  AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN)) < 0)
    {
        return false;
    }
        
    if ((ret = configure_filtergraph(graph, mVfiltersDescripe, filt_src, filt_out)) < 0)
    {
        return false;
    }
    
    in_video_filter  = filt_src;
    out_video_filter = filt_out;
    
    return true;
}

int FFVideoFilter::configure_filtergraph(AVFilterGraph *graph, const char *filtergraph,
                          AVFilterContext *source_ctx, AVFilterContext *sink_ctx)
{
    int ret, i;
    int nb_filters = graph->nb_filters;
    AVFilterInOut *outputs = NULL, *inputs = NULL;
    
    if (filtergraph) {
        outputs = avfilter_inout_alloc();
        inputs  = avfilter_inout_alloc();
        if (!outputs || !inputs) {
            ret = AVERROR(ENOMEM);
            
            avfilter_inout_free(&outputs);
            avfilter_inout_free(&inputs);
            
            return ret;
        }
        
        outputs->name       = av_strdup("in");
        outputs->filter_ctx = source_ctx;
        outputs->pad_idx    = 0;
        outputs->next       = NULL;
        
        inputs->name        = av_strdup("out");
        inputs->filter_ctx  = sink_ctx;
        inputs->pad_idx     = 0;
        inputs->next        = NULL;
        
        if ((ret = avfilter_graph_parse_ptr(graph, filtergraph, &inputs, &outputs, NULL)) < 0)
        {
            avfilter_inout_free(&outputs);
            avfilter_inout_free(&inputs);
            
            return ret;
        }
    } else {
        if ((ret = avfilter_link(source_ctx, 0, sink_ctx, 0)) < 0)
        {
            avfilter_inout_free(&outputs);
            avfilter_inout_free(&inputs);
            
            return ret;
        }
    }
    
    /* Reorder the filters to ensure that inputs of the custom filters are merged first */
    for (i = 0; i < graph->nb_filters - nb_filters; i++)
        FFSWAP(AVFilterContext*, graph->filters[i], graph->filters[i + nb_filters]);
    
    ret = avfilter_graph_config(graph, NULL);
    
    avfilter_inout_free(&outputs);
    avfilter_inout_free(&inputs);
    
    return ret;
}
