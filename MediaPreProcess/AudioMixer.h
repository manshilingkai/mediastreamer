//
//  AudioMixer.h
//  MediaStreamer
//
//  Created by Think on 2016/11/30.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef AudioMixer_h
#define AudioMixer_h

#include <stdlib.h>
#include <stdint.h>

#include <math.h>

#ifdef WIN32
#include <algorithm>
#endif

#ifndef INT16_MAX
#define INT16_MAX 0x7FFF
#endif

#ifndef INT16_MIN
#define INT16_MIN ~INT16_MAX
#endif

inline int16_t mixSample(int16_t a, int16_t b) {
    return
    // If both samples are negative, mixed signal must have an amplitude between the lesser of A and B, and the minimum permissible negative amplitude
    a < 0 && b < 0 ?
    ((int)a + (int)b) - (((int)a * (int)b)/INT16_MIN) :
    
    // If both samples are positive, mixed signal must have an amplitude between the greater of A and B, and the maximum permissible positive amplitude
    ( a > 0 && b > 0 ?
     ((int)a + (int)b) - (((int)a * (int)b)/INT16_MAX)
     
     // If samples are on opposite sides of the 0-crossing, mixed signal should reflect that samples cancel each other out somewhat
     :
     a + b);
}

inline int16_t fixSample(int16_t sample)
{
    if (sample > INT16_MAX)
    {
        sample = INT16_MAX;
    }
    else if (sample < INT16_MIN)
    {
        sample = INT16_MIN;
    }
    return sample;
}

inline int16_t mixSampleWithGain(int16_t srcSample1, float src1Gain, int16_t srcSample2, float src2Gain)
{
    return mixSample((int16_t)(srcSample1*src1Gain), (int16_t)(srcSample2*src2Gain));
}

inline int16_t* mixSamplesWithGain(int16_t *srcSamples1, int16_t *srcSamples2, int mixCount, float src1Gain, float src2Gain)
{
    for (int i=0; i<mixCount; i++) {
        srcSamples1[i] = mixSampleWithGain(srcSamples1[i], src1Gain, srcSamples2[i], src2Gain);
    }
    
    return srcSamples1;
}

inline int16_t* mixSamples(int16_t *srcSamples1, int16_t *srcSamples2, int mixCount)
{
    for (int i=0; i<mixCount; i++) {
        srcSamples1[i] = fixSample(mixSample(srcSamples1[i], srcSamples2[i]));
    }
    
    return srcSamples1;
}

#endif /* AudioMixer_h */
