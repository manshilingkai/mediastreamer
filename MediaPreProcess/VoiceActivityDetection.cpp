//
//  VoiceActivityDetection.cpp
//  MediaStreamer
//
//  Created by Think on 2019/12/10.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "VoiceActivityDetection.h"
#include "MediaLog.h"
#include "AudioChannelConvert.h"
#include "AudioSampleFormatConvert.h"

//#define VAD_CHANNELS 1
#define VAD_SAMPLERATE 44100
#define VAD_PER_MS 30

VoiceActivityDetection::VoiceActivityDetection()
{
    vadInst = NULL;
    
    float_pcm_data = NULL;
    float_pcm_data_size = 0;
    int16_pcm_data = NULL;
    int16_pcm_data_size = 0;
    
    mResampler = NULL;
    input_rate = 0;
    output_rate = 0;
    
    mAudioProcessFifo = av_audio_fifo_alloc(AV_SAMPLE_FMT_S16, 1, 1);
    mOutputContainerSize = VAD_SAMPLERATE*1*2;
    mOutputContainer = (char*)malloc(mOutputContainerSize);
}

VoiceActivityDetection::~VoiceActivityDetection()
{
    close();
    
    if (float_pcm_data) {
        delete [] float_pcm_data;
        float_pcm_data = NULL;
    }
    
    if (int16_pcm_data) {
        delete [] int16_pcm_data;
        int16_pcm_data = NULL;
    }
    
    if (mResampler) {
        delete mResampler;
        mResampler = NULL;
    }
    
    if (mAudioProcessFifo) {
        av_audio_fifo_free(mAudioProcessFifo);
        mAudioProcessFifo = NULL;
    }
    
    if (mOutputContainer) {
        free(mOutputContainer);
        mOutputContainer = NULL;
    }
}

bool VoiceActivityDetection::open()
{
    vadInst = WebRtcVad_Create();
    if (vadInst==NULL) return false;
    
    int status = WebRtcVad_Init(vadInst);
    if (status != 0) {
        LOGE("WebRtcVad_Init fail");
        WebRtcVad_Free(vadInst);
        vadInst = NULL;
        return false;
    }
    
    int vad_mode = 1;
    status = WebRtcVad_set_mode(vadInst, vad_mode);
    if (status != 0) {
        LOGE("WebRtcVad_set_mode fail");
        WebRtcVad_Free(vadInst);
        vadInst = NULL;
        return false;
    }
    
    LOGE("WebRtcVad_set_mode success");
    
    return true;
}

int VoiceActivityDetection::process(int sampleRate, int channles, char* pcm_data, int pcm_size)
{
    int16_t* out = NULL;
    int out_size = 0;
    
    if (channles==2) {
        int16_t* stereo = (int16_t*)pcm_data;
        int samples_per_channel = pcm_size/2/channles;
        
        if (int16_pcm_data==NULL) {
            int16_pcm_data = new int16_t[samples_per_channel];
            int16_pcm_data_size = samples_per_channel;
        }
        if (int16_pcm_data_size<samples_per_channel) {
            if (int16_pcm_data) {
                delete [] int16_pcm_data;
                int16_pcm_data = NULL;
            }
            int16_pcm_data = new int16_t[samples_per_channel];
            int16_pcm_data_size = samples_per_channel;
        }
        int16_t* mono = int16_pcm_data;
        int mono_size = samples_per_channel;
        ff_stereo_to_mono(mono, stereo, samples_per_channel);
        
        if (sampleRate!=VAD_SAMPLERATE) {
            if (float_pcm_data==NULL) {
                float_pcm_data = new float[mono_size];
                float_pcm_data_size = mono_size;
            }
            if (float_pcm_data_size<mono_size) {
                if (float_pcm_data) {
                    delete [] float_pcm_data;
                    float_pcm_data = NULL;
                }
                float_pcm_data = new float[mono_size];
                float_pcm_data_size = mono_size;
            }
            
            src_short_to_float_array(mono,float_pcm_data,mono_size);
            
            if (mResampler==NULL) {
                mResampler = new musly::resampler(sampleRate, VAD_SAMPLERATE);
                input_rate = sampleRate;
                output_rate = VAD_SAMPLERATE;
            }
            if (input_rate!=sampleRate || output_rate!=VAD_SAMPLERATE) {
                if (mResampler) {
                    delete mResampler;
                    mResampler = NULL;
                }
                
                mResampler = new musly::resampler(sampleRate, VAD_SAMPLERATE);
                input_rate = sampleRate;
                output_rate = VAD_SAMPLERATE;
            }
            
            std::vector<float> resampled_float_pcm_data = mResampler->resample(float_pcm_data, mono_size);
            size_t resampled_float_pcm_data_size = resampled_float_pcm_data.size();
            if (int16_pcm_data_size<resampled_float_pcm_data_size) {
                if (int16_pcm_data) {
                    delete [] int16_pcm_data;
                    int16_pcm_data = NULL;
                }
                int16_pcm_data = new int16_t[resampled_float_pcm_data_size];
                int16_pcm_data_size = (int)resampled_float_pcm_data_size;
            }
            
            src_float_to_short_array(resampled_float_pcm_data.data(), int16_pcm_data, (int)resampled_float_pcm_data_size);
            
            out = int16_pcm_data;
            out_size = (int)resampled_float_pcm_data_size;
        }else{
            out = mono;
            out_size = mono_size;
        }
    }else{
        int16_t* input = (int16_t*)pcm_data;
        int input_size = pcm_size/2;
                
        if (sampleRate!=VAD_SAMPLERATE) {
            if (float_pcm_data==NULL) {
                float_pcm_data = new float[input_size];
                float_pcm_data_size = input_size;
            }
            if (float_pcm_data_size<input_size) {
                if (float_pcm_data) {
                    delete [] float_pcm_data;
                    float_pcm_data = NULL;
                }
                float_pcm_data = new float[input_size];
                float_pcm_data_size = input_size;
            }
            
            src_short_to_float_array(input,float_pcm_data,input_size);
            
            if (mResampler==NULL) {
                mResampler = new musly::resampler(sampleRate, VAD_SAMPLERATE);
                input_rate = sampleRate;
                output_rate = VAD_SAMPLERATE;
            }
            if (input_rate!=sampleRate || output_rate!=VAD_SAMPLERATE) {
                if (mResampler) {
                    delete mResampler;
                    mResampler = NULL;
                }
                
                mResampler = new musly::resampler(sampleRate, VAD_SAMPLERATE);
                input_rate = sampleRate;
                output_rate = VAD_SAMPLERATE;
            }
            
            std::vector<float> resampled_float_pcm_data = mResampler->resample(float_pcm_data, input_size);
            size_t resampled_float_pcm_data_size = resampled_float_pcm_data.size();
            if (int16_pcm_data_size<resampled_float_pcm_data_size) {
                if (int16_pcm_data) {
                    delete [] int16_pcm_data;
                    int16_pcm_data = NULL;
                }
                int16_pcm_data = new int16_t[resampled_float_pcm_data_size];
                int16_pcm_data_size = (int)resampled_float_pcm_data_size;
            }
            
            src_float_to_short_array(resampled_float_pcm_data.data(), int16_pcm_data, (int)resampled_float_pcm_data_size);
            
            out = int16_pcm_data;
            out_size = (int)resampled_float_pcm_data_size;
        }else{
            out = (int16_t*)pcm_data;
            out_size = pcm_size/2;
        }
    }
    
    av_audio_fifo_write(mAudioProcessFifo, (void**)&out, out_size);
    int available_samples = av_audio_fifo_size(mAudioProcessFifo);
    int per_ms_samples = VAD_SAMPLERATE*VAD_PER_MS/1000;
    if (available_samples<per_ms_samples) {
        return -1;
    }

    int nVadRet = -1;
    for (int i = 0; i<available_samples/per_ms_samples; i++) {
//        char* pOutputContainer = mOutputContainer+i*per_ms_samples*2;
        av_audio_fifo_read(mAudioProcessFifo, (void**)&mOutputContainer, per_ms_samples);
        int keep_weight = 0;
        nVadRet = WebRtcVad_Process(vadInst, sampleRate, (const int16_t *)mOutputContainer, per_ms_samples, keep_weight);
    }
    
    if (nVadRet==1) {
        return 1;
    }
    
    if (nVadRet==0) {
        return 0;
    }
    
    return -1;
}

void VoiceActivityDetection::close()
{
    if (vadInst) {
        WebRtcVad_Free(vadInst);
        vadInst = NULL;
    }
}
