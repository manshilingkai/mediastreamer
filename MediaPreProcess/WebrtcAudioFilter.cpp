//
//  WebrtcAudioFilter.cpp
//  MediaStreamer
//
//  Created by Think on 2017/2/9.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "WebrtcAudioFilter.h"
#include "MediaLog.h"

WebrtcAudioFilter::WebrtcAudioFilter()
{
    mOutputAudioFrame = new AudioFrame;
}

WebrtcAudioFilter::~WebrtcAudioFilter()
{
    if (mOutputAudioFrame!=NULL) {
        delete mOutputAudioFrame;
        mOutputAudioFrame = NULL;
    }
}

bool WebrtcAudioFilter::open(char* afilters, uint64_t inChannelLayout, int inChannels, int inSampleRate, AVSampleFormat inSampleFormat, uint64_t outChannelLayout, int outChannels, int outSampleRate, AVSampleFormat outSampleFormat)
{
    nsxhandle = WebRtcNsx_Create();
    
    if (nsxhandle==NULL) {
        LOGE("WebRtcNsx_Create Fail!");
        return false;
    }

    if (WebRtcNsx_Init(nsxhandle,outSampleRate)) {
        WebRtcNsx_Free(nsxhandle);
        nsxhandle = NULL;
        LOGE("WebRtcNsx_Init Fail!");
        return false;
    }
    
    //0: Mild, 1: Medium , 2: Aggressive
    if (WebRtcNsx_set_policy(nsxhandle, 1)) {
        WebRtcNsx_Free(nsxhandle);
        nsxhandle = NULL;
        LOGE("WebRtcNsx_set_policy Fail!");
        return false;
    }
    
    mOutputAudioFrame->data = (uint8_t*)malloc(outSampleRate*2*1/50);
    
    return true;
}

void WebrtcAudioFilter::dispose()
{
    if (nsxhandle!=NULL) {
        WebRtcNsx_Free(nsxhandle);
        nsxhandle = NULL;
    }
    
    if (mOutputAudioFrame->data!=NULL ) {
        free(mOutputAudioFrame->data);
        mOutputAudioFrame->data = NULL;
    }
}

AudioFrame* WebrtcAudioFilter::filterIO(AudioFrame* inFrame)
{
    if (inFrame==NULL || inFrame->data==NULL || inFrame->frameSize<=0) {
        return NULL;
    }
    
    mOutputAudioFrame->frameSize = inFrame->frameSize;
    mOutputAudioFrame->pts = inFrame->pts;
    mOutputAudioFrame->duration = inFrame->duration;

    WebRtcNsx_Process(nsxhandle, (const short* const*)&(inFrame->data), 1, (short* const*)&(mOutputAudioFrame->data));
    
    return mOutputAudioFrame;
}
