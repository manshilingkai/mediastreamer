//
//  AudioEffect.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/6/24.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef AudioEffect_h
#define AudioEffect_h

#include <stdio.h>
#include "AudioProcess.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
}

class AudioEffect {
public:
    AudioEffect(int num_channels, int sample_rate);
    ~AudioEffect();
    
    int process(char* in_pcm_data, int in_pcm_size, char** out_pcm_data);
    
    void enableEffect(bool bEnable);
    void enableSoundTouch(bool bEnable);
    
    void setUserDefinedEffect(int effect);
    void setEqualizerStyle(int style);
    void setReverbStyle(int style);
    void setPitchSemiTones(int value);
private:
    int mNumChannels;
    int mSampleRate;
private:
    AudioProcess* mAudioProcess;
    AVAudioFifo* mAudioProcessFifo;
    char* mOutputContainer;
    int mOutputContainerSize;
};

#endif /* AudioEffect_h */
