//
//  FFVideoFilter.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef FFVideoFilter_h
#define FFVideoFilter_h

#include <stdio.h>
#include "VideoFilter.h"

extern "C" {
#include "libavfilter/avfilter.h"
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"
#include "libavfilter/avfiltergraph.h"
    
#include "libavformat/avformat.h"
#include "libavutil/avstring.h"
#include "libavutil/opt.h"
}

class FFVideoFilter : public VideoFilter {
public:
    FFVideoFilter();
    ~FFVideoFilter();
    
    bool open(char* vfilters, AVStream* videoStreamContext, int rotation = 0);
    void dispose();
    
    void setVideoFilter(VideoFilterType type) {}
    
    bool videoIn(AVFrame *inVideoFrame);
    void overlayIn(AVFrame *inOverlayFrame, int x, int y) {}
    void overlayIn(VideoFrame *inOverlayFrame, int x, int y) {}
    AVFrame* videoOut();
private:
    AVStream* mVideoStreamContext;
    
    AVFilterGraph *mGraph;
    
    AVFilterContext *in_video_filter;   // the first filter in the video chain
    AVFilterContext *out_video_filter;  // the last filter in the video chain
    
    bool configure_video_filters(AVFilterGraph *graph);
    
    int configure_filtergraph(AVFilterGraph *graph, const char *filtergraph,
                              AVFilterContext *source_ctx, AVFilterContext *sink_ctx);
    
    char *mVfiltersDescripe;
    
    AVFrame *mOutFrame;
};

#endif /* FFVideoFilter_h */
