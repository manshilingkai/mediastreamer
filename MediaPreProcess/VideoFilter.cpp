//
//  VideoFilter.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "VideoFilter.h"

#ifdef IOS
#include "CIVideoFilter.h"
#endif

#if defined(WIN) || defined(MAC)
#include "FFVideoFilter.h"
#endif

VideoFilter* VideoFilter::CreateVideoFilter(VideoFilterSolution solution)
{
#ifdef IOS
    if (solution==VIDEO_FILTER_IOS) {
        return new CIVideoFilter;
    }
#endif
    
#if defined(WIN) || defined(MAC)
    if (solution==VIDEO_FILTER_FFMPEG) {
        return new FFVideoFilter;
    }
#endif
    
    return NULL;
}

void VideoFilter::DeleteVideoFilter(VideoFilterSolution solution, VideoFilter* videoFilter)
{
#ifdef IOS
    if (solution==VIDEO_FILTER_IOS) {
        CIVideoFilter *ciVideoFilter = (CIVideoFilter *)videoFilter;
        if (ciVideoFilter!=NULL) {
            delete ciVideoFilter;
            ciVideoFilter = NULL;
        }
    }
#endif
    
#if defined(WIN) || defined(MAC)
    if (solution==VIDEO_FILTER_FFMPEG) {
        FFVideoFilter* ffVideoFilter = (FFVideoFilter*)videoFilter;
        delete ffVideoFilter;
        ffVideoFilter = NULL;
    }
#endif
}
