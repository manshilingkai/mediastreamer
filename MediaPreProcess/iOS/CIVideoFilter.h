//
//  CIVideoFilter.h
//  MediaStreamer
//
//  Created by Think on 2016/12/7.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef CIVideoFilter_h
#define CIVideoFilter_h

#include <stdio.h>
#include <map>
#include "VideoFilter.h"

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#include <CoreVideo/CoreVideo.h>
#include <CoreImage/CoreImage.h>

class CIVideoFilter : public VideoFilter{
public:
    CIVideoFilter();
    ~CIVideoFilter();
    
    bool open(char* vfilters, AVStream* videoStreamContext, int rotation = 0);

    void setVideoFilter(VideoFilterType type);
    
    void dispose();
    
    bool videoIn(AVFrame *inVideoFrame);
    
    void overlayIn(AVFrame *inOverlayFrame, int x, int y);
    void overlayIn(VideoFrame *inOverlayFrame, int x, int y);
    
    AVFrame* videoOut();
    
private:
    int mVideoWidth;
    int mVideoHeight;
    
    CVPixelBufferRef mRenderCVPixelBuffer;
    CIContext *mixCIContext;
    EAGLContext* mixEAGLContext;

    CIImage *videoCIImage;
    
    AVFrame *mOutFrame;
    
    int mRotation;
};

#endif /* CIVideoFilter_h */
