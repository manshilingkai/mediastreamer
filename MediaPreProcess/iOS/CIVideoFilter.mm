//
//  CIVideoFilter.cpp
//  MediaStreamer
//
//  Created by Think on 2016/12/7.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "CIVideoFilter.h"

CIVideoFilter::CIVideoFilter()
{
    mRenderCVPixelBuffer = nil;
    mixCIContext = nil;
    mixEAGLContext = nil;
    
    videoCIImage = nil;
    
    mOutFrame = av_frame_alloc();

    mRotation = 0;
}

CIVideoFilter::~CIVideoFilter()
{
    if (mOutFrame) {
        av_frame_free(&mOutFrame);
    }
}

bool CIVideoFilter::open(char* vfilters, AVStream* videoStreamContext, int rotation)
{
    if (rotation==0 || rotation==360 || rotation==180) {
        mVideoWidth = videoStreamContext->codec->width;
        mVideoHeight = videoStreamContext->codec->height;
    }else{
        mVideoHeight = videoStreamContext->codec->width;
        mVideoWidth = videoStreamContext->codec->height;
    }

    mRotation = rotation;
    
    NSDictionary* pixelBufferOptions = @{ (NSString*) kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA),
        (NSString*) kCVPixelBufferWidthKey : @(mVideoWidth),
        (NSString*) kCVPixelBufferHeightKey : @(mVideoHeight),
        (NSString*) kCVPixelBufferOpenGLESCompatibilityKey : @YES,
        (NSString*) kCVPixelBufferIOSurfacePropertiesKey : @{}};
    
    CVPixelBufferCreate(kCFAllocatorDefault, mVideoWidth, mVideoHeight, kCVPixelFormatType_32BGRA, (__bridge CFDictionaryRef)pixelBufferOptions, &mRenderCVPixelBuffer);
    
    EAGLContext* current = [EAGLContext currentContext];

    mixEAGLContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!mixEAGLContext) {
        return false;
    }
    
    if (![EAGLContext setCurrentContext:mixEAGLContext]) {
        return false;
    }
    
    NSDictionary *options = @{ kCIContextWorkingColorSpace : [NSNull null] };
    mixCIContext = [CIContext contextWithEAGLContext:mixEAGLContext options:options];

    [EAGLContext setCurrentContext:current];
    
    return true;
}

void CIVideoFilter::dispose()
{
    EAGLContext* current = [EAGLContext currentContext];
    
    [EAGLContext setCurrentContext:mixEAGLContext];
    
    if (mRenderCVPixelBuffer) {
        CVPixelBufferRelease(mRenderCVPixelBuffer);
        mRenderCVPixelBuffer = nil;
    }
    
    [EAGLContext setCurrentContext:current];

    if (mixEAGLContext!=nil) {
        [mixEAGLContext release];
        mixEAGLContext = nil;
    }
}

bool CIVideoFilter::videoIn(AVFrame *inVideoFrame)
{
    if (inVideoFrame->format==AV_PIX_FMT_VIDEOTOOLBOX && inVideoFrame->opaque!=NULL) {
        CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)inVideoFrame->opaque;
        
        NSDictionary *opt =  @{ (id)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA)};
        
        videoCIImage = [CIImage imageWithCVPixelBuffer:pixelBuffer options:opt];
        
        if (mRotation==180) {
            videoCIImage = [videoCIImage imageByApplyingTransform:CGAffineTransformMakeRotation(-M_PI/2)];
        }else if(mRotation==90) {
            videoCIImage = [videoCIImage imageByApplyingTransform:CGAffineTransformMakeRotation(-M_PI/4)];
        }else if(mRotation==270) {
            videoCIImage = [videoCIImage imageByApplyingTransform:CGAffineTransformMakeRotation(M_PI/4)];
        }
//        videoCIImage = [videoCIImage imageByApplyingTransform:CGAffineTransformMakeRotation(-M_PI/2)];
//        videoCIImage = [videoCIImage imageByApplyingTransform:CGAffineTransformMakeTranslation(0, videoCIImage.extent.size.height)];
        
        return true;
    }
    
    return false;
}

void CIVideoFilter::overlayIn(AVFrame *inOverlayFrame, int x, int y)
{
    if (inOverlayFrame->format==AV_PIX_FMT_VIDEOTOOLBOX && inOverlayFrame->opaque!=NULL) {
        CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)inOverlayFrame->opaque;
        
        NSDictionary *opt =  @{ (id)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA)};

        CIImage *overlayCIImage = [CIImage imageWithCVPixelBuffer:pixelBuffer options:opt];
//        overlayCIImage = [overlayCIImage imageByApplyingTransform:CGAffineTransformMakeScale(.25f, .25f)];
        overlayCIImage = [overlayCIImage imageByApplyingTransform:CGAffineTransformMakeTranslation(x, y)];
        videoCIImage = [overlayCIImage imageByCompositingOverImage:videoCIImage];
    }
}

void CIVideoFilter::overlayIn(VideoFrame *inOverlayFrame, int x, int y)
{
    NSDictionary *pixelBufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                           [NSDictionary dictionary], (id)kCVPixelBufferIOSurfacePropertiesKey,
                                           nil];
    
    CVPixelBufferRef pixelBuffer = nil;
    CVPixelBufferCreate(kCFAllocatorDefault, inOverlayFrame->width, inOverlayFrame->height, kCVPixelFormatType_32BGRA, (__bridge CFDictionaryRef)pixelBufferAttributes, &pixelBuffer);
    
    CVPixelBufferLockBaseAddress(pixelBuffer,0);
    uint8_t* data = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
    int frameSize = (int)CVPixelBufferGetDataSize(pixelBuffer);
    int bytesPerRow = (int)CVPixelBufferGetBytesPerRow(pixelBuffer);
    
    if (inOverlayFrame->width*4!=bytesPerRow) {
        for (int i = 0; i<inOverlayFrame->height; i++) {
            memcpy(data+bytesPerRow*i, inOverlayFrame->data+inOverlayFrame->width*4*i, inOverlayFrame->width*4);
        }
    }else{
        memcpy(data, inOverlayFrame->data, frameSize);
    }
    
    CVPixelBufferUnlockBaseAddress(pixelBuffer,0);
    
    NSDictionary *opt =  @{ (id)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA)};
    
    CIImage *overlayCIImage = [CIImage imageWithCVPixelBuffer:pixelBuffer options:opt];
    
//    overlayCIImage = [overlayCIImage imageByApplyingTransform:CGAffineTransformMakeScale(2.0f, 2.0f)];
    overlayCIImage = [overlayCIImage imageByApplyingTransform:CGAffineTransformMakeTranslation(x, y)];
    
    videoCIImage = [overlayCIImage imageByCompositingOverImage:videoCIImage];
    
    CVPixelBufferRelease(pixelBuffer);

}


AVFrame* CIVideoFilter::videoOut()
{
    EAGLContext* current = [EAGLContext currentContext];
    
    [EAGLContext setCurrentContext:mixEAGLContext];
    
    [mixCIContext render:videoCIImage toCVPixelBuffer:mRenderCVPixelBuffer];
    
    [EAGLContext setCurrentContext:current];
    
    mOutFrame->format = AV_PIX_FMT_VIDEOTOOLBOX;
    mOutFrame->opaque = (void*)mRenderCVPixelBuffer;
    
    return mOutFrame;
}

void CIVideoFilter::setVideoFilter(VideoFilterType type)
{

}

