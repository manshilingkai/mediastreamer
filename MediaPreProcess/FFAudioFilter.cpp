//
//  FFAudioFilter.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "FFAudioFilter.h"
#include "MediaLog.h"

FFAudioFilter::FFAudioFilter()
{
    avfilter_register_all();

    mOutFrame = av_frame_alloc();
    
    mAfiltersDescripe = NULL;
    
    mGraph = NULL;
}

FFAudioFilter::~FFAudioFilter()
{
    if (mAfiltersDescripe) {
        av_free(mAfiltersDescripe);
    }
    
    if (mOutFrame) {
        av_frame_free(&mOutFrame);
    }
}

bool FFAudioFilter::open(char* afilters, uint64_t inChannelLayout, int inChannels, int inSampleRate, AVSampleFormat inSampleFormat, uint64_t outChannelLayout, int outChannels, int outSampleRate, AVSampleFormat outSampleFormat)
{
    
    mAfiltersDescripe = av_strdup(afilters);

    mInChannelLayout = inChannelLayout;
    mInChannels = inChannels;
    mInSampleRate = inSampleRate;
    mInSampleFormat = inSampleFormat;
    
    mOutChannelLayout = outChannelLayout;
    mOutChannels = outChannels;
    mOutSampleRate = outSampleRate;
    mOutSampleFormat = outSampleFormat;
    
    mGraph = avfilter_graph_alloc();
    
    return configure_audio_filters(mGraph);
}

void FFAudioFilter::dispose()
{
    if (mGraph!=NULL) {
        avfilter_graph_free(&mGraph);
        mGraph = NULL;
    }
    
    if (mAfiltersDescripe) {
        av_free(mAfiltersDescripe);
        mAfiltersDescripe = NULL;
    }
}

bool FFAudioFilter::filterIn(AVFrame *inFrame)
{
    int ret = av_buffersrc_add_frame_flags(in_audio_filter, inFrame, 0);
    if (ret < 0)
    {
        LOGW("av_buffersrc_add_frame_flags failure");
        return false;
    }
    
    return true;
}

AVFrame * FFAudioFilter::filterOut()
{
    av_frame_unref(mOutFrame);
    
    int ret = av_buffersink_get_frame(out_audio_filter, mOutFrame);
    
    if (ret < 0) {
        
        if (ret==AVERROR(EAGAIN)) {
            LOGW("Need More inFrame to filterIn");
        }else{
            LOGW("av_buffersink_get_frame failure");
        }
        
        return NULL;
    }
    
    return mOutFrame;
}


bool FFAudioFilter::configure_audio_filters(AVFilterGraph *graph)
{
    static const enum AVSampleFormat sample_fmts[] = { AV_SAMPLE_FMT_S16, AV_SAMPLE_FMT_NONE };

    char aresample_swr_opts[512] = "";
    char asrc_args[256];
    
    AVFilterContext *filt_asrc = NULL, *filt_asink = NULL;
    
    int sample_rates[2] = { 0, -1 };
    int64_t channel_layouts[2] = { 0, -1 };
    int channels[2] = { 0, -1 };
    
    av_opt_set(graph, "aresample_swr_opts", aresample_swr_opts, 0);
    
    int ret = snprintf(asrc_args, sizeof(asrc_args),
                   "sample_rate=%d:sample_fmt=%s:channels=%d:time_base=%d/%d",
                   mInSampleRate, av_get_sample_fmt_name(mInSampleFormat),
                   mInChannels,
                   1, mInSampleRate);
    
    if (mInChannelLayout)
        snprintf(asrc_args + ret, sizeof(asrc_args) - ret,
                 ":channel_layout=0x%llx",  mInChannelLayout);
    
    ret = avfilter_graph_create_filter(&filt_asrc,
                                       avfilter_get_by_name("abuffer"), "ain",
                                       asrc_args, NULL, graph);
    if (ret < 0)
    {
        return false;
    }
    
    ret = avfilter_graph_create_filter(&filt_asink,
                                       avfilter_get_by_name("abuffersink"), "aout",
                                       NULL, NULL, graph);
    if (ret < 0)
    {
        return false;
    }
    
    if ((ret = av_opt_set_int_list(filt_asink, "sample_fmts", sample_fmts,  AV_SAMPLE_FMT_NONE, AV_OPT_SEARCH_CHILDREN)) < 0)
    {
        return false;
    }

    if ((ret = av_opt_set_int(filt_asink, "all_channel_counts", 1, AV_OPT_SEARCH_CHILDREN)) < 0)
    {
        return false;
    }
    
    channel_layouts[0] = mOutChannelLayout;
    channels       [0] = mOutChannels;
    sample_rates   [0] = mOutSampleRate;
    
    if ((ret = av_opt_set_int(filt_asink, "all_channel_counts", 0, AV_OPT_SEARCH_CHILDREN)) < 0)
        return false;
    if ((ret = av_opt_set_int_list(filt_asink, "channel_layouts", channel_layouts,  -1, AV_OPT_SEARCH_CHILDREN)) < 0)
        return false;
    if ((ret = av_opt_set_int_list(filt_asink, "channel_counts" , channels       ,  -1, AV_OPT_SEARCH_CHILDREN)) < 0)
        return false;
    if ((ret = av_opt_set_int_list(filt_asink, "sample_rates"   , sample_rates   ,  -1, AV_OPT_SEARCH_CHILDREN)) < 0)
        return false;
    
    if ((ret = configure_filtergraph(graph, mAfiltersDescripe, filt_asrc, filt_asink)) < 0)
    {
        return false;
    }
        
    in_audio_filter  = filt_asrc;
    out_audio_filter = filt_asink;
    
    return true;
}

int FFAudioFilter::configure_filtergraph(AVFilterGraph *graph, const char *filtergraph,
                                         AVFilterContext *source_ctx, AVFilterContext *sink_ctx)
{
    int ret, i;
    int nb_filters = graph->nb_filters;
    AVFilterInOut *outputs = NULL, *inputs = NULL;
    
    if (filtergraph) {
        outputs = avfilter_inout_alloc();
        inputs  = avfilter_inout_alloc();
        if (!outputs || !inputs) {
            ret = AVERROR(ENOMEM);
            
            avfilter_inout_free(&outputs);
            avfilter_inout_free(&inputs);
            
            return ret;
        }
        
        outputs->name       = av_strdup("in");
        outputs->filter_ctx = source_ctx;
        outputs->pad_idx    = 0;
        outputs->next       = NULL;
        
        inputs->name        = av_strdup("out");
        inputs->filter_ctx  = sink_ctx;
        inputs->pad_idx     = 0;
        inputs->next        = NULL;
        
        if ((ret = avfilter_graph_parse_ptr(graph, filtergraph, &inputs, &outputs, NULL)) < 0)
        {
            avfilter_inout_free(&outputs);
            avfilter_inout_free(&inputs);
            
            return ret;
        }
    } else {
        if ((ret = avfilter_link(source_ctx, 0, sink_ctx, 0)) < 0)
        {
            avfilter_inout_free(&outputs);
            avfilter_inout_free(&inputs);
            
            return ret;
        }
    }
    
    /* Reorder the filters to ensure that inputs of the custom filters are merged first */
    for (i = 0; i < graph->nb_filters - nb_filters; i++)
        FFSWAP(AVFilterContext*, graph->filters[i], graph->filters[i + nb_filters]);
    
    ret = avfilter_graph_config(graph, NULL);
    
    avfilter_inout_free(&outputs);
    avfilter_inout_free(&inputs);
    
    return ret;
}
