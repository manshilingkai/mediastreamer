/*
 *  Copyright (c) 2019 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

#include <stdlib.h>
#include <math.h>
#include "lpcx.h"

DurbinLpc::DurbinLpc(int lpc_order)
{
    lpc_order_ = lpc_order;

    lpcx = new double [lpc_order_ + 1];
    alpha = new double[lpc_order_+1];
    R = new double[lpc_order_+1];
}

DurbinLpc::~DurbinLpc()
{
    delete[]lpcx;
    delete[]alpha;
    delete[]R;
}

void DurbinLpc::ImplementDurbinLpc(float *data_input, int data_length)
{
    int i, j, m;
    double K;
    double Eb;
    double tt;

    for (i = 0; i <= lpc_order_; ++i) {
        R[i] = 0;
        for (j = 0; j < data_length - i; ++j) {
            R[i] += data_input[j] * data_input[j + i];
        }
    }

    //initialize K, lpcx[0], Eb
    K = -R[1] / R[0];  //m=1

    lpcx[0] = 1.0f;
    lpcx[1] = K; //m=1
    for (i = 2; i <= lpc_order_; ++i)
        lpcx[i] = 0;

    Eb = (1 - pow(K, 2)) * R[0];

    for (m = 2; m <= lpc_order_; ++m) {
        for (i = 0; i <= lpc_order_; i++) {
            alpha[i] = lpcx[i];
        }

        tt = 0;
        for (i = 1; i <= m - 1; i++)
            tt += alpha[i] * R[m - i];
        K = -(R[m] + tt) / Eb;

        lpcx[m] = K;
        for (i = 1; i <= m - 1; i++)
            lpcx[i] = alpha[i] + K * alpha[m - i];

        Eb = (1 - pow(K, 2)) * Eb;
    }
}
