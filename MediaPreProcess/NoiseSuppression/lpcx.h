/*
 *  Copyright (c) 2019 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

#ifndef LPC_H_
#define LPC_H_

class DurbinLpc
{
public:
    DurbinLpc(int lpc_order);
    ~DurbinLpc();

    void ImplementDurbinLpc(float *data_input, int data_length);
    double *lpcx;

private:
    int lpc_order_;   
    double *alpha;
    double *R;
};

#endif //LPC_H_
