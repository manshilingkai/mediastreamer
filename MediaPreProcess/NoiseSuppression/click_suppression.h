/*
 *  Copyright (c) 2019 YuPaoPao Ltd. All Rights Reserved.
 *  Author: Mr. Yu Shijing
 *  Contact: yushijing@yupaopao.cn
 */

#ifndef CLICK_SUPPRESSION_H_
#define CLICK_SUPPRESSION_H_
#include "lpcx.h"

static const int kNumHistoryGradient = 8;
static const int kNumHistoryEnergy = 3;
static const int kNumBlocksPerFrame = 10;

static const float kClickStartThreshold = 6.f;
static const float kClickEndThreshold = 3.f;
static const int kClickWidthThreshold = kNumBlocksPerFrame - 1;

static const float kGradientFloor = .0016f;
static const float kEnergyThreshold = 10.f;

#define LPC_ORDER (40)

class ClickSuppression {
public:
	ClickSuppression(int num_channels, int sample_rate, int frame_size);
	~ClickSuppression();

	//
	bool ImplementClickSuppression(short* data, int num_samples);

protected:
	int num_channels_;
	int sample_rate_;
	int frame_size_;

private:
	class DurbinLpc* pDurbinLpc = nullptr;

private:	
	int block_length_;
	int threshold_click_width_;

	float** data_frame_ = nullptr;
	float** frame_prev_ = nullptr;

	float (*energy_history_)[kNumHistoryEnergy] = nullptr;
	float (*gradient_history_)[kNumHistoryGradient] = nullptr;

	float (*filterAllZeros_buffer_)[LPC_ORDER] = nullptr;
	float (*filterAllPoles_buffer_)[LPC_ORDER] = nullptr;

	float *ee_ = nullptr;

	bool phase_startup_;

private:
	void ProcessFrame(float** data_frame, int num_samples);

	float filterAllZeros(float x, int idx_channel);
	float filterAllPoles(float x, int idx_channel);
};

#endif // CLICK_SUPPRESSION_H_