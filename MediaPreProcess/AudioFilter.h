//
//  AudioFilter.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef AudioFilter_h
#define AudioFilter_h

#include <stdio.h>

extern "C" {
#include "libavformat/avformat.h"
}

#include "MediaDataType.h"


enum AudioFilterType
{
    AUDIO_FILTER_FFMPEG = 0,
    AUDIO_FILTER_WEBRTC = 1,
};

class AudioFilter {
public:
    virtual ~AudioFilter() {}
    
    static AudioFilter* CreateAudioFilter(AudioFilterType type);
    static void DeleteAudioFilter(AudioFilterType type, AudioFilter* audioFilter);
    
    virtual bool open(char* afilters, uint64_t inChannelLayout, int inChannels, int inSampleRate, AVSampleFormat inSampleFormat, uint64_t outChannelLayout, int outChannels, int outSampleRate, AVSampleFormat outSampleFormat) = 0;
    virtual void dispose() = 0;
    
    virtual bool filterIn(AVFrame *inFrame) = 0;
    virtual AVFrame * filterOut() = 0;
    
    virtual AudioFrame* filterIO(AudioFrame* inFrame) = 0;
};

#endif /* AudioFilter_h */
