//
//  LibyuvColorSpaceConvert.h
//  MediaStreamer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef LibyuvColorSpaceConvert_h
#define LibyuvColorSpaceConvert_h

#include <stdio.h>
#include "ColorSpaceConvert.h"

class LibyuvColorSpaceConvert : public ColorSpaceConvert{

public:
    LibyuvColorSpaceConvert(int max_video_frame_size);
    LibyuvColorSpaceConvert();
    ~LibyuvColorSpaceConvert();
    
    bool NV21toI420(VideoFrame* nv21, VideoFrame* i420);
    bool NV21toI420_Rotation(VideoFrame* nv21, VideoFrame* i420, int rotation);
    bool NV21toI420_Crop_Rotation(VideoFrame* nv21, VideoFrame* i420, int rotation);
    bool NV21toI420_Crop_Rotation_Scale(VideoFrame* nv21, VideoFrame* i420, int rotation);
    bool NV21toI420_Crop_Rotation_Scale_Mirror(VideoFrame* nv21, VideoFrame* i420, int rotation);
    
    bool NV21toNV12_Crop_Rotation_Scale(VideoFrame* nv21, VideoFrame* nv12, int rotation);
    bool NV21toNV21_Crop_Rotation_Scale(VideoFrame* nv21, VideoFrame* outNV21, int rotation);
    
    bool ABGRtoI420_Crop(VideoFrame* abgr, VideoFrame* i420);
    
    bool I420toARGB_Crop_Scale(VideoFrame* in_i420, VideoFrame* out_argb);
    bool I420toARGB_Crop_Rotation_Scale(VideoFrame* in_i420, VideoFrame* out_argb, int rotation);

    bool I420toBGRA_Crop_Scale(VideoFrame* in_i420, VideoFrame* out_bgra);
    bool I420toRGBA_Crop_Scale(VideoFrame* in_i420, VideoFrame* out_rgba);
    bool I420toABGR_Crop_Rotation_Scale(VideoFrame* in_i420, VideoFrame* out_abgr, int rotation);

    bool NV12toARGB_Crop_Scale(VideoFrame* in_nv12, VideoFrame* out_argb);
    bool NV12toRGBA_Crop_Scale(VideoFrame* in_nv12, VideoFrame* out_rgba);
    bool NV12toBGRA_Crop_Scale(VideoFrame* in_nv12, VideoFrame* out_bgra);
    bool NV12toABGR_Crop_Scale(VideoFrame* in_nv12, VideoFrame* out_abgr);
    
    bool BGRAtoARGB_Crop_Scale(VideoFrame* in_bgra, VideoFrame* out_argb);
    bool BGRAtoBGRA_Crop_Scale(VideoFrame* in_bgra, VideoFrame* out_bgra);

    bool I420toI420_Crop_Scale(VideoFrame* in_i420, VideoFrame* out_i420);

    bool RGBAtoI420_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_i420);
    bool ARGBtoI420_Crop_Scale(VideoFrame* in_argb, VideoFrame* out_i420);
    bool ABGRtoI420_Crop_Scale(VideoFrame* in_abgr, VideoFrame* out_i420);
    bool ABGRtoI420_Scale(VideoFrame* in_abgr, VideoFrame* out_i420);
    
    bool ABGRtoARGB_Crop_Scale(VideoFrame* in_abgr, VideoFrame* out_argb);
    
    bool NV12toI420_Crop_Rotation(VideoFrame* nv12, VideoFrame* i420, int rotation);
    bool NV12toI420_Crop_Rotation_Scale(VideoFrame* nv12, VideoFrame* i420, int rotation);
    bool NV12toI420_Crop_Rotation_Scale_Mirror(VideoFrame* nv12, VideoFrame* i420, int rotation);
    
    bool ARGBtoNV21_Crop_Scale(VideoFrame* in_argb, VideoFrame* out_nv21);
    
    bool NV12toNV12_Crop_Rotation_Scale(VideoFrame* in_nv12, VideoFrame* out_nv12, int rotation);
    
    bool RGBAtoARGB_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_argb);
    bool RGBAtoBGRA_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_bgra);
    
private:
    VideoFrame *mVideoFrame1;
    VideoFrame *mVideoFrame2;
};

//-----------------------------------------------------------------------------------------------------------------------------------------//

class LibyuvColorSpaceConvertUtils {
public:
    static bool RGBPtoARGB_Crop_Scale(VideoFrame* in_rgbp, VideoFrame* out_argb);
    static bool RGBPtoRGBA_Crop_Scale(VideoFrame* in_rgbp, VideoFrame* out_rgba);

    static bool RGBAtoARGB_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_argb);
    static bool RGBAtoRGBA_Crop_Scale(VideoFrame* in_rgba, VideoFrame* out_rgba);
    
    static bool I420ToRGBA_Crop_Scale_Rotation(YUVVideoFrame* in_yuv420p, VideoFrame* out_rgba, int rotation);
    static bool NV12ToRGBA_Crop_Scale_Rotation(YUVVideoFrame* in_yuv420sp, VideoFrame* out_rgba, int rotation);
    static bool NV12ToRGBA_Crop_Scale_Rotation_Plus(YUVVideoFrame* in_yuv420sp, VideoFrame* out_rgba, int rotation);
};

#endif /* LibyuvColorSpaceConvert_h */
