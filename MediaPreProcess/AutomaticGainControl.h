//
//  AutomaticGainControl.h
//  MediaPlayer
//
//  Created by slklovewyy on 2020/3/9.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef AutomaticGainControl_h
#define AutomaticGainControl_h

#include <stdio.h>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
#include <libavutil/avstring.h>

#include "libavutil/avhook.h"
}

enum AGCLevel {
    AGC_LEVEL_LOW = 0,
    AGC_LEVEL_LOW_MIDDLE,
    AGC_LEVEL_MIDDLE,
    AGC_LEVEL_MIDDLE_HIGH,
    AGC_LEVEL_HIGH
};

class AutomaticGainControl {
public:
    AutomaticGainControl(int sampleRate, int channles);
    ~AutomaticGainControl();
    
    bool open(int agc_level);
    void close();
    
    int process(char* in_pcm_data, int in_pcm_size);
private:
    int mSampleRate;
    int mChannels;
    
    void *agcInst;
    
    int mSamplesPerProcess;
    char *mInputPerProcess;
    char *mOutputPerProcess;
    
    AVAudioFifo* mInProcessFifo;
    AVAudioFifo* mOutProcessFifo;
};

#endif /* AutomaticGainControl_h */
