//
//  Overlay.h
//  MediaStreamer
//
//  Created by Think on 2020/3/24.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef Overlay_h
#define Overlay_h

#include <stdio.h>

extern "C" {
#include "libavformat/avformat.h"
}

#include "MediaDataType.h"

class Overlay {
public:
    static void blend_image_i420(VideoFrame *i420_main, const VideoFrame *rgba_overlay, int x, int y, float alpha);
    static void blend_image_rgba(VideoFrame *main, const VideoFrame *overlay, int x, int y);
    static void get_alpha_channel_from_right_mask(VideoFrame *target, const VideoFrame *origin);
};

#endif /* Overlay_h */
