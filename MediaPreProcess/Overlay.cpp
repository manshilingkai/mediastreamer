//
//  Overlay.cpp
//  MediaStreamer
//
//  Created by Think on 2020/3/24.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "Overlay.h"
#include "libyuv.h"

// divide by 255 and round to nearest
// apply a fast variant: (X+127)/255 = ((X+127)*257+257)>>16 = ((X+128)*257)>>16
#define FAST_DIV255(x) ((((x) + 128) * 257) >> 16)

void Overlay::blend_image_i420(VideoFrame *i420_main, const VideoFrame *rgba_overlay, int x, int y, float alpha)
{
    uint8 *i420_overlay_data = new uint8[rgba_overlay->width * rgba_overlay->height * 3/2];
    const uint8 *rgba_overlay_data = rgba_overlay->data;
    size_t rgba_overlay_size = rgba_overlay->frameSize;
    uint8 *i420_overlay_data_y = i420_overlay_data;
    int i420_overlay_stride_y = rgba_overlay->width;
    uint8 *i420_overlay_data_u = i420_overlay_data_y + i420_overlay_stride_y * rgba_overlay->height;
    int i420_overlay_stride_u = rgba_overlay->width/2;
    uint8 *i420_overlay_data_v = i420_overlay_data_u + i420_overlay_stride_u * rgba_overlay->height/2;
    int i420_overlay_stride_v = rgba_overlay->width/2;
    
    int ret = libyuv::ConvertToI420(rgba_overlay_data, rgba_overlay_size, i420_overlay_data_y, i420_overlay_stride_y, i420_overlay_data_u, i420_overlay_stride_u, i420_overlay_data_v, i420_overlay_stride_v, 0, 0, rgba_overlay->width, rgba_overlay->height, rgba_overlay->width, rgba_overlay->height, libyuv::kRotate0, libyuv::FOURCC_RGBA);
    
    if (ret) {
        delete [] i420_overlay_data;
        return;
    }

    uint8_t* i420_main_data_y = i420_main->data;
    int i420_main_stride_y = i420_main->width;
    uint8_t* i420_main_data_u = i420_main_data_y + i420_main_stride_y * i420_main->height;
    int i420_main_stride_u = i420_main->width / 2;
    uint8_t* i420_main_data_v = i420_main_data_u + i420_main_stride_u * i420_main->height / 2;
    int i420_main_stride_v = i420_main->width / 2;

    int left = x;
    int right = x + rgba_overlay->width;
    int top = y;
    int bottom = y + rgba_overlay->height;

    for (int i = top; i < bottom; i++) {
        for (int j = left; j < right; j++) {
            //y
            int overlay_i = i - top;
            int overlay_j = j - left;
            int main_y = *(i420_main_data_y + i * i420_main_stride_y + j);
            int overlay_y = *(i420_overlay_data_y + overlay_i * i420_overlay_stride_y + overlay_j);
            float overlay_alpha = *(rgba_overlay_data + overlay_i * rgba_overlay->width * 4 + overlay_j * 4 + 3) * alpha;
            *(i420_main_data_y + i * i420_main_stride_y + j) = FAST_DIV255((int)(main_y * (255 - overlay_alpha) + overlay_y * overlay_alpha));
            //u
            int main_uv_i = i / 2;
            int main_uv_j = j / 2;
            int overlay_uv_i = overlay_i / 2;
            int overlay_uv_j = overlay_j / 2;
            int main_u = *(i420_main_data_u + main_uv_i * i420_main_stride_u + main_uv_j);
            int overlay_u = *(i420_overlay_data_u + overlay_uv_i * i420_overlay_stride_u + overlay_uv_j);
            *(i420_main_data_u + main_uv_i * i420_main_stride_u + main_uv_j) = FAST_DIV255((int)(main_u * (255 - overlay_alpha) + overlay_u * overlay_alpha));
            //v
            int main_v = *(i420_main_data_v + main_uv_i * i420_main_stride_v + main_uv_j);
            int overlay_v = *(i420_overlay_data_v + overlay_uv_i * i420_overlay_stride_v + overlay_uv_j);
            *(i420_main_data_v + main_uv_i * i420_main_stride_v + main_uv_j) = FAST_DIV255((int)(main_v * (255 - overlay_alpha) + overlay_v * overlay_alpha));
        }
    }

    delete [] i420_overlay_data;
}

void Overlay::blend_image_rgba(VideoFrame *main, const VideoFrame *overlay, int x, int y)
{
    int real_overlay_height = 0;
    int real_overlay_width = 0;
    if (x>=0) {
        real_overlay_width = main->width-x < overlay->width ? main->width-x : overlay->width;
    }else {
        real_overlay_width = main->width < overlay->width + x ? main->width : overlay->width + x;
    }
    
    if (y>=0) {
        real_overlay_height = main->height-y < overlay->height ? main->height-y : overlay->height;
    }else {
        real_overlay_height = main->height < overlay->height + y ? main->height : overlay->height + y;
    }
    
    for (int j=0; j<real_overlay_height; j++) {
        for (int i=0; i<real_overlay_width; i++) {
            int overlay_pixel_pos = 0;
            int main_pixel_pos = 0;
            if (x>=0) {
                if (y>=0) {
                    overlay_pixel_pos = j*overlay->width+i;
                }else{
                    overlay_pixel_pos = (-y+j)*overlay->width+i;
                }
            }else{
                if (y>=0) {
                    overlay_pixel_pos = j*overlay->width+i-x;
                }else{
                    overlay_pixel_pos = (-y+j)*overlay->width+i-x;
                }
            }
            if (x>=0) {
                if (y>=0) {
                    main_pixel_pos = (y+j)*main->width+(x+i);
                }else{
                    main_pixel_pos = j*main->width+(x+i);
                }
            }else{
                if (y>=0) {
                    main_pixel_pos = (y+j)*main->width+i;
                }else{
                    main_pixel_pos = j*main->width+i;
                }
            }

            if (main->videoRawType == VIDEOFRAME_RAWTYPE_BGRA && overlay->videoRawType == VIDEOFRAME_RAWTYPE_RGBA) {
                uint8_t overlay_alpha = *(overlay->data+overlay_pixel_pos*4+3);
                if (overlay_alpha==255) {
                    main->data[main_pixel_pos*4] = *(overlay->data+overlay_pixel_pos*4+2);
                    main->data[main_pixel_pos*4+1] = *(overlay->data+overlay_pixel_pos*4+1);
                    main->data[main_pixel_pos*4+2] = *(overlay->data+overlay_pixel_pos*4+0);
                    main->data[main_pixel_pos*4+3] = *(overlay->data+overlay_pixel_pos*4+3);
                }else if(overlay_alpha<255 && overlay_alpha>0) {
                    main->data[main_pixel_pos*4] = FAST_DIV255(main->data[main_pixel_pos*4] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+2] * overlay_alpha);
                    main->data[main_pixel_pos*4+1] = FAST_DIV255(main->data[main_pixel_pos*4+1] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+1] * overlay_alpha);
                    main->data[main_pixel_pos*4+2] = FAST_DIV255(main->data[main_pixel_pos*4+2] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+0] * overlay_alpha);
                    main->data[main_pixel_pos*4+3] = FAST_DIV255(main->data[main_pixel_pos*4+3] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+3] * overlay_alpha);
                }
            }else if (main->videoRawType == VIDEOFRAME_RAWTYPE_RGBA && overlay->videoRawType == VIDEOFRAME_RAWTYPE_RGBA) {
                uint8_t overlay_alpha = *(overlay->data+overlay_pixel_pos*4+3);
                if (overlay_alpha==255) {
                    main->data[main_pixel_pos*4] = *(overlay->data+overlay_pixel_pos*4); //R
                    main->data[main_pixel_pos*4+1] = *(overlay->data+overlay_pixel_pos*4+1); //G
                    main->data[main_pixel_pos*4+2] = *(overlay->data+overlay_pixel_pos*4+2); //B
                    main->data[main_pixel_pos*4+3] = *(overlay->data+overlay_pixel_pos*4+3); //A
                }else if(overlay_alpha<255 && overlay_alpha>0) {
                    main->data[main_pixel_pos*4] = FAST_DIV255(main->data[main_pixel_pos*4] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4] * overlay_alpha); //R
                    main->data[main_pixel_pos*4+1] = FAST_DIV255(main->data[main_pixel_pos*4+1] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+1] * overlay_alpha); //G
                    main->data[main_pixel_pos*4+2] = FAST_DIV255(main->data[main_pixel_pos*4+2] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+2] * overlay_alpha); //B
                    main->data[main_pixel_pos*4+3] = FAST_DIV255(main->data[main_pixel_pos*4+3] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+3] * overlay_alpha); //A
                }
            }else {
                uint8_t overlay_alpha = *(overlay->data+overlay_pixel_pos*4);
                if (overlay_alpha==255) {
                    main->data[main_pixel_pos*4] = *(overlay->data+overlay_pixel_pos*4); //A
                    main->data[main_pixel_pos*4+1] = *(overlay->data+overlay_pixel_pos*4+1); //B
                    main->data[main_pixel_pos*4+2] = *(overlay->data+overlay_pixel_pos*4+2); //G
                    main->data[main_pixel_pos*4+3] = *(overlay->data+overlay_pixel_pos*4+3); //R
                }else if(overlay_alpha<255 && overlay_alpha>0) {
                    main->data[main_pixel_pos*4] = FAST_DIV255(main->data[main_pixel_pos*4] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4] * overlay_alpha); //A
                    main->data[main_pixel_pos*4+1] = FAST_DIV255(main->data[main_pixel_pos*4+1] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+1] * overlay_alpha); //B
                    main->data[main_pixel_pos*4+2] = FAST_DIV255(main->data[main_pixel_pos*4+2] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+2] * overlay_alpha); //G
                    main->data[main_pixel_pos*4+3] = FAST_DIV255(main->data[main_pixel_pos*4+3] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+3] * overlay_alpha); //R
                }
            }
        }
    }
}

void Overlay::get_alpha_channel_from_right_mask(VideoFrame *target, const VideoFrame *origin)
{
    target->width = origin->width/2;
    target->height = origin->height;
    
    for (int j = 0; j<origin->height; j++) {
        for (int i = 0; i<origin->width/2; i++) {
            int origin_rgb_pixel_pos = j * origin->width + i;
            int origin_alpha_pixel_pos = j * origin->width + i + origin->width/2;
            int target_pixel_pos = j * origin->width/2 + i;
            
            target->data[target_pixel_pos*4] = origin->data[origin_alpha_pixel_pos*4+1]; //A
            target->data[target_pixel_pos*4+1] = origin->data[origin_rgb_pixel_pos*4+1]; //B
            target->data[target_pixel_pos*4+2] = origin->data[origin_rgb_pixel_pos*4+2]; //G
            target->data[target_pixel_pos*4+3] = origin->data[origin_rgb_pixel_pos*4+3]; //R
        }
    }
}

