//
//  VideoFilter.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef VideoFilter_h
#define VideoFilter_h

#include <stdio.h>

extern "C" {
#include "libavformat/avformat.h"
}

#include "MediaDataType.h"

enum VideoFilterSolution
{
    VIDEO_FILTER_FFMPEG = 0,
    VIDEO_FILTER_IOS = 1
};

enum VideoFilterType
{
};

class VideoFilter {
    
public:
    virtual ~VideoFilter() {}
    
    static VideoFilter* CreateVideoFilter(VideoFilterSolution solution);
    static void DeleteVideoFilter(VideoFilterSolution solution, VideoFilter* videoFilter);
    
    virtual bool open(char* vfilters, AVStream* videoStreamContext, int rotation = 0) = 0;
    
    virtual void setVideoFilter(VideoFilterType type) = 0;
    
    virtual void dispose() = 0;
    
    virtual bool videoIn(AVFrame *inVideoFrame) = 0;
    virtual void overlayIn(AVFrame *inOverlayFrame, int x, int y) = 0;
    virtual void overlayIn(VideoFrame *inOverlayFrame, int x, int y) = 0;
    virtual AVFrame* videoOut() = 0;
};

#endif /* VideoFilter_h */
