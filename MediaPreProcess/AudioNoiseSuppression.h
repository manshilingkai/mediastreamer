//
//  AudioNoiseSuppression.h
//  MediaStreamer
//
//  Created by Think on 2019/12/9.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef AudioNoiseSuppression_h
#define AudioNoiseSuppression_h

#include <stdio.h>
#include "noise_suppression.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
#include <libavutil/avstring.h>

#include "libavutil/avhook.h"
}

using namespace noiseSuppression;

class AudioNoiseSuppression {
public:
    AudioNoiseSuppression(int num_channels, int sample_rate);
    ~AudioNoiseSuppression();
    
    int process(char* in_pcm_data, int in_pcm_size, char** out_pcm_data);
private:
    int mNumChannels;
    int mSampleRate;
private:
    NoiseSuppression* mNoiseSuppression;
    AVAudioFifo* mAudioProcessFifo;
    char* mOutputContainer;
    int mOutputContainerSize;
};

#endif /* AudioNoiseSuppression_h */
