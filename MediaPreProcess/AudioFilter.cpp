//
//  AudioFilter.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "AudioFilter.h"
#include "FFAudioFilter.h"

//#include "WebrtcAudioFilter.h"

AudioFilter* AudioFilter::CreateAudioFilter(AudioFilterType type)
{
    if (type == AUDIO_FILTER_FFMPEG) {
        return new FFAudioFilter;
    }else if(type == AUDIO_FILTER_WEBRTC) {
//        return new WebrtcAudioFilter;
    }

    return NULL;
}

void AudioFilter::DeleteAudioFilter(AudioFilterType type, AudioFilter* audioFilter)
{
    if (type == AUDIO_FILTER_FFMPEG) {
        FFAudioFilter *ffAudioFilter = (FFAudioFilter *)audioFilter;
        if (ffAudioFilter!=NULL) {
            delete ffAudioFilter;
            ffAudioFilter = NULL;
        }
    }else if(type == AUDIO_FILTER_WEBRTC) {
//        WebrtcAudioFilter* webrtcAudioFilter = (WebrtcAudioFilter *)audioFilter;
//        if (webrtcAudioFilter!=NULL) {
//            delete webrtcAudioFilter;
//            webrtcAudioFilter = NULL;
//        }
    }
}
