//
//  LocalMP3MediaSource.h
//  MediaPlayer
//
//  Created by Think on 2017/2/24.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef LocalMP3MediaSource_h
#define LocalMP3MediaSource_h

#include <stdio.h>
#include "CustomMediaSource.h"

extern "C" {
#include "libavformat/avformat.h"
}

class LocalMP3MediaSource : public CustomMediaSource {
public:
    LocalMP3MediaSource();
    ~LocalMP3MediaSource();
    
    bool open(char* url);
    void close();
    
    int readPacket(uint8_t *buf, int buf_size);
    int64_t seek(int64_t offset, int whence);
private:
    FILE* file;
};

#endif /* LocalMP3MediaSource_h */
