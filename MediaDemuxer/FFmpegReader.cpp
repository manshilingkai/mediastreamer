//
//  FFmpegReader.cpp
//  MediaStreamer
//
//  Created by Think on 16/11/7.
//  Copyright © 2016年 Cell. All rights reserved.
//

#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#include "FFmpegReader.h"

#include "MediaLog.h"
#include "StringUtils.h"

#include "FFLog.h"

FFmpegReader::FFmpegReader(char *inputFile)
{
    mUrl = strdup(inputFile);
    
    pthread_mutex_init(&mLock, NULL);
    
    isInterrupt = 0;
    
    mCustomMediaSource = NULL;
    mCustomMediaSourceType = UNKNOWN_CUSTOM_MEDIA_SOURCE;
    
    char dst[8];
    StringUtils::left(dst, mUrl, 1);
    
    if (dst[0]=='/') {
        StringUtils::right(dst, mUrl, 4);
        
        if (!strcmp(dst, ".mp3") || !strcmp(dst, ".MP3")) {
            mCustomMediaSourceType = LOCAL_MP3_FILE;
        }
    }
    
    mCustomMediaSource = CustomMediaSource::CreateCustomMediaSource(mCustomMediaSourceType);
}

FFmpegReader::~FFmpegReader()
{
    if (mUrl) {
        free(mUrl);
        mUrl = NULL;
    }
    
    pthread_mutex_destroy(&mLock);
    
    if (mCustomMediaSource!=NULL) {
        CustomMediaSource::DeleteCustomMediaSource(mCustomMediaSource, mCustomMediaSourceType);
    }
}

bool FFmpegReader::open()
{
    av_register_all();
    
    FFLog::setLogLevel(AV_LOG_WARNING);
    
    //open custom media source
    if (mCustomMediaSource!=NULL) {
        bool ret = mCustomMediaSource->open(mUrl);
        
        if (!ret) {
            LOGE("%s",mUrl);
            LOGE("[FFmpegReader]:Fail to Open Custom Media Source");
            return false;
        }
    }
    
    mAVFormatContext = avformat_alloc_context();
    if (mAVFormatContext==NULL)
    {
        if (mCustomMediaSource!=NULL) {
            mCustomMediaSource->close();
        }
        
        LOGE("%s","[FFmpegReader]:Fail Allocate an AVFormatContext");
        return false;
    }
    
    AVInputFormat* inputFmt = NULL;
    if (mCustomMediaSource!=NULL) {
        int buffer_size = 1024 * 4;
        unsigned char* buffer = (unsigned char*)av_malloc(buffer_size);
        mAVFormatContext->pb = avio_alloc_context(buffer, buffer_size, 0, mCustomMediaSource, readPacket, NULL, seek);
        mAVFormatContext->flags |= AVFMT_FLAG_CUSTOM_IO;
        
        if (!av_probe_input_buffer(mAVFormatContext->pb, &inputFmt, mUrl, NULL, 0, mAVFormatContext->probesize))
        {
            LOGD("Custom Media Source Format:%s[%s]\n", inputFmt->name, inputFmt->long_name);
            
        }else{
            LOGE("Probe Custom Media Source Format Failed\n");
            
            if (mCustomMediaSourceType == LOCAL_MP3_FILE) {
                inputFmt = av_find_input_format("mp3");
            }
        }
    }
    
    AVDictionary* options = NULL;
    mAVFormatContext->interrupt_callback.callback = interruptCallback;
    mAVFormatContext->interrupt_callback.opaque = this;
    
    mAVFormatContext->flags |= AVFMT_FLAG_NONBLOCK;
    mAVFormatContext->flags |= AVFMT_FLAG_DISCARD_CORRUPT;
    mAVFormatContext->flags |= AVFMT_FLAG_FAST_SEEK;
    
    int err = -1;
    if (mCustomMediaSource!=NULL) {
        err = avformat_open_input(&mAVFormatContext, "", inputFmt, &options);
    }else{
        err = avformat_open_input(&mAVFormatContext, mUrl, inputFmt, &options);
    }
    
    if(err<0)
    {
        if (mCustomMediaSource!=NULL) {
            mCustomMediaSource->close();
            
            if(mAVFormatContext->pb) {
                if(mAVFormatContext->pb->buffer) {
                    av_free(mAVFormatContext->pb->buffer);
                    mAVFormatContext->pb->buffer = NULL;
                }
                
                av_free(mAVFormatContext->pb);
                mAVFormatContext->pb = NULL;
            }
        }
        
        avformat_free_context(mAVFormatContext);
        mAVFormatContext = NULL;
        
        LOGE("%s",mUrl);
        LOGE("%s","Open Input File Fail");
        LOGE("ERROR CODE:%d",err);
        return false;
    }
    
    err = avformat_find_stream_info(mAVFormatContext, NULL);
    if (err < 0)
    {
        if (mCustomMediaSource!=NULL) {
            mCustomMediaSource->close();
            
            if(mAVFormatContext->pb) {
                if(mAVFormatContext->pb->buffer) {
                    av_free(mAVFormatContext->pb->buffer);
                    mAVFormatContext->pb->buffer = NULL;
                }
                
                av_free(mAVFormatContext->pb);
                mAVFormatContext->pb = NULL;
            }
        }
        
        avformat_close_input(&mAVFormatContext);
        avformat_free_context(mAVFormatContext);
        mAVFormatContext = NULL;
        
        LOGE("%s","Get Stream Info Fail");
        LOGE("ERROR CODE:%d",err);
        return false;
    }
    
    LOGD("Input File Duration Ms:%lld",av_rescale(mAVFormatContext->duration, 1000, AV_TIME_BASE) );
    
    // get track info
    mTrackCount = mAVFormatContext->nb_streams;
    
    // select default video and audio track
    mAudioStreamIndex = -1;
    mVideoStreamIndex = -1;
    mTextStreamIndex = -1;
    for(int i= 0; i < mTrackCount; i++)
    {
        if (mAVFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first audio stream, and discard others.
            if(mAudioStreamIndex == -1)
            {
                mAudioStreamIndex = i;
            }
            else
            {
                mAVFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (mAVFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO /*&& mAVFormatContext->streams[i]->codec->codec_id==AV_CODEC_ID_H264*/)
        {
            //by default, use the first video stream, and discard others.
            if(mVideoStreamIndex == -1)
            {
                mVideoStreamIndex = i;
            }
            else
            {
                mAVFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (mAVFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
        {
            //by default, use the first text stream, and discard others.
            
            if (mTextStreamIndex==-1) {
                mTextStreamIndex = i;
            }else
            {
                mAVFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }
    }
    
    LOGD("mVideoStreamIndex:%d",mVideoStreamIndex);
    LOGD("mAudioStreamIndex:%d",mAudioStreamIndex);
    LOGD("mTextStreamIndex:%d",mTextStreamIndex);
    
    if (mAVFormatContext->iformat) {
        if (mAVFormatContext->iformat->name) {
            LOGD("input format name:%s",mAVFormatContext->iformat->name);
        }
        if (mAVFormatContext->iformat->long_name) {
            LOGD("input format long name:%s",mAVFormatContext->iformat->long_name);
        }
    }
    
    return true;
}

void FFmpegReader::close()
{
    interrupt();
    
    if (mAVFormatContext!=NULL) {
        
        if (mCustomMediaSource!=NULL) {
            if(mAVFormatContext->pb) {
                if(mAVFormatContext->pb->buffer) {
                    av_free(mAVFormatContext->pb->buffer);
                    mAVFormatContext->pb->buffer = NULL;
                }
                
                av_free(mAVFormatContext->pb);
                mAVFormatContext->pb = NULL;
            }
        }
        
        avformat_close_input(&mAVFormatContext);
        avformat_free_context(mAVFormatContext);
        mAVFormatContext = NULL;
    }
    
    if (mCustomMediaSource!=NULL) {
        mCustomMediaSource->close();
    }
}

FFSeekStatus FFmpegReader::seek(int64_t seekPosMs)
{
    FFSeekStatus seekStatus;
    
    mSeekTargetStreamIndex = -1;
    
    if (mVideoStreamIndex >= 0)
        mSeekTargetStreamIndex = mVideoStreamIndex;
    else if (mAudioStreamIndex >= 0)
        mSeekTargetStreamIndex = mAudioStreamIndex;
    
    mSeekTargetPos= av_rescale_q(seekPosMs*1000ll, AV_TIME_BASE_Q, mAVFormatContext->streams[mSeekTargetStreamIndex]->time_base) + mAVFormatContext->streams[mSeekTargetStreamIndex]->start_time;
    
    if (mSeekTargetPos>mAVFormatContext->streams[mSeekTargetStreamIndex]->start_time + mAVFormatContext->streams[mSeekTargetStreamIndex]->duration) {
        mSeekTargetPos = mAVFormatContext->streams[mSeekTargetStreamIndex]->start_time + mAVFormatContext->streams[mSeekTargetStreamIndex]->duration;
    }
    
    seekStatus.seekTargetStreamIndex = mSeekTargetStreamIndex;
    seekStatus.seekTargetPos = mSeekTargetPos;

    LOGD("In Function : avformat_seek_file");
    int ret = avformat_seek_file(mAVFormatContext, mSeekTargetStreamIndex, INT64_MIN, mSeekTargetPos, INT64_MAX, AVSEEK_FLAG_BACKWARD);
    //            int ret = av_seek_frame(avFormatContext, mSeekTargetStreamIndex, mSeekTargetPos, AVSEEK_FLAG_BACKWARD);
    LOGD("Out Function : avformat_seek_file");
    
    if (ret<0)
    {
        seekStatus.seekRet = false;
    }
    else
    {
        seekStatus.seekRet = true;
    }
    
    return seekStatus;
}

AVFormatContext* FFmpegReader::getAVFormatContext()
{
    return mAVFormatContext;
}

AVStream* FFmpegReader::getVideoStreamContext()
{
    if (mAVFormatContext==NULL || mVideoStreamIndex==-1) {
        return NULL;
    }else {
        return mAVFormatContext->streams[mVideoStreamIndex];
    }
}

AVStream* FFmpegReader::getAudioStreamContext()
{
    if (mAVFormatContext==NULL || mAudioStreamIndex==-1) {
        return NULL;
    }else {
        return mAVFormatContext->streams[mAudioStreamIndex];
    }
}

int FFmpegReader::interruptCallback(void* opaque)
{
    FFmpegReader *thiz = (FFmpegReader *)opaque;
    return thiz->interruptCallbackMain();
}

int FFmpegReader::interruptCallbackMain()
{
    int ret = 0;
    pthread_mutex_lock(&mLock);
    ret = isInterrupt;
    pthread_mutex_unlock(&mLock);
    
    return ret;
}

void FFmpegReader::interrupt()
{
    pthread_mutex_lock(&mLock);
    isInterrupt = 1;
    pthread_mutex_unlock(&mLock);
}

FFPacket* FFmpegReader::getMediaPacket()
{
    AVPacket* pPacket = (AVPacket*)av_malloc(sizeof(AVPacket));
    av_init_packet(pPacket);
    pPacket->data = NULL;
    pPacket->size = 0;
    pPacket->flags = 0;
    int ret = av_read_frame(mAVFormatContext, pPacket);
    
    ffPacket.avPacket = pPacket;
    ffPacket.ret = ret;

    if (pPacket->stream_index==mAudioStreamIndex) {
        ffPacket.mediaType = 1;
    }else if (pPacket->stream_index==mVideoStreamIndex) {
        ffPacket.mediaType = 0;
    }else if (pPacket->stream_index==mTextStreamIndex) {
        ffPacket.mediaType = 2;
    }else {
        ffPacket.mediaType = -1;
    }
    
    return &ffPacket;
}

int64_t FFmpegReader::getDuration()
{
    if (mAVFormatContext) {
        return av_rescale(mAVFormatContext->duration, 1000, AV_TIME_BASE);
    }
    
    return 0;
}

int FFmpegReader::getFps()
{
    int frameRate = 0;
    if (mAVFormatContext && mVideoStreamIndex!=-1 && mAVFormatContext->streams[mVideoStreamIndex]!=NULL) {
        frameRate = 20;//default
        AVRational fr = av_guess_frame_rate(mAVFormatContext, mAVFormatContext->streams[mVideoStreamIndex], NULL);
        if(fr.num > 0 && fr.den > 0)
        {
            frameRate = fr.num/fr.den;
            if(frameRate > 100 || frameRate <= 0)
            {
                frameRate = 20;
            }
        }
    }
    
    return frameRate;
}


int FFmpegReader::readPacket(void *opaque, uint8_t *buf, int buf_size)
{
    CustomMediaSource *customMediaSource = (CustomMediaSource*)opaque;
    return customMediaSource->readPacket(buf, buf_size);
}

int64_t FFmpegReader::seek(void *opaque, int64_t offset, int whence)
{
    CustomMediaSource *customMediaSource = (CustomMediaSource*)opaque;
    
    return customMediaSource->seek(offset, whence);
}
