//
//  FFmpegReader.h
//  MediaStreamer
//
//  Created by Think on 16/11/7.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef FFmpegReader_h
#define FFmpegReader_h

#include <stdio.h>
#include <pthread.h>

extern "C" {
#include "libavformat/avformat.h"
}

#include "CustomMediaSource.h"

struct FFSeekStatus
{
    int seekTargetStreamIndex;

    int64_t seekTargetPos;
    
    bool seekRet;
    
    FFSeekStatus()
    {
        seekTargetStreamIndex = -1;
        seekTargetPos = 0ll;
        
        seekRet = false;
    }
};

struct FFPacket {
    AVPacket* avPacket;
    int mediaType; //0:Video 1:Audio 2:Text -1:unknown
    
    int ret;
    
    FFPacket()
    {
        avPacket = NULL;
        mediaType = -1;
        
        ret = -1;
        
    }
};

class FFmpegReader {
    
public:
    FFmpegReader(char *inputFile);
    ~FFmpegReader();
    
    bool open();
    void close();
    
    FFSeekStatus seek(int64_t seekPosMs);
    
    AVStream* getVideoStreamContext();
    AVStream* getAudioStreamContext();
    
    AVFormatContext* getAVFormatContext();
    
    FFPacket* getMediaPacket();
    
    void interrupt();
    
    int64_t getDuration();
    int getFps();
    
private:
    char *mUrl;
    pthread_mutex_t mLock;

    AVFormatContext *mAVFormatContext;
    int mAudioStreamIndex;
    int mVideoStreamIndex;
    int mTextStreamIndex;
    int mTrackCount;
    
    static int interruptCallback(void* opaque);
    int interruptCallbackMain();
    int isInterrupt; // critical value
    
    FFPacket ffPacket;
    
private:
    int mSeekTargetStreamIndex;
    int64_t mSeekTargetPos;
    
private:
    // custom
    CustomMediaSourceType mCustomMediaSourceType;
    CustomMediaSource *mCustomMediaSource;
    
    static int readPacket(void *opaque, uint8_t *buf, int buf_size);
    static int64_t seek(void *opaque, int64_t offset, int whence);
};

#endif /* FFmpegReader_h */
