//
//  CustomMediaSource.h
//  MediaPlayer
//
//  Created by Think on 2017/2/23.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef CustomMediaSource_h
#define CustomMediaSource_h

#include <stdio.h>
#include <stdint.h>

enum CustomMediaSourceType {
    UNKNOWN_CUSTOM_MEDIA_SOURCE = -1,
    LOCAL_MP3_FILE = 0,
    REALTIME_RTP = 1,
};

class CustomMediaSource
{
public:
    virtual ~CustomMediaSource() {}
    
    static CustomMediaSource* CreateCustomMediaSource(CustomMediaSourceType type);
    static void DeleteCustomMediaSource(CustomMediaSource* source, CustomMediaSourceType type);
    
    virtual bool open(char* url) = 0;
    virtual void close() = 0;
    
    virtual int readPacket(uint8_t *buf, int buf_size) = 0;
    virtual int64_t seek(int64_t offset, int whence) = 0;
};

#endif /* CustomMediaSource_h */
