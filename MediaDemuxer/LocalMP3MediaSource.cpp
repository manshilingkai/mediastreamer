//
//  LocalMP3MediaSource.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/24.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "LocalMP3MediaSource.h"
#include "MediaLog.h"

LocalMP3MediaSource::LocalMP3MediaSource()
{
    file = NULL;
}

LocalMP3MediaSource::~LocalMP3MediaSource()
{
    file = NULL;
}

bool LocalMP3MediaSource::open(char* url)
{
    file = fopen(url, "rb");
    
    if (!file) {
        LOGE("could not open file : %s\n", url);
        return false;
    }
    
    return true;
}

void LocalMP3MediaSource::close()
{
    if (file) {
        fclose(file);
        file = NULL;
    }
}

int LocalMP3MediaSource::readPacket(uint8_t *buf, int buf_size)
{
    return fread(buf, 1, buf_size, file);
}

int64_t LocalMP3MediaSource::seek(int64_t offset, int whence)
{
    if(whence == AVSEEK_SIZE) return -1; // Ignore and return -1. This is supported by FFmpeg.
    if(whence & AVSEEK_FORCE) whence &= ~AVSEEK_FORCE; // Can be ignored.
    if(whence != SEEK_SET && whence != SEEK_CUR && whence != SEEK_END) {
        LOGE("seek: invalid whence in params offset:%lli whence:%i\n", offset, whence);
        return -1;
    }
    if(whence == SEEK_SET && offset < 0) {
        // This is a bug in FFmpeg: https://trac.ffmpeg.org/ticket/4038
        LOGE("seek: bug triggered, offset:%lli whence:%i\n", offset, whence);
        abort();
        return -1;
    }
    
    int ret = fseeko(file, offset, whence);
    if(ret < 0) {
        LOGE("seek() error: %s\n", strerror(errno));
        return -1;
    }
    
    return ftello(file);
}
