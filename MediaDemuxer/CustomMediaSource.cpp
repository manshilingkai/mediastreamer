//
//  CustomMediaSource.cpp
//  MediaPlayer
//
//  Created by Think on 2017/2/23.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "CustomMediaSource.h"
#include "LocalMP3MediaSource.h"

CustomMediaSource* CustomMediaSource::CreateCustomMediaSource(CustomMediaSourceType type)
{
    if (type == LOCAL_MP3_FILE) {
        return new LocalMP3MediaSource;
    }
    
    return NULL;
}

void CustomMediaSource::DeleteCustomMediaSource(CustomMediaSource* source, CustomMediaSourceType type)
{
    if (type == LOCAL_MP3_FILE) {
        LocalMP3MediaSource *localMP3MediaSource = (LocalMP3MediaSource *)source;
        if (localMP3MediaSource!=NULL) {
            delete localMP3MediaSource;
            localMP3MediaSource = NULL;
        }
    }
}
