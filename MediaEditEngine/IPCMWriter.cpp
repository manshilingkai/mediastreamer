//
//  IPCMWriter.cpp
//  MediaStreamer
//
//  Created by Think on 2019/9/4.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "IPCMWriter.h"
#include "WAVFile.h"
#include "FFAudioFileWriter.h"

IPCMWriter* IPCMWriter::CreatePCMWriter(PCMWriterType type, int channels, int sample_rate, char* audioFilePath)
{
    if (type == WAV_FILE_WRITER) {
        return new WAVFileWriter(channels, sample_rate, audioFilePath);
    }else if(type == FF_AUDIO_FILE_WRITER) {
        return new FFAudioFileWriter(channels, sample_rate, audioFilePath);
    }
    
    return NULL;
}

void IPCMWriter::DeletePCMWriter(IPCMWriter* iPCMWriter, PCMWriterType type)
{
    if (type == WAV_FILE_WRITER) {
        WAVFileWriter* wavFileWriter = (WAVFileWriter*)iPCMWriter;
        if (wavFileWriter) {
            delete wavFileWriter;
            wavFileWriter = NULL;
        }
    }else if (type == FF_AUDIO_FILE_WRITER) {
        FFAudioFileWriter* ffAudioFileWriter = (FFAudioFileWriter*)iPCMWriter;
        if (ffAudioFileWriter) {
            delete ffAudioFileWriter;
            ffAudioFileWriter = NULL;
        }
    }
}
