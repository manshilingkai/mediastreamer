//
//  YPPGPUOverlay.h
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/30.
//  Copyright © 2020 bolome. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YPPGPUOverlay : NSObject

- (instancetype) init;

- (void)initialize;

- (void)setOverlay:(NSString*)imageUrl;
- (BOOL)blendTo:(int)mainTextureId MainWidth:(int)mainWidth MainHeight:(int)mainHeight X:(int)x Y:(int)y OverlayRotation:(int)rotation OverlayScale:(float)scale OverlayFlipHorizontal:(bool)flipHorizontal OverlayFlipVertical:(bool)flipVertical OutputTextureId:(int)outputTextureId;

- (void)terminate;

@end

NS_ASSUME_NONNULL_END
