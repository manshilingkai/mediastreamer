//
//  AudioRemuxer.cpp
//  MediaStreamer
//
//  Created by Think on 2019/9/4.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "AudioRemuxer.h"
#include "AutoLock.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#else
#include "NormalMediaListener.h"
#endif

#include "MediaLog.h"
#include "MediaFile.h"
#include "FFLog.h"

struct AudioRemuxerEvent : public TimedEventQueue::Event {
    AudioRemuxerEvent(
                                AudioRemuxer *remuxer,
                                void (AudioRemuxer::*method)())
    : mRemuxer(remuxer),
    mMethod(method) {
    }
    
protected:
    virtual ~AudioRemuxerEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mRemuxer->*mMethod)();
    }
    
private:
    AudioRemuxer *mRemuxer;
    void (AudioRemuxer::*mMethod)();
    
    AudioRemuxerEvent(const AudioRemuxerEvent &);
    AudioRemuxerEvent &operator=(const AudioRemuxerEvent &);
};

#ifdef ANDROID
AudioRemuxer::AudioRemuxer(JavaVM *jvm, AudioRemuxerMaterialGroup inputMaterialGroup, AudioRemuxerProduct outputProduct)
{
    mJvm = jvm;

    init_ffmpeg_env();

    mInputMaterialGroup.audioRemuxerMaterialNum = inputMaterialGroup.audioRemuxerMaterialNum;
    for (int i = 0; i < mInputMaterialGroup.audioRemuxerMaterialNum; i++) {
        mInputMaterialGroup.audioRemuxerMaterials[i] = new AudioRemuxerMaterial;
        if (inputMaterialGroup.audioRemuxerMaterials[i]->url) {
            mInputMaterialGroup.audioRemuxerMaterials[i]->url = strdup(inputMaterialGroup.audioRemuxerMaterials[i]->url);
        }
        mInputMaterialGroup.audioRemuxerMaterials[i]->startPosMs = inputMaterialGroup.audioRemuxerMaterials[i]->startPosMs;
        mInputMaterialGroup.audioRemuxerMaterials[i]->endPosMs = inputMaterialGroup.audioRemuxerMaterials[i]->endPosMs;
        mInputMaterialGroup.audioRemuxerMaterials[i]->isRemuxAll = inputMaterialGroup.audioRemuxerMaterials[i]->isRemuxAll;
    }
    
    if (outputProduct.url) {
        mOutputProduct.url = strdup(outputProduct.url);
    }
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);
    
    mAsyncPrepareEvent = NULL;
    mRemuxEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    
    mAsyncPrepareEvent = new AudioRemuxerEvent(this, &AudioRemuxer::onPrepareAsyncEvent);
    mRemuxEvent = new AudioRemuxerEvent(this, &AudioRemuxer::onRemuxEvent);
    mStopEvent = new AudioRemuxerEvent(this, &AudioRemuxer::onStopEvent);
    mNotifyEvent = new AudioRemuxerEvent(this, &AudioRemuxer::onNotifyEvent);
    mRemuxEventPending = false;
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();

    pthread_cond_init(&mStopCondition, NULL);

    mMediaListener = NULL;

    gotError = false;
    
    mWorkInputMaterialIndex = 0;

    ifmt_ctx = NULL;
    mInputAudioStreamIndex = -1;
    
    ofmt_ctx = NULL;
    mOutputAudioStreamIndex = -1;
    
    gotWorkInputMaterialBaseline = false;
    mWorkInputMaterialBaseline = 0;
    mWorkInputMaterialTimeline = 0;
    mWorkOutputProductLastSegmentTimeline = 0;
    gotFirstAudioPacketFromWorkInputMaterial = false;
}
#else
AudioRemuxer::AudioRemuxer(AudioRemuxerMaterialGroup inputMaterialGroup, AudioRemuxerProduct outputProduct)
{
    init_ffmpeg_env();

    mInputMaterialGroup.audioRemuxerMaterialNum = inputMaterialGroup.audioRemuxerMaterialNum;
    for (int i = 0; i < mInputMaterialGroup.audioRemuxerMaterialNum; i++) {
        mInputMaterialGroup.audioRemuxerMaterials[i] = new AudioRemuxerMaterial;
        if (inputMaterialGroup.audioRemuxerMaterials[i]->url) {
            mInputMaterialGroup.audioRemuxerMaterials[i]->url = strdup(inputMaterialGroup.audioRemuxerMaterials[i]->url);
        }
        mInputMaterialGroup.audioRemuxerMaterials[i]->startPosMs = inputMaterialGroup.audioRemuxerMaterials[i]->startPosMs;
        mInputMaterialGroup.audioRemuxerMaterials[i]->endPosMs = inputMaterialGroup.audioRemuxerMaterials[i]->endPosMs;
        mInputMaterialGroup.audioRemuxerMaterials[i]->isRemuxAll = inputMaterialGroup.audioRemuxerMaterials[i]->isRemuxAll;
    }
    
    if (outputProduct.url) {
        mOutputProduct.url = strdup(outputProduct.url);
    }
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);
    
    mAsyncPrepareEvent = NULL;
    mRemuxEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    
    mAsyncPrepareEvent = new AudioRemuxerEvent(this, &AudioRemuxer::onPrepareAsyncEvent);
    mRemuxEvent = new AudioRemuxerEvent(this, &AudioRemuxer::onRemuxEvent);
    mStopEvent = new AudioRemuxerEvent(this, &AudioRemuxer::onStopEvent);
    mNotifyEvent = new AudioRemuxerEvent(this, &AudioRemuxer::onNotifyEvent);
    mRemuxEventPending = false;
    
    mQueue.start();

    pthread_cond_init(&mStopCondition, NULL);

    mMediaListener = NULL;

    gotError = false;
    
    mWorkInputMaterialIndex = 0;

    ifmt_ctx = NULL;
    mInputAudioStreamIndex = -1;
    
    ofmt_ctx = NULL;
    mOutputAudioStreamIndex = -1;
    
    gotWorkInputMaterialBaseline = false;
    mWorkInputMaterialBaseline = 0;
    mWorkInputMaterialTimeline = 0;
    mWorkOutputProductLastSegmentTimeline = 0;
    gotFirstAudioPacketFromWorkInputMaterial = false;
}
#endif

AudioRemuxer::~AudioRemuxer()
{
    stop(false);

    mQueue.stop(true);

    mNotificationQueue.flush();

    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
        
    if (mRemuxEvent!=NULL) {
        delete mRemuxEvent;
        mRemuxEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
        
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    pthread_cond_destroy(&mStopCondition);
    
    pthread_mutex_destroy(&mLock);
        
#ifdef ANDROID
    JniMediaListener *jniListener = (JniMediaListener*)mMediaListener;
    if (jniListener!=NULL) {
        delete jniListener;
        jniListener = NULL;
    }
#else
    NormalMediaListener* normalMediaListener = (NormalMediaListener*)mMediaListener;
    if (normalMediaListener!=NULL) {
        delete normalMediaListener;
        normalMediaListener = NULL;
    }
#endif
        
    mMediaListener = NULL;
    
    mInputMaterialGroup.Free();
    mOutputProduct.Free();
}

#ifdef ANDROID
void AudioRemuxer::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    modifyFlags(INITIALIZED, SET);
}
#else
void AudioRemuxer::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new NormalMediaListener(listener,arg);
    
    modifyFlags(INITIALIZED, SET);
}
#endif

void AudioRemuxer::notifyListener_l(int event, int ext1, int ext2)
{
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void AudioRemuxer::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    postNotifyEvent_l();
}

void AudioRemuxer::postNotifyEvent_l()
{
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void AudioRemuxer::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
}

void AudioRemuxer::start()
{
    AutoLock autoLock(&mLock);
    if (mFlags & CONNECTING) {
        LOGW("already connecting");
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    prepareAsync_l();
}

void AudioRemuxer::prepareAsync_l()
{
    modifyFlags(END, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(CONNECTING, SET);
    
    gotError = false;
    
    mQueue.postEvent(mAsyncPrepareEvent);
}

void AudioRemuxer::onPrepareAsyncEvent()
{
    AutoLock autoLock(&mLock);
    int ret = open_all_pipelines_l();
    
    if (ret>=0) {
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(CONNECTED, SET);
        
        play_l();
    }else{
        if (ret!=AVERROR_EXIT) {
            if (ret==-1) {
                notifyListener_l(AUDIO_REMUXER_ERROR, AUDIO_REMUXER_ERROR_NO_INPUT_MATERIAL);
            }else if (ret==-2) {
                notifyListener_l(AUDIO_REMUXER_ERROR, AUDIO_REMUXER_ERROR_OPEN_INPUT_MATERIAL_FAIL);
            }else if (ret==-3) {
                notifyListener_l(AUDIO_REMUXER_ERROR, AUDIO_REMUXER_ERROR_OPEN_OUTPUT_PRODUCT_FAIL);
            }else {
                notifyListener_l(AUDIO_REMUXER_ERROR, AUDIO_REMUXER_ERROR_UNKNOWN);
            }
        }
        
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(ERROR, SET);
        gotError = true;
        stop_l();
    }
}

void AudioRemuxer::pause()
{
    AutoLock autoLock(&mLock);
    return pause_l();
}

void AudioRemuxer::pause_l()
{
    if (!(mFlags & STREAMING)) {
        return;
    }
    
    modifyFlags(STREAMING, CLEAR);
    
    mQueue.cancelEvent(mRemuxEvent->eventID());
    mRemuxEventPending = false;
    
    modifyFlags(PAUSED, SET);
    
    return;
}

void AudioRemuxer::resume()
{
    AutoLock autoLock(&mLock);
    
    return play_l();
}

void AudioRemuxer::play_l()
{
    if (mFlags & STREAMING) {
        LOGW("%s","AudioRemuxer is streaming");
        return;
    }
    
    if (!(mFlags & CONNECTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    modifyFlags(CONNECTED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    postRemuxEvent_l();
    
    modifyFlags(STREAMING, SET);
    
    notifyListener_l(AUDIO_REMUXER_STREAMING);
}

void AudioRemuxer::cancelRemuxerEvents()
{
    mQueue.cancelEvent(mRemuxEvent->eventID());
    mRemuxEventPending = false;
    mQueue.cancelEvent(mNotifyEvent->eventID());
    mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
    mQueue.cancelEvent(mStopEvent->eventID());
}

void AudioRemuxer::stop(bool isCancel)
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & END) {
        return;
    }
    
    if (mFlags & ENDING) {
        LOGW("already ending");
        
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    if (isCancel) {
        gotError = true;
    }
    
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
    
    if (isCancel) {
        if (MediaFile::isExist(mOutputProduct.url)) {
            MediaFile::deleteFile(mOutputProduct.url);
        }
    }
}

void AudioRemuxer::stop_l()
{
    modifyFlags(ENDING, ASSIGN);
    
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void AudioRemuxer::postRemuxEvent_l(int64_t delayUs)
{
    if (mRemuxEventPending) {
        return;
    }
    mRemuxEventPending = true;
    mQueue.postEventWithDelay(mRemuxEvent, delayUs < 0 ? 0 : delayUs);
}

void AudioRemuxer::onStopEvent()
{
    AutoLock autoLock(&mLock);
    
    close_all_pipelines_l();
    
    cancelRemuxerEvents();
    mNotificationQueue.flush();
    
    modifyFlags(END, ASSIGN);
    
    if (!gotError) {
        notifyListener_l(AUDIO_REMUXER_END);
    }else{
        if (MediaFile::isExist(mOutputProduct.url)) {
            MediaFile::deleteFile(mOutputProduct.url);
        }
    }
    gotError = false;
    
    mWorkInputMaterialIndex = 0;

    ifmt_ctx = NULL;
    mInputAudioStreamIndex = -1;
    
    ofmt_ctx = NULL;
    mOutputAudioStreamIndex = -1;
    
    gotWorkInputMaterialBaseline = false;
    mWorkInputMaterialBaseline = 0;
    mWorkInputMaterialTimeline = 0;
    mWorkOutputProductLastSegmentTimeline = 0;
    gotFirstAudioPacketFromWorkInputMaterial = false;

    pthread_cond_broadcast(&mStopCondition);
}

void AudioRemuxer::onRemuxEvent()
{
    AutoLock autoLock(&mLock);
    
    if (!mRemuxEventPending) {
        // The event has been cancelled in reset_l() but had already // been scheduled for execution at that time.
        return;
    }
    mRemuxEventPending = false;
    
    int ret = flowing_l();
    if (ret==1) {
        postRemuxEvent_l(0);
        return;
    }else if (ret==-7) {
        //Write file trailer
        av_write_trailer(ofmt_ctx);
        
        stop_l();
        return;
    }else if (ret<0) {
        modifyFlags(STREAMING, CLEAR);
        modifyFlags(ERROR, SET);
        
        if (ret!=AVERROR_EXIT) {
            if (ret==-2) {
                notifyListener_l(AUDIO_REMUXER_ERROR, AUDIO_REMUXER_ERROR_OPEN_INPUT_MATERIAL_FAIL);
            }else if (ret==-4) {
                notifyListener_l(AUDIO_REMUXER_ERROR, AUDIO_REMUXER_ERROR_DEMUX_FAIL);
            }else if (ret==-5) {
                notifyListener_l(AUDIO_REMUXER_ERROR, AUDIO_REMUXER_ERROR_MUX_FAIL);
            }else {
                notifyListener_l(AUDIO_REMUXER_ERROR, AUDIO_REMUXER_ERROR_UNKNOWN);
            }
        }
        
        gotError = true;
        stop_l();
        
        return;
    }else {
        int64_t waitTimeUs = 10*1000;
        postRemuxEvent_l(waitTimeUs);
        return;
    }
}

void AudioRemuxer::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

void AudioRemuxer::init_ffmpeg_env()
{
    // init ffmpeg env
    av_register_all();
    avcodec_register_all();
    avfilter_register_all();
    
    FFLog::setLogLevel(AV_LOG_WARNING);
}

// -1 : No Input Material
// -2 : Open Input Material Fail
// -3 : Open Output Material Fail
int AudioRemuxer::open_all_pipelines_l()
{
    if (mInputMaterialGroup.audioRemuxerMaterialNum<=0) {
        return -1;
    }
    
    close_input_material();
    mWorkInputMaterialIndex = 0;
    
    int ret = open_input_material(mWorkInputMaterialIndex);
    if (ret<0) {
        close_input_material();
        
        if (ret==AVERROR_EXIT) {
            return AVERROR_EXIT;
        }
        return -2;
    }
    
    ret = open_output_product();
    if (ret<0) {
        close_output_product();
        
        if (ret==AVERROR_EXIT) {
            return AVERROR_EXIT;
        }
        return -3;
    }

    return 0;
}

void AudioRemuxer::close_all_pipelines_l()
{
    close_output_product();
    close_input_material();
}

int AudioRemuxer::open_input_material(int workInputMaterialIndex)
{
    int ret = 0;
    if ((ret = avformat_open_input(&ifmt_ctx, mInputMaterialGroup.audioRemuxerMaterials[workInputMaterialIndex]->url, 0, 0)) < 0) {
        ifmt_ctx = NULL;
        
        LOGE("Could not open input material file '%s'", mInputMaterialGroup.audioRemuxerMaterials[workInputMaterialIndex]->url);
        return ret;
    }
    
    if ((ret = avformat_find_stream_info(ifmt_ctx, 0)) < 0) {
        avformat_close_input(&ifmt_ctx);
        ifmt_ctx = NULL;
        
        LOGE("Failed to retrieve stream information for input material file");
        return ret;
    }
    
    mInputAudioStreamIndex = -1;
    
    for(int i= 0; i < ifmt_ctx->nb_streams; i++)
    {
        if (ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first video stream, and discard others.
            if(mInputAudioStreamIndex == -1)
            {
                mInputAudioStreamIndex = i;
            }
            else
            {
                ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
            }
        }else{
            ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
        }
    }
    
    if (mInputAudioStreamIndex==-1) {
        avformat_close_input(&ifmt_ctx);
        ifmt_ctx = NULL;
        
        mInputAudioStreamIndex = -1;
        
        LOGE("No Audio Stream");
        return AVERROR_STREAM_NOT_FOUND;
    }
    
    if (!mInputMaterialGroup.audioRemuxerMaterials[workInputMaterialIndex]->isRemuxAll) {
        
        int result = 0;
        while (true) {
            AVPacket packet;
            av_init_packet(&packet);
            packet.data = NULL;
            packet.size = 0;
            packet.flags = 0;
            int ret = av_read_frame(ifmt_ctx, &packet);
            if (ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN)) {
                av_packet_unref(&packet);
                continue;
            }else if (ret == AVERROR_EOF) {
                av_packet_unref(&packet);
                result = ret;
                break;
            }else if (ret<0) {
                av_packet_unref(&packet);
                result = ret;
                break;
            }else{
                if (packet.stream_index==mInputAudioStreamIndex) {
                    gotFirstAudioPacketFromWorkInputMaterial = true;
//                    ifmt_ctx->start_time = packet.pts;
                    if (ifmt_ctx->streams[mInputAudioStreamIndex]->start_time==AV_NOPTS_VALUE) {
                        ifmt_ctx->streams[mInputAudioStreamIndex]->start_time = packet.pts;
                    }
                    if (ifmt_ctx->start_time==AV_NOPTS_VALUE) {
                        ifmt_ctx->start_time = packet.pts;
                    }
                    av_packet_unref(&packet);
                    result = ret;
                    break;
                }else{
                    av_packet_unref(&packet);
                    continue;
                }
            }
        }
        
        if (!gotFirstAudioPacketFromWorkInputMaterial) {
            if (result==AVERROR_EXIT) {
                LOGW("Immediate exit was requested");
            }else{
                if (result==AVERROR_EOF) {
                    LOGE("got first audio packet is eof packet, exit...");
                }else{
                    LOGE("got error packet, exit...");
                }
            }
            
            avformat_close_input(&ifmt_ctx);
            ifmt_ctx = NULL;
            
            mInputAudioStreamIndex = -1;
            
            return result;
        }
        
        int seek_ret = avformat_seek_file(ifmt_ctx, -1, INT64_MIN, av_rescale(mInputMaterialGroup.audioRemuxerMaterials[workInputMaterialIndex]->startPosMs, AV_TIME_BASE, 1000) + ifmt_ctx->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
        if (seek_ret<0) {
            LOGE("Seek To %dMs Fail", mInputMaterialGroup.audioRemuxerMaterials[workInputMaterialIndex]->startPosMs);
            
            avformat_close_input(&ifmt_ctx);
            ifmt_ctx = NULL;
            
            mInputAudioStreamIndex = -1;
            gotFirstAudioPacketFromWorkInputMaterial = false;
            
            return seek_ret;
        }
    }
    
    return 0;
}

void AudioRemuxer::close_input_material()
{
    if (ifmt_ctx) {
        avformat_close_input(&ifmt_ctx);
        ifmt_ctx = NULL;
    }
    
    mInputAudioStreamIndex = -1;
    
    gotWorkInputMaterialBaseline = false;
    gotFirstAudioPacketFromWorkInputMaterial = false;
}

int AudioRemuxer::open_output_product()
{
    int ret = -1;
    if (strstr(mOutputProduct.url, ".m4a") || strstr(mOutputProduct.url, ".M4A")) {
        ret = avformat_alloc_output_context2(&ofmt_ctx, NULL, "mp4", mOutputProduct.url);
    }else{
        ret = avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, mOutputProduct.url);
    }
    if (ret<0) {
        ofmt_ctx = NULL;
        
        LOGE("Could not create output context");
        return ret;
    }
    
    ofmt_ctx->oformat->flags |= AVFMT_TS_NONSTRICT;

    AVStream *in_audio_stream = ifmt_ctx->streams[mInputAudioStreamIndex];
    AVCodecParameters *in_audio_codecpar = in_audio_stream->codecpar;
    AVStream *out_audio_stream = avformat_new_stream(ofmt_ctx, NULL);
    
    mOutputAudioStreamIndex = 0;
    
    ret = avcodec_parameters_copy(out_audio_stream->codecpar, in_audio_codecpar);
    if (ret < 0) {
        if (ofmt_ctx) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
        }
        
        LOGE("Failed to copy audio codec parameters");
        return ret;
    }
    
    out_audio_stream->codecpar->codec_tag = 0;
    
    av_dump_format(ofmt_ctx, 0, mOutputProduct.url, 1);

    if (!(ofmt_ctx->oformat->flags & AVFMT_NOFILE)) {
        ret = avio_open(&ofmt_ctx->pb, mOutputProduct.url, AVIO_FLAG_WRITE);
        if (ret < 0) {
            if (ofmt_ctx) {
                avformat_free_context(ofmt_ctx);
                ofmt_ctx = NULL;
            }
            
            LOGE("Could not open output audio file '%s'", mOutputProduct.url);
            return ret;
        }
    }
    
    ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        if (ofmt_ctx && ofmt_ctx->oformat && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
        {
            avio_close(ofmt_ctx->pb);
        }
        
        if (ofmt_ctx) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
        }
        
        LOGE("Error occurred when opening output file");
        return ret;
    }
    
    return 0;
}

void AudioRemuxer::close_output_product()
{
    if (ofmt_ctx && ofmt_ctx->oformat && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
    {
        avio_close(ofmt_ctx->pb);
    }
    
    if (ofmt_ctx) {
        avformat_free_context(ofmt_ctx);
        ofmt_ctx = NULL;
    }
}

//  0 : Wait Then Loop
//  1 : Just Loop

// -2 : Open Input Material Fail
// -4 : AUDIO DEMUX FAIL
// -5 : AUDIO MUX FAIL
// -7 : EOF
int AudioRemuxer::flowing_l()
{
    int ret = 0;
    AVPacket pkt;
    ret = av_read_frame(ifmt_ctx, &pkt);
    if (ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN)) {
        av_packet_unref(&pkt);
        return 0;
    }else if (ret == AVERROR_EOF) {
        av_packet_unref(&pkt);

        mWorkOutputProductLastSegmentTimeline += mWorkInputMaterialTimeline;
        
        if (mWorkInputMaterialIndex==mInputMaterialGroup.audioRemuxerMaterialNum-1) {
            return -7;
        }
        
        //switch to next
        close_input_material();
        mWorkInputMaterialIndex++;
        int result = open_input_material(mWorkInputMaterialIndex);
        if (result<0) {
            close_input_material();
            
            if (result==AVERROR_EXIT) {
                return AVERROR_EXIT;
            }
            
            return -2;
        }
        return 1;
    }else if (ret < 0) {
        av_packet_unref(&pkt);
        
        if (ret==AVERROR_EXIT) {
            return AVERROR_EXIT;
        }
        
        return -4;
    }else {
        if (pkt.stream_index != mInputAudioStreamIndex) {
            av_packet_unref(&pkt);
            return 1;
        }else{
            AVPacket* workAudioPacket = &pkt;
            AVStream *in_audio_stream = ifmt_ctx->streams[mInputAudioStreamIndex];
            AVStream *out_audio_stream = ofmt_ctx->streams[mOutputAudioStreamIndex];
            
            if (!gotFirstAudioPacketFromWorkInputMaterial) {
                gotFirstAudioPacketFromWorkInputMaterial = true;
                if (in_audio_stream->start_time==AV_NOPTS_VALUE) {
                    in_audio_stream->start_time = workAudioPacket->pts;
                }
                if (ifmt_ctx->start_time==AV_NOPTS_VALUE) {
                    ifmt_ctx->start_time = workAudioPacket->pts;
                }
            }
            
            int64_t real_audio_pts = av_rescale_q(workAudioPacket->pts - in_audio_stream->start_time, in_audio_stream->time_base, AV_TIME_BASE_Q);
            if (!mInputMaterialGroup.audioRemuxerMaterials[mWorkInputMaterialIndex]->isRemuxAll) {
                if (real_audio_pts < mInputMaterialGroup.audioRemuxerMaterials[mWorkInputMaterialIndex]->startPosMs * 1000) {
                    av_packet_unref(&pkt);
                    return 1;
                }
                if (real_audio_pts > mInputMaterialGroup.audioRemuxerMaterials[mWorkInputMaterialIndex]->endPosMs * 1000) {
                    av_packet_unref(&pkt);
                    
                    mWorkOutputProductLastSegmentTimeline += mWorkInputMaterialTimeline;
                    
                    if (mWorkInputMaterialIndex==mInputMaterialGroup.audioRemuxerMaterialNum-1) {
                        return -7;
                    }
                    
                    //switch to next
                    close_input_material();
                    mWorkInputMaterialIndex++;
                    int result = open_input_material(mWorkInputMaterialIndex);
                    if (result<0) {
                        close_input_material();
                        
                        if (result==AVERROR_EXIT) {
                            return AVERROR_EXIT;
                        }
                        
                        return -2;
                    }
                    return 1;
                }
            }
            
            if (!gotWorkInputMaterialBaseline) {
                gotWorkInputMaterialBaseline = true;
                mWorkInputMaterialBaseline = real_audio_pts;
            }
            
            mWorkInputMaterialTimeline = real_audio_pts - mWorkInputMaterialBaseline;
            
            workAudioPacket->stream_index = mOutputAudioStreamIndex;
            workAudioPacket->pts = av_rescale_q_rnd(mWorkOutputProductLastSegmentTimeline+mWorkInputMaterialTimeline, AV_TIME_BASE_Q, out_audio_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
            workAudioPacket->dts = av_rescale_q_rnd(mWorkOutputProductLastSegmentTimeline+mWorkInputMaterialTimeline, AV_TIME_BASE_Q, out_audio_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
            workAudioPacket->duration = av_rescale_q_rnd(workAudioPacket->duration, in_audio_stream->time_base, out_audio_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
            if (workAudioPacket->duration<0) {
                workAudioPacket->duration = 0;
            }
            workAudioPacket->pos = -1;

            ret = av_interleaved_write_frame(ofmt_ctx, workAudioPacket);
            av_packet_unref(workAudioPacket);
            
            if (ret < 0) {
                LOGE("Got Error When Muxing Audio Packet");
                
                if (ret==AVERROR_EXIT) {
                    return AVERROR_EXIT;
                }
                
                return -5;
            }
            
            return 1;
        }
    }
    
    return 0;
}
