//
//  RadioProducer.h
//  MediaStreamer
//
//  Created by Think on 2019/8/22.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef RadioProducer_h
#define RadioProducer_h

#include <stdio.h>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
}

#include "TimedEventQueue.h"
#include "IMediaListener.h"

#include "AudioPlayer.h"
#include "AudioCapture.h"
#include "AudioProcess.h"
#include "AudioRender.h"
#include "WAVFile.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include "RadioProducerCommon.h"

enum RADIO_COMMAND_TYPE {
    UNKNOWN_RADIO_COMMAND_TYPE = -1,
    
    PREPARE_BGM = 0,
    PREPARE_BGM_WITH_START_POS = 1,
    START_BGM = 2,
    PAUSE_BGM = 3,
    STOP_BGM = 4,
    SET_BGM_VOLUME = 5,
    
    START_MICROPHONE = 6,
    STOP_MICROPHONE = 7,
    SET_MICROPHONE_VOLUME = 8,
    
    PREPARE_EFFECT_MUSIC = 9,
    START_EFFECT_MUSIC = 10,
    PAUSE_EFFECT_MUSIC = 11,
    STOP_EFFECT_MUSIC = 12,
    SET_EFFECT_MUSIC_VOLUME = 13,
    
    SET_USER_DEFINED_EFFECT = 14,
    SET_EQUALIZER_STYLE = 15,
    SET_REVERB_STYLE = 16,
    SET_PITCH_SEMITONES = 17,
    
    SET_EAR_RETURN = 18,
    
    PAUSE = 19,
    STOP = 20,
};

struct PrepareBgmCommand {
    char* bgmUrl;
    
    PrepareBgmCommand()
    {
        bgmUrl = NULL;
    }
    
    inline void Free()
    {
        if(bgmUrl)
        {
            free(bgmUrl);
            bgmUrl = NULL;
        }
    }
};

struct PrepareBgmWithStartPosCommand {
    char* bgmUrl;
    int32_t startPosMs;
    
    PrepareBgmWithStartPosCommand()
    {
        bgmUrl = NULL;
        startPosMs = 0;
    }
    
    inline void Free()
    {
        if(bgmUrl)
        {
            free(bgmUrl);
            bgmUrl = NULL;
        }
        
        startPosMs = 0;
    }
};

struct SetBgmVolumeCommand {
    float volume;
    
    SetBgmVolumeCommand()
    {
        volume = 1.0f;
    }
};

struct SetMicrophoneVolumeCommand {
    float volume;
    
    SetMicrophoneVolumeCommand()
    {
        volume = 1.0f;
    }
};

struct PrepareEffectMusicCommand {
    char* effectMusicUrl;
    
    PrepareEffectMusicCommand()
    {
        effectMusicUrl = NULL;
    }
    
    inline void Free()
    {
        if(effectMusicUrl)
        {
            free(effectMusicUrl);
            effectMusicUrl = NULL;
        }
    }
};

struct SetEffectMusicVolumeCommand {
    float volume;

    SetEffectMusicVolumeCommand()
    {
        volume = 1.0f;
    }
};

struct SetUserDefinedEffectCommand {
    int effect;
    
    SetUserDefinedEffectCommand()
    {
        effect = NOEFFECT;
    }
};

struct SetEqualizerStyleCommand {
    int style;
    
    SetEqualizerStyleCommand()
    {
        style = NOEQUALIZER;
    }
};

struct SetReverbStyleCommand {
    int style;
    
    SetReverbStyleCommand()
    {
        style = NOEQUALIZER;
    }
};

struct SetPitchSemiTonesCommand {
    int value;
    
    SetPitchSemiTonesCommand()
    {
        value = 0;
    }
};

struct SetEarReturnCommand {
    bool isEnableEarReturn;
    
    SetEarReturnCommand()
    {
        isEnableEarReturn = false;
    }
};

struct PauseCommand {
    bool isExport;
    
    PauseCommand()
    {
        isExport = false;
    }
};

struct RadioCommand{
    RADIO_COMMAND_TYPE type;
    
    void *command;
    
    RadioCommand()
    {
        type = UNKNOWN_RADIO_COMMAND_TYPE;
        command = NULL;
    }
    
    inline void Free()
    {
        if(command)
        {
            if(type==PREPARE_BGM)
            {
                PrepareBgmCommand* prepareBgmCommand = (PrepareBgmCommand*)command;
                if(prepareBgmCommand)
                {
                    prepareBgmCommand->Free();
                    delete prepareBgmCommand;
                    prepareBgmCommand = NULL;
                }
            }else if(type==PREPARE_BGM_WITH_START_POS)
            {
                PrepareBgmWithStartPosCommand* prepareBgmWithStartPosCommand = (PrepareBgmWithStartPosCommand*)command;
                if(prepareBgmWithStartPosCommand)
                {
                    prepareBgmWithStartPosCommand->Free();
                    delete prepareBgmWithStartPosCommand;
                    prepareBgmWithStartPosCommand = NULL;
                }
            }else if(type==SET_BGM_VOLUME)
            {
                SetBgmVolumeCommand* setBgmVolumeCommand = (SetBgmVolumeCommand*)command;
                if(setBgmVolumeCommand)
                {
                    delete setBgmVolumeCommand;
                    setBgmVolumeCommand = NULL;
                }
            }else if(type==SET_MICROPHONE_VOLUME)
            {
                SetMicrophoneVolumeCommand* setMicrophoneVolumeCommand = (SetMicrophoneVolumeCommand*)command;
                if(setMicrophoneVolumeCommand)
                {
                    delete setMicrophoneVolumeCommand;
                    setMicrophoneVolumeCommand = NULL;
                }
            }
            else if(type==PREPARE_EFFECT_MUSIC)
            {
                PrepareEffectMusicCommand* prepareEffectMusicCommand = (PrepareEffectMusicCommand*)command;
                if(prepareEffectMusicCommand)
                {
                    prepareEffectMusicCommand->Free();
                    delete prepareEffectMusicCommand;
                    prepareEffectMusicCommand = NULL;
                }
            }else if(type==SET_EFFECT_MUSIC_VOLUME)
            {
                SetEffectMusicVolumeCommand* setEffectMusicVolumeCommand = (SetEffectMusicVolumeCommand*)command;
                if(setEffectMusicVolumeCommand)
                {
                    delete setEffectMusicVolumeCommand;
                    setEffectMusicVolumeCommand = NULL;
                }
            }
            else if(type==SET_USER_DEFINED_EFFECT)
            {
                SetUserDefinedEffectCommand* setUserDefinedEffectCommand = (SetUserDefinedEffectCommand*)command;
                if(setUserDefinedEffectCommand)
                {
                    delete setUserDefinedEffectCommand;
                    setUserDefinedEffectCommand = NULL;
                }
            }else if(type==SET_EQUALIZER_STYLE)
            {
                SetEqualizerStyleCommand* setEqualizerStyleCommand = (SetEqualizerStyleCommand*)command;
                if(setEqualizerStyleCommand)
                {
                    delete setEqualizerStyleCommand;
                    setEqualizerStyleCommand = NULL;
                }
            }else if(type==SET_REVERB_STYLE)
            {
                SetReverbStyleCommand* setReverbStyleCommand = (SetReverbStyleCommand*)command;
                if(setReverbStyleCommand)
                {
                    delete setReverbStyleCommand;
                    setReverbStyleCommand = NULL;
                }
            }else if(type==SET_PITCH_SEMITONES)
            {
                SetPitchSemiTonesCommand* setPitchSemiTonesCommand = (SetPitchSemiTonesCommand*)command;
                if(setPitchSemiTonesCommand)
                {
                    delete setPitchSemiTonesCommand;
                    setPitchSemiTonesCommand = NULL;
                }
            }else if (type==SET_EAR_RETURN)
            {
                SetEarReturnCommand* setEarReturnCommand = (SetEarReturnCommand*)command;
                if(setEarReturnCommand)
                {
                    delete setEarReturnCommand;
                    setEarReturnCommand = NULL;
                }
            }else if(type==PAUSE)
            {
                PauseCommand* pauseCommand = (PauseCommand*)command;
                if(pauseCommand)
                {
                    delete pauseCommand;
                    pauseCommand = NULL;
                }
            }

            command = NULL;
        }
    }
};

class RadioCommandQueue {
public:
    RadioCommandQueue();
    ~RadioCommandQueue();
    
    void push(RadioCommand* radioCommand);
    
    RadioCommand* pop();
    
    void flush();
private:
    pthread_mutex_t mLock;
    queue<RadioCommand*> mRadioCommandQueue;
};

class RadioProducer {
public:
#ifdef ANDROID
    RadioProducer(JavaVM *jvm, AudioRenderConfigure configure, RadioProductOptions options);
#else
    RadioProducer(AudioRenderConfigure configure, RadioProductOptions options);
#endif
    ~RadioProducer();
    
#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#else
    void setListener(void (*listener)(void*,int,int,int), void* arg);
#endif
    
    void prepareBgm(char* bgmUrl);
    void prepareBgmWithStartPos(char* bgmUrl, int32_t startPosMs);
    void startBgm();
    void pauseBgm();
    void stopBgm();
    void setBgmVolume(float volume);
    
    void startMicrophone();
    void stopMicrophone();
    void setMicrophoneVolume(float volume);
    
    void prepareEffectMusic(char* effectMusicUrl);
    void startEffectMusic();
    void pauseEffectMusic();
    void stopEffectMusic();
    void setEffectMusicVolume(float volume);
    
    void setAudioUserDefinedEffect(int effect);
    void setAudioEqualizerStyle(int style);
    void setAudioReverbStyle(int style);
    void setAudioPitchSemiTones(int value);
    
    void setEarReturn(bool isEnableEarReturn);

    void pause(bool isExport = false);
    void stop();

    int32_t getCurrentTimeMs();
    int32_t getBgmPlayingTimeMs();
    
private:
    enum {
        INITIALIZED          = 0x01,
        ENDING               = 0x02,
        END                  = 0x04,
    };
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
private:
    friend struct RadioProducerEvent;
    
    RadioProducer(const RadioProducer &);
    RadioProducer &operator=(const RadioProducer &);
    
    pthread_mutex_t mLock;
    
    TimedEventQueue mQueue;
    
    TimedEventQueue::Event *mCommandEvent;
    TimedEventQueue::Event *mProcessEvent;
    TimedEventQueue::Event *mStopEvent;
    bool mProcessEventPending;

    void postProcessEvent_l(int64_t delayUs = -1);
    
    void onCommandEvent();
    void onProcessEvent();
    void onStopEvent();
    
    void cancelProducerEvents();
    
    pthread_cond_t mStopCondition;
    
    RadioCommandQueue mRadioCommandQueue;
private:
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    AudioRenderConfigure mAudioRenderConfigure;
    RadioProductOptions mRadioProductOptions;
    
    IMediaListener* mMediaListener;
private:
    bool gotError;
private:
    static void init_ffmpeg_env();
private:
    void prepareBgm_l(char* bgmUrl);
    void prepareBgmWithStartPos_l(char* bgmUrl, int32_t startPosMs);
    void startBgm_l();
    void pauseBgm_l();
    void stopBgm_l();
    void setBgmVolume_l(float volume);
    bool isBgmPlaying();
    AudioPlayer* mBgmOutputer;
    float mBgmVolume;
private:
    bool startMicrophone_l();
    void stopMicrophone_l();
    bool isMicrophoneRecording();
    AUDIO_CAPTURE_TYPE mAudioCaptureType;
    AudioCapture* mAudioCapture;
    float mMicVolume;
private:
    void prepareEffectMusic_l(char* effectMusicUrl);
    void startEffectMusic_l();
    void pauseEffectMusic_l();
    void stopEffectMusic_l();
    void setEffectMusicVolume_l(float volume);
    bool isEffectMusicPlaying();
    AudioPlayer* mEffectMusicOutputer;
    float mEffectMusicVolume;
private:
    void setAudioUserDefinedEffect_l(int effect);
    void setAudioEqualizerStyle_l(int style);
    void setAudioReverbStyle_l(int style);
    void setAudioPitchSemiTones_l(int value);
    AudioProcess* mAudioProcess;
    bool isEnableEffect;
    UserDefinedEffect mUserDefinedEffect;
    EqualizerStyle mEqualizerStyle;
    ReverbStyle mReverbStyle;
    bool isEnablePitchSemiTones;
    int mPitchSemiTonesValue;
private:
    char* mMixOutputBuffer;
    char* mMixBgOutputBuffer;
    bool isReadyForMixOutput;
    void mix(char* micBuffer, char* bgmBuffer, char* effectMusicBuffer, char** mixOutBuffer, char** mixBgOutBuffer);
private:
    void setEarReturn_l(bool isEnableEarReturn);
    bool mIsEnableEarReturn;
private:
    void process_l();
    void pause_l(bool isExport = false);
    void stop_l();
    AudioRenderType mAudioRenderType;
    AudioRender* mAudioRender;
    WAVFileWriter* mProductWriter;
    WAVFileWriter* mBgProductWriter;
    bool isWorking;
    int32_t mCurrentTimeMs;
    int32_t mBgmPlayingTimeMs;
    pthread_mutex_t mPropertyLock;
    
    int mOutputWaveValue;
    int64_t mLastOutputWaveTimeMs;
private:
    void release();
};

#endif /* RadioProducer_h */
