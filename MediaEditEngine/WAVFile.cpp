//
//  WAVFile.cpp
//  MediaStreamer
//
//  Created by Think on 2019/7/11.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "WAVFile.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "MediaLog.h"
#include "AudioMixer.h"
#include "MediaFile.h"

static char* dummy_get_raw_pcm (char *p, int *bytes_read)
{
    long lSize;
    char *pcm_buf;
    size_t result;
    FILE *fp_pcm;
    
    fp_pcm = fopen (p, "rb");
    if (fp_pcm == NULL) {
        printf ("File error");
        return NULL;
    }
    
    // obtain file size:
    fseek (fp_pcm , 0 , SEEK_END);
    lSize = ftell (fp_pcm);
    rewind (fp_pcm);
    
    // allocate memory to contain the whole file:
    pcm_buf = (char*) malloc (sizeof(char) * lSize);
    if (pcm_buf == NULL) {
        printf ("Memory error");
        return NULL;
    }
    
    // copy the file into the pcm_buf:
    result = fread (pcm_buf, 1, lSize, fp_pcm);
    if (result != lSize) {
        printf ("Reading error");
        return NULL;
    }
    
    *bytes_read = (int) lSize;
    return pcm_buf;
}

static void get_wav_header(int raw_sz, int channels, int sample_rate, wav_header_t *wh)
{
    // RIFF chunk
    strncpy((char*)(wh->chunk_id), "RIFF", strlen("RIFF"));
//    strcpy((char*)(wh->chunk_id), "RIFF");
    wh->chunk_size = 36 + raw_sz;
    
    // fmt sub-chunk (to be optimized)
    strncpy((char*)(wh->sub_chunk1_id), "WAVEfmt ", strlen("WAVEfmt "));
    wh->sub_chunk1_size = 16;
    wh->audio_format = 1;
    wh->num_channels = channels;
    wh->sample_rate = sample_rate;
    wh->bits_per_sample = 16;
    wh->block_align = wh->num_channels * wh->bits_per_sample / 8;
    wh->byte_rate = wh->sample_rate * wh->num_channels * wh->bits_per_sample / 8;
    
    // data sub-chunk
    strncpy((char*)(wh->sub_chunk2_id), "data", strlen("data"));
    wh->sub_chunk2_size = raw_sz;
}

static void dump_wav_header (wav_header_t *wh)
{
    printf ("=========================================\n");
    printf ("chunk_id:\t\t\t%s\n", wh->chunk_id);
    printf ("chunk_size:\t\t\t%d\n", wh->chunk_size);
    printf ("sub_chunk1_id:\t\t\t%s\n", wh->sub_chunk1_id);
    printf ("sub_chunk1_size:\t\t%d\n", wh->sub_chunk1_size);
    printf ("audio_format:\t\t\t%d\n", wh->audio_format);
    printf ("num_channels:\t\t\t%d\n", wh->num_channels);
    printf ("sample_rate:\t\t\t%d\n", wh->sample_rate);
    printf ("bits_per_sample:\t\t%d\n", wh->bits_per_sample);
    printf ("block_align:\t\t\t%d\n", wh->block_align);
    printf ("byte_rate:\t\t\t%d\n", wh->byte_rate);
    printf ("sub_chunk2_id:\t\t\t%s\n", wh->sub_chunk2_id);
    printf ("sub_chunk2_size:\t\t%d\n", wh->sub_chunk2_size);
    printf ("=========================================\n");
}

bool WAVFile::PCMToWAV(char *pcmFilePath, int channels, int sample_rate, char *wavFilePath)
{
    int raw_sz = 0;
    FILE *fwav;
    wav_header_t wheader;
    
    memset (&wheader, '\0', sizeof (wav_header_t));
    
    char *pcm_buf = dummy_get_raw_pcm (pcmFilePath, &raw_sz);
    if (pcm_buf==NULL) {
        return false;
    }
    
    // construct wav header
    get_wav_header (raw_sz, channels, sample_rate, &wheader);
//    dump_wav_header (&wheader);
    
    // write out the .wav file
    fwav = fopen(wavFilePath, "wb");
    if (fwav==NULL) {
        if (pcm_buf) free (pcm_buf);
        return false;
    }
    
    fwrite(&wheader, 1, sizeof(wheader), fwav);
    fwrite(pcm_buf, 1, raw_sz, fwav);
    fclose(fwav);
    
    if (pcm_buf) free (pcm_buf);
    
    return true;
}

bool WAVFile::PCMToWAVWithOffset(char *pcmFilePath, int channels, int sample_rate, char *wavFilePath, long offset)
{
    int raw_sz = 0;
    FILE *fwav;
    wav_header_t wheader;
    
    memset (&wheader, '\0', sizeof (wav_header_t));
    
    char *pcm_buf = dummy_get_raw_pcm (pcmFilePath, &raw_sz);
    if (pcm_buf==NULL) {
        return false;
    }
    
    raw_sz += offset;
    if (raw_sz<=0) {
        if (pcm_buf) free(pcm_buf);
        return false;
    }
    
    // construct wav header
    get_wav_header (raw_sz, channels, sample_rate, &wheader);
    //    dump_wav_header (&wheader);
    
    // write out the .wav file
    fwav = fopen(wavFilePath, "wb");
    if (fwav==NULL) {
        if (pcm_buf) free (pcm_buf);
        return false;
    }
    
    fwrite(&wheader, 1, sizeof(wheader), fwav);
    if (offset<=0) {
        fwrite(pcm_buf-offset, 1, raw_sz, fwav);
    }else{
        char* offsetData = (char*)malloc(offset);
        memset(offsetData, 0, offset);
        fwrite(offsetData, 1, offset, fwav);
        if (offsetData) free(offsetData);
        fwrite(pcm_buf, 1, raw_sz-offset, fwav);
    }
    fclose(fwav);
    
    if (pcm_buf) free(pcm_buf);
    
    return true;
}

bool WAVFile::Cut(char* inputWavFilePath, long startPosMs, long endPosMs, char* outputWavFilePath)
{
    //Input Wav
    if (inputWavFilePath==NULL) {
        LOGE("Input Wav File Path is Null");
        return false;
    }
    
    FILE* inputWavFile = fopen(inputWavFilePath, "r");
    if (inputWavFile==NULL) {
        LOGE("[r] Open Input Wav File Fail");
        return false;
    }
    
    wav_header_t inputWavHeader;
    size_t ret = fread(&inputWavHeader, 1, sizeof(wav_header_t), inputWavFile);
    if (ret!=sizeof(wav_header_t)) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        LOGE("Read Input Wav Header Fail");
        return false;
    }
    
    int sample_rate = inputWavHeader.sample_rate;
    int channels = inputWavHeader.num_channels;
    int input_pcm_data_size = inputWavHeader.sub_chunk2_size;
    
    //Output Wav
    if (outputWavFilePath==NULL) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        
        LOGE("Output Wav File Path is Null");
        return false;
    }
    
    FILE* outputWavFile = fopen(outputWavFilePath, "w");
    if (outputWavFile==NULL) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        
        LOGE("[w] Open Output Wav File Fail");
        return false;
    }
    
    long startPosByte = startPosMs * sample_rate * channels * 2 / 1000;
    if (startPosByte<0) {
        startPosByte = 0;
    }
    if (startPosByte>input_pcm_data_size) {
        startPosByte = input_pcm_data_size;
    }
    
    long endPosByte = endPosMs * sample_rate * channels * 2 / 1000;
    if (endPosByte<0) {
        endPosByte = 0;
    }
    if (endPosByte>input_pcm_data_size) {
        endPosByte = input_pcm_data_size;
    }
    
    long output_pcm_data_size = endPosByte - startPosByte;
    if (output_pcm_data_size<=0) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        
        fclose(outputWavFile);
        outputWavFile = NULL;
        
        LOGE("Output PCM Data Size is %ld, it is invalid", output_pcm_data_size);
        return false;
    }
    
    //IO
    ret = fseek(inputWavFile, startPosByte, SEEK_SET);
    if (ret) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        
        fclose(outputWavFile);
        outputWavFile = NULL;
        
        LOGE("fseek Input Wav File to %ld Fail", startPosByte);
        return false;
    }
    
    inputWavHeader.sub_chunk2_size = output_pcm_data_size;
    
    fwrite(&inputWavHeader, 1, sizeof(wav_header_t), outputWavFile);
    
    char* tmp = (char*)malloc(sample_rate * channels * 2);
    bool isError = false;
    long haveReadByte = 0;
    long readBytePerTime = 0;
    while (true) {
        if (output_pcm_data_size-haveReadByte>=sample_rate * channels * 2) {
            readBytePerTime = sample_rate * channels * 2;
        }else{
            readBytePerTime = output_pcm_data_size-haveReadByte;
        }
        
        if (readBytePerTime<=0) {
            break;
        }
        
        size_t read = fread(tmp, 1, readBytePerTime, inputWavFile);
        if (read!=readBytePerTime) {
            if (feof(inputWavFile)) {
                LOGW("Read Input Wav File To EOF");
            }else{
                LOGE("Read Input Wav File Error, Error Code : %d", ferror(inputWavFile));
                isError = true;
                break;
            }
        }
        
        fwrite(tmp, 1, read, outputWavFile);
        fflush(outputWavFile);

        haveReadByte += read;
    }
    
    if (inputWavFile) {
        fclose(inputWavFile);
        inputWavFile = NULL;
    }
    
    if (outputWavFile) {
        fclose(outputWavFile);
        outputWavFile = NULL;
    }
    
    if (tmp) {
        free(tmp);
        tmp = NULL;
    }
    
    if (isError) {
        MediaFile::deleteFile(outputWavFilePath);
        return false;
    }else{
        return true;
    }
}

bool WAVFile::Merge(char* inputWavFilePath1, char* inputWavFilePath2, char* outputWavFilePath)
{
    if (inputWavFilePath1==NULL || inputWavFilePath2==NULL || outputWavFilePath==NULL) {
        LOGE("Input Param has Null");
        return false;
    }
    
    //Input Wav
    FILE* inputWavFile1 = fopen(inputWavFilePath1, "r");
    if (inputWavFile1==NULL) {
        LOGE("[r] Open Input Wav File Fail [%s]",inputWavFilePath1);
        return false;
    }
    
    wav_header_t inputWavHeader1;
    size_t ret = fread(&inputWavHeader1, 1, sizeof(wav_header_t), inputWavFile1);
    if (ret!=sizeof(wav_header_t)) {
        fclose(inputWavFile1);
        inputWavFile1 = NULL;
        
        LOGE("Read Input Wav Header Fail [%s]", inputWavFilePath1);
        return false;
    }
    
    FILE* inputWavFile2 = fopen(inputWavFilePath2, "r");
    if (inputWavFile2==NULL) {
        fclose(inputWavFile2);
        inputWavFile2 = NULL;
        
        fclose(inputWavFile1);
        inputWavFile1 = NULL;
        
        LOGE("[r] Open Input Wav File Fail [%s]",inputWavFilePath2);
        return false;
    }
    
    wav_header_t inputWavHeader2;
    ret = fread(&inputWavHeader2, 1, sizeof(wav_header_t), inputWavFile2);
    if (ret!=sizeof(wav_header_t)) {
        fclose(inputWavFile2);
        inputWavFile2 = NULL;
        
        fclose(inputWavFile1);
        inputWavFile1 = NULL;
        
        LOGE("Read Input Wav Header Fail [%s]", inputWavFilePath2);
        return false;
    }
    
    //Output Wav
    FILE* outputWavFile = fopen(outputWavFilePath, "w");
    if (outputWavFile==NULL) {
        fclose(inputWavFile2);
        inputWavFile2 = NULL;
        
        fclose(inputWavFile1);
        inputWavFile1 = NULL;
        
        LOGE("[w] Open Output Wav File Fail [%s]",outputWavFilePath);
        return false;
    }
    
    if (inputWavHeader1.sample_rate!=inputWavHeader2.sample_rate || inputWavHeader1.num_channels != inputWavHeader2.num_channels || inputWavHeader1.bits_per_sample != inputWavHeader2.bits_per_sample) {
        fclose(inputWavFile2);
        inputWavFile2 = NULL;
        
        fclose(inputWavFile1);
        inputWavFile1 = NULL;
        
        fclose(outputWavFile);
        outputWavFile = NULL;
        
        LOGE("Input two Wav Files 's sample_rate or channels or bits_per_sample is different");
        return false;
    }

    //IO
    wav_header_t outputWavHeader;
    outputWavHeader = inputWavHeader1;
    outputWavHeader.sub_chunk2_size = inputWavHeader1.sub_chunk2_size + inputWavHeader2.sub_chunk2_size;
    fwrite(&outputWavHeader, 1, sizeof(wav_header_t), outputWavFile);
    
    int size = outputWavHeader.sample_rate * outputWavHeader.num_channels * 2;
    char* tmp = (char*)malloc(size);
    
    bool gotError = false;
    while (true) {
        size_t read = fread(tmp, 1, size, inputWavFile1);
        if (read!=size) {
            if (feof(inputWavFile1)) {
                fwrite(tmp, 1, read, outputWavFile);
                fflush(outputWavFile);
                break;
            }else{
                LOGE("Read Input Wav File Error [%s] [Error Code : %d]", inputWavFilePath1, ferror(inputWavFile1));
                gotError = true;
                break;
            }
        }else{
            fwrite(tmp, 1, read, outputWavFile);
            fflush(outputWavFile);
        }
    }
    
    if (gotError) {
        fclose(inputWavFile2);
        inputWavFile2 = NULL;
        
        fclose(inputWavFile1);
        inputWavFile1 = NULL;
        
        fclose(outputWavFile);
        outputWavFile = NULL;
        
        if (tmp) {
            free(tmp);
            tmp = NULL;
        }
        
        MediaFile::deleteFile(outputWavFilePath);
        return false;
    }
    
    while (true) {
        size_t read = fread(tmp, 1, size, inputWavFile2);
        if (read!=size) {
            if (feof(inputWavFile2)) {
                fwrite(tmp, 1, read, outputWavFile);
                fflush(outputWavFile);
                break;
            }else{
                LOGE("Read Input Wav File Error [%s] [Error Code : %d]", inputWavFilePath2, ferror(inputWavFile2));
                gotError = true;
                break;
            }
        }else{
            fwrite(tmp, 1, read, outputWavFile);
            fflush(outputWavFile);
        }
    }
    
    if (gotError) {
        fclose(inputWavFile2);
        inputWavFile2 = NULL;
        
        fclose(inputWavFile1);
        inputWavFile1 = NULL;
        
        fclose(outputWavFile);
        outputWavFile = NULL;
        
        if (tmp) {
            free(tmp);
            tmp = NULL;
        }
        
        MediaFile::deleteFile(outputWavFilePath);
        return false;
    }
    
    fclose(inputWavFile2);
    inputWavFile2 = NULL;
    
    fclose(inputWavFile1);
    inputWavFile1 = NULL;
    
    fclose(outputWavFile);
    outputWavFile = NULL;
    
    if (tmp) {
        free(tmp);
        tmp = NULL;
    }
    
    return true;
}

int WAVFile::CreateWavData(char** wavData, int len, int channels, int sampleRate)
{
    if (len <= sizeof(wav_header_t)) {
        return -1;
    }
    
    wav_header_t header;
    get_wav_header(len - sizeof(wav_header_t), channels, sampleRate, &header);
    
    memset(*wavData, 0, len);
    memcpy(*wavData, &header, sizeof(wav_header_t));
    
    return len;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WAVFileReader::WAVFileReader(char* wavFilePath)
{
    if (wavFilePath) {
        mWavFilePath = strdup(wavFilePath);
    }
    
    memset(&mWavHeader, 0, sizeof(wav_header_t));
    
    mWavFile = NULL;
    
    mVolume = 1.0f;
}

WAVFileReader::~WAVFileReader()
{
    close();
    
    if (mWavFilePath) {
        free(mWavFilePath);
        mWavFilePath = NULL;
    }
}

bool WAVFileReader::open()
{
    if (mWavFilePath==NULL) {
        LOGE("Wav File Path is null");
        return false;
    }
    
    mWavFile = fopen(mWavFilePath, "r");
    if (mWavFile==NULL) {
        LOGE("[r] Open Wav File Fail");
        return false;
    }
    
    size_t ret = fread(&mWavHeader, 1, sizeof(wav_header_t), mWavFile);
    if (ret!=sizeof(wav_header_t)) {
        fclose(mWavFile);
        mWavFile = NULL;
        LOGE("Read Wav Header Fail");
        return false;
    }
    
    return true;
}

void WAVFileReader::setVolume(float volume)
{
    mVolume = volume;
}

int WAVFileReader::getChannelCount()
{
    return mWavHeader.num_channels;
}

int WAVFileReader::getSampleRate()
{
    return mWavHeader.sample_rate;
}

int WAVFileReader::getBitsPerSample()
{
    return mWavHeader.bits_per_sample;
}

int WAVFileReader::getDurationMs()
{
    long content_size = mWavHeader.sub_chunk2_size;
    if (MediaFile::getFileSizeWithStat(mWavFilePath) != mWavHeader.sub_chunk2_size + sizeof(wav_header_t)) {
        LOGW("WAV Header Info sub_chunk2_size is invalid");
        content_size = MediaFile::getFileSizeWithStat(mWavFilePath) - sizeof(wav_header_t);
    }
    return content_size * 1000 / (mWavHeader.num_channels * mWavHeader.sample_rate * (mWavHeader.bits_per_sample/8));
}

int WAVFileReader::getPcmData(char **pData, long size)
{
    char* data = *pData;
    memset(data, 0, size);
    
    if (mWavFile) {
        size_t ret = fread(data, 1, size, mWavFile);
        if (ret<=0) {
            return 0;
        }
        if (mVolume!=1.0f) {
            int16_t* pcmSample = (int16_t*)data;
            for (int i=0; i<ret/2; i++) {
                pcmSample[i] = fixSample(pcmSample[i] * mVolume);
            }
        }

        return (int)ret;
    }
    
    return 0;
}

void WAVFileReader::close()
{
    if (mWavFile) {
        fclose(mWavFile);
        mWavFile = NULL;
    }
    
    memset(&mWavHeader, 0, sizeof(wav_header_t));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WAVFileWriter::WAVFileWriter(int channels, int sample_rate, char* wavFilePath)
{
    mChannels = channels;
    mSampleRate = sample_rate;
    if (wavFilePath) {
        mWavFilePath = strdup(wavFilePath);
    }
    
    memset(&mWavHeader, 0, sizeof(wav_header_t));
    mWavFile = NULL;

    mTotalPCMWriteSize = 0;
}

WAVFileWriter::~WAVFileWriter()
{
    close();
    
    if (mWavFilePath) {
        free(mWavFilePath);
        mWavFilePath = NULL;
    }
}

bool WAVFileWriter::open()
{
    if (mWavFilePath==NULL) {
        LOGE("Wav File Path is null");
        return false;
    }
    
    mWavFile = fopen(mWavFilePath, "w");
    if (mWavFile==NULL) {
        LOGE("[w] Open Wav File Fail");
        return false;
    }
    
    mTotalPCMWriteSize = 0;
    get_wav_header (mTotalPCMWriteSize, mChannels, mSampleRate, &mWavHeader);
    fwrite(&mWavHeader, 1, sizeof(wav_header_t), mWavFile);
    fflush(mWavFile);

    memset(&mWavHeader, 0, sizeof(wav_header_t));
    return true;
}

void WAVFileWriter::putPcmData(char* data, int size)
{
    if (data==NULL || size<=0) {
        return;
    }
    
    if (mWavFile) {
        fwrite(data, 1, size, mWavFile);
        fflush(mWavFile);
        mTotalPCMWriteSize += size;
    }
}

int32_t WAVFileWriter::getWriteTimeMs()
{
    return mTotalPCMWriteSize*1000/(mSampleRate*mChannels*2);
}

void WAVFileWriter::close()
{
    if (mWavFile) {
        fseek(mWavFile, 0, SEEK_SET);
        
        get_wav_header (mTotalPCMWriteSize, mChannels, mSampleRate, &mWavHeader);
        fwrite(&mWavHeader, 1, sizeof(wav_header_t), mWavFile);
        fflush(mWavFile);
        fclose(mWavFile);
        mWavFile = NULL;
    }
}
