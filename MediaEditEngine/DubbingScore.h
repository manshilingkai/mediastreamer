//
//  DubbingScore.h
//  MediaStreamer
//
//  Created by Think on 2019/9/29.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef DubbingScore_h
#define DubbingScore_h

#include <stdio.h>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
}

#include "TimedEventQueue.h"
#include "IMediaListener.h"
#include "NotificationQueue.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include "AudioPlayer.h"
#include "WAVFile.h"

#include "AudioSimilarity.h"

//#define DUBBING_SCORE_METHOD_1 1
//#define DUBBING_SCORE_METHOD_2 1
#define DUBBING_SCORE_METHOD_3 1

#ifdef DUBBING_SCORE_METHOD_3
#include "dubbing_score.h"
#endif

class DubbingScore : public AudioSimilarity, IMediaListener{
public:
#ifdef ANDROID
    DubbingScore(JavaVM *jvm, char* originUrl, char* bgmUrl, char* dubUrl);
#else
    DubbingScore(char* originUrl, char* bgmUrl, char* dubUrl);
#endif
    ~DubbingScore();
    
#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#else
    void setListener(void (*listener)(void*,int,int,int), void* arg);
#endif
    
    void start();
    void resume();
    void pause();
    void stop();
    
    void notify(int event, int ext1, int ext2);
private:
    enum {
        CONNECTING           = 0x01,
        CONNECTED            = 0x02,
        STREAMING            = 0x04,
        PAUSED               = 0x10,
        ERROR                = 0x20,
        END                  = 0x40,
        INITIALIZED          = 0x80,
        ENDING               = 0x100,
    };
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
private:
    friend struct DubbingScoreEvent;

    DubbingScore(const DubbingScore &);
    DubbingScore &operator=(const DubbingScore &);
    
    pthread_mutex_t mLock;

    TimedEventQueue mQueue;

    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mCalculateEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mNotifyEvent;
    bool mCalculateEventPending;
    
    void prepareAsync_l();
    void play_l();
    void pause_l();
    void stop_l();
    
    void postCalculateEvent_l(int64_t delayUs = -1);
    void postNotifyEvent_l();
    
    void onPrepareAsyncEvent();
    void onCalculateEvent();
    void onStopEvent();
    void onNotifyEvent();
    
    void cancelScoreEvents();

    pthread_cond_t mStopCondition;

    NotificationQueue mNotificationQueue;
    
private:
    int open_all_pipelines_l();
    void close_all_pipelines_l();
    int flowing_l();
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    char* mOriginUrl;
    char* mBgmUrl;
    char* mDubUrl;
    
    IMediaListener* mMediaListener;
private:
    static void init_ffmpeg_env();
private:
    void open_origin();
    void close_origin();
    AudioPlayer* mOriginOutputer;
private:
    void open_bgm();
    void close_bgm();
    AudioPlayer* mBgmOutputer;
private:
    WAVFileReader *mDubWAVFile;
    int open_dub();
    void close_dub();
    char* mWorkAudioDubData;
    long mWorkAudioDubDataSize;
private:
    int mWorkSampleRate;
    AVSampleFormat mWorkSampleFormat;
    int mWorkChannelCount;
    int mSamplesPerFrame;
private:
    int mScoreValue;
private:
    bool isReadyForDubAndBgm;
#ifdef DUBBING_SCORE_METHOD_1
private://SinglePassCorrelation
    double sum_sq_x;
    double sum_sq_y;
    double sum_coproduct;
    double mean_x;
    double mean_y;
    double sweep;
    double delta_x;
    double delta_y;
    double pop_sd_x;
    double pop_sd_y;
    double cov_x_y;
    
    long index;
    long N;
#endif
#ifdef DUBBING_SCORE_METHOD_2
private://CorrelationCoefficient
    double sumA;
    double sumB;
    double aveA;
    double aveB;
    double R1;
    double R2;
    double R3;
    
    double correlation_sum;
    long correlation_num;
#endif
#ifdef DUBBING_SCORE_METHOD_3
private://Audio Similarity based on cosine of base frequency element
    dubbing_score* p_dubbing_score;
#endif
private:
    double correlation;
private:
    //Score according to correlation coefficient
    /*----------------------- From YPP ----------------------------*/
    // 低于30%相似度，在60~75区间打分取随机数，超过50~60%的用户
    // 30~50%相似度，在75~80区间打分取随机数，超过60~70%用户
    // 50~75%相似度，在80~85区间打分取随机数，超过70~80%用户
    // 75~85%相似度，在84~88区间打分取随机数，超过80~90%用户
    // 85~90%相似度，在86~90区间打分取随机数，超过90~95%用户
    // 90~95%相似度，在88~94区间打分取随机数，超过92~97%用户
    // 95~99%相似度，在92~98区间打分取随机数，超过95~99%用户
    // 100%相似度，在97~99区间打分取随机数，超过99%用户
    /*----------------------- From YPP ----------------------------*/

    int generateScorevalueFromCorrelation(double correlationValue);
};

#endif /* DubbingScore_h */
