//
//  AudioPlayer.cpp
//  MediaStreamer
//
//  Created by Think on 2019/7/24.
//  Copyright © 2019年 Cell. All rights reserved.
//

#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#include "AudioPlayer.h"
#include "AutoLock.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#else
#include "NormalMediaListener.h"
#endif

#include "MediaLog.h"
#include "FFLog.h"
#include "PCMUtils.h"
#include "AudioMixer.h"

AudioPlayerOutputQueue::AudioPlayerOutputQueue()
{
    pthread_mutex_init(&mLock, NULL);
    mCount = 0;
}

AudioPlayerOutputQueue::~AudioPlayerOutputQueue()
{
    pthread_mutex_destroy(&mLock);
}

void AudioPlayerOutputQueue::push(AudioPlayerOutput* audioPlayerOutput)
{
    if (audioPlayerOutput==NULL || audioPlayerOutput->type == AUDIO_PLAYER_OUTPUT_UNKNOWN_TYPE) {
        return;
    }
    
    pthread_mutex_lock(&mLock);
    
    mAudioPlayerOutputQueue.push(audioPlayerOutput);
    
    if (mCount<0) {
        mCount = 0;
    }
    mCount++;
    
    pthread_mutex_unlock(&mLock);
}

AudioPlayerOutput* AudioPlayerOutputQueue::pop()
{
    AudioPlayerOutput *audioPlayerOutput = NULL;
    
    pthread_mutex_lock(&mLock);
    
    if (!mAudioPlayerOutputQueue.empty()) {
        audioPlayerOutput = mAudioPlayerOutputQueue.front();
        mAudioPlayerOutputQueue.pop();
    }
    
    if (audioPlayerOutput) {
        if (mCount<0) {
            mCount = 0;
        }
        
        mCount--;
        
        if (mCount<0) {
            mCount = 0;
        }
    }
    
    pthread_mutex_unlock(&mLock);
    
    return audioPlayerOutput;
}

void AudioPlayerOutputQueue::flush()
{
    pthread_mutex_lock(&mLock);
    
    while (!mAudioPlayerOutputQueue.empty()) {
        AudioPlayerOutput *audioPlayerOutput = mAudioPlayerOutputQueue.front();
        if (audioPlayerOutput) {
            audioPlayerOutput->Free();
            delete audioPlayerOutput;
        }
        mAudioPlayerOutputQueue.pop();
    }
    
    mCount = 0;
    
    pthread_mutex_unlock(&mLock);
}

int AudioPlayerOutputQueue::count()
{
    int ret = 0;
    
    pthread_mutex_lock(&mLock);
    
    ret = mCount;
    
    pthread_mutex_unlock(&mLock);
    
    return ret;
}

struct AudioPlayerEvent : public TimedEventQueue::Event {
    AudioPlayerEvent(
                              AudioPlayer *player,
                              void (AudioPlayer::*method)())
    : mPlayer(player),
    mMethod(method) {
    }
    
protected:
    virtual ~AudioPlayerEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mPlayer->*mMethod)();
    }
    
private:
    AudioPlayer *mPlayer;
    void (AudioPlayer::*mMethod)();
    
    AudioPlayerEvent(const AudioPlayerEvent &);
    AudioPlayerEvent &operator=(const AudioPlayerEvent &);
};

#ifdef ANDROID
AudioPlayer::AudioPlayer(JavaVM *jvm, AUDIO_PLAYER_TYPE type, AudioRenderConfigure configure)
{
    mJvm = jvm;
    
    init_ffmpeg_env();
    
    mAudioPlayerType = type;
    mAudioRenderConfigure = configure;
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);
    
    mAsyncPrepareEvent = NULL;
    mPlayEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    mSeekToEvent = NULL;
    
    mAsyncPrepareEvent = new AudioPlayerEvent(this, &AudioPlayer::onPrepareAsyncEvent);
    mPlayEvent = new AudioPlayerEvent(this, &AudioPlayer::onPlayEvent);
    mStopEvent = new AudioPlayerEvent(this, &AudioPlayer::onStopEvent);
    mNotifyEvent = new AudioPlayerEvent(this, &AudioPlayer::onNotifyEvent);
    mSeekToEvent = new AudioPlayerEvent(this, &AudioPlayer::onSeekToEvent);
    mPlayEventPending = false;
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();
    
    pthread_cond_init(&mStopCondition, NULL);
    pthread_cond_init(&mPrepareCondition, NULL);
    
    mPlayUrl = NULL;
    mSourceId = -1;
    mMediaListener = NULL;
    mInternalMediaListener = NULL;
    
    isAutoPlay = false;
    
    ifmt_ctx = NULL;
    mAudioStreamIndex = -1;
    audio_dec_ctx = NULL;
    
    audio_buffersrc_ctx = NULL;
    audio_buffersink_ctx = NULL;
    audio_filter_graph = NULL;
    
    pthread_mutex_init(&mAVAudioFifoLock, NULL);
    filteredAudioDataFifo = NULL;
    
    mAudioRenderType = AUDIO_RENDER_OPENSLES;//AUDIO_RENDER_AUDIOTRACK;
    mAudioRender = NULL;
    
    mCurrentAudioPtsUs = 0ll;
    pthread_mutex_init(&mPropertyLock, NULL);
    mDurationMs = 0;
    mPlayTimeMs = 0;
    pthread_mutex_init(&mPcmDBPropertyLock, NULL);
    mPcmDB = 0;
    
    mVolume = 1.0f;
    mPlayRate = 1.0f;
    isNeedUpdateAudioFilter = false;

    mIsLooping = false;
    
    mSeekPosUs = 0ll;
    
    isInterrupt = 0;
    pthread_mutex_init(&mInterruptLock, NULL);
    
    mGotFirstAudioPacket = false;
    
    isPlayToEnd = false;
    
    isLocalAudioFile = false;
    
    pthread_mutex_init(&mOutputAVAudioFifoLock, NULL);
    mOutputAVAudioFifo = av_audio_fifo_alloc(mAudioRenderConfigure.sampleFormat, mAudioRenderConfigure.channelCount, 1);
    mOneOutoutBuffer = NULL;
    mOneOutoutBufferSize = 0;
    
    pthread_mutex_init(&mDrainLock, NULL);
    pthread_cond_init(&mDrainCondition, NULL);
}
#else
AudioPlayer::AudioPlayer(AUDIO_PLAYER_TYPE type, AudioRenderConfigure configure)
{
    init_ffmpeg_env();
    
    mAudioPlayerType = type;
    mAudioRenderConfigure = configure;
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);
    
    mAsyncPrepareEvent = NULL;
    mPlayEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    mSeekToEvent = NULL;
    
    mAsyncPrepareEvent = new AudioPlayerEvent(this, &AudioPlayer::onPrepareAsyncEvent);
    mPlayEvent = new AudioPlayerEvent(this, &AudioPlayer::onPlayEvent);
    mStopEvent = new AudioPlayerEvent(this, &AudioPlayer::onStopEvent);
    mNotifyEvent = new AudioPlayerEvent(this, &AudioPlayer::onNotifyEvent);
    mSeekToEvent = new AudioPlayerEvent(this, &AudioPlayer::onSeekToEvent);
    mPlayEventPending = false;
    
    mQueue.start();
    
    pthread_cond_init(&mStopCondition, NULL);
    pthread_cond_init(&mPrepareCondition, NULL);
    
    mPlayUrl = NULL;
    mSourceId = -1;
    mMediaListener = NULL;
    mInternalMediaListener = NULL;
    
    isAutoPlay = false;
    
    ifmt_ctx = NULL;
    mAudioStreamIndex = -1;
    audio_dec_ctx = NULL;
    
    audio_buffersrc_ctx = NULL;
    audio_buffersink_ctx = NULL;
    audio_filter_graph = NULL;
    
    pthread_mutex_init(&mAVAudioFifoLock, NULL);
    filteredAudioDataFifo = NULL;
    
    mAudioRenderType = AUDIO_RENDER_UNKNOWN;
#ifdef IOS
    mAudioRenderType = AUDIO_RENDER_AUDIOUNIT;
#endif
    mAudioRender = NULL;
    
    mCurrentAudioPtsUs = 0ll;
    pthread_mutex_init(&mPropertyLock, NULL);
    mDurationMs = 0;
    mPlayTimeMs = 0;
    pthread_mutex_init(&mPcmDBPropertyLock, NULL);
    mPcmDB = 0;
    
    mVolume = 1.0f;
    mPlayRate = 1.0f;
    isNeedUpdateAudioFilter = false;
    
    mIsLooping = false;
    
    mSeekPosUs = 0ll;
    
    isInterrupt = 0;
    pthread_mutex_init(&mInterruptLock, NULL);
    
    mGotFirstAudioPacket = false;
    
    isPlayToEnd = false;
    
    isLocalAudioFile = false;
    
    pthread_mutex_init(&mOutputAVAudioFifoLock, NULL);
    mOutputAVAudioFifo = av_audio_fifo_alloc(mAudioRenderConfigure.sampleFormat, mAudioRenderConfigure.channelCount, 1);
    mOneOutoutBuffer = NULL;
    mOneOutoutBufferSize = 0;
    
    pthread_mutex_init(&mDrainLock, NULL);
    pthread_cond_init(&mDrainCondition, NULL);
}
#endif

AudioPlayer::~AudioPlayer()
{
    stop();
    
    mQueue.stop(true);
    
    mNotificationQueue.flush();
    
    if (mAudioPlayerType==AUDIO_PLAYER_EXTERNAL_RENDER) {
        mAudioPlayerOutputQueue.flush();
    }
    
    if (mOutputAVAudioFifo) {
        av_audio_fifo_free(mOutputAVAudioFifo);
        mOutputAVAudioFifo = NULL;
    }
    if (mOneOutoutBuffer) {
        free(mOneOutoutBuffer);
        mOneOutoutBuffer = NULL;
    }
    mOneOutoutBufferSize = 0;
    pthread_mutex_destroy(&mOutputAVAudioFifoLock);
    
    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
    
    if (mPlayEvent!=NULL) {
        delete mPlayEvent;
        mPlayEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    if (mSeekToEvent!=NULL) {
        delete mSeekToEvent;
        mSeekToEvent = NULL;
    }
    
    pthread_cond_destroy(&mStopCondition);
    pthread_cond_destroy(&mPrepareCondition);
    pthread_mutex_destroy(&mLock);
    
    pthread_mutex_destroy(&mAVAudioFifoLock);
    pthread_mutex_destroy(&mPropertyLock);
    pthread_mutex_destroy(&mPcmDBPropertyLock);
    pthread_mutex_destroy(&mInterruptLock);
        
#ifdef ANDROID
    JniMediaListener *jniListener = (JniMediaListener*)mMediaListener;
    if (jniListener!=NULL) {
        delete jniListener;
        jniListener = NULL;
    }
#else
    NormalMediaListener* normalMediaListener = (NormalMediaListener*)mMediaListener;
    if (normalMediaListener!=NULL) {
        delete normalMediaListener;
        normalMediaListener = NULL;
    }
#endif
    
    mMediaListener = NULL;
    mInternalMediaListener = NULL;
    
    if (mPlayUrl) {
        free(mPlayUrl);
        mPlayUrl = NULL;
    }
    
    pthread_mutex_destroy(&mDrainLock);
    pthread_cond_destroy(&mDrainCondition);
}

#ifdef ANDROID
void AudioPlayer::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
}
#else
void AudioPlayer::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new NormalMediaListener(listener,arg);
}
#endif

void AudioPlayer::setInternalListener(IMediaCallback* listener)
{
    AutoLock autoLock(&mLock);

    mInternalMediaListener = listener;
}

void AudioPlayer::setDataSource(char* url)
{
    this->setDataSourceWithId(url, -1);
}

void AudioPlayer::setDataSourceWithId(char* url, int sourceId)
{
    AutoLock autoLock(&mLock);
    
    if (url==NULL) {
        return;
    }
    
    if (mFlags==0) {
        if (mPlayUrl) {
            free(mPlayUrl);
            mPlayUrl = NULL;
        }
        mPlayUrl = strdup(url);
        
        if (mPlayUrl[0]=='/') {
            isLocalAudioFile = true;
        }else{
            isLocalAudioFile = false;
        }
        
        modifyFlags(INITIALIZED, SET);
    }else{
        if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
            return;
        }
        
        if (mPlayUrl) {
            free(mPlayUrl);
            mPlayUrl = NULL;
        }
        mPlayUrl = strdup(url);
        
        if (mPlayUrl[0]=='/') {
            isLocalAudioFile = true;
        }else{
            isLocalAudioFile = false;
        }
    }
    
    mSourceId = sourceId;
}

void AudioPlayer::notifyListener_l(int event, int ext1, int ext2)
{
    if (mInternalMediaListener!=NULL) {
        mInternalMediaListener->onMediaCallback((void*)this, mSourceId, event, ext1, ext2);
    }
    
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void AudioPlayer::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    postNotifyEvent_l();
}

void AudioPlayer::postNotifyEvent_l()
{
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void AudioPlayer::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
    
    switch (event) {
        case AUDIO_PLAYER_ERROR:
            modifyFlags(ERRORED, ASSIGN);
            if (ext2!=AVERROR_EXIT) {
                notifyListener_l(AUDIO_PLAYER_ERROR, ext1, ext2);
            }
            stop_l();
            break;
        case AUDIO_PLAYER_INFO:
            break;
            
        default:
            break;
    }
}

void AudioPlayer::prepare()
{
    AutoLock autoLock(&mLock);

    if (mFlags & PREPARED) {
        return;
    }
    
    if (mFlags & PREPARING) {
        LOGW("already prepareing");
        pthread_cond_wait(&mPrepareCondition, &mLock);
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    isAutoPlay = false;
    
    prepareAsync_l();
    
    pthread_cond_wait(&mPrepareCondition, &mLock);
}

void AudioPlayer::prepareAsync()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & PREPARED) {
        return;
    }
    
    if (mFlags & PREPARING) {
        LOGW("already prepareing");
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    isAutoPlay = false;
    
    prepareAsync_l();
}

void AudioPlayer::prepareAsyncToPlay()
{
    AutoLock autoLock(&mLock);
    if (mFlags & PREPARED) {
        return;
    }
    
    if (mFlags & PREPARING) {
        LOGW("already prepareing");
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    isAutoPlay = true;
    
    prepareAsync_l();
}

void AudioPlayer::prepareAsync_l()
{
    modifyFlags(END, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(PREPARING, SET);
        
    mQueue.postEvent(mAsyncPrepareEvent);
}

void AudioPlayer::onPrepareAsyncEvent()
{
    AutoLock autoLock(&mLock);
    int ret = open_all_pipelines_l();
    
    if (ret>=0) {
        modifyFlags(PREPARING, CLEAR);
        modifyFlags(PREPARED, SET);
        
        if (isAutoPlay) {
            notifyListener_l(AUDIO_PLAYER_PREPARED, 1);
        }else{
            notifyListener_l(AUDIO_PLAYER_PREPARED, 0);
        }
        
        if (isAutoPlay) {
            play_l();
        }
    }else{
        if (ret!=AVERROR_EXIT) {
            int error_type = AUDIO_PLAYER_ERROR_UNKNOWN;
            if (ret==-1) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_NO_MEMORY);
                error_type = AUDIO_PLAYER_ERROR_NO_MEMORY;
            }else if (ret==-2) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_OPEN_PLAY_URL_FAIL);
                error_type = AUDIO_PLAYER_ERROR_OPEN_PLAY_URL_FAIL;
            }else if (ret==-3) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_FIND_STREAM_INFO_FAIL);
                error_type = AUDIO_PLAYER_ERROR_FIND_STREAM_INFO_FAIL;
            }else if (ret==-4) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_NO_AUDIO_STREAM);
                error_type = AUDIO_PLAYER_ERROR_NO_AUDIO_STREAM;
            }else if (ret==-5) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_AUDIO_DECODER_NOT_FOUND);
                error_type = AUDIO_PLAYER_ERROR_AUDIO_DECODER_NOT_FOUND;
            }else if (ret==-6) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_ALLOCATE_AUDIO_DECODER_CONTEXT_FAIL);
                error_type = AUDIO_PLAYER_ERROR_ALLOCATE_AUDIO_DECODER_CONTEXT_FAIL;
            }else if (ret==-7) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_COPY_AUDIO_STREAM_CODEC_PARAM_TO_AUDIO_DECODER_CONTEXT_FAIL);
                error_type = AUDIO_PLAYER_ERROR_COPY_AUDIO_STREAM_CODEC_PARAM_TO_AUDIO_DECODER_CONTEXT_FAIL;
            }else if (ret==-8) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_OPEN_AUDIO_DECODER_FAIL);
                error_type = AUDIO_PLAYER_ERROR_OPEN_AUDIO_DECODER_FAIL;
            }else if (ret==-9) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_OPEN_AUDIO_FILTER_FAIL);
                error_type = AUDIO_PLAYER_ERROR_OPEN_AUDIO_FILTER_FAIL;
            }else if (ret==-10) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_CREATE_AUDIO_RENDER_FAIL);
                error_type = AUDIO_PLAYER_ERROR_CREATE_AUDIO_RENDER_FAIL;
            }else if (ret==-11) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_INIT_AUDIO_RENDER_FAIL);
                error_type = AUDIO_PLAYER_ERROR_INIT_AUDIO_RENDER_FAIL;
            }else {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_UNKNOWN);
                error_type = AUDIO_PLAYER_ERROR_UNKNOWN;
            }
            
            if (mAudioPlayerType==AUDIO_PLAYER_EXTERNAL_RENDER)
            {
                AudioPlayerOutputError* audioPlayerOutputError = new AudioPlayerOutputError;
                audioPlayerOutputError->error_type = error_type;
                AudioPlayerOutput* audioPlayerOutput = new AudioPlayerOutput;
                audioPlayerOutput->type = AUDIO_PLAYER_OUTPUT_ERROR;
                audioPlayerOutput->output = audioPlayerOutputError;
                
                mAudioPlayerOutputQueue.push(audioPlayerOutput);
            }
        }
        
        modifyFlags(PREPARING, CLEAR);
        modifyFlags(ERRORED, SET);
        stop_l();
    }
    
    pthread_cond_broadcast(&mPrepareCondition);
}

void AudioPlayer::play()
{
    AutoLock autoLock(&mLock);
    
    return play_l();
}

void AudioPlayer::play_l()
{
    if (mFlags & PLAYING) {
        if (mFlags & COMPLETED) {
            LOGW("AudioPlayer has completed");
            return;
        }
        LOGW("%s","AudioPlayer is already playing");
        return;
    }
    
    if (!(mFlags & PREPARED) && !(mFlags & PAUSED)) {
        return;
    }
    
    modifyFlags(PREPARED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    if (mAudioRender) {
        mAudioRender->startPlayout();
    }
    
    postPlayEvent_l();
    
    modifyFlags(PLAYING, SET);
}

void AudioPlayer::pause()
{
    AutoLock autoLock(&mLock);
    return pause_l();
}

void AudioPlayer::pause_l()
{
    if (!(mFlags & PLAYING)) {
        return;
    }
    
    modifyFlags(PLAYING, CLEAR);
    
    mQueue.cancelEvent(mPlayEvent->eventID());
    mPlayEventPending = false;
    
    if (mAudioRender) {
        mAudioRender->stopPlayout();
    }
    
    modifyFlags(PAUSED, SET);

    return;
}

bool AudioPlayer::isPlaying()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & PLAYING){
        if (mFlags & COMPLETED) {
            return false;
        }
        return true;
    }else return false;
}

void AudioPlayer::seekTo(int32_t seekPosMs)
{
    AutoLock autoLock(&mLock);
    seekTo_l(seekPosMs);
}

void AudioPlayer::seekTo_l(int32_t seekPosMs)
{
    if (!(mFlags & PREPARED) && !(mFlags & PLAYING) && !(mFlags & PAUSED)) {
        return;
    }
    
    if (mFlags & SEEKING) {
        LOGW("is seeking!!");
        return;
    }
    
    if (seekPosMs<0) {
        LOGW("seekPosMs can't be negative number");
        seekPosMs = 0;
    }
    
    if (mDurationMs<=0) {
        seekPosMs = 0;
    }else{
        if (seekPosMs>=mDurationMs) {
            LOGW("seekPosMs can't be bigger than mDuration");
            seekPosMs = mDurationMs-1000;//fix for av_seek block
            if(seekPosMs<0)
            {
                seekPosMs = 0;
            }
        }
    }
    
    mSeekPosUs = (int64_t)(seekPosMs*1000ll);
    
    pthread_mutex_lock(&mPropertyLock);
    mPlayTimeMs = seekPosMs;
    pthread_mutex_unlock(&mPropertyLock);
    
    isPlayToEnd = false;
    modifyFlags(COMPLETED, CLEAR);
    
    modifyFlags(SEEKING, SET);
    
    mQueue.postEvent(mSeekToEvent);
}

void AudioPlayer::onSeekToEvent()
{
    AutoLock autoLock(&mLock);
    
    if(!mGotFirstAudioPacket)
    {
        int result = 0;
        while (true) {
            AVPacket packet;
            av_init_packet(&packet);
            packet.data = NULL;
            packet.size = 0;
            packet.flags = 0;
            int ret = av_read_frame(ifmt_ctx, &packet);
            if (ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN)) {
                av_packet_unref(&packet);
                continue;
            }else if (ret == AVERROR_EOF) {
                av_packet_unref(&packet);
                result = ret;
                break;
            }else if (ret<0) {
                av_packet_unref(&packet);
                result = ret;
                break;
            }else{
                if (packet.stream_index==mAudioStreamIndex) {
                    mGotFirstAudioPacket = true;
                    ifmt_ctx->start_time = packet.pts;
                    ifmt_ctx->streams[mAudioStreamIndex]->start_time = packet.pts;
                    av_packet_unref(&packet);
                    result = ret;
                    break;
                }else{
                    av_packet_unref(&packet);
                    continue;
                }
            }
        }
        
        if (!mGotFirstAudioPacket) {
            if (result==AVERROR_EXIT) {
                LOGW("Immediate exit was requested");
            }else{
                if (result==AVERROR_EOF) {
                    LOGE("got first audio packet is eof packet, exit...");
                }else{
                    LOGE("got error packet, exit...");
                }
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_DEMUXER_READ_FAIL);
                
                if (mAudioPlayerType==AUDIO_PLAYER_EXTERNAL_RENDER)
                {
                    AudioPlayerOutputError* audioPlayerOutputError = new AudioPlayerOutputError;
                    audioPlayerOutputError->error_type = AUDIO_PLAYER_ERROR_DEMUXER_READ_FAIL;
                    AudioPlayerOutput* audioPlayerOutput = new AudioPlayerOutput;
                    audioPlayerOutput->type = AUDIO_PLAYER_OUTPUT_ERROR;
                    audioPlayerOutput->output = audioPlayerOutputError;
                    
                    mAudioPlayerOutputQueue.push(audioPlayerOutput);
                }
            }
            
            stop_l();
            
            modifyFlags(SEEKING, CLEAR);
            notifyListener_l(AUDIO_PLAYER_SEEK_COMPLETE);
            
            return;
        }
    }
    
    avformat_seek_file(ifmt_ctx, -1, INT64_MIN, av_rescale(mSeekPosUs/1000, AV_TIME_BASE, 1000) + ifmt_ctx->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
    
    if (audio_dec_ctx) {
        avcodec_flush_buffers(audio_dec_ctx);
    }
    
    close_audio_filter();
    char afiltersDesc[256];
    afiltersDesc[0] = 0;
    if (mPlayRate>=0.5f && mPlayRate<=2.0f) {
        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,volume=%f", mPlayRate, mVolume);
//        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f", mPlayRate);
    }else if(mPlayRate<0.5f){
        float atempo2 = 0.0f;
        atempo2 = mPlayRate/0.5f;
        if (atempo2<0.5f) {
            atempo2 = 0.5f;
        }
        
        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 0.5f, atempo2, mVolume);
//        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f", 0.5f, atempo2);
    }else if (mPlayRate>2.0f) {
        float atempo2 = 0.0f;
        atempo2 = mPlayRate/2.0f;
        if (atempo2>2.0f) {
            atempo2 = 2.0f;
        }
        
        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 2.0f, atempo2, mVolume);
//        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f", 2.0f, atempo2);
    }
    open_audio_filter(audio_dec_ctx, mAudioRenderConfigure, afiltersDesc);
    
    pthread_mutex_lock(&mAVAudioFifoLock);
    av_audio_fifo_reset(filteredAudioDataFifo);
    pthread_mutex_unlock(&mAVAudioFifoLock);
    
    while (true) {
        AVPacket packet;
        av_init_packet(&packet);
        packet.data = NULL;
        packet.size = 0;
        packet.flags = 0;
        
        int ret = av_read_frame(ifmt_ctx, &packet);
        if (ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN)) {
            av_packet_unref(&packet);
            continue;
        }else if (ret == AVERROR_EOF) {
            av_packet_unref(&packet);
            
            LOGD("got eof packet");
            
            if (mIsLooping) {
                avformat_seek_file(ifmt_ctx, -1, INT64_MIN, ifmt_ctx->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
            }else{
                modifyFlags(COMPLETED, SET);
            }
            
            break;
        }else if (ret<0) {
            av_packet_unref(&packet);
            
            if (ret==AVERROR_EXIT) {
                LOGW("Immediate exit was requested");
            }else{
                LOGE("got error data, exit...");
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_DEMUXER_READ_FAIL);
                
                if (mAudioPlayerType==AUDIO_PLAYER_EXTERNAL_RENDER)
                {
                    AudioPlayerOutputError* audioPlayerOutputError = new AudioPlayerOutputError;
                    audioPlayerOutputError->error_type = AUDIO_PLAYER_ERROR_DEMUXER_READ_FAIL;
                    AudioPlayerOutput* audioPlayerOutput = new AudioPlayerOutput;
                    audioPlayerOutput->type = AUDIO_PLAYER_OUTPUT_ERROR;
                    audioPlayerOutput->output = audioPlayerOutputError;
                    
                    mAudioPlayerOutputQueue.push(audioPlayerOutput);
                }
            }
            
            stop_l();
            break;
        }else{
            if (packet.stream_index==mAudioStreamIndex) {
                
                AVRational av_time_base_q;
                av_time_base_q.num = 1;
                av_time_base_q.den = AV_TIME_BASE;
                int64_t seekTargetPos= av_rescale_q(mSeekPosUs, av_time_base_q, ifmt_ctx->streams[mAudioStreamIndex]->time_base) + ifmt_ctx->streams[mAudioStreamIndex]->start_time;
                if (packet.pts>=seekTargetPos) {
                    AVFrame *frame = av_frame_alloc();
                    int got_frame;
                    int ret = avcodec_decode_audio4(audio_dec_ctx, frame, &got_frame, &packet);
                    av_packet_unref(&packet);
                    
                    if (ret < 0) {
                        av_frame_free(&frame);
                        LOGW("Audio Decode Fail");
                        continue;
                    }else{
                        if (got_frame) {
                            calculateAudioFramePts(frame);
                            mCurrentAudioPtsUs = frame->pts;
                            
                            av_frame_free(&frame);
                            break;
                        }else{
                            av_frame_free(&frame);
                            continue;
                        }
                    }
                }else{
                    av_packet_unref(&packet);
                }
            }else{
                av_packet_unref(&packet);
            }
        }
    }
    
    modifyFlags(SEEKING, CLEAR);
    notifyListener_l(AUDIO_PLAYER_SEEK_COMPLETE);
}

void AudioPlayer::postPlayEvent_l(int64_t delayUs)
{
    if (mPlayEventPending) {
        return;
    }
    mPlayEventPending = true;
    mQueue.postEventWithDelay(mPlayEvent, delayUs < 0 ? 0 : delayUs);
}

void AudioPlayer::cancelPlayerEvents()
{
    mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
    mQueue.cancelEvent(mPlayEvent->eventID());
    mPlayEventPending = false;
    mQueue.cancelEvent(mStopEvent->eventID());
    mQueue.cancelEvent(mNotifyEvent->eventID());
    mQueue.cancelEvent(mSeekToEvent->eventID());
}

void AudioPlayer::stop()
{
    interrupt();
    
    AutoLock autoLock(&mLock);
    
    if (mFlags & END) {
        isInterrupt = 0;
        return;
    }
    
    if (mFlags & ENDING) {
        LOGW("already ending");
        
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
}

void AudioPlayer::stop_l()
{
    modifyFlags(ENDING, ASSIGN);
    
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void AudioPlayer::onStopEvent()
{
    AutoLock autoLock(&mLock);
    
    close_all_pipelines_l();
    
    cancelPlayerEvents();
    
    mNotificationQueue.flush();
    
    if (mAudioPlayerType!=AUDIO_PLAYER_EXTERNAL_RENDER) {
        mAudioPlayerOutputQueue.flush();
    }
    
    mDurationMs = 0;
    mPlayTimeMs = 0;
    mCurrentAudioPtsUs = 0ll;
    mPcmDB = 0;
    
    mVolume = 1.0f;
    mPlayRate = 1.0f;
    isNeedUpdateAudioFilter = false;
    
    mIsLooping = false;
    
    mSeekPosUs = 0ll;
    
    isInterrupt = 0;
    
    mGotFirstAudioPacket = false;
    
    isPlayToEnd = false;
    
    modifyFlags(END, ASSIGN);
    
    pthread_cond_broadcast(&mStopCondition);
}

void AudioPlayer::onPlayEvent()
{
    AutoLock autoLock(&mLock);
    
    if (!mPlayEventPending) {
        // The event has been cancelled in reset_l() but had already // been scheduled for execution at that time.
        return;
    }
    mPlayEventPending = false;
    
    int ret = flowing_l();
    if (ret==1) {
        postPlayEvent_l(0);
        return;
    }else if (ret<0) {
        modifyFlags(PLAYING, CLEAR);
        modifyFlags(ERRORED, SET);
        
        if (ret!=AVERROR_EXIT) {
            int error_type = AUDIO_PLAYER_ERROR_UNKNOWN;
            if (ret==-1) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_DEMUXER_READ_FAIL);
                error_type = AUDIO_PLAYER_ERROR_DEMUXER_READ_FAIL;
            }else if (ret==-2) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_NO_MEMORY);
                error_type = AUDIO_PLAYER_ERROR_NO_MEMORY;
            }else if (ret==-3) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_AUDIO_FILTER_INPUT_FAIL);
                error_type = AUDIO_PLAYER_ERROR_AUDIO_FILTER_INPUT_FAIL;
            }else if (ret==-4) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_AUDIO_FILTER_OUTPUT_FAIL);
                error_type = AUDIO_PLAYER_ERROR_AUDIO_FILTER_OUTPUT_FAIL;
            }else if (ret==-5) {
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_UPDATE_AUDIO_FILTER_FAIL);
                error_type = AUDIO_PLAYER_ERROR_UPDATE_AUDIO_FILTER_FAIL;
            }else{
                notifyListener_l(AUDIO_PLAYER_ERROR, AUDIO_PLAYER_ERROR_UNKNOWN);
                error_type = AUDIO_PLAYER_ERROR_UNKNOWN;
            }
            
            if (mAudioPlayerType==AUDIO_PLAYER_EXTERNAL_RENDER)
            {
                AudioPlayerOutputError* audioPlayerOutputError = new AudioPlayerOutputError;
                audioPlayerOutputError->error_type = error_type;
                AudioPlayerOutput* audioPlayerOutput = new AudioPlayerOutput;
                audioPlayerOutput->type = AUDIO_PLAYER_OUTPUT_ERROR;
                audioPlayerOutput->output = audioPlayerOutputError;
                
                mAudioPlayerOutputQueue.push(audioPlayerOutput);
            }
        }
        
        stop_l();
        
        return;
    }else {
        int64_t waitTimeUs = 20*1000;
        postPlayEvent_l(waitTimeUs);
        return;
    }
}

void AudioPlayer::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

void AudioPlayer::init_ffmpeg_env()
{
    // init ffmpeg env
    av_register_all();
    avcodec_register_all();
    avfilter_register_all();
    
    avformat_network_init();
    FFLog::setLogLevel(AV_LOG_WARNING);
}

// AVERROR_EXIT : EXIT
// -1 : NO MEMORY
// -2 : OPEN PLAY URL FAIL
// -3 : FIND STREAM INFO FAIL
// -4 : NO AUDIO STREAM
// -5 : AUDIO DECODER NOT FOUND
// -6 : ALLOCATE AUDIO DECODER CONTEXT FAIL
// -7 : COPY AUDIO STREAM CODEC PARAM TO AUDIO DECODER CONTEXT FAIL
// -8 : OPEN AUDIO DECODER FAIL
// -9 : OPEN AUDIO FILTER FAIL
// -10 : CREATE AUDIO RENDER FAIL
// -11 : INIT AUDIO RENDER FAIL
int AudioPlayer::open_all_pipelines_l()
{
    int err = -1;
    if (ifmt_ctx) {
        avformat_free_context(ifmt_ctx);
        ifmt_ctx = NULL;
    }
    ifmt_ctx = avformat_alloc_context();
    if (ifmt_ctx==NULL) {
        LOGE("%s","[AudioPlayer]:Fail Allocate an AVFormatContext");
//        return AVERROR(ENOMEM);
        return -1;
    }
    
    AVDictionary* options = NULL;

    if(strncmp(mPlayUrl, "http", 4) == 0){
        av_dict_set(&options, "timeout", "20000000", 0);
    }
    
    av_dict_set_int(&options, "enable_private_getaddrinfo", 1, 0);
    av_dict_set_int(&options, "addrinfo_one_by_one", 1, 0);
    av_dict_set_int(&options, "addrinfo_timeout", 10000000, 0);

    av_dict_set_int(&options, "dns_cache_timeout", 86400000000, 0);
        
    ifmt_ctx->interrupt_callback.callback = interruptCallback;
    ifmt_ctx->interrupt_callback.opaque = this;
    
    ifmt_ctx->flags |= AVFMT_FLAG_NONBLOCK;
    ifmt_ctx->flags |= AVFMT_FLAG_FAST_SEEK;
//    ifmt_ctx->flags |= AVFMT_FLAG_GENPTS;
    
#ifdef ANDROID
    ifmt_ctx->probesize = 48*1024;
    ifmt_ctx->max_analyze_duration = 0.5*AV_TIME_BASE;
#else
    ifmt_ctx->probesize = 48*1024;
    ifmt_ctx->max_analyze_duration = 0.5*AV_TIME_BASE;
#endif
    
    av_dict_set_int(&options, "enable_slk_dns_resolver", 0, 0);
    
    err = avformat_open_input(&ifmt_ctx, mPlayUrl, NULL, &options);
    if(err<0)
    {
        if (ifmt_ctx) {
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
        }
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
            return AVERROR_EXIT;
        }else{
            LOGE("%s",mPlayUrl);
            LOGE("%s","[AudioPlayer]:Open Data Source Fail");
            LOGE("[AudioPlayer]:ERROR CODE:%d",err);
            return -2;
        }
    }
    
    // get track info
    err = avformat_find_stream_info(ifmt_ctx, NULL);
    if (err < 0)
    {
        avformat_close_input(&ifmt_ctx);
        avformat_free_context(ifmt_ctx);
        ifmt_ctx = NULL;
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
            return AVERROR_EXIT;
        }else{
            LOGE("%s","[AudioPlayer]:Get Stream Info Fail");
            LOGE("[AudioPlayer]:ERROR CODE:%d",err);
            
            return -3;
        }
    }
    
    pthread_mutex_lock(&mPropertyLock);
    mDurationMs = av_rescale(ifmt_ctx->duration, 1000, AV_TIME_BASE);
    LOGD("Stream Duration Ms:%d",mDurationMs);
    pthread_mutex_unlock(&mPropertyLock);
    
    mAudioStreamIndex = -1;

    for(int i= 0; i < ifmt_ctx->nb_streams; i++)
    {
        if (ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first audio stream, and discard others.
            if(mAudioStreamIndex == -1)
            {
                mAudioStreamIndex = i;
            }
            else
            {
                ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
            }
        }else{
            ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
        }
    }
    
    if (mAudioStreamIndex!=-1 && ifmt_ctx->streams[mAudioStreamIndex]!=NULL) {
        int current_audio_sample_rate = ifmt_ctx->streams[mAudioStreamIndex]->codec->sample_rate;
        int current_audio_channels = ifmt_ctx->streams[mAudioStreamIndex]->codec->channels;
        AVSampleFormat current_audio_format = ifmt_ctx->streams[mAudioStreamIndex]->codec->sample_fmt;
        
        if (current_audio_sample_rate<=0) {
            current_audio_sample_rate = 44100;
            ifmt_ctx->streams[mAudioStreamIndex]->codec->sample_rate = 44100;
        }
        
        if (current_audio_format<0 || current_audio_format>=AV_SAMPLE_FMT_NB) {
            current_audio_format = AV_SAMPLE_FMT_FLTP;
            ifmt_ctx->streams[mAudioStreamIndex]->codec->sample_fmt = AV_SAMPLE_FMT_FLTP;
        }
        
        if (current_audio_sample_rate<=0 || current_audio_channels<=0 || current_audio_format<0 || current_audio_format>=AV_SAMPLE_FMT_NB) {
            LOGW("[AudioPlayer]:InValid Audio Stream");
            ifmt_ctx->streams[mAudioStreamIndex]->discard = AVDISCARD_ALL;
            mAudioStreamIndex=-1;
        }
    }
    
    if (mAudioStreamIndex==-1) {
        avformat_close_input(&ifmt_ctx);
        avformat_free_context(ifmt_ctx);
        ifmt_ctx = NULL;
        LOGE("[AudioPlayer]:No Audio Stream");
        return -4;
    }
    
    if (mAudioStreamIndex!=-1) {
        AVStream *audio_stream = ifmt_ctx->streams[mAudioStreamIndex];
#if defined(IOS) || defined(ANDROID)
        AVCodec* audio_dec = NULL;
        if (audio_stream->codecpar->codec_id==AV_CODEC_ID_AAC)
        {
            audio_dec = avcodec_find_decoder_by_name("libfdk_aac");
        }else{
            audio_dec = avcodec_find_decoder(audio_stream->codecpar->codec_id);
        }
#else
        AVCodec *audio_dec = avcodec_find_decoder(audio_stream->codecpar->codec_id);
#endif
        if (!audio_dec) {
            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
            
            LOGE("Failed to find audio decoder");
            
//            return AVERROR_DECODER_NOT_FOUND;
            return -5;
        }
        AVCodecContext *audio_codec_ctx = avcodec_alloc_context3(audio_dec);
        if (!audio_codec_ctx) {
            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
            
            LOGE("Failed to allocate the audio decoder context");
            
//            return AVERROR(ENOMEM);
            return -6;
        }
        
        err = avcodec_parameters_to_context(audio_codec_ctx, audio_stream->codecpar);
        if (err < 0) {
            avcodec_free_context(&audio_codec_ctx);
            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
            
            LOGE("Failed to copy audio stream codec parameters to audio decoder context");
            
            return -7;
        }
        
        audio_codec_ctx->refcounted_frames = 1;
        err = avcodec_open2(audio_codec_ctx, audio_dec, NULL);
        if (err < 0) {
            avcodec_free_context(&audio_codec_ctx);
            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
            
            LOGE("Failed to open audio decoder");
            
            return -8;
        }
        
        audio_dec_ctx = audio_codec_ctx;
    }
    
    char afiltersDesc[256];
    afiltersDesc[0] = 0;
    if (mPlayRate>=0.5f && mPlayRate<=2.0f) {
        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,volume=%f", mPlayRate, mVolume);
//        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f", mPlayRate);
    }else if(mPlayRate<0.5f){
        float atempo2 = 0.0f;
        atempo2 = mPlayRate/0.5f;
        if (atempo2<0.5f) {
            atempo2 = 0.5f;
        }
        
        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 0.5f, atempo2, mVolume);
//        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f", 0.5f, atempo2);
    }else if (mPlayRate>2.0f) {
        float atempo2 = 0.0f;
        atempo2 = mPlayRate/2.0f;
        if (atempo2>2.0f) {
            atempo2 = 2.0f;
        }
        
        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 2.0f, atempo2, mVolume);
//        av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f", 2.0f, atempo2);
    }
    isNeedUpdateAudioFilter = false;
    int ret = open_audio_filter(audio_dec_ctx, mAudioRenderConfigure, afiltersDesc);
    if (ret<0) {
        if (ifmt_ctx) {
            if (audio_dec_ctx) {
                avcodec_close(audio_dec_ctx);
                avcodec_free_context(&audio_dec_ctx);
                audio_dec_ctx = NULL;
            }
            
            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
        }
        
        LOGE("Open Audio Filter Fail");
        return -9;
    }
    
    pthread_mutex_lock(&mAVAudioFifoLock);
    filteredAudioDataFifo = av_audio_fifo_alloc(mAudioRenderConfigure.sampleFormat, mAudioRenderConfigure.channelCount, 1);
    pthread_mutex_unlock(&mAVAudioFifoLock);
    
    if (mAudioPlayerType==AUDIO_PLAYER_INTERNAL_RENDER || mAudioPlayerType==AUDIO_PLAYER_INTERNAL_RENDER_EXTERNAL_MIX) {
#ifdef ANDROID
        //AUDIO_RENDER_AUDIOTRACK or AUDIO_RENDER_OPENSLES
        if (mAudioRenderType==AUDIO_RENDER_AUDIOTRACK) {
            mAudioRender = AudioRender::CreateAudioRenderWithJniEnv(AUDIO_RENDER_AUDIOTRACK, mJvm);
        }else{
            mAudioRender = AudioRender::CreateAudioRender(mAudioRenderType);
        }
#else
        mAudioRender = AudioRender::CreateAudioRender(mAudioRenderType);
#endif
        
        if (mAudioRender==NULL) {
            pthread_mutex_lock(&mAVAudioFifoLock);
            if (filteredAudioDataFifo) {
                av_audio_fifo_free(filteredAudioDataFifo);
                filteredAudioDataFifo = NULL;
            }
            pthread_mutex_unlock(&mAVAudioFifoLock);
            
            close_audio_filter();
            
            if (ifmt_ctx) {
                if (audio_dec_ctx) {
                    avcodec_close(audio_dec_ctx);
                    avcodec_free_context(&audio_dec_ctx);
                    audio_dec_ctx = NULL;
                }
                
                avformat_close_input(&ifmt_ctx);
                avformat_free_context(ifmt_ctx);
                ifmt_ctx = NULL;
            }
            LOGE("Create AudioRender Fail");
            return -10;
        }
        
        mAudioRender->configureAdaptation(mAudioRenderConfigure.sampleRate, mAudioRenderConfigure.channelCount);
        mAudioRender->setPCMDataInputer(this);
        int result = mAudioRender->init(PLAYER_MODE);
        if (result!=0) {
            AudioRender::DeleteAudioRender(mAudioRender, mAudioRenderType);
            mAudioRender = NULL;
            
            pthread_mutex_lock(&mAVAudioFifoLock);
            if (filteredAudioDataFifo) {
                av_audio_fifo_free(filteredAudioDataFifo);
                filteredAudioDataFifo = NULL;
            }
            pthread_mutex_unlock(&mAVAudioFifoLock);
            
            close_audio_filter();
            
            if (ifmt_ctx) {
                if (audio_dec_ctx) {
                    avcodec_close(audio_dec_ctx);
                    avcodec_free_context(&audio_dec_ctx);
                    audio_dec_ctx = NULL;
                }
                
                avformat_close_input(&ifmt_ctx);
                avformat_free_context(ifmt_ctx);
                ifmt_ctx = NULL;
            }
            
            LOGE("Init AudioRender Fail");
            return -11;
        }
        
        if (mAudioPlayerType==AUDIO_PLAYER_INTERNAL_RENDER_EXTERNAL_MIX) {
            pthread_mutex_lock(&mOutputAVAudioFifoLock);
            av_audio_fifo_reset(mOutputAVAudioFifo);
            pthread_mutex_unlock(&mOutputAVAudioFifoLock);
        }
    }
    
    return 0;
}

void AudioPlayer::close_all_pipelines_l()
{
    if (mAudioPlayerType==AUDIO_PLAYER_INTERNAL_RENDER || mAudioPlayerType==AUDIO_PLAYER_INTERNAL_RENDER_EXTERNAL_MIX) {
        if (mAudioRender) {
            mAudioRender->terminate();
            AudioRender::DeleteAudioRender(mAudioRender, mAudioRenderType);
            mAudioRender = NULL;
        }
    }
    
    pthread_mutex_lock(&mAVAudioFifoLock);
    if (filteredAudioDataFifo) {
        av_audio_fifo_free(filteredAudioDataFifo);
        filteredAudioDataFifo = NULL;
    }
    pthread_mutex_unlock(&mAVAudioFifoLock);
    
    close_audio_filter();

    if (ifmt_ctx) {
        if (audio_dec_ctx) {
            avcodec_close(audio_dec_ctx);
            avcodec_free_context(&audio_dec_ctx);
            audio_dec_ctx = NULL;
        }
        
        avformat_close_input(&ifmt_ctx);
        avformat_free_context(ifmt_ctx);
        ifmt_ctx = NULL;
    }
}

int AudioPlayer::open_audio_filter(AVCodecContext *audio_dec_ctx, AudioRenderConfigure audioRenderConfigure, const char *filter_spec)
{
    uint64_t audio_dec_ctx_channel_layout = audio_dec_ctx->channel_layout;
    int audio_dec_ctx_channels = audio_dec_ctx->channels;
    int audio_dec_ctx_sample_rate = audio_dec_ctx->sample_rate;
    enum AVSampleFormat audio_dec_ctx_sample_fmt = audio_dec_ctx->sample_fmt;
	AVRational audio_dec_ctx_time_base;
	audio_dec_ctx_time_base.num = 1;
	audio_dec_ctx_time_base.den = audio_dec_ctx->sample_rate;
//    AVRational audio_dec_ctx_time_base = (AVRational){1, audio_dec_ctx->sample_rate};
    
    char args[512];
    int ret = 0;
    const AVFilter *buffersrc = NULL;
    const AVFilter *buffersink = NULL;
    AVFilterContext *buffersrc_ctx = NULL;
    AVFilterContext *buffersink_ctx = NULL;
    AVFilterInOut *outputs = avfilter_inout_alloc();
    AVFilterInOut *inputs  = avfilter_inout_alloc();
    AVFilterGraph *filter_graph = avfilter_graph_alloc();
    
    if (!outputs || !inputs || !filter_graph) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        ret = AVERROR(ENOMEM);
        LOGE("No memory space for avfilter alloc");
        return ret;
    }
    
    buffersrc = avfilter_get_by_name("abuffer");
    buffersink = avfilter_get_by_name("abuffersink");
    
    if (!buffersrc || !buffersink) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("filtering source or sink element not found");
        ret = AVERROR_UNKNOWN;
        return ret;
    }
    
    if (!audio_dec_ctx_channel_layout)
    {
        audio_dec_ctx_channel_layout = av_get_default_channel_layout(audio_dec_ctx_channels);
    }
    
    snprintf(args, sizeof(args),
             "time_base=%d/%d:sample_rate=%d:sample_fmt=%s:channel_layout=0x%" PRIx64,
             audio_dec_ctx_time_base.num, audio_dec_ctx_time_base.den, audio_dec_ctx_sample_rate,
             av_get_sample_fmt_name(audio_dec_ctx_sample_fmt),
             audio_dec_ctx_channel_layout);
    
    ret = avfilter_graph_create_filter(&buffersrc_ctx, buffersrc, "in",
                                       args, NULL, filter_graph);
    
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot create audio buffer source");
        return ret;
    }
    
    ret = avfilter_graph_create_filter(&buffersink_ctx, buffersink, "out",
                                       NULL, NULL, filter_graph);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot create audio buffer sink");
        return ret;
    }
    
    ret = av_opt_set_bin(buffersink_ctx, "sample_fmts",
                         (uint8_t*)&audioRenderConfigure.sampleFormat, sizeof(audioRenderConfigure.sampleFormat),
                         AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot set output sample format");
        return ret;
    }
    
    ret = av_opt_set_bin(buffersink_ctx, "channel_layouts",
                         (uint8_t*)&audioRenderConfigure.channelLayout,
                         sizeof(audioRenderConfigure.channelLayout), AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot set output channel layout");
        return ret;
    }
    
    ret = av_opt_set_bin(buffersink_ctx, "sample_rates",
                         (uint8_t*)&audioRenderConfigure.sampleRate, sizeof(audioRenderConfigure.sampleRate),
                         AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot set output sample rate");
        return ret;
    }
    
    /* Endpoints for the filter graph. */
    outputs->name       = av_strdup("in");
    outputs->filter_ctx = buffersrc_ctx;
    outputs->pad_idx    = 0;
    outputs->next       = NULL;
    
    inputs->name       = av_strdup("out");
    inputs->filter_ctx = buffersink_ctx;
    inputs->pad_idx    = 0;
    inputs->next       = NULL;
    
    if (!outputs->name || !inputs->name) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("No memory space");
        ret = AVERROR(ENOMEM);
        return ret;
    }
    
    if ((ret = avfilter_graph_parse_ptr(filter_graph, filter_spec,
                                        &inputs, &outputs, NULL)) < 0)
    {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("avfilter_graph_parse_ptr fail");
        return ret;
    }
    
    if ((ret = avfilter_graph_config(filter_graph, NULL)) < 0)
    {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("avfilter_graph_config fail");
        return ret;
    }
    
    /* Fill FilteringContext */
    audio_buffersrc_ctx = buffersrc_ctx;
    audio_buffersink_ctx = buffersink_ctx;
    audio_filter_graph = filter_graph;
    
    avfilter_inout_free(&inputs);
    avfilter_inout_free(&outputs);
    
    return 0;
}

void AudioPlayer::close_audio_filter()
{
    if (audio_filter_graph) {
        avfilter_graph_free(&audio_filter_graph);
        audio_filter_graph = NULL;
    }
    
    audio_buffersrc_ctx = NULL;
    audio_buffersink_ctx = NULL;
}

// AVERROR_EXIT : EXIT
// 1 : JUST LOOP
// 0 : WAIT THEN LOOP
// -1 : DEMUXER READ FAIL
// -2 : NO MEMORY SPACE
// -3 : AUDIO FILTER INPUT FAIL
// -4 : AUDIO FILTER OUTPUT FAIL
// -5 : UPDATE AUDIO FILTER FAIL
int AudioPlayer::flowing_l()
{
    if (mAudioPlayerType==AUDIO_PLAYER_EXTERNAL_RENDER)
    {
        while (true) {
            if (mAudioPlayerOutputQueue.count()>=MAX_AUDIO_PLAYER_OUTPUT_QUEUE_COUNT) {
                break;
            }
            pthread_mutex_lock(&mAVAudioFifoLock);
            int available_samples = av_audio_fifo_size(filteredAudioDataFifo);
            if (available_samples>=mAudioRenderConfigure.samplesPerFrame) {
                AudioPlayerOutputPcm* audioPlayerOutputPcm = new AudioPlayerOutputPcm;
                audioPlayerOutputPcm->size = mAudioRenderConfigure.samplesPerFrame * mAudioRenderConfigure.channelCount * av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat);
                audioPlayerOutputPcm->data = (uint8_t*)malloc(audioPlayerOutputPcm->size);
                memset(audioPlayerOutputPcm->data, 0, audioPlayerOutputPcm->size);
                uint8_t **pData = &(audioPlayerOutputPcm->data);
                av_audio_fifo_read(filteredAudioDataFifo, (void**)pData, mAudioRenderConfigure.samplesPerFrame);
                pthread_mutex_unlock(&mAVAudioFifoLock);
                
                AudioPlayerOutput* audioPlayerOutput = new AudioPlayerOutput;
                audioPlayerOutput->type = AUDIO_PLAYER_OUTPUT_PCM;
                audioPlayerOutput->output = audioPlayerOutputPcm;
                
                mAudioPlayerOutputQueue.push(audioPlayerOutput);
                continue;
            }else{
                if (mFlags & COMPLETED) {
                    if (available_samples>0) {
                        AudioPlayerOutputPcm* audioPlayerOutputPcm = new AudioPlayerOutputPcm;
                        audioPlayerOutputPcm->size = mAudioRenderConfigure.samplesPerFrame * mAudioRenderConfigure.channelCount * av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat);
                        audioPlayerOutputPcm->data = (uint8_t*)malloc(audioPlayerOutputPcm->size);
                        memset(audioPlayerOutputPcm->data, 0, audioPlayerOutputPcm->size);
                        uint8_t **pData = &(audioPlayerOutputPcm->data);
                        av_audio_fifo_read(filteredAudioDataFifo, (void**)pData, mAudioRenderConfigure.samplesPerFrame);
                        pthread_mutex_unlock(&mAVAudioFifoLock);
                        
                        AudioPlayerOutput* audioPlayerOutput = new AudioPlayerOutput;
                        audioPlayerOutput->type = AUDIO_PLAYER_OUTPUT_PCM;
                        audioPlayerOutput->output = audioPlayerOutputPcm;
                        
                        mAudioPlayerOutputQueue.push(audioPlayerOutput);
                        break;
                    }else{
                        pthread_mutex_unlock(&mAVAudioFifoLock);
                        break;
                    }
                }else{
                    pthread_mutex_unlock(&mAVAudioFifoLock);
                    break;
                }
            }
        }
    }
    
    if (mFlags & SEEKING) {
        return 0;
    }
    
    pthread_mutex_lock(&mAVAudioFifoLock);
    int available_samples = av_audio_fifo_size(filteredAudioDataFifo);
    pthread_mutex_unlock(&mAVAudioFifoLock);
    
    if (!isPlayToEnd)
    {
        pthread_mutex_lock(&mPropertyLock);
        int64_t playTimeMs = (mCurrentAudioPtsUs - 1000000ll * available_samples/mAudioRenderConfigure.sampleRate)/1000ll;
        if (playTimeMs>mPlayTimeMs) {
            mPlayTimeMs = playTimeMs;
        }
        if (mPlayTimeMs<0) {
            mPlayTimeMs = 0;
        }
        if (mPlayTimeMs>mDurationMs) {
            mPlayTimeMs = mDurationMs;
        }
//        LOGD("PlayTimeMs : %d", mPlayTimeMs);
        pthread_mutex_unlock(&mPropertyLock);
    }
    
    if (mFlags & COMPLETED) {
        if (!isPlayToEnd) {
            if (available_samples<=0) {
                pthread_mutex_lock(&mPropertyLock);
                mPlayTimeMs = mDurationMs;
                pthread_mutex_unlock(&mPropertyLock);
                
                notifyListener_l(AUDIO_PLAYER_PLAYBACK_COMPLETE, 0, 0);
                
                if (mAudioPlayerType==AUDIO_PLAYER_EXTERNAL_RENDER)
                {
                    AudioPlayerOutput* audioPlayerOutput = new AudioPlayerOutput;
                    audioPlayerOutput->type = AUDIO_PLAYER_OUTPUT_EOF;
                    audioPlayerOutput->output = NULL;
                    
                    mAudioPlayerOutputQueue.push(audioPlayerOutput);
                }
                
                isPlayToEnd = true;
            }
        }
        
        return 0;
    }
    
    int bufferTimeMsFIFO = kBufferTimeMsFIFO;
    if (isLocalAudioFile) {
        bufferTimeMsFIFO = kBufferTimeMsFIFO;
    }else{
        bufferTimeMsFIFO = kBufferTimeMsFIFO * 10;
    }
    if (available_samples >= mAudioRenderConfigure.sampleRate * bufferTimeMsFIFO / 1000) {
        return 0;
    }
    
    if (isNeedUpdateAudioFilter) {
        isNeedUpdateAudioFilter = false;

        while (true) {
            //pull filtered audio frames from the filtergraph
            AVFrame *filt_frame = av_frame_alloc();
            if (!filt_frame) {
                LOGE("No Memory Space");
                break;
            }
            int ret = av_buffersink_get_frame(audio_buffersink_ctx, filt_frame);
            if (ret < 0) {
                av_frame_free(&filt_frame);
                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                    break;
                }else{
                    LOGE("Error while pulling filtered audio frames from the filtergraph");
                    break;
                }
            }
                            
            filt_frame->pict_type = AV_PICTURE_TYPE_NONE;
            
            pthread_mutex_lock(&mAVAudioFifoLock);
            ret = av_audio_fifo_write(filteredAudioDataFifo, (void **)filt_frame->data, filt_frame->nb_samples);
            pthread_mutex_unlock(&mAVAudioFifoLock);

            if (ret < filt_frame->nb_samples) {
                LOGE("No Memory Space");
                av_frame_free(&filt_frame);
                break;
            }
            calculateAudioFramePts(filt_frame);
            mCurrentAudioPtsUs = filt_frame->pts;
            av_frame_free(&filt_frame);
        }
        
        close_audio_filter();
        
        char afiltersDesc[256];
        afiltersDesc[0] = 0;
        if (mPlayRate>=0.5f && mPlayRate<=2.0f) {
            av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,volume=%f", mPlayRate, mVolume);
//            av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f", mPlayRate);
        }else if(mPlayRate<0.5f){
            float atempo2 = 0.0f;
            atempo2 = mPlayRate/0.5f;
            if (atempo2<0.5f) {
                atempo2 = 0.5f;
            }
            
            av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 0.5f, atempo2, mVolume);
//            av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f", 0.5f, atempo2);
        }else if (mPlayRate>2.0f) {
            float atempo2 = 0.0f;
            atempo2 = mPlayRate/2.0f;
            if (atempo2>2.0f) {
                atempo2 = 2.0f;
            }
            
            av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f,volume=%f", 2.0f, atempo2, mVolume);
//            av_strlcatf(afiltersDesc, sizeof(afiltersDesc), "atempo=%f,atempo=%f", 2.0f, atempo2);
        }
        int ret = open_audio_filter(audio_dec_ctx, mAudioRenderConfigure, afiltersDesc);
        if (ret<0) {
            LOGE("Update Audio Filter Fail");
            return -5;
        }
    }
    
    AVPacket packet;
    av_init_packet(&packet);
    packet.data = NULL;
    packet.size = 0;
    packet.flags = 0;
    
    int ret = av_read_frame(ifmt_ctx, &packet);
    if (ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN)) {
        av_packet_unref(&packet);
        
        return 0;
    }else if (ret == AVERROR_EOF) {
        av_packet_unref(&packet);
        
        LOGD("got eof packet");
        
        if (mIsLooping) {
            avformat_seek_file(ifmt_ctx, -1, INT64_MIN, ifmt_ctx->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
            return 1;
        }else{
            modifyFlags(COMPLETED, SET);
            return 1;
        }
    }else if (ret<0) {
        av_packet_unref(&packet);
        
        if (ret==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
            return AVERROR_EXIT;
        }else{
            LOGE("got error data, exit...");
            return -1;
        }
    }else{
        if (packet.stream_index==mAudioStreamIndex) {
            
            if(!mGotFirstAudioPacket)
            {
                mGotFirstAudioPacket = true;
                ifmt_ctx->start_time = packet.pts;
                ifmt_ctx->streams[mAudioStreamIndex]->start_time = packet.pts;
            }
            
            AVFrame *frame = av_frame_alloc();
            if (!frame) {
                av_packet_unref(&packet);
                
                LOGE("No Memory Space");
                return -2;
            }
            
            int got_frame;
            int ret = avcodec_decode_audio4(audio_dec_ctx, frame, &got_frame, &packet);
            av_packet_unref(&packet);
            
            if (ret < 0) {
                av_frame_free(&frame);
                
                LOGW("Audio Decode Fail");
                return 1;
            }
            
            if (got_frame) {
                //push the decoded audio frame into the filtergraph
                int ret = av_buffersrc_add_frame_flags(audio_buffersrc_ctx, frame, 0);
                if (ret<0) {
                    av_frame_free(&frame);

                    LOGE("Error while feeding decoded audio frame into the filtergraph");
                    return -3;
                }
                av_frame_free(&frame);
                
                while (true) {
                    //pull filtered audio frames from the filtergraph
                    AVFrame *filt_frame = av_frame_alloc();
                    if (!filt_frame) {
                        LOGE("No Memory Space");
                        
                        return -2;
                    }
                    
                    ret = av_buffersink_get_frame(audio_buffersink_ctx, filt_frame);
                    if (ret < 0) {
                        av_frame_free(&filt_frame);
                        
                        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                            //                    LOGW("no more audio frames for output");
                            return 1;
                        }else{
                            LOGE("Error while pulling filtered audio frames from the filtergraph");
                            return -4;
                        }
                    }
                    
                    filt_frame->pict_type = AV_PICTURE_TYPE_NONE;
                    
//                    if (mVolume!=1.0f) {
//                        int16_t* pcmSample = (int16_t*)filt_frame->data[0];
//                        for (int i=0; i<filt_frame->nb_samples * audio_dec_ctx->channels; i++) {
//                            pcmSample[i] = fixSample(pcmSample[i] * mVolume);
//                        }
//                    }
                    
                    int ret = 0;
                    pthread_mutex_lock(&mAVAudioFifoLock);
                    ret = av_audio_fifo_write(filteredAudioDataFifo, (void **)filt_frame->data, filt_frame->nb_samples);
                    pthread_mutex_unlock(&mAVAudioFifoLock);

                    if (ret < filt_frame->nb_samples) {
                        LOGE("No Memory Space");
                        
                        av_frame_free(&filt_frame);
                        
                        return -2;
                    }
                    calculateAudioFramePts(filt_frame);
                    mCurrentAudioPtsUs = filt_frame->pts;
                    
                    av_frame_free(&filt_frame);
                }
            } else {
                av_frame_free(&frame);
                return 1;
            }
            
        }else{
            av_packet_unref(&packet);
            
            return 1;
        }
    }
    
    return 0;
}

int AudioPlayer::drainPCMDataFromExternal(void **pData, int size)
{
    if (mAudioPlayerType==AUDIO_PLAYER_EXTERNAL_RENDER_SIMPLE)
    {
        while (true) {
            pthread_mutex_lock(&mLock);
            if (mFlags & COMPLETED) {
                pthread_mutex_unlock(&mLock);

                int ret = 0;
                pthread_mutex_lock(&mAVAudioFifoLock);
                if (filteredAudioDataFifo) {
                    ret = av_audio_fifo_read(filteredAudioDataFifo, pData, size/mAudioRenderConfigure.channelCount/av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat));
                }else{
                    ret = 0;
                }
                pthread_mutex_unlock(&mAVAudioFifoLock);

                return ret*mAudioRenderConfigure.channelCount*av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat);
            }else{
                pthread_mutex_unlock(&mLock);

                pthread_mutex_lock(&mAVAudioFifoLock);
                if (filteredAudioDataFifo==NULL) {
                    pthread_mutex_unlock(&mAVAudioFifoLock);
                    return 0;
                }
                if (av_audio_fifo_size(filteredAudioDataFifo)*mAudioRenderConfigure.channelCount*av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat)>=size) {
                    int ret = av_audio_fifo_read(filteredAudioDataFifo, pData, size/mAudioRenderConfigure.channelCount/av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat));
                        pthread_mutex_unlock(&mAVAudioFifoLock);
                        return ret*mAudioRenderConfigure.channelCount*av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat);
                }else{
                    pthread_mutex_unlock(&mAVAudioFifoLock);
                    
                    pthread_mutex_lock(&mDrainLock);
                    int64_t reltime = 10 * 1000 * 1000ll;
                    struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
                    struct timeval t;
                    t.tv_sec = t.tv_usec = 0;
                    gettimeofday(&t, NULL);
                    ts.tv_sec = t.tv_sec;
                    ts.tv_nsec = t.tv_usec * 1000;
                    ts.tv_sec += reltime/1000000000;
                    ts.tv_nsec += reltime%1000000000;
                    ts.tv_sec += ts.tv_nsec / 1000000000;
                    ts.tv_nsec = ts.tv_nsec % 1000000000;
                    pthread_cond_timedwait(&mDrainCondition, &mDrainLock, &ts);
#else
                    ts.tv_sec  = reltime/1000000000;
                    ts.tv_nsec = reltime%1000000000;
                    pthread_cond_timedwait_relative_np(&mDrainCondition, &mDrainLock, &ts);
#endif
                    pthread_mutex_unlock(&mDrainLock);
                    continue;
                }
            }
        }
    }
    
    return 0;
}

AudioPlayerOutput* AudioPlayer::drainFromExternal()
{
    return mAudioPlayerOutputQueue.pop();
}

void AudioPlayer::mixWithExternal(uint8_t *externalInputPcmData, int externalInputPcmSize)
{
    if (externalInputPcmData==NULL || externalInputPcmSize<=0) return;
    if (mAudioPlayerType==AUDIO_PLAYER_INTERNAL_RENDER_EXTERNAL_MIX) {
        pthread_mutex_lock(&mOutputAVAudioFifoLock);
        if (av_audio_fifo_size(mOutputAVAudioFifo)>0) {
            if (externalInputPcmSize>mOneOutoutBufferSize) {
                if (mOneOutoutBuffer) {
                    free(mOneOutoutBuffer);
                    mOneOutoutBuffer = NULL;
                }
            }
            if (mOneOutoutBuffer==NULL) {
                mOneOutoutBuffer = (uint8_t*)malloc(externalInputPcmSize);
                mOneOutoutBufferSize = externalInputPcmSize;
            }
            int read = av_audio_fifo_read(mOutputAVAudioFifo, (void**)&mOneOutoutBuffer, externalInputPcmSize/mAudioRenderConfigure.channelCount/av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat));
            if (read>0) {
                mixSamples((int16_t *)externalInputPcmData, (int16_t *)mOneOutoutBuffer, read*mAudioRenderConfigure.channelCount);
            }
        }
        pthread_mutex_unlock(&mOutputAVAudioFifoLock);
    }
}

int AudioPlayer::onInputPCMData(uint8_t **pData, int size)
{
    pthread_mutex_lock(&mAVAudioFifoLock);
    int read = av_audio_fifo_read(filteredAudioDataFifo, (void**)pData, size/mAudioRenderConfigure.channelCount/av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat));
    pthread_mutex_unlock(&mAVAudioFifoLock);
    
    int ret = read*mAudioRenderConfigure.channelCount*av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat);
    
    if (mAudioPlayerType==AUDIO_PLAYER_INTERNAL_RENDER_EXTERNAL_MIX) {
        if (read>0) {
            pthread_mutex_lock(&mOutputAVAudioFifoLock);
            av_audio_fifo_write(mOutputAVAudioFifo, (void **)pData, read);
            pthread_mutex_unlock(&mOutputAVAudioFifoLock);
        }
    }else{
        int pcmDB = PCMUtils::getPcmDB(*pData, ret);
        if (pcmDB<0) {
            pcmDB = 0;
        }
        
        pthread_mutex_lock(&mPcmDBPropertyLock);
        mPcmDB = pcmDB;
        pthread_mutex_unlock(&mPcmDBPropertyLock);
    }
    
    return ret;
}

int32_t AudioPlayer::getPlayTimeMs()
{
    int32_t ret = 0;
    
    pthread_mutex_lock(&mPropertyLock);
    ret = mPlayTimeMs;
    pthread_mutex_unlock(&mPropertyLock);
    
    return ret;
}

int32_t AudioPlayer::getDurationMs()
{
    int32_t ret = 0;
    
    pthread_mutex_lock(&mPropertyLock);
    ret = mDurationMs;
    pthread_mutex_unlock(&mPropertyLock);
    
    return ret;
}

int AudioPlayer::getPcmDB()
{
    int ret = 0;
    pthread_mutex_lock(&mPcmDBPropertyLock);
    ret = mPcmDB;
    mPcmDB = 0;
    pthread_mutex_unlock(&mPcmDBPropertyLock);
    
    return ret;
}

void AudioPlayer::setVolume(float volume)
{
    AutoLock autoLock(&mLock);

    if (mVolume!=volume) {
        mVolume = volume;
        
        isNeedUpdateAudioFilter = true;
    }
}

void AudioPlayer::setPlayRate(float playrate)
{
    AutoLock autoLock(&mLock);
    if (mPlayRate!=playrate) {
        mPlayRate = playrate;

        isNeedUpdateAudioFilter = true;
    }
}

void AudioPlayer::setLooping(bool isLooping)
{
    AutoLock autoLock(&mLock);
    mIsLooping = isLooping;
}

void AudioPlayer::interrupt()
{
    pthread_mutex_lock(&mInterruptLock);
    isInterrupt = 1;
    pthread_mutex_unlock(&mInterruptLock);
}

int AudioPlayer::interruptCallback(void* opaque)
{
    AudioPlayer *thiz = (AudioPlayer *)opaque;
    return thiz->interruptCallbackMain();
}

int AudioPlayer::interruptCallbackMain()
{
    int ret = 0;
    pthread_mutex_lock(&mInterruptLock);
    ret = isInterrupt;
    pthread_mutex_unlock(&mInterruptLock);
    
    return ret;
}

void AudioPlayer::calculateAudioFramePts(AVFrame *audioFrame)
{
    int64_t currentPts = AV_NOPTS_VALUE;
    if(av_frame_get_best_effort_timestamp(audioFrame) != AV_NOPTS_VALUE)
    {
        currentPts = av_frame_get_best_effort_timestamp(audioFrame);
    }else if(audioFrame->pts!=AV_NOPTS_VALUE)
    {
        currentPts = audioFrame->pts;
    }else if(audioFrame->pkt_pts!=AV_NOPTS_VALUE)
    {
        currentPts = audioFrame->pkt_pts;
    }else if(audioFrame->pkt_dts!=AV_NOPTS_VALUE)
    {
        currentPts = audioFrame->pkt_dts;
    }
    
    audioFrame->pts = currentPts * AV_TIME_BASE * av_q2d(ifmt_ctx->streams[mAudioStreamIndex]->time_base);
}
