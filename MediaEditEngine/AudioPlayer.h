//
//  AudioPlayer.h
//  MediaStreamer
//
//  Created by Think on 2019/7/24.
//  Copyright © 2019年 Cell. All rights reserved.
//

//------------------------------------- Just For Local Audio File --------------------------------------------//

#ifndef AudioPlayer_h
#define AudioPlayer_h

#include <stdio.h>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
#include <libavutil/avstring.h>
}

#include "TimedEventQueue.h"
#include "IMediaListener.h"
#include "NotificationQueue.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include "AudioRender.h"

#define MAX_AUDIO_PLAYER_OUTPUT_QUEUE_COUNT 4

enum AUDIO_PLAYER_OUTPUT_TYPE {
    AUDIO_PLAYER_OUTPUT_UNKNOWN_TYPE = -1,
    AUDIO_PLAYER_OUTPUT_PCM = 0,
    AUDIO_PLAYER_OUTPUT_ERROR = 1,
    AUDIO_PLAYER_OUTPUT_EOF = 2,
};

struct AudioPlayerOutputPcm {
    uint8_t *data;
    int size;
    
    AudioPlayerOutputPcm()
    {
        data = NULL;
        size = 0;
    }
    
    inline void Free()
    {
        if(data)
        {
            free(data);
            data = NULL;
        }
        size = 0;
    }
};

struct AudioPlayerOutputError {
    int error_type;
    int error_code;
    
    AudioPlayerOutputError()
    {
        error_type = AUDIO_PLAYER_ERROR_UNKNOWN;
        error_code = 0;
    }
};

struct AudioPlayerOutput {
    AUDIO_PLAYER_OUTPUT_TYPE type;
    void *output;
    
    AudioPlayerOutput()
    {
        type = AUDIO_PLAYER_OUTPUT_UNKNOWN_TYPE;
        output = NULL;
    }
    
    inline void Free()
    {
        if(output)
        {
            if(type==AUDIO_PLAYER_OUTPUT_PCM)
            {
                AudioPlayerOutputPcm* audioPlayerOutputPcm = (AudioPlayerOutputPcm*)output;
                if(audioPlayerOutputPcm)
                {
                    audioPlayerOutputPcm->Free();
                    delete audioPlayerOutputPcm;
                    audioPlayerOutputPcm = NULL;
                }
            }else if(type==AUDIO_PLAYER_OUTPUT_ERROR)
            {
                AudioPlayerOutputError* audioPlayerOutputError = (AudioPlayerOutputError*)output;
                if(audioPlayerOutputError)
                {
                    delete audioPlayerOutputError;
                    audioPlayerOutputError = NULL;
                }
            }
            
            output = NULL;
        }
    }
};

class AudioPlayerOutputQueue {
public:
    AudioPlayerOutputQueue();
    ~AudioPlayerOutputQueue();
    
    void push(AudioPlayerOutput* audioPlayerOutput);
    AudioPlayerOutput* pop();
    
    void flush();
    
    int count();
private:
    pthread_mutex_t mLock;
    queue<AudioPlayerOutput*> mAudioPlayerOutputQueue;
    int mCount;
};

enum AUDIO_PLAYER_TYPE {
    AUDIO_PLAYER_INTERNAL_RENDER = 0,
    AUDIO_PLAYER_EXTERNAL_RENDER = 1,
    AUDIO_PLAYER_INTERNAL_RENDER_EXTERNAL_MIX = 2,
    AUDIO_PLAYER_EXTERNAL_RENDER_SIMPLE = 3,
};

class AudioPlayer : public IMediaListener, IPCMDataInputer{
public:
#ifdef ANDROID
    AudioPlayer(JavaVM *jvm, AUDIO_PLAYER_TYPE type, AudioRenderConfigure configure);
#else
    AudioPlayer(AUDIO_PLAYER_TYPE type, AudioRenderConfigure configure);
#endif
    ~AudioPlayer();

#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#else
    void setListener(void (*listener)(void*,int,int,int), void* arg);
#endif
    
    void setInternalListener(IMediaCallback* listener);
    
    void setDataSource(char* url);
    void setDataSourceWithId(char* url, int sourceId);
    
    void prepare();
    void prepareAsync();
    void prepareAsyncToPlay();
    void play();
    bool isPlaying();
    void pause();
    void stop();
    
    void seekTo(int32_t seekPosMs);
    
    int32_t getPlayTimeMs();
    int32_t getDurationMs();
    int getPcmDB();
    
    void setVolume(float volume);
    void setPlayRate(float playrate);
    
    void setLooping(bool isLooping);
    
    int drainPCMDataFromExternal(void **pData, int size);
    AudioPlayerOutput* drainFromExternal();
    
    void notify(int event, int ext1, int ext2);
    int onInputPCMData(uint8_t **pData, int size);
    
    void mixWithExternal(uint8_t *externalInputPcmData, int externalInputPcmSize);
private:
    enum {
        kBufferTimeMsFIFO = 200,
    };
private:
    enum {
        PREPARING            = 0x01,
        PREPARED             = 0x02,
        PLAYING              = 0x04,
        PAUSED               = 0x10,
        ERRORED                = 0x20,
        END                  = 0x40,
        INITIALIZED          = 0x80,
        ENDING               = 0x100,
        COMPLETED            = 0x200,
        SEEKING              = 0x1000,
    };
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
private:
    friend struct AudioPlayerEvent;
    
    AudioPlayer(const AudioPlayer &);
    AudioPlayer &operator=(const AudioPlayer &);
    
    pthread_mutex_t mLock;
    
    TimedEventQueue mQueue;
    
    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mPlayEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mNotifyEvent;
    TimedEventQueue::Event *mSeekToEvent;
    bool mPlayEventPending;
    
    void prepareAsync_l();
    void play_l();
    void pause_l();
    void stop_l();
    void seekTo_l(int32_t seekPosMs);

    void postPlayEvent_l(int64_t delayUs = -1);
    void postNotifyEvent_l();

    void onPrepareAsyncEvent();
    void onPlayEvent();
    void onStopEvent();
    void onNotifyEvent();
    void onSeekToEvent();

    void cancelPlayerEvents();
    
    pthread_cond_t mStopCondition;
    
    pthread_cond_t mPrepareCondition;

    NotificationQueue mNotificationQueue;
private:
    int open_all_pipelines_l();
    void close_all_pipelines_l();
    int flowing_l();
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    AUDIO_PLAYER_TYPE mAudioPlayerType;
    AudioRenderConfigure mAudioRenderConfigure;

    char* mPlayUrl;
    int mSourceId;
    IMediaListener* mMediaListener;
    IMediaCallback* mInternalMediaListener;
private:
    static void init_ffmpeg_env();
private:
    bool isAutoPlay;
private:
    AVFormatContext *ifmt_ctx;
    int mAudioStreamIndex;
    AVCodecContext *audio_dec_ctx;
private:
    //filter_spec = "anull";
    int open_audio_filter(AVCodecContext *audio_dec_ctx, AudioRenderConfigure audioRenderConfigure, const char *filter_spec);
    void close_audio_filter();
    
    AVFilterContext *audio_buffersrc_ctx;
    AVFilterContext *audio_buffersink_ctx;
    AVFilterGraph *audio_filter_graph;
    
    pthread_mutex_t mAVAudioFifoLock;
    AVAudioFifo* filteredAudioDataFifo;
private:
    AudioRenderType mAudioRenderType;
    AudioRender* mAudioRender;
private:
    int64_t mCurrentAudioPtsUs;
    pthread_mutex_t mPropertyLock;
    int32_t mDurationMs;
    int32_t mPlayTimeMs;
    pthread_mutex_t mPcmDBPropertyLock;
    int mPcmDB;
private:
    float mVolume;
    float mPlayRate;
    bool isNeedUpdateAudioFilter;
    
    bool mIsLooping;
private:
    int64_t mSeekPosUs;
private:
    static int interruptCallback(void* opaque);
    int interruptCallbackMain();
    int isInterrupt; // critical value
    pthread_mutex_t mInterruptLock;
    void interrupt();
private:
    AudioPlayerOutputQueue mAudioPlayerOutputQueue;
private:
    void calculateAudioFramePts(AVFrame *audioFrame);
    bool mGotFirstAudioPacket;
private:
    bool isPlayToEnd;
private:
    bool isLocalAudioFile;
private:
    pthread_mutex_t mOutputAVAudioFifoLock;
    AVAudioFifo* mOutputAVAudioFifo;
    uint8_t *mOneOutoutBuffer;
    int mOneOutoutBufferSize;
private:
    pthread_mutex_t mDrainLock;
    pthread_cond_t mDrainCondition;
};

#endif /* AudioPlayer_h */
