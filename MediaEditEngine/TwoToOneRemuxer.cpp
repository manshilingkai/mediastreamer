//
//  TwoToOneRemuxer.cpp
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/16.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "TwoToOneRemuxer.h"
#include "AutoLock.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#else
#include "NormalMediaListener.h"
#endif

#include "MediaLog.h"
#include "MediaFile.h"
#include "FFLog.h"

struct TwoToOneRemuxerEvent : public TimedEventQueue::Event {
    TwoToOneRemuxerEvent(
                                TwoToOneRemuxer *remuxer,
                                void (TwoToOneRemuxer::*method)())
    : mRemuxer(remuxer),
    mMethod(method) {
    }
    
protected:
    virtual ~TwoToOneRemuxerEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mRemuxer->*mMethod)();
    }
    
private:
    TwoToOneRemuxer *mRemuxer;
    void (TwoToOneRemuxer::*mMethod)();
    
    TwoToOneRemuxerEvent(const TwoToOneRemuxerEvent &);
    TwoToOneRemuxerEvent &operator=(const TwoToOneRemuxerEvent &);
};

#ifdef ANDROID
TwoToOneRemuxer::TwoToOneRemuxer(JavaVM *jvm, TTORVideoMaterial videoMaterial, TTORAudioMaterial audioMaterial, TTORProduct product)
{
    mJvm = jvm;

    init_ffmpeg_env();
    
    mVideoMaterial = videoMaterial;
    if (videoMaterial.url) {
        mVideoMaterial.url = strdup(videoMaterial.url);
    }
    
    mAudioMaterial = audioMaterial;
    if (audioMaterial.url) {
        mAudioMaterial.url = strdup(audioMaterial.url);
    }
    
    mProduct = product;
    if (product.url) {
        mProduct.url = strdup(product.url);
    }
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);
    
    mAsyncPrepareEvent = NULL;
    mRemuxEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    
    mAsyncPrepareEvent = new TwoToOneRemuxerEvent(this, &TwoToOneRemuxer::onPrepareAsyncEvent);
    mRemuxEvent = new TwoToOneRemuxerEvent(this, &TwoToOneRemuxer::onRemuxEvent);
    mStopEvent = new TwoToOneRemuxerEvent(this, &TwoToOneRemuxer::onStopEvent);
    mNotifyEvent = new TwoToOneRemuxerEvent(this, &TwoToOneRemuxer::onNotifyEvent);
    mRemuxEventPending = false;
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();

    pthread_cond_init(&mStopCondition, NULL);

    mMediaListener = NULL;

    gotError = false;
    
    mInputAVFormatContextForVideoMaterial = NULL;
    mInputVideoStreamIndex = -1;
    
    mInputAVFormatContextForAudioMaterial = NULL;
    mInputAudioStreamIndex = -1;
    
    ofmt_ctx = NULL;
    mOutputVideoStreamIndex = -1;
    mOutputAudioStreamIndex = -1;
    
    GotBaselineForWriteAudioPacket = false;
    baseLineForWriteAudioPacket = 0;
    mCurrentWriteAudioPacketPtsUs = 0;
    
    GotBaselineForWriteVideoPacket = false;
    baseLineForWriteVideoPacket = 0;
    mCurrentWriteVideoPacketPtsUs = 0;
    
    GotWriteVideoPacketEof = false;
}
#else
TwoToOneRemuxer::TwoToOneRemuxer(TTORVideoMaterial videoMaterial, TTORAudioMaterial audioMaterial, TTORProduct product)
{
    init_ffmpeg_env();

    mVideoMaterial = videoMaterial;
    if (videoMaterial.url) {
        mVideoMaterial.url = strdup(videoMaterial.url);
    }
    
    mAudioMaterial = audioMaterial;
    if (audioMaterial.url) {
        mAudioMaterial.url = strdup(audioMaterial.url);
    }
    
    mProduct = product;
    if (product.url) {
        mProduct.url = strdup(product.url);
    }
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);
    
    mAsyncPrepareEvent = NULL;
    mRemuxEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    
    mAsyncPrepareEvent = new TwoToOneRemuxerEvent(this, &TwoToOneRemuxer::onPrepareAsyncEvent);
    mRemuxEvent = new TwoToOneRemuxerEvent(this, &TwoToOneRemuxer::onRemuxEvent);
    mStopEvent = new TwoToOneRemuxerEvent(this, &TwoToOneRemuxer::onStopEvent);
    mNotifyEvent = new TwoToOneRemuxerEvent(this, &TwoToOneRemuxer::onNotifyEvent);
    mRemuxEventPending = false;
    
    mQueue.start();

    pthread_cond_init(&mStopCondition, NULL);

    mMediaListener = NULL;

    gotError = false;
    
    mInputAVFormatContextForVideoMaterial = NULL;
    mInputVideoStreamIndex = -1;
    
    mInputAVFormatContextForAudioMaterial = NULL;
    mInputAudioStreamIndex = -1;
    
    ofmt_ctx = NULL;
    mOutputVideoStreamIndex = -1;
    mOutputAudioStreamIndex = -1;
    
    GotBaselineForWriteAudioPacket = false;
    baseLineForWriteAudioPacket = 0;
    mCurrentWriteAudioPacketPtsUs = 0;
    
    GotBaselineForWriteVideoPacket = false;
    baseLineForWriteVideoPacket = 0;
    mCurrentWriteVideoPacketPtsUs = 0;
    
    GotWriteVideoPacketEof = false;
}
#endif

TwoToOneRemuxer::~TwoToOneRemuxer()
{
    stop(false);

    mQueue.stop(true);

    mNotificationQueue.flush();

    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }

    if (mRemuxEvent!=NULL) {
        delete mRemuxEvent;
        mRemuxEvent = NULL;
    }

    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    pthread_cond_destroy(&mStopCondition);
    
    pthread_mutex_destroy(&mLock);
            
#ifdef ANDROID
    JniMediaListener *jniListener = (JniMediaListener*)mMediaListener;
    if (jniListener!=NULL) {
        delete jniListener;
        jniListener = NULL;
    }
#else
    NormalMediaListener* normalMediaListener = (NormalMediaListener*)mMediaListener;
    if (normalMediaListener!=NULL) {
        delete normalMediaListener;
        normalMediaListener = NULL;
    }
#endif
            
    mMediaListener = NULL;

    if (mVideoMaterial.url) {
        free(mVideoMaterial.url);
        mVideoMaterial.url = NULL;
    }
    
    if (mAudioMaterial.url) {
        free(mAudioMaterial.url);
        mAudioMaterial.url = NULL;
    }
    
    if (mProduct.url) {
        free(mProduct.url);
        mProduct.url = NULL;
    }
}

#ifdef ANDROID
void TwoToOneRemuxer::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    modifyFlags(INITIALIZED, SET);
}
#else
void TwoToOneRemuxer::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new NormalMediaListener(listener,arg);
    
    modifyFlags(INITIALIZED, SET);
}
#endif

void TwoToOneRemuxer::notifyListener_l(int event, int ext1, int ext2)
{
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void TwoToOneRemuxer::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    postNotifyEvent_l();
}

void TwoToOneRemuxer::postNotifyEvent_l()
{
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void TwoToOneRemuxer::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
}

void TwoToOneRemuxer::start()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & CONNECTING) {
        LOGW("already connecting");
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    prepareAsync_l();
}

void TwoToOneRemuxer::prepareAsync_l()
{
    modifyFlags(END, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(CONNECTING, SET);
    
    gotError = false;
    
    mQueue.postEvent(mAsyncPrepareEvent);
}

void TwoToOneRemuxer::onPrepareAsyncEvent()
{
    AutoLock autoLock(&mLock);
    int ret = open_all_pipelines_l();
    
    if (ret>=0) {
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(CONNECTED, SET);
        
        play_l();
    }else{
        if (ret!=AVERROR_EXIT) {
            if (ret==-1) {
                notifyListener_l(TWO_TO_ONE_REMUXER_ERROR, TWO_TO_ONE_REMUXER_ERROR_OPEN_INPUT_VIDEO_MATERIAL_FAIL);
            }else if (ret==-2) {
                notifyListener_l(TWO_TO_ONE_REMUXER_ERROR, TWO_TO_ONE_REMUXER_ERROR_OPEN_INPUT_AUDIO_MATERIAL_FAIL);
            }else if (ret==-3) {
                notifyListener_l(TWO_TO_ONE_REMUXER_ERROR, TWO_TO_ONE_REMUXER_ERROR_OPEN_OUTPUT_PRODUCT_FAIL);
            }else {
                notifyListener_l(TWO_TO_ONE_REMUXER_ERROR, TWO_TO_ONE_REMUXER_ERROR_UNKNOWN);
            }
        }
        
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(ERROR, SET);
        gotError = true;
        stop_l();
    }
}

void TwoToOneRemuxer::pause()
{
    AutoLock autoLock(&mLock);
    return pause_l();
}

void TwoToOneRemuxer::pause_l()
{
    if (!(mFlags & STREAMING)) {
        return;
    }
    
    modifyFlags(STREAMING, CLEAR);
    
    mQueue.cancelEvent(mRemuxEvent->eventID());
    mRemuxEventPending = false;
    
    modifyFlags(PAUSED, SET);
    
    return;
}

void TwoToOneRemuxer::resume()
{
    AutoLock autoLock(&mLock);
    
    return play_l();
}

void TwoToOneRemuxer::play_l()
{
    if (mFlags & STREAMING) {
        LOGW("%s","TwoToOneRemuxer is streaming");
        return;
    }
    
    if (!(mFlags & CONNECTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    modifyFlags(CONNECTED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    postRemuxEvent_l();
    
    modifyFlags(STREAMING, SET);
    
    notifyListener_l(TWO_TO_ONE_REMUXER_STREAMING);
}

void TwoToOneRemuxer::cancelRemuxerEvents()
{
    mQueue.cancelEvent(mRemuxEvent->eventID());
    mRemuxEventPending = false;
    mQueue.cancelEvent(mNotifyEvent->eventID());
    mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
    mQueue.cancelEvent(mStopEvent->eventID());
}

void TwoToOneRemuxer::stop(bool isCancel)
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & END) {
        return;
    }
    
    if (mFlags & ENDING) {
        LOGW("already ending");
        
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    if (isCancel) {
        gotError = true;
    }
    
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
    
    if (isCancel) {
        if (MediaFile::isExist(mProduct.url)) {
            MediaFile::deleteFile(mProduct.url);
        }
    }
}

void TwoToOneRemuxer::stop_l()
{
    modifyFlags(ENDING, ASSIGN);
    
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void TwoToOneRemuxer::postRemuxEvent_l(int64_t delayUs)
{
    if (mRemuxEventPending) {
        return;
    }
    mRemuxEventPending = true;
    mQueue.postEventWithDelay(mRemuxEvent, delayUs < 0 ? 0 : delayUs);
}

void TwoToOneRemuxer::onStopEvent()
{
    AutoLock autoLock(&mLock);
    
    close_all_pipelines_l();
    
    cancelRemuxerEvents();
    mNotificationQueue.flush();
    
    modifyFlags(END, ASSIGN);
    
    if (!gotError) {
        notifyListener_l(TWO_TO_ONE_REMUXER_END);
    }else{
        if (MediaFile::isExist(mProduct.url)) {
            MediaFile::deleteFile(mProduct.url);
        }
    }
    gotError = false;
    
    mInputAVFormatContextForVideoMaterial = NULL;
    mInputVideoStreamIndex = -1;
    
    mInputAVFormatContextForAudioMaterial = NULL;
    mInputAudioStreamIndex = -1;
    
    ofmt_ctx = NULL;
    mOutputVideoStreamIndex = -1;
    mOutputAudioStreamIndex = -1;
    
    GotBaselineForWriteAudioPacket = false;
    baseLineForWriteAudioPacket = 0;
    mCurrentWriteAudioPacketPtsUs = 0;
    
    GotBaselineForWriteVideoPacket = false;
    baseLineForWriteVideoPacket = 0;
    mCurrentWriteVideoPacketPtsUs = 0;

    GotWriteVideoPacketEof = false;
    
    pthread_cond_broadcast(&mStopCondition);
}

void TwoToOneRemuxer::onRemuxEvent()
{
    AutoLock autoLock(&mLock);
    
    if (!mRemuxEventPending) {
        // The event has been cancelled in reset_l() but had already // been scheduled for execution at that time.
        return;
    }
    mRemuxEventPending = false;
    
    int ret = flowing_l();
    if (ret==1) {
        postRemuxEvent_l(0);
        return;
    }else if (ret==-7) {
        //Write file trailer
        av_write_trailer(ofmt_ctx);
        
        stop_l();
        return;
    }else if (ret<0) {
        modifyFlags(STREAMING, CLEAR);
        modifyFlags(ERROR, SET);
        
        if (ret!=AVERROR_EXIT) {
            if (ret==-1) {
                notifyListener_l(TWO_TO_ONE_REMUXER_ERROR, TWO_TO_ONE_REMUXER_ERROR_VIDEO_DEMUX_FAIL);
            }else if (ret==-2) {
                notifyListener_l(TWO_TO_ONE_REMUXER_ERROR, TWO_TO_ONE_REMUXER_ERROR_VIDEO_MUX_FAIL);
            }else if (ret==-4) {
                notifyListener_l(TWO_TO_ONE_REMUXER_ERROR, TWO_TO_ONE_REMUXER_ERROR_AUDIO_DEMUX_FAIL);
            }else if (ret==-5) {
                notifyListener_l(TWO_TO_ONE_REMUXER_ERROR, TWO_TO_ONE_REMUXER_ERROR_AUDIO_MUX_FAIL);
            }else {
                notifyListener_l(TWO_TO_ONE_REMUXER_ERROR, TWO_TO_ONE_REMUXER_ERROR_UNKNOWN);
            }
        }
        
        gotError = true;
        stop_l();
        
        return;
    }else {
        int64_t waitTimeUs = 10*1000;
        postRemuxEvent_l(waitTimeUs);
        return;
    }
}

void TwoToOneRemuxer::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

void TwoToOneRemuxer::init_ffmpeg_env()
{
    // init ffmpeg env
    av_register_all();
    avcodec_register_all();
    avfilter_register_all();
    
    FFLog::setLogLevel(AV_LOG_WARNING);
}

// -1 : Open Input Video Material Fail
// -2 : Open Input Audio Material Fail
// -3 : Open Output Material Fail
int TwoToOneRemuxer::open_all_pipelines_l()
{
    int ret = open_input_video_material();
    if (ret<0) {
        close_input_video_material();
        
        if (ret==AVERROR_EXIT) {
            return AVERROR_EXIT;
        }
        return -1;
    }
    
    ret = open_input_audio_material();
    if (ret<0) {
        close_input_audio_material();
        
        if (ret==AVERROR_EXIT) {
            return AVERROR_EXIT;
        }
        return -2;
    }
    
    ret = open_output_product();
    if (ret<0) {
        close_output_product();
        
        if (ret==AVERROR_EXIT) {
            return AVERROR_EXIT;
        }
        return -3;
    }

    return 0;
}

void TwoToOneRemuxer::close_all_pipelines_l()
{
    close_output_product();
    
    close_input_audio_material();
    close_input_video_material();
}

int TwoToOneRemuxer::open_input_video_material()
{
    int ret = 0;
    if ((ret = avformat_open_input(&mInputAVFormatContextForVideoMaterial, mVideoMaterial.url, 0, 0)) < 0) {
        mInputAVFormatContextForVideoMaterial = NULL;
        
        LOGE("Could not open input video material file '%s'", mVideoMaterial.url);
        return ret;
    }
    
    if ((ret = avformat_find_stream_info(mInputAVFormatContextForVideoMaterial, 0)) < 0) {
        avformat_close_input(&mInputAVFormatContextForVideoMaterial);
        mInputAVFormatContextForVideoMaterial = NULL;
        
        LOGE("Failed to retrieve stream information for input video material file");
        return ret;
    }
    
    mInputVideoStreamIndex = -1;
    for(int i= 0; i < mInputAVFormatContextForVideoMaterial->nb_streams; i++)
    {
        if (mInputAVFormatContextForVideoMaterial->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            //by default, use the first video stream, and discard others.
            if(mInputVideoStreamIndex == -1)
            {
                mInputVideoStreamIndex = i;
            }
            else
            {
                mInputAVFormatContextForVideoMaterial->streams[i]->discard = AVDISCARD_ALL;
            }
        }else{
            mInputAVFormatContextForVideoMaterial->streams[i]->discard = AVDISCARD_ALL;
        }
    }
    
    if (mInputVideoStreamIndex==-1) {
        avformat_close_input(&mInputAVFormatContextForVideoMaterial);
        mInputAVFormatContextForVideoMaterial = NULL;
        
        mInputVideoStreamIndex = -1;
        
        LOGE("No Video Stream");
        return AVERROR_STREAM_NOT_FOUND;
    }
    
    return 0;
}

void TwoToOneRemuxer::close_input_video_material()
{
    if (mInputAVFormatContextForVideoMaterial) {
        avformat_close_input(&mInputAVFormatContextForVideoMaterial);
        mInputAVFormatContextForVideoMaterial = NULL;
    }
    
    mInputVideoStreamIndex = -1;
}

int TwoToOneRemuxer::open_input_audio_material()
{
    int ret = 0;
    if ((ret = avformat_open_input(&mInputAVFormatContextForAudioMaterial, mAudioMaterial.url, 0, 0)) < 0) {
        mInputAVFormatContextForAudioMaterial = NULL;
        
        LOGE("Could not open input audio material file '%s'", mAudioMaterial.url);
        return ret;
    }
    
    if ((ret = avformat_find_stream_info(mInputAVFormatContextForAudioMaterial, 0)) < 0) {
        avformat_close_input(&mInputAVFormatContextForAudioMaterial);
        mInputAVFormatContextForAudioMaterial = NULL;
        
        LOGE("Failed to retrieve stream information for input audio material file");
        return ret;
    }
    
    mInputAudioStreamIndex = -1;
    for(int i= 0; i < mInputAVFormatContextForAudioMaterial->nb_streams; i++)
    {
        if (mInputAVFormatContextForAudioMaterial->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //by default, use the first video stream, and discard others.
            if(mInputAudioStreamIndex == -1)
            {
                mInputAudioStreamIndex = i;
            }
            else
            {
                mInputAVFormatContextForAudioMaterial->streams[i]->discard = AVDISCARD_ALL;
            }
        }else{
            mInputAVFormatContextForAudioMaterial->streams[i]->discard = AVDISCARD_ALL;
        }
    }
    
    if (mInputAudioStreamIndex==-1) {
        avformat_close_input(&mInputAVFormatContextForAudioMaterial);
        mInputAVFormatContextForAudioMaterial = NULL;
        
        mInputAudioStreamIndex = -1;
        
        LOGE("No Audio Stream");
        return AVERROR_STREAM_NOT_FOUND;
    }
    
    if (mAudioMaterial.startPosMs>=0 && mAudioMaterial.endPosMs>0 && mAudioMaterial.endPosMs - mAudioMaterial.startPosMs > 0) {
        if (mInputAVFormatContextForAudioMaterial->start_time==AV_NOPTS_VALUE) {
            mInputAVFormatContextForAudioMaterial->start_time = 0;
        }
        int seek_ret = avformat_seek_file(mInputAVFormatContextForAudioMaterial, -1, INT64_MIN, av_rescale(mAudioMaterial.startPosMs, AV_TIME_BASE, 1000) + mInputAVFormatContextForAudioMaterial->start_time, INT64_MAX, AVSEEK_FLAG_BACKWARD);
        if (seek_ret<0) {
            LOGE("Seek To %ld Ms Fail", mAudioMaterial.startPosMs);
            
            avformat_close_input(&mInputAVFormatContextForAudioMaterial);
            mInputAVFormatContextForAudioMaterial = NULL;
            
            mInputAudioStreamIndex = -1;
            return seek_ret;
        }
    }

    return 0;
}

void TwoToOneRemuxer::close_input_audio_material()
{
    if (mInputAVFormatContextForAudioMaterial) {
        avformat_close_input(&mInputAVFormatContextForAudioMaterial);
        mInputAVFormatContextForAudioMaterial = NULL;
    }
    
    mInputAudioStreamIndex = -1;
}

int TwoToOneRemuxer::open_output_product()
{
    int ret = -1;
    if (strstr(mProduct.url, ".m4a") || strstr(mProduct.url, ".M4A")) {
        ret = avformat_alloc_output_context2(&ofmt_ctx, NULL, "mp4", mProduct.url);
    }else{
        ret = avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, mProduct.url);
    }
    if (ret<0 || ofmt_ctx==NULL) {
        ofmt_ctx = NULL;
        
        LOGE("Could not create output context");
        return ret;
    }
    
    ofmt_ctx->oformat->flags |= AVFMT_TS_NONSTRICT;

    //Audio
    AVStream *in_audio_stream = mInputAVFormatContextForAudioMaterial->streams[mInputAudioStreamIndex];
    AVCodecParameters *in_audio_codecpar = in_audio_stream->codecpar;
    AVStream *out_audio_stream = avformat_new_stream(ofmt_ctx, NULL);
    
    mOutputAudioStreamIndex = 0;
    
    ret = avcodec_parameters_copy(out_audio_stream->codecpar, in_audio_codecpar);
    if (ret < 0) {
        if (ofmt_ctx) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
        }
        
        LOGE("Failed to copy audio codec parameters");
        return ret;
    }
    
    out_audio_stream->codecpar->codec_tag = 0;
    
    //Video
    AVStream *in_video_stream = mInputAVFormatContextForVideoMaterial->streams[mInputVideoStreamIndex];
    AVCodecParameters *in_video_codecpar = in_video_stream->codecpar;
    AVStream *out_video_stream = avformat_new_stream(ofmt_ctx, NULL);
    
    mOutputVideoStreamIndex = 1;
    
    ret = avcodec_parameters_copy(out_video_stream->codecpar, in_video_codecpar);
    if (ret < 0) {
        if (ofmt_ctx) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
        }
        
        LOGE("Failed to copy video codec parameters");
        return ret;
    }
    
    out_video_stream->codecpar->codec_tag = 0;
    
    AVDictionaryEntry *m = NULL;
    while((m=av_dict_get(in_video_stream->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
        if(strcmp(m->key, "rotate")) continue;
        else{
            int rotate = atoi(m->value);
            av_dict_set_int(&out_video_stream->metadata, "rotate", rotate, 0);
        }
    }

    // Audio+Video
    av_dump_format(ofmt_ctx, 0, mProduct.url, 1);

    if (!(ofmt_ctx->oformat->flags & AVFMT_NOFILE)) {
        ret = avio_open(&ofmt_ctx->pb, mProduct.url, AVIO_FLAG_WRITE);
        if (ret < 0) {
            if (ofmt_ctx) {
                avformat_free_context(ofmt_ctx);
                ofmt_ctx = NULL;
            }
            
            LOGE("Could not open output file '%s'", mProduct.url);
            return ret;
        }
    }
    
    ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        if (ofmt_ctx && ofmt_ctx->oformat && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
        {
            avio_close(ofmt_ctx->pb);
        }
        
        if (ofmt_ctx) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
        }
        
        LOGE("Error occurred when opening output file");
        return ret;
    }
    
    return 0;
}

void TwoToOneRemuxer::close_output_product()
{
    if (ofmt_ctx && ofmt_ctx->oformat && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
    {
        avio_close(ofmt_ctx->pb);
    }
    
    if (ofmt_ctx) {
        avformat_free_context(ofmt_ctx);
        ofmt_ctx = NULL;
    }
}

//  0 : Wait Then Loop
//  1 : Just Loop
// -1 : VIDEO DEMUX FAIL
// -2 : VIDEO MUX FAIL
// -4 : AUDIO DEMUX FAIL
// -5 : AUDIO MUX FAIL
// -7 : EOF
int TwoToOneRemuxer::flowing_l()
{
    AVPacket pkt;
    int ret = av_read_frame(mInputAVFormatContextForAudioMaterial, &pkt);
    if (ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN)) {
        av_packet_unref(&pkt);
        return 0;
    }else if (ret == AVERROR_EOF) {
        av_packet_unref(&pkt);

        if (!GotWriteVideoPacketEof) {
            int result = flowing_remux_video_l(0,true);
            if (result<0) {
                if (result==-7) {
                    GotWriteVideoPacketEof = true;
                }else{
                    return result;
                }
            }
        }
                
        return -7;
    }else if (ret < 0) {
        av_packet_unref(&pkt);
        
        if (ret==AVERROR_EXIT) {
            return AVERROR_EXIT;
        }
        
        return -4;
    }else {
        if (pkt.stream_index != mInputAudioStreamIndex) {
            av_packet_unref(&pkt);
            return 1;
        }else{
            AVPacket* workAudioPacket = &pkt;
            AVStream *in_audio_stream = mInputAVFormatContextForAudioMaterial->streams[mInputAudioStreamIndex];
            AVStream *out_audio_stream = ofmt_ctx->streams[mOutputAudioStreamIndex];
            
            int64_t real_audio_pts = av_rescale_q(workAudioPacket->pts - in_audio_stream->start_time, in_audio_stream->time_base, AV_TIME_BASE_Q);
            if (mAudioMaterial.startPosMs>=0 && mAudioMaterial.endPosMs>0 && mAudioMaterial.endPosMs - mAudioMaterial.startPosMs > 0) {
                if (real_audio_pts < mAudioMaterial.startPosMs * 1000) {
                    av_packet_unref(&pkt);
                    return 1;
                }
                if (real_audio_pts > mAudioMaterial.endPosMs * 1000) {
                    av_packet_unref(&pkt);
                    
                    if (!GotWriteVideoPacketEof) {
                        int result = flowing_remux_video_l(0,true);
                        if (result<0) {
                            if (result==-7) {
                                GotWriteVideoPacketEof = true;
                            }else{
                                return result;
                            }
                        }
                    }
                                        
                    return -7;
                }
            }
            
            if (!GotBaselineForWriteAudioPacket) {
                GotBaselineForWriteAudioPacket = true;
                baseLineForWriteAudioPacket = real_audio_pts;
            }
            
            mCurrentWriteAudioPacketPtsUs = real_audio_pts - baseLineForWriteAudioPacket;
            
            workAudioPacket->stream_index = mOutputAudioStreamIndex;
            workAudioPacket->pts = av_rescale_q_rnd(mCurrentWriteAudioPacketPtsUs, AV_TIME_BASE_Q, out_audio_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
            workAudioPacket->dts = av_rescale_q_rnd(mCurrentWriteAudioPacketPtsUs, AV_TIME_BASE_Q, out_audio_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
            workAudioPacket->duration = av_rescale_q_rnd(workAudioPacket->duration, in_audio_stream->time_base, out_audio_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
            if (workAudioPacket->duration<0) {
                workAudioPacket->duration = 0;
            }
            workAudioPacket->pos = -1;

            ret = av_interleaved_write_frame(ofmt_ctx, workAudioPacket);
            av_packet_unref(workAudioPacket);
            
            if (ret < 0) {
                LOGE("Got Error When Muxing Audio Packet");
                
                if (ret==AVERROR_EXIT) {
                    return AVERROR_EXIT;
                }
                
                return -5;
            }
            
            if (!GotWriteVideoPacketEof) {
                int result = flowing_remux_video_l(mCurrentWriteAudioPacketPtsUs);
                if (result<0) {
                    if (result==-7) {
                        GotWriteVideoPacketEof = true;
                    }else{
                        return result;
                    }
                }
            }
            
            return 1;
        }
    }
    
    return 0;
}

// -1 : VIDEO DEMUX FAIL
// -2 : VIDEO MUX FAIL
// -7 : VIDEO REMUX EOF
int TwoToOneRemuxer::flowing_remux_video_l(int64_t currentWriteAudioPacketPtsUs, bool isRemuxAll)
{
    while ((mCurrentWriteVideoPacketPtsUs <= currentWriteAudioPacketPtsUs) || isRemuxAll) {
        AVPacket pkt;
        int ret = av_read_frame(mInputAVFormatContextForVideoMaterial, &pkt);
        if (ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN)) {
            av_packet_unref(&pkt);
            continue;
        }else if (ret == AVERROR_EOF) {
            av_packet_unref(&pkt);
            
            return -7;
        }else if(ret<0){
            av_packet_unref(&pkt);
            
            return -1;
        }else if(pkt.stream_index!=mInputVideoStreamIndex){
            av_packet_unref(&pkt);
            continue;
        }
        
        AVPacket* workVideoPacket = &pkt;
        AVStream *in_video_stream = mInputAVFormatContextForVideoMaterial->streams[mInputVideoStreamIndex];
        AVStream *out_video_stream = ofmt_ctx->streams[mOutputVideoStreamIndex];
        workVideoPacket->stream_index = mOutputVideoStreamIndex;
        workVideoPacket->pts = av_rescale_q_rnd(workVideoPacket->pts, in_video_stream->time_base, out_video_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
        workVideoPacket->dts = av_rescale_q_rnd(workVideoPacket->dts, in_video_stream->time_base, out_video_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
//        workVideoPacket->duration = av_rescale_q(workVideoPacket->duration, in_video_stream->time_base, out_video_stream->time_base);
        workVideoPacket->duration = av_rescale_q_rnd(workVideoPacket->duration, in_video_stream->time_base, out_video_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
        if (workVideoPacket->duration<0) {
            workVideoPacket->duration = 0;
        }
        workVideoPacket->pos = -1;
        
        int64_t writeVideoPacketPts = workVideoPacket->pts*AV_TIME_BASE*av_q2d(ofmt_ctx->streams[mOutputVideoStreamIndex]->time_base);
        
        ret = av_interleaved_write_frame(ofmt_ctx, workVideoPacket);
        av_packet_unref(workVideoPacket);
        
        if (ret < 0) {
            LOGE("Got Error When Muxing Video Packet");
            return -2;
        }
        
        if (!GotBaselineForWriteVideoPacket) {
            GotBaselineForWriteVideoPacket = true;
            baseLineForWriteVideoPacket = writeVideoPacketPts;
        }
        
        mCurrentWriteVideoPacketPtsUs = writeVideoPacketPts - baseLineForWriteVideoPacket;
    }
    
    return 0;
}

