//
//  YPPDubbingProducer.m
//  MediaStreamer
//
//  Created by Think on 2019/7/22.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import "YPPDubbingProducer.h"
#include "MediaDubbingProducerWrapper.h"

@implementation YPPDubbingProducerOptions
- (instancetype) init
{
    self = [super init];
    if (self) {
        self.videoUrl = nil;
        self.bgmUrl = nil;
        self.bgmVolume = 1.0f;
        self.dubUrl = nil;
        self.dubVolume = 1.0f;
        self.productUrl = nil;
    }
    
    return self;
}
@end

@implementation YPPDubbingProducer
{
    MediaDubbingProducerWrapper *pMediaDubbingProducerWrapper;
    dispatch_queue_t dubbingNotificationQueue;
    int mTag;
}

- (instancetype) initWithTag:(int)tag
{
    self = [super init];
    if (self) {
        pMediaDubbingProducerWrapper = NULL;
        dubbingNotificationQueue = dispatch_queue_create("YPPDubbingProducerNotificationQueue", 0);
        mTag = tag;
    }
    
    return self;
}

void dubbingNotificationListener(void* owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak YPPDubbingProducer *thiz = (__bridge YPPDubbingProducer*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchDubbingNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchDubbingNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    __weak typeof(self) wself = self;
    dispatch_async(dubbingNotificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf handleDubbingNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    });
}

- (void)handleDubbingNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    switch (event) {
        case MEDIA_DUBBING_STREAMING:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onDubbingStart:)])) {
                    [self.delegate onDubbingStart:mTag];
                }
            }
            break;
            
        case MEDIA_DUBBING_END:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onDubbingEnd:)])) {
                    [self.delegate onDubbingEnd:mTag];
                }
            }
            break;
        case MEDIA_DUBBING_ERROR:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onDubbingErrorWithErrorType:Tag:)])) {
                    [self.delegate onDubbingErrorWithErrorType:ext1 Tag:mTag];
                }
            }
            break;
        case MEDIA_DUBBING_INFO:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onDubbingInfoWithInfoType:InfoValue:Tag:)])) {
                    [self.delegate onDubbingInfoWithInfoType:ext1 InfoValue:ext2 Tag:mTag];
                }
            }
            break;
            
        default:
            break;
    }
}

- (void)initialize:(YPPDubbingProducerOptions*)options
{
    char* videoUrl = NULL;
    if ([options videoUrl]) {
        videoUrl = (char*)[[options videoUrl] UTF8String];
    }
    
    char* bgmUrl = NULL;
    if ([options bgmUrl]) {
        bgmUrl = (char*)[[options bgmUrl] UTF8String];
    }
    float bgmVolume = [options bgmVolume];
    
    char* dubUrl = NULL;
    if ([options dubUrl]) {
        dubUrl = (char*)[[options dubUrl] UTF8String];
    }
    float dubVolume = [options dubVolume];
    
    char* productUrl = (char*)[[options productUrl] UTF8String];
    
    pMediaDubbingProducerWrapper = MediaDubbingProducer_GetInstance(videoUrl, bgmUrl, bgmVolume, dubUrl, dubVolume, productUrl);
    if (pMediaDubbingProducerWrapper) {
        MediaDubbingProducer_setListener(pMediaDubbingProducerWrapper, dubbingNotificationListener, (__bridge void*)self);
    }
}

- (void)start
{
    if (pMediaDubbingProducerWrapper) {
        MediaDubbingProducer_start(pMediaDubbingProducerWrapper);
    }
}

- (void)resume
{
    if (pMediaDubbingProducerWrapper) {
        MediaDubbingProducer_resume(pMediaDubbingProducerWrapper);
    }
}

- (void)pause
{
    if (pMediaDubbingProducerWrapper) {
        MediaDubbingProducer_pause(pMediaDubbingProducerWrapper);
    }
}

- (void)stop:(BOOL)isCancle
{
    if (pMediaDubbingProducerWrapper) {
        if (isCancle) {
            MediaDubbingProducer_stop(pMediaDubbingProducerWrapper,true);
        }else{
            MediaDubbingProducer_stop(pMediaDubbingProducerWrapper,false);
        }
    }
    
    dispatch_barrier_sync(dubbingNotificationQueue, ^{
        NSLog(@"finish all dubbing notifications");
    });
}

- (void)terminate
{
    if (pMediaDubbingProducerWrapper) {
        MediaDubbingProducer_ReleaseInstance(&pMediaDubbingProducerWrapper);
        pMediaDubbingProducerWrapper = NULL;
    }
    
    dispatch_barrier_sync(dubbingNotificationQueue, ^{
        NSLog(@"finish all dubbing notifications");
    });
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"YPPDubbingProducer dealloc");
}

@end
