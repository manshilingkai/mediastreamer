//
//  AudioRemuxerWrapper.h
//  MediaStreamer
//
//  Created by Think on 2019/10/31.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef AudioRemuxerWrapper_h
#define AudioRemuxerWrapper_h

#include <stdio.h>
#include "AudioRemuxerCommon.h"

struct AudioRemuxerWrapper;

#ifdef __cplusplus
extern "C" {
#endif

struct AudioRemuxerWrapper* AudioRemuxerWrapper_GetInstance(AudioRemuxerMaterialGroup inputMaterialGroup, AudioRemuxerProduct outputProduct);
void AudioRemuxerWrapper_ReleaseInstance(struct AudioRemuxerWrapper **ppInstance);

void AudioRemuxerWrapper_setListener(struct AudioRemuxerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner);

void AudioRemuxerWrapper_start(struct AudioRemuxerWrapper *pInstance);
void AudioRemuxerWrapper_resume(struct AudioRemuxerWrapper *pInstance);
void AudioRemuxerWrapper_pause(struct AudioRemuxerWrapper *pInstance);
void AudioRemuxerWrapper_stop(struct AudioRemuxerWrapper *pInstance, bool isCancel);

#ifdef __cplusplus
};
#endif

#endif /* AudioRemuxerWrapper_h */
