//
//  RadioProducerWrapper.cpp
//  MediaStreamer
//
//  Created by Think on 2019/10/23.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "RadioProducerWrapper.h"
#include "RadioProducer.h"

#ifdef __cplusplus
extern "C" {
#endif

struct RadioProducerWrapper
{
    RadioProducer* radioProducer;
    
    RadioProducerWrapper()
    {
        radioProducer = NULL;
    }
};

struct RadioProducerWrapper* RadioProducerWrapper_GetInstance(RadioProductOptions options)
{
    RadioProducerWrapper* pInstance = new RadioProducerWrapper;
    AudioRenderConfigure configure;
    pInstance->radioProducer = new RadioProducer(configure, options);
    return pInstance;
}

void RadioProducerWrapper_ReleaseInstance(struct RadioProducerWrapper **ppInstance)
{
    RadioProducerWrapper* pInstance  = *ppInstance;
    if (pInstance!=NULL) {
        if (pInstance->radioProducer!=NULL) {
            delete pInstance->radioProducer;
            pInstance->radioProducer = NULL;
        }
        
        delete pInstance;
        pInstance = NULL;
    }
}

void RadioProducerWrapper_setListener(struct RadioProducerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->setListener(listener, owner);
    }
}

void RadioProducerWrapper_prepareBgm(struct RadioProducerWrapper *pInstance, char* bgmUrl)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->prepareBgm(bgmUrl);
    }
}

void RadioProducerWrapper_prepareBgmWithStartPos(struct RadioProducerWrapper *pInstance, char* bgmUrl, int32_t startPosMs)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->prepareBgmWithStartPos(bgmUrl, startPosMs);
    }
}

void RadioProducerWrapper_startBgm(struct RadioProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->startBgm();
    }
}

void RadioProducerWrapper_pauseBgm(struct RadioProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->pauseBgm();
    }
}

void RadioProducerWrapper_stopBgm(struct RadioProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->stopBgm();
    }
}

void RadioProducerWrapper_setBgmVolume(struct RadioProducerWrapper *pInstance, float volume)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->setBgmVolume(volume);
    }
}

void RadioProducerWrapper_startMicrophone(struct RadioProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->startMicrophone();
    }
}

void RadioProducerWrapper_stopMicrophone(struct RadioProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->stopMicrophone();
    }
}

void RadioProducerWrapper_setMicrophoneVolume(struct RadioProducerWrapper *pInstance, float volume)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->setMicrophoneVolume(volume);
    }
}

void RadioProducerWrapper_prepareEffectMusic(struct RadioProducerWrapper *pInstance, char* effectMusicUrl)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->prepareEffectMusic(effectMusicUrl);
    }
}

void RadioProducerWrapper_startEffectMusic(struct RadioProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->startEffectMusic();
    }
}

void RadioProducerWrapper_pauseEffectMusic(struct RadioProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->pauseEffectMusic();
    }
}

void RadioProducerWrapper_stopEffectMusic(struct RadioProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->stopEffectMusic();
    }
}

void RadioProducerWrapper_setEffectMusicVolume(struct RadioProducerWrapper *pInstance, float volume)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->setEffectMusicVolume(volume);
    }
}

void RadioProducerWrapper_setAudioUserDefinedEffect(struct RadioProducerWrapper *pInstance, int effect)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->setAudioUserDefinedEffect(effect);
    }
}

void RadioProducerWrapper_setAudioEqualizerStyle(struct RadioProducerWrapper *pInstance, int style)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->setAudioEqualizerStyle(style);
    }
}

void RadioProducerWrapper_setAudioReverbStyle(struct RadioProducerWrapper *pInstance, int style)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->setAudioReverbStyle(style);
    }
}

void RadioProducerWrapper_setAudioPitchSemiTones(struct RadioProducerWrapper *pInstance, int value)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->setAudioPitchSemiTones(value);
    }
}

void RadioProducerWrapper_setEarReturn(struct RadioProducerWrapper *pInstance, bool isEnableEarReturn)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->setEarReturn(isEnableEarReturn);
    }
}

void RadioProducerWrapper_pause(struct RadioProducerWrapper *pInstance, bool isExport)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->pause(isExport);
    }
}

void RadioProducerWrapper_stop(struct RadioProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        pInstance->radioProducer->stop();
    }
}

int32_t RadioProducerWrapper_getCurrentTimeMs(struct RadioProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        return pInstance->radioProducer->getCurrentTimeMs();
    }
    
    return 0;
}

int32_t RadioProducerWrapper_getBgmPlayingTimeMs(struct RadioProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->radioProducer!=NULL) {
        return pInstance->radioProducer->getBgmPlayingTimeMs();
    }
    
    return 0;
}

#ifdef __cplusplus
};
#endif
