//
//  YPPTwoToOneRemuxer.m
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/18.
//  Copyright © 2020 bolome. All rights reserved.
//

#import "YPPTwoToOneRemuxer.h"
#include "TwoToOneRemuxerWrapper.h"

@implementation YPPTwoToOneRemuxerOptions
- (instancetype) init
{
    self = [super init];
    if (self) {
        self.inputVideoUrl = nil;
        self.inputAudioUrl = nil;
        self.startPosMsForInputAudioUrl = -1;
        self.endPosMsForInputAudioUrl = -1;
        self.outputUrl = nil;
    }
    
    return self;
}
@end

@implementation YPPTwoToOneRemuxer
{
    TwoToOneRemuxerWrapper* pTwoToOneRemuxerWrapper;
    dispatch_queue_t twoToOneRemuxerNotificationQueue;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pTwoToOneRemuxerWrapper = NULL;
        twoToOneRemuxerNotificationQueue = dispatch_queue_create("YPPTwoToOneRemuxerNotificationQueue", 0);
    }
    
    return self;
}

void twoToOneRemuxerNotificationListener(void* owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak YPPTwoToOneRemuxer *thiz = (__bridge YPPTwoToOneRemuxer*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchTwoToOneRemuxerNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchTwoToOneRemuxerNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    __weak typeof(self) wself = self;
    dispatch_async(twoToOneRemuxerNotificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf handleTwoToOneRemuxerNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    });
}

- (void)handleTwoToOneRemuxerNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    switch (event) {
        case TWO_TO_ONE_REMUXER_STREAMING:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onTwoToOneRemuxerStart)])) {
                    [self.delegate onTwoToOneRemuxerStart];
                }
            }
            break;
            
        case TWO_TO_ONE_REMUXER_END:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onTwoToOneRemuxerEnd)])) {
                    [self.delegate onTwoToOneRemuxerEnd];
                }
            }
            break;
        case TWO_TO_ONE_REMUXER_ERROR:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onTwoToOneRemuxerErrorWithErrorType:)])) {
                    [self.delegate onTwoToOneRemuxerErrorWithErrorType:ext1];
                }
            }
            break;
        case TWO_TO_ONE_REMUXER_INFO:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onTwoToOneRemuxerInfoWithInfoType:InfoValue:)])) {
                    [self.delegate onTwoToOneRemuxerInfoWithInfoType:ext1 InfoValue:ext2];
                }
            }
            break;
            
        default:
            break;
    }
}

- (void)initialize:(YPPTwoToOneRemuxerOptions*)options
{
    char* inputVideoUrl = (char*)[[options inputVideoUrl] UTF8String];
    char* inputAudioUrl = (char*)[[options inputAudioUrl] UTF8String];
    char* outputUrl = (char*)[[options outputUrl] UTF8String];
    
    TTORVideoMaterial videoMaterial;
    TTORAudioMaterial audioMaterial;
    TTORProduct product;
    
    videoMaterial.url = inputVideoUrl;
    audioMaterial.url = inputAudioUrl;
    audioMaterial.startPosMs = [options startPosMsForInputAudioUrl];
    audioMaterial.endPosMs = [options endPosMsForInputAudioUrl];
    product.url = outputUrl;

    pTwoToOneRemuxerWrapper = TwoToOneRemuxerWrapper_GetInstance(videoMaterial, audioMaterial, product);
    if (pTwoToOneRemuxerWrapper) {
        TwoToOneRemuxerWrapper_setListener(pTwoToOneRemuxerWrapper, twoToOneRemuxerNotificationListener, (__bridge void*)self);
    }
}

- (void)start
{
    if (pTwoToOneRemuxerWrapper) {
        TwoToOneRemuxerWrapper_start(pTwoToOneRemuxerWrapper);
    }
}

- (void)resume
{
    if (pTwoToOneRemuxerWrapper) {
        TwoToOneRemuxerWrapper_resume(pTwoToOneRemuxerWrapper);
    }
}

- (void)pause
{
    if (pTwoToOneRemuxerWrapper) {
        TwoToOneRemuxerWrapper_pause(pTwoToOneRemuxerWrapper);
    }
}

- (void)stop:(BOOL)isCancel
{
    if (pTwoToOneRemuxerWrapper) {
        if (isCancel) {
            TwoToOneRemuxerWrapper_stop(pTwoToOneRemuxerWrapper,true);
        }else{
            TwoToOneRemuxerWrapper_stop(pTwoToOneRemuxerWrapper,false);
        }
    }
    
    dispatch_barrier_sync(twoToOneRemuxerNotificationQueue, ^{
        NSLog(@"finish all TwoToOne remuxer notifications");
    });
}

- (void)terminate
{
    if (pTwoToOneRemuxerWrapper) {
        TwoToOneRemuxerWrapper_ReleaseInstance(&pTwoToOneRemuxerWrapper);
        pTwoToOneRemuxerWrapper = NULL;
    }
    
    dispatch_barrier_sync(twoToOneRemuxerNotificationQueue, ^{
        NSLog(@"finish all TwoToOne remuxer notifications");
    });
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"YPPTwoToOneRemuxer dealloc");
}

@end

