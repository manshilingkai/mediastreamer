//
//  AudioPlayerProtocol.h
//  MediaStreamer
//
//  Created by Think on 2019/11/15.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AudioPlayerCommon.h"
#import "AudioPlayerDelegate.h"

@protocol AudioPlayerProtocol <NSObject>

- (void)initialize;

- (void)setDataSource:(NSURL*)url;
- (void)setDataSourceWithUrl:(NSString*)url;

- (void)prepare;
- (void)prepareAsync;
- (void)prepareAsyncToPlay;
- (void)play;
- (BOOL)isPlaying;
- (void)pause;
- (void)stop;
- (void)seekTo:(NSTimeInterval)seekPosMs;

- (void)setVolume:(NSTimeInterval)volume;
- (void)setPlayRate:(NSTimeInterval)playrate;
- (void)setLooping:(BOOL)isLooping;

- (void)terminate;

@property (nonatomic, weak) id<AudioPlayerDelegate> delegate;

@property (nonatomic, readonly) NSTimeInterval durationMs;
@property (nonatomic, readonly) NSTimeInterval currentPlaybackTimeMs;
@property (nonatomic, readonly) NSInteger pcmDB; //40DB-80DB

@end
