//
//  MicrophoneAudioRecorderWrapper.h
//  MediaStreamer
//
//  Created by Think on 2019/7/13.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef MicrophoneAudioRecorderWrapper_h
#define MicrophoneAudioRecorderWrapper_h

#include <stdio.h>
#include <stdint.h>

struct MicrophoneAudioRecorderWrapper;

#ifdef __cplusplus
extern "C" {
#endif
    
    struct MicrophoneAudioRecorderWrapper *MicrophoneAudioRecorder_GetInstance(int sampleRate, int numChannels, char* workDir);
    void MicrophoneAudioRecorder_ReleaseInstance(struct MicrophoneAudioRecorderWrapper **ppInstance);
    
    bool MicrophoneAudioRecorder_initialize(struct MicrophoneAudioRecorderWrapper *pInstance);
    
    bool MicrophoneAudioRecorder_startRecord(struct MicrophoneAudioRecorderWrapper *pInstance);
    bool MicrophoneAudioRecorder_startRecordWithStartPos(struct MicrophoneAudioRecorderWrapper *pInstance, int64_t startPositionUs);
    void MicrophoneAudioRecorder_stopRecord(struct MicrophoneAudioRecorderWrapper *pInstance);
    int64_t MicrophoneAudioRecorder_getRecordTimeUs(struct MicrophoneAudioRecorderWrapper *pInstance);
    int MicrophoneAudioRecorder_getRecordPcmDB(struct MicrophoneAudioRecorderWrapper *pInstance);
    void MicrophoneAudioRecorder_setEarReturn(struct MicrophoneAudioRecorderWrapper *pInstance, bool isEnableEarReturn);
    void MicrophoneAudioRecorder_enableNoiseSuppression(struct MicrophoneAudioRecorderWrapper *pInstance, bool isEnableNoiseSuppression);

    bool MicrophoneAudioRecorder_openAudioPlayer(struct MicrophoneAudioRecorderWrapper *pInstance);
    bool MicrophoneAudioRecorder_startAudioPlay(struct MicrophoneAudioRecorderWrapper *pInstance);
    bool MicrophoneAudioRecorder_seekAudioPlay(struct MicrophoneAudioRecorderWrapper *pInstance, int64_t seekTimeUs);
    void MicrophoneAudioRecorder_pauseAudioPlay(struct MicrophoneAudioRecorderWrapper *pInstance);
    void MicrophoneAudioRecorder_closeAudioPlayer(struct MicrophoneAudioRecorderWrapper *pInstance);
    int64_t MicrophoneAudioRecorder_getPlayTimeUs(struct MicrophoneAudioRecorderWrapper *pInstance);
    int64_t MicrophoneAudioRecorder_getPlayDurationUs(struct MicrophoneAudioRecorderWrapper *pInstance);
    int MicrophoneAudioRecorder_getPlayPcmDB(struct MicrophoneAudioRecorderWrapper *pInstance);
    
    bool MicrophoneAudioRecorder_reverseOrderGeneration(struct MicrophoneAudioRecorderWrapper *pInstance);

    bool MicrophoneAudioRecorder_backDelete(struct MicrophoneAudioRecorderWrapper *pInstance, int64_t keepTimeUs);
    bool MicrophoneAudioRecorder_convertToWav(struct MicrophoneAudioRecorderWrapper *pInstance, char *wavFilePath);
    bool MicrophoneAudioRecorder_convertToWavWithOffset(struct MicrophoneAudioRecorderWrapper *pInstance, char *wavFilePath, long offsetMs);

    void MicrophoneAudioRecorder_terminate(struct MicrophoneAudioRecorderWrapper *pInstance);

    bool NoiseSuppressionWav(char* inputWavFilePath, char* outputWavFilePath);
    bool OffsetWav(char* inputWavFilePath, char* outputWavFilePath, long offsetMs);
    bool EffectWav(char* inputWavFilePath, char* outputWavFilePath, int effectType, int effect);
    
#ifdef __cplusplus
};
#endif

#endif /* MicrophoneAudioRecorderWrapper_h */
