//
//  MediaDubbingProducerWrapper.h
//  MediaStreamer
//
//  Created by Think on 2019/7/22.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef MediaDubbingProducerWrapper_h
#define MediaDubbingProducerWrapper_h

#include <stdio.h>

struct MediaDubbingProducerWrapper;

#ifdef __cplusplus
extern "C" {
#endif
    struct MediaDubbingProducerWrapper *MediaDubbingProducer_GetInstance(char* videoUrl, char* bgmUrl, float bgmVolume, char* dubUrl, float dubVolume, char* productUrl);
    void MediaDubbingProducer_ReleaseInstance(struct MediaDubbingProducerWrapper **ppInstance);

    void MediaDubbingProducer_setListener(struct MediaDubbingProducerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner);
    
    void MediaDubbingProducer_start(struct MediaDubbingProducerWrapper *pInstance);
    void MediaDubbingProducer_resume(struct MediaDubbingProducerWrapper *pInstance);
    void MediaDubbingProducer_pause(struct MediaDubbingProducerWrapper *pInstance);
    void MediaDubbingProducer_stop(struct MediaDubbingProducerWrapper *pInstance, bool isCancle);
#ifdef __cplusplus
};
#endif
    
#endif /* MediaDubbingProducerWrapper_h */
