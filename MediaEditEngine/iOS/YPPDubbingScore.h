//
//  YPPDubbingScore.h
//  MediaStreamer
//
//  Created by Think on 2019/9/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

enum dubbing_score_event_type {
    DUBBING_SCORE_PROCESSING = 0,
    DUBBING_SCORE_ERROR = 1,
    DUBBING_SCORE_INFO = 2,
    DUBBING_SCORE_END = 3,
};

enum dubbing_score_info_type {
    DUBBING_SCORE_INFO_PROCESS_TIMESTAMP = 1,
    DUBBING_SCORE_INFO_PROCESS_PERCENT = 2,
};

enum dubbing_score_error_type {
    DUBBING_SCORE_ERROR_UNKNOWN = -1,
    DUBBING_SCORE_ERROR_ORIGIN_ERROR = 0,
    DUBBING_SCORE_ERROR_BGM_ERROR = 1,
    DUBBING_SCORE_ERROR_OPEN_DUB_FAIL = 2,
    DUBBING_SCORE_ERROR_OPEN_OUTPUT_PRODUCT_FAIL = 3,
};

@protocol YPPDubbingScoreDelegate <NSObject>
@required
- (void)onDubbingScoreStart;
- (void)onDubbingScoreErrorWithErrorType:(int)errorType;
- (void)onDubbingScoreInfoWithInfoType:(int)infoType InfoValue:(int)infoValue;
- (void)onDubbingScoreEnd:(float)scoreValue;
@optional
@end

@interface YPPDubbingScoreOptions : NSObject
@property (nonatomic, strong) NSString *originUrl;//原声视频Url
@property (nonatomic, strong) NSString *bgmUrl;//背景音Url
@property (nonatomic, strong) NSString *dubUrl;//配音Url
@end

@interface YPPDubbingScore : NSObject

- (instancetype) init;

- (void)initialize:(YPPDubbingScoreOptions*)options;

- (void)start;
- (void)resume;
- (void)pause;
- (void)stop;

- (void)terminate;

@property (nonatomic, weak) id<YPPDubbingScoreDelegate> delegate;

@end
