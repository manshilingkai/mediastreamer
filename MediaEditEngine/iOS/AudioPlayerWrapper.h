//
//  AudioPlayerWrapper.h
//  MediaStreamer
//
//  Created by Think on 2019/8/1.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef AudioPlayerWrapper_h
#define AudioPlayerWrapper_h

#include <stdio.h>

struct AudioPlayerWrapper;

#ifdef __cplusplus
extern "C" {
#endif
    struct AudioPlayerWrapper *AudioPlayer_GetInstance();
    struct AudioPlayerWrapper *AudioPlayer_GetInstanceWithRenderAndMixOptions(int sampleRate, int channelCount, bool externalRender);
    void AudioPlayer_ReleaseInstance(struct AudioPlayerWrapper **ppInstance);
    
    void AudioPlayer_setListener(struct AudioPlayerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner);
    
    void AudioPlayer_setDataSource(struct AudioPlayerWrapper *pInstance, char* url);
    
    void AudioPlayer_prepare(struct AudioPlayerWrapper *pInstance);
    void AudioPlayer_prepareAsync(struct AudioPlayerWrapper *pInstance);
    void AudioPlayer_prepareAsyncToPlay(struct AudioPlayerWrapper *pInstance);
    void AudioPlayer_play(struct AudioPlayerWrapper *pInstance);
    bool AudioPlayer_isPlaying(struct AudioPlayerWrapper *pInstance);
    void AudioPlayer_pause(struct AudioPlayerWrapper *pInstance);
    void AudioPlayer_stop(struct AudioPlayerWrapper *pInstance);
    
    void AudioPlayer_seekTo(struct AudioPlayerWrapper *pInstance, int32_t seekPosMs);
    
    int32_t AudioPlayer_getPlayTimeMs(struct AudioPlayerWrapper *pInstance);
    int32_t AudioPlayer_getDurationMs(struct AudioPlayerWrapper *pInstance);
    int AudioPlayer_getPcmDB(struct AudioPlayerWrapper *pInstance);
    
    void AudioPlayer_setVolume(struct AudioPlayerWrapper *pInstance, float volume);
    void AudioPlayer_setPlayRate(struct AudioPlayerWrapper *pInstance, float playrate);
    void AudioPlayer_setLooping(struct AudioPlayerWrapper *pInstance, bool isLooping);

    void AudioPlayer_mixWithExternal(struct AudioPlayerWrapper *pInstance, char *externalInputPcmData, int externalInputPcmSize);
    int AudioPlayer_drainPCMDataFromExternal(struct AudioPlayerWrapper *pInstance, void **pData, int size);
#ifdef __cplusplus
};
#endif


#endif /* AudioPlayerWrapper_h */
