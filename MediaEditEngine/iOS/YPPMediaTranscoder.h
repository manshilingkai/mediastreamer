//
//  YPPMediaTranscoder.h
//  MediaStreamer
//
//  Created by Think on 2019/10/16.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

enum media_transcoder_event_type {
    MEDIA_TRANSCODER_CONNECTING = 0,
    MEDIA_TRANSCODER_CONNECTED = 1,
    MEDIA_TRANSCODER_STREAMING = 2,
    MEDIA_TRANSCODER_ERROR = 3,
    MEDIA_TRANSCODER_INFO = 4,
    MEDIA_TRANSCODER_END = 5,
    MEDIA_TRANSCODER_PAUSED = 6,
};

enum media_transcoder_info_type {
    MEDIA_TRANSCODER_INFO_PUBLISH_DELAY_TIME = 1,
    MEDIA_TRANSCODER_INFO_PUBLISH_REAL_BITRATE = 2,
    MEDIA_TRANSCODER_INFO_PUBLISH_REAL_FPS = 3,
    MEDIA_TRANSCODER_INFO_PUBLISH_DOWN_BITRATE = 4,
    MEDIA_TRANSCODER_INFO_PUBLISH_UP_BITRATE = 5,
    MEDIA_TRANSCODER_INFO_PUBLISH_TIME = 6,
    MEDIA_TRANSCODER_INFO_PUBLISH_DURATION = 7,
    
    MEDIA_TRANSCODER_WARN_AUDIO_DECODE_FAIL = 100,
    MEDIA_TRANSCODER_WARN_VIDEO_DECODE_FAIL = 101,
};

enum media_transcoder_error_type {
    MEDIA_TRANSCODER_ERROR_UNKNOWN = -1,
    MEDIA_TRANSCODER_ERROR_CONNECT_FAIL = 0,
    MEDIA_TRANSCODER_ERROR_OPEN_VIDEO_ENCODER_FAIL = 1,
    MEDIA_TRANSCODER_ERROR_OPEN_AUDIO_ENCODER_FAIL = 2,
    MEDIA_TRANSCODER_ERROR_VIDEO_FILTER_FAIL = 3,
    MEDIA_TRANSCODER_ERROR_AUDIO_FILTER_FAIL = 4,
    MEDIA_TRANSCODER_ERROR_VIDEO_ENCODE_FAIL = 5,
    MEDIA_TRANSCODER_ERROR_AUDIO_ENCODE_FAIL = 6,
    MEDIA_TRANSCODER_ERROR_AUDIO_MUX_FAIL = 7,
    MEDIA_TRANSCODER_ERROR_VIDEO_MUX_FAIL = 8,
    MEDIA_TRANSCODER_ERROR_AUDIO_FIFO_READ_FAIL = 9,
    MEDIA_TRANSCODER_ERROR_POOR_NETWORK = 10,
    
    MEDIA_TRANSCODER_ERROR_DEMUXER_READ_FAIL = 100,
    
    MEDIA_TRANSCODER_ERROR_NO_MEMORY = 200,
};

enum VIDEO_FLIP_MODE
{
    NO_FLIP = 0,
    VFLIP = 1,
    HFLIP = 2,
};

@protocol YPPMediaTranscoderDelegate <NSObject>
@required
- (void)gotConnecting;
- (void)didConnected;
- (void)gotTranscoding;
- (void)didPaused;
- (void)gotErrorWithErrorType:(int)errorType;
- (void)gotInfoWithInfoType:(int)infoType InfoValue:(int)infoValue;
- (void)didEnd;
@optional
@end

@interface YPPMediaTranscoderOptions : NSObject
@property (nonatomic, strong) NSString *inputUrl;
@property (nonatomic) long startPosMs;
@property (nonatomic) long endPosMs;

@property (nonatomic) int video_rotation_degree; // 0,90,180,270
@property (nonatomic) int video_flip; //0:no flip 1:vflip 2:hflip

@property (nonatomic, strong) NSString *overlayUrl;
@property (nonatomic) BOOL has_alpha_for_overlay_video;
@property (nonatomic) int overlay_x;
@property (nonatomic) int overlay_y;
@property (nonatomic) float overlay_scale;
@property (nonatomic) long overlay_effect_in_ms;
@property (nonatomic) long overlay_effect_out_ms;

@property (nonatomic) BOOL privateStrategy;
@property (nonatomic, strong) NSString *privateLogoIcon;
@property (nonatomic, strong) NSString *privateLogoId;
@property (nonatomic, strong) NSString *privateLogoFontLibPath;
@property (nonatomic, strong) NSString *privateMaskVertical;
@property (nonatomic, strong) NSString *privateMaskHorizontal;

@property (nonatomic, strong) NSString *outputUrl;
@property (nonatomic) BOOL hasVideo;
@property (nonatomic) int outputVideoWidth;
@property (nonatomic) int outputVideoHeight;
@property (nonatomic) int outputVideoBitrateKbps;
@property (nonatomic) BOOL hasAudio;
@property (nonatomic) int outputAudioSampleRate;
@property (nonatomic) int outputAudioBitrateKbps;

@end

@interface YPPMediaTranscoder : NSObject

- (instancetype) init;

- (void)initialize:(YPPMediaTranscoderOptions*)options;
- (void)initializeWithConfigure:(NSString*)configure;

- (void)start;
- (void)resume;
- (void)pause;
- (void)stop:(BOOL)isCancel;

- (void)terminate;

@property (nonatomic, weak) id<YPPMediaTranscoderDelegate> delegate;

@end
