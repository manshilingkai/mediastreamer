//
//  PrivateAudioPlayer.m
//  MediaStreamer
//
//  Created by slklovewyy on 2019/11/15.
//  Copyright © 2019 bolome. All rights reserved.
//

#import "PrivateAudioPlayer.h"
#include "AudioPlayerWrapper.h"

@implementation PrivateAudioPlayer
{
    AudioPlayerWrapper *pAudioPlayerWrapper;
    dispatch_queue_t audioPlayerNotificationQueue;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pAudioPlayerWrapper = NULL;
        audioPlayerNotificationQueue = dispatch_queue_create("PrivateAudioPlayerNotificationQueue", 0);
    }
    
    return self;
}

void audioPlayerNotificationListener(void* owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak PrivateAudioPlayer *thiz = (__bridge PrivateAudioPlayer*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchAudioPlayerNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchAudioPlayerNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    __weak typeof(self) wself = self;
    
    dispatch_async(audioPlayerNotificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf handleAudioPlayerNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    });
}

- (void)handleAudioPlayerNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    switch (event) {
        case AUDIO_PLAYER_PREPARED:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onPrepared)])) {
                    [self.delegate onPrepared];
                }
            }
            break;
        case AUDIO_PLAYER_ERROR:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onErrorWithErrorType:)])) {
                    [self.delegate onErrorWithErrorType:ext1];
                }
            }
            break;
        case AUDIO_PLAYER_INFO:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onInfoWithInfoType:InfoValue:)])) {
                    [self.delegate onInfoWithInfoType:ext1 InfoValue:ext2];
                }
            }
            break;
        case AUDIO_PLAYER_PLAYBACK_COMPLETE:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onCompletion)])) {
                    [self.delegate onCompletion];
                }
            }
            break;
        case AUDIO_PLAYER_SEEK_COMPLETE:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onSeekComplete)])) {
                    [self.delegate onSeekComplete];
                }
            }
            break;
        default:
            break;
    }
}

- (void)initialize
{
    pAudioPlayerWrapper = AudioPlayer_GetInstance();
    if (pAudioPlayerWrapper) {
        AudioPlayer_setListener(pAudioPlayerWrapper, audioPlayerNotificationListener, (__bridge void*)self);
    }
}

- (void)setDataSource:(NSURL*)url
{
    NSString *urlString = [url isFileURL] ? [url path] : [url absoluteString];
    [self setDataSourceWithUrl:urlString];
}

- (void)setDataSourceWithUrl:(NSString*)url
{
    if (pAudioPlayerWrapper) {
        AudioPlayer_setDataSource(pAudioPlayerWrapper, (char*)[url UTF8String]);
    }
}

- (void)prepare
{
    if (pAudioPlayerWrapper) {
        AudioPlayer_prepare(pAudioPlayerWrapper);
    }
}

- (void)prepareAsync
{
    if (pAudioPlayerWrapper) {
        AudioPlayer_prepareAsync(pAudioPlayerWrapper);
    }
}

- (void)prepareAsyncToPlay
{
    if (pAudioPlayerWrapper) {
        AudioPlayer_prepareAsyncToPlay(pAudioPlayerWrapper);
    }
}

- (void)play
{
    if (pAudioPlayerWrapper) {
        AudioPlayer_play(pAudioPlayerWrapper);
    }
}

- (BOOL)isPlaying
{
    if (pAudioPlayerWrapper) {
        return AudioPlayer_isPlaying(pAudioPlayerWrapper)?YES:NO;
    }
    
    return NO;
}

- (void)pause
{
    if (pAudioPlayerWrapper) {
        AudioPlayer_pause(pAudioPlayerWrapper);
    }
}

- (void)stop
{
    if (pAudioPlayerWrapper) {
        AudioPlayer_stop(pAudioPlayerWrapper);
    }
    
    NSLog(@"PrivateAudioPlayer stoped");
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    if (pAudioPlayerWrapper) {
        AudioPlayer_seekTo(pAudioPlayerWrapper, seekPosMs);
    }
}

- (void)setVolume:(NSTimeInterval)volume
{
    if (pAudioPlayerWrapper) {
        AudioPlayer_setVolume(pAudioPlayerWrapper, volume);
    }
}

- (void)setPlayRate:(NSTimeInterval)playrate
{
    if (pAudioPlayerWrapper) {
        AudioPlayer_setPlayRate(pAudioPlayerWrapper, playrate);
    }
}

- (void)setLooping:(BOOL)isLooping
{
    if (pAudioPlayerWrapper) {
        if (isLooping) {
            AudioPlayer_setLooping(pAudioPlayerWrapper, true);
        }else {
            AudioPlayer_setLooping(pAudioPlayerWrapper, false);
        }
    }
}

- (NSTimeInterval)currentPlaybackTimeMs
{
    if (pAudioPlayerWrapper) {
        return AudioPlayer_getPlayTimeMs(pAudioPlayerWrapper);
    }
    
    return 0;
}

- (NSTimeInterval)durationMs
{
    if (pAudioPlayerWrapper) {
        return AudioPlayer_getDurationMs(pAudioPlayerWrapper);
    }
    
    return 0;
}

- (NSInteger)pcmDB
{
    NSInteger ret = 0;
    if (pAudioPlayerWrapper) {
        ret = AudioPlayer_getPcmDB(pAudioPlayerWrapper);
    }
    
    if (ret<MIN_DB) {
        ret = MIN_DB;
    }
    
    if (ret>MAX_DB) {
        ret = MAX_DB;
    }
    
    return ret;
}

- (void)terminate
{
    if (pAudioPlayerWrapper) {
        AudioPlayer_ReleaseInstance(&pAudioPlayerWrapper);
        pAudioPlayerWrapper = NULL;
    }
    
    dispatch_barrier_sync(audioPlayerNotificationQueue, ^{
        NSLog(@"finish all PrivateAudioPlayer notifications");
    });
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"PrivateAudioPlayer dealloc");
}

@end
