//
//  VideoFileUtils.m
//  MediaStreamer
//
//  Created by Think on 2019/9/26.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "VideoFileUtils.h"
#import <AVFoundation/AVFoundation.h>

#include "MediaCoverImage.h"

@implementation VideoFileUtils

+ (int)getVideoFileDuration:(NSString*)videoFilePath
{
    AVURLAsset *asset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:videoFilePath]];
    CMTime time = [asset duration];
    return ceil(time.value/time.timescale);
}

+ (long long)getVideoFileSize:(NSString*)videoFilePath
{
    return [[NSFileManager defaultManager] attributesOfItemAtPath:videoFilePath error:nil].fileSize;
}

+ (UIImage *)getThumbnailImageFromVideoFilePath:(NSString *)videoFilePath Time:(NSTimeInterval)videoTime Accurate:(BOOL)isAccurate
{
    if (!videoFilePath) {
        return nil;
    }
    
    @autoreleasepool {
        AVURLAsset *asset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:videoFilePath]];
        AVAssetImageGenerator *assetImageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        assetImageGenerator.appliesPreferredTrackTransform = YES;
        assetImageGenerator.apertureMode = AVAssetImageGeneratorApertureModeEncodedPixels;
        
        if (isAccurate) {
            assetImageGenerator.requestedTimeToleranceAfter = kCMTimeZero;
            assetImageGenerator.requestedTimeToleranceBefore = kCMTimeZero;
        }
        
        CMTime asset_duration = [asset duration];
        NSTimeInterval videoDuration = ceil(asset_duration.value/asset_duration.timescale);
        if (videoTime>videoDuration) {
            videoTime = videoDuration;
        }
        
        CGImageRef thumbnailImageRef = nil;
        CFTimeInterval thumbnailImageTime = videoTime;
        thumbnailImageRef = [assetImageGenerator copyCGImageAtTime:CMTimeMakeWithSeconds(thumbnailImageTime, 600)
                                                        actualTime:NULL error:nil];
        if (thumbnailImageRef) {
            CGImageRelease(thumbnailImageRef);
            thumbnailImageRef = nil;
        }
        if (!thumbnailImageRef) {
            CVPixelBufferRef thumbnailPixelBufferRef = [VideoFileUtils getVideoImageWithInputFile:videoFilePath Position:videoTime*1000];
            if (thumbnailPixelBufferRef) {
                CIImage *ciImage = [CIImage imageWithCVPixelBuffer:thumbnailPixelBufferRef];
                
                CIContext *temporaryContext = [CIContext contextWithOptions:nil];
                CGImageRef videoImage = [temporaryContext
                                         createCGImage:ciImage
                                         fromRect:CGRectMake(0, 0,
                                                             CVPixelBufferGetWidth(thumbnailPixelBufferRef),
                                                             CVPixelBufferGetHeight(thumbnailPixelBufferRef))];
                CVPixelBufferRelease(thumbnailPixelBufferRef);
                UIImage *uiImage = [UIImage imageWithCGImage:videoImage];
                CGImageRelease(videoImage);
                
                return uiImage;
            }else{
                return nil;
            }
        }
        
        UIImage *thumbnailImage = [[UIImage alloc] initWithCGImage:thumbnailImageRef];
        CGImageRelease(thumbnailImageRef);
        
        return thumbnailImage;
    }
}

+ (UIImage *)getThumbnailImageFromVideoFilePath:(NSString *)videoFilePath Time:(NSTimeInterval)videoTime Accurate:(BOOL)isAccurate ToSize:(CGSize)reSize
{
    if (!videoFilePath) {
        return nil;
    }
    
    @autoreleasepool {
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[[NSURL alloc] initFileURLWithPath:videoFilePath] options:nil];
        AVAssetImageGenerator *assetImageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        assetImageGenerator.appliesPreferredTrackTransform = YES;
        assetImageGenerator.apertureMode = AVAssetImageGeneratorApertureModeEncodedPixels;
        
        if (isAccurate) {
            assetImageGenerator.requestedTimeToleranceAfter = kCMTimeZero;
            assetImageGenerator.requestedTimeToleranceBefore = kCMTimeZero;
        }
        
        //    assetImageGenerator.maximumSize = reSize;
        
        CMTime asset_duration = [asset duration];
        NSTimeInterval videoDuration = ceil(asset_duration.value/asset_duration.timescale);
        if (videoTime>videoDuration) {
            videoTime = videoDuration;
        }
        
        CGImageRef thumbnailImageRef = nil;
        CFTimeInterval thumbnailImageTime = videoTime;
        thumbnailImageRef = [assetImageGenerator copyCGImageAtTime:CMTimeMakeWithSeconds(thumbnailImageTime, 600)
                                                            actualTime:NULL error:nil];
        if (!thumbnailImageRef) {
            CVPixelBufferRef thumbnailPixelBufferRef = [VideoFileUtils getVideoImageWithInputFile:videoFilePath Position:videoTime*1000 OutputWidth:reSize.width OutputHeight:reSize.height];
            if (thumbnailPixelBufferRef) {
                CIImage *ciImage = [CIImage imageWithCVPixelBuffer:thumbnailPixelBufferRef];
                
                CIContext *temporaryContext = [CIContext contextWithOptions:nil];
                CGImageRef videoImage = [temporaryContext
                                         createCGImage:ciImage
                                         fromRect:CGRectMake(0, 0,
                                                             CVPixelBufferGetWidth(thumbnailPixelBufferRef),
                                                             CVPixelBufferGetHeight(thumbnailPixelBufferRef))];
                CVPixelBufferRelease(thumbnailPixelBufferRef);
                UIImage *uiImage = [UIImage imageWithCGImage:videoImage];
                CGImageRelease(videoImage);
                
                return uiImage;
            }else{
                return nil;
            }
        }
            
        CGRect cropRect;
        size_t thumbnail_image_width = CGImageGetWidth(thumbnailImageRef);
        size_t thumbnail_image_height = CGImageGetHeight(thumbnailImageRef);
        if (thumbnail_image_width*reSize.height <= thumbnail_image_height*reSize.width) {
            CGFloat width  = thumbnail_image_width;
            CGFloat height = thumbnail_image_width * reSize.height / reSize.width;
            cropRect = CGRectMake(0, (thumbnail_image_height -height)/2, width, height);
        }else{
            CGFloat width  = thumbnail_image_height * reSize.width / reSize.height;
            CGFloat height = thumbnail_image_height;
            cropRect = CGRectMake((thumbnail_image_width -width)/2, 0, width, height);
        }
        
        CGImageRef newThumbnailImageRef = CGImageCreateWithImageInRect(thumbnailImageRef, cropRect);
        CGImageRelease(thumbnailImageRef);
        
        UIImage *thumbnailImage = [[UIImage alloc] initWithCGImage:newThumbnailImageRef];
        CGImageRelease(newThumbnailImageRef);
        
        UIGraphicsBeginImageContext(CGSizeMake(reSize.width, reSize.height));
        [thumbnailImage drawInRect:CGRectMake(0, 0, reSize.width, reSize.height)];
        UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        thumbnailImage = nil;
        
        return reSizeImage;
    }
}

+ (CVPixelBufferRef)getVideoImageWithInputFile:(NSString*)inputFile Position:(NSTimeInterval)position
{
    char* inputMediaFile = (char*)[inputFile UTF8String];

    VideoFrame* outputImage = getVideoImageWithPosition(inputMediaFile, position);
    if (outputImage==NULL) {
        return nil;
    }else {
        NSDictionary *pixelBufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                       [NSDictionary dictionary], (id)kCVPixelBufferIOSurfacePropertiesKey,
                                                       nil];
        CVPixelBufferRef pixelBuffer = nil;
        CVPixelBufferCreate(NULL, outputImage->width, outputImage->height, kCVPixelFormatType_32BGRA, (__bridge CFDictionaryRef)pixelBufferAttributes, &pixelBuffer);
        
        CVPixelBufferLockBaseAddress(pixelBuffer,0);
        uint8_t* data = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
        int bytesPerRow = (int)CVPixelBufferGetBytesPerRow(pixelBuffer);
        if (outputImage->width*4!=bytesPerRow) {
            for (int i = 0; i<outputImage->height; i++) {
                memcpy(data+bytesPerRow*i, outputImage->data+outputImage->width*4*i, outputImage->width*4);
            }
        }else{
            memcpy(data, outputImage->data, outputImage->frameSize);
        }
                
        CVPixelBufferUnlockBaseAddress(pixelBuffer,0);
                
        if (outputImage!=NULL) {
            if (outputImage->data) {
                free(outputImage->data);
                outputImage->data = NULL;
            }
            delete outputImage;
            outputImage = NULL;
        }
        
        return pixelBuffer;
    }
}


+ (CVPixelBufferRef)getVideoImageWithInputFile:(NSString*)inputFile Position:(NSTimeInterval)position OutputWidth:(int)width OutputHeight:(int)height
{
    VideoFrame* outputImage = new VideoFrame();
    outputImage->width = width;
    outputImage->height = height;
    outputImage->frameSize = outputImage->width*outputImage->height*4;
    outputImage->data = (uint8_t*)malloc(outputImage->frameSize);
    
    char* inputMediaFile = (char*)[inputFile UTF8String];
    
    int ret = getCoverImageWithPosition(inputMediaFile, position, outputImage);
    
    if (ret) {
        if (outputImage!=NULL) {
            if (outputImage->data) {
                free(outputImage->data);
                outputImage->data = NULL;
            }
            delete outputImage;
            outputImage = NULL;
        }
        
        return nil;
    }else {
        NSDictionary *pixelBufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [NSDictionary dictionary], (id)kCVPixelBufferIOSurfacePropertiesKey,
                                               nil];
        
        CVPixelBufferRef pixelBuffer = nil;
        CVPixelBufferCreate(NULL, outputImage->width, outputImage->height, kCVPixelFormatType_32BGRA, (__bridge CFDictionaryRef)pixelBufferAttributes, &pixelBuffer);
        
        CVPixelBufferLockBaseAddress(pixelBuffer,0);
        uint8_t* data = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
//        int frameSize = (int)CVPixelBufferGetDataSize(pixelBuffer);
        int bytesPerRow = (int)CVPixelBufferGetBytesPerRow(pixelBuffer);
        
        if (outputImage->width*4!=bytesPerRow) {
            for (int i = 0; i<outputImage->height; i++) {
                memcpy(data+bytesPerRow*i, outputImage->data+outputImage->width*4*i, outputImage->width*4);
            }
        }else{
            memcpy(data, outputImage->data, outputImage->frameSize);
        }
        
        CVPixelBufferUnlockBaseAddress(pixelBuffer,0);
        
        if (outputImage!=NULL) {
            if (outputImage->data) {
                free(outputImage->data);
                outputImage->data = NULL;
            }
            delete outputImage;
            outputImage = NULL;
        }
        
        return pixelBuffer;
    }
}

- (CVPixelBufferRef)imageToYUVPixelBuffer:(UIImage *)image {
    CGSize frameSize = CGSizeMake(CGImageGetWidth(image.CGImage), CGImageGetHeight(image.CGImage));
    
    CFDictionaryRef empty = CFDictionaryCreate(kCFAllocatorDefault, NULL, NULL, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES],kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:YES],kCVPixelBufferCGBitmapContextCompatibilityKey,
                             empty, kCVPixelBufferIOSurfacePropertiesKey, nil];
    
    CVPixelBufferRef pxbuffer = NULL;
    CVPixelBufferCreate(kCFAllocatorDefault, frameSize.width, frameSize.height,kCVPixelFormatType_420YpCbCr8BiPlanarFullRange, (__bridge CFDictionaryRef)options,&pxbuffer);
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGContextRef context = CGBitmapContextCreate(pxdata, frameSize.width, frameSize.height,8,CVPixelBufferGetBytesPerRowOfPlane(pxbuffer, 0),colorSpace,kCGImageAlphaNone);
    CGContextDrawImage(context, CGRectMake(0, 0, CGImageGetWidth(image.CGImage),CGImageGetHeight(image.CGImage)), image.CGImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    return pxbuffer;
}

@end
