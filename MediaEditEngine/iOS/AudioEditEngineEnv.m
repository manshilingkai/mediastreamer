//
//  AudioEditEngineEnv.m
//  MediaStreamer
//
//  Created by Think on 2019/8/1.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import "AudioEditEngineEnv.h"

#import <AVFoundation/AVFoundation.h>

@implementation AudioEditEngineEnv

+ (void)setupAudioSession
{
    NSError *error = nil;
    if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers error:&error]) {
        NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
    }
    
    error = nil;
    if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeDefault error:&error]) {
        NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
    }
    
    error = nil;
    if (NO == [[AVAudioSession sharedInstance] setActive:YES error:&error]) {
        NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
    }
}

@end
