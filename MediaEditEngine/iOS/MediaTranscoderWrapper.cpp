//
//  MediaTranscoderWrapper.cpp
//  MediaStreamer
//
//  Created by Think on 2019/10/14.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "MediaTranscoderWrapper.h"
#include "MediaTranscoder.h"

#ifdef __cplusplus
extern "C" {
#endif

struct MediaTranscoderWrapper
{
    MediaTranscoder* mediaTranscoder;
    
    MediaTranscoderWrapper()
    {
        mediaTranscoder = NULL;
    }
};

struct MediaTranscoderWrapper* MediaTranscoder_GetInstanceWithParam(char* inputUrl, long startPosMs, long endPosMs, char* outputUrl, bool isEnableExternalRender)
{
    MediaMaterial mediaMaterial;
    MediaEffectGroup *pMediaEffectGroup = NULL;
    MediaProduct mediaProduct;
    
    mediaMaterial.url = inputUrl;
    mediaMaterial.startPosMs = startPosMs;
    mediaMaterial.endPosMs = endPosMs;
    
    if (isEnableExternalRender) {
        pMediaEffectGroup = new MediaEffectGroup;
        pMediaEffectGroup->mediaEffectCount++;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1] = new MediaEffect;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->type =  MEDIA_EFFECT_TYPE_EXTERNAL_RENDER;
    }
    
    mediaProduct.url = outputUrl;
    
    MediaTranscoderWrapper* pInstance = new MediaTranscoderWrapper;
    pInstance->mediaTranscoder = MediaTranscoder::CreateMediaTranscoder(MEDIA_TRANSCODER_TYPE_DEFAULT, NULL, mediaMaterial, pMediaEffectGroup, mediaProduct);
    
    if (pMediaEffectGroup) {
        pMediaEffectGroup->Free();
        delete pMediaEffectGroup;
        pMediaEffectGroup = NULL;
    }
    
    return pInstance;
}

struct MediaTranscoderWrapper* MediaTranscoder_GetInstance(MediaMaterial input, MediaEffectGroup *pMediaEffectGroup, MediaProduct product)
{
    MediaTranscoderWrapper* pInstance = new MediaTranscoderWrapper;
    pInstance->mediaTranscoder = MediaTranscoder::CreateMediaTranscoder(MEDIA_TRANSCODER_TYPE_DEFAULT, NULL, input, pMediaEffectGroup, product);
    
    return pInstance;
}

struct MediaTranscoderWrapper* MediaTranscoder_GetInstanceWithConfigure(char* configureInfo)
{
    MediaTranscoderWrapper* pInstance = new MediaTranscoderWrapper;
    pInstance->mediaTranscoder = MediaTranscoder::CreateMediaTranscoder(MEDIA_TRANSCODER_TYPE_DEFAULT, NULL, configureInfo);
    
    return pInstance;
}

void MediaTranscoder_ReleaseInstance(struct MediaTranscoderWrapper **ppInstance)
{
    MediaTranscoderWrapper* pInstance  = *ppInstance;
    if (pInstance!=NULL) {
        if (pInstance->mediaTranscoder!=NULL) {
            MediaTranscoder::DeleteMediaTranscoder(MEDIA_TRANSCODER_TYPE_DEFAULT, pInstance->mediaTranscoder);
            pInstance->mediaTranscoder = NULL;
        }
        
        delete pInstance;
        pInstance = NULL;
    }
}

void MediaTranscoder_setListener(struct MediaTranscoderWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner)
{
    if (pInstance!=NULL && pInstance->mediaTranscoder!=NULL) {
        pInstance->mediaTranscoder->setListener(listener,owner);
    }
}

void MediaTranscoder_start(struct MediaTranscoderWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaTranscoder!=NULL) {
        pInstance->mediaTranscoder->start();
    }
}

void MediaTranscoder_resume(struct MediaTranscoderWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaTranscoder!=NULL) {
        pInstance->mediaTranscoder->resume();
    }
}

void MediaTranscoder_pause(struct MediaTranscoderWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaTranscoder!=NULL) {
        pInstance->mediaTranscoder->pause();
    }
}

void MediaTranscoder_stop(struct MediaTranscoderWrapper *pInstance, bool isCancel)
{
    if (pInstance!=NULL && pInstance->mediaTranscoder!=NULL) {
        pInstance->mediaTranscoder->stop(isCancel);
    }
}

#ifdef __cplusplus
};
#endif
