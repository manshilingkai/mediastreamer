//
//  RadioProducerWrapper.h
//  MediaStreamer
//
//  Created by Think on 2019/10/23.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef RadioProducerWrapper_h
#define RadioProducerWrapper_h

#include <stdio.h>
#include "RadioProducerCommon.h"

struct RadioProducerWrapper;

#ifdef __cplusplus
extern "C" {
#endif

struct RadioProducerWrapper* RadioProducerWrapper_GetInstance(RadioProductOptions options);
void RadioProducerWrapper_ReleaseInstance(struct RadioProducerWrapper **ppInstance);

void RadioProducerWrapper_setListener(struct RadioProducerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner);

void RadioProducerWrapper_prepareBgm(struct RadioProducerWrapper *pInstance, char* bgmUrl);
void RadioProducerWrapper_prepareBgmWithStartPos(struct RadioProducerWrapper *pInstance, char* bgmUrl, int32_t startPosMs);
void RadioProducerWrapper_startBgm(struct RadioProducerWrapper *pInstance);
void RadioProducerWrapper_pauseBgm(struct RadioProducerWrapper *pInstance);
void RadioProducerWrapper_stopBgm(struct RadioProducerWrapper *pInstance);
void RadioProducerWrapper_setBgmVolume(struct RadioProducerWrapper *pInstance, float volume);

void RadioProducerWrapper_startMicrophone(struct RadioProducerWrapper *pInstance);
void RadioProducerWrapper_stopMicrophone(struct RadioProducerWrapper *pInstance);
void RadioProducerWrapper_setMicrophoneVolume(struct RadioProducerWrapper *pInstance, float volume);

void RadioProducerWrapper_prepareEffectMusic(struct RadioProducerWrapper *pInstance, char* effectMusicUrl);
void RadioProducerWrapper_startEffectMusic(struct RadioProducerWrapper *pInstance);
void RadioProducerWrapper_pauseEffectMusic(struct RadioProducerWrapper *pInstance);
void RadioProducerWrapper_stopEffectMusic(struct RadioProducerWrapper *pInstance);
void RadioProducerWrapper_setEffectMusicVolume(struct RadioProducerWrapper *pInstance, float volume);

void RadioProducerWrapper_setAudioUserDefinedEffect(struct RadioProducerWrapper *pInstance, int effect);
void RadioProducerWrapper_setAudioEqualizerStyle(struct RadioProducerWrapper *pInstance, int style);
void RadioProducerWrapper_setAudioReverbStyle(struct RadioProducerWrapper *pInstance, int style);
void RadioProducerWrapper_setAudioPitchSemiTones(struct RadioProducerWrapper *pInstance, int value);

void RadioProducerWrapper_setEarReturn(struct RadioProducerWrapper *pInstance, bool isEnableEarReturn);

void RadioProducerWrapper_pause(struct RadioProducerWrapper *pInstance, bool isExport = false);
void RadioProducerWrapper_stop(struct RadioProducerWrapper *pInstance);

int32_t RadioProducerWrapper_getCurrentTimeMs(struct RadioProducerWrapper *pInstance);
int32_t RadioProducerWrapper_getBgmPlayingTimeMs(struct RadioProducerWrapper *pInstance);

#ifdef __cplusplus
};
#endif

#endif /* RadioProducerWrapper_h */
