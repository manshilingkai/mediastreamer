//
//  MicrophoneAudioRecorderWrapper.cpp
//  MediaStreamer
//
//  Created by Think on 2019/7/13.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "MicrophoneAudioRecorderWrapper.h"
#include "MicrophoneAudioRecorder.h"

#ifdef __cplusplus
extern "C" {
#endif

    struct MicrophoneAudioRecorderWrapper{
        MicrophoneAudioRecorder* microphoneAudioRecorder;
        
        MicrophoneAudioRecorderWrapper()
        {
            microphoneAudioRecorder = NULL;
        }
    };
    
    struct MicrophoneAudioRecorderWrapper *MicrophoneAudioRecorder_GetInstance(int sampleRate, int numChannels, char* workDir)
    {
        MicrophoneAudioRecorderWrapper* pInstance = new MicrophoneAudioRecorderWrapper;
        pInstance->microphoneAudioRecorder = new MicrophoneAudioRecorder(sampleRate, numChannels, workDir);
        
        return pInstance;
    }
    
    void MicrophoneAudioRecorder_ReleaseInstance(struct MicrophoneAudioRecorderWrapper **ppInstance)
    {
        MicrophoneAudioRecorderWrapper* pInstance  = *ppInstance;
        if (pInstance!=NULL) {
            if (pInstance->microphoneAudioRecorder!=NULL) {
                delete pInstance->microphoneAudioRecorder;
                pInstance->microphoneAudioRecorder = NULL;
            }
            
            delete pInstance;
            pInstance = NULL;
        }
    }
    
    bool MicrophoneAudioRecorder_initialize(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->initialize();
        }
        
        return false;
    }
    
    bool MicrophoneAudioRecorder_startRecord(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->startRecord();
        }
        
        return false;
    }
    
    bool MicrophoneAudioRecorder_startRecordWithStartPos(struct MicrophoneAudioRecorderWrapper *pInstance, int64_t startPositionUs)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->startRecord(startPositionUs);
        }
        
        return false;
    }
    
    void MicrophoneAudioRecorder_stopRecord(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            pInstance->microphoneAudioRecorder->stopRecord();
        }
    }
    
    int64_t MicrophoneAudioRecorder_getRecordTimeUs(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->getRecordTimeUs();
        }
        
        return 0;
    }
    
    int MicrophoneAudioRecorder_getRecordPcmDB(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->getRecordPcmDB();
        }
        
        return 0;
    }

    void MicrophoneAudioRecorder_setEarReturn(struct MicrophoneAudioRecorderWrapper *pInstance, bool isEnableEarReturn)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->setEarReturn(isEnableEarReturn);
        }
    }

    void MicrophoneAudioRecorder_enableNoiseSuppression(struct MicrophoneAudioRecorderWrapper *pInstance, bool isEnableNoiseSuppression)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->enableNoiseSuppression(isEnableNoiseSuppression);
        }
    }
    
    bool MicrophoneAudioRecorder_openAudioPlayer(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->openAudioPlayer();
        }
        
        return false;
    }
    
    bool MicrophoneAudioRecorder_startAudioPlay(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->startAudioPlay();
        }
        
        return false;
    }
    
    bool MicrophoneAudioRecorder_seekAudioPlay(struct MicrophoneAudioRecorderWrapper *pInstance, int64_t seekTimeUs)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->seekAudioPlay(seekTimeUs);
        }
        
        return false;
    }
    
    void MicrophoneAudioRecorder_pauseAudioPlay(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            pInstance->microphoneAudioRecorder->pauseAudioPlay();
        }
    }
    
    void MicrophoneAudioRecorder_closeAudioPlayer(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            pInstance->microphoneAudioRecorder->closeAudioPlayer();
        }
    }
    
    int64_t MicrophoneAudioRecorder_getPlayTimeUs(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->getPlayTimeUs();
        }
        
        return 0;
    }
    
    int64_t MicrophoneAudioRecorder_getPlayDurationUs(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->getPlayDurationUs();
        }
        
        return 0;
    }
    
    int MicrophoneAudioRecorder_getPlayPcmDB(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->getPlayPcmDB();
        }
        
        return 0;
    }

    bool MicrophoneAudioRecorder_reverseOrderGeneration(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->reverseOrderGeneration();
        }
        
        return false;
    }
    
    bool MicrophoneAudioRecorder_backDelete(struct MicrophoneAudioRecorderWrapper *pInstance, int64_t keepTimeUs)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->backDelete(keepTimeUs);
        }
        
        return false;
    }
    
    bool MicrophoneAudioRecorder_convertToWav(struct MicrophoneAudioRecorderWrapper *pInstance, char *wavFilePath)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->convertToWav(wavFilePath);
        }
        
        return false;
    }
    
    bool MicrophoneAudioRecorder_convertToWavWithOffset(struct MicrophoneAudioRecorderWrapper *pInstance, char *wavFilePath, long offsetMs)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->convertToWavWithOffset(wavFilePath, offsetMs);
        }
        
        return false;
    }
    
    void MicrophoneAudioRecorder_terminate(struct MicrophoneAudioRecorderWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->microphoneAudioRecorder!=NULL) {
            return pInstance->microphoneAudioRecorder->terminate();
        }
    }

    bool NoiseSuppressionWav(char* inputWavFilePath, char* outputWavFilePath)
    {
        return convertToWavWithNS(inputWavFilePath, outputWavFilePath);
    }

    bool OffsetWav(char* inputWavFilePath, char* outputWavFilePath, long offsetMs)
    {
        return convertToWavWithOffset(inputWavFilePath, outputWavFilePath, offsetMs);
    }

    bool EffectWav(char* inputWavFilePath, char* outputWavFilePath, int effectType, int effect)
    {
        return convertToWavWithAudioEffect(inputWavFilePath, outputWavFilePath, effectType, effect);
    }
    
#ifdef __cplusplus
};
#endif
