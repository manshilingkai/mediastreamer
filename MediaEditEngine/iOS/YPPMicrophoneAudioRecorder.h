//
//  YPPMicrophoneAudioRecorder.h
//  MediaStreamer
//
//  Created by Think on 2019/7/13.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

enum MAR_EffectType
{
    MAR_UserDefined_Effect = 0,
    MAR_Equalizer_Style,
    MAR_Reverb_Style
};

// user-defined effects.
enum MAR_UserDefinedEffect {
    MAR_ATMOS = 0,     //全景声
    MAR_BASS,          //超重低音
    MAR_ENHANCEDVOICE, //清澈人声
    MAR_METAL,         //金属风
    MAR_ROTATE3D,      //旋转3D
    MAR_CHORUS,        //和声
    MAR_INTANGIBLE,    //空灵
    MAR_NOEFFECT
};

// define Equalizer style
enum MAR_EqualizerStyle {
    MAR_POPULAR = 0, //流行
    MAR_DANCE,       //律动
    MAR_BLUES,       //忧郁
    MAR_CLASSICAL,   //复古
    MAR_JUZZ,        //爵士
    MAR_LIGHT,       //舒缓
    MAR_ELECTRICAL,  //电音
    MAR_ROCK,        //摇滚
    MAR_PHONOGRAPH,  //留声机
    MAR_NOEQUALIZER
};

// define Reverb style
enum MAR_ReverbStyle {
    MAR_KTV = 0,  //KTV
    MAR_THEATER,  //剧场
    MAR_CONCERT,  //音乐会
    MAR_STUDIO,   //录音棚
    MAR_NOREVERB
};

@interface MicrophoneAudioRecorderOptions : NSObject

@property (nonatomic) BOOL isControlAudioSession;
@property (nonatomic) NSInteger sampleRate;
@property (nonatomic) NSInteger numChannels;
@property (nonatomic, strong) NSString *workDir;

@end

@interface YPPMicrophoneAudioRecorder : NSObject

- (instancetype) init;

- (BOOL)initialize:(MicrophoneAudioRecorderOptions*)options;

- (BOOL)startRecord;
- (BOOL)startRecordWithStartPos:(NSTimeInterval)startPositionMs;
- (void)stopRecord;
- (void)enableEarReturn:(BOOL)isEnable;
- (void)enableNoiseSuppression:(BOOL)isEnable;

- (BOOL)openAudioPlayer;
- (BOOL)startAudioPlay;
- (BOOL)seekAudioPlay:(NSTimeInterval)seekTimeMs;
- (void)pauseAudioPlay;
- (void)closeAudioPlayer;

- (BOOL)reverseOrderGeneration;

- (BOOL)backDelete:(NSInteger)keepTimeMs;
- (BOOL)convertToWav:(NSString*)wavFilePath;
- (BOOL)convertToWavWithOffset:(NSString*)wavFilePath Offset:(NSInteger)offsetMs;

- (void)terminate;

+ (BOOL)NoiseSuppressionWavWithInput:(NSString*)inputWavFilePath WithOutput:(NSString*)outputWavFilePath;
+ (BOOL)OffsetWavWithInput:(NSString*)inputWavFilePath WithOutput:(NSString*)outputWavFilePath WithOffset:(NSInteger)offsetMs;
+ (BOOL)EffectWavWithInput:(NSString*)inputWavFilePath WithOutput:(NSString*)outputWavFilePath WithEffectType:(int)effectType WithEffect:(int)effect;

@property (nonatomic, readonly) NSTimeInterval recordTimeMs;
@property (nonatomic, readonly) NSInteger recordPcmDB; //40DB-80DB

@property (nonatomic, readonly) NSTimeInterval playTimeMs;
@property (nonatomic, readonly) NSTimeInterval playDurationMs;
@property (nonatomic, readonly) NSInteger playPcmDB; //40DB-80DB

@end

NS_ASSUME_NONNULL_END
