//
//  YPPMicrophoneAudioRecorder.m
//  MediaStreamer
//
//  Created by Think on 2019/7/13.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import "YPPMicrophoneAudioRecorder.h"
#import "MicrophoneAudioRecorderWrapper.h"
#import <AVFoundation/AVFoundation.h>

#define MIN_DB 40
#define MAX_DB 80

@implementation MicrophoneAudioRecorderOptions

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.isControlAudioSession = NO;
        self.sampleRate = 44100;
        self.numChannels = 2;
        self.workDir = NSTemporaryDirectory();
    }
    
    return self;
}

@end

@implementation YPPMicrophoneAudioRecorder
{
    MicrophoneAudioRecorderWrapper* pMicrophoneAudioRecorderWrapper;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pMicrophoneAudioRecorderWrapper = NULL;
    }
    
    return self;
}

- (BOOL)initialize:(MicrophoneAudioRecorderOptions*)options
{
    if ([options isControlAudioSession]) {
        NSError *error = nil;
        if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers error:&error]) {
            NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
        }
        
        error = nil;
        if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeDefault error:&error]) {
            NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
        }
        
        error = nil;
        if (NO == [[AVAudioSession sharedInstance] setActive:YES error:&error]) {
            NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
        }
    }
    
    pMicrophoneAudioRecorderWrapper = MicrophoneAudioRecorder_GetInstance([options sampleRate], [options numChannels], (char*)[[options workDir] UTF8String]);
    if (pMicrophoneAudioRecorderWrapper) {
        bool ret = MicrophoneAudioRecorder_initialize(pMicrophoneAudioRecorderWrapper);
        if (ret) {
            return YES;
        }
    }
    
    
    if (pMicrophoneAudioRecorderWrapper) {
        MicrophoneAudioRecorder_ReleaseInstance(&pMicrophoneAudioRecorderWrapper);
        pMicrophoneAudioRecorderWrapper = NULL;
    }
    
    return NO;
}

- (BOOL)startRecord
{
    if (pMicrophoneAudioRecorderWrapper) {
        bool ret = MicrophoneAudioRecorder_startRecord(pMicrophoneAudioRecorderWrapper);
        if (ret) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)startRecordWithStartPos:(NSTimeInterval)startPositionMs
{
    if (pMicrophoneAudioRecorderWrapper) {
        bool ret = MicrophoneAudioRecorder_startRecordWithStartPos(pMicrophoneAudioRecorderWrapper, startPositionMs*1000);
        if (ret) {
            return YES;
        }
    }
    
    return NO;
}

- (void)stopRecord
{
    if (pMicrophoneAudioRecorderWrapper) {
        MicrophoneAudioRecorder_stopRecord(pMicrophoneAudioRecorderWrapper);
    }
}

- (void)enableEarReturn:(BOOL)isEnable
{
    if (pMicrophoneAudioRecorderWrapper) {
        MicrophoneAudioRecorder_setEarReturn(pMicrophoneAudioRecorderWrapper, isEnable?true:false);
    }
}

- (void)enableNoiseSuppression:(BOOL)isEnable
{
    if (pMicrophoneAudioRecorderWrapper) {
        MicrophoneAudioRecorder_enableNoiseSuppression(pMicrophoneAudioRecorderWrapper, isEnable?true:false);
    }
}

- (NSTimeInterval)recordTimeMs
{
    if (pMicrophoneAudioRecorderWrapper) {
        return MicrophoneAudioRecorder_getRecordTimeUs(pMicrophoneAudioRecorderWrapper)/1000;
    }
    
    return 0;
}

- (NSInteger)recordPcmDB
{
    NSInteger ret = 0;
    if (pMicrophoneAudioRecorderWrapper) {
        ret = MicrophoneAudioRecorder_getRecordPcmDB(pMicrophoneAudioRecorderWrapper);
    }
    
    if (ret<MIN_DB) {
        ret = MIN_DB;
    }
    
    if (ret>MAX_DB) {
        ret = MAX_DB;
    }
    
    return ret;
}

- (BOOL)openAudioPlayer
{
    if (pMicrophoneAudioRecorderWrapper) {
        bool ret = MicrophoneAudioRecorder_openAudioPlayer(pMicrophoneAudioRecorderWrapper);
        if (ret) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)startAudioPlay
{
    if (pMicrophoneAudioRecorderWrapper) {
        bool ret = MicrophoneAudioRecorder_startAudioPlay(pMicrophoneAudioRecorderWrapper);
        if (ret) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)seekAudioPlay:(NSTimeInterval)seekTimeMs
{
    if (pMicrophoneAudioRecorderWrapper) {
        bool ret = MicrophoneAudioRecorder_seekAudioPlay(pMicrophoneAudioRecorderWrapper, seekTimeMs*1000);
        if (ret) {
            return YES;
        }
    }
    
    return NO;
}

- (void)pauseAudioPlay
{
    if (pMicrophoneAudioRecorderWrapper) {
        MicrophoneAudioRecorder_pauseAudioPlay(pMicrophoneAudioRecorderWrapper);
    }
}

- (void)closeAudioPlayer
{
    if (pMicrophoneAudioRecorderWrapper) {
        MicrophoneAudioRecorder_closeAudioPlayer(pMicrophoneAudioRecorderWrapper);
    }
}

- (NSTimeInterval)playTimeMs
{
    if (pMicrophoneAudioRecorderWrapper) {
        return MicrophoneAudioRecorder_getPlayTimeUs(pMicrophoneAudioRecorderWrapper)/1000;
    }
    
    return 0;
}

- (NSTimeInterval)playDurationMs
{
    if (pMicrophoneAudioRecorderWrapper) {
        return MicrophoneAudioRecorder_getPlayDurationUs(pMicrophoneAudioRecorderWrapper)/1000;
    }
    
    return 0;
}

- (NSInteger)playPcmDB
{
    NSInteger ret = 0;

    if (pMicrophoneAudioRecorderWrapper) {
        ret = MicrophoneAudioRecorder_getPlayPcmDB(pMicrophoneAudioRecorderWrapper);
    }
    
    if (ret<MIN_DB) {
        ret = MIN_DB;
    }
    
    if (ret>MAX_DB) {
        ret = MAX_DB;
    }
    
    return ret;
}

- (BOOL)reverseOrderGeneration
{
    if (pMicrophoneAudioRecorderWrapper) {
        if (MicrophoneAudioRecorder_reverseOrderGeneration(pMicrophoneAudioRecorderWrapper)) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)backDelete:(NSInteger)keepTimeMs
{
    if (pMicrophoneAudioRecorderWrapper) {
        if (MicrophoneAudioRecorder_backDelete(pMicrophoneAudioRecorderWrapper, keepTimeMs*1000)) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)convertToWav:(NSString*)wavFilePath
{
    if (pMicrophoneAudioRecorderWrapper) {
        bool ret = MicrophoneAudioRecorder_convertToWav(pMicrophoneAudioRecorderWrapper, (char*)[wavFilePath UTF8String]);
        if (ret) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)convertToWavWithOffset:(NSString*)wavFilePath Offset:(NSInteger)offsetMs
{
    if (pMicrophoneAudioRecorderWrapper) {
        bool ret = MicrophoneAudioRecorder_convertToWavWithOffset(pMicrophoneAudioRecorderWrapper, (char*)[wavFilePath UTF8String], offsetMs);
        if (ret) {
            return YES;
        }
    }
    
    return NO;
}

- (void)terminate
{
    if (pMicrophoneAudioRecorderWrapper) {
        MicrophoneAudioRecorder_ReleaseInstance(&pMicrophoneAudioRecorderWrapper);
        pMicrophoneAudioRecorderWrapper = NULL;
    }
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"YPPMicrophoneAudioRecorder dealloc");
}

+ (BOOL)NoiseSuppressionWavWithInput:(NSString*)inputWavFilePath WithOutput:(NSString*)outputWavFilePath
{
    if (inputWavFilePath==nil || outputWavFilePath==nil) return NO;
    
    bool ret = NoiseSuppressionWav((char*)[inputWavFilePath UTF8String], (char*)[outputWavFilePath UTF8String]);
    if (ret) return YES;
    else return NO;
}

+ (BOOL)OffsetWavWithInput:(NSString*)inputWavFilePath WithOutput:(NSString*)outputWavFilePath WithOffset:(NSInteger)offsetMs
{
    if (inputWavFilePath==nil || outputWavFilePath==nil) return NO;
    
    bool ret = OffsetWav((char*)[inputWavFilePath UTF8String], (char*)[outputWavFilePath UTF8String], offsetMs);
    if (ret) return YES;
    else return NO;
}

+ (BOOL)EffectWavWithInput:(NSString*)inputWavFilePath WithOutput:(NSString*)outputWavFilePath WithEffectType:(int)effectType WithEffect:(int)effect
{
    if (inputWavFilePath==nil || outputWavFilePath==nil) return NO;

    bool ret = EffectWav((char*)[inputWavFilePath UTF8String], (char*)[outputWavFilePath UTF8String], effectType, effect);
    if (ret) return YES;
    else return NO;
}

@end
