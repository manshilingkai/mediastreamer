//
//  DubbingScoreWrapper.h
//  MediaStreamer
//
//  Created by Think on 2019/9/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef DubbingScoreWrapper_h
#define DubbingScoreWrapper_h

#include <stdio.h>

struct DubbingScoreWrapper;

#ifdef __cplusplus
extern "C" {
#endif
    struct DubbingScoreWrapper *DubbingScore_GetInstance(char* originUrl, char* bgmUrl, char* dubUrl, char* workDir);
    void DubbingScore_ReleaseInstance(struct DubbingScoreWrapper **ppInstance);

    void DubbingScore_setListener(struct DubbingScoreWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner);
    
    void DubbingScore_start(struct DubbingScoreWrapper *pInstance);
    void DubbingScore_resume(struct DubbingScoreWrapper *pInstance);
    void DubbingScore_pause(struct DubbingScoreWrapper *pInstance);
    void DubbingScore_stop(struct DubbingScoreWrapper *pInstance);
#ifdef __cplusplus
};
#endif

#endif /* DubbingScoreWrapper_h */
