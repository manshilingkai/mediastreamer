//
//  YPPAudioPlayer.h
//  MediaStreamer
//
//  Created by Think on 2019/8/1.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AudioPlayerCommon.h"
#import "AudioPlayerDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface YPPAudioPlayerOptions : NSObject
@property (nonatomic) int audioPlayerType;
@property (nonatomic) BOOL isControlAudioSession;
@property (nonatomic) BOOL isDubbingMode;
@end

@interface YPPAudioPlayer : NSObject

- (void)initialize:(YPPAudioPlayerOptions*)options;
- (void)initialize;

- (void)setDataSource:(NSURL*)url;
- (void)setDataSourceWithUrl:(NSString*)url;

- (void)prepare;
- (void)prepareAsync;
- (void)prepareAsyncToPlay;
- (void)play;
- (BOOL)isPlaying;
- (void)pause;
- (void)stop;
- (void)seekTo:(NSTimeInterval)seekPosMs;

- (void)setVolume:(NSTimeInterval)volume;
- (void)setPlayRate:(NSTimeInterval)playrate;
- (void)setLooping:(BOOL)isLooping;

- (void)terminate;

@property (nonatomic, weak) id<AudioPlayerDelegate> delegate;

@property (nonatomic, readonly) NSTimeInterval durationMs;
@property (nonatomic, readonly) NSTimeInterval currentPlaybackTimeMs;
@property (nonatomic, readonly) NSInteger pcmDB; //40DB-80DB

@end

NS_ASSUME_NONNULL_END
