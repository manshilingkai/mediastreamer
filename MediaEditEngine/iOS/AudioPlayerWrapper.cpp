//
//  AudioPlayerWrapper.cpp
//  MediaStreamer
//
//  Created by Think on 2019/8/1.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "AudioPlayerWrapper.h"
#include "AudioPlayer.h"

#ifdef __cplusplus
extern "C" {
#endif

    struct AudioPlayerWrapper{
        AudioPlayer* audioPlayer;
        
        AudioPlayerWrapper()
        {
            audioPlayer = NULL;
        }
    };
    
    struct AudioPlayerWrapper *AudioPlayer_GetInstance()
    {
        AudioRenderConfigure audioRenderConfigure;
        audioRenderConfigure.channelCount = 2;
        audioRenderConfigure.sampleFormat = AV_SAMPLE_FMT_S16;
        audioRenderConfigure.sampleRate = 44100;
        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_STEREO;
        
        AudioPlayerWrapper* pInstance = new AudioPlayerWrapper;
        pInstance->audioPlayer = new AudioPlayer(AUDIO_PLAYER_INTERNAL_RENDER, audioRenderConfigure);
        
        return pInstance;
    }

    struct AudioPlayerWrapper *AudioPlayer_GetInstanceWithRenderAndMixOptions(int sampleRate, int channelCount, bool externalRender)
    {
        AudioRenderConfigure audioRenderConfigure;
        audioRenderConfigure.channelCount = channelCount;
        audioRenderConfigure.sampleFormat = AV_SAMPLE_FMT_S16;
        audioRenderConfigure.sampleRate = sampleRate;
        if (audioRenderConfigure.channelCount<=1) {
            audioRenderConfigure.channelLayout = AV_CH_LAYOUT_MONO;
        }else{
            audioRenderConfigure.channelLayout = AV_CH_LAYOUT_STEREO;
        }
        
        AudioPlayerWrapper* pInstance = new AudioPlayerWrapper;
        if (externalRender) {
            pInstance->audioPlayer = new AudioPlayer(AUDIO_PLAYER_EXTERNAL_RENDER_SIMPLE, audioRenderConfigure);
        }else{
            pInstance->audioPlayer = new AudioPlayer(AUDIO_PLAYER_INTERNAL_RENDER_EXTERNAL_MIX, audioRenderConfigure);
        }
        
        return pInstance;
    }

    
    void AudioPlayer_ReleaseInstance(struct AudioPlayerWrapper **ppInstance)
    {
        AudioPlayerWrapper* pInstance  = *ppInstance;
        if (pInstance!=NULL) {
            if (pInstance->audioPlayer!=NULL) {
                delete pInstance->audioPlayer;
                pInstance->audioPlayer = NULL;
            }
            
            delete pInstance;
            pInstance = NULL;
        }
    }
    
    void AudioPlayer_setListener(struct AudioPlayerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            pInstance->audioPlayer->setListener(listener,owner);
        }
    }
    
    void AudioPlayer_setDataSource(struct AudioPlayerWrapper *pInstance, char* url)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            pInstance->audioPlayer->setDataSource(url);
        }
    }
    
    void AudioPlayer_prepare(struct AudioPlayerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            pInstance->audioPlayer->prepare();
        }
    }
    
    void AudioPlayer_prepareAsync(struct AudioPlayerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            pInstance->audioPlayer->prepareAsync();
        }
    }
    
    void AudioPlayer_prepareAsyncToPlay(struct AudioPlayerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            pInstance->audioPlayer->prepareAsyncToPlay();
        }
    }
    
    void AudioPlayer_play(struct AudioPlayerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            pInstance->audioPlayer->play();
        }
    }
    
    bool AudioPlayer_isPlaying(struct AudioPlayerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            return pInstance->audioPlayer->isPlaying();
        }
        
        return false;
    }
    
    void AudioPlayer_pause(struct AudioPlayerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            pInstance->audioPlayer->pause();
        }
    }
    
    void AudioPlayer_stop(struct AudioPlayerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            pInstance->audioPlayer->stop();
        }
    }
    
    void AudioPlayer_seekTo(struct AudioPlayerWrapper *pInstance, int32_t seekPosMs)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            pInstance->audioPlayer->seekTo(seekPosMs);
        }
    }
    
    int32_t AudioPlayer_getPlayTimeMs(struct AudioPlayerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            return pInstance->audioPlayer->getPlayTimeMs();
        }
        
        return 0;
    }
    
    int32_t AudioPlayer_getDurationMs(struct AudioPlayerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            return pInstance->audioPlayer->getDurationMs();
        }
        
        return 0;
    }
    
    int AudioPlayer_getPcmDB(struct AudioPlayerWrapper *pInstance)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            return pInstance->audioPlayer->getPcmDB();
        }
        
        return 0;
    }
    
    void AudioPlayer_setVolume(struct AudioPlayerWrapper *pInstance, float volume)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            pInstance->audioPlayer->setVolume(volume);
        }
    }
    
    void AudioPlayer_setPlayRate(struct AudioPlayerWrapper *pInstance, float playrate)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            pInstance->audioPlayer->setPlayRate(playrate);
        }
    }
    
    void AudioPlayer_setLooping(struct AudioPlayerWrapper *pInstance, bool isLooping)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            pInstance->audioPlayer->setLooping(isLooping);
        }
    }

    void AudioPlayer_mixWithExternal(struct AudioPlayerWrapper *pInstance, char *externalInputPcmData, int externalInputPcmSize)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            pInstance->audioPlayer->mixWithExternal((uint8_t*)externalInputPcmData, externalInputPcmSize);
        }
    }

    int AudioPlayer_drainPCMDataFromExternal(struct AudioPlayerWrapper *pInstance, void **pData, int size)
    {
        if (pInstance!=NULL && pInstance->audioPlayer!=NULL) {
            return pInstance->audioPlayer->drainPCMDataFromExternal(pData, size);
        }
        
        return 0;
    }

#ifdef __cplusplus
};
#endif
