//
//  DubbingScoreWrapper.cpp
//  MediaStreamer
//
//  Created by Think on 2019/9/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "DubbingScoreWrapper.h"
#include "AudioSimilarity.h"

#ifdef __cplusplus
extern "C" {
#endif

struct DubbingScoreWrapper{
    AudioSimilarity* dubbingScore;
    
    DubbingScoreWrapper()
    {
        dubbingScore = NULL;
    }
};

struct DubbingScoreWrapper *DubbingScore_GetInstance(char* originUrl, char* bgmUrl, char* dubUrl, char* workDir)
{
    DubbingScoreWrapper* pInstance = new DubbingScoreWrapper;
    pInstance->dubbingScore = AudioSimilarity::CreateAudioSimilarity(CORRELATION_COEFFICIENT, originUrl, bgmUrl, dubUrl, workDir);
    
    return pInstance;
}

void DubbingScore_ReleaseInstance(struct DubbingScoreWrapper **ppInstance)
{
    DubbingScoreWrapper* pInstance  = *ppInstance;
    if (pInstance!=NULL) {
        if (pInstance->dubbingScore!=NULL) {
            AudioSimilarity::DeleteAudioSimilarity(CORRELATION_COEFFICIENT, pInstance->dubbingScore);
            pInstance->dubbingScore = NULL;
        }
        
        delete pInstance;
        pInstance = NULL;
    }
}

void DubbingScore_setListener(struct DubbingScoreWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner)
{
    if (pInstance!=NULL && pInstance->dubbingScore!=NULL) {
        pInstance->dubbingScore->setListener(listener,owner);
    }
}

void DubbingScore_start(struct DubbingScoreWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->dubbingScore!=NULL) {
        pInstance->dubbingScore->start();
    }
}

void DubbingScore_resume(struct DubbingScoreWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->dubbingScore!=NULL) {
        pInstance->dubbingScore->resume();
    }
}

void DubbingScore_pause(struct DubbingScoreWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->dubbingScore!=NULL) {
        pInstance->dubbingScore->pause();
    }
}

void DubbingScore_stop(struct DubbingScoreWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->dubbingScore!=NULL) {
        pInstance->dubbingScore->stop();
    }
}

#ifdef __cplusplus
};
#endif
