//
//  SysMediaTranscoder.h
//  MediaStreamer
//
//  Created by Think on 2019/11/22.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SysMediaTranscoderDelegate <NSObject>
@required
- (void)onTranscodingFinished:(NSString *_Nullable)outputPath;
- (void)onTranscodingFailed:(NSError * _Nullable)error;
@optional
@end

@interface SysMediaTranscoderOptions : NSObject
@property (nonatomic, strong) NSURL *inputUrl;
@property (nonatomic) long startPos;//ms
@property (nonatomic) long endPos;//ms
@property (nonatomic, strong) NSString *outputPath;
@end

@interface SysMediaTranscoder : NSObject

- (instancetype)initWithOptions:(SysMediaTranscoderOptions*)options;

- (void)startTranscoding;
- (void)cancelTranscoding;
- (float)getTranscodingProgress;

@property (nonatomic, weak) id<SysMediaTranscoderDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
