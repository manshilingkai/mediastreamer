//
//  MediaDubbingProducerWrapper.cpp
//  MediaStreamer
//
//  Created by Think on 2019/7/22.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "MediaDubbingProducerWrapper.h"
#include "MediaDubbingProducer.h"

#ifdef __cplusplus
extern "C" {
#endif

    struct MediaDubbingProducerWrapper{
        MediaDubbingProducer* mediaDubbingProducer;
        
        MediaDubbingProducerWrapper()
        {
            mediaDubbingProducer = NULL;
        }
    };
    
struct MediaDubbingProducerWrapper *MediaDubbingProducer_GetInstance(char* videoUrl, char* bgmUrl, float bgmVolume, char* dubUrl, float dubVolume, char* productUrl)
{
    MediaDubbingProducerWrapper* pInstance = new MediaDubbingProducerWrapper;
    
    MediaDubbingVideoOptions videoOptions;
    videoOptions.videoUrl = videoUrl;
    
    MediaDubbingBGMOptions bgmOptions;
    bgmOptions.bgmUrl = bgmUrl;
    bgmOptions.bgmVolume = bgmVolume;
    
    MediaDubbingDubOptions dubOptions;
    dubOptions.dubUrl = dubUrl;
    dubOptions.dubVolume = dubVolume;
    
    MediaDubbingProductOptions productOptions;
    productOptions.productUrl = productUrl;
    
    pInstance->mediaDubbingProducer = new MediaDubbingProducer(videoOptions, bgmOptions, dubOptions, productOptions);
    
    return pInstance;
}

void MediaDubbingProducer_ReleaseInstance(struct MediaDubbingProducerWrapper **ppInstance)
{
    MediaDubbingProducerWrapper* pInstance  = *ppInstance;
    if (pInstance!=NULL) {
        if (pInstance->mediaDubbingProducer!=NULL) {
            delete pInstance->mediaDubbingProducer;
            pInstance->mediaDubbingProducer = NULL;
        }
        
        delete pInstance;
        pInstance = NULL;
    }
}

void MediaDubbingProducer_setListener(struct MediaDubbingProducerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner)
{
    if (pInstance!=NULL && pInstance->mediaDubbingProducer!=NULL) {
        pInstance->mediaDubbingProducer->setListener(listener,owner);
    }
}

void MediaDubbingProducer_start(struct MediaDubbingProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaDubbingProducer!=NULL) {
        pInstance->mediaDubbingProducer->start();
    }
}

void MediaDubbingProducer_resume(struct MediaDubbingProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaDubbingProducer!=NULL) {
        pInstance->mediaDubbingProducer->resume();
    }
}

void MediaDubbingProducer_pause(struct MediaDubbingProducerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->mediaDubbingProducer!=NULL) {
        pInstance->mediaDubbingProducer->pause();
    }
}

void MediaDubbingProducer_stop(struct MediaDubbingProducerWrapper *pInstance, bool isCancle)
{
    if (pInstance!=NULL && pInstance->mediaDubbingProducer!=NULL) {
        pInstance->mediaDubbingProducer->stop(isCancle);
    }
}

#ifdef __cplusplus
};
#endif
