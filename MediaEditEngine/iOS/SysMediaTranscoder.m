//
//  SysMediaTranscoder.m
//  MediaStreamer
//
//  Created by Think on 2019/11/22.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "SysMediaTranscoder.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>

#define KMaxVideoSideSize               1920.0f
#define KMaxVideoBitRate                8*1024*1024
#define KVideoFps                       24
#define KVideoMaxFps                    30
#define KMaxKeyFrameIntervalMs          5000
#define KAudioSampleRate                44100
#define KAudioChannels                  2
#define KAudioBitRate                   128*1024

@implementation SysMediaTranscoderOptions
- (instancetype) init
{
    self = [super init];
    if (self) {
        self.startPos = -1;
        self.endPos = -1;
    }
    
    return self;
}
@end

#if 0
@interface SysMediaTranscoder () {}

@property (nonatomic, strong) AVAsset *inputAsset;
@property (nonatomic, strong) NSURL *outputURL;

@property (nonatomic, strong) dispatch_queue_t mainSerializationQueue;
@property (nonatomic, strong) dispatch_queue_t rwAudioSerializationQueue;
@property (nonatomic, strong) dispatch_queue_t rwVideoSerializationQueue;

@property (nonatomic, strong) dispatch_group_t dispatchGroup;

@property (nonatomic, strong) AVAssetReader *assetReader;
@property (nonatomic, strong) AVAssetWriter *assetWriter;

@property (nonatomic, strong) AVAssetReaderTrackOutput *assetReaderVideoOutput;
@property (nonatomic, strong) AVAssetReaderTrackOutput *assetReaderAudioOutput;

@property (nonatomic, strong) AVAssetWriterInput *assetWriterAudioInput;
@property (nonatomic, strong) AVAssetWriterInput *assetWriterVideoInput;

@property (nonatomic, assign) BOOL cancelled;
@property (nonatomic, assign) BOOL audioFinished;
@property (nonatomic, assign) BOOL videoFinished;

@property (nonatomic) long startPos;
@property (nonatomic) long endPos;

@property (nonatomic) BOOL gotFirstVideoTimeStamp;
@property (nonatomic) long firstVideoTimeStamp;
@property (nonatomic) long currentVideoReadPos;

@property (nonatomic) BOOL gotFirstAudioTimeStamp;
@property (nonatomic) long firstAudioTimeStamp;
@property (nonatomic) long currentAudioReadPos;

@property (atomic) float progress;

@end

@implementation SysMediaTranscoder
{
    int targetVideoFps;
    bool isNeedDropVideoFrame;
    long targetVideoTimeStamp;
}

- (CGSize)makeVideoEncodeSize:(CGSize)originSize {
    CGSize exportSize = originSize;

    if (originSize.width<=KMaxVideoSideSize && originSize.height<=KMaxVideoSideSize) {
        return exportSize;
    }
    
    if (originSize.width > originSize.height) {
        exportSize.width = KMaxVideoSideSize;
        exportSize.height = exportSize.width * originSize.height / originSize.width;
    }else{
        exportSize.height = KMaxVideoSideSize;
        exportSize.width = exportSize.height * originSize.width / originSize.height;
    }
    
    NSUInteger finalWidth = exportSize.width;
    NSUInteger finalHeight = exportSize.height;
    
    if (finalWidth%2 != 0) {
        finalWidth++;
    }
    
    if (finalHeight%2 != 0) {
        finalHeight++;
    }
    
    if (finalWidth==0 || finalWidth==0) {
        finalWidth = 720;
        finalHeight = 1280;
    }
    
    exportSize.width = finalWidth;
    exportSize.height = finalHeight;
    
    return exportSize;
}

- (long)makeVideoEncodeBitrate:(CGSize)encodeSize {
    if (encodeSize.width<=640.0f && encodeSize.height<=640.0f) {
        return 1024*1024;
    }else if (encodeSize.width<=960.0f && encodeSize.height<=960.0f) {
        return 2*1024*1024;
    }else if (encodeSize.width<=1280.0f && encodeSize.height<=1280.0f) {
        return 4*1024*1024;
    }else if (encodeSize.width<=1600.0f && encodeSize.height<=1600.0f) {
        return 6*1024*1024;
    }else if (encodeSize.width<=1920.0f && encodeSize.height<=1920.0f) {
        return 8*1024*1024;
    }else return KMaxVideoBitRate;
}

-(int)makeTargetVideoFps:(float)originVideoFps
{
    NSLog(@"OriginVideoFps : %f", originVideoFps);
    
    int outputVideoFps = KVideoFps;
    
    if (originVideoFps<8.0f) {
        outputVideoFps = KVideoFps;
    }else {
        outputVideoFps = originVideoFps;
    }
    
    return outputVideoFps;
}

- (instancetype)initWithOptions:(SysMediaTranscoderOptions*)options
{
    self = [super init];
    if (self) {
        self.startPos = [options startPos];
        self.endPos = [options endPos];
        
        self.gotFirstVideoTimeStamp = NO;
        self.firstVideoTimeStamp = 0;
        self.currentVideoReadPos = 0;
        
        self.gotFirstAudioTimeStamp = NO;
        self.firstAudioTimeStamp = 0;
        self.currentAudioReadPos = 0;
        
        self.progress = 0.0f;
        
        self.inputAsset = [AVAsset assetWithURL:[options inputUrl]];
        if (self.inputAsset) {
//            NSArray *audioTracks = [self.inputAsset tracksWithMediaType:AVMediaTypeAudio];
            NSArray *videoTracks = [self.inputAsset tracksWithMediaType:AVMediaTypeVideo];
            NSString* outputPath = [options outputPath];
            if ([outputPath hasSuffix:@".MOV"]) {
                [outputPath stringByReplacingOccurrencesOfString:@".MOV" withString:@".mp4"];
            }
            if ([outputPath hasSuffix:@".mov"]) {
                [outputPath stringByReplacingOccurrencesOfString:@".mov" withString:@".mp4"];
            }
            if (videoTracks==nil || videoTracks.count==0) {
                if ([outputPath hasSuffix:@".mp4"]) {
                    [outputPath stringByReplacingOccurrencesOfString:@".mp4" withString:@".m4a"];
                }
            }
            
            self.outputURL = [NSURL fileURLWithPath:outputPath];
        }
    }
    return self;
}

- (void)startTranscoding
{
    if (!self.inputAsset) return;
    
    [self.inputAsset loadValuesAsynchronouslyForKeys:@[@"tracks"] completionHandler:^{
        // Once the tracks have finished loading, dispatch the work to the main serialization queue.
        dispatch_async(self.mainSerializationQueue, ^{
            // Due to asynchronous nature, check to see if user has already cancelled.
            if (self.cancelled)
                return;
            BOOL success = YES;
            NSError *localError = nil;
            // Check for success of loading the assets tracks.
            success = ([self.inputAsset statusOfValueForKey:@"tracks" error:&localError] == AVKeyValueStatusLoaded);
            if (success){
                // If the tracks loaded successfully, make sure that no file exists at the output path for the asset writer.
                NSFileManager *fm = [NSFileManager defaultManager];
                NSString *localOutputPath = [self.outputURL path];
                if ([fm fileExistsAtPath:localOutputPath])
                    success = [fm removeItemAtPath:localOutputPath error:&localError];
            }
            if (success)
                success = [self setupAssetReaderAndAssetWriter:&localError];
            if (success)
                success = [self startAssetReaderAndAssetWriter:&localError];
            if (!success)
                [self readingAndWritingDidFinishSuccessfully:success withError:localError];
        });
    }];
}

- (BOOL)setupAssetReaderAndAssetWriter:(NSError **)outError {
    // Create and initialize the asset reader.
    self.assetReader = [[AVAssetReader alloc] initWithAsset:self.inputAsset error:outError];
    BOOL success = (self.assetReader != nil);
    if (success) {
        // If the asset reader was successfully initialized, do the same for the asset writer.
        self.assetWriter = [[AVAssetWriter alloc] initWithURL:self.outputURL fileType:AVFileTypeMPEG4 error:outError];
        self.assetWriter.shouldOptimizeForNetworkUse = YES;
        success = (self.assetWriter != nil);
    }
    
    if (success){
        // If the reader and writer were successfully initialized, grab the audio and video asset tracks that will be used.
        AVAssetTrack *assetAudioTrack = nil, *assetVideoTrack = nil;
        NSArray *audioTracks = [self.inputAsset tracksWithMediaType:AVMediaTypeAudio];
        if (audioTracks && [audioTracks count] > 0)
            assetAudioTrack = [audioTracks objectAtIndex:0];
        NSArray *videoTracks = [self.inputAsset tracksWithMediaType:AVMediaTypeVideo];
        if (videoTracks && [videoTracks count] > 0)
            assetVideoTrack = [videoTracks objectAtIndex:0];
        
        if (assetAudioTrack) {
            // If there is an audio track to read, set the decompression settings to Linear PCM and create the asset reader output.
            NSDictionary *decompressionAudioSettings = @{ AVFormatIDKey : [NSNumber numberWithUnsignedInt:kAudioFormatLinearPCM],
                                                          AVSampleRateKey       : [NSNumber numberWithInteger:KAudioSampleRate],
                                                          AVNumberOfChannelsKey : [NSNumber numberWithUnsignedInteger:KAudioChannels]
                                                          };
            self.assetReaderAudioOutput = [AVAssetReaderTrackOutput assetReaderTrackOutputWithTrack:assetAudioTrack outputSettings:decompressionAudioSettings];
            [self.assetReader addOutput:self.assetReaderAudioOutput];
            
            // Then, set the compression settings to 128kbps AAC and create the asset writer input.
            AudioChannelLayout stereoChannelLayout = {
                .mChannelLayoutTag = kAudioChannelLayoutTag_Stereo,
                .mChannelBitmap = 0,
                .mNumberChannelDescriptions = 0
            };
            NSData *channelLayoutAsData = [NSData dataWithBytes:&stereoChannelLayout length:offsetof(AudioChannelLayout, mChannelDescriptions)];
            NSDictionary *compressionAudioSettings = @{
                                                       AVFormatIDKey         : [NSNumber numberWithUnsignedInt:kAudioFormatMPEG4AAC],
                                                       AVEncoderBitRateKey   : [NSNumber numberWithInteger:KAudioBitRate],
                                                       AVSampleRateKey       : [NSNumber numberWithInteger:KAudioSampleRate],
                                                       AVChannelLayoutKey    : channelLayoutAsData,
                                                       AVNumberOfChannelsKey : [NSNumber numberWithUnsignedInteger:KAudioChannels]
                                                       };
            
            self.assetWriterAudioInput = [AVAssetWriterInput assetWriterInputWithMediaType:[assetAudioTrack mediaType] outputSettings:compressionAudioSettings];
            [self.assetWriter addInput:self.assetWriterAudioInput];
        }
        
        if (assetVideoTrack) {
            // If there is a video track to read, set the decompression settings for YUV and create the asset reader output.
            NSDictionary *decompressionVideoSettings = @{
                                                         (id)kCVPixelBufferPixelFormatTypeKey     : [NSNumber numberWithUnsignedInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange],
                                                         (id)kCVPixelBufferIOSurfacePropertiesKey : [NSDictionary dictionary]
                                                         };
            self.assetReaderVideoOutput = [AVAssetReaderTrackOutput assetReaderTrackOutputWithTrack:assetVideoTrack outputSettings:decompressionVideoSettings];
            [self.assetReader addOutput:self.assetReaderVideoOutput];
            
            CMFormatDescriptionRef formatDescription = nil;
            // Grab the video format descriptions from the video track and grab the first one if it exists.
            NSArray *videoFormatDescriptions = [assetVideoTrack formatDescriptions];
            if (videoFormatDescriptions && [videoFormatDescriptions count] > 0)
                formatDescription = (__bridge CMFormatDescriptionRef)[videoFormatDescriptions objectAtIndex:0];
            CGSize trackDimensions = {
                .width = 0.f,
                .height = 0.f,
            };
            // If the video track had a format description, grab the track dimensions from there. Otherwise, grab them direcly from the track itself.
            NSMutableDictionary *compressionSettings = [NSMutableDictionary dictionary];
            if (formatDescription)
                trackDimensions = CMVideoFormatDescriptionGetPresentationDimensions(formatDescription, false, false);
            else
                trackDimensions = [assetVideoTrack naturalSize];
            
            targetVideoFps = [self makeTargetVideoFps:[assetVideoTrack nominalFrameRate]];
            if (targetVideoFps>KVideoMaxFps) {
                targetVideoFps = KVideoMaxFps;
                isNeedDropVideoFrame = true;
                targetVideoTimeStamp = 0;
            }else {
                isNeedDropVideoFrame = false;
            }
            
            // If the video track had a format description, attempt to grab the clean aperture settings and pixel aspect ratio used by the video.
            if (formatDescription) {
                NSDictionary *cleanAperture = nil;
                NSDictionary *pixelAspectRatio = nil;
                CFDictionaryRef cleanApertureFromCMFormatDescription = (CFDictionaryRef)CMFormatDescriptionGetExtension(formatDescription, kCMFormatDescriptionExtension_CleanAperture);
                if (cleanApertureFromCMFormatDescription) {
                    cleanAperture = @{
                                      AVVideoCleanApertureWidthKey            : (id)CFDictionaryGetValue(cleanApertureFromCMFormatDescription, kCMFormatDescriptionKey_CleanApertureWidth),
                                      AVVideoCleanApertureHeightKey           : (id)CFDictionaryGetValue(cleanApertureFromCMFormatDescription, kCMFormatDescriptionKey_CleanApertureHeight),
                                      AVVideoCleanApertureHorizontalOffsetKey : (id)CFDictionaryGetValue(cleanApertureFromCMFormatDescription, kCMFormatDescriptionKey_CleanApertureHorizontalOffset),
                                      AVVideoCleanApertureVerticalOffsetKey   : (id)CFDictionaryGetValue(cleanApertureFromCMFormatDescription, kCMFormatDescriptionKey_CleanApertureVerticalOffset)
                                      };
                }
                CFDictionaryRef pixelAspectRatioFromCMFormatDescription = (CFDictionaryRef)CMFormatDescriptionGetExtension(formatDescription, kCMFormatDescriptionExtension_PixelAspectRatio);
                if (pixelAspectRatioFromCMFormatDescription) {
                    pixelAspectRatio = @{
                                         AVVideoPixelAspectRatioHorizontalSpacingKey : (id)CFDictionaryGetValue(pixelAspectRatioFromCMFormatDescription, kCMFormatDescriptionKey_PixelAspectRatioHorizontalSpacing),
                                         AVVideoPixelAspectRatioVerticalSpacingKey   : (id)CFDictionaryGetValue(pixelAspectRatioFromCMFormatDescription, kCMFormatDescriptionKey_PixelAspectRatioVerticalSpacing)
                                         };
                }
                // Add whichever settings we could grab from the format description to the compression settings dictionary.
                if (cleanAperture || pixelAspectRatio) {
                    NSMutableDictionary *mutableCompressionSettings = [NSMutableDictionary dictionary];
                    if (cleanAperture)
                        [mutableCompressionSettings setObject:cleanAperture forKey:AVVideoCleanApertureKey];
                    if (pixelAspectRatio)
                        [mutableCompressionSettings setObject:pixelAspectRatio forKey:AVVideoPixelAspectRatioKey];
                    compressionSettings = mutableCompressionSettings;
                }
            }
            
            // Create the video settings dictionary for H.264.
            NSMutableDictionary *videoSettings = [NSMutableDictionary dictionary];
            if (@available(iOS 11.0,*)) {
                [videoSettings setObject:AVVideoCodecTypeH264 forKey:AVVideoCodecKey];
            }else{
                [videoSettings setObject:AVVideoCodecH264 forKey:AVVideoCodecKey];
            }
//            [videoSettings setObject:AVVideoCodecH264 forKey:AVVideoCodecKey];

            trackDimensions = [self makeVideoEncodeSize:trackDimensions];
            [videoSettings setObject:[NSNumber numberWithDouble:trackDimensions.width] forKey:AVVideoWidthKey];
            [videoSettings setObject:[NSNumber numberWithDouble:trackDimensions.height] forKey:AVVideoHeightKey];
            
            [compressionSettings setObject:AVVideoProfileLevelH264High41 forKey:AVVideoProfileLevelKey];
            [compressionSettings setObject:@(targetVideoFps) forKey:AVVideoExpectedSourceFrameRateKey];
            [compressionSettings setObject:@(targetVideoFps*KMaxKeyFrameIntervalMs/1000) forKey:AVVideoMaxKeyFrameIntervalKey];
            [compressionSettings setObject:@([self makeVideoEncodeBitrate:trackDimensions]) forKey:AVVideoAverageBitRateKey];
            
            // Put the compression settings into the video settings dictionary if we were able to grab them.
            if (compressionSettings)
                [videoSettings setObject:compressionSettings forKey:AVVideoCompressionPropertiesKey];
            // Create the asset writer input and add it to the asset writer.
            self.assetWriterVideoInput = [AVAssetWriterInput assetWriterInputWithMediaType:[assetVideoTrack mediaType] outputSettings:videoSettings];
            self.assetWriterVideoInput.transform = assetVideoTrack.preferredTransform;
            [self.assetWriter addInput:self.assetWriterVideoInput];
        }
        
        if (assetAudioTrack==nil && assetVideoTrack==nil) {
            success = NO;
        }
    }
    return success;
}

- (CMSampleBufferRef)adjustTime:(CMSampleBufferRef)sample by:(CMTime)offset {
    CMItemCount count;
    CMSampleBufferGetSampleTimingInfoArray(sample, 0, nil, &count);
    CMSampleTimingInfo* pInfo = (CMSampleTimingInfo*)malloc(sizeof(CMSampleTimingInfo) * count);
    CMSampleBufferGetSampleTimingInfoArray(sample, count, pInfo, &count);
    for (CMItemCount i = 0; i < count; i++) {
        pInfo[i].decodeTimeStamp = CMTimeSubtract(pInfo[i].decodeTimeStamp, offset);
        pInfo[i].presentationTimeStamp = CMTimeSubtract(pInfo[i].presentationTimeStamp, offset);
    }
    CMSampleBufferRef sout;
    CMSampleBufferCreateCopyWithNewTiming(nil, sample, count, pInfo, &sout);
    free(pInfo);
    return sout;
}

- (BOOL)startAssetReaderAndAssetWriter:(NSError **)outError {
    BOOL success = YES;
    // Attempt to start the asset reader.
    success = [self.assetReader startReading];
    if (!success)
        *outError = [self.assetReader error];
    if (success) {
        // If the reader started successfully, attempt to start the asset writer.
        success = [self.assetWriter startWriting];
        if (!success)
            *outError = [self.assetWriter error];
    }
    if (success) {
        // If the asset reader and writer both started successfully, create the dispatch group where the reencoding will take place and start a sample-writing session.
        self.dispatchGroup = dispatch_group_create();
        [self.assetWriter startSessionAtSourceTime:kCMTimeZero];
        self.audioFinished = NO;
        self.videoFinished = NO;
        
        if (self.assetWriterAudioInput) {
            // If there is audio to reencode, enter the dispatch group before beginning the work.
            dispatch_group_enter(self.dispatchGroup);
            // Specify the block to execute when the asset writer is ready for audio media data, and specify the queue to call it on.
            [self.assetWriterAudioInput requestMediaDataWhenReadyOnQueue:self.rwAudioSerializationQueue usingBlock:^{
                // Because the block is called asynchronously, check to see whether its task is complete.
                if (self.audioFinished)
                    return;
                BOOL completedOrFailed = NO;
                // If the task isn't complete yet, make sure that the input is actually ready for more media data.
                while ([self.assetWriterAudioInput isReadyForMoreMediaData] && !completedOrFailed) {
                    // Get the next audio sample buffer, and append it to the output file.
                    CMSampleBufferRef sampleBuffer = [self.assetReaderAudioOutput copyNextSampleBuffer];
                    if (sampleBuffer != NULL)
                    {
                        CMTime sampleBufferTimeStamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
                        if (!self.gotFirstAudioTimeStamp) {
                            self.gotFirstAudioTimeStamp = YES;
                            self.firstAudioTimeStamp = 1000*sampleBufferTimeStamp.value / sampleBufferTimeStamp.timescale;
                        }
                        self.currentAudioReadPos = 1000*sampleBufferTimeStamp.value / sampleBufferTimeStamp.timescale - self.firstAudioTimeStamp;
                        
                        if (self.startPos>=0 && self.endPos>0 && self.endPos>self.startPos) {
                            if (self.currentAudioReadPos<self.startPos) {
                                CFRelease(sampleBuffer);
                                sampleBuffer = NULL;
                                completedOrFailed = NO;
                                continue;
                            }else if(self.currentAudioReadPos>self.endPos){
                                CFRelease(sampleBuffer);
                                sampleBuffer = NULL;
                                completedOrFailed = YES;
                                break;
                            }else{
                                if (!self.assetWriterVideoInput) {
                                    self.progress = ((float)(self.currentAudioReadPos - self.startPos))/((float)(self.endPos - self.startPos));
                                    if (self.progress<0.0f) {
                                        self.progress = 0.0f;
                                    }
                                    if (self.progress>1.0f) {
                                        self.progress = 1.0f;
                                    }
                                }
                                CMSampleBufferRef ret = [self adjustTime:sampleBuffer by:CMTimeMakeWithSeconds((Float64)(self.startPos)/1000.0f, sampleBufferTimeStamp.timescale)];
                                CFRelease(sampleBuffer);
                                sampleBuffer = ret;
                            }
                        }else{
                            if (!self.assetWriterVideoInput) {
                                CMTime durationTime = [self.inputAsset duration];
                                long duration = 1000*durationTime.value / durationTime.timescale;
                                if (duration==0) {
                                    self.progress = 0.0f;
                                }else{
                                    self.progress = ((float)self.currentAudioReadPos)/((float)duration);
                                }
                                if (self.progress<0.0f) {
                                    self.progress = 0.0f;
                                }
                                if (self.progress>1.0f) {
                                    self.progress = 1.0f;
                                }
                            }
                        }
                        
                        BOOL success = [self.assetWriterAudioInput appendSampleBuffer:sampleBuffer];
                        CFRelease(sampleBuffer);
                        sampleBuffer = NULL;
                        completedOrFailed = !success;
                    }
                    else
                    {
                        completedOrFailed = YES;
                    }
                }
                if (completedOrFailed) {
                    // Mark the input as finished, but only if we haven't already done so, and then leave the dispatch group (since the audio work has finished).
                    BOOL oldFinished = self.audioFinished;
                    self.audioFinished = YES;
                    if (oldFinished == NO) {
                        [self.assetWriterAudioInput markAsFinished];
                    }
                    dispatch_group_leave(self.dispatchGroup);
                }
            }];
        }
        
        if (self.assetWriterVideoInput) {
            // If we had video to reencode, enter the dispatch group before beginning the work.
            dispatch_group_enter(self.dispatchGroup);
            // Specify the block to execute when the asset writer is ready for video media data, and specify the queue to call it on.
            [self.assetWriterVideoInput requestMediaDataWhenReadyOnQueue:self.rwVideoSerializationQueue usingBlock:^{
                // Because the block is called asynchronously, check to see whether its task is complete.
                if (self.videoFinished)
                    return;
                BOOL completedOrFailed = NO;
                // If the task isn't complete yet, make sure that the input is actually ready for more media data.
                while ([self.assetWriterVideoInput isReadyForMoreMediaData] && !completedOrFailed) {
                    // Get the next video sample buffer, and append it to the output file.
                    CMSampleBufferRef sampleBuffer = [self.assetReaderVideoOutput copyNextSampleBuffer];
                    if (sampleBuffer != NULL) {
                        CMTime sampleBufferTimeStamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
                        if (!self.gotFirstVideoTimeStamp) {
                            self.gotFirstVideoTimeStamp = YES;
                            self.firstVideoTimeStamp = 1000*sampleBufferTimeStamp.value / sampleBufferTimeStamp.timescale;
                        }
                        self.currentVideoReadPos = 1000*sampleBufferTimeStamp.value / sampleBufferTimeStamp.timescale - self.firstVideoTimeStamp;
                        
                        if (self.startPos>=0 && self.endPos>0 && self.endPos>self.startPos) {
                            if (self.currentVideoReadPos<self.startPos) {
                                CFRelease(sampleBuffer);
                                sampleBuffer = NULL;
                                completedOrFailed = NO;
                                continue;
                            }else if(self.currentVideoReadPos>self.endPos){
                                CFRelease(sampleBuffer);
                                sampleBuffer = NULL;
                                completedOrFailed = YES;
                                break;
                            }else{
                                self.progress = ((float)(self.currentVideoReadPos - self.startPos))/((float)(self.endPos - self.startPos));
                                if (self.progress<0.0f) {
                                    self.progress = 0.0f;
                                }
                                if (self.progress>1.0f) {
                                    self.progress = 1.0f;
                                }
                                
                                bool isDrop = false;
                                if (isNeedDropVideoFrame) {
                                    if (self.currentVideoReadPos - self.startPos >= targetVideoTimeStamp) {
                                        isDrop = false;
                                        targetVideoTimeStamp += 1000/targetVideoFps;
                                    }else {
                                        isDrop = true;
                                    }
                                }
                                
                                if (isDrop) {
                                    CFRelease(sampleBuffer);
                                    sampleBuffer = NULL;
                                    completedOrFailed = NO;
                                    continue;
                                }
                                
                                CMSampleBufferRef ret = [self adjustTime:sampleBuffer by:CMTimeMakeWithSeconds((Float64)(self.startPos)/1000.0f, sampleBufferTimeStamp.timescale)];
                                CFRelease(sampleBuffer);
                                sampleBuffer = ret;
                            }
                        }else{
                            CMTime durationTime = [self.inputAsset duration];
                            long duration = 1000*durationTime.value / durationTime.timescale;
                            if (duration==0) {
                                self.progress = 0.0f;
                            }else{
                                self.progress = ((float)self.currentVideoReadPos)/((float)duration);
                            }
                            if (self.progress<0.0f) {
                                self.progress = 0.0f;
                            }
                            if (self.progress>1.0f) {
                                self.progress = 1.0f;
                            }
                            
                            bool isDrop = false;
                            if (isNeedDropVideoFrame) {
                                if (self.currentVideoReadPos >= targetVideoTimeStamp) {
                                    isDrop = false;
                                    targetVideoTimeStamp += 1000/targetVideoFps;
                                }else {
                                    isDrop = true;
                                }
                            }
                            
                            if (isDrop) {
                                CFRelease(sampleBuffer);
                                sampleBuffer = NULL;
                                completedOrFailed = NO;
                                continue;
                            }
                        }
                        BOOL success = [self.assetWriterVideoInput appendSampleBuffer:sampleBuffer];
                        CFRelease(sampleBuffer);
                        sampleBuffer = NULL;
                        completedOrFailed = !success;
                        
//                        NSLog(@"%f", progress);
                    }
                    else
                    {
                        completedOrFailed = YES;
                    }
                }
                if (completedOrFailed) {
                    // Mark the input as finished, but only if we haven't already done so, and then leave the dispatch group (since the video work has finished).
                    BOOL oldFinished = self.videoFinished;
                    self.videoFinished = YES;
                    if (oldFinished == NO)  {
                        [self.assetWriterVideoInput markAsFinished];
                    }
                    dispatch_group_leave(self.dispatchGroup);
                }
            }];
        }
        // Set up the notification that the dispatch group will send when the audio and video work have both finished.
        dispatch_group_notify(self.dispatchGroup, self.mainSerializationQueue, ^{
            __block BOOL finalSuccess = YES;
            NSError *finalError = nil;
            // Check to see if the work has finished due to cancellation.
            if (self.cancelled) {
                // If so, cancel the reader and writer.
                [self.assetReader cancelReading];
                [self.assetWriter cancelWriting];
            }
            else
            {
                // If cancellation didn't occur, first make sure that the asset reader didn't fail.
                if ([self.assetReader status] == AVAssetReaderStatusFailed) {
                    finalSuccess = NO;
                    finalError = [self.assetReader error];
                    [self readingAndWritingDidFinishSuccessfully:finalSuccess withError:finalError];
                }
                // If the asset reader didn't fail, attempt to stop the asset writer and check for any errors.
                if (finalSuccess) {
                    [self.assetWriter finishWritingWithCompletionHandler:^{
                        NSLog(@"status:%ld", (long)self.assetWriter.status);
                        finalSuccess = YES;
                        [self readingAndWritingDidFinishSuccessfully:finalSuccess withError:finalError];
                    }];
                }
            }
            // Call the method to handle completion, and pass in the appropriate parameters to indicate whether reencoding was successful.
        });
    }
    // Return success here to indicate whether the asset reader and writer were started successfully.
    return success;
}

- (void)readingAndWritingDidFinishSuccessfully:(BOOL)success withError:(NSError *)error {
    if (!success) {
        // If the reencoding process failed, we need to cancel the asset reader and writer.
        [self.assetReader cancelReading];
        [self.assetWriter cancelWriting];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Handle any UI tasks here related to failure.
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onTranscodingFailed:)])) {
                    [self.delegate onTranscodingFailed:error];
                }
            }
        });
    } else {
        // Reencoding was successful, reset booleans.
        self.cancelled = NO;
        self.videoFinished = NO;
        self.audioFinished = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            // Handle any UI tasks here related to success;
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onTranscodingFinished:)])) {
                    [self.delegate onTranscodingFinished:[self.outputURL path]];
                }
            }
        });
    }
}

#pragma mark - public
- (void)cancelTranscoding {
    // Handle cancellation asynchronously, but serialize it with the main queue.
    dispatch_async(self.mainSerializationQueue, ^{
        // If we had audio data to reencode, we need to cancel the audio work.
        if (self.assetWriterAudioInput) {
            // Handle cancellation asynchronously again, but this time serialize it with the audio queue.
            dispatch_group_enter(self.dispatchGroup);
            dispatch_async(self.rwAudioSerializationQueue, ^{
                // Update the Boolean property indicating the task is complete and mark the input as finished if it hasn't already been marked as such.
                BOOL oldFinished = self.audioFinished;
                self.audioFinished = YES;
                if (oldFinished == NO) {
                    [self.assetWriterAudioInput markAsFinished];
                }
                // Leave the dispatch group since the audio work is finished now.
                dispatch_group_leave(self.dispatchGroup);
            });
        }
        
        if (self.assetWriterVideoInput) {
            // Handle cancellation asynchronously again, but this time serialize it with the video queue.
            dispatch_group_enter(self.dispatchGroup);
            dispatch_async(self.rwVideoSerializationQueue, ^{
                // Update the Boolean property indicating the task is complete and mark the input as finished if it hasn't already been marked as such.
                BOOL oldFinished = self.videoFinished;
                self.videoFinished = YES;
                if (oldFinished == NO) {
                    [self.assetWriterVideoInput markAsFinished];
                }
                // Leave the dispatch group, since the video work is finished now.
                dispatch_group_leave(self.dispatchGroup);
            });
        }
        // Set the cancelled Boolean property to YES to cancel any work on the main queue as well.
        self.cancelled = YES;
    });
}

- (float)getTranscodingProgress
{
    return self.progress;
}

// Create the main serialization queue.
- (dispatch_queue_t)mainSerializationQueue {
    if (!_mainSerializationQueue) {
        NSString *serializationQueueDescription = [NSString stringWithFormat:@"%@ serialization queue", self];
        _mainSerializationQueue = dispatch_queue_create([serializationQueueDescription UTF8String], NULL);
    }
    return _mainSerializationQueue;
}

// Create the serialization queue to use for reading and writing the audio data.
- (dispatch_queue_t)rwAudioSerializationQueue {
    if (!_rwAudioSerializationQueue) {
        NSString *rwAudioSerializationQueueDescription = [NSString stringWithFormat:@"%@ rw audio serialization queue", self];
        _rwAudioSerializationQueue = dispatch_queue_create([rwAudioSerializationQueueDescription UTF8String], NULL);
    }
    return _rwAudioSerializationQueue;
}

// Create the serialization queue to use for reading and writing the video data.
- (dispatch_queue_t)rwVideoSerializationQueue {
    if (!_rwVideoSerializationQueue) {
        NSString *rwVideoSerializationQueueDescription = [NSString stringWithFormat:@"%@ rw video serialization queue", self];
        _rwVideoSerializationQueue = dispatch_queue_create([rwVideoSerializationQueueDescription UTF8String], NULL);
    }
    return _rwVideoSerializationQueue;
}

@end

#else

@interface SysMediaTranscoder () {}
@property (nonatomic, strong) SysMediaTranscoderOptions *mSysMediaTranscoderOptions;
@property (nonatomic, strong) AVAssetExportSession *exportSession;
@end

@implementation SysMediaTranscoder
{
    
}

- (instancetype)initWithOptions:(SysMediaTranscoderOptions*)options
{
    self = [super init];
    if (self) {
        self.mSysMediaTranscoderOptions = options;
        self.exportSession = nil;
    }
    return self;
}

- (void)startTranscoding
{
    BOOL ret=[[NSFileManager defaultManager] fileExistsAtPath:[self.mSysMediaTranscoderOptions outputPath]];
    if (ret) {
        [[NSFileManager defaultManager] removeItemAtPath:[self.mSysMediaTranscoderOptions outputPath] error:nil];
    }
    
    NSURL *outputUrl = [NSURL fileURLWithPath:[self.mSysMediaTranscoderOptions outputPath]];

    AVAsset *asset = [AVAsset assetWithURL:[self.mSysMediaTranscoderOptions inputUrl]];
    self.exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPreset1920x1080];
    
    self.exportSession.outputURL = outputUrl;
    self.exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    
    if ([self.mSysMediaTranscoderOptions startPos] >=0 && [self.mSysMediaTranscoderOptions endPos]>0 && [self.mSysMediaTranscoderOptions endPos] > [self.mSysMediaTranscoderOptions startPos]) {
        CMTime start = CMTimeMakeWithSeconds(((Float64)[self.mSysMediaTranscoderOptions startPos])/1000.0f, asset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(((Float64)[self.mSysMediaTranscoderOptions endPos] - [self.mSysMediaTranscoderOptions startPos])/1000.0f, asset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        self.exportSession.timeRange = range;
    }
    
    [self.exportSession exportAsynchronouslyWithCompletionHandler:^{
        
        switch ([self.exportSession status]) {
            case AVAssetExportSessionStatusFailed:
            {
                NSLog(@"SysMediaTranscoder Fail：%@", [[self.exportSession error] description]);
                
                if (self.delegate!=nil) {
                    if (([self.delegate respondsToSelector:@selector(onTranscodingFailed:)])) {
                        [self.delegate onTranscodingFailed:[self.exportSession error]];
                    }
                }
            }
                break;
            case AVAssetExportSessionStatusCancelled:
            {
            }
                break;
            case AVAssetExportSessionStatusCompleted:
            {
                if (self.delegate!=nil) {
                    if (([self.delegate respondsToSelector:@selector(onTranscodingFinished:)])) {
                        [self.delegate onTranscodingFinished:[self.mSysMediaTranscoderOptions outputPath]];
                    }
                }
            }
                break;
            default:
            {
                if (self.delegate!=nil) {
                    if (([self.delegate respondsToSelector:@selector(onTranscodingFailed:)])) {
                        [self.delegate onTranscodingFailed:nil];
                    }
                }
            }
                break;
        }
    }];

}

- (void)cancelTranscoding
{
    if (self.exportSession) {
        [self.exportSession cancelExport];
    }
}

- (float)getTranscodingProgress
{
    if (self.exportSession) {
        return [self.exportSession progress];
    }else return 0.0f;
}

@end

#endif
