//
//  SysAudioPlayer.m
//  MediaStreamer
//
//  Created by Think on 2019/11/15.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "SysAudioPlayer.h"
#import <AVFoundation/AVFoundation.h>

@interface SysAudioPlayer () <AVAudioPlayerDelegate> {}
@property (nonatomic, strong) AVAudioPlayer *mAVAudioPlayer;
@property (nonatomic, strong) NSURL* mWorkUrl;
@end

@implementation SysAudioPlayer
{
    dispatch_queue_t audioPlayerNotificationQueue;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.mAVAudioPlayer = nil;
        self.mWorkUrl = nil;
        
        audioPlayerNotificationQueue = dispatch_queue_create("SysAudioPlayerNotificationQueue", 0);
    }
    
    return self;
}

- (void)initialize
{
    self.mAVAudioPlayer = nil;
    self.mWorkUrl = nil;
}

- (void)setDataSource:(NSURL*)url
{
    self.mWorkUrl = url;
}

- (void)setDataSourceWithUrl:(NSString*)url
{
    self.mWorkUrl = [NSURL URLWithString:url];
}

- (void)prepare
{
    if (self.mWorkUrl) {
        NSError *error = nil;
        self.mAVAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.mWorkUrl error:&error];
        self.mAVAudioPlayer.delegate = self;
        [self.mAVAudioPlayer prepareToPlay];
    }
}

- (void)prepareAsync
{
    if (self.mWorkUrl) {
        NSError *error = nil;
        self.mAVAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.mWorkUrl error:&error];
        self.mAVAudioPlayer.delegate = self;
        [self.mAVAudioPlayer prepareToPlay];
        
        __weak typeof(self) wself = self;
        dispatch_async(audioPlayerNotificationQueue, ^{
            __strong typeof(wself) strongSelf = wself;
            if(strongSelf!=nil)
            {
                if (strongSelf.delegate!=nil) {
                    if (([strongSelf.delegate respondsToSelector:@selector(onPrepared)])) {
                        [strongSelf.delegate onPrepared];
                    }
                }
            }
        });
    }
}

- (void)prepareAsyncToPlay
{
    if (self.mWorkUrl) {
        NSError *error = nil;
        self.mAVAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.mWorkUrl error:&error];
        self.mAVAudioPlayer.delegate = self;
        [self.mAVAudioPlayer prepareToPlay];
        
        [self.mAVAudioPlayer play];
    }
}

- (void)play
{
    if (self.mAVAudioPlayer) {
        [self.mAVAudioPlayer play];
    }
}

- (BOOL)isPlaying
{
    if (self.mAVAudioPlayer) {
        return [self.mAVAudioPlayer isPlaying];
    }
    
    return NO;
}

- (void)pause
{
    if (self.mAVAudioPlayer) {
        [self.mAVAudioPlayer pause];
    }
}

- (void)stop
{
    if (self.mAVAudioPlayer) {
        [self.mAVAudioPlayer stop];
        self.mAVAudioPlayer = nil;
    }
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    if (self.mAVAudioPlayer) {
        [self.mAVAudioPlayer setCurrentTime:seekPosMs/1000.0f];
    }
}

- (void)setVolume:(NSTimeInterval)volume
{
    if (self.mAVAudioPlayer) {
        [self.mAVAudioPlayer setVolume:volume];
    }
}

- (void)setPlayRate:(NSTimeInterval)playrate
{
    if (self.mAVAudioPlayer) {
        [self.mAVAudioPlayer setRate:playrate];
    }
}

- (void)setLooping:(BOOL)isLooping
{
    if (self.mAVAudioPlayer) {
        if (isLooping) {
            self.mAVAudioPlayer.numberOfLoops = -1;
        }else{
            self.mAVAudioPlayer.numberOfLoops = 0;
        }
    }
}

- (NSTimeInterval)currentPlaybackTimeMs
{
    if (self.mAVAudioPlayer) {
        return self.mAVAudioPlayer.currentTime*1000;
    }
    
    return 0;
}

- (NSTimeInterval)durationMs
{
    if (self.mAVAudioPlayer) {
        return self.mAVAudioPlayer.duration*1000;
    }
    
    return 0;
}

- (NSInteger)pcmDB
{
    return 0;
}

- (void)terminate
{
    if (self.mAVAudioPlayer) {
        [self.mAVAudioPlayer stop];
        self.mAVAudioPlayer = nil;
    }
    self.mWorkUrl = nil;
    
    dispatch_barrier_sync(audioPlayerNotificationQueue, ^{
        NSLog(@"finish all SysAudioPlayer notifications");
    });
}

//------------------------------------------------------------//
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    if (flag) {
        __weak typeof(self) wself = self;
        dispatch_async(audioPlayerNotificationQueue, ^{
            __strong typeof(wself) strongSelf = wself;
            if(strongSelf!=nil)
            {
                if (strongSelf.delegate!=nil) {
                    if (([strongSelf.delegate respondsToSelector:@selector(onCompletion)])) {
                        [strongSelf.delegate onCompletion];
                    }
                }
            }
        });
    }else{
        __weak typeof(self) wself = self;
        dispatch_async(audioPlayerNotificationQueue, ^{
            __strong typeof(wself) strongSelf = wself;
            if(strongSelf!=nil)
            {
                if (strongSelf.delegate!=nil) {
                    if (([strongSelf.delegate respondsToSelector:@selector(onErrorWithErrorType:)])) {
                        [strongSelf.delegate onErrorWithErrorType:AUDIO_PLAYER_ERROR_UNKNOWN];
                    }
                }
            }
        });
    }
}

/* if an error occurs while decoding it will be reported to the delegate. */
- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError * __nullable)error
{
    __weak typeof(self) wself = self;
    dispatch_async(audioPlayerNotificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            if (strongSelf.delegate!=nil) {
                if (([strongSelf.delegate respondsToSelector:@selector(onErrorWithErrorType:)])) {
                    [strongSelf.delegate onErrorWithErrorType:AUDIO_PLAYER_ERROR_AUDIO_DECODE_FAIL];
                }
            }
        }
    });
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"SysAudioPlayer dealloc");
}

@end
