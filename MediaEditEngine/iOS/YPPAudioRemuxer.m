//
//  YPPAudioRemuxer.m
//  MediaStreamer
//
//  Created by Think on 2019/11/2.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "YPPAudioRemuxer.h"
#include "AudioRemuxerWrapper.h"

@implementation YPPAudioRemuxerOptions
- (instancetype) init
{
    self = [super init];
    if (self) {
        self.inputUrl = nil;
        self.startPosMs = 0;
        self.endPosMs = 0;
        self.isRemuxAll = YES;
        
        self.outputUrl = nil;
    }
    
    return self;
}
@end

@implementation YPPAudioRemuxer
{
    AudioRemuxerWrapper* pAudioRemuxerWrapper;
    dispatch_queue_t audioRemuxerNotificationQueue;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pAudioRemuxerWrapper = NULL;
        audioRemuxerNotificationQueue = dispatch_queue_create("YPPAudioRemuxerNotificationQueue", 0);
    }
    
    return self;
}

void audioRemuxerNotificationListener(void* owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak YPPAudioRemuxer *thiz = (__bridge YPPAudioRemuxer*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchAudioRemuxerNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchAudioRemuxerNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    __weak typeof(self) wself = self;
    dispatch_async(audioRemuxerNotificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf handleAudioRemuxerNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    });
}

- (void)handleAudioRemuxerNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    switch (event) {
        case AUDIO_REMUXER_STREAMING:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onAudioRemuxerStart)])) {
                    [self.delegate onAudioRemuxerStart];
                }
            }
            break;
            
        case AUDIO_REMUXER_END:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onAudioRemuxerEnd)])) {
                    [self.delegate onAudioRemuxerEnd];
                }
            }
            break;
        case AUDIO_REMUXER_ERROR:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onAudioRemuxerErrorWithErrorType:)])) {
                    [self.delegate onAudioRemuxerErrorWithErrorType:ext1];
                }
            }
            break;
        case AUDIO_REMUXER_INFO:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onAudioRemuxerInfoWithInfoType:InfoValue:)])) {
                    [self.delegate onAudioRemuxerInfoWithInfoType:ext1 InfoValue:ext2];
                }
            }
            break;
            
        default:
            break;
    }
}

- (void)initialize:(YPPAudioRemuxerOptions*)options
{
    char* inputUrl = (char*)[[options inputUrl] UTF8String];
    char* outputUrl = (char*)[[options outputUrl] UTF8String];
    
    AudioRemuxerMaterialGroup inputMaterialGroup;
    AudioRemuxerProduct outputProduct;
    
    inputMaterialGroup.audioRemuxerMaterialNum = 1;
    inputMaterialGroup.audioRemuxerMaterials[0] = new AudioRemuxerMaterial;
    inputMaterialGroup.audioRemuxerMaterials[0]->url = strdup(inputUrl);
    inputMaterialGroup.audioRemuxerMaterials[0]->startPosMs = [options startPosMs];
    inputMaterialGroup.audioRemuxerMaterials[0]->endPosMs = [options endPosMs];
    if ([options isRemuxAll]) {
        inputMaterialGroup.audioRemuxerMaterials[0]->isRemuxAll = true;
    }else{
        inputMaterialGroup.audioRemuxerMaterials[0]->isRemuxAll = false;
    }
    
    outputProduct.url = outputUrl;

    pAudioRemuxerWrapper = AudioRemuxerWrapper_GetInstance(inputMaterialGroup, outputProduct);
    if (pAudioRemuxerWrapper) {
        AudioRemuxerWrapper_setListener(pAudioRemuxerWrapper, audioRemuxerNotificationListener, (__bridge void*)self);
    }
    
    inputMaterialGroup.Free();
}

- (void)start
{
    if (pAudioRemuxerWrapper) {
        AudioRemuxerWrapper_start(pAudioRemuxerWrapper);
    }
}

- (void)resume
{
    if (pAudioRemuxerWrapper) {
        AudioRemuxerWrapper_resume(pAudioRemuxerWrapper);
    }
}

- (void)pause
{
    if (pAudioRemuxerWrapper) {
        AudioRemuxerWrapper_pause(pAudioRemuxerWrapper);
    }
}

- (void)stop:(BOOL)isCancel
{
    if (pAudioRemuxerWrapper) {
        if (isCancel) {
            AudioRemuxerWrapper_stop(pAudioRemuxerWrapper,true);
        }else{
            AudioRemuxerWrapper_stop(pAudioRemuxerWrapper,false);
        }
    }
    
    dispatch_barrier_sync(audioRemuxerNotificationQueue, ^{
        NSLog(@"finish all audio remuxer notifications");
    });
}

- (void)terminate
{
    if (pAudioRemuxerWrapper) {
        AudioRemuxerWrapper_ReleaseInstance(&pAudioRemuxerWrapper);
        pAudioRemuxerWrapper = NULL;
    }
    
    dispatch_barrier_sync(audioRemuxerNotificationQueue, ^{
        NSLog(@"finish all audio remuxer notifications");
    });
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"YPPAudioRemuxer dealloc");
}

@end
