//
//  YPPCPUOverlay.h
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/20.
//  Copyright © 2020 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YPPCPUOverlay : NSObject

- (instancetype) init;

- (void)setOverlay:(NSString*)imageUrl;
- (void)blendTo:(CVPixelBufferRef)mainPixelBuffer X:(int)x Y:(int)y;

@end

NS_ASSUME_NONNULL_END
