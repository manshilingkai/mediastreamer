//
//  YPPTwoToOneRemuxer.h
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/18.
//  Copyright © 2020 bolome. All rights reserved.
//

#import <Foundation/Foundation.h>

enum two_to_one_remuxer_event_type {
    TWO_TO_ONE_REMUXER_STREAMING = 0,
    TWO_TO_ONE_REMUXER_ERROR = 1,
    TWO_TO_ONE_REMUXER_INFO = 2,
    TWO_TO_ONE_REMUXER_END = 3,
};

enum two_to_one_remuxer_error_type {
    TWO_TO_ONE_REMUXER_ERROR_UNKNOWN = -1,
    TWO_TO_ONE_REMUXER_ERROR_OPEN_INPUT_VIDEO_MATERIAL_FAIL = 0,
    TWO_TO_ONE_REMUXER_ERROR_OPEN_INPUT_AUDIO_MATERIAL_FAIL = 1,
    TWO_TO_ONE_REMUXER_ERROR_OPEN_OUTPUT_PRODUCT_FAIL = 2,
    TWO_TO_ONE_REMUXER_ERROR_VIDEO_DEMUX_FAIL = 3,
    TWO_TO_ONE_REMUXER_ERROR_VIDEO_MUX_FAIL = 4,
    TWO_TO_ONE_REMUXER_ERROR_AUDIO_DEMUX_FAIL = 5,
    TWO_TO_ONE_REMUXER_ERROR_AUDIO_MUX_FAIL = 6,
};

@protocol YPPTwoToOneRemuxerDelegate <NSObject>
@required
- (void)onTwoToOneRemuxerStart;
- (void)onTwoToOneRemuxerErrorWithErrorType:(int)errorType;
- (void)onTwoToOneRemuxerInfoWithInfoType:(int)infoType InfoValue:(int)infoValue;
- (void)onTwoToOneRemuxerEnd;
@optional
@end

@interface YPPTwoToOneRemuxerOptions : NSObject
@property (nonatomic, strong) NSString *inputVideoUrl;
@property (nonatomic, strong) NSString *inputAudioUrl;
@property (nonatomic) int startPosMsForInputAudioUrl;
@property (nonatomic) int endPosMsForInputAudioUrl;
@property (nonatomic, strong) NSString *outputUrl;
@end

@interface YPPTwoToOneRemuxer : NSObject

- (instancetype) init;

- (void)initialize:(YPPTwoToOneRemuxerOptions*)options;

- (void)start;
- (void)resume;
- (void)pause;
- (void)stop:(BOOL)isCancel;

- (void)terminate;

@property (nonatomic, weak) id<YPPTwoToOneRemuxerDelegate> delegate;

@end
