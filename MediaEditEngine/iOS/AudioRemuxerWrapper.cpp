//
//  AudioRemuxerWrapper.cpp
//  MediaStreamer
//
//  Created by Think on 2019/10/31.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "AudioRemuxerWrapper.h"
#include "AudioRemuxer.h"

#ifdef __cplusplus
extern "C" {
#endif

struct AudioRemuxerWrapper
{
    AudioRemuxer* audioRemuxer;
    
    AudioRemuxerWrapper()
    {
        audioRemuxer = NULL;
    }
};

struct AudioRemuxerWrapper* AudioRemuxerWrapper_GetInstance(AudioRemuxerMaterialGroup inputMaterialGroup, AudioRemuxerProduct outputProduct)
{
    AudioRemuxerWrapper* pInstance = new AudioRemuxerWrapper;
    pInstance->audioRemuxer = new AudioRemuxer(inputMaterialGroup, outputProduct);
    return pInstance;
}

void AudioRemuxerWrapper_ReleaseInstance(struct AudioRemuxerWrapper **ppInstance)
{
    AudioRemuxerWrapper* pInstance  = *ppInstance;
    if (pInstance!=NULL) {
        if (pInstance->audioRemuxer!=NULL) {
            delete pInstance->audioRemuxer;
            pInstance->audioRemuxer = NULL;
        }
        
        delete pInstance;
        pInstance = NULL;
    }
}

void AudioRemuxerWrapper_setListener(struct AudioRemuxerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner)
{
    if (pInstance!=NULL && pInstance->audioRemuxer!=NULL) {
        pInstance->audioRemuxer->setListener(listener, owner);
    }
}

void AudioRemuxerWrapper_start(struct AudioRemuxerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->audioRemuxer!=NULL) {
        pInstance->audioRemuxer->start();
    }
}

void AudioRemuxerWrapper_resume(struct AudioRemuxerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->audioRemuxer!=NULL) {
        pInstance->audioRemuxer->resume();
    }
}

void AudioRemuxerWrapper_pause(struct AudioRemuxerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->audioRemuxer!=NULL) {
        pInstance->audioRemuxer->pause();
    }
}

void AudioRemuxerWrapper_stop(struct AudioRemuxerWrapper *pInstance, bool isCancel)
{
    if (pInstance!=NULL && pInstance->audioRemuxer!=NULL) {
        pInstance->audioRemuxer->stop(isCancel);
    }
}

#ifdef __cplusplus
};
#endif
