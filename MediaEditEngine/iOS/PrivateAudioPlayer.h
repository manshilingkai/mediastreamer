//
//  PrivateAudioPlayer.h
//  MediaStreamer
//
//  Created by slklovewyy on 2019/11/15.
//  Copyright © 2019 bolome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AudioPlayerCommon.h"
#import "AudioPlayerDelegate.h"
#import "AudioPlayerProtocol.h"

@interface PrivateAudioPlayer : NSObject <AudioPlayerProtocol>

- (instancetype) init;

- (void)initialize;

- (void)setDataSource:(NSURL*)url;
- (void)setDataSourceWithUrl:(NSString*)url;

- (void)prepare;
- (void)prepareAsync;
- (void)prepareAsyncToPlay;
- (void)play;
- (BOOL)isPlaying;
- (void)pause;
- (void)stop;
- (void)seekTo:(NSTimeInterval)seekPosMs;

- (void)setVolume:(NSTimeInterval)volume;
- (void)setPlayRate:(NSTimeInterval)playrate;
- (void)setLooping:(BOOL)isLooping;

- (void)terminate;

@property (nonatomic, weak) id<AudioPlayerDelegate> delegate;

@property (nonatomic, readonly) NSTimeInterval durationMs;
@property (nonatomic, readonly) NSTimeInterval currentPlaybackTimeMs;
@property (nonatomic, readonly) NSInteger pcmDB; //40DB-80DB

@end
