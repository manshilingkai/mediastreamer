//
//  YPPDubbingProducer.h
//  MediaStreamer
//
//  Created by Think on 2019/7/22.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

enum media_dubbing_event_type {
    MEDIA_DUBBING_STREAMING = 1,
    MEDIA_DUBBING_ERROR = 2,
    MEDIA_DUBBING_INFO = 3,
    MEDIA_DUBBING_END = 4,
};

enum media_dubbing_info_type {
    MEDIA_DUBBING_INFO_WRITE_TIMESTAMP = 1,
    MEDIA_DUBBING_INFO_WRITE_PERCENT = 2,
    MEDIA_DUBBING_INFO_LOST_DUB_DATA = 3,
};

enum media_dubbing_error_type {
    MEDIA_DUBBING_ERROR_UNKNOWN = -1,
    MEDIA_DUBBING_ERROR_OPEN_INPUT_VIDEO_MATERIAL_FAIL = 0,
    MEDIA_DUBBING_ERROR_OPEN_INPUT_BGM_MATERIAL_FAIL = 1,
    MEDIA_DUBBING_ERROR_OPEN_INPUT_DUB_MATERIAL_FAIL = 2,
    MEDIA_DUBBING_ERROR_OPEN_OUTPUT_MEDIA_PRODUCT_FAIL = 3,
    MEDIA_DUBBING_ERROR_AUDIO_FILTER_INPUT_FAIL = 4,
    MEDIA_DUBBING_ERROR_AUDIO_FILTER_OUTPUT_FAIL = 5,
    MEDIA_DUBBING_ERROR_AUDIO_ENCODE_FAIL = 6,
    MEDIA_DUBBING_ERROR_AUDIO_MUX_FAIL = 7,
    MEDIA_DUBBING_ERROR_AUDIO_FIFO_READ_FAIL = 8,
    MEDIA_DUBBING_ERROR_VIDEO_REMUX_FAIL = 9,
    MEDIA_DUBBING_ERROR_NO_MEMORY_SPACE = 10,
    MEDIA_DUBBING_ERROR_AUDIO_DEMUX_FAIL = 11,
};

@protocol YPPDubbingProducerDelegate <NSObject>
@required
- (void)onDubbingStart:(int)tag;
- (void)onDubbingErrorWithErrorType:(int)errorType Tag:(int)tag;
- (void)onDubbingInfoWithInfoType:(int)infoType InfoValue:(int)infoValue Tag:(int)tag;
- (void)onDubbingEnd:(int)tag;
@optional
@end

@interface YPPDubbingProducerOptions : NSObject
@property (nonatomic, strong) NSString *videoUrl;
@property (nonatomic, strong) NSString *bgmUrl;
@property (nonatomic) float bgmVolume;
@property (nonatomic, strong) NSString *dubUrl;
@property (nonatomic) float dubVolume;
@property (nonatomic, strong) NSString *productUrl;
@end

@interface YPPDubbingProducer : NSObject

- (instancetype) initWithTag:(int)tag;

- (void)initialize:(YPPDubbingProducerOptions*)options;

- (void)start;
- (void)resume;
- (void)pause;
- (void)stop:(BOOL)isCancle;

- (void)terminate;

@property (nonatomic, weak) id<YPPDubbingProducerDelegate> delegate;

@end
