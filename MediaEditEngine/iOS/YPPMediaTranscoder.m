//
//  YPPMediaTranscoder.m
//  MediaStreamer
//
//  Created by Think on 2019/10/16.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "YPPMediaTranscoder.h"
#include "MediaTranscoderWrapper.h"

using namespace MT;

@implementation YPPMediaTranscoderOptions

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.inputUrl = nil;
        self.startPosMs = -1;
        self.endPosMs = -1;
        
        self.video_rotation_degree = 0;
        self.video_flip = 0;
        
        self.overlayUrl = nil;
        self.has_alpha_for_overlay_video = NO;
        self.overlay_x = 0;
        self.overlay_y = 0;
        self.overlay_scale = 1.0f;
        self.overlay_effect_in_ms = -1;
        self.overlay_effect_out_ms = -1;
        
        self.privateStrategy = NO;
        self.privateLogoIcon = nil;
        self.privateLogoId = nil;
        self.privateLogoFontLibPath = nil;
        
        self.outputUrl = nil;
        self.hasVideo = YES;
        self.outputVideoWidth = 0;
        self.outputVideoHeight = 0;
        self.outputVideoBitrateKbps = 2000;
        self.hasAudio = YES;
        self.outputAudioSampleRate = 44100;
        self.outputAudioBitrateKbps = 128;
    }
    
    return self;
}

@end

@implementation YPPMediaTranscoder
{
    MediaTranscoderWrapper* pMediaTranscoderWrapper;
    dispatch_queue_t mediaTranscoderNotificationQueue;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pMediaTranscoderWrapper = NULL;
        mediaTranscoderNotificationQueue = dispatch_queue_create("YPPMediaTranscoderNotificationQueue", 0);
    }
    
    return self;
}

void mediaTranscoderNotificationListener(void* owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak YPPMediaTranscoder *thiz = (__bridge YPPMediaTranscoder*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchMediaTranscoderNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchMediaTranscoderNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    __weak typeof(self) wself = self;
    dispatch_async(mediaTranscoderNotificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf handleMediaTranscoderNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    });
}

- (void)handleMediaTranscoderNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    switch (event) {
        case MEDIA_TRANSCODER_CONNECTING:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotConnecting)])) {
                    [self.delegate gotConnecting];
                }
            }
            break;
        case MEDIA_TRANSCODER_CONNECTED:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(didConnected)])) {
                    [self.delegate didConnected];
                }
            }
            break;
        case MEDIA_TRANSCODER_STREAMING:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotTranscoding)])) {
                    [self.delegate gotTranscoding];
                }
            }
            break;
        case MEDIA_TRANSCODER_ERROR:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotErrorWithErrorType:)])) {
                    [self.delegate gotErrorWithErrorType:ext1];
                }
            }
            break;
        case MEDIA_TRANSCODER_INFO:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(gotInfoWithInfoType:InfoValue:)])) {
                    [self.delegate gotInfoWithInfoType:ext1 InfoValue:ext2];
                }
            }
            break;
        case MEDIA_TRANSCODER_END:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(didEnd)])) {
                    [self.delegate didEnd];
                }
            }
            break;
        case MEDIA_TRANSCODER_PAUSED:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(didPaused)])) {
                    [self.delegate didPaused];
                }
            }
            break;
        default:
            break;
    }
}

- (void)initialize:(YPPMediaTranscoderOptions*)options
{
    MediaMaterial mediaMaterial;
    MediaEffectGroup *pMediaEffectGroup;
    MediaProduct mediaProduct;
    
    mediaMaterial.url = (char*)[[options inputUrl] UTF8String];
    mediaMaterial.startPosMs = [options startPosMs];
    mediaMaterial.endPosMs = [options endPosMs];
    
    pMediaEffectGroup = new MediaEffectGroup;
    if ([options video_rotation_degree] !=0 ) {
        pMediaEffectGroup->mediaEffectCount++;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1] = new MediaEffect;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->type =  MEDIA_EFFECT_TYPE_VIDEO_ROTATE;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->video_rotation_degree = [options video_rotation_degree];
    }
    if ([options video_flip] !=0 ) {
        pMediaEffectGroup->mediaEffectCount++;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1] = new MediaEffect;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->type =  MEDIA_EFFECT_TYPE_VIDEO_FLIP;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->video_flip = [options video_flip];
    }
    if ([options overlayUrl] != nil) {
        pMediaEffectGroup->mediaEffectCount++;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1] = new MediaEffect;
        if ([[options overlayUrl] hasSuffix:@".mp4"] || [[options overlayUrl] hasSuffix:@".MP4"] || [[options overlayUrl] hasSuffix:@".MOV"]) {
            if ([options has_alpha_for_overlay_video]) {
                pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->type =  MEDIA_EFFECT_TYPE_MP4_ALPHA;
            }else{
                pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->type =  MEDIA_EFFECT_TYPE_MP4;
            }
        }else{
            pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->type =  MEDIA_EFFECT_TYPE_PNG;
        }
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->resourceUrl = strdup((char*)[[options overlayUrl] UTF8String]);
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->x =  [options overlay_x];
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->y =  [options overlay_y];
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->scale = [options overlay_scale];
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->effect_in_ms = [options overlay_effect_in_ms];
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->effect_out_ms = [options overlay_effect_out_ms];
    }
    if ([options privateStrategy]) {
        pMediaEffectGroup->mediaEffectCount++;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1] = new MediaEffect;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->type =  MEDIA_EFFECT_TYPE_PRIVATE;
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->private_logo_icon = strdup((char*)[[options privateLogoIcon] UTF8String]);
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->private_logo_id = strdup((char*)[[options privateLogoId] UTF8String]);
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->private_logo_fontlib_path = strdup((char*)[[options privateLogoFontLibPath] UTF8String]);
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->private_mask_vertical = strdup((char*)[[options privateMaskVertical] UTF8String]);
        pMediaEffectGroup->mediaEffects[pMediaEffectGroup->mediaEffectCount-1]->private_mask_horizontal = strdup((char*)[[options privateMaskHorizontal] UTF8String]);
    }
    
    mediaProduct.type = MEDIA_PRODUCT_TYPE_MP4;
    if ([options hasVideo]) {
        mediaProduct.type = MEDIA_PRODUCT_TYPE_MP4;
    }else{
        mediaProduct.type = MEDIA_PRODUCT_TYPE_M4A;
    }
    mediaProduct.url = (char*)[[options outputUrl] UTF8String];
    
    if ([options hasVideo]) {
        mediaProduct.videoOptions.hasVideoTrack = true;
    }else{
        mediaProduct.videoOptions.hasVideoTrack = false;
    }
    mediaProduct.videoOptions.videoWidth = [options outputVideoWidth];
    mediaProduct.videoOptions.videoHeight = [options outputVideoHeight];
    mediaProduct.videoOptions.videoBitrateKbps = [options outputVideoBitrateKbps];
    
    if ([options hasAudio]) {
        mediaProduct.audioOptions.hasAudioTrack = true;
    }else{
        mediaProduct.audioOptions.hasAudioTrack = false;
    }
    mediaProduct.audioOptions.audioSampleRate = [options outputAudioSampleRate];
    mediaProduct.audioOptions.audioBitrateKbps = [options outputAudioBitrateKbps];
    
    pMediaTranscoderWrapper = MediaTranscoder_GetInstance(mediaMaterial, pMediaEffectGroup, mediaProduct);
    
    if (pMediaEffectGroup) {
        pMediaEffectGroup->Free();
        delete pMediaEffectGroup;
        pMediaEffectGroup = NULL;
    }
    
    if (pMediaTranscoderWrapper) {
        MediaTranscoder_setListener(pMediaTranscoderWrapper, mediaTranscoderNotificationListener, (__bridge void*)self);
    }
}

- (void)initializeWithConfigure:(NSString*)configure
{
    pMediaTranscoderWrapper = MediaTranscoder_GetInstanceWithConfigure((char*)[configure UTF8String]);
    if (pMediaTranscoderWrapper) {
        MediaTranscoder_setListener(pMediaTranscoderWrapper, mediaTranscoderNotificationListener, (__bridge void*)self);
    }
}

- (void)start
{
    if (pMediaTranscoderWrapper) {
        MediaTranscoder_start(pMediaTranscoderWrapper);
    }
}

- (void)resume
{
    if (pMediaTranscoderWrapper) {
        MediaTranscoder_resume(pMediaTranscoderWrapper);
    }
}

- (void)pause
{
    if (pMediaTranscoderWrapper) {
        MediaTranscoder_pause(pMediaTranscoderWrapper);
    }
}

- (void)stop:(BOOL)isCancel
{
    if (pMediaTranscoderWrapper) {
        if (isCancel) {
            MediaTranscoder_stop(pMediaTranscoderWrapper, true);
        }else{
            MediaTranscoder_stop(pMediaTranscoderWrapper, false);
        }
    }
    
    dispatch_barrier_sync(mediaTranscoderNotificationQueue, ^{
        NSLog(@"finish all media transcoder notifications");
    });
}

- (void)terminate
{
    if (pMediaTranscoderWrapper) {
        MediaTranscoder_ReleaseInstance(&pMediaTranscoderWrapper);
        pMediaTranscoderWrapper = NULL;
    }
    
    dispatch_barrier_sync(mediaTranscoderNotificationQueue, ^{
        NSLog(@"finish all media transcoder notifications");
    });
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"YPPMediaTranscoder dealloc");
}

@end
