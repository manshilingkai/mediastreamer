//
//  YPPDubbingScore.m
//  MediaStreamer
//
//  Created by Think on 2019/9/30.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "YPPDubbingScore.h"
#include "DubbingScoreWrapper.h"

@implementation YPPDubbingScoreOptions
- (instancetype) init
{
    self = [super init];
    if (self) {
        self.originUrl = nil;
        self.bgmUrl = nil;
        self.dubUrl = nil;
    }
    
    return self;
}
@end

@implementation YPPDubbingScore
{
    DubbingScoreWrapper* pDubbingScoreWrapper;
    dispatch_queue_t dubbingScoreNotificationQueue;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pDubbingScoreWrapper = NULL;
        dubbingScoreNotificationQueue = dispatch_queue_create("YPPDubbingScoreNotificationQueue", 0);
    }
    
    return self;
}

void dubbingScoreNotificationListener(void* owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak YPPDubbingScore *thiz = (__bridge YPPDubbingScore*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchDubbingScoreNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchDubbingScoreNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    __weak typeof(self) wself = self;
    dispatch_async(dubbingScoreNotificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf handleDubbingScoreNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    });
}

- (void)handleDubbingScoreNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    switch (event) {
        case DUBBING_SCORE_PROCESSING:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onDubbingScoreStart)])) {
                    [self.delegate onDubbingScoreStart];
                }
            }
            break;
            
        case DUBBING_SCORE_END:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onDubbingScoreEnd:)])) {
                    [self.delegate onDubbingScoreEnd:float(ext1)/10.0f];
                }
            }
            break;
        case DUBBING_SCORE_ERROR:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onDubbingScoreErrorWithErrorType:)])) {
                    [self.delegate onDubbingScoreErrorWithErrorType:ext1];
                }
            }
            break;
        case DUBBING_SCORE_INFO:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onDubbingScoreInfoWithInfoType:InfoValue:)])) {
                    [self.delegate onDubbingScoreInfoWithInfoType:ext1 InfoValue:ext2];
                }
            }
            break;
            
        default:
            break;
    }
}

- (void)initialize:(YPPDubbingScoreOptions*)options
{
    char* originUrl = (char*)[[options originUrl] UTF8String];
    char* bgmUrl = (char*)[[options bgmUrl] UTF8String];
    char* dubUrl = (char*)[[options dubUrl] UTF8String];
    
    char* workDir = (char*)[NSTemporaryDirectory() UTF8String];
    
    pDubbingScoreWrapper = DubbingScore_GetInstance(originUrl, bgmUrl, dubUrl, workDir);
    if (pDubbingScoreWrapper) {
        DubbingScore_setListener(pDubbingScoreWrapper, dubbingScoreNotificationListener, (__bridge void*)self);
    }
}

- (void)start
{
    if (pDubbingScoreWrapper) {
        DubbingScore_start(pDubbingScoreWrapper);
    }
}

- (void)resume
{
    if (pDubbingScoreWrapper) {
        DubbingScore_resume(pDubbingScoreWrapper);
    }
}

- (void)pause
{
    if (pDubbingScoreWrapper) {
        DubbingScore_pause(pDubbingScoreWrapper);
    }
}

- (void)stop
{
    if (pDubbingScoreWrapper) {
        DubbingScore_stop(pDubbingScoreWrapper);
    }
    
    dispatch_barrier_sync(dubbingScoreNotificationQueue, ^{
        NSLog(@"finish all dubbing score notifications");
    });
}

- (void)terminate
{
    if (pDubbingScoreWrapper) {
        DubbingScore_ReleaseInstance(&pDubbingScoreWrapper);
        pDubbingScoreWrapper = NULL;
    }
    
    dispatch_barrier_sync(dubbingScoreNotificationQueue, ^{
        NSLog(@"finish all dubbing score notifications");
    });
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"YPPDubbingScore dealloc");
}


@end
