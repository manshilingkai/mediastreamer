//
//  YPPAudioRemuxer.h
//  MediaStreamer
//
//  Created by Think on 2019/11/2.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

enum audio_remuxer_event_type {
    AUDIO_REMUXER_STREAMING = 0,
    AUDIO_REMUXER_ERROR = 1,
    AUDIO_REMUXER_INFO = 2,
    AUDIO_REMUXER_END = 3,
};

enum audio_remuxer_error_type {
    AUDIO_REMUXER_ERROR_UNKNOWN = -1,
    AUDIO_REMUXER_ERROR_NO_INPUT_MATERIAL = 0,
    AUDIO_REMUXER_ERROR_OPEN_INPUT_MATERIAL_FAIL = 1,
    AUDIO_REMUXER_ERROR_OPEN_OUTPUT_PRODUCT_FAIL = 2,
    AUDIO_REMUXER_ERROR_DEMUX_FAIL = 3,
    AUDIO_REMUXER_ERROR_MUX_FAIL = 4,
};

@protocol YPPAudioRemuxerDelegate <NSObject>
@required
- (void)onAudioRemuxerStart;
- (void)onAudioRemuxerErrorWithErrorType:(int)errorType;
- (void)onAudioRemuxerInfoWithInfoType:(int)infoType InfoValue:(int)infoValue;
- (void)onAudioRemuxerEnd;
@optional
@end

@interface YPPAudioRemuxerOptions : NSObject
@property (nonatomic, strong) NSString *inputUrl;
@property (nonatomic) int startPosMs;
@property (nonatomic) int endPosMs;
@property (nonatomic) BOOL isRemuxAll;
@property (nonatomic, strong) NSString *outputUrl;
@end

@interface YPPAudioRemuxer : NSObject

- (instancetype) init;

- (void)initialize:(YPPAudioRemuxerOptions*)options;

- (void)start;
- (void)resume;
- (void)pause;
- (void)stop:(BOOL)isCancel;

- (void)terminate;

@property (nonatomic, weak) id<YPPAudioRemuxerDelegate> delegate;

@end
