//
//  YPPAudioPlayer.m
//  MediaStreamer
//
//  Created by Think on 2019/8/1.
//  Copyright © 2019年 Cell. All rights reserved.
//

#import "YPPAudioPlayer.h"
#import <AVFoundation/AVFoundation.h>
#import "PrivateAudioPlayer.h"
#import "SysAudioPlayer.h"

@implementation YPPAudioPlayerOptions
- (instancetype) init
{
    self = [super init];
    if (self) {
        self.audioPlayerType = PRIVATE_AUDIO_PLAYER_TYPE;
        self.isControlAudioSession = NO;
        self.isDubbingMode = NO;
    }
    
    return self;
}
@end

@interface YPPAudioPlayer () {}
@property (nonatomic, strong) id<AudioPlayerProtocol> currentAudioPlayer;
@end

@implementation YPPAudioPlayer
{
    __weak id<AudioPlayerDelegate> _delegate;
    int currentAudioPlayerType;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.currentAudioPlayer = nil;
        _delegate = nil;
        currentAudioPlayerType = PRIVATE_AUDIO_PLAYER_TYPE;
    }
    
    return self;
}

- (void)initialize
{
    self.currentAudioPlayer = [[PrivateAudioPlayer alloc] init];
    currentAudioPlayerType = PRIVATE_AUDIO_PLAYER_TYPE;
    self.currentAudioPlayer.delegate = _delegate;
    [self.currentAudioPlayer initialize];
}

- (void)initialize:(YPPAudioPlayerOptions*)options
{
    if ([options isControlAudioSession]) {
        NSError *error = nil;
        if (NO == [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionInterruptSpokenAudioAndMixWithOthers error:&error]) {
            NSLog(@"AVAudioSession.setCategory() failed: %@\n", error ? [error localizedDescription] : @"nil");
        }
        
        error = nil;
        if (NO == [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeMoviePlayback error:&error]) {
            NSLog(@"AVAudioSession.setMode() failed: %@\n", error ? [error localizedDescription] : @"nil");
        }
        
        error = nil;
        if (NO == [[AVAudioSession sharedInstance] setActive:YES error:&error]) {
            NSLog(@"AVAudioSession.setActive(YES) failed: %@\n", error ? [error localizedDescription] : @"nil");
        }
    }
    
    if ([options audioPlayerType]==PRIVATE_AUDIO_PLAYER_TYPE) {
        self.currentAudioPlayer = [[PrivateAudioPlayer alloc] init];
    }else{
        self.currentAudioPlayer = [[SysAudioPlayer alloc] init];
    }
    currentAudioPlayerType = [options audioPlayerType];
    self.currentAudioPlayer.delegate = _delegate;
    [self.currentAudioPlayer initialize];
    
    if ([options isDubbingMode]) {
        if (![self isHeadsetPluggedIn]) {
            [self setVolume:0.0f];
        }
        NSError *error = nil;
        [[AVAudioSession sharedInstance] setActive:YES error:&error];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioRouteChangeListenerCallback:) name:AVAudioSessionRouteChangeNotification object:[AVAudioSession sharedInstance]];
    }
}

- (void)audioRouteChangeListenerCallback:(NSNotification*)notification {
        NSDictionary *notification_userinfo = notification.userInfo;
        NSInteger routeChangeReason = [[notification_userinfo valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
        switch (routeChangeReason) {
            case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
                NSLog(@"AVAudioSessionRouteChangeReasonNewDeviceAvailable");
                [self setVolume:1.0];
                break;
            case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
                NSLog(@"AVAudioSessionRouteChangeReasonOldDeviceUnavailable");
                [self setVolume:0.0f];
                break;
            case AVAudioSessionRouteChangeReasonCategoryChange:            // called at start - also when other audio wants to play
                break;
     }
}

- (BOOL)isHeadsetPluggedIn {
    AVAudioSessionRouteDescription* route = [[AVAudioSession sharedInstance] currentRoute];
    for (AVAudioSessionPortDescription* desc in [route outputs]) {
        if ([[desc portType] isEqualToString:AVAudioSessionPortHeadphones])
            return YES;
    }
    return NO;
}

- (void)setDataSource:(NSURL*)url
{
    if (url==nil) {
        @throw [NSException exceptionWithName:@"nil"
                                       reason:@"nil"
                                     userInfo:nil];
        return;
    }
    
    if (url && [[url absoluteString] hasPrefix:@"ipod-library://"] && currentAudioPlayerType==PRIVATE_AUDIO_PLAYER_TYPE) {
        if (self.currentAudioPlayer!=nil) {
            [self.currentAudioPlayer terminate];
            self.currentAudioPlayer = nil;
        }
        
        self.currentAudioPlayer = [[SysAudioPlayer alloc] init];
        currentAudioPlayerType = SYSTEM_AUDIO_PLAYER_TYPE;
        self.currentAudioPlayer.delegate = _delegate;
        [self.currentAudioPlayer initialize];
    }
    
    if (url && ![[url absoluteString] hasPrefix:@"ipod-library://"] && currentAudioPlayerType==SYSTEM_AUDIO_PLAYER_TYPE) {
        if (self.currentAudioPlayer!=nil) {
            [self.currentAudioPlayer terminate];
            self.currentAudioPlayer = nil;
        }
        
        self.currentAudioPlayer = [[PrivateAudioPlayer alloc] init];
        currentAudioPlayerType = PRIVATE_AUDIO_PLAYER_TYPE;
        self.currentAudioPlayer.delegate = _delegate;
        [self.currentAudioPlayer initialize];
    }
    
    if (self.currentAudioPlayer!=nil) {
        [self.currentAudioPlayer setDataSource:url];
    }
}

- (void)setDataSourceWithUrl:(NSString*)url
{
    if (url==nil) {
        @throw [NSException exceptionWithName:@"nil"
                                       reason:@"nil"
                                     userInfo:nil];
        return;
    }
    
    if (url && [url hasPrefix:@"ipod-library://"] && currentAudioPlayerType==PRIVATE_AUDIO_PLAYER_TYPE) {
        if (self.currentAudioPlayer!=nil) {
            [self.currentAudioPlayer terminate];
            self.currentAudioPlayer = nil;
        }
        
        self.currentAudioPlayer = [[SysAudioPlayer alloc] init];
        currentAudioPlayerType = SYSTEM_AUDIO_PLAYER_TYPE;
        self.currentAudioPlayer.delegate = _delegate;
        [self.currentAudioPlayer initialize];
    }
    
    if (url && ![url hasPrefix:@"ipod-library://"] && currentAudioPlayerType==SYSTEM_AUDIO_PLAYER_TYPE) {
        if (self.currentAudioPlayer!=nil) {
            [self.currentAudioPlayer terminate];
            self.currentAudioPlayer = nil;
        }
        
        self.currentAudioPlayer = [[PrivateAudioPlayer alloc] init];
        currentAudioPlayerType = PRIVATE_AUDIO_PLAYER_TYPE;
        self.currentAudioPlayer.delegate = _delegate;
        [self.currentAudioPlayer initialize];
    }
    
    if (self.currentAudioPlayer!=nil) {
        [self.currentAudioPlayer setDataSourceWithUrl:url];
    }
}

- (void)prepare
{
    if (self.currentAudioPlayer!=nil) {
        [self.currentAudioPlayer prepare];
    }
}

- (void)prepareAsync
{
    if (self.currentAudioPlayer!=nil) {
        [self.currentAudioPlayer prepareAsync];
    }
}

- (void)prepareAsyncToPlay
{
    if (self.currentAudioPlayer!=nil) {
        [self.currentAudioPlayer prepareAsyncToPlay];
    }
}

- (void)play
{
    if (self.currentAudioPlayer!=nil) {
        [self.currentAudioPlayer play];
    }
}

- (BOOL)isPlaying
{
    BOOL ret = NO;
    if (self.currentAudioPlayer!=nil) {
        ret = [self.currentAudioPlayer isPlaying];
    }
    return ret;
}

- (void)pause
{
    if (self.currentAudioPlayer!=nil) {
        [self.currentAudioPlayer pause];
    }
}

- (void)stop
{
    if (self.currentAudioPlayer!=nil) {
        [self.currentAudioPlayer stop];
    }
    NSLog(@"YPPAudioPlayer stoped");
}

- (void)seekTo:(NSTimeInterval)seekPosMs
{
    if (self.currentAudioPlayer!=nil) {
        [self.currentAudioPlayer seekTo:seekPosMs];
    }
}

- (void)setVolume:(NSTimeInterval)volume
{
    if (self.currentAudioPlayer!=nil) {
        [self.currentAudioPlayer setVolume:volume];
    }
}

- (void)setPlayRate:(NSTimeInterval)playrate
{
    if (self.currentAudioPlayer!=nil) {
        [self.currentAudioPlayer setPlayRate:playrate];
    }
}

- (void)setLooping:(BOOL)isLooping
{
    if (self.currentAudioPlayer!=nil) {
        [self.currentAudioPlayer setLooping:isLooping];
    }
}

- (NSTimeInterval)currentPlaybackTimeMs
{
    NSTimeInterval ret = 0;
    if (self.currentAudioPlayer!=nil) {
        ret = [self.currentAudioPlayer currentPlaybackTimeMs];
    }
    return ret;
}

- (NSTimeInterval)durationMs
{
    NSTimeInterval ret = 0;
    if (self.currentAudioPlayer!=nil) {
        ret = [self.currentAudioPlayer durationMs];
    }
    return ret;
}

- (NSInteger)pcmDB
{
    NSTimeInterval ret = 0;
    if (self.currentAudioPlayer!=nil) {
        ret = [self.currentAudioPlayer pcmDB];
    }
    return ret;
}

- (void)terminate
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if (self.currentAudioPlayer!=nil) {
        [self.currentAudioPlayer terminate];
        self.currentAudioPlayer = nil;
    }
}

- (void)setDelegate:(id<AudioPlayerDelegate>)dele
{
    _delegate = dele;
    
    if (self.currentAudioPlayer!=nil)
    {
        self.currentAudioPlayer.delegate = _delegate;
    }
}

- (id<AudioPlayerDelegate>)delegate
{
    return _delegate;
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"YPPAudioPlayer dealloc");
}

@end
