//
//  AudioFileUtils.m
//  MediaStreamer
//
//  Created by Think on 2019/11/14.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "AudioFileUtils.h"
#import <AVFoundation/AVFoundation.h>

@implementation AudioFileUtils

+ (void)transcodeWithInput:(NSURL*)assetURL WithOutput:(NSString*)exportPath WithHandler:(void (^)(AudioFileTrancodeResult result))handler
{
    //Input
    AVURLAsset *songAsset = [AVURLAsset URLAssetWithURL:assetURL options:nil];
    NSError *assetError = nil;
    AVAssetReader *assetReader = [AVAssetReader assetReaderWithAsset:songAsset error:&assetError];
    if (assetError) {
        NSLog (@"error: %@", assetError);
        return handler(AudioFileTrancodeFail);
    }
    AVAssetReaderOutput* assetReaderOutput = [AVAssetReaderAudioMixOutput assetReaderAudioMixOutputWithAudioTracks:songAsset.tracks audioSettings: nil];
    if (![assetReader canAddOutput:assetReaderOutput]) {
        NSLog (@"can't add reader output... die!");
        return handler(AudioFileTrancodeFail);
    }
    [assetReader addOutput:assetReaderOutput];
    
    //Output
    if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:exportPath error:nil];
    }
    NSURL *exportURL = [NSURL fileURLWithPath:exportPath];
    AVAssetWriter* assetWriter = [AVAssetWriter assetWriterWithURL:exportURL fileType:AVFileTypeAppleM4A error:&assetError];
    if (assetError) {
        NSLog (@"error: %@", assetError);
        return handler(AudioFileTrancodeFail);
    }
    AudioChannelLayout channelLayout;
    memset(&channelLayout, 0, sizeof(AudioChannelLayout));
    channelLayout.mChannelLayoutTag = kAudioChannelLayoutTag_Stereo;
    NSDictionary *outputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:kAudioFormatMPEG4AAC], AVFormatIDKey,
                                    [NSNumber numberWithFloat:44100.0], AVSampleRateKey,
                                    [NSNumber numberWithInt:2], AVNumberOfChannelsKey,
                                    [NSData dataWithBytes:&channelLayout length:sizeof(AudioChannelLayout)], AVChannelLayoutKey,
                                    [NSNumber numberWithInt: 128000], AVEncoderBitRateKey,
                                    nil];
    AVAssetWriterInput* assetWriterInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:outputSettings];
    if ([assetWriter canAddInput:assetWriterInput]) {
        [assetWriter addInput:assetWriterInput];
    } else {
        NSLog (@"can't add asset writer input... die!");
        return handler(AudioFileTrancodeFail);
    }
    assetWriterInput.expectsMediaDataInRealTime = NO;
    
    //Start
    [assetWriter startWriting];
    [assetReader startReading];
    
    AVAssetTrack *soundTrack = [songAsset.tracks objectAtIndex:0];
    CMTime startTime = CMTimeMake (0, soundTrack.naturalTimeScale);
    [assetWriter startSessionAtSourceTime: startTime];
    
    dispatch_queue_t mediaInputQueue = dispatch_queue_create("mediaInputQueue", NULL);
    
    [assetWriterInput requestMediaDataWhenReadyOnQueue:mediaInputQueue
                                                usingBlock: ^{
        while (assetWriterInput.readyForMoreMediaData) {
            CMSampleBufferRef nextBuffer = [assetReaderOutput copyNextSampleBuffer];
            if (!nextBuffer) {
                [assetWriterInput markAsFinished];
                [assetReader cancelReading];
                
                [assetWriter finishWritingWithCompletionHandler:^{
                    if (assetWriter.status != AVAssetWriterStatusFailed && assetWriter.status==AVAssetWriterStatusCompleted) {
                        return handler(AudioFileTrancodeSuccess);
                    }else{
                        return handler(AudioFileTrancodeFail);
                    }
                }];
            } else {
                [assetWriterInput appendSampleBuffer: nextBuffer];
            }
        }
    }];
}

@end
