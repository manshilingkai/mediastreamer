//
//  MediaTranscoderWrapper.h
//  MediaStreamer
//
//  Created by Think on 2019/10/14.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef MediaTranscoderWrapper_h
#define MediaTranscoderWrapper_h

#include <stdio.h>

#include "MTMediaMaterial.h"
#include "MTMediaEffect.h"
#include "MTMediaProduct.h"

using namespace MT;

struct MediaTranscoderWrapper;

#ifdef __cplusplus
extern "C" {
#endif

struct MediaTranscoderWrapper* MediaTranscoder_GetInstanceWithParam(char* inputUrl, long startPosMs, long endPosMs, char*outputUrl, bool isEnableExternalRender);
struct MediaTranscoderWrapper* MediaTranscoder_GetInstance(MediaMaterial input, MediaEffectGroup *pMediaEffectGroup, MediaProduct product);
struct MediaTranscoderWrapper* MediaTranscoder_GetInstanceWithConfigure(char* configureInfo);
void MediaTranscoder_ReleaseInstance(struct MediaTranscoderWrapper **ppInstance);

void MediaTranscoder_setListener(struct MediaTranscoderWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner);

void MediaTranscoder_start(struct MediaTranscoderWrapper *pInstance);
void MediaTranscoder_resume(struct MediaTranscoderWrapper *pInstance);
void MediaTranscoder_pause(struct MediaTranscoderWrapper *pInstance);
void MediaTranscoder_stop(struct MediaTranscoderWrapper *pInstance, bool isCancel);

#ifdef __cplusplus
};
#endif

#endif /* MediaTranscoderWrapper_h */
