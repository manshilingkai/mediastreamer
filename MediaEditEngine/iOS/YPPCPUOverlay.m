//
//  YPPCPUOverlay.m
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/20.
//  Copyright © 2020 Cell. All rights reserved.
//

#import "YPPCPUOverlay.h"
#include "MediaDataType.h"
#include <mutex>

#define FAST_DIV255(x) ((((x) + 128) * 257) >> 16)

@implementation YPPCPUOverlay
{
    VideoFrame* overlayImageFrame;
    std::mutex mYPPCPUOverlayMutex;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        overlayImageFrame = NULL;
    }
    
    return self;
}

- (void)setOverlay:(NSString*)imageUrl
{
    char* overlayUrl = (char*)[imageUrl UTF8String];
    
    CGDataProviderRef sourceDataProvider = CGDataProviderCreateWithFilename(overlayUrl);
    if (sourceDataProvider==nil) {
        return;
    }
    
    CGImageRef sourceImage = CGImageCreateWithPNGDataProvider(sourceDataProvider,
                                                              NULL,
                                                              NO,
                                                              kCGRenderingIntentDefault);
    if (sourceImage==nil) {
        CGDataProviderRelease(sourceDataProvider);
        return;
    }
    
    CGDataProviderRelease(sourceDataProvider);
    
    size_t width = CGImageGetWidth(sourceImage);
    size_t height = CGImageGetHeight(sourceImage);
    size_t size = width*height*4;
    unsigned char *buffer = (unsigned char *)malloc(size);
    memset(buffer, 0, size);
    CGContextRef context = CGBitmapContextCreate(buffer, width, height, 8, width*4, CGImageGetColorSpace(sourceImage), kCGImageAlphaPremultipliedLast);
    CGRect rect = CGRectMake(0.0, 0.0, (CGFloat)width, (CGFloat)height);
    CGContextDrawImage(context, rect, sourceImage);
    CGContextRelease(context);
    
    CFRelease(sourceImage);

    mYPPCPUOverlayMutex.lock();
    if (overlayImageFrame) {
        overlayImageFrame->Free();
        delete overlayImageFrame;
        overlayImageFrame = NULL;
    }
    overlayImageFrame = new VideoFrame;
    overlayImageFrame->frameSize = size;
    overlayImageFrame->width = width;
    overlayImageFrame->height = height;
    overlayImageFrame->data = buffer;
    overlayImageFrame->videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
    mYPPCPUOverlayMutex.unlock();
}

static void blend_image_rgba(struct VideoFrame *main, struct VideoFrame *overlay, int x, int y)
{
    int real_overlay_height = 0;
    int real_overlay_width = 0;
    if (x>=0) {
        real_overlay_width = main->width-x < overlay->width ? main->width-x : overlay->width;
    }else {
        real_overlay_width = main->width < overlay->width + x ? main->width : overlay->width + x;
    }
    
    if (y>=0) {
        real_overlay_height = main->height-y < overlay->height ? main->height-y : overlay->height;
    }else {
        real_overlay_height = main->height < overlay->height + y ? main->height : overlay->height + y;
    }
    
    for (int j=0; j<real_overlay_height; j++) {
        for (int i=0; i<real_overlay_width; i++) {
            int overlay_pixel_pos = 0;
            int main_pixel_pos = 0;
            if (x>=0) {
                if (y>=0) {
                    overlay_pixel_pos = j*overlay->width+i;
                }else{
                    overlay_pixel_pos = (-y+j)*overlay->width+i;
                }
            }else{
                if (y>=0) {
                    overlay_pixel_pos = j*overlay->width+i-x;
                }else{
                    overlay_pixel_pos = (-y+j)*overlay->width+i-x;
                }
            }
            if (x>=0) {
                if (y>=0) {
                    main_pixel_pos = (y+j)*main->width+(x+i);
                }else{
                    main_pixel_pos = j*main->width+(x+i);
                }
            }else{
                if (y>=0) {
                    main_pixel_pos = (y+j)*main->width+i;
                }else{
                    main_pixel_pos = j*main->width+i;
                }
            }
            
            if (main->videoRawType == VIDEOFRAME_RAWTYPE_BGRA && overlay->videoRawType == VIDEOFRAME_RAWTYPE_RGBA) {
                uint8_t overlay_alpha = *(overlay->data+overlay_pixel_pos*4+3);
                if (overlay_alpha==255) {
                    main->data[main_pixel_pos*4] = *(overlay->data+overlay_pixel_pos*4+2);
                    main->data[main_pixel_pos*4+1] = *(overlay->data+overlay_pixel_pos*4+1);
                    main->data[main_pixel_pos*4+2] = *(overlay->data+overlay_pixel_pos*4+0);
                    main->data[main_pixel_pos*4+3] = *(overlay->data+overlay_pixel_pos*4+3);
                }else if(overlay_alpha<255 && overlay_alpha>0) {
                    main->data[main_pixel_pos*4] = FAST_DIV255(main->data[main_pixel_pos*4] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+2] * overlay_alpha);
                    main->data[main_pixel_pos*4+1] = FAST_DIV255(main->data[main_pixel_pos*4+1] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+1] * overlay_alpha);
                    main->data[main_pixel_pos*4+2] = FAST_DIV255(main->data[main_pixel_pos*4+2] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+0] * overlay_alpha);
                    main->data[main_pixel_pos*4+3] = FAST_DIV255(main->data[main_pixel_pos*4+3] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+3] * overlay_alpha);
                }
            }else if (main->videoRawType == VIDEOFRAME_RAWTYPE_RGBA && overlay->videoRawType == VIDEOFRAME_RAWTYPE_RGBA) {
                uint8_t overlay_alpha = *(overlay->data+overlay_pixel_pos*4+3);
                if (overlay_alpha==255) {
                    main->data[main_pixel_pos*4] = *(overlay->data+overlay_pixel_pos*4); //R
                    main->data[main_pixel_pos*4+1] = *(overlay->data+overlay_pixel_pos*4+1); //G
                    main->data[main_pixel_pos*4+2] = *(overlay->data+overlay_pixel_pos*4+2); //B
                    main->data[main_pixel_pos*4+3] = *(overlay->data+overlay_pixel_pos*4+3); //A
                }else if(overlay_alpha<255 && overlay_alpha>0) {
                    main->data[main_pixel_pos*4] = FAST_DIV255(main->data[main_pixel_pos*4] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4] * overlay_alpha); //R
                    main->data[main_pixel_pos*4+1] = FAST_DIV255(main->data[main_pixel_pos*4+1] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+1] * overlay_alpha); //G
                    main->data[main_pixel_pos*4+2] = FAST_DIV255(main->data[main_pixel_pos*4+2] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+2] * overlay_alpha); //B
                    main->data[main_pixel_pos*4+3] = FAST_DIV255(main->data[main_pixel_pos*4+3] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+3] * overlay_alpha); //A
                }
            }else {
                uint8_t overlay_alpha = *(overlay->data+overlay_pixel_pos*4);
                if (overlay_alpha==255) {
                    main->data[main_pixel_pos*4] = *(overlay->data+overlay_pixel_pos*4); //A
                    main->data[main_pixel_pos*4+1] = *(overlay->data+overlay_pixel_pos*4+1); //B
                    main->data[main_pixel_pos*4+2] = *(overlay->data+overlay_pixel_pos*4+2); //G
                    main->data[main_pixel_pos*4+3] = *(overlay->data+overlay_pixel_pos*4+3); //R
                }else if(overlay_alpha<255 && overlay_alpha>0) {
                    main->data[main_pixel_pos*4] = FAST_DIV255(main->data[main_pixel_pos*4] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4] * overlay_alpha); //A
                    main->data[main_pixel_pos*4+1] = FAST_DIV255(main->data[main_pixel_pos*4+1] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+1] * overlay_alpha); //B
                    main->data[main_pixel_pos*4+2] = FAST_DIV255(main->data[main_pixel_pos*4+2] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+2] * overlay_alpha); //G
                    main->data[main_pixel_pos*4+3] = FAST_DIV255(main->data[main_pixel_pos*4+3] * (255 - overlay_alpha) + overlay->data[overlay_pixel_pos*4+3] * overlay_alpha); //R
                }
            }
        }
    }
}

- (void)blendTo:(CVPixelBufferRef)mainPixelBuffer X:(int)x Y:(int)y
{
    CVPixelBufferLockBaseAddress(mainPixelBuffer,0);
    
    int kCVPixelFormatType = CVPixelBufferGetPixelFormatType(mainPixelBuffer);
    if (kCVPixelFormatType == kCVPixelFormatType_32BGRA) {
        uint8_t* data = (uint8_t*)CVPixelBufferGetBaseAddress(mainPixelBuffer);
        int frameSize = (int)CVPixelBufferGetDataSize(mainPixelBuffer);
        int width = (int)CVPixelBufferGetWidth(mainPixelBuffer);
        int height = (int)CVPixelBufferGetHeight(mainPixelBuffer);
        
        struct VideoFrame main;
        main.data = data;
        main.frameSize = frameSize;
        main.width = width;
        main.height = height;
        main.videoRawType = VIDEOFRAME_RAWTYPE_BGRA;
        
        mYPPCPUOverlayMutex.lock();
        if (overlayImageFrame!=NULL && overlayImageFrame->data!=NULL) {
            blend_image_rgba(&main, overlayImageFrame, x, y);
        }
        mYPPCPUOverlayMutex.unlock();
    }
    
    CVPixelBufferUnlockBaseAddress(mainPixelBuffer,0);
}

- (void)dealloc
{
    if (overlayImageFrame) {
        overlayImageFrame->Free();
        delete overlayImageFrame;
        overlayImageFrame = NULL;
    }
    
    NSLog(@"YPPCPUOverlay dealloc");
}

@end
