//
//  YPPRadioProducer.m
//  MediaStreamer
//
//  Created by Think on 2019/10/23.
//  Copyright © 2019 Cell. All rights reserved.
//

#import "YPPRadioProducer.h"
#include "RadioProducerWrapper.h"

@implementation YPPRadioProducerOptions

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.radioProductType = RADIO_PRODUCT_TYPE_WAV;
        self.productUrl = nil;
        self.bgProductUrl = nil;
        self.outWaveValueIntervalMs = 200;
        self.defaultBgmVolume = 1.0f;
        self.defaultMicVolume = 1.2f;
        self.defaultEffectMusicVolume = 1.0f;
    }
    
    return self;
}

@end

@implementation YPPRadioProducer
{
    RadioProducerWrapper* pRadioProducerWrapper;
    dispatch_queue_t radioProducerNotificationQueue;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pRadioProducerWrapper = NULL;
        radioProducerNotificationQueue = dispatch_queue_create("YPPRadioProducerNotificationQueue", 0);
    }
    
    return self;
}

void radioProducerNotificationListener(void* owner, int event, int ext1, int ext2)
{
    @autoreleasepool {
        __weak YPPRadioProducer *thiz = (__bridge YPPRadioProducer*)owner;
        if(thiz!=nil)
        {
            [thiz dispatchRadioProducerNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    }
}

- (void)dispatchRadioProducerNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    __weak typeof(self) wself = self;
    dispatch_async(radioProducerNotificationQueue, ^{
        __strong typeof(wself) strongSelf = wself;
        if(strongSelf!=nil)
        {
            [strongSelf handleRadioProducerNotificationWithEvent:event Ext1:ext1 Ext2:ext2];
        }
    });
}

- (void)handleRadioProducerNotificationWithEvent:(int)event Ext1:(int)ext1 Ext2:(int)ext2
{
    switch (event) {
        case RADIO_PRODUCER_ERROR:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onRadioProducerError:)])) {
                    [self.delegate onRadioProducerError:ext1];
                }
            }
            break;
        case RADIO_PRODUCER_BGM_PLAY_TO_END:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onRadioProducerBgmPlayToEnd)])) {
                    [self.delegate onRadioProducerBgmPlayToEnd];
                }
            }
            break;
        case RADIO_PRODUCER_EFFECT_MUSIC_PLAY_TO_END:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onRadioProducerEffectMusicPlayToEnd)])) {
                    [self.delegate onRadioProducerEffectMusicPlayToEnd];
                }
            }
            break;
        case RADIO_PRODUCER_OUT_WAVE_VALUE:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onRadioProducerOutWaveValue:)])) {
                    [self.delegate onRadioProducerOutWaveValue:ext1];
                }
            }
            break;
        case RADIO_PRODUCER_EXPORT_PRODUCT:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onRadioProducerExportProduct:)])) {
                    if (ext1) {
                        [self.delegate onRadioProducerExportProduct:YES];
                    }else{
                        [self.delegate onRadioProducerExportProduct:NO];
                    }
                }
            }
            break;
        case RADIO_PRODUCER_WARN:
            if (self.delegate!=nil) {
                if (([self.delegate respondsToSelector:@selector(onRadioProducerWarn:)])) {
                    [self.delegate onRadioProducerWarn:ext1];
                }
            }
            break;
        default:
            break;
    }
}


- (void)initialize:(YPPRadioProducerOptions*)options
{
    RadioProductOptions radioProductOptions;
    radioProductOptions.type = [options radioProductType];
    radioProductOptions.productUrl = (char*)[[options productUrl] UTF8String];
    radioProductOptions.bgProductUrl = (char*)[[options bgProductUrl] UTF8String];
    radioProductOptions.outWaveValueIntervalMs = [options outWaveValueIntervalMs];
    radioProductOptions.defaultBgmVolume = [options defaultBgmVolume];
    radioProductOptions.defaultMicVolume = [options defaultMicVolume];
    radioProductOptions.defaultEffectMusicVolume = [options defaultEffectMusicVolume];
    
    pRadioProducerWrapper = RadioProducerWrapper_GetInstance(radioProductOptions);
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_setListener(pRadioProducerWrapper, radioProducerNotificationListener, (__bridge void*)self);
    }
}

- (void)prepareBgm:(NSString*)bgmUrl
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_prepareBgm(pRadioProducerWrapper, (char*)[bgmUrl UTF8String]);
    }
}

- (void)prepareBgm:(NSString*)bgmUrl WithStartPos:(int)startPosMs;
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_prepareBgmWithStartPos(pRadioProducerWrapper, (char*)[bgmUrl UTF8String], startPosMs);
    }
}

- (void)startBgm
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_startBgm(pRadioProducerWrapper);
    }
}

- (void)pauseBgm
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_pauseBgm(pRadioProducerWrapper);
    }
}

- (void)stopBgm
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_stopBgm(pRadioProducerWrapper);
    }
}

- (void)setBgmVolume:(float)volume
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_setBgmVolume(pRadioProducerWrapper, volume);
    }
}

- (void)startMicrophone
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_startMicrophone(pRadioProducerWrapper);
    }
}

- (void)stopMicrophone
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_stopMicrophone(pRadioProducerWrapper);
    }
}

- (void)setMicrophoneVolume:(float)volume
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_setMicrophoneVolume(pRadioProducerWrapper, volume);
    }
}

- (void)prepareEffectMusic:(NSString*)effectMusicUrl
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_prepareEffectMusic(pRadioProducerWrapper, (char*)[effectMusicUrl UTF8String]);
    }
}

- (void)startEffectMusic
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_startEffectMusic(pRadioProducerWrapper);
    }
}

- (void)pauseEffectMusic
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_pauseEffectMusic(pRadioProducerWrapper);
    }
}

- (void)stopEffectMusic
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_stopEffectMusic(pRadioProducerWrapper);
    }
}

- (void)setEffectMusicVolume:(float)volume
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_setEffectMusicVolume(pRadioProducerWrapper, volume);
    }
}

- (void)setAudioUserDefinedEffect:(int)effect
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_setAudioUserDefinedEffect(pRadioProducerWrapper, effect);
    }
}

- (void)setAudioEqualizerStyle:(int)style
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_setAudioEqualizerStyle(pRadioProducerWrapper, style);
    }
}

- (void)setAudioReverbStyle:(int)style
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_setAudioReverbStyle(pRadioProducerWrapper, style);
    }
}

- (void)setAudioPitchSemiTones:(int)value
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_setAudioPitchSemiTones(pRadioProducerWrapper, value);
    }
}

- (void)setEarReturn:(BOOL)isEnableEarReturn
{
    if (pRadioProducerWrapper) {
        if (isEnableEarReturn) {
            RadioProducerWrapper_setEarReturn(pRadioProducerWrapper, true);
        }else{
            RadioProducerWrapper_setEarReturn(pRadioProducerWrapper, false);
        }
    }
}

- (void)pause:(BOOL)isExport
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_pause(pRadioProducerWrapper);
    }
}

- (void)stop
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_stop(pRadioProducerWrapper);
    }
}

- (NSTimeInterval)currentTimeMs
{
    if (pRadioProducerWrapper) {
        return RadioProducerWrapper_getCurrentTimeMs(pRadioProducerWrapper);
    }
    
    return 0;
}

- (NSTimeInterval)bgmPlayingTimeMs
{
    if (pRadioProducerWrapper) {
        return RadioProducerWrapper_getBgmPlayingTimeMs(pRadioProducerWrapper);
    }
    
    return 0;
}

- (void)terminate
{
    if (pRadioProducerWrapper) {
        RadioProducerWrapper_ReleaseInstance(&pRadioProducerWrapper);
        pRadioProducerWrapper = NULL;
    }
    
    dispatch_barrier_sync(radioProducerNotificationQueue, ^{
        NSLog(@"finish all radio producer notifications");
    });
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"YPPRadioProducer dealloc");
}


@end
