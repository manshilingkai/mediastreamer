//
//  YPPRadioProducer.h
//  MediaStreamer
//
//  Created by Think on 2019/10/23.
//  Copyright © 2019 Cell. All rights reserved.
//

#import <Foundation/Foundation.h>

enum radio_producer_event_type {
    RADIO_PRODUCER_ERROR = 0,
    RADIO_PRODUCER_BGM_PLAY_TO_END = 1,
    RADIO_PRODUCER_EFFECT_MUSIC_PLAY_TO_END = 2,
    RADIO_PRODUCER_OUT_WAVE_VALUE = 3,
    RADIO_PRODUCER_EXPORT_PRODUCT = 4,
    RADIO_PRODUCER_WARN = 5,
};

enum radio_producer_warn_type {
    RADIO_PRODUCER_WARN_START_MICROPHONE_FAIL = 0,
    RADIO_PRODUCER_WARN_BGM_OUTPUT_FAIL = 1,
    RADIO_PRODUCER_WARN_EFFECT_MUSIC_OUTPUT_FAIL = 1,
};

enum radio_producer_error_type {
    RADIO_PRODUCER_ERROR_AUDIO_RENDER_INIT_FAIL = 0,
    RADIO_PRODUCER_ERROR_AUDIO_RENDER_START_FAIL = 1,
    RADIO_PRODUCER_ERROR_OPEN_PRODUCT_WRITER_FAIL = 2,
    RADIO_PRODUCER_ERROR_OPEN_BG_PRODUCT_WRITER_FAIL = 3,
};

enum YPP_RADIO_PRODUCT_TYPE {
    RADIO_PRODUCT_TYPE_RAW_PCM = 0,
    RADIO_PRODUCT_TYPE_WAV = 1,
    RADIO_PRODUCT_TYPE_M4A = 2,
    RADIO_PRODUCT_TYPE_MP3 = 3,
};

@protocol YPPRadioProducerDelegate <NSObject>
@required
- (void)onRadioProducerError:(int)errorType;
- (void)onRadioProducerWarn:(int)errorType;
- (void)onRadioProducerBgmPlayToEnd;
- (void)onRadioProducerEffectMusicPlayToEnd;
- (void)onRadioProducerOutWaveValue:(int)value;
- (void)onRadioProducerExportProduct:(BOOL)isSuccess;
@optional
@end

@interface YPPRadioProducerOptions : NSObject
@property (nonatomic) int radioProductType;
@property (nonatomic, strong) NSString *productUrl;
@property (nonatomic, strong) NSString *bgProductUrl;
@property (nonatomic) int outWaveValueIntervalMs;
@property (nonatomic) float defaultBgmVolume;
@property (nonatomic) float defaultMicVolume;
@property (nonatomic) float defaultEffectMusicVolume;
@end

@interface YPPRadioProducer : NSObject

- (instancetype) init;

- (void)initialize:(YPPRadioProducerOptions*)options;

- (void)prepareBgm:(NSString*)bgmUrl;
- (void)prepareBgm:(NSString*)bgmUrl WithStartPos:(int)startPosMs;
- (void)startBgm;
- (void)pauseBgm;
- (void)stopBgm;
- (void)setBgmVolume:(float)volume;

- (void)startMicrophone;
- (void)stopMicrophone;
- (void)setMicrophoneVolume:(float)volume;

- (void)prepareEffectMusic:(NSString*)effectMusicUrl;
- (void)startEffectMusic;
- (void)pauseEffectMusic;
- (void)stopEffectMusic;
- (void)setEffectMusicVolume:(float)volume;

- (void)setAudioUserDefinedEffect:(int)effect;
- (void)setAudioEqualizerStyle:(int)style;
- (void)setAudioReverbStyle:(int)style;
- (void)setAudioPitchSemiTones:(int)value;

- (void)setEarReturn:(BOOL)isEnableEarReturn;

- (void)pause:(BOOL)isExport;
- (void)stop;

- (void)terminate;

@property (nonatomic, weak) id<YPPRadioProducerDelegate> delegate;

@property (nonatomic, readonly) NSTimeInterval currentTimeMs;
@property (nonatomic, readonly) NSTimeInterval bgmPlayingTimeMs;

@end
