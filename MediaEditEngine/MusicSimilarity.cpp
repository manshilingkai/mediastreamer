//
//  MusicSimilarity.cpp
//  MediaStreamer
//
//  Created by Think on 2019/10/9.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "MusicSimilarity.h"
#include "AutoLock.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#else
#include "NormalMediaListener.h"
#endif

#include "MediaLog.h"
#include "MediaFile.h"
#include "FFLog.h"
#include "AudioMixer.h"
#include "StringUtils.h"
#include "MediaTime.h"

#define MUSLY_MANDELELLIS_SIMILARITY_MAX_VALUE 20.0f

struct MusicSimilarityEvent : public TimedEventQueue::Event {
    MusicSimilarityEvent(
                                MusicSimilarity *similarity,
                                void (MusicSimilarity::*method)())
    : mSimilarity(similarity),
    mMethod(method) {
    }
    
protected:
    virtual ~MusicSimilarityEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mSimilarity->*mMethod)();
    }
    
private:
    MusicSimilarity *mSimilarity;
    void (MusicSimilarity::*mMethod)();
    
    MusicSimilarityEvent(const MusicSimilarityEvent &);
    MusicSimilarityEvent &operator=(const MusicSimilarityEvent &);
};

#ifdef ANDROID
MusicSimilarity::MusicSimilarity(JavaVM *jvm, char* originUrl, char* bgmUrl, char* dubUrl, char* workDir)
{
    mJvm = jvm;

    init_ffmpeg_env();

    mOriginUrl = NULL;
    if (originUrl) {
        mOriginUrl = strdup(originUrl);
    }
    
    mBgmUrl = NULL;
    if (bgmUrl) {
        mBgmUrl = strdup(bgmUrl);
    }
    
    mDubUrl = NULL;
    if (dubUrl) {
        mDubUrl = strdup(dubUrl);
    }
    
    mWorkDir = NULL;
    if (workDir) {
        mWorkDir = strdup(workDir);
    }
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);

    mAsyncPrepareEvent = NULL;
    mCalculateEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    
    mAsyncPrepareEvent = new MusicSimilarityEvent(this, &MusicSimilarity::onPrepareAsyncEvent);
    mCalculateEvent = new MusicSimilarityEvent(this, &MusicSimilarity::onCalculateEvent);
    mStopEvent = new MusicSimilarityEvent(this, &MusicSimilarity::onStopEvent);
    mNotifyEvent = new MusicSimilarityEvent(this, &MusicSimilarity::onNotifyEvent);
    mCalculateEventPending = false;
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();

    pthread_cond_init(&mStopCondition, NULL);

    mMediaListener = NULL;
    
    mMediaFrameOutputer = NULL;
    audio_buffersrc_ctx = NULL;
    audio_buffersink_ctx = NULL;
    audio_filter_graph = NULL;
    
    mDubWAVFile = NULL;
    mWorkAudioDubData = NULL;
    mWorkAudioDubDataSize = 0;
    
    mOutputWAVFile = NULL;
    mOutputWAVFilePath = NULL;
    
    mWorkSampleRate = 44100;
    mWorkSampleFormat = AV_SAMPLE_FMT_S16;
    mWorkChannelCount = 2;
    mWorkChannelLayout = AV_CH_LAYOUT_STEREO;
    
    similarity = MUSLY_MANDELELLIS_SIMILARITY_MAX_VALUE;
    
//    srand((unsigned)time(NULL));
}
#else
MusicSimilarity::MusicSimilarity(char* originUrl, char* bgmUrl, char* dubUrl, char* workDir)
{
    init_ffmpeg_env();

    mOriginUrl = NULL;
    if (originUrl) {
        mOriginUrl = strdup(originUrl);
    }
    
    mBgmUrl = NULL;
    if (bgmUrl) {
        mBgmUrl = strdup(bgmUrl);
    }
    
    mDubUrl = NULL;
    if (dubUrl) {
        mDubUrl = strdup(dubUrl);
    }
    
    mWorkDir = NULL;
    if (workDir) {
        mWorkDir = strdup(workDir);
    }
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);

    mAsyncPrepareEvent = NULL;
    mCalculateEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    
    mAsyncPrepareEvent = new MusicSimilarityEvent(this, &MusicSimilarity::onPrepareAsyncEvent);
    mCalculateEvent = new MusicSimilarityEvent(this, &MusicSimilarity::onCalculateEvent);
    mStopEvent = new MusicSimilarityEvent(this, &MusicSimilarity::onStopEvent);
    mNotifyEvent = new MusicSimilarityEvent(this, &MusicSimilarity::onNotifyEvent);
    mCalculateEventPending = false;
    
    mQueue.start();

    pthread_cond_init(&mStopCondition, NULL);

    mMediaListener = NULL;
    
    mMediaFrameOutputer = NULL;
    audio_buffersrc_ctx = NULL;
    audio_buffersink_ctx = NULL;
    audio_filter_graph = NULL;
    
    mDubWAVFile = NULL;
    mWorkAudioDubData = NULL;
    mWorkAudioDubDataSize = 0;
    
    mOutputWAVFile = NULL;
    mOutputWAVFilePath = NULL;
    
    mWorkSampleRate = 44100;
    mWorkSampleFormat = AV_SAMPLE_FMT_S16;
    mWorkChannelCount = 2;
    mWorkChannelLayout = AV_CH_LAYOUT_STEREO;
    
    similarity = MUSLY_MANDELELLIS_SIMILARITY_MAX_VALUE;
    
//    srand((unsigned)time(NULL));
}
#endif

MusicSimilarity::~MusicSimilarity()
{
    stop();

    mQueue.stop(true);

    mNotificationQueue.flush();

    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
    
    if (mCalculateEvent!=NULL) {
        delete mCalculateEvent;
        mCalculateEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    pthread_cond_destroy(&mStopCondition);
    
    pthread_mutex_destroy(&mLock);
    
#ifdef ANDROID
    JniMediaListener *jniListener = (JniMediaListener*)mMediaListener;
    if (jniListener!=NULL) {
        delete jniListener;
        jniListener = NULL;
    }
#else
    NormalMediaListener* normalMediaListener = (NormalMediaListener*)mMediaListener;
    if (normalMediaListener!=NULL) {
        delete normalMediaListener;
        normalMediaListener = NULL;
    }
#endif
    
    mMediaListener = NULL;
    
    if (mOriginUrl) {
        free(mOriginUrl);
        mOriginUrl = NULL;
    }
    
    if (mBgmUrl) {
        free(mBgmUrl);
        mBgmUrl = NULL;
    }
    
    if (mDubUrl) {
        free(mDubUrl);
        mDubUrl = NULL;
    }
    
    if (mWorkDir) {
        free(mWorkDir);
        mWorkDir = NULL;
    }
}

#ifdef ANDROID
void MusicSimilarity::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    modifyFlags(INITIALIZED, SET);
}
#else
void MusicSimilarity::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new NormalMediaListener(listener,arg);
    
    modifyFlags(INITIALIZED, SET);
}
#endif

void MusicSimilarity::notifyListener_l(int event, int ext1, int ext2)
{
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void MusicSimilarity::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    postNotifyEvent_l();
}

void MusicSimilarity::postNotifyEvent_l()
{
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void MusicSimilarity::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
    
    switch (event) {
        case MEDIA_TRANSCODER_ERROR:
            modifyFlags(ERROR, ASSIGN);
            if (ext2!=AVERROR_EXIT) {
                if (ext1==MEDIA_TRANSCODER_ERROR_DEMUXER_READ_FAIL) {
                    notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_BGM_ERROR, ext1);
                }else if (ext1==MEDIA_TRANSCODER_ERROR_NO_MEMORY) {
                    notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_BGM_ERROR, ext1);
                }else{
                    notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_BGM_ERROR, ext1);
                }
            }
            stop_l();
            break;
        case MEDIA_TRANSCODER_INFO:
            if (ext1==MEDIA_TRANSCODER_WARN_AUDIO_DECODE_FAIL) {
                LOGW("[WARN] Got Audio Decode Fail");
            }
            break;
            
        default:
            break;
    }
}

void MusicSimilarity::start()
{
    AutoLock autoLock(&mLock);
    if (mFlags & CONNECTING) {
        LOGW("already connecting");
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    prepareAsync_l();
}

void MusicSimilarity::prepareAsync_l()
{
    modifyFlags(END, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(CONNECTING, SET);
        
    mQueue.postEvent(mAsyncPrepareEvent);
}

void MusicSimilarity::onPrepareAsyncEvent()
{
    AutoLock autoLock(&mLock);
    int ret = open_all_pipelines_l();
    
    if (ret>=0) {
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(CONNECTED, SET);
        
        play_l();
    }else{
        if (ret!=AVERROR_EXIT) {
            if (ret==-1) {
                notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_BGM_ERROR);
            }else if (ret==-2) {
                notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_OPEN_DUB_FAIL);
            }else if (ret==-3) {
                notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_OPEN_OUTPUT_PRODUCT_FAIL);
            }else {
                notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_UNKNOWN);
            }
        }
        
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(ERROR, SET);
        stop_l();
    }
}

void MusicSimilarity::pause()
{
    AutoLock autoLock(&mLock);
    return pause_l();
}

void MusicSimilarity::pause_l()
{
    if (!(mFlags & STREAMING)) {
        return;
    }
    
    modifyFlags(STREAMING, CLEAR);
    
    mQueue.cancelEvent(mCalculateEvent->eventID());
    mCalculateEventPending = false;
    
    if (mMediaFrameOutputer) {
        mMediaFrameOutputer->pause();
    }
    
    modifyFlags(PAUSED, SET);
    
    return;
}

void MusicSimilarity::resume()
{
    AutoLock autoLock(&mLock);
    
    return play_l();
}

void MusicSimilarity::play_l()
{
    if (mFlags & STREAMING) {
        LOGW("%s","DubbingScore is processing");
        return;
    }
    
    if (!(mFlags & CONNECTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    modifyFlags(CONNECTED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    if (mMediaFrameOutputer) {
        mMediaFrameOutputer->start();
    }
    
    postCalculateEvent_l();
    
    modifyFlags(STREAMING, SET);
    
    notifyListener_l(DUBBING_SCORE_PROCESSING);
}

void MusicSimilarity::cancelMusicSimilarityEvents()
{
    mQueue.cancelEvent(mCalculateEvent->eventID());
    mCalculateEventPending = false;
    mQueue.cancelEvent(mNotifyEvent->eventID());
    mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
    mQueue.cancelEvent(mStopEvent->eventID());
}

void MusicSimilarity::stop()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & END) {
        return;
    }
    
    if (mFlags & ENDING) {
        LOGW("already ending");
        
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
}

void MusicSimilarity::stop_l()
{
    modifyFlags(ENDING, ASSIGN);
    
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void MusicSimilarity::postCalculateEvent_l(int64_t delayUs)
{
    if (mCalculateEventPending) {
        return;
    }
    mCalculateEventPending = true;
    mQueue.postEventWithDelay(mCalculateEvent, delayUs < 0 ? 0 : delayUs);
}

void MusicSimilarity::onStopEvent()
{
    AutoLock autoLock(&mLock);
    
    close_all_pipelines_l();
    
    cancelMusicSimilarityEvents();
    mNotificationQueue.flush();
        
    modifyFlags(END, ASSIGN);
    
    int scoreValue = generateScorevalueFromSimilarity();
    notifyListener_l(DUBBING_SCORE_END, scoreValue);

    pthread_cond_broadcast(&mStopCondition);
}

void MusicSimilarity::onCalculateEvent()
{
    AutoLock autoLock(&mLock);
    
    if (!mCalculateEventPending) {
        // The event has been cancelled in reset_l() but had already // been scheduled for execution at that time.
        return;
    }
    mCalculateEventPending = false;
    
    int ret = flowing_l();
    if (ret==1) {
        postCalculateEvent_l(0);
        return;
    }else if (ret==-7) {
        calculateSimilarityWithMusly();
        stop_l();
        return;
    }else if (ret<0) {
        modifyFlags(STREAMING, CLEAR);
        modifyFlags(ERROR, SET);
        
        if (ret!=AVERROR_EXIT) {
            if (ret==-1) {
                notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_BGM_ERROR);
            }else if (ret==-2) {
                notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_BGM_ERROR);
            }else{
                notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_UNKNOWN);
            }
        }
        
        stop_l();
        
        return;
    }else {
        int64_t waitTimeUs = 1*1000;
        postCalculateEvent_l(waitTimeUs);
        return;
    }
}

void MusicSimilarity::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

void MusicSimilarity::init_ffmpeg_env()
{
    // init ffmpeg env
    av_register_all();
    avcodec_register_all();
    avfilter_register_all();
    
    FFLog::setLogLevel(AV_LOG_WARNING);
}

// -1 : OPEN INPUT AUDIO BGM MATERIAL FAIL
// -2 : OPEN INPUT AUDIO DUB MATERIAL FAIL
// -3 : OPEN OUTPUT MEDIA PRODUCT FAIL
int MusicSimilarity::open_all_pipelines_l()
{
    int ret = open_input_audio_dub_material();
    if (ret<0) {
        return -2;
    }
    
    ret = open_output_media_product();
    if (ret<0) {
        close_input_audio_dub_material();
        
        if (ret==AVERROR_EXIT) {
            return AVERROR_EXIT;
        }
        return -3;
    }
    
    ret = open_input_audio_bgm_material();
    if (ret<0) {
        close_output_media_product();
        close_input_audio_dub_material();
        
        if (ret==AVERROR_EXIT) {
            return AVERROR_EXIT;
        }
        return -1;
    }
    
    return 0;
}

void MusicSimilarity::close_all_pipelines_l()
{
    close_input_audio_bgm_material();
    close_output_media_product();
    close_input_audio_dub_material();
}

//-1 : CREATE MEDIAFRAMEOUTPUTER FAIL
//-2 : MEDIAFRAMEOUTPUTER PREPARE FAIL
//-3 : OPEN AUDIO FILTER FAIL
int MusicSimilarity::open_input_audio_bgm_material()
{
    mMediaFrameOutputer = MediaFrameOutputer::createMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, NULL, mBgmUrl, NULL, 2, false, true, -1, -1);
    if (mMediaFrameOutputer==NULL) {
        LOGE("Create MediaFrameOutputer Fail");
        return -1;
    }
    mMediaFrameOutputer->setListener(this);
    int ret = mMediaFrameOutputer->prepare();
    if (ret<0) {
        MediaFrameOutputer::deleteMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, mMediaFrameOutputer);
        mMediaFrameOutputer = NULL;
        
        if (ret==AVERROR_EXIT) {
            LOGW("Exit MediaFrameOutputer Prepare");
            return AVERROR_EXIT;
        }
        
        LOGE("MediaFrameOutputer Prepare Fail");
        return -2;
    }
    
    char audio_filter_spec[256];
    audio_filter_spec[0] = 0;
    av_strlcatf(audio_filter_spec, sizeof(audio_filter_spec), "anull");
    
    ret = open_audio_filter(mMediaFrameOutputer->getAudioContext(), mWorkSampleRate, mWorkSampleFormat, mWorkChannelCount, mWorkChannelLayout, audio_filter_spec);
    if (ret<0) {
        mMediaFrameOutputer->stop();
        MediaFrameOutputer::deleteMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, mMediaFrameOutputer);
        mMediaFrameOutputer = NULL;
        
        LOGE("Open Audio Filter Fail");
        return -3;
    }
        
    return 0;
}

void MusicSimilarity::close_input_audio_bgm_material()
{
    close_audio_filter();
    
    if (mMediaFrameOutputer) {
        mMediaFrameOutputer->stop();
        MediaFrameOutputer::deleteMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, mMediaFrameOutputer);
        mMediaFrameOutputer = NULL;
    }
}

int MusicSimilarity::open_audio_filter(AudioContext audioContext, int workSampleRate, AVSampleFormat workSampleFormat, int workChannelCount, uint64_t workChannelLayout, const char *filter_spec)
{
    char args[512];
    int ret = 0;
    const AVFilter *buffersrc = NULL;
    const AVFilter *buffersink = NULL;
    AVFilterContext *buffersrc_ctx = NULL;
    AVFilterContext *buffersink_ctx = NULL;
    AVFilterInOut *outputs = avfilter_inout_alloc();
    AVFilterInOut *inputs  = avfilter_inout_alloc();
    AVFilterGraph *filter_graph = avfilter_graph_alloc();
    
    if (!outputs || !inputs || !filter_graph) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        ret = AVERROR(ENOMEM);
        LOGE("No memory space for avfilter alloc");
        return ret;
    }
    
    buffersrc = avfilter_get_by_name("abuffer");
    buffersink = avfilter_get_by_name("abuffersink");
    
    if (!buffersrc || !buffersink) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("filtering source or sink element not found");
        ret = AVERROR_UNKNOWN;
        return ret;
    }
    
    if (!audioContext.channel_layout)
    {
        audioContext.channel_layout = av_get_default_channel_layout(audioContext.channels);
    }
    
    snprintf(args, sizeof(args),
             "time_base=%d/%d:sample_rate=%d:sample_fmt=%s:channel_layout=0x%" PRIx64,
             audioContext.time_base.num, audioContext.time_base.den, audioContext.sample_rate,
             av_get_sample_fmt_name(audioContext.sample_fmt),
             audioContext.channel_layout);
    
    ret = avfilter_graph_create_filter(&buffersrc_ctx, buffersrc, "in",
                                       args, NULL, filter_graph);
    
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot create audio buffer source");
        return ret;
    }
    
    ret = avfilter_graph_create_filter(&buffersink_ctx, buffersink, "out",
                                       NULL, NULL, filter_graph);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot create audio buffer sink");
        return ret;
    }
    
    ret = av_opt_set_bin(buffersink_ctx, "sample_fmts",
                         (uint8_t*)&workSampleFormat, sizeof(workSampleFormat),
                         AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot set output sample format");
        return ret;
    }
    
    ret = av_opt_set_bin(buffersink_ctx, "channel_layouts",
                         (uint8_t*)&workChannelLayout,
                         sizeof(workChannelLayout), AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot set output channel layout");
        return ret;
    }
    
    ret = av_opt_set_bin(buffersink_ctx, "sample_rates",
                         (uint8_t*)&workSampleRate, sizeof(workSampleRate),
                         AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot set output sample rate");
        return ret;
    }
    
    /* Endpoints for the filter graph. */
    outputs->name       = av_strdup("in");
    outputs->filter_ctx = buffersrc_ctx;
    outputs->pad_idx    = 0;
    outputs->next       = NULL;
    
    inputs->name       = av_strdup("out");
    inputs->filter_ctx = buffersink_ctx;
    inputs->pad_idx    = 0;
    inputs->next       = NULL;
    
    if (!outputs->name || !inputs->name) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("No memory space");
        ret = AVERROR(ENOMEM);
        return ret;
    }
    
    if ((ret = avfilter_graph_parse_ptr(filter_graph, filter_spec,
                                        &inputs, &outputs, NULL)) < 0)
    {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("avfilter_graph_parse_ptr fail");
        return ret;
    }
    
    if ((ret = avfilter_graph_config(filter_graph, NULL)) < 0)
    {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("avfilter_graph_config fail");
        return ret;
    }
    
    /* Fill FilteringContext */
    audio_buffersrc_ctx = buffersrc_ctx;
    audio_buffersink_ctx = buffersink_ctx;
    audio_filter_graph = filter_graph;
    
    avfilter_inout_free(&inputs);
    avfilter_inout_free(&outputs);
    
    return 0;
}

void MusicSimilarity::close_audio_filter()
{
    if (audio_filter_graph) {
        avfilter_graph_free(&audio_filter_graph);
        audio_filter_graph = NULL;
    }
    
    audio_buffersrc_ctx = NULL;
    audio_buffersink_ctx = NULL;
}

// -1 : OPEN DUB WAV FILE FAIL
int MusicSimilarity::open_input_audio_dub_material()
{
    mDubWAVFile = new WAVFileReader(mDubUrl);
    bool ret = mDubWAVFile->open();
    if (!ret) {
        delete mDubWAVFile;
        mDubWAVFile = NULL;
        
        LOGE("Open Dub WAV File Fail");
        return -1;
    }
    
    mWorkChannelCount = mDubWAVFile->getChannelCount();
    mWorkSampleRate = mDubWAVFile->getSampleRate();
    if (mDubWAVFile->getBitsPerSample()/8==1) {
        mWorkSampleFormat = AV_SAMPLE_FMT_U8;
    }else if (mDubWAVFile->getBitsPerSample()/8==2) {
        mWorkSampleFormat = AV_SAMPLE_FMT_S16;
    }else if (mDubWAVFile->getBitsPerSample()/8==4) {
        mWorkSampleFormat = AV_SAMPLE_FMT_S32;
    }else{
        mWorkSampleFormat = AV_SAMPLE_FMT_S16;
    }
    if (mWorkChannelCount==1) {
        mWorkChannelLayout = AV_CH_LAYOUT_MONO;
    }else{
        mWorkChannelLayout = AV_CH_LAYOUT_STEREO;
    }
    
    if(mWorkAudioDubData)
    {
        free(mWorkAudioDubData);
        mWorkAudioDubData = NULL;
    }
    if (mWorkAudioDubData==NULL) {
        mWorkAudioDubDataSize = mWorkSampleRate*mWorkChannelCount*av_get_bytes_per_sample(mWorkSampleFormat)/10; //100ms
        mWorkAudioDubData = (char*)malloc(mWorkAudioDubDataSize);
    }
    return 0;
}

void MusicSimilarity::close_input_audio_dub_material()
{
    if(mWorkAudioDubData)
    {
        free(mWorkAudioDubData);
        mWorkAudioDubData = NULL;
    }
    mWorkAudioDubDataSize = 0;
    
    if (mDubWAVFile) {
        mDubWAVFile->close();
        delete mDubWAVFile;
        mDubWAVFile = NULL;
    }
}

int MusicSimilarity::open_output_media_product()
{
    long workDirLen = strlen(mWorkDir);
    mOutputWAVFilePath = (char*)malloc(workDirLen+128);
    
    char dst[8];
    StringUtils::right(dst, mWorkDir, 1);
    if (dst[0]=='/')
    {
        sprintf(mOutputWAVFilePath, "%s%lld.wav",mWorkDir,systemTimeNs());
    }else{
        sprintf(mOutputWAVFilePath, "%s/%lld.wav",mWorkDir,systemTimeNs());
    }
    
    mOutputWAVFile = new WAVFileWriter(mWorkChannelCount,mWorkSampleRate,mOutputWAVFilePath);
    bool ret = mOutputWAVFile->open();
    if (!ret) {
        delete mOutputWAVFile;
        mOutputWAVFile = NULL;
        
        LOGE("Open Output WAV File Fail");
        return -1;
    }
    
    return 0;
}

void MusicSimilarity::close_output_media_product()
{
    if (mOutputWAVFile) {
        mOutputWAVFile->close();
        delete mOutputWAVFile;
        mOutputWAVFile = NULL;
    }
    
    if (MediaFile::isExist(mOutputWAVFilePath)) {
        MediaFile::deleteFile(mOutputWAVFilePath);
    }
    
    if (mOutputWAVFilePath) {
        free(mOutputWAVFilePath);
        mOutputWAVFilePath = NULL;
    }
}

// 1 : JUST LOOP
// 0 : NO INPUT MEDIAFRAME, WAIT THEN LOOP
//-1 : AUDIO FILTER INPUT FAIL
//-2 : AUDIO FILTER OUTPUT FAIL
//-7 : EOF
//-8 : NO MEMORY SPACE
int MusicSimilarity::flowing_l()
{
    MediaFrame* workMediaFrame = mMediaFrameOutputer->getMediaFrame();
    
    if (workMediaFrame==NULL) {
        return 0;
    }
    
    if (workMediaFrame->type==MEDIA_FRAME_TYPE_AUDIO) {
        workMediaFrame->avFrame->pts = workMediaFrame->pts;
        
        //push the decoded audio frame into the filtergraph
        int ret = av_buffersrc_add_frame_flags(audio_buffersrc_ctx, workMediaFrame->avFrame, 0);
        if (ret<0) {
            LOGE("Error while feeding decoded audio frame into the filtergraph");
            
            workMediaFrame->Free();
            delete workMediaFrame;
            
            return -1;
        }
        
        workMediaFrame->Free();
        delete workMediaFrame;
        
        while (true) {
            //pull filtered audio frames from the filtergraph
            AVFrame *filt_frame = av_frame_alloc();
            if (!filt_frame) {
                LOGE("No Memory Space");
                
                return -8;
            }
            
            ret = av_buffersink_get_frame(audio_buffersink_ctx, filt_frame);
            if (ret < 0) {
                av_frame_free(&filt_frame);
                
                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
//                    LOGW("no more audio frames for output");
                    return 1;
                }else{
                    LOGE("Error while pulling filtered audio frames from the filtergraph");
                    return -2;
                }
            }
            
            filt_frame->pict_type = AV_PICTURE_TYPE_NONE;
            
            //mix
            int filt_frame_size = filt_frame->nb_samples * mWorkChannelCount * av_get_bytes_per_sample(mWorkSampleFormat);
            if (filt_frame_size>mWorkAudioDubDataSize) {
                mWorkAudioDubDataSize = filt_frame_size;
                if (mWorkAudioDubData) {
                    free(mWorkAudioDubData);
                    mWorkAudioDubData = NULL;
                }
                mWorkAudioDubData = (char*)malloc(mWorkAudioDubDataSize);
            }
            if (mDubWAVFile) {
                int ret = mDubWAVFile->getPcmData(&mWorkAudioDubData, filt_frame_size);
                if (ret>0) {
                    mixSamples((int16_t *)(filt_frame->data[0]), (int16_t *)mWorkAudioDubData, ret/2);
                }
            }
            
            if (mOutputWAVFile) {
                mOutputWAVFile->putPcmData((char*)filt_frame->data[0], filt_frame_size);
            }

//            int64_t next_enc_frame_pts = filt_frame->pts;
            av_frame_free(&filt_frame);
        }
    }else if (workMediaFrame->type==MEDIA_FRAME_TYPE_EOF)
    {
        workMediaFrame->Free();
        delete workMediaFrame;
        
        //flushing audio filter
        int ret = av_buffersrc_add_frame_flags(audio_buffersrc_ctx, NULL, 0);
        if (ret<0) {
            LOGE("Error while feeding NULL audio frame into the filtergraph");
            return -1;
        }
        
        while (true) {
            //pull filtered audio frames from the filtergraph
            AVFrame *filt_frame = av_frame_alloc();
            if (!filt_frame) {
                LOGE("No Memory Space");
                
                return -8;
            }
            ret = av_buffersink_get_frame(audio_buffersink_ctx, filt_frame);
            if (ret < 0) {
                av_frame_free(&filt_frame);
                
                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
//                    LOGW("no more audio frames for output");
                }else{
                    LOGE("Error while pulling filtered audio frames from the filtergraph");
                }
                
                break;
            }
            
            filt_frame->pict_type = AV_PICTURE_TYPE_NONE;
            
            //mix
            int filt_frame_size = filt_frame->nb_samples * mWorkChannelCount * av_get_bytes_per_sample(mWorkSampleFormat);
            if (filt_frame_size>mWorkAudioDubDataSize) {
                mWorkAudioDubDataSize = filt_frame_size;
                if (mWorkAudioDubData) {
                    free(mWorkAudioDubData);
                    mWorkAudioDubData = NULL;
                }
                mWorkAudioDubData = (char*)malloc(mWorkAudioDubDataSize);
            }
            if (mDubWAVFile) {
                int ret = mDubWAVFile->getPcmData(&mWorkAudioDubData, filt_frame_size);
                if (ret>0) {
                    mixSamples((int16_t *)(filt_frame->data[0]), (int16_t *)mWorkAudioDubData, ret/2);
                }
            }
            
            if (mOutputWAVFile) {
                mOutputWAVFile->putPcmData((char*)filt_frame->data[0], filt_frame_size);
            }
            
//            int64_t next_enc_frame_pts = filt_frame->pts;
            av_frame_free(&filt_frame);
        }
        
        return -7;
    }
    
    workMediaFrame->Free();
    delete workMediaFrame;
    
    return 0;
}

void MusicSimilarity::calculateSimilarityWithMusly()
{
    LOGD("Available similarity methods: %s",musly_jukebox_listmethods());
    LOGD("Available audio file decoders: %s",musly_jukebox_listdecoders());
    
    LOGD("musly_jukebox_poweron : %s, %s", "mandelellis", "libav");
    musly_jukebox* mj = musly_jukebox_poweron("mandelellis","libav");
    
    musly_track* mt[2];
    mt[0] = musly_track_alloc(mj);
    mt[1] = musly_track_alloc(mj);
    
    LOGD("musly_track_analyze_audiofile : %s",mOriginUrl);
    int ret = musly_track_analyze_audiofile(mj,mOriginUrl,0,0,mt[0]);
    if (ret==0) {
        LOGD("musly_track_analyze_audiofile success [%s]", mOriginUrl);
    }else{
        LOGE("musly_track_analyze_audiofile fail [%s]", mOriginUrl);
        musly_track_free(mt[0]);
        musly_track_free(mt[1]);
        musly_jukebox_poweroff(mj);
        return;
    }
    
    LOGD("musly_track_analyze_audiofile : %s",mOutputWAVFilePath);
    ret = musly_track_analyze_audiofile(mj,mOutputWAVFilePath,0,0,mt[1]);
    if (ret==0) {
        LOGD("musly_track_analyze_audiofile success [%s]",mOutputWAVFilePath);
    }else{
        LOGE("musly_track_analyze_audiofile fail [%s]", mOutputWAVFilePath);
        musly_track_free(mt[0]);
        musly_track_free(mt[1]);
        musly_jukebox_poweroff(mj);
        return;
    }
    
    LOGD("musly_jukebox_setmusicstyle");
    ret = musly_jukebox_setmusicstyle(mj,mt,2);
    if (ret==0) {
        LOGD("musly_jukebox_setmusicstyle success");
    }else{
        LOGE("musly_jukebox_setmusicstyle fail");
        musly_track_free(mt[0]);
        musly_track_free(mt[1]);
        musly_jukebox_poweroff(mj);
        return;
    }
    
    LOGD("musly_jukebox_addtracks");
    musly_trackid* mtid = new musly_trackid[2];
    ret= musly_jukebox_addtracks(mj,mt,mtid, 2, true);
    if (ret==0) {
        LOGD("musly_jukebox_addtracks success");
    }else{
        LOGE("musly_jukebox_addtracks fail");
        delete [] mtid;
        musly_track_free(mt[0]);
        musly_track_free(mt[1]);
        musly_jukebox_poweroff(mj);
        return;
    }
    
    LOGD("musly_jukebox_similarity");
    ret=musly_jukebox_similarity(mj,mt[0],mtid[0],&(mt[1]),&(mtid[1]),1,&similarity);
    if (ret==0) {
        LOGD("musly_jukebox_similarity success");
    }else{
        LOGE("musly_jukebox_similarity fail");
    }
    
    delete [] mtid;
    musly_track_free(mt[0]);
    musly_track_free(mt[1]);
    musly_jukebox_poweroff(mj);
    
    LOGD("similarity result : %f",similarity);
}

int MusicSimilarity::generateScorevalueFromSimilarity()
{
    float similarity_coefficient = 0.0f;
    similarity_coefficient = 1.0f-similarity/MUSLY_MANDELELLIS_SIMILARITY_MAX_VALUE;
    if (similarity_coefficient<0) {
        similarity_coefficient = 0;
    }
    
    int ret = similarity_coefficient * 1000;
    if (ret<600) {
        ret = 600;
    }
    
    if (ret>1000) {
        ret = 1000;
    }
    
    return ret;
    
//    if (similarity_coefficient<=0.3f) {
//        return (rand() % (75-60))+ 60 +1;
//    }else if(similarity_coefficient>0.3f && similarity_coefficient<=0.5f) {
//        return (rand() % (80-75))+ 75 +1;
//    }else if(similarity_coefficient>0.5f && similarity_coefficient<=0.75f) {
//        return (rand() % (85-80))+ 80 +1;
//    }else if(similarity_coefficient>0.75f && similarity_coefficient<=0.85f) {
//        return (rand() % (88-84))+ 84 +1;
//    }else if(similarity_coefficient>0.85f && similarity_coefficient<=0.90f) {
//        return (rand() % (90-86))+ 86 +1;
//    }else if(similarity_coefficient>0.90f && similarity_coefficient<=0.95f) {
//        return (rand() % (94-88))+ 88 +1;
//    }else if(similarity_coefficient>0.95f && similarity_coefficient<=0.99f) {
//        return (rand() % (98-92))+ 92 +1;
//    }else if(similarity_coefficient>0.99f && similarity_coefficient<=1.0f) {
//        return (rand() % (99-97))+ 97 +1;
//    }else {
//        return (rand() % (100-60))+ 60;
//    }
}
