//
//  MicrophonePCMRecorder.h
//  MediaStreamer
//
//  Created by Think on 2019/7/11.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef MicrophonePCMRecorder_h
#define MicrophonePCMRecorder_h

#include <stdio.h>
#include "IPCMDataOutputer.h"
#include "AudioCapture.h"
#include <pthread.h>

#include "IPCMDataInputer.h"
#include "AudioRender.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
#include <libavutil/avstring.h>

#include "libavutil/avhook.h"
}

#include "AudioNoiseSuppression.h"

#ifdef ANDROID
#include "jni.h"
#endif

class MicrophonePCMRecorder : public IPCMDataOutputer, IPCMDataInputer {
public:
    MicrophonePCMRecorder(int sampleRate, int numChannels, char* workPCMFilePath);
    ~MicrophonePCMRecorder();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
    bool open();
    
    bool startRecord();
    bool startRecord(int64_t startPositionUs);
    
    void stopRecord();
    
    void updateRecordTimeUs(int64_t timeUs);
    int64_t getRecordTimeUs();
    int getPcmDB();
    
    void setEarReturn(bool isEnableEarReturn);
    void enableNoiseSuppression(bool isEnableNoiseSuppression);
    
    void close();
    
    void onOutputPCMData(uint8_t * data, int size);
    int onInputPCMData(uint8_t **pData, int size);
private:
    int mSampleRate;
    int mNumChannels;
    char* mWorkPCMFilePath;
#ifdef ANDROID
    JavaVM *mJvm;
#endif
private:
    pthread_mutex_t mLock;
    bool isOpened;
    bool isRecording;
private:
    AUDIO_CAPTURE_TYPE mAudioCaptureType;
    AudioCapture* mAudioCapture;
private:
    FILE *mWorkPCMFile;
    int64_t mRecordTimeUs;
    int mPcmDB;
    pthread_mutex_t mProduceLock;
private:
    bool isEarReturn;
    
    AudioRenderType mAudioRenderType;
    AudioRender* mAudioRender;
    
    AVAudioFifo* mAudioRenderFifo;
    pthread_mutex_t mAudioRenderFifoLock;

    bool openAudioRender();
    void closeAudioRender();
    bool isAudioRenderOpened;
    
    bool startAudioRender();
    void stopAudioRender();
    bool isAudioRenderPlaying;
private:
    AudioNoiseSuppression* mAudioNoiseSuppression;
    pthread_mutex_t mNSLock;
    bool isEnableNS;
};

#endif /* MicrophonePCMRecorder_h */
