//
//  FFAudioFileWriter.h
//  MediaStreamer
//
//  Created by Think on 2019/9/4.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef FFAudioFileWriter_h
#define FFAudioFileWriter_h

#include <stdio.h>
#include "IPCMWriter.h"
#include "MediaTranscoder.h"

#include <pthread.h>

using namespace MT;

class FFAudioFileWriter : public IPCMWriter {
public:
    FFAudioFileWriter(int channels, int sample_rate, char* audioFilePath);
    ~FFAudioFileWriter();
    
    bool open();
    void putPcmData(char* data, int size);
    int32_t getWriteTimeMs();
    void close();
private:
    static void onMediaTranscoderListener(void* owner, int event, int ext1, int ext2);
    void handleMediaTranscoderListener(int event, int ext1, int ext2);
private:
    int mChannels;
    int mSampleRate;
    char* mAudioFilePath;
private:
    MediaTranscoder* mMediaTranscoder;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    int mOpenRet;
private:
    int64_t pts;
};

#endif /* FFAudioFileWriter_h */
