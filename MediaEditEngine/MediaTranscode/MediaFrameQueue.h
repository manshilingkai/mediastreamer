//
//  MediaFrameQueue.h
//  Worker
//
//  Created by Think on 2018/9/26.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef MediaFrameQueue_h
#define MediaFrameQueue_h

#include <stdio.h>
#include <pthread.h>
#include <queue>

#include "MediaDataStruct.h"

#define MAX_MEDIA_FRAME_QUEUE_COUNT 4

using namespace std;

class MediaFrameQueue {
public:
    MediaFrameQueue();
    ~MediaFrameQueue();
    
    void push(MediaFrame* mediaFrame);
    MediaFrame* pop();
    MediaFrame* front();
    
    void flush();

    int count(MEDIA_FRAME_TYPE type);
    int count();
    
    long size(MEDIA_FRAME_TYPE type);
    long size();

private:
    pthread_mutex_t mLock;
    queue<MediaFrame*> mMediaFrameQueue;
    
    int mVideoFramesCount;
    int mAudioFramesCount;
    
    int mVideoFramesSize;
    int mAudioFramesSize;
};

#endif /* MediaFrameQueue_h */
