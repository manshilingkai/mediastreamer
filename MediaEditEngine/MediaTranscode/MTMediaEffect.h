//
//  MTMediaEffect.h
//  Worker
//
//  Created by Think on 2018/9/21.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef MTMediaEffect_h
#define MTMediaEffect_h

#include <stdio.h>
#include <stdlib.h>
#include <vector>

using namespace std;

#define MAX_MEDIA_EFFECT_COUNT 64

namespace MT
{

enum MEDIA_EFFECT_TYPE {
    MEDIA_EFFECT_TYPE_DEFAULT = -1,
    MEDIA_EFFECT_TYPE_VOLUME = 0,
    MEDIA_EFFECT_TYPE_SPEED = 1,
    MEDIA_EFFECT_TYPE_SUBTITLE = 2,
    MEDIA_EFFECT_TYPE_PNG = 3,
    MEDIA_EFFECT_TYPE_FILTER = 4,
    MEDIA_EFFECT_TYPE_WEBP = 5,
    MEDIA_EFFECT_TYPE_GIF = 6,
    MEDIA_EFFECT_TYPE_AUDIO_MIX = 7,
    
    MEDIA_EFFECT_TYPE_VIDEO_ROTATE = 8,
    MEDIA_EFFECT_TYPE_VIDEO_FLIP = 9,
    
    MEDIA_EFFECT_TYPE_MP4_ALPHA = 10,
    MEDIA_EFFECT_TYPE_MP4 = 11,
    
    MEDIA_EFFECT_TYPE_PRIVATE = 12,
    MEDIA_EFFECT_TYPE_EXTERNAL_RENDER = 13,
};

enum IMAGE_FILTER_TYPE {
    IMAGE_FILTER_TYPE_DEFAULT = -1,
    IMAGE_FILTER_TYPE_RGB = 0,
    IMAGE_FILTER_TYPE_SKETCH = 1,
    IMAGE_FILTER_TYPE_AMARO = 2,
    IMAGE_FILTER_TYPE_ANTIQUE = 3,
    IMAGE_FILTER_TYPE_BLACKCAT = 4,
    IMAGE_FILTER_TYPE_BEAUTY = 5,
    IMAGE_FILTER_TYPE_BRANNAN = 6,
    IMAGE_FILTER_TYPE_N1977 = 7,
    IMAGE_FILTER_TYPE_BROOKLYN = 8,
    IMAGE_FILTER_TYPE_COOL = 9,
    IMAGE_FILTER_TYPE_CRAYON = 10,
    
    IMAGE_FILTER_TYPE_BRIGHTNESS = 11,
    IMAGE_FILTER_TYPE_CONTRAST = 12,
    IMAGE_FILTER_TYPE_EXPOSURE = 13,
    IMAGE_FILTER_TYPE_HUE = 14,
    IMAGE_FILTER_TYPE_SATURATION = 15,
    IMAGE_FILTER_TYPE_SHARPEN = 16,

    IMAGE_FILTER_TYPE_ENHANCEMENT = 100,
};

struct FontColor
{
    float r;
    float g;
    float b;
    float a;
    
    FontColor()
    {
        r = 1.0f;
        g = 1.0f;
        b = 1.0f;
        a = 1.0f;
    }
};

struct Bullet
{
    int bulletChannel;
    int beginTimeMs;
    
    char* headIcon;
    char* text;
    char* tailIcon;
    
    Bullet()
    {
        bulletChannel = -1;
        beginTimeMs = 0;
        
        headIcon = NULL;
        text = NULL;
        tailIcon = NULL;
    }
    
    inline void Free()
    {
        if (headIcon) {
            free(headIcon);
            headIcon = NULL;
        }
        if (text) {
            free(text);
            text = NULL;
        }
        if (tailIcon) {
            free(tailIcon);
            tailIcon = NULL;
        }
    }
};

struct MediaEffect {
    MEDIA_EFFECT_TYPE type;
    
    char* resourceUrl;
    
    long effect_in_ms; //ms
    long effect_out_ms; //ms
    
    //for audio
    float volume;
    float speed;
    
    //for subtitle
    
    //for overlay
    int x;
    int y;
    float scale;
    float rotation;

    //for filter
    IMAGE_FILTER_TYPE image_filter_type;
    
    //for video
    int video_rotation_degree; //0 90 180 270
    int video_flip; //0:no flip 1:vflip 2:hflip
    
    void* reserved;
    
    //private
    char* private_logo_icon; //logo
    char* private_logo_id; //logo
    char* private_logo_fontlib_path; //logo
    char* private_mask_horizontal; //mask
    char* private_mask_vertical; //mask
    char* private_bullet_fontlib_path; //bullet
    vector<Bullet*> bullets; //bullet
    
    MediaEffect()
    {
        type = MEDIA_EFFECT_TYPE_DEFAULT;
        
        resourceUrl = NULL;
        
        effect_in_ms = -1;
        effect_out_ms = -1;
        
        volume = 1.0f;
        speed = 1.0f;
        
        x = 0;
        y = 0;
        scale = 1.0f;
        rotation = 0.0f;
        
        image_filter_type = IMAGE_FILTER_TYPE_DEFAULT;
        
        video_rotation_degree = 0;
        video_flip = 0;
        
        reserved = NULL;
        
        private_logo_icon = NULL;
        private_logo_id = NULL;
        private_logo_fontlib_path = NULL;
        private_mask_horizontal = NULL;
        private_mask_vertical = NULL;
        private_bullet_fontlib_path = NULL;
    }
    
    inline void Free()
    {
        if(resourceUrl)
        {
            free(resourceUrl);
            resourceUrl = NULL;
        }
        
        if (private_logo_icon) {
            free(private_logo_icon);
            private_logo_icon = NULL;
        }
        
        if (private_logo_id) {
            free(private_logo_id);
            private_logo_id = NULL;
        }
        
        if (private_logo_fontlib_path) {
            free(private_logo_fontlib_path);
            private_logo_fontlib_path = NULL;
        }
        
        if (private_mask_horizontal) {
            free(private_mask_horizontal);
            private_mask_horizontal = NULL;
        }
        
        if (private_mask_vertical) {
            free(private_mask_vertical);
            private_mask_vertical = NULL;
        }
        
        if (private_bullet_fontlib_path) {
            free(private_bullet_fontlib_path);
            private_bullet_fontlib_path = NULL;
        }
        
        for(vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end(); ++it)
        {
            Bullet* bullet = *it;
            if(bullet!=NULL)
            {
                bullet->Free();
                delete bullet;
                bullet = NULL;
            }
        }
        bullets.clear();
    }
};

struct MediaEffectGroup
{
    MediaEffect *mediaEffects[MAX_MEDIA_EFFECT_COUNT];
    int mediaEffectCount;
    
    MediaEffectGroup()
    {
        for(int i = 0; i < MAX_MEDIA_EFFECT_COUNT; i++)
        {
            mediaEffects[i] = NULL;
        }
        
        mediaEffectCount = 0;
    }
    
    inline void Clear()
    {
        for(int i = 0; i < MAX_MEDIA_EFFECT_COUNT; i++)
        {
            if(mediaEffects[i]!=NULL)
            {
                delete mediaEffects[i];
                mediaEffects[i] = NULL;
            }
        }
        
        mediaEffectCount = 0;
    }
    
    inline void Free()
    {
        for(int i = 0; i < MAX_MEDIA_EFFECT_COUNT; i++)
        {
            if(mediaEffects[i]!=NULL)
            {
                mediaEffects[i]->Free();
                delete mediaEffects[i];
                mediaEffects[i] = NULL;
            }
        }
        
        mediaEffectCount = 0;
    }
};

};

#endif /* MTMediaEffect_h */
