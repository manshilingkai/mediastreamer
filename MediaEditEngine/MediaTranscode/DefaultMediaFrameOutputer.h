//
//  DefaultMediaFrameOutputer.h
//  Worker
//
//  Created by Think on 2018/9/21.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef DefaultMediaFrameOutputer_h
#define DefaultMediaFrameOutputer_h

#include <stdio.h>
#include <pthread.h>
#include "MediaFrameOutputer.h"
#include "MediaFrameQueue.h"

class DefaultMediaFrameOutputer : public MediaFrameOutputer{
public:
    DefaultMediaFrameOutputer(MediaLog* mediaLog, char* inputUrl, char* http_proxy, int reconnectCount, bool needVideo, bool needAudio, long startPosMs, long endPosMs);
    ~DefaultMediaFrameOutputer();
    
    void setListener(IMediaListener* listener);
    
    int prepare();
    void start();
    void pause();
    void stop();
    void interrupt();

    MediaFrame* getMediaFrame();
    MediaFrame* frontMediaFrame();
    
    VideoContext getVideoContext();
    AudioContext getAudioContext();
    
    int64_t getDurationMs();
private:
    MediaLog* mMediaLog;
    char* mInputUrl;
    char* mHttpProxy;
    int mReconnectCount;
    IMediaListener* mListener;
    void notifyListener(int event, int ext1 = 0, int ext2 = 0);
private:
    pthread_t mThread;
    pthread_cond_t mCondition;
    pthread_mutex_t mLock;
    
    bool isMediaFrameOutputerThreadCreated;
    void createMediaFrameOutputerThread();
    static void* handleMediaFrameOutputerThread(void* ptr);
    void mediaFrameOutputerThreadMain();
    void deleteMediaFrameOutputerThread();
    
    bool isBreakThread; // critical value
    bool isWorking; // critical value
private:
    static int interruptCallback(void* opaque);
    int interruptCallbackMain();
    int isInterrupt; // critical value
    pthread_mutex_t mInterruptLock;

private:
    AVFormatContext *ifmt_ctx;
    int mAudioStreamIndex;
    int mVideoStreamIndex;
    int mTextStreamIndex;
    int mFrameRate;
    int mRotate;
    AVCodecContext *audio_dec_ctx;
    AVCodecContext *video_dec_ctx;
    
    MediaFrameQueue mMediaFrameQueue;
private:
    VideoContext mVideoContext;
    AudioContext mAudioContext;
    
private:
    bool isNeedVideo;
    bool isNeedAudio;
private:
    long mStartPosMs;
    long mEndPosMs;
    
    int mTargetVideoFps;
    bool isNeedDropVideoFrame;
    long targetVideoTimeStamp;
private:
    bool gotFirstKeyFrame;
    bool gotFirstAudioPacket;
};

#endif /* DefaultMediaFrameOutputer_h */
