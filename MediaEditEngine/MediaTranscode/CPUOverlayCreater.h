//
//  CPUOverlayCreater.h
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/25.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef CPUOverlayCreater_h
#define CPUOverlayCreater_h

#include <stdio.h>
#include "MediaDataType.h"
#include "MTMediaEffect.h"

using namespace MT;

struct MTBulletScreen {
    char* headIcon;
    char* bodyText;
    char* tailIcon;
    int headbodySpace;
    int bodytailSpace;
    
    //for icon
    int iconWidth;
    int iconHeight;
    
    //for text
    char* fontLibPath;
    int fontSize;
    FontColor fontColor;
    int leftMargin;
    int rightMargin;
    int topMargin;
    int bottomMargin;
    int wordSpace;
    
    MTBulletScreen()
    {
        headIcon = NULL;
        bodyText = NULL;
        tailIcon = NULL;
        headbodySpace = 18;
        bodytailSpace = 6;
        
        iconWidth = 72;
        iconHeight = 72;
        
        fontLibPath = NULL;
        fontSize = 48;
        fontColor.r = 0.0f;
        fontColor.g = 0.0f;
        fontColor.b = 0.0f;
        fontColor.a = 1.0f;
        leftMargin = 0;
        rightMargin = 0;
        topMargin = 0;
        bottomMargin = 0;
        wordSpace = 2;
    }
    
    inline void Free()
    {
        if(headIcon) {
            free(headIcon);
            headIcon = NULL;
        }
        
        if (bodyText) {
            free(bodyText);
            bodyText = NULL;
        }
        
        if (tailIcon) {
            free(tailIcon);
            tailIcon = NULL;
        }
        
        if (fontLibPath) {
            free(fontLibPath);
            fontLibPath = NULL;
        }
    }
};

struct MTLogo {
    char* icon;
    char* text;
    int iconTextSpace;
    
    int iconWidth;
    int iconHeight;
    int textWidth;
    int textHeight;

    MTLogo()
    {
        icon = NULL;
        text = NULL;
        iconTextSpace = 5;
        
        iconWidth = 126;
        iconHeight = 36;
        textWidth = 117;
        textHeight = 33;
        
        fontLibPath = NULL;
        fontSize = 30;
        fontColor.r = 1.0f;
        fontColor.g = 1.0f;
        fontColor.b = 1.0f;
        fontColor.a = 0.4f;
        leftMargin = 0;
        rightMargin = 0;
        topMargin = 0;
        bottomMargin = 0;
        wordSpace = 2;
    }
    
    //for text
    char* fontLibPath;
    int fontSize;
    FontColor fontColor;
    int leftMargin;
    int rightMargin;
    int topMargin;
    int bottomMargin;
    int wordSpace;
    
    inline void Free()
    {
        if (icon) {
            free(icon);
            icon = NULL;
        }
        
        if (text) {
            free(text);
            text = NULL;
        }
        
        if (fontLibPath) {
            free(fontLibPath);
            fontLibPath = NULL;
        }
    }
};

enum MTOverlayMaterialType {
    MT_LOGO = 0,
    MT_BULLET_SCREEN = 1,
};

struct MTOverlayMaterial {
    MTOverlayMaterialType type;
    void * content;
    
    inline void Free()
    {
        if (type == MT_LOGO) {
            MTLogo* logo = (MTLogo*)content;
            if (logo) {
                logo->Free();
                delete logo;
                logo = NULL;
            }
        }else if (type == MT_BULLET_SCREEN) {
            MTBulletScreen* bulletScreen = (MTBulletScreen*)content;
            if (bulletScreen) {
                bulletScreen->Free();
                delete bulletScreen;
                bulletScreen = NULL;
            }
        }
    }
};

#ifdef __cplusplus
extern "C" {
#endif

VideoFrame* produceCPUOverlay(MTOverlayMaterial material);

#ifdef __cplusplus
};
#endif

#endif /* CPUOverlayCreater_h */
