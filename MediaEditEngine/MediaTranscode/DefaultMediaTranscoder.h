//
//  DefaultMediaTranscoder.h
//  Worker
//
//  Created by Think on 2018/9/21.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef DefaultMediaTranscoder_h
#define DefaultMediaTranscoder_h

#include <stdio.h>
#include <pthread.h>
#include <list>

#include "TimedEventQueue.h"
#include "IMediaListener.h"
#include "MediaTranscoder.h"
#include "NotificationQueue.h"
#include "MediaLog.h"

#include "MediaFrameOutputer.h"
#include "MediaFrameQueue.h"

#include "Overlay.h"
#include "ColorSpaceConvert.h"

using namespace MT;

struct BulletView {
    int x;
    int y;
    VideoFrame* bulletImage;
    
    BulletView()
    {
        x = 0;
        y = 0;
        bulletImage = NULL;
    }
    
    inline void Free()
    {
        if (bulletImage) {
            bulletImage->Free();
            delete bulletImage;
            bulletImage = NULL;
        }
    }
};

struct BulletChannelView {
    int bulletSpeed;//px
    std::list<BulletView*> bulletViews;
    int canDrawSpace;
    
    BulletChannelView()
    {
        bulletSpeed = 4;
        canDrawSpace = 0;
    }
    
    inline void Free()
    {
        for(std::list<BulletView*>::iterator it = bulletViews.begin(); it != bulletViews.end(); ++it)
        {
            BulletView* bulletView = *it;
            if (bulletView!=NULL) {
                bulletView->Free();
                delete bulletView;
                bulletView = NULL;
            }
        }
        bulletViews.clear();
    }
};

class DefaultMediaTranscoder : public MediaTranscoder, IMediaListener {
public:
#ifdef ANDROID
    DefaultMediaTranscoder(JavaVM *jvm, MediaLog* mediaLog, MediaMaterial input, MediaEffectGroup *pMediaEffectGroup, MediaProduct product);
#else
    DefaultMediaTranscoder(MediaLog* mediaLog, MediaMaterial input, MediaEffectGroup *pMediaEffectGroup, MediaProduct product);
    DefaultMediaTranscoder(MediaLog* mediaLog, char* configureInfo);
#endif
    ~DefaultMediaTranscoder();
    
#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#else
    void setListener(void (*listener)(void*,int,int,int), void* arg);
    void setMediaDataListener(void (*listener)(void*, void*, int, int, int), void* arg);
#endif
    
    void pushMediaFrameFromExternal(MediaFrame* externalMediaFrame);

    void start();
    void resume();
    void pause();
    void stop(bool isCancle = false);
    
    void notify(int event, int ext1, int ext2);
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
private:
    enum {
        CONNECTING           = 0x01,
        CONNECTED            = 0x02,
        STREAMING            = 0x04,
        PAUSED               = 0x10,
        ERROR                = 0x20,
        END                  = 0x40,
        INITIALIZED          = 0x80,
        ENDING               = 0x100,
    };
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);

private:
    friend struct DefaultMediaTranscoderEvent;
    
    DefaultMediaTranscoder(const DefaultMediaTranscoder &);
    DefaultMediaTranscoder &operator=(const DefaultMediaTranscoder &);
    
    pthread_mutex_t mLock;

    TimedEventQueue mQueue;
    
    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mEncodeEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mNotifyEvent;
    bool mEncodeEventPending;
    
    void prepareAsync_l();
    void play_l();
    void pause_l();
    void stop_l();
    
    void postEncodeEvent_l(int64_t delayUs = -1);
    void postNotifyEvent_l();
    
    void onPrepareAsyncEvent();
    void onEncodeEvent();
    void onStopEvent();
    void onNotifyEvent();
    
    void cancelTranscoderEvents();
    
    pthread_cond_t mStopCondition;
    
    NotificationQueue mNotificationQueue;
    
private:
    int open_all_pipelines_l();
    void close_all_pipelines_l();
    int flowing_l();
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
private:
    MediaLog* mMediaLog;
    
    MediaMaterial mInput;
    MediaEffectGroup *mMediaEffectGroup;
    MediaProduct mProduct;
    bool isLocalFile;
    
    char *output_fmt_name;
    
    IMediaListener* mMediaListener;
    
    MediaFrameOutputer* mMediaFrameOutputer;
    pthread_mutex_t mMediaFrameOutputerLock;
    
    VideoContext mInputVideoContext;
    AudioContext mInputAudioContext;
    
    MediaFrameQueue mExternalMediaFrameQueue;

private:
    bool gotError;
    
private:
    static void init_ffmpeg_env();

private:
    int open_output_media_product(char *url);
    void close_output_media_product();

    AVFormatContext *ofmt_ctx;
    AVCodecContext *video_encoder_ctx;
    AVCodecContext *audio_encoder_ctx;
    int mOutputAudioStreamIndex;
    int mOutputVideoStreamIndex;
    
private:
    //filter_spec = "null";
    int open_video_filter(VideoContext videoContext, AVCodecContext *video_enc_ctx, const char *filter_spec);
    void close_video_filter();
    
    AVFilterContext *video_buffersrc_ctx;
    AVFilterContext *video_buffersink_ctx;
    AVFilterGraph *video_filter_graph;
    
private:
    //filter_spec = "anull";
    int open_audio_filter(AudioContext audioContext, AVCodecContext* audio_enc_ctx, const char *filter_spec);
    void close_audio_filter();
    
    AVFilterContext *audio_buffersrc_ctx;
    AVFilterContext *audio_buffersink_ctx;
    AVFilterGraph *audio_filter_graph;
    
private:
    AVAudioFifo* filteredAudioDataFifo;
    
private:
    bool mGot_AudioWriteTimeUs_Baseline;
    int64_t mAudioWriteTimeUs_Baseline;
    int64_t mAudioWriteTimeUs;
    
    bool mGot_VideoWriteTimeUs_Baseline;
    int64_t mVideoWriteTimeUs_Baseline;
    int64_t mVideoWriteTimeUs;
    
    int64_t mWriteTimeUs;
    int mLastSendWriteTimeS;
private:
    void produceVideoFilterSpec(bool isIgnoreOverlay);
    bool isImageOverlaying;
    char video_filter_spec[1024];
private:
    // Video Overlay
    void video_overlay(AVFrame *main_in);
    bool ignore_video_overlay;
    bool isVideoOverlaying;

    MediaFrameOutputer* mVideoOverlayMediaFrameOutputer;
    pthread_cond_t mVideoOverlayCondition;
    pthread_mutex_t mVideoOverlayLock;
    int64_t mVideoOverlayTimeMs;
    
    VideoFrame* mVideoMainYUVFrame;
    VideoFrame* mVideoMainRGBAFrame;
    VideoFrame* mVideoOverlayYUVFrame;
    VideoFrame* mVideoOverlayRGBAFrame;
    VideoFrame* mVideoOverlayRGBAAlphaFrame;
    ColorSpaceConvert *mColorSpaceConvert;
private:
    //Private
    VideoFrame* mPrivateMainYUVFrame;
    VideoFrame* mPrivateMainRGBAFrame;
    ColorSpaceConvert *mPrivateColorSpaceConvert;
    VideoFrame* mPrivateLogoRGBAFrame;
    int private_logo_x;
    int private_logo_y;
    VideoFrame* mPrivateMaskRGBAFrame;
    
    int bullet_channel_top_padding;
    int bullet_channel_height;
    int bullet_channel_space;
    int bullet_space;
    int bullet_index;
    
    BulletChannelView mBulletChannelViewOne;
    BulletChannelView mBulletChannelViewTwo;
private:
    void (*mMediaDataListener)(void*,void*,int,int,int);
    void* mMediaDataOwner;
};

#endif /* DefaultMediaTranscoder_h */
