//
//  MediaFrameOutputer.h
//  Worker
//
//  Created by Think on 2018/9/21.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef MediaFrameOutputer_h
#define MediaFrameOutputer_h

#include <stdio.h>
#include "IMediaListener.h"
#include "MediaLog.h"
#include "MediaDataStruct.h"

enum MEDIA_FRAME_OUTPUTER_TYPE{
    MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT = 0,
    MEDIA_FRAME_OUTPUTER_TYPE_WEBRTC = 1,
};

struct VideoContext {
    bool hasVideo;
    
    int width;
    int height;
    enum AVPixelFormat pix_fmt;
    AVRational sample_aspect_ratio;
    
    AVRational time_base;
    
    int rotate;
    
    VideoContext()
    {
        hasVideo = false;
        
        width = 0;
        height = 0;
        pix_fmt = AV_PIX_FMT_YUV420P;
        sample_aspect_ratio = av_make_q(1,1);
        
        time_base = av_make_q(1,20);
        
        rotate = 0;
    }
};

struct AudioContext {
    bool hasAudio;

    uint64_t channel_layout;
    int channels;
    int sample_rate;
    enum AVSampleFormat sample_fmt;
    
    AVRational time_base;
    
    AudioContext()
    {
        hasAudio = false;
        
        channel_layout = AV_CH_LAYOUT_STEREO;
        channels = 2;
        sample_rate = 44100;
        sample_fmt = AV_SAMPLE_FMT_S16;
        
        time_base = av_make_q(1,sample_rate);
    }
};

class MediaFrameOutputer {
public:
    virtual ~MediaFrameOutputer() {}
    
    static MediaFrameOutputer* createMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE type, MediaLog* mediaLog, char* inputUrl, char* http_proxy, int reconnectCount, bool needVideo, bool needAudio, long startPosMs, long endPosMs);
    static void deleteMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE type, MediaFrameOutputer* mediaFrameOutputer);

    virtual void setListener(IMediaListener* listener) = 0;
    
    virtual int prepare() = 0;
    virtual void start() = 0;
    virtual void pause() = 0;
    virtual void stop() = 0;
    virtual void interrupt() = 0;
    
    virtual MediaFrame* getMediaFrame() = 0;
    virtual MediaFrame* frontMediaFrame() = 0;
    
    virtual VideoContext getVideoContext() = 0;
    virtual AudioContext getAudioContext() = 0;
    
    virtual int64_t getDurationMs() = 0;
};

#endif /* MediaFrameOutputer_h */
