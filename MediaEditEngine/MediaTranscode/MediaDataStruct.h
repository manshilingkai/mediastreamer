//
//  MediaDataStruct.h
//  Worker
//
//  Created by Think on 2018/9/26.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef MediaDataStruct_h
#define MediaDataStruct_h

#define KNatveVideoMaxFps                    30

#include <stdio.h>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
#include "libavutil/avstring.h"
}

enum MEDIA_FRAME_TYPE {
    MEDIA_FRAME_TYPE_UNKNOWN = -0xFF,
    MEDIA_FRAME_TYPE_ERROR = -2,
    MEDIA_FRAME_TYPE_EOF = -1,
    MEDIA_FRAME_TYPE_AUDIO = 0,
    MEDIA_FRAME_TYPE_VIDEO = 1,
    MEDIA_FRAME_TYPE_SUBTITLE = 2,
};

struct MediaFrame {
    MEDIA_FRAME_TYPE type;

    AVFrame* avFrame;
    int64_t pts;
    int64_t duration;
    long size;
    
    MediaFrame()
    {
        type = MEDIA_FRAME_TYPE_UNKNOWN;

        avFrame = NULL;
        pts = 0;
        duration = 0;
        size = 0;
    }
    
    inline bool Alloc()
    {
        avFrame = av_frame_alloc();
        if(avFrame) return true;
        else return false;
    }
    
    inline void Free()
    {
        type = MEDIA_FRAME_TYPE_UNKNOWN;

        if(avFrame)
        {
            av_frame_free(&avFrame);
            avFrame = NULL;
        }
        
        pts = 0;
        duration = 0;
        size = 0;
    }
};

#endif /* MediaDataStruct_h */
