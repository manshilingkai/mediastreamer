//
//  MTMediaProduct.h
//  Worker
//
//  Created by Think on 2018/9/21.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef MTMediaProduct_h
#define MTMediaProduct_h

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define KNatveVideoFps                       24

#define AUDIO_CH_FRONT_LEFT             0x00000001
#define AUDIO_CH_FRONT_RIGHT            0x00000002
#define AUDIO_CH_FRONT_CENTER           0x00000004
#define AUDIO_CH_LOW_FREQUENCY          0x00000008
#define AUDIO_CH_BACK_LEFT              0x00000010
#define AUDIO_CH_BACK_RIGHT             0x00000020
#define AUDIO_CH_FRONT_LEFT_OF_CENTER   0x00000040
#define AUDIO_CH_FRONT_RIGHT_OF_CENTER  0x00000080
#define AUDIO_CH_BACK_CENTER            0x00000100
#define AUDIO_CH_SIDE_LEFT              0x00000200
#define AUDIO_CH_SIDE_RIGHT             0x00000400
#define AUDIO_CH_TOP_CENTER             0x00000800
#define AUDIO_CH_TOP_FRONT_LEFT         0x00001000
#define AUDIO_CH_TOP_FRONT_CENTER       0x00002000
#define AUDIO_CH_TOP_FRONT_RIGHT        0x00004000
#define AUDIO_CH_TOP_BACK_LEFT          0x00008000
#define AUDIO_CH_TOP_BACK_CENTER        0x00010000
#define AUDIO_CH_TOP_BACK_RIGHT         0x00020000
#define AUDIO_CH_STEREO_LEFT            0x20000000  ///< Stereo downmix.
#define AUDIO_CH_STEREO_RIGHT           0x40000000  ///< See AUDIO_CH_STEREO_LEFT.
#define AUDIO_CH_WIDE_LEFT              0x0000000080000000ULL
#define AUDIO_CH_WIDE_RIGHT             0x0000000100000000ULL
#define AUDIO_CH_SURROUND_DIRECT_LEFT   0x0000000200000000ULL
#define AUDIO_CH_SURROUND_DIRECT_RIGHT  0x0000000400000000ULL
#define AUDIO_CH_LOW_FREQUENCY_2        0x0000000800000000ULL

/** Channel mask value used for AVCodecContext.request_channel_layout
 to indicate that the user requests the channel order of the decoder output
 to be the native codec channel order. */
#define AUDIO_CH_LAYOUT_NATIVE          0x8000000000000000ULL

/**
 * @}
 * @defgroup channel_mask_c Audio channel layouts
 * @{
 * */
#define AUDIO_CH_LAYOUT_MONO              (AUDIO_CH_FRONT_CENTER)
#define AUDIO_CH_LAYOUT_STEREO            (AUDIO_CH_FRONT_LEFT|AUDIO_CH_FRONT_RIGHT)
#define AUDIO_CH_LAYOUT_2POINT1           (AUDIO_CH_LAYOUT_STEREO|AUDIO_CH_LOW_FREQUENCY)
#define AUDIO_CH_LAYOUT_2_1               (AUDIO_CH_LAYOUT_STEREO|AUDIO_CH_BACK_CENTER)
#define AUDIO_CH_LAYOUT_SURROUND          (AUDIO_CH_LAYOUT_STEREO|AUDIO_CH_FRONT_CENTER)
#define AUDIO_CH_LAYOUT_3POINT1           (AUDIO_CH_LAYOUT_SURROUND|AUDIO_CH_LOW_FREQUENCY)
#define AUDIO_CH_LAYOUT_4POINT0           (AUDIO_CH_LAYOUT_SURROUND|AUDIO_CH_BACK_CENTER)
#define AUDIO_CH_LAYOUT_4POINT1           (AUDIO_CH_LAYOUT_4POINT0|AUDIO_CH_LOW_FREQUENCY)
#define AUDIO_CH_LAYOUT_2_2               (AUDIO_CH_LAYOUT_STEREO|AUDIO_CH_SIDE_LEFT|AUDIO_CH_SIDE_RIGHT)
#define AUDIO_CH_LAYOUT_QUAD              (AUDIO_CH_LAYOUT_STEREO|AUDIO_CH_BACK_LEFT|AUDIO_CH_BACK_RIGHT)
#define AUDIO_CH_LAYOUT_5POINT0           (AUDIO_CH_LAYOUT_SURROUND|AUDIO_CH_SIDE_LEFT|AUDIO_CH_SIDE_RIGHT)
#define AUDIO_CH_LAYOUT_5POINT1           (AUDIO_CH_LAYOUT_5POINT0|AUDIO_CH_LOW_FREQUENCY)
#define AUDIO_CH_LAYOUT_5POINT0_BACK      (AUDIO_CH_LAYOUT_SURROUND|AUDIO_CH_BACK_LEFT|AUDIO_CH_BACK_RIGHT)
#define AUDIO_CH_LAYOUT_5POINT1_BACK      (AUDIO_CH_LAYOUT_5POINT0_BACK|AUDIO_CH_LOW_FREQUENCY)
#define AUDIO_CH_LAYOUT_6POINT0           (AUDIO_CH_LAYOUT_5POINT0|AUDIO_CH_BACK_CENTER)
#define AUDIO_CH_LAYOUT_6POINT0_FRONT     (AUDIO_CH_LAYOUT_2_2|AUDIO_CH_FRONT_LEFT_OF_CENTER|AUDIO_CH_FRONT_RIGHT_OF_CENTER)
#define AUDIO_CH_LAYOUT_HEXAGONAL         (AUDIO_CH_LAYOUT_5POINT0_BACK|AUDIO_CH_BACK_CENTER)
#define AUDIO_CH_LAYOUT_6POINT1           (AUDIO_CH_LAYOUT_5POINT1|AUDIO_CH_BACK_CENTER)
#define AUDIO_CH_LAYOUT_6POINT1_BACK      (AUDIO_CH_LAYOUT_5POINT1_BACK|AUDIO_CH_BACK_CENTER)
#define AUDIO_CH_LAYOUT_6POINT1_FRONT     (AUDIO_CH_LAYOUT_6POINT0_FRONT|AUDIO_CH_LOW_FREQUENCY)
#define AUDIO_CH_LAYOUT_7POINT0           (AUDIO_CH_LAYOUT_5POINT0|AUDIO_CH_BACK_LEFT|AUDIO_CH_BACK_RIGHT)
#define AUDIO_CH_LAYOUT_7POINT0_FRONT     (AUDIO_CH_LAYOUT_5POINT0|AUDIO_CH_FRONT_LEFT_OF_CENTER|AUDIO_CH_FRONT_RIGHT_OF_CENTER)
#define AUDIO_CH_LAYOUT_7POINT1           (AUDIO_CH_LAYOUT_5POINT1|AUDIO_CH_BACK_LEFT|AUDIO_CH_BACK_RIGHT)
#define AUDIO_CH_LAYOUT_7POINT1_WIDE      (AUDIO_CH_LAYOUT_5POINT1|AUDIO_CH_FRONT_LEFT_OF_CENTER|AUDIO_CH_FRONT_RIGHT_OF_CENTER)
#define AUDIO_CH_LAYOUT_7POINT1_WIDE_BACK (AUDIO_CH_LAYOUT_5POINT1_BACK|AUDIO_CH_FRONT_LEFT_OF_CENTER|AUDIO_CH_FRONT_RIGHT_OF_CENTER)
#define AUDIO_CH_LAYOUT_OCTAGONAL         (AUDIO_CH_LAYOUT_5POINT0|AUDIO_CH_BACK_LEFT|AUDIO_CH_BACK_CENTER|AUDIO_CH_BACK_RIGHT)
#define AUDIO_CH_LAYOUT_HEXADECAGONAL     (AUDIO_CH_LAYOUT_OCTAGONAL|AUDIO_CH_WIDE_LEFT|AUDIO_CH_WIDE_RIGHT|AUDIO_CH_TOP_BACK_LEFT|AUDIO_CH_TOP_BACK_RIGHT|AUDIO_CH_TOP_BACK_CENTER|AUDIO_CH_TOP_FRONT_CENTER|AUDIO_CH_TOP_FRONT_LEFT|AUDIO_CH_TOP_FRONT_RIGHT)
#define AUDIO_CH_LAYOUT_STEREO_DOWNMIX    (AUDIO_CH_STEREO_LEFT|AUDIO_CH_STEREO_RIGHT)

namespace MT
{

enum MEDIA_PRODUCT_TYPE {
    MEDIA_PRODUCT_TYPE_MP4 = 0,
    MEDIA_PRODUCT_TYPE_TS = 1,
    MEDIA_PRODUCT_TYPE_RTMP = 2,
    MEDIA_PRODUCT_TYPE_M4A = 3,
    MEDIA_PRODUCT_TYPE_MP3 = 4,
    MEDIA_PRODUCT_TYPE_GIF = 5,
};

enum VIDEO_ENCODER_TYPE {
    VIDEO_ENCODER_TYPE_X264 = 0,
    VIDEO_ENCODER_TYPE_X265 = 1,
    VIDEO_ENCODER_TYPE_VAAPI = 2,
};

enum VIDEO_ENCODE_MODE {
    VIDEO_ENCODE_MODE_VBR = 0,
    VIDEO_ENCODE_MODE_ABR = 1,
    VIDEO_ENCODE_MODE_CBR = 2,
};

static const char * const profile_names[] = { "baseline", "main", "high", "high10", "high422", "high444", 0 };

enum VIDEO_ENCODE_PROFILE {
    BASELINE = 0,
    MAIN = 1,
    HIGH = 2,
    HIGH_10 = 3,
    HIGH_422 = 4,
    HIGH_444_PREDICTIVE = 5,
};

static const char * const level_names[] = { "1", "1b", "1.1", "1.2", "1.3", "2", "2.1", "2.2", "3", "3.1", "3.2", "4", "4.1", "4.2", "5", "5.1" };

enum VIDEO_ENCODE_LEVEL {
    LEVEL_10 = 0,
    LEVEL_1b = 1,
    LEVEL_11 = 2,
    LEVEL_12 = 3,
    LEVEL_13 = 4,
    LEVEL_20 = 5,
    LEVEL_21 = 6,
    LEVEL_22 = 7,
    LEVEL_30 = 8,
    LEVEL_31 = 9,
    LEVEL_32 = 10,
    LEVEL_40 = 11,
    LEVEL_41 = 12,
    LEVEL_42 = 13,
    LEVEL_50 = 14,
    LEVEL_51 = 15,
};


static const char * const preset_names[] = { "ultrafast", "superfast", "veryfast", "faster", "fast", "medium", "slow", "slower", "veryslow", "placebo", 0 };

enum VIDEO_ENCODE_PRESET {
    ULTRAFAST = 0,
    SUPERFAST = 1,
    VERYFAST = 2,
    FASTER = 3,
    FAST = 4,
    MEDIUM = 5,
    SLOW = 6,
    SLOWER = 7,
    VERYSLOW = 8,
    PLACEBO = 9,
};

static const char * const tune_names[] = { "film", "animation", "grain", "stillimage", "psnr", "ssim", "fastdecode", "zerolatency", 0 };

enum VIDEO_ENCODE_TUNE {
    FILM = 0,
    ANIMATION = 1,
    GRAIN = 2,
    STILLIMAGE = 3,
    PSNR = 4,
    SSIM = 5,
    FASTDECODE = 6,
    ZEROLATENCY = 7,
};

struct VideoOptions {
    bool hasVideoTrack;
    
    VIDEO_ENCODER_TYPE videoEncoderType;
    
    int videoWidth;
    int videoHeight;
    int videoFps;
    
    int videoBitrateKbps;
    VIDEO_ENCODE_MODE videoEncodeMode;
    int maxKeyFrameIntervalMs;
    
    int quality; //[-5, 5]:CRF
    int deblockingFilterFactor; //[-6, 6] -6 light filter, 6 strong
    
    VIDEO_ENCODE_PROFILE profile;
    VIDEO_ENCODE_LEVEL level;
    VIDEO_ENCODE_PRESET preset;
    VIDEO_ENCODE_TUNE tune;
    
    VideoOptions()
    {
        hasVideoTrack = true;
        
        videoEncoderType = VIDEO_ENCODER_TYPE_X264;
        
        videoWidth = 0;
        videoHeight = 0;
        videoFps = KNatveVideoFps;
        
        videoBitrateKbps = 1000;
        videoEncodeMode = VIDEO_ENCODE_MODE_ABR;
        maxKeyFrameIntervalMs = 5000;
        
        profile = MAIN;
        level = LEVEL_30;
#ifdef IOS
        preset = ULTRAFAST;
#elif ANDROID
        preset = ULTRAFAST;
#else
        preset = FAST;
#endif
        tune = ZEROLATENCY;
        
        quality = 0;
        deblockingFilterFactor = 0;
    }
};

enum AUDIO_ENCODER_TYPE {
    AUDIO_ENCODER_TYPE_FDKAAC = 0,
    AUDIO_ENCODER_TYPE_FAAC = 1,
    AUDIO_ENCODER_TYPE_FFMPEG_AAC = 2,
    AUDIO_ENCODER_TYPE_FFMPEG_MP3 = 3,
    AUDIO_ENCODER_TYPE_FFMPEG_LAME = 4,
};

enum AudioSampleFormat {
    AUDIO_SAMPLE_FMT_NONE = -1,
    AUDIO_SAMPLE_FMT_U8,          ///< unsigned 8 bits
    AUDIO_SAMPLE_FMT_S16,         ///< signed 16 bits
    AUDIO_SAMPLE_FMT_S32,         ///< signed 32 bits
    AUDIO_SAMPLE_FMT_FLT,         ///< float
    AUDIO_SAMPLE_FMT_DBL,         ///< double
    
    AUDIO_SAMPLE_FMT_U8P,         ///< unsigned 8 bits, planar
    AUDIO_SAMPLE_FMT_S16P,        ///< signed 16 bits, planar
    AUDIO_SAMPLE_FMT_S32P,        ///< signed 32 bits, planar
    AUDIO_SAMPLE_FMT_FLTP,        ///< float, planar
    AUDIO_SAMPLE_FMT_DBLP,        ///< double, planar
    AUDIO_SAMPLE_FMT_S64,         ///< signed 64 bits
    AUDIO_SAMPLE_FMT_S64P,        ///< signed 64 bits, planar
    
    AUDIO_SAMPLE_FMT_NB           ///< Number of sample formats. DO NOT USE if linking dynamically
};

struct AudioOptions {
    bool hasAudioTrack;
    
    AUDIO_ENCODER_TYPE audioEncoderType;
    
    AudioSampleFormat audioSampleFormat;
    int audioSampleRate;
    int audioNumChannels;
    uint64_t channel_layout;
    
    int audioBitrateKbps;
    
    AudioOptions()
    {
        hasAudioTrack = true;
 
        audioEncoderType = AUDIO_ENCODER_TYPE_FDKAAC;
        
        audioSampleFormat = AUDIO_SAMPLE_FMT_S16;
        audioSampleRate = 44100;
        audioNumChannels = 2;
        channel_layout = AUDIO_CH_LAYOUT_STEREO;
        
        audioBitrateKbps = 128;
    }
};

struct MediaProduct {
    MEDIA_PRODUCT_TYPE type;
    char* url;
    
    VideoOptions videoOptions;
    AudioOptions audioOptions;
    
    MediaProduct()
    {
        type = MEDIA_PRODUCT_TYPE_MP4;
        url = NULL;
    }
    
    inline void Free()
    {
        if(url)
        {
            free(url);
            url = NULL;
        }
    }
};

};

#endif /* MTMediaProduct_h */
