//
//  CPUOverlayCreater.cpp
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/25.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "CPUOverlayCreater.h"
#include "ImageProcesserUtils.h"
#include "MTWordToBitmap.h"
#include "Overlay.h"
#include "LibyuvColorSpaceConvert.h"
#include "MediaLog.h"

VideoFrame* produceCPUOverlay(MTOverlayMaterial material)
{
    if (material.type == MT_LOGO) {
        MTLogo* logo = (MTLogo*)material.content;
        
        //ICON
        VideoFrame* icon = PNGImageFileToRGBAVideoFrame(logo->icon);
        if (icon==NULL) return NULL;
        
        //TEXT
        MTWordToBitmap * wordToBitmap = new MTWordToBitmap(logo->fontLibPath, logo->fontSize, logo->fontColor);
        bool ret = wordToBitmap->initialize();
        if (!ret) {
            delete wordToBitmap;
            wordToBitmap = NULL;
            
            if (icon) {
                icon->Free();
                delete icon;
                icon = NULL;
            }
            return NULL;
        }
        size_t size = strlen(logo->text) + 1;
        wchar_t* inputWText = new wchar_t[size];
        mbstowcs(inputWText, logo->text, size);
        vector<VideoFrame*> words;
        int i = -1;
        while (inputWText[++i]!=L'\0') {
            VideoFrame* wordBitmap = wordToBitmap->word2Bitmap(inputWText[i], 1.0f);
            if (wordBitmap) {
//                LOGD("i:%d w:%d h:%d",i,wordBitmap->width,wordBitmap->height);
                words.push_back(wordBitmap);
            }
        }
        if (inputWText) {
            free(inputWText);
            inputWText = NULL;
        }
        if (wordToBitmap) {
            wordToBitmap->terminate();
            delete wordToBitmap;
            wordToBitmap = NULL;
        }
        
        //OVERLAY
        int outputOverlayWidth = logo->iconWidth;
        int outputOverlayHeight = logo->iconHeight + logo->textHeight;
        VideoFrame* outputOverlay = new VideoFrame();
        outputOverlay->frameSize = outputOverlayWidth * outputOverlayHeight * 4;
        outputOverlay->data = (uint8_t*)malloc(outputOverlay->frameSize);
        outputOverlay->width = outputOverlayWidth;
        outputOverlay->height = outputOverlayHeight;
        outputOverlay->videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
        memset(outputOverlay->data, 0, outputOverlay->frameSize);
        
        //BLEND
        Overlay::blend_image_rgba(outputOverlay, icon, 0, 0);
        if (icon) {
            icon->Free();
            delete icon;
            icon = NULL;
        }
        int x = (logo->iconWidth - logo->textWidth)/2;
        int y = logo->iconHeight + logo->iconTextSpace;
        int lastOffset = 0;
        for(vector<VideoFrame*>::iterator it = words.begin(); it != words.end(); ++it)
        {
            VideoFrame* word = *it;
            if(word!=NULL)
            {
                x += lastOffset;
                Overlay::blend_image_rgba(outputOverlay, word, x, y);
                lastOffset = word->width + logo->wordSpace;
                
                word->Free();
                delete word;
                word = NULL;
            }
        }
        words.clear();
        
        return outputOverlay;
    }else if(material.type == MT_BULLET_SCREEN) {
        MTBulletScreen* bullet = (MTBulletScreen*)material.content;
        
        int outputOverlayWidth = 0;
        int outputOverlayHeight = bullet->iconHeight;
        
        //HEAD
        VideoFrame* head = PNGImageFileToRGBAVideoFrame(bullet->headIcon);
        if (head==NULL) return NULL;
        if (head->width!=bullet->iconWidth || head->height!=bullet->iconHeight) {
            VideoFrame* newHead = new VideoFrame;
            newHead->width = bullet->iconWidth;
            newHead->height = bullet->iconHeight;
            newHead->frameSize = newHead->width * newHead->height * 4;
            newHead->data = (uint8_t*)malloc(newHead->frameSize);
            newHead->videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
            bool ret = LibyuvColorSpaceConvertUtils::RGBAtoRGBA_Crop_Scale(head, newHead);
            if (head) {
                head->Free();
                delete head;
                head = NULL;
            }
            if (!ret) {
                if (newHead) {
                    newHead->Free();
                    delete newHead;
                    newHead = NULL;
                }
                return NULL;
            }else {
                head = newHead;
            }
        }
        outputOverlayWidth += bullet->iconWidth + bullet->headbodySpace;
        
        //BODY
        MTWordToBitmap * wordToBitmap = new MTWordToBitmap(bullet->fontLibPath, bullet->fontSize, bullet->fontColor);
        bool ret = wordToBitmap->initialize();
        if (!ret) {
            delete wordToBitmap;
            wordToBitmap = NULL;
            
            if (head) {
                head->Free();
                delete head;
                head = NULL;
            }
            return NULL;
        }
        size_t size = strlen(bullet->bodyText) + 1;
        wchar_t* inputWText = new wchar_t[size];
        mbstowcs(inputWText, bullet->bodyText, size);
        vector<VideoFrame*> words;
        int i = -1;
        while (inputWText[++i]!=L'\0') {
            VideoFrame* wordBitmap = wordToBitmap->word2Bitmap(inputWText[i], 1.0f);
            if (wordBitmap) {
                outputOverlayWidth += wordBitmap->width;
                words.push_back(wordBitmap);
            }
        }
        if (inputWText) {
            free(inputWText);
            inputWText = NULL;
        }
        if (wordToBitmap) {
            wordToBitmap->terminate();
            delete wordToBitmap;
            wordToBitmap = NULL;
        }
        
        //TAILER
        VideoFrame* tail = NULL;
        if (bullet->tailIcon) {
            tail = PNGImageFileToRGBAVideoFrame(bullet->tailIcon);
            if (tail==NULL)
            {
                for(vector<VideoFrame*>::iterator it = words.begin(); it != words.end(); ++it)
                {
                    VideoFrame* word = *it;
                    if(word!=NULL)
                    {
                        word->Free();
                        delete word;
                        word = NULL;
                    }
                }
                words.clear();
                
                if (head) {
                    head->Free();
                    delete head;
                    head = NULL;
                }
                
                return NULL;
            }
            if (tail->width!=bullet->iconWidth || tail->height!=bullet->iconHeight) {
                VideoFrame* newTail = new VideoFrame;
                newTail->width = bullet->iconWidth;
                newTail->height = bullet->iconHeight;
                newTail->frameSize = newTail->width * newTail->height * 4;
                newTail->data = (uint8_t*)malloc(newTail->frameSize);
                newTail->videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
                bool ret = LibyuvColorSpaceConvertUtils::RGBAtoRGBA_Crop_Scale(tail, newTail);
                if (tail) {
                    tail->Free();
                    delete tail;
                    tail = NULL;
                }
                if (!ret) {
                    if (newTail) {
                        newTail->Free();
                        delete newTail;
                        newTail = NULL;
                    }
                    
                    for(vector<VideoFrame*>::iterator it = words.begin(); it != words.end(); ++it)
                    {
                        VideoFrame* word = *it;
                        if(word!=NULL)
                        {
                            word->Free();
                            delete word;
                            word = NULL;
                        }
                    }
                    words.clear();
                    
                    if (head) {
                        head->Free();
                        delete head;
                        head = NULL;
                    }
                                        
                    return NULL;
                }else {
                    tail = newTail;
                }
            }
        }
        
        if (tail) {
            outputOverlayWidth += bullet->bodytailSpace;
            outputOverlayWidth += bullet->iconWidth;
        }
        
        //OVERLAY
        VideoFrame* outputOverlay = new VideoFrame();
        outputOverlay->frameSize = outputOverlayWidth * outputOverlayHeight * 4;
        outputOverlay->data = (uint8_t*)malloc(outputOverlay->frameSize);
        outputOverlay->width = outputOverlayWidth;
        outputOverlay->height = outputOverlayHeight;
        outputOverlay->videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
        memset(outputOverlay->data, 0, outputOverlay->frameSize);
        
        //BLEND
        int x = 0;
        int y = 0;
        Overlay::blend_image_rgba(outputOverlay, head, x, y);
        if (head) {
            head->Free();
            delete head;
            head = NULL;
        }
        
        x += bullet->iconWidth + bullet->headbodySpace;
        int lastOffset = 0;
        for(vector<VideoFrame*>::iterator it = words.begin(); it != words.end(); ++it)
        {
            VideoFrame* word = *it;
            if(word!=NULL)
            {
                x += lastOffset;
                Overlay::blend_image_rgba(outputOverlay, word, x, y);
                lastOffset = word->width + bullet->wordSpace;
                
                word->Free();
                delete word;
                word = NULL;
            }
        }
        words.clear();
        
        if (tail) {
            x += bullet->bodytailSpace;
            Overlay::blend_image_rgba(outputOverlay, tail, x, y);
            
            tail->Free();
            delete tail;
            tail = NULL;
        }
        
        return outputOverlay;
    }
    
    return NULL;
}

