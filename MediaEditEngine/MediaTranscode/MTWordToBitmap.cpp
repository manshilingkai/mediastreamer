//
//  MTWordToBitmap.cpp
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/26.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "MTWordToBitmap.h"
#include "MediaLog.h"
#include <wchar.h>

MTWordToBitmap::MTWordToBitmap(const char* fontLibPath, int fontSize, FontColor fontColor)
{
    mFontLibPath = strdup(fontLibPath);
    mFontSize = fontSize;
    mFontColor = fontColor;
}

MTWordToBitmap::~MTWordToBitmap()
{
    free(mFontLibPath);
}

bool MTWordToBitmap::initialize()
{
    if (FT_Init_FreeType(&library))
    {
        LOGE("ERROR::FREETYPE: Could not init FreeType Library");
        return false;
    }
    
    if(FT_New_Face(library, mFontLibPath, 0, &face))
    {
        FT_Done_FreeType(library);
        
        LOGE("ERROR::FREETYPE: Failed to load font");
        return false;
    }
    
    FT_Set_Pixel_Sizes(face, 0, mFontSize);
    
    return true;
}

void MTWordToBitmap::terminate()
{
    FT_Done_Face(face);
    FT_Done_FreeType(library);
}

void MTWordToBitmap::setFontSize(int fontSize)
{
    mFontSize = fontSize;
    FT_Set_Pixel_Sizes(face, 0, mFontSize);
}

void MTWordToBitmap::setFontColor(FontColor fontColor)
{
    mFontColor = fontColor;
}

VideoFrame* MTWordToBitmap::word2Bitmap(wchar_t inputWord, float alpha)
{
    if (FT_Load_Char(face, inputWord, FT_LOAD_RENDER)) {
        LOGE("ERROR::FREETYTPE: Failed to load Glyph");
        return NULL;
    }
    
    if (face->glyph->bitmap.width==0 || face->glyph->bitmap.rows==0) {
        return NULL;
    }
    
    VideoFrame *wordFrame = new VideoFrame;
    wordFrame->width =  face->glyph->bitmap.width;
    wordFrame->height = face->glyph->bitmap.rows;;
    wordFrame->frameSize = wordFrame->width * wordFrame->height * 4;
    wordFrame->data = (uint8_t*)malloc(wordFrame->frameSize);
    memset(wordFrame->data, 0, wordFrame->frameSize);
    for (int i = 0; i < wordFrame->height; i++) {
        for (int j = 0; j < wordFrame->width; j++) {
            wordFrame->data[4*(i*wordFrame->width+j)+0] = (uint8_t)(mFontColor.r*(float)255);
            wordFrame->data[4*(i*wordFrame->width+j)+1] = (uint8_t)(mFontColor.g*(float)255);
            wordFrame->data[4*(i*wordFrame->width+j)+2] = (uint8_t)(mFontColor.b*(float)255);
            wordFrame->data[4*(i*wordFrame->width+j)+3] = uint8_t((float)(face->glyph->bitmap.buffer[i*wordFrame->width+j])*mFontColor.a*alpha);
        }
    }
    wordFrame->videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
    
    return wordFrame;
}
