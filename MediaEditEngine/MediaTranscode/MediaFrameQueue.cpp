//
//  MediaFrameQueue.cpp
//  Worker
//
//  Created by Think on 2018/9/26.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "MediaFrameQueue.h"

MediaFrameQueue::MediaFrameQueue()
{
    pthread_mutex_init(&mLock, NULL);
    
    mVideoFramesCount = 0;
    mAudioFramesCount = 0;
    
    mVideoFramesSize = 0;
    mAudioFramesSize = 0;
}

MediaFrameQueue::~MediaFrameQueue()
{
    pthread_mutex_destroy(&mLock);
}

void MediaFrameQueue::push(MediaFrame* mediaFrame)
{
    if (mediaFrame==NULL || mediaFrame->type == MEDIA_FRAME_TYPE_UNKNOWN) {
        return;
    }
    
    pthread_mutex_lock(&mLock);
    
    mMediaFrameQueue.push(mediaFrame);
    
    if (mediaFrame->type==MEDIA_FRAME_TYPE_VIDEO) {
        
        if (mVideoFramesCount<0) {
            mVideoFramesCount = 0;
        }
        
        if (mVideoFramesSize<0) {
            mVideoFramesSize = 0;
        }
        
        mVideoFramesCount++;
        mVideoFramesSize += mediaFrame->size;
    }
    
    if (mediaFrame->type==MEDIA_FRAME_TYPE_AUDIO) {
        
        if (mAudioFramesCount<0) {
            mAudioFramesCount = 0;
        }
        
        if (mAudioFramesSize<0) {
            mAudioFramesSize = 0;
        }
        
        mAudioFramesCount++;
        mAudioFramesSize += mediaFrame->size;
    }
    
    pthread_mutex_unlock(&mLock);
}

MediaFrame* MediaFrameQueue::pop()
{
    MediaFrame* mediaFrame = NULL;
    
    pthread_mutex_lock(&mLock);
    
    if (!mMediaFrameQueue.empty()) {
        mediaFrame = mMediaFrameQueue.front();
        mMediaFrameQueue.pop();
    }
    
    if (mediaFrame) {
        if (mediaFrame->type==MEDIA_FRAME_TYPE_VIDEO) {

            if (mVideoFramesCount<0) {
                mVideoFramesCount = 0;
            }
            
            if (mVideoFramesSize<0) {
                mVideoFramesSize = 0;
            }
            
            mVideoFramesCount--;
            mVideoFramesSize -= mediaFrame->size;
            
            if (mVideoFramesCount<0) {
                mVideoFramesCount = 0;
            }
            
            if (mVideoFramesSize<0) {
                mVideoFramesSize = 0;
            }
        }
        
        if (mediaFrame->type==MEDIA_FRAME_TYPE_AUDIO) {
            if (mAudioFramesCount<0) {
                mAudioFramesCount = 0;
            }
            
            if (mAudioFramesSize<0) {
                mAudioFramesSize = 0;
            }
            
            mAudioFramesCount--;
            mAudioFramesSize -= mediaFrame->size;
            
            if (mAudioFramesCount<0) {
                mAudioFramesCount = 0;
            }
            
            if (mAudioFramesSize<0) {
                mAudioFramesSize = 0;
            }
        }
    }
    
    pthread_mutex_unlock(&mLock);
    
    return mediaFrame;
}

MediaFrame* MediaFrameQueue::front()
{
    MediaFrame* mediaFrame = NULL;
    pthread_mutex_lock(&mLock);
    if (!mMediaFrameQueue.empty()) {
        mediaFrame = mMediaFrameQueue.front();
    }
    pthread_mutex_unlock(&mLock);
    return mediaFrame;
}

void MediaFrameQueue::flush()
{
    pthread_mutex_lock(&mLock);
    
    while (!mMediaFrameQueue.empty()) {
        MediaFrame* mediaFrame = mMediaFrameQueue.front();
        if (mediaFrame) {
            mediaFrame->Free();
            delete mediaFrame;
        }
        mMediaFrameQueue.pop();
    }
    
    mVideoFramesCount = 0;
    mAudioFramesCount = 0;
    
    mVideoFramesSize = 0;
    mAudioFramesSize = 0;
    
    pthread_mutex_unlock(&mLock);
}

int MediaFrameQueue::count(MEDIA_FRAME_TYPE type)
{
    int ret = 0;
    
    pthread_mutex_lock(&mLock);
    
    if (type==MEDIA_FRAME_TYPE_VIDEO) {
        ret = mVideoFramesCount;
    }
    
    if (type==MEDIA_FRAME_TYPE_AUDIO) {
        ret = mAudioFramesCount;
    }
    
    pthread_mutex_unlock(&mLock);
    
    return ret;
}


int MediaFrameQueue::count()
{
    int ret = 0;
    
    pthread_mutex_lock(&mLock);
    ret = mVideoFramesCount + mAudioFramesCount;
    pthread_mutex_unlock(&mLock);
    
    return ret;
}

long MediaFrameQueue::size(MEDIA_FRAME_TYPE type)
{
    long ret = 0;
    
    pthread_mutex_lock(&mLock);
    
    if (type==MEDIA_FRAME_TYPE_VIDEO) {
        ret = mVideoFramesSize;
    }
    
    if (type==MEDIA_FRAME_TYPE_AUDIO) {
        ret = mAudioFramesSize;
    }
    
    pthread_mutex_unlock(&mLock);
    
    return ret;
}

long MediaFrameQueue::size()
{
    long ret = 0;
    
    pthread_mutex_lock(&mLock);
    ret = mVideoFramesSize + mAudioFramesSize;
    pthread_mutex_unlock(&mLock);
    
    return ret;
}
