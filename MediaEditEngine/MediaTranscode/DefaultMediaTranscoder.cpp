//
//  DefaultMediaTranscoder.cpp
//  Worker
//
//  Created by Think on 2018/9/21.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "DefaultMediaTranscoder.h"
#include "AutoLock.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#else
#include "NormalMediaListener.h"
#endif

#include "FFLog.h"
#include "StringUtils.h"
#include "MediaFile.h"

#include "CPUOverlayCreater.h"
#include "ImageProcesserUtils.h"

#ifdef ANDROID
#else
#include "json.hpp"
using json = nlohmann::json;
#endif

// https://ffmpeg.org/ffmpeg-filters.html

double MT_PI = 3.141592653589793f;

struct DefaultMediaTranscoderEvent : public TimedEventQueue::Event {
    DefaultMediaTranscoderEvent(
                          DefaultMediaTranscoder *transcoder,
                          void (DefaultMediaTranscoder::*method)())
    : mTranscoder(transcoder),
    mMethod(method) {
    }
    
protected:
    virtual ~DefaultMediaTranscoderEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mTranscoder->*mMethod)();
    }
    
private:
    DefaultMediaTranscoder *mTranscoder;
    void (DefaultMediaTranscoder::*mMethod)();
    
    DefaultMediaTranscoderEvent(const DefaultMediaTranscoderEvent &);
    DefaultMediaTranscoderEvent &operator=(const DefaultMediaTranscoderEvent &);
};

#ifdef ANDROID
DefaultMediaTranscoder::DefaultMediaTranscoder(JavaVM *jvm, MediaLog* mediaLog, MediaMaterial input, MediaEffectGroup *pMediaEffectGroup, MediaProduct product)
{
    mJvm = jvm;

    init_ffmpeg_env();
    
    mMediaLog = mediaLog;
    
    LOGI("DefaultMediaTranscoder::DefaultMediaTranscoder Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::DefaultMediaTranscoder Enter");
    }
    
    mInput = input;
    if (input.url) {
        mInput.url = strdup(input.url);
    }
    if (input.http_proxy) {
        mInput.http_proxy = strdup(input.http_proxy);
    }
    
    if (pMediaEffectGroup) {
        mMediaEffectGroup = new MediaEffectGroup;
        mMediaEffectGroup->mediaEffectCount = pMediaEffectGroup->mediaEffectCount;
        for (int i = 0; i<mMediaEffectGroup->mediaEffectCount; i++) {
            mMediaEffectGroup->mediaEffects[i] = new MediaEffect;
            mMediaEffectGroup->mediaEffects[i]->type = pMediaEffectGroup->mediaEffects[i]->type;
            if (pMediaEffectGroup->mediaEffects[i]->resourceUrl) {
                mMediaEffectGroup->mediaEffects[i]->resourceUrl = strdup(pMediaEffectGroup->mediaEffects[i]->resourceUrl);
            }
            mMediaEffectGroup->mediaEffects[i]->effect_in_ms = pMediaEffectGroup->mediaEffects[i]->effect_in_ms;
            mMediaEffectGroup->mediaEffects[i]->effect_out_ms = pMediaEffectGroup->mediaEffects[i]->effect_out_ms;
            
            mMediaEffectGroup->mediaEffects[i]->volume = pMediaEffectGroup->mediaEffects[i]->volume;
            mMediaEffectGroup->mediaEffects[i]->speed = pMediaEffectGroup->mediaEffects[i]->speed;
            
            mMediaEffectGroup->mediaEffects[i]->x = pMediaEffectGroup->mediaEffects[i]->x;
            mMediaEffectGroup->mediaEffects[i]->y = pMediaEffectGroup->mediaEffects[i]->y;
            mMediaEffectGroup->mediaEffects[i]->scale = pMediaEffectGroup->mediaEffects[i]->scale;
            mMediaEffectGroup->mediaEffects[i]->rotation = pMediaEffectGroup->mediaEffects[i]->rotation;
            
            mMediaEffectGroup->mediaEffects[i]->image_filter_type = pMediaEffectGroup->mediaEffects[i]->image_filter_type;
            
            mMediaEffectGroup->mediaEffects[i]->video_rotation_degree = pMediaEffectGroup->mediaEffects[i]->video_rotation_degree;
            mMediaEffectGroup->mediaEffects[i]->video_flip = pMediaEffectGroup->mediaEffects[i]->video_flip;
            
            if (mMediaEffectGroup->mediaEffects[i]->type == MEDIA_EFFECT_TYPE_PRIVATE) {
                if (pMediaEffectGroup->mediaEffects[i]->private_logo_icon) {
                    mMediaEffectGroup->mediaEffects[i]->private_logo_icon = strdup(pMediaEffectGroup->mediaEffects[i]->private_logo_icon);
                }
                if (pMediaEffectGroup->mediaEffects[i]->private_logo_id) {
                    mMediaEffectGroup->mediaEffects[i]->private_logo_id = strdup(pMediaEffectGroup->mediaEffects[i]->private_logo_id);
                }
                if (pMediaEffectGroup->mediaEffects[i]->private_logo_fontlib_path) {
                    mMediaEffectGroup->mediaEffects[i]->private_logo_fontlib_path = strdup(pMediaEffectGroup->mediaEffects[i]->private_logo_fontlib_path);
                }
                if (pMediaEffectGroup->mediaEffects[i]->private_mask_horizontal) {
                    mMediaEffectGroup->mediaEffects[i]->private_mask_horizontal = strdup(pMediaEffectGroup->mediaEffects[i]->private_mask_horizontal);
                }
                if (pMediaEffectGroup->mediaEffects[i]->private_mask_vertical) {
                    mMediaEffectGroup->mediaEffects[i]->private_mask_vertical = strdup(pMediaEffectGroup->mediaEffects[i]->private_mask_vertical);
                }
                if (pMediaEffectGroup->mediaEffects[i]->private_bullet_fontlib_path) {
                    mMediaEffectGroup->mediaEffects[i]->private_bullet_fontlib_path = strdup(pMediaEffectGroup->mediaEffects[i]->private_bullet_fontlib_path);
                }
                
                for(vector<Bullet*>::iterator it = pMediaEffectGroup->mediaEffects[i]->bullets.begin(); it != pMediaEffectGroup->mediaEffects[i]->bullets.end(); ++it)
                {
                    Bullet* bullet = *it;
                    
                    Bullet* newBullet = new Bullet();
                    newBullet->bulletChannel = bullet->bulletChannel;
                    newBullet->beginTimeMs = bullet->beginTimeMs;
                    if (bullet->headIcon) {
                        newBullet->headIcon = strdup(bullet->headIcon);
                    }
                    if (bullet->text) {
                        newBullet->text = strdup(bullet->text);
                    }
                    if (bullet->tailIcon) {
                        newBullet->tailIcon = strdup(bullet->tailIcon);
                    }
                    mMediaEffectGroup->mediaEffects[i]->bullets.push_back(newBullet);
                }
            }
        }
    }else{
        mMediaEffectGroup = NULL;
    }
    
    mProduct = product;
    isLocalFile = false;
    if (product.url) {
        mProduct.url = strdup(product.url);
        char left[1];
        StringUtils::left(left, mProduct.url, 1);
        if (left[0]=='/') {
            isLocalFile = true;
        }
    }
    
    mFlags = 0;

    pthread_mutex_init(&mLock, NULL);

    mAsyncPrepareEvent = NULL;
    mEncodeEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    
    mAsyncPrepareEvent = new DefaultMediaTranscoderEvent(this, &DefaultMediaTranscoder::onPrepareAsyncEvent);
    mEncodeEvent = new DefaultMediaTranscoderEvent(this, &DefaultMediaTranscoder::onEncodeEvent);
    mStopEvent = new DefaultMediaTranscoderEvent(this, &DefaultMediaTranscoder::onStopEvent);
    mNotifyEvent = new DefaultMediaTranscoderEvent(this, &DefaultMediaTranscoder::onNotifyEvent);
    mEncodeEventPending = false;

    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();

    pthread_cond_init(&mStopCondition, NULL);

    mMediaListener = NULL;
    
    mMediaFrameOutputer = NULL;
    pthread_mutex_init(&mMediaFrameOutputerLock, NULL);

    gotError = false;

    ofmt_ctx = NULL;
    video_encoder_ctx = NULL;
    audio_encoder_ctx = NULL;
    
    video_buffersrc_ctx = NULL;
    video_buffersink_ctx = NULL;
    video_filter_graph = NULL;
    
    audio_buffersrc_ctx = NULL;
    audio_buffersink_ctx = NULL;
    audio_filter_graph = NULL;
    
    mOutputAudioStreamIndex = -1;
    mOutputVideoStreamIndex = -1;
    
    filteredAudioDataFifo = NULL;
    
    mGot_AudioWriteTimeUs_Baseline = false;
    mAudioWriteTimeUs_Baseline = 0;
    mAudioWriteTimeUs = 0;
    
    mGot_VideoWriteTimeUs_Baseline = false;
    mVideoWriteTimeUs_Baseline = 0;
    mVideoWriteTimeUs = 0;
    
    mWriteTimeUs = 0;
    mLastSendWriteTimeS = 0;
    
    if (mProduct.type==MEDIA_PRODUCT_TYPE_RTMP) {
        output_fmt_name = av_strdup("flv");
    }else if (mProduct.type==MEDIA_PRODUCT_TYPE_MP4) {
        output_fmt_name = av_strdup("mp4");
    }else if (mProduct.type==MEDIA_PRODUCT_TYPE_M4A) {
        output_fmt_name = av_strdup("mp4");
    }else{
        output_fmt_name = NULL;
    }
    
    isImageOverlaying = false;
    
    // Video Overlay
    ignore_video_overlay = false;
    isVideoOverlaying = false;
    
    mVideoOverlayMediaFrameOutputer = NULL;
    pthread_cond_init(&mVideoOverlayCondition, NULL);
    pthread_mutex_init(&mVideoOverlayLock, NULL);
    mVideoOverlayTimeMs = 0;
    
    mVideoMainYUVFrame = NULL;
    mVideoMainRGBAFrame = NULL;
    mVideoOverlayYUVFrame = NULL;
    mVideoOverlayRGBAAlphaFrame = NULL;
    mVideoOverlayRGBAFrame = NULL;
    mColorSpaceConvert = NULL;
    
    //Private
    mPrivateMainYUVFrame = NULL;
    mPrivateMainRGBAFrame = NULL;
    mPrivateColorSpaceConvert = NULL;
    mPrivateLogoRGBAFrame = NULL;
    private_logo_x = 0;
    private_logo_y = 0;
    mPrivateMaskRGBAFrame = NULL;
    bullet_channel_top_padding = 0;
    bullet_channel_space = 0;
    bullet_space = 0;
    srand((unsigned)time(NULL));
    
    mMediaDataListener = NULL;
    mMediaDataOwner = NULL;
    
    LOGI("DefaultMediaTranscoder::DefaultMediaTranscoder Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::DefaultMediaTranscoder Leave");
    }
}
#else
DefaultMediaTranscoder::DefaultMediaTranscoder(MediaLog* mediaLog, MediaMaterial input, MediaEffectGroup *pMediaEffectGroup, MediaProduct product)
{
    init_ffmpeg_env();
    
    mMediaLog = mediaLog;
    
    LOGI("DefaultMediaTranscoder::DefaultMediaTranscoder Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::DefaultMediaTranscoder Enter");
    }
    
    mInput = input;
    if (input.url) {
        mInput.url = strdup(input.url);
    }
    if (input.http_proxy) {
        mInput.http_proxy = strdup(input.http_proxy);
    }
    
    if (pMediaEffectGroup) {
        mMediaEffectGroup = new MediaEffectGroup;
        mMediaEffectGroup->mediaEffectCount = pMediaEffectGroup->mediaEffectCount;
        for (int i = 0; i<mMediaEffectGroup->mediaEffectCount; i++) {
            mMediaEffectGroup->mediaEffects[i] = new MediaEffect;
            mMediaEffectGroup->mediaEffects[i]->type = pMediaEffectGroup->mediaEffects[i]->type;
            if (pMediaEffectGroup->mediaEffects[i]->resourceUrl) {
                mMediaEffectGroup->mediaEffects[i]->resourceUrl = strdup(pMediaEffectGroup->mediaEffects[i]->resourceUrl);
            }
            mMediaEffectGroup->mediaEffects[i]->effect_in_ms = pMediaEffectGroup->mediaEffects[i]->effect_in_ms;
            mMediaEffectGroup->mediaEffects[i]->effect_out_ms = pMediaEffectGroup->mediaEffects[i]->effect_out_ms;
            
            mMediaEffectGroup->mediaEffects[i]->volume = pMediaEffectGroup->mediaEffects[i]->volume;
            mMediaEffectGroup->mediaEffects[i]->speed = pMediaEffectGroup->mediaEffects[i]->speed;
            
            mMediaEffectGroup->mediaEffects[i]->x = pMediaEffectGroup->mediaEffects[i]->x;
            mMediaEffectGroup->mediaEffects[i]->y = pMediaEffectGroup->mediaEffects[i]->y;
            mMediaEffectGroup->mediaEffects[i]->scale = pMediaEffectGroup->mediaEffects[i]->scale;
            mMediaEffectGroup->mediaEffects[i]->rotation = pMediaEffectGroup->mediaEffects[i]->rotation;
            
            mMediaEffectGroup->mediaEffects[i]->image_filter_type = pMediaEffectGroup->mediaEffects[i]->image_filter_type;
            
            mMediaEffectGroup->mediaEffects[i]->video_rotation_degree = pMediaEffectGroup->mediaEffects[i]->video_rotation_degree;
            mMediaEffectGroup->mediaEffects[i]->video_flip = pMediaEffectGroup->mediaEffects[i]->video_flip;
            
            if (mMediaEffectGroup->mediaEffects[i]->type == MEDIA_EFFECT_TYPE_PRIVATE) {
                if (pMediaEffectGroup->mediaEffects[i]->private_logo_icon) {
                    mMediaEffectGroup->mediaEffects[i]->private_logo_icon = strdup(pMediaEffectGroup->mediaEffects[i]->private_logo_icon);
                }
                if (pMediaEffectGroup->mediaEffects[i]->private_logo_id) {
                    mMediaEffectGroup->mediaEffects[i]->private_logo_id = strdup(pMediaEffectGroup->mediaEffects[i]->private_logo_id);
                }
                if (pMediaEffectGroup->mediaEffects[i]->private_logo_fontlib_path) {
                    mMediaEffectGroup->mediaEffects[i]->private_logo_fontlib_path = strdup(pMediaEffectGroup->mediaEffects[i]->private_logo_fontlib_path);
                }
                if (pMediaEffectGroup->mediaEffects[i]->private_mask_horizontal) {
                    mMediaEffectGroup->mediaEffects[i]->private_mask_horizontal = strdup(pMediaEffectGroup->mediaEffects[i]->private_mask_horizontal);
                }
                if (pMediaEffectGroup->mediaEffects[i]->private_mask_vertical) {
                    mMediaEffectGroup->mediaEffects[i]->private_mask_vertical = strdup(pMediaEffectGroup->mediaEffects[i]->private_mask_vertical);
                }
                if (pMediaEffectGroup->mediaEffects[i]->private_bullet_fontlib_path) {
                    mMediaEffectGroup->mediaEffects[i]->private_bullet_fontlib_path = strdup(pMediaEffectGroup->mediaEffects[i]->private_bullet_fontlib_path);
                }
                
                for(vector<Bullet*>::iterator it = pMediaEffectGroup->mediaEffects[i]->bullets.begin(); it != pMediaEffectGroup->mediaEffects[i]->bullets.end(); ++it)
                {
                    Bullet* bullet = *it;
                    
                    Bullet* newBullet = new Bullet();
                    newBullet->bulletChannel = bullet->bulletChannel;
                    newBullet->beginTimeMs = bullet->beginTimeMs;
                    if (bullet->headIcon) {
                        newBullet->headIcon = strdup(bullet->headIcon);
                    }
                    if (bullet->text) {
                        newBullet->text = strdup(bullet->text);
                    }
                    if (bullet->tailIcon) {
                        newBullet->tailIcon = strdup(bullet->tailIcon);
                    }
                    mMediaEffectGroup->mediaEffects[i]->bullets.push_back(newBullet);
                }
            }
        }
    }else{
        mMediaEffectGroup = NULL;
    }
    
    mProduct = product;
    isLocalFile = false;
    if (product.url) {
        mProduct.url = strdup(product.url);
        char left[1];
        StringUtils::left(left, mProduct.url, 1);
        if (left[0]=='/') {
            isLocalFile = true;
        }
    }
    
    mFlags = 0;

    pthread_mutex_init(&mLock, NULL);

    mAsyncPrepareEvent = NULL;
    mEncodeEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    
    mAsyncPrepareEvent = new DefaultMediaTranscoderEvent(this, &DefaultMediaTranscoder::onPrepareAsyncEvent);
    mEncodeEvent = new DefaultMediaTranscoderEvent(this, &DefaultMediaTranscoder::onEncodeEvent);
    mStopEvent = new DefaultMediaTranscoderEvent(this, &DefaultMediaTranscoder::onStopEvent);
    mNotifyEvent = new DefaultMediaTranscoderEvent(this, &DefaultMediaTranscoder::onNotifyEvent);
    mEncodeEventPending = false;

    mQueue.start();

    pthread_cond_init(&mStopCondition, NULL);

    mMediaListener = NULL;
    
    mMediaFrameOutputer = NULL;
    pthread_mutex_init(&mMediaFrameOutputerLock, NULL);

    gotError = false;

    ofmt_ctx = NULL;
    video_encoder_ctx = NULL;
    audio_encoder_ctx = NULL;
    
    video_buffersrc_ctx = NULL;
    video_buffersink_ctx = NULL;
    video_filter_graph = NULL;
    
    audio_buffersrc_ctx = NULL;
    audio_buffersink_ctx = NULL;
    audio_filter_graph = NULL;
    
    mOutputAudioStreamIndex = -1;
    mOutputVideoStreamIndex = -1;
    
    filteredAudioDataFifo = NULL;
    
    mGot_AudioWriteTimeUs_Baseline = false;
    mAudioWriteTimeUs_Baseline = 0;
    mAudioWriteTimeUs = 0;
    
    mGot_VideoWriteTimeUs_Baseline = false;
    mVideoWriteTimeUs_Baseline = 0;
    mVideoWriteTimeUs = 0;
    
    mWriteTimeUs = 0;
    mLastSendWriteTimeS = 0;
    
    if (mProduct.type==MEDIA_PRODUCT_TYPE_RTMP) {
        output_fmt_name = av_strdup("flv");
    }else if (mProduct.type==MEDIA_PRODUCT_TYPE_MP4) {
        output_fmt_name = av_strdup("mp4");
    }else if (mProduct.type==MEDIA_PRODUCT_TYPE_M4A) {
        output_fmt_name = av_strdup("mp4");
    }else{
        output_fmt_name = NULL;
    }
    
    isImageOverlaying = false;
    
    // Video Overlay
    ignore_video_overlay = false;
    isVideoOverlaying = false;
    
    mVideoOverlayMediaFrameOutputer = NULL;
    pthread_cond_init(&mVideoOverlayCondition, NULL);
    pthread_mutex_init(&mVideoOverlayLock, NULL);
    mVideoOverlayTimeMs = 0;
    
    mVideoMainYUVFrame = NULL;
    mVideoMainRGBAFrame = NULL;
    mVideoOverlayYUVFrame = NULL;
    mVideoOverlayRGBAAlphaFrame = NULL;
    mVideoOverlayRGBAFrame = NULL;
    mColorSpaceConvert = NULL;
    
    //Private
    mPrivateMainYUVFrame = NULL;
    mPrivateMainRGBAFrame = NULL;
    mPrivateColorSpaceConvert = NULL;
    mPrivateLogoRGBAFrame = NULL;
    private_logo_x = 0;
    private_logo_y = 0;
    mPrivateMaskRGBAFrame = NULL;
    bullet_channel_top_padding = 0;
    bullet_channel_space = 0;
    bullet_space = 0;
    srand((unsigned)time(NULL));
    
    mMediaDataListener = NULL;
    mMediaDataOwner = NULL;
    
    LOGI("DefaultMediaTranscoder::DefaultMediaTranscoder Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::DefaultMediaTranscoder Leave");
    }
}

DefaultMediaTranscoder::DefaultMediaTranscoder(MediaLog* mediaLog, char* configureInfo)
{
    mMediaLog = mediaLog;
    
    LOGI("DefaultMediaTranscoder::DefaultMediaTranscoder Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::DefaultMediaTranscoder Enter");
    }
    
    std::string configure_info_string = configureInfo;
    json configure_info_json = json::parse(configure_info_string);
    if (configure_info_json.find("MediaMaterial") != configure_info_json.end() && configure_info_json["MediaMaterial"].is_object()) {
        json media_material_content_json = configure_info_json["MediaMaterial"];
        if (media_material_content_json.find("Url") != media_material_content_json.end() && media_material_content_json["Url"].is_string()) {
            std::string url_str = media_material_content_json["Url"];
            mInput.url = strdup(url_str.c_str());
        }
        if (media_material_content_json.find("StartPosMs") != media_material_content_json.end() && media_material_content_json["StartPosMs"].is_number()) {
            mInput.startPosMs = media_material_content_json["StartPosMs"];
        }
        if (media_material_content_json.find("EndPosMs") != media_material_content_json.end() && media_material_content_json["EndPosMs"].is_number()) {
            mInput.endPosMs = media_material_content_json["EndPosMs"];
        }
    }
    if (configure_info_json.find("MediaEffect") != configure_info_json.end() && configure_info_json["MediaEffect"].is_object()) {
        json media_effect_content_json = configure_info_json["MediaEffect"];
        mMediaEffectGroup = new MediaEffectGroup;
        if (media_effect_content_json.find("Private") != media_effect_content_json.end() && media_effect_content_json["Private"].is_object()) {
            json private_content_json = media_effect_content_json["Private"];
            mMediaEffectGroup->mediaEffectCount++;
            mMediaEffectGroup->mediaEffects[mMediaEffectGroup->mediaEffectCount-1] = new MediaEffect;
            mMediaEffectGroup->mediaEffects[mMediaEffectGroup->mediaEffectCount-1]->type =  MEDIA_EFFECT_TYPE_PRIVATE;
            if (private_content_json.find("PrivateLogoIcon") != private_content_json.end() && private_content_json["PrivateLogoIcon"].is_string()) {
                std::string private_logo_icon_str = private_content_json["PrivateLogoIcon"];
                mMediaEffectGroup->mediaEffects[mMediaEffectGroup->mediaEffectCount-1]->private_logo_icon = strdup(private_logo_icon_str.c_str());
            }
            if (private_content_json.find("PrivateLogoId") != private_content_json.end() && private_content_json["PrivateLogoId"].is_string()) {
                std::string private_logo_id_str = private_content_json["PrivateLogoId"];
                mMediaEffectGroup->mediaEffects[mMediaEffectGroup->mediaEffectCount-1]->private_logo_id = strdup(private_logo_id_str.c_str());
            }
            if (private_content_json.find("PrivateLogoFontLibPath") != private_content_json.end() && private_content_json["PrivateLogoFontLibPath"].is_string()) {
                std::string private_logo_font_lib_path_str = private_content_json["PrivateLogoFontLibPath"];
                mMediaEffectGroup->mediaEffects[mMediaEffectGroup->mediaEffectCount-1]->private_logo_fontlib_path = strdup(private_logo_font_lib_path_str.c_str());
            }
            if (private_content_json.find("PrivateMaskHorizontal") != private_content_json.end() && private_content_json["PrivateMaskHorizontal"].is_string()) {
                std::string private_mask_horizontal_str = private_content_json["PrivateMaskHorizontal"];
                mMediaEffectGroup->mediaEffects[mMediaEffectGroup->mediaEffectCount-1]->private_mask_horizontal = strdup(private_mask_horizontal_str.c_str());
            }
            if (private_content_json.find("PrivateMaskVertical") != private_content_json.end() && private_content_json["PrivateMaskVertical"].is_string()) {
                std::string private_mask_vertical_str = private_content_json["PrivateMaskVertical"];
                mMediaEffectGroup->mediaEffects[mMediaEffectGroup->mediaEffectCount-1]->private_mask_vertical = strdup(private_mask_vertical_str.c_str());
            }
        }
    }
    if (configure_info_json.find("MediaProduct") != configure_info_json.end() && configure_info_json["MediaProduct"].is_object()) {
        json media_product_content_json = configure_info_json["MediaProduct"];
        if (media_product_content_json.find("Url") != media_product_content_json.end() && media_product_content_json["Url"].is_string()) {
            std::string url_str = media_product_content_json["Url"];
            mProduct.url = strdup(url_str.c_str());
        }
    }
    
    init_ffmpeg_env();
    
    isLocalFile = false;
    char left[1];
    StringUtils::left(left, mProduct.url, 1);
    if (left[0]=='/') {
        isLocalFile = true;
    }
    
    mFlags = 0;

    pthread_mutex_init(&mLock, NULL);

    mAsyncPrepareEvent = NULL;
    mEncodeEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    
    mAsyncPrepareEvent = new DefaultMediaTranscoderEvent(this, &DefaultMediaTranscoder::onPrepareAsyncEvent);
    mEncodeEvent = new DefaultMediaTranscoderEvent(this, &DefaultMediaTranscoder::onEncodeEvent);
    mStopEvent = new DefaultMediaTranscoderEvent(this, &DefaultMediaTranscoder::onStopEvent);
    mNotifyEvent = new DefaultMediaTranscoderEvent(this, &DefaultMediaTranscoder::onNotifyEvent);
    mEncodeEventPending = false;

    mQueue.start();

    pthread_cond_init(&mStopCondition, NULL);

    mMediaListener = NULL;
    
    mMediaFrameOutputer = NULL;
    pthread_mutex_init(&mMediaFrameOutputerLock, NULL);

    gotError = false;

    ofmt_ctx = NULL;
    video_encoder_ctx = NULL;
    audio_encoder_ctx = NULL;
    
    video_buffersrc_ctx = NULL;
    video_buffersink_ctx = NULL;
    video_filter_graph = NULL;
    
    audio_buffersrc_ctx = NULL;
    audio_buffersink_ctx = NULL;
    audio_filter_graph = NULL;
    
    mOutputAudioStreamIndex = -1;
    mOutputVideoStreamIndex = -1;
    
    filteredAudioDataFifo = NULL;
    
    mGot_AudioWriteTimeUs_Baseline = false;
    mAudioWriteTimeUs_Baseline = 0;
    mAudioWriteTimeUs = 0;
    
    mGot_VideoWriteTimeUs_Baseline = false;
    mVideoWriteTimeUs_Baseline = 0;
    mVideoWriteTimeUs = 0;
    
    mWriteTimeUs = 0;
    mLastSendWriteTimeS = 0;
    
    if (mProduct.type==MEDIA_PRODUCT_TYPE_RTMP) {
        output_fmt_name = av_strdup("flv");
    }else if (mProduct.type==MEDIA_PRODUCT_TYPE_MP4) {
        output_fmt_name = av_strdup("mp4");
    }else if (mProduct.type==MEDIA_PRODUCT_TYPE_M4A) {
        output_fmt_name = av_strdup("mp4");
    }else{
        output_fmt_name = NULL;
    }
    
    isImageOverlaying = false;
    
    // Video Overlay
    ignore_video_overlay = false;
    isVideoOverlaying = false;
    
    mVideoOverlayMediaFrameOutputer = NULL;
    pthread_cond_init(&mVideoOverlayCondition, NULL);
    pthread_mutex_init(&mVideoOverlayLock, NULL);
    mVideoOverlayTimeMs = 0;
    
    mVideoMainYUVFrame = NULL;
    mVideoMainRGBAFrame = NULL;
    mVideoOverlayYUVFrame = NULL;
    mVideoOverlayRGBAAlphaFrame = NULL;
    mVideoOverlayRGBAFrame = NULL;
    mColorSpaceConvert = NULL;
    
    //Private
    mPrivateMainYUVFrame = NULL;
    mPrivateMainRGBAFrame = NULL;
    mPrivateColorSpaceConvert = NULL;
    mPrivateLogoRGBAFrame = NULL;
    private_logo_x = 0;
    private_logo_y = 0;
    mPrivateMaskRGBAFrame = NULL;
    bullet_channel_top_padding = 0;
    bullet_channel_space = 0;
    bullet_space = 0;
    srand((unsigned)time(NULL));
    
    mMediaDataListener = NULL;
    mMediaDataOwner = NULL;
    
    LOGI("DefaultMediaTranscoder::DefaultMediaTranscoder Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::DefaultMediaTranscoder Leave");
    }
}
#endif

DefaultMediaTranscoder::~DefaultMediaTranscoder()
{
    LOGI("DefaultMediaTranscoder::~DefaultMediaTranscoder Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::~DefaultMediaTranscoder Enter");
    }
    
    mQueue.stop(true);

    LOGI("DefaultMediaTranscoder::~DefaultMediaTranscoder TimedEventQueue Stoped");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::~DefaultMediaTranscoder TimedEventQueue Stoped");
    }
    
    mNotificationQueue.flush();

    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
    
    if (mEncodeEvent!=NULL) {
        delete mEncodeEvent;
        mEncodeEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    pthread_cond_destroy(&mStopCondition);
    
    pthread_mutex_destroy(&mLock);
    
#ifdef ANDROID
    JniMediaListener *jniListener = (JniMediaListener*)mMediaListener;
    if (jniListener!=NULL) {
        delete jniListener;
        jniListener = NULL;
    }
#else
    NormalMediaListener* normalMediaListener = (NormalMediaListener*)mMediaListener;
    if (normalMediaListener!=NULL) {
        delete normalMediaListener;
        normalMediaListener = NULL;
    }
#endif
    
    mInput.Free();
    
    if (mMediaEffectGroup) {
        mMediaEffectGroup->Free();
        delete mMediaEffectGroup;
        mMediaEffectGroup = NULL;
    }
    
    mProduct.Free();
    
    pthread_mutex_destroy(&mMediaFrameOutputerLock);
    
    pthread_cond_destroy(&mVideoOverlayCondition);
    pthread_mutex_destroy(&mVideoOverlayLock);

    LOGI("DefaultMediaTranscoder::~DefaultMediaTranscoder Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::~DefaultMediaTranscoder Leave");
    }
    
    avformat_network_deinit();
    
    if (output_fmt_name) av_free(output_fmt_name);
}

#ifdef ANDROID
void DefaultMediaTranscoder::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    modifyFlags(INITIALIZED, SET);
}
#else
void DefaultMediaTranscoder::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);

    mMediaListener = new NormalMediaListener(listener,arg);
    
    modifyFlags(INITIALIZED, SET);
}

void DefaultMediaTranscoder::setMediaDataListener(void (*listener)(void*, void*, int, int, int), void* arg)
{
    AutoLock autoLock(&mLock);
    
    mMediaDataListener = listener;
    mMediaDataOwner = arg;
}
#endif

void DefaultMediaTranscoder::notifyListener_l(int event, int ext1, int ext2)
{
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void DefaultMediaTranscoder::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    postNotifyEvent_l();
}

void DefaultMediaTranscoder::postNotifyEvent_l()
{
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void DefaultMediaTranscoder::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
    
    switch (event) {
        case MEDIA_TRANSCODER_ERROR:
            modifyFlags(ERROR, ASSIGN);
            if (ext2!=AVERROR_EXIT) {
                notifyListener_l(event, ext1, ext2);
            }
            gotError = true;
            stop_l();
            break;
        case MEDIA_TRANSCODER_INFO:
            notifyListener_l(event, ext1, ext2);
            break;
            
        default:
            notifyListener_l(event, ext1, ext2);
            break;
    }
}

void DefaultMediaTranscoder::start()
{
    LOGI("DefaultMediaTranscoder::start Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::start Enter");
    }
    
    AutoLock autoLock(&mLock);
    if (mFlags & CONNECTING) {
        LOGW("already connecting");
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    prepareAsync_l();
    
    LOGI("DefaultMediaTranscoder::start Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::start Leave");
    }
}

void DefaultMediaTranscoder::prepareAsync_l()
{
    LOGI("DefaultMediaTranscoder::prepareAsync_l Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::prepareAsync_l Enter");
    }
    
    modifyFlags(END, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(CONNECTING, SET);
    
    gotError = false;
    
    mQueue.postEvent(mAsyncPrepareEvent);
    
    LOGI("DefaultMediaTranscoder::prepareAsync_l Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::prepareAsync_l Leave");
    }
}

void DefaultMediaTranscoder::onPrepareAsyncEvent()
{
    LOGI("DefaultMediaTranscoder::onPrepareAsyncEvent");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::onPrepareAsyncEvent");
    }
    
    AutoLock autoLock(&mLock);
    notifyListener_l(MEDIA_TRANSCODER_CONNECTING);
    int ret = open_all_pipelines_l();

    if (ret>=0) {
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(CONNECTED, SET);
        
        notifyListener_l(MEDIA_TRANSCODER_CONNECTED);
        
        play_l();
    }else{
        if (ret!=AVERROR_EXIT) {
            notifyListener_l(MEDIA_TRANSCODER_ERROR, MEDIA_TRANSCODER_ERROR_CONNECT_FAIL);
        }
        
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(ERROR, SET);
        gotError = true;
        stop_l();
    }
}

void DefaultMediaTranscoder::pause()
{
    AutoLock autoLock(&mLock);
    return pause_l();
}

void DefaultMediaTranscoder::pause_l()
{
    if (!(mFlags & STREAMING)) {
        return;
    }
    
    modifyFlags(STREAMING, CLEAR);
    
    mQueue.cancelEvent(mEncodeEvent->eventID());
    mEncodeEventPending = false;

    if (mMediaFrameOutputer) {
        mMediaFrameOutputer->pause();
    }
    
    modifyFlags(PAUSED, SET);
    
    notifyListener_l(MEDIA_TRANSCODER_PAUSED);
    
    return;
}

void DefaultMediaTranscoder::resume()
{
    AutoLock autoLock(&mLock);
    
    return play_l();
}

void DefaultMediaTranscoder::play_l()
{
    LOGI("DefaultMediaTranscoder::play_l Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::play_l Enter");
    }
    
    if (mFlags & STREAMING) {
        LOGW("%s","DefaultMediaTranscoder is streaming");
        if (mMediaLog) {
            mMediaLog->writeLog("DefaultMediaTranscoder is streaming");
        }
        return;
    }
    
    if (!(mFlags & CONNECTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    modifyFlags(CONNECTED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    if (mMediaFrameOutputer) {
        mMediaFrameOutputer->start();
    }
    
    postEncodeEvent_l();

    modifyFlags(STREAMING, SET);
    
    notifyListener_l(MEDIA_TRANSCODER_STREAMING);
    
    LOGI("DefaultMediaTranscoder::play_l Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::play_l Leave");
    }
}

void DefaultMediaTranscoder::cancelTranscoderEvents()
{
    mQueue.cancelEvent(mEncodeEvent->eventID());
    mEncodeEventPending = false;
    mQueue.cancelEvent(mNotifyEvent->eventID());
    mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
    mQueue.cancelEvent(mStopEvent->eventID());
}

void DefaultMediaTranscoder::stop(bool isCancle)
{
    LOGI("DefaultMediaTranscoder::stop Enter");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::stop Enter");
    }
    
    pthread_mutex_lock(&mMediaFrameOutputerLock);
    if (mMediaFrameOutputer) {
        mMediaFrameOutputer->interrupt();
    }
    pthread_mutex_unlock(&mMediaFrameOutputerLock);
    
    AutoLock autoLock(&mLock);
    
    if (mFlags & END) {
        return;
    }
    
    if (mFlags & ENDING) {
        LOGW("already ending");
        
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    if (isCancle) {
        gotError = true;
    }
    
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
    
    if (isCancle) {
        //delete the local file if transcoding to local file
        if (isLocalFile) {
            if (MediaFile::isExist(mProduct.url)) {
                MediaFile::deleteFile(mProduct.url);
            }
        }
    }
    
    LOGI("DefaultMediaTranscoder::stop Leave");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::stop Leave");
    }
}

void DefaultMediaTranscoder::stop_l()
{
    LOGI("DefaultMediaTranscoder::stop_l");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::stop_l");
    }
    
    modifyFlags(ENDING, ASSIGN);
    
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void DefaultMediaTranscoder::postEncodeEvent_l(int64_t delayUs)
{
    if (mEncodeEventPending) {
        return;
    }
    mEncodeEventPending = true;
    mQueue.postEventWithDelay(mEncodeEvent, delayUs < 0 ? 0 : delayUs);
}

void DefaultMediaTranscoder::onStopEvent()
{
    LOGI("DefaultMediaTranscoder::onStopEvent");
    if (mMediaLog) {
        mMediaLog->writeLog("DefaultMediaTranscoder::onStopEvent");
    }
    
    AutoLock autoLock(&mLock);
    
    close_all_pipelines_l();
    
    cancelTranscoderEvents();
    mNotificationQueue.flush();
    
    isImageOverlaying = false;
    
    modifyFlags(END, ASSIGN);
    
    if (!gotError) {
        notifyListener_l(MEDIA_TRANSCODER_END);
    }else{
        //delete the local file if transcoding to local file
        if (isLocalFile) {
            if (MediaFile::isExist(mProduct.url)) {
                MediaFile::deleteFile(mProduct.url);
            }
        }
    }
    gotError = false;
    pthread_cond_broadcast(&mStopCondition);
}

void DefaultMediaTranscoder::onEncodeEvent()
{
    AutoLock autoLock(&mLock);
    
    if (!mEncodeEventPending) {
        // The event has been cancelled in reset_l() but had already // been scheduled for execution at that time.
        return;
    }
    mEncodeEventPending = false;
    
    int ret = flowing_l();
    if (ret==1) {
        postEncodeEvent_l(0);
        return;
    }else if (ret==-7) {
        stop_l();
        return;
    }else if (ret<0) {
        modifyFlags(STREAMING, CLEAR);
        modifyFlags(ERROR, SET);
        
        if (ret==-1) {
            notifyListener_l(MEDIA_TRANSCODER_ERROR, MEDIA_TRANSCODER_ERROR_AUDIO_FILTER_FAIL);
        }
        
        if (ret==-2) {
            notifyListener_l(MEDIA_TRANSCODER_ERROR, MEDIA_TRANSCODER_ERROR_AUDIO_ENCODE_FAIL);
        }
        
        if (ret==-3) {
            notifyListener_l(MEDIA_TRANSCODER_ERROR, MEDIA_TRANSCODER_ERROR_AUDIO_MUX_FAIL);
        }
        
        if (ret==-4) {
            notifyListener_l(MEDIA_TRANSCODER_ERROR, MEDIA_TRANSCODER_ERROR_VIDEO_FILTER_FAIL);
        }
        
        if (ret==-5) {
            notifyListener_l(MEDIA_TRANSCODER_ERROR, MEDIA_TRANSCODER_ERROR_VIDEO_ENCODE_FAIL);
        }
        
        if (ret==-6) {
            notifyListener_l(MEDIA_TRANSCODER_ERROR, MEDIA_TRANSCODER_ERROR_VIDEO_MUX_FAIL);
        }
        
        if (ret==-8) {
            notifyListener_l(MEDIA_TRANSCODER_ERROR, MEDIA_TRANSCODER_ERROR_NO_MEMORY, AVERROR(ENOMEM));
        }
        
        if (ret==-9) {
            notifyListener_l(MEDIA_TRANSCODER_ERROR, MEDIA_TRANSCODER_ERROR_AUDIO_FIFO_READ_FAIL);
        }
        
        if (ret==-10) {
            notifyListener_l(MEDIA_TRANSCODER_ERROR, MEDIA_TRANSCODER_ERROR_OPEN_VIDEO_FILTER_FAIL);
        }
        
        gotError = true;
        stop_l();
        
        return;
    }else {
        int64_t waitTimeUs = 10*1000;
        postEncodeEvent_l(waitTimeUs);
        return;
    }
}

void DefaultMediaTranscoder::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

void DefaultMediaTranscoder::init_ffmpeg_env()
{
    // init ffmpeg env
    av_register_all();
    avcodec_register_all();
    avfilter_register_all();
    
    avformat_network_init();
    FFLog::setLogLevel(AV_LOG_WARNING);
}

//-1 : Create MediaFrameOutputer Fail
//-2 : MediaFrameOutputer Prepare Fail
//-3 : Open Output Media Product Fail
//-4 : Open Video Filter Fail
//-5 : Open Audio Filter Fail
int DefaultMediaTranscoder::open_all_pipelines_l()
{
    if (!mInput.isExternalMediaFrame) {
        pthread_mutex_lock(&mMediaFrameOutputerLock);
        mMediaFrameOutputer = MediaFrameOutputer::createMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, mMediaLog, mInput.url, mInput.http_proxy, mInput.reconnectCount, mProduct.videoOptions.hasVideoTrack, mProduct.audioOptions.hasAudioTrack, mInput.startPosMs, mInput.endPosMs);
        pthread_mutex_unlock(&mMediaFrameOutputerLock);
        
        if (mMediaFrameOutputer==NULL) {
            LOGE("Create MediaFrameOutputer Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Create MediaFrameOutputer Fail");
            }
            return -1;
        }
        mMediaFrameOutputer->setListener(this);
        int ret = mMediaFrameOutputer->prepare();
        if (ret<0) {
            if (ret==AVERROR_EXIT) {
                LOGW("Exit MediaFrameOutputer Prepare");
                if (mMediaLog) {
                    mMediaLog->writeLog("Exit MediaFrameOutputer Prepare");
                }
                return AVERROR_EXIT;
            }
            
            LOGE("MediaFrameOutputer Prepare Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("MediaFrameOutputer Prepare Fail");
            }
            return -2;
        }
        
        notifyListener_l(MEDIA_TRANSCODER_INFO, MEDIA_TRANSCODER_INFO_PUBLISH_DURATION, int(mMediaFrameOutputer->getDurationMs()/1000));
        
        mInputVideoContext = mMediaFrameOutputer->getVideoContext();
        mInputAudioContext = mMediaFrameOutputer->getAudioContext();
    }else{
        mInputVideoContext.hasVideo = mInput.hasVideo;
        mInputVideoContext.width = mInput.width;
        mInputVideoContext.height = mInput.height;
        mInputVideoContext.pix_fmt = (AVPixelFormat)mInput.pixelFormat;
        mInputVideoContext.sample_aspect_ratio = av_make_q(mInput.width,mInput.height);
        mInputVideoContext.time_base = av_make_q(1,90000);
        
        mInputAudioContext.hasAudio = mInput.hasAudio;
        mInputAudioContext.channels = mInput.numChannels;
        if (mInputAudioContext.channels<=1) {
            mInputAudioContext.channel_layout = AV_CH_LAYOUT_MONO;
        }else{
            mInputAudioContext.channel_layout = AV_CH_LAYOUT_STEREO;
        }
        mInputAudioContext.sample_rate = mInput.sampleRate;
        mInputAudioContext.sample_fmt = AV_SAMPLE_FMT_S16;
        mInputAudioContext.time_base = (AVRational){1, mInputAudioContext.sample_rate};
    }
    
    if (!mInputVideoContext.hasVideo) {
        mProduct.videoOptions.hasVideoTrack = false;
    }
    
    if (!mInputAudioContext.hasAudio) {
        mProduct.audioOptions.hasAudioTrack = false;
    }
    
    if (mProduct.videoOptions.hasVideoTrack) {
        if (mProduct.videoOptions.videoWidth==0 || mProduct.videoOptions.videoHeight==0) {
            mProduct.videoOptions.videoWidth = mInputVideoContext.width;
            mProduct.videoOptions.videoHeight = mInputVideoContext.height;
            
            if(mInputVideoContext.rotate==90 || mInputVideoContext.rotate==270)
            {
                int tmp = mProduct.videoOptions.videoWidth;
                mProduct.videoOptions.videoWidth = mProduct.videoOptions.videoHeight;
                mProduct.videoOptions.videoHeight = tmp;
            }
        }
        
        if (mMediaEffectGroup) {
            for (int i = 0; i < mMediaEffectGroup->mediaEffectCount; i++) {
                MediaEffect* mediaEffect = mMediaEffectGroup->mediaEffects[i];
                if (mediaEffect) {
                    if (mediaEffect->type==MEDIA_EFFECT_TYPE_PRIVATE || mediaEffect->type==MEDIA_EFFECT_TYPE_EXTERNAL_RENDER) {
                        if (mProduct.videoOptions.videoWidth<=mProduct.videoOptions.videoHeight) {
                            mProduct.videoOptions.videoWidth = 1242;
                            mProduct.videoOptions.videoHeight = 2208;
                            mProduct.videoOptions.videoBitrateKbps = 8192;
                            
                            private_logo_x = mProduct.videoOptions.videoWidth - 36 - 126;
                            private_logo_y = 132 + 72 + 48 + 72 + 48;
                            
                            bullet_channel_top_padding = 132;
                            bullet_channel_height = 72;
                            bullet_channel_space = 48;
                            bullet_space = 60;
                            
                            bullet_index = 0;
                        }else {
                            mProduct.videoOptions.videoWidth = 1242;
                            mProduct.videoOptions.videoHeight = 698;
                            mProduct.videoOptions.videoBitrateKbps = 4096;
                            
                            private_logo_x = 36;
                            private_logo_y = mProduct.videoOptions.videoHeight - 30 - 69;
                            
                            bullet_channel_top_padding = 24;
                            bullet_channel_height = 72;
                            bullet_channel_space = 48;
                            bullet_space = 60;
                            
                            bullet_index = 0;
                        }
                        break;
                    }
                }
            }
        }
    }
    
    int ret = open_output_media_product(mProduct.url);
    if (ret<0) {
        LOGE("Open Output Media Product Fail");
        if (mMediaLog) {
            mMediaLog->writeLog("Open Output Media Product Fail");
        }
        return -3;
    }

    if (mProduct.videoOptions.hasVideoTrack) {
        produceVideoFilterSpec(false);
        int ret = open_video_filter(mInputVideoContext, video_encoder_ctx, video_filter_spec);
        if (ret<0) {
            LOGE("Open Video Filter Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Open Video Filter Fail");
            }
            return -4;
        }
    }
    
    if (mProduct.audioOptions.hasAudioTrack) {
        char audio_filter_spec[1024];
        audio_filter_spec[0] = 0;
        av_strlcatf(audio_filter_spec, sizeof(audio_filter_spec), "anull");
        
        int ret = open_audio_filter(mInputAudioContext, audio_encoder_ctx, audio_filter_spec);
        if (ret<0) {
            LOGE("Open Audio Filter Fail");
            if (mMediaLog) {
                mMediaLog->writeLog("Open Audio Filter Fail");
            }
            return -5;
        }
    }
    
    if (mProduct.audioOptions.hasAudioTrack) {
        filteredAudioDataFifo = av_audio_fifo_alloc(audio_encoder_ctx->sample_fmt, audio_encoder_ctx->channels, 1);
    }
    
    // Video Overlay
    ignore_video_overlay = false;
    isVideoOverlaying = false;
    
    mVideoOverlayMediaFrameOutputer = NULL;
    mVideoOverlayTimeMs = 0;
    
    mVideoMainYUVFrame = NULL;
    mVideoMainRGBAFrame = NULL;
    mVideoOverlayYUVFrame = NULL;
    mVideoOverlayRGBAAlphaFrame = NULL;
    mVideoOverlayRGBAFrame = NULL;
    mColorSpaceConvert = NULL;
    
    //Private
    mPrivateMainYUVFrame = NULL;
    mPrivateMainRGBAFrame = NULL;
    mPrivateColorSpaceConvert = NULL;
    mPrivateLogoRGBAFrame = NULL;
    mPrivateMaskRGBAFrame = NULL;
    
    return 0;
}

void DefaultMediaTranscoder::produceVideoFilterSpec(bool isIgnoreOverlay)
{
    video_filter_spec[0] = 0;
    
    int inputVideoWidth = mInputVideoContext.width;
    int inputVideoHeight = mInputVideoContext.height;
    
    if (mInputVideoContext.rotate) {
        if (mInputVideoContext.rotate==90) {
            if (video_filter_spec[0]==0)
            {
                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), "transpose=%d", 1);
            }else{
                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), ",transpose=%d", 1);
            }
        }else if(mInputVideoContext.rotate==180) {
            if (video_filter_spec[0]==0)
            {
                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), "transpose=%d,transpose=%d", 1,1);
            }else{
                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), ",transpose=%d,transpose=%d", 1,1);
            }
        }else if(mInputVideoContext.rotate==270) {
            if (video_filter_spec[0]==0)
            {
                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), "transpose=%d", 2);
            }else{
                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), ",transpose=%d", 2);
            }
        }
        
        if (mInputVideoContext.rotate==90 || mInputVideoContext.rotate==270) {
            int tmp = inputVideoWidth;
            inputVideoWidth = inputVideoHeight;
            inputVideoHeight = tmp;
        }
    }

    if (mMediaEffectGroup) {
        for (int i = 0; i < mMediaEffectGroup->mediaEffectCount; i++) {
            MediaEffect* mediaEffect = mMediaEffectGroup->mediaEffects[i];
            if (mediaEffect) {
                if (mediaEffect->type==MEDIA_EFFECT_TYPE_VIDEO_ROTATE) {
                    if (mediaEffect->video_rotation_degree) {
                        if (mediaEffect->video_rotation_degree==90) {
                            if (video_filter_spec[0]==0)
                            {
                                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), "transpose=%d", 1);
                            }else{
                                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), ",transpose=%d", 1);
                            }
                        }else if(mediaEffect->video_rotation_degree==180) {
                            if (video_filter_spec[0]==0)
                            {
                                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), "transpose=%d,transpose=%d", 1,1);
                            }else{
                                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), ",transpose=%d,transpose=%d", 1,1);
                            }
                        }else if (mediaEffect->video_rotation_degree==270) {
                            if (video_filter_spec[0]==0)
                            {
                                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), "transpose=%d", 2);
                            }else{
                                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), ",transpose=%d", 2);
                            }
                        }
                        
                        if (mediaEffect->video_rotation_degree==90 || mediaEffect->video_rotation_degree==270) {
                            int tmp = inputVideoWidth;
                            inputVideoWidth = inputVideoHeight;
                            inputVideoHeight = tmp;
                        }
                    }
                }else if(mediaEffect->type==MEDIA_EFFECT_TYPE_VIDEO_FLIP) {
                    if(mediaEffect->video_flip) {
                        if (mediaEffect->video_flip==1) {
                            if (video_filter_spec[0]==0)
                            {
                                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), "vflip");
                            }else{
                                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), ",vflip");
                            }
                        }else if(mediaEffect->video_flip==2) {
                            if (video_filter_spec[0]==0)
                            {
                                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), "hflip");
                            }else{
                                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), ",hflip");
                            }
                        }
                    }
                }
            }
        }
    }
    
    int outputVideoWidth = video_encoder_ctx->width;
    int outputVideoHeight = video_encoder_ctx->height;
    
    if (inputVideoWidth != outputVideoWidth || inputVideoHeight != outputVideoHeight) {
        
        int video_crop_x = 0;
        int video_crop_y = 0;
        int video_crop_width = 0;
        int video_crop_height = 0;
        
        if(inputVideoWidth*outputVideoHeight>outputVideoWidth*inputVideoHeight)
        {
            video_crop_width = outputVideoWidth*inputVideoHeight/outputVideoHeight;
            video_crop_height = inputVideoHeight;
            
            video_crop_x = (inputVideoWidth - video_crop_width)/2;
            video_crop_y = 0;
            
            if (video_filter_spec[0]==0) {
                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), "crop=%d:%d:%d:%d",video_crop_width,video_crop_height,video_crop_x,video_crop_y);
            }else{
                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), ",crop=%d:%d:%d:%d",video_crop_width,video_crop_height,video_crop_x,video_crop_y);
            }
        }else if(inputVideoWidth*outputVideoHeight<outputVideoWidth*inputVideoHeight)
        {
            video_crop_width = inputVideoWidth;
            video_crop_height = outputVideoHeight*inputVideoWidth/outputVideoWidth;
            
            video_crop_x = 0;
            video_crop_y = (inputVideoHeight - video_crop_height)/2;
            
            if (video_filter_spec[0]==0) {
                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), "crop=%d:%d:%d:%d",video_crop_width,video_crop_height,video_crop_x,video_crop_y);
            }else{
                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), ",crop=%d:%d:%d:%d",video_crop_width,video_crop_height,video_crop_x,video_crop_y);
            }
        }else{
            video_crop_width = inputVideoWidth;
            video_crop_height = inputVideoHeight;
            
            video_crop_x = 0;
            video_crop_y = 0;
        }
        
        if (video_crop_width != outputVideoWidth || video_crop_height != outputVideoHeight) {
            if (video_filter_spec[0]==0) {
                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), "scale=%d:%d",outputVideoWidth,outputVideoHeight);
            }else{
                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), ",scale=%d:%d",outputVideoWidth,outputVideoHeight);
            }
        }
    }
    
    if (!isIgnoreOverlay) {
        if (mMediaEffectGroup) {
            for (int i = 0; i < mMediaEffectGroup->mediaEffectCount; i++) {
                MediaEffect* mediaEffect = mMediaEffectGroup->mediaEffects[i];
                if (mediaEffect) {
                    if (mediaEffect->type==MEDIA_EFFECT_TYPE_PNG)
                    {
                        if (mediaEffect->resourceUrl) {
                            if (video_filter_spec[0]==0) {
                                av_strlcatf(video_filter_spec, sizeof(video_filter_spec), "movie=%s,scale=w=%f*iw:h=%f*ih [watermark]; [in][watermark] overlay=%d:%d [out]", mediaEffect->resourceUrl, mediaEffect->scale, mediaEffect->scale, mediaEffect->x, mediaEffect->y);
                            }else{
                                char new_video_filter_spec[1024];
                                new_video_filter_spec[0] = 0;
                                av_strlcatf(new_video_filter_spec, sizeof(new_video_filter_spec), "movie=%s,scale=w=%f*iw:h=%f*ih [watermark]; [in]",mediaEffect->resourceUrl,mediaEffect->scale, mediaEffect->scale);
                                char* sub_video_filter_spec = strtok(video_filter_spec, ",");
                                while (sub_video_filter_spec!=NULL) {
                                    if (strstr(sub_video_filter_spec, "transpose")) {
                                        av_strlcatf(new_video_filter_spec, sizeof(new_video_filter_spec), "%s [transpose]; [transpose]", sub_video_filter_spec);
                                    }else if (strstr(sub_video_filter_spec, "vflip")) {
                                        av_strlcatf(new_video_filter_spec, sizeof(new_video_filter_spec), "%s [vflip]; [vflip]", sub_video_filter_spec);
                                    }else if(strstr(sub_video_filter_spec, "hflip")) {
                                        av_strlcatf(new_video_filter_spec, sizeof(new_video_filter_spec), "%s [hflip]; [hflip]", sub_video_filter_spec);
                                    }else if (strstr(sub_video_filter_spec, "crop")) {
                                        av_strlcatf(new_video_filter_spec, sizeof(new_video_filter_spec), "%s [crop]; [crop]", sub_video_filter_spec);
                                    }else if (strstr(sub_video_filter_spec, "scale")) {
                                        av_strlcatf(new_video_filter_spec, sizeof(new_video_filter_spec), "%s [scale]; [scale]", sub_video_filter_spec);
                                    }
                                    sub_video_filter_spec=strtok(NULL,",");
                                }
                                av_strlcatf(new_video_filter_spec, sizeof(new_video_filter_spec), "[watermark] overlay=x=%d:y=%d [out]", mediaEffect->x, mediaEffect->y);
                                
                                av_strlcpy(video_filter_spec, new_video_filter_spec, 1024);
                            }
                            isImageOverlaying = true;
                            break;
                        }
                    }
                }
            }
        }
    }
    
    if (video_filter_spec[0]==0) {
        av_strlcatf(video_filter_spec, sizeof(video_filter_spec), "null");
    }
}

void DefaultMediaTranscoder::close_all_pipelines_l()
{
    // Video Overlay
    if (mVideoOverlayMediaFrameOutputer) {
        mVideoOverlayMediaFrameOutputer->stop();
        MediaFrameOutputer::deleteMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, mVideoOverlayMediaFrameOutputer);
        mVideoOverlayMediaFrameOutputer = NULL;
    }
    
    if (mVideoMainYUVFrame) {
        mVideoMainYUVFrame->Free();
        delete mVideoMainYUVFrame;
        mVideoMainYUVFrame = NULL;
    }
    
    if (mVideoMainRGBAFrame) {
        mVideoMainRGBAFrame->Free();
        delete mVideoMainRGBAFrame;
        mVideoMainRGBAFrame = NULL;
    }
    
    if (mVideoOverlayYUVFrame) {
        mVideoOverlayYUVFrame->Free();
        delete mVideoOverlayYUVFrame;
        mVideoOverlayYUVFrame = NULL;
    }
    
    if (mVideoOverlayRGBAAlphaFrame) {
        mVideoOverlayRGBAAlphaFrame->Free();
        delete mVideoOverlayRGBAAlphaFrame;
        mVideoOverlayRGBAAlphaFrame = NULL;
    }
    
    if (mVideoOverlayRGBAFrame) {
        mVideoOverlayRGBAFrame->Free();
        delete mVideoOverlayRGBAFrame;
        mVideoOverlayRGBAFrame = NULL;
    }
    
    if (mColorSpaceConvert) {
        ColorSpaceConvert::DeleteColorSpaceConvert(mColorSpaceConvert, LIBYUV);
        mColorSpaceConvert = NULL;
    }
    
    mVideoOverlayTimeMs = 0;
    isVideoOverlaying = false;
    ignore_video_overlay = false;
    
    //Private
    if (mPrivateMainYUVFrame) {
        mPrivateMainYUVFrame->Free();
        delete mPrivateMainYUVFrame;
        mPrivateMainYUVFrame = NULL;
    }
    if (mPrivateMainYUVFrame) {
        mPrivateMainYUVFrame->Free();
        delete mPrivateMainYUVFrame;
        mPrivateMainYUVFrame = NULL;
    }
    if (mPrivateColorSpaceConvert) {
        ColorSpaceConvert::DeleteColorSpaceConvert(mPrivateColorSpaceConvert, LIBYUV);
        mPrivateColorSpaceConvert = NULL;
    }
    if (mPrivateLogoRGBAFrame) {
        mPrivateLogoRGBAFrame->Free();
        delete mPrivateLogoRGBAFrame;
        mPrivateLogoRGBAFrame = NULL;
    }
    if (mPrivateMaskRGBAFrame) {
        mPrivateMaskRGBAFrame->Free();
        delete mPrivateMaskRGBAFrame;
        mPrivateMaskRGBAFrame = NULL;
    }
    private_logo_x = 0;
    private_logo_y = 0;
    bullet_channel_top_padding = 0;
    bullet_channel_space = 0;
    bullet_space = 0;
    mBulletChannelViewOne.Free();
    mBulletChannelViewTwo.Free();
    
    if (filteredAudioDataFifo) {
        av_audio_fifo_free(filteredAudioDataFifo);
        filteredAudioDataFifo = NULL;
    }
    
    close_video_filter();
    close_audio_filter();
    
    close_output_media_product();
    
    pthread_mutex_lock(&mMediaFrameOutputerLock);
    if (mMediaFrameOutputer!=NULL) {
        mMediaFrameOutputer->stop();
        delete mMediaFrameOutputer;
        mMediaFrameOutputer = NULL;
    }
    pthread_mutex_unlock(&mMediaFrameOutputerLock);
    
    mExternalMediaFrameQueue.flush();
}

//-1 : Audio Filter Fail
//-2 : Audio Encode Fail
//-3 : Audio Mux Fail
//-4 : Video Filter Fail
//-5 : Video Encode Fail
//-6 : Video Mux Fail
//-7 : EOF
//-8 : No Memory Space
//-9 : Audio Fifo Read Fail
//-10 : Open Video Filter Fail
int DefaultMediaTranscoder::flowing_l()
{
    MediaFrame* workMediaFrame = NULL;
    if (mInput.isExternalMediaFrame) {
        workMediaFrame = mExternalMediaFrameQueue.pop();
    }else{
        workMediaFrame = mMediaFrameOutputer->getMediaFrame();
    }
    
    if (workMediaFrame==NULL) {
        return 0;
    }
    
    if(workMediaFrame->type==MEDIA_FRAME_TYPE_AUDIO)
    {
        if (!mProduct.audioOptions.hasAudioTrack) {
            workMediaFrame->Free();
            delete workMediaFrame;
            return 1;
        }
        
        workMediaFrame->avFrame->pts = workMediaFrame->pts;

//        LOGD("audio in_frame pts : %lld",workMediaFrame->avFrame->pts);
        
        //push the decoded audio frame into the filtergraph
        int ret = av_buffersrc_add_frame_flags(audio_buffersrc_ctx, workMediaFrame->avFrame, 0);
        if (ret<0) {
            LOGE("Error while feeding decoded audio frame into the filtergraph");
            
            workMediaFrame->Free();
            delete workMediaFrame;
            
            return -1;
        }
        
        workMediaFrame->Free();
        delete workMediaFrame;
        
        while (true) {
            //pull filtered audio frames from the filtergraph
            AVFrame *filt_frame = av_frame_alloc();
            if (!filt_frame) {
                LOGE("No Memory Space");
                
                return -8;
            }
            
            ret = av_buffersink_get_frame(audio_buffersink_ctx, filt_frame);
            if (ret < 0) {
                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
//                    LOGW("no more audio frames for output");
                }else{
                    LOGE("Error while pulling filtered audio frames from the filtergraph");
                }
                av_frame_free(&filt_frame);
                
                return 1;
            }
            
            filt_frame->pict_type = AV_PICTURE_TYPE_NONE;
            
 //           LOGD("audio filt_frame pts : %lld",filt_frame->pts);
            filt_frame->pts = filt_frame->pts * mInputAudioContext.sample_rate/audio_encoder_ctx->sample_rate;
            
            int ret = av_audio_fifo_write(filteredAudioDataFifo, (void **)filt_frame->data, filt_frame->nb_samples);
            if (ret < filt_frame->nb_samples) {
                LOGE("No Memory Space");
                
                av_frame_free(&filt_frame);
                
                return -8;
            }
            int64_t next_enc_frame_pts = filt_frame->pts;
            av_frame_free(&filt_frame);
            
            while (av_audio_fifo_size(filteredAudioDataFifo) >= audio_encoder_ctx->frame_size) {
                AVFrame *enc_frame = av_frame_alloc();
                if (!enc_frame) {
                    LOGE("No Memory Space");
                    
                    return -8;
                }
                enc_frame->nb_samples = audio_encoder_ctx->frame_size;
                enc_frame->channels = audio_encoder_ctx->channels;
                enc_frame->channel_layout = audio_encoder_ctx->channel_layout;
                enc_frame->format = audio_encoder_ctx->sample_fmt;
                enc_frame->sample_rate = audio_encoder_ctx->sample_rate;
                
                int ret = av_frame_get_buffer(enc_frame, 0);
                if (ret < 0) {
                    LOGE("No Memory Space");
                    
                    av_frame_free(&enc_frame);
                    
                    return -8;
                }
                
                ret = av_audio_fifo_read(filteredAudioDataFifo, (void **)enc_frame->data, audio_encoder_ctx->frame_size);
                if (ret < 0 || ret>audio_encoder_ctx->frame_size) {
                    LOGE("Audio Fifo Read Fail");
                    
                    av_frame_free(&enc_frame);
                    
                    return -9;
                }
                
                enc_frame->nb_samples = ret;
                enc_frame->pts = next_enc_frame_pts;
                next_enc_frame_pts += 1000000 * enc_frame->nb_samples / enc_frame->sample_rate;
                
                //Encoding filtered audio frame
                int got_packet;
                AVPacket enc_pkt;
                enc_pkt.data = NULL;
                enc_pkt.size = 0;
                av_init_packet(&enc_pkt);
                ret = avcodec_encode_audio2(audio_encoder_ctx, &enc_pkt, enc_frame, &got_packet);
                av_frame_free(&enc_frame);

                if (ret < 0) {
                    LOGE("Error while encoding filtered audio frame");
                    
                    return -2;
                }
                
                if (!got_packet) {
                    continue;
                }
                
                //LOGD("audio enc_pkt pts : %lld",enc_pkt.pts);
                if (!mGot_AudioWriteTimeUs_Baseline) {
                    mGot_AudioWriteTimeUs_Baseline = true;
                    mAudioWriteTimeUs_Baseline = enc_pkt.pts;
                }
                mAudioWriteTimeUs = enc_pkt.pts - mAudioWriteTimeUs_Baseline;
                if (mAudioWriteTimeUs>mWriteTimeUs) {
                    mWriteTimeUs = mAudioWriteTimeUs;
                }
                
                //prepare audio packet for muxing
                enc_pkt.stream_index = mOutputAudioStreamIndex;
                av_packet_rescale_ts(&enc_pkt, AV_TIME_BASE_Q, ofmt_ctx->streams[mOutputAudioStreamIndex]->time_base);
                ret = av_interleaved_write_frame(ofmt_ctx, &enc_pkt);
                av_packet_unref(&enc_pkt);
                
                if (ret < 0) {
                    LOGE("Error while muxing audio packet");
                    
                    return -3;
                }else{
                    if (mWriteTimeUs>=(mLastSendWriteTimeS+1)*1000000) {
                        mLastSendWriteTimeS++;
                        notifyListener_l(MEDIA_TRANSCODER_INFO, MEDIA_TRANSCODER_INFO_PUBLISH_TIME, mLastSendWriteTimeS);
                    }
                }
            }
        }
    }else if(workMediaFrame->type==MEDIA_FRAME_TYPE_VIDEO)
    {
        if (!mProduct.videoOptions.hasVideoTrack) {
            workMediaFrame->Free();
            delete workMediaFrame;
            return 1;
        }
        
        if (mMediaEffectGroup) {
            for (int i = 0; i < mMediaEffectGroup->mediaEffectCount; i++) {
                MediaEffect* mediaEffect = mMediaEffectGroup->mediaEffects[i];
                if (mediaEffect) {
                    if (mediaEffect->type==MEDIA_EFFECT_TYPE_PNG)
                    {
                        if (mediaEffect->resourceUrl) {
                            if (mediaEffect->effect_in_ms>=0 && mediaEffect->effect_out_ms>=0 && mediaEffect->effect_out_ms>mediaEffect->effect_in_ms) {
                                bool isResetFilter = false;
                                if (workMediaFrame->pts/1000<mediaEffect->effect_in_ms) {
                                    if (isImageOverlaying) {
                                        isImageOverlaying = false;
                                        isResetFilter = true;
                                    }
                                }else if (workMediaFrame->pts/1000>=mediaEffect->effect_in_ms && workMediaFrame->pts/1000 <= mediaEffect->effect_out_ms) {
                                    if (!isImageOverlaying) {
                                        isImageOverlaying = true;
                                        isResetFilter = true;
                                    }
                                }else if (workMediaFrame->pts/1000 > mediaEffect->effect_out_ms) {
                                    if (isImageOverlaying) {
                                        isImageOverlaying = false;
                                        isResetFilter = true;
                                    }
                                }
                                if (isResetFilter) {
                                    produceVideoFilterSpec(!isImageOverlaying);
                                    close_video_filter();
                                    int ret = open_video_filter(mInputVideoContext, video_encoder_ctx, video_filter_spec);
                                    if (ret<0) {
                                        workMediaFrame->Free();
                                        delete workMediaFrame;
                                        
                                        LOGE("Open Video Filter Fail");
                                        if (mMediaLog) {
                                            mMediaLog->writeLog("Open Video Filter Fail");
                                        }
                                        return -10;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        
        workMediaFrame->avFrame->pts = workMediaFrame->pts;

//        LOGD("video in_frame pts : %lld",workMediaFrame->avFrame->pts);
        
        //push the decoded video frame into the filtergraph
        int ret = av_buffersrc_add_frame_flags(video_buffersrc_ctx, workMediaFrame->avFrame, 0);
        if (ret<0) {
            LOGE("Error while feeding decoded video frame into the filtergraph");
            
            workMediaFrame->Free();
            delete workMediaFrame;
            
            return -4;
        }
        
        workMediaFrame->Free();
        delete workMediaFrame;
        
        while (true) {
            //pull filtered video frames from the filtergraph
            AVFrame *filt_frame = av_frame_alloc();
            if (!filt_frame) {
                LOGE("No Memory Space");
                
                return -8;
            }
            ret = av_buffersink_get_frame(video_buffersink_ctx, filt_frame);
            if (ret < 0) {
                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
//                    LOGW("no more video frames for output");
                }else{
                    LOGE("Error while pulling filtered video frames from the filtergraph");
                }
                av_frame_free(&filt_frame);
                
                return 1;
            }
            
            filt_frame->pict_type = AV_PICTURE_TYPE_NONE;
//            LOGD("video filt_frame pts : %lld",filt_frame->pts);
            video_overlay(filt_frame);
            
            //Encoding filtered video frame
            int got_packet;
            AVPacket enc_pkt;
            enc_pkt.data = NULL;
            enc_pkt.size = 0;
            av_init_packet(&enc_pkt);
            ret = avcodec_encode_video2(video_encoder_ctx, &enc_pkt, filt_frame, &got_packet);
            av_frame_free(&filt_frame);
            if (ret < 0) {
                LOGE("Error while encoding filtered video frame");
                
                return -5;
            }
            
            if (!got_packet) {
                continue;
            }
            
//            LOGD("video enc_pkt pts : %lld",enc_pkt.pts);
//            LOGD("video enc_pkt dts : %lld",enc_pkt.dts);
            if (!mGot_VideoWriteTimeUs_Baseline) {
                mGot_VideoWriteTimeUs_Baseline = true;
                mVideoWriteTimeUs_Baseline = enc_pkt.pts;
            }
            mVideoWriteTimeUs = enc_pkt.pts - mVideoWriteTimeUs_Baseline;
            if (mVideoWriteTimeUs>mWriteTimeUs) {
                mWriteTimeUs = mVideoWriteTimeUs;
            }
            
            //prepare video packet for muxing
            enc_pkt.stream_index = mOutputVideoStreamIndex;
            av_packet_rescale_ts(&enc_pkt, AV_TIME_BASE_Q, ofmt_ctx->streams[mOutputVideoStreamIndex]->time_base);
            ret = av_interleaved_write_frame(ofmt_ctx, &enc_pkt);
            av_packet_unref(&enc_pkt);
            if (ret < 0) {
                LOGE("Error while muxing video packet");
                
                return -6;
            }else{
                if (mWriteTimeUs>=(mLastSendWriteTimeS+1)*1000000) {
                    mLastSendWriteTimeS++;
                    notifyListener_l(MEDIA_TRANSCODER_INFO, MEDIA_TRANSCODER_INFO_PUBLISH_TIME, mLastSendWriteTimeS);
                }
            }
        }

    }else if(workMediaFrame->type==MEDIA_FRAME_TYPE_EOF){
        
        workMediaFrame->Free();
        delete workMediaFrame;
        
        if (mProduct.audioOptions.hasAudioTrack) {
            //flushing audio filter
            int ret = av_buffersrc_add_frame_flags(audio_buffersrc_ctx, NULL, 0);
            if (ret<0) {
                LOGE("Error while feeding NULL audio frame into the filtergraph");
                return -1;
            }
            
            while (true) {
                //pull filtered audio frames from the filtergraph
                AVFrame *filt_frame = av_frame_alloc();
                if (!filt_frame) {
                    LOGE("No Memory Space");
                    
                    return -8;
                }
                ret = av_buffersink_get_frame(audio_buffersink_ctx, filt_frame);
                if (ret < 0) {
                    if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
    //                    LOGW("no more audio frames for output");
                    }else{
                        LOGE("Error while pulling filtered audio frames from the filtergraph");
                    }
                    av_frame_free(&filt_frame);
                    
                    break;
                }
                
                filt_frame->pict_type = AV_PICTURE_TYPE_NONE;
                filt_frame->pts = filt_frame->pts * mInputAudioContext.sample_rate/audio_encoder_ctx->sample_rate;
                
                int ret = av_audio_fifo_write(filteredAudioDataFifo, (void **)filt_frame->data, filt_frame->nb_samples);
                if (ret < filt_frame->nb_samples) {
                    LOGE("No Memory Space");
                    
                    av_frame_free(&filt_frame);
                    
                    return -8;
                }
                int64_t next_enc_frame_pts = filt_frame->pts;
                av_frame_free(&filt_frame);
                
                while (av_audio_fifo_size(filteredAudioDataFifo) >= audio_encoder_ctx->frame_size) {
                    AVFrame *enc_frame = av_frame_alloc();
                    if (!enc_frame) {
                        LOGE("No Memory Space");
                        
                        return -8;
                    }
                    enc_frame->nb_samples = audio_encoder_ctx->frame_size;
                    enc_frame->channels = audio_encoder_ctx->channels;
                    enc_frame->channel_layout = audio_encoder_ctx->channel_layout;
                    enc_frame->format = audio_encoder_ctx->sample_fmt;
                    enc_frame->sample_rate = audio_encoder_ctx->sample_rate;
                    
                    int ret = av_frame_get_buffer(enc_frame, 0);
                    if (ret < 0) {
                        LOGE("No Memory Space");
                        
                        av_frame_free(&enc_frame);
                        
                        return -8;
                    }
                    
                    ret = av_audio_fifo_read(filteredAudioDataFifo, (void **)enc_frame->data, audio_encoder_ctx->frame_size);
                    if (ret < 0 || ret>audio_encoder_ctx->frame_size) {
                        LOGE("Audio Fifo Read Fail");
                        
                        av_frame_free(&enc_frame);
                        
                        return -9;
                    }
                    
                    enc_frame->nb_samples = ret;
                    enc_frame->pts = next_enc_frame_pts;
                    next_enc_frame_pts += 1000000 * enc_frame->nb_samples / enc_frame->sample_rate;
                    
                    //Encoding filtered audio frame
                    int got_packet;
                    AVPacket enc_pkt;
                    enc_pkt.data = NULL;
                    enc_pkt.size = 0;
                    av_init_packet(&enc_pkt);
                    ret = avcodec_encode_audio2(audio_encoder_ctx, &enc_pkt, enc_frame, &got_packet);
                    av_frame_free(&enc_frame);
                    
                    if (ret < 0) {
                        LOGE("Error while encoding filtered audio frame");
                        
                        return -2;
                    }
                    
                    if (!got_packet) {
                        continue;
                    }
                    
                    //LOGD("audio enc_pkt pts : %lld",enc_pkt.pts);
                    if (!mGot_AudioWriteTimeUs_Baseline) {
                        mGot_AudioWriteTimeUs_Baseline = true;
                        mAudioWriteTimeUs_Baseline = enc_pkt.pts;
                    }
                    mAudioWriteTimeUs = enc_pkt.pts - mAudioWriteTimeUs_Baseline;
                    if (mAudioWriteTimeUs>mWriteTimeUs) {
                        mWriteTimeUs = mAudioWriteTimeUs;
                    }
                    
                    //prepare audio packet for muxing
                    enc_pkt.stream_index = mOutputAudioStreamIndex;
                    av_packet_rescale_ts(&enc_pkt, AV_TIME_BASE_Q, ofmt_ctx->streams[mOutputAudioStreamIndex]->time_base);
                    ret = av_interleaved_write_frame(ofmt_ctx, &enc_pkt);
                    av_packet_unref(&enc_pkt);
                    
                    if (ret < 0) {
                        LOGE("Error while muxing audio packet");
                        
                        return -3;
                    }else{
                        if (mWriteTimeUs>=(mLastSendWriteTimeS+1)*1000000) {
                            mLastSendWriteTimeS++;
                            notifyListener_l(MEDIA_TRANSCODER_INFO, MEDIA_TRANSCODER_INFO_PUBLISH_TIME, mLastSendWriteTimeS);
                        }
                    }
                }
            }
        }
        
        if (mProduct.videoOptions.hasVideoTrack) {
            //flushing video filter
            int ret = av_buffersrc_add_frame_flags(video_buffersrc_ctx, NULL, 0);
            if (ret<0) {
                LOGE("Error while feeding NULL video frame into the filtergraph");
                return -4;
            }
            
            while (true) {
                //pull filtered video frames from the filtergraph
                AVFrame *filt_frame = av_frame_alloc();
                if (!filt_frame) {
                    LOGE("No Memory Space");
                    
                    return -8;
                }
                ret = av_buffersink_get_frame(video_buffersink_ctx, filt_frame);
                if (ret < 0) {
                    if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
    //                    LOGW("no more video frames for output");
                    }else{
                        LOGE("Error while pulling filtered video frames from the filtergraph");
                    }
                    av_frame_free(&filt_frame);
                    
                    break;
                }
                
                filt_frame->pict_type = AV_PICTURE_TYPE_NONE;
                
                //Encoding filtered video frame
                int got_packet;
                AVPacket enc_pkt;
                enc_pkt.data = NULL;
                enc_pkt.size = 0;
                av_init_packet(&enc_pkt);
                ret = avcodec_encode_video2(video_encoder_ctx, &enc_pkt, filt_frame, &got_packet);
                av_frame_free(&filt_frame);
                if (ret < 0) {
                    LOGE("Error while encoding filtered video frame");
                    
                    return -5;
                }
                
                if (!got_packet) {
                    continue;
                }
                
                if (!mGot_VideoWriteTimeUs_Baseline) {
                    mGot_VideoWriteTimeUs_Baseline = true;
                    mVideoWriteTimeUs_Baseline = enc_pkt.pts;
                }
                mVideoWriteTimeUs = enc_pkt.pts - mVideoWriteTimeUs_Baseline;
                if (mVideoWriteTimeUs>mWriteTimeUs) {
                    mWriteTimeUs = mVideoWriteTimeUs;
                }
                
                //prepare video packet for muxing
                enc_pkt.stream_index = mOutputVideoStreamIndex;
                av_packet_rescale_ts(&enc_pkt, AV_TIME_BASE_Q, ofmt_ctx->streams[mOutputVideoStreamIndex]->time_base);
                ret = av_interleaved_write_frame(ofmt_ctx, &enc_pkt);
                av_packet_unref(&enc_pkt);

                if (ret < 0) {
                    LOGE("Error while muxing video packet");
                    
                    return -6;
                }else{
                    if (mWriteTimeUs>=(mLastSendWriteTimeS+1)*1000000) {
                        mLastSendWriteTimeS++;
                        notifyListener_l(MEDIA_TRANSCODER_INFO, MEDIA_TRANSCODER_INFO_PUBLISH_TIME, mLastSendWriteTimeS);
                    }
                }
            }
        }
        
        if (mProduct.audioOptions.hasAudioTrack) {
            //Flushing audio encoder
            if (audio_encoder_ctx->codec->capabilities & AV_CODEC_CAP_DELAY) {
                while (true) {
                    int got_packet;
                    AVPacket enc_pkt;
                    enc_pkt.data = NULL;
                    enc_pkt.size = 0;
                    av_init_packet(&enc_pkt);
                    int ret = avcodec_encode_audio2(audio_encoder_ctx, &enc_pkt, NULL, &got_packet);
                    if (ret < 0) {
                        LOGE("Error while flushing the audio encoder");
                        
                        return -2;
                    }
                    
                    if (!got_packet) {
                        break;
                    }
                    
                    if (!mGot_AudioWriteTimeUs_Baseline) {
                        mGot_AudioWriteTimeUs_Baseline = true;
                        mAudioWriteTimeUs_Baseline = enc_pkt.pts;
                    }
                    mAudioWriteTimeUs = enc_pkt.pts - mAudioWriteTimeUs_Baseline;
                    if (mAudioWriteTimeUs>mWriteTimeUs) {
                        mWriteTimeUs = mAudioWriteTimeUs;
                    }
                    
                    //prepare audio packet for muxing
                    enc_pkt.stream_index = mOutputAudioStreamIndex;
                    av_packet_rescale_ts(&enc_pkt, AV_TIME_BASE_Q, ofmt_ctx->streams[mOutputAudioStreamIndex]->time_base);
                    ret = av_interleaved_write_frame(ofmt_ctx, &enc_pkt);
                    av_packet_unref(&enc_pkt);

                    if (ret < 0) {
                        LOGE("Error while muxing audio packet");
                        
                        return -3;
                    }else{
                        if (mWriteTimeUs>=(mLastSendWriteTimeS+1)*1000000) {
                            mLastSendWriteTimeS++;
                            notifyListener_l(MEDIA_TRANSCODER_INFO, MEDIA_TRANSCODER_INFO_PUBLISH_TIME, mLastSendWriteTimeS);
                        }
                    }
                }
            }
        }
        
        if (mProduct.videoOptions.hasVideoTrack) {
            //Flushing video encoder
            if (video_encoder_ctx->codec->capabilities & AV_CODEC_CAP_DELAY) {
                while (true) {
                    int got_packet;
                    AVPacket enc_pkt;
                    enc_pkt.data = NULL;
                    enc_pkt.size = 0;
                    av_init_packet(&enc_pkt);
                    int ret = avcodec_encode_video2(video_encoder_ctx, &enc_pkt, NULL, &got_packet);
                    if (ret < 0) {
                        LOGE("Error while flushing the video encoder");
                        
                        return -5;
                    }
                    
                    if (!got_packet) {
                        break;
                    }
                    
                    if (!mGot_VideoWriteTimeUs_Baseline) {
                        mGot_VideoWriteTimeUs_Baseline = true;
                        mVideoWriteTimeUs_Baseline = enc_pkt.pts;
                    }
                    mVideoWriteTimeUs = enc_pkt.pts - mVideoWriteTimeUs_Baseline;
                    if (mVideoWriteTimeUs>mWriteTimeUs) {
                        mWriteTimeUs = mVideoWriteTimeUs;
                    }
                    
                    //prepare video packet for muxing
                    enc_pkt.stream_index = mOutputVideoStreamIndex;
                    av_packet_rescale_ts(&enc_pkt, AV_TIME_BASE_Q, ofmt_ctx->streams[mOutputVideoStreamIndex]->time_base);
                    ret = av_interleaved_write_frame(ofmt_ctx, &enc_pkt);
                    av_packet_unref(&enc_pkt);
                    
                    if (ret < 0) {
                        LOGE("Error while muxing video packet");
                        
                        return -6;
                    }else{
                        if (mWriteTimeUs>=(mLastSendWriteTimeS+1)*1000000) {
                            mLastSendWriteTimeS++;
                            notifyListener_l(MEDIA_TRANSCODER_INFO, MEDIA_TRANSCODER_INFO_PUBLISH_TIME, mLastSendWriteTimeS);
                        }
                    }
                }
            }
        }
        
        av_write_trailer(ofmt_ctx);
        
        return -7;
    }
    
    workMediaFrame->Free();
    delete workMediaFrame;
    
    return 0;
}

int DefaultMediaTranscoder::open_output_media_product(char *url)
{
    int ret = avformat_alloc_output_context2(&ofmt_ctx, NULL, output_fmt_name, url);
    if (ret < 0 || !ofmt_ctx) {
        LOGE("Could not create output context");
        if (mMediaLog) {
            mMediaLog->writeLog("Could not create output context");
        }
        return AVERROR_UNKNOWN;
    }
    
    int stream_index = -1;
    if (mProduct.videoOptions.hasVideoTrack) {
        AVStream *out_video_stream = avformat_new_stream(ofmt_ctx, NULL);
        if (!out_video_stream) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
            
            LOGE("Failed allocating output video stream");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed allocating output video stream");
            }
            return AVERROR_UNKNOWN;
        }
        
        mOutputVideoStreamIndex = ++stream_index;

        AVCodec *video_encoder = NULL;
        if (mProduct.videoOptions.videoEncoderType==VIDEO_ENCODER_TYPE_X264) {
            video_encoder = avcodec_find_encoder(AV_CODEC_ID_H264);
            if (video_encoder==NULL) {
                LOGE("h264 video encoder not found");
            }
        }else if (mProduct.videoOptions.videoEncoderType==VIDEO_ENCODER_TYPE_X265) {
            video_encoder = avcodec_find_encoder(AV_CODEC_ID_HEVC);
            if (video_encoder==NULL) {
                LOGE("h265 video encoder not found");
            }
        }
        if (!video_encoder) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
            
            LOGE("Necessary video encoder not found");
            if (mMediaLog) {
                mMediaLog->writeLog("Necessary video encoder not found");
            }
            return AVERROR_INVALIDDATA;
        }
        
        AVCodecContext *video_enc_ctx = avcodec_alloc_context3(video_encoder);  //avcodec_free_context
        if (!video_enc_ctx) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
            
            LOGE("Failed to allocate the video encoder context");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed to allocate the video encoder context");
            }
            return AVERROR(ENOMEM);
        }
        
        video_enc_ctx->width = mProduct.videoOptions.videoWidth;
        video_enc_ctx->height = mProduct.videoOptions.videoHeight;
        video_enc_ctx->sample_aspect_ratio = mInputVideoContext.sample_aspect_ratio;
        video_enc_ctx->pix_fmt = AV_PIX_FMT_YUV420P;
        video_enc_ctx->time_base.num = 1;
        video_enc_ctx->time_base.den = mProduct.videoOptions.videoFps;
        video_enc_ctx->gop_size = mProduct.videoOptions.maxKeyFrameIntervalMs / (1000 / mProduct.videoOptions.videoFps);
        video_enc_ctx->keyint_min = video_enc_ctx->gop_size;
        
        AVDictionary *opts = NULL;
        av_dict_set(&opts, "profile", profile_names[mProduct.videoOptions.profile], 0);
        av_dict_set(&opts, "level", level_names[mProduct.videoOptions.level], 0);
        av_dict_set(&opts, "preset", preset_names[mProduct.videoOptions.preset], 0);
        av_dict_set(&opts, "tune", tune_names[mProduct.videoOptions.tune], 0);
        
        video_enc_ctx->rc_max_rate = mProduct.videoOptions.videoBitrateKbps * 1024;
        video_enc_ctx->rc_buffer_size = mProduct.videoOptions.videoBitrateKbps * 1024;

        if (mProduct.videoOptions.videoEncodeMode == VIDEO_ENCODE_MODE_ABR || mProduct.videoOptions.videoEncodeMode == VIDEO_ENCODE_MODE_CBR) {
            video_enc_ctx->bit_rate = mProduct.videoOptions.videoBitrateKbps * 1024;
        }else{
            video_enc_ctx->bit_rate = 0;
            
            float crf_value = 23.0 + mProduct.videoOptions.quality;
            char crf[16];
            sprintf(crf, "%f",crf_value);
            av_opt_set(video_enc_ctx->priv_data, "crf", crf, 0);
            
            char crf_max[16];
            sprintf(crf_max, "%f",48.0);
            av_opt_set(video_enc_ctx->priv_data, "crf_max", crf_max, 0);
        }
        
        video_enc_ctx->flags = video_enc_ctx->flags | AV_CODEC_FLAG_LOOP_FILTER;
        int deblock_value = mProduct.videoOptions.deblockingFilterFactor;
        char deblock[16];
        sprintf(deblock, "%d:%d",deblock_value,deblock_value);
        av_opt_set(video_enc_ctx->priv_data, "deblock", deblock, 0);
        
        if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
            video_enc_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
        
        int ret = avcodec_open2(video_enc_ctx, video_encoder, &opts);
        if (ret < 0) {
            avcodec_free_context(&video_enc_ctx);
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
            
            LOGE("Cannot open video encoder");
            if (mMediaLog) {
                mMediaLog->writeLog("Cannot open video encoder");
            }
            
            return ret;
        }
        ret = avcodec_parameters_from_context(out_video_stream->codecpar, video_enc_ctx);
        if (ret < 0) {
            avcodec_close(video_enc_ctx);
            avcodec_free_context(&video_enc_ctx);
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
            
            LOGE("Failed to copy video encoder parameters to output video stream");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed to copy video encoder parameters to output video stream");
            }
            
            return ret;
        }
        out_video_stream->time_base = video_enc_ctx->time_base;
        
        video_encoder_ctx = video_enc_ctx;
    }
    
    if (mProduct.audioOptions.hasAudioTrack)
    {
        AVStream *out_audio_stream = avformat_new_stream(ofmt_ctx, NULL);
        if (!out_audio_stream) {
            if (video_encoder_ctx) {
                avcodec_close(video_encoder_ctx);
                avcodec_free_context(&video_encoder_ctx);
                video_encoder_ctx = NULL;
            }
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
            
            LOGE("Failed allocating output audio stream");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed allocating output audio stream");
            }
            return AVERROR_UNKNOWN;
        }
        
        mOutputAudioStreamIndex = ++stream_index;
        
//        AVCodec *audio_encoder = avcodec_find_encoder(AV_CODEC_ID_AAC);
        AVCodec *audio_encoder = avcodec_find_encoder_by_name("libfdk_aac");
        if (!audio_encoder) {
            if (video_encoder_ctx) {
                avcodec_close(video_encoder_ctx);
                avcodec_free_context(&video_encoder_ctx);
                video_encoder_ctx = NULL;
            }
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
            
            LOGE("Necessary audio encoder not found");
            if (mMediaLog) {
                mMediaLog->writeLog("Necessary audio encoder not found");
            }
            return AVERROR_INVALIDDATA;
        }
        
        AVCodecContext *audio_enc_ctx = avcodec_alloc_context3(audio_encoder);  //avcodec_free_context
        if (!audio_enc_ctx) {
            if (video_encoder_ctx) {
                avcodec_close(video_encoder_ctx);
                avcodec_free_context(&video_encoder_ctx);
                video_encoder_ctx = NULL;
            }
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
            
            LOGE("Failed to allocate the audio encoder context");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed to allocate the audio encoder context");
            }
            return AVERROR(ENOMEM);
        }
        
        audio_enc_ctx->sample_rate = mProduct.audioOptions.audioSampleRate;
        audio_enc_ctx->channel_layout = av_get_default_channel_layout(mProduct.audioOptions.audioNumChannels);
        audio_enc_ctx->channels = mProduct.audioOptions.audioNumChannels;
        audio_enc_ctx->sample_fmt = AV_SAMPLE_FMT_S16;
        audio_enc_ctx->time_base = (AVRational){1, mProduct.audioOptions.audioSampleRate};
        audio_enc_ctx->bit_rate = mProduct.audioOptions.audioBitrateKbps * 1024;
        audio_enc_ctx->strict_std_compliance = FF_COMPLIANCE_EXPERIMENTAL;
        
        if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
            audio_enc_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
        
        int ret = avcodec_open2(audio_enc_ctx, audio_encoder, NULL);
        if (ret < 0) {
            if (video_encoder_ctx) {
                avcodec_close(video_encoder_ctx);
                avcodec_free_context(&video_encoder_ctx);
                video_encoder_ctx = NULL;
            }
            avcodec_free_context(&audio_enc_ctx);
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
            
            LOGE("Cannot open audio encoder");
            if (mMediaLog) {
                mMediaLog->writeLog("Cannot open audio encoder");
            }
            
            return ret;
        }
        ret = avcodec_parameters_from_context(out_audio_stream->codecpar, audio_enc_ctx);
        if (ret < 0) {
            if (video_encoder_ctx) {
                avcodec_close(video_encoder_ctx);
                avcodec_free_context(&video_encoder_ctx);
                video_encoder_ctx = NULL;
            }
            avcodec_close(audio_enc_ctx);
            avcodec_free_context(&audio_enc_ctx);
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
            
            LOGE("Failed to copy audio encoder parameters to output audio stream");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed to copy audio encoder parameters to output audio stream");
            }
            
            return ret;
        }
        out_audio_stream->time_base = audio_enc_ctx->time_base;
        audio_encoder_ctx = audio_enc_ctx;
    }
    
    av_dump_format(ofmt_ctx, 0, url, 1);

    if (!(ofmt_ctx->oformat->flags & AVFMT_NOFILE)) {
        ret = avio_open(&ofmt_ctx->pb, url, AVIO_FLAG_WRITE);
        if (ret < 0) {
            if (video_encoder_ctx) {
                avcodec_close(video_encoder_ctx);
                avcodec_free_context(&video_encoder_ctx);
                video_encoder_ctx = NULL;
            }
            
            if (audio_encoder_ctx) {
                avcodec_close(audio_encoder_ctx);
                avcodec_free_context(&audio_encoder_ctx);
                audio_encoder_ctx = NULL;
            }
            
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
            
            LOGE("Could not open output url :%s", url);
            char log[1024];
            sprintf(log, "Could not open output url :%s", url);
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
            return ret;
        }
    }
    
    ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        if (video_encoder_ctx) {
            avcodec_close(video_encoder_ctx);
            avcodec_free_context(&video_encoder_ctx);
            video_encoder_ctx = NULL;
        }
        
        if (audio_encoder_ctx) {
            avcodec_close(audio_encoder_ctx);
            avcodec_free_context(&audio_encoder_ctx);
            audio_encoder_ctx = NULL;
        }
        
        if (ofmt_ctx && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
        {
            avio_closep(&ofmt_ctx->pb);
        }
        avformat_free_context(ofmt_ctx);
        ofmt_ctx = NULL;
        
        LOGE("Error occurred when opening output file");
        if (mMediaLog) {
            mMediaLog->writeLog("Error occurred when opening output file");
        }
        return ret;
    }

    return 0;
}

void DefaultMediaTranscoder::close_output_media_product()
{
    if (ofmt_ctx) {
        if (video_encoder_ctx) {
            avcodec_close(video_encoder_ctx);
            avcodec_free_context(&video_encoder_ctx);
            video_encoder_ctx = NULL;
        }
        
        if (audio_encoder_ctx) {
            avcodec_close(audio_encoder_ctx);
            avcodec_free_context(&audio_encoder_ctx);
            audio_encoder_ctx = NULL;
        }
        
        if (ofmt_ctx && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
        {
            avio_closep(&ofmt_ctx->pb);
        }
        avformat_free_context(ofmt_ctx);
        ofmt_ctx = NULL;
    }
}

int DefaultMediaTranscoder::open_video_filter(VideoContext inputVideoContext, AVCodecContext *video_enc_ctx, const char *filter_spec)
{
    char args[512];
    int ret = 0;
    const AVFilter *buffersrc = NULL;
    const AVFilter *buffersink = NULL;
    AVFilterContext *buffersrc_ctx = NULL;
    AVFilterContext *buffersink_ctx = NULL;
    AVFilterInOut *outputs = avfilter_inout_alloc();
    AVFilterInOut *inputs  = avfilter_inout_alloc();
    AVFilterGraph *filter_graph = avfilter_graph_alloc();
    
    if (!outputs || !inputs || !filter_graph) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }

        ret = AVERROR(ENOMEM);
        LOGE("No memory space for avfilter alloc");
        if (mMediaLog) {
            mMediaLog->writeLog("No memory space for avfilter alloc");
        }
        return ret;
    }
    
    buffersrc = avfilter_get_by_name("buffer");
    buffersink = avfilter_get_by_name("buffersink");
    
    if (!buffersrc || !buffersink) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        ret = AVERROR_UNKNOWN;
        
        LOGE("filtering source or sink element not found");
        if (mMediaLog) {
            mMediaLog->writeLog("filtering source or sink element not found");
        }
        
        return ret;
    }
    
    snprintf(args, sizeof(args),
             "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
             inputVideoContext.width, inputVideoContext.height, inputVideoContext.pix_fmt,
             inputVideoContext.time_base.num, inputVideoContext.time_base.den,
             inputVideoContext.sample_aspect_ratio.num,
             inputVideoContext.sample_aspect_ratio.den);
    
    ret = avfilter_graph_create_filter(&buffersrc_ctx, buffersrc, "in",
                                       args, NULL, filter_graph);
    if(ret < 0)
    {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot create video buffer source");
        if (mMediaLog) {
            mMediaLog->writeLog("Cannot create video buffer source");
        }
        
        return ret;
    }
    
    ret = avfilter_graph_create_filter(&buffersink_ctx, buffersink, "out",
                                       NULL, NULL, filter_graph);
    
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot create video buffer sink");
        if (mMediaLog) {
            mMediaLog->writeLog("Cannot create video buffer sink");
        }
        
        return ret;
    }
    
    ret = av_opt_set_bin(buffersink_ctx, "pix_fmts",
                         (uint8_t*)&video_enc_ctx->pix_fmt, sizeof(video_enc_ctx->pix_fmt),
                         AV_OPT_SEARCH_CHILDREN);
    
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot set output pixel format");
        if (mMediaLog) {
            mMediaLog->writeLog("Cannot set output pixel format");
        }
        
        return ret;
    }
    
    /* Endpoints for the filter graph. */
    outputs->name       = av_strdup("in");
    outputs->filter_ctx = buffersrc_ctx;
    outputs->pad_idx    = 0;
    outputs->next       = NULL;
    
    inputs->name       = av_strdup("out");
    inputs->filter_ctx = buffersink_ctx;
    inputs->pad_idx    = 0;
    inputs->next       = NULL;

    if (!outputs->name || !inputs->name) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        ret = AVERROR(ENOMEM);
        LOGE("No memory space");
        if (mMediaLog) {
            mMediaLog->writeLog("No memory space");
        }
        return ret;
    }
    
    if ((ret = avfilter_graph_parse_ptr(filter_graph, filter_spec,
                                        &inputs, &outputs, NULL)) < 0)
    {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("avfilter_graph_parse_ptr fail");
        if (mMediaLog) {
            mMediaLog->writeLog("avfilter_graph_parse_ptr fail");
        }
        return ret;
    }
    
    if ((ret = avfilter_graph_config(filter_graph, NULL)) < 0)
    {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("avfilter_graph_config fail");
        if (mMediaLog) {
            mMediaLog->writeLog("avfilter_graph_config fail");
        }
        return ret;
    }
    
    /* Fill FilteringContext */
    video_buffersrc_ctx = buffersrc_ctx;
    video_buffersink_ctx = buffersink_ctx;
    video_filter_graph = filter_graph;
    
    avfilter_inout_free(&inputs);
    avfilter_inout_free(&outputs);
    
    return 0;
}

void DefaultMediaTranscoder::close_video_filter()
{
    if (video_filter_graph) {
        avfilter_graph_free(&video_filter_graph);
        video_filter_graph = NULL;
    }
    
    video_buffersrc_ctx = NULL;
    video_buffersink_ctx = NULL;
}

int DefaultMediaTranscoder::open_audio_filter(AudioContext audioContext, AVCodecContext* audio_enc_ctx, const char *filter_spec)
{
    char args[512];
    int ret = 0;
    const AVFilter *buffersrc = NULL;
    const AVFilter *buffersink = NULL;
    AVFilterContext *buffersrc_ctx = NULL;
    AVFilterContext *buffersink_ctx = NULL;
    AVFilterInOut *outputs = avfilter_inout_alloc();
    AVFilterInOut *inputs  = avfilter_inout_alloc();
    AVFilterGraph *filter_graph = avfilter_graph_alloc();
    
    if (!outputs || !inputs || !filter_graph) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        ret = AVERROR(ENOMEM);
        LOGE("No memory space for avfilter alloc");
        if (mMediaLog) {
            mMediaLog->writeLog("No memory space for avfilter alloc");
        }
        return ret;
    }
    
    buffersrc = avfilter_get_by_name("abuffer");
    buffersink = avfilter_get_by_name("abuffersink");
    
    if (!buffersrc || !buffersink) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("filtering source or sink element not found");
        if (mMediaLog) {
            mMediaLog->writeLog("filtering source or sink element not found");
        }
        
        ret = AVERROR_UNKNOWN;
        return ret;
    }
    
    if (!audioContext.channel_layout)
    {
        audioContext.channel_layout = av_get_default_channel_layout(audioContext.channels);
    }
    
    snprintf(args, sizeof(args),
             "time_base=%d/%d:sample_rate=%d:sample_fmt=%s:channel_layout=0x%" PRIx64,
             audioContext.time_base.num, audioContext.time_base.den, audioContext.sample_rate,
             av_get_sample_fmt_name(audioContext.sample_fmt),
             audioContext.channel_layout);
    
    ret = avfilter_graph_create_filter(&buffersrc_ctx, buffersrc, "in",
                                       args, NULL, filter_graph);
    
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot create audio buffer source");
        if (mMediaLog) {
            mMediaLog->writeLog("Cannot create audio buffer source");
        }
        
        return ret;
    }
    
    ret = avfilter_graph_create_filter(&buffersink_ctx, buffersink, "out",
                                       NULL, NULL, filter_graph);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot create audio buffer sink");
        if (mMediaLog) {
            mMediaLog->writeLog("Cannot create audio buffer sink");
        }
        return ret;
    }
    
    ret = av_opt_set_bin(buffersink_ctx, "sample_fmts",
                         (uint8_t*)&audio_enc_ctx->sample_fmt, sizeof(audio_enc_ctx->sample_fmt),
                         AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot set output sample format");
        if (mMediaLog) {
            mMediaLog->writeLog("Cannot set output sample format");
        }
        return ret;
    }
    
    ret = av_opt_set_bin(buffersink_ctx, "channel_layouts",
                         (uint8_t*)&audio_enc_ctx->channel_layout,
                         sizeof(audio_enc_ctx->channel_layout), AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot set output channel layout");
        if (mMediaLog) {
            mMediaLog->writeLog("Cannot set output channel layout");
        }
        return ret;
    }
    
    ret = av_opt_set_bin(buffersink_ctx, "sample_rates",
                         (uint8_t*)&audio_enc_ctx->sample_rate, sizeof(audio_enc_ctx->sample_rate),
                         AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot set output sample rate");
        if (mMediaLog) {
            mMediaLog->writeLog("Cannot set output sample rate");
        }
        return ret;
    }
    
    /* Endpoints for the filter graph. */
    outputs->name       = av_strdup("in");
    outputs->filter_ctx = buffersrc_ctx;
    outputs->pad_idx    = 0;
    outputs->next       = NULL;
    
    inputs->name       = av_strdup("out");
    inputs->filter_ctx = buffersink_ctx;
    inputs->pad_idx    = 0;
    inputs->next       = NULL;
    
    if (!outputs->name || !inputs->name) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("No memory space");
        if (mMediaLog) {
            mMediaLog->writeLog("No memory space");
        }
        ret = AVERROR(ENOMEM);
        return ret;
    }
    
    if ((ret = avfilter_graph_parse_ptr(filter_graph, filter_spec,
                                        &inputs, &outputs, NULL)) < 0)
    {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("avfilter_graph_parse_ptr fail");
        if (mMediaLog) {
            mMediaLog->writeLog("avfilter_graph_parse_ptr fail");
        }
        return ret;
    }
    
    if ((ret = avfilter_graph_config(filter_graph, NULL)) < 0)
    {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("avfilter_graph_config fail");
        if (mMediaLog) {
            mMediaLog->writeLog("avfilter_graph_config fail");
        }
        return ret;
    }
    
    /* Fill FilteringContext */
    audio_buffersrc_ctx = buffersrc_ctx;
    audio_buffersink_ctx = buffersink_ctx;
    audio_filter_graph = filter_graph;
    
    avfilter_inout_free(&inputs);
    avfilter_inout_free(&outputs);
    
    return 0;
}

void DefaultMediaTranscoder::close_audio_filter()
{
    if (audio_filter_graph) {
        avfilter_graph_free(&audio_filter_graph);
        audio_filter_graph = NULL;
    }
    
    audio_buffersrc_ctx = NULL;
    audio_buffersink_ctx = NULL;
}

void DefaultMediaTranscoder::pushMediaFrameFromExternal(MediaFrame* externalMediaFrame)
{
    if (mInput.isExternalMediaFrame) {
        mExternalMediaFrameQueue.push(externalMediaFrame);
    }
}

void DefaultMediaTranscoder::video_overlay(AVFrame *main_in)
{
    if (ignore_video_overlay) return;
    
    if (mMediaEffectGroup) {
        for (int i = 0; i < mMediaEffectGroup->mediaEffectCount; i++) {
            MediaEffect* mediaEffect = mMediaEffectGroup->mediaEffects[i];
            if (mediaEffect) {
                if (mediaEffect->type==MEDIA_EFFECT_TYPE_MP4 || mediaEffect->type==MEDIA_EFFECT_TYPE_MP4_ALPHA)
                {
                    if (mediaEffect->resourceUrl) {
                        if (mediaEffect->effect_in_ms>=0 && mediaEffect->effect_out_ms>=0 && mediaEffect->effect_out_ms>mediaEffect->effect_in_ms) {
                            if (main_in->pts/1000<mediaEffect->effect_in_ms) {
                                if (isVideoOverlaying) {
                                    isVideoOverlaying = false;
                                }
                            }else if (main_in->pts/1000>=mediaEffect->effect_in_ms && main_in->pts/1000 <= mediaEffect->effect_out_ms) {
                                if (!isVideoOverlaying) {
                                    isVideoOverlaying = true;
                                }
                            }else if (main_in->pts/1000 > mediaEffect->effect_out_ms) {
                                if (isVideoOverlaying) {
                                    isVideoOverlaying = false;
                                }
                            }
                            
                            if (isVideoOverlaying) {
                                if (!mVideoOverlayMediaFrameOutputer) {
                                    mVideoOverlayMediaFrameOutputer = MediaFrameOutputer::createMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, mMediaLog, mediaEffect->resourceUrl, NULL, 0, true, false, -1, -1);
                                    mVideoOverlayMediaFrameOutputer->prepare();
                                    mVideoOverlayMediaFrameOutputer->start();
                                }
                                
                                if (!mVideoMainYUVFrame) {
                                    mVideoMainYUVFrame = new VideoFrame;
                                    mVideoMainYUVFrame->width = main_in->width;
                                    mVideoMainYUVFrame->height = main_in->height;
                                    mVideoMainYUVFrame->videoRawType = VIDEOFRAME_RAWTYPE_I420;
                                    mVideoMainYUVFrame->frameSize = mVideoMainYUVFrame->width*mVideoMainYUVFrame->height*3/2;
                                    mVideoMainYUVFrame->data = (uint8_t*)malloc(mVideoMainYUVFrame->frameSize);
                                }
                                
                                if (!mVideoMainRGBAFrame) {
                                    mVideoMainRGBAFrame = new VideoFrame;
                                    mVideoMainRGBAFrame->width = main_in->width;
                                    mVideoMainRGBAFrame->height = main_in->height;
                                    mVideoMainRGBAFrame->videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
                                    mVideoMainRGBAFrame->frameSize = mVideoMainRGBAFrame->width*mVideoMainRGBAFrame->height*4;
                                    mVideoMainRGBAFrame->data = (uint8_t*)malloc(mVideoMainRGBAFrame->frameSize);
                                    memset(mVideoMainRGBAFrame->data, 0, mVideoMainRGBAFrame->frameSize);
                                }
                                
                                if (!mVideoOverlayYUVFrame) {
                                    mVideoOverlayYUVFrame = new VideoFrame;
                                    mVideoOverlayYUVFrame->width = mVideoOverlayMediaFrameOutputer->getVideoContext().width;
                                    mVideoOverlayYUVFrame->height = mVideoOverlayMediaFrameOutputer->getVideoContext().height;
                                    mVideoOverlayYUVFrame->videoRawType = VIDEOFRAME_RAWTYPE_I420;
                                    mVideoOverlayYUVFrame->frameSize = mVideoOverlayYUVFrame->width * mVideoOverlayYUVFrame->height *3/2;
                                    mVideoOverlayYUVFrame->data = (uint8_t*)malloc(mVideoOverlayYUVFrame->frameSize);
                                }
                                
                                if (mediaEffect->type==MEDIA_EFFECT_TYPE_MP4) {
                                    if (!mVideoOverlayRGBAFrame) {
                                        mVideoOverlayRGBAFrame = new VideoFrame;
                                        mVideoOverlayRGBAFrame->width = mVideoOverlayMediaFrameOutputer->getVideoContext().width*mediaEffect->scale;
                                        mVideoOverlayRGBAFrame->height = mVideoOverlayMediaFrameOutputer->getVideoContext().height*mediaEffect->scale;
                                        mVideoOverlayRGBAFrame->videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
                                        mVideoOverlayRGBAFrame->frameSize = mVideoOverlayRGBAFrame->width * mVideoOverlayRGBAFrame->height *4;
                                        mVideoOverlayRGBAFrame->data = (uint8_t*)malloc(mVideoOverlayRGBAFrame->frameSize);
                                        memset(mVideoOverlayRGBAFrame->data, 0, mVideoOverlayRGBAFrame->frameSize);
                                    }
                                }else{
                                    if (!mVideoOverlayRGBAAlphaFrame) {
                                        mVideoOverlayRGBAAlphaFrame = new VideoFrame;
                                        mVideoOverlayRGBAAlphaFrame->width = mVideoOverlayMediaFrameOutputer->getVideoContext().width*mediaEffect->scale;
                                        mVideoOverlayRGBAAlphaFrame->height = mVideoOverlayMediaFrameOutputer->getVideoContext().height*mediaEffect->scale;
                                        mVideoOverlayRGBAAlphaFrame->videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
                                        mVideoOverlayRGBAAlphaFrame->frameSize = mVideoOverlayRGBAAlphaFrame->width * mVideoOverlayRGBAAlphaFrame->height *4;
                                        mVideoOverlayRGBAAlphaFrame->data = (uint8_t*)malloc(mVideoOverlayRGBAAlphaFrame->frameSize);
                                    }
                                    
                                    if (!mVideoOverlayRGBAFrame) {
                                        mVideoOverlayRGBAFrame = new VideoFrame;
                                        mVideoOverlayRGBAFrame->width = mVideoOverlayMediaFrameOutputer->getVideoContext().width*mediaEffect->scale/2;
                                        mVideoOverlayRGBAFrame->height = mVideoOverlayMediaFrameOutputer->getVideoContext().height*mediaEffect->scale;
                                        mVideoOverlayRGBAFrame->videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
                                        mVideoOverlayRGBAFrame->frameSize = mVideoOverlayRGBAFrame->width * mVideoOverlayRGBAFrame->height *4;
                                        mVideoOverlayRGBAFrame->data = (uint8_t*)malloc(mVideoOverlayRGBAFrame->frameSize);
                                        memset(mVideoOverlayRGBAFrame->data, 0, mVideoOverlayRGBAFrame->frameSize);
                                    }
                                }
                                
                                if (!mColorSpaceConvert) {
                                    mColorSpaceConvert = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
                                }
                                
                                while (mVideoOverlayTimeMs<=main_in->pts/1000-mediaEffect->effect_in_ms) {
                                    MediaFrame* video_overlay_frame = mVideoOverlayMediaFrameOutputer->getMediaFrame();
                                    if (!video_overlay_frame) {
                                        pthread_mutex_lock(&mVideoOverlayLock);
                                        int64_t reltime = 10 * 1000 * 1000ll;
                                        struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
                                        struct timeval t;
                                        t.tv_sec = t.tv_usec = 0;
                                        gettimeofday(&t, NULL);
                                        ts.tv_sec = t.tv_sec;
                                        ts.tv_nsec = t.tv_usec * 1000;
                                        ts.tv_sec += reltime/1000000000;
                                        ts.tv_nsec += reltime%1000000000;
                                        ts.tv_sec += ts.tv_nsec / 1000000000;
                                        ts.tv_nsec = ts.tv_nsec % 1000000000;
                                        pthread_cond_timedwait(&mVideoOverlayCondition, &mVideoOverlayLock, &ts);
#else
                                        ts.tv_sec  = reltime/1000000000;
                                        ts.tv_nsec = reltime%1000000000;
                                        pthread_cond_timedwait_relative_np(&mVideoOverlayCondition, &mVideoOverlayLock, &ts);
#endif
                                        pthread_mutex_unlock(&mVideoOverlayLock);
                                        continue;
                                    }
                                    
                                    if (video_overlay_frame->type==MEDIA_FRAME_TYPE_UNKNOWN || video_overlay_frame->type==MEDIA_FRAME_TYPE_AUDIO || video_overlay_frame->type==MEDIA_FRAME_TYPE_SUBTITLE) {
                                        video_overlay_frame->Free();
                                        delete video_overlay_frame;
                                        continue;
                                    }else if (video_overlay_frame->type==MEDIA_FRAME_TYPE_EOF) {
                                        video_overlay_frame->Free();
                                        delete video_overlay_frame;
                                        
                                        if (mVideoOverlayMediaFrameOutputer) {
                                            mVideoOverlayMediaFrameOutputer->stop();
                                            MediaFrameOutputer::deleteMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, mVideoOverlayMediaFrameOutputer);
                                            mVideoOverlayMediaFrameOutputer = NULL;
                                        }
                                        
                                        if (!mVideoOverlayMediaFrameOutputer) {
                                            mVideoOverlayMediaFrameOutputer = MediaFrameOutputer::createMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, mMediaLog, mediaEffect->resourceUrl, NULL, 0, true, false, -1, -1);
                                            mVideoOverlayMediaFrameOutputer->prepare();
                                            mVideoOverlayMediaFrameOutputer->start();
                                        }
                                        
                                        continue;
                                    }else if (video_overlay_frame->type==MEDIA_FRAME_TYPE_ERROR) {
                                        video_overlay_frame->Free();
                                        delete video_overlay_frame;
                                        
                                        if (mVideoOverlayMediaFrameOutputer) {
                                            mVideoOverlayMediaFrameOutputer->stop();
                                            MediaFrameOutputer::deleteMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, mVideoOverlayMediaFrameOutputer);
                                            mVideoOverlayMediaFrameOutputer = NULL;
                                        }
                                        
                                        if (mVideoMainYUVFrame) {
                                            free(mVideoMainYUVFrame->data);
                                            delete mVideoMainYUVFrame;
                                            mVideoMainYUVFrame = NULL;
                                        }
                                        
                                        if (mVideoMainRGBAFrame) {
                                            free(mVideoMainRGBAFrame->data);
                                            delete mVideoMainRGBAFrame;
                                            mVideoMainRGBAFrame = NULL;
                                        }
                                        
                                        if (mVideoOverlayYUVFrame) {
                                            free(mVideoOverlayYUVFrame->data);
                                            delete mVideoOverlayYUVFrame;
                                            mVideoOverlayYUVFrame = NULL;
                                        }
                                        
                                        if (mVideoOverlayRGBAAlphaFrame) {
                                            free(mVideoOverlayRGBAAlphaFrame->data);
                                            delete mVideoOverlayRGBAAlphaFrame;
                                            mVideoOverlayRGBAAlphaFrame = NULL;
                                        }
                                        
                                        if (mVideoOverlayRGBAFrame) {
                                            free(mVideoOverlayRGBAFrame->data);
                                            delete mVideoOverlayRGBAFrame;
                                            mVideoOverlayRGBAFrame = NULL;
                                        }
                                        
                                        if (mColorSpaceConvert) {
                                            ColorSpaceConvert::DeleteColorSpaceConvert(mColorSpaceConvert, LIBYUV);
                                            mColorSpaceConvert = NULL;
                                        }
                                        
                                        mVideoOverlayTimeMs = 0;
                                        
                                        ignore_video_overlay = true;
                                        
                                        return;
                                    }else {
                                        AVFrame* overlay = video_overlay_frame->avFrame;
                                        if (overlay->linesize[0]!=overlay->width || overlay->linesize[1]!=overlay->width/2 || overlay->linesize[2]!=overlay->width/2) {
                                            //Y
                                            for (int i=0; i<overlay->height; i++) {
                                                memcpy(mVideoOverlayYUVFrame->data + i*overlay->width, overlay->data[0]+i*overlay->linesize[0], overlay->width);
                                            }
                                            //U
                                            for (int i=0; i<overlay->height/2; i++) {
                                                memcpy(mVideoOverlayYUVFrame->data+overlay->height*overlay->width+i*overlay->width/2, overlay->data[1]+i*overlay->linesize[1], overlay->width/2);
                                            }
                                            //V
                                            for (int i=0; i<overlay->height/2; i++) {
                                                memcpy(mVideoOverlayYUVFrame->data+overlay->height*overlay->width+overlay->height/2*overlay->width/2+i*overlay->width/2, overlay->data[2]+i*overlay->linesize[2], overlay->width/2);
                                            }
                                        }else{
                                            memcpy(mVideoOverlayYUVFrame->data, overlay->data[0], overlay->linesize[0]*overlay->height);
                                            memcpy(mVideoOverlayYUVFrame->data + overlay->linesize[0]*overlay->height, overlay->data[1], overlay->linesize[1]*overlay->height/2);
                                            memcpy(mVideoOverlayYUVFrame->data + overlay->linesize[0]*overlay->height + overlay->linesize[1]*overlay->height/2, overlay->data[2], overlay->linesize[2]*overlay->height/2);
                                        }
                                        
                                        if (mediaEffect->type==MEDIA_EFFECT_TYPE_MP4) {
                                            mColorSpaceConvert->I420toRGBA_Crop_Scale(mVideoOverlayYUVFrame, mVideoOverlayRGBAFrame);
                                        }else{
                                            mColorSpaceConvert->I420toRGBA_Crop_Scale(mVideoOverlayYUVFrame, mVideoOverlayRGBAAlphaFrame);
                                            Overlay::get_alpha_channel_from_right_mask(mVideoOverlayRGBAFrame, mVideoOverlayRGBAAlphaFrame);
                                        }
                                        
                                        while (true) {
                                            MediaFrame* front_next_video_overlay_frame = mVideoOverlayMediaFrameOutputer->frontMediaFrame();
                                            if (!front_next_video_overlay_frame) {
                                                pthread_mutex_lock(&mVideoOverlayLock);
                                                int64_t reltime = 10 * 1000 * 1000ll;
                                                struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
                                                struct timeval t;
                                                t.tv_sec = t.tv_usec = 0;
                                                gettimeofday(&t, NULL);
                                                ts.tv_sec = t.tv_sec;
                                                ts.tv_nsec = t.tv_usec * 1000;
                                                ts.tv_sec += reltime/1000000000;
                                                ts.tv_nsec += reltime%1000000000;
                                                ts.tv_sec += ts.tv_nsec / 1000000000;
                                                ts.tv_nsec = ts.tv_nsec % 1000000000;
                                                pthread_cond_timedwait(&mVideoOverlayCondition, &mVideoOverlayLock, &ts);
#else
                                                ts.tv_sec  = reltime/1000000000;
                                                ts.tv_nsec = reltime%1000000000;
                                                pthread_cond_timedwait_relative_np(&mVideoOverlayCondition, &mVideoOverlayLock, &ts);
#endif
                                                pthread_mutex_unlock(&mVideoOverlayLock);
                                                continue;
                                            }
                                            
                                            if (front_next_video_overlay_frame->type==MEDIA_FRAME_TYPE_UNKNOWN || front_next_video_overlay_frame->type==MEDIA_FRAME_TYPE_AUDIO || front_next_video_overlay_frame->type==MEDIA_FRAME_TYPE_SUBTITLE) {
                                                break;
                                            }else if (front_next_video_overlay_frame->type==MEDIA_FRAME_TYPE_EOF) {
                                                break;
                                            }else if (video_overlay_frame->type==MEDIA_FRAME_TYPE_ERROR) {
                                                break;
                                            }else{
                                                if (front_next_video_overlay_frame->pts - video_overlay_frame->pts <= 200 * 1000 || front_next_video_overlay_frame->pts - video_overlay_frame->pts >= -200 * 1000) {
                                                    video_overlay_frame->duration = front_next_video_overlay_frame->pts - video_overlay_frame->pts;
                                                }
                                                break;
                                            }
                                        }
                                        
                                        mVideoOverlayTimeMs += video_overlay_frame->duration/1000;
                                        
                                        video_overlay_frame->Free();
                                        delete video_overlay_frame;
                                        break;
                                    }
                                }
                                
                                if (main_in->linesize[0]!=main_in->width || main_in->linesize[1]!=main_in->width/2 || main_in->linesize[2]!=main_in->width/2) {
                                    //Y
                                    for (int i=0; i<main_in->height; i++) {
                                        memcpy(mVideoMainYUVFrame->data + i*main_in->width, main_in->data[0]+i*main_in->linesize[0], main_in->width);
                                    }
                                    //U
                                    for (int i=0; i<main_in->height/2; i++) {
                                        memcpy(mVideoMainYUVFrame->data+main_in->height*main_in->width+i*main_in->width/2, main_in->data[1]+i*main_in->linesize[1], main_in->width/2);
                                    }
                                    //V
                                    for (int i=0; i<main_in->height/2; i++) {
                                        memcpy(mVideoMainYUVFrame->data+main_in->height*main_in->width+main_in->height/2*main_in->width/2+i*main_in->width/2, main_in->data[2]+i*main_in->linesize[2], main_in->width/2);
                                    }
                                }else{
                                    memcpy(mVideoMainYUVFrame->data, main_in->data[0], main_in->linesize[0]*main_in->height);
                                    memcpy(mVideoMainYUVFrame->data + main_in->linesize[0]*main_in->height, main_in->data[1], main_in->linesize[1]*main_in->height/2);
                                    memcpy(mVideoMainYUVFrame->data + main_in->linesize[0]*main_in->height + main_in->linesize[1]*main_in->height/2, main_in->data[2], main_in->linesize[2]*main_in->height/2);
                                }
                                mColorSpaceConvert->I420toRGBA_Crop_Scale(mVideoMainYUVFrame, mVideoMainRGBAFrame);
                                
                                Overlay::blend_image_rgba(mVideoMainRGBAFrame, mVideoOverlayRGBAFrame, mediaEffect->x, mediaEffect->y);
                                
                                mColorSpaceConvert->RGBAtoI420_Crop_Scale(mVideoMainRGBAFrame, mVideoMainYUVFrame);
                                main_in->linesize[0] = main_in->width;
                                main_in->linesize[1] = main_in->width/2;
                                main_in->linesize[2] = main_in->width/2;
                                memcpy(main_in->data[0], mVideoMainYUVFrame->data, main_in->linesize[0]*main_in->height);
                                memcpy(main_in->data[1], mVideoMainYUVFrame->data + main_in->linesize[0]*main_in->height, main_in->linesize[1]*main_in->height/2);
                                memcpy(main_in->data[2], mVideoMainYUVFrame->data + main_in->linesize[0]*main_in->height + main_in->linesize[1]*main_in->height/2, main_in->linesize[2]*main_in->height/2);
                            }else{
                                if (mVideoOverlayMediaFrameOutputer) {
                                    mVideoOverlayMediaFrameOutputer->stop();
                                    MediaFrameOutputer::deleteMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, mVideoOverlayMediaFrameOutputer);
                                    mVideoOverlayMediaFrameOutputer = NULL;
                                }
                                
                                if (mVideoMainYUVFrame) {
                                    free(mVideoMainYUVFrame->data);
                                    delete mVideoMainYUVFrame;
                                    mVideoMainYUVFrame = NULL;
                                }
                                
                                if (mVideoMainRGBAFrame) {
                                    free(mVideoMainRGBAFrame->data);
                                    delete mVideoMainRGBAFrame;
                                    mVideoMainRGBAFrame = NULL;
                                }
                                
                                if (mVideoOverlayYUVFrame) {
                                    free(mVideoOverlayYUVFrame->data);
                                    delete mVideoOverlayYUVFrame;
                                    mVideoOverlayYUVFrame = NULL;
                                }
                                
                                if (mVideoOverlayRGBAAlphaFrame) {
                                    free(mVideoOverlayRGBAAlphaFrame->data);
                                    delete mVideoOverlayRGBAAlphaFrame;
                                    mVideoOverlayRGBAAlphaFrame = NULL;
                                }
                                
                                if (mVideoOverlayRGBAFrame) {
                                    free(mVideoOverlayRGBAFrame->data);
                                    delete mVideoOverlayRGBAFrame;
                                    mVideoOverlayRGBAFrame = NULL;
                                }
                                
                                if (mColorSpaceConvert) {
                                    ColorSpaceConvert::DeleteColorSpaceConvert(mColorSpaceConvert, LIBYUV);
                                    mColorSpaceConvert = NULL;
                                }
                                
                                mVideoOverlayTimeMs = 0;
                            }
                        }
                    }
                }else if (mediaEffect->type==MEDIA_EFFECT_TYPE_PRIVATE || mediaEffect->type==MEDIA_EFFECT_TYPE_EXTERNAL_RENDER) {
                    if (!mPrivateMainYUVFrame) {
                        mPrivateMainYUVFrame = new VideoFrame;
                        mPrivateMainYUVFrame->width = main_in->width;
                        mPrivateMainYUVFrame->height = main_in->height;
                        mPrivateMainYUVFrame->videoRawType = VIDEOFRAME_RAWTYPE_I420;
                        mPrivateMainYUVFrame->frameSize = mPrivateMainYUVFrame->width*mPrivateMainYUVFrame->height*3/2;
                        mPrivateMainYUVFrame->data = (uint8_t*)malloc(mPrivateMainYUVFrame->frameSize);
                    }
                    
                    if (!mPrivateMainRGBAFrame) {
                        mPrivateMainRGBAFrame = new VideoFrame;
                        mPrivateMainRGBAFrame->width = main_in->width;
                        mPrivateMainRGBAFrame->height = main_in->height;
                        mPrivateMainRGBAFrame->videoRawType = VIDEOFRAME_RAWTYPE_RGBA;
                        mPrivateMainRGBAFrame->frameSize = mPrivateMainRGBAFrame->width*mPrivateMainRGBAFrame->height*4;
                        mPrivateMainRGBAFrame->data = (uint8_t*)malloc(mPrivateMainRGBAFrame->frameSize);
                        memset(mPrivateMainRGBAFrame->data, 0, mPrivateMainRGBAFrame->frameSize);
                    }
                    
                    if (!mPrivateColorSpaceConvert) {
                        mPrivateColorSpaceConvert = ColorSpaceConvert::CreateColorSpaceConvert(LIBYUV);
                    }
                    
                    if (main_in->linesize[0]!=main_in->width || main_in->linesize[1]!=main_in->width/2 || main_in->linesize[2]!=main_in->width/2) {
                        //Y
                        for (int i=0; i<main_in->height; i++) {
                            memcpy(mPrivateMainYUVFrame->data + i*main_in->width, main_in->data[0]+i*main_in->linesize[0], main_in->width);
                        }
                        //U
                        for (int i=0; i<main_in->height/2; i++) {
                            memcpy(mPrivateMainYUVFrame->data+main_in->height*main_in->width+i*main_in->width/2, main_in->data[1]+i*main_in->linesize[1], main_in->width/2);
                        }
                        //V
                        for (int i=0; i<main_in->height/2; i++) {
                            memcpy(mPrivateMainYUVFrame->data+main_in->height*main_in->width+main_in->height/2*main_in->width/2+i*main_in->width/2, main_in->data[2]+i*main_in->linesize[2], main_in->width/2);
                        }
                    }else{
                        memcpy(mPrivateMainYUVFrame->data, main_in->data[0], main_in->linesize[0]*main_in->height);
                        memcpy(mPrivateMainYUVFrame->data + main_in->linesize[0]*main_in->height, main_in->data[1], main_in->linesize[1]*main_in->height/2);
                        memcpy(mPrivateMainYUVFrame->data + main_in->linesize[0]*main_in->height + main_in->linesize[1]*main_in->height/2, main_in->data[2], main_in->linesize[2]*main_in->height/2);
                    }
                    mPrivateColorSpaceConvert->I420toRGBA_Crop_Scale(mPrivateMainYUVFrame, mPrivateMainRGBAFrame);
                    
                    if (mediaEffect->type==MEDIA_EFFECT_TYPE_PRIVATE) {
                        if (!mPrivateMaskRGBAFrame) {
                            if (mProduct.videoOptions.videoWidth<=mProduct.videoOptions.videoHeight)
                            {
                                if (mediaEffect->private_mask_vertical) {
                                    mPrivateMaskRGBAFrame = PNGImageFileToRGBAVideoFrame(mediaEffect->private_mask_vertical);
                                }
                            }else {
                                if (mediaEffect->private_mask_horizontal) {
                                    mPrivateMaskRGBAFrame = PNGImageFileToRGBAVideoFrame(mediaEffect->private_mask_horizontal);
                                }
                            }
                        }
                        if (mPrivateMaskRGBAFrame) {
                            Overlay::blend_image_rgba(mPrivateMainRGBAFrame, mPrivateMaskRGBAFrame, 0, 0);
                        }
                        if (!mPrivateLogoRGBAFrame) {
                            MTOverlayMaterial material;
                            material.type = MT_LOGO;
                            MTLogo *logo = new MTLogo();
                            logo->icon = strdup(mediaEffect->private_logo_icon);
                            logo->text = strdup(mediaEffect->private_logo_id);
                            logo->fontLibPath = strdup(mediaEffect->private_logo_fontlib_path);
                            material.content = logo;
                            mPrivateLogoRGBAFrame = produceCPUOverlay(material);
                            material.Free();
                        }
                        Overlay::blend_image_rgba(mPrivateMainRGBAFrame, mPrivateLogoRGBAFrame, private_logo_x, private_logo_y);
                        //draw bullet
                        for(list<BulletView*>::iterator it = mBulletChannelViewOne.bulletViews.begin(); it != mBulletChannelViewOne.bulletViews.end();)
                        {
                            BulletView* bulletView = *it;
                            bulletView->x -= mBulletChannelViewOne.bulletSpeed;
                            mBulletChannelViewOne.canDrawSpace = mProduct.videoOptions.videoWidth - bulletView->x - bulletView->bulletImage->width - bullet_space;
                            if (bulletView->x <= -bulletView->bulletImage->width) {
                                bulletView->Free();
                                delete bulletView;
                                it = mBulletChannelViewOne.bulletViews.erase(it);
                            }else {
                                ++it;
                            }
                        }
                        
                        for(list<BulletView*>::iterator it = mBulletChannelViewTwo.bulletViews.begin(); it != mBulletChannelViewTwo.bulletViews.end();)
                        {
                            BulletView* bulletView = *it;
                            bulletView->x -= mBulletChannelViewTwo.bulletSpeed;
                            mBulletChannelViewTwo.canDrawSpace = mProduct.videoOptions.videoWidth - bulletView->x - bulletView->bulletImage->width - bullet_space;
                            if (bulletView->x <= -bulletView->bulletImage->width) {
                                bulletView->Free();
                                delete bulletView;
                                it = mBulletChannelViewTwo.bulletViews.erase(it);
                            }else {
                                ++it;
                            }
                        }
                        Bullet* bullet = NULL;
                        if (!mediaEffect->bullets.empty()) {
                            bullet = mediaEffect->bullets[bullet_index];
                        }
                        if (bullet && bullet->beginTimeMs<=main_in->pts/1000) {
                            if (mBulletChannelViewOne.canDrawSpace>=0 && mBulletChannelViewTwo.canDrawSpace>=0) {
                                int rand_value = rand() % 2;
                                if (rand_value==0) {
                                    bullet->bulletChannel = 0;
                                }else {
                                    bullet->bulletChannel = 1;
                                }
                            }else if (mBulletChannelViewOne.canDrawSpace>=0 && mBulletChannelViewTwo.canDrawSpace<0) {
                                bullet->bulletChannel = 0;
                            }else if (mBulletChannelViewOne.canDrawSpace < 0 && mBulletChannelViewTwo.canDrawSpace>=0) {
                                bullet->bulletChannel = 1;
                            }else {
                                bullet->bulletChannel = -1;
                            }
                            if (bullet->bulletChannel==0) {
                                BulletView *bulletView = new BulletView;
                                bulletView->x = mProduct.videoOptions.videoWidth;
                                bulletView->y = bullet_channel_top_padding;
                                
                                MTOverlayMaterial material;
                                material.type = MT_BULLET_SCREEN;
                                MTBulletScreen* bulletScreen = new MTBulletScreen();
                                bulletScreen->headIcon = strdup(bullet->headIcon);
                                bulletScreen->bodyText = strdup(bullet->text);
                                bulletScreen->tailIcon = strdup(bullet->tailIcon);
                                bulletScreen->fontLibPath = mediaEffect->private_bullet_fontlib_path;
                                material.content = bulletScreen;
                                bulletView->bulletImage = produceCPUOverlay(material);
                                material.Free();
                                
                                if (bulletView->bulletImage == NULL) {
                                    bulletView->Free();
                                    delete bulletView;
                                    bulletView = NULL;
                                }else {
                                    mBulletChannelViewOne.bulletViews.push_back(bulletView);
                                }
                                
                                bullet_index++;
                            }else if(bullet->bulletChannel==1) {
                                BulletView *bulletView = new BulletView;
                                bulletView->x = mProduct.videoOptions.videoWidth;
                                bulletView->y = bullet_channel_top_padding + bullet_channel_height + bullet_channel_space;

                                MTOverlayMaterial material;
                                material.type = MT_BULLET_SCREEN;
                                MTBulletScreen* bulletScreen = new MTBulletScreen();
                                bulletScreen->headIcon = strdup(bullet->headIcon);
                                bulletScreen->bodyText = strdup(bullet->text);
                                bulletScreen->tailIcon = strdup(bullet->tailIcon);
                                bulletScreen->fontLibPath = mediaEffect->private_bullet_fontlib_path;
                                material.content = bulletScreen;
                                bulletView->bulletImage = produceCPUOverlay(material);
                                material.Free();
                                
                                if (bulletView->bulletImage == NULL) {
                                    bulletView->Free();
                                    delete bulletView;
                                    bulletView = NULL;
                                }else {
                                    mBulletChannelViewTwo.bulletViews.push_back(bulletView);
                                }
                                
                                bullet_index++;
                            }
                        }
                        for(list<BulletView*>::iterator it = mBulletChannelViewOne.bulletViews.begin(); it != mBulletChannelViewOne.bulletViews.end(); ++it)
                        {
                            BulletView* bulletView = *it;
                            Overlay::blend_image_rgba(mPrivateMainRGBAFrame, bulletView->bulletImage, bulletView->x, bulletView->y);
                        }
                        for(list<BulletView*>::iterator it = mBulletChannelViewTwo.bulletViews.begin(); it != mBulletChannelViewTwo.bulletViews.end(); ++it)
                        {
                            BulletView* bulletView = *it;
                            Overlay::blend_image_rgba(mPrivateMainRGBAFrame, bulletView->bulletImage, bulletView->x, bulletView->y);
                        }
                        //end draw bullet
                    }else if (mediaEffect->type==MEDIA_EFFECT_TYPE_EXTERNAL_RENDER)
                    {
                        if (this->mMediaDataListener!=NULL) {
                            this->mMediaDataListener(mMediaDataOwner,mPrivateMainRGBAFrame->data,mPrivateMainRGBAFrame->frameSize,mPrivateMainRGBAFrame->width, mPrivateMainRGBAFrame->height);
                        }
                    }
                    mPrivateColorSpaceConvert->RGBAtoI420_Crop_Scale(mPrivateMainRGBAFrame, mPrivateMainYUVFrame);
                    main_in->linesize[0] = main_in->width;
                    main_in->linesize[1] = main_in->width/2;
                    main_in->linesize[2] = main_in->width/2;
                    memcpy(main_in->data[0], mPrivateMainYUVFrame->data, main_in->linesize[0]*main_in->height);
                    memcpy(main_in->data[1], mPrivateMainYUVFrame->data + main_in->linesize[0]*main_in->height, main_in->linesize[1]*main_in->height/2);
                    memcpy(main_in->data[2], mPrivateMainYUVFrame->data + main_in->linesize[0]*main_in->height + main_in->linesize[1]*main_in->height/2, main_in->linesize[2]*main_in->height/2);
                }
            }
        }
    }
}
