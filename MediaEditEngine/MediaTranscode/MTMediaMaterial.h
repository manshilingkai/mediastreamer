//
//  MTMediaMaterial.h
//  Worker
//
//  Created by Think on 2018/9/20.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef MTMediaMaterial_h
#define MTMediaMaterial_h

#include <stdio.h>
#include <stdlib.h>

namespace MT
{

enum VideoPixelFormat {
    VIDEO_PIX_FMT_NONE = -1,
    VIDEO_PIX_FMT_YUV420P = 0,   ///< planar YUV 4:2:0, 12bpp, (1 Cr & Cb sample per 2x2 Y samples)
    VIDEO_PIX_FMT_YUVJ420P = 12,
};

struct MediaMaterial {
    bool isExternalMediaFrame;
    
    //if internal media frame
    char* url;
    char* http_proxy;
    int reconnectCount;
    long startPosMs;
    long endPosMs;
    
    //if external media frame
    bool hasVideo;
    int width;
    int height;
    VideoPixelFormat pixelFormat;
    
    bool hasAudio;
    int sampleRate;
    int numChannels;
    
    MediaMaterial()
    {
        isExternalMediaFrame = false;
        
        url = NULL;
        http_proxy = NULL;
        reconnectCount = 1;
        startPosMs = -1;
        endPosMs = -1;
        
        hasVideo = false;
        width = 0;
        height = 0;
        pixelFormat = VIDEO_PIX_FMT_YUV420P;
        
        hasAudio = false;
        sampleRate = 44100;
        numChannels = 2;
    }
    
    inline void Free()
    {
        if(url)
        {
            free(url);
            url = NULL;
        }
        
        if(http_proxy)
        {
            free(http_proxy);
            http_proxy = NULL;
        }
        
        reconnectCount = 1;
    }
};

};

#endif /* MTMediaMaterial_h */
