//
//  MTWordToBitmap.h
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/26.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef MTWordToBitmap_h
#define MTWordToBitmap_h

#include <stdio.h>
#include "MediaDataType.h"
#include "MTMediaEffect.h"

extern "C"
{
#include <ft2build.h>
#include FT_FREETYPE_H
}

using namespace MT;

class MTWordToBitmap {
public:
    MTWordToBitmap(const char* fontLibPath, int fontSize, FontColor fontColor);
    ~MTWordToBitmap();
    
    bool initialize();
    void terminate();
    
    void setFontSize(int fontSize);
    void setFontColor(FontColor fontColor);
    
    VideoFrame* word2Bitmap(wchar_t inputWord, float alpha);
private:
    FT_Library library;
    FT_Face face;
    
    char* mFontLibPath;
    int mFontSize;
    FontColor mFontColor;
};

#endif /* MTWordToBitmap_h */
