//
//  MediaTranscoder.cpp
//  Worker
//
//  Created by Think on 2018/9/21.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "MediaTranscoder.h"
#include "DefaultMediaTranscoder.h"

#ifdef ANDROID
MediaTranscoder* MediaTranscoder::CreateMediaTranscoder(JavaVM *jvm, MEDIA_TRANSCODER_TYPE type, MediaLog* mediaLog, MediaMaterial input, MediaEffectGroup *pMediaEffectGroup, MediaProduct product)
{
    if (type == MEDIA_TRANSCODER_TYPE_DEFAULT) {
        return new DefaultMediaTranscoder(jvm, mediaLog, input, pMediaEffectGroup, product);
    }
}
#else
MediaTranscoder* MediaTranscoder::CreateMediaTranscoder(MEDIA_TRANSCODER_TYPE type, MediaLog* mediaLog, MediaMaterial input, MediaEffectGroup *pMediaEffectGroup, MediaProduct product)
{
    if (type == MEDIA_TRANSCODER_TYPE_DEFAULT)
    {
        return new DefaultMediaTranscoder(mediaLog, input, pMediaEffectGroup, product);
    }
    
    return NULL;
}
MediaTranscoder* MediaTranscoder::CreateMediaTranscoder(MEDIA_TRANSCODER_TYPE type, MediaLog* mediaLog, char* configureInfo)
{
    if (type == MEDIA_TRANSCODER_TYPE_DEFAULT)
    {
        return new DefaultMediaTranscoder(mediaLog, configureInfo);
    }
    
    return NULL;
}
#endif

void MediaTranscoder::DeleteMediaTranscoder(MEDIA_TRANSCODER_TYPE type, MediaTranscoder* transcoder)
{
    if (type == MEDIA_TRANSCODER_TYPE_DEFAULT) {
        DefaultMediaTranscoder* defaultMediaTranscoder = (DefaultMediaTranscoder*)transcoder;
        if (defaultMediaTranscoder) {
            delete defaultMediaTranscoder;
            defaultMediaTranscoder = NULL;
        }
    }
}
