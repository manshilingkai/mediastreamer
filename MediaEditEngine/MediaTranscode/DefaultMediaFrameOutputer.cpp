//
//  DefaultMediaFrameOutputer.cpp
//  Worker
//
//  Created by Think on 2018/9/21.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "DefaultMediaFrameOutputer.h"
#include "FFLog.h"

DefaultMediaFrameOutputer::DefaultMediaFrameOutputer(MediaLog* mediaLog, char* inputUrl, char* http_proxy, int reconnectCount, bool needVideo, bool  needAudio, long startPosMs, long endPosMs)
{
    mMediaLog = mediaLog;
    if (inputUrl) {
        mInputUrl = strdup(inputUrl);
    }else{
        mInputUrl = NULL;
    }
    
    if (http_proxy) {
        mHttpProxy = strdup(http_proxy);
    }else{
        mHttpProxy = NULL;
    }
    mReconnectCount = reconnectCount;
    
    isNeedVideo = needVideo;
    isNeedAudio = needAudio;
    
    mListener = NULL;
    
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    isMediaFrameOutputerThreadCreated = false;
    isBreakThread = false;
    isWorking = false;
    
    isInterrupt = false;
    pthread_mutex_init(&mInterruptLock, NULL);
    
    ifmt_ctx = NULL;
    mAudioStreamIndex = -1;
    mVideoStreamIndex = -1;
    mTextStreamIndex = -1;
    mFrameRate = 0;
    mRotate = 0;
    audio_dec_ctx = NULL;
    video_dec_ctx = NULL;
    
    mStartPosMs = startPosMs;
    mEndPosMs = endPosMs;
    
    mTargetVideoFps = 0;
    isNeedDropVideoFrame = false;
    targetVideoTimeStamp = 0;
    
    gotFirstKeyFrame = false;
    gotFirstAudioPacket = false;
}

DefaultMediaFrameOutputer::~DefaultMediaFrameOutputer()
{
    pthread_mutex_destroy(&mInterruptLock);

    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
    
    if (mHttpProxy) {
        free(mHttpProxy);
        mHttpProxy = NULL;
    }
    
    if (mInputUrl) {
        free(mInputUrl);
        mInputUrl = NULL;
    }
}

void DefaultMediaFrameOutputer::setListener(IMediaListener* listener)
{
    mListener = listener;
}

int DefaultMediaFrameOutputer::prepare()
{
    gotFirstKeyFrame = false;
    gotFirstAudioPacket = false;
    
    char log[2048];

    LOGD("[DefaultMediaFrameOutputer]:Open Data Source [Url]:%s",mInputUrl);
    sprintf(log, "[DefaultMediaFrameOutputer]:Open Data Source [Url]:%s",mInputUrl);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    
    AVDictionary* options = NULL;
    av_dict_set(&options, "rtsp_transport", "udp", 0);
    
    if (strncmp(mInputUrl, "http", 4) == 0) {
        av_dict_set(&options, "timeout", "20000000", 0); // in ms
    }
    
    if (mHttpProxy) {
        av_dict_set(&options, "http_proxy", mHttpProxy, 0);
    }
    
    int err = -1;
    for (int i = 0; i<mReconnectCount+1; i++) {
        pthread_mutex_lock(&mInterruptLock);
        if (isInterrupt == 1) {
            err=AVERROR_EXIT;
            pthread_mutex_unlock(&mInterruptLock);
            break;
        }else{
            pthread_mutex_unlock(&mInterruptLock);
        }
        
        if (ifmt_ctx) {
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
        }
        
        ifmt_ctx = avformat_alloc_context();
        if (ifmt_ctx==NULL) {
            LOGE("%s","[DefaultMediaFrameOutputer]:Fail Allocate an AVFormatContext");
            if (mMediaLog) {
                mMediaLog->writeLog("[DefaultMediaFrameOutputer]:Fail Allocate an AVFormatContext");
            }
            return AVERROR(ENOMEM);
        }
        
        ifmt_ctx->interrupt_callback.callback = interruptCallback;
        ifmt_ctx->interrupt_callback.opaque = this;
        
        ifmt_ctx->flags |= AVFMT_FLAG_NONBLOCK;
        ifmt_ctx->flags |= AVFMT_FLAG_FAST_SEEK;
        
        err = avformat_open_input(&ifmt_ctx, mInputUrl, NULL, &options);
        
        if (err>=0 || err==AVERROR_EXIT) {
            break;
        }
    }
    
    if(err<0)
    {
        if (ifmt_ctx) {
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
        }
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
            if (mMediaLog) {
                mMediaLog->writeLog("Immediate exit was requested");
            }
        }else{
            LOGE("%s",mInputUrl);
            LOGE("%s","[DefaultMediaFrameOutputer]:Open Data Source Fail");
            LOGE("[DefaultMediaFrameOutputer]:ERROR CODE:%d",err);
            
            sprintf(log, "[DefaultMediaFrameOutputer]:Open Data Source Fail [Url]:%s [Error Code]:%d",mInputUrl,err);
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
        }
        
        return err;
    }
    
    // get track info
    err = avformat_find_stream_info(ifmt_ctx, NULL);
    if (err < 0)
    {
        avformat_close_input(&ifmt_ctx);
        avformat_free_context(ifmt_ctx);
        ifmt_ctx = NULL;
        
        if (err==AVERROR_EXIT) {
            LOGW("Immediate exit was requested");
            if (mMediaLog) {
                mMediaLog->writeLog("Immediate exit was requested");
            }
        }else{
            LOGE("%s","[DefaultMediaFrameOutputer]:Get Stream Info Fail");
            LOGE("[DefaultMediaFrameOutputer]:ERROR CODE:%d",err);
            
            sprintf(log, "[DefaultMediaFrameOutputer]:Get Stream Info Fail [Error Code]:%d",err);
            if (mMediaLog) {
                mMediaLog->writeLog(log);
            }
        }
        
        return err;
    }
    
    LOGD("Stream Duration Ms:%lld",av_rescale(ifmt_ctx->duration, 1000, AV_TIME_BASE));
    sprintf(log, "Stream Duration Ms:%lld",av_rescale(ifmt_ctx->duration, 1000, AV_TIME_BASE));
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    
    // select video and audio track
    mAudioStreamIndex = -1;
    mVideoStreamIndex = -1;
    mTextStreamIndex = -1;
    
    if (ifmt_ctx->nb_programs>0 ) {
        int selectProgramIndex = 0;
        
        if (ifmt_ctx->iformat && strcmp(ifmt_ctx->iformat->name, "hls,applehttp") == 0) {
            int selectBitrate = 0;
            for (int i = 0; i<ifmt_ctx->nb_programs; i++) {
                AVDictionaryEntry *tag = av_dict_get(ifmt_ctx->programs[i]->metadata, "variant_bitrate", NULL, 0);
                if (tag)
                {
                    int strBitrate = atoi(tag->value);
                    if (strBitrate>selectBitrate) {
                        selectBitrate = strBitrate;
                        selectProgramIndex = i;
                    }
                }else continue;
            }
        }
        
        for (int i = 0; i<ifmt_ctx->nb_programs; i++) {
            if (i != selectProgramIndex) {
                ifmt_ctx->programs[i]->discard = AVDISCARD_ALL;
            }
        }
        
        for (int i = 0; i < ifmt_ctx->programs[selectProgramIndex]->nb_stream_indexes; i++)
        {
            int stream_index = ifmt_ctx->programs[selectProgramIndex]->stream_index[i];
            
            if (ifmt_ctx->streams[stream_index]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
            {
                //by default, use the first audio stream, and discard others.
                if(mAudioStreamIndex == -1 && isNeedAudio)
                {
                    mAudioStreamIndex = stream_index;
                }
                else
                {
                    ifmt_ctx->streams[stream_index]->discard = AVDISCARD_ALL;
                }
            }else if (ifmt_ctx->streams[stream_index]->codec->codec_type == AVMEDIA_TYPE_VIDEO && (ifmt_ctx->streams[stream_index]->codec->codec_id==AV_CODEC_ID_H264 || ifmt_ctx->streams[stream_index]->codec->codec_id==AV_CODEC_ID_HEVC || ifmt_ctx->streams[stream_index]->codec->codec_id==AV_CODEC_ID_MPEG4))
            {
                //by default, use the first video stream, and discard others.
                if(mVideoStreamIndex == -1 && isNeedVideo)
                {
                    mVideoStreamIndex = stream_index;
                }
                else
                {
                    ifmt_ctx->streams[stream_index]->discard = AVDISCARD_ALL;
                }
            }else if (ifmt_ctx->streams[stream_index]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
            {
                //by default, use the first text stream, and discard others.
                
                if (mTextStreamIndex==-1) {
                    mTextStreamIndex = stream_index;
                }else
                {
                    ifmt_ctx->streams[stream_index]->discard = AVDISCARD_ALL;
                }
            }else{
                ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
            }
        }
        
    }else{
        for(int i= 0; i < ifmt_ctx->nb_streams; i++)
        {
            if (ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
            {
                //by default, use the first audio stream, and discard others.
                if(mAudioStreamIndex == -1 && isNeedAudio)
                {
                    mAudioStreamIndex = i;
                }
                else
                {
                    ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
                }
            }else if (ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && (ifmt_ctx->streams[i]->codec->codec_id==AV_CODEC_ID_H264 || ifmt_ctx->streams[i]->codec->codec_id==AV_CODEC_ID_HEVC || ifmt_ctx->streams[i]->codec->codec_id==AV_CODEC_ID_MPEG4))
            {
                //by default, use the first video stream, and discard others.
                if(mVideoStreamIndex == -1 && isNeedVideo)
                {
                    mVideoStreamIndex = i;
                }
                else
                {
                    ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
                }
            }else if (ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_SUBTITLE)
            {
                //by default, use the first text stream, and discard others.
                
                if (mTextStreamIndex==-1) {
                    mTextStreamIndex = i;
                }else
                {
                    ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
                }
            }else{
                ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
            }
        }
    }
    
    LOGD("mVideoStreamIndex:%d",mVideoStreamIndex);
    sprintf(log, "mVideoStreamIndex:%d",mVideoStreamIndex);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    LOGD("mAudioStreamIndex:%d",mAudioStreamIndex);
    sprintf(log, "mAudioStreamIndex:%d",mAudioStreamIndex);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    LOGD("mTextStreamIndex:%d",mTextStreamIndex);
    sprintf(log, "mTextStreamIndex:%d",mTextStreamIndex);
    if (mMediaLog) {
        mMediaLog->writeLog(log);
    }
    
    if (mVideoStreamIndex==-1) {
        LOGW("[DefaultMediaFrameOutputer]:No Video Stream");
        if (mMediaLog) {
            mMediaLog->writeLog("[DefaultMediaFrameOutputer]:No Video Stream");
        }
    }
    
    if (mAudioStreamIndex==-1) {
        LOGW("[DefaultMediaFrameOutputer]:No Audio Stream");
        if (mMediaLog) {
            mMediaLog->writeLog("[DefaultMediaFrameOutputer]:No Audio Stream");
        }
    }
    
    if (mTextStreamIndex>=0)
    {
        LOGW("[DefaultMediaFrameOutputer]:Got Text Stream");
        if (mMediaLog) {
            mMediaLog->writeLog("[DefaultMediaFrameOutputer]:Got Text Stream");
        }
    }

    mFrameRate = 0;
    if (mVideoStreamIndex!=-1 && ifmt_ctx->streams[mVideoStreamIndex]!=NULL) {
        mFrameRate = 20;//default
        AVRational fr = av_guess_frame_rate(ifmt_ctx, ifmt_ctx->streams[mVideoStreamIndex], NULL);
        
        LOGD("fr.num:%d",fr.num);
        LOGD("fr.den:%d",fr.den);
        sprintf(log, "fr.num:%d, fr.den:%d",fr.num, fr.den);
        if (mMediaLog) {
            mMediaLog->writeLog(log);
        }
        
        if(fr.num > 0 && fr.den > 0)
        {
            mFrameRate = fr.num/fr.den;
            if(mFrameRate > 100 || mFrameRate <= 0)
            {
                mFrameRate = 20;
            }
        }
    }
    
    isNeedDropVideoFrame = false;
    if (mFrameRate>KNatveVideoMaxFps) {
        mTargetVideoFps = KNatveVideoMaxFps;
        isNeedDropVideoFrame = true;
        targetVideoTimeStamp = 0;
    }else{
        isNeedDropVideoFrame = false;
    }

    mRotate = 0;
    if (mVideoStreamIndex!=-1 && ifmt_ctx->streams[mVideoStreamIndex]!=NULL) {
        AVDictionaryEntry *m = NULL;
        while((m=av_dict_get(ifmt_ctx->streams[mVideoStreamIndex]->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
            if(strcmp(m->key, "rotate")) continue;
            else{
                mRotate = atoi(m->value);
            }
        }
    }
    
    if (mAudioStreamIndex!=-1 && ifmt_ctx->streams[mAudioStreamIndex]!=NULL) {
        int current_audio_sample_rate = ifmt_ctx->streams[mAudioStreamIndex]->codec->sample_rate;
        int current_audio_channels = ifmt_ctx->streams[mAudioStreamIndex]->codec->channels;
        AVSampleFormat current_audio_format = ifmt_ctx->streams[mAudioStreamIndex]->codec->sample_fmt;
        
        if (current_audio_sample_rate<=0 || current_audio_channels<=0 || current_audio_format<0 || current_audio_format>=AV_SAMPLE_FMT_NB) {
            LOGW("[DefaultMediaFrameOutputer]:InValid Audio Stream");
            if (mMediaLog) {
                mMediaLog->writeLog("[DefaultMediaFrameOutputer]:InValid Audio Stream");
            }
            ifmt_ctx->streams[mAudioStreamIndex]->discard = AVDISCARD_ALL;
            mAudioStreamIndex=-1;
        }
    }
    
    if (mAudioStreamIndex==-1 && mVideoStreamIndex==-1) {
        avformat_close_input(&ifmt_ctx);
        avformat_free_context(ifmt_ctx);
        ifmt_ctx = NULL;
        LOGE("[DefaultMediaFrameOutputer]:No Audio Stream and No Video Stream");
        if (mMediaLog) {
            mMediaLog->writeLog("[DefaultMediaFrameOutputer]:No Audio Stream and No Video Stream");
        }
        return AVERROR_STREAM_NOT_FOUND;
    }
    
    if (mAudioStreamIndex!=-1) {
        AVStream *audio_stream = ifmt_ctx->streams[mAudioStreamIndex];
#if defined(IOS) || defined(ANDROID)
        AVCodec* audio_dec = NULL;
        if (audio_stream->codecpar->codec_id==AV_CODEC_ID_AAC)
        {
            audio_dec = avcodec_find_decoder_by_name("libfdk_aac");
        }else{
            audio_dec = avcodec_find_decoder(audio_stream->codecpar->codec_id);
        }
#else
        AVCodec *audio_dec = avcodec_find_decoder(audio_stream->codecpar->codec_id);
#endif
        if (!audio_dec) {
            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
            
            LOGE("Failed to find audio decoder");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed to find audio decoder");
            }
            
            return AVERROR_DECODER_NOT_FOUND;
        }
        AVCodecContext *audio_codec_ctx = avcodec_alloc_context3(audio_dec);
        if (!audio_codec_ctx) {
            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
            
            LOGE("Failed to allocate the audio decoder context");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed to allocate the audio decoder context");
            }
            
            return AVERROR(ENOMEM);
        }
        
        err = avcodec_parameters_to_context(audio_codec_ctx, audio_stream->codecpar);
        if (err < 0) {
            avcodec_free_context(&audio_codec_ctx);
            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
            
            LOGE("Failed to copy audio decoder parameters to input audio decoder context");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed to copy audio decoder parameters to input audio decoder context");
            }
            
            return err;
        }
        
        audio_codec_ctx->refcounted_frames = 1;
        err = avcodec_open2(audio_codec_ctx, audio_dec, NULL);
        if (err < 0) {
            avcodec_free_context(&audio_codec_ctx);
            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
            
            LOGE("Failed to open audio decoder");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed to open audio decoder");
            }
            
            return err;
        }
        
        audio_dec_ctx = audio_codec_ctx;
    }
    
    if (mVideoStreamIndex!=-1) {
        AVStream *video_stream = ifmt_ctx->streams[mVideoStreamIndex];
        AVCodec *video_dec = avcodec_find_decoder(video_stream->codecpar->codec_id);
        if (!video_dec) {
            if (audio_dec_ctx) {
                avcodec_close(audio_dec_ctx);
                avcodec_free_context(&audio_dec_ctx);
                audio_dec_ctx = NULL;
            }

            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
            
            LOGE("Failed to find video decoder");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed to find video decoder");
            }
            
            return AVERROR_DECODER_NOT_FOUND;
        }
        
        AVCodecContext *video_codec_ctx = avcodec_alloc_context3(video_dec);
        if (!video_codec_ctx) {
            if (audio_dec_ctx) {
                avcodec_close(audio_dec_ctx);
                avcodec_free_context(&audio_dec_ctx);
                audio_dec_ctx = NULL;
            }
            
            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
            
            LOGE("Failed to allocate the video decoder context");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed to allocate the video decoder context");
            }
            
            return AVERROR(ENOMEM);
        }
        
        err = avcodec_parameters_to_context(video_codec_ctx, video_stream->codecpar);
        if (err < 0) {
            if (audio_dec_ctx) {
                avcodec_close(audio_dec_ctx);
                avcodec_free_context(&audio_dec_ctx);
                audio_dec_ctx = NULL;
            }
            avcodec_free_context(&video_codec_ctx);
            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
            
            LOGE("Failed to copy video decoder parameters to input video decoder context");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed to copy video decoder parameters to input video decoder context");
            }
            
            return err;
        }
        
        video_codec_ctx->refcounted_frames = 1;
        
        AVDictionary *opts = NULL;
        av_dict_set(&opts, "threads", "auto", 0);
        
        video_codec_ctx->framerate = av_guess_frame_rate(ifmt_ctx, video_stream, NULL);

        err = avcodec_open2(video_codec_ctx, video_dec, &opts);
        if (err < 0) {
            if (audio_dec_ctx) {
                avcodec_close(audio_dec_ctx);
                avcodec_free_context(&audio_dec_ctx);
                audio_dec_ctx = NULL;
            }
            avcodec_free_context(&video_codec_ctx);
            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
            
            LOGE("Failed to open video decoder");
            if (mMediaLog) {
                mMediaLog->writeLog("Failed to open video decoder");
            }
            
            return err;
        }
        
        video_dec_ctx = video_codec_ctx;
    }
    
    // For Debug
    // av_dump_format(ifmt_ctx, 0, mInputUrl, 0);

    if (mStartPosMs>0 && mEndPosMs>0 && mEndPosMs>mStartPosMs) {
        int seekTargetStreamIndex = -1;
        int64_t seekTargetPos = 0;
        
        if (mVideoStreamIndex >= 0) {
            seekTargetStreamIndex = mVideoStreamIndex;
        }else if (mAudioStreamIndex >= 0) {
            seekTargetStreamIndex = mAudioStreamIndex;
        }
        if (seekTargetStreamIndex==-1) {
            seekTargetPos = av_rescale(mStartPosMs, AV_TIME_BASE, 1000) + ifmt_ctx->start_time;
//            if (seekTargetPos>ifmt_ctx->start_time+ifmt_ctx->duration) {
//                seekTargetPos = ifmt_ctx->start_time+ifmt_ctx->duration;
//            }
        }else{
            seekTargetPos= av_rescale_q(mStartPosMs * 1000ll, AV_TIME_BASE_Q, ifmt_ctx->streams[seekTargetStreamIndex]->time_base) + ifmt_ctx->streams[seekTargetStreamIndex]->start_time;
//            if (seekTargetPos>ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration) {
//                seekTargetPos = ifmt_ctx->streams[seekTargetStreamIndex]->start_time + ifmt_ctx->streams[seekTargetStreamIndex]->duration;
//            }
        }

        int ret = avformat_seek_file(ifmt_ctx, seekTargetStreamIndex, INT64_MIN, seekTargetPos, INT64_MAX, AVSEEK_FLAG_BACKWARD);
        if (ret<0) {
            if (audio_dec_ctx) {
                avcodec_close(audio_dec_ctx);
                avcodec_free_context(&audio_dec_ctx);
                audio_dec_ctx = NULL;
            }
            if (video_dec_ctx) {
                avcodec_close(video_dec_ctx);
                avcodec_free_context(&video_dec_ctx);
                video_dec_ctx = NULL;
            }
            avformat_close_input(&ifmt_ctx);
            avformat_free_context(ifmt_ctx);
            ifmt_ctx = NULL;
            
            if (ret==AVERROR_EXIT) {
                LOGW("Immediate exit was requested");
                if (mMediaLog) {
                    mMediaLog->writeLog("Immediate exit was requested");
                }
            }else{
                LOGE("got error data, exit...");
                if (mMediaLog) {
                    mMediaLog->writeLog("got error data, exit...");
                }
            }
            
            return ret;
        }else{
            if (video_dec_ctx) {
                avcodec_flush_buffers(video_dec_ctx);
            }
            
            if (audio_dec_ctx) {
                avcodec_flush_buffers(audio_dec_ctx);
            }
            
            int err = -1;
            while (true) {
                pthread_mutex_lock(&mInterruptLock);
                if (isInterrupt == 1) {
                    err=AVERROR_EXIT;
                    pthread_mutex_unlock(&mInterruptLock);
                    break;
                }else{
                    pthread_mutex_unlock(&mInterruptLock);
                }
                
                AVPacket packet;
                av_init_packet(&packet);
                packet.data = NULL;
                packet.size = 0;
                packet.flags = 0;
                err = av_read_frame(ifmt_ctx, &packet);
                if (err == AVERROR_INVALIDDATA || err == AVERROR(EAGAIN)) {
                    av_packet_unref(&packet);
                    continue;
                }else if (err == AVERROR_EOF) {
                    av_packet_unref(&packet);
                    
                    LOGD("got eof packet");
                    if (mMediaLog) {
                        mMediaLog->writeLog("got eof packet");
                    }
                    
                    break;
                }else if (err<0) {
                    av_packet_unref(&packet);

                    if (ret==AVERROR_EXIT) {
                        LOGW("Immediate exit was requested");
                        if (mMediaLog) {
                            mMediaLog->writeLog("Immediate exit was requested");
                        }
                    }else{
                        LOGE("got error data, exit...");
                        if (mMediaLog) {
                            mMediaLog->writeLog("got error data, exit...");
                        }
                    }
                    
                    break;
                }else{
                    if (mVideoStreamIndex>=0) {
                        if (!gotFirstKeyFrame && packet.stream_index==mVideoStreamIndex && packet.flags & AV_PKT_FLAG_KEY) {
                            gotFirstKeyFrame = true;
                        }
                        
                        if(!gotFirstKeyFrame)
                        {
                            if (packet.stream_index==mVideoStreamIndex) {
                                LOGW("hasn't got first key frame, drop this video packet");
                                if (mMediaLog) {
                                    mMediaLog->writeLog("hasn't got first key frame, drop this video packet");
                                }
                            }else if(packet.stream_index==mAudioStreamIndex) {
                                LOGW("hasn't got first key frame, drop this audio packet");
                                if (mMediaLog) {
                                    mMediaLog->writeLog("hasn't got first key frame, drop this audio packet");
                                }
                            }
                            
                            av_packet_unref(&packet);
                            continue;
                        }
                    }
                    if (packet.stream_index==mAudioStreamIndex) {
                        if (!gotFirstAudioPacket) {
                            gotFirstAudioPacket = true;
                            if (ifmt_ctx->streams[mAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_PCM_S16LE) {
                                LOGD("ifmt_ctx->start_time : %lld", ifmt_ctx->start_time);
                                LOGD("ifmt_ctx->streams[mAudioStreamIndex]->start_time : %lld", ifmt_ctx->streams[mAudioStreamIndex]->start_time);
                                ifmt_ctx->start_time = packet.pts;
                                ifmt_ctx->streams[mAudioStreamIndex]->start_time = packet.pts;
                                LOGD("ifmt_ctx->start_time : %lld", ifmt_ctx->start_time);
                                LOGD("ifmt_ctx->streams[mAudioStreamIndex]->start_time : %lld", ifmt_ctx->streams[mAudioStreamIndex]->start_time);
                            }
                        }
                        if (av_rescale_q((packet.pts - ifmt_ctx->streams[mAudioStreamIndex]->start_time), ifmt_ctx->streams[mAudioStreamIndex]->time_base, AV_TIME_BASE_Q)>=mStartPosMs*1000) {
                            av_packet_unref(&packet);
                            break;
                        }else{
                            av_packet_unref(&packet);
                            continue;
                        }
                    }else if (packet.stream_index==mVideoStreamIndex) {
                        AVFrame *frame = av_frame_alloc();
                        if (!frame) {
                            av_packet_unref(&packet);
                            LOGE("No Memory Space");
                            if (mMediaLog) {
                                mMediaLog->writeLog("No Memory Space");
                            }
                            err = AVERROR(ENOMEM);
                            break;
                        }
                        int got_frame;
                        err = avcodec_decode_video2(video_dec_ctx, frame, &got_frame, &packet);
                        av_packet_unref(&packet);
                        if (err < 0) {
                            av_frame_free(&frame);
                            LOGE("Video Decode Fail");
                            if (mMediaLog) {
                                mMediaLog->writeLog("Video Decode Fail");
                            }
                            break;
                        }
                        if (got_frame) {
                            frame->pts = frame->best_effort_timestamp;
                            if (av_rescale_q((frame->pts - ifmt_ctx->streams[mVideoStreamIndex]->start_time), ifmt_ctx->streams[mVideoStreamIndex]->time_base, AV_TIME_BASE_Q)>=mStartPosMs*1000) {
                                av_frame_free(&frame);
                                break;
                            }else{
                                av_frame_free(&frame);
                                continue;
                            }
                        } else {
                            av_frame_free(&frame);
                            continue;
                        }
                    }else {
                        av_packet_unref(&packet);
                        continue;
                    }
                }
            }
            
            if(err<0)
            {
                if (audio_dec_ctx) {
                    avcodec_close(audio_dec_ctx);
                    avcodec_free_context(&audio_dec_ctx);
                    audio_dec_ctx = NULL;
                }
                if (video_dec_ctx) {
                    avcodec_close(video_dec_ctx);
                    avcodec_free_context(&video_dec_ctx);
                    video_dec_ctx = NULL;
                }
                avformat_close_input(&ifmt_ctx);
                avformat_free_context(ifmt_ctx);
                ifmt_ctx = NULL;
                
                if (err==AVERROR_EXIT) {
                    LOGW("Immediate exit was requested");
                    if (mMediaLog) {
                        mMediaLog->writeLog("Immediate exit was requested");
                    }
                }else{
                    sprintf(log, "[DefaultMediaFrameOutputer]:Seek To StartPos[%ld] Fail -> [Error Code]:%d",mStartPosMs,err);
                    if (mMediaLog) {
                        mMediaLog->writeLog(log);
                    }
                }
                
                return err;
            }
        }
    }
    
    createMediaFrameOutputerThread();
    isMediaFrameOutputerThreadCreated = true;
    
    return 0;
}

void DefaultMediaFrameOutputer::start()
{
    pthread_mutex_lock(&mLock);
    isWorking = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void DefaultMediaFrameOutputer::pause()
{
    pthread_mutex_lock(&mLock);
    isWorking = false;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
}

void DefaultMediaFrameOutputer::stop()
{
    if (isMediaFrameOutputerThreadCreated) {
        deleteMediaFrameOutputerThread();
        isMediaFrameOutputerThreadCreated = false;
    }
    
    if (ifmt_ctx) {
        if (audio_dec_ctx) {
            avcodec_close(audio_dec_ctx);
            avcodec_free_context(&audio_dec_ctx);
            audio_dec_ctx = NULL;
        }
        
        if (video_dec_ctx) {
            avcodec_close(video_dec_ctx);
            avcodec_free_context(&video_dec_ctx);
            video_dec_ctx = NULL;
        }
        
        avformat_close_input(&ifmt_ctx);
        avformat_free_context(ifmt_ctx);
        ifmt_ctx = NULL;
    }
    
    gotFirstKeyFrame = false;
    gotFirstAudioPacket = false;
}

void DefaultMediaFrameOutputer::notifyListener(int event, int ext1, int ext2)
{
    if (mListener) {
        mListener->notify(event, ext1, ext2);
    }else {
        if (event==MEDIA_TRANSCODER_ERROR) {
            MediaFrame* mediaFrame = new MediaFrame;
            mediaFrame->type = MEDIA_FRAME_TYPE_ERROR;
            mMediaFrameQueue.push(mediaFrame);
        }
    }
}

void DefaultMediaFrameOutputer::createMediaFrameOutputerThread()
{
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_create(&mThread, NULL, handleMediaFrameOutputerThread, this);
    
    pthread_attr_destroy(&attr);
}

void* DefaultMediaFrameOutputer::handleMediaFrameOutputerThread(void* ptr)
{
    DefaultMediaFrameOutputer* thiz = (DefaultMediaFrameOutputer*)ptr;
    thiz->mediaFrameOutputerThreadMain();
    return NULL;
}

void DefaultMediaFrameOutputer::mediaFrameOutputerThreadMain()
{
    while (true) {
        pthread_mutex_lock(&mLock);
        if (isBreakThread) {
            pthread_mutex_unlock(&mLock);
            break;
        }
        
        if (!isWorking) {
            pthread_cond_wait(&mCondition, &mLock);
            pthread_mutex_unlock(&mLock);
            continue;
        }
        pthread_mutex_unlock(&mLock);
        
        if (mMediaFrameQueue.count()>=MAX_MEDIA_FRAME_QUEUE_COUNT) {
            pthread_mutex_lock(&mLock);
            int64_t reltime = 100 * 1000 * 1000ll;
            struct timespec ts;

#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
            struct timeval t;
            t.tv_sec = t.tv_usec = 0;
            gettimeofday(&t, NULL);
            ts.tv_sec = t.tv_sec;
            ts.tv_nsec = t.tv_usec * 1000;
            ts.tv_sec += reltime/1000000000;
            ts.tv_nsec += reltime%1000000000;
            ts.tv_sec += ts.tv_nsec / 1000000000;
            ts.tv_nsec = ts.tv_nsec % 1000000000;
            pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
            ts.tv_sec  = reltime/1000000000;
            ts.tv_nsec = reltime%1000000000;
            pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif

            pthread_mutex_unlock(&mLock);
            
            continue;
        }
        
        AVPacket packet;
        av_init_packet(&packet);
        packet.data = NULL;
        packet.size = 0;
        packet.flags = 0;

        int ret = av_read_frame(ifmt_ctx, &packet);
        if (ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN)) {
            av_packet_unref(&packet);
            
            continue;
        }else if (ret == AVERROR_EOF) {
            av_packet_unref(&packet);
            
            LOGD("got eof packet");
            if (mMediaLog) {
                mMediaLog->writeLog("got eof packet");
            }
            
            MediaFrame* mediaFrame = new MediaFrame;
            mediaFrame->type = MEDIA_FRAME_TYPE_EOF;
            mMediaFrameQueue.push(mediaFrame);
            
            pthread_mutex_lock(&mLock);
            isWorking = false;
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else if (ret<0) {
            av_packet_unref(&packet);

            if (ret==AVERROR_EXIT) {
                LOGW("Immediate exit was requested");
                if (mMediaLog) {
                    mMediaLog->writeLog("Immediate exit was requested");
                }
                
                MediaFrame* mediaFrame = new MediaFrame;
                mediaFrame->type = MEDIA_FRAME_TYPE_EOF;
                mMediaFrameQueue.push(mediaFrame);
            }else{
                LOGE("got error data, exit...");
                if (mMediaLog) {
                    mMediaLog->writeLog("got error data, exit...");
                }
                
                notifyListener(MEDIA_TRANSCODER_ERROR, MEDIA_TRANSCODER_ERROR_DEMUXER_READ_FAIL, ret);
            }
                        
            pthread_mutex_lock(&mLock);
            isWorking = false;
            pthread_mutex_unlock(&mLock);
            
            continue;
        }else{
            if (mVideoStreamIndex>=0) {
                if (!gotFirstKeyFrame && packet.stream_index==mVideoStreamIndex && packet.flags & AV_PKT_FLAG_KEY) {
                    gotFirstKeyFrame = true;
                }
                
                if(!gotFirstKeyFrame)
                {
                    if (packet.stream_index==mVideoStreamIndex) {
                        LOGW("hasn't got first key frame, drop this video packet");
                        if (mMediaLog) {
                            mMediaLog->writeLog("hasn't got first key frame, drop this video packet");
                        }
                    }else if(packet.stream_index==mAudioStreamIndex) {
                        LOGW("hasn't got first key frame, drop this audio packet");
                        if (mMediaLog) {
                            mMediaLog->writeLog("hasn't got first key frame, drop this audio packet");
                        }
                    }
                    
                    av_packet_unref(&packet);
                    continue;
                }
            }
            
            if (packet.stream_index==mAudioStreamIndex) {
                if (!gotFirstAudioPacket) {
                    gotFirstAudioPacket = true;
                    if (ifmt_ctx->streams[mAudioStreamIndex]->codec->codec_id==AV_CODEC_ID_PCM_S16LE) {
                        LOGD("ifmt_ctx->start_time : %lld", ifmt_ctx->start_time);
                        LOGD("ifmt_ctx->streams[mAudioStreamIndex]->start_time : %lld", ifmt_ctx->streams[mAudioStreamIndex]->start_time);
                        ifmt_ctx->start_time = packet.pts;
                        ifmt_ctx->streams[mAudioStreamIndex]->start_time = packet.pts;
                        LOGD("ifmt_ctx->start_time : %lld", ifmt_ctx->start_time);
                        LOGD("ifmt_ctx->streams[mAudioStreamIndex]->start_time : %lld", ifmt_ctx->streams[mAudioStreamIndex]->start_time);
                    }
                }
                
                AVFrame *frame = av_frame_alloc();
                if (!frame) {
                    av_packet_unref(&packet);

                    LOGE("No Memory Space");
                    if (mMediaLog) {
                        mMediaLog->writeLog("No Memory Space");
                    }
                    notifyListener(MEDIA_TRANSCODER_ERROR, MEDIA_TRANSCODER_ERROR_NO_MEMORY, AVERROR(ENOMEM));
                    
                    pthread_mutex_lock(&mLock);
                    isWorking = false;
                    pthread_mutex_unlock(&mLock);
                    
                    continue;
                }
                
//                av_packet_rescale_ts(&packet, ifmt_ctx->streams[mAudioStreamIndex]->time_base, audio_dec_ctx->time_base);
                int got_frame;
                int ret = avcodec_decode_audio4(audio_dec_ctx, frame, &got_frame, &packet);
                av_packet_unref(&packet);
                
                if (ret < 0) {
                    av_frame_free(&frame);
                    
                    LOGE("Audio Decode Fail");
                    if (mMediaLog) {
                        mMediaLog->writeLog("Audio Decode Fail");
                    }
                    notifyListener(MEDIA_TRANSCODER_INFO, MEDIA_TRANSCODER_WARN_AUDIO_DECODE_FAIL, ret);
                    
                    continue;
                }
                
                if (got_frame) {
                    frame->pts = frame->best_effort_timestamp;
                    MediaFrame* mediaFrame = new MediaFrame;
                    mediaFrame->avFrame = frame;
                    mediaFrame->type = MEDIA_FRAME_TYPE_AUDIO;
                    mediaFrame->pts = av_rescale_q((frame->pkt_pts - ifmt_ctx->streams[mAudioStreamIndex]->start_time), ifmt_ctx->streams[mAudioStreamIndex]->time_base, AV_TIME_BASE_Q);
                    if (mStartPosMs>=0 && mEndPosMs>0 && mEndPosMs>mStartPosMs)
                    {
                        mediaFrame->pts = mediaFrame->pts - mStartPosMs*1000;
                    }
                    mediaFrame->duration = av_rescale_q(av_frame_get_pkt_duration(frame), ifmt_ctx->streams[mAudioStreamIndex]->time_base, AV_TIME_BASE_Q);
//                    mediaFrame->pts = frame->pts * AV_TIME_BASE * av_q2d(audio_dec_ctx->time_base);
//                    mediaFrame->duration = av_frame_get_pkt_duration(frame) * AV_TIME_BASE * av_q2d(audio_dec_ctx->time_base);
                    mediaFrame->size = av_samples_get_buffer_size(NULL, av_frame_get_channels(frame), frame->nb_samples, (enum AVSampleFormat)frame->format, 1);
                    mMediaFrameQueue.push(mediaFrame);
                    
                    if (mStartPosMs>=0 && mEndPosMs>0 && mEndPosMs>mStartPosMs)
                    {
                        if (mediaFrame->pts>=(mEndPosMs-mStartPosMs)*1000) {
                            LOGD("got eof");
                            if (mMediaLog) {
                                mMediaLog->writeLog("got eof");
                            }
                            
                            MediaFrame* mediaEofFrame = new MediaFrame;
                            mediaEofFrame->type = MEDIA_FRAME_TYPE_EOF;
                            mMediaFrameQueue.push(mediaEofFrame);
                            
                            pthread_mutex_lock(&mLock);
                            isWorking = false;
                            pthread_mutex_unlock(&mLock);
                            
                            continue;
                        }
                    }
                } else {
                    av_frame_free(&frame);
                }
            }else if (packet.stream_index==mVideoStreamIndex) {
                AVFrame *frame = av_frame_alloc();
                if (!frame) {
                    av_packet_unref(&packet);
                    
                    LOGE("No Memory Space");
                    if (mMediaLog) {
                        mMediaLog->writeLog("No Memory Space");
                    }
                    notifyListener(MEDIA_TRANSCODER_ERROR, MEDIA_TRANSCODER_ERROR_NO_MEMORY, AVERROR(ENOMEM));
                    
                    pthread_mutex_lock(&mLock);
                    isWorking = false;
                    pthread_mutex_unlock(&mLock);
                    
                    continue;
                }
                
//                av_packet_rescale_ts(&packet, ifmt_ctx->streams[mVideoStreamIndex]->time_base, video_dec_ctx->time_base);
                int got_frame;
                int ret = avcodec_decode_video2(video_dec_ctx, frame, &got_frame, &packet);
                av_packet_unref(&packet);
                
                if (ret < 0) {
                    av_frame_free(&frame);
                    
                    LOGE("Video Decode Fail");
                    if (mMediaLog) {
                        mMediaLog->writeLog("Video Decode Fail");
                    }
                    notifyListener(MEDIA_TRANSCODER_INFO, MEDIA_TRANSCODER_WARN_VIDEO_DECODE_FAIL, ret);
                    
                    continue;
                }
                
                if (got_frame) {
                    frame->pts = frame->best_effort_timestamp;
                    MediaFrame* mediaFrame = new MediaFrame;
                    mediaFrame->avFrame = frame;
                    mediaFrame->type = MEDIA_FRAME_TYPE_VIDEO;
                    mediaFrame->pts = av_rescale_q((frame->pts - ifmt_ctx->streams[mVideoStreamIndex]->start_time), ifmt_ctx->streams[mVideoStreamIndex]->time_base, AV_TIME_BASE_Q);
                    if (mStartPosMs>=0 && mEndPosMs>0 && mEndPosMs>mStartPosMs)
                    {
                        mediaFrame->pts = mediaFrame->pts - mStartPosMs*1000;
                    }
                    mediaFrame->duration = av_rescale_q(av_frame_get_pkt_duration(frame), ifmt_ctx->streams[mVideoStreamIndex]->time_base, AV_TIME_BASE_Q);
//                    mediaFrame->pts = frame->pts * AV_TIME_BASE * av_q2d(video_dec_ctx->time_base);
//                    mediaFrame->duration = av_frame_get_pkt_duration(frame) * AV_TIME_BASE * av_q2d(video_dec_ctx->time_base);
                    if (frame->format==AV_PIX_FMT_YUV420P || frame->format==AV_PIX_FMT_YUVJ420P) {
                        mediaFrame->size = frame->linesize[0] * frame->height + frame->linesize[1] * frame->height/2 + frame->linesize[2] * frame->height/2;
                    }else{
                        LOGW("Unknown PixelFormat For Decoded Video Frame");
                        if (mMediaLog) {
                            mMediaLog->writeLog("Unknown PixelFormat For Decoded Video Frame");
                        }
                        mediaFrame->size = 0;
                    }
                    
                    bool isDrop = false;
                    if (isNeedDropVideoFrame) {
                        if (mediaFrame->pts/1000 >= targetVideoTimeStamp) {
                            isDrop = false;
                            targetVideoTimeStamp += 1000/mTargetVideoFps;
                        }else {
                            isDrop = true;
                        }
                    }
                    
                    if (isDrop) {
                        if (mediaFrame) {
                            mediaFrame->Free();
                            delete mediaFrame;
                            mediaFrame = NULL;
                        }
                        continue;
                    }
                    
                    mMediaFrameQueue.push(mediaFrame);
                    
                    if (mStartPosMs>=0 && mEndPosMs>0 && mEndPosMs>mStartPosMs)
                    {
                        if (mediaFrame->pts>=(mEndPosMs-mStartPosMs)*1000) {
                            LOGD("got eof");
                            if (mMediaLog) {
                                mMediaLog->writeLog("got eof");
                            }
                            
                            MediaFrame* mediaEofFrame = new MediaFrame;
                            mediaEofFrame->type = MEDIA_FRAME_TYPE_EOF;
                            mMediaFrameQueue.push(mediaEofFrame);
                            
                            pthread_mutex_lock(&mLock);
                            isWorking = false;
                            pthread_mutex_unlock(&mLock);
                            
                            continue;
                        }
                    }
                } else {
                    av_frame_free(&frame);
                }
            }else{
                av_packet_unref(&packet);
            }
        }
    }
    
    mMediaFrameQueue.flush();
}

void DefaultMediaFrameOutputer::deleteMediaFrameOutputerThread()
{
    pthread_mutex_lock(&mLock);
    isBreakThread = true;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_signal(&mCondition);
    
    pthread_join(mThread, NULL);
}

void DefaultMediaFrameOutputer::interrupt()
{
    pthread_mutex_lock(&mInterruptLock);
    isInterrupt = 1;
    pthread_mutex_unlock(&mInterruptLock);
}

int DefaultMediaFrameOutputer::interruptCallback(void* opaque)
{
    DefaultMediaFrameOutputer *thiz = (DefaultMediaFrameOutputer *)opaque;
    return thiz->interruptCallbackMain();
}

int DefaultMediaFrameOutputer::interruptCallbackMain()
{
    int ret = 0;
    pthread_mutex_lock(&mInterruptLock);
    ret = isInterrupt;
    pthread_mutex_unlock(&mInterruptLock);
    
    return ret;
}

MediaFrame* DefaultMediaFrameOutputer::getMediaFrame()
{
    MediaFrame* mediaFrame = mMediaFrameQueue.pop();
    
    pthread_cond_signal(&mCondition);
    
    return mediaFrame;
}

MediaFrame* DefaultMediaFrameOutputer::frontMediaFrame()
{
    MediaFrame* mediaFrame = mMediaFrameQueue.front();
    
    pthread_cond_signal(&mCondition);
    
    return mediaFrame;
}

VideoContext DefaultMediaFrameOutputer::getVideoContext()
{
    VideoContext videoContext;
    
    if (video_dec_ctx) {
        videoContext.hasVideo = true;
        videoContext.width = video_dec_ctx->width;
        videoContext.height = video_dec_ctx->height;
        videoContext.pix_fmt = video_dec_ctx->pix_fmt;
        videoContext.sample_aspect_ratio = video_dec_ctx->sample_aspect_ratio;
//    videoContext.time_base = video_dec_ctx->time_base;
        videoContext.time_base = ifmt_ctx->streams[mVideoStreamIndex]->time_base;
        videoContext.rotate = mRotate;
    }
    
    return videoContext;
}

AudioContext DefaultMediaFrameOutputer::getAudioContext()
{
    AudioContext audioContext;
    
    if (audio_dec_ctx) {
        audioContext.hasAudio = true;
        audioContext.channel_layout = audio_dec_ctx->channel_layout;
        audioContext.channels = audio_dec_ctx->channels;
        audioContext.sample_rate = audio_dec_ctx->sample_rate;
        audioContext.sample_fmt = audio_dec_ctx->sample_fmt;
//    audioContext.time_base = audio_dec_ctx->time_base;
        audioContext.time_base = (AVRational){1, audio_dec_ctx->sample_rate};
    }
    
    return audioContext;
}

int64_t DefaultMediaFrameOutputer::getDurationMs()
{
    if (mStartPosMs>=0 && mEndPosMs>0 && mEndPosMs>mStartPosMs)
    {
        return mEndPosMs - mStartPosMs;
    }else{
        if (ifmt_ctx) {
            return av_rescale(ifmt_ctx->duration, 1000, AV_TIME_BASE);
        }
        
        return 0;
    }
}
