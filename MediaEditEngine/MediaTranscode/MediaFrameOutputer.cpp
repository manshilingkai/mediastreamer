//
//  MediaFrameOutputer.cpp
//  Worker
//
//  Created by Think on 2018/9/21.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include "MediaFrameOutputer.h"
#include "DefaultMediaFrameOutputer.h"

MediaFrameOutputer* MediaFrameOutputer::createMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE type, MediaLog* mediaLog, char* inputUrl, char* http_proxy, int reconnectCount, bool needVideo, bool needsAudio, long startPosMs, long endPosMs)
{
    if (type == MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT) {
        return new DefaultMediaFrameOutputer(mediaLog, inputUrl, http_proxy, reconnectCount, needVideo, needsAudio, startPosMs, endPosMs);
    }
    
    return NULL;
}

void MediaFrameOutputer::deleteMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE type, MediaFrameOutputer* mediaFrameOutputer)
{
    if (type == MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT) {
        DefaultMediaFrameOutputer* defaultMediaFrameOutputer = (DefaultMediaFrameOutputer*)mediaFrameOutputer;
        if (defaultMediaFrameOutputer) {
            delete defaultMediaFrameOutputer;
            defaultMediaFrameOutputer = NULL;
        }
    }
}
