//
//  MediaTranscoder.h
//  Worker
//
//  Created by Think on 2018/9/21.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef MediaTranscoder_h
#define MediaTranscoder_h

#include <stdio.h>
#include "MTMediaMaterial.h"
#include "MTMediaEffect.h"
#include "MTMediaProduct.h"
#include "MediaLog.h"
#include "IMediaListener.h"
#include "MediaDataStruct.h"

#ifdef ANDROID
#include "jni.h"
#endif

using namespace MT;

enum MEDIA_TRANSCODER_TYPE {
    MEDIA_TRANSCODER_TYPE_DEFAULT = 0,
};

class MediaTranscoder {
public:
    virtual ~MediaTranscoder() {}
    
#ifdef ANDROID
    static MediaTranscoder* CreateMediaTranscoder(JavaVM *jvm, MEDIA_TRANSCODER_TYPE type, MediaLog* mediaLog, MediaMaterial input, MediaEffectGroup *pMediaEffectGroup, MediaProduct product);
#else
    static MediaTranscoder* CreateMediaTranscoder(MEDIA_TRANSCODER_TYPE type, MediaLog* mediaLog, MediaMaterial input, MediaEffectGroup *pMediaEffectGroup, MediaProduct product);
    static MediaTranscoder* CreateMediaTranscoder(MEDIA_TRANSCODER_TYPE type, MediaLog* mediaLog, char* configureInfo);
#endif
    static void DeleteMediaTranscoder(MEDIA_TRANSCODER_TYPE type, MediaTranscoder* transcoder);
    
#ifdef ANDROID
    virtual void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event) = 0;
#else
    virtual void setListener(void (*listener)(void*,int,int,int), void* arg) = 0;
    virtual void setMediaDataListener(void (*listener)(void*, void*, int, int, int), void* arg) = 0;
#endif
    
    virtual void pushMediaFrameFromExternal(MediaFrame* externalMediaFrame) = 0;
    
    virtual void start() = 0;
    virtual void resume() = 0;
    virtual void pause() = 0;
    virtual void stop(bool isCancle = false) = 0;
    
    virtual void notify(int event, int ext1, int ext2) = 0;
};

#endif /* MediaTranscoder_h */
