//
//  DubbingScore.cpp
//  MediaStreamer
//
//  Created by Think on 2019/9/29.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "DubbingScore.h"
#include "AutoLock.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#else
#include "NormalMediaListener.h"
#endif

#include "MediaLog.h"
#include "MediaFile.h"
#include "FFLog.h"
#include "AudioMixer.h"

#include "MediaTime.h"

struct DubbingScoreEvent : public TimedEventQueue::Event {
    DubbingScoreEvent(
                                DubbingScore *score,
                                void (DubbingScore::*method)())
    : mDubbingScore(score),
    mMethod(method) {
    }
    
protected:
    virtual ~DubbingScoreEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mDubbingScore->*mMethod)();
    }
    
private:
    DubbingScore *mDubbingScore;
    void (DubbingScore::*mMethod)();
    
    DubbingScoreEvent(const DubbingScoreEvent &);
    DubbingScoreEvent &operator=(const DubbingScoreEvent &);
};

#ifdef ANDROID
DubbingScore::DubbingScore(JavaVM *jvm, char* originUrl, char* bgmUrl, char* dubUrl)
{
    mJvm = jvm;
    
    init_ffmpeg_env();

    mOriginUrl = NULL;
    if (originUrl) {
        mOriginUrl = strdup(originUrl);
    }
    
    mBgmUrl = NULL;
    if (bgmUrl) {
        mBgmUrl = strdup(bgmUrl);
    }
    
    mDubUrl = NULL;
    if (dubUrl) {
        mDubUrl = strdup(dubUrl);
    }
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);

    mAsyncPrepareEvent = NULL;
    mCalculateEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    
    mAsyncPrepareEvent = new DubbingScoreEvent(this, &DubbingScore::onPrepareAsyncEvent);
    mCalculateEvent = new DubbingScoreEvent(this, &DubbingScore::onCalculateEvent);
    mStopEvent = new DubbingScoreEvent(this, &DubbingScore::onStopEvent);
    mNotifyEvent = new DubbingScoreEvent(this, &DubbingScore::onNotifyEvent);
    mCalculateEventPending = false;
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();

    pthread_cond_init(&mStopCondition, NULL);

    mMediaListener = NULL;
    
    mOriginOutputer = NULL;
    
    mBgmOutputer = NULL;
    
    mDubWAVFile = NULL;
    mWorkAudioDubData = NULL;
    mWorkAudioDubDataSize = 0;
    
    mWorkSampleRate = 44100;
    mWorkSampleFormat = AV_SAMPLE_FMT_S16;
    mWorkChannelCount = 2;
    mSamplesPerFrame = KDefaultSamplesPerFrame;

    mScoreValue = 0;
    
    isReadyForDubAndBgm = false;
    
#ifdef DUBBING_SCORE_METHOD_1
    sum_sq_x = 0.0f;
    sum_sq_y = 0.0f;
    sum_coproduct = 0.0f;
    mean_x = 0.0f;
    mean_y = 0.0f;
    sweep = 0.0f;
    delta_x = 0.0f;
    delta_y = 0.0f;
    pop_sd_x = 0.0f;
    pop_sd_y = 0.0f;
    cov_x_y = 0.0f;
    
    index = 0;
    N = 0;
#endif
    
#ifdef DUBBING_SCORE_METHOD_2
    sumA = 0.0f;
    sumB = 0.0f;
    aveA = 0.0f;
    aveB = 0.0f;
    R1 = 0.0f;
    R2 = 0.0f;
    R3 = 0.0f;
    
    correlation_sum = 0.0f;
    correlation_num = 0;
#endif
    
#ifdef DUBBING_SCORE_METHOD_3
    p_dubbing_score = NULL;
#endif
    
    correlation = 0.0f;
    
//    srand((unsigned)time(NULL));
}
#else
DubbingScore::DubbingScore(char* originUrl, char* bgmUrl, char* dubUrl)
{
    init_ffmpeg_env();

    mOriginUrl = NULL;
    if (originUrl) {
        mOriginUrl = strdup(originUrl);
    }
    
    mBgmUrl = NULL;
    if (bgmUrl) {
        mBgmUrl = strdup(bgmUrl);
    }
    
    mDubUrl = NULL;
    if (dubUrl) {
        mDubUrl = strdup(dubUrl);
    }
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);

    mAsyncPrepareEvent = NULL;
    mCalculateEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    
    mAsyncPrepareEvent = new DubbingScoreEvent(this, &DubbingScore::onPrepareAsyncEvent);
    mCalculateEvent = new DubbingScoreEvent(this, &DubbingScore::onCalculateEvent);
    mStopEvent = new DubbingScoreEvent(this, &DubbingScore::onStopEvent);
    mNotifyEvent = new DubbingScoreEvent(this, &DubbingScore::onNotifyEvent);
    mCalculateEventPending = false;
    
    mQueue.start();

    pthread_cond_init(&mStopCondition, NULL);

    mMediaListener = NULL;
    
    mOriginOutputer = NULL;
    
    mBgmOutputer = NULL;
    
    mDubWAVFile = NULL;
    mWorkAudioDubData = NULL;
    mWorkAudioDubDataSize = 0;
    
    mWorkSampleRate = 44100;
    mWorkSampleFormat = AV_SAMPLE_FMT_S16;
    mWorkChannelCount = 2;
    mSamplesPerFrame = KDefaultSamplesPerFrame;
    
    mScoreValue = 0;
    
    isReadyForDubAndBgm = false;
    
#ifdef DUBBING_SCORE_METHOD_1
    sum_sq_x = 0.0f;
    sum_sq_y = 0.0f;
    sum_coproduct = 0.0f;
    mean_x = 0.0f;
    mean_y = 0.0f;
    sweep = 0.0f;
    delta_x = 0.0f;
    delta_y = 0.0f;
    pop_sd_x = 0.0f;
    pop_sd_y = 0.0f;
    cov_x_y = 0.0f;
    
    index = 0;
    N = 0;
#endif
    
#ifdef DUBBING_SCORE_METHOD_2
    sumA = 0.0f;
    sumB = 0.0f;
    aveA = 0.0f;
    aveB = 0.0f;
    R1 = 0.0f;
    R2 = 0.0f;
    R3 = 0.0f;
    
    correlation_sum = 0.0f;
    correlation_num = 0;
#endif
    
#ifdef DUBBING_SCORE_METHOD_3
    p_dubbing_score = NULL;
#endif
    
    correlation = 0.0f;
    
//    srand((unsigned)time(NULL));
}
#endif

DubbingScore::~DubbingScore()
{
    stop();

    mQueue.stop(true);

    mNotificationQueue.flush();

    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
    
    if (mCalculateEvent!=NULL) {
        delete mCalculateEvent;
        mCalculateEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    pthread_cond_destroy(&mStopCondition);
    
    pthread_mutex_destroy(&mLock);
    
#ifdef ANDROID
    JniMediaListener *jniListener = (JniMediaListener*)mMediaListener;
    if (jniListener!=NULL) {
        delete jniListener;
        jniListener = NULL;
    }
#else
    NormalMediaListener* normalMediaListener = (NormalMediaListener*)mMediaListener;
    if (normalMediaListener!=NULL) {
        delete normalMediaListener;
        normalMediaListener = NULL;
    }
#endif
    
    mMediaListener = NULL;
    
    if (mOriginUrl) {
        free(mOriginUrl);
        mOriginUrl = NULL;
    }
    
    if (mBgmUrl) {
        free(mBgmUrl);
        mBgmUrl = NULL;
    }
    
    if (mDubUrl) {
        free(mDubUrl);
        mDubUrl = NULL;
    }
}

#ifdef ANDROID
void DubbingScore::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    modifyFlags(INITIALIZED, SET);
}
#else
void DubbingScore::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new NormalMediaListener(listener,arg);
    
    modifyFlags(INITIALIZED, SET);
}
#endif

void DubbingScore::notifyListener_l(int event, int ext1, int ext2)
{
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void DubbingScore::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    postNotifyEvent_l();
}

void DubbingScore::postNotifyEvent_l()
{
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void DubbingScore::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
    
    switch (event) {
        default:
            break;
    }
}

void DubbingScore::start()
{
    AutoLock autoLock(&mLock);
    if (mFlags & CONNECTING) {
        LOGW("already connecting");
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    prepareAsync_l();
}

void DubbingScore::prepareAsync_l()
{
    modifyFlags(END, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(CONNECTING, SET);
        
    mQueue.postEvent(mAsyncPrepareEvent);
}

void DubbingScore::onPrepareAsyncEvent()
{
    AutoLock autoLock(&mLock);
    int ret = open_all_pipelines_l();
    
    if (ret>=0) {
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(CONNECTED, SET);
        
        play_l();
    }else{
        if (ret!=AVERROR_EXIT) {
            if (ret==-3) {
                notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_OPEN_DUB_FAIL);
            } else if (ret==-4) {
                notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_INIT_YPP_DUBBING_SCORE_METHOD_FAIL);
            } else {
                notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_UNKNOWN);
            }
        }
        
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(ERROR, SET);
        stop_l();
    }
}

void DubbingScore::pause()
{
    AutoLock autoLock(&mLock);
    return pause_l();
}

void DubbingScore::pause_l()
{
    if (!(mFlags & STREAMING)) {
        return;
    }
    
    modifyFlags(STREAMING, CLEAR);
    
    mQueue.cancelEvent(mCalculateEvent->eventID());
    mCalculateEventPending = false;
    
    modifyFlags(PAUSED, SET);
    
    return;
}

void DubbingScore::resume()
{
    AutoLock autoLock(&mLock);
    
    return play_l();
}

void DubbingScore::play_l()
{
    if (mFlags & STREAMING) {
        LOGW("%s","DubbingScore is processing");
        return;
    }
    
    if (!(mFlags & CONNECTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    modifyFlags(CONNECTED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    postCalculateEvent_l();
    
    modifyFlags(STREAMING, SET);
    
    notifyListener_l(DUBBING_SCORE_PROCESSING);
}

void DubbingScore::cancelScoreEvents()
{
    mQueue.cancelEvent(mCalculateEvent->eventID());
    mCalculateEventPending = false;
    mQueue.cancelEvent(mNotifyEvent->eventID());
    mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
    mQueue.cancelEvent(mStopEvent->eventID());
}

void DubbingScore::stop()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & END) {
        return;
    }
    
    if (mFlags & ENDING) {
        LOGW("already ending");
        
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
}

void DubbingScore::stop_l()
{
    modifyFlags(ENDING, ASSIGN);
    
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void DubbingScore::postCalculateEvent_l(int64_t delayUs)
{
    if (mCalculateEventPending) {
        return;
    }
    mCalculateEventPending = true;
    mQueue.postEventWithDelay(mCalculateEvent, delayUs < 0 ? 0 : delayUs);
}

void DubbingScore::onStopEvent()
{
    AutoLock autoLock(&mLock);
    
    close_all_pipelines_l();
    
    cancelScoreEvents();
    mNotificationQueue.flush();
        
    modifyFlags(END, ASSIGN);
    
    double score_factor = 6.0f;
#ifdef DUBBING_SCORE_METHOD_3
    score_factor = 1.0f;
#endif
    mScoreValue = generateScorevalueFromCorrelation(correlation*score_factor);
    notifyListener_l(DUBBING_SCORE_END, mScoreValue);

    pthread_cond_broadcast(&mStopCondition);
}

void DubbingScore::onCalculateEvent()
{
    AutoLock autoLock(&mLock);
    
    if (!mCalculateEventPending) {
        // The event has been cancelled in reset_l() but had already // been scheduled for execution at that time.
        return;
    }
    mCalculateEventPending = false;
    
    int ret = flowing_l();
    if (ret==1) {
        postCalculateEvent_l(0);
        return;
    }else if (ret==-7) {
        stop_l();
        return;
    }else if (ret<0) {
        modifyFlags(STREAMING, CLEAR);
        modifyFlags(ERROR, SET);
        
        if (ret!=AVERROR_EXIT) {
            if (ret==-1) {
                notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_BGM_ERROR);
            }else if (ret==-2) {
                notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_ORIGIN_ERROR);
            }else{
                notifyListener_l(DUBBING_SCORE_ERROR, DUBBING_SCORE_ERROR_UNKNOWN);
            }
        }
        
        stop_l();
        
        return;
    }else {
        int64_t waitTimeUs = 1*1000;
        postCalculateEvent_l(waitTimeUs);
        return;
    }
}

void DubbingScore::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

void DubbingScore::init_ffmpeg_env()
{
    // init ffmpeg env
    av_register_all();
    avcodec_register_all();
    avfilter_register_all();
    
    FFLog::setLogLevel(AV_LOG_WARNING);
}

// -3 : OPEN DUB FAIL
// -4 : INIT DUBBING_SCORE FROM YPP FAIL
int DubbingScore::open_all_pipelines_l()
{
    int ret = open_dub();
    if (ret<0) {
        close_dub();
        
        if (ret==-2) {
            return -4;
        }else {
            return -3;
        }
    }
    
    open_origin();
    open_bgm();
    
    return 0;
}

void DubbingScore::close_all_pipelines_l()
{
    close_origin();
    close_bgm();
    close_dub();
}

void DubbingScore::open_origin()
{
    AudioRenderConfigure audioRenderConfigure;
    audioRenderConfigure.channelCount = mWorkChannelCount;
    audioRenderConfigure.sampleFormat = mWorkSampleFormat;
    audioRenderConfigure.sampleRate = mWorkSampleRate;
    if (audioRenderConfigure.channelCount==1) {
        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_MONO;
    }else{
        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_STEREO;
    }
    audioRenderConfigure.samplesPerFrame = mSamplesPerFrame;
    
#ifdef ANDROID
    mOriginOutputer = new AudioPlayer(mJvm, AUDIO_PLAYER_EXTERNAL_RENDER, audioRenderConfigure);
#else
    mOriginOutputer = new AudioPlayer(AUDIO_PLAYER_EXTERNAL_RENDER, audioRenderConfigure);
#endif
    mOriginOutputer->setDataSource(mOriginUrl);
    mOriginOutputer->prepare();
    mOriginOutputer->play();
}

void DubbingScore::close_origin()
{
    if (mOriginOutputer) {
        mOriginOutputer->stop();
        delete mOriginOutputer;
        mOriginOutputer = NULL;
    }
}

void DubbingScore::open_bgm()
{
    AudioRenderConfigure audioRenderConfigure;
    audioRenderConfigure.channelCount = mWorkChannelCount;
    audioRenderConfigure.sampleFormat = mWorkSampleFormat;
    audioRenderConfigure.sampleRate = mWorkSampleRate;
    if (audioRenderConfigure.channelCount==1) {
        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_MONO;
    }else{
        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_STEREO;
    }
    audioRenderConfigure.samplesPerFrame = mSamplesPerFrame;
    
#ifdef ANDROID
    mBgmOutputer = new AudioPlayer(mJvm, AUDIO_PLAYER_EXTERNAL_RENDER, audioRenderConfigure);
#else
    mBgmOutputer = new AudioPlayer(AUDIO_PLAYER_EXTERNAL_RENDER, audioRenderConfigure);
#endif
    mBgmOutputer->setDataSource(mBgmUrl);
    mBgmOutputer->prepare();
    mBgmOutputer->play();
}

void DubbingScore::close_bgm()
{
    if (mBgmOutputer) {
        mBgmOutputer->stop();
        delete mBgmOutputer;
        mBgmOutputer = NULL;
    }
}

// -1 : OPEN DUB WAV FILE FAIL
// -2 : INIT DUBBING_SCORE FROM YPP FAIL
int DubbingScore::open_dub()
{
    mDubWAVFile = new WAVFileReader(mDubUrl);
    bool ret = mDubWAVFile->open();
    if (!ret) {
        delete mDubWAVFile;
        mDubWAVFile = NULL;
        
        LOGE("Open Dub WAV File Fail");
        return -1;
    }
    
    mWorkChannelCount = mDubWAVFile->getChannelCount();
    mWorkSampleRate = mDubWAVFile->getSampleRate();
    if (mDubWAVFile->getBitsPerSample()/8==1) {
        mWorkSampleFormat = AV_SAMPLE_FMT_U8;
    }else if (mDubWAVFile->getBitsPerSample()/8==2) {
        mWorkSampleFormat = AV_SAMPLE_FMT_S16;
    }else if (mDubWAVFile->getBitsPerSample()/8==4) {
        mWorkSampleFormat = AV_SAMPLE_FMT_S32;
    }else{
        mWorkSampleFormat = AV_SAMPLE_FMT_S16;
    }
    
#ifdef DUBBING_SCORE_METHOD_3
    p_dubbing_score = new dubbing_score(mWorkChannelCount,mWorkSampleRate);
    int error_code;
    p_dubbing_score->init(error_code);
    if (error_code == NoError) {
        mSamplesPerFrame = p_dubbing_score->getFrameSize();
    }else {
        delete p_dubbing_score;
        p_dubbing_score = NULL;
        
        if (mDubWAVFile) {
            mDubWAVFile->close();
            delete mDubWAVFile;
            mDubWAVFile = NULL;
        }
        
        return -2;
    }
#endif
    
    if(mWorkAudioDubData)
    {
        free(mWorkAudioDubData);
        mWorkAudioDubData = NULL;
    }
    if (mWorkAudioDubData==NULL) {
//        mWorkAudioDubDataSize = mWorkSampleRate*mWorkChannelCount*av_get_bytes_per_sample(mWorkSampleFormat)/10; //100ms
        mWorkAudioDubDataSize = mSamplesPerFrame * mWorkChannelCount * av_get_bytes_per_sample(mWorkSampleFormat);
        mWorkAudioDubData = (char*)malloc(mWorkAudioDubDataSize);
    }
    return 0;
}

void DubbingScore::close_dub()
{
    if(mWorkAudioDubData)
    {
        free(mWorkAudioDubData);
        mWorkAudioDubData = NULL;
    }
    mWorkAudioDubDataSize = 0;
    
#ifdef DUBBING_SCORE_METHOD_3
    if (p_dubbing_score) {
        delete p_dubbing_score;
        p_dubbing_score = NULL;
    }
#endif
    
    if (mDubWAVFile) {
        mDubWAVFile->close();
        delete mDubWAVFile;
        mDubWAVFile = NULL;
    }
}

// 1 : JUST LOOP
// 0 : WAIT THEN LOOP
// -1 : BGM ERROR
// -2 : ORIGIN ERROR
//-7 : EOF
int DubbingScore::flowing_l()
{
    if (!isReadyForDubAndBgm) {
        //Bgm
        AudioPlayerOutput *bgmOutput = mBgmOutputer->drainFromExternal();
        if (bgmOutput==NULL) {
            return 0;
        }
        if (bgmOutput->type==AUDIO_PLAYER_OUTPUT_ERROR) {
            bgmOutput->Free();
            delete bgmOutput;
            bgmOutput = NULL;
            
#ifdef DUBBING_SCORE_METHOD_1
            N = index;
            if (N!=0) {
                pop_sd_x=sqrt(sum_sq_x/N);
                pop_sd_y=sqrt(sum_sq_y/N);
                cov_x_y=sum_coproduct/N;
                if (pop_sd_x!=0 && pop_sd_y!=0) {
                    correlation=cov_x_y/(pop_sd_x*pop_sd_y);
                    if (correlation<0) {
                        correlation = -correlation;
                    }
                }else {
                    correlation = 0.0f;
                }
            }else{
                correlation = 0.0f;
            }
#endif
            
#ifdef DUBBING_SCORE_METHOD_2
            if (correlation_num!=0) {
                correlation = correlation_sum / correlation_num;
                if (correlation<0) {
                    correlation = -correlation;
                }
            }
#endif
            
#ifdef DUBBING_SCORE_METHOD_3
            if (p_dubbing_score) {
                int dubbing_score_result = p_dubbing_score->getDubbingScore();
                LOGD("ypp dubbing score result : %f",(float)dubbing_score_result/10.0f);
                correlation = (double)dubbing_score_result/1000.0f;
            }
#endif
            
            return -1;
        }
        if (bgmOutput->type==AUDIO_PLAYER_OUTPUT_EOF) {
            bgmOutput->Free();
            delete bgmOutput;
            bgmOutput = NULL;
            
#ifdef DUBBING_SCORE_METHOD_1
            N = index;
            if (N!=0) {
                pop_sd_x=sqrt(sum_sq_x/N);
                pop_sd_y=sqrt(sum_sq_y/N);
                cov_x_y=sum_coproduct/N;
                if (pop_sd_x!=0 && pop_sd_y!=0) {
                    correlation=cov_x_y/(pop_sd_x*pop_sd_y);
                    if (correlation<0) {
                        correlation = -correlation;
                    }
                }else {
                    correlation = 0.0f;
                }
            }else{
                correlation = 0.0f;
            }
#endif
            
#ifdef DUBBING_SCORE_METHOD_2
            if (correlation_num!=0) {
                correlation = correlation_sum / correlation_num;
                if (correlation<0) {
                    correlation = -correlation;
                }
            }
#endif
            
#ifdef DUBBING_SCORE_METHOD_3
            if (p_dubbing_score) {
                int dubbing_score_result = p_dubbing_score->getDubbingScore();
                LOGD("ypp dubbing score result : %f",(float)dubbing_score_result/10.0f);
                correlation = (double)dubbing_score_result/1000.0f;
            }
#endif
            
            return -7;
        }
        
        AudioPlayerOutputPcm* bgmOutputPcm = (AudioPlayerOutputPcm*)bgmOutput->output;
        
        //Dub
        mDubWAVFile->getPcmData(&mWorkAudioDubData, mWorkAudioDubDataSize);
        
        //Mix
        mixSamples((int16_t *)(mWorkAudioDubData), (int16_t *)(bgmOutputPcm->data), (int)(mWorkAudioDubDataSize/2));
        
        bgmOutput->Free();
        delete bgmOutput;
        bgmOutput = NULL;
        
        isReadyForDubAndBgm = true;
    }

    //Origin
    AudioPlayerOutput* originOutput = mOriginOutputer->drainFromExternal();
    if (originOutput==NULL) {
        return 0;
    }
    if (originOutput->type==AUDIO_PLAYER_OUTPUT_ERROR) {
        originOutput->Free();
        delete originOutput;
        originOutput = NULL;
        
#ifdef DUBBING_SCORE_METHOD_1
        N = index;
        if (N!=0) {
            pop_sd_x=sqrt(sum_sq_x/N);
            pop_sd_y=sqrt(sum_sq_y/N);
            cov_x_y=sum_coproduct/N;
            if (pop_sd_x!=0 && pop_sd_y!=0) {
                correlation=cov_x_y/(pop_sd_x*pop_sd_y);
                if (correlation<0) {
                    correlation = -correlation;
                }
            }else {
                correlation = 0.0f;
            }
        }else{
            correlation = 0.0f;
        }
#endif
        
#ifdef DUBBING_SCORE_METHOD_2
        if (correlation_num!=0) {
            correlation = correlation_sum / correlation_num;
            if (correlation<0) {
                correlation = -correlation;
            }
        }
#endif
        
#ifdef DUBBING_SCORE_METHOD_3
        if (p_dubbing_score) {
            int dubbing_score_result = p_dubbing_score->getDubbingScore();
            LOGD("ypp dubbing score result : %f",(float)dubbing_score_result/10.0f);
            correlation = (double)dubbing_score_result/1000.0f;
        }
#endif
        
        return -2;
    }
    if (originOutput->type==AUDIO_PLAYER_OUTPUT_EOF) {
        originOutput->Free();
        delete originOutput;
        originOutput = NULL;
        
#ifdef DUBBING_SCORE_METHOD_1
        N = index;
        if (N!=0) {
            pop_sd_x=sqrt(sum_sq_x/N);
            pop_sd_y=sqrt(sum_sq_y/N);
            cov_x_y=sum_coproduct/N;
            if (pop_sd_x!=0 && pop_sd_y!=0) {
                correlation=cov_x_y/(pop_sd_x*pop_sd_y);
                if (correlation<0) {
                    correlation = -correlation;
                }
            }else {
                correlation = 0.0f;
            }
        }else{
            correlation = 0.0f;
        }
#endif

#ifdef DUBBING_SCORE_METHOD_2
        if (correlation_num!=0) {
            correlation = correlation_sum / correlation_num;
            if (correlation<0) {
                correlation = -correlation;
            }
        }
#endif
        
#ifdef DUBBING_SCORE_METHOD_3
        if (p_dubbing_score) {
            int dubbing_score_result = p_dubbing_score->getDubbingScore();
            LOGD("ypp dubbing score result : %f",(float)dubbing_score_result/10.0f);
            correlation = (double)dubbing_score_result/1000.0f;
        }
#endif
        
        return -7;
    }
    
    AudioPlayerOutputPcm* originOutputPcm = (AudioPlayerOutputPcm*)originOutput->output;
    
    int16_t* X = (int16_t*)(originOutputPcm->data);
    int16_t* Y = (int16_t*)mWorkAudioDubData;
    
#ifdef DUBBING_SCORE_METHOD_1
    for (int k = 0; k<mWorkAudioDubDataSize/2; k++) {
        if (index==0) {
            mean_x = (double)X[0];
            mean_y = (double)Y[0];
            index = 1;
        }else{
            index++;
            sweep=(index-1.0)/index;
            delta_x=(double)X[k]-mean_x;
            delta_y=(double)Y[k]-mean_y;
            sum_sq_x+=delta_x*delta_x*sweep;
            sum_sq_y+=delta_y*delta_y*sweep;
            sum_coproduct+=delta_x*delta_y*sweep;
            mean_x+=delta_x/index;
            mean_y+=delta_y/index;
        }
    }
#endif
    
#ifdef DUBBING_SCORE_METHOD_2
    sumA = 0.0f;
    sumB = 0.0f;
    for (int i=0; i<mWorkAudioDubDataSize/2; i++) {
        sumA += (double)X[i];
        sumB += (double)Y[i];
    }
    aveA = sumA/(mWorkAudioDubDataSize/2);
    aveB = sumB/(mWorkAudioDubDataSize/2);
    R1 = 0.0f;
    R2 = 0.0f;
    R3 = 0.0f;
    for (int i = 0; i<mWorkAudioDubDataSize/2; i++) {
        R1 += (X[i] - aveA) * (Y[i] - aveB);
        R2 += pow((X[i] - aveA), 2);
        R3 += pow((Y[i] - aveB), 2);
    }
    double numerator = sqrt(R2*R3);
    if (numerator!=0) {
        double result = R1/numerator;
        if (result<0) {
            result = -result;
        }
        correlation_sum += result;
        correlation_num++;
    }
#endif
    
#ifdef DUBBING_SCORE_METHOD_3
    if (p_dubbing_score) {
        p_dubbing_score->prepareFrameScore((char*)originOutputPcm->data, mWorkAudioDubData, mSamplesPerFrame);
    }
#endif
    
    originOutput->Free();
    delete originOutput;
    originOutput = NULL;
    
    isReadyForDubAndBgm = false;
    
    return 1;
}

//要取得[a,b)的随机整数，使用(rand() % (b-a))+ a;
//要取得[a,b]的随机整数，使用(rand() % (b-a+1))+ a;
//要取得(a,b]的随机整数，使用(rand() % (b-a))+ a + 1;
int DubbingScore::generateScorevalueFromCorrelation(double correlationValue)
{
    int ret = 1000*correlationValue;
    if (ret<600) {
        ret = 600;
    }
    
    if (ret>1000) {
        ret = 1000;
    }
    
    return ret;
    
//    if (correlationValue<=0.3f) {
//        return (rand() % (75-60))+ 60 +1;
//    }else if(correlationValue>0.3f && correlationValue<=0.5f) {
//        return (rand() % (80-75))+ 75 +1;
//    }else if(correlationValue>0.5f && correlationValue<=0.75f) {
//        return (rand() % (85-80))+ 80 +1;
//    }else if(correlationValue>0.75f && correlationValue<=0.85f) {
//        return (rand() % (88-84))+ 84 +1;
//    }else if(correlationValue>0.85f && correlationValue<=0.90f) {
//        return (rand() % (90-86))+ 86 +1;
//    }else if(correlationValue>0.90f && correlationValue<=0.95f) {
//        return (rand() % (94-88))+ 88 +1;
//    }else if(correlationValue>0.95f && correlationValue<=0.99f) {
//        return (rand() % (98-92))+ 92 +1;
//    }else if(correlationValue>0.99f && correlationValue<=1.0f) {
//        return (rand() % (99-97))+ 97 +1;
//    }else {
//        return (rand() % (100-60))+ 60;
//    }
}
