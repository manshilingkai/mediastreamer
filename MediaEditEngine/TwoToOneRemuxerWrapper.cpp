//
//  TwoToOneRemuxerWrapper.cpp
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/18.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "TwoToOneRemuxerWrapper.h"
#include "TwoToOneRemuxer.h"

#ifdef __cplusplus
extern "C" {
#endif

struct TwoToOneRemuxerWrapper
{
    TwoToOneRemuxer* twoToOneRemuxer;
    
    TwoToOneRemuxerWrapper()
    {
        twoToOneRemuxer = NULL;
    }
};

struct TwoToOneRemuxerWrapper* TwoToOneRemuxerWrapper_GetInstance(TTORVideoMaterial videoMaterial, TTORAudioMaterial audioMaterial, TTORProduct product)
{
    TwoToOneRemuxerWrapper* pInstance = new TwoToOneRemuxerWrapper;
    pInstance->twoToOneRemuxer = new TwoToOneRemuxer(videoMaterial, audioMaterial, product);
    return pInstance;
}

void TwoToOneRemuxerWrapper_ReleaseInstance(struct TwoToOneRemuxerWrapper **ppInstance)
{
    TwoToOneRemuxerWrapper* pInstance  = *ppInstance;
    if (pInstance!=NULL) {
        if (pInstance->twoToOneRemuxer!=NULL) {
            delete pInstance->twoToOneRemuxer;
            pInstance->twoToOneRemuxer = NULL;
        }
        
        delete pInstance;
        pInstance = NULL;
    }
}

void TwoToOneRemuxerWrapper_setListener(struct TwoToOneRemuxerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner)
{
    if (pInstance!=NULL && pInstance->twoToOneRemuxer!=NULL) {
        pInstance->twoToOneRemuxer->setListener(listener, owner);
    }
}

void TwoToOneRemuxerWrapper_start(struct TwoToOneRemuxerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->twoToOneRemuxer!=NULL) {
        pInstance->twoToOneRemuxer->start();
    }
}

void TwoToOneRemuxerWrapper_resume(struct TwoToOneRemuxerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->twoToOneRemuxer!=NULL) {
        pInstance->twoToOneRemuxer->resume();
    }
}

void TwoToOneRemuxerWrapper_pause(struct TwoToOneRemuxerWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->twoToOneRemuxer!=NULL) {
        pInstance->twoToOneRemuxer->pause();
    }
}

void TwoToOneRemuxerWrapper_stop(struct TwoToOneRemuxerWrapper *pInstance, bool isCancel)
{
    if (pInstance!=NULL && pInstance->twoToOneRemuxer!=NULL) {
        pInstance->twoToOneRemuxer->stop(isCancel);
    }
}

#ifdef __cplusplus
};
#endif
