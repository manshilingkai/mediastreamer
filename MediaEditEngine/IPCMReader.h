//
//  IPCMReader.h
//  MediaStreamer
//
//  Created by Think on 2020/2/3.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef IPCMReader_h
#define IPCMReader_h

#include <stdio.h>

enum PCMReaderType
{
    UNKNOWN_PCM_READER_TYPE = -1,
    WAV_FILE_READER = 0,
    FF_AUDIO_FILE_READER = 1,
};

class IPCMReader {
public:
    virtual ~IPCMReader() {}
    
    static IPCMReader* CreatePCMReader(PCMReaderType type, char* audioFilePath);
    static void DeletePCMReader(IPCMReader* iPCMReader, PCMReaderType type);
    
    virtual bool open() = 0;
    
    virtual void setVolume(float volume) = 0;
    
    virtual int getChannelCount() = 0;
    virtual int getSampleRate() = 0;
    virtual int getBitsPerSample() = 0;
    
    virtual int getDurationMs() = 0;
    
    virtual int getPcmData(char **pData, long size) = 0;
    
    virtual void close() = 0;
};

#endif /* IPCMReader_h */
