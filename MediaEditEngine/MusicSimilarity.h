//
//  MusicSimilarity.h
//  MediaStreamer
//
//  Created by Think on 2019/10/9.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef MusicSimilarity_h
#define MusicSimilarity_h

#include <stdio.h>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
}

#include "TimedEventQueue.h"
#include "IMediaListener.h"
#include "NotificationQueue.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include "MediaFrameOutputer.h"
#include "WAVFile.h"

#include "AudioSimilarity.h"

#define MUSLY_SUPPORT_STDIO
#include "musly/musly.h"

class MusicSimilarity : public AudioSimilarity, IMediaListener {
public:
#ifdef ANDROID
    MusicSimilarity(JavaVM *jvm, char* originUrl, char* bgmUrl, char* dubUrl, char* workDir);
#else
    MusicSimilarity(char* originUrl, char* bgmUrl, char* dubUrl, char* workDir);
#endif
    ~MusicSimilarity();

#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#else
    void setListener(void (*listener)(void*,int,int,int), void* arg);
#endif
    
    void start();
    void resume();
    void pause();
    void stop();
    
    void notify(int event, int ext1, int ext2);
private:
    enum {
        CONNECTING           = 0x01,
        CONNECTED            = 0x02,
        STREAMING            = 0x04,
        PAUSED               = 0x10,
        ERROR                = 0x20,
        END                  = 0x40,
        INITIALIZED          = 0x80,
        ENDING               = 0x100,
    };
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
private:
    friend struct MusicSimilarityEvent;

    MusicSimilarity(const MusicSimilarity &);
    MusicSimilarity &operator=(const MusicSimilarity &);
    
    pthread_mutex_t mLock;

    TimedEventQueue mQueue;

    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mCalculateEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mNotifyEvent;
    bool mCalculateEventPending;
    
    void prepareAsync_l();
    void play_l();
    void pause_l();
    void stop_l();
    
    void postCalculateEvent_l(int64_t delayUs = -1);
    void postNotifyEvent_l();
    
    void onPrepareAsyncEvent();
    void onCalculateEvent();
    void onStopEvent();
    void onNotifyEvent();
    
    void cancelMusicSimilarityEvents();

    pthread_cond_t mStopCondition;

    NotificationQueue mNotificationQueue;
private:
    int open_all_pipelines_l();
    void close_all_pipelines_l();
    int flowing_l();
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    char* mOriginUrl;
    char* mBgmUrl;
    char* mDubUrl;
    char* mWorkDir;
    IMediaListener* mMediaListener;
private:
    static void init_ffmpeg_env();
private:
    MediaFrameOutputer* mMediaFrameOutputer;
    int open_input_audio_bgm_material();
    void close_input_audio_bgm_material();
    
    //filter_spec = "anull";
    int open_audio_filter(AudioContext audioContext, int workSampleRate, AVSampleFormat workSampleFormat, int workChannelCount, uint64_t workChannelLayout, const char *filter_spec);
    void close_audio_filter();
    
    AVFilterContext *audio_buffersrc_ctx;
    AVFilterContext *audio_buffersink_ctx;
    AVFilterGraph *audio_filter_graph;
private:
    WAVFileReader *mDubWAVFile;
    int open_input_audio_dub_material();
    void close_input_audio_dub_material();
    char* mWorkAudioDubData;
    long mWorkAudioDubDataSize;
private:
    WAVFileWriter *mOutputWAVFile;
    int open_output_media_product();
    void close_output_media_product();
    char* mOutputWAVFilePath;
private:
    int mWorkSampleRate;
    AVSampleFormat mWorkSampleFormat;
    int mWorkChannelCount;
    uint64_t mWorkChannelLayout;
//Musly
private:
    void calculateSimilarityWithMusly();
    float similarity;
    int generateScorevalueFromSimilarity();
};

#endif /* MusicSimilarity_h */
