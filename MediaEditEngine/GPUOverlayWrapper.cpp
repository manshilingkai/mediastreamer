//
//  GPUOverlayWrapper.cpp
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/30.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "GPUOverlayWrapper.h"
#include "GPUOverlay.h"

#ifdef __cplusplus
extern "C" {
#endif

struct GPUOverlayWrapper
{
    GPUOverlay* gpuOverlay;
    
    GPUOverlayWrapper()
    {
        gpuOverlay = NULL;
    }
};

struct GPUOverlayWrapper *GPUOverlay_GetInstance()
{
    GPUOverlayWrapper* pInstance = new GPUOverlayWrapper;
    pInstance->gpuOverlay = new GPUOverlay();
    return pInstance;
}

void GPUOverlay_ReleaseInstance(struct GPUOverlayWrapper **ppInstance)
{
    GPUOverlayWrapper* pInstance  = *ppInstance;
    if (pInstance!=NULL) {
        if (pInstance->gpuOverlay!=NULL) {
            delete pInstance->gpuOverlay;
            pInstance->gpuOverlay = NULL;
        }
        
        delete pInstance;
        pInstance = NULL;
    }
}

void GPUOverlay_initialize(struct GPUOverlayWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->gpuOverlay!=NULL) {
        pInstance->gpuOverlay->initialize();
    }
}

void GPUOverlay_terminate(struct GPUOverlayWrapper *pInstance)
{
    if (pInstance!=NULL && pInstance->gpuOverlay!=NULL) {
        pInstance->gpuOverlay->terminate();
    }
}

void GPUOverlay_setOverlay(struct GPUOverlayWrapper *pInstance, char* imageUrl)
{
    if (pInstance!=NULL && pInstance->gpuOverlay!=NULL) {
        pInstance->gpuOverlay->setOverlay(imageUrl);
    }
}

bool GPUOverlay_blendTo(struct GPUOverlayWrapper *pInstance, int mainTextureId, int mainWidth, int mainHeight, int x, int y, int overlayRotation, float overlayScale, bool overlayFlipHorizontal, bool overlayFlipVertical, int outputTextureId)
{
    if (pInstance!=NULL && pInstance->gpuOverlay!=NULL) {
        return pInstance->gpuOverlay->blendTo(mainTextureId, mainWidth, mainHeight, x, y, overlayRotation, overlayScale, overlayFlipHorizontal, overlayFlipVertical, outputTextureId);
    }
    
    return false;
}

#ifdef __cplusplus
};
#endif
