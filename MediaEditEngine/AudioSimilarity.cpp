//
//  AudioSimilarity.cpp
//  MediaStreamer
//
//  Created by Think on 2019/10/9.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "AudioSimilarity.h"
#include "DubbingScore.h"
#include "MusicSimilarity.h"

#ifdef ANDROID
AudioSimilarity* AudioSimilarity::CreateAudioSimilarity(JavaVM *jvm, AUDIO_SIMILARITY_ALGORITHM algorithm, char* originUrl, char* bgmUrl, char* dubUrl, char* workDir)
{
    if (algorithm==CORRELATION_COEFFICIENT) {
        return new DubbingScore(jvm, originUrl, bgmUrl, dubUrl);
    }else if(algorithm==MUSLY) {
        return new MusicSimilarity(jvm, originUrl, bgmUrl, dubUrl, workDir);
    }
    
    return NULL;
}
#else
AudioSimilarity* AudioSimilarity::CreateAudioSimilarity(AUDIO_SIMILARITY_ALGORITHM algorithm, char* originUrl, char* bgmUrl, char* dubUrl, char* workDir)
{
    if (algorithm==CORRELATION_COEFFICIENT) {
        return new DubbingScore(originUrl, bgmUrl, dubUrl);
    }else if(algorithm==MUSLY) {
        return new MusicSimilarity(originUrl, bgmUrl, dubUrl, workDir);
    }
    
    return NULL;
}
#endif

void AudioSimilarity::DeleteAudioSimilarity(AUDIO_SIMILARITY_ALGORITHM algorithm, AudioSimilarity* audioSimilarity)
{
    if (algorithm==CORRELATION_COEFFICIENT) {
        DubbingScore* dubbingScore = (DubbingScore*)audioSimilarity;
        if (dubbingScore) {
            delete dubbingScore;
            dubbingScore = NULL;
        }
    }else if (algorithm==MUSLY) {
        MusicSimilarity* musicSimilarity = (MusicSimilarity*)audioSimilarity;
        if (musicSimilarity) {
            delete musicSimilarity;
            musicSimilarity = NULL;
        }
    }
}
