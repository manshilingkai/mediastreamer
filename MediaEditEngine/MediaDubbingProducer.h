//
//  MediaDubbingProducer.h
//  MediaStreamer
//
//  Created by Think on 2019/7/17.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef MediaDubbingProducer_h
#define MediaDubbingProducer_h

#include <stdio.h>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
}

#include "TimedEventQueue.h"
#include "IMediaListener.h"
#include "NotificationQueue.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include "MediaFrameOutputer.h"
#include "IPCMReader.h"

struct MediaDubbingVideoOptions {
    char* videoUrl;
    
    MediaDubbingVideoOptions()
    {
        videoUrl = NULL;
    }
};

struct MediaDubbingBGMOptions {
    char* bgmUrl;
    float bgmVolume;
    
    MediaDubbingBGMOptions()
    {
        bgmUrl = NULL;
        bgmVolume = 1.0f;
    }
};

struct MediaDubbingDubOptions {
    char* dubUrl;
    float dubVolume;
    
    MediaDubbingDubOptions()
    {
        dubUrl = NULL;
        dubVolume = 1.0f;
    }
};

struct MediaDubbingProductOptions {
    char* productUrl;
    
    AVSampleFormat audioSampleFormat;
    int audioSampleRate;
    int audioNumChannels;
    int audioBitrateKbps;
    
    MediaDubbingProductOptions()
    {
        productUrl = NULL;
        
        audioSampleFormat = AV_SAMPLE_FMT_S16;
        audioSampleRate = 44100;
        audioNumChannels = 2;
        audioBitrateKbps = 96*audioNumChannels;
    }
};

class MediaDubbingProducer : public IMediaListener{
public:
#ifdef ANDROID
    MediaDubbingProducer(JavaVM *jvm, MediaDubbingVideoOptions videoOptions, MediaDubbingBGMOptions bgmOptions, MediaDubbingDubOptions dubOptions, MediaDubbingProductOptions productOptions);
#else
    MediaDubbingProducer(MediaDubbingVideoOptions videoOptions, MediaDubbingBGMOptions bgmOptions, MediaDubbingDubOptions dubOptions, MediaDubbingProductOptions productOptions);
#endif
    ~MediaDubbingProducer();
    
#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#else
    void setListener(void (*listener)(void*,int,int,int), void* arg);
#endif
    
    void start();
    void resume();
    void pause();
    void stop(bool isCancle = false);
    
    void notify(int event, int ext1, int ext2);
private:
    enum {
        CONNECTING           = 0x01,
        CONNECTED            = 0x02,
        STREAMING            = 0x04,
        PAUSED               = 0x10,
        ERROR                = 0x20,
        END                  = 0x40,
        INITIALIZED          = 0x80,
        ENDING               = 0x100,
    };
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
private:
    friend struct MediaDubbingProducerEvent;

    MediaDubbingProducer(const MediaDubbingProducer &);
    MediaDubbingProducer &operator=(const MediaDubbingProducer &);
    
    pthread_mutex_t mLock;

    TimedEventQueue mQueue;

    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mEncodeEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mNotifyEvent;
    bool mEncodeEventPending;
    
    void prepareAsync_l();
    void play_l();
    void pause_l();
    void stop_l();
    
    void postEncodeEvent_l(int64_t delayUs = -1);
    void postNotifyEvent_l();
    
    void onPrepareAsyncEvent();
    void onEncodeEvent();
    void onStopEvent();
    void onNotifyEvent();
    
    void cancelProducerEvents();

    pthread_cond_t mStopCondition;

    NotificationQueue mNotificationQueue;

private:
    int open_all_pipelines_l();
    void close_all_pipelines_l();
    int flowing_l();
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
    
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    MediaDubbingVideoOptions mVideoOptions;
    MediaDubbingBGMOptions mBgmOptions;
    MediaDubbingDubOptions mDubOptions;
    MediaDubbingProductOptions mProductOptions;
    
    IMediaListener* mMediaListener;
private:
    bool gotError;
private:
    static void init_ffmpeg_env();
private:
    int open_input_video_material();
    void close_input_video_material();
    AVFormatContext *ifmt_ctx;
    int mInputVideoStreamIndex;
    int mInputVideoMaterialDurationSecond;
private:
    MediaFrameOutputer* mMediaFrameOutputer;
    int open_input_audio_bgm_material();
    void close_input_audio_bgm_material();
    int mInputAudioBgmMaterialDurationSecond;

    //filter_spec = "anull";
    int open_audio_filter(AudioContext audioContext, AVCodecContext* audio_enc_ctx, const char *filter_spec);
    void close_audio_filter();
    
    AVFilterContext *audio_buffersrc_ctx;
    AVFilterContext *audio_buffersink_ctx;
    AVFilterGraph *audio_filter_graph;
    
    AVAudioFifo* filteredAudioDataFifo;
private:
    IPCMReader *mDubPCMReader;
    PCMReaderType mDubPCMReaderType;
    int open_input_audio_dub_material();
    void close_input_audio_dub_material();
    char* mWorkAudioDubData;
    long mWorkAudioDubDataSize;
private:
    int open_output_media_product();
    void close_output_media_product();
    AVFormatContext *ofmt_ctx;
    int mOutputAudioStreamIndex;
    int mOutputVideoStreamIndex;
    AVCodecContext *audio_encoder_ctx;
private:
    bool GotBaselineForWriteAudioPacket;
    int64_t baseLineForWriteAudioPacket;
    int64_t mCurrentWriteAudioPacketPtsUs;
    
    bool GotWriteAudioPacketEof;
private:
    int flowing_remux_video_l(int64_t currentWriteAudioPacketPtsUs = 0, bool isRemuxAll = false);
    
    bool GotBaselineForWriteVideoPacket;
    int64_t baseLineForWriteVideoPacket;
    int64_t mCurrentWriteVideoPacketPtsUs;
    
    bool GotWriteVideoPacketEof;
private:
    int mOutputMediaProductDurationSecond;
    int writeTimeStampSecond;
    void reportWriteProgress(int64_t currentWriteAVPacketPtsUs);
private:
    bool hasVideoMaterial;
private:
    bool hasAudioDubMaterial;
};

#endif /* MediaDubbingProducer_h */
