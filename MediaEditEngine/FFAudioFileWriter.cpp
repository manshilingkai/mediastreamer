//
//  FFAudioFileWriter.cpp
//  MediaStreamer
//
//  Created by Think on 2019/9/4.
//  Copyright © 2019 Cell. All rights reserved.
//

#include "FFAudioFileWriter.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
#include "libavutil/avstring.h"
}

FFAudioFileWriter::FFAudioFileWriter(int channels, int sample_rate, char* audioFilePath)
{
    mChannels = channels;
    mSampleRate = sample_rate;
    if (audioFilePath) {
        mAudioFilePath = strdup(audioFilePath);
    }
    
    mMediaTranscoder = NULL;
    pthread_cond_init(&mCondition, NULL);
    pthread_mutex_init(&mLock, NULL);
    mOpenRet = 0;
    
    pts = 0;
}

FFAudioFileWriter::~FFAudioFileWriter()
{
    close();
    
    if (mAudioFilePath) {
        free(mAudioFilePath);
        mAudioFilePath = NULL;
    }
    
    pthread_cond_destroy(&mCondition);
    pthread_mutex_destroy(&mLock);
}

bool FFAudioFileWriter::open()
{
    MediaMaterial input;
    MediaProduct product;
    
    input.isExternalMediaFrame = true;
    input.hasAudio = true;
    input.sampleRate = mSampleRate;
    input.numChannels = mChannels;
    
    product.type = MEDIA_PRODUCT_TYPE_M4A;
    product.url = mAudioFilePath;
    product.videoOptions.hasVideoTrack = false;
    product.audioOptions.hasAudioTrack = true;

    mMediaTranscoder = MediaTranscoder::CreateMediaTranscoder(MEDIA_TRANSCODER_TYPE_DEFAULT, NULL, input, NULL, product);
    mMediaTranscoder->setListener(onMediaTranscoderListener, this);
    mMediaTranscoder->start();

    int64_t timeout = 5000 * 1000 * 1000ll;
    int64_t current = 0;
    while (true) {
        pthread_mutex_lock(&mLock);
        if (mOpenRet==-1) {
            pthread_mutex_unlock(&mLock);
            
            mMediaTranscoder->stop();
            MediaTranscoder::DeleteMediaTranscoder(MEDIA_TRANSCODER_TYPE_DEFAULT, mMediaTranscoder);
            mMediaTranscoder = NULL;
            
            return false;
        }else if (mOpenRet==1) {
            pthread_mutex_unlock(&mLock);
            return true;
        }
        
        int64_t reltime = 100 * 1000 * 1000ll;
        struct timespec ts;
#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
        struct timeval t;
        t.tv_sec = t.tv_usec = 0;
        gettimeofday(&t, NULL);
        ts.tv_sec = t.tv_sec;
        ts.tv_nsec = t.tv_usec * 1000;
        ts.tv_sec += reltime/1000000000;
        ts.tv_nsec += reltime%1000000000;
        ts.tv_sec += ts.tv_nsec / 1000000000;
        ts.tv_nsec = ts.tv_nsec % 1000000000;
        pthread_cond_timedwait(&mCondition, &mLock, &ts);
#else
        ts.tv_sec  = reltime/1000000000;
        ts.tv_nsec = reltime%1000000000;
        pthread_cond_timedwait_relative_np(&mCondition, &mLock, &ts);
#endif
        pthread_mutex_unlock(&mLock);
        
        current += reltime;
        
        if (current>timeout) {
            mMediaTranscoder->stop();
            MediaTranscoder::DeleteMediaTranscoder(MEDIA_TRANSCODER_TYPE_DEFAULT, mMediaTranscoder);
            mMediaTranscoder = NULL;
            
            return false;
        }
        
        continue;
    }
}

void FFAudioFileWriter::onMediaTranscoderListener(void* owner, int event, int ext1, int ext2)
{
    FFAudioFileWriter* ffAudioFileWriter = (FFAudioFileWriter*)owner;
    if (ffAudioFileWriter) {
        ffAudioFileWriter->handleMediaTranscoderListener(event, ext1, ext2);
    }
}

void FFAudioFileWriter::handleMediaTranscoderListener(int event, int ext1, int ext2)
{
    if (event == MEDIA_TRANSCODER_ERROR && ext1 == MEDIA_TRANSCODER_ERROR_CONNECT_FAIL) {
        pthread_mutex_lock(&mLock);
        mOpenRet = -1;
        pthread_mutex_unlock(&mLock);
        
        pthread_cond_broadcast(&mCondition);
    }
    
    if (event == MEDIA_TRANSCODER_CONNECTED) {
        pthread_mutex_lock(&mLock);
        mOpenRet = 1;
        pthread_mutex_unlock(&mLock);
        
        pthread_cond_broadcast(&mCondition);
    }
}

void FFAudioFileWriter::putPcmData(char* data, int size)
{
    if (data==NULL || size<=0) return;
    
    AVFrame* audioFrame = av_frame_alloc();
    audioFrame->channels = mChannels;
    audioFrame->channel_layout = av_get_default_channel_layout(audioFrame->channels);
    audioFrame->format = AV_SAMPLE_FMT_S16;
    audioFrame->nb_samples = size/mChannels/2;
    int ret = av_frame_get_buffer(audioFrame, 0);
    if (ret!=0) {
        av_frame_free(&audioFrame);
        LOGE("av_frame_get_buffer Fail");
        return;
    }
    memcpy(audioFrame->data[0], data, audioFrame->nb_samples*2*mChannels);
    
    pts += audioFrame->nb_samples*1000*1000/mSampleRate;
    
    MediaFrame* mediaFrame = new MediaFrame;
    mediaFrame->avFrame = audioFrame;
    mediaFrame->type = MEDIA_FRAME_TYPE_AUDIO;
    mediaFrame->pts = pts;
    
    if (mMediaTranscoder) {
        mMediaTranscoder->pushMediaFrameFromExternal(mediaFrame);
    }
}

int32_t FFAudioFileWriter::getWriteTimeMs()
{
    return int32_t(pts/1000);
}

void FFAudioFileWriter::close()
{
    if (mMediaTranscoder) {
        mMediaTranscoder->stop();
        MediaTranscoder::DeleteMediaTranscoder(MEDIA_TRANSCODER_TYPE_DEFAULT, mMediaTranscoder);
        mMediaTranscoder = NULL;
    }
}
