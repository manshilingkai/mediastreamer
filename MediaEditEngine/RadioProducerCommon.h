//
//  RadioProducerCommon.h
//  MediaStreamer
//
//  Created by Think on 2019/10/23.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef RadioProducerCommon_h
#define RadioProducerCommon_h

enum RADIO_PRODUCT_TYPE {
    RAW_PCM = 0,
    WAV = 1,
    M4A = 2,
    MP3 = 3,
};

struct RadioProductOptions {
    int type;
    
    char* productUrl;
    char* bgProductUrl;
    
    int outWaveValueIntervalMs;
    
    float defaultBgmVolume;
    float defaultMicVolume;
    float defaultEffectMusicVolume;
    
    RadioProductOptions()
    {
        type = WAV;
        
        productUrl = NULL;
        bgProductUrl = NULL;
        
        outWaveValueIntervalMs = 200;
        
        defaultBgmVolume = 1.0f;
        defaultMicVolume = 1.2f;
        defaultEffectMusicVolume = 1.0f;
    }
};

#endif /* RadioProducerCommon_h */
