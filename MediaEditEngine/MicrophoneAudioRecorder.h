//
//  MicrophoneAudioRecorder.h
//  MediaStreamer
//
//  Created by Think on 2019/7/10.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef MicrophoneAudioRecorder_h
#define MicrophoneAudioRecorder_h

#include <stdio.h>
#include <pthread.h>

#include "MicrophonePCMRecorder.h"
#include "PCMPlayer.h"

#ifdef ANDROID
#include "jni.h"
#endif

class MicrophoneAudioRecorder{
public:
    MicrophoneAudioRecorder(int sampleRate, int numChannels, char* workDir);
    ~MicrophoneAudioRecorder();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
    bool initialize();
    
    bool startRecord();
    bool startRecord(int64_t startPositionUs);
    bool isAudioRecording();
    void stopRecord();
    int64_t getRecordTimeUs();
    int getRecordPcmDB();
    void setEarReturn(bool isEnableEarReturn);
    void enableNoiseSuppression(bool isEnableNoiseSuppression);
    
    bool openAudioPlayer();
    bool startAudioPlay();
    bool seekAudioPlay(int64_t seekTimeUs);
    void pauseAudioPlay();
    bool isAudioPlaying();
    void closeAudioPlayer();
    int64_t getPlayTimeUs();
    int64_t getPlayDurationUs();
    int getPlayPcmDB();
    
    bool reverseOrderGeneration();
    
    bool backDelete(int64_t keepTimeUs);
    bool convertToWav(char *wavFilePath);
    bool convertToWavWithOffset(char *wavFilePath, long offsetMs);
    
    void terminate();
private:
    int mSampleRate;
    int mNumChannels;
    char* mWorkDir;
#ifdef ANDROID
    JavaVM *mJvm;
#endif
private:
    char* mWorkPCMFilePath;
    MicrophonePCMRecorder* mMicrophonePCMRecorder;
    PCMPlayer* mPCMPlayer;
private:
    pthread_mutex_t mLock;
    bool initialized;
    bool isWorkMediaFileLocked;
    bool isRecording;
    bool isAudioPlayerOpened;
};

bool convertToWavWithNS(char* inputWavFilePath, char* outputWavFilePath);
bool convertToWavWithOffset(char* inputWavFilePath, char* outputWavFilePath, long offsetMs);
//effectType : [UserDefinedEffect : 0] [EqualizerStyle : 1] [ReverbStyle :2]
bool convertToWavWithAudioEffect(char* inputWavFilePath, char* outputWavFilePath, int effectType, int effect);

#endif /* MicrophoneAudioRecorder_h */
