//
//  AudioRemuxerCommon.h
//  MediaStreamer
//
//  Created by Think on 2019/10/31.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef AudioRemuxerCommon_h
#define AudioRemuxerCommon_h

#include <stdio.h>
#include <stdlib.h>

#define MAX_AUDIO_REMUXER_MATERIAL_NUM 64

struct AudioRemuxerMaterial {
    char* url;
    int32_t startPosMs;
    int32_t endPosMs;
    bool isRemuxAll;
    
    AudioRemuxerMaterial()
    {
        url = NULL;
        startPosMs = 0;
        endPosMs = 0;
        
        isRemuxAll = true;
    }
    
    inline void Free()
    {
        if(url)
        {
            free(url);
            url = NULL;
        }
    }
};

struct AudioRemuxerMaterialGroup {
    AudioRemuxerMaterial* audioRemuxerMaterials[MAX_AUDIO_REMUXER_MATERIAL_NUM];
    int audioRemuxerMaterialNum;
    
    AudioRemuxerMaterialGroup()
    {
        for(int i = 0; i < MAX_AUDIO_REMUXER_MATERIAL_NUM; i++)
        {
            audioRemuxerMaterials[i] = NULL;
        }
        
        audioRemuxerMaterialNum = 0;
    }
    
    inline void Free()
    {
        for(int i = 0; i < MAX_AUDIO_REMUXER_MATERIAL_NUM; i++)
        {
            if(audioRemuxerMaterials[i])
            {
                audioRemuxerMaterials[i]->Free();
                
                delete audioRemuxerMaterials[i];
                audioRemuxerMaterials[i] = NULL;
            }
        }
        
        audioRemuxerMaterialNum = 0;
    }
};

struct AudioRemuxerProduct {
    char* url;
    
    AudioRemuxerProduct()
    {
        url = NULL;
    }
    
    inline void Free()
    {
        if(url)
        {
            free(url);
            url = NULL;
        }
    }
};


#endif /* AudioRemuxerCommon_h */
