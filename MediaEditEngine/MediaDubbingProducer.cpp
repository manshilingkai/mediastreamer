//
//  MediaDubbingProducer.cpp
//  MediaStreamer
//
//  Created by Think on 2019/7/17.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "MediaDubbingProducer.h"
#include "AutoLock.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#else
#include "NormalMediaListener.h"
#endif

#include "MediaLog.h"
#include "MediaFile.h"
#include "FFLog.h"
#include "AudioMixer.h"

struct MediaDubbingProducerEvent : public TimedEventQueue::Event {
    MediaDubbingProducerEvent(
                                MediaDubbingProducer *producer,
                                void (MediaDubbingProducer::*method)())
    : mProducer(producer),
    mMethod(method) {
    }
    
protected:
    virtual ~MediaDubbingProducerEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mProducer->*mMethod)();
    }
    
private:
    MediaDubbingProducer *mProducer;
    void (MediaDubbingProducer::*mMethod)();
    
    MediaDubbingProducerEvent(const MediaDubbingProducerEvent &);
    MediaDubbingProducerEvent &operator=(const MediaDubbingProducerEvent &);
};

#ifdef ANDROID
MediaDubbingProducer::MediaDubbingProducer(JavaVM *jvm, MediaDubbingVideoOptions videoOptions, MediaDubbingBGMOptions bgmOptions, MediaDubbingDubOptions dubOptions, MediaDubbingProductOptions productOptions)
{
    mJvm = jvm;

    init_ffmpeg_env();
    
    mVideoOptions = videoOptions;
    if (videoOptions.videoUrl) {
        mVideoOptions.videoUrl = strdup(videoOptions.videoUrl);
    }
    
    mBgmOptions = bgmOptions;
    if (bgmOptions.bgmUrl) {
        mBgmOptions.bgmUrl = strdup(bgmOptions.bgmUrl);
    }
    
    mDubOptions = dubOptions;
    if (dubOptions.dubUrl) {
        mDubOptions.dubUrl = strdup(dubOptions.dubUrl);
    }
    
    mProductOptions = productOptions;
    if (productOptions.productUrl) {
        mProductOptions.productUrl = strdup(productOptions.productUrl);
    }
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);
    
    mAsyncPrepareEvent = NULL;
    mEncodeEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    
    mAsyncPrepareEvent = new MediaDubbingProducerEvent(this, &MediaDubbingProducer::onPrepareAsyncEvent);
    mEncodeEvent = new MediaDubbingProducerEvent(this, &MediaDubbingProducer::onEncodeEvent);
    mStopEvent = new MediaDubbingProducerEvent(this, &MediaDubbingProducer::onStopEvent);
    mNotifyEvent = new MediaDubbingProducerEvent(this, &MediaDubbingProducer::onNotifyEvent);
    mEncodeEventPending = false;
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();
    
    pthread_cond_init(&mStopCondition, NULL);
    
    mMediaListener = NULL;
    
    gotError = false;
    
    ifmt_ctx = NULL;
    mInputVideoStreamIndex = -1;
    mInputVideoMaterialDurationSecond = 0;
    
    mMediaFrameOutputer = NULL;
    mInputAudioBgmMaterialDurationSecond = 0;
    audio_buffersrc_ctx = NULL;
    audio_buffersink_ctx = NULL;
    audio_filter_graph = NULL;
    filteredAudioDataFifo = NULL;
    
    mDubPCMReader = NULL;
    mDubPCMReaderType = UNKNOWN_PCM_READER_TYPE;
    mWorkAudioDubData = NULL;
    mWorkAudioDubDataSize = 0;
    
    ofmt_ctx = NULL;
    mOutputAudioStreamIndex = -1;
    mOutputVideoStreamIndex = -1;
    audio_encoder_ctx = NULL;
    
    GotBaselineForWriteAudioPacket = false;
    baseLineForWriteAudioPacket = 0ll;
    mCurrentWriteAudioPacketPtsUs = 0ll;
    GotWriteAudioPacketEof = false;
    
    GotBaselineForWriteVideoPacket = false;
    baseLineForWriteVideoPacket = 0ll;
    mCurrentWriteVideoPacketPtsUs = 0ll;
    GotWriteVideoPacketEof = false;
    
    mOutputMediaProductDurationSecond = 0;
    writeTimeStampSecond = 0;
    
    hasVideoMaterial = false;
    hasAudioDubMaterial = false;
}
#else
MediaDubbingProducer::MediaDubbingProducer(MediaDubbingVideoOptions videoOptions, MediaDubbingBGMOptions bgmOptions, MediaDubbingDubOptions dubOptions, MediaDubbingProductOptions productOptions)
{
    init_ffmpeg_env();

    mVideoOptions = videoOptions;
    if (videoOptions.videoUrl) {
        mVideoOptions.videoUrl = strdup(videoOptions.videoUrl);
    }
    
    mBgmOptions = bgmOptions;
    if (bgmOptions.bgmUrl) {
        mBgmOptions.bgmUrl = strdup(bgmOptions.bgmUrl);
    }
    
    mDubOptions = dubOptions;
    if (dubOptions.dubUrl) {
        mDubOptions.dubUrl = strdup(dubOptions.dubUrl);
    }
    
    mProductOptions = productOptions;
    if (productOptions.productUrl) {
        mProductOptions.productUrl = strdup(productOptions.productUrl);
    }
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);

    mAsyncPrepareEvent = NULL;
    mEncodeEvent = NULL;
    mStopEvent = NULL;
    mNotifyEvent = NULL;
    
    mAsyncPrepareEvent = new MediaDubbingProducerEvent(this, &MediaDubbingProducer::onPrepareAsyncEvent);
    mEncodeEvent = new MediaDubbingProducerEvent(this, &MediaDubbingProducer::onEncodeEvent);
    mStopEvent = new MediaDubbingProducerEvent(this, &MediaDubbingProducer::onStopEvent);
    mNotifyEvent = new MediaDubbingProducerEvent(this, &MediaDubbingProducer::onNotifyEvent);
    mEncodeEventPending = false;
    
    mQueue.start();

    pthread_cond_init(&mStopCondition, NULL);

    mMediaListener = NULL;

    gotError = false;
    
    ifmt_ctx = NULL;
    mInputVideoStreamIndex = -1;
    mInputVideoMaterialDurationSecond = 0;
    
    mMediaFrameOutputer = NULL;
    mInputAudioBgmMaterialDurationSecond = 0;
    audio_buffersrc_ctx = NULL;
    audio_buffersink_ctx = NULL;
    audio_filter_graph = NULL;
    filteredAudioDataFifo = NULL;
    
    mDubPCMReader = NULL;
    mDubPCMReaderType = UNKNOWN_PCM_READER_TYPE;
    mWorkAudioDubData = NULL;
    mWorkAudioDubDataSize = 0;
    
    ofmt_ctx = NULL;
    mOutputAudioStreamIndex = -1;
    mOutputVideoStreamIndex = -1;
    audio_encoder_ctx = NULL;
    
    GotBaselineForWriteAudioPacket = false;
    baseLineForWriteAudioPacket = 0ll;
    mCurrentWriteAudioPacketPtsUs = 0ll;
    GotWriteAudioPacketEof = false;
    
    GotBaselineForWriteVideoPacket = false;
    baseLineForWriteVideoPacket = 0ll;
    mCurrentWriteVideoPacketPtsUs = 0ll;
    GotWriteVideoPacketEof = false;
    
    mOutputMediaProductDurationSecond = 0;
    writeTimeStampSecond = 0;
    
    hasVideoMaterial = false;
    hasAudioDubMaterial = false;
}
#endif

MediaDubbingProducer::~MediaDubbingProducer()
{
    stop(false);

    mQueue.stop(true);

    mNotificationQueue.flush();

    if (mAsyncPrepareEvent!=NULL) {
        delete mAsyncPrepareEvent;
        mAsyncPrepareEvent = NULL;
    }
    
    if (mEncodeEvent!=NULL) {
        delete mEncodeEvent;
        mEncodeEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    if (mNotifyEvent!=NULL) {
        delete mNotifyEvent;
        mNotifyEvent = NULL;
    }
    
    pthread_cond_destroy(&mStopCondition);
    
    pthread_mutex_destroy(&mLock);
    
#ifdef ANDROID
    JniMediaListener *jniListener = (JniMediaListener*)mMediaListener;
    if (jniListener!=NULL) {
        delete jniListener;
        jniListener = NULL;
    }
#else
    NormalMediaListener* normalMediaListener = (NormalMediaListener*)mMediaListener;
    if (normalMediaListener!=NULL) {
        delete normalMediaListener;
        normalMediaListener = NULL;
    }
#endif
    
    mMediaListener = NULL;
    
    if (mVideoOptions.videoUrl) {
        free(mVideoOptions.videoUrl);
        mVideoOptions.videoUrl = NULL;
    }
    
    if (mBgmOptions.bgmUrl) {
        free(mBgmOptions.bgmUrl);
        mBgmOptions.bgmUrl = NULL;
    }
    
    if (mDubOptions.dubUrl) {
        free(mDubOptions.dubUrl);
        mDubOptions.dubUrl = NULL;
    }
    
    if (mProductOptions.productUrl) {
        free(mProductOptions.productUrl);
        mProductOptions.productUrl = NULL;
    }
}

#ifdef ANDROID
void MediaDubbingProducer::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    modifyFlags(INITIALIZED, SET);
}
#else
void MediaDubbingProducer::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new NormalMediaListener(listener,arg);
    
    modifyFlags(INITIALIZED, SET);
}
#endif

void MediaDubbingProducer::notifyListener_l(int event, int ext1, int ext2)
{
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void MediaDubbingProducer::notify(int event, int ext1, int ext2)
{
    Notification *notification = new Notification();
    notification->event = event;
    notification->ext1 = ext1;
    notification->ext2 = ext2;
    
    mNotificationQueue.push(notification);
    postNotifyEvent_l();
}

void MediaDubbingProducer::postNotifyEvent_l()
{
    mQueue.postEventWithDelay(mNotifyEvent, 0);
}

void MediaDubbingProducer::onNotifyEvent()
{
    AutoLock autoLock(&mLock);
    
    Notification *notification = mNotificationQueue.pop();
    if (notification==NULL) {
        return;
    }
    
    int event = notification->event;
    int ext1 = notification->ext1;
    int ext2 = notification->ext2;
    
    if (notification!=NULL) {
        delete notification;
        notification = NULL;
    }
    
    switch (event) {
        case MEDIA_TRANSCODER_ERROR:
            modifyFlags(ERROR, ASSIGN);
            if (ext2!=AVERROR_EXIT) {
                if (ext1==MEDIA_TRANSCODER_ERROR_DEMUXER_READ_FAIL) {
                    notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_AUDIO_DEMUX_FAIL, ext2);
                }else if (ext1==MEDIA_TRANSCODER_ERROR_NO_MEMORY) {
                    notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_NO_MEMORY_SPACE, ext2);
                }else{
                    notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_UNKNOWN, ext2);
                }
            }
            gotError = true;
            stop_l();
            break;
        case MEDIA_TRANSCODER_INFO:
            if (ext1==MEDIA_TRANSCODER_WARN_AUDIO_DECODE_FAIL) {
                LOGW("[WARN] Got Audio Decode Fail");
            }
            break;
            
        default:
            break;
    }
}

void MediaDubbingProducer::start()
{
    AutoLock autoLock(&mLock);
    if (mFlags & CONNECTING) {
        LOGW("already connecting");
        return;
    }
    
    if (!(mFlags & INITIALIZED) && !(mFlags & END)) {
        return;
    }
    
    prepareAsync_l();
}

void MediaDubbingProducer::prepareAsync_l()
{
    modifyFlags(END, CLEAR);
    modifyFlags(INITIALIZED, CLEAR);
    modifyFlags(CONNECTING, SET);
    
    gotError = false;
    
    mQueue.postEvent(mAsyncPrepareEvent);
}

void MediaDubbingProducer::onPrepareAsyncEvent()
{
    AutoLock autoLock(&mLock);
    int ret = open_all_pipelines_l();
    
    if (ret>=0) {
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(CONNECTED, SET);
        
        play_l();
    }else{
        if (ret!=AVERROR_EXIT) {
            if (ret==-1) {
                notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_OPEN_INPUT_VIDEO_MATERIAL_FAIL);
            }else if (ret==-2) {
                notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_OPEN_INPUT_BGM_MATERIAL_FAIL);
            }else if (ret==-3) {
                notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_OPEN_INPUT_DUB_MATERIAL_FAIL);
            }else if (ret==-4) {
                notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_OPEN_OUTPUT_MEDIA_PRODUCT_FAIL);
            }else {
                notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_UNKNOWN);
            }
        }
        
        modifyFlags(CONNECTING, CLEAR);
        modifyFlags(ERROR, SET);
        gotError = true;
        stop_l();
    }
}

void MediaDubbingProducer::pause()
{
    AutoLock autoLock(&mLock);
    return pause_l();
}

void MediaDubbingProducer::pause_l()
{
    if (!(mFlags & STREAMING)) {
        return;
    }
    
    modifyFlags(STREAMING, CLEAR);
    
    mQueue.cancelEvent(mEncodeEvent->eventID());
    mEncodeEventPending = false;
    
    if (mMediaFrameOutputer) {
        mMediaFrameOutputer->pause();
    }
    
    modifyFlags(PAUSED, SET);
    
    return;
}

void MediaDubbingProducer::resume()
{
    AutoLock autoLock(&mLock);
    
    return play_l();
}

void MediaDubbingProducer::play_l()
{
    if (mFlags & STREAMING) {
        LOGW("%s","MediaDubbingProducer is streaming");
        return;
    }
    
    if (!(mFlags & CONNECTED) && !(mFlags & PAUSED)) {
        return;
    }
    
    modifyFlags(CONNECTED, CLEAR);
    modifyFlags(PAUSED, CLEAR);
    
    if (mMediaFrameOutputer) {
        mMediaFrameOutputer->start();
    }
    
    postEncodeEvent_l();
    
    modifyFlags(STREAMING, SET);
    
    notifyListener_l(MEDIA_DUBBING_STREAMING);
}

void MediaDubbingProducer::cancelProducerEvents()
{
    mQueue.cancelEvent(mEncodeEvent->eventID());
    mEncodeEventPending = false;
    mQueue.cancelEvent(mNotifyEvent->eventID());
    mQueue.cancelEvent(mAsyncPrepareEvent->eventID());
    mQueue.cancelEvent(mStopEvent->eventID());
}

void MediaDubbingProducer::stop(bool isCancle)
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & END) {
        return;
    }
    
    if (mFlags & ENDING) {
        LOGW("already ending");
        
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    if (isCancle) {
        gotError = true;
    }
    
    stop_l();
    
    pthread_cond_wait(&mStopCondition, &mLock);
    
    if (isCancle) {
        if (MediaFile::isExist(mProductOptions.productUrl)) {
            MediaFile::deleteFile(mProductOptions.productUrl);
        }
    }
}

void MediaDubbingProducer::stop_l()
{
    modifyFlags(ENDING, ASSIGN);
    
    mQueue.postEventWithDelay(mStopEvent, 0);
}

void MediaDubbingProducer::postEncodeEvent_l(int64_t delayUs)
{
    if (mEncodeEventPending) {
        return;
    }
    mEncodeEventPending = true;
    mQueue.postEventWithDelay(mEncodeEvent, delayUs < 0 ? 0 : delayUs);
}

void MediaDubbingProducer::onStopEvent()
{
    AutoLock autoLock(&mLock);
    
    close_all_pipelines_l();
    
    cancelProducerEvents();
    mNotificationQueue.flush();
    
    modifyFlags(END, ASSIGN);
    
    if (!gotError) {
        notifyListener_l(MEDIA_DUBBING_INFO, MEDIA_DUBBING_INFO_WRITE_PERCENT, 100);
        notifyListener_l(MEDIA_DUBBING_END);
    }else{
        if (MediaFile::isExist(mProductOptions.productUrl)) {
            MediaFile::deleteFile(mProductOptions.productUrl);
        }
    }
    gotError = false;
    pthread_cond_broadcast(&mStopCondition);
}

void MediaDubbingProducer::onEncodeEvent()
{
    AutoLock autoLock(&mLock);
    
    if (!mEncodeEventPending) {
        // The event has been cancelled in reset_l() but had already // been scheduled for execution at that time.
        return;
    }
    mEncodeEventPending = false;
    
    int ret = flowing_l();
    if (ret==1) {
        postEncodeEvent_l(0);
        return;
    }else if (ret==-7) {
        stop_l();
        return;
    }else if (ret<0) {
        modifyFlags(STREAMING, CLEAR);
        modifyFlags(ERROR, SET);
        
        if (ret!=AVERROR_EXIT) {
            if (ret==-1) {
                notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_AUDIO_FILTER_INPUT_FAIL);
            }else if (ret==-2) {
                notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_AUDIO_FILTER_OUTPUT_FAIL);
            }else if (ret==-3) {
                notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_AUDIO_ENCODE_FAIL);
            }else if (ret==-4) {
                notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_AUDIO_MUX_FAIL);
            }else if (ret==-5) {
                notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_AUDIO_FIFO_READ_FAIL);
            }else if (ret==-6) {
                notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_VIDEO_REMUX_FAIL);
            }else if (ret==-8) {
                notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_NO_MEMORY_SPACE);
            }else{
                notifyListener_l(MEDIA_DUBBING_ERROR, MEDIA_DUBBING_ERROR_UNKNOWN);
            }
        }
        
        gotError = true;
        stop_l();
        
        return;
    }else {
        int64_t waitTimeUs = 10*1000;
        postEncodeEvent_l(waitTimeUs);
        return;
    }
}

void MediaDubbingProducer::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

void MediaDubbingProducer::init_ffmpeg_env()
{
    // init ffmpeg env
    av_register_all();
    avcodec_register_all();
    avfilter_register_all();
    
    FFLog::setLogLevel(AV_LOG_WARNING);
}

// -1 : OPEN INPUT VIDEO MATERIAL FAIL
// -2 : OPEN INPUT AUDIO BGM MATERIAL FAIL
// -3 : OPEN INPUT AUDIO DUB MATERIAL FAIL
// -4 : OPEN OUTPUT MEDIA PRODUCT FAIL
int MediaDubbingProducer::open_all_pipelines_l()
{
    if (mVideoOptions.videoUrl==NULL) {
        hasVideoMaterial = false;
    }else{
        hasVideoMaterial = true;
    }
    if (hasVideoMaterial) {
        int ret = open_input_video_material();
        if (ret<0) {
            if (ret==AVERROR_EXIT) {
                return AVERROR_EXIT;
            }
            return -1;
        }
    }

    if (mDubOptions.dubUrl==NULL) {
        hasAudioDubMaterial = false;
    }else{
        hasAudioDubMaterial = true;
    }
    
    if (hasAudioDubMaterial) {
        int ret = open_input_audio_dub_material();
        if (ret<0) {
            close_input_video_material();
            return -3;
        }
    }
    
    int ret = open_output_media_product();
    if (ret<0) {
        close_input_audio_dub_material();
        close_input_video_material();
        
        if (ret==AVERROR_EXIT) {
            return AVERROR_EXIT;
        }
        return -4;
    }
    
    ret = open_input_audio_bgm_material();
    if (ret<0) {
        close_output_media_product();
        close_input_audio_dub_material();
        close_input_video_material();
        
        if (ret==AVERROR_EXIT) {
            return AVERROR_EXIT;
        }
        return -2;
    }
    
    if (mInputVideoMaterialDurationSecond > mInputAudioBgmMaterialDurationSecond) {
        mOutputMediaProductDurationSecond = mInputVideoMaterialDurationSecond;
    }else{
        mOutputMediaProductDurationSecond = mInputAudioBgmMaterialDurationSecond;
    }
    
    return 0;
}

void MediaDubbingProducer::close_all_pipelines_l()
{
    close_input_audio_bgm_material();
    close_output_media_product();
    close_input_audio_dub_material();
    close_input_video_material();
}

// -1: OPEN INPUT VIDEO FILE FAIL
// -2: RETRIEVE STREAM INFO FAIL FOR INPUT VIDEO FILE
// -3: NO VIDEO STREAM FOR INPUT VIDEO FILE
int MediaDubbingProducer::open_input_video_material()
{
    int ret = 0;
    if ((ret = avformat_open_input(&ifmt_ctx, mVideoOptions.videoUrl, 0, 0)) < 0) {
        ifmt_ctx = NULL;
        
        LOGE("Could not open input video file '%s'", mVideoOptions.videoUrl);
        return -1;
    }
    
    if ((ret = avformat_find_stream_info(ifmt_ctx, 0)) < 0) {
        avformat_close_input(&ifmt_ctx);
        ifmt_ctx = NULL;
        
        LOGE("Failed to retrieve stream information for input video file");
        return -2;
    }
    
//    av_dump_format(ifmt_ctx, 0, mVideoOptions.videoUrl, 0);
    
    mInputVideoMaterialDurationSecond = (int)(av_rescale(ifmt_ctx->duration, 1000, AV_TIME_BASE) / 1000);
    
    mInputVideoStreamIndex = -1;
    
    for(int i= 0; i < ifmt_ctx->nb_streams; i++)
    {
        if (ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && (ifmt_ctx->streams[i]->codec->codec_id==AV_CODEC_ID_H264 || ifmt_ctx->streams[i]->codec->codec_id==AV_CODEC_ID_HEVC || ifmt_ctx->streams[i]->codec->codec_id==AV_CODEC_ID_MPEG4))
        {
            //by default, use the first video stream, and discard others.
            if(mInputVideoStreamIndex == -1)
            {
                mInputVideoStreamIndex = i;
            }
            else
            {
                ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
            }
        }else{
            ifmt_ctx->streams[i]->discard = AVDISCARD_ALL;
        }
    }
    
    if (mInputVideoStreamIndex==-1) {
        avformat_close_input(&ifmt_ctx);
        ifmt_ctx = NULL;
        
        mInputVideoStreamIndex = -1;
        
        LOGE("No Video Stream");
        return -3;
    }
    
    return 0;
}

void MediaDubbingProducer::close_input_video_material()
{
    if (ifmt_ctx) {
        avformat_close_input(&ifmt_ctx);
        ifmt_ctx = NULL;
    }

    mInputVideoStreamIndex = -1;
    
    mInputVideoMaterialDurationSecond = 0;
}

//-1 : CREATE MEDIAFRAMEOUTPUTER FAIL
//-2 : MEDIAFRAMEOUTPUTER PREPARE FAIL
//-3 : OPEN AUDIO FILTER FAIL
int MediaDubbingProducer::open_input_audio_bgm_material()
{
    mMediaFrameOutputer = MediaFrameOutputer::createMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, NULL, mBgmOptions.bgmUrl, NULL, 2, false, true, -1, -1);
    if (mMediaFrameOutputer==NULL) {
        LOGE("Create MediaFrameOutputer Fail");
        return -1;
    }
    mMediaFrameOutputer->setListener(this);
    int ret = mMediaFrameOutputer->prepare();
    if (ret<0) {
        MediaFrameOutputer::deleteMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, mMediaFrameOutputer);
        mMediaFrameOutputer = NULL;
        
        if (ret==AVERROR_EXIT) {
            LOGW("Exit MediaFrameOutputer Prepare");
            return AVERROR_EXIT;
        }
        
        LOGE("MediaFrameOutputer Prepare Fail");
        return -2;
    }
    
    mInputAudioBgmMaterialDurationSecond = (int)(mMediaFrameOutputer->getDurationMs() / 1000);
    
    char audio_filter_spec[256];
    audio_filter_spec[0] = 0;
    if (mBgmOptions.bgmVolume!=1.0f) {
        av_strlcatf(audio_filter_spec, sizeof(audio_filter_spec), "volume=%f", mBgmOptions.bgmVolume);
    }else {
        av_strlcatf(audio_filter_spec, sizeof(audio_filter_spec), "anull");
    }
    
    ret = open_audio_filter(mMediaFrameOutputer->getAudioContext(), audio_encoder_ctx, audio_filter_spec);
    if (ret<0) {
        mMediaFrameOutputer->stop();
        MediaFrameOutputer::deleteMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, mMediaFrameOutputer);
        mMediaFrameOutputer = NULL;
        
        LOGE("Open Audio Filter Fail");
        return -3;
    }
    
    filteredAudioDataFifo = av_audio_fifo_alloc(audio_encoder_ctx->sample_fmt, audio_encoder_ctx->channels, 1);
    
    return 0;
}

void MediaDubbingProducer::close_input_audio_bgm_material()
{
    if (filteredAudioDataFifo) {
        av_audio_fifo_free(filteredAudioDataFifo);
        filteredAudioDataFifo = NULL;
    }
    
    close_audio_filter();
    
    if (mMediaFrameOutputer) {
        mMediaFrameOutputer->stop();
        MediaFrameOutputer::deleteMediaFrameOutputer(MEDIA_FRAME_OUTPUTER_TYPE_DEFAULT, mMediaFrameOutputer);
        mMediaFrameOutputer = NULL;
    }
}

int MediaDubbingProducer::open_audio_filter(AudioContext audioContext, AVCodecContext* audio_enc_ctx, const char *filter_spec)
{
    char args[512];
    int ret = 0;
    const AVFilter *buffersrc = NULL;
    const AVFilter *buffersink = NULL;
    AVFilterContext *buffersrc_ctx = NULL;
    AVFilterContext *buffersink_ctx = NULL;
    AVFilterInOut *outputs = avfilter_inout_alloc();
    AVFilterInOut *inputs  = avfilter_inout_alloc();
    AVFilterGraph *filter_graph = avfilter_graph_alloc();
    
    if (!outputs || !inputs || !filter_graph) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        ret = AVERROR(ENOMEM);
        LOGE("No memory space for avfilter alloc");
        return ret;
    }
    
    buffersrc = avfilter_get_by_name("abuffer");
    buffersink = avfilter_get_by_name("abuffersink");
    
    if (!buffersrc || !buffersink) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("filtering source or sink element not found");
        ret = AVERROR_UNKNOWN;
        return ret;
    }
    
    if (!audioContext.channel_layout)
    {
        audioContext.channel_layout = av_get_default_channel_layout(audioContext.channels);
    }
    
    snprintf(args, sizeof(args),
             "time_base=%d/%d:sample_rate=%d:sample_fmt=%s:channel_layout=0x%" PRIx64,
             audioContext.time_base.num, audioContext.time_base.den, audioContext.sample_rate,
             av_get_sample_fmt_name(audioContext.sample_fmt),
             audioContext.channel_layout);
    
    ret = avfilter_graph_create_filter(&buffersrc_ctx, buffersrc, "in",
                                       args, NULL, filter_graph);
    
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot create audio buffer source");
        return ret;
    }
    
    ret = avfilter_graph_create_filter(&buffersink_ctx, buffersink, "out",
                                       NULL, NULL, filter_graph);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot create audio buffer sink");
        return ret;
    }
    
    ret = av_opt_set_bin(buffersink_ctx, "sample_fmts",
                         (uint8_t*)&audio_enc_ctx->sample_fmt, sizeof(audio_enc_ctx->sample_fmt),
                         AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot set output sample format");
        return ret;
    }
    
    ret = av_opt_set_bin(buffersink_ctx, "channel_layouts",
                         (uint8_t*)&audio_enc_ctx->channel_layout,
                         sizeof(audio_enc_ctx->channel_layout), AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot set output channel layout");
        return ret;
    }
    
    ret = av_opt_set_bin(buffersink_ctx, "sample_rates",
                         (uint8_t*)&audio_enc_ctx->sample_rate, sizeof(audio_enc_ctx->sample_rate),
                         AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("Cannot set output sample rate");
        return ret;
    }
    
    /* Endpoints for the filter graph. */
    outputs->name       = av_strdup("in");
    outputs->filter_ctx = buffersrc_ctx;
    outputs->pad_idx    = 0;
    outputs->next       = NULL;
    
    inputs->name       = av_strdup("out");
    inputs->filter_ctx = buffersink_ctx;
    inputs->pad_idx    = 0;
    inputs->next       = NULL;
    
    if (!outputs->name || !inputs->name) {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("No memory space");
        ret = AVERROR(ENOMEM);
        return ret;
    }
    
    if ((ret = avfilter_graph_parse_ptr(filter_graph, filter_spec,
                                        &inputs, &outputs, NULL)) < 0)
    {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("avfilter_graph_parse_ptr fail");
        return ret;
    }
    
    if ((ret = avfilter_graph_config(filter_graph, NULL)) < 0)
    {
        if (inputs) {
            avfilter_inout_free(&inputs);
        }
        if (outputs) {
            avfilter_inout_free(&outputs);
        }
        
        if (filter_graph) {
            avfilter_graph_free(&filter_graph);
        }
        
        LOGE("avfilter_graph_config fail");
        return ret;
    }
    
    /* Fill FilteringContext */
    audio_buffersrc_ctx = buffersrc_ctx;
    audio_buffersink_ctx = buffersink_ctx;
    audio_filter_graph = filter_graph;
    
    avfilter_inout_free(&inputs);
    avfilter_inout_free(&outputs);
    
    return 0;
}

void MediaDubbingProducer::close_audio_filter()
{
    if (audio_filter_graph) {
        avfilter_graph_free(&audio_filter_graph);
        audio_filter_graph = NULL;
    }
    
    audio_buffersrc_ctx = NULL;
    audio_buffersink_ctx = NULL;
}

// -1 : OPEN DUB WAV FILE FAIL
int MediaDubbingProducer::open_input_audio_dub_material()
{
    if (strstr(mDubOptions.dubUrl, ".wav") || strstr(mDubOptions.dubUrl, ".WAV")) {
        mDubPCMReaderType = WAV_FILE_READER;
    }else{
        mDubPCMReaderType = FF_AUDIO_FILE_READER;
    }
    mDubPCMReader = IPCMReader::CreatePCMReader(mDubPCMReaderType, mDubOptions.dubUrl);
    mDubPCMReader->setVolume(mDubOptions.dubVolume);
    bool ret = mDubPCMReader->open();
    if (!ret) {
        IPCMReader::DeletePCMReader(mDubPCMReader, mDubPCMReaderType);
        mDubPCMReader = NULL;
        mDubPCMReaderType = UNKNOWN_PCM_READER_TYPE;
        
        LOGE("Open Dub WAV File Fail");
        return -1;
    }
    
    mProductOptions.audioNumChannels = mDubPCMReader->getChannelCount();
    mProductOptions.audioSampleRate = mDubPCMReader->getSampleRate();
    if (mDubPCMReader->getBitsPerSample()/8==1) {
        mProductOptions.audioSampleFormat = AV_SAMPLE_FMT_U8;
    }else if (mDubPCMReader->getBitsPerSample()/8==2) {
        mProductOptions.audioSampleFormat = AV_SAMPLE_FMT_S16;
    }else if (mDubPCMReader->getBitsPerSample()/8==4) {
        mProductOptions.audioSampleFormat = AV_SAMPLE_FMT_S32;
    }else{
        mProductOptions.audioSampleFormat = AV_SAMPLE_FMT_S16;
    }
    
    if(mWorkAudioDubData)
    {
        free(mWorkAudioDubData);
        mWorkAudioDubData = NULL;
    }
    if (mWorkAudioDubData==NULL) {
        mWorkAudioDubDataSize = mProductOptions.audioSampleRate*mProductOptions.audioNumChannels*av_get_bytes_per_sample(mProductOptions.audioSampleFormat)/10; //100ms
        mWorkAudioDubData = (char*)malloc(mWorkAudioDubDataSize);
    }
    
    if (mDubPCMReader->getDurationMs()<=0) {
        notifyListener_l(MEDIA_DUBBING_INFO, MEDIA_DUBBING_INFO_LOST_DUB_DATA);
    }
    
    return 0;
}

void MediaDubbingProducer::close_input_audio_dub_material()
{
    if(mWorkAudioDubData)
    {
        free(mWorkAudioDubData);
        mWorkAudioDubData = NULL;
    }
    mWorkAudioDubDataSize = 0;
    
    if (mDubPCMReader) {
        mDubPCMReader->close();
        IPCMReader::DeletePCMReader(mDubPCMReader, mDubPCMReaderType);
        mDubPCMReader = NULL;
        mDubPCMReaderType = UNKNOWN_PCM_READER_TYPE;
    }
}

// -1: ALLOC AVFORMAT OUTPUT CONTEXT FAIL
// -2: ALLOC OUTPUT VIDEO STREAM FAIL
// -3: COPY VIDEO CODEC PARAM FAIL FROM INPUT TO OUTPUT
// -4: OPEN OUTPUT FILE FAIL
// -5: WRITE HEADER INTO OUTPUT FILE FAIL
// -6: ALLOC OUTPUT AUDIO STREAM FAIL
// -7: NOT FOUND AUDIO ENCODER
// -8: ALLOC AUDIO ENCODER CONTEXT FAIL
// -9: OPEN AUDIO ENCODER FAIL
// -10 COPY AUDIO ENCODER PARM TO OUTPUT AUDIO STREAM FAIL
int MediaDubbingProducer::open_output_media_product()
{
    if (strstr(mProductOptions.productUrl, ".m4a") || strstr(mProductOptions.productUrl, ".M4A")) {
            avformat_alloc_output_context2(&ofmt_ctx, NULL, "mp4", mProductOptions.productUrl);
    }else{
        avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, mProductOptions.productUrl);
    }
//    avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, mProductOptions.productUrl);
    if (!ofmt_ctx) {
        LOGE("Could not create output context");
        return -1;
    }
    
    ofmt_ctx->oformat->flags |= AVFMT_TS_NONSTRICT;
    
    int stream_index = -1;
    if (mInputVideoStreamIndex>=0) {
        AVStream *in_video_stream = ifmt_ctx->streams[mInputVideoStreamIndex];
        AVCodecParameters *in_video_codecpar = in_video_stream->codecpar;
        AVStream *out_video_stream = avformat_new_stream(ofmt_ctx, NULL);
        
        if (!out_video_stream){
            if (ofmt_ctx) {
                avformat_free_context(ofmt_ctx);
                ofmt_ctx = NULL;
            }
            
            LOGE("Failed allocating output stream");
            return -2;
        }
        
        mOutputVideoStreamIndex = ++stream_index;
        
        int ret = avcodec_parameters_copy(out_video_stream->codecpar, in_video_codecpar);
        if (ret < 0) {
            if (ofmt_ctx) {
                avformat_free_context(ofmt_ctx);
                ofmt_ctx = NULL;
            }
            
            LOGE("Failed to copy video codec parameters");
            return -3;
        }
        
        out_video_stream->codecpar->codec_tag = 0;
        
        AVDictionaryEntry *m = NULL;
        while((m=av_dict_get(in_video_stream->metadata,"",m,AV_DICT_IGNORE_SUFFIX))!=NULL){
            if(strcmp(m->key, "rotate")) continue;
            else{
                int rotate = atoi(m->value);
                av_dict_set_int(&out_video_stream->metadata, "rotate", rotate, 0);
            }
        }
    }
    
    AVStream *out_audio_stream = avformat_new_stream(ofmt_ctx, NULL);
    if (!out_audio_stream) {
        if (ofmt_ctx) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
        }
        
        LOGE("Failed allocating output audio stream");
        return -6;
    }
    
    mOutputAudioStreamIndex = ++stream_index;
    
    AVCodec *audio_encoder = avcodec_find_encoder_by_name("libfdk_aac");
    if (!audio_encoder) {
        if (ofmt_ctx) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
        }
        
        LOGE("Necessary audio encoder not found");
        return -7;
    }
    
    AVCodecContext *audio_enc_ctx = avcodec_alloc_context3(audio_encoder);  //avcodec_free_context
    if (!audio_enc_ctx) {
        if (ofmt_ctx) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
        }
        
        LOGE("Failed to allocate the audio encoder context");
        return -8;
    }
    
    audio_enc_ctx->sample_rate = mProductOptions.audioSampleRate;
    audio_enc_ctx->channel_layout = av_get_default_channel_layout(mProductOptions.audioNumChannels);
    audio_enc_ctx->channels = mProductOptions.audioNumChannels;
    audio_enc_ctx->sample_fmt = mProductOptions.audioSampleFormat;
    audio_enc_ctx->time_base = (AVRational){1, mProductOptions.audioSampleRate};
    audio_enc_ctx->bit_rate = mProductOptions.audioBitrateKbps * 1024;
    audio_enc_ctx->strict_std_compliance = FF_COMPLIANCE_EXPERIMENTAL;
    
    if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        audio_enc_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    
    int ret = avcodec_open2(audio_enc_ctx, audio_encoder, NULL);
    if (ret < 0) {
        avcodec_free_context(&audio_enc_ctx);
        if (ofmt_ctx) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
        }
        
        LOGE("Cannot open audio encoder");
        return -9;
    }
    
    ret = avcodec_parameters_from_context(out_audio_stream->codecpar, audio_enc_ctx);
    if (ret < 0) {
        avcodec_close(audio_enc_ctx);
        avcodec_free_context(&audio_enc_ctx);
        if (ofmt_ctx) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
        }
        
        LOGE("Failed to copy audio encoder parameters to output audio stream");
        return -10;
    }
    out_audio_stream->time_base = audio_enc_ctx->time_base;
    audio_encoder_ctx = audio_enc_ctx;
    //--
    
//    av_dump_format(ofmt_ctx, 0, mProductOptions.productUrl, 1);
    
    if (!(ofmt_ctx->oformat->flags & AVFMT_NOFILE)) {
        int ret = avio_open(&ofmt_ctx->pb, mProductOptions.productUrl, AVIO_FLAG_WRITE);
        if (ret < 0) {
            if (audio_encoder_ctx) {
                avcodec_close(audio_encoder_ctx);
                avcodec_free_context(&audio_encoder_ctx);
                audio_encoder_ctx = NULL;
            }
            
            if (ofmt_ctx) {
                avformat_free_context(ofmt_ctx);
                ofmt_ctx = NULL;
            }
            
            LOGE("Could not open output file '%s'", mProductOptions.productUrl);
            return -4;
        }
    }
    
    ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        if (ofmt_ctx && ofmt_ctx->oformat && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
        {
            avio_close(ofmt_ctx->pb);
        }
        
        if (audio_encoder_ctx) {
            avcodec_close(audio_encoder_ctx);
            avcodec_free_context(&audio_encoder_ctx);
            audio_encoder_ctx = NULL;
        }

        if (ofmt_ctx) {
            avformat_free_context(ofmt_ctx);
            ofmt_ctx = NULL;
        }
        
        LOGE("Error occurred when opening output file");
        return -5;
    }
    
    return 0;
}

void MediaDubbingProducer::close_output_media_product()
{
    if (ofmt_ctx && ofmt_ctx->oformat && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
    {
        avio_close(ofmt_ctx->pb);
    }
    
    if (audio_encoder_ctx) {
        avcodec_close(audio_encoder_ctx);
        avcodec_free_context(&audio_encoder_ctx);
        audio_encoder_ctx = NULL;
    }
    
    if (ofmt_ctx) {
        avformat_free_context(ofmt_ctx);
        ofmt_ctx = NULL;
    }
}

// 1 : JUST LOOP
// 0 : NO INPUT MEDIAFRAME, WAIT THEN LOOP
//-1 : AUDIO FILTER INPUT FAIL
//-2 : AUDIO FILTER OUTPUT FAIL
//-3 : AUDIO ENCODE FAIL
//-4 : AUDIO MUX FAIL
//-5 : AUDIO FIFO READ FAIL
//-6 : VIDEO REMUX FAIL
//-7 : EOF
//-8 : NO MEMORY SPACE
int MediaDubbingProducer::flowing_l()
{
    MediaFrame* workMediaFrame = mMediaFrameOutputer->getMediaFrame();
    
    if (workMediaFrame==NULL) {
        return 0;
    }
    
    if (workMediaFrame->type==MEDIA_FRAME_TYPE_AUDIO) {
        workMediaFrame->avFrame->pts = workMediaFrame->pts;
        
        //push the decoded audio frame into the filtergraph
        int ret = av_buffersrc_add_frame_flags(audio_buffersrc_ctx, workMediaFrame->avFrame, 0);
        if (ret<0) {
            LOGE("Error while feeding decoded audio frame into the filtergraph");
            
            workMediaFrame->Free();
            delete workMediaFrame;
            
            return -1;
        }
        
        workMediaFrame->Free();
        delete workMediaFrame;
        
        while (true) {
            //pull filtered audio frames from the filtergraph
            AVFrame *filt_frame = av_frame_alloc();
            if (!filt_frame) {
                LOGE("No Memory Space");
                
                return -8;
            }
            
            ret = av_buffersink_get_frame(audio_buffersink_ctx, filt_frame);
            if (ret < 0) {
                av_frame_free(&filt_frame);
                
                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
//                    LOGW("no more audio frames for output");
                    return 1;
                }else{
                    LOGE("Error while pulling filtered audio frames from the filtergraph");
                    return -2;
                }
            }
            
            filt_frame->pict_type = AV_PICTURE_TYPE_NONE;
            filt_frame->pts = filt_frame->pts * mMediaFrameOutputer->getAudioContext().sample_rate/audio_encoder_ctx->sample_rate;
            
            //mix
            if (hasAudioDubMaterial) {
                long filt_frame_size = filt_frame->nb_samples * audio_encoder_ctx->channels * av_get_bytes_per_sample(audio_encoder_ctx->sample_fmt);
                if (filt_frame_size>mWorkAudioDubDataSize) {
                    mWorkAudioDubDataSize = filt_frame_size;
                    if (mWorkAudioDubData) {
                        free(mWorkAudioDubData);
                        mWorkAudioDubData = NULL;
                    }
                    mWorkAudioDubData = (char*)malloc(mWorkAudioDubDataSize);
                }
                if (mDubPCMReader) {
                    int ret = mDubPCMReader->getPcmData(&mWorkAudioDubData, filt_frame_size);
                    if (ret>0) {
                        mixSamples((int16_t *)(filt_frame->data[0]), (int16_t *)mWorkAudioDubData, ret/2);
                    }
                }
            }
            
            int ret = av_audio_fifo_write(filteredAudioDataFifo, (void **)filt_frame->data, filt_frame->nb_samples);
            if (ret < filt_frame->nb_samples) {
                LOGE("No Memory Space");
                
                av_frame_free(&filt_frame);
                
                return -8;
            }
            int64_t next_enc_frame_pts = filt_frame->pts;
            av_frame_free(&filt_frame);
            
            while (av_audio_fifo_size(filteredAudioDataFifo) >= audio_encoder_ctx->frame_size) {
                AVFrame *enc_frame = av_frame_alloc();
                if (!enc_frame) {
                    LOGE("No Memory Space");
                    
                    return -8;
                }
                enc_frame->nb_samples = audio_encoder_ctx->frame_size;
                enc_frame->channels = audio_encoder_ctx->channels;
                enc_frame->channel_layout = audio_encoder_ctx->channel_layout;
                enc_frame->format = audio_encoder_ctx->sample_fmt;
                enc_frame->sample_rate = audio_encoder_ctx->sample_rate;
                
                int ret = av_frame_get_buffer(enc_frame, 0);
                if (ret < 0) {
                    LOGE("No Memory Space");
                    
                    av_frame_free(&enc_frame);
                    
                    return -8;
                }
                
                ret = av_audio_fifo_read(filteredAudioDataFifo, (void **)enc_frame->data, audio_encoder_ctx->frame_size);
                if (ret < 0 || ret>audio_encoder_ctx->frame_size) {
                    LOGE("Audio Fifo Read Fail");
                    
                    av_frame_free(&enc_frame);
                    
                    return -5;
                }
                
                enc_frame->nb_samples = ret;
                enc_frame->pts = next_enc_frame_pts;
                next_enc_frame_pts += 1000000 * enc_frame->nb_samples / enc_frame->sample_rate;
                
                //Encoding filtered audio frame
                int got_packet;
                AVPacket enc_pkt;
                enc_pkt.data = NULL;
                enc_pkt.size = 0;
                av_init_packet(&enc_pkt);
                ret = avcodec_encode_audio2(audio_encoder_ctx, &enc_pkt, enc_frame, &got_packet);
                av_frame_free(&enc_frame);
                
                if (ret < 0) {
                    LOGE("Error while encoding filtered audio frame");
                    
                    return -3;
                }
                
                if (!got_packet) {
                    continue;
                }
                
                //prepare audio packet for muxing
                enc_pkt.stream_index = mOutputAudioStreamIndex;
                av_packet_rescale_ts(&enc_pkt, AV_TIME_BASE_Q, ofmt_ctx->streams[mOutputAudioStreamIndex]->time_base);
                int64_t writeAudioPacketPts = enc_pkt.pts * AV_TIME_BASE * av_q2d(ofmt_ctx->streams[mOutputAudioStreamIndex]->time_base);
                ret = av_interleaved_write_frame(ofmt_ctx, &enc_pkt);
                av_packet_unref(&enc_pkt);
                
                if (ret < 0) {
                    LOGE("Error while muxing audio packet");
                    
                    return -4;
                }
                
                if (!GotBaselineForWriteAudioPacket) {
                    GotBaselineForWriteAudioPacket = true;
                    baseLineForWriteAudioPacket = writeAudioPacketPts;
                }
                
                mCurrentWriteAudioPacketPtsUs = writeAudioPacketPts - baseLineForWriteAudioPacket;
                
                reportWriteProgress(mCurrentWriteAudioPacketPtsUs);
                
                if (hasVideoMaterial) {
                    if (!GotWriteVideoPacketEof) {
                        int ret = flowing_remux_video_l(mCurrentWriteAudioPacketPtsUs);
                        if (ret<0) {
                            if (ret==-7) {
                                GotWriteVideoPacketEof = true;
                            }else{
                                return -6;
                            }
                        }
                    }else{
                        mMediaFrameOutputer->interrupt();
                    }
                }
            }
        }
    }else if (workMediaFrame->type==MEDIA_FRAME_TYPE_EOF)
    {
        workMediaFrame->Free();
        delete workMediaFrame;
        
        //flushing audio filter
        int ret = av_buffersrc_add_frame_flags(audio_buffersrc_ctx, NULL, 0);
        if (ret<0) {
            LOGE("Error while feeding NULL audio frame into the filtergraph");
            return -1;
        }
        
        while (true) {
            //pull filtered audio frames from the filtergraph
            AVFrame *filt_frame = av_frame_alloc();
            if (!filt_frame) {
                LOGE("No Memory Space");
                
                return -8;
            }
            ret = av_buffersink_get_frame(audio_buffersink_ctx, filt_frame);
            if (ret < 0) {
                av_frame_free(&filt_frame);
                
                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
//                    LOGW("no more audio frames for output");
                }else{
                    LOGE("Error while pulling filtered audio frames from the filtergraph");
                }
                
                break;
            }
            
            filt_frame->pict_type = AV_PICTURE_TYPE_NONE;
            filt_frame->pts = filt_frame->pts * mMediaFrameOutputer->getAudioContext().sample_rate/audio_encoder_ctx->sample_rate;
            
            int ret = av_audio_fifo_write(filteredAudioDataFifo, (void **)filt_frame->data, filt_frame->nb_samples);
            if (ret < filt_frame->nb_samples) {
                LOGE("No Memory Space");
                
                av_frame_free(&filt_frame);
                
                return -8;
            }
            int64_t next_enc_frame_pts = filt_frame->pts;
            av_frame_free(&filt_frame);
            
            while (av_audio_fifo_size(filteredAudioDataFifo) >= audio_encoder_ctx->frame_size) {
                AVFrame *enc_frame = av_frame_alloc();
                if (!enc_frame) {
                    LOGE("No Memory Space");
                    
                    return -8;
                }
                enc_frame->nb_samples = audio_encoder_ctx->frame_size;
                enc_frame->channels = audio_encoder_ctx->channels;
                enc_frame->channel_layout = audio_encoder_ctx->channel_layout;
                enc_frame->format = audio_encoder_ctx->sample_fmt;
                enc_frame->sample_rate = audio_encoder_ctx->sample_rate;
                
                int ret = av_frame_get_buffer(enc_frame, 0);
                if (ret < 0) {
                    LOGE("No Memory Space");
                    
                    av_frame_free(&enc_frame);
                    
                    return -8;
                }
                
                ret = av_audio_fifo_read(filteredAudioDataFifo, (void **)enc_frame->data, audio_encoder_ctx->frame_size);
                if (ret < 0 || ret>audio_encoder_ctx->frame_size) {
                    LOGE("Audio Fifo Read Fail");
                    
                    av_frame_free(&enc_frame);
                    
                    return -5;
                }
                
                enc_frame->nb_samples = ret;
                enc_frame->pts = next_enc_frame_pts;
                next_enc_frame_pts += 1000000 * enc_frame->nb_samples / enc_frame->sample_rate;
                
                //Encoding filtered audio frame
                int got_packet;
                AVPacket enc_pkt;
                enc_pkt.data = NULL;
                enc_pkt.size = 0;
                av_init_packet(&enc_pkt);
                ret = avcodec_encode_audio2(audio_encoder_ctx, &enc_pkt, enc_frame, &got_packet);
                av_frame_free(&enc_frame);
                
                if (ret < 0) {
                    LOGE("Error while encoding filtered audio frame");
                    
                    return -3;
                }
                
                if (!got_packet) {
                    continue;
                }
                
                //prepare audio packet for muxing
                enc_pkt.stream_index = mOutputAudioStreamIndex;
                av_packet_rescale_ts(&enc_pkt, AV_TIME_BASE_Q, ofmt_ctx->streams[mOutputAudioStreamIndex]->time_base);
                int64_t writeAudioPacketPts = enc_pkt.pts * AV_TIME_BASE * av_q2d(ofmt_ctx->streams[mOutputAudioStreamIndex]->time_base);
                ret = av_interleaved_write_frame(ofmt_ctx, &enc_pkt);
                av_packet_unref(&enc_pkt);
                
                if (ret < 0) {
                    LOGE("Error while muxing audio packet");
                    
                    return -4;
                }
                
                if (!GotBaselineForWriteAudioPacket) {
                    GotBaselineForWriteAudioPacket = true;
                    baseLineForWriteAudioPacket = writeAudioPacketPts;
                }
                
                mCurrentWriteAudioPacketPtsUs = writeAudioPacketPts - baseLineForWriteAudioPacket;
                
                reportWriteProgress(mCurrentWriteAudioPacketPtsUs);
                
                if (hasVideoMaterial) {
                    if (!GotWriteVideoPacketEof) {
                        int ret = flowing_remux_video_l(mCurrentWriteAudioPacketPtsUs);
                        if (ret<0) {
                            if (ret==-7) {
                                GotWriteVideoPacketEof = true;
                            }else{
                                return -6;
                            }
                        }
                    }
                }
            }
        }
        
        //Flushing audio encoder
        if (audio_encoder_ctx->codec->capabilities & AV_CODEC_CAP_DELAY) {
            while (true) {
                int got_packet;
                AVPacket enc_pkt;
                enc_pkt.data = NULL;
                enc_pkt.size = 0;
                av_init_packet(&enc_pkt);
                ret = avcodec_encode_audio2(audio_encoder_ctx, &enc_pkt, NULL, &got_packet);
                if (ret < 0) {
                    LOGE("Error while flushing the audio encoder");
                    
                    return -3;
                }
                
                if (!got_packet) {
                    break;
                }
                
                //prepare audio packet for muxing
                enc_pkt.stream_index = mOutputAudioStreamIndex;
                av_packet_rescale_ts(&enc_pkt, AV_TIME_BASE_Q, ofmt_ctx->streams[mOutputAudioStreamIndex]->time_base);
                int64_t writeAudioPacketPts = enc_pkt.pts * AV_TIME_BASE * av_q2d(ofmt_ctx->streams[mOutputAudioStreamIndex]->time_base);
                ret = av_interleaved_write_frame(ofmt_ctx, &enc_pkt);
                av_packet_unref(&enc_pkt);
                
                if (ret < 0) {
                    LOGE("Error while muxing audio packet");
                    
                    return -4;
                }
                
                if (!GotBaselineForWriteAudioPacket) {
                    GotBaselineForWriteAudioPacket = true;
                    baseLineForWriteAudioPacket = writeAudioPacketPts;
                }
                
                mCurrentWriteAudioPacketPtsUs = writeAudioPacketPts - baseLineForWriteAudioPacket;
                
                reportWriteProgress(mCurrentWriteAudioPacketPtsUs);
                
                if (hasVideoMaterial) {
                    if (!GotWriteVideoPacketEof) {
                        int ret = flowing_remux_video_l(mCurrentWriteAudioPacketPtsUs);
                        if (ret<0) {
                            if (ret==-7) {
                                GotWriteVideoPacketEof = true;
                            }else{
                                return -6;
                            }
                        }
                    }
                }
            }
        }
        
        if (hasVideoMaterial) {
            if (!GotWriteVideoPacketEof) {
                int ret = flowing_remux_video_l(0,true);
                if (ret<0) {
                    if (ret==-7) {
                        GotWriteVideoPacketEof = true;
                    }else{
                        return -6;
                    }
                }
            }
        }
        
        av_write_trailer(ofmt_ctx);
        
        return -7;
    }
    
    workMediaFrame->Free();
    delete workMediaFrame;
    
    return 0;
}

// -1 : VIDEO DEMUX FAIL
// -2 : VIDEO MUX FAIL
// -7 : VIDEO REMUX EOF
int MediaDubbingProducer::flowing_remux_video_l(int64_t currentWriteAudioPacketPtsUs, bool isRemuxAll)
{
    while ((mCurrentWriteVideoPacketPtsUs <= currentWriteAudioPacketPtsUs) || isRemuxAll) {
        AVPacket pkt;
        int ret = av_read_frame(ifmt_ctx, &pkt);
        if (ret == AVERROR_INVALIDDATA || ret == AVERROR(EAGAIN)) {
            av_packet_unref(&pkt);
            continue;
        }else if (ret == AVERROR_EOF) {
            av_packet_unref(&pkt);
            
            return -7;
        }else if(ret<0){
            av_packet_unref(&pkt);
            
            return -1;
        }else if(pkt.stream_index!=mInputVideoStreamIndex){
            av_packet_unref(&pkt);
            continue;
        }
        
        AVPacket* workVideoPacket = &pkt;
        AVStream *in_video_stream = ifmt_ctx->streams[mInputVideoStreamIndex];
        AVStream *out_video_stream = ofmt_ctx->streams[mOutputVideoStreamIndex];
        workVideoPacket->stream_index = mOutputVideoStreamIndex;
        workVideoPacket->pts = av_rescale_q_rnd(workVideoPacket->pts, in_video_stream->time_base, out_video_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
        workVideoPacket->dts = av_rescale_q_rnd(workVideoPacket->dts, in_video_stream->time_base, out_video_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
//        workVideoPacket->duration = av_rescale_q(workVideoPacket->duration, in_video_stream->time_base, out_video_stream->time_base);
        workVideoPacket->duration = av_rescale_q_rnd(workVideoPacket->duration, in_video_stream->time_base, out_video_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
        if (workVideoPacket->duration<0) {
            workVideoPacket->duration = 0;
        }
        workVideoPacket->pos = -1;
        
        int64_t writeVideoPacketPts = workVideoPacket->pts*AV_TIME_BASE*av_q2d(ofmt_ctx->streams[mOutputVideoStreamIndex]->time_base);
        
        ret = av_interleaved_write_frame(ofmt_ctx, workVideoPacket);
        av_packet_unref(workVideoPacket);
        
        if (ret < 0) {
            LOGE("Error muxing packet");
            return -2;
        }
        
        if (!GotBaselineForWriteVideoPacket) {
            GotBaselineForWriteVideoPacket = true;
            baseLineForWriteVideoPacket = writeVideoPacketPts;
        }
        
        mCurrentWriteVideoPacketPtsUs = writeVideoPacketPts - baseLineForWriteVideoPacket;
        
        reportWriteProgress(mCurrentWriteVideoPacketPtsUs);
    }
    
    return 0;
}

void MediaDubbingProducer::reportWriteProgress(int64_t currentWriteAVPacketPtsUs)
{
    if (currentWriteAVPacketPtsUs/1000000ll-writeTimeStampSecond>=1) {
        writeTimeStampSecond++;
        if (writeTimeStampSecond>mOutputMediaProductDurationSecond) {
            writeTimeStampSecond = mOutputMediaProductDurationSecond;
        }
        notifyListener_l(MEDIA_DUBBING_INFO, MEDIA_DUBBING_INFO_WRITE_TIMESTAMP, writeTimeStampSecond);
        
        int percent = writeTimeStampSecond*100/mOutputMediaProductDurationSecond;
        if (percent<0) {
            percent = 0;
        }
        if (percent>100) {
            percent = 100;
        }
        notifyListener_l(MEDIA_DUBBING_INFO, MEDIA_DUBBING_INFO_WRITE_PERCENT, percent);
    }
}
