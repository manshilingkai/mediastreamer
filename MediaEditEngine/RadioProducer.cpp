//
//  RadioProducer.cpp
//  MediaStreamer
//
//  Created by Think on 2019/8/22.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "RadioProducer.h"

#include "MediaLog.h"
#include "FFLog.h"

#ifdef ANDROID
#include "JniMediaListener.h"
#else
#include "NormalMediaListener.h"
#endif

#include "AutoLock.h"

#include "AudioMixer.h"

#include "MediaTime.h"
#include "PCMUtils.h"

RadioCommandQueue::RadioCommandQueue()
{
    pthread_mutex_init(&mLock, NULL);
}

RadioCommandQueue::~RadioCommandQueue()
{
    pthread_mutex_destroy(&mLock);
}

void RadioCommandQueue::push(RadioCommand* radioCommand)
{
    if (radioCommand==NULL || radioCommand->type==UNKNOWN_RADIO_COMMAND_TYPE) {
        return;
    }
    
    pthread_mutex_lock(&mLock);
    
    mRadioCommandQueue.push(radioCommand);
    
    pthread_mutex_unlock(&mLock);
}

RadioCommand* RadioCommandQueue::pop()
{
    RadioCommand *radioCommand = NULL;
    
    pthread_mutex_lock(&mLock);
    
    if (!mRadioCommandQueue.empty()) {
        radioCommand = mRadioCommandQueue.front();
        mRadioCommandQueue.pop();
    }
    
    pthread_mutex_unlock(&mLock);
    
    return radioCommand;
}

void RadioCommandQueue::flush()
{
    pthread_mutex_lock(&mLock);
    RadioCommand *radioCommand = NULL;
    while (!mRadioCommandQueue.empty()) {
        radioCommand = mRadioCommandQueue.front();
        if (radioCommand!=NULL) {
            radioCommand->Free();
            delete radioCommand;
            radioCommand = NULL;
        }
        mRadioCommandQueue.pop();
    }
    pthread_mutex_unlock(&mLock);
}


struct RadioProducerEvent : public TimedEventQueue::Event {
    RadioProducerEvent(
                              RadioProducer *producer,
                              void (RadioProducer::*method)())
    : mProducer(producer),
    mMethod(method) {
    }
    
protected:
    virtual ~RadioProducerEvent() {}
    
    virtual void fire(TimedEventQueue * /* queue */, int64_t /* now_us */) {
        (mProducer->*mMethod)();
    }
    
private:
    RadioProducer *mProducer;
    void (RadioProducer::*mMethod)();
    
    RadioProducerEvent(const RadioProducerEvent &);
    RadioProducerEvent &operator=(const RadioProducerEvent &);
};

#ifdef ANDROID
RadioProducer::RadioProducer(JavaVM *jvm, AudioRenderConfigure configure, RadioProductOptions options)
{
    init_ffmpeg_env();
    
    mJvm = jvm;
    mAudioRenderConfigure = configure;
    mRadioProductOptions = options;
    if(options.productUrl)
    {
        mRadioProductOptions.productUrl = strdup(options.productUrl);
    }
    if(options.bgProductUrl)
    {
        mRadioProductOptions.bgProductUrl = strdup(options.bgProductUrl);
    }
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);
    
    mCommandEvent = NULL;
    mProcessEvent = NULL;
    mStopEvent = NULL;
    
    mCommandEvent = new RadioProducerEvent(this, &RadioProducer::onCommandEvent);
    mProcessEvent = new RadioProducerEvent(this, &RadioProducer::onProcessEvent);
    mStopEvent = new RadioProducerEvent(this, &RadioProducer::onStopEvent);
    mProcessEventPending = false;
    
    mQueue.registerJavaVMEnv(mJvm);
    mQueue.start();
    
    pthread_cond_init(&mStopCondition, NULL);
    
    mMediaListener = NULL;
    
    gotError = false;
    
    mBgmOutputer = NULL;
    mBgmVolume = mRadioProductOptions.defaultBgmVolume;

    mAudioCaptureType = OPENSLES;
    mAudioCapture = NULL;
    mMicVolume = mRadioProductOptions.defaultMicVolume;
    
    mEffectMusicOutputer = NULL;
    mEffectMusicVolume = mRadioProductOptions.defaultEffectMusicVolume;
    
    mAudioProcess = NULL;
    isEnableEffect = false;
    mUserDefinedEffect = NOEFFECT;
    mEqualizerStyle = NOEQUALIZER;
    mReverbStyle = NOREVERB;
    isEnablePitchSemiTones = false;
    mPitchSemiTonesValue = 0;
    
    mMixOutputBuffer = (char*)malloc(mAudioRenderConfigure.samplesPerFrame*mAudioRenderConfigure.channelCount*av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat));
    mMixBgOutputBuffer = (char*)malloc(mAudioRenderConfigure.samplesPerFrame*mAudioRenderConfigure.channelCount*av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat));
    isReadyForMixOutput = false;
    
    mIsEnableEarReturn = false;
    
    mAudioRenderType = AUDIO_RENDER_OPENSLES;
    mAudioRender = NULL;
    mProductWriter = NULL;
    mBgProductWriter = NULL;
    isWorking = false;
    mCurrentTimeMs = 0;
    mBgmPlayingTimeMs = 0;
    pthread_mutex_init(&mPropertyLock, NULL);
    
    mOutputWaveValue = 0;
    mLastOutputWaveTimeMs = 0;
}
#else
RadioProducer::RadioProducer(AudioRenderConfigure configure, RadioProductOptions options)
{
    init_ffmpeg_env();

    mAudioRenderConfigure = configure;
    mRadioProductOptions = options;
    if(options.productUrl)
    {
        mRadioProductOptions.productUrl = strdup(options.productUrl);
    }
    if(options.bgProductUrl)
    {
        mRadioProductOptions.bgProductUrl = strdup(options.bgProductUrl);
    }
    
    mFlags = 0;
    pthread_mutex_init(&mLock, NULL);
    
    mCommandEvent = NULL;
    mProcessEvent = NULL;
    mStopEvent = NULL;
    
    mCommandEvent = new RadioProducerEvent(this, &RadioProducer::onCommandEvent);
    mProcessEvent = new RadioProducerEvent(this, &RadioProducer::onProcessEvent);
    mStopEvent = new RadioProducerEvent(this, &RadioProducer::onStopEvent);
    mProcessEventPending = false;
    
    mQueue.start();
    
    pthread_cond_init(&mStopCondition, NULL);
    
    mMediaListener = NULL;
    
    gotError = false;
    
    mBgmOutputer = NULL;
    mBgmVolume = mRadioProductOptions.defaultBgmVolume;
    
    mAudioCaptureType = AUDIO_CAPTURE_UNKNOWN;
#ifdef IOS
    mAudioCaptureType = AUDIOUNIT;
#endif
    mAudioCapture = NULL;
    mMicVolume = mRadioProductOptions.defaultMicVolume;
    
    mEffectMusicOutputer = NULL;
    mEffectMusicVolume = mRadioProductOptions.defaultEffectMusicVolume;
    
    mAudioProcess = NULL;
    isEnableEffect = false;
    mUserDefinedEffect = NOEFFECT;
    mEqualizerStyle = NOEQUALIZER;
    mReverbStyle = NOREVERB;
    isEnablePitchSemiTones = false;
    mPitchSemiTonesValue = 0;
    
    mMixOutputBuffer = (char*)malloc(mAudioRenderConfigure.samplesPerFrame*mAudioRenderConfigure.channelCount*av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat));
    mMixBgOutputBuffer = (char*)malloc(mAudioRenderConfigure.samplesPerFrame*mAudioRenderConfigure.channelCount*av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat));
    isReadyForMixOutput = false;
    
    mIsEnableEarReturn = false;

    mAudioRenderType = AUDIO_RENDER_UNKNOWN;
#ifdef IOS
    mAudioRenderType = AUDIO_RENDER_AUDIOUNIT;
#endif
    mAudioRender = NULL;
    mProductWriter = NULL;
    mBgProductWriter = NULL;
    isWorking = false;
    mCurrentTimeMs = 0;
    mBgmPlayingTimeMs = 0;
    pthread_mutex_init(&mPropertyLock, NULL);
    
    mOutputWaveValue = 0;
    mLastOutputWaveTimeMs = 0;
}
#endif

RadioProducer::~RadioProducer()
{
    release();
    
    mQueue.stop(true);
    
    mRadioCommandQueue.flush();
    
    if (mCommandEvent!=NULL) {
        delete mCommandEvent;
        mCommandEvent = NULL;
    }
    
    if (mProcessEvent!=NULL) {
        delete mProcessEvent;
        mProcessEvent = NULL;
    }
    
    if (mStopEvent!=NULL) {
        delete mStopEvent;
        mStopEvent = NULL;
    }
    
    pthread_mutex_destroy(&mPropertyLock);
    
    pthread_cond_destroy(&mStopCondition);
    
    pthread_mutex_destroy(&mLock);
    
#ifdef ANDROID
    JniMediaListener *jniListener = (JniMediaListener*)mMediaListener;
    if (jniListener!=NULL) {
        delete jniListener;
        jniListener = NULL;
    }
#else
    NormalMediaListener* normalMediaListener = (NormalMediaListener*)mMediaListener;
    if (normalMediaListener!=NULL) {
        delete normalMediaListener;
        normalMediaListener = NULL;
    }
#endif
    
    mMediaListener = NULL;
    
    if (mMixOutputBuffer) {
        free(mMixOutputBuffer);
        mMixOutputBuffer = NULL;
    }
    
    if (mMixBgOutputBuffer) {
        free(mMixBgOutputBuffer);
        mMixBgOutputBuffer = NULL;
    }
    
    if (mRadioProductOptions.productUrl) {
        free(mRadioProductOptions.productUrl);
        mRadioProductOptions.productUrl = NULL;
    }
    
    if (mRadioProductOptions.bgProductUrl) {
        free(mRadioProductOptions.bgProductUrl);
        mRadioProductOptions.bgProductUrl = NULL;
    }
}

void RadioProducer::init_ffmpeg_env()
{
    // init ffmpeg env
    av_register_all();
    avcodec_register_all();
    avfilter_register_all();
    
    avformat_network_init();
    FFLog::setLogLevel(AV_LOG_WARNING);
}

#ifdef ANDROID
void RadioProducer::setListener(jobject thiz, jobject weak_thiz, jmethodID post_event)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new JniMediaListener(mJvm, thiz, weak_thiz, post_event);
    
    modifyFlags(INITIALIZED, SET);
}
#else
void RadioProducer::setListener(void (*listener)(void*,int,int,int), void* arg)
{
    AutoLock autoLock(&mLock);
    
    mMediaListener = new NormalMediaListener(listener,arg);
    
    modifyFlags(INITIALIZED, SET);
}
#endif

void RadioProducer::notifyListener_l(int event, int ext1, int ext2)
{
    if (mMediaListener!=NULL) {
        mMediaListener->notify(event, ext1, ext2);
    }
}

void RadioProducer::prepareBgm(char* bgmUrl)
{
    if (bgmUrl==NULL) return;
    
    PrepareBgmCommand* prepareBgmCommand = new PrepareBgmCommand;
    prepareBgmCommand->bgmUrl = strdup(bgmUrl);
    
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = PREPARE_BGM;
    radioCommand->command = prepareBgmCommand;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::prepareBgmWithStartPos(char* bgmUrl, int32_t startPosMs)
{
    if (bgmUrl==NULL) return;
    
    if (startPosMs<0) {
        startPosMs = 0;
    }

    PrepareBgmWithStartPosCommand* prepareBgmWithStartPosCommand = new PrepareBgmWithStartPosCommand;
    prepareBgmWithStartPosCommand->bgmUrl = strdup(bgmUrl);
    prepareBgmWithStartPosCommand->startPosMs = startPosMs;
    
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = PREPARE_BGM_WITH_START_POS;
    radioCommand->command = prepareBgmWithStartPosCommand;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::startBgm()
{
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = START_BGM;
    radioCommand->command = NULL;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::pauseBgm()
{
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = PAUSE_BGM;
    radioCommand->command = NULL;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::stopBgm()
{
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = STOP_BGM;
    radioCommand->command = NULL;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::setBgmVolume(float volume)
{
    if (volume<0.0f) {
        volume = 0.0f;
    }
    
    if (volume>2.0f) {
        volume = 2.0f;
    }
    
    SetBgmVolumeCommand* setBgmVolumeCommand = new SetBgmVolumeCommand;
    setBgmVolumeCommand->volume = volume;
    
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = SET_BGM_VOLUME;
    radioCommand->command = setBgmVolumeCommand;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::startMicrophone()
{
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = START_MICROPHONE;
    radioCommand->command = NULL;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::stopMicrophone()
{
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = STOP_MICROPHONE;
    radioCommand->command = NULL;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::setMicrophoneVolume(float volume)
{
    if (volume<0.0f) {
        volume = 0.0f;
    }
    
    if (volume>2.0f) {
        volume = 2.0f;
    }
    
    SetMicrophoneVolumeCommand* setMicrophoneVolumeCommand = new SetMicrophoneVolumeCommand;
    setMicrophoneVolumeCommand->volume = volume;
    
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = SET_MICROPHONE_VOLUME;
    radioCommand->command = setMicrophoneVolumeCommand;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::prepareEffectMusic(char* effectMusicUrl)
{
    if (effectMusicUrl==NULL) return;
    
    PrepareEffectMusicCommand* prepareEffectMusicCommand = new PrepareEffectMusicCommand;
    prepareEffectMusicCommand->effectMusicUrl = strdup(effectMusicUrl);
    
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = PREPARE_EFFECT_MUSIC;
    radioCommand->command = prepareEffectMusicCommand;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::startEffectMusic()
{
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = START_EFFECT_MUSIC;
    radioCommand->command = NULL;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::pauseEffectMusic()
{
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = PAUSE_EFFECT_MUSIC;
    radioCommand->command = NULL;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::stopEffectMusic()
{
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = STOP_EFFECT_MUSIC;
    radioCommand->command = NULL;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::setEffectMusicVolume(float volume)
{
    if (volume<0.0f) {
        volume = 0.0f;
    }
    
    if (volume>2.0f) {
        volume = 2.0f;
    }
    
    SetEffectMusicVolumeCommand* setEffectMusicVolumeCommand = new SetEffectMusicVolumeCommand;
    setEffectMusicVolumeCommand->volume = volume;
    
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = SET_EFFECT_MUSIC_VOLUME;
    radioCommand->command = setEffectMusicVolumeCommand;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::setAudioUserDefinedEffect(int effect)
{
    SetUserDefinedEffectCommand* setUserDefinedEffectCommand = new SetUserDefinedEffectCommand;
    setUserDefinedEffectCommand->effect = effect;
    
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = SET_USER_DEFINED_EFFECT;
    radioCommand->command = setUserDefinedEffectCommand;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::setAudioEqualizerStyle(int style)
{
    SetEqualizerStyleCommand* setEqualizerStyleCommand = new SetEqualizerStyleCommand;
    setEqualizerStyleCommand->style = style;
    
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = SET_EQUALIZER_STYLE;
    radioCommand->command = setEqualizerStyleCommand;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::setAudioReverbStyle(int style)
{
    SetReverbStyleCommand* setReverbStyleCommand = new SetReverbStyleCommand;
    setReverbStyleCommand->style = style;
    
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = SET_REVERB_STYLE;
    radioCommand->command = setReverbStyleCommand;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::setAudioPitchSemiTones(int value)
{
    SetPitchSemiTonesCommand* setPitchSemiTonesCommand = new SetPitchSemiTonesCommand;
    setPitchSemiTonesCommand->value = value;
    
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = SET_PITCH_SEMITONES;
    radioCommand->command = setPitchSemiTonesCommand;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::setEarReturn(bool isEnableEarReturn)
{
    SetEarReturnCommand* setEarReturnCommand = new SetEarReturnCommand;
    setEarReturnCommand->isEnableEarReturn = isEnableEarReturn;
    
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = SET_EAR_RETURN;
    radioCommand->command = setEarReturnCommand;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::pause(bool isExport)
{
    PauseCommand* pauseCommand = new PauseCommand;
    pauseCommand->isExport = isExport;
    
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = PAUSE;
    radioCommand->command = pauseCommand;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

void RadioProducer::stop()
{
    RadioCommand* radioCommand = new RadioCommand;
    radioCommand->type = STOP;
    radioCommand->command = NULL;
    
    mRadioCommandQueue.push(radioCommand);
    
    mQueue.postEvent(mCommandEvent);
}

int32_t RadioProducer::getCurrentTimeMs()
{
    int32_t ret = 0;
    
    pthread_mutex_lock(&mPropertyLock);
    ret = mCurrentTimeMs;
    pthread_mutex_unlock(&mPropertyLock);
    
    return ret;
}

int32_t RadioProducer::getBgmPlayingTimeMs()
{
    int32_t ret = 0;
    
    pthread_mutex_lock(&mPropertyLock);
    ret = mBgmPlayingTimeMs;
    pthread_mutex_unlock(&mPropertyLock);
    
    return ret;
}

void RadioProducer::modifyFlags(unsigned value, FlagMode mode)
{
    switch (mode) {
        case SET:
            mFlags |= value;
            break;
        case CLEAR:
            mFlags &= ~value;
            break;
        case ASSIGN:
            mFlags = value;
            break;
    }
}

void RadioProducer::postProcessEvent_l(int64_t delayUs)
{
    if (mProcessEventPending) {
        return;
    }
    mProcessEventPending = true;
    mQueue.postEventWithDelay(mProcessEvent, delayUs < 0 ? 0 : delayUs);
}

void RadioProducer::onCommandEvent()
{
    AutoLock autoLock(&mLock);

    RadioCommand* radioCommand = mRadioCommandQueue.pop();
    if (radioCommand==NULL) {
        return;
    }
    
    if (radioCommand->type==PREPARE_BGM) {
        PrepareBgmCommand* prepareBgmCommand = (PrepareBgmCommand*)radioCommand->command;
        prepareBgm_l(prepareBgmCommand->bgmUrl);
    }else if(radioCommand->type==PREPARE_BGM_WITH_START_POS) {
        PrepareBgmWithStartPosCommand* prepareBgmWithStartPosCommand = (PrepareBgmWithStartPosCommand*)radioCommand->command;
        prepareBgmWithStartPos_l(prepareBgmWithStartPosCommand->bgmUrl, prepareBgmWithStartPosCommand->startPosMs);
    }else if (radioCommand->type==START_BGM) {
        startBgm_l();
        process_l();
    }else if (radioCommand->type==PAUSE_BGM) {
        pauseBgm_l();
        if (!isBgmPlaying() && !isMicrophoneRecording() && !isEffectMusicPlaying() && isWorking) {
            pause_l(false);
        }
    }else if (radioCommand->type==STOP_BGM) {
        stopBgm_l();
        if (!isBgmPlaying() && !isMicrophoneRecording() && !isEffectMusicPlaying() && isWorking) {
            pause_l(false);
        }
    }else if (radioCommand->type==SET_BGM_VOLUME) {
        SetBgmVolumeCommand* setBgmVolumeCommand = (SetBgmVolumeCommand*)radioCommand->command;
        setBgmVolume_l(setBgmVolumeCommand->volume);
    }else if(radioCommand->type==START_MICROPHONE) {
        bool ret = startMicrophone_l();
        if(ret){
            process_l();
        }else{
            notifyListener_l(RADIO_PRODUCER_WARN, RADIO_PRODUCER_WARN_START_MICROPHONE_FAIL);
        }
    }else if(radioCommand->type==STOP_MICROPHONE) {
        stopMicrophone_l();
        if (!isBgmPlaying() && !isMicrophoneRecording() && !isEffectMusicPlaying() && isWorking) {
            pause_l(false);
        }
    }else if(radioCommand->type==SET_MICROPHONE_VOLUME) {
        SetMicrophoneVolumeCommand* setMicrophoneVolumeCommand = (SetMicrophoneVolumeCommand*)radioCommand->command;
        mMicVolume = setMicrophoneVolumeCommand->volume;
    }else if(radioCommand->type==PREPARE_EFFECT_MUSIC) {
        PrepareEffectMusicCommand* prepareEffectMusicCommand = (PrepareEffectMusicCommand*)radioCommand->command;
        prepareEffectMusic_l(prepareEffectMusicCommand->effectMusicUrl);
    }else if(radioCommand->type==START_EFFECT_MUSIC) {
        startEffectMusic_l();
        process_l();
    }else if(radioCommand->type==PAUSE_EFFECT_MUSIC) {
        pauseEffectMusic_l();
        if (!isBgmPlaying() && !isMicrophoneRecording() && !isEffectMusicPlaying() && isWorking) {
            pause_l(false);
        }
    }else if(radioCommand->type==STOP_EFFECT_MUSIC) {
        stopEffectMusic_l();
        if (!isBgmPlaying() && !isMicrophoneRecording() && !isEffectMusicPlaying() && isWorking) {
            pause_l(false);
        }
    }else if(radioCommand->type==SET_EFFECT_MUSIC_VOLUME) {
        SetEffectMusicVolumeCommand* setEffectMusicVolumeCommand = (SetEffectMusicVolumeCommand*)radioCommand->command;
        setEffectMusicVolume_l(setEffectMusicVolumeCommand->volume);
    }else if(radioCommand->type==SET_USER_DEFINED_EFFECT) {
        SetUserDefinedEffectCommand* setUserDefinedEffectCommand = (SetUserDefinedEffectCommand*)radioCommand->command;
        setAudioUserDefinedEffect_l(setUserDefinedEffectCommand->effect);
    }else if(radioCommand->type==SET_EQUALIZER_STYLE) {
        SetEqualizerStyleCommand* setEqualizerStyleCommand = (SetEqualizerStyleCommand*)radioCommand->command;
        setAudioEqualizerStyle_l(setEqualizerStyleCommand->style);
    }else if(radioCommand->type==SET_REVERB_STYLE) {
        SetReverbStyleCommand* setReverbStyleCommand = (SetReverbStyleCommand*)radioCommand->command;
        setAudioReverbStyle_l(setReverbStyleCommand->style);
    }else if(radioCommand->type==SET_PITCH_SEMITONES) {
        SetPitchSemiTonesCommand* setPitchSemiTonesCommand = (SetPitchSemiTonesCommand*)radioCommand->command;
        setAudioPitchSemiTones_l(setPitchSemiTonesCommand->value);
    }else if(radioCommand->type==SET_EAR_RETURN) {
        SetEarReturnCommand* setEarReturnCommand = (SetEarReturnCommand*)radioCommand->command;
        setEarReturn_l(setEarReturnCommand->isEnableEarReturn);
    }else if(radioCommand->type==PAUSE) {
        PauseCommand* pauseCommand = (PauseCommand*)radioCommand->command;
        pause_l(pauseCommand->isExport);
    }else if(radioCommand->type==STOP) {
        stop_l();
    }
    
    if (radioCommand!=NULL) {
        radioCommand->Free();
        delete radioCommand;
        radioCommand = NULL;
    }
}

void RadioProducer::onProcessEvent()
{
    AutoLock autoLock(&mLock);
    
    if (!mProcessEventPending) {
        // The event has been cancelled in reset_l() but had already // been scheduled for execution at that time.
        return;
    }
    mProcessEventPending = false;
    
    if (!isReadyForMixOutput) {
        if (!mBgmOutputer && !mAudioCapture && !mEffectMusicOutputer) {
            return;
        }
        
        //BGM
        AudioPlayerOutputPcm* bgmOutputPcm = NULL;
        if (mBgmOutputer) {
            AudioPlayerOutput* bgmOutput = mBgmOutputer->drainFromExternal();
            if (bgmOutput) {
                if (bgmOutput->type==AUDIO_PLAYER_OUTPUT_ERROR) {
                    bgmOutput->Free();
                    delete bgmOutput;
                    bgmOutput = NULL;
                    
                    stopBgm_l();
                    
                    notifyListener_l(RADIO_PRODUCER_WARN,RADIO_PRODUCER_WARN_BGM_OUTPUT_FAIL);
                }else if (bgmOutput->type==AUDIO_PLAYER_OUTPUT_EOF) {
                    bgmOutput->Free();
                    delete bgmOutput;
                    bgmOutput = NULL;
                    
                    stopBgm_l();
                    
                    notifyListener_l(RADIO_PRODUCER_BGM_PLAY_TO_END);
                }else if (bgmOutput->type==AUDIO_PLAYER_OUTPUT_PCM) {
                    bgmOutputPcm = (AudioPlayerOutputPcm*)bgmOutput->output;
                    delete bgmOutput;
                    bgmOutput = NULL;
                }
            }
            
            int32_t bgmPlayingTimeMs = mBgmOutputer->getPlayTimeMs();

            pthread_mutex_lock(&mPropertyLock);
            mBgmPlayingTimeMs = bgmPlayingTimeMs;
            pthread_mutex_unlock(&mPropertyLock);
        }
        
        //MIC
        AudioFrame* micAudioFrame = NULL;
        if (mAudioCapture) {
            micAudioFrame = mAudioCapture->frontAudioFrame();
            if (micAudioFrame && micAudioFrame->data) {
                //Set Volume
                if (mMicVolume!=1.0f) {
                    int16_t* pData = (int16_t*)micAudioFrame->data;
                    for (int i = 0; i<micAudioFrame->frameSize/2; i++) {
                        pData[i] = fixSample(pData[i]*mMicVolume);
                    }
                }
                //Audio Process
                if (mAudioProcess) {
                    mAudioProcess->RenderAudio(micAudioFrame->data);
                }
            }
        }
        
        //EFFECT MUSIC
        AudioPlayerOutputPcm* effectMusicOutputPcm = NULL;
        if (mEffectMusicOutputer) {
            AudioPlayerOutput* effectMusicOutput = mEffectMusicOutputer->drainFromExternal();
            if (effectMusicOutput) {
                if (effectMusicOutput->type==AUDIO_PLAYER_OUTPUT_ERROR) {
                    effectMusicOutput->Free();
                    delete effectMusicOutput;
                    effectMusicOutput = NULL;
                    
                    stopEffectMusic_l();
                    
                    notifyListener_l(RADIO_PRODUCER_WARN, RADIO_PRODUCER_WARN_EFFECT_MUSIC_OUTPUT_FAIL);
                }else if (effectMusicOutput->type==AUDIO_PLAYER_OUTPUT_EOF) {
                    effectMusicOutput->Free();
                    delete effectMusicOutput;
                    effectMusicOutput = NULL;
                    
                    stopEffectMusic_l();
                    
                    notifyListener_l(RADIO_PRODUCER_EFFECT_MUSIC_PLAY_TO_END);
                }else if(effectMusicOutput->type==AUDIO_PLAYER_OUTPUT_PCM) {
                    effectMusicOutputPcm = (AudioPlayerOutputPcm*)effectMusicOutput->output;
                    delete effectMusicOutput;
                    effectMusicOutput = NULL;
                }
            }
        }
        
        //Mix
        char* micBuffer = NULL;
        char* bgmBuffer = NULL;
        char* effectMusicBuffer = NULL;
        char* outputBufferPointer = NULL;
        char* outputBgBufferPointer = NULL;
        
        if (micAudioFrame && micAudioFrame->data) {
            micBuffer = (char*)micAudioFrame->data;
        }
        
        if (bgmOutputPcm && bgmOutputPcm->data) {
            bgmBuffer = (char*)bgmOutputPcm->data;
        }
        
        if (effectMusicOutputPcm && effectMusicOutputPcm->data) {
            effectMusicBuffer = (char*)effectMusicOutputPcm->data;
        }
        
        mix(micBuffer, bgmBuffer, effectMusicBuffer, &outputBufferPointer, &outputBgBufferPointer);
        
        if (bgmOutputPcm) {
            bgmOutputPcm->Free();
            delete bgmOutputPcm;
            bgmOutputPcm = NULL;
        }
        
        if (effectMusicOutputPcm) {
            effectMusicOutputPcm->Free();
            delete effectMusicOutputPcm;
            effectMusicOutputPcm = NULL;
        }
        
        if (!micBuffer && !bgmBuffer && !effectMusicBuffer) {
            postProcessEvent_l(10*1000);
            return;
        }
        
        isReadyForMixOutput = true;
    }
    
    //Render
    int mixOutputSize = mAudioRenderConfigure.samplesPerFrame*mAudioRenderConfigure.channelCount*av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat);
    if (mAudioRender) {
        int ret = -1;
        if (mIsEnableEarReturn) {
            ret = mAudioRender->pushPCMData((uint8_t*)mMixOutputBuffer, mixOutputSize, 0);
        }else{
            ret = mAudioRender->pushPCMData((uint8_t*)mMixBgOutputBuffer, mixOutputSize, 0);
        }
        
        if (ret==1) {
            postProcessEvent_l(10*1000);
            return;
        }
    }
    
    //Output Wave Value
    if (GetNowMs()-mLastOutputWaveTimeMs>=mRadioProductOptions.outWaveValueIntervalMs) {
        mLastOutputWaveTimeMs = GetNowMs();
        
        mOutputWaveValue = PCMUtils::getPcmDB((unsigned char *)mMixOutputBuffer, mixOutputSize);
        
        notifyListener_l(RADIO_PRODUCER_OUT_WAVE_VALUE, mOutputWaveValue);
    }
    
    //Write
    if (mProductWriter) {
        mProductWriter->putPcmData(mMixOutputBuffer, mixOutputSize);
        int32_t currentTimeMs = mProductWriter->getWriteTimeMs();
        pthread_mutex_lock(&mPropertyLock);
        mCurrentTimeMs = currentTimeMs;
        pthread_mutex_unlock(&mPropertyLock);
    }
    if (mBgProductWriter) {
        mBgProductWriter->putPcmData(mMixBgOutputBuffer, mixOutputSize);
    }
    
    isReadyForMixOutput = false;
    
    postProcessEvent_l();
}

void RadioProducer::onStopEvent()
{
    AutoLock autoLock(&mLock);
    
    mQueue.cancelEvent(mProcessEvent->eventID());
    mProcessEventPending = false;
    
    stopBgm_l();
    stopMicrophone_l();
    stopEffectMusic_l();
    
    if (mAudioProcess) {
        delete mAudioProcess;
        mAudioProcess = NULL;
    }
    
    if (mAudioRender) {
        AudioRender::DeleteAudioRender(mAudioRender, mAudioRenderType);
        mAudioRender = NULL;
    }
    
    if (mProductWriter) {
        delete mProductWriter;
        mProductWriter = NULL;
    }
    
    if (mBgProductWriter) {
        delete mBgProductWriter;
        mBgProductWriter = NULL;
    }
    
    pthread_mutex_lock(&mPropertyLock);
    mCurrentTimeMs = 0;
    mBgmPlayingTimeMs = 0;
    pthread_mutex_unlock(&mPropertyLock);
    
    isWorking = false;
    
    cancelProducerEvents();
    mRadioCommandQueue.flush();
    
    modifyFlags(END, ASSIGN);
    
    pthread_cond_broadcast(&mStopCondition);
}

void RadioProducer::cancelProducerEvents()
{
    mQueue.cancelEvent(mProcessEvent->eventID());
    mProcessEventPending = false;
    mQueue.cancelEvent(mCommandEvent->eventID());
    mQueue.cancelEvent(mStopEvent->eventID());
}

void RadioProducer::prepareBgm_l(char* bgmUrl)
{
    if (mBgmOutputer) {
        delete mBgmOutputer;
        mBgmOutputer = NULL;
    }
    
#ifdef ANDROID
    mBgmOutputer = new AudioPlayer(mJvm, AUDIO_PLAYER_EXTERNAL_RENDER, mAudioRenderConfigure);
#else
    mBgmOutputer = new AudioPlayer(AUDIO_PLAYER_EXTERNAL_RENDER, mAudioRenderConfigure);
#endif
    
    mBgmOutputer->setDataSource(bgmUrl);
    mBgmOutputer->setVolume(mBgmVolume);
    mBgmOutputer->prepare();
    
    pthread_mutex_lock(&mPropertyLock);
    mBgmPlayingTimeMs = 0;
    pthread_mutex_unlock(&mPropertyLock);
}

void RadioProducer::prepareBgmWithStartPos_l(char* bgmUrl, int32_t startPosMs)
{
    if (mBgmOutputer) {
        delete mBgmOutputer;
        mBgmOutputer = NULL;
    }
    
#ifdef ANDROID
    mBgmOutputer = new AudioPlayer(mJvm, AUDIO_PLAYER_EXTERNAL_RENDER, mAudioRenderConfigure);
#else
    mBgmOutputer = new AudioPlayer(AUDIO_PLAYER_EXTERNAL_RENDER, mAudioRenderConfigure);
#endif
    
    mBgmOutputer->setDataSource(bgmUrl);
    mBgmOutputer->setVolume(mBgmVolume);
    mBgmOutputer->prepare();
    mBgmOutputer->seekTo(startPosMs);
    
    pthread_mutex_lock(&mPropertyLock);
    mBgmPlayingTimeMs = startPosMs;
    pthread_mutex_unlock(&mPropertyLock);
}

void RadioProducer::startBgm_l()
{
    if (mBgmOutputer) {
        mBgmOutputer->play();
    }
}

void RadioProducer::pauseBgm_l()
{
    if (mBgmOutputer) {
        mBgmOutputer->pause();
    }
}

void RadioProducer::stopBgm_l()
{
    if (mBgmOutputer) {
        mBgmOutputer->stop();
        delete mBgmOutputer;
        mBgmOutputer = NULL;
    }
    
    pthread_mutex_lock(&mPropertyLock);
    mBgmPlayingTimeMs = 0;
    pthread_mutex_unlock(&mPropertyLock);
}

void RadioProducer::setBgmVolume_l(float volume)
{
    mBgmVolume = volume;
    
    if (mBgmOutputer) {
        mBgmOutputer->setVolume(mBgmVolume);
    }
}

bool RadioProducer::isBgmPlaying()
{
    bool ret = false;
    
    if (mBgmOutputer) {
        ret = mBgmOutputer->isPlaying();
    }
    
    return ret;
}

bool RadioProducer::startMicrophone_l()
{
    if (mAudioCapture) {
        AudioCapture::DeleteAudioCapture(mAudioCapture, mAudioCaptureType);
        mAudioCapture = NULL;
    }
    
    mAudioCapture = AudioCapture::CreateAudioCapture(mAudioCaptureType, NULL, mAudioRenderConfigure.sampleRate, mAudioRenderConfigure.channelCount);
#ifdef ANDROID
    mAudioCapture->registerJavaVMEnv(mJvm);
#endif
    mAudioCapture->setRecBufSizeInBytes(mAudioRenderConfigure.samplesPerFrame*mAudioRenderConfigure.channelCount*av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat));

    int result = mAudioCapture->Init();
    if (result!=0) {
        AudioCapture::DeleteAudioCapture(mAudioCapture, mAudioCaptureType);
        mAudioCapture = NULL;
        
        LOGE("Init AudioCapture Fail");
        return false;
    }
    
    result = mAudioCapture->StartRecording();
    if (result!=0) {
        AudioCapture::DeleteAudioCapture(mAudioCapture, mAudioCaptureType);
        mAudioCapture = NULL;
        
        LOGE("Start AudioCapture Fail");
        return false;
    }
    
    return true;
}

void RadioProducer::stopMicrophone_l()
{
    if (mAudioCapture) {
        AudioCapture::DeleteAudioCapture(mAudioCapture, mAudioCaptureType);
        mAudioCapture = NULL;
    }
}

bool RadioProducer::isMicrophoneRecording()
{
    bool ret = false;
    
    if (mAudioCapture) {
        ret = mAudioCapture->Recording();
    }
    
    return ret;
}

void RadioProducer::prepareEffectMusic_l(char* effectMusicUrl)
{
    if (mEffectMusicOutputer) {
        delete mEffectMusicOutputer;
        mEffectMusicOutputer = NULL;
    }
    
#ifdef ANDROID
    mEffectMusicOutputer = new AudioPlayer(mJvm, AUDIO_PLAYER_EXTERNAL_RENDER, mAudioRenderConfigure);
#else
    mEffectMusicOutputer = new AudioPlayer(AUDIO_PLAYER_EXTERNAL_RENDER, mAudioRenderConfigure);
#endif
    
    mEffectMusicOutputer->setDataSource(effectMusicUrl);
    mEffectMusicOutputer->setVolume(mEffectMusicVolume);
    mEffectMusicOutputer->prepare();
}

void RadioProducer::startEffectMusic_l()
{
    if (mEffectMusicOutputer) {
        mEffectMusicOutputer->play();
    }
}

void RadioProducer::pauseEffectMusic_l()
{
    if (mEffectMusicOutputer) {
        mEffectMusicOutputer->pause();
    }
}

void RadioProducer::stopEffectMusic_l()
{
    if (mEffectMusicOutputer) {
        mEffectMusicOutputer->stop();
        delete mEffectMusicOutputer;
        mEffectMusicOutputer = NULL;
    }
}

void RadioProducer::setEffectMusicVolume_l(float volume)
{
    mEffectMusicVolume = volume;
    
    if (mEffectMusicOutputer) {
        mEffectMusicOutputer->setVolume(mEffectMusicVolume);
    }
}

bool RadioProducer::isEffectMusicPlaying()
{
    bool ret = false;
    
    if (mEffectMusicOutputer) {
        ret = mEffectMusicOutputer->isPlaying();
    }
    
    return ret;
}

void RadioProducer::setAudioUserDefinedEffect_l(int effect)
{
    if (effect==mUserDefinedEffect) {
        return;
    }
    
    if (effect==NOEFFECT) {
        isEnableEffect = false;
    }else{
        isEnableEffect = true;
        mUserDefinedEffect = (UserDefinedEffect)effect;
        mEqualizerStyle = NOEQUALIZER;
        mReverbStyle = NOREVERB;
    }
    
    if (mAudioProcess==NULL) {
        mAudioProcess = new AudioProcess(mAudioRenderConfigure.channelCount, mAudioRenderConfigure.sampleRate, mAudioRenderConfigure.samplesPerFrame);
    }
    
    //configure
    if (isEnableEffect) {
        mAudioProcess->EnableAudioEffect(true);
        mAudioProcess->SetUserDefineEffect(mUserDefinedEffect);
        mAudioProcess->SetEqualizerStyle(mEqualizerStyle);
        mAudioProcess->SetReverbStyle(mReverbStyle);
    }else{
        mAudioProcess->EnableAudioEffect(false);
    }
    
    if (isEnablePitchSemiTones) {
        mAudioProcess->EnableSoundTouch(true);
        mAudioProcess->SetSoundTouch(mPitchSemiTonesValue);
    }else{
        mAudioProcess->EnableSoundTouch(false);
    }
}

void RadioProducer::setAudioEqualizerStyle_l(int style)
{
    if (style==mEqualizerStyle) {
        return;
    }
    
    if (style==NOEQUALIZER) {
        isEnableEffect = false;
    }else {
        isEnableEffect = true;
        mUserDefinedEffect = NOEFFECT;
        mEqualizerStyle = (EqualizerStyle)style;
        mReverbStyle = NOREVERB;
    }
    
    if (mAudioProcess==NULL) {
        mAudioProcess = new AudioProcess(mAudioRenderConfigure.channelCount, mAudioRenderConfigure.sampleRate, mAudioRenderConfigure.samplesPerFrame);
    }
    
    //configure
    if (isEnableEffect) {
        mAudioProcess->EnableAudioEffect(true);
        mAudioProcess->SetUserDefineEffect(mUserDefinedEffect);
        mAudioProcess->SetEqualizerStyle(mEqualizerStyle);
        mAudioProcess->SetReverbStyle(mReverbStyle);
    }else{
        mAudioProcess->EnableAudioEffect(false);
    }
    
    if (isEnablePitchSemiTones) {
        mAudioProcess->EnableSoundTouch(true);
        mAudioProcess->SetSoundTouch(mPitchSemiTonesValue);
    }else{
        mAudioProcess->EnableSoundTouch(false);
    }
}

void RadioProducer::setAudioReverbStyle_l(int style)
{
    if (style == mReverbStyle) {
        return;
    }
    
    if (style==NOREVERB) {
        isEnableEffect = false;
    }else{
        isEnableEffect = true;
        mUserDefinedEffect = NOEFFECT;
        mEqualizerStyle = NOEQUALIZER;
        mReverbStyle = (ReverbStyle)style;
    }
    
    if (mAudioProcess==NULL) {
        mAudioProcess = new AudioProcess(mAudioRenderConfigure.channelCount, mAudioRenderConfigure.sampleRate, mAudioRenderConfigure.samplesPerFrame);
    }
    
    //configure
    if (isEnableEffect) {
        mAudioProcess->EnableAudioEffect(true);
        mAudioProcess->SetUserDefineEffect(mUserDefinedEffect);
        mAudioProcess->SetEqualizerStyle(mEqualizerStyle);
        mAudioProcess->SetReverbStyle(mReverbStyle);
    }else{
        mAudioProcess->EnableAudioEffect(false);
    }
    
    if (isEnablePitchSemiTones) {
        mAudioProcess->EnableSoundTouch(true);
        mAudioProcess->SetSoundTouch(mPitchSemiTonesValue);
    }else{
        mAudioProcess->EnableSoundTouch(false);
    }
}

void RadioProducer::setAudioPitchSemiTones_l(int value)
{
    if (value==mPitchSemiTonesValue) {
        return;
    }
    
    if (value==0) {
        isEnablePitchSemiTones = false;
    }else{
        isEnablePitchSemiTones = true;
        mPitchSemiTonesValue = value;
    }
    
    if (mAudioProcess==NULL) {
        mAudioProcess = new AudioProcess(mAudioRenderConfigure.channelCount, mAudioRenderConfigure.sampleRate, mAudioRenderConfigure.samplesPerFrame);
    }
    
    //configure
    if (isEnableEffect) {
        mAudioProcess->EnableAudioEffect(true);
        mAudioProcess->SetUserDefineEffect(mUserDefinedEffect);
        mAudioProcess->SetEqualizerStyle(mEqualizerStyle);
        mAudioProcess->SetReverbStyle(mReverbStyle);
    }else{
        mAudioProcess->EnableAudioEffect(false);
    }
    
    if (isEnablePitchSemiTones) {
        mAudioProcess->EnableSoundTouch(true);
        mAudioProcess->SetSoundTouch(mPitchSemiTonesValue);
    }else{
        mAudioProcess->EnableSoundTouch(false);
    }
}

void RadioProducer::mix(char* micBuffer, char* bgmBuffer, char* effectMusicBuffer, char** mixOutBuffer, char** mixBgOutBuffer)
{
    int16_t* mic_buffer = (int16_t*)micBuffer;
    int16_t* bgm_buffer = (int16_t*)bgmBuffer;
    int16_t* effect_music_buffer = (int16_t*)effectMusicBuffer;
    int mixSampleCount = mAudioRenderConfigure.samplesPerFrame*mAudioRenderConfigure.channelCount;
    int16_t* mixOutputBuffer = (int16_t*)mMixOutputBuffer;
    int16_t* mixBgOutputBuffer = (int16_t*)mMixBgOutputBuffer;
    
    for (int i = 0; i<mixSampleCount; i++) {
        int16_t mic = mic_buffer?mic_buffer[i]:0;
        int16_t bgm = bgm_buffer?bgm_buffer[i]:0;
        int16_t effect_music = effect_music_buffer?effect_music_buffer[i]:0;
        mixBgOutputBuffer[i] = fixSample(mixSample(bgm, effect_music));
        mixOutputBuffer[i] = fixSample(mixSample(mic, mixBgOutputBuffer[i]));
    }
    
    if (NULL != mixOutBuffer)
    {
        *mixOutBuffer = mMixOutputBuffer;
    }
    if (NULL != mixBgOutBuffer)
    {
        *mixBgOutBuffer = mMixBgOutputBuffer;
    }
}

void RadioProducer::setEarReturn_l(bool isEnableEarReturn)
{
    mIsEnableEarReturn = isEnableEarReturn;
}

void RadioProducer::process_l()
{
    if (isWorking) {
        return;
    }
    
    if (mAudioProcess==NULL) {
        mAudioProcess = new AudioProcess(mAudioRenderConfigure.channelCount, mAudioRenderConfigure.sampleRate, mAudioRenderConfigure.samplesPerFrame);
        //configure
        if (isEnableEffect) {
            mAudioProcess->EnableAudioEffect(true);
            mAudioProcess->SetUserDefineEffect(mUserDefinedEffect);
            mAudioProcess->SetEqualizerStyle(mEqualizerStyle);
            mAudioProcess->SetReverbStyle(mReverbStyle);
        }else{
            mAudioProcess->EnableAudioEffect(false);
        }
        
        if (isEnablePitchSemiTones) {
            mAudioProcess->EnableSoundTouch(true);
            mAudioProcess->SetSoundTouch(mPitchSemiTonesValue);
        }else{
            mAudioProcess->EnableSoundTouch(false);
        }
    }
    
    if (mAudioRender==NULL) {
#ifdef ANDROID
        //AUDIO_RENDER_AUDIOTRACK or AUDIO_RENDER_OPENSLES
        if (mAudioRenderType==AUDIO_RENDER_AUDIOTRACK) {
            mAudioRender = AudioRender::CreateAudioRenderWithJniEnv(AUDIO_RENDER_AUDIOTRACK, mJvm);
        }else{
            mAudioRender = AudioRender::CreateAudioRender(mAudioRenderType);
        }
#else
        mAudioRender = AudioRender::CreateAudioRender(mAudioRenderType);
#endif
        
        mAudioRender->configureAdaptation(mAudioRenderConfigure.sampleRate, mAudioRenderConfigure.channelCount);
        int result = mAudioRender->init(PLAYER_MODE);
        if (result!=0) {
            if (mAudioProcess) {
                delete mAudioProcess;
                mAudioProcess = NULL;
            }
            
            AudioRender::DeleteAudioRender(mAudioRender, mAudioRenderType);
            mAudioRender = NULL;
            
            stop_l();

            LOGE("Init AudioRender Fail");
            notifyListener_l(RADIO_PRODUCER_ERROR, RADIO_PRODUCER_ERROR_AUDIO_RENDER_INIT_FAIL);
            return;
        }
        
        result = mAudioRender->startPlayout();
        if (result!=0) {
            if (mAudioProcess) {
                delete mAudioProcess;
                mAudioProcess = NULL;
            }
            
            AudioRender::DeleteAudioRender(mAudioRender, mAudioRenderType);
            mAudioRender = NULL;
            
            stop_l();
            
            LOGE("Start AudioRender Fail");
            notifyListener_l(RADIO_PRODUCER_ERROR, RADIO_PRODUCER_ERROR_AUDIO_RENDER_START_FAIL);
            return;
        }
    }
    
    if (mProductWriter==NULL) {
        mProductWriter = new WAVFileWriter(mAudioRenderConfigure.channelCount,mAudioRenderConfigure.sampleRate,mRadioProductOptions.productUrl);
        bool ret = mProductWriter->open();
        if (!ret) {
            if (mAudioProcess) {
                delete mAudioProcess;
                mAudioProcess = NULL;
            }
            
            if (mAudioRender) {
                AudioRender::DeleteAudioRender(mAudioRender, mAudioRenderType);
                mAudioRender = NULL;
            }
            
            if (mProductWriter) {
                delete mProductWriter;
                mProductWriter = NULL;
            }
            
            stop_l();
            
            LOGE("Open Product Writer Fail");
            notifyListener_l(RADIO_PRODUCER_ERROR, RADIO_PRODUCER_ERROR_OPEN_PRODUCT_WRITER_FAIL);
            return;
        }
    }
    
    if (mBgProductWriter==NULL) {
        mBgProductWriter = new WAVFileWriter(mAudioRenderConfigure.channelCount,mAudioRenderConfigure.sampleRate,mRadioProductOptions.bgProductUrl);
        bool ret = mBgProductWriter->open();
        if (!ret) {
            if (mAudioProcess) {
                delete mAudioProcess;
                mAudioProcess = NULL;
            }
            
            if (mAudioRender) {
                AudioRender::DeleteAudioRender(mAudioRender, mAudioRenderType);
                mAudioRender = NULL;
            }
            
            if (mProductWriter) {
                delete mProductWriter;
                mProductWriter = NULL;
            }
            
            if (mBgProductWriter) {
                delete mBgProductWriter;
                mBgProductWriter = NULL;
            }
            
            stop_l();
            
            LOGE("Open BG Product Writer Fail");
            notifyListener_l(RADIO_PRODUCER_ERROR, RADIO_PRODUCER_ERROR_OPEN_BG_PRODUCT_WRITER_FAIL);
            return;
        }
    }
    
    postProcessEvent_l();
    
    isWorking = true;
}

void RadioProducer::pause_l(bool isExport)
{
    if (!isWorking) {
        return;
    }
    
    mQueue.cancelEvent(mProcessEvent->eventID());
    mProcessEventPending = false;
    
    pauseBgm_l();
    stopMicrophone_l();
    pauseEffectMusic_l();
    
    if (isExport) {
        if (mProductWriter && mBgProductWriter) {
            if (mProductWriter) {
                delete mProductWriter;
                mProductWriter = NULL;
            }
            
            if (mBgProductWriter) {
                delete mBgProductWriter;
                mBgProductWriter = NULL;
            }
            
            notifyListener_l(RADIO_PRODUCER_EXPORT_PRODUCT,1);
        }else{
            if (mProductWriter) {
                delete mProductWriter;
                mProductWriter = NULL;
            }
            
            if (mBgProductWriter) {
                delete mBgProductWriter;
                mBgProductWriter = NULL;
            }
            
            notifyListener_l(RADIO_PRODUCER_EXPORT_PRODUCT,0);
        }
        
        if (mAudioRender) {
            AudioRender::DeleteAudioRender(mAudioRender, mAudioRenderType);
            mAudioRender = NULL;
        }
        
        isReadyForMixOutput = false;
        
        pthread_mutex_lock(&mPropertyLock);
        mCurrentTimeMs = 0;
        pthread_mutex_unlock(&mPropertyLock);
    }
    
    isWorking = false;
}

void RadioProducer::stop_l()
{
    mQueue.cancelEvent(mProcessEvent->eventID());
    mProcessEventPending = false;
    
    stopBgm_l();
    stopMicrophone_l();
    stopEffectMusic_l();
    
    if (mAudioProcess) {
        delete mAudioProcess;
        mAudioProcess = NULL;
    }
    
    if (mAudioRender) {
        AudioRender::DeleteAudioRender(mAudioRender, mAudioRenderType);
        mAudioRender = NULL;
    }
    
    if (mProductWriter) {
        delete mProductWriter;
        mProductWriter = NULL;
    }
    
    if (mBgProductWriter) {
        delete mBgProductWriter;
        mBgProductWriter = NULL;
    }
    
    isReadyForMixOutput = false;
    
    pthread_mutex_lock(&mPropertyLock);
    mCurrentTimeMs = 0;
    mBgmPlayingTimeMs = 0;
    pthread_mutex_unlock(&mPropertyLock);
    
    mOutputWaveValue = 0;
    mLastOutputWaveTimeMs = 0;
    
    isWorking = false;
}

void RadioProducer::release()
{
    AutoLock autoLock(&mLock);
    
    if (mFlags & END) {
        return;
    }
    
    if (mFlags & ENDING) {
        LOGW("already ending");
        
        pthread_cond_wait(&mStopCondition, &mLock);
        
        return;
    }
    
    modifyFlags(ENDING, ASSIGN);
    mQueue.postEventWithDelay(mStopEvent, 0);
    
    pthread_cond_wait(&mStopCondition, &mLock);
}
