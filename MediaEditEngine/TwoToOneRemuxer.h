//
//  TwoToOneRemuxer.h
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/16.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef TwoToOneRemuxer_h
#define TwoToOneRemuxer_h

#include <stdio.h>
#include <stdlib.h>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libavutil/audio_fifo.h>
}

#include "TimedEventQueue.h"
#include "IMediaListener.h"
#include "NotificationQueue.h"

#ifdef ANDROID
#include "jni.h"
#endif

#include "TwoToOneRemuxerCommon.h"

class TwoToOneRemuxer {
public:
#ifdef ANDROID
    TwoToOneRemuxer(JavaVM *jvm, TTORVideoMaterial videoMaterial, TTORAudioMaterial audioMaterial, TTORProduct product);
#else
    TwoToOneRemuxer(TTORVideoMaterial videoMaterial, TTORAudioMaterial audioMaterial, TTORProduct product);
#endif

    ~TwoToOneRemuxer();

#ifdef ANDROID
    void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event);
#else
    void setListener(void (*listener)(void*,int,int,int), void* arg);
#endif
    
    void start();
    void resume();
    void pause();
    void stop(bool isCancel = false);
    
    void notify(int event, int ext1, int ext2);
private:
    enum {
        CONNECTING           = 0x01,
        CONNECTED            = 0x02,
        STREAMING            = 0x04,
        PAUSED               = 0x10,
        ERROR                = 0x20,
        END                  = 0x40,
        INITIALIZED          = 0x80,
        ENDING               = 0x100,
    };
    unsigned int mFlags;
    enum FlagMode {
        SET,
        CLEAR,
        ASSIGN
    };
    void modifyFlags(unsigned value, FlagMode mode);
private:
    friend struct TwoToOneRemuxerEvent;

    TwoToOneRemuxer(const TwoToOneRemuxer &);
    TwoToOneRemuxer &operator=(const TwoToOneRemuxer &);
    
    pthread_mutex_t mLock;

    TimedEventQueue mQueue;

    TimedEventQueue::Event *mAsyncPrepareEvent;
    TimedEventQueue::Event *mRemuxEvent;
    TimedEventQueue::Event *mStopEvent;
    TimedEventQueue::Event *mNotifyEvent;
    bool mRemuxEventPending;
    
    void prepareAsync_l();
    void play_l();
    void pause_l();
    void stop_l();

    void postRemuxEvent_l(int64_t delayUs = -1);
    void postNotifyEvent_l();
    
    void onPrepareAsyncEvent();
    void onRemuxEvent();
    void onStopEvent();
    void onNotifyEvent();
    
    void cancelRemuxerEvents();
    
    pthread_cond_t mStopCondition;

    NotificationQueue mNotificationQueue;
private:
    int open_all_pipelines_l();
    void close_all_pipelines_l();
    int flowing_l();
    
    void notifyListener_l(int event, int ext1 = 0, int ext2 = 0);
private:
#ifdef ANDROID
    JavaVM *mJvm;
#endif
    TTORVideoMaterial mVideoMaterial;
    TTORAudioMaterial mAudioMaterial;
    TTORProduct mProduct;
    
    IMediaListener* mMediaListener;
private:
    bool gotError;
private:
    static void init_ffmpeg_env();
private:
    AVFormatContext *mInputAVFormatContextForVideoMaterial;
    int mInputVideoStreamIndex;
    int open_input_video_material();
    void close_input_video_material();
private:
    AVFormatContext *mInputAVFormatContextForAudioMaterial;
    int mInputAudioStreamIndex;
    int open_input_audio_material();
    void close_input_audio_material();
private:
    AVFormatContext *ofmt_ctx;
    int mOutputVideoStreamIndex;
    int mOutputAudioStreamIndex;
    int open_output_product();
    void close_output_product();
private:
    bool GotBaselineForWriteAudioPacket;
    int64_t baseLineForWriteAudioPacket;
    int64_t mCurrentWriteAudioPacketPtsUs;
    
    bool GotBaselineForWriteVideoPacket;
    int64_t baseLineForWriteVideoPacket;
    int64_t mCurrentWriteVideoPacketPtsUs;
    
    bool GotWriteVideoPacketEof;
    
    int flowing_remux_video_l(int64_t currentWriteAudioPacketPtsUs = 0, bool isRemuxAll = false);
};

#endif /* TwoToOneRemuxer_h */
