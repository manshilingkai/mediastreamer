//
//  TwoToOneRemuxerCommon.h
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/18.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef TwoToOneRemuxerCommon_h
#define TwoToOneRemuxerCommon_h

#include <stdio.h>
#include <stdlib.h>

struct TTORVideoMaterial {
    char* url;
    
    TTORVideoMaterial()
    {
        url = NULL;
    }
};

struct TTORAudioMaterial {
    char* url;
    long startPosMs;
    long endPosMs;
    
    TTORAudioMaterial()
    {
        url = NULL;
        startPosMs = -1;
        endPosMs = -1;
    }
};

struct TTORProduct {
    char* url;
    
    TTORProduct()
    {
        url = NULL;
    }
};

#endif /* TwoToOneRemuxerCommon_h */
