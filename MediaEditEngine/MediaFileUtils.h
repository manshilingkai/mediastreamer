//
//  MediaFileUtils.h
//  MediaStreamer
//
//  Created by slklovewyy on 2020/3/3.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef MediaFileUtils_h
#define MediaFileUtils_h

#include <stdio.h>

//-1 : UnKnown Media File
// 0 : Audio File
// 1 : Video File
int getMediaFileType(char* mediaFilePath);

#endif /* MediaFileUtils_h */
