//
//  PCMPlayer.h
//  MediaStreamer
//
//  Created by Think on 2019/7/11.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef PCMPlayer_h
#define PCMPlayer_h

#include <stdio.h>
#include "IPCMDataInputer.h"
#include "AudioRender.h"
#include <pthread.h>

#ifdef ANDROID
#include "jni.h"
#endif

class PCMPlayer : public IPCMDataInputer {
public:
    PCMPlayer(int sampleRate, int numChannels, char* workPCMFilePath);
    ~PCMPlayer();
    
#ifdef ANDROID
    void registerJavaVMEnv(JavaVM *jvm);
#endif
    
    bool open();
    
    bool startPlay();
    bool isPCMPlaying();
    void stopPlay();
    
    bool seek(int64_t seekTimeUs);
    
    void close();
        
    int64_t getPlayTimeUs();
    int64_t getDurationUs();
    int getPcmDB();
    
    int onInputPCMData(uint8_t **pData, int size);
private:
    int mSampleRate;
    int mNumChannels;
    char* mWorkPCMFilePath;
#ifdef ANDROID
    JavaVM *mJvm;
#endif
private:
    pthread_mutex_t mLock;
    bool isOpened;
    bool isPlaying;
private:
    AudioRenderType mAudioRenderType;
    AudioRender* mAudioRender;
private:
    FILE *mWorkPCMFile;
    int64_t mPlayTimeUs;
    int64_t mDurationUs;
    int mPcmDB;
    pthread_mutex_t mConsumeLock;
};

#endif /* PCMPlayer_hpp */
