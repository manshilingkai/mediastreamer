//
//  TwoToOneRemuxerWrapper.h
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/18.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef TwoToOneRemuxerWrapper_h
#define TwoToOneRemuxerWrapper_h

#include <stdio.h>
#include "TwoToOneRemuxerCommon.h"

struct TwoToOneRemuxerWrapper;

#ifdef __cplusplus
extern "C" {
#endif

struct TwoToOneRemuxerWrapper* TwoToOneRemuxerWrapper_GetInstance(TTORVideoMaterial videoMaterial, TTORAudioMaterial audioMaterial, TTORProduct product);
void TwoToOneRemuxerWrapper_ReleaseInstance(struct TwoToOneRemuxerWrapper **ppInstance);

void TwoToOneRemuxerWrapper_setListener(struct TwoToOneRemuxerWrapper *pInstance, void (*listener)(void*,int,int,int), void* owner);

void TwoToOneRemuxerWrapper_start(struct TwoToOneRemuxerWrapper *pInstance);
void TwoToOneRemuxerWrapper_resume(struct TwoToOneRemuxerWrapper *pInstance);
void TwoToOneRemuxerWrapper_pause(struct TwoToOneRemuxerWrapper *pInstance);
void TwoToOneRemuxerWrapper_stop(struct TwoToOneRemuxerWrapper *pInstance, bool isCancel);

#ifdef __cplusplus
};
#endif

#endif /* TwoToOneRemuxerWrapper_h */
