//
//  IPCMWriter.h
//  MediaStreamer
//
//  Created by Think on 2019/9/4.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef IPCMWriter_h
#define IPCMWriter_h

#include <stdio.h>

enum PCMWriterType
{
    UNKNOWN_PCM_WRITER_TYPE = -1,
    WAV_FILE_WRITER = 0,
    FF_AUDIO_FILE_WRITER = 1,
};

class IPCMWriter {
    
public:
    virtual ~IPCMWriter() {}

    static IPCMWriter* CreatePCMWriter(PCMWriterType type, int channels, int sample_rate, char* audioFilePath);
    static void DeletePCMWriter(IPCMWriter* iPCMWriter, PCMWriterType type);
    
    virtual bool open() = 0;
    virtual void putPcmData(char* data, int size) = 0;
    virtual int32_t getWriteTimeMs() = 0;
    virtual void close() = 0;
};

#endif /* IPCMWriter_h */
