//
//  MicrophonePCMRecorder.cpp
//  MediaStreamer
//
//  Created by Think on 2019/7/11.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "MicrophonePCMRecorder.h"
#include "MediaLog.h"
#include "MediaFile.h"
#include "PCMUtils.h"

#include "MediaTime.h"

MicrophonePCMRecorder::MicrophonePCMRecorder(int sampleRate, int numChannels, char* workPCMFilePath)
{
    mSampleRate = sampleRate;
    mNumChannels = numChannels;
    
    if (workPCMFilePath) {
        mWorkPCMFilePath = strdup(workPCMFilePath);
    }
    
    pthread_mutex_init(&mLock, NULL);
    isOpened = false;
    isRecording = false;
    
    mAudioCaptureType = AUDIO_CAPTURE_UNKNOWN;
#ifdef ANDROID
    mAudioCaptureType = AUDIORECORD;
#endif
#ifdef IOS
    mAudioCaptureType = AUDIOUNIT;
#endif
    mAudioCapture = NULL;
    
    mWorkPCMFile = NULL;
    mRecordTimeUs = 0ll;
    mPcmDB = 0;
    pthread_mutex_init(&mProduceLock, NULL);
    
#ifdef ANDROID
    mJvm = NULL;
#endif

    isEarReturn = false;
    mAudioRenderType = AUDIO_RENDER_UNKNOWN;
#ifdef ANDROID
    mAudioRenderType = AUDIO_RENDER_AUDIOTRACK;//AUDIO_RENDER_OPENSLES;
#endif
#ifdef IOS
    mAudioRenderType = AUDIO_RENDER_AUDIOUNIT;
#endif
    mAudioRender = NULL;
    pthread_mutex_init(&mAudioRenderFifoLock, NULL);
    mAudioRenderFifo = NULL;
    isAudioRenderOpened = false;
    isAudioRenderPlaying = false;
    
    mAudioNoiseSuppression = new AudioNoiseSuppression(numChannels, sampleRate);
    pthread_mutex_init(&mNSLock, NULL);
    isEnableNS = false;
}

MicrophonePCMRecorder::~MicrophonePCMRecorder()
{
    close();
    
    pthread_mutex_destroy(&mNSLock);
    
    if (mAudioNoiseSuppression) {
        delete mAudioNoiseSuppression;
        mAudioNoiseSuppression = NULL;
    }
    
    pthread_mutex_destroy(&mAudioRenderFifoLock);

    pthread_mutex_destroy(&mProduceLock);
    
    pthread_mutex_destroy(&mLock);
    
    if (mWorkPCMFilePath) {
        free(mWorkPCMFilePath);
        mWorkPCMFilePath = NULL;
    }
}

#ifdef ANDROID
void MicrophonePCMRecorder::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

bool MicrophonePCMRecorder::open()
{
    pthread_mutex_lock(&mLock);
    if (isOpened) {
        pthread_mutex_unlock(&mLock);
        return true;
    }
    
    mAudioCapture = AudioCapture::CreateAudioCapture(mAudioCaptureType, NULL, mSampleRate, mNumChannels);
#ifdef ANDROID
    mAudioCapture->registerJavaVMEnv(mJvm);
#endif
    
    if (mAudioCapture==NULL) {
        LOGE("Create AudioCapture Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    mAudioCapture->setPCMDataOutputer(this);
#ifdef ANDROID
    mAudioCapture->setRecBufSizeInBytes(mSampleRate*mNumChannels*2*10/1000);
#endif
    int result = mAudioCapture->Init();
    if (result!=0) {
        AudioCapture::DeleteAudioCapture(mAudioCapture, mAudioCaptureType);
        mAudioCapture = NULL;
        
        LOGE("Init AudioCapture Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    pthread_mutex_lock(&mProduceLock);
    mRecordTimeUs = 0ll;
    mPcmDB = 0;
    pthread_mutex_unlock(&mProduceLock);
    
    openAudioRender();
    
    isOpened = true;
    pthread_mutex_unlock(&mLock);
    return true;
}

bool MicrophonePCMRecorder::startRecord()
{
    pthread_mutex_lock(&mLock);
    if (!isOpened) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isRecording) {
        pthread_mutex_unlock(&mLock);
        return true;
    }
    
    if (mWorkPCMFilePath==NULL) {
        LOGE("Work PCM File Path is null");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    pthread_mutex_lock(&mProduceLock);
    mWorkPCMFile = fopen(mWorkPCMFilePath, "ab+");
    if (mWorkPCMFile==NULL) {
        pthread_mutex_unlock(&mProduceLock);

        LOGE("[ab+] Open Work PCM File Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    int64_t fileSize = MediaFile::getFileSizeWithStat(mWorkPCMFilePath);
    mRecordTimeUs = fileSize * 1000 * 1000 / (mSampleRate * mNumChannels * 2);
    mPcmDB = 0;
    pthread_mutex_unlock(&mProduceLock);
    
    int result = mAudioCapture->StartRecording();
    if (result!=0) {
        pthread_mutex_lock(&mProduceLock);
        if (mWorkPCMFile) {
            fclose(mWorkPCMFile);
            mWorkPCMFile = NULL;
        }
        pthread_mutex_unlock(&mProduceLock);
        
        LOGE("Start AudioCapture Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isEarReturn) {
        startAudioRender();
    }
    
    isRecording = true;
    pthread_mutex_unlock(&mLock);
    return true;
}

bool MicrophonePCMRecorder::startRecord(int64_t startPositionUs)
{
    pthread_mutex_lock(&mLock);
    if (!isOpened) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isRecording) {
        pthread_mutex_unlock(&mLock);
        return true;
    }
    
    if (mWorkPCMFilePath==NULL) {
        LOGE("Work PCM File Path is null");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    /*
    u_int8_t *pBuffer = NULL;
    long size = 0;
    bool ret = MediaFile::readDataFromDisk(mWorkPCMFilePath, false, &pBuffer, &size);
    if (!ret) {
        LOGE("Read PCM Data From Work PCM File Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    mWorkPCMFile = fopen(mWorkPCMFilePath, "wb+");
    if (mWorkPCMFile==NULL) {
        if (pBuffer) {
            free(pBuffer);
            pBuffer = NULL;
        }
        LOGE("[wb+] Open Work PCM File Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    size_t result = fwrite(pBuffer, 1, size, mWorkPCMFile);
    if (pBuffer) {
        free(pBuffer);
        pBuffer = NULL;
    }
    if (result!=size) {
        if (mWorkPCMFile) {
            fclose(mWorkPCMFile);
            mWorkPCMFile = NULL;
        }
        LOGE("Write PCM Data To Work PCM File Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
     */
    
    pthread_mutex_lock(&mProduceLock);
    mWorkPCMFile = fopen(mWorkPCMFilePath, "rb+");
    if (mWorkPCMFile==NULL) {
        pthread_mutex_unlock(&mProduceLock);

        LOGE("[rb+] Open Work PCM File Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    long long startPositionByte = (startPositionUs/1000) * (mSampleRate * mNumChannels * 2) / 1000;
    long long fileSize = MediaFile::getFileSizeWithStat(mWorkPCMFilePath);
    if (startPositionByte>fileSize) {
        startPositionByte = fileSize;
    }
    
    if (startPositionByte%(mNumChannels * 2)!=0) {
        long long startPositionSamples = startPositionByte/(mNumChannels * 2);
        startPositionByte = startPositionSamples*(mNumChannels * 2);
    }
    
    size_t result = fseek(mWorkPCMFile, (long)startPositionByte, SEEK_SET);
    if (result) {
        if (mWorkPCMFile) {
            fclose(mWorkPCMFile);
            mWorkPCMFile = NULL;
        }
        pthread_mutex_unlock(&mProduceLock);
        
        LOGE("Seek Work PCM File Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    mRecordTimeUs = startPositionByte * 1000 * 1000 / (mSampleRate * mNumChannels * 2);
    mPcmDB = 0;
    pthread_mutex_unlock(&mProduceLock);
    
    result = mAudioCapture->StartRecording();
    if (result!=0) {
        pthread_mutex_lock(&mProduceLock);
        if (mWorkPCMFile) {
            fclose(mWorkPCMFile);
            mWorkPCMFile = NULL;
        }
        pthread_mutex_unlock(&mProduceLock);
        
        LOGE("Start AudioCapture Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isEarReturn) {
        startAudioRender();
    }
    
    isRecording = true;
    pthread_mutex_unlock(&mLock);
    return true;
}

void MicrophonePCMRecorder::stopRecord()
{
    pthread_mutex_lock(&mLock);
    if (!isOpened) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    if (!isRecording) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    int result = mAudioCapture->StopRecording();
    if (result!=0) {
        LOGE("Stop AudioCapture Fail");
    }
    
    pthread_mutex_lock(&mProduceLock);
    if (mWorkPCMFile) {
        fclose(mWorkPCMFile);
        mWorkPCMFile = NULL;
    }
    mPcmDB = 0;
    pthread_mutex_unlock(&mProduceLock);
    
    stopAudioRender();
    
    isRecording = false;
    pthread_mutex_unlock(&mLock);
    return;
}

void MicrophonePCMRecorder::close()
{
    this->stopRecord();

    pthread_mutex_lock(&mLock);
    if (!isOpened) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    if (mAudioCapture) {
        mAudioCapture->Terminate();
        AudioCapture::DeleteAudioCapture(mAudioCapture, mAudioCaptureType);
        mAudioCapture = NULL;
    }
    
    closeAudioRender();
    
    isOpened = false;
    pthread_mutex_unlock(&mLock);
}

void MicrophonePCMRecorder::onOutputPCMData(uint8_t * data, int size)
{
    if (data==NULL || size<=0) return;
    
    pthread_mutex_lock(&mNSLock);
    if (isEnableNS) {
        char* out_pcm_data;
        int out_pcm_data_size = mAudioNoiseSuppression->process((char*)data, size, &out_pcm_data);
        if (out_pcm_data_size<=0)
        {
            pthread_mutex_unlock(&mNSLock);
            return;
        }

        data = (uint8_t *)out_pcm_data;
        size = out_pcm_data_size;
    }
    pthread_mutex_unlock(&mNSLock);
    
    pthread_mutex_lock(&mProduceLock);
    size_t ret = 0;
    if (mWorkPCMFile) {
        ret = fwrite(data, size, 1, mWorkPCMFile);
    }
    if (ret==1) {
        mRecordTimeUs += ret * size * 1000 * 1000 / (mSampleRate * mNumChannels * 2);
        mPcmDB = PCMUtils::getPcmDB((unsigned char *)(data), ret*size);
    }else{
        mPcmDB = 0;
        LOGE("Write Capture PCM Data To File Fail");
    }

    pthread_mutex_unlock(&mProduceLock);
    
    //Capture PCM to Render
    pthread_mutex_lock(&mAudioRenderFifoLock);
    if (mAudioRenderFifo) {
        av_audio_fifo_write(mAudioRenderFifo, (void **)&data, size/mNumChannels/2);
    }
    pthread_mutex_unlock(&mAudioRenderFifoLock);    
}

void MicrophonePCMRecorder::updateRecordTimeUs(int64_t timeUs)
{
    pthread_mutex_lock(&mProduceLock);
    mRecordTimeUs = timeUs;
    pthread_mutex_unlock(&mProduceLock);
}

int64_t MicrophonePCMRecorder::getRecordTimeUs()
{
    int64_t ret = 0;
    pthread_mutex_lock(&mProduceLock);
    ret = mRecordTimeUs;
    pthread_mutex_unlock(&mProduceLock);

    return ret;
}

int MicrophonePCMRecorder::getPcmDB()
{
    int ret = 0;
    pthread_mutex_lock(&mProduceLock);
    ret = mPcmDB;
    mPcmDB = 0;
    pthread_mutex_unlock(&mProduceLock);

    return ret;
}

void MicrophonePCMRecorder::setEarReturn(bool isEnableEarReturn)
{
    pthread_mutex_lock(&mLock);
    isEarReturn = isEnableEarReturn;
    if (isRecording) {
        if (isEarReturn) {
            startAudioRender();
        }else{
            stopAudioRender();
        }
    }
    pthread_mutex_unlock(&mLock);
}

void MicrophonePCMRecorder::enableNoiseSuppression(bool isEnableNoiseSuppression)
{
    pthread_mutex_lock(&mNSLock);
    isEnableNS = isEnableNoiseSuppression;
    pthread_mutex_unlock(&mNSLock);
}

bool MicrophonePCMRecorder::openAudioRender()
{
    if (isAudioRenderOpened) return true;
    
#ifdef ANDROID
    if (mAudioRenderType==AUDIO_RENDER_AUDIOTRACK) {
        mAudioRender = AudioRender::CreateAudioRenderWithJniEnv(AUDIO_RENDER_AUDIOTRACK, mJvm);
    }else{
        mAudioRender = AudioRender::CreateAudioRender(mAudioRenderType);
    }
#else
    mAudioRender = AudioRender::CreateAudioRender(mAudioRenderType);
#endif
    
    if (mAudioRender==NULL) {
        LOGE("Create AudioRender Fail");
        return false;
    }
    
    mAudioRender->configureAdaptation(mSampleRate, mNumChannels);
    mAudioRender->setPCMDataInputer(this);
    int result = mAudioRender->init(PLAYER_MODE);
    if (result!=0) {
        AudioRender::DeleteAudioRender(mAudioRender, mAudioRenderType);
        mAudioRender = NULL;
        
        LOGE("Init AudioRender Fail");
        return false;
    }
    
    isAudioRenderOpened = true;
    
    return true;
}

void MicrophonePCMRecorder::closeAudioRender()
{
    stopAudioRender();

    if (!isAudioRenderOpened) return;
    
    if (mAudioRender) {
        mAudioRender->terminate();
        AudioRender::DeleteAudioRender(mAudioRender, mAudioRenderType);
        mAudioRender = NULL;
    }
    
    isAudioRenderOpened = false;
}

bool MicrophonePCMRecorder::startAudioRender()
{
    if (!isAudioRenderOpened) return false;
    if (isAudioRenderPlaying) return true;
    
    int result = mAudioRender->startPlayout();
    if (result!=0) {
        LOGE("Start AudioRender Fail");
        return false;
    }
    
    pthread_mutex_lock(&mAudioRenderFifoLock);
    if (mAudioRenderFifo) {
        av_audio_fifo_free(mAudioRenderFifo);
        mAudioRenderFifo = NULL;
    }
    mAudioRenderFifo = av_audio_fifo_alloc(AV_SAMPLE_FMT_S16, mNumChannels, 1);
    pthread_mutex_unlock(&mAudioRenderFifoLock);
    
    isAudioRenderPlaying = true;
    return true;
}

void MicrophonePCMRecorder::stopAudioRender()
{
    if (!isAudioRenderOpened) return;
    if (!isAudioRenderPlaying) return;
    
    pthread_mutex_lock(&mAudioRenderFifoLock);
    if (mAudioRenderFifo) {
        av_audio_fifo_free(mAudioRenderFifo);
        mAudioRenderFifo = NULL;
    }
    pthread_mutex_unlock(&mAudioRenderFifoLock);
    
    int result = mAudioRender->stopPlayout();
    if (result!=0) {
        LOGE("Stop AudioRender Fail");
    }
    
    isAudioRenderPlaying = false;
}

int MicrophonePCMRecorder::onInputPCMData(uint8_t **pData, int size)
{
    int read = 0;
    pthread_mutex_lock(&mAudioRenderFifoLock);
    if (mAudioRenderFifo) {
        read = av_audio_fifo_read(mAudioRenderFifo, (void**)pData, size/mNumChannels/2);
    }
    pthread_mutex_unlock(&mAudioRenderFifoLock);
    
    return read*mNumChannels*2;
}
