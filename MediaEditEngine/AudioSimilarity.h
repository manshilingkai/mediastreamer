//
//  AudioSimilarity.h
//  MediaStreamer
//
//  Created by Think on 2019/10/9.
//  Copyright © 2019 Cell. All rights reserved.
//

#ifndef AudioSimilarity_h
#define AudioSimilarity_h

#include <stdio.h>

#ifdef ANDROID
#include "jni.h"
#endif

enum AUDIO_SIMILARITY_ALGORITHM
{
    CORRELATION_COEFFICIENT = 0,
    MUSLY = 1,
};

class AudioSimilarity {
public:
    virtual ~AudioSimilarity() {}

#ifdef ANDROID
    static AudioSimilarity* CreateAudioSimilarity(JavaVM *jvm, AUDIO_SIMILARITY_ALGORITHM algorithm, char* originUrl, char* bgmUrl, char* dubUrl, char* workDir);
#else
    static AudioSimilarity* CreateAudioSimilarity(AUDIO_SIMILARITY_ALGORITHM algorithm, char* originUrl, char* bgmUrl, char* dubUrl, char* workDir);
#endif
    static void DeleteAudioSimilarity(AUDIO_SIMILARITY_ALGORITHM algorithm, AudioSimilarity* audioSimilarity);
    
#ifdef ANDROID
    virtual void setListener(jobject thiz, jobject weak_thiz, jmethodID post_event) = 0;
#else
    virtual void setListener(void (*listener)(void*,int,int,int), void* arg) = 0;
#endif
    
    virtual void start() = 0;
    virtual void resume() = 0;
    virtual void pause() = 0;
    virtual void stop() = 0;
};

#endif /* AudioSimilarity_h */
