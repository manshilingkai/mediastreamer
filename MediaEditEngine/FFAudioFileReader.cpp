//
//  FFAudioFileReader.cpp
//  MediaStreamer
//
//  Created by Think on 2020/2/5.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "FFAudioFileReader.h"

FFAudioFileReader::FFAudioFileReader(char* audioFilePath)
{
    if (audioFilePath) {
        mAudioFilePath = strdup(audioFilePath);
    }
    
    mAudioRenderConfigure.channelCount = 2;
    mAudioRenderConfigure.sampleFormat = AV_SAMPLE_FMT_S16;
    mAudioRenderConfigure.sampleRate = 44100;
    mAudioRenderConfigure.channelLayout = AV_CH_LAYOUT_STEREO;
    
    mAudioPlayer = NULL;
    
    mVolume = 1.0f;
}

FFAudioFileReader::~FFAudioFileReader()
{
    if (mAudioFilePath) {
        free(mAudioFilePath);
        mAudioFilePath = NULL;
    }
}

bool FFAudioFileReader::open()
{
    if (mAudioFilePath==NULL) return false;
    
#ifdef ANDROID
    mAudioPlayer = new AudioPlayer(NULL, AUDIO_PLAYER_EXTERNAL_RENDER_SIMPLE, mAudioRenderConfigure);
#else
    mAudioPlayer = new AudioPlayer(AUDIO_PLAYER_EXTERNAL_RENDER_SIMPLE, mAudioRenderConfigure);
#endif
    mAudioPlayer->setDataSource(mAudioFilePath);
    mAudioPlayer->setVolume(mVolume);
    mAudioPlayer->prepareAsyncToPlay();
    
    return true;
}

void FFAudioFileReader::setVolume(float volume)
{
    if (mVolume==volume) {
        return;
    }
    
    mVolume = volume;
    
    if (mAudioPlayer) {
        mAudioPlayer->setVolume(mVolume);
    }
}

int FFAudioFileReader::getChannelCount()
{
    return mAudioRenderConfigure.channelCount;
}

int FFAudioFileReader::getSampleRate()
{
    return mAudioRenderConfigure.sampleRate;
}

int FFAudioFileReader::getBitsPerSample()
{
    return av_get_bytes_per_sample(mAudioRenderConfigure.sampleFormat)*8;
}

int FFAudioFileReader::getDurationMs()
{
    if (mAudioPlayer) {
        return mAudioPlayer->getDurationMs();
    }
    
    return 0;
}

int FFAudioFileReader::getPcmData(char **pData, long size)
{
    if (mAudioPlayer) {
        return mAudioPlayer->drainPCMDataFromExternal((void**)pData, (int)size);
    }
    
    return 0;
}

void FFAudioFileReader::close()
{
    if (mAudioPlayer) {
        mAudioPlayer->stop();
        delete mAudioPlayer;
        mAudioPlayer = NULL;
    }
}
