//
//  GPUOverlayWrapper.h
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/30.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef GPUOverlayWrapper_h
#define GPUOverlayWrapper_h

#include <stdio.h>

struct GPUOverlayWrapper;

#ifdef __cplusplus
extern "C" {
#endif
    struct GPUOverlayWrapper *GPUOverlay_GetInstance();
    void GPUOverlay_ReleaseInstance(struct GPUOverlayWrapper **ppInstance);

    void GPUOverlay_initialize(struct GPUOverlayWrapper *pInstance);
    void GPUOverlay_terminate(struct GPUOverlayWrapper *pInstance);

    void GPUOverlay_setOverlay(struct GPUOverlayWrapper *pInstance, char* imageUrl);
    bool GPUOverlay_blendTo(struct GPUOverlayWrapper *pInstance, int mainTextureId, int mainWidth, int mainHeight, int x, int y, int overlayRotation, float overlayScale, bool overlayFlipHorizontal, bool overlayFlipVertical, int outputTextureId);
#ifdef __cplusplus
};
#endif

#endif /* GPUOverlayWrapper_h */
