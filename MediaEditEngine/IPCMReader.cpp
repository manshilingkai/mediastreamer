//
//  IPCMReader.cpp
//  MediaStreamer
//
//  Created by Think on 2020/2/3.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "IPCMReader.h"
#include "WAVFile.h"
#include "FFAudioFileReader.h"

IPCMReader* IPCMReader::CreatePCMReader(PCMReaderType type, char* audioFilePath)
{
    if (type==WAV_FILE_READER) {
        return new WAVFileReader(audioFilePath);
    }else if (type==FF_AUDIO_FILE_READER) {
        return new FFAudioFileReader(audioFilePath);
    }
    
    return NULL;
}

void IPCMReader::DeletePCMReader(IPCMReader* iPCMReader, PCMReaderType type)
{
    if (type == WAV_FILE_READER) {
        WAVFileReader* wavFileReader = (WAVFileReader*)iPCMReader;
        if (wavFileReader) {
            delete wavFileReader;
            wavFileReader = NULL;
        }
    }else if (type == FF_AUDIO_FILE_READER) {
        FFAudioFileReader* ffAudioFileReader = (FFAudioFileReader*)iPCMReader;
        if (ffAudioFileReader) {
            delete ffAudioFileReader;
            ffAudioFileReader = NULL;
        }
    }
}
