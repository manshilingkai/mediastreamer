//
//  YPPGPUOverlay.m
//  MediaStreamer
//
//  Created by slklovewyy on 2020/5/30.
//  Copyright © 2020 bolome. All rights reserved.
//

#import "YPPGPUOverlay.h"
#include "GPUOverlayWrapper.h"

@implementation YPPGPUOverlay
{
    GPUOverlayWrapper* pGPUOverlayWrapper;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        pGPUOverlayWrapper = NULL;
    }
    
    return self;
}

- (void)initialize
{
    pGPUOverlayWrapper = GPUOverlay_GetInstance();
    if (pGPUOverlayWrapper) {
        GPUOverlay_initialize(pGPUOverlayWrapper);
    }
}

- (void)setOverlay:(NSString*)imageUrl
{
    if (pGPUOverlayWrapper) {
        GPUOverlay_setOverlay(pGPUOverlayWrapper, (char*)[imageUrl UTF8String]);
    }
}

- (BOOL)blendTo:(int)mainTextureId MainWidth:(int)mainWidth MainHeight:(int)mainHeight X:(int)x Y:(int)y OverlayRotation:(int)rotation OverlayScale:(float)scale OverlayFlipHorizontal:(bool)flipHorizontal OverlayFlipVertical:(bool)flipVertical OutputTextureId:(int)outputTextureId
{
    bool ret = false;
    if (pGPUOverlayWrapper) {
        ret = GPUOverlay_blendTo(pGPUOverlayWrapper, mainTextureId, mainWidth, mainHeight, x, y, rotation, scale, flipHorizontal, flipVertical, outputTextureId);
    }
    
    return ret?YES:NO;
}

- (void)terminate
{
    if (pGPUOverlayWrapper) {
        GPUOverlay_terminate(pGPUOverlayWrapper);
        GPUOverlay_ReleaseInstance(&pGPUOverlayWrapper);
        pGPUOverlayWrapper = NULL;
    }
}

- (void)dealloc
{
    [self terminate];
    
    NSLog(@"YPPGPUOverlay dealloc");
}

@end
