//
//  FFAudioFileReader.h
//  MediaStreamer
//
//  Created by Think on 2020/2/5.
//  Copyright © 2020 Cell. All rights reserved.
//

#ifndef FFAudioFileReader_h
#define FFAudioFileReader_h

#include <stdio.h>
#include "IPCMReader.h"
#include "AudioPlayer.h"

class FFAudioFileReader : public IPCMReader{
public:
    FFAudioFileReader(char* audioFilePath);
    ~FFAudioFileReader();
    
    bool open();
    
    void setVolume(float volume);
    
    int getChannelCount();
    int getSampleRate();
    int getBitsPerSample();
    
    int getDurationMs();
    
    int getPcmData(char **pData, long size);
    
    void close();
private:
    char* mAudioFilePath;
    AudioRenderConfigure mAudioRenderConfigure;
    AudioPlayer* mAudioPlayer;
    float mVolume;
};

#endif /* FFAudioFileReader_h */
