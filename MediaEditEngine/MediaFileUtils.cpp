//
//  MediaFileUtils.cpp
//  MediaStreamer
//
//  Created by slklovewyy on 2020/3/3.
//  Copyright © 2020 Cell. All rights reserved.
//

#include "MediaFileUtils.h"

extern "C" {
#include "libavformat/avformat.h"
}

#include "FFLog.h"

int getMediaFileType(char* mediaFilePath)
{
    if (mediaFilePath==NULL) return -1;
    
    av_register_all();
    FFLog::setLogLevel(AV_LOG_WARNING);
    
    AVFormatContext* avFormatContext = avformat_alloc_context();
    int err = avformat_open_input(&avFormatContext, mediaFilePath, 0, 0);
    if(err < 0)
    {
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
        
        LOGE("Open Media File Fail [Media File Path : %s] [Error Code : %d]",mediaFilePath, err);
        return -1;
    }
    
    err = avformat_find_stream_info(avFormatContext, NULL);
    if (err < 0)
    {
        avformat_close_input(&avFormatContext);
        avformat_free_context(avFormatContext);
        avFormatContext = NULL;
        
        LOGE("Get Stream Info Fail [Error Code : %d]", err);
        return -1;
    }
        
    int trackCount = avFormatContext->nb_streams;
    int audioStreamIndex = -1;
    int videoStreamIndex = -1;
    for(int i= 0; i < trackCount; i++)
    {
        if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            if(audioStreamIndex == -1)
            {
                audioStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else if (avFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            if(videoStreamIndex == -1)
            {
                videoStreamIndex = i;
            }
            else
            {
                avFormatContext->streams[i]->discard = AVDISCARD_ALL;
            }
        }else{
            avFormatContext->streams[i]->discard = AVDISCARD_ALL;
        }
    }
    
    if (audioStreamIndex!=-1 && avFormatContext->streams[audioStreamIndex]!=NULL) {
        int audio_sample_rate = avFormatContext->streams[audioStreamIndex]->codec->sample_rate;
        int audio_channels = avFormatContext->streams[audioStreamIndex]->codec->channels;
        AVSampleFormat audio_format = avFormatContext->streams[audioStreamIndex]->codec->sample_fmt;
        
        if (audio_sample_rate<=0 || audio_channels<=0 || audio_format<0 || audio_format>=AV_SAMPLE_FMT_NB) {
            LOGE("InValid Audio Stream Format Info");
            avFormatContext->streams[audioStreamIndex]->discard = AVDISCARD_ALL;
            audioStreamIndex = -1;
        }
    }
    
    avformat_close_input(&avFormatContext);
    avformat_free_context(avFormatContext);
    avFormatContext = NULL;
    
    if (videoStreamIndex!=-1) {
        return 1;
    }else{
        if (audioStreamIndex!=-1) {
            return 0;
        }else return -1;
    }
}
