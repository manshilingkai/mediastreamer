//
//  MicrophoneAudioRecorder.cpp
//  MediaStreamer
//
//  Created by Think on 2019/7/10.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include <string.h>
#include <stdlib.h>
#include "MicrophoneAudioRecorder.h"
#include "MediaLog.h"
#include "MediaDir.h"
#include "StringUtils.h"
#include "MediaTime.h"
#include "MediaFile.h"
#include "WAVFile.h"

#include "AudioNoiseSuppression.h"
#include "AudioEffect.h"

MicrophoneAudioRecorder::MicrophoneAudioRecorder(int sampleRate, int numChannels, char* workDir)
{
    mSampleRate = sampleRate;
    mNumChannels = numChannels;
    if (workDir) {
        mWorkDir = strdup(workDir);
    }
    
    mWorkPCMFilePath = NULL;
    mMicrophonePCMRecorder = NULL;
    mPCMPlayer = NULL;
    
    pthread_mutex_init(&mLock, NULL);
    initialized = false;
    isWorkMediaFileLocked = false;
    isRecording = false;
    isAudioPlayerOpened = false;
    
#ifdef ANDROID
    mJvm = NULL;
#endif
}

MicrophoneAudioRecorder::~MicrophoneAudioRecorder()
{
    terminate();
    
    pthread_mutex_destroy(&mLock);
    
    if (mWorkDir) {
        free(mWorkDir);
        mWorkDir = NULL;
    }
}

#ifdef ANDROID
void MicrophoneAudioRecorder::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

bool MicrophoneAudioRecorder::initialize()
{
    LOGD("MicrophoneAudioRecorder::initialize");
    
    pthread_mutex_lock(&mLock);
    
    if (initialized) {
        pthread_mutex_unlock(&mLock);
        return true;
    }
    
    if (mWorkDir==NULL) {
        LOGE("No Working Dir!!");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    bool ret = MediaDir::isExist(mWorkDir);
    if (!ret) {
        LOGE("No Exist Working Dir!!");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    long dirLen = strlen(mWorkDir);
    mWorkPCMFilePath = (char*)malloc(dirLen+128);
    
    char dst[8];
    StringUtils::right(dst, mWorkDir, 1);
    if (dst[0]=='/')
    {
        sprintf(mWorkPCMFilePath, "%s%lld.pcm",mWorkDir,systemTimeNs());
    }else{
        sprintf(mWorkPCMFilePath, "%s/%lld.pcm",mWorkDir,systemTimeNs());
    }
    
    if (MediaFile::isExist(mWorkPCMFilePath)) {
        MediaFile::deleteFile(mWorkPCMFilePath);
    }
    
    LOGD("Work PCM File Path: %s",mWorkPCMFilePath);
    
    mMicrophonePCMRecorder = new MicrophonePCMRecorder(mSampleRate, mNumChannels, mWorkPCMFilePath);
#ifdef ANDROID
    mMicrophonePCMRecorder->registerJavaVMEnv(mJvm);
#endif
    ret = mMicrophonePCMRecorder->open();
    if (!ret) {
        delete mMicrophonePCMRecorder;
        mMicrophonePCMRecorder = NULL;
        
        free(mWorkPCMFilePath);
        mWorkPCMFilePath = NULL;
        
        LOGE("Open MicrophonePCMRecorder Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    isWorkMediaFileLocked = false;
    isRecording = false;
    isAudioPlayerOpened = false;
    
    initialized = true;
    
    pthread_mutex_unlock(&mLock);
    return true;
}

bool MicrophoneAudioRecorder::startRecord()
{
    LOGD("MicrophoneAudioRecorder::startRecord");

    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isRecording) {
        pthread_mutex_unlock(&mLock);
        return true;
    }
    
    if (isWorkMediaFileLocked) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    bool ret = mMicrophonePCMRecorder->startRecord();
    if (!ret) {
        LOGE("MicrophonePCMRecorder Start Record Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    isWorkMediaFileLocked = true;
    isRecording = true;
    pthread_mutex_unlock(&mLock);
    return true;
}

bool MicrophoneAudioRecorder::startRecord(int64_t startPositionUs)
{
    LOGD("MicrophoneAudioRecorder::startRecord : %lld", startPositionUs);

    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isRecording) {
        pthread_mutex_unlock(&mLock);
        return true;
    }
    
    if (isWorkMediaFileLocked) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    bool ret = mMicrophonePCMRecorder->startRecord(startPositionUs);
    if (!ret) {
        LOGE("MicrophonePCMRecorder Start Record Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    isWorkMediaFileLocked = true;
    isRecording = true;
    pthread_mutex_unlock(&mLock);
    return true;
}

bool MicrophoneAudioRecorder::isAudioRecording()
{
    LOGD("MicrophoneAudioRecorder::isAudioRecording");
    
    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isRecording) {
        pthread_mutex_unlock(&mLock);
        return true;
    }else{
        pthread_mutex_unlock(&mLock);
        return false;
    }
}

void MicrophoneAudioRecorder::stopRecord()
{
    LOGD("MicrophoneAudioRecorder::stopRecord");

    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    if (!isRecording) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    mMicrophonePCMRecorder->stopRecord();
        
    isWorkMediaFileLocked = false;
    isRecording = false;
    pthread_mutex_unlock(&mLock);
}

int64_t MicrophoneAudioRecorder::getRecordTimeUs()
{
    int64_t ret = 0;
    pthread_mutex_lock(&mLock);

    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return 0;
    }
    
    ret = mMicrophonePCMRecorder->getRecordTimeUs();
    
    pthread_mutex_unlock(&mLock);
    return ret;
}

int MicrophoneAudioRecorder::getRecordPcmDB()
{
    int ret = 0;
    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return 0;
    }
    
    ret = mMicrophonePCMRecorder->getPcmDB();
    
    pthread_mutex_unlock(&mLock);
    return ret;
}

void MicrophoneAudioRecorder::setEarReturn(bool isEnableEarReturn)
{
    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    mMicrophonePCMRecorder->setEarReturn(isEnableEarReturn);
    
    pthread_mutex_unlock(&mLock);
}

void MicrophoneAudioRecorder::enableNoiseSuppression(bool isEnableNoiseSuppression)
{
    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    mMicrophonePCMRecorder->enableNoiseSuppression(isEnableNoiseSuppression);
    
    pthread_mutex_unlock(&mLock);
}

bool MicrophoneAudioRecorder::openAudioPlayer()
{
    LOGD("MicrophoneAudioRecorder::openAudioPlayer");

    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isAudioPlayerOpened) {
        pthread_mutex_unlock(&mLock);
        return true;
    }
    
    if (isWorkMediaFileLocked) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    mPCMPlayer = new PCMPlayer(mSampleRate, mNumChannels, mWorkPCMFilePath);
#ifdef ANDROID
    mPCMPlayer->registerJavaVMEnv(mJvm);
#endif
    bool ret = mPCMPlayer->open();
    if (!ret) {
        delete mPCMPlayer;
        mPCMPlayer = NULL;
        
        LOGE("PCMPlayer Open Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    isWorkMediaFileLocked = true;
    isAudioPlayerOpened = true;
    pthread_mutex_unlock(&mLock);
    return true;
}

bool MicrophoneAudioRecorder::startAudioPlay()
{
    LOGD("MicrophoneAudioRecorder::startAudioPlay");

    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (!isAudioPlayerOpened) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    bool ret = mPCMPlayer->startPlay();
    if (!ret) {
        LOGE("PCMPlayer Start Play Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    pthread_mutex_unlock(&mLock);
    return true;
}

bool MicrophoneAudioRecorder::seekAudioPlay(int64_t seekTimeUs)
{
    LOGD("MicrophoneAudioRecorder::seekAudioPlay");

    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (!isAudioPlayerOpened) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    bool ret = mPCMPlayer->seek(seekTimeUs);
    if (!ret) {
        LOGE("PCMPlayer Seek Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    pthread_mutex_unlock(&mLock);
    return true;
}

void MicrophoneAudioRecorder::pauseAudioPlay()
{
    LOGD("MicrophoneAudioRecorder::pauseAudioPlay");

    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    if (!isAudioPlayerOpened) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    mPCMPlayer->stopPlay();
    
    pthread_mutex_unlock(&mLock);
}

bool MicrophoneAudioRecorder::isAudioPlaying()
{
    LOGD("MicrophoneAudioRecorder::isAudioPlaying");

    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (!isAudioPlayerOpened) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    bool ret = mPCMPlayer->isPCMPlaying();
    pthread_mutex_unlock(&mLock);

    return ret;
}

void MicrophoneAudioRecorder::closeAudioPlayer()
{
    LOGD("MicrophoneAudioRecorder::closeAudioPlayer");

    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    if (!isAudioPlayerOpened) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    mPCMPlayer->close();
    delete mPCMPlayer;
    mPCMPlayer = NULL;
    
    isWorkMediaFileLocked = false;
    isAudioPlayerOpened = false;
    pthread_mutex_unlock(&mLock);
}

int64_t MicrophoneAudioRecorder::getPlayTimeUs()
{
    int64_t ret = 0;
    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return 0;
    }
    
    if (!isAudioPlayerOpened) {
        pthread_mutex_unlock(&mLock);
        return 0;
    }
    
    ret = mPCMPlayer->getPlayTimeUs();
    pthread_mutex_unlock(&mLock);
    return ret;
}

int64_t MicrophoneAudioRecorder::getPlayDurationUs()
{
    int64_t ret = 0;
    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return 0;
    }
    
    if (!isAudioPlayerOpened) {
        pthread_mutex_unlock(&mLock);
        return 0;
    }
    
    ret = mPCMPlayer->getDurationUs();
    pthread_mutex_unlock(&mLock);
    return ret;
}

int MicrophoneAudioRecorder::getPlayPcmDB()
{
    int ret = 0;
    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return 0;
    }
    
    if (!isAudioPlayerOpened) {
        pthread_mutex_unlock(&mLock);
        return 0;
    }
    
    ret = mPCMPlayer->getPcmDB();
    pthread_mutex_unlock(&mLock);
    return ret;
}

bool MicrophoneAudioRecorder::reverseOrderGeneration()
{
    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isWorkMediaFileLocked) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    bool ret = MediaFile::reverseOrderGeneration(mWorkPCMFilePath, mNumChannels);
    if (!ret) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    pthread_mutex_unlock(&mLock);
    return true;
}

bool MicrophoneAudioRecorder::backDelete(int64_t keepTimeUs)
{
    LOGD("MicrophoneAudioRecorder::backDelete");

    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isWorkMediaFileLocked) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    int64_t keepByte = (keepTimeUs/1000) * (mSampleRate * mNumChannels * 2) /1000;
    bool ret = MediaFile::backDeleteFile(mWorkPCMFilePath, (long)keepByte);
    if (!ret) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    mMicrophonePCMRecorder->updateRecordTimeUs(keepTimeUs);
    pthread_mutex_unlock(&mLock);
    return true;
}

bool MicrophoneAudioRecorder::convertToWav(char *wavFilePath)
{
    LOGD("MicrophoneAudioRecorder::convertToWav");

    if (wavFilePath==NULL) {
        LOGE("Input Param is Null");
        return false;
    }
    
    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isWorkMediaFileLocked) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (MediaFile::isExist(wavFilePath)) {
        LOGD("Wav File is Exist : %s", wavFilePath);
        bool ret = MediaFile::deleteFile(wavFilePath);
        if (ret) {
            LOGD("Delete Wav File Success : %s", wavFilePath);
        } else {
            LOGD("Delete Wav File Fail : %s", wavFilePath);
        }
    }else {
        LOGD("Wav File is not Exist : %s", wavFilePath);
    }
    
    bool ret = WAVFile::PCMToWAV(mWorkPCMFilePath, mNumChannels, mSampleRate, wavFilePath);
    if (!ret) {
        LOGE("WAVFile PCM To WAV Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    pthread_mutex_unlock(&mLock);
    return true;
}

bool MicrophoneAudioRecorder::convertToWavWithOffset(char *wavFilePath, long offsetMs)
{
    LOGD("MicrophoneAudioRecorder::convertToWavWithOffset");

    if (wavFilePath==NULL) {
        LOGE("Input Param is Null");
        return false;
    }
    
    pthread_mutex_lock(&mLock);
    
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isWorkMediaFileLocked) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (MediaFile::isExist(wavFilePath)) {
        MediaFile::deleteFile(wavFilePath);
    }
    
    long offset = offsetMs * mSampleRate * mNumChannels * 2 / 1000;
    long offset_flag = 1;
    if (offset<0) {
        offset_flag = -1;
        offset = offset * offset_flag;
    }
    if (offset%(mNumChannels * 2)!=0) {
        long offsetSamples = offset/(mNumChannels * 2);
        offset = offsetSamples*(mNumChannels * 2);
    }
    offset = offset * offset_flag;
    
    bool ret = WAVFile::PCMToWAVWithOffset(mWorkPCMFilePath, mNumChannels, mSampleRate, wavFilePath, offset);
    if (!ret) {
        LOGE("WAVFile PCM To WAV Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    pthread_mutex_unlock(&mLock);
    return true;
}

void MicrophoneAudioRecorder::terminate()
{
    LOGD("MicrophoneAudioRecorder::terminate");

    this->closeAudioPlayer();
    
    this->stopRecord();
    
    pthread_mutex_lock(&mLock);
    if (!initialized) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    if (mMicrophonePCMRecorder) {
        mMicrophonePCMRecorder->close();
        delete mMicrophonePCMRecorder;
        mMicrophonePCMRecorder = NULL;
    }
    
    if (MediaFile::isExist(mWorkPCMFilePath)) {
        LOGD("Work PCM File is Exist : %s", mWorkPCMFilePath);

        bool ret = MediaFile::deleteFile(mWorkPCMFilePath);
        if (ret) {
            LOGD("Delete Work PCM File Success : %s", mWorkPCMFilePath);
        } else {
            LOGD("Delete Work PCM File Fail : %s", mWorkPCMFilePath);
        }
    }else{
        LOGD("Work PCM File is not Exist : %s", mWorkPCMFilePath);
    }
    
    if (mWorkPCMFilePath) {
        free(mWorkPCMFilePath);
        mWorkPCMFilePath = NULL;
    }
    
    isWorkMediaFileLocked = false;
    isRecording = false;
    isAudioPlayerOpened = false;
    
    initialized = false;
    pthread_mutex_unlock(&mLock);
    return;
}

bool convertToWavWithNS(char* inputWavFilePath, char* outputWavFilePath)
{
    //Input Wav
    if (inputWavFilePath==NULL) {
        LOGE("Input Wav File Path is Null");
        return false;
    }
    
    FILE* inputWavFile = fopen(inputWavFilePath, "r");
    if (inputWavFile==NULL) {
        LOGE("[r] Open Input Wav File Fail");
        return false;
    }
    
    wav_header_t inputWavHeader;
    size_t ret = fread(&inputWavHeader, 1, sizeof(wav_header_t), inputWavFile);
    if (ret!=sizeof(wav_header_t)) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        LOGE("Read Input Wav Header Fail");
        return false;
    }
    
    int sample_rate = inputWavHeader.sample_rate;
    int channels = inputWavHeader.num_channels;
    int input_pcm_data_size = inputWavHeader.sub_chunk2_size;
    
    long long input_wav_file_size = MediaFile::getFileSizeWithStat(inputWavFilePath);
    if (input_wav_file_size!=input_pcm_data_size+sizeof(wav_header_t)) {
        LOGW("Input Wav File Pcm Raw Data Size From Header Info is %d, but Real is %lld", input_pcm_data_size, input_wav_file_size-sizeof(wav_header_t));
    }
    input_pcm_data_size = input_wav_file_size - sizeof(wav_header_t);
    
    //Output Wav
    if (outputWavFilePath==NULL) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        
        LOGE("Output Wav File Path is Null");
        return false;
    }
    
    FILE* outputWavFile = fopen(outputWavFilePath, "w");
    if (outputWavFile==NULL) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        
        LOGE("[w] Open Output Wav File Fail");
        return false;
    }
    
    long output_pcm_data_size = input_pcm_data_size;
    if (output_pcm_data_size<=0) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        
        fclose(outputWavFile);
        outputWavFile = NULL;
        
        LOGE("Output PCM Data Size is %ld, it is invalid", output_pcm_data_size);
        return false;
    }
    
    inputWavHeader.sub_chunk2_size = output_pcm_data_size;
    fwrite(&inputWavHeader, 1, sizeof(wav_header_t), outputWavFile);
    
    AudioNoiseSuppression* audioNoiseSuppression = new AudioNoiseSuppression(channels, sample_rate);
    
    long processBytePerTime = sample_rate * channels * 2 * 0.03;
    char* tmp = (char*)malloc(processBytePerTime);
    bool isError = false;
    long haveReadByte = 0;
    long readBytePerTime = 0;
    long haveWriteByte = 0;
    while (true) {
        if (output_pcm_data_size-haveReadByte>=processBytePerTime) {
            readBytePerTime = processBytePerTime;
        }else{
            readBytePerTime = output_pcm_data_size-haveReadByte;
        }
        
        if (readBytePerTime<=0) {
            break;
        }
        
        size_t read = fread(tmp, 1, readBytePerTime, inputWavFile);
        bool isEarlyEof = false;
        if (read!=readBytePerTime) {
            if (feof(inputWavFile)) {
                isEarlyEof = true;
                LOGW("Read Input Wav File To EOF");
            }else{
                LOGE("Read Input Wav File Error, Error Code : %d", ferror(inputWavFile));
                isError = true;
                break;
            }
        }
        
        char* out_pcm_data;
        int out_pcm_data_size = audioNoiseSuppression->process((char*)tmp, (int)read, &out_pcm_data);
        if (out_pcm_data_size<=0)
        {
        }else{
            fwrite(out_pcm_data, 1, out_pcm_data_size, outputWavFile);
            fflush(outputWavFile);
            haveWriteByte += out_pcm_data_size;
        }

        haveReadByte += read;
        
        if (isEarlyEof) {
            break;
        }
    }
    if (haveWriteByte!=inputWavHeader.sub_chunk2_size) {
        fseek(outputWavFile, 0, SEEK_SET);
        inputWavHeader.sub_chunk2_size = haveWriteByte;
        fwrite(&inputWavHeader, 1, sizeof(wav_header_t), outputWavFile);
        fflush(outputWavFile);
    }
    
    if (inputWavFile) {
        fclose(inputWavFile);
        inputWavFile = NULL;
    }
    
    if (outputWavFile) {
        fclose(outputWavFile);
        outputWavFile = NULL;
    }
    
    if (tmp) {
        free(tmp);
        tmp = NULL;
    }
    
    if (audioNoiseSuppression) {
        delete audioNoiseSuppression;
        audioNoiseSuppression = NULL;
    }
    
    if (isError) {
        MediaFile::deleteFile(outputWavFilePath);
        return false;
    }else{
        return true;
    }
}

bool convertToWavWithOffset(char* inputWavFilePath, char* outputWavFilePath, long offsetMs)
{
    //Input Wav
    if (inputWavFilePath==NULL) {
        LOGE("Input Wav File Path is Null");
        return false;
    }
    
    FILE* inputWavFile = fopen(inputWavFilePath, "r");
    if (inputWavFile==NULL) {
        LOGE("[r] Open Input Wav File Fail");
        return false;
    }
    
    wav_header_t inputWavHeader;
    size_t ret = fread(&inputWavHeader, 1, sizeof(wav_header_t), inputWavFile);
    if (ret!=sizeof(wav_header_t)) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        LOGE("Read Input Wav Header Fail");
        return false;
    }
    
    int sample_rate = inputWavHeader.sample_rate;
    int channels = inputWavHeader.num_channels;
    int input_pcm_data_size = inputWavHeader.sub_chunk2_size;

    long long input_wav_file_size = MediaFile::getFileSizeWithStat(inputWavFilePath);
    if (input_wav_file_size!=input_pcm_data_size+sizeof(wav_header_t)) {
        LOGW("Input Wav File Pcm Raw Data Size From Header Info is %d, but Real is %lld", input_pcm_data_size, input_wav_file_size-sizeof(wav_header_t));
    }
    input_pcm_data_size = input_wav_file_size - sizeof(wav_header_t);
    
    //Output Wav
    if (outputWavFilePath==NULL) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        
        LOGE("Output Wav File Path is Null");
        return false;
    }
    
    FILE* outputWavFile = fopen(outputWavFilePath, "w");
    if (outputWavFile==NULL) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        
        LOGE("[w] Open Output Wav File Fail");
        return false;
    }
    
    long offset = offsetMs * sample_rate * channels * 2 / 1000;
    long offset_flag = 1;
    if (offset<0) {
        offset_flag = -1;
        offset = offset * offset_flag;
    }
    if (offset%(channels * 2)!=0) {
        long offsetSamples = offset/(channels * 2);
        offset = offsetSamples*(channels * 2);
    }
    offset = offset * offset_flag;
    
    long output_pcm_data_size = input_pcm_data_size + offset;
    if (output_pcm_data_size<=0) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        
        fclose(outputWavFile);
        outputWavFile = NULL;
        
        LOGE("Output PCM Data Size is %ld, it is invalid", output_pcm_data_size);
        return false;
    }
    
    inputWavHeader.sub_chunk2_size = output_pcm_data_size;
    fwrite(&inputWavHeader, 1, sizeof(wav_header_t), outputWavFile);
    
    //IO
    long haveWriteByte = 0;
    if (offset<=0) {
        ret = fseek(inputWavFile, -offset, SEEK_CUR);
        if (ret) {
            fclose(inputWavFile);
            inputWavFile = NULL;
            
            fclose(outputWavFile);
            outputWavFile = NULL;
            
            LOGE("fseek(SEEK_CUR) Input Wav File to %ld Fail", -offset);
            return false;
        }
    }else{
        char* offsetData = (char*)malloc(offset);
        memset(offsetData, 0, offset);
        fwrite(offsetData, 1, offset, outputWavFile);
        if (offsetData) free(offsetData);
        output_pcm_data_size -= offset;
        haveWriteByte += offset;
    }
    
    char* tmp = (char*)malloc(sample_rate * channels * 2);
    bool isError = false;
    long haveReadByte = 0;
    long readBytePerTime = 0;
    while (true) {
        if (output_pcm_data_size-haveReadByte>=sample_rate * channels * 2) {
            readBytePerTime = sample_rate * channels * 2;
        }else{
            readBytePerTime = output_pcm_data_size-haveReadByte;
        }
        
        if (readBytePerTime<=0) {
            break;
        }
        
        size_t read = fread(tmp, 1, readBytePerTime, inputWavFile);
        bool isEarlyEof = false;
        if (read!=readBytePerTime) {
            if (feof(inputWavFile)) {
                isEarlyEof = true;
                LOGW("Read Input Wav File To EOF");
            }else{
                LOGE("Read Input Wav File Error, Error Code : %d", ferror(inputWavFile));
                isError = true;
                break;
            }
        }
        
        fwrite(tmp, 1, read, outputWavFile);
        fflush(outputWavFile);
        haveWriteByte += read;

        haveReadByte += read;
        
        if (isEarlyEof) {
            break;
        }
    }
    
    if (haveWriteByte!=inputWavHeader.sub_chunk2_size) {
        fseek(outputWavFile, 0, SEEK_SET);
        inputWavHeader.sub_chunk2_size = haveWriteByte;
        fwrite(&inputWavHeader, 1, sizeof(wav_header_t), outputWavFile);
        fflush(outputWavFile);
    }
    
    if (inputWavFile) {
        fclose(inputWavFile);
        inputWavFile = NULL;
    }
    
    if (outputWavFile) {
        fclose(outputWavFile);
        outputWavFile = NULL;
    }
    
    if (tmp) {
        free(tmp);
        tmp = NULL;
    }
    
    if (isError) {
        MediaFile::deleteFile(outputWavFilePath);
        return false;
    }else{
        return true;
    }
}

bool convertToWavWithAudioEffect(char* inputWavFilePath, char* outputWavFilePath, int effectType, int effect)
{
    //Input Wav
    if (inputWavFilePath==NULL) {
        LOGE("Input Wav File Path is Null");
        return false;
    }
    
    FILE* inputWavFile = fopen(inputWavFilePath, "r");
    if (inputWavFile==NULL) {
        LOGE("[r] Open Input Wav File Fail");
        return false;
    }
    
    wav_header_t inputWavHeader;
    size_t ret = fread(&inputWavHeader, 1, sizeof(wav_header_t), inputWavFile);
    if (ret!=sizeof(wav_header_t)) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        LOGE("Read Input Wav Header Fail");
        return false;
    }
    
    int sample_rate = inputWavHeader.sample_rate;
    int channels = inputWavHeader.num_channels;
    int input_pcm_data_size = inputWavHeader.sub_chunk2_size;
    
    long long input_wav_file_size = MediaFile::getFileSizeWithStat(inputWavFilePath);
    if (input_wav_file_size!=input_pcm_data_size+sizeof(wav_header_t)) {
        LOGW("Input Wav File Pcm Raw Data Size From Header Info is %d, but Real is %lld", input_pcm_data_size, input_wav_file_size-sizeof(wav_header_t));
    }
    input_pcm_data_size = input_wav_file_size - sizeof(wav_header_t);
    
    //Output Wav
    if (outputWavFilePath==NULL) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        
        LOGE("Output Wav File Path is Null");
        return false;
    }
    
    FILE* outputWavFile = fopen(outputWavFilePath, "w");
    if (outputWavFile==NULL) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        
        LOGE("[w] Open Output Wav File Fail");
        return false;
    }
    
    long output_pcm_data_size = input_pcm_data_size;
    if (output_pcm_data_size<=0) {
        fclose(inputWavFile);
        inputWavFile = NULL;
        
        fclose(outputWavFile);
        outputWavFile = NULL;
        
        LOGE("Output PCM Data Size is %ld, it is invalid", output_pcm_data_size);
        return false;
    }
    
    inputWavHeader.sub_chunk2_size = output_pcm_data_size;
    fwrite(&inputWavHeader, 1, sizeof(wav_header_t), outputWavFile);
    
    AudioEffect* audioEffect = new AudioEffect(channels, sample_rate);
    if (effectType == 0) {
        if (effect == NOEFFECT) {
            audioEffect->enableEffect(false);
        }else {
            audioEffect->enableEffect(true);
            audioEffect->setUserDefinedEffect(effect);
        }
    }else if (effectType == 1) {
        if (effect == NOEQUALIZER) {
            audioEffect->enableEffect(false);
        }else {
            audioEffect->enableEffect(true);
            audioEffect->setEqualizerStyle(effect);
        }
    }else if (effectType == 2) {
        if (effect == NOREVERB) {
            audioEffect->enableEffect(false);
        }else {
            audioEffect->enableEffect(true);
            audioEffect->setReverbStyle(effect);
        }
    }else {
        audioEffect->enableEffect(false);
    }
    
    long processBytePerTime = sample_rate * channels * 2 * 0.03;
    char* tmp = (char*)malloc(processBytePerTime);
    bool isError = false;
    long haveReadByte = 0;
    long readBytePerTime = 0;
    long haveWriteByte = 0;
    while (true) {
        if (output_pcm_data_size-haveReadByte>=processBytePerTime) {
            readBytePerTime = processBytePerTime;
        }else{
            readBytePerTime = output_pcm_data_size-haveReadByte;
        }
        
        if (readBytePerTime<=0) {
            break;
        }
        
        size_t read = fread(tmp, 1, readBytePerTime, inputWavFile);
        bool isEarlyEof = false;
        if (read!=readBytePerTime) {
            if (feof(inputWavFile)) {
                isEarlyEof = true;
                LOGW("Read Input Wav File To EOF");
            }else{
                LOGE("Read Input Wav File Error, Error Code : %d", ferror(inputWavFile));
                isError = true;
                break;
            }
        }
        
        char* out_pcm_data;
        int out_pcm_data_size = audioEffect->process((char*)tmp, (int)read, &out_pcm_data);
        if (out_pcm_data_size<=0)
        {
        }else{
            fwrite(out_pcm_data, 1, out_pcm_data_size, outputWavFile);
            fflush(outputWavFile);
            haveWriteByte += out_pcm_data_size;
        }

        haveReadByte += read;
        
        if (isEarlyEof) {
            break;
        }
    }
    if (haveWriteByte!=inputWavHeader.sub_chunk2_size) {
        fseek(outputWavFile, 0, SEEK_SET);
        inputWavHeader.sub_chunk2_size = haveWriteByte;
        fwrite(&inputWavHeader, 1, sizeof(wav_header_t), outputWavFile);
        fflush(outputWavFile);
    }
    
    if (inputWavFile) {
        fclose(inputWavFile);
        inputWavFile = NULL;
    }
    
    if (outputWavFile) {
        fclose(outputWavFile);
        outputWavFile = NULL;
    }
    
    if (tmp) {
        free(tmp);
        tmp = NULL;
    }
    
    if (audioEffect) {
        delete audioEffect;
        audioEffect = NULL;
    }
    
    if (isError) {
        MediaFile::deleteFile(outputWavFilePath);
        return false;
    }else{
        return true;
    }
}
