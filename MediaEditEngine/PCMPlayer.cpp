//
//  PCMPlayer.cpp
//  MediaStreamer
//
//  Created by Think on 2019/7/11.
//  Copyright © 2019年 Cell. All rights reserved.
//

#include "PCMPlayer.h"
#include "MediaLog.h"
#include "PCMUtils.h"

PCMPlayer::PCMPlayer(int sampleRate, int numChannels, char* workPCMFilePath)
{
    mSampleRate = sampleRate;
    mNumChannels = numChannels;
    if (workPCMFilePath) {
        mWorkPCMFilePath = strdup(workPCMFilePath);
    }
    
    pthread_mutex_init(&mLock, NULL);
    isOpened = false;
    isPlaying = false;
    
    mAudioRenderType = AUDIO_RENDER_UNKNOWN;
#ifdef ANDROID
    mAudioRenderType = AUDIO_RENDER_AUDIOTRACK;//AUDIO_RENDER_OPENSLES;
#endif
#ifdef IOS
    mAudioRenderType = AUDIO_RENDER_AUDIOUNIT;
#endif
    mAudioRender = NULL;
    
    mWorkPCMFile = NULL;
    mPlayTimeUs = 0ll;
    mDurationUs = 0ll;
    mPcmDB = 0;
    pthread_mutex_init(&mConsumeLock, NULL);
    
#ifdef ANDROID
    mJvm = NULL;
#endif
}

PCMPlayer::~PCMPlayer()
{
    close();
    
    pthread_mutex_destroy(&mConsumeLock);
    
    pthread_mutex_destroy(&mLock);

    if (mWorkPCMFilePath) {
        free(mWorkPCMFilePath);
        mWorkPCMFilePath = NULL;
    }
}

#ifdef ANDROID
void PCMPlayer::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif

bool PCMPlayer::open()
{
    pthread_mutex_lock(&mLock);
    if (isOpened) {
        pthread_mutex_unlock(&mLock);
        return true;
    }
    
    if (mWorkPCMFilePath==NULL) {
        LOGE("Work PCM File Path is null");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    mWorkPCMFile = fopen(mWorkPCMFilePath, "rb");
    if (mWorkPCMFile==NULL) {
        LOGE("[rb] Open Work PCM File Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
#ifdef ANDROID
    //AUDIO_RENDER_AUDIOTRACK or AUDIO_RENDER_OPENSLES
    if (mAudioRenderType==AUDIO_RENDER_AUDIOTRACK) {
        mAudioRender = AudioRender::CreateAudioRenderWithJniEnv(AUDIO_RENDER_AUDIOTRACK, mJvm);
    }else{
        mAudioRender = AudioRender::CreateAudioRender(mAudioRenderType);
    }
#else
    mAudioRender = AudioRender::CreateAudioRender(mAudioRenderType);
#endif
    
    if (mAudioRender==NULL) {
        if (mWorkPCMFile) {
            fclose(mWorkPCMFile);
            mWorkPCMFile = NULL;
        }
        
        LOGE("Create AudioRender Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    mAudioRender->configureAdaptation(mSampleRate, mNumChannels);
    mAudioRender->setPCMDataInputer(this);
    int result = mAudioRender->init(PLAYER_MODE);
    if (result!=0) {
        AudioRender::DeleteAudioRender(mAudioRender, mAudioRenderType);
        mAudioRender = NULL;
        
        if (mWorkPCMFile) {
            fclose(mWorkPCMFile);
            mWorkPCMFile = NULL;
        }
        
        LOGE("Init AudioRender Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    pthread_mutex_lock(&mConsumeLock);
    mPlayTimeUs = 0ll;
    int64_t fileSize = MediaFile::getFileSizeWithStat(mWorkPCMFilePath);
    mDurationUs = fileSize * 1000 * 1000 / (mSampleRate * mNumChannels * 2);
    mPcmDB = 0;
    pthread_mutex_unlock(&mConsumeLock);
    
    isOpened = true;
    pthread_mutex_unlock(&mLock);
    return true;
}

void PCMPlayer::close()
{
    this->stopPlay();

    pthread_mutex_lock(&mLock);
    if (!isOpened) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    if (mAudioRender) {
        mAudioRender->terminate();
        AudioRender::DeleteAudioRender(mAudioRender, mAudioRenderType);
        mAudioRender = NULL;
    }
    
    if (mWorkPCMFile) {
        fclose(mWorkPCMFile);
        mWorkPCMFile = NULL;
    }
    
    isOpened = false;
    pthread_mutex_unlock(&mLock);
}

bool PCMPlayer::startPlay()
{
    pthread_mutex_lock(&mLock);
    if (!isOpened) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isPlaying) {
        pthread_mutex_unlock(&mLock);
        return true;
    }
    
    int result = mAudioRender->startPlayout();
    if (result!=0) {
        LOGE("Start AudioRender Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    isPlaying = true;
    pthread_mutex_unlock(&mLock);
    return true;
}

bool PCMPlayer::isPCMPlaying()
{
    pthread_mutex_lock(&mLock);
    if (!isOpened) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    if (isPlaying) {
        pthread_mutex_unlock(&mLock);
        return true;
    }else{
        pthread_mutex_unlock(&mLock);
        return false;
    }
}

void PCMPlayer::stopPlay()
{
    pthread_mutex_lock(&mLock);
    if (!isOpened) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    if (!isPlaying) {
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    int result = mAudioRender->stopPlayout();
    if (result!=0) {
        LOGE("Stop AudioRender Fail");
        pthread_mutex_unlock(&mLock);
        return;
    }
    
    pthread_mutex_lock(&mConsumeLock);
    mPcmDB = 0;
    pthread_mutex_unlock(&mConsumeLock);
    
    isPlaying = false;
    pthread_mutex_unlock(&mLock);
}

bool PCMPlayer::seek(int64_t seekTimeUs)
{
    pthread_mutex_lock(&mLock);
    if (!isOpened) {
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    int64_t seekByte = (seekTimeUs / 1000) * (mSampleRate * mNumChannels * 2) / 1000;
    if (seekByte%(mNumChannels * 2)!=0) {
        int64_t seekSamples = seekByte/(mNumChannels * 2);
        seekByte = seekSamples*(mNumChannels * 2);
    }
    pthread_mutex_lock(&mConsumeLock);
    int ret = fseek(mWorkPCMFile, (long)seekByte, SEEK_SET);
    if (ret) {
        pthread_mutex_unlock(&mConsumeLock);

        LOGE("Seek Work PCM File Fail");
        pthread_mutex_unlock(&mLock);
        return false;
    }
    
    mPlayTimeUs = seekTimeUs;
    pthread_mutex_unlock(&mConsumeLock);
    
    pthread_mutex_unlock(&mLock);
    return true;
}

int PCMPlayer::onInputPCMData(uint8_t **pData, int size)
{
    size_t ret = 0;
    
    pthread_mutex_lock(&mConsumeLock);
    ret = fread(*pData, 1, size, mWorkPCMFile);
    if (feof(mWorkPCMFile)) {
        mPlayTimeUs = mDurationUs;
    }else{
        mPlayTimeUs += ret * 1000 * 1000/(mSampleRate * mNumChannels * 2);
    }
    if (ret<=0) {
        mPcmDB = 0;
    }else{
        mPcmDB = PCMUtils::getPcmDB((unsigned char *)(*pData), ret);
        if (mPcmDB<0) {
            mPcmDB = 0;
        }
    }
    pthread_mutex_unlock(&mConsumeLock);

    return (int)ret;
}

int64_t PCMPlayer::getPlayTimeUs()
{
    int64_t ret = 0;
    pthread_mutex_lock(&mConsumeLock);
    ret = mPlayTimeUs;
    pthread_mutex_unlock(&mConsumeLock);
    return ret;
}

int64_t PCMPlayer::getDurationUs()
{
    int64_t ret = 0;
    pthread_mutex_lock(&mConsumeLock);
    ret = mDurationUs;
    pthread_mutex_unlock(&mConsumeLock);
    return ret;
}

int PCMPlayer::getPcmDB()
{
    int ret = 0;
    pthread_mutex_lock(&mConsumeLock);
    ret = mPcmDB;
    mPcmDB = 0;
    pthread_mutex_unlock(&mConsumeLock);
    return ret;
}
