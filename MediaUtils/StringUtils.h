//
//  StringUtils.hpp
//  MediaStreamer
//
//  Created by Think on 2016/12/20.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef StringUtils_h
#define StringUtils_h

#include <stdio.h>

class StringUtils {
    
public:
    static char* left(char *dst,char *src, int n);
    static char * mid(char *dst,char *src, int n,int m);
    static char * right(char *dst,char *src, int n);
    
    static char* cat(char* s1, char* s2);
    
    static size_t strlcpy(char * dst, const char * src, size_t maxlen);
};


#endif /* StringUtils_h */
