/*
 *  Copyright (c) 2017 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 *
 */

#include "objc_h265.h"

#if defined(WEBRTC_IOS)
#include <string>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <sys/param.h>
#import <mach/mach_host.h>
#import <sys/sysctl.h>
#include <mach/mach_time.h>
#include <CoreVideo/CVHostTime.h>
#include "rtc_base/logging.h"
#else
#include <string>
#import <Foundation/Foundation.h>
#import <sys/param.h>
#import <mach/mach_host.h>
#import <sys/sysctl.h>
#include <mach/mach_time.h>
#include <CoreVideo/CVHostTime.h>
#include "rtc_base/logging.h"
#endif

#if defined(WEBRTC_IOS)
bool isHWH265EncodeSupported() {

    static int supportCode = -1;
    if(supportCode==-1)
    {
        float systemversion = [[[UIDevice currentDevice] systemVersion] floatValue];
        if(systemversion>=11.0)
        {
            std::string iOSDeviceId;
            
            // Gets a string with the device model
            size_t size;
            sysctlbyname("hw.machine", NULL, &size, NULL, 0);
            char *machine = new char[size];
            if (sysctlbyname("hw.machine", machine, &size, NULL, 0) == 0 && machine[0])
            {
                iOSDeviceId.assign(machine, size -1);
            }else
            {
                iOSDeviceId = "unknown0,0";
            }
            
            delete [] machine;
            
            const char* iOSDeviceIdStr = iOSDeviceId.c_str();
            if(strncmp(iOSDeviceIdStr, "AirPods", 7)==0
               || strncmp(iOSDeviceIdStr, "AppleTV", 7)==0
               || strncmp(iOSDeviceIdStr, "Watch", 5)==0
               || strncmp(iOSDeviceIdStr, "AudioAccessory", 14)==0
               || strncmp(iOSDeviceIdStr, "iPod", 4)==0)
            {
                supportCode = 0;
            }else if(strncmp(iOSDeviceIdStr, "iPad", 4)==0)
            {
                if(iOSDeviceId=="iPad1,1"
                   || iOSDeviceId=="iPad2,1"
                   || iOSDeviceId=="iPad2,2"
                   || iOSDeviceId=="iPad2,3"
                   || iOSDeviceId=="iPad2,4"
                   || iOSDeviceId=="iPad3,1"
                   || iOSDeviceId=="iPad3,2"
                   || iOSDeviceId=="iPad3,3"
                   || iOSDeviceId=="iPad3,4"
                   || iOSDeviceId=="iPad3,5"
                   || iOSDeviceId=="iPad3,6"
                   || iOSDeviceId=="iPad6,11"
                   || iOSDeviceId=="iPad6,12"
                   || iOSDeviceId=="iPad4,1"
                   || iOSDeviceId=="iPad4,2"
                   || iOSDeviceId=="iPad4,3"
                   || iOSDeviceId=="iPad5,3"
                   || iOSDeviceId=="iPad5,4"
                   || iOSDeviceId=="iPad2,5"
                   || iOSDeviceId=="iPad2,6"
                   || iOSDeviceId=="iPad2,7"
                   || iOSDeviceId=="iPad4,4"
                   || iOSDeviceId=="iPad4,5"
                   || iOSDeviceId=="iPad4,6"
                   || iOSDeviceId=="iPad4,7"
                   || iOSDeviceId=="iPad4,8"
                   || iOSDeviceId=="iPad4,9"
                   || iOSDeviceId=="iPad5,1"
                   || iOSDeviceId=="iPad5,2")
                {
                    supportCode = 0;
                }else{
                    supportCode = 1;
                }
            }else if(strncmp(iOSDeviceIdStr, "iPhone", 6)==0)
            {
                if(iOSDeviceId=="iPhone1,1"
                   || iOSDeviceId=="iPhone1,2"
                   || iOSDeviceId=="iPhone2,1"
                   || iOSDeviceId=="iPhone3,1"
                   || iOSDeviceId=="iPhone3,2"
                   || iOSDeviceId=="iPhone3,3"
                   || iOSDeviceId=="iPhone4,1"
                   || iOSDeviceId=="iPhone5,1"
                   || iOSDeviceId=="iPhone5,2"
                   || iOSDeviceId=="iPhone5,3"
                   || iOSDeviceId=="iPhone5,4"
                   || iOSDeviceId=="iPhone6,1"
                   || iOSDeviceId=="iPhone6,2"
                   || iOSDeviceId=="iPhone7,2"
                   || iOSDeviceId=="iPhone7,1"
                   || iOSDeviceId=="iPhone8,1"
                   || iOSDeviceId=="iPhone8,2"
                   || iOSDeviceId=="iPhone8,4")
                {
                    supportCode = 0;
                }else{
                    supportCode = 1;
                }
            }else{
                supportCode = 0;
            }
            
        }else{
            supportCode = 0;
        }
    }
    
    return (supportCode==1);
}

bool isHWH265DecodeSupported() 
{
    static int supportCode = -1;
    if(supportCode==-1)
    {
        float systemversion = [[[UIDevice currentDevice] systemVersion] floatValue];
        if(systemversion>=11.0)
        {
            std::string iOSDeviceId;
            
            // Gets a string with the device model
            size_t size;
            sysctlbyname("hw.machine", NULL, &size, NULL, 0);
            char *machine = new char[size];
            if (sysctlbyname("hw.machine", machine, &size, NULL, 0) == 0 && machine[0])
            {
                iOSDeviceId.assign(machine, size -1);
            }else
            {
                iOSDeviceId = "unknown0,0";
            }
            
            delete [] machine;
            
            const char* iOSDeviceIdStr = iOSDeviceId.c_str();
            if(strncmp(iOSDeviceIdStr, "AirPods", 7)==0
               || strncmp(iOSDeviceIdStr, "AppleTV", 7)==0
               || strncmp(iOSDeviceIdStr, "Watch", 5)==0
               || strncmp(iOSDeviceIdStr, "AudioAccessory", 14)==0
               || strncmp(iOSDeviceIdStr, "iPod", 4)==0)
            {
                supportCode = 0;
            }else if(strncmp(iOSDeviceIdStr, "iPad", 4)==0)
            {
                if(iOSDeviceId=="iPad1,1"
                   || iOSDeviceId=="iPad2,1"
                   || iOSDeviceId=="iPad2,2"
                   || iOSDeviceId=="iPad2,3"
                   || iOSDeviceId=="iPad2,4"
                   || iOSDeviceId=="iPad3,1"
                   || iOSDeviceId=="iPad3,2"
                   || iOSDeviceId=="iPad3,3"
                   || iOSDeviceId=="iPad3,4"
                   || iOSDeviceId=="iPad3,5"
                   || iOSDeviceId=="iPad3,6"
                   || iOSDeviceId=="iPad4,1"
                   || iOSDeviceId=="iPad4,2"
                   || iOSDeviceId=="iPad4,3"
                   || iOSDeviceId=="iPad5,3"
                   || iOSDeviceId=="iPad5,4"
                   || iOSDeviceId=="iPad2,5"
                   || iOSDeviceId=="iPad2,6"
                   || iOSDeviceId=="iPad2,7"
                   || iOSDeviceId=="iPad4,4"
                   || iOSDeviceId=="iPad4,5"
                   || iOSDeviceId=="iPad4,6"
                   || iOSDeviceId=="iPad4,7"
                   || iOSDeviceId=="iPad4,8"
                   || iOSDeviceId=="iPad4,9"
                   || iOSDeviceId=="iPad5,1"
                   || iOSDeviceId=="iPad5,2")
                {
                    supportCode = 0;
                }else{
                    supportCode = 1;
                }
            }else if(strncmp(iOSDeviceIdStr, "iPhone", 6)==0)
            {
                if(iOSDeviceId=="iPhone1,1"
                   || iOSDeviceId=="iPhone1,2"
                   || iOSDeviceId=="iPhone2,1"
                   || iOSDeviceId=="iPhone3,1"
                   || iOSDeviceId=="iPhone3,2"
                   || iOSDeviceId=="iPhone3,3"
                   || iOSDeviceId=="iPhone4,1"
                   || iOSDeviceId=="iPhone5,1"
                   || iOSDeviceId=="iPhone5,2"
                   || iOSDeviceId=="iPhone5,3"
                   || iOSDeviceId=="iPhone5,4"
                   || iOSDeviceId=="iPhone6,1"
                   || iOSDeviceId=="iPhone6,2"
                   || iOSDeviceId=="iPhone7,2"
                   || iOSDeviceId=="iPhone7,1")
                {
                    supportCode = 0;
                }else{
                    supportCode = 1;
                }
            }else{
                supportCode = 0;
            }
            
        }else{
            supportCode = 0;
        }
    }
    
    return (supportCode==1);
}

std::string getAppleDeviceModel()
{
    std::string iOSDeviceId;
    
    // Gets a string with the device model
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = new char[size];
    if (sysctlbyname("hw.machine", machine, &size, NULL, 0) == 0 && machine[0])
    {
        iOSDeviceId.assign(machine, size -1);
    }else
    {
        iOSDeviceId = "unknown0,0";
    }
    
    delete [] machine;

    return iOSDeviceId;
}

#else
bool isHWH265EncodeSupported()
{
  if(@available(macOS 10.13, *)) return true;
  else return false;
}

bool isHWH265DecodeSupported()
{
  if(@available(macOS 10.13, *)) return true;
  else return false;
}

std::string getAppleDeviceModel()
{
    std::string macOSDeviceId;

    // Gets a string with the device model
    size_t size;
    sysctlbyname("hw.model", NULL, &size, NULL, 0);
    char *machine = new char[size];
    if (sysctlbyname("hw.model", machine, &size, NULL, 0) == 0 && machine[0])
    {
        macOSDeviceId.assign(machine, size -1);
    }else
    {
        macOSDeviceId = "unknown0,0";
    }
    delete [] machine;

    return macOSDeviceId;
}

#endif

#if defined(WEBRTC_MAC) && !defined(WEBRTC_IOS)
bool isAppleMSeriesDevice()
{
    bool is_support = false;
    size_t size;
    sysctlbyname("machdep.cpu.brand_string", NULL, &size, NULL, 0);
    char *machine = new char[size];
    memset(machine, 0, size);
    if (sysctlbyname("machdep.cpu.brand_string", machine, &size, NULL, 0) == 0 && machine[0])
    {
        if(strncmp("Apple M", machine, 7)==0)
        {
            is_support = true;
        }
    }
    delete [] machine;

    return is_support;
}

#endif