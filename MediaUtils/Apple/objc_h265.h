/*
 *  Copyright (c) 2017 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 *
 */

#ifndef SDK_OBJC_FRAMEWORK_CLASSES_VIDEOTOOLBOX_OBJC_H265_H_
#define SDK_OBJC_FRAMEWORK_CLASSES_VIDEOTOOLBOX_OBJC_H265_H_

#include <string>

bool isHWH265EncodeSupported();
bool isHWH265DecodeSupported();

std::string getAppleDeviceModel();

#if defined(WEBRTC_MAC) && !defined(WEBRTC_IOS)
bool isAppleMSeriesDevice();
#endif


#endif  // SDK_OBJC_FRAMEWORK_CLASSES_VIDEOTOOLBOX_OBJC_H265_H_
