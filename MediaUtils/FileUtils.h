//
//  FileUtils.h
//
//  Created by Think on 16/2/23.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef FileUtils_h
#define FileUtils_h

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string>

class FileUtils
{
public:
    static bool deleteFile(char * filePath);
    
    static int64_t getFileSize(const char *path);

    static FILE* fopen(std::string filename, std::string mode);
};

#endif /* FileUtils_h */
