//
//  FileUtils.cpp
//
//  Created by Think on 16/2/23.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "FileUtils.h"
#include <sys/stat.h>

#ifdef WIN32
#include <locale>
#include <codecvt>
#include <windows.h>
#endif

#ifdef WIN32
std::wstring string_to_wstring(std::string& input) {
    std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
    return converter.from_bytes(input);
}
#endif

bool FileUtils::deleteFile(char *filePath)
{
    return false;
}

int64_t FileUtils::getFileSize(const char *path)
{
    int64_t filesize = -1;
    struct stat statbuff;
    if(stat(path, &statbuff) < 0){
        return filesize;
    }else{
        filesize = statbuff.st_size;
    }
    return filesize;
}

FILE* FileUtils::fopen(std::string filename, std::string mode)
{
#if defined(WIN32)
    return _wfopen(string_to_wstring(filename).c_str(), string_to_wstring(mode).c_str());
#else
    return fopen(filename.c_str(), mode.c_str());
#endif 
}