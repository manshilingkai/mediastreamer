#include "DeviceInfo.h"
#include <string.h>
#include "MediaLog.h"

DeviceInfo *DeviceInfo::GetInstance() {
	static DeviceInfo deviceInfo;
	return &deviceInfo;
}

DeviceInfo::DeviceInfo() {
	product_model[0] = '\0';
	version_release[0] = '\0';
	version_sdk[0] = '\0';
	version_incremental[0] = '\0';
	product_device[0] = '\0';
	product_manufacturer[0] = '\0';

	int ret = -1;
	ret = __system_property_get("ro.product.model", product_model);

	if (ret <= 0) {
		LOGW("The property [ro.product.model] is not defined");
	}

	ret = __system_property_get("ro.build.version.release", version_release);

	if (ret <= 0) {
		LOGW("The property [ro.build.version.release] is not defined");
	}

	ret = __system_property_get("ro.build.version.sdk", version_sdk);
	if (ret <= 0) {
		LOGW("The property [ro.build.version.sdk] is not defined");
	}

	ret = __system_property_get("ro.build.version.incremental",
			version_incremental);

	if (ret <= 0) {
		LOGW("The property [ro.build.version.incremental] is not defined");
	}

	ret = __system_property_get("ro.product.device", product_device);

	if (ret <= 0) {
		LOGW("The property [ro.product.device] is not defined");
	}

	ret = __system_property_get("ro.product.manufacturer", product_manufacturer);

	if (ret <= 0) {
		LOGW("The property [ro.product.manufacturer] is not defined");
	}
}

DeviceInfo::~DeviceInfo() {

}

char *DeviceInfo::get_Product_Model() {
	return product_model;
}

char *DeviceInfo::get_Version_Release() {
	return version_release;
}

char *DeviceInfo::get_Version_Sdk() {
	return version_sdk;
}

char *DeviceInfo::get_Version_Incremental() {
	return version_incremental;
}

char *DeviceInfo::get_Product_Device() {
	return product_device;
}

char *DeviceInfo::get_Product_Manufacturer() {
	return product_manufacturer;
}
