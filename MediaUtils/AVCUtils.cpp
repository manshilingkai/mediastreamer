//
//  AVCUtils.cpp
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "AVCUtils.h"
#include <string.h>

#define UUID_SIZE 16
static unsigned char start_code[] = {0x00,0x00,0x00,0x01};

/* NOTE: I noticed that FFmpeg does some unusual special handling of certain
 * scenarios that I was unaware of, so instead of just searching for {0, 0, 1}
 * we'll just use the code from FFmpeg - http://www.ffmpeg.org/ */
const uint8_t *AVCUtils::ff_avc_find_startcode_internal(const uint8_t *p, const uint8_t *end)
{
    const uint8_t *a = p + 4 - ((intptr_t)p & 3);
    
    for (end -= 3; p < a && p < end; p++) {
        if (p[0] == 0 && p[1] == 0 && p[2] == 1)
            return p;
    }
    
    for (end -= 3; p < end; p += 4) {
        uint32_t x = *(const uint32_t*)p;
        
        if ((x - 0x01010101) & (~x) & 0x80808080) {
            if (p[1] == 0) {
                if (p[0] == 0 && p[2] == 1)
                    return p;
                if (p[2] == 0 && p[3] == 1)
                    return p+1;
            }
            
            if (p[3] == 0) {
                if (p[2] == 0 && p[4] == 1)
                    return p+2;
                if (p[4] == 0 && p[5] == 1)
                    return p+3;
            }
        }
    }
    
    for (end += 3; p < end; p++) {
        if (p[0] == 0 && p[1] == 0 && p[2] == 1)
            return p;
    }
    
    return end + 3;
}

const uint8_t *AVCUtils::avc_find_startcode(const uint8_t *p, const uint8_t *end)
{
    const uint8_t *out= ff_avc_find_startcode_internal(p, end);
    if (p < out && out < end && !out[-1]) out--;
    return out;
}

bool AVCUtils::avc_keyframe(const uint8_t *data, size_t size)
{
    const uint8_t *nal_start, *nal_end;
    const uint8_t *end = data + size;
    int type;
    
    nal_start = avc_find_startcode(data, end);
    while (true) {
        while (nal_start < end && !*(nal_start++));
        
        if (nal_start == end)
            break;
        
        type = nal_start[0] & 0x1F;
        
        if (type == 5 || type == 1)
            return (type == 5);
        
        nal_end = avc_find_startcode(nal_start, end);
        nal_start = nal_end;
    }
    
    return false;
}

static uint32_t reversebytes(uint32_t value) {
    return (value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 |
        (value & 0x00FF0000U) >> 8 | (value & 0xFF000000U) >> 24;
}

static uint32_t get_sei_nalu_size(uint32_t content)
{
    //SEI payload size
    uint32_t sei_payload_size = content + UUID_SIZE;
    //NALU + payload类型 + 数据长度 + 数据 
    uint32_t sei_size = 1 + 1 + (sei_payload_size / 0xFF + (sei_payload_size % 0xFF != 0 ? 1 : 0)) + sei_payload_size;
    //截止码 
    uint32_t tail_size = 2;
    if (sei_size % 2 == 1)
    {
        tail_size -= 1;
    }
    sei_size += tail_size;
 
    return sei_size;
}
 
static uint32_t get_sei_packet_size(uint32_t size)
{
    return get_sei_nalu_size(size) + 4;
}

static unsigned char uuid[] = { 0x54, 0x80, 0x83, 0x97, 0xf0, 0x23, 0x47, 0x4b, 0xb7, 0xf7, 0x4f, 0x32, 0xb5, 0x4e, 0x06, 0xac };

int AVCUtils::fill_sei_packet(unsigned char * packet,bool isAnnexb, const char * content, uint32_t size)
{
        unsigned char * data = (unsigned char*)packet;
        unsigned int nalu_size = (unsigned int)get_sei_nalu_size(size);
        uint32_t sei_size = nalu_size;
        //大端转小端 
        nalu_size = reversebytes(nalu_size);
     
        //NALU开始码 
        unsigned int * size_ptr = &nalu_size;
        if (isAnnexb)
        {
            memcpy(data, start_code, sizeof(unsigned int));
        }
        else
        {
            memcpy(data, size_ptr, sizeof(unsigned int));
        }
        data += sizeof(unsigned int);
     
        unsigned char * sei = data;
        //NAL header
        *data++ = 6; //SEI
        //sei payload type
        *data++ = 5; //unregister
        size_t sei_payload_size = size + UUID_SIZE;
        //数据长度 
        while (true)
        {
            *data++ = (sei_payload_size >= 0xFF ? 0xFF : (char)sei_payload_size);
            if (sei_payload_size < 0xFF) break;
            sei_payload_size -= 0xFF;
        }
     
        //UUID
        memcpy(data, uuid, UUID_SIZE);
        data += UUID_SIZE;
        //数据 
        memcpy(data, content, size);
        data += size;
     
        //tail 截止对齐码 
        if (sei + sei_size - data == 1)
        {
            *data = 0x80;
        }
        else if (sei + sei_size - data == 2)
        {
            *data++ = 0x00;
            *data++ = 0x80;
        }
     
        return true;
}
