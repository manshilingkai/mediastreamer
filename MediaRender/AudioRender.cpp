//
//  AudioRender.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "AudioRender.h"

#ifdef IOS
#import "AudioUnitAudioRender.h"
#endif

#ifdef ANDROID
#include "OpenSLESRender.h"
#include "JniAudioTrackRender.h"
#endif

#ifdef MAC
#include "SDLAudioRender.h"
#include "audio_device_mac.h"
#endif

#ifdef WIN32
#include "WinWaveAudioRender.h"
#endif

AudioRender* AudioRender::CreateAudioRender(AudioRenderType type)
{
    #ifdef IOS
    if(type==AUDIO_RENDER_AUDIOUNIT)
    {
        return new AudioUnitAudioRender;
    }
    #endif
    
    #ifdef ANDROID
    if (type==AUDIO_RENDER_OPENSLES) {
        return new OpenSLESRender;
    }
    #endif
    
    #ifdef MAC
    if (type==AUDIO_RENDER_SDL) {
        return new SDLAudioRender;
    }
    if (type==AUDIO_RENDER_MAC) {
        return new AudioDeviceMac;
    }
    #endif
    
	#ifdef WIN32
	if (type==AUDIO_RENDER_WIN_WAVE)
	{
		return new WinWaveAudioRender;
	}
	#endif
    
    return NULL;
}

#ifdef ANDROID
AudioRender* AudioRender::CreateAudioRenderWithJniEnv(AudioRenderType type, JavaVM *jvm)
{
    if (type == AUDIO_RENDER_AUDIOTRACK) {
        return new JniAudioTrackRender(jvm);
    }
    
    return NULL;
}
#endif

void AudioRender::DeleteAudioRender(AudioRender* audioRender, AudioRenderType type)
{
    #ifdef IOS
    if (type == AUDIO_RENDER_AUDIOUNIT) {
        AudioUnitAudioRender* audioUnitAudioRender = (AudioUnitAudioRender*)audioRender;
        delete audioUnitAudioRender;
        audioUnitAudioRender = NULL;
    }
    #endif
    
    #ifdef ANDROID
    if (type==AUDIO_RENDER_OPENSLES) {
        OpenSLESRender *openSLESRender = (OpenSLESRender*)audioRender;
        delete openSLESRender;
        openSLESRender = NULL;
    }
    
    if (type==AUDIO_RENDER_AUDIOTRACK) {
        JniAudioTrackRender* jniAudioTrackRender = (JniAudioTrackRender*)audioRender;
        delete jniAudioTrackRender;
        jniAudioTrackRender = NULL;
    }
    #endif
    
    #ifdef MAC
    if (type==AUDIO_RENDER_SDL) {
        SDLAudioRender* sdlAudioRender = (SDLAudioRender*)audioRender;
        delete sdlAudioRender;
        sdlAudioRender = NULL;
    }
    
    if (type==AUDIO_RENDER_MAC) {
        AudioDeviceMac* macAudioRender = (AudioDeviceMac*)audioRender;
        delete macAudioRender;
        macAudioRender = NULL;
    }
    #endif

	#ifdef WIN32
	if (type == AUDIO_RENDER_WIN_WAVE)
	{
		WinWaveAudioRender* winWaveAudioRender = (WinWaveAudioRender*)audioRender;
		if (winWaveAudioRender)
		{
			delete winWaveAudioRender;
			winWaveAudioRender = NULL;
		}
	}
	#endif
}
