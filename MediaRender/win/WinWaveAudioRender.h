#ifndef WinWaveAudioRender_h
#define WinWaveAudioRender_h

#include "WinAudioMixerManager.h"
#include "AudioRender.h"

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#pragma comment( lib, "winmm.lib" )

const uint32_t N_PLAY_SAMPLES_PER_SEC = 44100;
const uint32_t N_PLAY_CHANNELS = 2; // default is stereo playout

									// NOTE - CPU load will not be correct for other sizes than 10ms
const uint32_t PLAY_BUF_SIZE_IN_SAMPLES = (N_PLAY_SAMPLES_PER_SEC / 100);

enum { N_BUFFERS_OUT = 16 };

static const int kAdmMaxDeviceNameSize = 128;
static const int kAdmMaxGuidSize = 128;

class WinWaveAudioRender : public AudioRender
{
public:
	WinWaveAudioRender();
	~WinWaveAudioRender();

	AudioRenderConfigure* configureAdaptation(int sampleRate, int channelCount);

	void setPCMDataInputer(IPCMDataInputer* inputer);

	int init(AudioRenderMode mode);
	int terminate();
	bool isInitialized();

	int startPlayout();
	int stopPlayout();
	bool isPlaying();

	//0-1
	void setVolume(float volume);
	void setMute(bool mute);

	int pushPCMData(uint8_t* data, int size, int64_t pts);

	void flush();

	int64_t getCurrentPts();

private:
	// Main initializaton and termination
	int32_t Init();
	int32_t Terminate();
	bool Initialized() const;

	// Device enumeration
	int16_t PlayoutDevices();
	int32_t PlayoutDeviceName(uint16_t index, char name[kAdmMaxDeviceNameSize], char guid[kAdmMaxGuidSize]);

	// Device selection
	int32_t SetPlayoutDevice(uint16_t index);
	int32_t SetPlayoutDevice(WindowsDeviceType device);

	// Audio transport initialization
	int32_t PlayoutIsAvailable(bool& available);
	int32_t InitPlayout();
	bool PlayoutIsInitialized() const;

	// Audio transport control
	int32_t StartPlayout();
	int32_t StopPlayout();
	bool Playing() const;

	// Volume control based on the Windows Wave API (Windows only)
	int32_t SetWaveOutVolume(uint16_t volumeLeft, uint16_t volumeRight);
	int32_t WaveOutVolume(uint16_t& volumeLeft, uint16_t& volumeRight) const;

	// Audio mixer initialization
	int32_t InitSpeaker();
	bool SpeakerIsInitialized() const;

	// Speaker volume controls
	int32_t SpeakerVolumeIsAvailable(bool& available);
	int32_t SetSpeakerVolume(uint32_t volume);
	int32_t SpeakerVolume(uint32_t& volume) const;
	int32_t MaxSpeakerVolume(uint32_t& maxVolume) const;
	int32_t MinSpeakerVolume(uint32_t& minVolume) const;
	int32_t SpeakerVolumeStepSize(uint16_t& stepSize) const;

	// Speaker mute control
	int32_t SpeakerMuteIsAvailable(bool& available);
	int32_t SetSpeakerMute(bool enable);
	int32_t SpeakerMute(bool& enabled) const;

	// Stereo support
	int32_t StereoPlayoutIsAvailable(bool& available);
	int32_t SetStereoPlayout(bool enable);
	int32_t StereoPlayout(bool& enabled) const;

	bool IsUsingOutputDeviceIndex() const { return _usingOutputDeviceIndex; }
	WindowsDeviceType OutputDevice() const { return _outputDevice; }
	uint16_t OutputDeviceIndex() const { return _outputDeviceIndex; }

	int32_t EnumeratePlayoutDevices();
	void TraceSupportFlags(DWORD dwSupport) const;
	void TraceWaveOutError(MMRESULT error) const;
	int32_t PrepareStartPlayout();

	int32_t GetPlayoutBufferDelay(uint32_t& writtenSamples, uint32_t& playedSamples);
	int32_t Write(int8_t* data, uint16_t nSamples);

private:
	WinAudioMixerManager _mixerManager;

	bool                 _usingOutputDeviceIndex;
	WindowsDeviceType    _outputDevice;
	uint16_t             _outputDeviceIndex;
	bool                 _outputDeviceIsSpecified;

	WAVEFORMATEX         _waveFormatOut;

	HWAVEOUT             _hWaveOut;
	WAVEHDR              _waveHeaderOut[N_BUFFERS_OUT];

	uint8_t              _playChannels;

	int8_t               *_playBuffer[N_BUFFERS_OUT];

	bool                 _initialized;
	bool                 _playing;
	bool                 _playIsInitialized;

	uint16_t             _playBufIndex;          // playout buffer index
	uint16_t             _playBufCount;

	uint32_t             _writtenSamples;
private:
	static void CALLBACK waveOutProc(HWAVEOUT hwo, UINT uMsg, DWORD_PTR dwInstance, DWORD_PTR dwParam1, DWORD_PTR dwParam2);
	void handleWaveOutProc();
private:
	pthread_t mThread;
	pthread_cond_t mCondition;
	pthread_mutex_t mLock;
	bool mIsBreakThread;

	void createAudioRenderThread();
	static void* handleAudioRenderThread(void* ptr);
	void audioRenderThreadMain();
	void deleteAudioRenderThread();

private:
	AudioRenderConfigure audioRenderConfigure;
private:
	// 1 buffer = 10ms
	enum {
		kNumFIFOBuffers = 32,
	};

	uint8_t *fifo;
	int fifoSize;

	int write_pos;
	int read_pos;
	int cache_len;

	int64_t currentPts;
};

#endif
