#include <winsock2.h>
#include <windows.h>
#include <objbase.h>    // CoTaskMemAlloc, CoTaskMemFree
#include <strsafe.h>    // StringCchCopy(), StringCchCat(), StringCchPrintf()
#include <assert.h>

#include "WinWaveAudioRender.h"
#include "MediaLog.h"

// Avoids the need of Windows 7 SDK
#ifndef WAVE_MAPPED_DEFAULT_COMMUNICATION_DEVICE
#define WAVE_MAPPED_DEFAULT_COMMUNICATION_DEVICE   0x0010
#endif

// Supported in Windows Vista and Windows 7.
// http://msdn.microsoft.com/en-us/library/dd370819(v=VS.85).aspx
// Taken from Mmddk.h.
#define DRV_RESERVED                      0x0800
#define DRV_QUERYFUNCTIONINSTANCEID       (DRV_RESERVED + 17)
#define DRV_QUERYFUNCTIONINSTANCEIDSIZE   (DRV_RESERVED + 18)

#define POW2(A) (2 << ((A) - 1))

// ============================================================================
//                            Construction & Destruction
// ============================================================================

// ----------------------------------------------------------------------------
//  WinWaveAudioRender - ctor
// ----------------------------------------------------------------------------

WinWaveAudioRender::WinWaveAudioRender() :
	_usingOutputDeviceIndex(false),
	_outputDevice(kDefaultDevice),
	_outputDeviceIndex(0),
	_outputDeviceIsSpecified(false),
	_initialized(false),
	_playIsInitialized(false),
	_playing(false),
	_hWaveOut(NULL),
	_playChannels(N_PLAY_CHANNELS),
	_playBufIndex(0)
{
	for (size_t i = 0; i < N_BUFFERS_OUT; i++)
	{
		_playBuffer[i] = NULL;
	}

	mIsBreakThread = false;
	_playBufCount = 0;

	LOGD("%s created", __FUNCTION__);
}

// ----------------------------------------------------------------------------
//  WinWaveAudioRender - dtor
// ----------------------------------------------------------------------------

WinWaveAudioRender::~WinWaveAudioRender()
{
	LOGD("%s destroyed", __FUNCTION__);

	Terminate();
}

AudioRenderConfigure* WinWaveAudioRender::configureAdaptation(int sampleRate, int channelCount)
{
	audioRenderConfigure.sampleRate = N_PLAY_SAMPLES_PER_SEC;
	audioRenderConfigure.channelCount = N_PLAY_CHANNELS;

	audioRenderConfigure.sampleFormat = AV_SAMPLE_FMT_S16;

	if (audioRenderConfigure.channelCount == 1) {
		audioRenderConfigure.channelLayout = AV_CH_LAYOUT_MONO;
	}
	else {
		audioRenderConfigure.channelCount = 2;
		audioRenderConfigure.channelLayout = AV_CH_LAYOUT_STEREO;
	}

	return &audioRenderConfigure;
}

void WinWaveAudioRender::setPCMDataInputer(IPCMDataInputer* inputer)
{
	//
}


int WinWaveAudioRender::init(AudioRenderMode mode)
{
	return Init();
}

int WinWaveAudioRender::terminate()
{
	return Terminate();
}

bool WinWaveAudioRender::isInitialized()
{
	return Initialized();
}

int WinWaveAudioRender::startPlayout()
{
	int32_t ret = SetPlayoutDevice(0);
	if (ret) {
		LOGE("SetPlayoutDevice Fail");
		return -1;
	}

	ret = InitPlayout();
	if (ret) {
		LOGE("InitPlayout Fail");
		return -2;
	}

	ret = StartPlayout();
	if (ret) {
		LOGE("StartPlayout Fail");
		return -3;
	}

	return 0;
}

int WinWaveAudioRender::stopPlayout()
{
	return StopPlayout();
}

bool WinWaveAudioRender::isPlaying()
{
	return Playing();
}

void WinWaveAudioRender::setVolume(float volume)
{
	SetSpeakerVolume(volume * 255);
}

void WinWaveAudioRender::setMute(bool mute)
{
	SetSpeakerMute(mute);
}

int WinWaveAudioRender::pushPCMData(uint8_t* data, int size, int64_t pts)
{
	if (data == NULL || size <= 0) return -1;

	pthread_mutex_lock(&mLock);

	if (cache_len + size>fifoSize)
	{
		pthread_mutex_unlock(&mLock);

		return 1;
	}
	else
	{
		if (size>fifoSize - write_pos)
		{
			memcpy(fifo + write_pos, data, fifoSize - write_pos);
			memcpy(fifo, data + fifoSize - write_pos, size - (fifoSize - write_pos));
			write_pos = size - (fifoSize - write_pos);
		}
		else
		{
			memcpy(fifo + write_pos, data, size);
			write_pos = write_pos + size;
		}

		//update pts
		currentPts = pts - _playBufCount *10 *1000 - static_cast<int64_t>(cache_len * 10 * 1000 * kNumFIFOBuffers / fifoSize);;

		cache_len = cache_len + size;

		pthread_mutex_unlock(&mLock);

		pthread_cond_signal(&mCondition);

		return 0;
	}
}

void WinWaveAudioRender::flush()
{
	pthread_mutex_lock(&mLock);
	cache_len = 0;
	write_pos = 0;
	read_pos = 0;
	pthread_mutex_unlock(&mLock);

	pthread_cond_broadcast(&mCondition);
}

int64_t WinWaveAudioRender::getCurrentPts()
{
	int64_t pts = 0;
	pthread_mutex_lock(&mLock);
	pts = currentPts;
	pthread_mutex_unlock(&mLock);
	return pts;
}

// ----------------------------------------------------------------------------
//  Init
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::Init() {

	if (_initialized) {
		return 0;
	}

	_mixerManager.EnumerateAll();

	for (size_t i = 0; i < N_BUFFERS_OUT; i++)
	{
		_playBuffer[i] = (int8_t*)malloc(audioRenderConfigure.sampleRate * audioRenderConfigure.channelCount * av_get_bytes_per_sample(audioRenderConfigure.sampleFormat) / 100);
	}

	pthread_mutex_init(&mLock, NULL);
	pthread_cond_init(&mCondition, NULL);

	fifoSize = audioRenderConfigure.sampleRate * audioRenderConfigure.channelCount * av_get_bytes_per_sample(audioRenderConfigure.sampleFormat) / 100 * kNumFIFOBuffers;
	fifo = (uint8_t *)malloc(fifoSize);
	write_pos = 0;
	read_pos = 0;
	cache_len = 0;

	currentPts = 0;

	_initialized = true;

	return 0;
}

// ----------------------------------------------------------------------------
//  Terminate
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::Terminate()
{
	if (!_initialized)
	{
		return 0;
	}

	_mixerManager.Close();

	pthread_mutex_destroy(&mLock);
	pthread_cond_destroy(&mCondition);

	if (fifo != NULL) {
		free(fifo);
		fifo = NULL;
	}
	write_pos = 0;
	read_pos = 0;
	cache_len = 0;

	for (size_t i = 0; i < N_BUFFERS_OUT; i++)
	{
		if (_playBuffer[i])
		{
			free(_playBuffer[i]);
			_playBuffer[i] = NULL;
		}
	}

	_initialized = false;
	_outputDeviceIsSpecified = false;

	return 0;
}

// ----------------------------------------------------------------------------
//  Initialized
// ----------------------------------------------------------------------------

bool WinWaveAudioRender::Initialized() const
{
	return (_initialized);
}

// ----------------------------------------------------------------------------
//  InitSpeaker
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::InitSpeaker()
{
	if (_playing)
	{
		return -1;
	}

	if (_mixerManager.EnumerateSpeakers() == -1)
	{
		// failed to locate any valid/controllable speaker
		return -1;
	}

	if (IsUsingOutputDeviceIndex())
	{
		if (_mixerManager.OpenSpeaker(OutputDeviceIndex()) == -1)
		{
			return -1;
		}
	}
	else
	{
		if (_mixerManager.OpenSpeaker(OutputDevice()) == -1)
		{
			return -1;
		}
	}

	return 0;
}

// ----------------------------------------------------------------------------
//  SpeakerIsInitialized
// ----------------------------------------------------------------------------

bool WinWaveAudioRender::SpeakerIsInitialized() const
{
	return (_mixerManager.SpeakerIsInitialized());
}

// ----------------------------------------------------------------------------
//  SpeakerVolumeIsAvailable
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::SpeakerVolumeIsAvailable(bool& available)
{

	bool isAvailable(false);

	// Enumerate all avaliable speakers and make an attempt to open up the
	// output mixer corresponding to the currently selected output device.
	//
	if (InitSpeaker() == -1)
	{
		// failed to find a valid speaker
		available = false;
		return 0;
	}

	// Check if the selected speaker has a volume control
	//
	_mixerManager.SpeakerVolumeIsAvailable(isAvailable);
	available = isAvailable;

	// Close the initialized output mixer
	//
	_mixerManager.CloseSpeaker();

	return 0;
}

// ----------------------------------------------------------------------------
//  SetSpeakerVolume
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::SetSpeakerVolume(uint32_t volume)
{

	return (_mixerManager.SetSpeakerVolume(volume));
}

// ----------------------------------------------------------------------------
//  SpeakerVolume
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::SpeakerVolume(uint32_t& volume) const
{

	uint32_t level(0);

	if (_mixerManager.SpeakerVolume(level) == -1)
	{
		return -1;
	}

	volume = level;
	return 0;
}

// ----------------------------------------------------------------------------
//  SetWaveOutVolume
//
//    The low-order word contains the left-channel volume setting, and the
//    high-order word contains the right-channel setting.
//    A value of 0xFFFF represents full volume, and a value of 0x0000 is silence.
//
//    If a device does not support both left and right volume control,
//    the low-order word of dwVolume specifies the volume level,
//    and the high-order word is ignored.
//
//    Most devices do not support the full 16 bits of volume-level control
//    and will not use the least-significant bits of the requested volume setting.
//    For example, if a device supports 4 bits of volume control, the values
//    0x4000, 0x4FFF, and 0x43BE will all be truncated to 0x4000.
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::SetWaveOutVolume(uint16_t volumeLeft, uint16_t volumeRight)
{
	MMRESULT res(0);
	WAVEOUTCAPS caps;

	if (_hWaveOut == NULL)
	{
		LOGW("no open playout device exists => using default");
	}

	// To determine whether the device supports volume control on both
	// the left and right channels, use the WAVECAPS_LRVOLUME flag.
	//
	res = waveOutGetDevCaps((UINT_PTR)_hWaveOut, &caps, sizeof(WAVEOUTCAPS));
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("waveOutGetDevCaps() failed (err=%d)", res);
		TraceWaveOutError(res);
	}
	if (!(caps.dwSupport & WAVECAPS_VOLUME))
	{
		// this device does not support volume control using the waveOutSetVolume API
		LOGE("device does not support volume control using the Wave API");
		return -1;
	}
	if (!(caps.dwSupport & WAVECAPS_LRVOLUME))
	{
		// high-order word (right channel) is ignored
		LOGW("device does not support volume control on both channels");
	}

	DWORD dwVolume(0x00000000);
	dwVolume = (DWORD)(((volumeRight & 0xFFFF) << 16) | (volumeLeft & 0xFFFF));

	res = waveOutSetVolume(_hWaveOut, dwVolume);
	if (MMSYSERR_NOERROR != res)
	{
		LOGE("waveOutSetVolume() failed (err=%d)", res);
		TraceWaveOutError(res);
		return -1;
	}

	return 0;
}

// ----------------------------------------------------------------------------
//  WaveOutVolume
//
//    The low-order word of this location contains the left-channel volume setting,
//    and the high-order word contains the right-channel setting.
//    A value of 0xFFFF (65535) represents full volume, and a value of 0x0000
//    is silence.
//
//    If a device does not support both left and right volume control,
//    the low-order word of the specified location contains the mono volume level.
//
//    The full 16-bit setting(s) set with the waveOutSetVolume function is returned,
//    regardless of whether the device supports the full 16 bits of volume-level
//    control.
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::WaveOutVolume(uint16_t& volumeLeft, uint16_t& volumeRight) const
{
	MMRESULT res(0);
	WAVEOUTCAPS caps;

	if (_hWaveOut == NULL)
	{
		LOGW("no open playout device exists => using default");
	}

	// To determine whether the device supports volume control on both
	// the left and right channels, use the WAVECAPS_LRVOLUME flag.
	//
	res = waveOutGetDevCaps((UINT_PTR)_hWaveOut, &caps, sizeof(WAVEOUTCAPS));
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("waveOutGetDevCaps() failed (err=%d)", res);
		TraceWaveOutError(res);
	}
	if (!(caps.dwSupport & WAVECAPS_VOLUME))
	{
		// this device does not support volume control using the waveOutSetVolume API
		LOGE("device does not support volume control using the Wave API");
		return -1;
	}
	if (!(caps.dwSupport & WAVECAPS_LRVOLUME))
	{
		// high-order word (right channel) is ignored
		LOGW("device does not support volume control on both channels");
	}

	DWORD dwVolume(0x00000000);

	res = waveOutGetVolume(_hWaveOut, &dwVolume);
	if (MMSYSERR_NOERROR != res)
	{
		LOGE("waveOutGetVolume() failed (err=%d)", res);
		TraceWaveOutError(res);
		return -1;
	}

	WORD wVolumeLeft = LOWORD(dwVolume);
	WORD wVolumeRight = HIWORD(dwVolume);

	volumeLeft = static_cast<uint16_t> (wVolumeLeft);
	volumeRight = static_cast<uint16_t> (wVolumeRight);

	return 0;
}

// ----------------------------------------------------------------------------
//  MaxSpeakerVolume
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::MaxSpeakerVolume(uint32_t& maxVolume) const
{
	uint32_t maxVol(0);

	if (_mixerManager.MaxSpeakerVolume(maxVol) == -1)
	{
		return -1;
	}

	maxVolume = maxVol;
	return 0;
}

// ----------------------------------------------------------------------------
//  MinSpeakerVolume
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::MinSpeakerVolume(uint32_t& minVolume) const
{
	uint32_t minVol(0);

	if (_mixerManager.MinSpeakerVolume(minVol) == -1)
	{
		return -1;
	}

	minVolume = minVol;
	return 0;
}

// ----------------------------------------------------------------------------
//  SpeakerVolumeStepSize
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::SpeakerVolumeStepSize(uint16_t& stepSize) const
{

	uint16_t delta(0);

	if (_mixerManager.SpeakerVolumeStepSize(delta) == -1)
	{
		return -1;
	}

	stepSize = delta;
	return 0;
}

// ----------------------------------------------------------------------------
//  SpeakerMuteIsAvailable
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::SpeakerMuteIsAvailable(bool& available)
{

	bool isAvailable(false);

	// Enumerate all avaliable speakers and make an attempt to open up the
	// output mixer corresponding to the currently selected output device.
	//
	if (InitSpeaker() == -1)
	{
		// If we end up here it means that the selected speaker has no volume
		// control, hence it is safe to state that there is no mute control
		// already at this stage.
		available = false;
		return 0;
	}

	// Check if the selected speaker has a mute control
	//
	_mixerManager.SpeakerMuteIsAvailable(isAvailable);
	available = isAvailable;

	// Close the initialized output mixer
	//
	_mixerManager.CloseSpeaker();

	return 0;
}

// ----------------------------------------------------------------------------
//  SetSpeakerMute
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::SetSpeakerMute(bool enable)
{
	return (_mixerManager.SetSpeakerMute(enable));
}

// ----------------------------------------------------------------------------
//  SpeakerMute
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::SpeakerMute(bool& enabled) const
{

	bool muted(0);

	if (_mixerManager.SpeakerMute(muted) == -1)
	{
		return -1;
	}

	enabled = muted;
	return 0;
}


// ----------------------------------------------------------------------------
//  StereoPlayoutIsAvailable
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::StereoPlayoutIsAvailable(bool& available)
{
	available = true;
	return 0;
}


// ----------------------------------------------------------------------------
//  SetStereoPlayout
//
//  Specifies the number of output channels.
//
//  NOTE - the setting will only have an effect after InitPlayout has
//  been called.
//
//  16-bit mono:
//
//  Each sample is 2 bytes. Sample 1 is followed by samples 2, 3, 4, and so on.
//  For each sample, the first byte is the low-order byte of channel 0 and the
//  second byte is the high-order byte of channel 0.
//
//  16-bit stereo:
//
//  Each sample is 4 bytes. Sample 1 is followed by samples 2, 3, 4, and so on.
//  For each sample, the first byte is the low-order byte of channel 0 (left channel);
//  the second byte is the high-order byte of channel 0; the third byte is the
//  low-order byte of channel 1 (right channel); and the fourth byte is the
//  high-order byte of channel 1.
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::SetStereoPlayout(bool enable)
{

	if (enable)
		_playChannels = 2;
	else
		_playChannels = 1;

	return 0;
}

// ----------------------------------------------------------------------------
//  StereoPlayout
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::StereoPlayout(bool& enabled) const
{

	if (_playChannels == 2)
		enabled = true;
	else
		enabled = false;

	return 0;
}

// ----------------------------------------------------------------------------
//  PlayoutDevices
// ----------------------------------------------------------------------------

int16_t WinWaveAudioRender::PlayoutDevices()
{
	return (waveOutGetNumDevs());
}


// ----------------------------------------------------------------------------
//  SetPlayoutDevice I (II)
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::SetPlayoutDevice(uint16_t index)
{

	if (_playIsInitialized)
	{
		return -1;
	}

	UINT nDevices = waveOutGetNumDevs();
	LOGI("number of availiable waveform-audio output devices is %u", nDevices);

	if (index < 0 || index >(nDevices - 1))
	{
		LOGE("device index is out of range [0,%u]", (nDevices - 1));
		return -1;
	}

	_usingOutputDeviceIndex = true;
	_outputDeviceIndex = index;
	_outputDeviceIsSpecified = true;

	return 0;
}

// ----------------------------------------------------------------------------
//  SetPlayoutDevice II (II)
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::SetPlayoutDevice(WindowsDeviceType device)
{
	if (_playIsInitialized)
	{
		return -1;
	}

	if (device == kDefaultDevice)
	{
	}
	else if (device == kDefaultCommunicationDevice)
	{
	}

	_usingOutputDeviceIndex = false;
	_outputDevice = device;
	_outputDeviceIsSpecified = true;

	return 0;
}

// ----------------------------------------------------------------------------
//  PlayoutDeviceName
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::PlayoutDeviceName(
	uint16_t index,
	char name[kAdmMaxDeviceNameSize],
	char guid[kAdmMaxGuidSize])
{

	uint16_t nDevices(PlayoutDevices());

	// Special fix for the case when the user asks for the name of the default device.
	//
	if (index == (uint16_t)(-1))
	{
		index = 0;
	}

	if ((index > (nDevices - 1)) || (name == NULL))
	{
		return -1;
	}

	memset(name, 0, kAdmMaxDeviceNameSize);

	if (guid != NULL)
	{
		memset(guid, 0, kAdmMaxGuidSize);
	}

	WAVEOUTCAPSW caps;    // szPname member (product name (NULL terminated) is a WCHAR
	MMRESULT res;

	res = waveOutGetDevCapsW(index, &caps, sizeof(WAVEOUTCAPSW));
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("waveOutGetDevCapsW() failed (err=%d)", res);
		return -1;
	}
	if (WideCharToMultiByte(CP_UTF8, 0, caps.szPname, -1, name, kAdmMaxDeviceNameSize, NULL, NULL) == 0)
	{
		LOGE("WideCharToMultiByte(CP_UTF8) failed with error code %d - 1", GetLastError());
	}

	if (guid == NULL)
	{
		return 0;
	}

	// It is possible to get the unique endpoint ID string using the Wave API.
	// However, it is only supported on Windows Vista and Windows 7.

	size_t cbEndpointId(0);

	// Get the size (including the terminating null) of the endpoint ID string of the waveOut device.
	// Windows Vista supports the DRV_QUERYFUNCTIONINSTANCEIDSIZE and DRV_QUERYFUNCTIONINSTANCEID messages.
	res = waveOutMessage((HWAVEOUT)IntToPtr(index),
		DRV_QUERYFUNCTIONINSTANCEIDSIZE,
		(DWORD_PTR)&cbEndpointId, NULL);
	if (res != MMSYSERR_NOERROR)
	{
		// DRV_QUERYFUNCTIONINSTANCEIDSIZE is not supported <=> earlier version of Windows than Vista
		LOGI("waveOutMessage(DRV_QUERYFUNCTIONINSTANCEIDSIZE) failed (err=%d)", res);
		TraceWaveOutError(res);
		// Best we can do is to copy the friendly name and use it as guid
		if (WideCharToMultiByte(CP_UTF8, 0, caps.szPname, -1, guid, kAdmMaxGuidSize, NULL, NULL) == 0)
		{
			LOGE("WideCharToMultiByte(CP_UTF8) failed with error code %d - 2", GetLastError());
		}
		return 0;
	}

	// waveOutMessage(DRV_QUERYFUNCTIONINSTANCEIDSIZE) worked => we are on a Vista or Windows 7 device

	WCHAR *pstrEndpointId = NULL;
	pstrEndpointId = (WCHAR*)CoTaskMemAlloc(cbEndpointId);

	// Get the endpoint ID string for this waveOut device.
	res = waveOutMessage((HWAVEOUT)IntToPtr(index),
		DRV_QUERYFUNCTIONINSTANCEID,
		(DWORD_PTR)pstrEndpointId,
		cbEndpointId);
	if (res != MMSYSERR_NOERROR)
	{
		LOGI("waveOutMessage(DRV_QUERYFUNCTIONINSTANCEID) failed (err=%d)", res);
		TraceWaveOutError(res);
		// Best we can do is to copy the friendly name and use it as guid
		if (WideCharToMultiByte(CP_UTF8, 0, caps.szPname, -1, guid, kAdmMaxGuidSize, NULL, NULL) == 0)
		{
			LOGE("WideCharToMultiByte(CP_UTF8) failed with error code %d - 3", GetLastError());
		}
		CoTaskMemFree(pstrEndpointId);
		return 0;
	}

	if (WideCharToMultiByte(CP_UTF8, 0, pstrEndpointId, -1, guid, kAdmMaxGuidSize, NULL, NULL) == 0)
	{
		LOGE("WideCharToMultiByte(CP_UTF8) failed with error code %d - 4", GetLastError());
	}
	CoTaskMemFree(pstrEndpointId);

	return 0;
}

// ----------------------------------------------------------------------------
//  PlayoutIsAvailable
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::PlayoutIsAvailable(bool& available)
{
    available = false;

    // Try to initialize the playout side
    int32_t res = InitPlayout();

    // Cancel effect of initialization
    StopPlayout();

    if (res != -1)
    {
        available = true;
    }

    return 0;
}


// ----------------------------------------------------------------------------
//  InitPlayout
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::InitPlayout()
{
	if (_playing)
	{
		return -1;
	}

	if (!_outputDeviceIsSpecified)
	{
		return -1;
	}

	if (_playIsInitialized)
	{
		return 0;
	}

	// Initialize the speaker (devices might have been added or removed)
	if (InitSpeaker() == -1)
	{
		LOGW("InitSpeaker() failed");
	}

	// Enumerate all availiable output devices
	EnumeratePlayoutDevices();

	// Start by closing any existing wave-output devices
	//
	MMRESULT res(MMSYSERR_ERROR);

	if (_hWaveOut != NULL)
	{
		res = waveOutClose(_hWaveOut);
		if (MMSYSERR_NOERROR != res)
		{
			LOGW("waveOutClose() failed (err=%d)", res);
			TraceWaveOutError(res);
		}
	}

	// Set the output wave format
	//
	WAVEFORMATEX waveFormat;

	waveFormat.wFormatTag = WAVE_FORMAT_PCM;
	waveFormat.nChannels = _playChannels;  // mono <=> 1, stereo <=> 2
	waveFormat.nSamplesPerSec = N_PLAY_SAMPLES_PER_SEC;
	waveFormat.wBitsPerSample = 16;
	waveFormat.nBlockAlign = waveFormat.nChannels * (waveFormat.wBitsPerSample / 8);
	waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
	waveFormat.cbSize = 0;

	// Open the given waveform-audio output device for playout
	//
	HWAVEOUT hWaveOut(NULL);

	if (IsUsingOutputDeviceIndex())
	{
		// verify settings first
		res = waveOutOpen(NULL, _outputDeviceIndex, &waveFormat, 0, 0, CALLBACK_NULL | WAVE_FORMAT_QUERY);
		if (MMSYSERR_NOERROR == res)
		{
			// open the given waveform-audio output device for recording
			res = waveOutOpen(&hWaveOut, _outputDeviceIndex, &waveFormat, (DWORD_PTR)waveOutProc, (DWORD_PTR) this, CALLBACK_FUNCTION);
			LOGI("opening output device corresponding to device ID %u", _outputDeviceIndex);
		}
	}
	else
	{
		if (_outputDevice == kDefaultCommunicationDevice)
		{
			// check if it is possible to open the default communication device (supported on Windows 7)
			res = waveOutOpen(NULL, WAVE_MAPPER, &waveFormat, 0, 0, CALLBACK_NULL | WAVE_MAPPED_DEFAULT_COMMUNICATION_DEVICE | WAVE_FORMAT_QUERY);
			if (MMSYSERR_NOERROR == res)
			{
				// if so, open the default communication device for real
				res = waveOutOpen(&hWaveOut, WAVE_MAPPER, &waveFormat, (DWORD_PTR)waveOutProc, (DWORD_PTR) this, CALLBACK_FUNCTION | WAVE_MAPPED_DEFAULT_COMMUNICATION_DEVICE);
				LOGI("opening default communication device");
			}
			else
			{
				// use default device since default communication device was not avaliable
				res = waveOutOpen(&hWaveOut, WAVE_MAPPER, &waveFormat, (DWORD_PTR)waveOutProc, (DWORD_PTR) this, CALLBACK_FUNCTION);
				LOGI("unable to open default communication device => using default instead");
			}
		}
		else if (_outputDevice == kDefaultDevice)
		{
			// open default device since it has been requested
			res = waveOutOpen(NULL, WAVE_MAPPER, &waveFormat, 0, 0, CALLBACK_NULL | WAVE_FORMAT_QUERY);
			if (MMSYSERR_NOERROR == res)
			{
				res = waveOutOpen(&hWaveOut, WAVE_MAPPER, &waveFormat, (DWORD_PTR)waveOutProc, (DWORD_PTR) this, CALLBACK_FUNCTION);
				LOGI("opening default output device");
			}
		}
	}

	if (MMSYSERR_NOERROR != res)
	{
		LOGE("waveOutOpen() failed (err=%d)", res);
		TraceWaveOutError(res);
		return -1;
	}

	// Log information about the aquired output device
	//
	WAVEOUTCAPS caps;

	res = waveOutGetDevCaps((UINT_PTR)hWaveOut, &caps, sizeof(WAVEOUTCAPS));
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("waveOutGetDevCaps() failed (err=%d)", res);
		TraceWaveOutError(res);
	}

	UINT deviceID(0);
	res = waveOutGetID(hWaveOut, &deviceID);
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("waveOutGetID() failed (err=%d)", res);
		TraceWaveOutError(res);
	}
	LOGI("utilized device ID : %u", deviceID);
	LOGI("product name       : %s", caps.szPname);

	// Store valid handle for the open waveform-audio output device
	_hWaveOut = hWaveOut;

	// Store the input wave header as well
	_waveFormatOut = waveFormat;

	// Prepare wave-out headers
	//
	const uint8_t bytesPerSample = 2 * _playChannels;

	for (int n = 0; n < N_BUFFERS_OUT; n++)
	{
		// set up the output wave header
		_waveHeaderOut[n].lpData = reinterpret_cast<LPSTR>(_playBuffer[n]);
		_waveHeaderOut[n].dwBufferLength = bytesPerSample*PLAY_BUF_SIZE_IN_SAMPLES;
		_waveHeaderOut[n].dwFlags = 0;
		_waveHeaderOut[n].dwLoops = 0;

		memset(_playBuffer[n], 0, bytesPerSample*PLAY_BUF_SIZE_IN_SAMPLES);

		// The waveOutPrepareHeader function prepares a waveform-audio data block for playback.
		// The lpData, dwBufferLength, and dwFlags members of the WAVEHDR structure must be set
		// before calling this function.
		//
		res = waveOutPrepareHeader(_hWaveOut, &_waveHeaderOut[n], sizeof(WAVEHDR));
		if (MMSYSERR_NOERROR != res)
		{
			LOGW("waveOutPrepareHeader(%d) failed (err=%d)", n, res);
			TraceWaveOutError(res);
		}

		// perform extra check to ensure that the header is prepared
		if (_waveHeaderOut[n].dwFlags != WHDR_PREPARED)
		{
			LOGW("waveOutPrepareHeader(%d) failed (dwFlags != WHDR_PREPARED)", n);
		}
	}

	// Mark playout side as initialized
	_playIsInitialized = true;

	_playBufIndex = 0;          // index of active output wave header (<=> output buffer index)

	_writtenSamples = 0;

	return 0;
}


// ----------------------------------------------------------------------------
//  PlayoutIsInitialized
// ----------------------------------------------------------------------------

bool WinWaveAudioRender::PlayoutIsInitialized() const
{
	return (_playIsInitialized);
}

int32_t WinWaveAudioRender::StartPlayout()
{
	if (!_playIsInitialized)
	{
		return -1;
	}

	if (_playing)
	{
		return 0;
	}

	if (PrepareStartPlayout() == 0)
	{
		_playing = true;
	}
	else
	{
		LOGE("failed to activate playing");
		return -1;
	}

	createAudioRenderThread();

	return 0;
}


// ----------------------------------------------------------------------------
//  StopPlayout
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::StopPlayout()
{
	if (!_playIsInitialized)
	{
		return 0;
	}

	if (_hWaveOut == NULL)
	{
		return -1;
	}

	if (!_playing)
	{
		return 0;
	}

	deleteAudioRenderThread();

	_playIsInitialized = false;
	_playing = false;

	MMRESULT res;

	// The waveOutReset function stops playback on the given waveform-audio
	// output device and resets the current position to zero. All pending
	// playback buffers are marked as done (WHDR_DONE) and returned to the application.
	// After this function returns, the application can send new playback buffers
	// to the device by calling waveOutWrite, or close the device by calling waveOutClose.
	//
	res = waveOutReset(_hWaveOut);
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("waveOutReset() failed (err=%d)", res);
		TraceWaveOutError(res);
	}

	// The waveOutUnprepareHeader function cleans up the preparation performed
	// by the waveOutPrepareHeader function. This function must be called after
	// the device driver is finished with a data block.
	// You must call this function before freeing the buffer.
	//
	for (int n = 0; n < N_BUFFERS_OUT; n++)
	{
		res = waveOutUnprepareHeader(_hWaveOut, &_waveHeaderOut[n], sizeof(WAVEHDR));
		if (MMSYSERR_NOERROR != res)
		{
			LOGW("waveOutUnprepareHeader() failed (err=%d)", res);
			TraceWaveOutError(res);
		}
	}

	// The waveOutClose function closes the given waveform-audio output device.
	// The close operation fails if the device is still playing a waveform-audio
	// buffer that was previously sent by calling waveOutWrite. Before calling
	// waveOutClose, the application must wait for all buffers to finish playing
	// or call the waveOutReset function to terminate playback.
	//
	res = waveOutClose(_hWaveOut);
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("waveOutClose() failed (err=%d)", res);
		TraceWaveOutError(res);
	}

	_hWaveOut = NULL;
	LOGI("_hWaveOut is now set to NULL");

	return 0;
}

// ----------------------------------------------------------------------------
//  Playing
// ----------------------------------------------------------------------------

bool WinWaveAudioRender::Playing() const
{
	return (_playing);
}

// ----------------------------------------------------------------------------
//  EnumeratePlayoutDevices
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::EnumeratePlayoutDevices()
{
	uint16_t nDevices(PlayoutDevices());
	LOGI("===============================================================");
	LOGI("#output devices: %u", nDevices);

	WAVEOUTCAPS caps;
	MMRESULT res;

	for (UINT deviceID = 0; deviceID < nDevices; deviceID++)
	{
		res = waveOutGetDevCaps(deviceID, &caps, sizeof(WAVEOUTCAPS));
		if (res != MMSYSERR_NOERROR)
		{
			LOGW("waveOutGetDevCaps() failed (err=%d)", res);
		}

		LOGI("===============================================================");
		LOGI("Device ID %u:", deviceID);
		LOGI("manufacturer ID      : %u", caps.wMid);
		LOGI("product ID           : %u", caps.wPid);
		LOGI("version of driver    : %u.%u", HIBYTE(caps.vDriverVersion), LOBYTE(caps.vDriverVersion));
		LOGI("product name         : %s", caps.szPname);
		LOGI("dwFormats            : 0x%x", caps.dwFormats);
		if (caps.dwFormats & WAVE_FORMAT_4S16 || caps.dwFormats & WAVE_FORMAT_44S16)
		{
			LOGI("  44.1kHz,stereo,16bit : SUPPORTED");
		}
		else
		{
			LOGW(" 44.1kHz,stereo,16bit  : *NOT* SUPPORTED");
		}
		if (caps.dwFormats & WAVE_FORMAT_4M16 || caps.dwFormats & WAVE_FORMAT_44M16)
		{
			LOGI("  44.1kHz,mono,16bit   : SUPPORTED");
		}
		else
		{
			LOGW(" 44.1kHz,mono,16bit    : *NOT* SUPPORTED");
		}
		LOGI("wChannels            : %u", caps.wChannels);
		TraceSupportFlags(caps.dwSupport);
	}

	return 0;
}


// ----------------------------------------------------------------------------
//  TraceSupportFlags
// ----------------------------------------------------------------------------

void WinWaveAudioRender::TraceSupportFlags(DWORD dwSupport) const
{
	TCHAR buf[256];

	StringCchPrintf(buf, 128, TEXT("support flags        : 0x%x "), dwSupport);

	if (dwSupport & WAVECAPS_PITCH)
	{
		// supports pitch control
		StringCchCat(buf, 256, TEXT("(PITCH)"));
	}
	if (dwSupport & WAVECAPS_PLAYBACKRATE)
	{
		// supports playback rate control
		StringCchCat(buf, 256, TEXT("(PLAYBACKRATE)"));
	}
	if (dwSupport & WAVECAPS_VOLUME)
	{
		// supports volume control
		StringCchCat(buf, 256, TEXT("(VOLUME)"));
	}
	if (dwSupport & WAVECAPS_LRVOLUME)
	{
		// supports separate left and right volume control
		StringCchCat(buf, 256, TEXT("(LRVOLUME)"));
	}
	if (dwSupport & WAVECAPS_SYNC)
	{
		// the driver is synchronous and will block while playing a buffer
		StringCchCat(buf, 256, TEXT("(SYNC)"));
	}
	if (dwSupport & WAVECAPS_SAMPLEACCURATE)
	{
		// returns sample-accurate position information
		StringCchCat(buf, 256, TEXT("(SAMPLEACCURATE)"));
	}

	LOGI("%S", buf);
}


// ----------------------------------------------------------------------------
//  TraceWaveOutError
// ----------------------------------------------------------------------------

void WinWaveAudioRender::TraceWaveOutError(MMRESULT error) const
{
	TCHAR buf[MAXERRORLENGTH];
	TCHAR msg[MAXERRORLENGTH];

	StringCchPrintf(buf, MAXERRORLENGTH, TEXT("Error details: "));
	waveOutGetErrorText(error, msg, MAXERRORLENGTH);
	StringCchCat(buf, MAXERRORLENGTH, msg);
	LOGI("%S", buf);
}


// ----------------------------------------------------------------------------
//  PrepareStartPlayout
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::PrepareStartPlayout()
{
	if (_hWaveOut == NULL)
	{
		return -1;
	}

	// A total of 30ms of data is immediately placed in the SC buffer
	//
	int8_t zeroVec[4 * PLAY_BUF_SIZE_IN_SAMPLES];  // max allocation
	memset(zeroVec, 0, 4 * PLAY_BUF_SIZE_IN_SAMPLES);

	{
//		Write(zeroVec, PLAY_BUF_SIZE_IN_SAMPLES);
//		Write(zeroVec, PLAY_BUF_SIZE_IN_SAMPLES);
//		Write(zeroVec, PLAY_BUF_SIZE_IN_SAMPLES);
	}

	return 0;
}

// ----------------------------------------------------------------------------
//  GetPlayoutBufferDelay
// ----------------------------------------------------------------------------

int32_t WinWaveAudioRender::GetPlayoutBufferDelay(uint32_t& writtenSamples, uint32_t& playedSamples)
{
	int msecInPlayoutBuffer(0);   // #milliseconds of audio in the playout buffer

	const uint16_t nSamplesPerMs = (uint16_t)(N_PLAY_SAMPLES_PER_SEC / 1000);  // default is 48000/1000 = 48

	MMRESULT res;
	MMTIME mmtime;

	if (!_playing)
	{
		playedSamples = 0;
		return (0);
	}

	// Retrieve the current playback position.
	//
	mmtime.wType = TIME_SAMPLES;  // number of waveform-audio samples
	res = waveOutGetPosition(_hWaveOut, &mmtime, sizeof(mmtime));
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("waveOutGetPosition() failed (err=%d)", res);
		TraceWaveOutError(res);
	}

	writtenSamples = _writtenSamples;   // #samples written to the playout buffer
	playedSamples = mmtime.u.sample;    // current playout position in the playout buffer

										// derive remaining amount (in ms) of data in the playout buffer
	msecInPlayoutBuffer = ((writtenSamples - playedSamples) / nSamplesPerMs);

	return msecInPlayoutBuffer;
}

int32_t WinWaveAudioRender::Write(int8_t* data, uint16_t nSamples)
{
	if (_hWaveOut == NULL)
	{
		return -1;
	}

	if (_playIsInitialized)
	{
		MMRESULT res;

		const uint16_t bufIndex(_playBufIndex);

		// Place data in the memory associated with _waveHeaderOut[bufCount]
		//
		const int16_t nBytes = (2 * _playChannels)*nSamples;
		memcpy(_playBuffer[bufIndex], data, nBytes);
//		memcpy(&_playBuffer[bufIndex][0], &data[0], nBytes);

		// Send a data block to the given waveform-audio output device.
		//
		// When the buffer is finished, the WHDR_DONE bit is set in the dwFlags
		// member of the WAVEHDR structure. The buffer must be prepared with the
		// waveOutPrepareHeader function before it is passed to waveOutWrite.
		// Unless the device is paused by calling the waveOutPause function,
		// playback begins when the first data block is sent to the device.
		//
		res = waveOutWrite(_hWaveOut, &_waveHeaderOut[bufIndex], sizeof(_waveHeaderOut[bufIndex]));
		if (MMSYSERR_NOERROR != res)
		{
			LOGE("waveOutWrite(%d) failed (err=%d)", bufIndex, res);
			TraceWaveOutError(res);

			return -1;
		}

		_playBufIndex = (_playBufIndex + 1) % N_BUFFERS_OUT;  // increase buffer counter modulo size of total buffer
		_writtenSamples += nSamples;                        // each sample is 2 or 4 bytes
		_playBufCount++;
	}

	return 0;
}

void CALLBACK WinWaveAudioRender::waveOutProc(HWAVEOUT hwo, UINT uMsg, DWORD_PTR dwInstance, DWORD_PTR dwParam1, DWORD_PTR dwParam2)
{
	if (uMsg==WOM_DONE)
	{
		WinWaveAudioRender *thiz = (WinWaveAudioRender *)dwInstance;
		if (thiz)
		{
			thiz->handleWaveOutProc();
		}
	}
}

void WinWaveAudioRender::handleWaveOutProc()
{
	pthread_mutex_lock(&mLock);

	_playBufCount--;
	currentPts += 10*1000;

	pthread_mutex_unlock(&mLock);

	pthread_cond_signal(&mCondition);
}

void WinWaveAudioRender::createAudioRenderThread()
{
	mIsBreakThread = false;

#ifndef WIN32
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&mThread, &attr, handleVideoRenderThread, this);
	pthread_attr_destroy(&attr);
#else
	pthread_create(&mThread, NULL, handleAudioRenderThread, this);
#endif
}

void* WinWaveAudioRender::handleAudioRenderThread(void* ptr)
{
	WinWaveAudioRender *audioRender = (WinWaveAudioRender *)ptr;
	audioRender->audioRenderThreadMain();

	return NULL;
}

void WinWaveAudioRender::audioRenderThreadMain()
{
	while (true)
	{
		pthread_mutex_lock(&mLock);

		if (mIsBreakThread)
		{
			pthread_mutex_unlock(&mLock);

			break;
		}

		if (_playBufCount<N_BUFFERS_OUT && cache_len>=PLAY_BUF_SIZE_IN_SAMPLES * (2 * _playChannels))
		{
			const uint16_t bufIndex(_playBufIndex);
			int8_t *onePlayBuffer = _playBuffer[bufIndex];
			unsigned int onebufferSize = (2 * _playChannels)*PLAY_BUF_SIZE_IN_SAMPLES;

			if (onebufferSize>fifoSize - read_pos)
			{
				memcpy(onePlayBuffer, fifo + read_pos, fifoSize - read_pos);
				memcpy(onePlayBuffer + fifoSize - read_pos, fifo, onebufferSize - (fifoSize - read_pos));
				read_pos = onebufferSize - (fifoSize - read_pos);
			}
			else
			{
				memcpy(onePlayBuffer, fifo + read_pos, onebufferSize);
				read_pos = read_pos + onebufferSize;
			}

			cache_len = cache_len - onebufferSize;
			pthread_mutex_unlock(&mLock);

			MMRESULT res;
			res = waveOutWrite(_hWaveOut, &_waveHeaderOut[bufIndex], sizeof(_waveHeaderOut[bufIndex]));
			if (MMSYSERR_NOERROR != res)
			{
				LOGE("waveOutWrite(%d) failed (err=%d)", bufIndex, res);
				TraceWaveOutError(res);

				continue;
			}

			_playBufIndex = (_playBufIndex + 1) % N_BUFFERS_OUT;  // increase buffer counter modulo size of total buffer
			_writtenSamples += PLAY_BUF_SIZE_IN_SAMPLES;                        // each sample is 2 or 4 bytes

			pthread_mutex_lock(&mLock);
			_playBufCount++;
			pthread_mutex_unlock(&mLock);

			continue;
		}
		else {
			pthread_cond_wait(&mCondition, &mLock);
			pthread_mutex_unlock(&mLock);
			continue;
		}
	}
}

void WinWaveAudioRender::deleteAudioRenderThread()
{
	pthread_mutex_lock(&mLock);
	mIsBreakThread = true;
	pthread_mutex_unlock(&mLock);

	pthread_cond_signal(&mCondition);

	pthread_join(mThread, NULL);

	mIsBreakThread = false;
}