#include "WinAudioMixerManager.h"
#include <assert.h>      // assert()
#include <strsafe.h>    // StringCchCopy(), StringCchCat(), StringCchPrintf()

#include "MediaLog.h"

#ifdef _WIN32
// removes warning: "reinterpret_cast: conversion from 'UINT' to 'HMIXEROBJ'
//                of greater size"
#pragma warning(disable:4312)
#endif

// Avoids the need of Windows 7 SDK
#ifndef WAVE_MAPPED_kDefaultCommunicationDevice
#define  WAVE_MAPPED_kDefaultCommunicationDevice   0x0010
#endif

// ============================================================================
//                             CONSTRUCTION/DESTRUCTION
// ============================================================================

WinAudioMixerManager::WinAudioMixerManager()
{
	LOGD("%s constructed", __FUNCTION__);

	_outputMixerHandle = NULL;

	ClearSpeakerState();
}

WinAudioMixerManager::~WinAudioMixerManager()
{
	LOGD("%s destructed", __FUNCTION__);

	Close();
}

// ============================================================================
//                                 PUBLIC METHODS
// ============================================================================

// ----------------------------------------------------------------------------
//  Close
// ----------------------------------------------------------------------------

int32_t WinAudioMixerManager::Close()
{
	LOGD("%s", __FUNCTION__);

	if (_outputMixerHandle != NULL)
	{
		mixerClose(_outputMixerHandle);
		_outputMixerHandle = NULL;
	}

	return 0;
}

// ----------------------------------------------------------------------------
//  CloseSpeaker
// ----------------------------------------------------------------------------
int32_t WinAudioMixerManager::CloseSpeaker()
{
	LOGD("%s", __FUNCTION__);

	if (_outputMixerHandle == NULL)
	{
		return -1;
	}

	ClearSpeakerState(_outputMixerID);

	mixerClose(_outputMixerHandle);
	_outputMixerHandle = NULL;

	return 0;
}

// ----------------------------------------------------------------------------
//  EnumerateAll
// ----------------------------------------------------------------------------
int32_t WinAudioMixerManager::EnumerateAll()
{
	LOGD("%s", __FUNCTION__);
	UINT nDevices = mixerGetNumDevs();
	LOGD("#mixer devices: %u", nDevices);

	MIXERCAPS    caps;
	MIXERLINE    destLine;
	MIXERLINE    sourceLine;
	MIXERCONTROL controlArray[MAX_NUMBER_OF_LINE_CONTROLS];

	UINT mixId(0);
	UINT destId(0);
	UINT sourceId(0);

	for (mixId = 0; mixId < nDevices; mixId++)
	{
		if (!GetCapabilities(mixId, caps, true))
			continue;

		for (destId = 0; destId < caps.cDestinations; destId++)
		{
			GetDestinationLineInfo(mixId, destId, destLine, true);
			GetAllLineControls(mixId, destLine, controlArray, true);

			for (sourceId = 0; sourceId < destLine.cConnections; sourceId++)
			{
				GetSourceLineInfo(mixId, destId, sourceId, sourceLine, true);
				GetAllLineControls(mixId, sourceLine, controlArray, true);
			}
		}
	}

	return 0;
}

// ----------------------------------------------------------------------------
//  EnumerateSpeakers
// ----------------------------------------------------------------------------

int32_t WinAudioMixerManager::EnumerateSpeakers()
{
	LOGI("%s", __FUNCTION__);

	UINT nDevices = mixerGetNumDevs();
	if (nDevices > MAX_NUMBER_MIXER_DEVICES)
	{
		assert(false);
		return -1;
	}
	LOGI("#mixer devices: %u", nDevices);

	MIXERCAPS    caps;
	MIXERLINE    destLine;
	MIXERCONTROL controlArray[MAX_NUMBER_OF_LINE_CONTROLS];

	UINT mixId(0);
	UINT destId(0);

	ClearSpeakerState();

	// scan all avaliable mixer devices
	for (mixId = 0; mixId < nDevices; mixId++)
	{
		// get capabilities for the specified mixer ID
		if (!GetCapabilities(mixId, caps))
			continue;

		LOGI("[mixerID=%d] %s: ", mixId, WideToUTF8(caps.szPname));
		// scan all avaliable destinations for this mixer
		for (destId = 0; destId < caps.cDestinations; destId++)
		{
			GetDestinationLineInfo(mixId, destId, destLine);
			if ((destLine.cControls == 0) ||    // no controls or
				(destLine.cConnections == 0) ||    // no source lines or
				(destLine.fdwLine & MIXERLINE_LINEF_DISCONNECTED) ||    // disconnected or
				!(destLine.fdwLine & MIXERLINE_LINEF_ACTIVE))           // inactive
			{
				// don't store this line ID since it will not be possible to control
				continue;
			}
			if ((destLine.dwComponentType == MIXERLINE_COMPONENTTYPE_DST_SPEAKERS) ||
				(destLine.dwComponentType == MIXERLINE_COMPONENTTYPE_DST_HEADPHONES))
			{
				LOGI("found valid speaker/headphone (name: %s, ID: %u)", WideToUTF8(destLine.szName), destLine.dwLineID);
				_speakerState[mixId].dwLineID = destLine.dwLineID;
				_speakerState[mixId].speakerIsValid = true;
				// retrieve all controls for the speaker component
				GetAllLineControls(mixId, destLine, controlArray);
				for (UINT c = 0; c < destLine.cControls; c++)
				{
					if (controlArray[c].dwControlType == MIXERCONTROL_CONTROLTYPE_VOLUME)
					{
						_speakerState[mixId].dwVolumeControlID = controlArray[c].dwControlID;
						_speakerState[mixId].volumeControlIsValid = true;
						LOGI("found volume control (name: %s, ID: %u)", WideToUTF8(controlArray[c].szName), controlArray[c].dwControlID);
					}
					else if (controlArray[c].dwControlType == MIXERCONTROL_CONTROLTYPE_MUTE)
					{
						_speakerState[mixId].dwMuteControlID = controlArray[c].dwControlID;
						_speakerState[mixId].muteControlIsValid = true;
						LOGI("found mute control (name: %s, ID: %u)", WideToUTF8(controlArray[c].szName), controlArray[c].dwControlID);
					}
				}
				break;
			}
		}
		if (!SpeakerIsValid(mixId))
		{
			LOGI("unable to find a valid speaker destination line", mixId);
		}
	}

	if (ValidSpeakers() == 0)
	{
		LOGW("failed to locate any valid speaker line");
		return -1;
	}

	return 0;
}


// ----------------------------------------------------------------------------
//  OpenSpeaker I(II)
//
//  Verifies that the mixer contains a valid speaker destination line.
//  Avoids opening the mixer if valid control has not been found.
// ----------------------------------------------------------------------------

int32_t WinAudioMixerManager::OpenSpeaker(WindowsDeviceType device)
{
	if (device == kDefaultDevice)
	{
		LOGI("WinAudioMixerManager::OpenSpeaker(kDefaultDevice)");
	}
	else if (device == kDefaultCommunicationDevice)
	{
		LOGI("OpenSpeaker(kDefaultCommunicationDevice)");
	}

	// Close any existing output mixer handle
	//
	if (_outputMixerHandle != NULL)
	{
		mixerClose(_outputMixerHandle);
		_outputMixerHandle = NULL;
	}

	MMRESULT     res = MMSYSERR_NOERROR;
	WAVEFORMATEX waveFormat;
	HWAVEOUT     hWaveOut(NULL);

	waveFormat.wFormatTag = WAVE_FORMAT_PCM;
	waveFormat.nChannels = 2;
	waveFormat.nSamplesPerSec = 44100;
	waveFormat.wBitsPerSample = 16;
	waveFormat.nBlockAlign = waveFormat.nChannels * waveFormat.wBitsPerSample / 8;
	waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
	waveFormat.cbSize = 0;

	// We need a waveform-audio output handle for the currently selected output device.
	// This handle will then give us the corresponding mixer identifier. Once the mixer
	// ID is known, it is possible to open the output mixer.
	//
	if (device == kDefaultCommunicationDevice)
	{
		// check if it is possible to open the default communication device (supported on Windows 7)
		res = waveOutOpen(NULL, WAVE_MAPPER, &waveFormat, 0, 0, CALLBACK_NULL |
			WAVE_MAPPED_kDefaultCommunicationDevice | WAVE_FORMAT_QUERY);
		if (MMSYSERR_NOERROR == res)
		{
			// if so, open the default communication device for real
			res = waveOutOpen(&hWaveOut, WAVE_MAPPER, &waveFormat, 0, 0, CALLBACK_NULL | WAVE_MAPPED_kDefaultCommunicationDevice);
			LOGI("opening default communication device");
		}
		else
		{
			// use default device since default communication device was not avaliable
			res = waveOutOpen(&hWaveOut, WAVE_MAPPER, &waveFormat, 0, 0, CALLBACK_NULL);
			LOGI("unable to open default communication device => using default instead");
		}
	}
	else if (device == kDefaultDevice)
	{
		// open default device since it has been requested
		res = waveOutOpen(&hWaveOut, WAVE_MAPPER, &waveFormat, 0, 0, CALLBACK_NULL);
		LOGI("opening default output device");
	}

	if (MMSYSERR_NOERROR != res)
	{
		LOGW("waveOutOpen() failed (err=%d)", res);
		TraceWaveOutError(res);
	}

	UINT   mixerId(0);
	HMIXER hMixer(NULL);

	// Retrieve the device identifier for a mixer device associated with the
	// aquired waveform-audio output handle.
	//
	res = mixerGetID((HMIXEROBJ)hWaveOut, &mixerId, MIXER_OBJECTF_HWAVEOUT);
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("mixerGetID(MIXER_OBJECTF_HWAVEOUT) failed (err=%d)", res);
		// identification failed => use default mixer identifier (=0)
		mixerId = 0;
	}
	LOGI("specified output device <=> mixer ID %u", mixerId);

	// The waveform-audio output handle is no longer needed.
	//
	waveOutClose(hWaveOut);

	// Verify that the mixer contains a valid speaker destination line.
	// Avoid opening the mixer if valid control has not been found.
	//
	if (!SpeakerIsValid(mixerId))
	{
		LOGW("it is not possible to control the speaker volume for this mixer device");
		return -1;
	}

	// Open the specified mixer device and ensure that the device will not
	// be removed until the application closes the handle.
	//
	res = mixerOpen(&hMixer, mixerId, 0, 0, MIXER_OBJECTF_MIXER);
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("mixerOpen() failed (err=%d)", res);
	}

	// Store the output mixer handle and active mixer identifier
	//
	_outputMixerHandle = hMixer;
	_outputMixerID = mixerId;

	if (_outputMixerHandle != NULL)
	{
		LOGI("the output mixer device is now open (0x%x)", _outputMixerHandle);
	}

	return 0;
}

// ----------------------------------------------------------------------------
//  OpenSpeaker II(II)
//
//  Verifies that the mixer contains a valid speaker destination line.
//  Avoids opening the mixer if valid control has not been found.
// ----------------------------------------------------------------------------

int32_t WinAudioMixerManager::OpenSpeaker(uint16_t index)
{
	LOGI("WinAudioMixerManager::OpenSpeaker(index=%d)", index);

	// Close any existing output mixer handle
	//
	if (_outputMixerHandle != NULL)
	{
		mixerClose(_outputMixerHandle);
		_outputMixerHandle = NULL;
	}

	MMRESULT     res;
	WAVEFORMATEX waveFormat;
	HWAVEOUT     hWaveOut(NULL);

	const UINT   deviceID(index);  // use index parameter as device identifier

	waveFormat.wFormatTag = WAVE_FORMAT_PCM;
	waveFormat.nChannels = 2;
	waveFormat.nSamplesPerSec = 44100;
	waveFormat.wBitsPerSample = 16;
	waveFormat.nBlockAlign = waveFormat.nChannels * waveFormat.wBitsPerSample / 8;
	waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
	waveFormat.cbSize = 0;

	// We need a waveform-audio output handle for the currently selected output device.
	// This handle will then give us the corresponding mixer identifier. Once the mixer
	// ID is known, it is possible to open the output mixer.
	//
	res = waveOutOpen(&hWaveOut, deviceID, &waveFormat, 0, 0, CALLBACK_NULL);
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("waveOutOpen(deviceID=%u) failed (err=%d)", index, res);
		TraceWaveOutError(res);
	}

	UINT   mixerId(0);
	HMIXER hMixer(NULL);

	// Retrieve the device identifier for a mixer device associated with the
	// aquired waveform-audio output handle.
	//
	res = mixerGetID((HMIXEROBJ)hWaveOut, &mixerId, MIXER_OBJECTF_HWAVEOUT);
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("mixerGetID(MIXER_OBJECTF_HWAVEOUT) failed (err=%d)", res);
		// identification failed => use default mixer identifier (=0)
		mixerId = 0;
	}
	LOGI("specified output device <=> mixer ID %u", mixerId);

	// The waveform-audio output handle is no longer needed.
	//
	waveOutClose(hWaveOut);

	// Verify that the mixer contains a valid speaker destination line.
	// Avoid opening the mixer if valid control has not been found.
	//
	if (!SpeakerIsValid(mixerId))
	{
		LOGW("it is not possible to control the speaker volume for this mixer device");
		return -1;
	}

	// Open the specified mixer device and ensure that the device will not
	// be removed until the application closes the handle.
	//
	res = mixerOpen(&hMixer, mixerId, 0, 0, MIXER_OBJECTF_MIXER);
	if (MMSYSERR_NOERROR != res)
	{
		LOGW("mixerOpen() failed (err=%d)", res);
	}

	// Store the output mixer handle and active mixer identifier
	//
	_outputMixerHandle = hMixer;
	_outputMixerID = mixerId;

	if (_outputMixerHandle != NULL)
	{
		LOGI("the output mixer device is now open (0x%x)", _outputMixerHandle);
	}

	return 0;
}


// ----------------------------------------------------------------------------
// SpeakerIsInitialized
// ----------------------------------------------------------------------------

bool WinAudioMixerManager::SpeakerIsInitialized() const
{
	LOGD("%s", __FUNCTION__);

	return (_outputMixerHandle != NULL);
}


// ----------------------------------------------------------------------------
// SetSpeakerVolume
// ----------------------------------------------------------------------------

int32_t WinAudioMixerManager::SetSpeakerVolume(uint32_t volume)
{
	LOGI("WinAudioMixerManager::SetSpeakerVolume(volume=%u)", volume);

	if (_outputMixerHandle == NULL)
	{
		LOGW("no avaliable output mixer exists");
		return -1;
	}

	const UINT mixerID(_outputMixerID);
	const DWORD dwControlID(_speakerState[_outputMixerID].dwVolumeControlID);
	DWORD dwValue(volume);

	// Set one unsigned control value for a specified volume-control identifier
	//
	if (!SetUnsignedControlValue(mixerID, dwControlID, dwValue))
	{
		return -1;
	}

	return (0);
}


// ----------------------------------------------------------------------------
//  SpeakerVolume
//
//  Note that (MIXERCONTROL_CONTROLTYPE_VOLUME & MIXERCONTROL_CT_UNITS_MASK)
//  always equals MIXERCONTROL_CT_UNITS_UNSIGNED;
// ----------------------------------------------------------------------------

int32_t WinAudioMixerManager::SpeakerVolume(uint32_t& volume) const
{

	if (_outputMixerHandle == NULL)
	{
		LOGW("no avaliable output mixer exists");
		return -1;
	}

	const UINT mixerID(_outputMixerID);
	const DWORD dwControlID(_speakerState[_outputMixerID].dwVolumeControlID);
	DWORD dwValue(0);

	// Retrieve one unsigned control value for a specified volume-control identifier
	//
	if (!GetUnsignedControlValue(mixerID, dwControlID, dwValue))
	{
		return -1;
	}

	volume = dwValue;

	return 0;
}

// ----------------------------------------------------------------------------
//  MaxSpeakerVolume
//
//  Note that (MIXERCONTROL_CONTROLTYPE_VOLUME & MIXERCONTROL_CT_UNITS_MASK)
//  always equals MIXERCONTROL_CT_UNITS_UNSIGNED
// ----------------------------------------------------------------------------

int32_t WinAudioMixerManager::MaxSpeakerVolume(uint32_t& maxVolume) const
{

	if (_outputMixerHandle == NULL)
	{
		LOGW("no avaliable output mixer exists");
		return -1;
	}

	const UINT mixerID(_outputMixerID);
	const DWORD dwControlID(_speakerState[_outputMixerID].dwVolumeControlID);
	MIXERCONTROL mixerControl;

	// Retrieve one control line for a specified volume-control identifier
	//
	if (!GetLineControl(mixerID, dwControlID, mixerControl))
	{
		return -1;
	}

	maxVolume = mixerControl.Bounds.dwMaximum;

	return 0;
}

// ----------------------------------------------------------------------------
// MinSpeakerVolume
// ----------------------------------------------------------------------------

int32_t WinAudioMixerManager::MinSpeakerVolume(uint32_t& minVolume) const
{
	if (_outputMixerHandle == NULL)
	{
		LOGW("no avaliable output mixer exists");
		return -1;
	}

	const UINT mixerID(_outputMixerID);
	const DWORD dwControlID(_speakerState[_outputMixerID].dwVolumeControlID);
	MIXERCONTROL mixerControl;

	// Retrieve one control line for a specified volume-control identifier
	//
	if (!GetLineControl(mixerID, dwControlID, mixerControl))
	{
		return -1;
	}

	minVolume = mixerControl.Bounds.dwMinimum;

	return 0;
}

// ----------------------------------------------------------------------------
// SpeakerVolumeStepSize
// ----------------------------------------------------------------------------

int32_t WinAudioMixerManager::SpeakerVolumeStepSize(uint16_t& stepSize) const
{
	if (_outputMixerHandle == NULL)
	{
		LOGW("no avaliable output mixer exists");
		return -1;
	}

	const UINT mixerID(_outputMixerID);
	MIXERCONTROL mixerControl;

	// Retrieve one control line for a specified volume-control identifier
	//
	if (!GetLineControl(mixerID, _speakerState[mixerID].dwVolumeControlID, mixerControl))
	{
		return -1;
	}

	stepSize = static_cast<uint16_t> (mixerControl.Metrics.cSteps);

	return 0;
}

// ----------------------------------------------------------------------------
// SpeakerVolumeIsAvailable
// ----------------------------------------------------------------------------

int32_t WinAudioMixerManager::SpeakerVolumeIsAvailable(bool& available)
{
	if (_outputMixerHandle == NULL)
	{
		LOGW("no avaliable output mixer exists");
		return -1;
	}

	available = _speakerState[_outputMixerID].volumeControlIsValid;

	return 0;
}

// ----------------------------------------------------------------------------
// SpeakerMuteIsAvailable
// ----------------------------------------------------------------------------

int32_t WinAudioMixerManager::SpeakerMuteIsAvailable(bool& available)
{
	if (_outputMixerHandle == NULL)
	{
		LOGW("no avaliable output mixer exists");
		return -1;
	}

	available = _speakerState[_outputMixerID].muteControlIsValid;

	return 0;
}

// ----------------------------------------------------------------------------
//  SetSpeakerMute
//
//  This mute function works a master mute for the output speaker.
// ----------------------------------------------------------------------------

int32_t WinAudioMixerManager::SetSpeakerMute(bool enable)
{
	LOGI("AudioMixerManager::SetSpeakerMute(enable=%u)", enable);

	if (_outputMixerHandle == NULL)
	{
		LOGW("no avaliable output mixer exists");
		return -1;
	}

	// Ensure that the selected speaker destination has a valid mute control.
	// If so, its identifier was stored during the enumeration phase which must
	// have taken place since the output mixer handle exists.
	//
	if (!_speakerState[_outputMixerID].muteControlIsValid)
	{
		LOGW("it is not possible to mute this speaker line");
		return -1;
	}

	const DWORD dwControlID(_speakerState[_outputMixerID].dwMuteControlID);

	// Set one boolean control value for the specified mute-control
	//
	if (!SetBooleanControlValue(_outputMixerID, dwControlID, enable))
	{
		return -1;
	}

	return (0);
}

// ----------------------------------------------------------------------------
//  SpeakerMute
// ----------------------------------------------------------------------------

int32_t WinAudioMixerManager::SpeakerMute(bool& enabled) const
{
	if (_outputMixerHandle == NULL)
	{
		LOGW("no avaliable output mixer exists");
		return -1;
	}

	// Ensure that the selected speaker destination has a valid mute control.
	// If so, its identifier was stored during the enumeration phase which must
	// have taken place since the output mixer handle exists.
	//
	if (!_speakerState[_outputMixerID].muteControlIsValid)
	{
		LOGW("it is not possible to mute this speaker line");
		return -1;
	}

	const DWORD dwControlID(_speakerState[_outputMixerID].dwMuteControlID);
	bool value(false);

	// Retrieve one boolean control value for a specified mute-control identifier
	//
	if (!GetBooleanControlValue(_outputMixerID, dwControlID, value))
	{
		return -1;
	}

	enabled = value;

	return 0;
}


// ============================================================================
//                              PRIVATE METHODS
// ============================================================================

// ----------------------------------------------------------------------------
//  Devices
//
//  A given audio card has one Mixer device associated with it. All of the
//  various components on that card are controlled through that card's one
//  Mixer device.
// ----------------------------------------------------------------------------

UINT WinAudioMixerManager::Devices() const
{
	UINT nDevs = mixerGetNumDevs();
	return nDevs;
}

// ----------------------------------------------------------------------------
//  DestinationLines
//
//  # destination lines given mixer ID.
// ----------------------------------------------------------------------------

UINT WinAudioMixerManager::DestinationLines(UINT mixId) const
{
	MIXERCAPS caps;
	if (!GetCapabilities(mixId, caps))
	{
		return 0;
	}
	return (caps.cDestinations);
}

// ----------------------------------------------------------------------------
//  DestinationLines
//
//  # source lines given mixer ID and destination ID.
// ----------------------------------------------------------------------------

UINT WinAudioMixerManager::SourceLines(UINT mixId, DWORD destId) const
{
	MIXERLINE dline;
	if (!GetDestinationLineInfo(mixId, destId, dline))
	{
		return 0;
	}
	return (dline.cConnections);
}


// ----------------------------------------------------------------------------
//  GetCapabilities
//
//  Queries a specified mixer device to determine its capabilities.
// ----------------------------------------------------------------------------

bool WinAudioMixerManager::GetCapabilities(UINT mixId, MIXERCAPS& caps, bool trace) const
{
	MMRESULT res;
	MIXERCAPS mcaps;

	res = mixerGetDevCaps(mixId, &mcaps, sizeof(MIXERCAPS));
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("mixerGetDevCaps() failed (err=%d)", res);
		return false;
	}

	memcpy(&caps, &mcaps, sizeof(MIXERCAPS));

	if (trace)
	{

		LOGI("===============================================================");
		LOGI("Mixer ID %u:", mixId);
		LOGI("manufacturer ID      : %u", caps.wMid);
		LOGI("product ID           : %u", caps.wPid);
		LOGI("version of driver    : %u", caps.vDriverVersion);
		LOGI("product name         : %s", WideToUTF8(caps.szPname));
		LOGI("misc. support bits   : %u", caps.fdwSupport);
		LOGI("count of destinations: %u (+)", caps.cDestinations);
		LOGI("===============================================================");
	}

	if (caps.cDestinations == 0)
	{
		LOGE("invalid number of mixer destinations");
		return false;
	}

	return true;
}

// ----------------------------------------------------------------------------
//  GetDestinationLineInfo
// ----------------------------------------------------------------------------

bool WinAudioMixerManager::GetDestinationLineInfo(UINT mixId, DWORD destId, MIXERLINE& line, bool trace) const
{
	MMRESULT  res;
	MIXERLINE mline;

	mline.cbStruct = sizeof(MIXERLINE);
	mline.dwDestination = destId;   // max destination index is cDestinations-1
	mline.dwSource = 0;             // not set for MIXER_GETLINEINFOF_DESTINATION

									// Retrieve information about the specified destination line of a mixer device.
									// Note that we use the mixer ID here and not a handle to an opened mixer.
									// It is not required to open the mixer for enumeration purposes only.
									//
	res = mixerGetLineInfo(reinterpret_cast<HMIXEROBJ>(mixId), &mline, MIXER_OBJECTF_MIXER | MIXER_GETLINEINFOF_DESTINATION);
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("mixerGetLineInfo(MIXER_GETLINEINFOF_DESTINATION) failed (err=%d)", res);
		return false;
	}

	memcpy(&line, &mline, sizeof(MIXERLINE));

	if (trace)
	{
		LOGI("> Destination Line ID %u:", destId);
		LOGI("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
		LOGI("destination line index : %u", mline.dwDestination);
		LOGI("dwLineID               : %lu (unique)", mline.dwLineID);
		TraceStatusAndSupportFlags(mline.fdwLine);
		TraceComponentType(mline.dwComponentType);
		LOGI("count of channels      : %u", mline.cChannels);
		LOGI("# audio source lines   : %u (+)", mline.cConnections);    // valid only for destinations
		LOGI("# controls             : %u (*)", mline.cControls);       // can be zero
		LOGI("short name             : %s", WideToUTF8(mline.szShortName));
		LOGI("full name              : %s", WideToUTF8(mline.szName));
		LOGI("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
		TraceTargetType(mline.Target.dwType);
		LOGI("target device ID       : %lu", mline.Target.dwDeviceID);
		LOGI("manufacturer ID        : %u", mline.Target.wMid);
		LOGI("product ID             : %u", mline.Target.wPid);
		LOGI("driver version         : %u", mline.Target.vDriverVersion);
		LOGI("product name           : %s", WideToUTF8(mline.Target.szPname));
		LOGI("---------------------------------------------------------------");
	}

	return true;
}


// ----------------------------------------------------------------------------
//  GetSourceLineInfo
// ----------------------------------------------------------------------------

bool WinAudioMixerManager::GetSourceLineInfo(UINT mixId, DWORD destId, DWORD srcId, MIXERLINE& line, bool trace) const
{
	MMRESULT  res;
	MIXERLINE mline;

	mline.cbStruct = sizeof(MIXERLINE);
	mline.dwDestination = destId;   // we want the source info for this destination
	mline.dwSource = srcId;         // source index (enumerate over these)

									// Retrieve information about the specified source line of a mixer device.
									// Note that we use the mixer ID here and not a handle to an opened mixer.
									// It is not required to open the mixer for enumeration purposes only.
									//
	res = mixerGetLineInfo(reinterpret_cast<HMIXEROBJ>(mixId), &mline, MIXER_OBJECTF_MIXER | MIXER_GETLINEINFOF_SOURCE);
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("mixerGetLineInfo(MIXER_GETLINEINFOF_SOURCE) failed (err=%d)", res);
		return false;
	}

	memcpy(&line, &mline, sizeof(MIXERLINE));

	if (trace)
	{
		LOGI(" >> Source Line ID %u:", srcId);
		LOGI("destination line index : %u", mline.dwDestination);
		LOGI("dwSource               : %u", mline.dwSource);
		LOGI("dwLineID               : %lu (unique)", mline.dwLineID);
		TraceStatusAndSupportFlags(mline.fdwLine);
		TraceComponentType(mline.dwComponentType);
		LOGI("# controls             : %u (*)", mline.cControls);
		LOGI("full name              : %s", WideToUTF8(mline.szName));
		LOGI("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
		TraceTargetType(mline.Target.dwType);
		LOGI("---------------------------------------------------------------");
	}

	return true;
}

// ----------------------------------------------------------------------------
// GetAllLineControls
// ----------------------------------------------------------------------------

bool WinAudioMixerManager::GetAllLineControls(UINT mixId, const MIXERLINE& line, MIXERCONTROL* controlArray, bool trace) const
{
	// Ensure that we don't try to aquire information if there are no controls for this line
	//
	if (line.cControls == 0)
		return false;

	MMRESULT          res;
	MIXERLINECONTROLS mlineControls;            // contains information about the controls of an audio line

	mlineControls.dwLineID = line.dwLineID;    // unique audio line identifier
	mlineControls.cControls = line.cControls;   // number of controls associated with the line
	mlineControls.pamxctrl = controlArray;     // points to the first MIXERCONTROL structure to be filled
	mlineControls.cbStruct = sizeof(MIXERLINECONTROLS);
	mlineControls.cbmxctrl = sizeof(MIXERCONTROL);

	// Get information on ALL controls associated with the specified audio line
	//
	res = mixerGetLineControls(reinterpret_cast<HMIXEROBJ>(mixId), &mlineControls, MIXER_OBJECTF_MIXER | MIXER_GETLINECONTROLSF_ALL);
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("mixerGetLineControls(MIXER_GETLINECONTROLSF_ALL) failed  (err=%d)", res);
		return false;
	}

	if (trace)
	{
		for (UINT c = 0; c < line.cControls; c++)
		{
			LOGI(" >> Control ID %u:", c);
			LOGI("dwControlID            : %u (unique)", controlArray[c].dwControlID);
			TraceControlType(controlArray[c].dwControlType);
			TraceControlStatusAndSupportFlags(controlArray[c].fdwControl);
			LOGI("cMultipleItems         : %u", controlArray[c].cMultipleItems);
			LOGI("short name             : %s", WideToUTF8(controlArray[c].szShortName));
			LOGI("full name              : %s", WideToUTF8(controlArray[c].szName));
			if ((controlArray[c].dwControlType & MIXERCONTROL_CT_UNITS_MASK) == MIXERCONTROL_CT_UNITS_SIGNED)
			{
				LOGI("min signed value       : %d", controlArray[c].Bounds.lMinimum);
				LOGI("max signed value       : %d", controlArray[c].Bounds.lMaximum);
			}
			else if ((controlArray[c].dwControlType & MIXERCONTROL_CT_UNITS_MASK) == MIXERCONTROL_CT_UNITS_UNSIGNED ||
				(controlArray[c].dwControlType & MIXERCONTROL_CT_UNITS_MASK) == MIXERCONTROL_CT_UNITS_BOOLEAN)
			{
				LOGI("min unsigned value     : %u", controlArray[c].Bounds.dwMinimum);
				LOGI("max unsigned value     : %u", controlArray[c].Bounds.dwMaximum);
			}
			if (controlArray[c].dwControlType != MIXERCONTROL_CONTROLTYPE_CUSTOM)
			{
				LOGI("cSteps                 : %u", controlArray[c].Metrics.cSteps);
			}
			LOGI("...............................................................");
			GetControlDetails(mixId, controlArray[c], true);
			LOGI("...............................................................");

		}
	}

	return true;
}

// ----------------------------------------------------------------------------
//  GetLineControls
// ----------------------------------------------------------------------------

bool WinAudioMixerManager::GetLineControl(UINT mixId, DWORD dwControlID, MIXERCONTROL& control) const
{
	MMRESULT          res;
	MIXERLINECONTROLS mlineControl;

	mlineControl.dwControlID = dwControlID;
	mlineControl.cControls = 1;
	mlineControl.pamxctrl = &control;
	mlineControl.cbStruct = sizeof(MIXERLINECONTROLS);
	mlineControl.cbmxctrl = sizeof(MIXERCONTROL);

	// Get information on one controls associated with the specified conrol identifier
	//
	res = mixerGetLineControls(reinterpret_cast<HMIXEROBJ>(mixId), &mlineControl, MIXER_OBJECTF_MIXER | MIXER_GETLINECONTROLSF_ONEBYID);
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("mixerGetLineControls(MIXER_GETLINECONTROLSF_ONEBYID) failed (err=%d)", res);
		return false;
	}

	return true;
}

// ----------------------------------------------------------------------------
//  GetControlDetails
// ----------------------------------------------------------------------------

bool WinAudioMixerManager::GetControlDetails(UINT mixId, MIXERCONTROL& controlArray, bool trace) const
{
	assert(controlArray.cMultipleItems <= MAX_NUMBER_OF_MULTIPLE_ITEMS);

	MMRESULT                     res;
	MIXERCONTROLDETAILS          controlDetails;

	MIXERCONTROLDETAILS_UNSIGNED valueUnsigned[MAX_NUMBER_OF_MULTIPLE_ITEMS];
	MIXERCONTROLDETAILS_SIGNED   valueSigned[MAX_NUMBER_OF_MULTIPLE_ITEMS];
	MIXERCONTROLDETAILS_BOOLEAN  valueBoolean[MAX_NUMBER_OF_MULTIPLE_ITEMS];

	enum ControlType
	{
		CT_UNITS_UNSIGNED,
		CT_UNITS_SIGNED,
		CT_UNITS_BOOLEAN
	};

	ControlType ctype(CT_UNITS_UNSIGNED);

	controlDetails.cbStruct = sizeof(MIXERCONTROLDETAILS);
	controlDetails.dwControlID = controlArray.dwControlID;       // control identifier
	controlDetails.cChannels = 1;                              // we need to set values as if they were uniform
	controlDetails.cMultipleItems = controlArray.cMultipleItems;    // only nonzero for CONTROLF_MULTIPLE controls
																	// can e.g. happen for CONTROLTYPE_MUX
	if (controlDetails.cMultipleItems > MAX_NUMBER_OF_MULTIPLE_ITEMS)
	{
		LOGW("cMultipleItems > %d", MAX_NUMBER_OF_MULTIPLE_ITEMS);
		controlDetails.cMultipleItems = MAX_NUMBER_OF_MULTIPLE_ITEMS;
	}

	if ((controlArray.dwControlType & MIXERCONTROL_CT_UNITS_MASK) == MIXERCONTROL_CT_UNITS_SIGNED)
	{
		ctype = CT_UNITS_SIGNED;
		controlDetails.cbDetails = sizeof(MIXERCONTROLDETAILS_SIGNED);
		controlDetails.paDetails = &valueSigned[0];
	}
	else if ((controlArray.dwControlType & MIXERCONTROL_CT_UNITS_MASK) == MIXERCONTROL_CT_UNITS_UNSIGNED)
	{
		ctype = CT_UNITS_UNSIGNED;
		controlDetails.cbDetails = sizeof(MIXERCONTROLDETAILS_UNSIGNED);
		controlDetails.paDetails = &valueUnsigned[0];
	}
	else if ((controlArray.dwControlType & MIXERCONTROL_CT_UNITS_MASK) == MIXERCONTROL_CT_UNITS_BOOLEAN)
	{
		ctype = CT_UNITS_BOOLEAN;
		controlDetails.cbDetails = sizeof(MIXERCONTROLDETAILS_BOOLEAN);
		controlDetails.paDetails = &valueBoolean[0];
	}

	// Retrieve a control's value
	//
	res = mixerGetControlDetails(reinterpret_cast<HMIXEROBJ>(mixId), &controlDetails, MIXER_OBJECTF_MIXER | MIXER_GETCONTROLDETAILSF_VALUE);
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("mixerGetControlDetails(MIXER_GETCONTROLDETAILSF_VALUE) failed (err=%d)", res);
		return false;
	}

	if (trace)
	{
		UINT nItems(1);
		nItems = (controlDetails.cMultipleItems > 0 ? controlDetails.cMultipleItems : 1);
		for (UINT i = 0; i < nItems; i++)
		{
			if (ctype == CT_UNITS_SIGNED)
			{
				LOGI("signed value           : %d", valueSigned[i].lValue);
			}
			else if (ctype == CT_UNITS_UNSIGNED)
			{
				LOGI("unsigned value         : %u", valueUnsigned[i].dwValue);
			}
			else if (ctype == CT_UNITS_BOOLEAN)
			{
				LOGI("boolean value          : %u", valueBoolean[i].fValue);
			}
		}
	}

	return true;
}


// ----------------------------------------------------------------------------
//  GetUnsignedControlValue
// ----------------------------------------------------------------------------

bool WinAudioMixerManager::GetUnsignedControlValue(UINT mixId, DWORD dwControlID, DWORD& dwValue) const
{
	MMRESULT                     res;
	MIXERCONTROLDETAILS          controlDetails;
	MIXERCONTROLDETAILS_UNSIGNED valueUnsigned;

	controlDetails.dwControlID = dwControlID;
	controlDetails.cbStruct = sizeof(MIXERCONTROLDETAILS);
	controlDetails.cChannels = 1;
	controlDetails.cMultipleItems = 0;
	controlDetails.cbDetails = sizeof(MIXERCONTROLDETAILS_UNSIGNED);
	controlDetails.paDetails = &valueUnsigned;

	// Retrieve the unsigned value
	//
	res = mixerGetControlDetails(reinterpret_cast<HMIXEROBJ>(mixId), &controlDetails, MIXER_OBJECTF_MIXER | MIXER_GETCONTROLDETAILSF_VALUE);
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("mixerGetControlDetails(MIXER_GETCONTROLDETAILSF_VALUE) failed (err=%d)", res);
		return false;
	}

	// Deliver the retrieved value
	//
	dwValue = valueUnsigned.dwValue;

	return true;
}

// ----------------------------------------------------------------------------
//  SetUnsignedControlValue
// ----------------------------------------------------------------------------

bool WinAudioMixerManager::SetUnsignedControlValue(UINT mixId, DWORD dwControlID, DWORD dwValue) const
{
	LOGI("AudioMixerManager::SetUnsignedControlValue(mixId=%u, dwControlID=%d, dwValue=%d)", mixId, dwControlID, dwValue);

	MMRESULT                     res;
	MIXERCONTROLDETAILS          controlDetails;
	MIXERCONTROLDETAILS_UNSIGNED valueUnsigned;

	controlDetails.dwControlID = dwControlID;
	controlDetails.cbStruct = sizeof(MIXERCONTROLDETAILS);
	controlDetails.cChannels = 1;
	controlDetails.cMultipleItems = 0;
	controlDetails.cbDetails = sizeof(MIXERCONTROLDETAILS_UNSIGNED);
	controlDetails.paDetails = &valueUnsigned;

	valueUnsigned.dwValue = dwValue;

	// Set the unsigned value
	//
	res = mixerSetControlDetails(reinterpret_cast<HMIXEROBJ>(mixId), &controlDetails, MIXER_OBJECTF_MIXER | MIXER_GETCONTROLDETAILSF_VALUE);
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("mixerSetControlDetails(MIXER_GETCONTROLDETAILSF_VALUE) failed (err=%d)", res);
		return false;
	}

	return true;
}

// ----------------------------------------------------------------------------
//  SetBooleanControlValue
// ----------------------------------------------------------------------------

bool WinAudioMixerManager::SetBooleanControlValue(UINT mixId, DWORD dwControlID, bool value) const
{
	LOGI("AudioMixerManager::SetBooleanControlValue(mixId=%u, dwControlID=%d, value=%d)", mixId, dwControlID, value);

	MMRESULT                    res;
	MIXERCONTROLDETAILS         controlDetails;
	MIXERCONTROLDETAILS_BOOLEAN valueBoolean;

	controlDetails.dwControlID = dwControlID;
	controlDetails.cbStruct = sizeof(MIXERCONTROLDETAILS);
	controlDetails.cChannels = 1;
	controlDetails.cMultipleItems = 0;
	controlDetails.cbDetails = sizeof(MIXERCONTROLDETAILS_BOOLEAN);
	controlDetails.paDetails = &valueBoolean;

	if (value == true)
		valueBoolean.fValue = TRUE;
	else
		valueBoolean.fValue = FALSE;

	// Set the boolean value
	//
	res = mixerSetControlDetails(reinterpret_cast<HMIXEROBJ>(mixId), &controlDetails, MIXER_OBJECTF_MIXER | MIXER_GETCONTROLDETAILSF_VALUE);
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("mixerSetControlDetails(MIXER_GETCONTROLDETAILSF_VALUE) failed (err=%d)", res);
		return false;
	}

	return true;
}

// ----------------------------------------------------------------------------
//  GetBooleanControlValue
// ----------------------------------------------------------------------------

bool WinAudioMixerManager::GetBooleanControlValue(UINT mixId, DWORD dwControlID, bool& value) const
{
	MMRESULT                    res;
	MIXERCONTROLDETAILS         controlDetails;
	MIXERCONTROLDETAILS_BOOLEAN valueBoolean;

	controlDetails.dwControlID = dwControlID;
	controlDetails.cbStruct = sizeof(MIXERCONTROLDETAILS);
	controlDetails.cChannels = 1;
	controlDetails.cMultipleItems = 0;
	controlDetails.cbDetails = sizeof(MIXERCONTROLDETAILS_BOOLEAN);
	controlDetails.paDetails = &valueBoolean;

	// Retrieve the boolean value
	//
	res = mixerGetControlDetails(reinterpret_cast<HMIXEROBJ>(mixId), &controlDetails, MIXER_OBJECTF_MIXER | MIXER_GETCONTROLDETAILSF_VALUE);
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("mixerGetControlDetails(MIXER_GETCONTROLDETAILSF_VALUE) failed (err=%d)", res);
		return false;
	}

	// Deliver the retrieved value
	//
	if (valueBoolean.fValue == 0)
		value = false;
	else
		value = true;

	return true;
}

// ----------------------------------------------------------------------------
//  GetSelectedMuxSource
// ----------------------------------------------------------------------------

bool WinAudioMixerManager::GetSelectedMuxSource(UINT mixId, DWORD dwControlID, DWORD cMultipleItems, UINT& index) const
{
	assert(cMultipleItems <= MAX_NUMBER_OF_MULTIPLE_ITEMS);

	MMRESULT                    res;
	MIXERCONTROLDETAILS         controlDetails;
	MIXERCONTROLDETAILS_BOOLEAN valueBoolean[MAX_NUMBER_OF_MULTIPLE_ITEMS];
	memset(&valueBoolean, 0, sizeof(valueBoolean));

	controlDetails.dwControlID = dwControlID;
	controlDetails.cbStruct = sizeof(MIXERCONTROLDETAILS);
	controlDetails.cChannels = 1;
	controlDetails.cMultipleItems = cMultipleItems;
	controlDetails.cbDetails = sizeof(MIXERCONTROLDETAILS_BOOLEAN);
	controlDetails.paDetails = &valueBoolean;

	// Retrieve the boolean values
	//
	res = mixerGetControlDetails(reinterpret_cast<HMIXEROBJ>(mixId), &controlDetails, MIXER_OBJECTF_MIXER | MIXER_GETCONTROLDETAILSF_VALUE);
	if (res != MMSYSERR_NOERROR)
	{
		LOGW("mixerGetControlDetails(MIXER_GETCONTROLDETAILSF_VALUE) failed (err=%d)", res);
		return false;
	}

	// Map the current MUX setting to an index corresponding to a source index.
	// e.g. with cMultipleItems = 3,
	//  valueBoolean[] = {1,0,0} => index = 2
	//  valueBoolean[] = {0,1,0} => index = 1
	//  valueBoolean[] = {0,0,1} => index = 0
	//
	// If there is no "1" in the array, we assume index should be 0.
	index = 0;
	for (DWORD i = 0; i < cMultipleItems; i++)
	{
		if (valueBoolean[i].fValue > 0)
		{
			index = (cMultipleItems - 1) - i;
			break;
		}
	}

	return true;
}


// ----------------------------------------------------------------------------
//  TraceStatusAndSupportFlags
// ----------------------------------------------------------------------------

void WinAudioMixerManager::TraceStatusAndSupportFlags(DWORD fdwLine) const
{
	TCHAR buf[128];

	StringCchPrintf(buf, 128, TEXT("status & support flags : 0x%x "), fdwLine);

	switch (fdwLine)
	{
	case MIXERLINE_LINEF_ACTIVE:
		StringCchCat(buf, 128, TEXT("(ACTIVE DESTINATION)"));
		break;
	case MIXERLINE_LINEF_DISCONNECTED:
		StringCchCat(buf, 128, TEXT("(DISCONNECTED)"));
		break;
	case MIXERLINE_LINEF_SOURCE:
		StringCchCat(buf, 128, TEXT("(INACTIVE SOURCE)"));
		break;
	case MIXERLINE_LINEF_SOURCE | MIXERLINE_LINEF_ACTIVE:
		StringCchCat(buf, 128, TEXT("(ACTIVE SOURCE)"));
		break;
	default:
		StringCchCat(buf, 128, TEXT("(INVALID)"));
		break;
	}

	LOGI("%s", WideToUTF8(buf));
}

// ----------------------------------------------------------------------------
//  TraceComponentType
// ----------------------------------------------------------------------------

void WinAudioMixerManager::TraceComponentType(DWORD dwComponentType) const
{
	TCHAR buf[128];

	StringCchPrintf(buf, 128, TEXT("component type         : 0x%x "), dwComponentType);

	switch (dwComponentType)
	{
		// Destination
	case MIXERLINE_COMPONENTTYPE_DST_UNDEFINED:
		StringCchCat(buf, 128, TEXT("(DST_UNDEFINED)"));
		break;
	case MIXERLINE_COMPONENTTYPE_DST_DIGITAL:
		StringCchCat(buf, 128, TEXT("(DST_DIGITAL)"));
		break;
	case MIXERLINE_COMPONENTTYPE_DST_LINE:
		StringCchCat(buf, 128, TEXT("(DST_LINE)"));
		break;
	case MIXERLINE_COMPONENTTYPE_DST_MONITOR:
		StringCchCat(buf, 128, TEXT("(DST_MONITOR)"));
		break;
	case MIXERLINE_COMPONENTTYPE_DST_SPEAKERS:
		StringCchCat(buf, 128, TEXT("(DST_SPEAKERS)"));
		break;
	case MIXERLINE_COMPONENTTYPE_DST_HEADPHONES:
		StringCchCat(buf, 128, TEXT("(DST_HEADPHONES)"));
		break;
	case MIXERLINE_COMPONENTTYPE_DST_TELEPHONE:
		StringCchCat(buf, 128, TEXT("(DST_TELEPHONE)"));
		break;
	case MIXERLINE_COMPONENTTYPE_DST_WAVEIN:
		StringCchCat(buf, 128, TEXT("(DST_WAVEIN)"));
		break;
	case MIXERLINE_COMPONENTTYPE_DST_VOICEIN:
		StringCchCat(buf, 128, TEXT("(DST_VOICEIN)"));
		break;
		// Source
	case MIXERLINE_COMPONENTTYPE_SRC_UNDEFINED:
		StringCchCat(buf, 128, TEXT("(SRC_UNDEFINED)"));
		break;
	case MIXERLINE_COMPONENTTYPE_SRC_DIGITAL:
		StringCchCat(buf, 128, TEXT("(SRC_DIGITAL)"));
		break;
	case MIXERLINE_COMPONENTTYPE_SRC_LINE:
		StringCchCat(buf, 128, TEXT("(SRC_LINE)"));
		break;
	case MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE:
		StringCchCat(buf, 128, TEXT("(SRC_MICROPHONE)"));
		break;
	case MIXERLINE_COMPONENTTYPE_SRC_SYNTHESIZER:
		StringCchCat(buf, 128, TEXT("(SRC_SYNTHESIZER)"));
		break;
	case MIXERLINE_COMPONENTTYPE_SRC_COMPACTDISC:
		StringCchCat(buf, 128, TEXT("(SRC_COMPACTDISC)"));
		break;
	case MIXERLINE_COMPONENTTYPE_SRC_TELEPHONE:
		StringCchCat(buf, 128, TEXT("(SRC_TELEPHONE)"));
		break;
	case MIXERLINE_COMPONENTTYPE_SRC_PCSPEAKER:
		StringCchCat(buf, 128, TEXT("(SRC_PCSPEAKER)"));
		break;
	case MIXERLINE_COMPONENTTYPE_SRC_WAVEOUT:
		StringCchCat(buf, 128, TEXT("(SRC_WAVEOUT)"));
		break;
	case MIXERLINE_COMPONENTTYPE_SRC_AUXILIARY:
		StringCchCat(buf, 128, TEXT("(SRC_AUXILIARY)"));
		break;
	case MIXERLINE_COMPONENTTYPE_SRC_ANALOG:
		StringCchCat(buf, 128, TEXT("(SRC_ANALOG)"));
		break;
	default:
		StringCchCat(buf, 128, TEXT("(INVALID)"));
		break;
	}

	LOGI("%s", WideToUTF8(buf));
}

// ----------------------------------------------------------------------------
//  TraceTargetType
// ----------------------------------------------------------------------------

void WinAudioMixerManager::TraceTargetType(DWORD dwType) const
{
	TCHAR buf[128];

	StringCchPrintf(buf, 128, TEXT("media device type      : 0x%x "), dwType);

	switch (dwType)
	{
	case MIXERLINE_TARGETTYPE_UNDEFINED:
		StringCchCat(buf, 128, TEXT("(UNDEFINED)"));
		break;
	case MIXERLINE_TARGETTYPE_WAVEOUT:
		StringCchCat(buf, 128, TEXT("(WAVEOUT)"));
		break;
	case MIXERLINE_TARGETTYPE_WAVEIN:
		StringCchCat(buf, 128, TEXT("(WAVEIN)"));
		break;
	case MIXERLINE_TARGETTYPE_MIDIOUT:
		StringCchCat(buf, 128, TEXT("(MIDIOUT)"));
		break;
	case MIXERLINE_TARGETTYPE_MIDIIN:
		StringCchCat(buf, 128, TEXT("(MIDIIN)"));
		break;
	default:
		StringCchCat(buf, 128, TEXT("(INVALID)"));
		break;
	}

	LOGI("%s", WideToUTF8(buf));
}


// ----------------------------------------------------------------------------
//  TraceControlType
// ----------------------------------------------------------------------------

void WinAudioMixerManager::TraceControlType(DWORD dwControlType) const
{
	TCHAR buf[128];

	// Class type classification
	//
	StringCchPrintf(buf, 128, TEXT("class type             : 0x%x "), dwControlType);

	switch (dwControlType & MIXERCONTROL_CT_CLASS_MASK)
	{
	case MIXERCONTROL_CT_CLASS_CUSTOM:
		StringCchCat(buf, 128, TEXT("(CT_CLASS_CUSTOM)"));
		break;
	case MIXERCONTROL_CT_CLASS_METER:
		StringCchCat(buf, 128, TEXT("(CT_CLASS_METER)"));
		break;
	case MIXERCONTROL_CT_CLASS_SWITCH:
		StringCchCat(buf, 128, TEXT("(CT_CLASS_SWITCH)"));
		break;
	case MIXERCONTROL_CT_CLASS_NUMBER:
		StringCchCat(buf, 128, TEXT("(CT_CLASS_NUMBER)"));
		break;
	case MIXERCONTROL_CT_CLASS_SLIDER:
		StringCchCat(buf, 128, TEXT("(CT_CLASS_SLIDER)"));
		break;
	case MIXERCONTROL_CT_CLASS_FADER:
		StringCchCat(buf, 128, TEXT("(CT_CLASS_FADER)"));
		break;
	case MIXERCONTROL_CT_CLASS_TIME:
		StringCchCat(buf, 128, TEXT("(CT_CLASS_TIME)"));
		break;
	case MIXERCONTROL_CT_CLASS_LIST:
		StringCchCat(buf, 128, TEXT("(CT_CLASS_LIST)"));
		break;
	default:
		StringCchCat(buf, 128, TEXT("(INVALID)"));
		break;
	}

	LOGI("%s", WideToUTF8(buf));

	// Control type (for each class)
	//
	StringCchPrintf(buf, 128, TEXT("control type           : 0x%x "), dwControlType);

	switch (dwControlType)
	{
	case MIXERCONTROL_CONTROLTYPE_CUSTOM:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_CUSTOM)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_BOOLEANMETER:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_BOOLEANMETER)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_SIGNEDMETER:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_SIGNEDMETER)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_PEAKMETER:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_PEAKMETER)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_UNSIGNEDMETER:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_UNSIGNEDMETER)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_BOOLEAN:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_BOOLEAN)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_ONOFF:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_ONOFF)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_MUTE:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_MUTE)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_MONO:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_MONO)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_LOUDNESS:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_LOUDNESS)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_STEREOENH:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_STEREOENH)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_BASS_BOOST:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_BASS_BOOST)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_BUTTON:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_BUTTON)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_DECIBELS:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_DECIBELS)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_SIGNED:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_SIGNED)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_UNSIGNED:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_UNSIGNED)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_PERCENT:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_PERCENT)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_SLIDER:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_SLIDER)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_PAN:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_PAN)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_QSOUNDPAN:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_QSOUNDPAN)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_FADER:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_FADER)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_VOLUME:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_VOLUME)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_BASS:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_BASS)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_TREBLE:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_TREBLE)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_EQUALIZER:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_EQUALIZER)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_SINGLESELECT:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_SINGLESELECT)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_MUX:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_MUX)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_MULTIPLESELECT:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_MULTIPLESELECT)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_MIXER:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_MIXER)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_MICROTIME:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_MICROTIME)"));
		break;
	case MIXERCONTROL_CONTROLTYPE_MILLITIME:
		StringCchCat(buf, 128, TEXT("(CONTROLTYPE_MILLITIME)"));
		break;
	default:
		StringCchCat(buf, 128, TEXT("(INVALID)"));
		break;
	}

	LOGI("%s", WideToUTF8(buf));
}

// ----------------------------------------------------------------------------
//  TraceControlStatusAndSupportFlags
//
//  fdwControl
//
//  Status and support flags for the audio line control. The following values
//  are defined:
//
//  MIXERCONTROL_CONTROLF_DISABLED
//
//  The control is disabled, perhaps due to other settings for the mixer hardware,
//  and cannot be used. An application can read current settings from a
//  disabled control, but it cannot apply settings.
//
//  MIXERCONTROL_CONTROLF_MULTIPLE
//
//  The control has two or more settings per channel. An equalizer, for example,
//  requires this flag because each frequency band can be set to a different value.
//  An equalizer that affects both channels of a stereo line in a uniform fashion
//  will also specify the MIXERCONTROL_CONTROLF_UNIFORM flag.
//
//  MIXERCONTROL_CONTROLF_UNIFORM
//
//  The control acts on all channels of a multichannel line in a uniform fashion.
//  For example, a control that mutes both channels of a stereo line would set
//  this flag. Most MIXERCONTROL_CONTROLTYPE_MUX and
//  MIXERCONTROL_CONTROLTYPE_MIXER controls also specify the
//  MIXERCONTROL_CONTROLF_UNIFORM flag.
// ----------------------------------------------------------------------------

void WinAudioMixerManager::TraceControlStatusAndSupportFlags(DWORD fdwControl) const
{
	TCHAR buf[128];

	StringCchPrintf(buf, 128, TEXT("control support flags  : 0x%x "), fdwControl);

	if (fdwControl & MIXERCONTROL_CONTROLF_DISABLED)
	{
		// The control is disabled, perhaps due to other settings for the mixer hardware,
		// and cannot be used. An application can read current settings from a disabled
		// control, but it cannot apply settings.
		StringCchCat(buf, 128, TEXT("(CONTROLF_DISABLED)"));
	}

	if (fdwControl & MIXERCONTROL_CONTROLF_MULTIPLE)
	{
		// The control has two or more settings per channel. An equalizer, for example,
		// requires this flag because each frequency band can be set to a different
		// value. An equalizer that affects both channels of a stereo line in a
		// uniform fashion will also specify the MIXERCONTROL_CONTROLF_UNIFORM flag.
		StringCchCat(buf, 128, TEXT("(CONTROLF_MULTIPLE)"));
	}

	if (fdwControl & MIXERCONTROL_CONTROLF_UNIFORM)
	{
		// The control acts on all channels of a multichannel line in a uniform
		// fashion. For example, a control that mutes both channels of a stereo
		// line would set this flag. Most MIXERCONTROL_CONTROLTYPE_MUX and
		// MIXERCONTROL_CONTROLTYPE_MIXER controls also specify the
		// MIXERCONTROL_CONTROLF_UNIFORM flag.
		StringCchCat(buf, 128, TEXT("(CONTROLF_UNIFORM)"));
	}

	LOGI("%s", WideToUTF8(buf));
}


// ----------------------------------------------------------------------------
//  ClearSpeakerState II (II)
// ----------------------------------------------------------------------------

void WinAudioMixerManager::ClearSpeakerState()
{
	for (int i = 0; i < MAX_NUMBER_MIXER_DEVICES; i++)
	{
		ClearSpeakerState(i);
	}
}

// ----------------------------------------------------------------------------
//  ClearSpeakerState I (II)
// ----------------------------------------------------------------------------

void WinAudioMixerManager::ClearSpeakerState(UINT idx)
{
	_speakerState[idx].dwLineID = 0L;
	_speakerState[idx].dwVolumeControlID = 0L;
	_speakerState[idx].dwMuteControlID = 0L;
	_speakerState[idx].speakerIsValid = false;
	_speakerState[idx].muteControlIsValid = false;
	_speakerState[idx].volumeControlIsValid = false;
}

// ----------------------------------------------------------------------------
//  SpeakerIsValid
// ----------------------------------------------------------------------------

bool WinAudioMixerManager::SpeakerIsValid(UINT idx) const
{
	return (_speakerState[idx].speakerIsValid);
}

// ----------------------------------------------------------------------------
//  ValidSpeakers
//
//  Counts number of valid speaker destinations for all mixer devices.
// ----------------------------------------------------------------------------

UINT WinAudioMixerManager::ValidSpeakers() const
{
	UINT nSpeakers(0);
	for (int i = 0; i < MAX_NUMBER_MIXER_DEVICES; i++)
	{
		if (SpeakerIsValid(i))
			nSpeakers++;
	}
	return nSpeakers;
}

// ----------------------------------------------------------------------------
//  TraceWaveOutError
// ----------------------------------------------------------------------------

void WinAudioMixerManager::TraceWaveOutError(MMRESULT error) const
{
	TCHAR buf[MAXERRORLENGTH];
	TCHAR msg[MAXERRORLENGTH];

	StringCchPrintf(buf, MAXERRORLENGTH, TEXT("Error details: "));
	waveOutGetErrorText(error, msg, MAXERRORLENGTH);
	StringCchCat(buf, MAXERRORLENGTH, msg);
	LOGI("%s", WideToUTF8(buf));
}

// ----------------------------------------------------------------------------
//  WideToUTF8
// ----------------------------------------------------------------------------

char* WinAudioMixerManager::WideToUTF8(const TCHAR* src) const {
#ifdef UNICODE
	const size_t kStrLen = sizeof(_str);
	memset(_str, 0, kStrLen);
	// Get required size (in bytes) to be able to complete the conversion.
	unsigned int required_size = (unsigned int)WideCharToMultiByte(CP_UTF8, 0, src, -1, _str, 0, 0, 0);
	if (required_size <= kStrLen)
	{
		// Process the entire input string, including the terminating null char.
		if (WideCharToMultiByte(CP_UTF8, 0, src, -1, _str, kStrLen, 0, 0) == 0)
			memset(_str, 0, kStrLen);
	}
	return _str;
#else
	return const_cast<char*>(src);
#endif
}