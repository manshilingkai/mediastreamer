#ifndef WinAudioMixerManager_h
#define WinAudioMixerManager_h

#include <winsock2.h>
#include <Windows.h>
#include <mmsystem.h>
#include <stdint.h>

enum WindowsDeviceType {
	kDefaultCommunicationDevice = -1,
	kDefaultDevice = -2
};

class WinAudioMixerManager
{
public:
	enum { MAX_NUMBER_MIXER_DEVICES = 40 };
	enum { MAX_NUMBER_OF_LINE_CONTROLS = 20 };
	enum { MAX_NUMBER_OF_MULTIPLE_ITEMS = 20 };
	struct SpeakerLineInfo
	{
		DWORD dwLineID;
		bool  speakerIsValid;
		DWORD dwVolumeControlID;
		bool  volumeControlIsValid;
		DWORD dwMuteControlID;
		bool  muteControlIsValid;
	};
public:
	int32_t EnumerateAll();
	int32_t EnumerateSpeakers();
	int32_t OpenSpeaker(WindowsDeviceType device);
	int32_t OpenSpeaker(uint16_t index);
	int32_t SetSpeakerVolume(uint32_t volume);
	int32_t SpeakerVolume(uint32_t& volume) const;
	int32_t MaxSpeakerVolume(uint32_t& maxVolume) const;
	int32_t MinSpeakerVolume(uint32_t& minVolume) const;
	int32_t SpeakerVolumeStepSize(uint16_t& stepSize) const;
	int32_t SpeakerVolumeIsAvailable(bool& available);
	int32_t SpeakerMuteIsAvailable(bool& available);
	int32_t SetSpeakerMute(bool enable);
	int32_t SpeakerMute(bool& enabled) const;
	int32_t Close();
	int32_t CloseSpeaker();
	bool SpeakerIsInitialized() const;
	UINT Devices() const;
private:
	UINT DestinationLines(UINT mixId) const;
	UINT SourceLines(UINT mixId, DWORD destId) const;
	bool GetCapabilities(UINT mixId, MIXERCAPS& caps, bool trace = false) const;
	bool GetDestinationLineInfo(UINT mixId, DWORD destId, MIXERLINE& line, bool trace = false) const;
	bool GetSourceLineInfo(UINT mixId, DWORD destId, DWORD srcId, MIXERLINE& line, bool trace = false) const;

	bool GetAllLineControls(UINT mixId, const MIXERLINE& line, MIXERCONTROL* controlArray, bool trace = false) const;
	bool GetLineControl(UINT mixId, DWORD dwControlID, MIXERCONTROL& control) const;
	bool GetControlDetails(UINT mixId, MIXERCONTROL& controlArray, bool trace = false) const;
	bool GetUnsignedControlValue(UINT mixId, DWORD dwControlID, DWORD& dwValue) const;
	bool SetUnsignedControlValue(UINT mixId, DWORD dwControlID, DWORD dwValue) const;
	bool SetBooleanControlValue(UINT mixId, DWORD dwControlID, bool value) const;
	bool GetBooleanControlValue(UINT mixId, DWORD dwControlID, bool& value) const;
	bool GetSelectedMuxSource(UINT mixId, DWORD dwControlID, DWORD cMultipleItems, UINT& index) const;

private:
	void ClearSpeakerState();
	void ClearSpeakerState(UINT idx);
	bool SpeakerIsValid(UINT idx) const;
	UINT ValidSpeakers() const;

	void TraceStatusAndSupportFlags(DWORD fdwLine) const;
	void TraceTargetType(DWORD dwType) const;
	void TraceComponentType(DWORD dwComponentType) const;
	void TraceControlType(DWORD dwControlType) const;
	void TraceControlStatusAndSupportFlags(DWORD fdwControl) const;
	void TraceWaveOutError(MMRESULT error) const;
	// Converts from wide-char to UTF-8 if UNICODE is defined.
	// Does nothing if UNICODE is undefined.
	char* WideToUTF8(const TCHAR* src) const;

public:
	WinAudioMixerManager();
	~WinAudioMixerManager();

private:
	HMIXER                  _outputMixerHandle;
	UINT                    _outputMixerID;
	SpeakerLineInfo         _speakerState[MAX_NUMBER_MIXER_DEVICES];
	mutable char            _str[MAXERRORLENGTH];
};

#endif
