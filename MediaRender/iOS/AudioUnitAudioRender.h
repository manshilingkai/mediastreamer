//
//  AudioUnitAudioRender.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__AudioUnitAudioRender__
#define __MediaPlayer__AudioUnitAudioRender__

#include <stdio.h>
#include "AudioRender.h"

#include <pthread.h>

#include <AudioUnit/AudioUnit.h>

class AudioUnitAudioRender : public AudioRender
{
public:
    AudioUnitAudioRender();
    ~AudioUnitAudioRender();
    
    AudioRenderConfigure* configureAdaptation(int sampleRate, int channelCount);
    
    void setPCMDataInputer(IPCMDataInputer* inputer);

    int init(AudioRenderMode mode);
    int terminate();
    bool isInitialized() { return initialized; }
    
    int startPlayout();
    int stopPlayout();
    
#ifdef IOS
    void iOSAVAudioSessionInterruption(bool isInterrupting);
    void pause();
    void resume();
#endif

    bool isPlaying();
    
    void setVolume(float volume);
    void setMute(bool mute);
    
    // -1 : input data invalid
    //  0 : push success
    //  1 : is full
    int pushPCMData(uint8_t* data, int size, int64_t pts);
    
    void flush();

    int64_t getCurrentPts();
private:
    // 1 buffer = 10ms
    enum {
//        kNumFIFOBuffers = 16,
        kNumFIFOBuffers = 10,
    };
    
    static OSStatus PlayoutProcess(void *inRefCon,
                                   AudioUnitRenderActionFlags *ioActionFlags,
                                   const AudioTimeStamp *timeStamp,
                                   UInt32 inBusNumber,
                                   UInt32 inNumberFrames,
                                   AudioBufferList *ioData);
    
    OSStatus PlayoutProcessImpl(uint32_t inNumberFrames,
                                AudioBufferList *ioData);
        
    int64_t getLatency();
    
    AudioRenderConfigure audioRenderConfigure;
    
    AudioUnit _auAudioRender;
    pthread_mutex_t mAUAudioRenderLock;
//    void* _audioInterruptionObserver;

    bool initialized;
    bool playing;
    pthread_mutex_t mAudioRenderStateLock;
    
    uint8_t *fifo;
    int fifoSize;
    
    void allocBuffers();
    void freeBuffers();
    
    pthread_mutex_t mLock;
    pthread_cond_t mCondition;
    
    int write_pos;
    int read_pos;
    int cache_len;

    int64_t currentPts;
    
private:
    bool isMute;
    pthread_mutex_t mMuteLock;
    
private:
    IPCMDataInputer* mPCMDataInputer;
};

#endif /* defined(__MediaPlayer__AudioUnitAudioRender__) */
