//
//  AudioUnitAudioRender.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#import <AVFoundation/AVFAudio.h>
#import <AudioToolbox/AudioSession.h>
//#import <Foundation/Foundation.h>

#include "AudioUnitAudioRender.h"
#include "MediaLog.h"

AudioUnitAudioRender::AudioUnitAudioRender()
{
    _auAudioRender = NULL;
    pthread_mutex_init(&mAUAudioRenderLock, NULL);
//    _audioInterruptionObserver = NULL;
    
    initialized = false;
    playing = false;
    pthread_mutex_init(&mAudioRenderStateLock, NULL);

    isMute = false;
    pthread_mutex_init(&mMuteLock, NULL);
    
    mPCMDataInputer = NULL;
}

AudioUnitAudioRender::~AudioUnitAudioRender()
{
    terminate();
    
    pthread_mutex_destroy(&mAudioRenderStateLock);
    
    pthread_mutex_destroy(&mMuteLock);
    
    pthread_mutex_destroy(&mAUAudioRenderLock);
}

AudioRenderConfigure* AudioUnitAudioRender::configureAdaptation(int sampleRate, int channelCount)
{
    audioRenderConfigure.sampleRate = sampleRate;
    audioRenderConfigure.channelCount = channelCount;
    
    audioRenderConfigure.sampleFormat = AV_SAMPLE_FMT_S16;
    
    if (audioRenderConfigure.channelCount==1) {
        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_MONO;
    }else {
        audioRenderConfigure.channelCount = 2;
        audioRenderConfigure.channelLayout = AV_CH_LAYOUT_STEREO;
    }
    
    return &audioRenderConfigure;
}

void AudioUnitAudioRender::setPCMDataInputer(IPCMDataInputer* inputer)
{
    mPCMDataInputer = inputer;
}

void AudioUnitAudioRender::allocBuffers()
{
    fifoSize = audioRenderConfigure.sampleRate * audioRenderConfigure.channelCount * av_get_bytes_per_sample(audioRenderConfigure.sampleFormat) / 100 * kNumFIFOBuffers;
    
    fifo = (uint8_t *)malloc(fifoSize);
}

void AudioUnitAudioRender::freeBuffers()
{
    if (fifo!=NULL) {
        free(fifo);
        fifo = NULL;
    }
}

int AudioUnitAudioRender::init(AudioRenderMode mode)
{
    if (initialized) return 0;
    
    OSStatus result = -1;
    
    // Check if already initialized
    if (NULL != _auAudioRender) {
        // We already have initialized before and created any of the audio unit,
        // check that all exist
        LOGW("Already initialized");
        // todo: Call AudioUnitReset() here and empty all buffers?
        return 0;
    }
    
    // Create Audio Render Audio Unit
    AudioComponentDescription desc;
    AudioComponent comp;
    desc.componentType = kAudioUnitType_Output;
    if (mode==VOICE_MODE) {
        desc.componentSubType = kAudioUnitSubType_VoiceProcessingIO;
    }else{
        desc.componentSubType = kAudioUnitSubType_RemoteIO;
    }
    desc.componentManufacturer = kAudioUnitManufacturer_Apple;
    desc.componentFlags = 0;
    desc.componentFlagsMask = 0;
    
    comp = AudioComponentFindNext(NULL, &desc);
    if (NULL == comp) {
        LOGE("Could not find audio component for Audio Unit");
        return -1;
    }
    
    pthread_mutex_lock(&mAUAudioRenderLock);
    result = AudioComponentInstanceNew(comp, &_auAudioRender);
    if (0 != result) {
        _auAudioRender = NULL;
        pthread_mutex_unlock(&mAUAudioRenderLock);
        LOGE("Could not create Audio Unit instance (result=%d)",(int)result);
        return -1;
    }
    
    Float64 preferredSampleRate(audioRenderConfigure.sampleRate);
    
    /*
    // Set preferred hardware sample rate
    NSError* error = nil;
    AVAudioSession* session = [AVAudioSession sharedInstance];

    Float64 preferredSampleRate(audioRenderConfigure.sampleRate);
    [session setPreferredSampleRate:preferredSampleRate
                              error:&error];
    if (error != nil) {
        const char* errorString = [[error localizedDescription] UTF8String];
        LOGE("Could not set preferred sample rate: %s", errorString);
    }

    error = nil;
    if (mode==VOICE_MODE) {
        [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker | AVAudioSessionCategoryOptionAllowBluetooth error:&error];
    }else{
        [session setCategory:AVAudioSessionCategoryPlayback error:&error];
    }
    if (error != nil) {
        const char* errorString = [[error localizedDescription] UTF8String];
        LOGE("Could not set category: %s", errorString);
    }
    
    error = nil;
    if (mode==VOICE_MODE) {
        [session setMode:AVAudioSessionModeVoiceChat
                   error:&error];
    }else{
        [session setMode:AVAudioSessionModeMoviePlayback
                   error:&error];
    }

    if (error != nil) {
        const char* errorString = [[error localizedDescription] UTF8String];
        LOGE("Could not set mode: %s", errorString);
    }
    
    // Activate audio session.
    error = nil;
    [session setActive:YES
                 error:&error];
    if (error != nil) {
        LOGE("Error activating audio session");
    }
    */
    
    UInt32 enableIO = 1;
    result = AudioUnitSetProperty(_auAudioRender,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Output,
                                  0,   // output bus
                                  &enableIO,
                                  sizeof(enableIO));
    if (0 != result) {
        LOGE("Could not enable IO on output (result=%ld)", result);
    }
    
    if (mode==VOICE_MODE)
    {
        //turn on agc
        UInt32 turnOnAGC = 1;
        result = AudioUnitSetProperty(_auAudioRender,
                                      kAUVoiceIOProperty_VoiceProcessingEnableAGC,
                                      kAudioUnitScope_Global,
                                      0,
                                      &turnOnAGC,
                                      sizeof(turnOnAGC));
        if (0 != result) {
            LOGE("Could not turn on agc (result=%d)", result);
        }
    }
    
    // Set playout callback
    AURenderCallbackStruct auCbS;
    memset(&auCbS, 0, sizeof(auCbS));
    auCbS.inputProc = PlayoutProcess;
    auCbS.inputProcRefCon = this;
    result = AudioUnitSetProperty(_auAudioRender,
                                  kAudioUnitProperty_SetRenderCallback,
                                  kAudioUnitScope_Global, 0,
                                  &auCbS, sizeof(auCbS));
    if (0 != result) {
        LOGE("Could not set play callback for Audio Unit (result=%ld)", result);
    }
    
    // Get stream format for out/0
    AudioStreamBasicDescription playoutDesc;
    UInt32 size = sizeof(playoutDesc);
    result = AudioUnitGetProperty(_auAudioRender,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Output, 0, &playoutDesc,
                                  &size);
    if (0 != result) {
        LOGE("Could not get stream format Audio Unit out/0 (result=%ld)",result);
    }
    LOGI("Audio Unit playout opened in sampling rate %f",playoutDesc.mSampleRate);
    
    playoutDesc.mSampleRate = preferredSampleRate;
    
    // Set stream format for out/0  (use same sampling frequency as for out/0)
    playoutDesc.mFormatID = kAudioFormatLinearPCM;
    if (audioRenderConfigure.channelCount==1) {
        playoutDesc.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger
        | kLinearPCMFormatFlagIsPacked
        | kLinearPCMFormatFlagIsNonInterleaved;
    }else{
        playoutDesc.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger;
    }
    playoutDesc.mFramesPerPacket = 1;
    playoutDesc.mChannelsPerFrame = audioRenderConfigure.channelCount;
    playoutDesc.mBitsPerChannel = av_get_bytes_per_sample(audioRenderConfigure.sampleFormat)*8;
    playoutDesc.mBytesPerFrame = av_get_bytes_per_sample(audioRenderConfigure.sampleFormat)*playoutDesc.mChannelsPerFrame;
    playoutDesc.mBytesPerPacket = playoutDesc.mBytesPerFrame * playoutDesc.mFramesPerPacket;
    
    result = AudioUnitSetProperty(_auAudioRender,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input, 0, &playoutDesc, size);
    if (0 != result) {
        LOGE("Could not set stream format Audio Unit in/0 (result=%ld)", result);
    }
    
    // Initialize here already to be able to get/set stream properties.
    result = AudioUnitInitialize(_auAudioRender);
    if (0 != result) {
        LOGE("Could not init Audio Unit (result=%ld)", result);
        
        if (_auAudioRender) {
            AudioComponentInstanceDispose(_auAudioRender);
            _auAudioRender = NULL;
        }
        
        pthread_mutex_unlock(&mAUAudioRenderLock);
        return -1;
    }
    pthread_mutex_unlock(&mAUAudioRenderLock);
    
    /*
    // Get hardware sample rate for logging (see if we get what we asked for)
    double sampleRate = session.sampleRate;
    LOGI("Current HW sample rate is %f",sampleRate);
    */
    
    /*
    // Listen to audio interruptions.
    NSNotificationCenter* center = [NSNotificationCenter defaultCenter];
    id observer =
    [center addObserverForName:AVAudioSessionInterruptionNotification
                        object:nil
                         queue:[NSOperationQueue mainQueue]
                    usingBlock:^(NSNotification* notification) {
                        NSNumber* typeNumber =
                        [notification userInfo][AVAudioSessionInterruptionTypeKey];
                        AVAudioSessionInterruptionType type =
                        (AVAudioSessionInterruptionType)[typeNumber unsignedIntegerValue];
                        switch (type) {
                            case AVAudioSessionInterruptionTypeBegan:
                                // At this point our audio session has been deactivated and the
                                // audio unit render callbacks no longer occur. Nothing to do.
                                break;
                            case AVAudioSessionInterruptionTypeEnded: {
                                NSError* error = nil;
                                AVAudioSession* session = [AVAudioSession sharedInstance];
                                [session setActive:YES
                                             error:&error];
                                if (error != nil) {
                                    LOGE("Error activating audio session");
                                }
                                // Post interruption the audio unit render callbacks don't
                                // automatically continue, so we restart the unit manually here.
                                AudioOutputUnitStop(_auAudioRender);
                                AudioOutputUnitStart(_auAudioRender);
                                break;
                            }
                        }
                    }];
    // Increment refcount on observer using ARC bridge. Instance variable is a
    // void* instead of an id because header is included in other pure C++
    // files.
    _audioInterruptionObserver = (__bridge_retained void*)observer;
    */

    allocBuffers();
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mCondition, NULL);
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    currentPts = 0;

    pthread_mutex_lock(&mAudioRenderStateLock);
    playing = false;
    pthread_mutex_unlock(&mAudioRenderStateLock);
    
    initialized = true;
    
    return 0;
}

int AudioUnitAudioRender::terminate()
{
    if (!initialized) return 0;
    
    /*
    if (_audioInterruptionObserver != NULL) {
        NSNotificationCenter* center = [NSNotificationCenter defaultCenter];
        // Transfer ownership of observer back to ARC, which will dealloc the
        // observer once it exits this scope.
        id observer = (__bridge_transfer id)_audioInterruptionObserver;
        [center removeObserver:observer];
        _audioInterruptionObserver = NULL;
    }
    */
    
    // Close and delete AU
    pthread_mutex_lock(&mAUAudioRenderLock);
    OSStatus result = -1;
    if (NULL != _auAudioRender) {
        result = AudioOutputUnitStop(_auAudioRender);
        if (0 != result) {
            LOGE("Error stopping Audio Unit (result=%ld)", result);
        }
        
        result = AudioUnitUninitialize(_auAudioRender);
        if (0 != result) {
            LOGE("Error Uninitialize Audio Unit (result=%ld)", result);
        }
        
        result = AudioComponentInstanceDispose(_auAudioRender);
        if (0 != result) {
            LOGE("Error disposing Audio Unit (result=%ld)", result);
        }
        _auAudioRender = NULL;
    }
    pthread_mutex_unlock(&mAUAudioRenderLock);

/*
    // Disactivate audio session.
    NSError* error = nil;
    AVAudioSession* session = [AVAudioSession sharedInstance];
    [session setActive:NO
                 error:&error];
    if (error != nil) {
        LOGE("Error disactivating audio session");
    }
*/
    freeBuffers();
    
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mCondition);
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;

    pthread_mutex_lock(&mAudioRenderStateLock);
    playing = false;
    pthread_mutex_unlock(&mAudioRenderStateLock);
    
    initialized = false;
    
    return 0;
}

int AudioUnitAudioRender::startPlayout()
{
    if (!initialized) return -1;

    OSStatus result = AudioOutputUnitStart(_auAudioRender);
    if (0 != result) {
        LOGE("Error starting Audio Unit (result=%ld)", result);
        return -1;
    }
    
    pthread_mutex_lock(&mAudioRenderStateLock);
    playing = true;
    pthread_mutex_unlock(&mAudioRenderStateLock);

    return 0;
}

int AudioUnitAudioRender::stopPlayout()
{
    if (!initialized) return -1;

    OSStatus result = AudioOutputUnitStop(_auAudioRender);
    if (0 != result) {
        LOGE("Error stoping Audio Unit (result=%ld)", result);
        return -1;
    }
    
    pthread_mutex_lock(&mAudioRenderStateLock);
    playing = false;
    pthread_mutex_unlock(&mAudioRenderStateLock);
    
    return 0;
}

#ifdef IOS
void AudioUnitAudioRender::iOSAVAudioSessionInterruption(bool isInterrupting)
{
    pthread_mutex_lock(&mAUAudioRenderLock);
    if (isInterrupting) {
        if (_auAudioRender) {
            OSStatus result = AudioOutputUnitStop(_auAudioRender);
            if (0 != result) {
                LOGE("Error stoping Audio Unit (result=%ld)", result);
            }
        }
    }else{
        if (_auAudioRender) {
            OSStatus result = AudioOutputUnitStart(_auAudioRender);
            if (0 != result) {
                LOGE("Error starting Audio Unit (result=%ld)", result);
            }
        }
    }
    pthread_mutex_unlock(&mAUAudioRenderLock);
}

void AudioUnitAudioRender::pause()
{
    pthread_mutex_lock(&mAudioRenderStateLock);
    playing = false;
    pthread_mutex_unlock(&mAudioRenderStateLock);
}

void AudioUnitAudioRender::resume()
{
    pthread_mutex_lock(&mAudioRenderStateLock);
    playing = true;
    pthread_mutex_unlock(&mAudioRenderStateLock);
}
#endif

bool AudioUnitAudioRender::isPlaying()
{
    bool ret = false;
    
    pthread_mutex_lock(&mAudioRenderStateLock);
    ret = playing;
    pthread_mutex_unlock(&mAudioRenderStateLock);
    
    return ret;
}

// -1 : input data invalid
//  0 : push success
//  1 : is full
int AudioUnitAudioRender::pushPCMData(uint8_t* data, int size, int64_t pts)
{
    if (data==NULL || size<=0) return -1;
    
    NSError *error = nil;
    BOOL ret = [[AVAudioSession sharedInstance] setActive:YES error:&error];
    if (ret) {
    }else{
        LOGE("AVAudioSession sharedInstance setActive:YES Fail");
    }
    
    pthread_mutex_lock(&mLock);
    
    if(cache_len + size>fifoSize)
    {
        pthread_mutex_unlock(&mLock);
        return 1;
    }else
    {
        if(size>fifoSize-write_pos)
        {
            memcpy(fifo+write_pos,data,fifoSize-write_pos);
            memcpy(fifo,data+fifoSize-write_pos,size-(fifoSize-write_pos));
            write_pos = size-(fifoSize-write_pos);
        }else
        {
            memcpy(fifo+write_pos,data,size);
            write_pos = write_pos+size;
        }
        
        //update pts
        currentPts = pts - this->getLatency();
        
        cache_len = cache_len+size;
        
        pthread_mutex_unlock(&mLock);
        
        return 0;
    }
}

void AudioUnitAudioRender::setVolume(float volume)
{
    OSStatus result = AudioUnitSetParameter(_auAudioRender,
                                         kHALOutputParam_Volume,
                                         kAudioUnitScope_Global,
                                         0,
                                         volume,
                                         0 );
    
    if (0 != result) {
        LOGE("Set Volume Fail (result=%d)",result);
    }
}

void AudioUnitAudioRender::setMute(bool mute)
{
    pthread_mutex_lock(&mMuteLock);
    isMute = mute;
    pthread_mutex_unlock(&mMuteLock);
}

void AudioUnitAudioRender::flush()
{
    pthread_mutex_lock(&mLock);
    cache_len = 0;
    write_pos = 0;
    read_pos = 0;
    
    currentPts = 0;
    
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_broadcast(&mCondition);
}

int64_t AudioUnitAudioRender::getLatency()
{
    int64_t _playoutDelay = 0;

    // HW output latency
    Float32 f32(0);
    UInt32 size = sizeof(f32);
    OSStatus result = AudioSessionGetProperty(
                                              kAudioSessionProperty_CurrentHardwareOutputLatency, &size, &f32);
    if (0 != result) {
        LOGE("error HW latency (result=%d)", result);
    }
    _playoutDelay += static_cast<int>(f32 * 1000000);
    // HW buffer duration
    f32 = 0;
    result = AudioSessionGetProperty(
                                     kAudioSessionProperty_CurrentHardwareIOBufferDuration, &size, &f32);
    if (0 != result) {
        LOGE("error HW buffer duration (result=%d)", result);
    }
    _playoutDelay += static_cast<int>(f32 * 1000000);
    // AU latency
    Float64 f64(0);
    size = sizeof(f64);
    result = AudioUnitGetProperty(_auAudioRender,
                                  kAudioUnitProperty_Latency, kAudioUnitScope_Global, 0, &f64, &size);
    if (0 != result) {
        LOGE("error AU latency (result=%d)", result);
    }
    _playoutDelay += static_cast<int>(f64 * 1000000);

    return static_cast<int64_t>(_playoutDelay + cache_len * 10 * 1000 * kNumFIFOBuffers / fifoSize);
}

/*
int64_t AudioUnitAudioRender::getLatency()
{
    // Since this is eventually rounded to integral ms, add 0.5ms
    // here to get round-to-nearest-int behavior instead of
    // truncation.
    
    double totalDelaySeconds = 0.0005;
    
    // HW output latency
    AVAudioSession* session = [AVAudioSession sharedInstance];
    double latency = session.outputLatency;
    assert(latency >= 0);
    totalDelaySeconds += latency;
    
    // HW buffer duration
    double ioBufferDuration = session.IOBufferDuration;
    assert(ioBufferDuration >= 0);
    totalDelaySeconds += ioBufferDuration;
    
    // AU latency
    Float64 f64(0);
    UInt32 size = sizeof(f64);
    OSStatus result = AudioUnitGetProperty(_auAudioRender, kAudioUnitProperty_Latency,
                                           kAudioUnitScope_Global, 0, &f64, &size);
    if (0 != result) {
        LOGE("error AU latency (result=%d)", result);
    }
    assert(f64 >= 0);
    totalDelaySeconds += f64;
    
    //FIFO latency
    int64_t _playoutDelayNs = static_cast<int64_t>(totalDelaySeconds*1000000 + cache_len * 10 * 1000 * kNumFIFOBuffers / fifoSize);
    
    // To ns
    return _playoutDelayNs;
}
*/

OSStatus AudioUnitAudioRender::PlayoutProcess(void *inRefCon,
                        AudioUnitRenderActionFlags *ioActionFlags,
                        const AudioTimeStamp *timeStamp,
                        UInt32 inBusNumber,
                        UInt32 inNumberFrames,
                        AudioBufferList *ioData)
{
    AudioUnitAudioRender* ptrThis = static_cast<AudioUnitAudioRender*>(inRefCon);
    
    return ptrThis->PlayoutProcessImpl(inNumberFrames, ioData);
}

OSStatus AudioUnitAudioRender::PlayoutProcessImpl(uint32_t inNumberFrames,
                            AudioBufferList *ioData)
{
    uint8_t* playBuffer = static_cast<uint8_t*>(ioData->mBuffers[0].mData);
    unsigned int onebufferSize = ioData->mBuffers[0].mDataByteSize;
    memset(playBuffer, 0, onebufferSize);  // Start with empty buffer
    
    pthread_mutex_lock(&mAudioRenderStateLock);
    if (!playing) {
        pthread_mutex_unlock(&mAudioRenderStateLock);
        return 0;
    }
    pthread_mutex_unlock(&mAudioRenderStateLock);
    
    if (mPCMDataInputer!=NULL) {
        int readSize = mPCMDataInputer->onInputPCMData(&playBuffer, onebufferSize);
        
        pthread_mutex_lock(&mLock);
        currentPts += readSize * 1000 * 1000 / (audioRenderConfigure.sampleRate * audioRenderConfigure.channelCount * av_get_bytes_per_sample(audioRenderConfigure.sampleFormat));
        pthread_mutex_unlock(&mLock);
    }else {
        pthread_mutex_lock(&mLock);
        
        if (cache_len >= onebufferSize) {
            
            if(onebufferSize>fifoSize-read_pos)
            {
                memcpy(playBuffer,fifo+read_pos,fifoSize-read_pos);
                memcpy(playBuffer+fifoSize-read_pos,fifo,onebufferSize-(fifoSize-read_pos));
                read_pos = onebufferSize-(fifoSize-read_pos);
            }else
            {
                memcpy(playBuffer,fifo+read_pos,onebufferSize);
                read_pos = read_pos + onebufferSize;
            }
            
            cache_len = cache_len - onebufferSize;
            
            //update pts
            currentPts += static_cast<int64_t>(onebufferSize * 10 * 1000 * kNumFIFOBuffers / fifoSize);
            
        }else{
            
        }
        pthread_mutex_unlock(&mLock);
    }

    pthread_mutex_lock(&mMuteLock);
    if (isMute) {
        memset(playBuffer, 0, onebufferSize);
    }
    pthread_mutex_unlock(&mMuteLock);
    
    return 0;
}

int64_t AudioUnitAudioRender::getCurrentPts()
{
    int64_t pts = 0;
    pthread_mutex_lock(&mLock);
    pts = currentPts;
    pthread_mutex_unlock(&mLock);
    return pts;
}
