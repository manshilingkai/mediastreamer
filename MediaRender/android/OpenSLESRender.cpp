//
//  OpenSLESRender.cpp
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include <assert.h>
#include <unistd.h>
#include "OpenSLESRender.h"
#include "MediaLog.h"

#define VOID_RETURN
#define OPENSL_RETURN_ON_FAILURE(op, ret_val)                    \
  do {                                                           \
    SLresult err = (op);                                         \
    if (err != SL_RESULT_SUCCESS) {                              \
      LOGE("OpenSL error: %d", err);                             \
      assert(false);                                             \
      return ret_val;                                            \
    }                                                            \
} while (0)

static const SLEngineOption kOption[] = {
    { SL_ENGINEOPTION_THREADSAFE, static_cast<SLuint32>(SL_BOOLEAN_TRUE) },
};
// aux effect on the output mix, used by the buffer queue player
static const SLEnvironmentalReverbSettings reverbSettings = SL_I3DL2_ENVIRONMENT_PRESET_STONECORRIDOR;

OpenSLESRender::OpenSLESRender()
    : initialized(false),
      playing(false),
      sles_engine_(NULL),
      sles_engine_itf_(NULL),
      sles_player_(NULL),
      sles_player_itf_(NULL),
      sles_player_sbq_itf_(NULL),
      sles_output_mixer_(NULL),
      sles_output_mixer_environmentalReverb_itf_(NULL),
      sles_player_effectSend_itf_(NULL),
      sles_player_muteSolo_itf_(NULL),
      sles_player_volume_itf_(NULL),
      audioRenderConfigure(NULL)
{
    openSLESLatency = 0;
    audioRenderConfigure = new AudioRenderConfigure;
    
    mPCMDataInputer = NULL;
}

OpenSLESRender::~OpenSLESRender()
{
    if (initialized) {
        terminate();
    }
    
    if(audioRenderConfigure!=NULL)
    {
        delete audioRenderConfigure;
        audioRenderConfigure = NULL;
    }
}

AudioRenderConfigure* OpenSLESRender::configureAdaptation(int sampleRate, int channelCount)
{
    audioRenderConfigure->sampleRate = sampleRate;
    audioRenderConfigure->channelCount = channelCount;
    
    audioRenderConfigure->sampleFormat = AV_SAMPLE_FMT_S16;
    
    if (audioRenderConfigure->channelCount==1) {
        audioRenderConfigure->channelLayout = AV_CH_LAYOUT_MONO;
    }else
    {
        audioRenderConfigure->channelCount = 2;
        audioRenderConfigure->channelLayout = AV_CH_LAYOUT_STEREO;
    }
    
    return audioRenderConfigure;
}

void OpenSLESRender::setPCMDataInputer(IPCMDataInputer* inputer)
{
    mPCMDataInputer = inputer;
}

int OpenSLESRender::init(AudioRenderMode mode)
{
    assert(!initialized);
    
    // Set up OpenSl engine.
    OPENSL_RETURN_ON_FAILURE(slCreateEngine(&sles_engine_, 1, kOption, 0, NULL, NULL), -1);
    OPENSL_RETURN_ON_FAILURE((*sles_engine_)->Realize(sles_engine_, SL_BOOLEAN_FALSE), -1);
    OPENSL_RETURN_ON_FAILURE((*sles_engine_)->GetInterface(sles_engine_, SL_IID_ENGINE, &sles_engine_itf_), -1);
    
    // create output mix, with environmental reverb specified as a non-required interface
    const SLInterfaceID ids[1] = { SL_IID_ENVIRONMENTALREVERB };
    const SLboolean req[1] = { SL_BOOLEAN_FALSE };
    
    // Set up OpenSl output mix.
    OPENSL_RETURN_ON_FAILURE((*sles_engine_itf_)->CreateOutputMix(sles_engine_itf_, &sles_output_mixer_, 1, ids, req), -1);
    OPENSL_RETURN_ON_FAILURE((*sles_output_mixer_)->Realize(sles_output_mixer_, SL_BOOLEAN_FALSE), -1);
    
    SLresult result = (*sles_output_mixer_)->GetInterface(sles_output_mixer_, SL_IID_ENVIRONMENTALREVERB, &sles_output_mixer_environmentalReverb_itf_);
    if (SL_RESULT_SUCCESS == result) {
        result = (*sles_output_mixer_environmentalReverb_itf_)->SetEnvironmentalReverbProperties(sles_output_mixer_environmentalReverb_itf_, &reverbSettings);
    }
    
    allocateBuffers();
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mCondition, NULL);
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    currentPts = 0;
    
    active_queue = 0;
    
    initialized = true;
    return 0;
}

int OpenSLESRender::terminate()
{
    if (isPlaying()) {
        stopPlayout();
    }
    
    assert(initialized);
    
    // It is assumed that the caller has stopped recording before terminating.
    assert(!playing);
    (*sles_output_mixer_)->Destroy(sles_output_mixer_);
    (*sles_engine_)->Destroy(sles_engine_);
    
    sles_output_mixer_environmentalReverb_itf_ = NULL;
    sles_engine_itf_ = NULL;
    
    freeBuffers();
    
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mCondition);
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    currentPts = 0;
    
    active_queue = 0;
    
    initialized = false;
    return 0;
}

//at here
int OpenSLESRender::startPlayout()
{
    assert(!playing);
    if (!createAudioPlayer()) {
        return -1;
    }
    
    // Register callback to receive enqueued buffers.
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_player_sbq_itf_)->RegisterCallback(sles_player_sbq_itf_,
                                                                       playerSimpleBufferQueueCallback,
                                                                       this),
                             -1);
    
    if (!enqueueAllBuffers()) {
        return -1;
    }
    
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_player_itf_)->SetPlayState(sles_player_itf_,
                                                               SL_PLAYSTATE_PLAYING),
                             -1);
    
    // default set max volume
    this->setVolume(1.0);
    
    playing = true;
    return 0;
}

int OpenSLESRender::stopPlayout()
{
    assert(playing);
    
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_player_itf_)->SetPlayState(sles_player_itf_,
                                                               SL_PLAYSTATE_STOPPED), -1);
    
    destroyAudioPlayer();
    
    playing = false;
    return 0;
}

void OpenSLESRender::setVolume(float volume)
{
    SLmillibel maxLevel;
    SLresult result;
    if (NULL != sles_player_volume_itf_) {
        result = (*sles_player_volume_itf_)->GetMaxVolumeLevel(sles_player_volume_itf_, &maxLevel);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;
    }

    if (NULL != sles_player_volume_itf_) {
        SLmillibel volume_ = volume * maxLevel;
        result = (*sles_player_volume_itf_)->SetVolumeLevel(sles_player_volume_itf_, volume_);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;
    }
}

void OpenSLESRender::setMute(bool mute)
{
    SLresult result;
    if (NULL != sles_player_volume_itf_) {
        result = (*sles_player_volume_itf_)->SetMute(sles_player_volume_itf_, mute);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;
    }
}

void OpenSLESRender::flush()
{
    pthread_mutex_lock(&mLock);
    cache_len = 0;
    write_pos = 0;
    read_pos = 0;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_broadcast(&mCondition);
}

int64_t OpenSLESRender::getLatency()
{
    return cache_len * 10 * 1000/onebufferSize + openSLESLatency;
}

// -1 : input data invalid
//  0 : push success
//  1 : is full
int OpenSLESRender::pushPCMData(uint8_t* data, int size, int64_t pts)
{
    if (data==NULL || size<=0) return -1;
    
    pthread_mutex_lock(&mLock);
    
    if(cache_len + size>fifoSize)
    {
        pthread_mutex_unlock(&mLock);
        return 1;
    }else
    {
        if(size>fifoSize-write_pos)
        {
            memcpy(fifo+write_pos,data,fifoSize-write_pos);
            memcpy(fifo,data+fifoSize-write_pos,size-(fifoSize-write_pos));
            write_pos = size-(fifoSize-write_pos);
        }else
        {
            memcpy(fifo+write_pos,data,size);
            write_pos = write_pos+size;
        }
        
        //update pts
        currentPts = pts - this->getLatency();
        
        cache_len = cache_len+size;
        
        pthread_mutex_unlock(&mLock);
        
        return 0;
    }
}

void OpenSLESRender::allocateBuffers()
{
	onebufferSize = audioRenderConfigure->sampleRate*audioRenderConfigure->channelCount*av_get_bytes_per_sample(audioRenderConfigure->sampleFormat)/100;
    
    for(int i = 0; i < kNumOpenSlBuffers+kNumPlayBuffers; i++)
    {
        play_buf[i] = (uint8_t *)malloc(onebufferSize);
        memset(play_buf[i],0,onebufferSize);
    }
    
    fifoSize = onebufferSize*kNumFIFOBuffers;
    fifo = (uint8_t *)malloc(fifoSize);
}

void OpenSLESRender::freeBuffers()
{
    for(int i = 0; i < kNumOpenSlBuffers+kNumPlayBuffers; i++)
    {
        if(play_buf[i]!=NULL)
        {
            free(play_buf[i]);
            play_buf[i] = NULL;
        }
    }
    
    if(fifo!=NULL)
    {
        free(fifo);
        fifo = NULL;
    }
}

bool OpenSLESRender::enqueueAllBuffers()
{
    for (int i = 0; i < kNumOpenSlBuffers; ++i) {
        memset(play_buf[i], 0, onebufferSize);
        OPENSL_RETURN_ON_FAILURE((*sles_player_sbq_itf_)->Enqueue(sles_player_sbq_itf_, reinterpret_cast<void*>(play_buf[i]), onebufferSize), false);
    }
    return true;
}

SLDataFormat_PCM OpenSLESRender::createPCMConfiguration(int sampleRate, int channelCount, AVSampleFormat sampleFormat, uint64_t channelLayout)
{
    SLDataFormat_PCM configuration;
    configuration.formatType = SL_DATAFORMAT_PCM;
    configuration.numChannels = channelCount;
    // According to the opensles documentation in the ndk:
    // samplesPerSec is actually in units of milliHz, despite the misleading name.
    // It further recommends using constants. However, this would lead to a lot
    // of boilerplate code so it is not done here.
    configuration.samplesPerSec = sampleRate*1000;
    configuration.bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_16;
    configuration.containerSize = SL_PCMSAMPLEFORMAT_FIXED_16;
    configuration.channelMask = channelLayout;
    configuration.endianness = SL_BYTEORDER_LITTLEENDIAN;
    return configuration;
}

bool OpenSLESRender::createAudioPlayer()
{
    SLDataLocator_AndroidSimpleBufferQueue simple_buf_queue = {
        SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE,
        static_cast<SLuint32>(kNumOpenSlBuffers)
    };
    SLDataFormat_PCM configuration =
    createPCMConfiguration(audioRenderConfigure->sampleRate, audioRenderConfigure->channelCount, audioRenderConfigure->sampleFormat, audioRenderConfigure->channelLayout);
    SLDataSource audio_source = { &simple_buf_queue, &configuration };
    
    SLDataLocator_OutputMix locator_outputmix;
    // Setup the data sink structure.
    locator_outputmix.locatorType = SL_DATALOCATOR_OUTPUTMIX;
    locator_outputmix.outputMix = sles_output_mixer_;
    SLDataSink audio_sink = { &locator_outputmix, NULL };
    
    // Interfaces for streaming audio data, setting volume and Android are needed.
    // Note the interfaces still need to be initialized. This only tells OpenSl
    // that the interfaces will be needed at some point.
    SLInterfaceID ids[kNumInterfaces] = {
        SL_IID_BUFFERQUEUE, SL_IID_VOLUME, SL_IID_ANDROIDCONFIGURATION, SL_IID_EFFECTSEND };
    SLboolean req[kNumInterfaces] = {
        SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE };
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_engine_itf_)->CreateAudioPlayer(sles_engine_itf_, &sles_player_,
                                                                    &audio_source, &audio_sink,
                                                                    kNumInterfaces, ids, req),
                             false);
    
    SLAndroidConfigurationItf player_config;
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_player_)->GetInterface(sles_player_,
                                                           SL_IID_ANDROIDCONFIGURATION,
                                                           &player_config),
                             false);
    
    // Set audio player configuration to SL_ANDROID_STREAM_MEDIA which corresponds
    // to android.media.AudioManager.STREAM_MUSIC.
    SLint32 stream_type = SL_ANDROID_STREAM_MEDIA;
    OPENSL_RETURN_ON_FAILURE(
                             (*player_config)->SetConfiguration(player_config,
                                                                SL_ANDROID_KEY_STREAM_TYPE,
                                                                &stream_type,
                                                                sizeof(SLint32)),
                             false);
    
    // Realize the player in synchronous mode.
    OPENSL_RETURN_ON_FAILURE((*sles_player_)->Realize(sles_player_,
                                                      SL_BOOLEAN_FALSE),
                             false);
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_player_)->GetInterface(sles_player_, SL_IID_PLAY,
                                                           &sles_player_itf_),
                             false);
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_player_)->GetInterface(sles_player_, SL_IID_BUFFERQUEUE,
                                                           &sles_player_sbq_itf_),
                             false);
    
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_player_)->GetInterface(sles_player_,  SL_IID_EFFECTSEND,
                                                           &sles_player_effectSend_itf_),
                             false);
    
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_player_)->GetInterface(sles_player_,  SL_IID_VOLUME,
                                                           &sles_player_volume_itf_),
                             false);
    
    /*
    // Get the audio player's latency
    SLuint32 paramSize = sizeof(SLuint32);
    SLint32 result = 0;
    openSLESLatency = 0;
    result = (*player_config)->GetConfiguration(player_config,
                                               (const SLchar * )"androidGetAudioLatency", &paramSize, &openSLESLatency);
    
    if (result == SL_RESULT_SUCCESS) {
        // The hardware+software audio latency is filled in SLuint32 type of variable audioLatency.
        LOGD("OpenSLES AudioDevice Latency : %d", openSLESLatency);
    } else {
        // Call your current get_audio_latency API. You will only get hardware audio latency value.
        openSLESLatency = 0;
    }
    */
    
    return true;
}

void OpenSLESRender::destroyAudioPlayer()
{
    // Release all buffers currently queued up.
    OPENSL_RETURN_ON_FAILURE(
                             (*sles_player_sbq_itf_)->Clear(sles_player_sbq_itf_),
                             VOID_RETURN);
    
    if (sles_player_) {
        (*sles_player_)->Destroy(sles_player_);
        sles_player_ = NULL;
    }
    
    sles_player_volume_itf_ = NULL;
    sles_player_muteSolo_itf_ = NULL;
    sles_player_effectSend_itf_ = NULL;
    sles_player_sbq_itf_ = NULL;
    sles_player_itf_ = NULL;
}

void OpenSLESRender::playerSimpleBufferQueueCallback(SLAndroidSimpleBufferQueueItf queueItf, void* pContext)
{
    OpenSLESRender* audio_render = reinterpret_cast<OpenSLESRender*>(pContext);
    audio_render->playerSimpleBufferQueueCallbackHandler(queueItf);
}

void OpenSLESRender::playerSimpleBufferQueueCallbackHandler(SLAndroidSimpleBufferQueueItf queueItf)
{
    active_queue = (active_queue + 1) % (kNumOpenSlBuffers+kNumPlayBuffers);
    int next_free_buffer = (active_queue + kNumOpenSlBuffers - 1) % (kNumOpenSlBuffers+kNumPlayBuffers);
    
    uint8_t *playBuffer = play_buf[next_free_buffer];
    
    if (mPCMDataInputer!=NULL) {
        memset(playBuffer, 0, onebufferSize);
        int readSize = mPCMDataInputer->onInputPCMData(&playBuffer, onebufferSize);
        
        pthread_mutex_lock(&mLock);
        currentPts += readSize * 1000 * 1000 / (audioRenderConfigure->sampleRate * audioRenderConfigure->channelCount * av_get_bytes_per_sample(audioRenderConfigure->sampleFormat));
        pthread_mutex_unlock(&mLock);
    }else{
        pthread_mutex_lock(&mLock);
        if (cache_len >= onebufferSize) {
            
            if(onebufferSize>fifoSize-read_pos)
            {
                memcpy(playBuffer,fifo+read_pos,fifoSize-read_pos);
                memcpy(playBuffer+fifoSize-read_pos,fifo,onebufferSize-(fifoSize-read_pos));
                read_pos = onebufferSize-(fifoSize-read_pos);
            }else
            {
                memcpy(playBuffer,fifo+read_pos,onebufferSize);
                read_pos = read_pos + onebufferSize;
            }
            
            cache_len = cache_len - onebufferSize;
            
            //update pts
            currentPts += 10*1000;
        }else{
            memset(playBuffer, 0, onebufferSize);
        }
        
        pthread_mutex_unlock(&mLock);
    }
    
    OPENSL_RETURN_ON_FAILURE(
                             (*queueItf)->Enqueue(queueItf, playBuffer,
                                                  onebufferSize), VOID_RETURN);
}

int64_t OpenSLESRender::getCurrentPts()
{
    int64_t pts = 0;
    pthread_mutex_lock(&mLock);
    pts = currentPts;
    pthread_mutex_unlock(&mLock);
    return pts;
}

