//
//  OpenSLESRender.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__OpenSLESRender__
#define __MediaPlayer__OpenSLESRender__

#include <stdio.h>
#include "AudioRender.h"

#include <pthread.h>

#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#include <SLES/OpenSLES_AndroidConfiguration.h>

class OpenSLESRender : public AudioRender
{
public:
    OpenSLESRender();
    ~OpenSLESRender();
    
    AudioRenderConfigure* configureAdaptation(int sampleRate, int channelCount);
    
    void setPCMDataInputer(IPCMDataInputer* inputer);

    int init(AudioRenderMode mode);
    int terminate();
    bool isInitialized() { return initialized; }
    
    int startPlayout();
    int stopPlayout();
    bool isPlaying() { return playing; }
    
    void setVolume(float volume);
    void setMute(bool mute);
    
    // -1 : input data invalid
    //  0 : push success
    //  1 : is full
    int pushPCMData(uint8_t* data, int size, int64_t pts);
    
    void flush();
    
    int64_t getCurrentPts();
private:
    // 1 buffer = 10ms
    enum {
        kNumInterfaces = 4,
        kNumOpenSlBuffers = 2,
        kNumPlayBuffers = 3,
        
        kNumFIFOBuffers = 16,
//        kNumFIFOBuffers = 10,
    };
    
    void allocateBuffers();
    bool enqueueAllBuffers();
    void freeBuffers();
    
    int onebufferSize;
    uint8_t *play_buf[kNumOpenSlBuffers+kNumPlayBuffers];
    int active_queue;
    
    SLDataFormat_PCM createPCMConfiguration(int sampleRate, int channelCount, AVSampleFormat sampleFormat, uint64_t channelLayout);
    
    bool createAudioPlayer();
    void destroyAudioPlayer();
    
    static void playerSimpleBufferQueueCallback(SLAndroidSimpleBufferQueueItf queueItf, void* pContext);
    void playerSimpleBufferQueueCallbackHandler(SLAndroidSimpleBufferQueueItf queueItf);
    
    //us
    int64_t getLatency();
    
    pthread_mutex_t mLock;
    pthread_cond_t mCondition;

    uint8_t *fifo;
    int fifoSize;
    
    int write_pos;
    int read_pos;
    int cache_len;

    bool initialized;
    bool playing;
    
    AudioRenderConfigure* audioRenderConfigure;
    
    int64_t currentPts;
    
    // OpenSLES handles
    SLObjectItf sles_engine_;
    SLEngineItf sles_engine_itf_;
    SLObjectItf sles_player_;
    SLPlayItf sles_player_itf_;
    SLAndroidSimpleBufferQueueItf sles_player_sbq_itf_;
    SLObjectItf sles_output_mixer_;
    
    SLEnvironmentalReverbItf sles_output_mixer_environmentalReverb_itf_;
    SLEffectSendItf sles_player_effectSend_itf_;
    SLMuteSoloItf sles_player_muteSolo_itf_;
    SLVolumeItf sles_player_volume_itf_;
    
    SLuint32 openSLESLatency;
private:
    IPCMDataInputer* mPCMDataInputer;
};

#endif /* defined(__MediaPlayer__OpenSLESRender__) */
