//
//  JniAudioTrackRender.h
//  AndroidMediaPlayer
//
//  Created by slklovewyy on 2018/12/10.
//  Copyright © 2018年 Cell. All rights reserved.
//

#ifndef JniAudioTrackRender_h
#define JniAudioTrackRender_h

#include <stdio.h>
#include <pthread.h>
#include "AudioRender.h"

class JniAudioTrackRender : public AudioRender{
public:
    JniAudioTrackRender(JavaVM *jvm);
    ~JniAudioTrackRender();
    
    AudioRenderConfigure* configureAdaptation(int sampleRate, int channelCount);
    
    void setPCMDataInputer(IPCMDataInputer* inputer);

    int init(AudioRenderMode mode);
    int terminate();
    bool isInitialized() { return initialized; }

    int startPlayout();
    int stopPlayout();
    bool isPlaying() { return playing; }

    //0-1
    void setVolume(float volume);
    void setMute(bool mute);
    
    // -1 : input data invalid
    //  0 : push success
    //  1 : is full
    int pushPCMData(uint8_t* data, int size, int64_t pts);
    
    void flush();
    
    int64_t getCurrentPts();
    
    void OnCacheDirectBufferAddress(jobject byte_buffer);
    void OnGetPlayoutData(int bytes);
    
    void setAudioTrackLatency(int64_t latency);
private:
    enum {
//        kNumFIFOBuffers = 16,
        kNumFIFOBuffers = 16,
    };
    
    JavaVM *mJvm;
    JNIEnv *mEnv;
    
    bool initialized;
    bool playing;
    
    AudioRenderConfigure* audioRenderConfigure;
    
    jobject audioRenderJavaObject;
    void* direct_buffer_address;

private:
    int64_t getLatency();
    int64_t audioTrackLatency;
    
    //fifo
    uint8_t *fifo;
    int fifoSize;
    
    void allocBuffers();
    void freeBuffers();
    void flushBuffers();
    
    pthread_mutex_t mLock;
    pthread_cond_t mCondition;
    bool isWaitting;
    
    int write_pos;
    int read_pos;
    int cache_len;
    
    int64_t currentPts;
private:
    bool isMute;
    pthread_mutex_t mMuteLock;
private:
    IPCMDataInputer* mPCMDataInputer;
};

#endif /* JniAudioTrackRender_h */
