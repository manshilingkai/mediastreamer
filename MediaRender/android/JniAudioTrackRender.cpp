//
//  JniAudioTrackRender.cpp
//  AndroidMediaPlayer
//
//  Created by slklovewyy on 2018/12/10.
//  Copyright © 2018年 Cell. All rights reserved.
//

#include <assert.h>
#include "JniAudioTrackRender.h"
#include "MediaLog.h"
#include "AndroidUtils.h"

#include "android_slkmedia_mediastreamer.h"

JniAudioTrackRender::JniAudioTrackRender(JavaVM *jvm)
{
    mJvm = jvm;
    mEnv = AndroidUtils::getJNIEnv(mJvm);

    initialized = false;
    playing = false;
    
    audioRenderConfigure = new AudioRenderConfigure;
    audioRenderJavaObject = NULL;
    direct_buffer_address = NULL;
    
    isMute = false;
    pthread_mutex_init(&mMuteLock, NULL);
    
    audioTrackLatency = 0;
    
    mPCMDataInputer = NULL;
}

JniAudioTrackRender::~JniAudioTrackRender()
{
    if (initialized) {
        terminate();
    }
    
    if(audioRenderConfigure!=NULL)
    {
        delete audioRenderConfigure;
        audioRenderConfigure = NULL;
    }
    
    pthread_mutex_destroy(&mMuteLock);
}

AudioRenderConfigure* JniAudioTrackRender::configureAdaptation(int sampleRate, int channelCount)
{
    audioRenderConfigure->sampleRate = sampleRate;
    audioRenderConfigure->channelCount = channelCount;
    
    audioRenderConfigure->sampleFormat = AV_SAMPLE_FMT_S16;
    
    if (audioRenderConfigure->channelCount==1) {
        audioRenderConfigure->channelLayout = AV_CH_LAYOUT_MONO;
    }else
    {
        audioRenderConfigure->channelCount = 2;
        audioRenderConfigure->channelLayout = AV_CH_LAYOUT_STEREO;
    }
    
    return audioRenderConfigure;
}

void JniAudioTrackRender::setPCMDataInputer(IPCMDataInputer* inputer)
{
    mPCMDataInputer = inputer;
}

int JniAudioTrackRender::init(AudioRenderMode mode)
{
    assert(!initialized);
    
    mEnv = AndroidUtils::getJNIEnv(mJvm);

    jmethodID audio_render_init_method = mEnv->GetMethodID(g_audio_render_class_for_mediaeditengine, "<init>", "(J)V");
    if (mEnv->ExceptionOccurred()) {
        
        LOGE("Fail to GetMethodID for class [android/slkmedia/mediaeditengine/audiorender/AudioTrackRender] and method [<init>]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    jobject audioRenderObject = mEnv->NewObject(g_audio_render_class_for_mediaeditengine, audio_render_init_method, (jlong)this);
    if (mEnv->ExceptionOccurred()) {
        
        LOGE("Fail to NewObject for Class [android/slkmedia/mediaeditengine/audiorender/AudioTrackRender]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    this->audioRenderJavaObject = mEnv->NewGlobalRef(audioRenderObject);
    mEnv->DeleteLocalRef(audioRenderObject);
    
    jmethodID audio_render_initPlayout_method = mEnv->GetMethodID(g_audio_render_class_for_mediaeditengine, "initPlayout", "(II)Z");
    if (mEnv->ExceptionOccurred()) {
        mEnv->DeleteGlobalRef(this->audioRenderJavaObject);
        this->audioRenderJavaObject = NULL;
        
        LOGE("Fail to GetMethodID for class [android/slkmedia/mediaeditengine/audiorender/AudioTrackRender] and method [initPlayout]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    jboolean ret = mEnv->CallBooleanMethod(this->audioRenderJavaObject, audio_render_initPlayout_method, audioRenderConfigure->sampleRate, audioRenderConfigure->channelCount);
    if (mEnv->ExceptionOccurred()) {
        mEnv->DeleteGlobalRef(this->audioRenderJavaObject);
        this->audioRenderJavaObject = NULL;

        LOGE("Fail to CallBooleanMethod for class [android/slkmedia/mediaeditengine/audiorender/AudioTrackRender] and method [initPlayout]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    if(ret!=JNI_TRUE)
    {
        mEnv->DeleteGlobalRef(this->audioRenderJavaObject);
        this->audioRenderJavaObject = NULL;

        LOGE("initPlayout failed!");
        return -1;
    }
    
    pthread_mutex_init(&mLock, NULL);
    pthread_cond_init(&mCondition, NULL);
    isWaitting = false;
    
    allocBuffers();
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    currentPts = 0;
    
    initialized = true;
    return 0;
}

int JniAudioTrackRender::startPlayout()
{
    assert(!playing);
    
    mEnv = AndroidUtils::getJNIEnv(mJvm);

    jmethodID audio_render_startPlayout_method = mEnv->GetMethodID(g_audio_render_class_for_mediaeditengine, "startPlayout", "()Z");
    if (mEnv->ExceptionOccurred()) {
        LOGE("Fail to GetMethodID for class [android/slkmedia/mediaeditengine/audiorender/AudioTrackRender] and method [startPlayout]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    jboolean ret = mEnv->CallBooleanMethod(this->audioRenderJavaObject, audio_render_startPlayout_method);
    if (mEnv->ExceptionOccurred()) {
        LOGE("Fail to CallBooleanMethod for class [android/slkmedia/mediaeditengine/audiorender/AudioTrackRender] and method [startPlayout]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    if (ret!=JNI_TRUE) {
        LOGE("startPlayout failed!");
        
        return -1;
    }
    
    playing = true;
    
    return 0;
}

int JniAudioTrackRender::stopPlayout()
{
    assert(playing);
    
    mEnv = AndroidUtils::getJNIEnv(mJvm);

    jmethodID audio_render_stopPlayout_method = mEnv->GetMethodID(g_audio_render_class_for_mediaeditengine, "stopPlayout", "()Z");
    if (mEnv->ExceptionOccurred()) {
        LOGE("Fail to GetMethodID for class [android/slkmedia/mediaeditengine/audiorender/AudioTrackRender] and method [stopPlayout]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    jboolean ret = mEnv->CallBooleanMethod(this->audioRenderJavaObject, audio_render_stopPlayout_method);
    if (mEnv->ExceptionOccurred()) {
        LOGE("Fail to CallBooleanMethod for class [android/slkmedia/mediaeditengine/audiorender/AudioTrackRender] and method [stopPlayout]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    if (ret!=JNI_TRUE) {
        LOGE("stopPlayout failed!");
        
        return -1;
    }
    
    playing = false;
    
    return 0;
}

int JniAudioTrackRender::terminate()
{
    if (isPlaying()) {
        stopPlayout();
    }
    
    assert(initialized);
    
    assert(!playing);

    mEnv = AndroidUtils::getJNIEnv(mJvm);

    jmethodID audio_render_terminatePlayout_method = mEnv->GetMethodID(g_audio_render_class_for_mediaeditengine, "terminatePlayout", "()V");
    if (mEnv->ExceptionOccurred()) {
        
        LOGE("Fail to GetMethodID for class [android/slkmedia/mediaeditengine/audiorender/AudioTrackRender] and method [terminatePlayout]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    mEnv->CallVoidMethod(this->audioRenderJavaObject,audio_render_terminatePlayout_method);
    if (mEnv->ExceptionOccurred()) {
        LOGE("Fail to CallVoidMethod for class [android/slkmedia/mediaeditengine/audiorender/AudioTrackRender] and method [terminatePlayout]");
        mEnv->ExceptionClear();
        
        return -1;
    }
    
    if (this->audioRenderJavaObject) {
        mEnv->DeleteGlobalRef(this->audioRenderJavaObject);
        this->audioRenderJavaObject = NULL;
    }

    freeBuffers();
    
    write_pos = 0;
    read_pos = 0;
    cache_len = 0;
    
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mCondition);
    isWaitting = false;
    
    currentPts = 0;
    
    initialized = false;
    return 0;
}

void JniAudioTrackRender::flush()
{
    pthread_mutex_lock(&mLock);
    cache_len = 0;
    write_pos = 0;
    read_pos = 0;
    pthread_mutex_unlock(&mLock);
    
    pthread_cond_broadcast(&mCondition);
}

int64_t JniAudioTrackRender::getCurrentPts()
{
    int64_t pts = 0;
    pthread_mutex_lock(&mLock);
    pts = currentPts;
    pthread_mutex_unlock(&mLock);
    return pts;
}

void JniAudioTrackRender::setVolume(float volume)
{
    //todo
}

void JniAudioTrackRender::setMute(bool mute)
{
    pthread_mutex_lock(&mMuteLock);
    isMute = mute;
    pthread_mutex_unlock(&mMuteLock);
}

void JniAudioTrackRender::OnCacheDirectBufferAddress(jobject byte_buffer)
{
    mEnv = AndroidUtils::getJNIEnv(mJvm);
    direct_buffer_address = mEnv->GetDirectBufferAddress(byte_buffer);
    jlong capacity = mEnv->GetDirectBufferCapacity(byte_buffer);
    LOGD("direct buffer capacity: %lld", capacity);
}

void JniAudioTrackRender::setAudioTrackLatency(int64_t latency)
{
    audioTrackLatency = latency;
}

void JniAudioTrackRender::OnGetPlayoutData(int bytes)
{
    uint8_t* playBuffer = static_cast<uint8_t*>(direct_buffer_address);
    int onebufferSize = bytes;
    
    if (mPCMDataInputer!=NULL) {
        memset(playBuffer, 0, onebufferSize);
        int readSize = mPCMDataInputer->onInputPCMData(&playBuffer, onebufferSize);
        
        pthread_mutex_lock(&mLock);
        currentPts += readSize * 1000 * 1000 / (audioRenderConfigure->sampleRate * audioRenderConfigure->channelCount * av_get_bytes_per_sample(audioRenderConfigure->sampleFormat));
        pthread_mutex_unlock(&mLock);
    }else{
        pthread_mutex_lock(&mLock);
        if (cache_len >= onebufferSize) {
            
            if(onebufferSize>fifoSize-read_pos)
            {
                memcpy(playBuffer,fifo+read_pos,fifoSize-read_pos);
                memcpy(playBuffer+fifoSize-read_pos,fifo,onebufferSize-(fifoSize-read_pos));
                read_pos = onebufferSize-(fifoSize-read_pos);
            }else
            {
                memcpy(playBuffer,fifo+read_pos,onebufferSize);
                read_pos = read_pos + onebufferSize;
            }
            
            cache_len = cache_len - onebufferSize;
            
            //update pts
            currentPts += static_cast<int64_t>(onebufferSize * 10 * 1000 * kNumFIFOBuffers / fifoSize);
            
        }else{
            memset(playBuffer, 0, onebufferSize);
        }
        pthread_mutex_unlock(&mLock);
    }
    
    pthread_mutex_lock(&mMuteLock);
    if (isMute) {
        memset(playBuffer, 0, onebufferSize);
    }
    pthread_mutex_unlock(&mMuteLock);
}

int JniAudioTrackRender::pushPCMData(uint8_t* data, int size, int64_t pts)
{
    if (data==NULL || size<=0) return -1;
    
    pthread_mutex_lock(&mLock);
    
    if(cache_len + size>fifoSize)
    {
        pthread_mutex_unlock(&mLock);
        return 1;
    }else
    {
        if(size>fifoSize-write_pos)
        {
            memcpy(fifo+write_pos,data,fifoSize-write_pos);
            memcpy(fifo,data+fifoSize-write_pos,size-(fifoSize-write_pos));
            write_pos = size-(fifoSize-write_pos);
        }else
        {
            memcpy(fifo+write_pos,data,size);
            write_pos = write_pos+size;
        }
        
        //update pts
        currentPts = pts - this->getLatency();
        
        cache_len = cache_len+size;

        pthread_mutex_unlock(&mLock);
        
        return 0;
    }
}

int64_t JniAudioTrackRender::getLatency()
{
    return static_cast<int64_t>(cache_len * 10 * 1000 * kNumFIFOBuffers / fifoSize) + audioTrackLatency;
}

void JniAudioTrackRender::allocBuffers()
{
    fifoSize = audioRenderConfigure->sampleRate * audioRenderConfigure->channelCount * av_get_bytes_per_sample(audioRenderConfigure->sampleFormat) / 100 * kNumFIFOBuffers;
    
    fifo = (uint8_t *)malloc(fifoSize);
}

void JniAudioTrackRender::freeBuffers()
{
    if (fifo!=NULL) {
        free(fifo);
        fifo = NULL;
    }
}
