//
//  IPCMDataInputer.h
//  MediaStreamer
//
//  Created by Think on 2019/7/10.
//  Copyright © 2019年 Cell. All rights reserved.
//

#ifndef IPCMDataInputer_h
#define IPCMDataInputer_h

#include <stdio.h>
#include <stdint.h>

class IPCMDataInputer {
public:
    virtual ~IPCMDataInputer() {}
    virtual int onInputPCMData(uint8_t **pData, int size) = 0;
};

#endif /* IPCMDataInputer_h */
