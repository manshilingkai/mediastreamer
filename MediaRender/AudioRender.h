//
//  AudioRender.h
//  MediaPlayer
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef __MediaPlayer__AudioRender__
#define __MediaPlayer__AudioRender__

#include <stdio.h>

#ifdef ANDROID
#include "jni.h"
#endif

extern "C" {
#include "libavformat/avformat.h"
#include "libavutil/channel_layout.h"
}

#include "IPCMDataInputer.h"

#ifdef ANDROID
#define KDefaultSamplesPerFrame     512
#else
#define KDefaultSamplesPerFrame     1024
#endif

struct AudioRenderConfigure
{
    int channelCount;
    AVSampleFormat sampleFormat;
    int sampleRate;
    uint64_t channelLayout;
    
    int samplesPerFrame;
    
    AudioRenderConfigure()
    {
        channelCount = 2;
        sampleFormat = AV_SAMPLE_FMT_S16;
        sampleRate = 44100;
        channelLayout = AV_CH_LAYOUT_STEREO;
        
        samplesPerFrame = KDefaultSamplesPerFrame;
    }
};

enum AudioRenderType
{
    AUDIO_RENDER_UNKNOWN = -1,
    AUDIO_RENDER_AUDIOUNIT = 0,
    AUDIO_RENDER_AUDIOQUEUE = 1,
    AUDIO_RENDER_OPENAL = 2,
    
    AUDIO_RENDER_OPENSLES = 3,
    AUDIO_RENDER_AUDIOTRACK = 4,
    
    AUDIO_RENDER_SDL = 5,
    AUDIO_RENDER_MAC = 6,

	AUDIO_RENDER_WIN_WAVE = 7,
};

enum AudioRenderMode
{
    PLAYER_MODE = 0,
    VOICE_MODE = 1,
};

class AudioRender
{
public:
    virtual ~AudioRender() {}
    
    static AudioRender* CreateAudioRender(AudioRenderType type);
#ifdef ANDROID
    static AudioRender* CreateAudioRenderWithJniEnv(AudioRenderType type, JavaVM *jvm);
#endif
    static void DeleteAudioRender(AudioRender* audioRender, AudioRenderType type);
    
    virtual AudioRenderConfigure* configureAdaptation(int sampleRate, int channelCount) = 0;
    
    virtual void setPCMDataInputer(IPCMDataInputer* inputer) = 0;
    
    virtual int init(AudioRenderMode mode) = 0;
    virtual int terminate() = 0;
    virtual bool isInitialized() = 0;
    
    virtual int startPlayout() = 0;
    virtual int stopPlayout() = 0;
    virtual bool isPlaying() = 0;
#ifdef IOS
    virtual void iOSAVAudioSessionInterruption(bool isInterrupting) = 0;
    virtual void pause() = 0;
    virtual void resume() = 0;
#endif
    //0-1
    virtual void setVolume(float volume) = 0;
    virtual void setMute(bool mute) = 0;
    
    virtual int pushPCMData(uint8_t* data, int size, int64_t pts) = 0;
    
    virtual void flush() = 0;
    
    virtual int64_t getCurrentPts() = 0;
};

#endif /* defined(__MediaPlayer__AudioRender__) */
