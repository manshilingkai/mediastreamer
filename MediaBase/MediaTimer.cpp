//
//  MediaTimer.cpp
//  MediaStreamer
//
//  Created by Think on 2016/11/16.
//  Copyright © 2016年 Cell. All rights reserved.
//

#include "MediaTimer.h"

int64_t MediaTimer::onLineTimeDuration = 0ll;
int64_t MediaTimer::onLineLastTimePoint = 0ll;
pthread_mutex_t MediaTimer::mLock;
bool MediaTimer::isOnLine = false;

void MediaTimer::onLine()
{
    pthread_mutex_lock(&MediaTimer::mLock);
    
    MediaTimer::isOnLine = true;
    MediaTimer::onLineLastTimePoint = GetNowMs();
    
    pthread_mutex_unlock(&MediaTimer::mLock);
}

void MediaTimer::offLine()
{
    pthread_mutex_lock(&MediaTimer::mLock);

    MediaTimer::isOnLine = false;
    
    pthread_mutex_unlock(&MediaTimer::mLock);
}

int64_t MediaTimer::GetNowMediaTimeMS()
{
    int64_t nowMediaTimeMS = 0;
    
    pthread_mutex_lock(&MediaTimer::mLock);

    if (MediaTimer::isOnLine) {
        MediaTimer::onLineTimeDuration += GetNowMs() - MediaTimer::onLineLastTimePoint;
        MediaTimer::onLineLastTimePoint = GetNowMs();
    }
    
    nowMediaTimeMS = MediaTimer::onLineTimeDuration;
    
    pthread_mutex_unlock(&MediaTimer::mLock);
    
    return nowMediaTimeMS;
}

void MediaTimer::open()
{
    MediaTimer::onLineTimeDuration = 0;
    MediaTimer::onLineLastTimePoint = 0;
    MediaTimer::isOnLine = false;
    
    pthread_mutex_init(&MediaTimer::mLock, NULL);

}

void MediaTimer::close()
{
    pthread_mutex_destroy(&MediaTimer::mLock);
    
    MediaTimer::onLineTimeDuration = 0;
    MediaTimer::onLineLastTimePoint = 0;
    MediaTimer::isOnLine = false;
}
