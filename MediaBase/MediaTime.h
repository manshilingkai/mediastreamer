//
//  MediaTime.h
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MEDIA_TIME_H_
#define MEDIA_TIME_H_

#include <stdint.h>
#include <stdio.h>

#ifdef ANDROID
int64_t elapsedRealtimeNano();
int64_t elapsedRealtimeMs();
#endif

int64_t GetNowMs();
int64_t GetNowUs();
int64_t systemTimeNs();

void sprintfTime1(char *buffer, int bufferSize);
void sprintfTime2(char *buffer, int bufferSize);

#endif // MEDIA_TIME_H_
