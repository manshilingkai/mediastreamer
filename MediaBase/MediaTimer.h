//
//  MediaTimer.h
//  MediaStreamer
//
//  Created by Think on 2016/11/16.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef MediaTimer_h
#define MediaTimer_h

#include <stdio.h>

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

#include "MediaTime.h"

class MediaTimer {
public:
    static void open();
    static void close();
    
    static void onLine();
    static void offLine();
    
    static int64_t GetNowMediaTimeMS();
private:
    static int64_t onLineTimeDuration;
    static int64_t onLineLastTimePoint;
    static pthread_mutex_t mLock;
    static bool isOnLine;
};
    
#endif /* MediaTimer_h */
