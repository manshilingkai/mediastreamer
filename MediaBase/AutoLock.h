//
//  AutoLock.h
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifndef AUTO_LOCK_H
#define AUTO_LOCK_H

#ifdef WIN32
#include "w32pthreads.h"
#else
#include <pthread.h>
#endif

class AutoLock {
public:
	AutoLock(pthread_mutex_t* lock) : pLock(lock) {
		pthread_mutex_lock(pLock);
	}
	~AutoLock() {
		pthread_mutex_unlock(pLock);
	}
private:
	pthread_mutex_t* pLock;
};
#endif
