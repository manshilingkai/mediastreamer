//
//  MediaTime.cpp
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#ifdef WIN32
#include "w32common.h"
#else
#include <sys/time.h>
#endif

#include <string.h>

#include "MediaTime.h"

#include <time.h>

#ifdef ANDROID
int64_t elapsedRealtimeNano()
{
    struct timespec ts;
    int result;
    int64_t timestamp;
    
    ts.tv_sec = ts.tv_nsec = 0;
    result = clock_gettime(CLOCK_BOOTTIME, &ts);
    if (result == 0) {
        timestamp = ts.tv_sec*1000000000LL + ts.tv_nsec;
        return timestamp;
    }
    
    return 0;
}

int64_t elapsedRealtimeMs()
{
    return elapsedRealtimeNano()/1000000ll;
}
#endif

int64_t systemTimeNs() {
    struct timeval t;
    t.tv_sec = t.tv_usec = 0;
    gettimeofday(&t, NULL);
    return t.tv_sec * 1000000000LL + t.tv_usec * 1000LL;
}

int64_t GetNowMs()
{
    return systemTimeNs() / 1000000ll;
}

int64_t GetNowUs() {
    return systemTimeNs() / 1000ll;
}

void sprintfTime1(char *buffer, int bufferSize)
{
    time_t timep;
    time(&timep);
    strlcpy(buffer, ctime(&timep), bufferSize);
}

void sprintfTime2(char *buffer, int bufferSize)
{
    struct timeval tv;
    tv.tv_sec = tv.tv_usec = 0;
    gettimeofday(&tv, NULL);
    
#ifdef WIN32
	time_t t = (time_t)tv.tv_sec;
	struct tm* p_time = localtime(&t);
#else
	struct tm* p_time = localtime(&tv.tv_sec);
#endif
	strftime(buffer, bufferSize, "%Y-%m-%d %H:%M:%S", p_time);
}
