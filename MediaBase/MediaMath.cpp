//
//  MediaMath.cpp
//  MediaStreamer
//
//  Created by Think on 2017/3/23.
//  Copyright © 2017年 Cell. All rights reserved.
//

#include "MediaMath.h"
#include <math.h>

double MediaMath::PI = 3.141592653589793;

double MediaMath::Sqrt(double d)
{
    return sqrt(d);
}

double MediaMath::Sin(double d)
{
    return sin(d);
}

double MediaMath::Cos(double d)
{
    return cos(d);
}
