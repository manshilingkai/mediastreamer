//
//  TimedEventQueue.cpp
//
//  Created by Think on 16/2/14.
//  Copyright © 2016年 Cell. All rights reserved.
//

#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>

#include "MediaLog.h"

#include "TimedEventQueue.h"

#include "MediaTime.h"
#include "AutoLock.h"

#include <sys/types.h>
#include <errno.h>

#ifndef WIN32
#include <sys/time.h>
#include <sys/resource.h>
#endif

TimedEventQueue::TimedEventQueue() :
		mNextEventID(1), mRunning(false), mStopped(false) {
	pthread_mutex_init(&mLock, NULL);
	pthread_cond_init(&mQueueNotEmptyCondition, NULL);
	pthread_cond_init(&mQueueHeadChangedCondition, NULL);
}

TimedEventQueue::~TimedEventQueue() {
	stop();
	pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mQueueNotEmptyCondition);
    pthread_cond_destroy(&mQueueHeadChangedCondition);
}

void TimedEventQueue::start() {
	if (mRunning) {
		return;
	}

	mStopped = false;

#ifdef WIN32
	pthread_create(&mThread, NULL, ThreadWrapper, this);
#else
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_create(&mThread, &attr, ThreadWrapper, this);
	pthread_attr_destroy(&attr);
#endif

	mRunning = true;
}

void TimedEventQueue::stop(bool flush) {
	if (!mRunning) {
		return;
	}

	if (flush) {
		postEventToBack(new StopEvent);
	} else {
		postTimedEvent(new StopEvent, INT64_MIN);
	}

	void *dummy;
	pthread_join(mThread, &dummy);

	mQueue.clear();

	mRunning = false;
}

TimedEventQueue::event_id TimedEventQueue::postEvent(Event *event) {
	// Reserve an earlier timeslot an INT64_MIN to be able to post
	// the StopEvent to the absolute head of the queue.
	return postTimedEvent(event, INT64_MIN + 1);
}

TimedEventQueue::event_id TimedEventQueue::postEventToBack(Event *event) {
	return postTimedEvent(event, INT64_MAX);
}

TimedEventQueue::event_id TimedEventQueue::postEventWithDelay(Event *event,
		int64_t delay_us) {
	if (delay_us < 0) {
		delay_us = 0;
	}

	return postTimedEvent(event, GetNowUs() + delay_us);
}

TimedEventQueue::event_id TimedEventQueue::postTimedEvent(
		Event *event, int64_t realtime_us) {

	AutoLock autoLock(&mLock);

	event->setEventID(mNextEventID++);

#ifdef WIN32
	list<QueueItem>::iterator it = mQueue.begin();
#else
	List<QueueItem>::iterator it = mQueue.begin();
#endif

	while (it != mQueue.end() && realtime_us >= (*it).realtime_us) {
		++it;
	}

	QueueItem item;
	item.event = event;
	item.realtime_us = realtime_us;

	if (it == mQueue.begin()) {
		pthread_cond_signal(&mQueueHeadChangedCondition);
	}

	mQueue.insert(it, item);

	pthread_cond_signal(&mQueueNotEmptyCondition);

	return event->eventID();
}

static bool MatchesEventID(void *cookie,
		TimedEventQueue::Event *event) {
	TimedEventQueue::event_id *id =
			static_cast<TimedEventQueue::event_id *>(cookie);

	if (event->eventID() != *id) {
		return false;
	}

	*id = 0;

	return true;
}

bool TimedEventQueue::cancelEvent(event_id id) {
	if (id == 0) {
		return false;
	}

	cancelEvents(&MatchesEventID, &id, true /* stopAfterFirstMatch */);

	// if MatchesEventID found a match, it will have set id to 0
	// (which is not a valid event_id).

	return id == 0;
}

void TimedEventQueue::cancelEvents(
		bool (*predicate)(void *cookie, Event *event), void *cookie,
		bool stopAfterFirstMatch) {
	AutoLock autoLock(&mLock);

#ifdef WIN32
	list<QueueItem>::iterator it = mQueue.begin();
#else
	List<QueueItem>::iterator it = mQueue.begin();
#endif

	while (it != mQueue.end()) {
		if (!(*predicate)(cookie, (*it).event)) {
			++it;
			continue;
		}

		if (it == mQueue.begin()) {
			pthread_cond_signal(&mQueueHeadChangedCondition);
		}

		LOGV("cancelling event %d", (*it).event->eventID());

		(*it).event->setEventID(0);
		it = mQueue.erase(it);

		if (stopAfterFirstMatch) {
			return;
		}
	}
}

// static
void *TimedEventQueue::ThreadWrapper(void *me) {

#ifdef ANDROID
    LOGI("getpriority before:%d", getpriority(PRIO_PROCESS, 0));
    int threadPriority = -6;
    if(setpriority(PRIO_PROCESS, 0, threadPriority) != 0)
    {
        LOGE("%s","set thread priority failed");
    }
    LOGI("getpriority after:%d", getpriority(PRIO_PROCESS, 0));
#endif
    
	static_cast<TimedEventQueue *>(me)->threadEntry();
    
	return NULL;
}

void TimedEventQueue::threadEntry() {
#ifdef ANDROID
    JNIEnv *env = NULL;
    if (mJvm!=NULL) {
        if(mJvm->AttachCurrentThread(&env, NULL)!=JNI_OK)
        {
            LOGE("AttachCurrentThread() failed");
            return;
        }
    }
#endif
    
	for (;;) {
		int64_t now_us = 0;
		Event *event;

		{
			AutoLock autoLock(&mLock);

			if (mStopped) {
				break;
			}

			while (mQueue.empty()) {
				pthread_cond_wait(&mQueueNotEmptyCondition, &mLock);
			}

			event_id eventID = 0;
			for (;;) {
                
				if (mQueue.empty()) {
					// The only event in the queue could have been cancelled
					// while we were waiting for its scheduled time.
					break;
				}

#ifdef WIN32
				list<QueueItem>::iterator it = mQueue.begin();
#else
				List<QueueItem>::iterator it = mQueue.begin();
#endif

				eventID = (*it).event->eventID();

				now_us = GetNowUs();
				int64_t when_us = (*it).realtime_us;

				int64_t delay_us;
				if (when_us < 0 || when_us == INT64_MAX) {
					delay_us = 0;
				} else {
					delay_us = when_us - now_us;
				}

				if (delay_us <= 0) {
					break;
				}

				static int64_t kMaxTimeoutUs = 10000000ll;  // 10 secs
				bool timeoutCapped = false;
				if (delay_us > kMaxTimeoutUs) {
					LOGW("delay_us exceeds max timeout: %lld us", delay_us);

					// We'll never block for more than 10 secs, instead
					// we will split up the full timeout into chunks of
					// 10 secs at a time. This will also avoid overflow
					// when converting from us to ns.
					delay_us = kMaxTimeoutUs;
					timeoutCapped = true;
				}

                int64_t reltime = delay_us * 1000ll;
                struct timespec ts;

#if defined(__ANDROID__) && (__ANDROID_API__ >= 21) && !defined(HAVE_PTHREAD_COND_TIMEDWAIT_RELATIVE)
                struct timeval t;
                t.tv_sec = t.tv_usec = 0;
                gettimeofday(&t, NULL);
                ts.tv_sec = t.tv_sec;
                ts.tv_nsec = t.tv_usec * 1000;
                ts.tv_sec += reltime/1000000000;
                ts.tv_nsec += reltime%1000000000;
                ts.tv_sec += ts.tv_nsec / 1000000000;
                ts.tv_nsec = ts.tv_nsec % 1000000000;
                status_t err = -pthread_cond_timedwait(&mQueueHeadChangedCondition, &mLock, &ts);
#else
                ts.tv_sec  = reltime/1000000000;
                ts.tv_nsec = reltime%1000000000;
                status_t err = -pthread_cond_timedwait_relative_np(&mQueueHeadChangedCondition, &mLock, &ts);
#endif
                
				if (!timeoutCapped && err == -ETIMEDOUT) {
					// We finally hit the time this event is supposed to
					// trigger.
					now_us = GetNowUs();
					break;
				}
			}

			// The event w/ this id may have been cancelled while we're
			// waiting for its trigger-time, in that case
			// removeEventFromQueue_l will return NULL.
			// Otherwise, the QueueItem will be removed
			// from the queue and the referenced event returned.
			event = removeEventFromQueue_l(eventID);
		}

		if (event != NULL) {
			// Fire event with the lock NOT held.
			event->fire(this, now_us);
		}
	}
    
#ifdef ANDROID
    if (mJvm!=NULL) {
        
        if(mJvm->DetachCurrentThread() != JNI_OK)
        {
            LOGE("%s: DetachCurrentThread() failed", __FUNCTION__);
        }
    }
#endif
}

TimedEventQueue::Event* TimedEventQueue::removeEventFromQueue_l(
		event_id id) {
#ifdef WIN32
	for (list<QueueItem>::iterator it = mQueue.begin(); it != mQueue.end(); ++it)
#else
	for (List<QueueItem>::iterator it = mQueue.begin(); it != mQueue.end(); ++it)
#endif
	{
		if ((*it).event->eventID() == id) {
			Event *event = (*it).event;
			event->setEventID(0);

			mQueue.erase(it);

			return event;
		}
	}

	LOGW("Event %d was not found in the queue, already cancelled?", id);

	return NULL;
}

#ifdef ANDROID
void TimedEventQueue::registerJavaVMEnv(JavaVM *jvm)
{
    mJvm = jvm;
}
#endif
