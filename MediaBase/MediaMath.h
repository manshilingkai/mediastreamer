//
//  MediaMath.h
//  MediaStreamer
//
//  Created by Think on 2017/3/23.
//  Copyright © 2017年 Cell. All rights reserved.
//

#ifndef MediaMath_h
#define MediaMath_h

#include <stdio.h>

/* __MAX and __MIN: self explanatory */
#ifndef __MAX
#define __MAX(a, b)   ( ((a) > (b)) ? (a) : (b) )
#endif

#ifndef __MIN
#define __MIN(a, b)   ( ((a) < (b)) ? (a) : (b) )
#endif

class MediaMath {
public:
    static double PI;
    
    static double Sqrt(double d);
    static double Sin(double d);
    static double Cos(double d);
};

#endif /* MediaMath_h */
