package android.slkmedia.mediaeditengine;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Environment;
import android.slkmedia.mediastreamer.utils.FolderUtils;
import android.util.Log;
import android.os.Process;

public class MicrophoneAudioRecorder{
	private static final String TAG = "MicrophoneAudioRecorder";

	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}
	
	//Native Context
	private long mNativeContext = 0;
	private static native final void Native_Init();
	
	private Context mContext = null;
	public MicrophoneAudioRecorder()
	{
		mContext = null;
	}
	
	public MicrophoneAudioRecorder(Context context)
	{
		mContext = context;
	}
	
	private int oldAudioMode = AudioManager.MODE_CURRENT;
	private int oldAudioVolume = 0;
	private MicrophoneAudioRecorderOptions mMicrophoneAudioRecorderOptions = null;
	public boolean initialize(MicrophoneAudioRecorderOptions options)
	{
		mMicrophoneAudioRecorderOptions = options;
		if(mMicrophoneAudioRecorderOptions.workDir==null)
		{
			Log.w(TAG, "MicrophoneAudioRecorderOptions WorkDir is Null, So Set ExternalStorageDirectory to WorkDir");
			mMicrophoneAudioRecorderOptions.workDir = Environment.getExternalStorageDirectory().getPath()+"/YPPMediaStreamer";
		}
		
		boolean ret = FolderUtils.isFolderExists(mMicrophoneAudioRecorderOptions.workDir);
		if(!ret)
		{
			Log.e(TAG, "MicrophoneAudioRecorderOptions WorkDir is not exist");
			return false;
		}
		
		if(mMicrophoneAudioRecorderOptions.isControlAudioManger && mContext!=null)
		{
		    if (!hasPermission(mContext, android.Manifest.permission.RECORD_AUDIO)) {
		        Log.e(TAG, "RECORD_AUDIO permission is missing");
		        
		        return false;
		    }
		    
		    AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		    oldAudioMode = audioManager.getMode();
		    audioManager.setMode(AudioManager.MODE_NORMAL);
		    oldAudioVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), AudioManager.FLAG_PLAY_SOUND);
		}
		
		ret = Native_Initialize(mMicrophoneAudioRecorderOptions.sampleRate, mMicrophoneAudioRecorderOptions.numChannels, mMicrophoneAudioRecorderOptions.workDir);
		if(!ret)
		{
			if(mMicrophoneAudioRecorderOptions.isControlAudioManger && mContext!=null)
			{
			    AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
			    audioManager.setMode(oldAudioMode);
			    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, oldAudioVolume, AudioManager.FLAG_PLAY_SOUND);
			}
		}
		return ret;
	}
	private native boolean Native_Initialize(int sampleRate, int numChannels, String workDir);
	
	public boolean startRecord()
	{
		return Native_StartRecord();
	}
	private native boolean Native_StartRecord();
	
	public boolean startRecordWithStartPos(int startPositionMs)
	{
		return Native_StartRecordWithStartPos(startPositionMs);
	}
	private native boolean Native_StartRecordWithStartPos(int startPositionMs);
	
	public void stopRecord()
	{
		Native_StopRecord();
	}
	private native void Native_StopRecord();
	
	public int getRecordTimeMs()
	{
		return Native_GetRecordTimeMs();
	}
	private native int Native_GetRecordTimeMs();
	
	public int getRecordPcmDB()
	{
		return Native_GetRecordPcmDB();
	}
	private native int Native_GetRecordPcmDB();
	
	public boolean openAudioPlayer()
	{
		return Native_OpenAudioPlayer();
	}
	private native boolean Native_OpenAudioPlayer();
	
	public boolean startAudioPlay()
	{
		return Native_StartAudioPlay();
	}
	private native boolean Native_StartAudioPlay();
	
	public boolean seekAudioPlay(int seekTimeMs)
	{
		return Native_SeekAudioPlay(seekTimeMs);
	}
	private native boolean Native_SeekAudioPlay(int seekTimeMs);
	
	public void pauseAudioPlay()
	{
		Native_PauseAudioPlay();
	}
	private native void Native_PauseAudioPlay();
	
	public void closeAudioPlayer()
	{
		Native_CloseAudioPlayer();
	}
	private native void Native_CloseAudioPlayer();
	
	public int getPlayTimeMs()
	{
		return Native_GetPlayTimeMs();
	}
	private native int Native_GetPlayTimeMs();
	
	public int getPlayDurationMs()
	{
		return Native_GetPlayDurationMs();
	}
	private native int Native_GetPlayDurationMs();
	
	public int getPlayPcmDB()
	{
		return Native_GetPlayPcmDB();
	}
	private native int Native_GetPlayPcmDB();
	
	public boolean convertToWav(String wavFilePath)
	{
		return Native_ConvertToWav(wavFilePath);
	}
	private native boolean Native_ConvertToWav(String wavFilePath);
	
	public void terminate()
	{
		Native_Terminate();
		
		if(mMicrophoneAudioRecorderOptions!=null && mMicrophoneAudioRecorderOptions.isControlAudioManger && mContext!=null)
		{
		    AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		    audioManager.setMode(oldAudioMode);
		    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, oldAudioVolume, AudioManager.FLAG_PLAY_SOUND);
		}
	}
	private native void Native_Terminate();
	
    @Override
    protected void finalize() throws Throwable {
        try {
        	terminate();
        } finally {
            super.finalize();
        }
    }
    
	// Checks if the process has as specified permission or not.
	private static boolean hasPermission(Context context, String permission) {
	    return context.checkPermission(permission, Process.myPid(), Process.myUid())
	        == PackageManager.PERMISSION_GRANTED;
	}
}
