package android.slkmedia.mediaeditengine;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

public class MediaDubbingProducer {
	private static final String TAG = "MediaDubbingProducer";

	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}

	//Native Context
	private long mNativeContext = 0;
	
	private static native final void Native_Init();
	private native void Native_Start(String videoUrl, String bgmUrl, float bgmVolume, String dubUrl, float dubVolume, String productUrl, Object mediadubbingproducer_this);
	private native void Native_Pause();
	private native void Native_Resume();
	private native void Native_Stop(boolean isCancle);
	
	//////////////////////////////////////////////////////////////////////////
	/**
	 * Called from native code when an interesting event happens. This method
	 * just uses the EventHandler system to post the event back to the main app
	 * thread. We use a weak reference to the original MediaPlayer object so
	 * that the native code is safe from the object disappearing from underneath
	 * it. (This is the cookie passed to native_setup().)
	 */
	private static void postEventFromNative(Object mediadubbingproducer_ref, int what,
			int arg1, int arg2, Object obj) {
		MediaDubbingProducer dp = (MediaDubbingProducer) ((WeakReference<?>) mediadubbingproducer_ref).get();
		if (dp == null) {
			return;
		}

		if (dp.dubbingProducerCallbackHandler != null) {
			Message msg = dp.dubbingProducerCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}
	//////////////////////////////////////////////////////////////////////////

	private Lock mDubbingProducerLock = null;
	private Condition mDubbingProducerCondition = null;
	private HandlerThread mHandlerThread = null;
	private Handler dubbingProducerCallbackHandler = null;
	
	public MediaDubbingProducer()
	{
		mDubbingProducerLock = new ReentrantLock(); 
		mDubbingProducerCondition = mDubbingProducerLock.newCondition();
		
		mHandlerThread = new HandlerThread("MediaDubbingProducerHandlerThread");
		mHandlerThread.start();
		
		dubbingProducerCallbackHandler = new Handler(mHandlerThread.getLooper()) {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case MediaDubbingProducer.CALLBACK_MEDIA_DUBBING_STREAMING:
					//Log.i(TAG, "CALLBACK_MEDIA_DUBBING_STREAMING");
					if(mMediaDubbingProducerListener!=null)
					{
						mMediaDubbingProducerListener.onDubbingStart();
					}
					break;
				case MediaDubbingProducer.CALLBACK_MEDIA_DUBBING_ERROR:
					//Log.i(TAG, "CALLBACK_MEDIA_DUBBING_ERROR");
					if(mMediaDubbingProducerListener!=null)
					{
						mMediaDubbingProducerListener.onDubbingError(msg.arg1);
					}
					
					break;
				case MediaDubbingProducer.CALLBACK_MEDIA_DUBBING_INFO:
					//Log.i(TAG, "CALLBACK_MEDIA_DUBBING_INFO");
					if(mMediaDubbingProducerListener!=null)
					{
						mMediaDubbingProducerListener.onDubbingInfo(msg.arg1, msg.arg2);
					}
					break;
				case MediaDubbingProducer.CALLBACK_MEDIA_DUBBING_END:
					//Log.i(TAG, "CALLBACK_MEDIA_DUBBING_END");
					if(mMediaDubbingProducerListener!=null)
					{
						mMediaDubbingProducerListener.onDubbingEnd();
					}
					
					break;
				default:
					break;
				}
			}
		};
	}
	
	private MediaDubbingProducerListener mMediaDubbingProducerListener = null;
	public void setMediaDubbingProducerListener(MediaDubbingProducerListener listener)
	{
		mMediaDubbingProducerListener = listener;
	}
	
	private boolean isStarted = false;
	public void start(MediaDubbingProducerOptions options)
	{
		mDubbingProducerLock.lock();

		if(isStarted)
		{
			Log.w(TAG, "MediaDubbingProducer has started!!");
			mDubbingProducerLock.unlock();
			return;
		}
		
		Native_Start(options.videoUrl, options.bgmUrl, options.bgmVolume, options.dubUrl, options.dubVolume, options.productUrl, new WeakReference<MediaDubbingProducer>(this));
		
		isStarted = true;
		isPaused = false;
		mDubbingProducerLock.unlock();
	}
	
	private boolean isPaused = false;
	public void Pause()
	{
		mDubbingProducerLock.lock();
		if(!isPaused)
		{
			Native_Pause();
			isPaused = true;
		}
		mDubbingProducerLock.unlock();
	}
	
	public void Resume()
	{
		mDubbingProducerLock.lock();
		
		if(isPaused)
		{
			Native_Resume();
			isPaused = false;
		}
		
		mDubbingProducerLock.unlock();
	}
	
	public void Stop(boolean isCancle)
	{
		mDubbingProducerLock.lock();

		if(!isStarted)
		{
			Log.w(TAG, "MediaDubbingProducer has stopped");
			mDubbingProducerLock.unlock();
			return;
		}
		
		Native_Stop(isCancle);
	
		isStarted = false;
		
		mDubbingProducerLock.unlock();
	}
	
	private boolean isFinishAllCallbacksAndMessages = false;
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2) public void Release()
	{
		Stop(false);
		
		mDubbingProducerLock.lock();
		dubbingProducerCallbackHandler.post(new Runnable() {
			@Override
			public void run() {
				dubbingProducerCallbackHandler.removeCallbacksAndMessages(null);
				
				mDubbingProducerLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mDubbingProducerCondition.signalAll();
				mDubbingProducerLock.unlock();
			}
		});
		
		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mDubbingProducerCondition.await(10, TimeUnit.MILLISECONDS);
			}
			
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
		}
		
		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR2)
		{
			mHandlerThread.quitSafely();
		}else{
			mHandlerThread.quit();
		}
		
		mDubbingProducerLock.unlock();
	}
	
	
	//media_dubbing_event_type
	public final static int CALLBACK_MEDIA_DUBBING_STREAMING = 1;
	public final static int CALLBACK_MEDIA_DUBBING_ERROR = 2;
	public final static int CALLBACK_MEDIA_DUBBING_INFO = 3;
	public final static int CALLBACK_MEDIA_DUBBING_END = 4;
	
	//media_dubbing_info_type
	public final static int CALLBACK_MEDIA_DUBBING_INFO_WRITE_TIMESTAMP = 1;
	
	//media_dubbing_error_type
	public final static int CALLBACK_MEDIA_DUBBING_ERROR_UNKNOWN = -1;
	public final static int CALLBACK_MEDIA_DUBBING_ERROR_OPEN_INPUT_VIDEO_MATERIAL_FAIL = 0;
	public final static int CALLBACK_MEDIA_DUBBING_ERROR_OPEN_INPUT_BGM_MATERIAL_FAIL = 1;
	public final static int CALLBACK_MEDIA_DUBBING_ERROR_OPEN_INPUT_DUB_MATERIAL_FAIL = 2;
	public final static int CALLBACK_MEDIA_DUBBING_ERROR_OPEN_OUTPUT_MEDIA_PRODUCT_FAIL = 3;
	public final static int CALLBACK_MEDIA_DUBBING_ERROR_AUDIO_FILTER_INPUT_FAIL = 4;
	public final static int CALLBACK_MEDIA_DUBBING_ERROR_AUDIO_FILTER_OUTPUT_FAIL = 5;
	public final static int CALLBACK_MEDIA_DUBBING_ERROR_AUDIO_ENCODE_FAIL = 6;
	public final static int CALLBACK_MEDIA_DUBBING_ERROR_AUDIO_MUX_FAIL = 7;
	public final static int CALLBACK_MEDIA_DUBBING_ERROR_AUDIO_FIFO_READ_FAIL = 8;
	public final static int CALLBACK_MEDIA_DUBBING_ERROR_VIDEO_REMUX_FAIL = 9;
	public final static int CALLBACK_MEDIA_DUBBING_ERROR_NO_MEMORY_SPACE = 10;
	public final static int CALLBACK_MEDIA_DUBBING_ERROR_AUDIO_DEMUX_FAIL = 11;
}
