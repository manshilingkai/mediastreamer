package android.slkmedia.mediaeditengine;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class AudioPlayer {
	private static final String TAG = "AudioPlayer";

	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}

	//Native Context
	private long mNativeContext = 0;
	
	private static native final void Native_Init();
	private native final void Native_Setup(Object audioplayer_this);
	private native final void Native_Finalize();
	
	// PlayState
	private static final int UNKNOWN = -1;
	private static final int IDLE = 0;
	private static final int INITIALIZED = 1;
	private static final int PREPARING = 2;
	private static final int PREPARED = 3;
	private static final int STARTED = 4;
	private static final int STOPPED = 5;
	private static final int PAUSED = 6;
//	private static final int PLAYBACK_COMPLETED = 7;
	private static final int END = 8;
	private static final int ERROR = 9;
	private int currentPlayState = UNKNOWN;
	
	// ////////////////////////////////////////////////////////////////////////////////
	// AudioPlayerCallbackHandler
	private final static int CALLBACK_AUDIO_PLAYER_PREPARED = 0;
	private final static int CALLBACK_AUDIO_PLAYER_ERROR = 1;
	private final static int CALLBACK_AUDIO_PLAYER_INFO = 2;
	private final static int CALLBACK_AUDIO_PLAYER_PLAYBACK_COMPLETE = 3;
	private final static int CALLBACK_AUDIO_PLAYER_SEEK_COMPLETE = 4;
	
	/**
	 * Called from native code when an interesting event happens. This method
	 * just uses the EventHandler system to post the event back to the main app
	 * thread. We use a weak reference to the original MediaPlayer object so
	 * that the native code is safe from the object disappearing from underneath
	 * it. (This is the cookie passed to native_setup().)
	 */
	private static void postEventFromNative(Object audioplayer_ref, int what,
			int arg1, int arg2, Object obj) {
		AudioPlayer ap = (AudioPlayer) ((WeakReference<?>) audioplayer_ref).get();
		if (ap == null) {
			return;
		}

		if (ap.audioPlayerCallbackHandler != null) {
			Message msg = ap.audioPlayerCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}
	
	private Handler audioPlayerCallbackHandler = null;

	public AudioPlayer()
	{
		construct();
	}
	
	private Context mContext = null;
	private int oldAudioMode = AudioManager.MODE_CURRENT;
	private int oldAudioVolume = 0;
	private AudioPlayerOptions mAudioPlayerOptions = null;
	public AudioPlayer(Context context, AudioPlayerOptions options)
	{
		mContext = context;
		mAudioPlayerOptions = options;
		
		if(mAudioPlayerOptions!=null && mAudioPlayerOptions.isControlAudioManger && mContext!=null)
		{
		    AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		    oldAudioMode = audioManager.getMode();
		    audioManager.setMode(AudioManager.MODE_NORMAL);
		    oldAudioVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), AudioManager.FLAG_PLAY_SOUND);
		}
		
		construct();
	}
	
	private void construct()
	{
		Looper workLooper = Looper.myLooper();
		if(workLooper==null)
		{
			workLooper = Looper.getMainLooper();
		}
		
		audioPlayerCallbackHandler = new Handler(workLooper) {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case CALLBACK_AUDIO_PLAYER_PREPARED:
					if(msg.arg1==1)
					{
						currentPlayState = STARTED;
					}else{
						currentPlayState = PREPARED;
					}
					if(audioPlayerListener != null)
					{
						audioPlayerListener.onPrepared();
					}
					break;
				case CALLBACK_AUDIO_PLAYER_ERROR:
					currentPlayState = ERROR;
					if(audioPlayerListener != null)
					{
						audioPlayerListener.onError(msg.arg1, msg.arg2);
					}
					break;
				case CALLBACK_AUDIO_PLAYER_INFO:
					if(audioPlayerListener != null)
					{
						audioPlayerListener.onInfo(msg.arg1, msg.arg2);
					}
					break;
				case CALLBACK_AUDIO_PLAYER_PLAYBACK_COMPLETE:
					if(audioPlayerListener !=null)
					{
						audioPlayerListener.onCompletion();
					}
					break;
				case CALLBACK_AUDIO_PLAYER_SEEK_COMPLETE:
					if(audioPlayerListener!=null)
					{
						audioPlayerListener.OnSeekComplete();
					}
					break;
				default:
					break;
				}
			}
		};
		
		Native_Setup(new WeakReference<AudioPlayer>(this));
		
		currentPlayState = IDLE;
	}
	
	private AudioPlayerListener audioPlayerListener = null;
	public void setListener(AudioPlayerListener listener)
	{
		audioPlayerListener = listener;
	}
	
	public void setDataSource(String path) throws IllegalStateException
	{
		if (currentPlayState != IDLE && currentPlayState != STOPPED && currentPlayState != ERROR) {
			
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		Native_SetDataSource(path);
		currentPlayState = INITIALIZED;
	}
	private native void Native_SetDataSource(String path);
	
	public void prepare() throws IllegalStateException
	{
		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		currentPlayState = PREPARING;
		Native_Prepare();
		currentPlayState = PREPARED;
	}
	private native void Native_Prepare();
	
	public void prepareAsync() throws IllegalStateException {
		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		currentPlayState = PREPARING;
		
		Native_PrepareAsync();
	}
	private native void Native_PrepareAsync();
	
	public void prepareAsyncToPlay() throws IllegalStateException {
		if (currentPlayState != INITIALIZED && currentPlayState != STOPPED && currentPlayState != ERROR) {
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		currentPlayState = PREPARING;
		
		Native_PrepareAsyncToPlay();
	}
	private native void Native_PrepareAsyncToPlay();
	
	public void play() throws IllegalStateException
	{
		if (currentPlayState==STARTED) {
			return;
		} else if (currentPlayState != PREPARED && currentPlayState != PAUSED) {
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		Native_Play();
		
		currentPlayState = STARTED;
	}
	private native void Native_Play();
	
	public boolean isPlaying() throws IllegalStateException {

		boolean bIsPlaying = false;

		if(currentPlayState == STARTED)
		{
			bIsPlaying = Native_IsPlaying();
		}

		return bIsPlaying;
	}
	private native boolean Native_IsPlaying();
	
	public void pause() throws IllegalStateException {
		if (currentPlayState == PAUSED) {
			return;
		}

		if (currentPlayState != STARTED) {
//			throw new IllegalStateException("Error State: " + currentPlayState);
            return;
		}

		Native_Pause();
		
		currentPlayState = PAUSED;
	}
	private native void Native_Pause();
	
	public void stop() throws IllegalStateException {
		if (currentPlayState==STOPPED) {
			return;
		} else if (currentPlayState != PREPARING && currentPlayState != PREPARED && currentPlayState != STARTED
				&& currentPlayState != PAUSED && currentPlayState != ERROR) {
			throw new IllegalStateException("Error State: " + currentPlayState);
		}

		Native_Stop();
		
		currentPlayState = STOPPED;
	}
	private native void Native_Stop();
	
	public void seekTo(int msec) throws IllegalStateException
	{
		if(currentPlayState != STARTED && currentPlayState != PAUSED && currentPlayState != PREPARED)
		{
			throw new IllegalStateException("Error State: " + currentPlayState);
		}
		
		Native_SeekTo(msec);		
	}
	
	private native void Native_SeekTo(int msec);
	
	public int getCurrentPositionMs() {
		
		int currentPositionMs = 0;
		
		if (currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED) {
			return currentPositionMs;
		}
		currentPositionMs = Native_GetPlayTimeMs();
		
		return currentPositionMs;
	}
	private native int Native_GetPlayTimeMs();
	
	public int getDurationMs() {
		
		int durationMs = 0;
				
		if (currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED) {
			return durationMs;
		}
		
		durationMs = Native_GetDurationMs();
		
		return durationMs;
	}
	private native int Native_GetDurationMs();
	
	public int getPcmDB() {
		
		int pcmDB = 0;
				
		if (currentPlayState != PREPARED && currentPlayState != STARTED && currentPlayState != PAUSED) {
			return pcmDB;
		}
		
		pcmDB = Native_GetPcmDB();
		
		return pcmDB;
	}
	private native int Native_GetPcmDB();
	
	public void setVolume(float volume) {		
		if(currentPlayState==END)
		{
			return;
		}
		
		Native_SetVolume(volume);
	}
	private native void Native_SetVolume(float volume);
	
	public void setPlayRate(float playrate) {		
		if(currentPlayState==END)
		{
			return;
		}
		
		Native_SetPlayRate(playrate);
	}
	private native void Native_SetPlayRate(float playrate);

	public void setLooping(boolean isLooping) {
		if(currentPlayState==END)
		{
			return;
		}
		
		Native_SetLooping(isLooping);
	}
	private native void Native_SetLooping(boolean isLooping);
	
	public void release() {
		if (currentPlayState == END) {
			return;
		}

		if (currentPlayState == PREPARING || currentPlayState == PREPARED || currentPlayState == STARTED
				|| currentPlayState == PAUSED || currentPlayState == ERROR) {
			Native_Stop();
		}
		
		Native_Finalize();
		
		audioPlayerCallbackHandler.removeCallbacksAndMessages(null);
		
		
		if(mAudioPlayerOptions!=null && mAudioPlayerOptions.isControlAudioManger && mContext!=null)
		{
		    AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		    audioManager.setMode(oldAudioMode);
		    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, oldAudioVolume, AudioManager.FLAG_PLAY_SOUND);
		}
		
		currentPlayState = END;		
	}
	
    @Override
    protected void finalize() throws Throwable {
        try {
        	release();
        } finally {
            super.finalize();
        }
    }
}
