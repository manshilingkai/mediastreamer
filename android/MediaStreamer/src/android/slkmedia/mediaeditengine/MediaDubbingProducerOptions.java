package android.slkmedia.mediaeditengine;

public class MediaDubbingProducerOptions {
	public String videoUrl = null;
	public String bgmUrl = null;
	public float bgmVolume = 1.0f;
	public String dubUrl = null;
	public float dubVolume = 1.0f;
	public String productUrl = null;
}
