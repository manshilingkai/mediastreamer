package android.slkmedia.mediaeditengine;

public class MicrophoneAudioRecorderOptions {
	public boolean isControlAudioManger = false;
	public int sampleRate = 44100;
	public int numChannels = 2;
	public String workDir = null;
}
