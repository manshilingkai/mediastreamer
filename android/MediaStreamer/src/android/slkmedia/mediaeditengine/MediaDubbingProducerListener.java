package android.slkmedia.mediaeditengine;

public interface MediaDubbingProducerListener {
	public abstract void onDubbingStart();
	public abstract void onDubbingError(int errorType);
	public abstract void onDubbingInfo(int infoType, int infoValue);
	public abstract void onDubbingEnd();
}
