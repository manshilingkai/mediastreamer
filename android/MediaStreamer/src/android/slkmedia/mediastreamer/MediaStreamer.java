package android.slkmedia.mediastreamer;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.view.Surface;
import android.os.Process;

public class MediaStreamer {
	private static final String TAG = "MediaStreamer";
	
	public static final int VIDEOFRAME_RAWTYPE_I420 = 0x0001;
	public static final int VIDEOFRAME_RAWTYPE_NV12 = 0x0002;
	public static final int VIDEOFRAME_RAWTYPE_NV21 = 0x0003;
	public static final int VIDEOFRAME_RAWTYPE_BGRA = 0x0004;
	public static final int VIDEOFRAME_RAWTYPE_RGBA = 0x0005;
	
	public static final int VIDEO_HARD_ENCODE = 0;
	public static final int VIDEO_SOFT_ENCODE = 1;
	
	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}

	//Native Context
	private int mNativeContext;

	private static native final void Native_Init();
	
	private native void Native_StartWithEncodeSurface(VideoOptions videoOptions, AudioOptions audioOptions, String publishUrl, Object mediastreamer_this, Surface encodeSurface, EncodeSurfaceCore encodeSurfaceCore);
	private native void Native_Start(VideoOptions videoOptions, AudioOptions audioOptions, String publishUrl, Object mediastreamer_this);
	private native void Native_Pause();
	private native void Native_Resume();
	private native void Native_Stop();
	private native void Native_InputPreviewFrame(byte[] data, int length, int width, int height, long timestamp, int rotation, int videoRawType);
	private native void Native_EnableAudio(int isEnable);
	
	/*
	static public class VideoOptions
	{
	    public boolean hasVideo = false;

	    public int videoEncodeType = VIDEO_SOFT_ENCODE;

	    public int videoWidth = 0;
	    public int videoHeight = 0;
	    public int videoFps = 0;
	    public int videoRawType = VIDEOFRAME_RAWTYPE_I420;

	    public int videoProfile = 0; //0:base_line 1:main_profile 2:high_profile
	    public int videoBitRate = 0;
	    public int encodeMode = 0; //0:VBR or 1:CBR
	    public int maxKeyFrameIntervalMs = 0;

	    public int quality = 0; //[-5, 5]:CRF
	    public boolean bStrictCBR = false;
	    public int deblockingFilterFactor = 0; //[-6, 6] -6 light filter, 6 strong
	}*/
	
	/*
	static public class AudioOptions
	{
		public boolean hasAudio = false;
		
		public int audioSampleRate = 0;
		public int audioNumChannels = 0;
		public int audioBitRate = 0;
	}*/
	
	//////////////////////////////////////////////////////////////////////////
	/**
	 * Called from native code when an interesting event happens. This method
	 * just uses the EventHandler system to post the event back to the main app
	 * thread. We use a weak reference to the original MediaPlayer object so
	 * that the native code is safe from the object disappearing from underneath
	 * it. (This is the cookie passed to native_setup().)
	 */
	private static void postEventFromNative(Object mediastreamer_ref, int what,
			int arg1, int arg2, Object obj) {
		MediaStreamer ms = (MediaStreamer) ((WeakReference<?>) mediastreamer_ref).get();
		if (ms == null) {
			return;
		}

		if (ms.mediaStreamerCallbackHandler != null) {
			Message msg = ms.mediaStreamerCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}
	//////////////////////////////////////////////////////////////////////////
	
	private boolean isConnected= false;
	private Lock mMediaStreamerLock = null;
	private Condition mMediaStreamerCondition = null;
	private HandlerThread mHandlerThread = null;
	private Handler mediaStreamerCallbackHandler = null;

	private Context context;
	
	public MediaStreamer(Context context)
	{
		this.context = context;
		
		mMediaStreamerLock = new ReentrantLock(); 
		mMediaStreamerCondition = mMediaStreamerLock.newCondition();
		
		mHandlerThread = new HandlerThread("MediaStreamerHandlerThread");
		mHandlerThread.start();
		
		mediaStreamerCallbackHandler = new Handler(mHandlerThread.getLooper()) {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case MediaStreamer.CALLBACK_MEDIA_STREAMER_CONNECTING:
					Log.i(TAG, "CALLBACK_MEDIA_STREAMER_CONNECTING");
					if(mediaStreamerListener!=null)
					{
						mediaStreamerListener.onMediaStreamerConnecting();
					}
					break;
				case MediaStreamer.CALLBACK_MEDIA_STREAMER_CONNECTED:
					Log.i(TAG, "CALLBACK_MEDIA_STREAMER_CONNECTED");
					if(mediaStreamerListener!=null)
					{
						mediaStreamerListener.onMediaStreamerConnected();
					}
					
					mMediaStreamerLock.lock();
					isConnected = true;
					mMediaStreamerLock.unlock();
					
					break;
				case MediaStreamer.CALLBACK_MEDIA_STREAMER_STREAMING:
					Log.i(TAG, "CALLBACK_MEDIA_STREAMER_STREAMING");
					if(mediaStreamerListener!=null)
					{
						mediaStreamerListener.onMediaStreamerStreaming();
					}
					break;
				case MediaStreamer.CALLBACK_MEDIA_STREAMER_ERROR:
					Log.i(TAG, "CALLBACK_MEDIA_STREAMER_ERROR");
					
					if(mediaStreamerListener!=null)
					{
						mediaStreamerListener.onMediaStreamerError(msg.arg1);
					}
					
					mMediaStreamerLock.lock();
					isConnected = false;
					mMediaStreamerLock.unlock();
					
					break;
				case MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO:
					Log.i(TAG, "CALLBACK_MEDIA_STREAMER_INFO");
					if(mediaStreamerListener!=null)
					{
						mediaStreamerListener.onMediaStreamerInfo(msg.arg1, msg.arg2);
					}
					break;
				case MediaStreamer.CALLBACK_MEDIA_STREAMER_END:
					Log.i(TAG, "CALLBACK_MEDIA_STREAMER_END");
					
					if(mediaStreamerListener!=null)
					{
						mediaStreamerListener.onMediaStreamerEnd();
					}
					
					mMediaStreamerLock.lock();
					isConnected = false;
					mMediaStreamerLock.unlock();
					
					break;
				default:
					break;
				}
			}
		};
	}
	
	private MediaStreamerListener mediaStreamerListener = null;
	public void setMediaStreamerListener(MediaStreamerListener listener)
	{
		mediaStreamerListener = listener;
	}
	
	private boolean isStarted = false;
	private WeakReference<MediaStreamer> mWeakReferenceMediaStreamer_this = null;
	private int oldAudioMode = AudioManager.MODE_CURRENT;
	public void Start(VideoOptions videoOptions, AudioOptions audioOptions, String publishUrl)
	{
		mMediaStreamerLock.lock();

		if(isStarted)
		{
			Log.w(TAG, "MediaStreamer has started!!");
			mMediaStreamerLock.unlock();
			return;
		}
		
		if(mWeakReferenceMediaStreamer_this!=null)
		{
			mWeakReferenceMediaStreamer_this.clear();
			mWeakReferenceMediaStreamer_this = null;
		}
		mWeakReferenceMediaStreamer_this = new WeakReference<MediaStreamer>(this);
		
		if(audioOptions.hasAudio)
		{
		    AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		    oldAudioMode = audioManager.getMode();
		    audioManager.setMode(AudioManager.MODE_NORMAL);
//		    audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
		    
		    if (!hasPermission(context, android.Manifest.permission.RECORD_AUDIO)) {
		        Log.e(TAG, "RECORD_AUDIO permission is missing");
		        audioOptions.hasAudio = false;
		    }
		}
		
		Native_Start(videoOptions,audioOptions,publishUrl,mWeakReferenceMediaStreamer_this);
		
		isStarted = true;
		
		isPaused = false;
		mMediaStreamerLock.unlock();
	}
	
	public void StartWithEncodeSurface(VideoOptions videoOptions, AudioOptions audioOptions, String publishUrl, Surface encodeSurface, EncodeSurfaceCore encodeSurfaceCore)
	{
		mMediaStreamerLock.lock();

		if(isStarted)
		{
			Log.w(TAG, "MediaStreamer has started!!");
			mMediaStreamerLock.unlock();
			return;
		}
		
		if(mWeakReferenceMediaStreamer_this!=null)
		{
			mWeakReferenceMediaStreamer_this.clear();
			mWeakReferenceMediaStreamer_this = null;
		}
		mWeakReferenceMediaStreamer_this = new WeakReference<MediaStreamer>(this);
		
		if(audioOptions.hasAudio)
		{
		    AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		    oldAudioMode = audioManager.getMode();
		    audioManager.setMode(AudioManager.MODE_NORMAL);
//		    audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
		    
		    if (!hasPermission(context, android.Manifest.permission.RECORD_AUDIO)) {
		        Log.e(TAG, "RECORD_AUDIO permission is missing");
		        audioOptions.hasAudio = false;
		    }
		}
		
		Native_StartWithEncodeSurface(videoOptions,audioOptions,publishUrl,mWeakReferenceMediaStreamer_this, encodeSurface, encodeSurfaceCore);
		
		isStarted = true;
		
		isPaused = false;
		mMediaStreamerLock.unlock();
	}
	
	private boolean isPaused = false;
	public void Pause()
	{
		mMediaStreamerLock.lock();
		if(isConnected)
		{
			if(!isPaused)
			{
				Native_Pause();
				isPaused = true;
			}
		}
	    mMediaStreamerLock.unlock();
	}
	
	public void Resume()
	{
		mMediaStreamerLock.lock();
		
		if(isConnected)
		{
			if(isPaused)
			{
				Native_Resume();
				isPaused = false;
			}
		}
		
	    mMediaStreamerLock.unlock();
	}
	
	public void Stop()
	{
		mMediaStreamerLock.lock();

		if(!isStarted)
		{
			Log.w(TAG, "MediaStreamer has stopped");
			mMediaStreamerLock.unlock();
			return;
		}
		
		Native_Stop();
		
	    AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
	    audioManager.setMode(oldAudioMode);
		
		if(mWeakReferenceMediaStreamer_this!=null)
		{
			mWeakReferenceMediaStreamer_this.clear();
			mWeakReferenceMediaStreamer_this = null;
		}
		
		isStarted = false;
		
		mMediaStreamerLock.unlock();
	}
	
	public void EnableAudio(boolean isEnable)
	{
		mMediaStreamerLock.lock();
		
		if(isStarted)
		{
			if(isEnable)
			{
				Native_EnableAudio(1);
			}else{
				Native_EnableAudio(0);
			}
		}
		
		mMediaStreamerLock.unlock();
	}
	
	public void InputPreviewFrame(byte[] data, int width, int height, int rotation, int rawType)
	{
		mMediaStreamerLock.lock();
		if(isConnected)
		{
			long captureTimeMs = System.currentTimeMillis();
		    Native_InputPreviewFrame(data,data.length,width,height,captureTimeMs,rotation, rawType);
		}
	    mMediaStreamerLock.unlock();
	}
	
	private boolean isFinishAllCallbacksAndMessages = false;
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2) public void Release()
	{
		Stop();
		
		mediaStreamerCallbackHandler.post(new Runnable() {
			@Override
			public void run() {
				mediaStreamerCallbackHandler.removeCallbacksAndMessages(null);
				
				mMediaStreamerLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mMediaStreamerCondition.signalAll();
				mMediaStreamerLock.unlock();

				if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR2)
				{
					mHandlerThread.quitSafely();
				}else{
					mHandlerThread.quit();
				}
			}
		});
		
		mMediaStreamerLock.lock();
		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mMediaStreamerCondition.await(1, TimeUnit.SECONDS);
			}
			
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
			mMediaStreamerLock.unlock();
		}
	}
	
	// Checks if the process has as specified permission or not.
	private static boolean hasPermission(Context context, String permission) {
	    return context.checkPermission(permission, Process.myPid(), Process.myUid())
	        == PackageManager.PERMISSION_GRANTED;
	}
	
	//media_streamer_event_type
	public final static int CALLBACK_MEDIA_STREAMER_CONNECTING = 0;
	public final static int CALLBACK_MEDIA_STREAMER_CONNECTED = 1;
	public final static int CALLBACK_MEDIA_STREAMER_STREAMING = 2;
	public final static int CALLBACK_MEDIA_STREAMER_ERROR = 3;
	public final static int CALLBACK_MEDIA_STREAMER_INFO = 4;
	public final static int CALLBACK_MEDIA_STREAMER_END = 5;
	public final static int CALLBACK_MEDIA_STREAMER_PAUSED = 6;
	
	//media_streamer_info_type
	public final static int CALLBACK_MEDIA_STREAMER_INFO_ALREADY_CONNECTING = 1;
	public final static int CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_DELAY_TIME = 2;
	public final static int CALLBACK_MEDIA_STREAMER_INFO_ALREADY_ENDING = 3;
	public final static int CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_REAL_BITRATE = 4;
	public final static int CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS = 5;
	public final static int CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_DOWN_BITRATE = 6;
	public final static int CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_UP_BITRATE = 7;
	public final static int CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_TIME = 8;
	public final static int CALLBACK_MEDIA_STREAMER_INFO_AUDIO_INPUT_DATA_LOST = 100;
	
	//media_streamer_error_type
	public final static int CALLBACK_MEDIA_STREAMER_ERROR_UNKNOWN = -1;
	public final static int CALLBACK_MEDIA_STREAMER_ERROR_CONNECT_FAIL = 0;
	public final static int CALLBACK_MEDIA_STREAMER_ERROR_MUX_FAIL = 1;
	public final static int CALLBACK_MEDIA_STREAMER_ERROR_COLORSPACECONVERT_FAIL = 2;
	public final static int CALLBACK_MEDIA_STREAMER_ERROR_VIDEO_ENCODE_FAIL = 3;
	public final static int CALLBACK_MEDIA_STREAMER_ERROR_AUDIO_CAPTURE_START_FAIL = 4;
	public final static int CALLBACK_MEDIA_STREAMER_ERROR_AUDIO_ENCODE_FAIL = 5;
	public final static int CALLBACK_MEDIA_STREAMER_ERROR_AUDIO_CAPTURE_STOP_FAIL = 6;
	public final static int CALLBACK_MEDIA_STREAMER_ERROR_POOR_NETWORK = 7;

}
