package android.slkmedia.mediastreamer;

import java.nio.ByteBuffer;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.util.Log;

public class SLKMediaMuxer implements MediaMuxerInterface, MediaMuxerListener{
	private static final String TAG = "SLKMediaMuxer";

	public static final int SYSTEM_MEDIAMUXER_TYPE = 1;
	public static final int FFMPEG_MEDIAMUXER_TYPE = 2;
	
	public static final int ONLY_AUDIO_TRACK = 0;
	public static final int ONLY_VIDEO_TRACK = 1;
	public static final int AUDIO_VIDEO_TRACK = 2;
	
    public static final class OutputFormat {
        public static final int MUXER_OUTPUT_MPEG_4 = 0;
        public static final int MUXER_OUTPUT_FLV   = 1;
    };
	
	private MediaMuxerInterface mMediaMuxer = null;
	private Lock mMediaMuxerLock = null;
	private int mTrack = AUDIO_VIDEO_TRACK;
	public SLKMediaMuxer(int type, String path, int format, int track) //MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4
	{
		mMediaMuxerLock = new ReentrantLock();
		
		if(type == SYSTEM_MEDIAMUXER_TYPE) {
			if(format==OutputFormat.MUXER_OUTPUT_MPEG_4)
			{
				mMediaMuxer = new AndroidMediaMuxer(path, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
				mMediaMuxer.setMediaMuxerListener(this);
			}else{
				mMediaMuxer = new FFMediaMuxer(path, format);
				mMediaMuxer.setMediaMuxerListener(this);
			}
		}else if(type == FFMPEG_MEDIAMUXER_TYPE) {
			mMediaMuxer = new FFMediaMuxer(path, format);
		}
		
		mTrack = track;
	}
	
	private MediaMuxerListener mMediaMuxerListener = null;
	@Override
	public void setMediaMuxerListener(MediaMuxerListener listener) {
		mMediaMuxerListener = listener;
	}
	
	private boolean gotAudioTrack = false;
	private boolean gotVideoTrack = false;
	private int mVideoTrackIndex = -1;
	private int mAudioTrackIndex = -1;
	@Override
	public int addTrack(MediaFormat format) {
		int ret = -1;
		mMediaMuxerLock.lock();
		
		if(isMediaMuxerStarted)
		{
			mMediaMuxerLock.unlock();
			return -1;
		}
		
		if(format.getString(MediaFormat.KEY_MIME).startsWith("video/"))
		{
			if(mTrack==ONLY_AUDIO_TRACK)
			{
				mMediaMuxerLock.unlock();
				Log.w(TAG, "only support audio track, but add video track now");
				return -1;
			}else{
				if(gotVideoTrack)
				{
					mMediaMuxerLock.unlock();
					Log.w(TAG, "add video track twice");
					return mVideoTrackIndex;
				}else{
					mVideoTrackIndex = mMediaMuxer.addTrack(format);
					ret = mVideoTrackIndex;
					gotVideoTrack = true;
					mMediaMuxerLock.unlock();
					return ret;
				}
			}
		}else{
			if(mTrack==ONLY_VIDEO_TRACK)
			{
				mMediaMuxerLock.unlock();
				Log.w(TAG, "only support video track, but add audio track now");
				return -1;
			}else{
				if(gotAudioTrack)
				{
					mMediaMuxerLock.unlock();
					Log.w(TAG, "add audio track twice");
					return mAudioTrackIndex;
				}else{
					mAudioTrackIndex = mMediaMuxer.addTrack(format);
					ret = mAudioTrackIndex;
					gotAudioTrack = true;
					mMediaMuxerLock.unlock();
					return ret;
				}
			}
		}
	}

	private boolean isMediaMuxerStarted = false;
	@Override
	public void start() {
		mMediaMuxerLock.lock();
		if(isMediaMuxerStarted)
		{
			mMediaMuxerLock.unlock();
			return;
		}
		if((mTrack==ONLY_AUDIO_TRACK && gotAudioTrack) || (mTrack==ONLY_VIDEO_TRACK && gotVideoTrack) || (mTrack==AUDIO_VIDEO_TRACK && gotAudioTrack && gotVideoTrack))
		{
			mMediaMuxer.start();
			
			isMediaMuxerStarted = true;
		}
		mMediaMuxerLock.unlock();
	}

	@Override
	public void stop() {
		mMediaMuxerLock.lock();
		if(!isMediaMuxerStarted)
		{
			mMediaMuxerLock.unlock();
			return;
		}
		
		mMediaMuxer.stop();
		isGotFirstVideoKeyFrame = false;
		isMediaMuxerStarted = false;
		mMediaMuxerLock.unlock();
	}

	@Override
	public void release() {
		mMediaMuxerLock.lock();
		if(mMediaMuxer!=null)
		{
			mMediaMuxer.release();
			mMediaMuxer = null;
		}
		mMediaMuxerLock.unlock();
	}

	private boolean isGotFirstVideoKeyFrame = false;
	@Override
	public void writeSampleData(int trackIndex, ByteBuffer byteBuf,
			BufferInfo bufferInfo) {
		mMediaMuxerLock.lock();
		if(isMediaMuxerStarted)
		{
			if(mTrack==ONLY_AUDIO_TRACK)
			{
				mMediaMuxer.writeSampleData(trackIndex, byteBuf, bufferInfo);
			}else{
				if((bufferInfo.flags & MediaCodec.BUFFER_FLAG_KEY_FRAME) != 0)
				{
					isGotFirstVideoKeyFrame = true;
				}
				if(isGotFirstVideoKeyFrame)
				{
					mMediaMuxer.writeSampleData(trackIndex, byteBuf, bufferInfo);
				}
			}
		}
		mMediaMuxerLock.unlock();
	}
	
	@Override
	public long getPublishDelayTimeMs()
	{
		long ret = 0;
		
		mMediaMuxerLock.lock();
		
		if(isMediaMuxerStarted)
		{
			if(mMediaMuxer!=null)
			{
				ret = mMediaMuxer.getPublishDelayTimeMs();
			}
		}
		
		mMediaMuxerLock.unlock();
		
		return ret;
	}
	
	@Override
	public void onMediaMuxerConnecting() {
		if(mMediaMuxerListener!=null)
		{
			mMediaMuxerListener.onMediaMuxerConnecting();
		}
	}

	@Override
	public void onMediaMuxerConnected() {
		if(mMediaMuxerListener!=null)
		{
			mMediaMuxerListener.onMediaMuxerConnected();
		}
	}

	@Override
	public void onMediaMuxerStreaming() {
		if(mMediaMuxerListener!=null)
		{
			mMediaMuxerListener.onMediaMuxerStreaming();
		}
	}

	@Override
	public void onMediaMuxerError(int errorType) {
		if(mMediaMuxerListener!=null)
		{
			mMediaMuxerListener.onMediaMuxerError(errorType);
		}
	}

	@Override
	public void onMediaMuxerInfo(int infoType, int infoValue) {
		if(mMediaMuxerListener!=null)
		{
			mMediaMuxerListener.onMediaMuxerInfo(infoType, infoValue);
		}
	}

	@Override
	public void onMediaMuxerEnd() {
		if(mMediaMuxerListener!=null)
		{
			mMediaMuxerListener.onMediaMuxerEnd();
		}
	}
	
	//mediamuxer_event_type
	public final static int CALLBACK_MEDIA_MUXER_CONNECTING = 0;
	public final static int CALLBACK_MEDIA_MUXER_CONNECTED = 1;
	public final static int CALLBACK_MEDIA_MUXER_STREAMING = 2;
	public final static int CALLBACK_MEDIA_MUXER_ERROR = 3;
	public final static int CALLBACK_MEDIA_MUXER_INFO = 4;
	public final static int CALLBACK_MEDIA_MUXER_END = 5;
	
	//mediamuxer_info_type
	public final static int CALLBACK_MEDIA_MUXER_INFO_ALREADY_CONNECTING = 1;
	public final static int CALLBACK_MEDIA_MUXER_INFO_PUBLISH_DELAY_TIME = 2;
	public final static int CALLBACK_MEDIA_MUXER_INFO_ALREADY_ENDING = 3;
	public final static int CALLBACK_MEDIA_MUXER_INFO_PUBLISH_REAL_BITRATE = 4;
	public final static int CALLBACK_MEDIA_MUXER_INFO_PUBLISH_REAL_FPS = 5;
	public final static int CALLBACK_MEDIA_MUXER_INFO_PUBLISH_TIME = 6;
	
	//mediamuxer_error_type
	public final static int CALLBACK_MEDIA_MUXER_ERROR_UNKNOWN = -1;
	public final static int CALLBACK_MEDIA_MUXER_ERROR_CONNECT_FAIL = 0;
	public final static int CALLBACK_MEDIA_MUXER_ERROR_MUX_FAIL = 1;
	public final static int CALLBACK_MEDIA_MUXER_ERROR_POOR_NETWORK = 2;
}
