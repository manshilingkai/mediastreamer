package android.slkmedia.mediastreamer;

public interface MediaStreamerListener {
	public abstract void onMediaStreamerConnecting();

	public abstract void onMediaStreamerConnected();

	public abstract void onMediaStreamerStreaming();
	
	public abstract void onMediaStreamerError(int errorType);

	public abstract void onMediaStreamerInfo(int infoType, int infoValue);
	
	public abstract void onMediaStreamerEnd();
}
