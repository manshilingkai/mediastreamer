package android.slkmedia.mediastreamer;

public class PublishStreamOptions {
	public boolean hasVideo = true;
	public boolean hasAudio = true;
	public boolean enableMediaCodecEncoder = false;
	public int videoWidth = 480;
	public int videoHeight = 640;
	public int videoFps = 20;
	public int bitRate = 500;
	public String publishUrl = null;
}
