package android.slkmedia.mediastreamer;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.graphics.SurfaceTexture;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.opengl.EGL14;
import android.opengl.EGLContext;
import android.opengl.GLES20;
import android.os.Build;
import android.slkmedia.mediastreamer.gpuimage.GPUImageAmaroFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageAntiqueFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBeautyFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBlackCatFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBrannanFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBrooklynFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageCoolFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageCrayonFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageN1977Filter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageRGBFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageRotationMode;
import android.slkmedia.mediastreamer.gpuimage.GPUImageSketchFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageUtils;
import android.slkmedia.mediastreamer.gpuimage.OpenGLUtils;
import android.slkmedia.mediastreamer.gpuimage.TextureRotationUtil;
import android.slkmedia.mediastreamer.gpuimage.GPUImageFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageInputFilter;
import android.slkmedia.mediastreamer.SLKMediaMuxer.OutputFormat;
import android.slkmedia.mediastreamer.egl.EGL;
import android.util.Log;
import android.view.Surface;

public class EncodeSurfaceCore {
	private static final String TAG = "EncodeSurfaceCore";
	    
    public EncodeSurfaceCore()
    {
    }
	
	///////////////////////////////////////////////////////////// GPUImage[GL] //////////////////////////////////////////////////////////////

	private FloatBuffer mGLCubeBuffer;
	private FloatBuffer mGLTextureBuffer;
    private float[] rotatedTex;
	
	private GPUImageInputFilter mInputFilter = null;
	private GPUImageFilter mWorkFilter = null;
		
    private void initGPUImageParam(int videoScalingMode, int filterType, String filterDir)
    {
		mGLCubeBuffer = ByteBuffer.allocateDirect(TextureRotationUtil.CUBE.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLCubeBuffer.put(TextureRotationUtil.CUBE).position(0);

        rotatedTex = new float[8];
        TextureRotationUtil.calculateCropTextureCoordinates(GPUImageRotationMode.kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        
        mGLTextureBuffer = ByteBuffer.allocateDirect(8 * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLTextureBuffer.put(rotatedTex).position(0);
    	
        mInputFilter = new GPUImageInputFilter();

		mVideoScalingMode = videoScalingMode;
		rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateLeft;
        
		chooseWorkFilter(filterType, filterDir);
    }
    
    private void chooseWorkFilter(int type, String filterDir)
    {
    	if(mWorkFilter!=null)
    	{
    		mWorkFilter.destroy();
    		mWorkFilter = null;
    	}
    	
        switch (type) {
        case GPUImageUtils.FILTER_SKETCH:
        	mWorkFilter = new GPUImageSketchFilter();
        	mWorkFilter.init();
            break;
        case GPUImageUtils.FILTER_AMARO:
        	mWorkFilter = new GPUImageAmaroFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_ANTIQUE:
        	mWorkFilter = new GPUImageAntiqueFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BEAUTY:
        	mWorkFilter = new GPUImageBeautyFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BLACKCAT:
        	mWorkFilter = new GPUImageBlackCatFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BRANNAN:
        	mWorkFilter = new GPUImageBrannanFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BROOKLYN:
        	mWorkFilter = new GPUImageBrooklynFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_COOL:
        	mWorkFilter = new GPUImageCoolFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_CRAYON:
        	mWorkFilter = new GPUImageCrayonFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_N1977:
        	mWorkFilter = new GPUImageN1977Filter(filterDir);
        	mWorkFilter.init();
        	break;
        default:
            mWorkFilter = new GPUImageRGBFilter();
            mWorkFilter.init();
            break;
        }
    }
    
	private boolean isGPUImageWorkerOpened = false;
	private void openGPUImageWorker()
	{
    	mInputFilter.init();
        mWorkFilter.init();
        isOutputSizeUpdated = true;
	}
	
	private void closeGPUImageWorker()
	{	
    	mInputFilter.destroy();
    	mWorkFilter.destroy();
        isOutputSizeUpdated = false;
	}
	
	private float[] mTransformMatrix = new float[16];
	private boolean onDrawFrame() {		
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
		if(mInputTextureId == OpenGLUtils.NO_TEXTURE || mInputSurfaceTexture==null) return false;
		if(mVideoWidth<=0 || mVideoHeight<=0) return false;
		if(mSurfaceWidth<=0 || mSurfaceHeight<=0) return false;
		
		mInputSurfaceTexture.getTransformMatrix(mTransformMatrix);
		mInputFilter.setTextureTransformMatrix(mTransformMatrix);
        int inputTextureID = mInputFilter.onDrawToTexture(mInputTextureId, mVideoWidth, mVideoHeight);
        if(inputTextureID == OpenGLUtils.NO_TEXTURE) return false;
		
        int displayWidth = (int) (mSurfaceWidth*mVideoScaleRate);
        int displayHeight = (int) (mSurfaceHeight*mVideoScaleRate);
        int displayX = (mSurfaceWidth-displayWidth)/2;
        int displayY = (mSurfaceHeight-displayHeight)/2;
		// draw to display
        if(mVideoScalingMode == GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT)
        {
        	ScaleAspectFit(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
        }else if(mVideoScalingMode == GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING){
        	ScaleAspectFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
        }else{
        	ScaleToFill(rotationModeForWorkFilter, displayX, displayY, displayWidth, displayHeight, mVideoWidth, mVideoHeight);
        }
		mWorkFilter.onDrawFrame(inputTextureID, mGLCubeBuffer, mGLTextureBuffer);
		return true;
	}
	
	private void switchFilter(int type, String filterDir)
	{
		chooseWorkFilter(type, filterDir);
    
        isOutputSizeUpdated = true;
	}
	
	private int outputWidth = -1;
	private int outputHeight = -1;
	private boolean isOutputSizeUpdated = false;
	private void ScaleToFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
	{
	    int src_width;
	    int src_height;
	    
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
	    {
	        src_width = videoHeight;
	        src_height = videoWidth;
	    }else{
	        src_width = videoWidth;
	        src_height = videoHeight;
	    }
	    
	    videoWidth = src_width;
	    videoHeight = src_height;
	    
	    if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
	        outputWidth = videoWidth;
	        outputHeight = videoHeight;
	        
	        isOutputSizeUpdated = true;
	    }
	    
	    if (isOutputSizeUpdated) {
	        isOutputSizeUpdated = false;
	        mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
	    }
	    	    
	    GLES20.glClearColor(0.f, 0.f, 0.f, 0.f);
	    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	    
	    GLES20.glViewport(displayX, displayY, displayWidth, displayHeight);
	    
	    TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
	    mGLTextureBuffer.put(rotatedTex).position(0);
	}
	
    private void ScaleAspectFit(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {    	
        int x = 0;
        int y = 0;
        int w = 0;
        int h = 0;
        
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        videoWidth = src_width;
        videoHeight = src_height;
        
        if(displayWidth*videoHeight>videoWidth*displayHeight)
        {
            int viewPortVideoWidth = videoWidth*displayHeight/videoHeight;
            int viewPortVideoHeight = displayHeight;
            
            x = displayX+(displayWidth - viewPortVideoWidth)/2;
            y = displayY;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }else
        {
            int viewPortVideoWidth = displayWidth;
            int viewPortVideoHeight = videoHeight*displayWidth/videoWidth;
            
            x = displayX;
            y = displayY+(displayHeight - viewPortVideoHeight)/2;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }
        
        if (outputWidth!=videoWidth || outputHeight!=videoHeight) {
            outputWidth = videoWidth;
            outputHeight = videoHeight;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
                
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
                
        GLES20.glViewport(x, y, w, h);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
    
    //LayoutModeScaleAspectFill
    private void ScaleAspectFill(GPUImageRotationMode rotationMode, int displayX, int displayY, int displayWidth, int displayHeight, int videoWidth, int videoHeight)
    {    	
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = videoHeight;
            src_height = videoWidth;
        }else{
            src_width = videoWidth;
            src_height = videoHeight;
        }
        
        int dst_width = displayWidth;
        int dst_height = displayHeight;
        
        int crop_x;
        int crop_y;
        
        int crop_width;
        int crop_height;
        
        if(src_width*dst_height>dst_width*src_height)
        {
            crop_width = dst_width*src_height/dst_height;
            crop_height = src_height;
            
            crop_x = (src_width - crop_width)/2;
            crop_y = 0;

        }else if(src_width*dst_height<dst_width*src_height)
        {
            crop_width = src_width;
            crop_height = dst_height*src_width/dst_width;
            
            crop_x = 0;
            crop_y = (src_height - crop_height)/2;
        }else {
            crop_width = src_width;
            crop_height = src_height;
            crop_x = 0;
            crop_y = 0;
        }
        
        float minX = (float)crop_x/(float)src_width;
        float minY = (float)crop_y/(float)src_height;
        float maxX = 1.0f - minX;
        float maxY = 1.0f - minY;
        
        if (outputWidth!=crop_width || outputHeight!=crop_height) {
            outputWidth = crop_width;
            outputHeight = crop_height;
            
            isOutputSizeUpdated = true;
        }
        
        if (isOutputSizeUpdated) {
            isOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(outputWidth, outputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(displayX, displayY, displayWidth, displayHeight);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
    private boolean isRendering = false;
    
	private EGLContext mSharedEGLContext = EGL14.EGL_NO_CONTEXT;
	private SurfaceTexture mInputSurfaceTexture = null;
	private int mInputTextureId = OpenGLUtils.NO_TEXTURE;
	private int mVideoWidth = 0, mVideoHeight = 0;
	private int mSurfaceWidth = 0, mSurfaceHeight = 0;
	private VideoOptions mVideoOptions = null;
	private AudioOptions mAudioOptions = null;
	private String mPublishUrl = null;
	private MediaStreamerListener mMediaStreamerListener = null;
	public void startRender(EGLContext sharedContext, SurfaceTexture inputSurfaceTexture, int inputTextureId, int inputWidth, int inputHeight, VideoOptions videoOptions, AudioOptions audioOptions, String publishUrl,
			int videoScalingMode, int filterType, String filterDir, MediaStreamerListener mediaStreamerListener)
	{
		if(isRendering) return;
		
		mSharedEGLContext = sharedContext;
		mInputSurfaceTexture = inputSurfaceTexture;
		mInputTextureId = inputTextureId;
		mVideoWidth = inputWidth;
		mVideoHeight = inputHeight;
		
		mVideoOptions = videoOptions;
		mAudioOptions = audioOptions;
		mPublishUrl = publishUrl;
		
		mSurfaceWidth = mVideoOptions.videoWidth;
		mSurfaceHeight = mVideoOptions.videoHeight;
		
		mMediaStreamerListener = mediaStreamerListener;
		
		initGPUImageParam(videoScalingMode, filterType, filterDir);
		createRenderThread();
		
		isRendering = true;
	}
	
	private Lock mLock = null;
	private Condition mCondition = null;
	private Thread mRenderThread = null;
	private boolean isBreakRenderThread = false;
	private void createRenderThread()
	{
		mLock = new ReentrantLock(); 
		mCondition = mLock.newCondition();
		
		isBreakRenderThread = false;
		mRenderThread = new Thread(mRenderRunnable);
		mRenderThread.start();
	}
	
	private EGL mEGL = null;
	private SLKMediaMuxer mSLKMediaMuxer = null;
    private VideoEncoderCore mVideoEncoderCore = null;
    private Surface mEncodeSurface = null;
    private long mBaseLinePresentationTimestampNs = 0;
    private boolean mGotBaseLinePresentationTimestamp = false;
	private Runnable mRenderRunnable = new Runnable() {
		@Override
		public void run() {
			// Start MediaMuxer
			int type = -1;
			int format = -1;
			int track = -1;
			if(mPublishUrl.startsWith("/"))
			{
				type = SLKMediaMuxer.SYSTEM_MEDIAMUXER_TYPE;
				format = OutputFormat.MUXER_OUTPUT_MPEG_4;
			}else{
				type = SLKMediaMuxer.FFMPEG_MEDIAMUXER_TYPE;
				format = OutputFormat.MUXER_OUTPUT_FLV;
			}
			
			if(mAudioOptions.hasAudio)
			{
				track = SLKMediaMuxer.AUDIO_VIDEO_TRACK;
			}else
			{
				track = SLKMediaMuxer.ONLY_VIDEO_TRACK;
			}
			
			track = SLKMediaMuxer.ONLY_VIDEO_TRACK;//todo
			
			mSLKMediaMuxer = new SLKMediaMuxer(type, mPublishUrl, format, track);
			mSLKMediaMuxer.setMediaMuxerListener(mMediaMuxerListener);
			
			// Start VideoEncoder
			mVideoEncoderCore = new VideoEncoderCore(mVideoOptions, mSLKMediaMuxer);
	    	mEncodeSurface = mVideoEncoderCore.getInputSurface();
			
	    	//Create EGL
			if(mEGL==null)
			{
				mEGL = new EGL();
				mEGL.Egl_Initialize(mSharedEGLContext, true);
				
		        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
			}
			mEGL.Egl_AttachToSurface(mEncodeSurface);
			mGotBaseLinePresentationTimestamp = false;
			
			while(true)
			{
				mLock.lock();
				if(isBreakRenderThread)
				{
					mLock.unlock();
					break;
				}
				
				if(!isGPUImageWorkerOpened)
				{
					openGPUImageWorker();

					isGPUImageWorkerOpened = true;
				}
				
				if(isSwitchFilter)
				{
					isSwitchFilter = false;
					
					switchFilter(mFilterType, mFilterDir);
				}
				
				mVideoEncoderCore.drainEncoder(false);
				boolean ret = onDrawFrame();
				if(ret) {
					if(!mGotBaseLinePresentationTimestamp)
					{
						mBaseLinePresentationTimestampNs = mInputSurfaceTexture.getTimestamp();
						mGotBaseLinePresentationTimestamp = true;
					}
					mEGL.Egl_SetPresentationTime(mInputSurfaceTexture.getTimestamp()-mBaseLinePresentationTimestampNs);
				}
				mEGL.Egl_SwapBuffers();
				
				try {
					mCondition.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				mLock.unlock();
				continue;
			}
						
			if(isGPUImageWorkerOpened)
			{
				closeGPUImageWorker();
				isGPUImageWorkerOpened = false;
			}
			
			// Release EGL
			mEGL.Egl_DetachFromSurfaceTexture();
			mEGL.Egl_Terminate();
			mEGL = null;
			
			// Release VideoEncoder
			mVideoEncoderCore.release();
			mVideoEncoderCore = null;
			
			// Release MediaMuxer
			mSLKMediaMuxer.stop();
			mSLKMediaMuxer.release();
			mSLKMediaMuxer = null;
		}
	};

	private void deleteRenderThread()
	{
		mLock.lock();
		isBreakRenderThread = true;
		mCondition.signal();
		mLock.unlock();
		
		try {
			mRenderThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void requestRender(long renderTimeStampMs)
	{
		if(isRendering)
		{
			mLock.lock();
			mCondition.signal();
			mLock.unlock();
		}
	}
	
	private int mVideoScalingMode = GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT;
	public void requestScalingMode (int mode)
	{
		if(isRendering)
		{
			mLock.lock();
			mVideoScalingMode = mode;
			mLock.unlock();	
		}
	}
	
	private float mVideoScaleRate = 1.0f;
	public void requestScaleRate (float scaleRate)
	{
		if(isRendering)
		{
			mLock.lock();
			mVideoScaleRate = scaleRate;
			mLock.unlock();	
		}
	}
	
    private GPUImageRotationMode rotationModeForWorkFilter = GPUImageRotationMode.kGPUImageNoRotation;
	public void requestGPUImageRotationMode(GPUImageRotationMode rotationMode)
	{
		if(isRendering)
		{
			mLock.lock();
    		rotationModeForWorkFilter = rotationMode;
			mLock.unlock();
		}
	}
	
	private int mFilterType = GPUImageUtils.FILTER_RGB;
	private String mFilterDir = null;
	private boolean isSwitchFilter = false;
	public void requestFilter(int type, String filterDir)
	{
		if(isRendering)
		{
			mLock.lock();
			mFilterType = type;
			mFilterDir = new String(filterDir);
			isSwitchFilter = true;
			mLock.unlock();
		}
	}
	
	public void stopRender()
	{
		if(isRendering)
		{
			deleteRenderThread();
			isRendering = false;
		}
	}
	
	public void Finalize() {
		stopRender();
	}
	
	private MediaMuxerListener mMediaMuxerListener = new MediaMuxerListener()
	{

		@Override
		public void onMediaMuxerConnecting() {
			if(mMediaStreamerListener!=null)
			{
				mMediaStreamerListener.onMediaStreamerConnecting();
			}
		}

		@Override
		public void onMediaMuxerConnected() {
			if(mMediaStreamerListener!=null)
			{
				mMediaStreamerListener.onMediaStreamerConnected();
			}
		}

		@Override
		public void onMediaMuxerStreaming() {
			if(mMediaStreamerListener!=null)
			{
				mMediaStreamerListener.onMediaStreamerStreaming();
			}
		}

		@Override
		public void onMediaMuxerError(int errorType) {
			if(mMediaStreamerListener!=null)
			{
				mMediaStreamerListener.onMediaStreamerError(errorType);
			}
		}

		@Override
		public void onMediaMuxerInfo(int infoType, int infoValue) {
			if(mMediaStreamerListener!=null)
			{
				mMediaStreamerListener.onMediaStreamerInfo(infoType, infoValue);
			}
		}

		@Override
		public void onMediaMuxerEnd() {
			if(mMediaStreamerListener!=null)
			{
				mMediaStreamerListener.onMediaStreamerEnd();
			}
		}};
}
