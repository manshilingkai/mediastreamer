package android.slkmedia.mediastreamer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.media.MediaCodec;
import android.opengl.GLES20;
import android.os.Process;
import android.slkmedia.mediastreamer.egl.EGL;
import android.slkmedia.mediastreamer.gpuimage.GPUImageAmaroFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageAntiqueFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBeautyFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBlackCatFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBrannanFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageBrooklynFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageCoolFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageCrayonFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageInputFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageN1977Filter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageRGBFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageRotationMode;
import android.slkmedia.mediastreamer.gpuimage.GPUImageSketchFilter;
import android.slkmedia.mediastreamer.gpuimage.GPUImageUtils;
import android.slkmedia.mediastreamer.gpuimage.OpenGLUtils;
import android.slkmedia.mediastreamer.gpuimage.TextureRotationUtil;
import android.util.Log;
import android.view.Surface;

public class CameraTextureCore{
	private static final String TAG = "CameraTextureCore";

	private Lock mLock = null;
	private Condition mCondition = null;
	private Thread mRenderThread = null;
	private boolean isBreakRenderThread = false;
	private EGL mEGL = null;
	private void createRenderThread()
	{
		mLock = new ReentrantLock(); 
		mCondition = mLock.newCondition();
		
		mRenderThread = new Thread(mRenderRunnable);
		mRenderThread.start();
	}
	
	private int mSourcePreviewWidth = 0, mSourcePreviewHeight = 0;
	private Runnable mRenderRunnable = new Runnable() {
		@Override
		public void run() {
			if(mEGL==null)
			{
				mEGL = new EGL();
				mEGL.Egl_Initialize();
				
		        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
			}
			
			openCamera(mDefaultCameraFacing);
			Size size = getPreviewSize();
			mSourcePreviewWidth = size.width;
			mSourcePreviewHeight = size.height;
			
			while(true)
			{
				mLock.lock();
				if(isBreakRenderThread)
				{
					mLock.unlock();
					break;
				}
				
				if(isStartEncodeSurfaceCore)
				{
					if(isGPUImageWorkerOpened)
					{
						mEncodeSurfaceCore.startRender(mEGL.getEGLContext(), mInputSurfaceTexture, mInputTextureId, mSourcePreviewWidth, mSourcePreviewHeight, mVideoOptions, mAudioOptions, mPublishUrl,
								mScalingModeForDisplayFilter, mFilterType, mFilterDir, mMediaStreamerListener);
					}
				}else{
					mEncodeSurfaceCore.stopRender();
				}
				
				if(isSwitchCamera && mInputSurfaceTexture!=null)
				{
					isSwitchCamera = false;
					switchCameraFace(mInputSurfaceTexture);
					
					Log.d(TAG, "Switch Camera Face");
				}
				
				if(isSwitchCameraFlashLight)
				{
					isSwitchCameraFlashLight = false;
					switchFlashLight();
					
					Log.d(TAG, "Switch FlashLight");
				}
				
				if(isOutputSurfaceTextureUpdated)
				{
					if(mEGL!=null)
					{
						mEGL.Egl_DetachFromSurfaceTexture();
					}
					
					isOutputSurfaceTextureUpdated = false;
				}
				
				if(mOutputSurfaceTexture==null)
				{
					try {
						mCondition.await();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					mLock.unlock();
					continue;
				}
				
				mEGL.Egl_AttachToSurfaceTexture(mOutputSurfaceTexture);

				if(!isGPUImageWorkerOpened)
				{
					openGPUImageWorker();
					startPreview(mInputSurfaceTexture);

					isGPUImageWorkerOpened = true;
					
					Log.d(TAG, "Open GPUImage Worker and Start Camera Preview");
				}
				
				if(isSwitchFilter)
				{
					isSwitchFilter = false;
					
					switchFilter(mFilterType, mFilterDir);
					
					Log.d(TAG, "Switch Filter");
				}
				
				onDrawFrame();
				
				if(mEGL!=null)
				{
					mEGL.Egl_SwapBuffers();
				}
								
				try {
					mCondition.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				mLock.unlock();
				continue;
			}
			
			mEncodeSurfaceCore.stopRender();
			isStartEncodeSurfaceCore = false;
			
			if(isGPUImageWorkerOpened)
			{
				stopPreview();
				closeGPUImageWorker();
				isGPUImageWorkerOpened = false;
				
				Log.d(TAG, "Close GPUImage Worker and Stop Preview");
			}
			
			if(mEGL!=null)
			{
				mEGL.Egl_Terminate();
				mEGL = null;
			}
			
			releaseCamera();
		}
	};

	private void deleteRenderThread()
	{
		mLock.lock();
		isBreakRenderThread = true;
		mCondition.signal();
		mLock.unlock();
		
		try {
			mRenderThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Log.d(TAG, "Finish deleteRenderThread");
	}
	
	private void requestRender()
	{
		mLock.lock();
		mCondition.signal();
		mLock.unlock();
		
		mEncodeSurfaceCore.requestRender(0);
	}
	
	private int mScalingModeForDisplayFilter = GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT;
	private void requestDisplayScalingMode (int mode)
	{
		mLock.lock();
		mScalingModeForDisplayFilter = mode;
		mCondition.signal();
		mLock.unlock();
		
		mEncodeSurfaceCore.requestScalingMode(mode);
	}
	
    private GPUImageRotationMode mRotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateRightFlipVertical;
    private GPUImageRotationMode mRotationModeForDisplayFilter = GPUImageRotationMode.kGPUImageFlipVertical;
	private void requestRotationMode(int mode)
	{
		mLock.lock();
    	if(mode==GPUImageUtils.ROTATION_LEFT)
    	{
    	}else if(mode==GPUImageUtils.ROTATION_RIGHT)
    	{
    	}else if(mode==GPUImageUtils.ROTATION_180)
    	{
    	}else
    	{
    		mRotationModeForWorkFilter = GPUImageRotationMode.kGPUImageRotateRightFlipVertical;
    		mRotationModeForDisplayFilter = GPUImageRotationMode.kGPUImageFlipVertical;
    	}
    	
		mCondition.signal();
    	
		mLock.unlock();
		
		mEncodeSurfaceCore.requestGPUImageRotationMode(mRotationModeForDisplayFilter);
	}
	
	private int mFilterType = GPUImageUtils.FILTER_RGB;
	private String mFilterDir = null;
	private boolean isSwitchFilter = false;
	private void requestFilter(int type, String filterDir)
	{
		mLock.lock();
		mFilterType = type;
		if(filterDir!=null)
		{
			mFilterDir = new String(filterDir);
		}else{
			mFilterDir = null;
		}
		
		isSwitchFilter = true;
		
		mCondition.signal();
		
		mLock.unlock();
		
		mEncodeSurfaceCore.requestFilter(type, filterDir);
	}
	
	private boolean isSwitchCameraFlashLight = false;
	private void requestSwitchCameraFlashLight()
	{
		mLock.lock();
		
		isSwitchCameraFlashLight = true;
		
		mCondition.signal();
		
		mLock.unlock();
	}
	
	private boolean isSwitchCamera = false;
	private void requestSwitchCamera()
	{
		mLock.lock();
		
		isSwitchCamera = true;
		
		mCondition.signal();
		
		mLock.unlock();
	}
	
	private boolean isStartEncodeSurfaceCore = false;
	private VideoOptions mVideoOptions = null;
	private AudioOptions mAudioOptions = null;
	private String mPublishUrl = null;
	private MediaStreamerListener mMediaStreamerListener = null;
	private void requestStartEncodeSurfaceCore(VideoOptions videoOptions, AudioOptions audioOptions, String publishUrl, MediaStreamerListener mediaStreamerListener)
	{
		mLock.lock();
		
		mVideoOptions = videoOptions;
		mAudioOptions = audioOptions;
		mPublishUrl = publishUrl;
		mMediaStreamerListener = mediaStreamerListener;
		isStartEncodeSurfaceCore = true;
		
		mCondition.signal();
		
		mLock.unlock();
	}
	
	private void requestStopEncodeSurfaceCore()
	{
		mLock.lock();
		
		isStartEncodeSurfaceCore = false;
		
		mCondition.signal();
		
		mLock.unlock();
	}
		
	///////////////////////////////////////////////////////////// GPUImage[GL] //////////////////////////////////////////////////////////////
	
	private FloatBuffer mGLCubeBuffer;
	private FloatBuffer mGLTextureBuffer;
    private float[] rotatedTex;
	
	private GPUImageInputFilter mInputFilter = null;
	private GPUImageFilter mWorkFilter = null;
	private GPUImageRGBFilter mDisplayFilter = null;
	
	private int mInputTextureId = OpenGLUtils.NO_TEXTURE;
	private SurfaceTexture mInputSurfaceTexture = null;
	
    private void initGPUImageParam()
    {
		mGLCubeBuffer = ByteBuffer.allocateDirect(TextureRotationUtil.CUBE.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLCubeBuffer.put(TextureRotationUtil.CUBE).position(0);

        rotatedTex = new float[8];
        TextureRotationUtil.calculateCropTextureCoordinates(GPUImageRotationMode.kGPUImageNoRotation, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        
        mGLTextureBuffer = ByteBuffer.allocateDirect(8 * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        mGLTextureBuffer.put(rotatedTex).position(0);
    	
        mInputFilter = new GPUImageInputFilter();
        mWorkFilter = new GPUImageRGBFilter();
        mDisplayFilter = new GPUImageRGBFilter();
    }
    
    private SurfaceTexture.OnFrameAvailableListener mOnVideoFrameAvailableListener = new SurfaceTexture.OnFrameAvailableListener()
    {
    	@Override
    	public void onFrameAvailable(SurfaceTexture surfaceTexture) {
//    		Log.d(TAG, "onFrameAvailable");
    		requestRender();
    	}
    };

	private boolean isGPUImageWorkerOpened = false;
	private void openGPUImageWorker()
	{
    	if(mInputTextureId == OpenGLUtils.NO_TEXTURE) {
    		mInputTextureId = OpenGLUtils.getExternalOESTextureID();	
    		mInputSurfaceTexture = new SurfaceTexture(mInputTextureId);
    		mInputSurfaceTexture.setOnFrameAvailableListener(mOnVideoFrameAvailableListener);
    	}
		
    	mInputFilter.init();
    	
        mWorkFilter.init();
        isWorkFilterOutputSizeUpdated = true;
        
        mDisplayFilter.init();
        isDisplayFilterOutputSizeUpdated = true;
	}
	
	private void closeGPUImageWorker()
	{
	    if(mInputTextureId != OpenGLUtils.NO_TEXTURE)
	    {
	    	GLES20.glDeleteTextures(1, new int[]{mInputTextureId}, 0);
	    	mInputTextureId = OpenGLUtils.NO_TEXTURE;
	    }
	    
	    if(mInputSurfaceTexture!=null)
	    {
	    	mInputSurfaceTexture.release();
	    	mInputSurfaceTexture = null;
	    }
		
    	mInputFilter.destroy();
    	
    	mWorkFilter.destroy();
    	isWorkFilterOutputSizeUpdated = false;
    	
    	mDisplayFilter.destroy();
    	isDisplayFilterOutputSizeUpdated = false;
    	
    	destroyFrameBufferObject();
	}

	private float[] mTransformMatrix = new float[16];
	private void onDrawFrame() {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
		if(mInputTextureId == OpenGLUtils.NO_TEXTURE || mInputSurfaceTexture==null || mInputFilter==null) return;
		if(mSourcePreviewWidth<=0 || mSourcePreviewHeight<=0) return;
		
		mInputSurfaceTexture.updateTexImage();
		mInputSurfaceTexture.getTransformMatrix(mTransformMatrix);
		mInputFilter.setTextureTransformMatrix(mTransformMatrix);
		
        int inputTextureID = mInputFilter.onDrawToTexture(mInputTextureId, mSourcePreviewWidth, mSourcePreviewHeight);
        if(inputTextureID == OpenGLUtils.NO_TEXTURE) return;
		
    	initFrameBufferObject(mTargetPreviewWidth, mTargetPreviewHeight);
		
    	//bind
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
		GLES20.glViewport(0, 0, mFrameBufferWidth, mFrameBufferHeight);
    	GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
        
        ScaleAspectFillForWorkFilter(mRotationModeForWorkFilter, mTargetPreviewWidth, mTargetPreviewHeight, mSourcePreviewWidth, mSourcePreviewHeight);
        mWorkFilter.onDrawFrame(inputTextureID, mGLCubeBuffer, mGLTextureBuffer);

        //output PixelBuffer From FBO
        mListenerLock.lock();
        if(mCameraTextureCoreListener!=null)
        {
        	ByteBuffer pixelBuffer = outputPixelBufferFromFBO();
        	if(pixelBuffer!=null)
        	{
        		mCameraTextureCoreListener.onHandleCameraFrame(pixelBuffer.array(), mFrameBufferWidth, mFrameBufferHeight);
        	}
        }
        mListenerLock.unlock();
        //
        
        //unbind
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
		int outputTextureID = mFrameBufferTextures[0];
        if(outputTextureID == OpenGLUtils.NO_TEXTURE) return;

		// Display
        if(mScalingModeForDisplayFilter == GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT)
        {
        	ScaleAspectFitForDisplayFilter(mRotationModeForDisplayFilter, mSurfaceWidth, mSurfaceHeight, mFrameBufferWidth, mFrameBufferHeight);
        }else if(mScalingModeForDisplayFilter == GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING){
        	ScaleAspectFillForDisplayFilter(mRotationModeForDisplayFilter, mSurfaceWidth, mSurfaceHeight, mFrameBufferWidth, mFrameBufferHeight);
        }else{
        	ScaleToFillForDisplayFilter(mRotationModeForDisplayFilter, mSurfaceWidth, mSurfaceHeight, mFrameBufferWidth, mFrameBufferHeight);
        }
        
        mDisplayFilter.onDrawFrame(outputTextureID, mGLCubeBuffer, mGLTextureBuffer);		
    }
	
	private void switchFilter(int type, String filterDir)
	{
    	if(mWorkFilter!=null)
    	{
    		mWorkFilter.destroy();
    		mWorkFilter = null;
    	}
    	
        switch (type) {
        case GPUImageUtils.FILTER_SKETCH:
        	mWorkFilter = new GPUImageSketchFilter();
        	mWorkFilter.init();
            break;
        case GPUImageUtils.FILTER_AMARO:
        	mWorkFilter = new GPUImageAmaroFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_ANTIQUE:
        	mWorkFilter = new GPUImageAntiqueFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BEAUTY:
        	mWorkFilter = new GPUImageBeautyFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BLACKCAT:
        	mWorkFilter = new GPUImageBlackCatFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BRANNAN:
        	mWorkFilter = new GPUImageBrannanFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_BROOKLYN:
        	mWorkFilter = new GPUImageBrooklynFilter(filterDir);
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_COOL:
        	mWorkFilter = new GPUImageCoolFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_CRAYON:
        	mWorkFilter = new GPUImageCrayonFilter();
        	mWorkFilter.init();
        	break;
        case GPUImageUtils.FILTER_N1977:
        	mWorkFilter = new GPUImageN1977Filter(filterDir);
        	mWorkFilter.init();
        	break;
        default:
            mWorkFilter = new GPUImageRGBFilter();
            mWorkFilter.init();
            break;
        }
    
        isWorkFilterOutputSizeUpdated = true;
	}
	
	private int mWorkFilterOutputWidth = -1;
	private int mWorkFilterOutputHeight = -1;
	private boolean isWorkFilterOutputSizeUpdated = false;
      
    private void ScaleAspectFillForWorkFilter(GPUImageRotationMode rotationMode, int targetWidth, int targetHeight, int sourceWidth, int sourceHeight)
    {
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = sourceHeight;
            src_height = sourceWidth;
        }else{
            src_width = sourceWidth;
            src_height = sourceHeight;
        }
        
        int dst_width = targetWidth;
        int dst_height = targetHeight;
        
        int crop_x;
        int crop_y;
        
        int crop_width;
        int crop_height;
        
        if(src_width*dst_height>dst_width*src_height)
        {
            crop_width = dst_width*src_height/dst_height;
            crop_height = src_height;
            
            crop_x = (src_width - crop_width)/2;
            crop_y = 0;
            
        }else if(src_width*dst_height<dst_width*src_height)
        {
            crop_width = src_width;
            crop_height = dst_height*src_width/dst_width;
            
            crop_x = 0;
            crop_y = (src_height - crop_height)/2;
        }else {
            crop_width = src_width;
            crop_height = src_height;
            crop_x = 0;
            crop_y = 0;
        }
        
        float minX = (float)crop_x/(float)src_width;
        float minY = (float)crop_y/(float)src_height;
        float maxX = 1.0f - minX;
        float maxY = 1.0f - minY;
        
        if (mWorkFilterOutputWidth!=crop_width || mWorkFilterOutputHeight!=crop_height) {
        	mWorkFilterOutputWidth = crop_width;
        	mWorkFilterOutputHeight = crop_height;
            
        	isWorkFilterOutputSizeUpdated = true;
        }
        
        if (isWorkFilterOutputSizeUpdated) {
        	isWorkFilterOutputSizeUpdated = false;
            mWorkFilter.onOutputSizeChanged(mWorkFilterOutputWidth, mWorkFilterOutputHeight);
        }
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }

    private static int[] mFrameBuffers = null;
    private static int[] mFrameBufferTextures = null;
    private int mFrameBufferWidth = -1;
    private int mFrameBufferHeight = -1;
    private ByteBuffer mPixelBuffer = null;

	private void initFrameBufferObject(int frameBufferWidth, int frameBufferHeight) {
		if(mFrameBuffers != null && (mFrameBufferWidth != frameBufferWidth || mFrameBufferHeight != frameBufferHeight))
			destroyFrameBufferObject();
        if (mFrameBuffers == null) {
        	mFrameBufferWidth = frameBufferWidth;
        	mFrameBufferHeight = frameBufferHeight;
        	mFrameBuffers = new int[1];
            mFrameBufferTextures = new int[1];

            GLES20.glGenFramebuffers(1, mFrameBuffers, 0);
            
            GLES20.glGenTextures(1, mFrameBufferTextures, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mFrameBufferTextures[0]);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, frameBufferWidth, frameBufferHeight, 0,
                    GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
                    GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
            GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                    GLES20.GL_TEXTURE_2D, mFrameBufferTextures[0], 0);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
            
            mPixelBuffer = ByteBuffer.allocate(mFrameBufferWidth * mFrameBufferHeight * 4);
        }
	}
	
	private void destroyFrameBufferObject() {
        if (mFrameBufferTextures != null) {
            GLES20.glDeleteTextures(1, mFrameBufferTextures, 0);
            mFrameBufferTextures = null;
        }
        if (mFrameBuffers != null) {
            GLES20.glDeleteFramebuffers(1, mFrameBuffers, 0);
            mFrameBuffers = null;
        }
        mFrameBufferWidth = -1;
        mFrameBufferHeight = -1;
        
        mPixelBuffer.clear();
        mPixelBuffer = null;
    }
	
	private ByteBuffer outputPixelBufferFromFBO()
	{
		if(mPixelBuffer!=null)
		{
			mPixelBuffer.clear();
			mPixelBuffer.order(ByteOrder.LITTLE_ENDIAN);
			mPixelBuffer.rewind();

	        GLES20.glReadPixels(0, 0, mFrameBufferWidth, mFrameBufferHeight, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, mPixelBuffer);
		}
		
		return mPixelBuffer;
	}
	
	private int mDisplayFilterOutputWidth = -1;
	private int mDisplayFilterOutputHeight = -1;
	private boolean isDisplayFilterOutputSizeUpdated = false;
	
	private void ScaleToFillForDisplayFilter(GPUImageRotationMode rotationMode, int displayWidth, int displayHeight, int inputWidth, int inputHeight)
	{
	    int src_width;
	    int src_height;
	    
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
	    {
	        src_width = inputHeight;
	        src_height = inputWidth;
	    }else{
	        src_width = inputWidth;
	        src_height = inputHeight;
	    }
	    
	    inputWidth = src_width;
	    inputHeight = src_height;
	    
	    if (mDisplayFilterOutputWidth!=inputWidth || mDisplayFilterOutputHeight!=inputHeight) {
	    	mDisplayFilterOutputWidth = inputWidth;
	    	mDisplayFilterOutputHeight = inputHeight;
	        
	    	isDisplayFilterOutputSizeUpdated = true;
	    }
	    
	    if (isDisplayFilterOutputSizeUpdated) {
	    	isDisplayFilterOutputSizeUpdated = false;
	    	mDisplayFilter.onOutputSizeChanged(mDisplayFilterOutputWidth, mDisplayFilterOutputHeight);
	    }
	    
	    GLES20.glClearColor(0.f, 0.f, 0.f, 0.f);
	    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	    
	    GLES20.glViewport(0, 0, displayWidth, displayHeight);
	    
	    TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
	    mGLTextureBuffer.put(rotatedTex).position(0);
	}
	
    private void ScaleAspectFitForDisplayFilter(GPUImageRotationMode rotationMode, int displayWidth, int displayHeight, int inputWidth, int inputHeight)
    {
        int x = 0;
        int y = 0;
        int w = 0;
        int h = 0;
        
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = inputHeight;
            src_height = inputWidth;
        }else{
            src_width = inputWidth;
            src_height = inputHeight;
        }
        
        inputWidth = src_width;
        inputHeight = src_height;
        
        if(displayWidth*inputHeight>inputWidth*displayHeight)
        {
            int viewPortVideoWidth = inputWidth*displayHeight/inputHeight;
            int viewPortVideoHeight = displayHeight;
            
            x = -1*(viewPortVideoWidth-displayWidth)/2;
            y = 0;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }else
        {
            int viewPortVideoWidth = displayWidth;
            int viewPortVideoHeight = inputHeight*displayWidth/inputWidth;
            
            x = 0;
            y = -1*(viewPortVideoHeight-displayHeight)/2;
            
            w = viewPortVideoWidth;
            h = viewPortVideoHeight;
        }
        
        if (mDisplayFilterOutputWidth!=inputWidth || mDisplayFilterOutputHeight!=inputHeight) {
        	mDisplayFilterOutputWidth = inputWidth;
        	mDisplayFilterOutputHeight = inputHeight;
            
        	isDisplayFilterOutputSizeUpdated = true;
        }
        
        if (isDisplayFilterOutputSizeUpdated) {
        	isDisplayFilterOutputSizeUpdated = false;
        	mDisplayFilter.onOutputSizeChanged(mDisplayFilterOutputWidth, mDisplayFilterOutputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(x, y, w, h);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, 0.0f, 0.0f, 1.0f, 1.0f, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
    
    //LayoutModeScaleAspectFill
    private void ScaleAspectFillForDisplayFilter(GPUImageRotationMode rotationMode, int displayWidth, int displayHeight, int inputWidth, int inputHeight)
    {
        int src_width;
        int src_height;
        
        if (rotationMode == GPUImageRotationMode.kGPUImageRotateLeft || rotationMode == GPUImageRotationMode.kGPUImageRotateRight 
        		|| rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipVertical || rotationMode == GPUImageRotationMode.kGPUImageRotateRightFlipHorizontal)
        {
            src_width = inputHeight;
            src_height = inputWidth;
        }else{
            src_width = inputWidth;
            src_height = inputHeight;
        }
        
        int dst_width = displayWidth;
        int dst_height = displayHeight;
        
        int crop_x;
        int crop_y;
        
        int crop_width;
        int crop_height;
        
        if(src_width*dst_height>dst_width*src_height)
        {
            crop_width = dst_width*src_height/dst_height;
            crop_height = src_height;
            
            crop_x = (src_width - crop_width)/2;
            crop_y = 0;
            
        }else if(src_width*dst_height<dst_width*src_height)
        {
            crop_width = src_width;
            crop_height = dst_height*src_width/dst_width;
            
            crop_x = 0;
            crop_y = (src_height - crop_height)/2;
        }else {
            crop_width = src_width;
            crop_height = src_height;
            crop_x = 0;
            crop_y = 0;
        }
        
        float minX = (float)crop_x/(float)src_width;
        float minY = (float)crop_y/(float)src_height;
        float maxX = 1.0f - minX;
        float maxY = 1.0f - minY;
        
        if (mDisplayFilterOutputWidth!=crop_width || mDisplayFilterOutputHeight!=crop_height) {
        	mDisplayFilterOutputWidth = crop_width;
        	mDisplayFilterOutputHeight = crop_height;
            
        	isDisplayFilterOutputSizeUpdated = true;
        }
        
        if (isDisplayFilterOutputSizeUpdated) {
        	isDisplayFilterOutputSizeUpdated = false;
        	mDisplayFilter.onOutputSizeChanged(mDisplayFilterOutputWidth, mDisplayFilterOutputHeight);
        }
        
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        GLES20.glViewport(0, 0, displayWidth, displayHeight);
        
        TextureRotationUtil.calculateCropTextureCoordinates(rotationMode, minX, minY, maxX, maxY, rotatedTex);
        mGLTextureBuffer.put(rotatedTex).position(0);
    }
    
	/////////////////////////////////////////// Camera ///////////////////////////////////////////////////////////////

	private static Camera mCamera = null;
	private static int mCameraID = 0;
	
	private static int mCameraFacing = Camera.CameraInfo.CAMERA_FACING_FRONT;
	
	private static boolean openCamera(int facing)
	{
		if(mCamera!=null)
		{
			releaseCamera();
		}
		
		if(facing != Camera.CameraInfo.CAMERA_FACING_BACK && facing != Camera.CameraInfo.CAMERA_FACING_FRONT)
		{
			Log.d(TAG, "unknown camera facing, only support CAMERA_FACING_BACK or CAMERA_FACING_FRONT");
			return false;
		}
		
        Camera.CameraInfo info = new Camera.CameraInfo();
        int numCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numCameras; i++) {
            Camera.getCameraInfo(i, info);
            if (info.facing == facing) {
                mCamera = Camera.open(i);
                mCameraID = i;
                break;
            }
        }

        if (mCamera == null) {
            Log.d(TAG, "Unable to open camera with facing "+String.valueOf(facing));
            return false;
        }
        
        setDefaultParameters();
        
        mCameraFacing = facing;
        
        return true;
	}
    
	private static void setDefaultParameters(){
		
		if(mCamera!=null)
		{
			Parameters parameters = mCamera.getParameters();
			
			parameters.setPreviewFormat(ImageFormat.NV21);
			
			if (parameters.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
	            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
	        }else if(parameters.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
	        	parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
	        }
			
			Camera.Size previewSize = getPreferredPreviewSize();
			parameters.setPreviewSize(previewSize.width, previewSize.height);
			
			if(hasFlashLight)
			{
				if(isFlashLightEnabled)
				{
					parameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
				}else{
					parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
				}
			}
			
			mCamera.setParameters(parameters);
		}
	}
	
	private static Size getPreferredPreviewSize(){
		if(mCamera != null){
			
	        Camera.Size ppsfv = mCamera.getParameters().getPreferredPreviewSizeForVideo();
	        if (ppsfv != null) {
	        	return ppsfv;
	        }
			
			List<Size> sizes = mCamera.getParameters().getSupportedPreviewSizes();
			Size temp = sizes.get(0);
				for(int i = 1;i < sizes.size();i ++){
					if(temp.width < sizes.get(i).width)
						temp = sizes.get(i);
			}
			return temp;
		}
		return null;
	}
	
	private static Size getPreviewSize(){
		if(mCamera!=null)
		{
			return mCamera.getParameters().getPreviewSize();	
		}
		return null;
	}
	
	private static boolean isPreviewing = false;
	private static void startPreview(SurfaceTexture surfaceTexture) {
		if(mCamera!=null)
		{
			if(isPreviewing) return;
			
			try {
				mCamera.setPreviewTexture(surfaceTexture);
				mCamera.startPreview();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			isPreviewing = true;
		}
	}
	
	private static void stopPreview() {
		if(mCamera != null)
		{
			if(!isPreviewing) return;
			
			mCamera.stopPreview();
			try {
				mCamera.setPreviewTexture(null);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			isPreviewing = false;
		}
	}
	
	private static boolean hasFlashLight = false;
	private static boolean isFlashLightEnabled = false;
	private static void switchFlashLight()
	{
		if(mCamera!=null && hasFlashLight)
		{
			isFlashLightEnabled = !isFlashLightEnabled;

			Parameters parameters = mCamera.getParameters();
			if(isFlashLightEnabled)
			{
				parameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
			}else{
				parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
			}
			
			if(isPreviewing)
			{
				mCamera.stopPreview();
				mCamera.setParameters(parameters);
				mCamera.startPreview();
			}else{
				mCamera.setParameters(parameters);
			}
		}
	}
	
	private static void switchCameraFace(SurfaceTexture surfaceTexture)
	{
		if(mCamera!=null)
		{
			int switchCameraFacing = Camera.CameraInfo.CAMERA_FACING_FRONT;
			
			if(mCameraFacing==Camera.CameraInfo.CAMERA_FACING_FRONT)
			{
				switchCameraFacing = Camera.CameraInfo.CAMERA_FACING_BACK;
			}else{
				switchCameraFacing = Camera.CameraInfo.CAMERA_FACING_FRONT;
			}
			
			boolean isNeedPreview = isPreviewing;
			releaseCamera();
			openCamera(switchCameraFacing);
			if(isNeedPreview)
			{
				startPreview(surfaceTexture);
			}
		}
	}
	
	private static void releaseCamera(){
		if(mCamera != null){
			stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}
	
    /////////////////////////////////////////// Public ///////////////////////////////////////////////////////////////
	private EncodeSurfaceCore mEncodeSurfaceCore = null;
	public CameraTextureCore(CameraOptions cameraOptions)
	{
		mEncodeSurfaceCore = new EncodeSurfaceCore();
		Setup(cameraOptions);
	}
	
	private int mDefaultCameraFacing = Camera.CameraInfo.CAMERA_FACING_FRONT;
	private int mTargetPreviewWidth = 0;
	private int mTargetPreviewHeight = 0;
	
	private Lock mListenerLock = null;

	private void Setup(CameraOptions cameraOptions) {
		if(cameraOptions.facing==CameraOptions.CAMERA_FACING_FRONT)
		{
			mDefaultCameraFacing = Camera.CameraInfo.CAMERA_FACING_FRONT;
		}else{
			mDefaultCameraFacing = Camera.CameraInfo.CAMERA_FACING_BACK;
		}
		
		mTargetPreviewWidth = cameraOptions.previewWidth;
		mTargetPreviewHeight = cameraOptions.previewHeight;
				
		hasFlashLight = cameraOptions.hasFlashLight;
		
		mListenerLock = new ReentrantLock(); 
		
		initGPUImageParam();
		createRenderThread();
	}
    
	private int mSurfaceWidth = 0, mSurfaceHeight = 0;
	private SurfaceTexture mOutputSurfaceTexture = null;
	private boolean isOutputSurfaceTextureUpdated = false;

	public void setSurfaceTexture(SurfaceTexture surface, int width,
			int height) {
		if(surface!=null && width>0 && height>0)
		{
			mLock.lock();
			mOutputSurfaceTexture = surface;
			mSurfaceWidth = width;
			mSurfaceHeight = height;
			isOutputSurfaceTextureUpdated = true;
			mCondition.signal();

			mLock.unlock();
		}else{
			mLock.lock();
			mSurfaceWidth = 0;
			mSurfaceHeight = 0;
			mOutputSurfaceTexture = null;
			isOutputSurfaceTextureUpdated = true;
			mCondition.signal();
			
			mLock.unlock();
		}

	}

	public void resizeSurfaceTexture(SurfaceTexture surface, int width,
			int height) {
		mLock.lock();
		mSurfaceWidth = width;
		mSurfaceHeight = height;
		
		mCondition.signal();

		mLock.unlock();
	}

	public void setFilter(int type, String filterDir) {
		requestFilter(type, filterDir);
	}
	
	public void setDisplayScalingMode(int mode) {
		requestDisplayScalingMode(mode);
	}

	public void setRotationMode(int mode) {
		requestRotationMode(mode);
	}
	
	private CameraTextureCoreListener mCameraTextureCoreListener = null;
	public void setListener(CameraTextureCoreListener listener)
	{
		mListenerLock.lock();
		mCameraTextureCoreListener = listener;
		mListenerLock.unlock();
	}
	
	public void switchCameraFlashLight()
	{
		requestSwitchCameraFlashLight();
	}
	
	public void switchCamera()
	{
		requestSwitchCamera();
	}
	
	/////////////////////////- Call EncodeSurfaceCore -/////////////////////////////
	public void startEncodeSurfaceCore(VideoOptions videoOptions, AudioOptions audioOptions, String publishUrl, MediaStreamerListener mediaStreamerListener)
	{
		requestStartEncodeSurfaceCore(videoOptions, audioOptions, publishUrl, mediaStreamerListener);
	}
		
	public void stopEncodeSurfaceCore()
	{
		requestStopEncodeSurfaceCore();
	}
	
	public EncodeSurfaceCore getEncodeSurfaceCore()
	{
		return mEncodeSurfaceCore;
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	
	public void Finalize() {
		deleteRenderThread();
		mEncodeSurfaceCore.Finalize();
	}
}
