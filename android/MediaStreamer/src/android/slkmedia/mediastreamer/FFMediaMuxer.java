package android.slkmedia.mediastreamer;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.annotation.TargetApi;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

public class FFMediaMuxer implements MediaMuxerInterface {
	private static final String TAG = "FFMediaMuxer";

	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}
	
	//Native Context
	private int mNativeContext;
	private static native final void Native_Init();
	
	/**
	 * Called from native code when an interesting event happens. This method
	 * just uses the EventHandler system to post the event back to the main app
	 * thread. We use a weak reference to the original MediaPlayer object so
	 * that the native code is safe from the object disappearing from underneath
	 * it. (This is the cookie passed to native_setup().)
	 */
	private static void postEventFromNative(Object ffmediamuxer_ref, int what,
			int arg1, int arg2, Object obj) {
		FFMediaMuxer ffmm = (FFMediaMuxer) ((WeakReference<?>) ffmediamuxer_ref).get();
		if (ffmm == null) {
			return;
		}

		if (ffmm.ffMediaMuxerCallbackHandler != null) {
			Message msg = ffmm.ffMediaMuxerCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}
	
	private Lock mMediaMuxerLock = null;
	private Condition mMediaMuxerCondition = null;
	private HandlerThread mHandlerThread = null;
	private Handler ffMediaMuxerCallbackHandler = null;

	private String mPublishUrl = null;
	private int mFormat = SLKMediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4;
	public FFMediaMuxer(String path, int format)
	{
		mMediaMuxerLock = new ReentrantLock(); 
		mMediaMuxerCondition = mMediaMuxerLock.newCondition();
		
		mHandlerThread = new HandlerThread("FFMediaMuxerHandlerThread");
		mHandlerThread.start();
		
		ffMediaMuxerCallbackHandler = new Handler(mHandlerThread.getLooper()) {
			@Override
			public void handleMessage(Message msg) {
				
				switch (msg.what) {
				case SLKMediaMuxer.CALLBACK_MEDIA_MUXER_CONNECTING:
					if(mediaMuxerListener!=null)
					{
						mediaMuxerListener.onMediaMuxerConnecting();
					}
					break;
				case SLKMediaMuxer.CALLBACK_MEDIA_MUXER_CONNECTED:
					if(mediaMuxerListener!=null)
					{
						mediaMuxerListener.onMediaMuxerConnected();
					}
					break;
				case SLKMediaMuxer.CALLBACK_MEDIA_MUXER_STREAMING:
					if(mediaMuxerListener!=null)
					{
						mediaMuxerListener.onMediaMuxerStreaming();
					}
					break;
				case SLKMediaMuxer.CALLBACK_MEDIA_MUXER_ERROR:
					if(mediaMuxerListener!=null)
					{
						mediaMuxerListener.onMediaMuxerError(msg.arg1);
					}
					break;
				case SLKMediaMuxer.CALLBACK_MEDIA_MUXER_INFO:
					if(mediaMuxerListener!=null)
					{
						mediaMuxerListener.onMediaMuxerInfo(msg.arg1, msg.arg2);
					}
					break;
				case SLKMediaMuxer.CALLBACK_MEDIA_MUXER_END:
					if(mediaMuxerListener!=null)
					{
						mediaMuxerListener.onMediaMuxerEnd();
					}
					break;
				default:
					break;
				}
			}
		};
		
		mPublishUrl = path;
		mFormat = format;
	}
	
	private MediaMuxerListener mediaMuxerListener = null;
	@Override
	public void setMediaMuxerListener(MediaMuxerListener listener)
	{
		mediaMuxerListener = listener;
	}
	
	private int mVideoWidth = 0;
	private int mVideoHeight = 0;
	private int mVideoFps = 0;
	private int mVideoBitrate = 0;
	private ByteBuffer mSps = null;
	private ByteBuffer mPps = null;
	
	private int mAudioSamplerate = 0;
	private int mAudioChannels = 0;
	private int mAudioBitrate = 0;
	private ByteBuffer mAsc = null;
	
	private int mTrackIndex = -1;
	private int mVideoTrackIndex = -1;
	private int mAudioTrackIndex = -1;
	@Override
	public int addTrack(MediaFormat format) {
		mMediaMuxerLock.lock();
		if(isStarted)
		{
			mMediaMuxerLock.unlock();
			return -1;
		}
		
		if(format.getString(MediaFormat.KEY_MIME).startsWith(MediaFormat.MIMETYPE_VIDEO_AVC))
		{
			if(mVideoTrackIndex==-1)
			{
				mTrackIndex++;
				mVideoTrackIndex = mTrackIndex;
			}
			
			mVideoWidth = format.getInteger(MediaFormat.KEY_WIDTH);
			mVideoHeight = format.getInteger(MediaFormat.KEY_HEIGHT);
			
			try{
				mVideoFps = format.getInteger(MediaFormat.KEY_FRAME_RATE);
			}catch(NullPointerException e)
			{
				mVideoFps = 25;
			}
			
//			mVideoBitrate = format.getInteger(MediaFormat.KEY_BIT_RATE)/1024; //bits/sec
			mVideoBitrate = 0;
			
			mSps = format.getByteBuffer("csd-0");
			mPps = format.getByteBuffer("csd-1");
			
			mMediaMuxerLock.unlock();
			return mVideoTrackIndex;
		}else if(format.getString(MediaFormat.KEY_MIME).startsWith(MediaFormat.MIMETYPE_AUDIO_AAC))
		{
			if(mAudioTrackIndex==-1)
			{
				mTrackIndex++;
				mAudioTrackIndex = mTrackIndex;
			}
			
			mAudioSamplerate = format.getInteger(MediaFormat.KEY_SAMPLE_RATE);
			mAudioChannels = format.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
//			mAudioBitrate = format.getInteger(MediaFormat.KEY_BIT_RATE)/1024; //bits/sec
			mAudioBitrate = 0;
			
			mAsc = format.getByteBuffer("csd-0");
			
			mMediaMuxerLock.unlock();
			return mAudioTrackIndex;
		}
		
		mMediaMuxerLock.unlock();
		return -1;
	}
	
	private boolean isStarted = false;

	@Override
	public void start() {
		mMediaMuxerLock.lock();
		if(isStarted)
		{
			mMediaMuxerLock.unlock();
			return;
		}
		
		boolean hasVideo = false;
		if(mVideoTrackIndex!=-1)
		{
			hasVideo = true;
		}
		boolean hasAudio = false;
		if(mAudioTrackIndex!=-1)
		{
			hasAudio = true;
		}
		
		byte[] sps = mSps.array();
		byte[] pps = mPps.array();
		byte[] asc = mAsc.array();
		Native_Start(mPublishUrl, mFormat, hasVideo, mVideoWidth, mVideoHeight, mVideoFps, mVideoBitrate, sps, sps.length, pps, pps.length,
				hasAudio, mAudioSamplerate, mAudioChannels, mAudioBitrate, asc, asc.length, new WeakReference<FFMediaMuxer>(this));
		
		isStarted = true;
		mMediaMuxerLock.unlock();
		return;
	}
	private native void Native_Start(String publishUrl, int format, boolean hasVideo, int videoWidth, int videoHeight, int videoFps, int videoBitrate, byte[] sps, int sps_len, byte[] pps, int pps_len,
			boolean hasAudio, int audioSamplerate, int audioChannels, int audioBitrate, byte[] asc, int asc_len, Object ffmediamuxer_this);

	@Override
	public void stop() {
		mMediaMuxerLock.lock();
		if(!isStarted)
		{
			mMediaMuxerLock.unlock();
			return;
		}
		
		Native_Stop();
		
		isStarted = false;
		mMediaMuxerLock.unlock();
		return;
	}
	private native void Native_Stop();

	
	public final static int VIDEO_PACKET_H264_SPS_PPS = 0;
	public final static int VIDEO_PACKET_H264_KEY_FRAME = 1;
	public final static int VIDEO_PACKET_H264_P_OR_B_FRAME = 2;
	public final static int AUDIO_PACKET_AAC_HEADER = 3;
	public final static int AUDIO_PACKET_AAC_BODY = 4;
    
	private int mVideoSampleCount = 0;
	@Override
	public void writeSampleData(int trackIndex, ByteBuffer byteBuf,
			BufferInfo bufferInfo) {
		mMediaMuxerLock.lock();
		if(!isStarted)
		{
			Log.w(TAG, "FFMediaMuxer have not Started, So MediaSample May be droped");
			mMediaMuxerLock.unlock();
			return;
		}
		if(trackIndex==mVideoTrackIndex)
		{
			mVideoSampleCount++;
			byte[] data = byteBuf.array();
			int size = bufferInfo.size;
			long pts = bufferInfo.presentationTimeUs/1000;
//			long dts = (1000/mVideoFps)*mVideoSampleCount;
			long dts = pts;
			int packetType = VIDEO_PACKET_H264_P_OR_B_FRAME;
			if((bufferInfo.flags & MediaCodec.BUFFER_FLAG_KEY_FRAME) != 0)
			{
				packetType = VIDEO_PACKET_H264_KEY_FRAME;
			}
			
			Native_WriteAVPacket(data,size,pts,dts,packetType);
		}else if(trackIndex==mAudioTrackIndex)
		{
			byte[] data = byteBuf.array();
			int size = bufferInfo.size;
			long pts = bufferInfo.presentationTimeUs/1000;
			long dts = pts;
			int packetType = AUDIO_PACKET_AAC_BODY;
			Native_WriteAVPacket(data,size,pts,dts,packetType);
		}
		mMediaMuxerLock.unlock();
	}
	private native void Native_WriteAVPacket(byte[] data, int size, long pts, long dts, int packetType);
	
	@Override
	public long getPublishDelayTimeMs()
	{
		long ret = 0;
		mMediaMuxerLock.lock();
		if(!isStarted)
		{
			mMediaMuxerLock.unlock();
			return ret;
		}
		ret = Native_GetPublishDelayTimeMs();
		mMediaMuxerLock.unlock();
		
		return ret;
	}
	private native long Native_GetPublishDelayTimeMs();
	
	private boolean isFinishAllCallbacksAndMessages = false;
	@Override
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2) public void release()
	{
		stop();
		
		ffMediaMuxerCallbackHandler.post(new Runnable() {
			@Override
			public void run() {
				ffMediaMuxerCallbackHandler.removeCallbacksAndMessages(null);
				
				mMediaMuxerLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mMediaMuxerCondition.signalAll();
				mMediaMuxerLock.unlock();

				if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR2)
				{
					mHandlerThread.quitSafely();
				}else{
					mHandlerThread.quit();
				}
			}
		});
		
		mMediaMuxerLock.lock();
		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mMediaMuxerCondition.await(1, TimeUnit.SECONDS);
			}
			
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
			mMediaMuxerLock.unlock();
		}
	}
}
