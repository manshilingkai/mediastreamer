package android.slkmedia.mediastreamer;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.os.Build;
import android.slkmedia.mediastreamer.AudioOptions;
import android.slkmedia.mediastreamer.VideoOptions;
import android.util.AttributeSet;
import android.util.Log;
import android.view.TextureView;
import android.os.Process;

public class CameraTextureView extends TextureView implements CameraTextureCoreListener, MediaStreamerListener{
	private static final String TAG = "CameraTextureView";

	public CameraTextureView(Context context) {
		super(context);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public CameraTextureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public CameraTextureView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	public CameraTextureView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		setSurfaceTextureListener(mSurfaceTextureListener);
	}

	private int mOutputSurfaceTextureWidth = 0, mOutputSurfaceTextureHeight = 0;
	private SurfaceTexture mOutputSurfaceTexture = null;
	
	private SurfaceTextureListener mSurfaceTextureListener = new SurfaceTextureListener()
	{
		@Override
		public void onSurfaceTextureAvailable(SurfaceTexture surface,
				int width, int height) {
			mOutputSurfaceTexture = surface;
			mOutputSurfaceTextureWidth = width;
			mOutputSurfaceTextureHeight = height;
			
			if(mCameraTextureCore!=null)
			{
				mCameraTextureCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
			}
		}

		@Override
		public void onSurfaceTextureSizeChanged(SurfaceTexture surface,
				int width, int height) {
			mOutputSurfaceTextureWidth = width;
			mOutputSurfaceTextureHeight = height;
			
			if(mCameraTextureCore!=null)
			{
				mCameraTextureCore.resizeSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
			}
		}

		@Override
		public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
			mOutputSurfaceTexture = null;
			mOutputSurfaceTextureWidth = 0;
			mOutputSurfaceTextureHeight = 0;
			
			if(mCameraTextureCore!=null)
			{
				mCameraTextureCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
			}
			
			return true;
		}

		@Override
		public void onSurfaceTextureUpdated(SurfaceTexture surface) {
//			Log.d(TAG, "onSurfaceTextureUpdated");
		}
	};
	
	public void initialize(CameraOptions options)
	{
		startCameraPreview(options);
	}
	
    private CameraTextureCore mCameraTextureCore = null;
	private void startCameraPreview(CameraOptions options)
	{
	    if (!hasPermission(this.getContext(), android.Manifest.permission.CAMERA)) {
	        Log.e(TAG, "CAMERA permission is missing");
	        options.hasFlashLight = false;
	    }else{
//		    if (!hasPermission(this.getContext(), android.Manifest.permission.FLASHLIGHT)) {
//		        Log.e(TAG, "FLASHLIGHT permission is missing");
//		    }
	        options.hasFlashLight = true;

			mCameraTextureCore = new CameraTextureCore(options);
			mCameraTextureCore.setSurfaceTexture(mOutputSurfaceTexture, mOutputSurfaceTextureWidth, mOutputSurfaceTextureHeight);
	    }
	}
	
	private void stopCameraPreview()
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.Finalize();
			mCameraTextureCore = null;
		}
	}
	
	public void setFilter(int type, String filterDir)
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.setFilter(type, filterDir);
		}
	}
	
	public void setDisplayScalingMode(int mode)
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.setDisplayScalingMode(mode);
		}
	}
	
	public void switchCameraFlashLight()
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.switchCameraFlashLight();
		}
	}

	public void switchCamera()
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.switchCamera();
		}
	}
	
	private CameraTextureViewListener mCameraTextureViewListener = null;
	public void setListener(CameraTextureViewListener listener)
	{
		mCameraTextureViewListener = listener;
	}
	
	private MediaStreamer mMediaStreamer = null;
	private VideoOptions videoOptions = null;
	private AudioOptions audioOptions = null;
	private static final int DEFAULT_AUDIO_BITRATE = 64;//64k
	public boolean startPublish(PublishStreamOptions options)
	{
		videoOptions = new VideoOptions();
		audioOptions = new AudioOptions();
		
		if(options.hasVideo)
		{
			if(mCameraTextureCore!=null)
			{
				videoOptions.hasVideo = true;
			}else{
				videoOptions.hasVideo = false;
			}
		}else{
			videoOptions.hasVideo = false;
		}
		
		if(videoOptions.hasVideo)
		{
			if(options.enableMediaCodecEncoder)
			{
				videoOptions.videoEncodeType = MediaStreamer.VIDEO_HARD_ENCODE;
			}else{
				videoOptions.videoEncodeType = MediaStreamer.VIDEO_SOFT_ENCODE;
			}
			videoOptions.videoWidth = options.videoWidth;
			videoOptions.videoHeight = options.videoHeight;

			videoOptions.videoFps = options.videoFps;
			videoOptions.videoProfile = 0; //0:base_line 1:main_profile 2:high_profile
			videoOptions.videoBitRate = options.bitRate-DEFAULT_AUDIO_BITRATE;
			
			if(videoOptions.videoEncodeType == MediaStreamer.VIDEO_SOFT_ENCODE)
			{
				videoOptions.videoRawType = MediaStreamer.VIDEOFRAME_RAWTYPE_I420;
			}else{
				videoOptions.videoRawType = MediaStreamer.VIDEOFRAME_RAWTYPE_NV21;
			}
			
			videoOptions.encodeMode = 1; //0:VBR or 1:CBR
			videoOptions.quality = 0;
			videoOptions.maxKeyFrameIntervalMs = 3000;
			videoOptions.bStrictCBR = true;
			videoOptions.deblockingFilterFactor = 0;
		}

		audioOptions.hasAudio = options.hasAudio;
		
		if(audioOptions.hasAudio)
		{
			audioOptions.audioSampleRate = 44100;
			audioOptions.audioNumChannels = 1;
			audioOptions.audioBitRate = DEFAULT_AUDIO_BITRATE;
		}
		
		if(!videoOptions.hasVideo && !audioOptions.hasAudio) return false;
		
		if(videoOptions.hasVideo && videoOptions.videoEncodeType == MediaStreamer.VIDEO_HARD_ENCODE)
		{
			mCameraTextureCore.startEncodeSurfaceCore(videoOptions, audioOptions, options.publishUrl, this);
		}else{
			mMediaStreamer = new MediaStreamer(this.getContext());
			mMediaStreamer.setMediaStreamerListener(this);
			mMediaStreamer.Start(videoOptions, audioOptions, options.publishUrl);
		}

		if(videoOptions.hasVideo && videoOptions.videoEncodeType == MediaStreamer.VIDEO_SOFT_ENCODE)
		{
			if(mCameraTextureCore!=null)
			{
				mCameraTextureCore.setListener(this);
			}
		}
		
		return true;
	}
	
	public void pausePublish()
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.setListener(null);
		}
		
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.Pause();
		}
	}
	
	public void resumePublish()
	{
		if(videoOptions.hasVideo && videoOptions.videoEncodeType == MediaStreamer.VIDEO_SOFT_ENCODE)
		{
			if(mCameraTextureCore!=null)
			{
				mCameraTextureCore.setListener(this);
			}
		}
		
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.Resume();
		}
	}
	
	public void stopPublish()
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.setListener(null);
		}
		
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.Stop();
			mMediaStreamer.Release();
			mMediaStreamer = null;
		}
		
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.stopEncodeSurfaceCore();
		}
		
		videoOptions = null;
		audioOptions = null;
	}
	
	public void enableAudio(boolean isEnable)
	{
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.EnableAudio(isEnable);
		}
	}
	
	public void release()
	{
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.setListener(null);
		}
		
		if(mMediaStreamer!=null)
		{
			mMediaStreamer.Stop();
			mMediaStreamer.Release();
			mMediaStreamer = null;
		}
		
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.stopEncodeSurfaceCore();
		}
		
		videoOptions = null;
		audioOptions = null;
		
		stopCameraPreview();
	}
	
	@Override
	public void onHandleCameraFrame(byte[] videoFrameData, int videoFrameWidth,
			int videoFrameHeight) {
    	if(mMediaStreamer!=null)
    	{
    		mMediaStreamer.InputPreviewFrame(videoFrameData, videoFrameWidth, videoFrameHeight, 0, MediaStreamer.VIDEOFRAME_RAWTYPE_RGBA);
    	}
	}

	@Override
	public void onMediaStreamerConnecting() {
		Log.d(TAG, "onMediaStreamerConnecting");
		if(mCameraTextureViewListener!=null)
		{
			mCameraTextureViewListener.onMediaStreamerConnecting();
		}
	}

	@Override
	public void onMediaStreamerConnected() {
		Log.d(TAG, "onMediaStreamerConnected");
		if(mCameraTextureViewListener!=null)
		{
			mCameraTextureViewListener.onMediaStreamerConnected();
		}
	}

	@Override
	public void onMediaStreamerStreaming() {
		Log.d(TAG, "onMediaStreamerStreaming");
		if(mCameraTextureViewListener!=null)
		{
			mCameraTextureViewListener.onMediaStreamerStreaming();
		}
	}

	@Override
	public void onMediaStreamerError(int errorType) {
		
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.setListener(null);
		}
		
		Log.d(TAG, "onMediaStreamerError [ErrorType : " + String.valueOf(errorType) + "]");
		if(mCameraTextureViewListener!=null)
		{
			mCameraTextureViewListener.onMediaStreamerError(errorType);
		}
	}

	@Override
	public void onMediaStreamerInfo(int infoType, int infoValue) {
		Log.d(TAG, "onMediaStreamerInfo [InfoType : " + String.valueOf(infoType) + "]" + "[infoValue : " + String.valueOf(infoValue) + "]");
		if(mCameraTextureViewListener!=null)
		{
			mCameraTextureViewListener.onMediaStreamerInfo(infoType, infoValue);
		}
	}

	@Override
	public void onMediaStreamerEnd() {
		if(mCameraTextureCore!=null)
		{
			mCameraTextureCore.setListener(null);
		}
		
		Log.d(TAG, "onMediaStreamerEnd");
		
		if(mCameraTextureViewListener!=null)
		{
			mCameraTextureViewListener.onMediaStreamerEnd();
		}
	}
	
	// Checks if the process has as specified permission or not.
	private static boolean hasPermission(Context context, String permission) {
	    return context.checkPermission(permission, Process.myPid(), Process.myUid())
	        == PackageManager.PERMISSION_GRANTED;
	}
}
