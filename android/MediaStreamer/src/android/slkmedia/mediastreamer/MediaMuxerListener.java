package android.slkmedia.mediastreamer;

public interface MediaMuxerListener {
	public abstract void onMediaMuxerConnecting();
	public abstract void onMediaMuxerConnected();
	public abstract void onMediaMuxerStreaming();
	public abstract void onMediaMuxerError(int errorType);
	public abstract void onMediaMuxerInfo(int infoType, int infoValue);
	public abstract void onMediaMuxerEnd();
}
