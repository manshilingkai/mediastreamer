package android.slkmedia.mediastreamer.utils;

import java.io.File;

public class FolderUtils {
	public static boolean isFolderExists(String strFolder) {
		File file = new File(strFolder);
		if (!file.exists()) {
			if (file.mkdirs()) {
				return true;
			} else {
				return false;

			}
		}
		return true;
	}
}
