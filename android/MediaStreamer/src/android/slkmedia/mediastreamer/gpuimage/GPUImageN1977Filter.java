package android.slkmedia.mediastreamer.gpuimage;

import android.opengl.GLES20;

public class GPUImageN1977Filter extends GPUImageFilter{
	
	private static final String N1977_FRAGMENT_SHADER = ""+
		    "precision mediump float;\n"+
		    "varying mediump vec2 textureCoordinate;\n"+
		    "uniform sampler2D inputImageTexture;\n"+
		    "uniform sampler2D inputImageTexture2;\n"+
		    "void main()\n"+
		    "{\n"+
		    "vec3 texel = texture2D(inputImageTexture, textureCoordinate).rgb;\n"+
		    "texel = vec3(texture2D(inputImageTexture2, vec2(texel.r, .16666)).r,texture2D(inputImageTexture2, vec2(texel.g, .5)).g,texture2D(inputImageTexture2, vec2(texel.b, .83333)).b);\n"+
		    "gl_FragColor = vec4(texel, 1.0);\n"+
		    "}\n";
	
	private int[] inputTextureHandles = {-1,-1};
	private int[] inputTextureUniformLocations = {-1,-1};
	
	private String mFilterDir = null;
	public GPUImageN1977Filter(String filterDir){
		super(NO_FILTER_VERTEX_SHADER,N1977_FRAGMENT_SHADER);
		mFilterDir = new String(filterDir);
	}
	
	protected void onDrawArraysAfter(){
		for(int i = 0; i < inputTextureHandles.length
				&& inputTextureHandles[i] != OpenGLUtils.NO_TEXTURE; i++){
			GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + (i+1));
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
//			GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		}
	}
	  
	protected void onDrawArraysPre(){
		for(int i = 0; i < inputTextureHandles.length 
				&& inputTextureHandles[i] != OpenGLUtils.NO_TEXTURE; i++){
			GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + (i+1) );
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, inputTextureHandles[i]);
			GLES20.glUniform1i(inputTextureUniformLocations[i], (i+1));
		}
	}
	
	protected void onInit(){
		super.onInit();
		for(int i=0; i < inputTextureUniformLocations.length; i++){
			inputTextureUniformLocations[i] = GLES20.glGetUniformLocation(mGLProgId, "inputImageTexture"+(2+i));
		}
	}
	
	protected void onInitialized(){
		super.onInitialized();
	    runOnDraw(new Runnable(){
		    public void run(){
		    	inputTextureHandles[0] = OpenGLUtils.loadTexture(mFilterDir+"/n1977map.png");
				inputTextureHandles[1] = OpenGLUtils.loadTexture(mFilterDir+"/n1977blowout.png");
		    }
	    });
	}
}
