package android.slkmedia.mediastreamer.gpuimage;

import android.opengl.GLES20;

public class GPUImageAmaroFilter extends GPUImageFilter{
	
	private static final String AMARO_FRAGMENT_SHADER = ""+
		    "precision mediump float;\n"+
		    "varying mediump vec2 textureCoordinate;\n"+
		    "uniform sampler2D inputImageTexture;\n"+
		    "uniform sampler2D inputImageTexture2;\n"+
		    "uniform sampler2D inputImageTexture3;\n"+
		    "uniform sampler2D inputImageTexture4;\n"+
		    "uniform float strength;\n"+
		    "void main()\n"+
		    "{\n"+
		    "vec4 originColor = texture2D(inputImageTexture, textureCoordinate);\n"+
		    "vec4 texel = texture2D(inputImageTexture, textureCoordinate);\n"+
		    "vec3 bbTexel = texture2D(inputImageTexture2, textureCoordinate).rgb;\n"+
		    "texel.r = texture2D(inputImageTexture3, vec2(bbTexel.r, texel.r)).r;\n"+
		    "texel.g = texture2D(inputImageTexture3, vec2(bbTexel.g, texel.g)).g;\n"+
		    "texel.b = texture2D(inputImageTexture3, vec2(bbTexel.b, texel.b)).b;\n"+
		    "vec4 mapped;\n"+
		    "mapped.r = texture2D(inputImageTexture4, vec2(texel.r, .16666)).r;\n"+
		    "mapped.g = texture2D(inputImageTexture4, vec2(texel.g, .5)).g;\n"+
		    "mapped.b = texture2D(inputImageTexture4, vec2(texel.b, .83333)).b;\n"+
		    "mapped.a = 1.0;\n"+
		    "mapped.rgb = mix(originColor.rgb, mapped.rgb, strength);\n"+
		    "gl_FragColor = mapped;\n"+
		    "}\n";
	
	private int[] inputTextureHandles = {-1,-1,-1};
	private int[] inputTextureUniformLocations = {-1,-1,-1};
	
	private String mFilterDir = null;
	public GPUImageAmaroFilter(String filterDir){
		super(NO_FILTER_VERTEX_SHADER,AMARO_FRAGMENT_SHADER);
		mFilterDir = new String(filterDir);
	}
	
	protected void onDestroy() {
        super.onDestroy();
        GLES20.glDeleteTextures(inputTextureHandles.length, inputTextureHandles, 0);
        for(int i = 0; i < inputTextureHandles.length; i++)
        	inputTextureHandles[i] = -1;
    }
	
	protected void onDrawArraysAfter(){
		for(int i = 0; i < inputTextureHandles.length
				&& inputTextureHandles[i] != OpenGLUtils.NO_TEXTURE; i++){
			GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + (i+1));
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
//			GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		}
	}
	  
	protected void onDrawArraysPre(){
		for(int i = 0; i < inputTextureHandles.length 
				&& inputTextureHandles[i] != OpenGLUtils.NO_TEXTURE; i++){
			GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + (i+1));
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, inputTextureHandles[i]);
			GLES20.glUniform1i(inputTextureUniformLocations[i], (i+1));
		}
	}
	
	protected void onInit(){
		super.onInit();
		for(int i=0; i < inputTextureUniformLocations.length; i++)
			inputTextureUniformLocations[i] = GLES20.glGetUniformLocation(mGLProgId, "inputImageTexture"+(2+i));
	}
	
	protected void onInitialized(){
		super.onInitialized();
	    runOnDraw(new Runnable(){
		    public void run(){
		    	inputTextureHandles[0] = OpenGLUtils.loadTexture(mFilterDir+"/brannan_blowout.png");
				inputTextureHandles[1] = OpenGLUtils.loadTexture(mFilterDir+"/overlaymap.png");
				inputTextureHandles[2] = OpenGLUtils.loadTexture(mFilterDir+"/amaromap.png");
		    }
	    });
	}
}
