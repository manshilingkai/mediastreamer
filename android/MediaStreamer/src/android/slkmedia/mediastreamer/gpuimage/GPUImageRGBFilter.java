package android.slkmedia.mediastreamer.gpuimage;

import android.content.Context;
import android.opengl.GLES20;

public class GPUImageRGBFilter extends GPUImageFilter{
	
	private static final String RGB_FRAGMENT_SHADER = ""+
			"precision highp float;\n" +
			"varying highp vec2 textureCoordinate;\n" +
			"uniform sampler2D inputImageTexture;\n" +
			"uniform highp float redAdjustment;\n" +
			"uniform highp float greenAdjustment;\n" +
			"uniform highp float blueAdjustment;\n" +
			"void main()\n" +
			"{\n" +
			"highp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);\n" +
			"gl_FragColor = vec4(textureColor.r * redAdjustment, textureColor.g * greenAdjustment, textureColor.b * blueAdjustment, textureColor.a);\n" +
			"}";
	
	private int mRedLocation;
	private int mGreenLocation;
	private int mBlueLocation;
	
    public GPUImageRGBFilter() {
    	super(NO_FILTER_VERTEX_SHADER, RGB_FRAGMENT_SHADER);
    }
    
    protected void onInit() {
        super.onInit();
        mRedLocation = GLES20.glGetUniformLocation(mGLProgId, "redAdjustment");
        mGreenLocation = GLES20.glGetUniformLocation(mGLProgId, "greenAdjustment");
        mBlueLocation = GLES20.glGetUniformLocation(mGLProgId, "blueAdjustment");
    }
    
    protected void onInitialized() {
    	super.onInitialized();
        setFloat(mRedLocation, 1.0f);
        setFloat(mGreenLocation, 1.0f);
        setFloat(mBlueLocation, 1.0f);
    }
}
