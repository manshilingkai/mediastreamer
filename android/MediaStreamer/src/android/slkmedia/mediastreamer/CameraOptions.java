package android.slkmedia.mediastreamer;

public class CameraOptions {
    /**
     * The facing of the camera is opposite to that of the screen.
     */
    public static final int CAMERA_FACING_BACK = 0;

    /**
     * The facing of the camera is the same as that of the screen.
     */
    public static final int CAMERA_FACING_FRONT = 1;
    
	public int facing = CAMERA_FACING_FRONT;
	
	public int previewWidth = 480;
	public int previewHeight = 640;
	
	public boolean hasFlashLight = true;
}
