package android.slkmedia.mediastreamer;

import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

import android.annotation.TargetApi;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaCodec;
import android.media.MediaRecorder.AudioSource;
import android.os.Build;
import android.os.Process;
import android.os.SystemClock;
import android.slkmedia.mediastreamer.audiocapture.AudioProcesser;
import android.util.Log;

public class EncodeAudioCore {
	  private static final boolean DEBUG = false;

	  private static final String TAG = "EncodeAudioCore";

	  // Default audio data format is PCM 16 bit per sample.
	  // Guaranteed to be supported by all devices.
	  private static final int BITS_PER_SAMPLE = 16;

	  private static final int FRAMES_PER_BUFFER = 1024;
	  
	  // We ask for a native buffer size of BUFFER_SIZE_FACTOR * (minimum required
	  // buffer size). The extra space is allocated to guard against glitches under
	  // high load.
	  private static final int BUFFER_SIZE_FACTOR = 4;

	  // The AudioRecordJavaThread is allowed to wait for successful call to join()
	  // but the wait times out afther this amount of time.
	  private static final long AUDIO_RECORD_THREAD_JOIN_TIMEOUT_MS = 2000;

	  private AudioProcesser audioProcesser = null;

	  private AudioRecord audioRecord = null;
	  private AudioRecordThread audioThread = null;

	  /**
	   * Audio thread which keeps calling ByteBuffer.read() waiting for audio
	   * to be recorded. Feeds recorded data to the native counterpart as a
	   * periodic sequence of callbacks using DataIsRecorded().
	   * This thread uses a Process.THREAD_PRIORITY_URGENT_AUDIO priority.
	   */
	  private class AudioRecordThread extends Thread {
	    private volatile boolean keepAlive = true;

	    public AudioRecordThread(String name) {
	      super(name);
	    }

	    @Override
	    public void run() {
	      Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
	      assertTrue(audioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING);

	      long lastTime = System.nanoTime();
	      
	      long baseLinePresentationTimestampNs = System.nanoTime();
	      while (keepAlive) {
	        ByteBuffer[] inputBuffers = mAudioEncoderCore.getMediaCodec().getInputBuffers();
            int audioInputBufferIndex = mAudioEncoderCore.getMediaCodec().dequeueInputBuffer(-1);
            if(audioInputBufferIndex<0)
            {
            	mAudioEncoderCore.drainEncoder(false);
            	continue;
            }
            ByteBuffer inputBuffer = inputBuffers[audioInputBufferIndex];
            inputBuffer.clear();
	        int bytesRead = audioRecord.read(inputBuffer, mBytesPerFrame*FRAMES_PER_BUFFER);
	        if (bytesRead == mBytesPerFrame*FRAMES_PER_BUFFER) {
	        	long audioPtsUs = (System.nanoTime() - baseLinePresentationTimestampNs) / 1000L;
	        	audioPtsUs -= (1000000 * FRAMES_PER_BUFFER) / mSampleRate;
	        	if(audioPtsUs<0) audioPtsUs = 0;
	        	mAudioEncoderCore.getMediaCodec().queueInputBuffer(audioInputBufferIndex, 0, bytesRead, audioPtsUs, 0);
            	mAudioEncoderCore.drainEncoder(false);
	        } else {
	          Log.e(TAG, "AudioRecord.read failed: " + bytesRead);
	          if (bytesRead == AudioRecord.ERROR_INVALID_OPERATION || bytesRead == AudioRecord.ERROR_BAD_VALUE || bytesRead == AudioRecord.ERROR) {
	            keepAlive = false;
	          }
	        }
	        if (DEBUG) {
	          long nowTime = System.nanoTime();
	          long durationInMs = TimeUnit.NANOSECONDS.toMillis((nowTime - lastTime));
	          lastTime = nowTime;
	          Log.d(TAG, "bytesRead[" + durationInMs + "] " + bytesRead);
	        }
	      }

	      try {
	        if (audioRecord != null) {
	          audioRecord.stop();
	        }
	      } catch (IllegalStateException e) {
	        Log.e(TAG, "AudioRecord.stop failed: " + e.getMessage());
	      }
	    }

	    // Stops the inner thread loop and also calls AudioRecord.stop().
	    // Does not block the calling thread.
	    public void stopThread() {
	      Log.d(TAG, "stopThread");
	      keepAlive = false;
	    }
	  }
	  
	  private AudioEncoderCore mAudioEncoderCore = null;
	  public EncodeAudioCore(AudioEncoderCore audioEncoderCore) {
	    audioProcesser = AudioProcesser.create();
	    enableBuiltInAEC(true);
	    enableBuiltInNS(true);
	    enableBuiltInAGC(true);
	    
	    mAudioEncoderCore = audioEncoderCore;
	  }

	  private boolean enableBuiltInAEC(boolean enable) {
	    Log.d(TAG, "enableBuiltInAEC(" + enable + ')');
	    if (audioProcesser == null) {
	      Log.e(TAG, "Built-in AEC is not supported on this platform");
	      return false;
	    }
	    return audioProcesser.setAEC(enable);
	  }

	  private boolean enableBuiltInNS(boolean enable) {
	    Log.d(TAG, "enableBuiltInNS(" + enable + ')');
	    if (audioProcesser == null) {
	      Log.e(TAG, "Built-in NS is not supported on this platform");
	      return false;
	    }
	    return audioProcesser.setNS(enable);
	  }
	  
	  private boolean enableBuiltInAGC(boolean enable) {
		    Log.d(TAG, "enableBuiltInAGC(" + enable + ')');
		    if (audioProcesser == null) {
		      Log.e(TAG, "Built-in AGC is not supported on this platform");
		      return false;
		    }
		    
		    return audioProcesser.setAGC(enable);
	  }

	  private int mBytesPerFrame = 0;
	  private int mSampleRate = 0;
	  private int mChannels = 0;
	  public int initRecording(int sampleRate, int channels) {
	    Log.d(TAG, "initRecording(sampleRate=" + sampleRate + ", channels=" + channels + ")");
	    if (audioRecord != null) {
	      Log.e(TAG, "InitRecording() called twice without StopRecording()");
	      return -1;
	    }
	    mSampleRate = sampleRate;
	    mChannels = channels;
	    mBytesPerFrame = channels * (BITS_PER_SAMPLE / 8);
	    
	    // Get the minimum buffer size required for the successful creation of
	    // an AudioRecord object, in byte units.
	    // Note that this size doesn't guarantee a smooth recording under load.
	    final int channelConfig = channelCountToConfiguration(channels);
	    int minBufferSize =
	        AudioRecord.getMinBufferSize(sampleRate, channelConfig, AudioFormat.ENCODING_PCM_16BIT);
	    if (minBufferSize == AudioRecord.ERROR || minBufferSize == AudioRecord.ERROR_BAD_VALUE) {
	      Log.e(TAG, "AudioRecord.getMinBufferSize failed: " + minBufferSize);
	      return -1;
	    }
	    Log.d(TAG, "AudioRecord.getMinBufferSize: " + minBufferSize);

	    // Use a larger buffer size than the minimum required when creating the
	    // AudioRecord instance to ensure smooth recording under load. It has been
	    // verified that it does not increase the actual recording latency.
	    int bufferSizeInBytes = Math.max(BUFFER_SIZE_FACTOR * minBufferSize, mBytesPerFrame*FRAMES_PER_BUFFER);
	    Log.d(TAG, "bufferSizeInBytes: " + bufferSizeInBytes);
	    try {
	      audioRecord = new AudioRecord(AudioSource.VOICE_COMMUNICATION, sampleRate, channelConfig,
	          AudioFormat.ENCODING_PCM_16BIT, bufferSizeInBytes);
	    } catch (IllegalArgumentException e) {
	      Log.e(TAG, e.getMessage());
	      releaseAudioResources();
	      return -1;
	    }
	    if (audioRecord == null || audioRecord.getState() != AudioRecord.STATE_INITIALIZED) {
	      Log.e(TAG, "Failed to create a new AudioRecord instance");
	      releaseAudioResources();
	      return -1;
	    }
	    if (audioProcesser != null) {
	    	audioProcesser.enable(audioRecord.getAudioSessionId());
	    }
	    logMainParameters();
	    logMainParametersExtended();
	    return FRAMES_PER_BUFFER;
	  }

	  public boolean startRecording() {
	    Log.d(TAG, "startRecording");
	    assertTrue(audioRecord != null);
	    assertTrue(audioThread == null);
	    try {
	      audioRecord.startRecording();
	    } catch (IllegalStateException e) {
	      Log.e(TAG, "AudioRecord.startRecording failed: " + e.getMessage());
	      return false;
	    }
	    if (audioRecord.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING) {
	      Log.e(TAG, "AudioRecord.startRecording failed");
	      return false;
	    }
	    audioThread = new AudioRecordThread("AudioRecordJavaThread");
	    audioThread.start();
	    return true;
	  }

	  public boolean stopRecording() {
	    Log.d(TAG, "stopRecording");
	    assertTrue(audioThread != null);
	    audioThread.stopThread();
	    if (!joinUninterruptibly(audioThread, AUDIO_RECORD_THREAD_JOIN_TIMEOUT_MS)) {
	      Log.e(TAG, "Join of AudioRecordJavaThread timed out");
	    }
	    audioThread = null;

	    return true;
	  }

	  public void terminateRecording() {
		  if (audioProcesser != null) {
			  audioProcesser.release();
		  }
		  releaseAudioResources();
	  }
	  
	  private static boolean joinUninterruptibly(final Thread thread, long timeoutMs) {
		    final long startTimeMs = SystemClock.elapsedRealtime();
		    long timeRemainingMs = timeoutMs;
		    boolean wasInterrupted = false;
		    while (timeRemainingMs > 0) {
		      try {
		        thread.join(timeRemainingMs);
		        break;
		      } catch (InterruptedException e) {
		        // Someone is asking us to return early at our convenience. We can't cancel this operation,
		        // but we should preserve the information and pass it along.
		        wasInterrupted = true;
		        final long elapsedTimeMs = SystemClock.elapsedRealtime() - startTimeMs;
		        timeRemainingMs = timeoutMs - elapsedTimeMs;
		      }
		    }
		    // Pass interruption information along.
		    if (wasInterrupted) {
		      Thread.currentThread().interrupt();
		    }
		    return !thread.isAlive();
	  }
	  
	  private void logMainParameters() {
	    Log.d(TAG, "AudioRecord: "
	            + "session ID: " + audioRecord.getAudioSessionId() + ", "
	            + "channels: " + audioRecord.getChannelCount() + ", "
	            + "sample rate: " + audioRecord.getSampleRate());
	  }

	  @TargetApi(23) private void logMainParametersExtended() {
	    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
	      Log.d(TAG, "AudioRecord: "
	              // The frame count of the native AudioRecord buffer.
	              + "buffer size in frames: " + audioRecord.getBufferSizeInFrames());
	    }
	  }

	  // Helper method which throws an exception  when an assertion has failed.
	  private static void assertTrue(boolean condition) {
	    if (!condition) {
	      throw new AssertionError("Expected condition to be true");
	    }
	  }

	  private int channelCountToConfiguration(int channels) {
	    return (channels == 1 ? AudioFormat.CHANNEL_IN_MONO : AudioFormat.CHANNEL_IN_STEREO);
	  }

	  // Releases the native AudioRecord resources.
	  private void releaseAudioResources() {
	    if (audioRecord != null) {
	      audioRecord.release();
	      audioRecord = null;
	    }
	  }
}
