package android.slkmedia.mediastreamer;

public interface CameraTextureCoreListener {
	public abstract void onHandleCameraFrame(byte[] videoFrameData, int videoFrameWidth, int videoFrameHeight);
}
