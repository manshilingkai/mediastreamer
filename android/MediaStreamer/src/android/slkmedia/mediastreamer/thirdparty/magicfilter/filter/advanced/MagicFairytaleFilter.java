package android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced;

import android.slkmedia.mediastreamer.thirdparty.magicfilter.MagicEngine;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.base.MagicLookupFilter;

public class MagicFairytaleFilter extends MagicLookupFilter{

	public MagicFairytaleFilter() {
		super("filter/fairy_tale.png");
	}
}
