package android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.helper;

import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicAmaroFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicAntiqueFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicBlackCatFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicBrannanFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicBrooklynFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicCalmFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicCoolFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicCrayonFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicEarlyBirdFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicEmeraldFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicEvergreenFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicFairytaleFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicFreudFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicHealthyFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicHefeFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicHudsonFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicImageAdjustFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicInkwellFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicKevinFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicLatteFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicLomoFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicN1977Filter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicNashvilleFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicNostalgiaFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicPixarFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicRiseFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicRomanceFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicSakuraFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicSierraFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicSketchFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicSkinWhitenFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicSunriseFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicSunsetFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicSutroFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicSweetsFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicTenderFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicToasterFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicValenciaFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicWaldenFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicWarmFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicWhiteCatFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.advanced.MagicXproIIFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.base.gpuimage.GPUImageBrightnessFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.base.gpuimage.GPUImageContrastFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.base.gpuimage.GPUImageExposureFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.base.gpuimage.GPUImageFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.base.gpuimage.GPUImageHueFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.base.gpuimage.GPUImageSaturationFilter;
import android.slkmedia.mediastreamer.thirdparty.magicfilter.filter.base.gpuimage.GPUImageSharpenFilter;

public class MagicFilterFactory{
	
	private static MagicFilterType filterType = MagicFilterType.NONE;
	
	public static GPUImageFilter initFilters(MagicFilterType type){
		filterType = type;
		switch (type) {
		case WHITECAT:
			return new MagicWhiteCatFilter();
		case BLACKCAT:
			return new MagicBlackCatFilter();
		case SKINWHITEN:
			return new MagicSkinWhitenFilter();
		case ROMANCE:
			return new MagicRomanceFilter();
		case SAKURA:
			return new MagicSakuraFilter();
		case AMARO:
			return new MagicAmaroFilter();
		case WALDEN:
			return new MagicWaldenFilter();
		case ANTIQUE:
			return new MagicAntiqueFilter();
		case CALM:
			return new MagicCalmFilter();
		case BRANNAN:
			return new MagicBrannanFilter();
		case BROOKLYN:
			return new MagicBrooklynFilter();
		case EARLYBIRD:
			return new MagicEarlyBirdFilter();
		case FREUD:
			return new MagicFreudFilter();
		case HEFE:
			return new MagicHefeFilter();
		case HUDSON:
			return new MagicHudsonFilter();
		case INKWELL:
			return new MagicInkwellFilter();
		case KEVIN:
			return new MagicKevinFilter();
		case LOMO:
			return new MagicLomoFilter();
		case N1977:
			return new MagicN1977Filter();
		case NASHVILLE:
			return new MagicNashvilleFilter();
		case PIXAR:
			return new MagicPixarFilter();
		case RISE:
			return new MagicRiseFilter();
		case SIERRA:
			return new MagicSierraFilter();
		case SUTRO:
			return new MagicSutroFilter();
		case TOASTER2:
			return new MagicToasterFilter();
		case VALENCIA:
			return new MagicValenciaFilter();
		case XPROII:
			return new MagicXproIIFilter();
		case EVERGREEN:
			return new MagicEvergreenFilter();
		case HEALTHY:
			return new MagicHealthyFilter();
		case COOL:
			return new MagicCoolFilter();
		case EMERALD:
			return new MagicEmeraldFilter();
		case LATTE:
			return new MagicLatteFilter();
		case WARM:
			return new MagicWarmFilter();
		case TENDER:
			return new MagicTenderFilter();
		case SWEETS:
			return new MagicSweetsFilter();
		case NOSTALGIA:
			return new MagicNostalgiaFilter();
		case FAIRYTALE:
			return new MagicFairytaleFilter();
		case SUNRISE:
			return new MagicSunriseFilter();
		case SUNSET:
			return new MagicSunsetFilter();
		case CRAYON:
			return new MagicCrayonFilter();
		case SKETCH:
			return new MagicSketchFilter();
		//image adjust
		case BRIGHTNESS:
			return new GPUImageBrightnessFilter();
		case CONTRAST:
			return new GPUImageContrastFilter();
		case EXPOSURE:
			return new GPUImageExposureFilter();
		case HUE:
			return new GPUImageHueFilter();
		case SATURATION:
			return new GPUImageSaturationFilter();
		case SHARPEN:
			return new GPUImageSharpenFilter();
		case IMAGE_ADJUST:
			return new MagicImageAdjustFilter();
		default:
			return null;
		}
	}
	
	public MagicFilterType getCurrentFilterType(){
		return filterType;
	}
}
