package android.slkmedia.mediastreamer;

public class AudioOptions {
	public boolean hasAudio = false;
	
	public int audioSampleRate = 0;
	public int audioNumChannels = 0;
	public int audioBitRate = 0;
}
