package android.slkmedia.mediaprocesser;

public interface MediaProcesserListener {
	public abstract void onMediaProcesserError(int errorType);

	public abstract void onMediaProcesserInfo(int infoType, int infoValue);
	public abstract void onMediaProcesserMediaDetailInfo(long duration, int width, int height);
	public abstract void onMediaProcesserMediaThumbnail(String imagePath);
	
	public abstract void onMediaProcesserEnd();
}
