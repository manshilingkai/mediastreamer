package android.slkmedia.mediaprocesser;

public class MediaMaterial {
	public final static int MEDIA_MATERIAL_TYPE_UNKNOWN = -1;
	public final static int MEDIA_MATERIAL_TYPE_VIDEO_AUDIO = 0;
	public final static int MEDIA_MATERIAL_TYPE_VIDEO = 1;
	public final static int MEDIA_MATERIAL_TYPE_AUDIO = 2;
	public final static int MEDIA_MATERIAL_TYPE_TEXT = 3;
	public final static int MEDIA_MATERIAL_TYPE_PNG = 4;
	public final static int MEDIA_MATERIAL_TYPE_JPEG = 5;
	
	public int iD;
	public String url;
	public int media_material_type;
	public long startPos;
	public long endPos;
	public float volume;
	public float speed;
	
	public MediaMaterial()
	{
		iD = -1;
		url = "";
		media_material_type = MEDIA_MATERIAL_TYPE_UNKNOWN;
		startPos = -1;
		endPos = -1;
		volume = 1.0f;
		speed = 1.0f;
	}
}
