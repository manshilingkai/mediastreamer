package android.slkmedia.mediaprocesser;

import java.nio.ByteBuffer;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.media.AudioFormat;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaCodec.BufferInfo;
import android.os.Build;
import android.util.Log;

public class FFMediaWriter implements MediaWriterInterface{
	private static final String TAG = "FFMediaWriter";

	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}
	
	//Native Context
	private int mNativeContext = 0;
	private static native final void Native_Init();
	
	private Lock mMediaWriterLock = null;
	private String mPublishUrl = null;
	private int mFormat = MediaWriter.OutputFormat.MUXER_OUTPUT_MPEG_4;
	public FFMediaWriter(String path, int format)
	{
		mMediaWriterLock = new ReentrantLock();
		mPublishUrl = path;
		mFormat = format;
	}
	
	public final static int AV_SAMPLE_FMT_NONE = -1;
	public final static int AV_SAMPLE_FMT_U8 = 0;          ///< unsigned 8 bits
	public final static int AV_SAMPLE_FMT_S16 = 1;         ///< signed 16 bits
	public final static int AV_SAMPLE_FMT_S32 = 2;         ///< signed 32 bits
	public final static int AV_SAMPLE_FMT_FLT = 3;         ///< float
	public final static int AV_SAMPLE_FMT_DBL = 4;         ///< double
	
	private int mVideoWidth = 0;
	private int mVideoHeight = 0;
	private int mVideoFps = 0;
	private int mVideoRotation = 0;
	private ByteBuffer mSps = null;
	private ByteBuffer mPps = null;
	
	private int mAudioSamplerate = 0;
	private int mAudioChannels = 0;
	private int mAudioSampleFormat = AV_SAMPLE_FMT_S16;
	private ByteBuffer mAsc = null;
	
	private int mTrackIndex = -1;
	private int mVideoTrackIndex = -1;
	private int mAudioTrackIndex = -1;
	public int addTrack(MediaFormat format)
	{
		mMediaWriterLock.lock();
		if(isStarted)
		{
			mMediaWriterLock.unlock();
			return -1;
		}
		
		if(format.getString(MediaFormat.KEY_MIME).startsWith(MediaFormat.MIMETYPE_VIDEO_AVC))
		{
			if(mVideoTrackIndex==-1)
			{
				mTrackIndex++;
				mVideoTrackIndex = mTrackIndex;
			}
			
			mVideoWidth = format.getInteger(MediaFormat.KEY_WIDTH);
			mVideoHeight = format.getInteger(MediaFormat.KEY_HEIGHT);
			
			try{
				mVideoFps = format.getInteger(MediaFormat.KEY_FRAME_RATE);
			}catch(NullPointerException e)
			{
				mVideoFps = 25;
			}
			
//			mVideoBitrate = format.getInteger(MediaFormat.KEY_BIT_RATE)/1024; //(bits/sec)/1024
			if(Build.VERSION.SDK_INT>=21)
			{
				try{
					mVideoRotation = format.getInteger(MediaFormat.KEY_ROTATION);
				}catch(NullPointerException e)
				{
					mVideoRotation = 0;
				}	
			}else{
				mVideoRotation = 0;
			}
			
			mSps = format.getByteBuffer("csd-0");
			mPps = format.getByteBuffer("csd-1");
			
			mMediaWriterLock.unlock();
			return mVideoTrackIndex;
		}else if(format.getString(MediaFormat.KEY_MIME).startsWith(MediaFormat.MIMETYPE_AUDIO_AAC))
		{
			if(mAudioTrackIndex==-1)
			{
				mTrackIndex++;
				mAudioTrackIndex = mTrackIndex;
			}
			
			mAudioSamplerate = format.getInteger(MediaFormat.KEY_SAMPLE_RATE);
			mAudioChannels = format.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
//			mAudioBitrate = format.getInteger(MediaFormat.KEY_BIT_RATE)/1024; //bits/sec
			if(Build.VERSION.SDK_INT>=24)
			{
				int pcm_encoding = AudioFormat.ENCODING_PCM_16BIT;
				try{
					pcm_encoding = format.getInteger("pcm-encoding");
				}catch(NullPointerException e)
				{
					pcm_encoding = AudioFormat.ENCODING_PCM_16BIT;
				}
				
				if(pcm_encoding==AudioFormat.ENCODING_PCM_8BIT)
				{
					mAudioSampleFormat = AV_SAMPLE_FMT_U8;
				}else if(pcm_encoding==AudioFormat.ENCODING_PCM_FLOAT)
				{
					mAudioSampleFormat = AV_SAMPLE_FMT_FLT;
				}else{
					mAudioSampleFormat = AV_SAMPLE_FMT_S16;
				}
			}
			mAsc = format.getByteBuffer("csd-0");
			
			mMediaWriterLock.unlock();
			return mAudioTrackIndex;
		}
		
		mMediaWriterLock.unlock();
		return -1;
	}
	
	private boolean isStarted = false;
	public boolean start() {
		boolean ret = false;
		mMediaWriterLock.lock();
		if(isStarted)
		{
			mMediaWriterLock.unlock();
			return true;
		}
		
		boolean hasVideo = false;
		if(mVideoTrackIndex!=-1)
		{
			hasVideo = true;
		}
		boolean hasAudio = false;
		if(mAudioTrackIndex!=-1)
		{
			hasAudio = true;
		}
		
		byte[] sps = mSps.array();
		byte[] pps = mPps.array();
		byte[] asc = mAsc.array();
		ret = Native_Open(mPublishUrl, mFormat, hasVideo, mVideoWidth, mVideoHeight, mVideoFps, mVideoRotation, sps, sps.length, pps, pps.length,
				hasAudio, mAudioSamplerate, mAudioChannels, mAudioSampleFormat, asc, asc.length);
		if(!ret)
		{
			Native_Close();
		}else{
			isStarted = true;
		}
		mMediaWriterLock.unlock();
		return ret;
	}
	private native boolean Native_Open(String publishUrl, int format, boolean hasVideo, int videoWidth, int videoHeight, int videoFps, int videoRotation, byte[] sps, int sps_len, byte[] pps, int pps_len,
			boolean hasAudio, int audioSamplerate, int audioChannels, int audioSampleFormat, byte[] asc, int asc_len);
	
	public void stop() {
		mMediaWriterLock.lock();
		if(!isStarted)
		{
			mMediaWriterLock.unlock();
			return;
		}
		
		Native_Close();
		
		mVideoSampleCount = 0;
		
		isStarted = false;
		mMediaWriterLock.unlock();
		return;
	}
	private native void Native_Close();
	
	public void release()
	{
		stop();
		mPublishUrl = null;
	}
	
	public final static int VIDEO_PACKET_H264_SPS_PPS = 0;
	public final static int VIDEO_PACKET_H264_KEY_FRAME = 1;
	public final static int VIDEO_PACKET_H264_P_OR_B_FRAME = 2;
	public final static int AUDIO_PACKET_AAC_HEADER = 3;
	public final static int AUDIO_PACKET_AAC_BODY = 4;
    
	private int mVideoSampleCount = 0;
	public boolean writeSampleData(int trackIndex, ByteBuffer byteBuf,
			BufferInfo bufferInfo) {
		mMediaWriterLock.lock();
		if(!isStarted)
		{
			Log.w(TAG, "FFMediaWriter have not Started, So MediaSample May be droped");
			mMediaWriterLock.unlock();
			return false;
		}
		boolean ret = false;
		if(trackIndex==mVideoTrackIndex)
		{
			mVideoSampleCount++;
			byte[] data = byteBuf.array();
			int size = bufferInfo.size;
			long pts = bufferInfo.presentationTimeUs/1000;
//			long dts = (1000/mVideoFps)*mVideoSampleCount;
			long dts = pts;
			int packetType = VIDEO_PACKET_H264_P_OR_B_FRAME;
			if((bufferInfo.flags & MediaCodec.BUFFER_FLAG_KEY_FRAME) != 0)
			{
				packetType = VIDEO_PACKET_H264_KEY_FRAME;
			}
			
			ret = Native_WriteAVPacket(data,size,pts,dts,packetType);
		}else if(trackIndex==mAudioTrackIndex)
		{
			byte[] data = byteBuf.array();
			int size = bufferInfo.size;
			long pts = bufferInfo.presentationTimeUs/1000;
			long dts = pts;
			int packetType = AUDIO_PACKET_AAC_BODY;
			ret = Native_WriteAVPacket(data,size,pts,dts,packetType);
		}
		mMediaWriterLock.unlock();
		
		return ret;
	}
	private native boolean Native_WriteAVPacket(byte[] data, int size, long pts, long dts, int packetType);
}
