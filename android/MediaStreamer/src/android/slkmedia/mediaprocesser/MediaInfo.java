package android.slkmedia.mediaprocesser;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

public class MediaInfo {
	private static final String TAG = "MediaInfo";

	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}

	//Native Context
	private int mNativeContext;

	private static native final void Native_Init();
	private native void Native_StartWithPosition(String inputMediaFile, long startPos, long endPos, String saveDir, int width, int height, int thumbnailCount, Object mediainfo_this);
	private native void Native_Start(String inputMediaFile, String saveDir, int width, int height, int thumbnailCount, Object mediainfo_this);
	private native void Native_Stop();

	//////////////////////////////////////////////////////////////////////////
	private static void postEventFromNative(Object mediaprocesser_ref, int what,
			int arg1, int arg2, Object obj) {
		MediaInfo mi = (MediaInfo) ((WeakReference<?>) mediaprocesser_ref).get();
		if (mi == null) {
			return;
		}

		if (mi.mediaProcesserCallbackHandler != null) {
			Message msg = mi.mediaProcesserCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}	
	//////////////////////////////////////////////////////////////////////////
	private static void postDetailInfoFromNative(Object mediaprocesser_ref, long duration,
			int width, int height, Object obj) {
		MediaInfo mi = (MediaInfo) ((WeakReference<?>) mediaprocesser_ref).get();
		if (mi == null) {
			return;
		}

		if (mi.mediaProcesserCallbackHandler != null) {
			Message message = new Message();
			Bundle bundle = new Bundle();
			bundle.putLong("MediaDuration", duration);
			bundle.putInt("MediaWidth", width);
			bundle.putInt("MediaHeight", height);
			message.setData(bundle);
			message.what = MediaProcesserCommon.CALLBACK_MEDIA_PROCESSER_INFO;
			message.arg1 = MediaProcesserCommon.CALLBACK_MEDIA_PROCESSER_INFO_MEDIA_DETAIL_INFO;
			mi.mediaProcesserCallbackHandler.sendMessage(message);
		}
	}
	//////////////////////////////////////////////////////////////////////////
	private static void postThumbnailFromNative(Object mediaprocesser_ref, String imagePath, Object obj) {
		MediaInfo mi = (MediaInfo) ((WeakReference<?>) mediaprocesser_ref).get();
		if (mi == null) {
			return;
		}

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(imagePath);
		
		if (mi.mediaProcesserCallbackHandler != null) {
			Message message = new Message();
			Bundle bundle = new Bundle();
			bundle.putString("MediaThumbnail", stringBuilder.toString());
			message.setData(bundle);
			message.what = MediaProcesserCommon.CALLBACK_MEDIA_PROCESSER_INFO;
			message.arg1 = MediaProcesserCommon.CALLBACK_MEDIA_PROCESSER_INFO_MEDIA_THUMBNAIL_INFO;
			mi.mediaProcesserCallbackHandler.sendMessage(message);
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	
	private Lock mMediaProcesserLock = null;
	private Condition mMediaProcesserCondition = null;
	private HandlerThread mHandlerThread = null;
	private Handler mediaProcesserCallbackHandler = null;
	public MediaInfo()
	{
		mMediaProcesserLock = new ReentrantLock(); 
		mMediaProcesserCondition = mMediaProcesserLock.newCondition();
		
		mHandlerThread = new HandlerThread("MediaEventHandler");
		mHandlerThread.start();
		
		mediaProcesserCallbackHandler = new Handler(mHandlerThread.getLooper()) {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case MediaProcesserCommon.CALLBACK_MEDIA_PROCESSER_ERROR:
					Log.i(TAG, "CALLBACK_MEDIA_PROCESSER_ERROR");
					
					if(mediaProcesserListener!=null)
					{
						mediaProcesserListener.onMediaProcesserError(msg.arg1);
					}
					
					break;
				case MediaProcesserCommon.CALLBACK_MEDIA_PROCESSER_INFO:
					Log.i(TAG, "CALLBACK_MEDIA_PROCESSER_INFO");
					
					if(msg.arg1==MediaProcesserCommon.CALLBACK_MEDIA_PROCESSER_INFO_MEDIA_DETAIL_INFO)
					{
						Bundle bundle = msg.getData();
						
						if(mediaProcesserListener!=null && bundle!=null)
						{
							mediaProcesserListener.onMediaProcesserMediaDetailInfo(bundle.getLong("MediaDuration"), bundle.getInt("MediaWidth"), bundle.getInt("MediaHeight"));
						}
					}else if(msg.arg1==MediaProcesserCommon.CALLBACK_MEDIA_PROCESSER_INFO_MEDIA_THUMBNAIL_INFO)
					{
						Bundle bundle = msg.getData();
						
						if(mediaProcesserListener!=null && bundle!=null)
						{
							mediaProcesserListener.onMediaProcesserMediaThumbnail(bundle.getString("MediaThumbnail"));
						}
					}else{
						if(mediaProcesserListener!=null)
						{
							mediaProcesserListener.onMediaProcesserInfo(msg.arg1, msg.arg2);
						}
					}
					
					break;
				case MediaProcesserCommon.CALLBACK_MEDIA_PROCESSER_END:
					Log.i(TAG, "CALLBACK_MEDIA_PROCESSER_END");
					
					if(mediaProcesserListener!=null)
					{
						mediaProcesserListener.onMediaProcesserEnd();
					}
					
					break;
				default:
					break;
				}
			}
		};
	}
	
	private MediaProcesserListener mediaProcesserListener = null;
	public void setMediaProcesserListener(MediaProcesserListener listener)
	{
		mediaProcesserListener = listener;
	}
	
	private boolean isStarted = false;
	private WeakReference<MediaInfo> mWeakReferenceMediaInfo_this = null;
	
	public void start(String inputMediaFile, String saveDir, int width, int height, int thumbnailCount)
	{
		mMediaProcesserLock.lock();

		if(isStarted)
		{
			Log.w(TAG, "MediaInfo has started!!");
			mMediaProcesserLock.unlock();
			return;
		}
		
		if(mWeakReferenceMediaInfo_this!=null)
		{
			mWeakReferenceMediaInfo_this.clear();
			mWeakReferenceMediaInfo_this = null;
		}
		mWeakReferenceMediaInfo_this = new WeakReference<MediaInfo>(this);
		Native_Start(inputMediaFile, saveDir, width, height, thumbnailCount, mWeakReferenceMediaInfo_this);
		
		isStarted = true;
		
		mMediaProcesserLock.unlock();
	}
	
	public void start(String inputMediaFile, long startPos, long endPos, String saveDir, int width, int height, int thumbnailCount)
	{
		mMediaProcesserLock.lock();

		if(isStarted)
		{
			Log.w(TAG, "MediaInfo has started!!");
			mMediaProcesserLock.unlock();
			return;
		}
		
		if(mWeakReferenceMediaInfo_this!=null)
		{
			mWeakReferenceMediaInfo_this.clear();
			mWeakReferenceMediaInfo_this = null;
		}
		mWeakReferenceMediaInfo_this = new WeakReference<MediaInfo>(this);
		Native_StartWithPosition(inputMediaFile, startPos, endPos, saveDir, width, height, thumbnailCount, mWeakReferenceMediaInfo_this);
		
		isStarted = true;
		
		mMediaProcesserLock.unlock();
	}

	public void stop()
	{
		mMediaProcesserLock.lock();

		if(!isStarted)
		{
			Log.w(TAG, "MediaInfo has stopped");
			mMediaProcesserLock.unlock();
			return;
		}
		
		Native_Stop();
		
		if(mWeakReferenceMediaInfo_this!=null)
		{
			mWeakReferenceMediaInfo_this.clear();
			mWeakReferenceMediaInfo_this = null;
		}
		
		isStarted = false;
		
		mMediaProcesserLock.unlock();
	}
	
	
	private boolean isFinishAllCallbacksAndMessages = false;
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2) public void release()
	{
		stop();
		
		mediaProcesserCallbackHandler.post(new Runnable() {
			@Override
			public void run() {
				mediaProcesserCallbackHandler.removeCallbacksAndMessages(null);
				
				mMediaProcesserLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mMediaProcesserCondition.signalAll();
				mMediaProcesserLock.unlock();

				if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR2)
				{
					mHandlerThread.quitSafely();
				}else{
					mHandlerThread.quit();
				}
			}
		});
		
		mMediaProcesserLock.lock();
		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mMediaProcesserCondition.await(1, TimeUnit.SECONDS);
			}
			
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
			mMediaProcesserLock.unlock();
		}
	}
		
	public static long getMediaDuration(String inputMediaFile)
	{
		return syncGetMediaDuration(inputMediaFile);
	}
	
	public static boolean getCoverImageToImageFile(String inputMediaFile, int outputImageWidth, int outputImageHeight, String outputPNGFile)
	{
		int ret = syncGetCoverImageToImageFile(inputMediaFile, outputImageWidth, outputImageHeight, outputPNGFile);
		if(ret!=0) return false;
		else return true;
	}
	
	public static boolean getCoverImageToImageFileWithPosition(String inputMediaFile, long seekPositionMS, int outputImageWidth, int outputImageHeight, String outputPNGFile)
	{
		int ret = syncGetCoverImageToImageFileWithPosition(inputMediaFile, seekPositionMS, outputImageWidth, outputImageHeight, outputPNGFile);
		if(ret!=0) return false;
		else return true;
	}
	
	private static native long syncGetMediaDuration(String inputMediaFile);
	private static native int syncGetCoverImageToImageFile(String inputMediaFile, int outputImageWidth, int outputImageHeight, String outputImageFile);
	private static native int syncGetCoverImageToImageFileWithPosition(String inputMediaFile, long seekPositionMS, int outputImageWidth, int outputImageHeight, String outputImageFile);

	static public class MediaDetailInfo
	{
		public boolean hasVideo = false;
		public boolean hasAudio = false;
		
	    public int videoWidth = 0;
	    public int videoHeight = 0;
	    public int videoFps = 0;
	    
	    public int kbps = 0;
	    public long durationMs = 0;
	    
	    public int rotate = 0;
	    
	    public boolean isH264Codec = false;
	    public boolean isAACCodec = false;
	}

	public static MediaDetailInfo getMediaDetailInfo(String inputMediaFile)
	{
		MediaDetailInfo detailInfo = new MediaDetailInfo();
		
		boolean ret = syncGetMediaDetailInfo(inputMediaFile, detailInfo);
		
		if(ret) return detailInfo;
		else return null;
	}
	
	private static native boolean syncGetMediaDetailInfo(String inputMediaFile, MediaDetailInfo detailInfo);
}
