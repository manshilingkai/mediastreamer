package android.slkmedia.mediaprocesser;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.View;

public class MediaProcesserUtils {
	static {
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");
	}
	
	public static boolean qtFastStart(String inputFile, String outputFile)
	{
		int ret = MediaProcesserUtils.QTFastStart(inputFile, outputFile);
		
		if(ret!=0)
		{
			return false;
		}else {
			return true;
		}
	}
	
	public static Bitmap getCoverImage(String inputFile, int outputImageWidth, int outputImageHeight)
	{
		int outputImageDataSize = outputImageWidth*outputImageHeight*4;
		ByteBuffer imageBuffer = ByteBuffer.allocate(outputImageDataSize);
		
		int ret = MediaProcesserUtils.GetCoverImage(inputFile, outputImageWidth, outputImageHeight, outputImageDataSize, imageBuffer);
		
		if(ret!=0)
		{
			imageBuffer = null;
			
			return null;
		}else{
			
			Bitmap mBitmap = Bitmap.createBitmap(outputImageWidth, outputImageHeight, Bitmap.Config.ARGB_8888);
			mBitmap.copyPixelsFromBuffer(imageBuffer);
			
			return mBitmap;
		}
	}
	
	public static boolean getCoverImageFile(String inputFile, int outputImageWidth, int outputImageHeight, String outputFile)
	{
		Bitmap bm = MediaProcesserUtils.getCoverImage(inputFile, outputImageWidth, outputImageHeight);
		
		if(bm==null) return false;
		else {
			return saveBitmapToDisk(bm, outputFile);
		}
	}
	
	public static boolean getCoverImageData(String inputFile, int outputImageWidth, int outputImageHeight, int outputImageDataSize, ByteBuffer outputImageData)
	{
		int ret = MediaProcesserUtils.GetCoverImage(inputFile, outputImageWidth, outputImageHeight, outputImageDataSize, outputImageData);

		if(ret!=0)
		{
			return false;
		}else {
			return true;
		}
	}
	
	public static boolean saveBitmapToDisk(Bitmap inBitmap, String outputFile)
	{
        try {
            FileOutputStream fout = new FileOutputStream(outputFile);
            inBitmap.compress(Bitmap.CompressFormat.PNG, 100, fout);
            fout.flush();
            fout.close();

            inBitmap.recycle();
            
            return true;

        } catch (IOException e) {
            return false;
        }
	}
	
	public static Bitmap convertViewToBitmap(View view)
	{
		Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		view.draw(canvas);
	   return bitmap;  
	}
	
	public static boolean convertViewToPNGImageFile(View view, String outputImageFile)
	{
		return saveBitmapToDisk(convertViewToBitmap(view), outputImageFile);
	}
	
	private static native int QTFastStart(String inputFile, String outputFile);
	
	private static native int GetCoverImage(String inputFile, int outputImageWidth, int outputImageHeight, int outputImageDataSize, ByteBuffer outputImageData);
	
}
