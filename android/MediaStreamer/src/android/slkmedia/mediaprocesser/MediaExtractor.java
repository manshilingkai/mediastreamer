package android.slkmedia.mediaprocesser;

import java.nio.ByteBuffer;

import android.media.MediaFormat;

public class MediaExtractor implements MediaExtractorInterface{

	public static final int SYSTEM_MEDIAEXTRATOR_TYPE = 1;
	public static final int PRIVATE_MEDIAEXTRATOR_TYPE = 2;
	
	private MediaExtractorInterface mMediaExtractorInterface = null;
	public MediaExtractor(int type)
	{
		if(type == SYSTEM_MEDIAEXTRATOR_TYPE)
		{
			mMediaExtractorInterface = new AndroidMediaExtractor();
		}else{
			mMediaExtractorInterface = new AndroidMediaExtractor();
		}
	}
	
	public MediaExtractor()
	{
		mMediaExtractorInterface = new AndroidMediaExtractor();
	}
	
	@Override
	public void setDataSource(String path) {
		mMediaExtractorInterface.setDataSource(path);
	}

	@Override
	public int getTrackCount() {
		return mMediaExtractorInterface.getTrackCount();
	}

	@Override
	public MediaFormat getTrackFormat(int index) {
		return mMediaExtractorInterface.getTrackFormat(index);
	}

	@Override
	public void selectTrack(int index) {
		mMediaExtractorInterface.selectTrack(index);
	}

	@Override
	public void unselectTrack(int index) {
		mMediaExtractorInterface.unselectTrack(index);
	}

	@Override
	public int readSampleData(ByteBuffer byteBuf, int offset) {
		return mMediaExtractorInterface.readSampleData(byteBuf, offset);
	}

	@Override
	public int getSampleTrackIndex() {
		return mMediaExtractorInterface.getSampleTrackIndex();
	}

	@Override
	public long getSampleTime() {
		return mMediaExtractorInterface.getSampleTime();
	}

	@Override
	public int getSampleFlags() {
		return mMediaExtractorInterface.getSampleFlags();
	}

	@Override
	public void seekTo(long timeUs, int mode) {
		mMediaExtractorInterface.seekTo(timeUs, mode);
	}

	@Override
	public long getCachedDuration() {
		return mMediaExtractorInterface.getCachedDuration();
	}

	@Override
	public boolean hasCacheReachedEndOfStream() {
		return mMediaExtractorInterface.hasCacheReachedEndOfStream();
	}

	@Override
	public boolean advance() {
		return mMediaExtractorInterface.advance();
	}

	@Override
	public void release() {
		mMediaExtractorInterface.release();
	}

}
