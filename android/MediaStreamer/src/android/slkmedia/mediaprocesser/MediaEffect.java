package android.slkmedia.mediaprocesser;
import android.graphics.Color;

public class MediaEffect {
	public final static int MEDIA_EFFECT_TYPE_UNKNOWN      = -1;
	public final static int MEDIA_EFFECT_TYPE_VIDEO_AUDIO  = 0;
	public final static int MEDIA_EFFECT_TYPE_VIDEO        = 1;
	public final static int MEDIA_EFFECT_TYPE_AUDIO        = 2;
	public final static int MEDIA_EFFECT_TYPE_TEXT         = 3;
	public final static int MEDIA_EFFECT_TYPE_PNG          = 4;
	public final static int MEDIA_EFFECT_TYPE_JPEG         = 5;
	public final static int MEDIA_EFFECT_TYPE_FILTER       = 6;
	public final static int MEDIA_EFFECT_TYPE_TRANSITION   = 7;
	public final static int MEDIA_EFFECT_TYPE_WEBP         = 8;
	public final static int MEDIA_EFFECT_TYPE_GIF          = 9;

	
	public final static int GPU_IMAGE_FILTER_RGB = 0;
	public final static int GPU_IMAGE_FILTER_SKETCH = 1;
	public final static int GPU_IMAGE_FILTER_AMARO = 2;
	public final static int GPU_IMAGE_FILTER_ANTIQUE = 3;
	public final static int GPU_IMAGE_FILTER_BLACKCAT = 4;
	public final static int GPU_IMAGE_FILTER_BEAUTY = 5;
	public final static int GPU_IMAGE_FILTER_BRANNAN = 6;
	public final static int GPU_IMAGE_FILTER_N1977 = 7;
	public final static int GPU_IMAGE_FILTER_BROOKLYN = 8;
	public final static int GPU_IMAGE_FILTER_COOL = 9;
	public final static int GPU_IMAGE_FILTER_CRAYON = 10;
	
	public final static int GPU_IMAGE_TRANSITION_NONE = -1;
	public final static int GPU_IMAGE_TRANSITION_SLIDE = 0;
	
	public final static int TEXT_ANIMATION_NONE = 0;
	
	public int iD;
	public String url;
	public int media_effect_type;
	public long effect_in_pos;
	public long effect_out_pos;
	public long startPos;
	public long endPos;
	public float volume;
	public float speed;
	
	//for overlay
	public int x;
	public int y;
	public int rotation;
	public float scale;
	public boolean flipHorizontal;
	public boolean flipVertical;
	
	public int gpu_image_filter_type;
	public int gpu_image_transition_type;
	public int transition_source_id;
	
	//for text
	public String text;
	public String fontLibPath;
	public int fontSize;
	public int fontColor;
	public int leftMargin;
	public int rightMargin;
	public int topMargin;
	public int bottomMargin;
	public int lineSpace;
	public int wordSpace;
	public int text_animation_type;
	
	public MediaEffect()
	{
		iD = -1;
		url = "";
		media_effect_type = MEDIA_EFFECT_TYPE_UNKNOWN;
		effect_in_pos = -1;
		effect_out_pos = -1;
		startPos = -1;
		endPos = -1;
		volume = 1.0f;
		speed = 1.0f;
		
		//for overlay
		x = 0;
		y = 0;
		rotation = 0;
		scale = 1.0f;
		flipHorizontal = false;
		flipVertical = false;
		
		//for text
		text = "";
		fontLibPath = "";
		fontSize = 16;
		fontColor = Color.argb(255, 0, 0, 0);
		leftMargin = 0;
		rightMargin = 0;
		topMargin = 0;
		bottomMargin = 0;
		lineSpace = 0;
		wordSpace = 0;
		text_animation_type = TEXT_ANIMATION_NONE;
		
		gpu_image_filter_type = GPU_IMAGE_FILTER_RGB;
		gpu_image_transition_type = GPU_IMAGE_TRANSITION_NONE;
		transition_source_id = -1;
	}
}
