package android.slkmedia.mediaprocesser;

public class MediaProduct {
	
	public final static int MEDIA_PRODUCT_TYPE_UNKNOWN = -1;
	public final static int MEDIA_PRODUCT_TYPE_MP4 = 0;
	public final static int MEDIA_PRODUCT_TYPE_GIF = 1;
	
	public int iD;
	public String url;
	public int media_product_type;
	public boolean hasVideo;
	public int videoWidth;
	public int videoHeight;
	public boolean isAspectFit;
	public boolean hasAudio;
	public int bps;
	
	public MediaProduct()
	{
		iD = -1;
		url = "";
		media_product_type = MEDIA_PRODUCT_TYPE_UNKNOWN;
		hasVideo = false;
		videoWidth = 0;
		videoHeight = 0;
		isAspectFit = false;
		hasAudio = false;
		bps = 0;
	}
}
