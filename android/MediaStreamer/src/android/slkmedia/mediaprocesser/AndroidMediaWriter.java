package android.slkmedia.mediaprocesser;

import java.io.IOException;
import java.nio.ByteBuffer;

import android.media.MediaCodec.BufferInfo;
import android.media.MediaFormat;
import android.media.MediaMuxer;

public class AndroidMediaWriter implements MediaWriterInterface{

	//It also supports muxing B-frames in MP4 since Android Nougat
	private MediaMuxer mSystemMediaMuxer = null;
	public AndroidMediaWriter(String path, int format)
	{
		try {
			mSystemMediaMuxer = new MediaMuxer(path, format);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int addTrack(MediaFormat format) {
		if(mSystemMediaMuxer!=null)
		{
			return mSystemMediaMuxer.addTrack(format);
		}
		return -1;
	}

	@Override
	public boolean start() {
		if(mSystemMediaMuxer!=null)
		{
			mSystemMediaMuxer.start();
			return true;
		}
		return false;
	}

	@Override
	public void stop() {
		if(mSystemMediaMuxer!=null)
		{
			if(haveWrittenMediaData)
			{
				mSystemMediaMuxer.stop();
				haveWrittenMediaData = false;
			}
		}
	}

	@Override
	public void release() {
		if(mSystemMediaMuxer!=null)
		{
			mSystemMediaMuxer.release();
			mSystemMediaMuxer = null;
		}
		haveWrittenMediaData = false;
	}

	private boolean haveWrittenMediaData = false;
	@Override
	public boolean writeSampleData(int trackIndex, ByteBuffer byteBuf,
			BufferInfo bufferInfo) {
		
		if(mSystemMediaMuxer!=null)
		{
			mSystemMediaMuxer.writeSampleData(trackIndex, byteBuf, bufferInfo);
			
			if(!haveWrittenMediaData)
			{
				haveWrittenMediaData = true;
			}
			
			return true;
		}
		
		return false;
	}

}
