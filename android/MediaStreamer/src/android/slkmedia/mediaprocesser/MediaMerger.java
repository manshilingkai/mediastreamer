package android.slkmedia.mediaprocesser;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

public class MediaMerger {
	private static final String TAG = "MediaMerger";

	static {
		System.loadLibrary("anim_util");
		System.loadLibrary("ffmpeg_ypp");
		System.loadLibrary("MediaStreamer");

		Native_Init();
	}

	//Native Context
	private int mNativeContext;

	private static native final void Native_Init();
	
	private native void Native_Start(MediaMaterial inputMediaMaterials[], MediaEffect inputMediaEffects[], int mediaMergeAlgorithm, MediaProduct outputMediaProduct, Object mediastreamer_this);
	private native void Native_Pause();
	private native void Native_Resume();
	private native void Native_Stop(int mediaMergeAlgorithm);
	
	//////////////////////////////////////////////////////////////////////////
	/**
	 * Called from native code when an interesting event happens. This method
	 * just uses the EventHandler system to post the event back to the main app
	 * thread. We use a weak reference to the original MediaPlayer object so
	 * that the native code is safe from the object disappearing from underneath
	 * it. (This is the cookie passed to native_setup().)
	 */
	private static void postEventFromNative(Object mediastreamer_ref, int what,
			int arg1, int arg2, Object obj) {
		MediaMerger ms = (MediaMerger) ((WeakReference<?>) mediastreamer_ref).get();
		if (ms == null) {
			return;
		}

		if (ms.mediaProcesserCallbackHandler != null) {
			Message msg = ms.mediaProcesserCallbackHandler.obtainMessage(what, arg1, arg2, obj);
			msg.sendToTarget();
		}
	}
	//////////////////////////////////////////////////////////////////////////
		
	private Lock mMediaProcesserLock = null;
	private Condition mMediaProcesserCondition = null;
	private HandlerThread mHandlerThread = null;
	private Handler mediaProcesserCallbackHandler = null;

	public MediaMerger()
	{
		mMediaProcesserLock = new ReentrantLock(); 
		mMediaProcesserCondition = mMediaProcesserLock.newCondition();
		
		mHandlerThread = new HandlerThread("MediaEventHandler");
		mHandlerThread.start();
		
		mediaProcesserCallbackHandler = new Handler(mHandlerThread.getLooper()) {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case MediaProcesserCommon.CALLBACK_MEDIA_PROCESSER_ERROR:
					Log.i(TAG, "CALLBACK_MEDIA_PROCESSER_ERROR");
					
					if(mediaProcesserListener!=null)
					{
						mediaProcesserListener.onMediaProcesserError(msg.arg1);
					}
					
					break;
				case MediaProcesserCommon.CALLBACK_MEDIA_PROCESSER_INFO:
					Log.i(TAG, "CALLBACK_MEDIA_PROCESSER_INFO");
					if(mediaProcesserListener!=null)
					{
						mediaProcesserListener.onMediaProcesserInfo(msg.arg1, msg.arg2);
					}
					break;
				case MediaProcesserCommon.CALLBACK_MEDIA_PROCESSER_END:
					Log.i(TAG, "CALLBACK_MEDIA_PROCESSER_END");
					
					if(mediaProcesserListener!=null)
					{
						mediaProcesserListener.onMediaProcesserEnd();
					}
					
					break;
				default:
					break;
				}
			}
		};
	}
	
	private MediaProcesserListener mediaProcesserListener = null;
	public void setMediaProcesserListener(MediaProcesserListener listener)
	{
		mediaProcesserListener = listener;
	}
	
	
	private boolean isStarted = false;
	private WeakReference<MediaMerger> mWeakReferenceMediaMerger_this = null;
	private int mMediaMergeAlgorithm = 0;
	public void Start(MediaMaterial inputMediaMaterials[], MediaEffect inputMediaEffects[], int mediaMergeAlgorithm, MediaProduct outputMediaProduct)
	{
		mMediaProcesserLock.lock();

		if(isStarted)
		{
			Log.w(TAG, "MediaMerger has started!!");
			mMediaProcesserLock.unlock();
			return;
		}
		
		if(mWeakReferenceMediaMerger_this!=null)
		{
			mWeakReferenceMediaMerger_this.clear();
			mWeakReferenceMediaMerger_this = null;
		}
		mWeakReferenceMediaMerger_this = new WeakReference<MediaMerger>(this);
		Native_Start(inputMediaMaterials, inputMediaEffects, mediaMergeAlgorithm,outputMediaProduct,mWeakReferenceMediaMerger_this);
		mMediaMergeAlgorithm = mediaMergeAlgorithm;
		isStarted = true;
		
		isPaused = false;
		mMediaProcesserLock.unlock();
	}
	
	private boolean isPaused = false;
	public void Pause()
	{
		mMediaProcesserLock.lock();
		if(isStarted)
		{
			if(!isPaused)
			{
				Native_Pause();
				isPaused = true;
			}
		}
		mMediaProcesserLock.unlock();
	}
	
	public void Resume()
	{
		mMediaProcesserLock.lock();
		
		if(isStarted)
		{
			if(isPaused)
			{
				Native_Resume();
				isPaused = false;
			}
		}
		
		mMediaProcesserLock.unlock();
	}
	
	public void Stop()
	{
		mMediaProcesserLock.lock();

		if(!isStarted)
		{
			Log.w(TAG, "MediaMerger has stopped");
			mMediaProcesserLock.unlock();
			return;
		}
		
		Native_Stop(mMediaMergeAlgorithm);
		mMediaMergeAlgorithm = 0;
		
		if(mWeakReferenceMediaMerger_this!=null)
		{
			mWeakReferenceMediaMerger_this.clear();
			mWeakReferenceMediaMerger_this = null;
		}
		
		isStarted = false;
		
		mMediaProcesserLock.unlock();
	}
	

	private boolean isFinishAllCallbacksAndMessages = false;
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2) public void Release()
	{
		Stop();
		
		mediaProcesserCallbackHandler.post(new Runnable() {
			@Override
			public void run() {
				mediaProcesserCallbackHandler.removeCallbacksAndMessages(null);
				
				mMediaProcesserLock.lock();
				isFinishAllCallbacksAndMessages = true;
				mMediaProcesserCondition.signalAll();
				mMediaProcesserLock.unlock();

				if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR2)
				{
					mHandlerThread.quitSafely();
				}else{
					mHandlerThread.quit();
				}
			}
		});
		
		mMediaProcesserLock.lock();
		try {
			while(!isFinishAllCallbacksAndMessages)
			{
				mMediaProcesserCondition.await(1, TimeUnit.SECONDS);
			}
			
		} catch (InterruptedException e) {
			Log.e(TAG, e.getLocalizedMessage());
		}finally {
			mMediaProcesserLock.unlock();
		}
	}
	
	public final static int SLK_GPU_IMAGE_FILTER_RGB = 0;
	public final static int SLK_GPU_IMAGE_FILTER_SKETCH = 1;
}
