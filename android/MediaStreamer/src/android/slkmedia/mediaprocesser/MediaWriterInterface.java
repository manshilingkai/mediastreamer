package android.slkmedia.mediaprocesser;

import java.nio.ByteBuffer;

import android.media.MediaCodec;
import android.media.MediaFormat;

public interface MediaWriterInterface {
	public abstract int addTrack(MediaFormat format);
	public abstract boolean start();
	public abstract void stop();
	public abstract void release();
	public abstract boolean writeSampleData(int trackIndex, ByteBuffer byteBuf, MediaCodec.BufferInfo bufferInfo);
}
