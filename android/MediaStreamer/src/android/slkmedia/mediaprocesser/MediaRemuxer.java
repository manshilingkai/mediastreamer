package android.slkmedia.mediaprocesser;

import java.nio.ByteBuffer;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.util.Log;

public class MediaRemuxer implements Runnable{
	private static final String TAG = "MediaRemuxer";
	
	//mediaremuxer_error_type
	public final static int CALLBACK_MEDIA_REMUXER_ERROR_UNKNOWN = -1;
	public final static int CALLBACK_MEDIA_REMUXER_ERROR_NO_INPUT_TRACKs = 0;
	public final static int CALLBACK_MEDIA_REMUXER_ERROR_ADD_TRACK_FAIL = 1;
	public final static int CALLBACK_MEDIA_REMUXER_ERROR_NO_OUTPUT_TRACKs = 2;
	public final static int CALLBACK_MEDIA_REMUXER_ERROR_WRITER_START_FAIL = 3;
	public final static int CALLBACK_MEDIA_REMUXER_ERROR_MUX_FAIL = 4;
	
	//mediaremuxer_info_type
	public final static int CALLBACK_MEDIA_REMUXER_INFO_PUBLISH_TIME = 0;
	
	//////////////////////////////////////////////////////////////////////////////////////////
	
	//https://github.com/jcodeing/K-Sonic/blob/master/library-exolib/src/main/java/com/google/android/exoplayer2/video/MediaCodecVideoRenderer.java
	private int minCompressionRatio = 2;
	
	private String mInputPath = null;
	private String mOutputPath = null;
	public MediaRemuxer(String inputPath, String outputPath)
	{
		mInputPath = inputPath;
		mOutputPath = outputPath;
	}
	
	private MediaRemuxerListener mMediaRemuxerListener = null;
	public void setMediaRemuxerListener(MediaRemuxerListener listener)
	{
		mMediaRemuxerListener = listener;
	}
	
	private Lock mLock = new ReentrantLock();
	private boolean isBreak = false;

	public void interrupt()
	{
		mLock.lock();
		isBreak = true;
		mLock.unlock();
	}
	
	@Override
	public void run() {
		mLock.lock();
		if(isBreak)
		{
			mLock.unlock();
			return;
		}
		mLock.unlock();
		
		MediaExtractor extractor = new MediaExtractor();
		extractor.setDataSource(mInputPath);
		int numTracks = extractor.getTrackCount();
		if(numTracks<=0)
		{
			extractor.release();  
			
			if(mMediaRemuxerListener!=null)
			{
				mMediaRemuxerListener.onMediaRemuxerError(CALLBACK_MEDIA_REMUXER_ERROR_NO_INPUT_TRACKs);
			}
			
			return;
		}
		
		MediaWriter writer = new MediaWriter(mOutputPath, MediaWriter.OutputFormat.MUXER_OUTPUT_MPEG_4);
		int inputVideoTrackIndex = -1;
		int inputAudioTrackIndex = -1;
		int outputVideoTrackIndex = -1;
		int outputAudioTrackIndex = -1;
		
		int videoWidth = 0;
		int videoHeight = 0;
		int maxInputSize = 0;
		
		for (int i = 0; i < numTracks; ++i)
		{
			   MediaFormat format = extractor.getTrackFormat(i);
			   String mime = format.getString(MediaFormat.KEY_MIME);
			   if(mime.startsWith(MediaFormat.MIMETYPE_VIDEO_AVC) && inputVideoTrackIndex==-1)
			   {
				   extractor.selectTrack(i);
				   inputVideoTrackIndex = i;
				   
				   videoWidth = format.getInteger(MediaFormat.KEY_WIDTH);
				   videoHeight = format.getInteger(MediaFormat.KEY_HEIGHT);
				   maxInputSize = format.getInteger(MediaFormat.KEY_MAX_INPUT_SIZE);
				   
				   if(maxInputSize>videoWidth*videoHeight*3/2/minCompressionRatio)
				   {
					   maxInputSize = videoWidth*videoHeight*3/2/minCompressionRatio;
				   }
				   Log.d(TAG, "maxInputSize: "+String.valueOf(maxInputSize));
				   
				   outputVideoTrackIndex = writer.addTrack(format);
				   if(outputVideoTrackIndex==-1)
				   {						   
					   writer.release();
					   extractor.release();
					   
						if(mMediaRemuxerListener!=null)
						{
							mMediaRemuxerListener.onMediaRemuxerError(CALLBACK_MEDIA_REMUXER_ERROR_ADD_TRACK_FAIL);
						}
					   
					   return;
				   }
				   
				   continue;
			   }
			   
			   if(mime.startsWith(MediaFormat.MIMETYPE_AUDIO_AAC) && inputAudioTrackIndex==-1)
			   {
				   extractor.selectTrack(i);
				   inputAudioTrackIndex = i;
				   
				   outputAudioTrackIndex = writer.addTrack(format);
				   if(outputAudioTrackIndex==-1)
				   {
					   writer.release();
					   extractor.release();
					   
						if(mMediaRemuxerListener!=null)
						{
							mMediaRemuxerListener.onMediaRemuxerError(CALLBACK_MEDIA_REMUXER_ERROR_ADD_TRACK_FAIL);
						}
					   
					   return;
				   }

				   continue;
			   }
		}
		
		if(outputVideoTrackIndex!=-1 || outputAudioTrackIndex!=-1)
		{
			boolean ret = writer.start();
			if(!ret)
			{
				   writer.release();
				   extractor.release();
				   
					if(mMediaRemuxerListener!=null)
					{
						mMediaRemuxerListener.onMediaRemuxerError(CALLBACK_MEDIA_REMUXER_ERROR_WRITER_START_FAIL);
					}
				   
				   return;
			}
		}else{
			writer.release();
			extractor.release();
			
			if(mMediaRemuxerListener!=null)
			{
				mMediaRemuxerListener.onMediaRemuxerError(CALLBACK_MEDIA_REMUXER_ERROR_NO_OUTPUT_TRACKs);
			}
			
			return;
		}

		ByteBuffer inputBuffer = ByteBuffer.allocate(maxInputSize);
		inputBuffer.position(0);
		BufferInfo bufferInfo = new BufferInfo();
		
		long mLastSendPresentationTimeUs = 0;
		
		boolean isDone = false;
		while (true)
		{
			mLock.lock();
			if(isBreak)
			{
				mLock.unlock();
				break;
			}
			mLock.unlock();
			
			inputBuffer.position(0);
			int sampleSize = extractor.readSampleData(inputBuffer, 0);
			if(sampleSize==-1)
			{
				isDone = true;
				break;
			}
			if(sampleSize>0)
			{
				int trackIndex = extractor.getSampleTrackIndex();
				if(trackIndex==inputVideoTrackIndex)
				{
					bufferInfo.presentationTimeUs = extractor.getSampleTime();
					bufferInfo.size = sampleSize;
					bufferInfo.offset = 0;
					bufferInfo.flags = extractor.getSampleFlags();
										
					inputBuffer.position(bufferInfo.offset);
					inputBuffer.limit(bufferInfo.offset + bufferInfo.size);
					boolean ret = writer.writeSampleData(outputVideoTrackIndex, inputBuffer, bufferInfo);
					inputBuffer.clear();
					if(!ret)
					{
						if(mMediaRemuxerListener!=null)
						{
							mMediaRemuxerListener.onMediaRemuxerError(CALLBACK_MEDIA_REMUXER_ERROR_MUX_FAIL);
						}
						
						break;
					}
				}else if(trackIndex==inputAudioTrackIndex)
				{
					bufferInfo.presentationTimeUs = extractor.getSampleTime();
					bufferInfo.size = sampleSize;
					bufferInfo.offset = 0;
					bufferInfo.flags = extractor.getSampleFlags();
										
					inputBuffer.position(bufferInfo.offset);
					inputBuffer.limit(bufferInfo.offset + bufferInfo.size);
					boolean ret = writer.writeSampleData(outputAudioTrackIndex, inputBuffer, bufferInfo);
					inputBuffer.clear();
					if(!ret)
					{
						if(mMediaRemuxerListener!=null)
						{
							mMediaRemuxerListener.onMediaRemuxerError(CALLBACK_MEDIA_REMUXER_ERROR_MUX_FAIL);
						}
						
						break;
					}
				}
				
				if(writer.getWriteTimeUs()-mLastSendPresentationTimeUs>=1000*1000)
				{
					mLastSendPresentationTimeUs = mLastSendPresentationTimeUs+1000*1000;
					
					if(mMediaRemuxerListener!=null)
					{
						mMediaRemuxerListener.onMediaRemuxerInfo(CALLBACK_MEDIA_REMUXER_INFO_PUBLISH_TIME, (int)(mLastSendPresentationTimeUs/1000000));
					}
				}
			}
			
			boolean ret = extractor.advance();
			if(!ret)
			{
				isDone = true;

				break;
			}
		}
		
		writer.stop();
		writer.release();
		extractor.release();
		inputBuffer.clear();
		inputBuffer = null;
		
		if(isDone)
		{
			if(mMediaRemuxerListener!=null)
			{
				mMediaRemuxerListener.onMediaRemuxerEnd();
			}
		}
	}
}
