package android.slkmedia.mediaprocesser;

public interface MediaRemuxerListener {
	public abstract void onMediaRemuxerError(int errorType);
	public abstract void onMediaRemuxerInfo(int infoType, int infoValue);
	public abstract void onMediaRemuxerEnd();
}
