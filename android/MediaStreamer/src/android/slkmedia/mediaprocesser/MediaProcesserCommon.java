package android.slkmedia.mediaprocesser;

public class MediaProcesserCommon {
	//media_processer_event_type
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR = 0;
	public final static int CALLBACK_MEDIA_PROCESSER_INFO = 1;
	public final static int CALLBACK_MEDIA_PROCESSER_END = 2;
	
	//media_processer_info_type
	public final static int CALLBACK_MEDIA_PROCESSER_INFO_WRITE_TIMESTAMP = 1;
	public final static int CALLBACK_MEDIA_PROCESSER_INFO_MEDIA_DETAIL_INFO = 2;
	public final static int CALLBACK_MEDIA_PROCESSER_INFO_MEDIA_THUMBNAIL_INFO = 3;

	//media_processer_error_type
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_OPEN_INPUT_FILE_FAIL = 0;
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_OPEN_VIDEO_DECODER_FAIL = 1;
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_OPEN_AUDIO_DECODER_FAIL = 2;
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_OPEN_AUDIO_PREPROCESS_FAIL = 3;
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_OPEN_VIDEO_ENCODER_FAIL = 4;
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_OPEN_AUDIO_ENCODER_FAIL = 5;
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_OPEN_OUTPUT_FILE_FAIL = 6;
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_OPEN_VIDEO_PREPROCESS_FAIL = 7;
    
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_READ_INPUT_FILE_FAIL = 10;
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_VIDEO_DECODE_FAIL = 11;
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_COLORSPACECONVERT_FAIL  = 12;
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_VIDEO_ENCODE_FAIL  = 13;
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_AUDIO_DECODE_FAIL = 14;
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_AUDIO_ENCODE_FAIL = 15;
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_AUDIO_MIX_FAIL = 16;

	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_WRITE_OUTPUT_FILE_FAIL = 18;
    
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_SEEK_INPUT_FILE_FAIL = 19;
    
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_SAVE_PNG_FAIL = 20;
	
	public final static int CALLBACK_MEDIA_PROCESSER_ERROR_REMUX_FAIL = 100;
}
