// Created by Think on 16/2/25.
// Copyright © 2016年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaprocesser_MediaMerger
#define _Included_android_slkmedia_mediaprocesser_MediaMerger

#include <assert.h>

#include "JNIHelp.h"
#include "MediaMerger.h"
#include "MediaMaterial.h"
#include "MediaEffect.h"
#include "MediaMergeAlgorithm.h"
#include "MediaProduct.h"
#include "MediaLog.h"

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;
static jmethodID post_event = NULL;

JNIEXPORT void JNICALL Java_android_slkmedia_mediaprocesser_MediaMerger_Native_1Init
  (JNIEnv *env, jclass thiz)
{
	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/mediaprocesser/MediaMerger");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/mediaprocesser/MediaMerger");
        return;
    }
    
    context = env->GetFieldID(clazz, "mNativeContext", "I");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find MediaMerger.mNativeContext");
        return;
    }

	post_event = env->GetStaticMethodID(clazz, "postEventFromNative",
			"(Ljava/lang/Object;IIILjava/lang/Object;)V");
	if (post_event == NULL) {
		jniThrowRuntimeException(env, "Can't find MediaMerger.postEventFromNative");
		return;
	}

	env->DeleteLocalRef(clazz);
}


JNIEXPORT void JNICALL Java_android_slkmedia_mediaprocesser_MediaMerger_Native_1Start
  (JNIEnv *env, jobject thiz, jobjectArray jInputMediaMaterialArray, jobjectArray jInputMediaEffectArray, jint jMediaMergeAlgorithm, jobject jOutputMediaProduct, jobject weak_this)
{
    MediaMaterialGroup inputMediaMaterialGroup;
    MediaEffectGroup inputMediaEffectGroup;
    MEDIA_MERGE_ALGORITHM algorithm = MEDIA_MERGE_ALGORITHM_TIMELINE;
    MediaProduct ouputProduct;
    
    MediaMerger* mediaMerger = (MediaMerger*)env->GetIntField(thiz, context);
    if (mediaMerger!=NULL) return;

    // get the java object fields
    int mediaMaterialCount = env->GetArrayLength(jInputMediaMaterialArray);

    inputMediaMaterialGroup.mediaMaterialNum = mediaMaterialCount;

    for(int i = 0; i < mediaMaterialCount; i++)
    {
    	jobject jMediaMaterialObject = env->GetObjectArrayElement(jInputMediaMaterialArray, i);
    	jclass ClassMediaMaterial = env->GetObjectClass(jMediaMaterialObject);

    	jfieldID jUrlID = env->GetFieldID(ClassMediaMaterial, "url", "Ljava/lang/String;");
    	jfieldID jMediaMaterialTypeID = env->GetFieldID(ClassMediaMaterial, "media_material_type", "I");
    	jfieldID jStartPosID = env->GetFieldID(ClassMediaMaterial, "startPos", "J");
    	jfieldID jEndPosID = env->GetFieldID(ClassMediaMaterial, "endPos", "J");
    	jfieldID jVolumeID = env->GetFieldID(ClassMediaMaterial, "volume", "F");
    	jfieldID jSpeedID = env->GetFieldID(ClassMediaMaterial, "speed", "F");

    	inputMediaMaterialGroup.mediaMaterials[i] = new MediaMaterial;
    	jstring jUrl = env->GetObjectField(jMediaMaterialObject, jUrlID);
    	const char*url = env->GetStringUTFChars(jUrl, NULL);
    	inputMediaMaterialGroup.mediaMaterials[i]->url = strdup(url);
    	env->ReleaseStringUTFChars(jUrl, url);

    	inputMediaMaterialGroup.mediaMaterials[i]->type = (MEDIA_MATERIAL_TYPE)env->GetIntField(jMediaMaterialObject, jMediaMaterialTypeID);
    	inputMediaMaterialGroup.mediaMaterials[i]->startPos = env->GetLongField(jMediaMaterialObject, jStartPosID);
    	inputMediaMaterialGroup.mediaMaterials[i]->endPos = env->GetLongField(jMediaMaterialObject, jEndPosID);
    	inputMediaMaterialGroup.mediaMaterials[i]->volume = env->GetFloatField(jMediaMaterialObject, jVolumeID);
    	inputMediaMaterialGroup.mediaMaterials[i]->speed = env->GetFloatField(jMediaMaterialObject, jSpeedID);

    	env->DeleteLocalRef(jMediaMaterialObject);
    	env->DeleteLocalRef(ClassMediaMaterial);
    }

    if(jInputMediaEffectArray!=NULL)
    {
        int mediaEffectCount = env->GetArrayLength(jInputMediaEffectArray);

        inputMediaEffectGroup.mediaEffectNum = mediaEffectCount;

        for(int i = 0; i < mediaEffectCount; i++)
        {
        	jobject jMediaEffectObject = env->GetObjectArrayElement(jInputMediaEffectArray, i);
        	jclass ClassMediaEffect = env->GetObjectClass(jMediaEffectObject);

        	jfieldID jUrlID = env->GetFieldID(ClassMediaEffect, "url", "Ljava/lang/String;");
        	jfieldID jMediaEffectTypeID = env->GetFieldID(ClassMediaEffect, "media_effect_type", "I");
        	jfieldID jEffectInPosID = env->GetFieldID(ClassMediaEffect, "effect_in_pos", "J");
        	jfieldID jEffectOutPosID = env->GetFieldID(ClassMediaEffect, "effect_out_pos", "J");
        	jfieldID jStartPosID = env->GetFieldID(ClassMediaEffect, "startPos", "J");
        	jfieldID jEndPosID = env->GetFieldID(ClassMediaEffect, "endPos", "J");
        	jfieldID jVolumeID = env->GetFieldID(ClassMediaEffect, "volume", "F");
        	jfieldID jSpeedID = env->GetFieldID(ClassMediaEffect, "speed", "F");

        	//overlay
        	jfieldID jXID = env->GetFieldID(ClassMediaEffect, "x", "I");
        	jfieldID jYID = env->GetFieldID(ClassMediaEffect, "y", "I");
        	jfieldID jRotationID = env->GetFieldID(ClassMediaEffect, "rotation", "I");
        	jfieldID jScaleID = env->GetFieldID(ClassMediaEffect, "scale", "F");
        	jfieldID jFlipHorizontalID = env->GetFieldID(ClassMediaEffect, "flipHorizontal", "Z");
        	jfieldID jFlipVerticalID = env->GetFieldID(ClassMediaEffect, "flipVertical", "Z");

        	//text
        	jfieldID jTextID = env->GetFieldID(ClassMediaEffect, "text", "Ljava/lang/String;");
        	jfieldID jFontLibPathID = env->GetFieldID(ClassMediaEffect, "fontLibPath", "Ljava/lang/String;");
        	jfieldID jFontSizeID = env->GetFieldID(ClassMediaEffect, "fontSize", "I");
        	jfieldID jFontColorID = env->GetFieldID(ClassMediaEffect, "fontColor", "I");
        	jfieldID jLeftMarginID = env->GetFieldID(ClassMediaEffect, "leftMargin", "I");
        	jfieldID jRightMarginID = env->GetFieldID(ClassMediaEffect, "rightMargin", "I");
        	jfieldID jTopMarginID = env->GetFieldID(ClassMediaEffect, "topMargin", "I");
        	jfieldID jBottomMarginID = env->GetFieldID(ClassMediaEffect, "bottomMargin", "I");
        	jfieldID jLineSpaceID = env->GetFieldID(ClassMediaEffect, "lineSpace", "I");
        	jfieldID jWordSpaceID = env->GetFieldID(ClassMediaEffect, "wordSpace", "I");
        	jfieldID jTextAnimationTypeID = env->GetFieldID(ClassMediaEffect, "text_animation_type", "I");

        	jfieldID jFilterTypeID = env->GetFieldID(ClassMediaEffect, "gpu_image_filter_type", "I");
        	jfieldID jTransitionTypeID = env->GetFieldID(ClassMediaEffect, "gpu_image_transition_type", "I");
        	jfieldID jTransitionSourceID = env->GetFieldID(ClassMediaEffect, "transition_source_id", "I");

        	inputMediaEffectGroup.mediaEffects[i] = new MediaEffect;
        	jstring jUrl = env->GetObjectField(jMediaEffectObject, jUrlID);
        	if(jUrl==NULL)
        	{
        		inputMediaEffectGroup.mediaEffects[i]->url = NULL;
        	}else{
            	const char*url = env->GetStringUTFChars(jUrl, NULL);
            	inputMediaEffectGroup.mediaEffects[i]->url = strdup(url);
            	env->ReleaseStringUTFChars(jUrl, url);
        	}

        	inputMediaEffectGroup.mediaEffects[i]->type = (MEDIA_EFFECT_TYPE)env->GetIntField(jMediaEffectObject, jMediaEffectTypeID);
        	inputMediaEffectGroup.mediaEffects[i]->effect_in_pos = env->GetLongField(jMediaEffectObject, jEffectInPosID);
        	inputMediaEffectGroup.mediaEffects[i]->effect_out_pos = env->GetLongField(jMediaEffectObject, jEffectOutPosID);
        	inputMediaEffectGroup.mediaEffects[i]->startPos = env->GetLongField(jMediaEffectObject, jStartPosID);
        	inputMediaEffectGroup.mediaEffects[i]->endPos = env->GetLongField(jMediaEffectObject, jEndPosID);
        	inputMediaEffectGroup.mediaEffects[i]->volume = env->GetFloatField(jMediaEffectObject, jVolumeID);
        	inputMediaEffectGroup.mediaEffects[i]->speed = env->GetFloatField(jMediaEffectObject, jSpeedID);

        	//overlay
        	inputMediaEffectGroup.mediaEffects[i]->x = env->GetIntField(jMediaEffectObject, jXID);
        	inputMediaEffectGroup.mediaEffects[i]->y = env->GetIntField(jMediaEffectObject, jYID);
        	inputMediaEffectGroup.mediaEffects[i]->rotation = env->GetIntField(jMediaEffectObject, jRotationID);
        	inputMediaEffectGroup.mediaEffects[i]->scale = env->GetFloatField(jMediaEffectObject, jScaleID);
        	inputMediaEffectGroup.mediaEffects[i]->flipHorizontal = env->GetBooleanField(jMediaEffectObject, jFlipHorizontalID);
        	inputMediaEffectGroup.mediaEffects[i]->flipVertical = env->GetBooleanField(jMediaEffectObject, jFlipVerticalID);

        	//text
        	jstring jText = env->GetObjectField(jMediaEffectObject, jTextID);
        	const char*text = env->GetStringUTFChars(jText, NULL);
        	inputMediaEffectGroup.mediaEffects[i]->text = strdup(text);
        	env->ReleaseStringUTFChars(jText, text);

        	jstring jFontLibPath = env->GetObjectField(jMediaEffectObject, jFontLibPathID);
        	const char*fontLibPath = env->GetStringUTFChars(jFontLibPath, NULL);
        	inputMediaEffectGroup.mediaEffects[i]->fontLibPath = strdup(fontLibPath);
        	env->ReleaseStringUTFChars(jFontLibPath, fontLibPath);

        	inputMediaEffectGroup.mediaEffects[i]->fontSize =  env->GetIntField(jMediaEffectObject, jFontSizeID);

        	int color = env->GetIntField(jMediaEffectObject, jFontColorID);
        	int a = (color >> 24) & 0xff; // or color >>> 24
        	int r = (color >> 16) & 0xff;
        	int g = (color >>  8) & 0xff;
        	int b = (color      ) & 0xff;

        	FontColor fontColor;
        	fontColor.r = (float)r/255.0f;
        	fontColor.g = (float)g/255.0f;
        	fontColor.b = (float)b/255.0f;
        	fontColor.a = (float)a/255.0f;

        	inputMediaEffectGroup.mediaEffects[i]->fontColor = fontColor;
        	inputMediaEffectGroup.mediaEffects[i]->leftMargin = env->GetIntField(jMediaEffectObject, jLeftMarginID);
        	inputMediaEffectGroup.mediaEffects[i]->rightMargin = env->GetIntField(jMediaEffectObject, jRightMarginID);
        	inputMediaEffectGroup.mediaEffects[i]->topMargin = env->GetIntField(jMediaEffectObject, jTopMarginID);
        	inputMediaEffectGroup.mediaEffects[i]->bottomMargin = env->GetIntField(jMediaEffectObject, jBottomMarginID);
        	inputMediaEffectGroup.mediaEffects[i]->lineSpace = env->GetIntField(jMediaEffectObject, jLineSpaceID);
        	inputMediaEffectGroup.mediaEffects[i]->wordSpace = env->GetIntField(jMediaEffectObject, jWordSpaceID);
        	inputMediaEffectGroup.mediaEffects[i]->text_animation_type = env->GetIntField(jMediaEffectObject, jTextAnimationTypeID);

        	inputMediaEffectGroup.mediaEffects[i]->gpu_image_filter_type = (GPU_IMAGE_FILTER_TYPE)env->GetIntField(jMediaEffectObject, jFilterTypeID);
        	inputMediaEffectGroup.mediaEffects[i]->gpu_image_transition_type = (GPU_IMAGE_TRANSITION_TYPE)env->GetIntField(jMediaEffectObject, jTransitionTypeID);
        	inputMediaEffectGroup.mediaEffects[i]->transition_source_id = env->GetIntField(jMediaEffectObject, jTransitionSourceID);

        	env->DeleteLocalRef(jMediaEffectObject);
        	env->DeleteLocalRef(ClassMediaEffect);
        }
    }else{
    	inputMediaEffectGroup.mediaEffectNum = 0;
    }

    algorithm = (MEDIA_MERGE_ALGORITHM)jMediaMergeAlgorithm;

    jclass ClassMediaProduct = env->GetObjectClass(jOutputMediaProduct);
	jfieldID jUrlID = env->GetFieldID(ClassMediaProduct, "url", "Ljava/lang/String;");
	jfieldID jTypeID = env->GetFieldID(ClassMediaProduct, "media_product_type", "I");
    jfieldID jhasVideoID = env->GetFieldID(ClassMediaProduct, "hasVideo", "Z");
    jfieldID jWidthID = env->GetFieldID(ClassMediaProduct, "videoWidth", "I");
    jfieldID jHeightID = env->GetFieldID(ClassMediaProduct, "videoHeight", "I");
    jfieldID jIsAspectFit = env->GetFieldID(ClassMediaProduct, "isAspectFit", "Z");
    jfieldID jhasAudioID = env->GetFieldID(ClassMediaProduct, "hasAudio", "Z");
    jfieldID jBpsID = env->GetFieldID(ClassMediaProduct, "bps", "I");

	jstring jUrl = env->GetObjectField(jOutputMediaProduct, jUrlID);
	const char*url = env->GetStringUTFChars(jUrl, NULL);
    ouputProduct.url = strdup(url);
	env->ReleaseStringUTFChars(jUrl, url);

	ouputProduct.type = (MEDIA_PRODUCT_TYPE)env->GetIntField(jOutputMediaProduct, jTypeID);

    ouputProduct.audioOptions.hasAudio = env->GetBooleanField(jOutputMediaProduct, jhasAudioID);
    ouputProduct.audioOptions.audioSampleRate = 44100;
    ouputProduct.audioOptions.audioNumChannels = 1;
    ouputProduct.audioOptions.audioBitRate = 64; //k

    ouputProduct.videoOptions.hasVideo = env->GetBooleanField(jOutputMediaProduct, jhasVideoID);
    ouputProduct.videoOptions.videoRawType = VIDEOFRAME_RAWTYPE_I420;
    ouputProduct.videoOptions.videoWidth = env->GetIntField(jOutputMediaProduct, jWidthID);
    ouputProduct.videoOptions.videoHeight = env->GetIntField(jOutputMediaProduct, jHeightID);
    ouputProduct.videoOptions.videoFps = 24;
    ouputProduct.videoOptions.videoEncodeType = VIDEO_SOFT_ENCODE;
    ouputProduct.videoOptions.videoProfile = 2; //0:base_line 1:main_profile 2:high_profile
    ouputProduct.videoOptions.videoBitRate = env->GetIntField(jOutputMediaProduct, jBpsID) - ouputProduct.audioOptions.audioBitRate;
    ouputProduct.videoOptions.encodeMode = 0;//VBR
    ouputProduct.videoOptions.maxKeyFrameIntervalMs = 6000;

    ouputProduct.videoOptions.isAspectFit = env->GetBooleanField(jOutputMediaProduct, jIsAspectFit);

    //for X264
    if(ouputProduct.videoOptions.videoBitRate<1000)
    {
    	ouputProduct.videoOptions.quality = 0;
    }else if(ouputProduct.videoOptions.videoBitRate>=1000 && ouputProduct.videoOptions.videoBitRate<2000)
    {
    	ouputProduct.videoOptions.quality = -1;
    }else if(ouputProduct.videoOptions.videoBitRate>=2000 && ouputProduct.videoOptions.videoBitRate<3000)
    {
    	ouputProduct.videoOptions.quality = -2;
    }else if(ouputProduct.videoOptions.videoBitRate>=3000 && ouputProduct.videoOptions.videoBitRate<4000){
    	ouputProduct.videoOptions.quality = -3;
    }else if(ouputProduct.videoOptions.videoBitRate>=4000 && ouputProduct.videoOptions.videoBitRate<5000){
    	ouputProduct.videoOptions.quality = -4;
    }else if(ouputProduct.videoOptions.videoBitRate>=5000 && ouputProduct.videoOptions.videoBitRate<6000){
    	ouputProduct.videoOptions.quality = -5;
    }else {
    	ouputProduct.videoOptions.quality = -6;
    }

    ouputProduct.videoOptions.bStrictCBR = false;
    ouputProduct.videoOptions.deblockingFilterFactor = 4;

	env->DeleteLocalRef(ClassMediaProduct);

    MediaMerger *androidMediaMerger = MediaMerger::CreateMediaMerger(jvm, &inputMediaMaterialGroup, &inputMediaEffectGroup, algorithm, &ouputProduct);

    inputMediaMaterialGroup.Free();
    inputMediaEffectGroup.Free();

    if(ouputProduct.url)
    {
    	free(ouputProduct.url);
    	ouputProduct.url = NULL;
    }

    androidMediaMerger->setListener(thiz, weak_this, post_event);
    androidMediaMerger->start();

    env->SetIntField(thiz, context, (int32_t)androidMediaMerger);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaprocesser_MediaMerger_Native_1Pause
  (JNIEnv *env, jobject thiz)
{
	MediaMerger* androidMediaMerger = (MediaMerger*)env->GetIntField(thiz, context);
    if (androidMediaMerger==NULL) return;

    androidMediaMerger->pause();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaprocesser_MediaMerger_Native_1Resume
  (JNIEnv *env, jobject thiz)
{
	MediaMerger* androidMediaMerger = (MediaMerger*)env->GetIntField(thiz, context);
    if (androidMediaMerger==NULL) return;

    androidMediaMerger->resume();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaprocesser_MediaMerger_Native_1Stop
  (JNIEnv *env, jobject thiz, jint jMediaMergeAlgorithm)
{
	MediaMerger* androidMediaMerger = (MediaMerger*)env->GetIntField(thiz, context);
    if (androidMediaMerger==NULL) return;
    
    androidMediaMerger->stop();
    
    MediaMerger::DeleteMediaMerger((MEDIA_MERGE_ALGORITHM)jMediaMergeAlgorithm, androidMediaMerger);
    androidMediaMerger = NULL;

    env->SetIntField(thiz, context, (int32_t)0);
}

#ifdef __cplusplus
}
#endif

#endif
