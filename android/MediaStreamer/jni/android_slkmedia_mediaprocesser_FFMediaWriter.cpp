// Created by Think on 2019/7/2.
// Copyright © 2019年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaprocesser_FFMediaWriter
#define _Included_android_slkmedia_mediaprocesser_FFMediaWriter

#include <assert.h>

#include "JNIHelp.h"
#include "FFMediaWriter.h"
#include "MediaLog.h"

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;

JNIEXPORT void JNICALL Java_android_slkmedia_mediaprocesser_FFMediaWriter_Native_1Init
  (JNIEnv *env, jclass thiz)
{
	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/mediaprocesser/FFMediaWriter");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/mediaprocesser/FFMediaWriter");
        return;
    }
    
    context = env->GetFieldID(clazz, "mNativeContext", "I");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find FFMediaWriter.mNativeContext");
        return;
    }

	env->DeleteLocalRef(clazz);
}


JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaprocesser_FFMediaWriter_Native_1Open
  (JNIEnv *env, jobject thiz, jstring jPublishUrl, jint jFormat, jboolean jHasVideo, jint jVideoWidth, jint jVideoHeight, jint jVideoFps, jint jVideoRotation, jbyteArray jSps, jint jSpsLen, jbyteArray jPps, jint jPpsLen, jboolean jHasAudio, jint jAudioSamplerate, jint jAudioChannels, jint jAudioSampleFormat, jbyteArray jAsc, jint jAscLen)
{
    FFMediaWriter* mediaWriter = (FFMediaWriter*)env->GetIntField(thiz, context);
    if (mediaWriter!=NULL) return JNI_FALSE;

    int format = jFormat;
    bool hasVideo;
    if(jHasVideo == JNI_TRUE)
    {
        hasVideo = true;
    }else{
        hasVideo = false;
    }
    int width = jVideoWidth;
    int height = jVideoHeight;
    int fps = jVideoFps;
    int videoRotation = jVideoRotation;
    jbyte* sps_raw_bytes = env->GetByteArrayElements(jSps, NULL);
    uint8_t* sps = (uint8_t*)sps_raw_bytes;
    int sps_len = jSpsLen;
    jbyte* pps_raw_bytes = env->GetByteArrayElements(jPps, NULL);
    uint8_t *pps = (uint8_t*)pps_raw_bytes;
    int pps_len = jPpsLen;
    bool hasAudio;
    if (jHasAudio == JNI_TRUE) {
        hasAudio = true;
    }else{
        hasAudio = false;
    }
    int samplerate = jAudioSamplerate;
    int channels = jAudioChannels;
    int audioSampleFormat = jAudioSampleFormat;
    jbyte* asc_raw_bytes = env->GetByteArrayElements(jAsc, NULL);
    uint8_t* asc = (uint8_t*)asc_raw_bytes;
    int asc_len = jAscLen;

    const char *publishUrl;
    publishUrl = env->GetStringUTFChars(jPublishUrl, NULL);

    mediaWriter = new FFMediaWriter(jvm, publishUrl, format, hasVideo, width, height, fps, videoRotation, sps, sps_len, pps, pps_len, hasAudio, samplerate, channels, audioSampleFormat, asc, asc_len);
    bool ret = mediaWriter->open();
    if(!ret)
    {
        mediaWriter->close();
        delete mediaWriter;
        mediaWriter = NULL;
    }else{
        env->SetIntField(thiz, context, (int32_t)mediaWriter);
    }
    
    env->ReleaseStringUTFChars(jPublishUrl, publishUrl);
    
    env->ReleaseByteArrayElements(jSps, sps_raw_bytes, 0);
    env->ReleaseByteArrayElements(jPps, pps_raw_bytes, 0);
    env->ReleaseByteArrayElements(jAsc, asc_raw_bytes, 0);
    
    return ret ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaprocesser_FFMediaWriter_Native_1Close
  (JNIEnv *env, jobject thiz)
{
    FFMediaWriter* mediaWriter = (FFMediaWriter*)env->GetIntField(thiz, context);
    if (mediaWriter==NULL) return;
    
    mediaWriter->close();
    
    delete mediaWriter;
    mediaWriter = NULL;
    
    env->SetIntField(thiz, context, (int32_t)0);
}

JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaprocesser_FFMediaWriter_Native_1WriteAVPacket
  (JNIEnv *env, jobject thiz, jbyteArray jData, jint jSize, jlong jPts, jlong jDts, jint jPacketType)
{
    FFMediaWriter* mediaWriter = (FFMediaWriter*)env->GetIntField(thiz, context);
    if (mediaWriter==NULL) return JNI_FALSE;

    jbyte* raw_bytes = env->GetByteArrayElements(jData, NULL);
    uint8_t *data = (uint8_t*)raw_bytes;
    int size = jSize;
    int64_t pts = (int64_t)jPts;
    int64_t dts = (int64_t)jDts;
    int packet_type = jPacketType;

    bool ret = mediaWriter->writeAVPacket(data, size, pts, dts, packet_type);

    env->ReleaseByteArrayElements(jData, raw_bytes, 0);
    
    return ret ? JNI_TRUE : JNI_FALSE;
}

#ifdef __cplusplus
}
#endif

#endif
