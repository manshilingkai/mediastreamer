#  Created by Think on 16/2/25.
#  Copyright © 2016年 Cell. All rights reserved.

LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
    X264_BASE := $(LOCAL_PATH)/../../../ThirdParty/x264/android/build/x264-armv7/output
    FFMPEG_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg/android/build/ffmpeg-armv7a/output
    FFMPEG_LIBRTMP_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg_librtmp/android/build/ffmpeg-armv7a/output
    LIBYUV_BASE := $(LOCAL_PATH)/../../../ThirdParty/libyuv/libyuv/android/armeabi-v7a
    LIBFAAC_BASE := $(LOCAL_PATH)/../../../ThirdParty/faac/android/build/faac-armv7/output
    LIBPNG_BASE := $(LOCAL_PATH)/../../../ThirdParty/libpng-android/android/armeabi-v7a
    LIBAMRNB_BASE := $(LOCAL_PATH)/../../../ThirdParty/opencore-amr-android/output/armeabi-v7a
    LIBVPX_BASE := $(LOCAL_PATH)/../../../ThirdParty/vpx/output/android/armeabi-v7a
    LIBWEBRTC_AUDIO_PROCESSING := $(LOCAL_PATH)/../../../ThirdParty/webrtc/output/android/armeabi-v7a
    OPENCV_BASE := $(LOCAL_PATH)/../../../ThirdParty/OpenCV/android/output/armeabi-v7a
    FREETYPE_BASE := $(LOCAL_PATH)/../../../ThirdParty/freetype2-android/output/armeabi-v7a
    LIBWEBP_BASE := $(LOCAL_PATH)/../../../ThirdParty/libwebp/output/android/armeabi-v7a
    GIFLIB_BASE := $(LOCAL_PATH)/../../../ThirdParty/giflib/output/android/armeabi-v7a
    GIFH_BASE := $(LOCAL_PATH)/../../../ThirdParty/gif-h
    GIFFLEN_BASE := $(LOCAL_PATH)/../../../ThirdParty/gifflen
endif

ifeq ($(TARGET_ARCH_ABI),x86)
    X264_BASE := $(LOCAL_PATH)/../../../ThirdParty/x264/android/build/x264-x86/output
    FFMPEG_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg/android/build/ffmpeg-x86/output
    FFMPEG_LIBRTMP_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg_librtmp/android/build/ffmpeg-x86/output
    LIBYUV_BASE := $(LOCAL_PATH)/../../../ThirdParty/libyuv/libyuv/android/x86
    LIBFAAC_BASE := $(LOCAL_PATH)/../../../ThirdParty/faac/android/build/faac-x86/output
    LIBPNG_BASE := $(LOCAL_PATH)/../../../ThirdParty/libpng-android/android/x86
    LIBAMRNB_BASE := $(LOCAL_PATH)/../../../ThirdParty/opencore-amr-android/output/x86
    LIBWEBRTC_AUDIO_PROCESSING := $(LOCAL_PATH)/../../../ThirdParty/webrtc/output/android/x86
    OPENCV_BASE := $(LOCAL_PATH)/../../../ThirdParty/OpenCV/android/output/x86
    FREETYPE_BASE := $(LOCAL_PATH)/../../../ThirdParty/freetype2-android/output/x86
    LIBWEBP_BASE := $(LOCAL_PATH)/../../../ThirdParty/libwebp/output/android/x86
    GIFLIB_BASE := $(LOCAL_PATH)/../../../ThirdParty/giflib/output/android/x86
    GIFH_BASE := $(LOCAL_PATH)/../../../ThirdParty/gif-h
    GIFFLEN_BASE := $(LOCAL_PATH)/../../../ThirdParty/gifflen
endif

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
    X264_BASE := $(LOCAL_PATH)/../../../ThirdParty/x264/android/build/x264-arm64/output
    FFMPEG_BASE := $(LOCAL_PATH)/../../../ThirdParty/ffmpeg/android/build/ffmpeg-arm64-v8a/output
    LIBYUV_BASE := $(LOCAL_PATH)/../../../ThirdParty/libyuv/libyuv/android/arm64-v8a
    LIBAMRNB_BASE := $(LOCAL_PATH)/../../../ThirdParty/opencore-amr-android/output/arm64-v8a
endif

############################### X264 #############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(X264_BASE)/lib/libx264.a
LOCAL_MODULE := x264
include $(PREBUILT_STATIC_LIBRARY)

############################### FFMPEG ############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(FFMPEG_BASE)/libffmpeg_ypp.so
LOCAL_MODULE := ffmpeg_ypp
include $(PREBUILT_SHARED_LIBRARY)

############################### LIBYUV ############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBYUV_BASE)/lib/libyuv_static.a
LOCAL_MODULE := yuv_static
include $(PREBUILT_STATIC_LIBRARY)

############################### FAAC ##############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBFAAC_BASE)/lib/libfaac.a
LOCAL_MODULE := faac
include $(PREBUILT_STATIC_LIBRARY)

############################### LIBPNG ############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBPNG_BASE)/lib/libpng.a
LOCAL_MODULE := png
include $(PREBUILT_STATIC_LIBRARY)

############################### LIBAMRNB ##########################

# include $(CLEAR_VARS)
# LOCAL_SRC_FILES := $(LIBAMRNB_BASE)/lib/libamr-codec.so
# LOCAL_MODULE := amrnb
# include $(PREBUILT_SHARED_LIBRARY)

############################### LIBVPX ############################

# ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
#	 include $(CLEAR_VARS)
#	 LOCAL_SRC_FILES := $(LIBVPX_BASE)/lib/libvpx.a
#	 LOCAL_MODULE := vpx
#	 include $(PREBUILT_STATIC_LIBRARY)
# endif

############################### LIBWEBRTC_AUDIO_PROCESSING ########

# ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
#	 include $(CLEAR_VARS)
#	 LOCAL_SRC_FILES := $(LIBWEBRTC_AUDIO_PROCESSING)/lib/libwebrtc-arm-unknown-linux-androideabi.a
#	 LOCAL_MODULE := webrtc-audio-processing
#	 include $(PREBUILT_STATIC_LIBRARY)
# endif

# ifeq ($(TARGET_ARCH_ABI),x86)
#	 include $(CLEAR_VARS)
#	 LOCAL_SRC_FILES := $(LIBWEBRTC_AUDIO_PROCESSING)/lib/libwebrtc-x86-unknown-linux-android.a
#	 LOCAL_MODULE := webrtc-audio-processing
#	 include $(PREBUILT_STATIC_LIBRARY)
# endif

############################### OPENCV ############################

# include $(CLEAR_VARS)
# LOCAL_SRC_FILES := $(OPENCV_BASE)/lib/libopencv_java3.so
# LOCAL_MODULE := opencv
# include $(PREBUILT_SHARED_LIBRARY)

############################### FREETYPE ############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(FREETYPE_BASE)/lib/libfreetype2-static.a
LOCAL_MODULE := freetype2-static
include $(PREBUILT_STATIC_LIBRARY)

############################### LIBWEBP ############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(LIBWEBP_BASE)/lib/libanim_util.so
LOCAL_MODULE := anim_util
include $(PREBUILT_SHARED_LIBRARY)

############################### GIFLIB ############################

include $(CLEAR_VARS)
LOCAL_SRC_FILES := $(GIFLIB_BASE)/lib/libgif.a
LOCAL_MODULE := gif
include $(PREBUILT_STATIC_LIBRARY)

###################################################################



include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(X264_BASE)/include \
    $(FFMPEG_BASE)/include \
    $(LIBYUV_BASE)/include \
    $(LIBFAAC_BASE)/include \
    $(LIBPNG_BASE)/include \
    $(LIBAMRNB_BASE)/include \
    $(LIBWEBRTC_AUDIO_PROCESSING)/include \
    $(OPENCV_BASE)/include \
    $(FREETYPE_BASE)/include \
    $(LIBWEBP_BASE)/include \
    $(GIFLIB_BASE)/include \
    $(GIFH_BASE) \
    $(GIFFLEN_BASE) \
    $(LOCAL_PATH)/../../../MediaBase \
    $(LOCAL_PATH)/../../../MediaCapture \
    $(LOCAL_PATH)/../../../MediaCapture/android \
	$(LOCAL_PATH)/../../../MediaDataPool \
	$(LOCAL_PATH)/../../../MediaEncoder \
	$(LOCAL_PATH)/../../../MediaEncoder/android \
	$(LOCAL_PATH)/../../../MediaMuxer \
	$(LOCAL_PATH)/../../../MediaPreProcess \
	$(LOCAL_PATH)/../../../MediaUtils \
	$(LOCAL_PATH)/../../../MediaUtils/android \
	$(LOCAL_PATH)/../../../MediaListener \
	$(LOCAL_PATH)/../../../MediaListener/android \
	$(LOCAL_PATH)/../../../MediaStreamer \
	$(LOCAL_PATH)/../../../MediaStreamer/android \
	$(LOCAL_PATH)/../../../MediaFile \
	$(LOCAL_PATH)/../../../MediaDebug \
    $(LOCAL_PATH)/../../../MediaDebug/android \
    $(LOCAL_PATH)/../../../MediaDecoder \
    $(LOCAL_PATH)/../../../MediaDecoder/android \
    $(LOCAL_PATH)/../../../MediaDemuxer \
    $(LOCAL_PATH)/../../../MediaProcesser \
    $(LOCAL_PATH)/../../../MediaProcesser/android \
    $(LOCAL_PATH)/../../../GPUImageFilter \
    $(LOCAL_PATH)/../../../GPUImageFilter/android \
    $(LOCAL_PATH)/../../../MediaRender \
    $(LOCAL_PATH)/../../../MediaRender/android \
    $(LOCAL_PATH)/../../../MediaEditEngine \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode \
    $(LOCAL_PATH)/../../../OpenCV \
    $(LOCAL_PATH)/../../../Text \
    $(LOCAL_PATH)/../../../Webp \
    $(LOCAL_PATH)/../../../Gif \
    $(LOCAL_PATH)/ \
    $(LOCAL_PATH)/beautify \
    $(LOCAL_PATH)/bitmap \


ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
LOCAL_C_INCLUDES += $(LIBVPX_BASE)/include
endif

#    $(LOCAL_PATH)/../../../OpenCV/cvAdd4cMat.cpp
#    $(LOCAL_PATH)/../../../OpenCV/OpenCVUtils.cpp
#    $(LOCAL_PATH)/../../../MediaEncoder/OpenCoreAMRNBEncoder.cpp
#    $(LOCAL_PATH)/../../../MediaPreProcess/WebrtcAudioFilter.cpp
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../../MediaBase/MediaTime.cpp \
	$(LOCAL_PATH)/../../../MediaBase/MediaTimer.cpp \
	$(LOCAL_PATH)/../../../MediaBase/TimedEventQueue.cpp \
	$(LOCAL_PATH)/../../../MediaBase/MediaMath.cpp \
	$(LOCAL_PATH)/../../../MediaDataPool/VideoFrameBufferPool.cpp \
	$(LOCAL_PATH)/../../../MediaDataPool/AudioFrameBufferPool.cpp \
	$(LOCAL_PATH)/../../../MediaDataPool/MediaPacketQueue.cpp \
	$(LOCAL_PATH)/../../../MediaDataPool/AudioPCMDataPool.cpp \
	$(LOCAL_PATH)/../../../MediaDataPool/AVPacketQueue.cpp \
	$(LOCAL_PATH)/../../../MediaCapture/AudioCapture.cpp \
	$(LOCAL_PATH)/../../../MediaCapture/android/OpenSlesInput.cpp \
	$(LOCAL_PATH)/../../../MediaCapture/android/JniAudioCapturer.cpp \
    $(LOCAL_PATH)/../../../MediaCapture/ExternalAudioInput.cpp \
	$(LOCAL_PATH)/../../../MediaEncoder/X264Encoder.cpp \
    $(LOCAL_PATH)/../../../MediaEncoder/VideoEncoder.cpp \
    $(LOCAL_PATH)/../../../MediaEncoder/FAACEncoder.cpp \
    $(LOCAL_PATH)/../../../MediaEncoder/AudioEncoder.cpp \
    $(LOCAL_PATH)/../../../MediaEncoder/android/MediaCodecEncoder.cpp \
    $(LOCAL_PATH)/../../../MediaMuxer/MediaMuxer.cpp \
    $(LOCAL_PATH)/../../../MediaMuxer/FFmpegMuxer.cpp \
    $(LOCAL_PATH)/../../../MediaMuxer/FFmpegWriter.cpp \
    $(LOCAL_PATH)/../../../MediaMuxer/FFMediaMuxer.cpp \
    $(LOCAL_PATH)/../../../MediaMuxer/FFMediaWriter.cpp \
    $(LOCAL_PATH)/../../../MediaPreProcess/ColorSpaceConvert.cpp \
    $(LOCAL_PATH)/../../../MediaPreProcess/LibyuvColorSpaceConvert.cpp \
    $(LOCAL_PATH)/../../../MediaPreProcess/AudioFilter.cpp \
    $(LOCAL_PATH)/../../../MediaPreProcess/FFAudioFilter.cpp \
    $(LOCAL_PATH)/../../../MediaPreProcess/VideoFilter.cpp \
    $(LOCAL_PATH)/../../../MediaUtils/android/DeviceInfo.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/AVCUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/StringUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/FileUtils.cpp \
	$(LOCAL_PATH)/../../../MediaUtils/android/AndroidUtils.cpp \
	$(LOCAL_PATH)/../../../MediaListener/IMediaListener.cpp \
	$(LOCAL_PATH)/../../../MediaListener/NotificationQueue.cpp \
	$(LOCAL_PATH)/../../../MediaListener/android/JniMediaListener.cpp \
	$(LOCAL_PATH)/../../../MediaListener/NormalMediaListener.cpp \
    $(LOCAL_PATH)/../../../MediaStreamer/MediaStreamer.cpp \
    $(LOCAL_PATH)/../../../MediaStreamer/AudioStreamer.cpp \
    $(LOCAL_PATH)/../../../MediaStreamer/SLKAudioStreamer.cpp \
    $(LOCAL_PATH)/../../../MediaStreamer/SLKMediaStreamer.cpp \
    $(LOCAL_PATH)/../../../MediaStreamer/AnimatedImageMediaStreamer.cpp \
    $(LOCAL_PATH)/../../../MediaFile/MediaFile.cpp \
    $(LOCAL_PATH)/../../../MediaFile/MediaDir.cpp \
    $(LOCAL_PATH)/../../../MediaDebug/android/JNIHelp.cpp \
    $(LOCAL_PATH)/../../../MediaDebug/MediaLog.cpp \
    $(LOCAL_PATH)/../../../MediaDebug/FFLog.cpp \
    $(LOCAL_PATH)/../../../MediaDecoder/VideoDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDecoder/FFVideoDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDecoder/android/MediaCodecDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDecoder/AudioDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDecoder/FFAudioDecoder.cpp \
    $(LOCAL_PATH)/../../../MediaDemuxer/FFmpegReader.cpp \
    $(LOCAL_PATH)/../../../MediaDemuxer/CustomMediaSource.cpp \
    $(LOCAL_PATH)/../../../MediaDemuxer/LocalMP3MediaSource.cpp \
    $(LOCAL_PATH)/../../../MediaProcesser/qt-faststart.cpp \
    $(LOCAL_PATH)/../../../MediaProcesser/MediaCoverImage.cpp \
    $(LOCAL_PATH)/../../../MediaProcesser/ImageProcesserUtils.cpp \
    $(LOCAL_PATH)/../../../MediaProcesser/MediaInfo.cpp \
    $(LOCAL_PATH)/../../../MediaProcesser/MediaMerger.cpp \
    $(LOCAL_PATH)/../../../MediaProcesser/MediaTimelineMerger.cpp \
    $(LOCAL_PATH)/../../../MediaProcesser/MediaTransPacketMerger.cpp \
    $(LOCAL_PATH)/../../../MediaProcesser/MediaRemuxer.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageI420InputFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageRawPixelOutputFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageRGBFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageSketchFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageAmaroFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageAntiqueFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageBeautyFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageBlackCatFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageBrannanFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageN1977Filter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageBrooklynFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageCoolFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageCrayonFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageTwoInputFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageNormalBlendFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageTransformFilter.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/Matrix.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/LinkedList.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/OpenGLUtils.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/Runnable.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/TextureRotationUtil.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/GPUImageOffScreenRender.cpp \
    $(LOCAL_PATH)/../../../GPUImageFilter/android/AndroidI420GPUImageOffScreenRender.cpp \
    $(LOCAL_PATH)/../../../MediaRender/AudioRender.cpp \
    $(LOCAL_PATH)/../../../MediaRender/android/OpenSLESRender.cpp \
    $(LOCAL_PATH)/../../../MediaRender/android/JniAudioTrackRender.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode/MediaDataStruct.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode/MediaFrameQueue.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode/MediaFrameOutputer.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaTranscode/DefaultMediaFrameOutputer.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/WAVFile.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/PCMPlayer.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/PCMUtils.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MicrophonePCMRecorder.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MicrophoneAudioRecorder.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/MediaDubbingProducer.cpp \
    $(LOCAL_PATH)/../../../MediaEditEngine/AudioPlayer.cpp \
    $(LOCAL_PATH)/../../../Text/Word2Bitmap.cpp \
    $(LOCAL_PATH)/../../../Webp/AnimatedWebp.cpp \
    $(LOCAL_PATH)/../../../Gif/AnimatedGif.cpp \
    $(GIFFLEN_BASE)/dib.cpp \
    $(GIFFLEN_BASE)/gifflen.cpp \
    $(LOCAL_PATH)/../../../Gif/AnimatedGifCreater.cpp \
    $(LOCAL_PATH)/../../../Gif/GifHAnimatedGifCreater.cpp \
    $(LOCAL_PATH)/../../../Gif/GifflenAnimatedGifCreater.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediastreamer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediastreamer_MediaStreamer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaprocesser_MediaProcesserUtils.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaprocesser_MediaInfo.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaprocesser_MediaMerger.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaprocesser_FFMediaWriter.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediastreamer_audiocapture_AudioCapturer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaplayer_audiorender_AudioTrackRender.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaeditengine_MicrophoneAudioRecorder.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaeditengine_MediaDubbingProducer.cpp \
    $(LOCAL_PATH)/android_slkmedia_mediaeditengine_AudioPlayer.cpp \
    $(LOCAL_PATH)/bitmap/Conversion.cpp \
    $(LOCAL_PATH)/bitmap/BitmapOperation.cpp \
    $(LOCAL_PATH)/beautify/MagicBeautify.cpp \
    $(LOCAL_PATH)/MagicJni.cpp \

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
# LOCAL_SRC_FILES += $(LOCAL_PATH)/../../../MediaEncoder/VP8Encoder.cpp
endif

LOCAL_CFLAGS += -DANDROID -D__STDC_FORMAT_MACROS

ifeq ($(TARGET_ARCH_ABI),x86)
LOCAL_CFLAGS += -DANDROID_X86
endif

LOCAL_LDLIBS := -llog -lOpenSLES -lz -lEGL -lGLESv2 -ljnigraphics

# libwebrtc-audio-processing
LOCAL_STATIC_LIBRARIES := libx264 libyuv_static libfaac libpng libfreetype2-static libgif cpufeatures

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
# LOCAL_STATIC_LIBRARIES += libvpx
endif

# opencv amrnb
LOCAL_SHARED_LIBRARIES := ffmpeg_ypp anim_util

LOCAL_MODULE := MediaStreamer

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/cpufeatures)
