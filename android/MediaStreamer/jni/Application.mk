#  Created by Think on 16/2/25.
#  Copyright © 2016年 Cell. All rights reserved.

APP_OPTIM := release
APP_PLATFORM := android-16
#APP_ABI := armeabi-v7a arm64-v8a x86
APP_ABI := armeabi-v7a x86
NDK_TOOLCHAIN_VERSION=4.9
APP_PIE := false
APP_STL := stlport_static
#APP_CPPFLAGS := -fno-rtti -fpermissive -fPIC
APP_CPPFLAGS := -fpermissive -fPIC -fexceptions