// Created by Think on 16/2/25.
// Copyright © 2016年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaprocesser_MediaInfo
#define _Included_android_slkmedia_mediaprocesser_MediaInfo

#include <assert.h>

#include "JNIHelp.h"
#include "MediaInfo.h"
#include "MediaLog.h"

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;
static jmethodID post_mediadetailinfo_event = NULL;
static jmethodID post_mediathumbnail_event = NULL;
static jmethodID post_mediastatus_event = NULL;

JNIEXPORT void JNICALL Java_android_slkmedia_mediaprocesser_MediaInfo_Native_1Init
  (JNIEnv *env, jclass thiz)
{
	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/mediaprocesser/MediaInfo");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/mediaprocesser/MediaInfo");
        return;
    }
    
    context = env->GetFieldID(clazz, "mNativeContext", "I");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find MediaInfo.mNativeContext");
        return;
    }
    
    post_mediastatus_event = env->GetStaticMethodID(clazz, "postEventFromNative",
			"(Ljava/lang/Object;IIILjava/lang/Object;)V");
	if (post_mediastatus_event == NULL) {
		jniThrowRuntimeException(env, "Can't find MediaInfo.postEventFromNative");
		return;
	}

	post_mediadetailinfo_event = env->GetStaticMethodID(clazz, "postDetailInfoFromNative",
			"(Ljava/lang/Object;JIILjava/lang/Object;)V");
	if (post_mediadetailinfo_event == NULL) {
		jniThrowRuntimeException(env, "Can't find MediaInfo.postDetailInfoFromNative");
		return;
	}

	post_mediathumbnail_event = env->GetStaticMethodID(clazz, "postThumbnailFromNative",
			"(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	if (post_mediathumbnail_event == NULL) {
		jniThrowRuntimeException(env, "Can't find MediaInfo.postThumbnailFromNative");
		return;
	}

	env->DeleteLocalRef(clazz);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaprocesser_MediaInfo_Native_1Start
  (JNIEnv *env, jobject thiz, jstring jInputMediaFile, jstring jSaveDir, jint jWidth, jint jHeight, jint jThumbnailCount, jobject weak_this)
{

    MediaInfo* mediaInfo = (MediaInfo*)env->GetIntField(thiz, context);
    if (mediaInfo!=NULL) return;

    const char*inputMediaFile = env->GetStringUTFChars(jInputMediaFile, NULL);
    const char*saveDir = env->GetStringUTFChars(jSaveDir, NULL);

    mediaInfo = new MediaInfo(jvm, saveDir);
    mediaInfo->setMediaThumbnailsOption(jWidth, jHeight,jThumbnailCount);
    mediaInfo->setMediaListener(thiz, weak_this, post_mediadetailinfo_event, post_mediathumbnail_event, post_mediastatus_event);
    mediaInfo->loadAsync(inputMediaFile);

    env->SetIntField(thiz, context, (int32_t)mediaInfo);

    env->ReleaseStringUTFChars(jSaveDir, saveDir);
    env->ReleaseStringUTFChars(jInputMediaFile, inputMediaFile);

}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaprocesser_MediaInfo_Native_1StartWithPosition
  (JNIEnv *env, jobject thiz, jstring jInputMediaFile, jlong jStartPos, jlong jEndPos, jstring jSaveDir, jint jWidth, jint jHeight, jint jThumbnailCount, jobject weak_this)
{
    MediaInfo* mediaInfo = (MediaInfo*)env->GetIntField(thiz, context);
    if (mediaInfo!=NULL) return;
    
    const char*inputMediaFile = env->GetStringUTFChars(jInputMediaFile, NULL);
    const char*saveDir = env->GetStringUTFChars(jSaveDir, NULL);

    mediaInfo = new MediaInfo(jvm, saveDir);
    mediaInfo->setMediaThumbnailsOption(jWidth, jHeight,jThumbnailCount);
    mediaInfo->setMediaListener(thiz, weak_this, post_mediadetailinfo_event, post_mediathumbnail_event, post_mediastatus_event);
    mediaInfo->loadAsync(inputMediaFile, (int64_t)jStartPos, (int64_t)jEndPos);

    env->SetIntField(thiz, context, (int32_t)mediaInfo);

    env->ReleaseStringUTFChars(jSaveDir, saveDir);
    env->ReleaseStringUTFChars(jInputMediaFile, inputMediaFile);
}


JNIEXPORT void JNICALL Java_android_slkmedia_mediaprocesser_MediaInfo_Native_1Stop
  (JNIEnv *env, jobject thiz)
{
	MediaInfo* mediaInfo = (MediaInfo*)env->GetIntField(thiz, context);
    if (mediaInfo==NULL) return;
    
    mediaInfo->quit();
    
    delete mediaInfo;
    mediaInfo = NULL;
    
    env->SetIntField(thiz, context, (int32_t)0);
}

JNIEXPORT jlong JNICALL Java_android_slkmedia_mediaprocesser_MediaInfo_syncGetMediaDuration
  (JNIEnv *env, jobject thiz, jstring jInputMediaFile)
{
    const char *inputFile = env->GetStringUTFChars(jInputMediaFile, NULL);

    jlong ret = MediaInfo::syncGetMediaDuration(inputFile);

    env->ReleaseStringUTFChars(jInputMediaFile, inputFile);

    return ret;
}

JNIEXPORT jint JNICALL Java_android_slkmedia_mediaprocesser_MediaInfo_syncGetCoverImageToImageFile
  (JNIEnv *env, jobject thiz, jstring jInputMediaFile, jint jOutputImageWidth, jint jOutputImageHeight, jstring jOutputImageFile)
{
    const char *inputMediaFile = env->GetStringUTFChars(jInputMediaFile, NULL);
    const char *outputImageFile = env->GetStringUTFChars(jOutputImageFile, NULL);

    int ret = MediaInfo::syncGetCoverImageToImageFile(inputMediaFile, jOutputImageWidth, jOutputImageHeight, outputImageFile);

    env->ReleaseStringUTFChars(jInputMediaFile, inputMediaFile);
    env->ReleaseStringUTFChars(jOutputImageFile, outputImageFile);

    return ret;
}

JNIEXPORT jint JNICALL Java_android_slkmedia_mediaprocesser_MediaInfo_syncGetCoverImageToImageFileWithPosition
  (JNIEnv *env, jobject thiz, jstring jInputMediaFile, jlong jSeekPositionMS, jint jOutputImageWidth, jint jOutputImageHeight, jstring jOutputImageFile)
{
    const char *inputMediaFile = env->GetStringUTFChars(jInputMediaFile, NULL);
    const char *outputImageFile = env->GetStringUTFChars(jOutputImageFile, NULL);

    int ret = MediaInfo::syncGetCoverImageToImageFileWithPosition(inputMediaFile, jSeekPositionMS, jOutputImageWidth, jOutputImageHeight, outputImageFile);

    env->ReleaseStringUTFChars(jInputMediaFile, inputMediaFile);
    env->ReleaseStringUTFChars(jOutputImageFile, outputImageFile);

    return ret;
}

JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaprocesser_MediaInfo_syncGetMediaDetailInfo
  (JNIEnv *env, jobject thiz, jstring jInputMediaFile, jobject jDetailInfo)
{
    const char *inputMediaFile = env->GetStringUTFChars(jInputMediaFile, NULL);

    MediaDetailInfo *detailInfo = MediaInfo::syncGetMediaDetailInfo(inputMediaFile);
    env->ReleaseStringUTFChars(jInputMediaFile, inputMediaFile);

    if(detailInfo!=NULL)
    {
        // get the java object fields
        jclass ClassMediaDetailInfo = env->GetObjectClass(jDetailInfo);

        jfieldID jHasVideoId = env->GetFieldID(ClassMediaDetailInfo, "hasVideo", "Z");
        jfieldID jHasAudioId = env->GetFieldID(ClassMediaDetailInfo, "hasAudio", "Z");
        jfieldID jVideoWidthId = env->GetFieldID(ClassMediaDetailInfo, "videoWidth", "I");
        jfieldID jVideoHeightId = env->GetFieldID(ClassMediaDetailInfo, "videoHeight", "I");
        jfieldID jVideoFpsId = env->GetFieldID(ClassMediaDetailInfo, "videoFps", "I");
        jfieldID jKbpsId = env->GetFieldID(ClassMediaDetailInfo, "kbps", "I");
        jfieldID jDurationMsId = env->GetFieldID(ClassMediaDetailInfo, "durationMs", "J");
        jfieldID jRotateId = env->GetFieldID(ClassMediaDetailInfo, "rotate", "I");
        jfieldID jIsH264Codec = env->GetFieldID(ClassMediaDetailInfo, "isH264Codec", "Z");
        jfieldID jIsAACCodec = env->GetFieldID(ClassMediaDetailInfo, "isAACCodec", "Z");

        env->SetBooleanField(jDetailInfo,jHasVideoId,detailInfo->hasVideo);
        env->SetBooleanField(jDetailInfo,jHasAudioId,detailInfo->hasAudio);
        env->SetIntField(jDetailInfo,jVideoWidthId,detailInfo->width);
        env->SetIntField(jDetailInfo,jVideoHeightId,detailInfo->height);
        env->SetIntField(jDetailInfo,jVideoFpsId,detailInfo->fps);
        env->SetIntField(jDetailInfo,jKbpsId,detailInfo->bps);
        env->SetLongField(jDetailInfo,jDurationMsId,detailInfo->duration);
        env->SetIntField(jDetailInfo,jRotateId,detailInfo->rotate);
        env->SetBooleanField(jDetailInfo,jIsH264Codec,detailInfo->isH264Codec);
        env->SetBooleanField(jDetailInfo,jIsAACCodec,detailInfo->isAACCodec);


        return JNI_TRUE;
    }

    return JNI_FALSE;
}

#ifdef __cplusplus
}
#endif

#endif
