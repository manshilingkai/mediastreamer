// Created by Think on 16/2/25.
// Copyright © 2016年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediastreamer_audiocapture_AudioCapturer
#define _Included_android_slkmedia_mediastreamer_audiocapture_AudioCapturer

#include <assert.h>

#include "JNIHelp.h"
#include "MediaLog.h"

#include "JniAudioCapturer.h"

#ifdef __cplusplus
extern "C" {
#endif


JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_audiocapture_AudioCapturer_nativeCacheDirectBufferAddress
	(JNIEnv* env, jobject thiz, jobject byte_buffer, jlong nativeAudioCapturer)
{
	if(nativeAudioCapturer==0)
	{
		LOGE("input param nativeAudioCapturer is 0");
		return;
	}

	JniAudioCapturer *jniAudioCapturer = (JniAudioCapturer*)nativeAudioCapturer;
	if(jniAudioCapturer==NULL)
	{
		LOGE("jniAudioCapturer is NULL");
		return;
	}

	jniAudioCapturer->OnCacheDirectBufferAddress(byte_buffer);
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediastreamer_audiocapture_AudioCapturer_nativeDataIsRecorded
	(JNIEnv* env, jobject thiz, jint length, jlong nativeAudioCapturer)
{
	if(nativeAudioCapturer==0)
	{
		LOGE("input param nativeAudioCapturer is 0");
		return;
	}

	JniAudioCapturer *jniAudioCapturer = (JniAudioCapturer*)nativeAudioCapturer;
	if(jniAudioCapturer==NULL)
	{
		LOGE("jniAudioCapturer is NULL");
		return;
	}

	jniAudioCapturer->OnDataIsRecorded(length);
}

#ifdef __cplusplus
}
#endif

#endif
