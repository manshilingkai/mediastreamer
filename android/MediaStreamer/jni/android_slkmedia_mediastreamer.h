// Created by Think on 16/2/25.
// Copyright © 2016年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediastreamer
#define _Included_android_slkmedia_mediastreamer

#include <assert.h>

#include "JNIHelp.h"
#include "MediaLog.h"

#ifdef __cplusplus
extern "C" {
#endif

extern jclass g_audio_capturer_class;
extern jclass g_audio_render_class;

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved);

JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* vm, void* reserved);

#ifdef __cplusplus
}
#endif

#endif
