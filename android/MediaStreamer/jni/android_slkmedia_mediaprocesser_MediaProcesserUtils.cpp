// Created by Think on 16/2/25.
// Copyright © 2016年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaprocesser_MediaProcesserUtils
#define _Included_android_slkmedia_mediaprocesser_MediaProcesserUtils

#include <assert.h>

#include "JNIHelp.h"
#include "MediaLog.h"

#include "qt-faststart.h"
#include "MediaCoverImage.h"

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jint JNICALL Java_android_slkmedia_mediaprocesser_MediaProcesserUtils_QTFastStart
  (JNIEnv *env, jobject thiz, jstring jInputFile, jstring jOutputFile)
{
    char *argv[3];
    argv[0] = (char *)malloc(16);
    strcpy(argv[0], "qt-faststart");

    const char *inputFile = env->GetStringUTFChars(jInputFile, NULL);
    const char *outputFile = env->GetStringUTFChars(jOutputFile, NULL);

    argv[1] = (char*)inputFile;
    argv[2] = (char*)outputFile;

    int ret = qt_faststart_main(3, argv);

    env->ReleaseStringUTFChars(jInputFile, inputFile);
    env->ReleaseStringUTFChars(jOutputFile, outputFile);

    if (argv[0]!=NULL) {
        free(argv[0]);
        argv[0] = NULL;
    }

    return ret;
}

JNIEXPORT jint JNICALL Java_android_slkmedia_mediaprocesser_MediaProcesserUtils_GetCoverImage
  (JNIEnv *env, jobject thiz, jstring jInputFile, jint jOutputImageWidth, jint jOutputImageHeight, jint jOutputImageSize, jobject byteBuf)
{
    VideoFrame* outputImage = new VideoFrame();
    outputImage->width = jOutputImageWidth;
    outputImage->height = jOutputImageHeight;
    outputImage->frameSize = outputImage->width*outputImage->height*4;
    outputImage->data = (uint8_t*)malloc(outputImage->frameSize);

    const char *inputMediaFile = env->GetStringUTFChars(jInputFile, NULL);

    int ret = getCoverImage(inputMediaFile, outputImage);
    if (ret) {
        if (outputImage!=NULL) {
            if (outputImage->data) {
                free(outputImage->data);
                outputImage->data = NULL;
            }
            delete outputImage;
            outputImage = NULL;
        }

    }else {
		void *dst = env->GetDirectBufferAddress(byteBuf);

		jlong dstSize;
		jbyteArray byteArray = NULL;

	    if (dst == NULL) {
	        jclass byteBufClass = env->FindClass("java/nio/ByteBuffer");
	        if(byteBufClass == NULL)
	        {
	        	jniThrowRuntimeException(env, "Can't find java/nio/ByteBuffer");

	        	return -1;
	        }

	        jmethodID arrayID =
	            env->GetMethodID(byteBufClass, "array", "()[B");
        	env->DeleteLocalRef(byteBufClass);
	        if(arrayID == NULL)
	        {
	        	jniThrowRuntimeException(env, "Can't find java/nio/ByteBuffer 's array method");

	        	return -1;
	        }

	        byteArray =
	            (jbyteArray)env->CallObjectMethod(byteBuf, arrayID);

	        if (byteArray == NULL) {
	            return -1;
	        }

	        jboolean isCopy;
	        dst = env->GetByteArrayElements(byteArray, &isCopy);

	        dstSize = env->GetArrayLength(byteArray);
	    } else {
	        dstSize = env->GetDirectBufferCapacity(byteBuf);
	    }

	    if (dstSize < jOutputImageSize) {
	        if (byteArray != NULL) {
	            env->ReleaseByteArrayElements(byteArray, (jbyte *)dst, 0);
	        }

	        return -1;
	    }

	    memcpy(dst, (void*)outputImage->data, outputImage->frameSize);

        if (outputImage!=NULL) {
            if (outputImage->data) {
                free(outputImage->data);
                outputImage->data = NULL;
            }
            delete outputImage;
            outputImage = NULL;
        }

	    if (byteArray != NULL) {
	        env->ReleaseByteArrayElements(byteArray, (jbyte *)dst, 0);
	    }
    }

    env->ReleaseStringUTFChars(jInputFile, inputMediaFile);

    return ret;
}

#ifdef __cplusplus
}
#endif

#endif
