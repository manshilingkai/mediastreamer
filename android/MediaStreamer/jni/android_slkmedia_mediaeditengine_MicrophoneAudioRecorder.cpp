// Created by Think on 2019/7/16.
// Copyright © 2019年 Cell. All rights reserved.

#include <jni.h>

#ifndef _Included_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder
#define _Included_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder

#include <assert.h>

#include "JNIHelp.h"
#include "MicrophoneAudioRecorder.h"
#include "MediaLog.h"

#ifdef __cplusplus
extern "C" {
#endif

static JavaVM* jvm = NULL;
static jfieldID context = NULL;

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1Init
  (JNIEnv *env, jclass thiz)
{
	env->GetJavaVM(&jvm);
	assert(jvm != NULL);

	jclass clazz = env->FindClass("android/slkmedia/mediaeditengine/MicrophoneAudioRecorder");
    if (clazz == NULL) {
        jniThrowRuntimeException(env, "Can't find android/slkmedia/mediaeditengine/MicrophoneAudioRecorder");
        return;
    }
    
    context = env->GetFieldID(clazz, "mNativeContext", "J");
    if (context == NULL) {
        jniThrowRuntimeException(env, "Can't find MicrophoneAudioRecorder.mNativeContext");
        return;
    }

	env->DeleteLocalRef(clazz);
}


JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1Initialize
  (JNIEnv *env, jobject thiz, jint jSampleRate, jint jChannelCount, jstring jWorkDir)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder!=NULL) return JNI_FALSE;

    const char *workDir;
    workDir = env->GetStringUTFChars(jWorkDir, NULL);

    microphoneAudioRecorder = new MicrophoneAudioRecorder(jSampleRate, jChannelCount, (char*)workDir);
    microphoneAudioRecorder->registerJavaVMEnv(jvm);
    bool ret = microphoneAudioRecorder->initialize();
    if(!ret)
    {
    	microphoneAudioRecorder->terminate();
        delete microphoneAudioRecorder;
        microphoneAudioRecorder = NULL;
    }else{
        env->SetLongField(thiz, context, (int64_t)microphoneAudioRecorder);
    }
    
    env->ReleaseStringUTFChars(jWorkDir, workDir);
    
    return ret ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1StartRecord
  (JNIEnv *env, jobject thiz)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return JNI_FALSE;
    
    bool ret = microphoneAudioRecorder->startRecord();
    
    return ret ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1StartRecordWithStartPos
  (JNIEnv *env, jobject thiz, jint jStartPositionMs)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return JNI_FALSE;

    int64_t startPositionUs = (int64_t)jStartPositionMs*1000ll;
    bool ret = microphoneAudioRecorder->startRecord(startPositionUs);

    return ret ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1StopRecord
  (JNIEnv *env, jobject thiz)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return;

    microphoneAudioRecorder->stopRecord();
}

JNIEXPORT jint JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1GetRecordTimeMs
  (JNIEnv *env, jobject thiz)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return 0;

    int64_t recordTimeUs = microphoneAudioRecorder->getRecordTimeUs();
    return recordTimeUs/1000;
}

JNIEXPORT jint JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1GetRecordPcmDB
  (JNIEnv *env, jobject thiz)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return 0;

    return microphoneAudioRecorder->getRecordPcmDB();
}

JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1OpenAudioPlayer
  (JNIEnv *env, jobject thiz)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return JNI_FALSE;

    bool ret = microphoneAudioRecorder->openAudioPlayer();

    return ret ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1StartAudioPlay
  (JNIEnv *env, jobject thiz)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return JNI_FALSE;

    bool ret = microphoneAudioRecorder->startAudioPlay();

    return ret ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1SeekAudioPlay
  (JNIEnv *env, jobject thiz, jint jSeekTimeMs)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return JNI_FALSE;

    int64_t seekTimeUs = (int64_t)jSeekTimeMs*1000ll;
    bool ret = microphoneAudioRecorder->seekAudioPlay(seekTimeUs);

    return ret ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1PauseAudioPlay
  (JNIEnv *env, jobject thiz)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return;

    microphoneAudioRecorder->pauseAudioPlay();
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1CloseAudioPlayer
  (JNIEnv *env, jobject thiz)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return;

    microphoneAudioRecorder->closeAudioPlayer();
}

JNIEXPORT jint JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1GetPlayTimeMs
  (JNIEnv *env, jobject thiz)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return 0;

    int64_t playTimeUs = microphoneAudioRecorder->getPlayTimeUs();
    return playTimeUs/1000;
}

JNIEXPORT jint JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1GetPlayDurationMs
  (JNIEnv *env, jobject thiz)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return 0;

    int64_t playDurationUs = microphoneAudioRecorder->getPlayDurationUs();
    return playDurationUs/1000;
}

JNIEXPORT jint JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1GetPlayPcmDB
  (JNIEnv *env, jobject thiz)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return 0;

    return microphoneAudioRecorder->getPlayPcmDB();
}

JNIEXPORT jboolean JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1ConvertToWav
  (JNIEnv *env, jobject thiz, jstring jWavFilePath)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return JNI_FALSE;

    const char *wavFilePath;
    wavFilePath = env->GetStringUTFChars(jWavFilePath, NULL);

    bool ret = microphoneAudioRecorder->convertToWav((char*)wavFilePath);

    env->ReleaseStringUTFChars(jWavFilePath, wavFilePath);

    return ret ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT void JNICALL Java_android_slkmedia_mediaeditengine_MicrophoneAudioRecorder_Native_1Terminate
  (JNIEnv *env, jobject thiz)
{
	MicrophoneAudioRecorder* microphoneAudioRecorder = (MicrophoneAudioRecorder*)env->GetLongField(thiz, context);
    if (microphoneAudioRecorder==NULL) return;

    microphoneAudioRecorder->terminate();
    delete microphoneAudioRecorder;
    microphoneAudioRecorder = NULL;

    env->SetLongField(thiz, context, (int64_t)0);
}

#ifdef __cplusplus
}
#endif

#endif
