package android.slkmedia.mediastreamerdemo;
import android.slkmedia.mediastreamer.CameraOptions;
import android.slkmedia.mediastreamer.CameraTextureCore;
import android.slkmedia.mediastreamer.CameraTextureView;
import android.slkmedia.mediastreamer.CameraTextureViewListener;
import android.slkmedia.mediastreamer.MediaStreamer;
import android.slkmedia.mediastreamer.MediaStreamerListener;
import android.slkmedia.mediastreamer.PublishStreamOptions;
import android.slkmedia.mediastreamer.gpuimage.GPUImageUtils;
import android.slkmedia.mediastreamerdemo.R;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.media.MediaCodecInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class CameraActivity extends Activity implements CameraTextureViewListener, ActivityCompat.OnRequestPermissionsResultCallback{
	private static final String TAG = "CameraActivity";
	
    private CameraTextureView mCameraView;
    private PublishStreamOptions mPublishStreamOptions = null;
    
    private boolean mIsPaused = false;
    
    private int MY_PERMISSIONS_REQUEST_CAMERA_AND_RECORDAUDIO = 0;
    private int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    private int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 2;
    
    private String mPublishUrl = null;
    
    private boolean mCameraGranted = false;
    private boolean mRecordAudioGranted = false;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        setContentView(R.layout.activity_camera);

        Intent intent = getIntent();
        mPublishUrl = intent.getStringExtra("PublishUrl");
        
        mCameraView = (CameraTextureView)findViewById(R.id.cameraPreview_surfaceView);

        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        
        mPublishStreamOptions = new PublishStreamOptions();
        mPublishStreamOptions.enableMediaCodecEncoder = true;
        mPublishStreamOptions.publishUrl = mPublishUrl;
        mPublishStreamOptions.videoWidth = 360;
        mPublishStreamOptions.videoHeight = 640;
        mPublishStreamOptions.bitRate = 800;
        
		if (Build.VERSION.SDK_INT>22){
            if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
               //先判断有没有权限 ，没有就在这里进行权限的申请
            	mCameraGranted = false;
            }else {
              //说明已经获取到摄像头权限了 想干嘛干嘛
            	mCameraGranted = true;
            }
            
            if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED)
            {
            	mRecordAudioGranted = false;
            }else {
            	mRecordAudioGranted = true;
            }
        }else {
        	//这个说明系统版本在6.0之下，不需要动态获取权限
        	mCameraGranted = true;
        	mRecordAudioGranted = true;
        }
        
		if(mCameraGranted && mRecordAudioGranted)
		{
			startCameraPreview();
		}else if(!mCameraGranted && mRecordAudioGranted)
		{
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA},MY_PERMISSIONS_REQUEST_CAMERA);
		}else if(mCameraGranted && !mRecordAudioGranted)
		{
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.RECORD_AUDIO},MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
		}else{
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.RECORD_AUDIO},MY_PERMISSIONS_REQUEST_CAMERA_AND_RECORDAUDIO);
		}
        
        Button toggleRecording = (Button) findViewById(R.id.toggleRecording_button);
        toggleRecording.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // show interest in events resulting from ACTION_DOWN
                if (event.getAction() == MotionEvent.ACTION_DOWN) return true;
                // don't handle event unless its ACTION_UP so "doSomething()" only runs once.
                if (event.getAction() != MotionEvent.ACTION_UP) return false;

                toggleRecording();

                return true;
            }

        });
        
        Button switchCamera = (Button) findViewById(R.id.toggleSwitch_button);
        switchCamera.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mCameraView.switchCamera();
			}
		});
        
        Button enableAudioButton = (Button) findViewById(R.id.toggleEnableAudio_button);
        enableAudioButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mEnableAudio = !mEnableAudio;
				mCameraView.enableAudio(mEnableAudio);
				
				Button enableAudioButton = (Button) findViewById(R.id.toggleEnableAudio_button);
				
				if(mEnableAudio)
				{
					enableAudioButton.setText("Disable Audio");
				}else{
					enableAudioButton.setText("Enable Audio");
				}
			}
		});
        
        Button testMediaProcesser = (Button) findViewById(R.id.toggleTestMediaProcesser_button);
        testMediaProcesser.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				MediaProcesserUtils.getCoverImageFile("/sdcard/ppy.mp4", 480, 480, "/sdcard/slk.png");
			}
		});
        
        Button pauseOrResumeButton = (Button) findViewById(R.id.togglePauseOrResume_button);
        pauseOrResumeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mIsPaused = !mIsPaused;
				
				Button pauseOrResumeButton = (Button) findViewById(R.id.togglePauseOrResume_button);
				
				if(mIsPaused)
				{
					mCameraView.pausePublish();
					pauseOrResumeButton.setText("Resume");
				}else{
					mCameraView.resumePublish();
					pauseOrResumeButton.setText("Pause");
				}
			}
		});
        
        Log.v(TAG, "onCreate");
    }
    
    private boolean mEnableAudio = true;
    
    private boolean mRecordingEnabled = false;
    private void toggleRecording()
    {
        mRecordingEnabled = !mRecordingEnabled;
        if (mRecordingEnabled) {
        	mCameraView.startPublish(mPublishStreamOptions);
        } else {
        	mCameraView.stopPublish();
        }
    }
    
    private void startCameraPreview()
    {
        CameraOptions cameraOptions = new CameraOptions();
        cameraOptions.previewWidth = 720;
        cameraOptions.previewHeight = 1280;
        
        mCameraView.initialize(cameraOptions);
        mCameraView.setListener(this);

        mCameraView.setFilter(GPUImageUtils.FILTER_RGB, null);
        mCameraView.setDisplayScalingMode(GPUImageUtils.DISPLAY_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v(TAG, "onStop");
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCameraView.release();
        mCameraView = null;
        Log.v(TAG, "onDestroy");
    }
    
    @Override
    public void onRequestPermissionsResult(int requestCode,
            String permissions[], int[] grantResults) {
    	
    	if(requestCode==MY_PERMISSIONS_REQUEST_CAMERA)
    	{    		
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, yay! Do the
                // contacts-related task you need to do.
            	
            	mCameraGranted = true;
            	
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CameraActivity.this, "已获取摄像头权限", Toast.LENGTH_SHORT).show();
                    }
                });	

            } else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            	
            	mCameraGranted = false;
            	
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CameraActivity.this, "获取摄像头权限被拒绝", Toast.LENGTH_SHORT).show();
                    }
                });	
            }
    	}else if(requestCode==MY_PERMISSIONS_REQUEST_RECORD_AUDIO)
    	{    		
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, yay! Do the
                // contacts-related task you need to do.
            	
            	mRecordAudioGranted = true;
            	
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CameraActivity.this, "已获取麦克风权限", Toast.LENGTH_SHORT).show();
                    }
                });	

            } else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            	
            	mRecordAudioGranted = false;
            	
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CameraActivity.this, "获取麦克风权限被拒绝", Toast.LENGTH_SHORT).show();
                    }
                });	
            }
    	}else if(requestCode==MY_PERMISSIONS_REQUEST_CAMERA_AND_RECORDAUDIO)
    	{
    		if(grantResults.length == 2)
    		{
    			if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
    			{
                	mCameraGranted = true;
                	
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CameraActivity.this, "已获取摄像头权限", Toast.LENGTH_SHORT).show();
                        }
                    });
    			}else{
                	mCameraGranted = false;
                	
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CameraActivity.this, "获取摄像头权限被拒绝", Toast.LENGTH_SHORT).show();
                        }
                    });	
    			}
    			
    			if(grantResults[1] == PackageManager.PERMISSION_GRANTED)
    			{
                	mRecordAudioGranted = true;
                	
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CameraActivity.this, "已获取麦克风权限", Toast.LENGTH_SHORT).show();
                        }
                    });	
    			}else{
                	mRecordAudioGranted = false;
                	
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CameraActivity.this, "获取麦克风权限被拒绝", Toast.LENGTH_SHORT).show();
                        }
                    });	
    			}
    		}
    	}
    	
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				startCameraPreview();
			}
		});
    }

	@Override
	public void onMediaStreamerConnecting() {
		Log.i(TAG, "onMediaStreamerConnecting");
	}

	@Override
	public void onMediaStreamerConnected() {
		Log.i(TAG, "onMediaStreamerConnected");
	}

	@Override
	public void onMediaStreamerStreaming() {
		Log.i(TAG, "onMediaStreamerStreaming");

		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Button toggleRecording = (Button) findViewById(R.id.toggleRecording_button);
				toggleRecording.setPressed(true);
			}
		});
	}

	@Override
	public void onMediaStreamerError(int errortype) {
		Log.i(TAG, "onMediaStreamerError");
		
		if(errortype == MediaStreamer.CALLBACK_MEDIA_STREAMER_ERROR_POOR_NETWORK)
		{
			Log.i(TAG, "network is too poor");
		}
	}

	@Override
	public void onMediaStreamerEnd() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Button toggleRecording = (Button) findViewById(R.id.toggleRecording_button);
				toggleRecording.setPressed(false);
			}
		});
	}

	@Override
	public void onMediaStreamerInfo(int infoType, int infoValue) {
		if(infoType == MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_REAL_BITRATE)
		{
			Log.i(TAG, "Real bitrate : "+String.valueOf(infoValue));
		}else if(infoType == MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_REAL_FPS)
		{
			Log.i(TAG, "Real fps : "+String.valueOf(infoValue));
		}else if(infoType == MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_DELAY_TIME)
		{
			Log.i(TAG, "Delay time : "+String.valueOf(infoValue));
		}else if(infoType == MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_DOWN_BITRATE)
		{
			Log.i(TAG, "Down target bitrate : "+String.valueOf(infoValue));
		}else if(infoType == MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_UP_BITRATE)
		{
			Log.i(TAG, "Up target bitrate : "+String.valueOf(infoValue));
		}else if(infoType == MediaStreamer.CALLBACK_MEDIA_STREAMER_INFO_PUBLISH_TIME)
		{
			Log.i(TAG, "Record time : "+String.valueOf((float)(infoValue*0.1f)));
		}
	}
}
