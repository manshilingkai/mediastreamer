package android.slkmedia.mediastreamerdemo;

import android.app.Activity;
import android.os.Bundle;
import android.slkmedia.mediaprocesser.MediaInfo;
import android.slkmedia.mediaprocesser.MediaProcesserListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MediaInfoActivity extends Activity implements MediaProcesserListener{
	private static final String TAG = "MediaInfoActivity";

	private String mPublishUrl = "/sdcard/slk.mp4";
	
	private MediaInfo mMediaInfo;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_mediainfo);

        mMediaInfo = new MediaInfo();
        
        Button start = (Button)findViewById(R.id.Start);
        start.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mMediaInfo.setMediaProcesserListener(MediaInfoActivity.this);
				mMediaInfo.start(mPublishUrl, 10000, 14000, "/sdcard/", 180, 180, 4);
//				mMediaInfo.start(mPublishUrl, "/sdcard/", 180, 180, 6);
			}
		});
        
        Button stop = (Button)findViewById(R.id.Stop);
        stop.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mMediaInfo.stop();
			}
		});
    }
    
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    
	@Override
	protected void onStart() {
		super.onStart();
	}

    @Override
    protected void onStop() {
        super.onStop();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        
        mMediaInfo.stop();
        mMediaInfo.release();
    }

	@Override
	public void onMediaProcesserError(int errorType) {
		Log.i(TAG, "onMediaProcesserError errorType : " + String.valueOf(errorType));
	}

	@Override
	public void onMediaProcesserInfo(int infoType, int infoValue) {
		Log.i(TAG, "onMediaProcesserInfo infoType : " + String.valueOf(infoType) + " infoValue : " + String.valueOf(infoValue));
	}

	@Override
	public void onMediaProcesserMediaDetailInfo(long duration, int width,
			int height) {
		Log.i(TAG, "onMediaProcesserMediaDetailInfo duration : " + String.valueOf(duration) + " width : " + String.valueOf(width) + " height : " + String.valueOf(height));
	}

	@Override
	public void onMediaProcesserMediaThumbnail(String imagePath) {
		Log.i(TAG, "onMediaProcesserMediaThumbnail imagePath : " + imagePath);
		
//		this.runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				MediaInfo.getCoverImageToImageFile("/sdcard/slk.mp4", 720, 1280, "/sdcard/slk.png");
//			}
//		});
	}

	@Override
	public void onMediaProcesserEnd() {
		Log.i(TAG, "onMediaProcesserEnd");
	}
}
