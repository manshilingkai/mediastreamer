package android.slkmedia.mediastreamerdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.slkmedia.mediaprocesser.MediaInfo;
import android.slkmedia.mediaprocesser.MediaInfo.MediaDetailInfo;
import android.slkmedia.mediaprocesser.MediaProcesserUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity{
	private static final String TAG = "MainActivity";

//	private String mPublishUrl = "rtmp://douya.push.pptvyun.com/W1bM/0a2dn6yXoaWcnqeL4K2fnq2ap-ydm6udoqWinQ?ppyunid=166215051&cpn=35757&type=web.cloudplay&ydpf_pt=2&wsSecret=f239e87729e7718338877b3c13d37ecb&wsABSTime=5da536f0&wsSeek=on";
	private String mPublishUrl = "/sdcard/2019_04_23_1.mp4";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_main);

        Button record = (Button)findViewById(R.id.Record);
        record.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent in = new Intent();
				in.putExtra("PublishUrl", mPublishUrl);
				in.setClass(MainActivity.this, CameraActivity.class);
				startActivity(in);
			}
		});
        
        Button merge = (Button)findViewById(R.id.Merge);
        merge.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent in = new Intent();
				in.setClass(MainActivity.this, MergeActivity.class);
				startActivity(in);
			}
		});

        Button mediaInfo = (Button)findViewById(R.id.MediaInfo);
                
        mediaInfo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				Intent in = new Intent();
//				in.setClass(MainActivity.this, MediaInfoActivity.class);
//				startActivity(in);

//				MediaDetailInfo detailInfo = MediaInfo.getMediaDetailInfo("/sdcard/slk.mp4");
//				Log.i(TAG, "Width : " + String.valueOf(detailInfo.videoWidth));
//				Log.i(TAG, "Height : " + String.valueOf(detailInfo.videoHeight));
//				Log.i(TAG, "FPS : " + String.valueOf(detailInfo.videoFps));
//				Log.i(TAG, "KBPS : " + String.valueOf(detailInfo.kbps));
//				Log.i(TAG, "DurationMs : " + String.valueOf(detailInfo.durationMs));

				MediaInfo.getCoverImageToImageFileWithPosition("/sdcard/no_thumb_image.mp4", 10000, 720, 1280, "/sdcard/no_thumb_image.png");
			}
		});
        
//        Log.i(TAG, "Duration : "+String.valueOf(MediaInfo.getMediaDuration(mPublishUrl)) + "Ms");
        
        Button remuxer = (Button)findViewById(R.id.MediaRemuxer);
        remuxer.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent in = new Intent();
				in.setClass(MainActivity.this, MediaRemuxerActivity.class);
				startActivity(in);
			}
		});
        
        Button mediaEditEngine = (Button)findViewById(R.id.MediaEditEngine);
        mediaEditEngine.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent in = new Intent();
				in.setClass(MainActivity.this, MediaEditEngineActivity.class);
				startActivity(in);
			}
		});
    }
    
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    
	@Override
	protected void onStart() {
		super.onStart();
	}

    @Override
    protected void onStop() {
        super.onStop();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
