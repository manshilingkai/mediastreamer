package android.slkmedia.mediastreamerdemo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.slkmedia.mediaeditengine.AudioEditEngineEnv;
import android.slkmedia.mediaeditengine.AudioPlayer;
import android.slkmedia.mediaeditengine.AudioPlayerListener;
import android.slkmedia.mediaeditengine.MediaDubbingProducer;
import android.slkmedia.mediaeditengine.MediaDubbingProducerListener;
import android.slkmedia.mediaeditengine.MediaDubbingProducerOptions;
import android.slkmedia.mediaeditengine.MicrophoneAudioRecorder;
import android.slkmedia.mediaeditengine.MicrophoneAudioRecorderOptions;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MediaEditEngineActivity extends Activity implements MediaDubbingProducerListener, AudioPlayerListener{
	private static final String TAG = "MediaEditEngineActivity";

	private MicrophoneAudioRecorder mMicrophoneAudioRecorder = null;
	private Handler workHandler=new Handler();
	
	private MediaDubbingProducer mMediaDubbingProducer = null;
	
	private AudioPlayer mAudioPlayer = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_mediaeditengine);

		AudioEditEngineEnv.setupAudioManager(this);
		
		mMicrophoneAudioRecorder = new MicrophoneAudioRecorder(this);
		MicrophoneAudioRecorderOptions options = new MicrophoneAudioRecorderOptions();
		options.isControlAudioManger = false;
		mMicrophoneAudioRecorder.initialize(options);
		
		mMediaDubbingProducer = new MediaDubbingProducer();
		mMediaDubbingProducer.setMediaDubbingProducerListener(this);
		
		mAudioPlayer = new AudioPlayer();
		mAudioPlayer.setListener(this);
		
		Button startRecord = (Button) findViewById(R.id.StartRecord);
		startRecord.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				workHandler.post(recordinfoRunnable);
				mMicrophoneAudioRecorder.startRecord();
			}
		});

		Button stopRecord = (Button) findViewById(R.id.StopRecord);
		stopRecord.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mMicrophoneAudioRecorder.stopRecord();
				workHandler.removeCallbacks(recordinfoRunnable); 
			}
		});
		
		Button openAudioPlayer = (Button) findViewById(R.id.OpenAudioPlayer);
		openAudioPlayer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
//				mMicrophoneAudioRecorder.openAudioPlayer();
				
				mAudioPlayer.setDataSource("/sdcard/fnby.mp3");
				mAudioPlayer.prepareAsyncToPlay();
			}
		});
		
		
		Button startAudioPlay = (Button) findViewById(R.id.StartAudioPlay);
		startAudioPlay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
//				mMicrophoneAudioRecorder.startAudioPlay();
				
				mAudioPlayer.play();
			}
		});
		
		Button seekAudioPlay = (Button) findViewById(R.id.SeekAudioPlay);
		seekAudioPlay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
//				mMicrophoneAudioRecorder.seekAudioPlay(2000);
				
				mAudioPlayer.seekTo(2000);
			}
		});
		
		Button pauseAudioPlay = (Button) findViewById(R.id.PauseAudioPlay);
		pauseAudioPlay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
//				mMicrophoneAudioRecorder.pauseAudioPlay();
				
				mAudioPlayer.pause();
			}
		});
		
		Button closeAudioPlayer = (Button) findViewById(R.id.CloseAudioPlayer);
		closeAudioPlayer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
//				mMicrophoneAudioRecorder.closeAudioPlayer();
				
				mAudioPlayer.stop();
			}
		});
		
		Button covertToWav = (Button) findViewById(R.id.ConvertToWAV);
		covertToWav.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mMicrophoneAudioRecorder.convertToWav("/sdcard/123.wav");
			}
		});
		
		Button startProduce = (Button) findViewById(R.id.StartProduce);
		startProduce.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				MediaDubbingProducerOptions options = new MediaDubbingProducerOptions();
				options.videoUrl = "/sdcard/fnby.mp4";
				options.bgmUrl = "/sdcard/fnby.mp3";
				options.dubUrl = "/sdcard/123.wav";
				options.productUrl = "/sdcard/product.mp4";
				mMediaDubbingProducer.start(options);
			}
		});
		
		Button stopProduce = (Button) findViewById(R.id.StopProduce);
		stopProduce.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mMediaDubbingProducer.Stop(false);
			}
		});
	}
	
	private Runnable recordinfoRunnable = new Runnable()
	{
		@Override
		public void run() {
			int recordTimeMs = mMicrophoneAudioRecorder.getRecordTimeMs();
			int recordPcmDB = mMicrophoneAudioRecorder.getRecordPcmDB();
			Log.d(TAG, "recordTimeMs : "+String.valueOf(recordTimeMs));
			Log.d(TAG, "recordPcmDB : "+String.valueOf(recordPcmDB));
			workHandler.postDelayed(this, 50); 
		}
	};

	@Override
	public void onDubbingStart() {
		Log.d(TAG, "onDubbingStart");
	}

	@Override
	public void onDubbingError(int errorType) {
		Log.d(TAG, "onDubbingError : " + String.valueOf(errorType));
	}

	@Override
	public void onDubbingInfo(int infoType, int infoValue) {
		if(infoType == MediaDubbingProducer.CALLBACK_MEDIA_DUBBING_INFO_WRITE_TIMESTAMP)
		{
			Log.d(TAG, "Dubbing Write TimeStamp : " + String.valueOf(infoValue));
		}
	}

	@Override
	public void onDubbingEnd() {
		Log.d(TAG, "onDubbingEnd");
	}

	// AudioPlayer
	@Override
	public void onPrepared() {
	}

	@Override
	public void onError(int what, int extra) {
	}

	@Override
	public void onInfo(int what, int extra) {
	}

	@Override
	public void onCompletion() {
	}

	@Override
	public void OnSeekComplete() {
	}
}
