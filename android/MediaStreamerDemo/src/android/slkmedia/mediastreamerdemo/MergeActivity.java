package android.slkmedia.mediastreamerdemo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Bundle;
import android.slkmedia.mediaprocesser.MediaEffect;
import android.slkmedia.mediaprocesser.MediaInfo;
import android.slkmedia.mediaprocesser.MediaInfo.MediaDetailInfo;
import android.slkmedia.mediaprocesser.MediaMaterial;
import android.slkmedia.mediaprocesser.MediaMergeAlgorithm;
import android.slkmedia.mediaprocesser.MediaMerger;
import android.slkmedia.mediaprocesser.MediaProcesserListener;
import android.slkmedia.mediaprocesser.MediaProduct;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MergeActivity extends Activity implements MediaProcesserListener{
	private static final String TAG = "MergeActivity";

	private String mResource1Url = "/sdcard/slk.mp4";
	private String mResource2Url = "/sdcard/XBH.wav";
	private String mResource3Url = "/sdcard/overlay.png";
	private String mResource4Url = "/sdcard/IMG_0549.MOV";
	private String mResourceUrlForWebp = "/sdcard/test.webp";
	private String mResourceUrlForGif = "/sdcard/test.gif";
	private String mResource5Url = "/sdcard/ppy2.mp3";
	
//	private String mResource10Url = "/sdcard/temp_cache_1499655265266.mp4";
//	private String mResource11Url = "/sdcard/temp_cache_1499658119036.mp4";
	
	private MediaMerger mediaMerger = null;
	private MediaMaterial[] mediaMaterials = null;
	private MediaEffect[] mediaEffects = null;
	private MediaProduct mediaProduct = null;
	int mediaMergeAlgorithm = MediaMergeAlgorithm.MEDIA_MERGE_ALGORITHM_TIMELINE;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        copyFileOrDir("filter");
        copyFileOrDir("ttf");
        
        setContentView(R.layout.activity_merge);

        MediaDetailInfo detailInfo = MediaInfo.getMediaDetailInfo(mResource4Url);
        if(detailInfo!=null)
        {
        	Log.v(TAG, "MediaDetailInfo hasVideo : " + String.valueOf(detailInfo.hasVideo));
        	Log.v(TAG, "MediaDetailInfo hasAudio : " + String.valueOf(detailInfo.hasAudio));
            Log.v(TAG, "MediaDetailInfo videoWidth : " + String.valueOf(detailInfo.videoWidth));
            Log.v(TAG, "MediaDetailInfo videoHeight : " + String.valueOf(detailInfo.videoHeight));
            Log.v(TAG, "MediaDetailInfo videoFps : " + String.valueOf(detailInfo.videoFps));
            Log.v(TAG, "MediaDetailInfo kbps : " + String.valueOf(detailInfo.kbps));
            Log.v(TAG, "MediaDetailInfo durationMs : " + String.valueOf(detailInfo.durationMs));
            Log.v(TAG, "MediaDetailInfo rotate : " + String.valueOf(detailInfo.rotate));
            Log.v(TAG, "MediaDetailInfo isH264Codec : " + String.valueOf(detailInfo.isH264Codec));
            Log.v(TAG, "MediaDetailInfo isAACCodec : " + String.valueOf(detailInfo.isAACCodec));
        }else {
        	Log.v(TAG, "getMediaDetailInfo Fail");
        	return;
        }

        mediaMerger = new MediaMerger();
        
        /*
//        detailInfo.isH264Codec = false;
        if(detailInfo.isH264Codec && detailInfo.isAACCodec)
        {
            mediaMaterials = new MediaMaterial[1];
            mediaMaterials[0] = new MediaMaterial();
            mediaMaterials[0].url = mResource4Url;
            
            mediaEffects = null;
            
            mediaProduct = new MediaProduct();
            mediaProduct.url = "/sdcard/product_remux.mp4";
            mediaProduct.media_product_type = MediaProduct.MEDIA_PRODUCT_TYPE_MP4;
            
            mediaMergeAlgorithm = MediaMergeAlgorithm.MEDIA_MERGE_ALGORITHM_REMUX;
        }else {
            mediaMaterials = new MediaMaterial[1];
            mediaMaterials[0] = new MediaMaterial();
            mediaMaterials[0].url = mResource4Url;
            
            if(detailInfo.hasVideo && !detailInfo.hasAudio)
            {
            	mediaMaterials[0].media_material_type = MediaMaterial.MEDIA_MATERIAL_TYPE_VIDEO;
            }else if(!detailInfo.hasVideo && detailInfo.hasAudio)
            {
            	mediaMaterials[0].media_material_type = MediaMaterial.MEDIA_MATERIAL_TYPE_AUDIO;
            }else {
            	mediaMaterials[0].media_material_type = MediaMaterial.MEDIA_MATERIAL_TYPE_VIDEO_AUDIO;
            }
            
            mediaMaterials[0].startPos = 0*1000;
            mediaMaterials[0].endPos = detailInfo.durationMs;
            mediaMaterials[0].volume = 1.0f;
            mediaMaterials[0].speed = 1.0f;
            
            mediaEffects = null;
            
            mediaProduct = new MediaProduct();
            mediaProduct.url = "/sdcard/product.mp4";
            mediaProduct.media_product_type = MediaProduct.MEDIA_PRODUCT_TYPE_MP4;
            mediaProduct.hasVideo = detailInfo.hasVideo;
            if(detailInfo.rotate==90 || detailInfo.rotate==270)
            {
            	mediaProduct.videoWidth = detailInfo.videoHeight;
            	mediaProduct.videoHeight = detailInfo.videoWidth;
            }else {
                mediaProduct.videoWidth = detailInfo.videoWidth;
                mediaProduct.videoHeight = detailInfo.videoHeight;
            }

            mediaProduct.hasAudio = detailInfo.hasAudio;
            mediaProduct.bps = detailInfo.kbps;
            
            mediaMergeAlgorithm = MediaMergeAlgorithm.MEDIA_MERGE_ALGORITHM_TIMELINE;
        }
        */
        
        
        mediaMaterials = new MediaMaterial[1];
        mediaMaterials[0] = new MediaMaterial();
        mediaMaterials[0].url = "/sdcard/YP.mp4";
        mediaMaterials[0].media_material_type = MediaMaterial.MEDIA_MATERIAL_TYPE_VIDEO_AUDIO;
        mediaMaterials[0].startPos = 0*1000;
        mediaMaterials[0].endPos = 13*1000;
        mediaMaterials[0].volume = 1.0f;
        mediaMaterials[0].speed = 1.0f;
        
        /*
        mediaProduct = new MediaProduct();
        mediaProduct.url = "/sdcard/product.gif";
        mediaProduct.media_product_type = MediaProduct.MEDIA_PRODUCT_TYPE_GIF;
        mediaProduct.hasVideo = true;
        mediaProduct.videoWidth = 180;
        mediaProduct.videoHeight = 320;
        mediaProduct.hasAudio = false;
        mediaProduct.bps = 256;*/
        
        mediaProduct = new MediaProduct();
        mediaProduct.url = "/sdcard/product.mp4";
        mediaProduct.media_product_type = MediaProduct.MEDIA_PRODUCT_TYPE_MP4;
        mediaProduct.hasVideo = true;
        mediaProduct.videoWidth = 544;
        mediaProduct.videoHeight = 960;
        mediaProduct.isAspectFit = true;
        mediaProduct.hasAudio = true;
        mediaProduct.bps = 6500;
                
        /*
        mediaMaterials = new MediaMaterial[2];
        mediaMaterials[0] = new MediaMaterial();
        mediaMaterials[0].url = mResource4Url;
        mediaMaterials[0].media_material_type = MediaMaterial.MEDIA_MATERIAL_TYPE_VIDEO_AUDIO;
        mediaMaterials[0].startPos = 0;
        mediaMaterials[0].endPos = 10*1000;
        mediaMaterials[0].volume = 1.0f;
        mediaMaterials[0].speed = 2.0f;
        
        mediaMaterials[1] = new MediaMaterial();
        mediaMaterials[1].url = mResource4Url;
        mediaMaterials[1].media_material_type = MediaMaterial.MEDIA_MATERIAL_TYPE_VIDEO_AUDIO;
        mediaMaterials[1].startPos = 0*1000;
        mediaMaterials[1].endPos = 7500;
        mediaMaterials[1].volume = 1.0f;
        mediaMaterials[1].speed = 0.5f;
        
        mediaEffects = new MediaEffect[5];
        mediaEffects[0] = new MediaEffect();
        mediaEffects[0].url = "/data/data/" + MergeActivity.this.getPackageName() + "/" + "filter";
        mediaEffects[0].media_effect_type = MediaEffect.MEDIA_EFFECT_TYPE_FILTER;
        mediaEffects[0].effect_in_pos = 0*1000;
        mediaEffects[0].effect_out_pos = 6*1000;
        mediaEffects[0].gpu_image_filter_type = MediaEffect.GPU_IMAGE_FILTER_N1977;
        
        mediaEffects[1] = new MediaEffect();
        mediaEffects[1].url = mResource2Url;
        mediaEffects[1].media_effect_type = MediaEffect.MEDIA_EFFECT_TYPE_AUDIO;
        mediaEffects[1].effect_in_pos = 0*1000;
        mediaEffects[1].effect_out_pos = 20*1000;
        mediaEffects[1].startPos = 240*1000;
        mediaEffects[1].endPos = 260*1000;
        mediaEffects[1].volume = 0.8f;
        mediaEffects[1].speed = 1.0f;
        
        mediaEffects[2] = new MediaEffect();
        mediaEffects[2].url = mResourceUrlForGif;
        mediaEffects[2].media_effect_type = MediaEffect.MEDIA_EFFECT_TYPE_GIF;
        mediaEffects[2].effect_in_pos = 2*1000;
        mediaEffects[2].effect_out_pos = 15*1000;
        mediaEffects[2].x = 240;
        mediaEffects[2].y = 320;
        mediaEffects[2].rotation = 30;
        mediaEffects[2].scale = 0.9f;
        mediaEffects[2].flipHorizontal = false;
        mediaEffects[2].flipVertical = true;
        
        mediaEffects[3] = new MediaEffect();
        mediaEffects[3].url = null;
        mediaEffects[3].media_effect_type = MediaEffect.MEDIA_EFFECT_TYPE_TEXT;
        mediaEffects[3].effect_in_pos = 0*1000;
        mediaEffects[3].effect_out_pos = 10*1000;
        mediaEffects[3].x = 80;
        mediaEffects[3].y = 240;
        mediaEffects[3].rotation = 60;
        mediaEffects[3].scale = 1.2f;
        mediaEffects[3].flipHorizontal = true;
        mediaEffects[3].flipVertical = false;
        mediaEffects[3].text = "LLll123 魔都凯凯";
        mediaEffects[3].fontLibPath = "/data/data/" + MergeActivity.this.getPackageName() + "/" + "ttf/" + "han_yi_you_rang_ti.ttf";
        mediaEffects[3].fontSize = 32;
        mediaEffects[3].fontColor = Color.argb(255, 255, 0, 0);
        mediaEffects[3].leftMargin = 4;
        mediaEffects[3].rightMargin = 4;
        mediaEffects[3].topMargin = 4;
        mediaEffects[3].bottomMargin = 4;
        mediaEffects[3].lineSpace = 2;
        mediaEffects[3].wordSpace = 2;
        mediaEffects[3].text_animation_type = MediaEffect.TEXT_ANIMATION_NONE;
        
        mediaEffects[4] = new MediaEffect();
        mediaEffects[4].url = mResource5Url;
        mediaEffects[4].media_effect_type = MediaEffect.MEDIA_EFFECT_TYPE_AUDIO;
        mediaEffects[4].effect_in_pos = 0*1000;
        mediaEffects[4].effect_out_pos = 20*1000;
        mediaEffects[4].startPos = 20*1000;
        mediaEffects[4].endPos = 40*1000;
        mediaEffects[4].volume = 0.0f;
        mediaEffects[4].speed = 1.0f;
        
        mediaProduct = new MediaProduct();
        mediaProduct.url = "/sdcard/product.mp4";
        mediaProduct.media_product_type = MediaProduct.MEDIA_PRODUCT_TYPE_MP4;
        mediaProduct.hasVideo = true;
        mediaProduct.videoWidth = 480;
        mediaProduct.videoHeight = 640;
        mediaProduct.hasAudio = true;
        mediaProduct.bps = 2000;
        */
        
        Button start = (Button)findViewById(R.id.Start);
        start.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mediaMerger.setMediaProcesserListener(MergeActivity.this);
				mediaMerger.Start(mediaMaterials, mediaEffects, mediaMergeAlgorithm, mediaProduct);
				
		        runOnUiThread(new Runnable() {
		            @Override
		            public void run() {
		                Toast.makeText(MergeActivity.this, "Media Processer Start", Toast.LENGTH_SHORT).show();
		            }
		        });	
			}
		});
        
        Button stop = (Button)findViewById(R.id.Stop);
        stop.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mediaMerger.Stop();
			}
		});

    }
    
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    
	@Override
	protected void onStart() {
		super.onStart();
	}

    @Override
    protected void onStop() {
        super.onStop();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        
        if(mediaMerger!=null)
        {
            mediaMerger.Stop();
            mediaMerger.Release();
            mediaMerger = null;
        }
    }

	@Override
	public void onMediaProcesserError(int errorType) {
		Log.i(TAG, "onMediaProcesserError errorType : " + String.valueOf(errorType));
	}

	private int mProgressValue = 0;
	@Override
	public void onMediaProcesserInfo(int infoType, int infoValue) {
		Log.i(TAG, "onMediaProcesserInfo infoType : " + String.valueOf(infoType) + " infoValue : " + String.valueOf(infoValue));
		mProgressValue = infoValue;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MergeActivity.this, String.valueOf(mProgressValue), Toast.LENGTH_SHORT).show();
            }
        });	
	}

	@Override
	public void onMediaProcesserMediaDetailInfo(long duration, int width,
			int height) {
		Log.i(TAG, "onMediaProcesserMediaDetailInfo duration : " + String.valueOf(duration) + " width : " + String.valueOf(width) + " height : " + String.valueOf(height));
	}

	@Override
	public void onMediaProcesserMediaThumbnail(String imagePath) {
		Log.i(TAG, "onMediaProcesserMediaThumbnail imagePath : " + imagePath);
	}

	@Override
	public void onMediaProcesserEnd() {
		Log.i(TAG, "onMediaProcesserEnd");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MergeActivity.this, "Media Processer End", Toast.LENGTH_SHORT).show();
            }
        });	
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private void copyFileOrDir(String path) {
	    AssetManager assetManager = this.getAssets();
	    String assets[] = null;
	    try {
	        assets = assetManager.list(path);
	        if (assets.length == 0) {
	            copyFile(path);
	        } else {
	            String fullPath = "/data/data/" + this.getPackageName() + "/" + path;
	            File dir = new File(fullPath);
	            if (!dir.exists())
	                dir.mkdir();
	            for (int i = 0; i < assets.length; ++i) {
	                copyFileOrDir(path + "/" + assets[i]);
	            }
	        }
	    } catch (IOException ex) {
	        Log.e("tag", "I/O Exception", ex);
	    }
	}

	private void copyFile(String filename) {
	    AssetManager assetManager = this.getAssets();

	    InputStream in = null;
	    OutputStream out = null;
	    try {
	        in = assetManager.open(filename);
	        String newFileName = "/data/data/" + this.getPackageName() + "/" + filename;
	        out = new FileOutputStream(newFileName);

	        byte[] buffer = new byte[1024];
	        int read;
	        while ((read = in.read(buffer)) != -1) {
	            out.write(buffer, 0, read);
	        }
	        in.close();
	        in = null;
	        out.flush();
	        out.close();
	        out = null;
	    } catch (Exception e) {
	        Log.e("tag", e.getMessage());
	    }

	}
}
