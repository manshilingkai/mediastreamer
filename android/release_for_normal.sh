#! /usr/bin/env bash
#
# Copyright (C) 2014-2017 William Shi <manshilingkai@gmail.com>
#
#

set -e

# MediaStreamer
cd ./MediaStreamer/

# MediaStreamer libs
rm -rf ./libs
rm -rf ./obj

cd jni/

ndk-build NDK_PROJECT_PATH=../ APP_BUILD_SCRIPT=./Android.mk NDK_APPLICATION_MK=./Application.mk

cd ../

# MediaStreamer jar + libs
ant release
cd ../


