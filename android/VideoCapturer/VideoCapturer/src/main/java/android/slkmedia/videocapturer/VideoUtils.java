package android.slkmedia.videocapturer;

public class VideoUtils {
    private static final int ALIGNMENT_DEFAULT = 2;
    private static int ALIGNMENT = ALIGNMENT_DEFAULT;

    public static Size scaleSizeWithAlignment(int inputW, int inputH, int maxW, int maxH) {
        if (inputW * inputH * maxW * maxH == 0) {
            return new Size(inputW, inputH);
        }

        int inputLenShort = Math.min(inputW, inputH);
        int inputLenLong = Math.max(inputW, inputH);
        int requestLenShort = Math.min(maxW, maxH);
        int requestLenLong = Math.max(maxW, maxH);

        int targetLong;
        int targetShort;
        if(requestLenShort*inputLenLong>inputLenShort*requestLenLong)
        {
            targetLong = alignment(requestLenLong);
            targetShort = alignment(requestLenLong * 1.0f * inputLenShort / inputLenLong);
        }else {
            targetLong = alignment(requestLenShort * 1.0f * inputLenLong / inputLenShort);
            targetShort = alignment(requestLenShort);
        }

        return inputW > inputH ? new Size(targetLong, targetShort) : new Size(targetShort, targetLong);
    }

    public static int alignment(double i) {
        return ALIGNMENT * ((int) Math.floor((i) / ALIGNMENT));
    }
}
