package android.slkmedia.videocapturer;

import android.slkmedia.videocapturer.CameraEnumerationAndroid.CaptureFormat;
import java.util.List;

public interface CameraEnumerator {
    public String[] getDeviceNames();
    public boolean isFrontFacing(String deviceName);
    public boolean isBackFacing(String deviceName);
    public List<CaptureFormat> getSupportedFormats(String deviceName);

    public CameraVideoCapturer createCapturer(
            String deviceName, CameraVideoCapturer.CameraEventsHandler eventsHandler);
}
