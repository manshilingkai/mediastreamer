package android.slkmedia.videocapturer;

import android.content.Context;
import android.view.Surface;
import android.view.WindowManager;

public interface CameraSession {
    enum FailureType { ERROR, DISCONNECTED }

    // Callbacks are fired on the camera thread.
    interface CreateSessionCallback {
        void onDone(CameraSession session);
        void onFailure(FailureType failureType, String error);
    }

    // Events are fired on the camera thread.
    interface Events {
        void onCameraOpening();
        void onCameraError(CameraSession session, String error);
        void onCameraDisconnected(CameraSession session);
        void onCameraClosed(CameraSession session);
        void onFrameCaptured(CameraSession session, VideoFrame frame);
    }

    /**
     * Stops the capture. Waits until no more calls to capture observer will be made.
     * If waitCameraStop is true, also waits for the camera to stop.
     */
    void stop();

    static int getDeviceOrientation(Context context) {
        final WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        switch (wm.getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_90:
                return 90;
            case Surface.ROTATION_180:
                return 180;
            case Surface.ROTATION_270:
                return 270;
            case Surface.ROTATION_0:
            default:
                return 0;
        }
    }
}
