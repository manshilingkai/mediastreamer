package android.slkmedia.videocapturer;

import android.content.Context;
import android.os.Handler;

import androidx.annotation.Nullable;

public class Camera1Capturer extends CameraCapturer {
    private final boolean captureToTexture;

    public Camera1Capturer(
            String cameraName, CameraEventsHandler eventsHandler, boolean captureToTexture) {
        super(cameraName, eventsHandler, new Camera1Enumerator(captureToTexture));

        this.captureToTexture = captureToTexture;
    }

    @Override
    protected void createCameraSession(CameraSession.CreateSessionCallback createSessionCallback, CameraSession.Events events, Context applicationContext, Handler workHandler, String cameraName, int width, int height, int framerate) {
        Camera1Session.create(createSessionCallback, events, captureToTexture, applicationContext,
                workHandler, Camera1Enumerator.getCameraIndex(cameraName), width, height,
                framerate);
    }
}
