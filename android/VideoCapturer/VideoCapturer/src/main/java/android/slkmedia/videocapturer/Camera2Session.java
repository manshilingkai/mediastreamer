package android.slkmedia.videocapturer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.os.Build;
import android.os.Handler;
import android.slkmedia.videocapturer.CameraEnumerationAndroid.CaptureFormat;
import android.util.Log;
import android.util.Range;
import android.view.Surface;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
class Camera2Session implements CameraSession {
    private static final String TAG = "Camera2Session";

    private static enum SessionState {RUNNING, STOPPED}

    private final Handler cameraThreadHandler;
    private final CreateSessionCallback callback;
    private final Events events;
    private final Context applicationContext;
    private final CameraManager cameraManager;
    private final String cameraId;
    private final int width;
    private final int height;
    private final int framerate;

    // Initialized at start
    private CameraCharacteristics cameraCharacteristics;
    private int cameraOrientation;
    private boolean isCameraFrontFacing;
    private int fpsUnitFactor;
    private CaptureFormat captureFormat;

    // Initialized when camera opens
    @Nullable
    private CameraDevice cameraDevice;
    @Nullable
    private Surface surface;

    // Initialized when capture session is created
    @Nullable
    private CameraCaptureSession captureSession;

    // State
    private SessionState state = SessionState.RUNNING;
    private boolean firstFrameReported;

    // Used only for stats. Only used on the camera thread.
    private final long constructionTimeNs; // Construction time of this class.

    private final SurfaceTexture surfaceTexture;
    private final int oesTextureId;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private class CameraStateCallback extends CameraDevice.StateCallback {
        private String getErrorDescription(int errorCode) {
            switch (errorCode) {
                case CameraDevice.StateCallback.ERROR_CAMERA_DEVICE:
                    return "Camera device has encountered a fatal error.";
                case CameraDevice.StateCallback.ERROR_CAMERA_DISABLED:
                    return "Camera device could not be opened due to a device policy.";
                case CameraDevice.StateCallback.ERROR_CAMERA_IN_USE:
                    return "Camera device is in use already.";
                case CameraDevice.StateCallback.ERROR_CAMERA_SERVICE:
                    return "Camera service has encountered a fatal error.";
                case CameraDevice.StateCallback.ERROR_MAX_CAMERAS_IN_USE:
                    return "Camera device could not be opened because"
                            + " there are too many other open camera devices.";
                default:
                    return "Unknown camera error: " + errorCode;
            }
        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            checkIsOnCameraThread();
            final boolean startFailure = (captureSession == null) && (state != SessionState.STOPPED);
            state = SessionState.STOPPED;
            stopInternal();
            if (startFailure) {
                callback.onFailure(FailureType.DISCONNECTED, "Camera disconnected / evicted.");
            } else {
                events.onCameraDisconnected(Camera2Session.this);
            }
        }

        @Override
        public void onError(CameraDevice camera, int errorCode) {
            checkIsOnCameraThread();
            reportError(getErrorDescription(errorCode));
        }

        @Override
        public void onOpened(CameraDevice camera) {
            checkIsOnCameraThread();

            Log.d(TAG, "Camera opened.");
            cameraDevice = camera;

            int textureWidth = captureFormat.width;
            int textureHeight = captureFormat.height;
            if (textureWidth <= 0) {
                throw new IllegalArgumentException("Texture width must be positive, but was " + textureWidth);
            }
            if (textureHeight <= 0) {
                throw new IllegalArgumentException(
                        "Texture height must be positive, but was " + textureHeight);
            }
            surfaceTexture.setDefaultBufferSize(textureWidth, textureHeight);
            surface = new Surface(surfaceTexture);
            try {
                camera.createCaptureSession(
                        Arrays.asList(surface), new CaptureSessionCallback(), cameraThreadHandler);
            } catch (CameraAccessException e) {
                reportError("Failed to create capture session. " + e);
                return;
            }
        }

        @Override
        public void onClosed(CameraDevice camera) {
            checkIsOnCameraThread();

            Log.d(TAG, "Camera device closed.");
            events.onCameraClosed(Camera2Session.this);
        }
    }

    ;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private class CaptureSessionCallback extends CameraCaptureSession.StateCallback {
        @Override
        public void onConfigureFailed(CameraCaptureSession session) {
            checkIsOnCameraThread();
            session.close();
            reportError("Failed to configure capture session.");
        }

        @Override
        public void onConfigured(CameraCaptureSession session) {
            checkIsOnCameraThread();
            Log.d(TAG, "Camera capture session configured.");
            captureSession = session;
            try {
                /*
                 * The viable options for video capture requests are:
                 * TEMPLATE_PREVIEW: High frame rate is given priority over the highest-quality
                 *   post-processing.
                 * TEMPLATE_RECORD: Stable frame rate is used, and post-processing is set for recording
                 *   quality.
                 */
                final CaptureRequest.Builder captureRequestBuilder =
                        cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
                // Set auto exposure fps range.
                captureRequestBuilder.set(CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE,
                        new Range<Integer>(captureFormat.framerate.min / fpsUnitFactor,
                                captureFormat.framerate.max / fpsUnitFactor));
                captureRequestBuilder.set(
                        CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
                captureRequestBuilder.set(CaptureRequest.CONTROL_AE_LOCK, false);
                chooseStabilizationMode(captureRequestBuilder, false);
                chooseFocusMode(captureRequestBuilder);

                captureRequestBuilder.addTarget(surface);
                session.setRepeatingRequest(
                        captureRequestBuilder.build(), new CameraCaptureCallback(), cameraThreadHandler);
            } catch (CameraAccessException e) {
                reportError("Failed to start capture request. " + e);
                return;
            }

            listenForTextureFrames();

            Log.d(TAG, "Camera device successfully started.");
            callback.onDone(Camera2Session.this);
        }

        // Prefers optical stabilization over software stabilization if available. Only enables one of
        // the stabilization modes at a time because having both enabled can cause strange results.
        private void chooseStabilizationMode(CaptureRequest.Builder captureRequestBuilder, boolean enableSoftwareStabilization) {
            final int[] availableOpticalStabilization = cameraCharacteristics.get(
                    CameraCharacteristics.LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION);
            if (availableOpticalStabilization != null) {
                for (int mode : availableOpticalStabilization) {
                    if (mode == CaptureRequest.LENS_OPTICAL_STABILIZATION_MODE_ON) {
                        captureRequestBuilder.set(CaptureRequest.LENS_OPTICAL_STABILIZATION_MODE,
                                CaptureRequest.LENS_OPTICAL_STABILIZATION_MODE_ON);
                        captureRequestBuilder.set(CaptureRequest.CONTROL_VIDEO_STABILIZATION_MODE,
                                CaptureRequest.CONTROL_VIDEO_STABILIZATION_MODE_OFF);
                        Log.d(TAG, "Using optical stabilization.");
                        return;
                    }
                }
            }
            if(enableSoftwareStabilization) {
                // If no optical mode is available, try software.
                final int[] availableVideoStabilization = cameraCharacteristics.get(
                        CameraCharacteristics.CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES);
                for (int mode : availableVideoStabilization) {
                    if (mode == CaptureRequest.CONTROL_VIDEO_STABILIZATION_MODE_ON) {
                        captureRequestBuilder.set(CaptureRequest.CONTROL_VIDEO_STABILIZATION_MODE,
                                CaptureRequest.CONTROL_VIDEO_STABILIZATION_MODE_ON);
                        captureRequestBuilder.set(CaptureRequest.LENS_OPTICAL_STABILIZATION_MODE,
                                CaptureRequest.LENS_OPTICAL_STABILIZATION_MODE_OFF);
                        Log.d(TAG, "Using video stabilization.");
                        return;
                    }
                }
            }
            Log.d(TAG, "Stabilization not available.");
        }

        private void chooseFocusMode(CaptureRequest.Builder captureRequestBuilder) {
            final int[] availableFocusModes =
                    cameraCharacteristics.get(CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES);
            for (int mode : availableFocusModes) {
                if (mode == CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_VIDEO) {
                    captureRequestBuilder.set(
                            CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_VIDEO);
                    Log.d(TAG, "Using continuous video auto-focus.");
                    return;
                }
            }
            Log.d(TAG, "Auto-focus is not available.");
        }
    }

    ;

    private static class CameraCaptureCallback extends CameraCaptureSession.CaptureCallback {
        @Override
        public void onCaptureFailed(
                CameraCaptureSession session, CaptureRequest request, CaptureFailure failure) {
            Log.d(TAG, "Capture failed: " + failure);
        }
    }

    private void listenForTextureFrames() {
        setOnFrameAvailableListener(surfaceTexture, new SurfaceTexture.OnFrameAvailableListener() {
            @Override
            public void onFrameAvailable(SurfaceTexture surfaceTexture) {
                checkIsOnCameraThread();

                if (state != SessionState.RUNNING) {
                    Log.d(TAG, "Texture frame captured but camera is no longer running.");
                    return;
                }

                if (!firstFrameReported) {
                    firstFrameReported = true;
                    final int startTimeMs =
                            (int) TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - constructionTimeNs);
                    Log.d(TAG, "camera2StartTimeMs : " + startTimeMs);
                }

                final VideoFrame textureFrame = new VideoFrame(oesTextureId, surfaceTexture, isCameraFrontFacing, getFrameOrientation());
                events.onFrameCaptured(Camera2Session.this, textureFrame);
            }
        }, cameraThreadHandler);
    }

    @TargetApi(21)
    private static void setOnFrameAvailableListener(SurfaceTexture surfaceTexture,
                                                    SurfaceTexture.OnFrameAvailableListener listener, Handler handler) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            surfaceTexture.setOnFrameAvailableListener(listener, handler);
        } else {
            // The documentation states that the listener will be called on an arbitrary thread, but in
            // pratice, it is always the thread on which the SurfaceTexture was constructed. There are
            // assertions in place in case this ever changes. For API >= 21, we use the new API to
            // explicitly specify the handler.
            surfaceTexture.setOnFrameAvailableListener(listener);
        }
    }

    public static void create(CreateSessionCallback callback, Events events,
                              Context applicationContext, CameraManager cameraManager,
                              Handler workHandler, String cameraId, int width, int height,
                              int framerate) {
        new Camera2Session(callback, events, applicationContext, cameraManager, workHandler,
                cameraId, width, height, framerate);
    }

    private Camera2Session(CreateSessionCallback callback, Events events, Context applicationContext,
                           CameraManager cameraManager, Handler workHandler, String cameraId,
                           int width, int height, int framerate) {
        Log.d(TAG, "Create new camera2 session on camera " + cameraId);

        constructionTimeNs = System.nanoTime();

        this.cameraThreadHandler = workHandler;
        this.callback = callback;
        this.events = events;
        this.applicationContext = applicationContext;
        this.cameraManager = cameraManager;
        this.cameraId = cameraId;
        this.width = width;
        this.height = height;
        this.framerate = framerate;

        oesTextureId = GlUtil.generateTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES);
        surfaceTexture = new SurfaceTexture(oesTextureId);

        start();
    }

    private void start() {
        checkIsOnCameraThread();
        Log.d(TAG, "start");

        try {
            cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
        } catch (final CameraAccessException e) {
            reportError("getCameraCharacteristics(): " + e.getMessage());
            return;
        }
        cameraOrientation = cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
        isCameraFrontFacing = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING)
                == CameraMetadata.LENS_FACING_FRONT;

        findCaptureFormat();
        openCamera();
    }

    private void findCaptureFormat() {
        checkIsOnCameraThread();

        Range<Integer>[] fpsRanges =
                cameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES);
        fpsUnitFactor = Camera2Enumerator.getFpsUnitFactor(fpsRanges);
        List<CaptureFormat.FramerateRange> framerateRanges =
                Camera2Enumerator.convertFramerates(fpsRanges, fpsUnitFactor);
        List<Size> sizes = Camera2Enumerator.getSupportedSizes(cameraCharacteristics);
        Log.d(TAG, "Available preview sizes: " + sizes);
        Log.d(TAG, "Available fps ranges: " + framerateRanges);

        if (framerateRanges.isEmpty() || sizes.isEmpty()) {
            reportError("No supported capture formats.");
            return;
        }

        final CaptureFormat.FramerateRange bestFpsRange =
                CameraEnumerationAndroid.getClosestSupportedFramerateRange(framerateRanges, framerate, false);

        final Size bestSize = CameraEnumerationAndroid.getClosestSupportedSize(sizes, width, height);

        captureFormat = new CaptureFormat(bestSize.width, bestSize.height, bestFpsRange);
        Log.d(TAG, "Using capture format: " + captureFormat);
    }

    @SuppressLint("MissingPermission")
    private void openCamera() {
        checkIsOnCameraThread();

        Log.d(TAG, "Opening camera " + cameraId);
        events.onCameraOpening();

        try {
            cameraManager.openCamera(cameraId, new CameraStateCallback(), cameraThreadHandler);
        } catch (CameraAccessException e) {
            reportError("Failed to open camera: " + e);
            return;
        }
    }

    @Override
    public void stop() {
        Log.d(TAG, "Stop camera2 session on camera " + cameraId);
        checkIsOnCameraThread();
        if (state != SessionState.STOPPED) {
            final long stopStartTime = System.nanoTime();
            state = SessionState.STOPPED;
            stopInternal();
            final int stopTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - stopStartTime);
            Log.d(TAG, "camera2StopTimeMs : " + stopTimeMs);
        }
    }

    private void stopInternal() {
        Log.d(TAG, "Stop internal");
        checkIsOnCameraThread();

        if (captureSession != null) {
            captureSession.close();
            captureSession = null;
        }
        if (surface != null) {
            surface.release();
            surface = null;
        }
        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }

        GLES20.glDeleteTextures(1, new int[] {oesTextureId}, 0);
        surfaceTexture.release();

        Log.d(TAG, "Stop done");
    }

    private void reportError(String error) {
        checkIsOnCameraThread();
        Log.e(TAG, "Error: " + error);

        final boolean startFailure = (captureSession == null) && (state != SessionState.STOPPED);
        state = SessionState.STOPPED;
        stopInternal();
        if (startFailure) {
            callback.onFailure(FailureType.ERROR, error);
        } else {
            events.onCameraError(this, error);
        }
    }

    private int getFrameOrientation() {
        int rotation = CameraSession.getDeviceOrientation(applicationContext);
        if (!isCameraFrontFacing) {
            rotation = 360 - rotation;
        }
        return (cameraOrientation + rotation) % 360;
    }

    private void checkIsOnCameraThread() {
        if (Thread.currentThread() != cameraThreadHandler.getLooper().getThread()) {
            throw new IllegalStateException("Wrong thread");
        }
    }
}
