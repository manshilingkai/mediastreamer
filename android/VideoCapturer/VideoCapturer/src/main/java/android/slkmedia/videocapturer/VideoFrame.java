package android.slkmedia.videocapturer;

import android.graphics.SurfaceTexture;

public class VideoFrame {
    private int oesTextureId;
    private SurfaceTexture surfaceTexture;
    private boolean mirror;
    private int rotation;

    private byte[] data;
    private int width;
    private int height;
    private long timestampNs;

    public VideoFrame(int oesTextureId, SurfaceTexture surfaceTexture, boolean mirror, int rotation) {
        this.oesTextureId = oesTextureId;
        this.surfaceTexture = surfaceTexture;
        this.mirror = mirror;
        this.rotation = rotation;
    }

    public VideoFrame(byte[] data, int width, int height, int rotation, long timestampNs) {
        this.data = data;
        this.width = width;
        this.height = height;
        this.rotation = rotation;
        this.timestampNs = timestampNs;
    }
}
