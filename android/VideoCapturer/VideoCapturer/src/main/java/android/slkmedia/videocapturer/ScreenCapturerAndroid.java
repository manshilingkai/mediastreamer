package android.slkmedia.videocapturer;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.SurfaceTexture;
import android.hardware.SensorManager;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.os.Build;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.WindowManager;

import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;

/**
 * An implementation of VideoCapturer to capture the screen content as a video stream.
 * Capturing is done by {@code MediaProjection} on a {@code SurfaceTexture}.
 *
 * @note This class is only supported on Android Lollipop and above.
 */
@TargetApi(21)
public class ScreenCapturerAndroid implements VideoCapturer {
    private static final String TAG = "ScreenCapturerAndroid";

    private static final int DISPLAY_FLAGS =
            DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC | DisplayManager.VIRTUAL_DISPLAY_FLAG_PRESENTATION;
    // DPI for VirtualDisplay, does not seem to matter for us.
    private static final int VIRTUAL_DISPLAY_DPI = 400;

    private final Intent mediaProjectionPermissionResultData;
    private final MediaProjection.Callback mediaProjectionCallback;

    private int width;
    private int height;
    @Nullable private VirtualDisplay virtualDisplay;
    @Nullable private CapturerObserver capturerObserver;
    private long numCapturedFrames;
    @Nullable private MediaProjection mediaProjection;
    private boolean isDisposed;
    @Nullable private MediaProjectionManager mediaProjectionManager;
    @Nullable private Handler workHandler;
    @Nullable private Context applicationContext;

    private SurfaceTexture surfaceTexture;
    private int oesTextureId;

    OrientationEventListener orientationListener;

    private KeepAliveMonitor keepAliveMonitor;

    /**
     * Constructs a new Screen Capturer.
     *
     * @param mediaProjectionPermissionResultData the result data of MediaProjection permission
     *     activity; the calling app must validate that result code is Activity.RESULT_OK before
     *     calling this method.
     * @param mediaProjectionCallback MediaProjection callback to implement application specific
     *     logic in events such as when the user revokes a previously granted capture permission.
     **/
    public ScreenCapturerAndroid(Intent mediaProjectionPermissionResultData,
                                 MediaProjection.Callback mediaProjectionCallback) {
        this.mediaProjectionPermissionResultData = mediaProjectionPermissionResultData;
        this.mediaProjectionCallback = mediaProjectionCallback;
    }

    private void checkNotDisposed() {
        if (isDisposed) {
            throw new RuntimeException("capturer is disposed.");
        }
    }

    @Override
    public synchronized void initialize(final Handler workHandler, final Context applicationContext, final CapturerObserver capturerObserver) {
        checkNotDisposed();

        if (capturerObserver == null) {
            throw new RuntimeException("capturerObserver not set.");
        }
        this.capturerObserver = capturerObserver;

        if (workHandler == null) {
            throw new RuntimeException("workHandler not set.");
        }
        this.workHandler = workHandler;

        if (applicationContext == null) {
            throw new RuntimeException("applicationContext not set.");
        }
        this.applicationContext = applicationContext;

        mediaProjectionManager = (MediaProjectionManager) applicationContext.getSystemService(
                Context.MEDIA_PROJECTION_SERVICE);
    }

    @Override
    public synchronized void startCapture(final int width, final int height, final int ignoredFramerate) {
        checkNotDisposed();

        bestScreenCaptureConfig(width, height);

        mediaProjection = mediaProjectionManager.getMediaProjection(
                Activity.RESULT_OK, mediaProjectionPermissionResultData);

        // Let MediaProjection callback use the SurfaceTextureHelper thread.
        mediaProjection.registerCallback(mediaProjectionCallback, workHandler);

        oesTextureId = GlUtil.generateTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES);
        surfaceTexture = new SurfaceTexture(oesTextureId);

        createVirtualDisplay();
        capturerObserver.onCapturerStarted(true);
        listenForTextureFrames();
        keepAliveMonitor = new KeepAliveMonitor();
        workHandler.postDelayed(keepAliveMonitor, SCREEN_KEEP_ALIVE_PERIOD_MS);
        listenOrientationChange();
    }

    @Override
    public synchronized void stopCapture() {
        checkNotDisposed();
        orientationListener.disable();
        ThreadUtils.invokeAtFrontUninterruptibly(workHandler, new Runnable() {
            @Override
            public void run() {
                capturerObserver.onCapturerStopped();

                workHandler.removeCallbacks(keepAliveMonitor);

                surfaceTexture.setOnFrameAvailableListener(null);
                numCapturedFrames = 0;

                if (virtualDisplay != null) {
                    virtualDisplay.release();
                    virtualDisplay = null;
                }

                if (mediaProjection != null) {
                    // Unregister the callback before stopping, otherwise the callback recursively
                    // calls this method.
                    mediaProjection.unregisterCallback(mediaProjectionCallback);
                    mediaProjection.stop();
                    mediaProjection = null;
                }

                GLES20.glDeleteTextures(1, new int[] {oesTextureId}, 0);
                surfaceTexture.release();
            }
        });
    }

    @Override
    public synchronized void dispose() {
        isDisposed = true;
    }

    @Override
    public synchronized void changeCaptureFormat(final int width, final int height, final int ignoredFramerate) {
        checkNotDisposed();

        bestScreenCaptureConfig(width, height);

        if (virtualDisplay == null) {
            // Capturer is stopped, the virtual display will be created in startCaptuer().
            return;
        }

        // Create a new virtual display on the workHandler thread to avoid interference
        // with frame processing, which happens on the same thread (we serialize events by running
        // them on the same thread).
        ThreadUtils.invokeAtFrontUninterruptibly(workHandler, new Runnable() {
            @Override
            public void run() {
                virtualDisplay.release();
                createVirtualDisplay();
            }
        });
    }

    private void createVirtualDisplay() {
        int textureWidth = width;
        int textureHeight = height;
        if (textureWidth <= 0) {
            throw new IllegalArgumentException("Texture width must be positive, but was " + textureWidth);
        }
        if (textureHeight <= 0) {
            throw new IllegalArgumentException(
                    "Texture height must be positive, but was " + textureHeight);
        }
        surfaceTexture.setDefaultBufferSize(textureWidth, textureHeight);
        virtualDisplay = mediaProjection.createVirtualDisplay("SLKMedia_ScreenCapture", width, height,
                VIRTUAL_DISPLAY_DPI, DISPLAY_FLAGS, new Surface(surfaceTexture),
                null /* callback */, null /* callback handler */);
    }

    private void listenForTextureFrames() {
        numCapturedFrames = 0;
        setOnFrameAvailableListener(surfaceTexture, new SurfaceTexture.OnFrameAvailableListener() {
            @Override
            public void onFrameAvailable(SurfaceTexture st) {
                numCapturedFrames++;
                keepAliveMonitor.reportOneCapturedFrame();
                final VideoFrame textureFrame = new VideoFrame(oesTextureId, surfaceTexture, false, 0);
                capturerObserver.onFrameCaptured(textureFrame);
            }
        }, workHandler);
    }

    private void forceDeliverCachedTextureFrame() {
        workHandler.post(new Runnable() {
            @Override
            public void run() {
                if (numCapturedFrames > 0) {
                    final VideoFrame textureFrame = new VideoFrame(oesTextureId, surfaceTexture, false, 0);
                    capturerObserver.onFrameCaptured(textureFrame);
                }
            }
        });
    }

    private final static int SCREEN_KEEP_ALIVE_PERIOD_MS = 500;
    private class KeepAliveMonitor implements Runnable {
        private int numCapturedFramesPerPeriod = 0;

        public void reportOneCapturedFrame() {
            ++numCapturedFramesPerPeriod;
        }

        @Override
        public void run() {
            boolean isFreeze = numCapturedFramesPerPeriod == 0 ? true : false;
            numCapturedFramesPerPeriod = 0;
            if (isFreeze) {
                forceDeliverCachedTextureFrame();
            }
            workHandler.postDelayed(this, SCREEN_KEEP_ALIVE_PERIOD_MS);
        }
    }

    @TargetApi(21)
    private static void setOnFrameAvailableListener(SurfaceTexture surfaceTexture,
                                                    SurfaceTexture.OnFrameAvailableListener listener, Handler handler) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            surfaceTexture.setOnFrameAvailableListener(listener, handler);
        } else {
            // The documentation states that the listener will be called on an arbitrary thread, but in
            // pratice, it is always the thread on which the SurfaceTexture was constructed. There are
            // assertions in place in case this ever changes. For API >= 21, we use the new API to
            // explicitly specify the handler.
            surfaceTexture.setOnFrameAvailableListener(listener);
        }
    }

    @Override
    public boolean isScreencast() {
        return true;
    }

    public long getNumCapturedFrames() {
        return numCapturedFrames;
    }

    private DisplayMetrics getDisplayMetrics() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager =
                (WindowManager) applicationContext.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getRealMetrics(displayMetrics);
        return displayMetrics;
    }

    private void bestScreenCaptureConfig(int requestW, int requestH) {
        DisplayMetrics displayMetrics = getDisplayMetrics();

        if (displayMetrics == null) {
            return;
        }

        int screenWidth = displayMetrics.widthPixels;
        int screenHeight = displayMetrics.heightPixels;
        int densityDpi = displayMetrics.densityDpi;

        Size bestSize = VideoUtils.scaleSizeWithAlignment(screenWidth, screenHeight, requestW, requestH);

        this.width = bestSize.width;
        this.height = bestSize.height;

        Log.i(TAG, " best screen Capture config  W: " + this.width + " H: " + this.height
                + " Screen: dpi " + densityDpi + " W: " + screenWidth + " H: " + screenHeight + " requestW: " + requestW + " requestH: " + requestH);
    }

    private void listenOrientationChange() {
        orientationListener = new OrientationEventListener(applicationContext, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int rotation) {
                if (rotation == ORIENTATION_UNKNOWN) {
                    return;
                }

                int orientation = Configuration.ORIENTATION_UNDEFINED;
                if (rotation > 315 || rotation < 45) {
                    // PORTRAIT
                    orientation = Configuration.ORIENTATION_PORTRAIT;
                } else if (rotation >= 45 && rotation < 135) {
                    // RIGHT LANDSCAPE
                    orientation = Configuration.ORIENTATION_LANDSCAPE;
                } else if (rotation >= 135 && rotation < 225) {
                    // UPSIDE DOWN
                    orientation = Configuration.ORIENTATION_PORTRAIT;
                } else if (rotation >= 225 && rotation < 315) {
                    // LEFT LANDSCAPE
                    orientation = Configuration.ORIENTATION_LANDSCAPE;
                }

                switch (orientation) {
                    case Configuration.ORIENTATION_PORTRAIT:
                        if (width > height) {
                            changeCaptureFormat(width, height, 0);
                        }
                        break;
                    case Configuration.ORIENTATION_LANDSCAPE:
                        if (width < height) {
                            changeCaptureFormat(width, height, 0);
                        }
                        break;
                    default:
                        break;
                }
            }
        };

        orientationListener.enable();
    }

    }
