package android.slkmedia.bitratecontroller;

/** BitrateAdjuster that tracks bitrate and framerate but does not adjust them. */
public class BaseBitrateAdjuster implements BitrateAdjuster {
    protected int targetBitrateBps;
    protected double targetFramerateFps;
    @Override
    public void setTargets(int targetBitrateBps, double targetFramerateFps) {
        this.targetBitrateBps = targetBitrateBps;
        this.targetFramerateFps = targetFramerateFps;
    }
    @Override
    public void reportEncodedFrame(int size) {
        // No op.
    }
    @Override
    public int getAdjustedBitrateBps() {
        return targetBitrateBps;
    }
    @Override
    public double getAdjustedFramerateFps() {
        return targetFramerateFps;
    }
}
