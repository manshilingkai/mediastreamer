package android.slkmedia.bitratecontroller;

/**
 * BitrateAdjuster that adjusts the bitrate to compensate for changes in the framerate.  Used with
 * hardware codecs that assume the framerate never changes.
 */
public class FramerateBitrateAdjuster extends BaseBitrateAdjuster {
    private static final int DEFAULT_FRAMERATE_FPS = 30;
    @Override
    public void setTargets(int targetBitrateBps, double targetFramerateFps) {
        // Keep frame rate unchanged and adjust bit rate.
        this.targetFramerateFps = DEFAULT_FRAMERATE_FPS;
        this.targetBitrateBps = (int) (targetBitrateBps * DEFAULT_FRAMERATE_FPS / targetFramerateFps);
    }
}
