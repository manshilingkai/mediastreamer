package android.slkmedia.bitratecontroller;

/** Object that adjusts the bitrate of a hardware codec. */
public interface BitrateAdjuster {
    /**
     * Sets the target bitrate in bits per second and framerate in frames per second.
     */
    void setTargets(int targetBitrateBps, double targetFramerateFps);

    /**
     * Should be used to report the size of an encoded frame to the bitrate adjuster. Use
     * getAdjustedBitrateBps to get the updated bitrate after calling this method.
     */
    void reportEncodedFrame(int size);

    /** Gets the current bitrate. */
    int getAdjustedBitrateBps();

    /** Gets the current framerate. */
    double getAdjustedFramerateFps();
}
